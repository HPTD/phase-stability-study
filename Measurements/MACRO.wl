(* ::Package:: *)

BeginPackage["MACRO`"];

xBBlue::usage = "blue intense color";
xGGreen::usage = "green intense color";
xYYellow::usage = "yellow intense color";
xRRed::usage = "red intense color";

xBlue::usage = "blue color";
xYellow::usage = "yellow color";
xGreen::usage = "green color";
xRed::usage = "red color";
aRRed::usage = "dark red color for ambientTemp";

sBlue::usage = "standard blue"
sYellow::usage = "standard yellow"
sGreen::usage = "standard green"
sRed::usage = "standard red"

Begin["`Private`"];

xBBlue  = RGBColor[0.1,0.15,0.8];
xGGreen  = RGBColor[0.1,0.8,0.1];
xYYellow = RGBColor[0.98,0.85,0.25];
xRRed    = RGBColor[0.95,0.15,0.15];
aRRed    = RGBColor[0.65,0.1,0.95,0.15];

xBlue   = RGBColor[0.4,0.7,0.95];
xYellow = RGBColor[0.9,0.8,0.2];
xGreen  = RGBColor[0.4,0.8,0.4];
xRed    = RGBColor[0.8,0.5,0.5];

sBlue   = ColorData[97,First@#1];
sYellow = ColorData[97,First@#2];
sGreen  = ColorData[97,First@#3];
sRed    = ColorData[97,First@#4];

End[];

dmtdErrorAtResetN::usage = "gives the dmtd measure, the previous measure for phase adj. 
and the jump between the two.";
Begin["`Private`"];
dmtdErrorAtResetN[n_,m_,shiftx_,dmtdx_,dr_,stepx_,done_]:=Block[{
meas0=shiftx[[m*n+1]],
meas1=dmtdx[[m*n+1]]/10^-12,
meas1Mod=Mod[dmtdx[[m*n+1]]/10^-12,1/dr*10^12],
jump=meas0-meas1Mod,
shift=(stepx*done)[[m*n+1]]/(dr*64)/(10^-12)
},
nPad=StringPadRight[ToString[n],2];
meas0Pad=StringPadRight[ToString[N[NumberForm[meas0,{10,1}]]],7];
meas1Pad=StringPadRight[ToString[N[NumberForm[meas1,{10,1}]]],10];
jumpSign=If[jump>=0,"+",""];
jumpPad=StringPadRight[jumpSign~~ToString[N[NumberForm[jump,{10,1}]]],7];
nPad~~" -> dmtd meas for adj (MOD): "~~meas0Pad~~"dmtd next meas: "~~meas1Pad~~
"jump: "~~jumpPad~~"shift: "~~ToString[N[NumberForm[shift,{10,1}]]]
]
End[];

Histo::usage = "Histogram with legends, takes: 
lists of data lists, 
lists of colors, 
list of names, 
title,
grouping of the legends (Row or Column).";

Begin["`Private`"];

Histo[data_,colors_,names_,title_,time_,limit_]:=Block[{dataR,range,mapLegend,legend,bigData,pkpkBins,maxBin,len,bins,legendNames,legendNamesPad},

legendNames={"\[Sigma]: ","pkpk: ","points: ","time: "};
legendNamesPad=StringPadRight[legendNames];
len=Length[data[[1]]];
(*
bins=IntegerPart[Which[len>1000,50, 500<len<=1000,35, 200<len<=500,20, 150<len<=200,10, 50<len<=150,8, 20<len<=50,5, 10<len<=20,len/4, 4<len<=10,len/2, len<=4,len]];
*)

bigData=Length[data]>limit;
pkpkBins=Abs[#1-#2]&@@(QuantityMagnitude@MinMax[data]);
maxBin=Which[pkpkBins<=5,25, 5<pkpkBins<=9,50, pkpkBins>9,60, pkpkBins>13,70];
bins=IntegerPart[Which[len>=500,maxBin, 200<len<500,len/10, 100<len<=200,len/5, 40<len<=100,20, 20<len<=40,len/2, 10<len<=20,10, len<=10,len]];

dataR=Max[(Max[#1]-Min[#1])&/@data];
range=If[dataR<=20,10,If[dataR>15,dataR+1,13]];

legend=
MapIndexed[
Framed[
Column[
{
Style[First@names[[#2]],Bold,15],
Style[legendNames[[1]]~~ToString[NumberForm[N[StandardDeviation[#1]],{10,2}]] ~~" ps",15],
Style[legendNames[[2]]~~ToString[NumberForm[Max[#1]-Min[#1],{10,2}]] ~~" ps",15],
Style[legendNames[[3]]~~ToString[NumberForm[Length[#1],{10,2}]],15],
Style[legendNames[[4]]~~StringReplace[ToString[NumberForm[
DateDifference[First[time],Last[time], {"Hours","Minutes"}],{10,0}
]],{"hours"->"h","minutes"->"m","."->""},3],15]
},Spacings->1
],RoundingRadius->5,Background->Opacity[0.3,colors[[First@#2]]]]&,data
];

mapLegend[range_]:=Row[legend[[range]],"     "];

Histogram[data,bins,
"ProbabilityDensity",AxesLabel->{"\[CapitalDelta]\[CurlyPhi] [ps]","P(\[CapitalDelta]\[CurlyPhi])"},LabelStyle->{FontSize->12, Black},ChartStyle->colors,
PlotRange->{{-range,range},Automatic},Ticks->{Range[-range,range,2],Automatic},
PlotLabel-> Column[
If[Length[title]>1,
{Style[title[[1]],Bold,Black,FontSize->16],
Style[title[[2]],Italic,Black,FontSize->14]}
,{Style[title[[1]],Bold,Black,FontSize->16]}
],Alignment->Center],
ImageMargins -> 10,
AxesStyle->Directive[{Black,Arrowheads[0.015],15}],ImagePadding -> {{Scaled[.02], Scaled[.05]},{Scaled[.005], Scaled[.025]}},
PlotRangePadding->Scaled[.05],ImageSize-> 900,
Epilog->Inset[
Column[
If[bigData,
{
mapLegend[;;limit],
mapLegend[limit+1;;]
}
,
{mapLegend[;;]}
],Alignment->Right,Spacings->0.8
],
Scaled[{If[Length[data]>2,0.75,0.8], If[bigData,0.78,0.9]}]]
]
]

End[];

Trend::usage="Trend creates a trend with the date of each data on the x axis, it takes:
lists of data lists, 
lists of colors, 
list of names, 
title,
list of times,
y axix Automatic range: if False it is {-10,10}.";
Begin["`Private`"];

Trend[data_,colors_,names_,title_,time_,temp_,ambientTemp_,resets_,autoY_,y01_,sizeM_]:=Block[{maxT,maxAT,styleS,style,sizes,circle,wantJump,wantMax,adjAT,pkpkAT,splitPadName,TstatsSplit,legend1,legend2,legends,allLegend,appendAT,allData0,allData,allColors0,allColors,xTime,shortDayF,shortDayL,shortDay,shine,myRed,rstW, hasAmbT,listR,linesR,totalOffset,dn,offsetT,pkpkData,pkpkT,pkpkPad,stdData,stdT,stdPad,max,backLegendOpacity,customLegend,dataStats,length,xAxisN,xAxisTicks,shortDate,xAxisTimeTicks,rangeY,gridY,meanTemp,tempAxisTicks,hasT,padNames,pkpkPadT,wantPkPk},

shine=ContainsAny[colors,{xBBlue,xGGreen,xYellow}];
myRed=If[shine,xRRed,sRed];
hasT=Or[Length[temp]>0,Length[ambientTemp]>0];
appendAT=Length[ambientTemp]>0;
dn=Length[data];

totalOffset=Which[dn>=5,-2, dn==4,-2, 3<=dn<4,-2, dn<3,0];
offsetT=If[totalOffset==0,Which[dn>=4,3, 3<=dn<4,3, dn<3,4],4];

length=Length[data[[1]]];
xAxisN=IntegerPart[length/8];
xAxisTicks=Range[0,length,xAxisN];
shortDate=DateString[#,{"Hour",":","Minute"}]&/@time;
shortDayF=DateString[First[time],{"DayNameShort"," ","Day","/","Month","/","YearShort"}];
shortDayL=DateString[Last[time],{"DayNameShort"," ","Day","/","Month","/","YearShort"}];
shortDay=If[shortDayF==shortDayL,shortDayF,shortDayF~~" - "~~shortDayL];
(*xTime=Row[{Style[shortDay,Transparent,13],Style["  Time  ",16],Style[shortDay,13]}];*)
xTime=Style[shortDay,16];
xAxisTimeTicks=Transpose@{xAxisTicks,shortDate[[#]]&/@Replace[xAxisTicks,0->1,1]};
gridY=If[autoY,Automatic,Range[y01[[1]]-totalOffset,y01[[2]]-totalOffset, 2]];
rangeY=If[autoY,All,{y01[[1]]-totalOffset,y01[[2]]-totalOffset}];

padNames=StringPadRight[names];

stdData=(ToString[NumberForm[N[StandardDeviation[#]],{10,2}]] ~~" ps")&/@data;
stdPad=StringPadLeft[stdData];

wantPkPk[s_]:=StringEndsQ[ToLowerCase[s],{" "}];
wantMax[s_]:=StringEndsQ[ToLowerCase[s],{"."}];
wantJump[s_]:=Not[And[wantPkPk[s],wantMax[s]]];
pkpkData=(Which[wantPkPk[#[[2]]],ToString[NumberForm[Max[#[[1]]]-Min[#[[1]]],{10,2}]] ~~" ps",
wantMax[#[[2]]],ToString[NumberForm[Max[Abs[#[[1]]]],{10,2}]] ~~" ps",
wantJump[#[[2]]],ToString[NumberForm[Max[Abs[Differences[#[[1]]]]],{10,2}]] ~~" ps"
])&/@Transpose@{data,names};
pkpkT=If[hasT,ToString[NumberForm[Max[temp]-Min[temp],{10,2}]]~~"\[Degree]C ",""];
pkpkPad=StringPadLeft[pkpkData];

pkpkAT=If[appendAT,ToString[NumberForm[Max[ambientTemp]-Min[ambientTemp],{10,2}]]~~"\[Degree]C ",""];
pkpkPadT=StringPadLeft[If[appendAT,Append[{pkpkT},pkpkAT],{pkpkT}]];

meanTemp=IntegerPart[Mean[temp]];
adjAT=meanTemp-Mean[temp];
tempAxisTicks=Range[meanTemp+(y01[[1]]-totalOffset),meanTemp+(y01[[2]]-totalOffset),2]-offsetT;
splitPadName=StringPadRight[If[appendAT,Append[{"Die Temp"},"Temp+"~~ToString[NumberForm[-Mean[ambientTemp]+Mean[temp]+1.5,{10,1}]]],{"Die temp"}]];
TstatsSplit={If[hasT,Style[splitPadName[[1]]~~"  pkpk = "~~pkpkPadT[[1]],14,FontFamily->"Courier"],""],
If[appendAT,Style[splitPadName[[2]]~~"  pkpk = "~~pkpkPadT[[2]],14,FontFamily->"Courier"],""]
};

dataStats={padNames,stdPad,pkpkPad,(Which[wantPkPk[#],"pkpk",wantMax[#],"max ",wantJump[#],"jump"]&/@names)};

customLegend=Style[#[[1]]~~"  \[Sigma] = "~~#[[2]]~~"  "~~#[[4]]~~" = "~~#[[3]],14,FontFamily->"Courier"]&/@Transpose@dataStats;

maxT=If[Length[temp]>0,Max[temp[[Floor[Length[temp]/2];;]]]-meanTemp+offsetT,0];
maxAT=If[appendAT,Max[ambientTemp[[Floor[Length[ambientTemp]/2];;]]]-Mean[ambientTemp]+offsetT-adjAT+1.5,0];
max=Max[{Max[#[[Floor[length/2];;]]&/@data],maxT,maxAT}];
backLegendOpacity=If[And[max>6.4,Length[data]>4],Directive[Opacity[0.75], White],White];

listR=Min[#]&/@(Position[resets,#]&/@DeleteDuplicates[resets]);
linesR=Line[{{#,y01[[1]]-totalOffset},{#,y01[[2]]-totalOffset}}]&/@listR;
rstW=Which[Length[linesR]>=60,AbsoluteThickness[0.8],20<Length[linesR]<60,AbsoluteThickness[1.5],Length[linesR]<=20,Thick];

allData0=If[appendAT,Insert[data,ambientTemp-Mean[ambientTemp]+offsetT-adjAT+1.5,1],data];
allData=If[hasT,Insert[allData0,temp-meanTemp+offsetT,1],allData0];
allColors0=If[appendAT,Insert[colors,aRRed,1],colors];
allColors=If[hasT,Insert[allColors0,myRed,1],allColors0];
sizes=ConstantArray[sizeM,Length[allColors]];
styleS=Directive[#[[1]],PointSize[Scaled[#[[2]]/1000]]]&/@Transpose@{allColors,sizes};
style=If[sizeM>0,styleS,allColors];

legend1=Placed[LineLegend[colors,
customLegend,
LegendFunction->(Framed[#,Background->backLegendOpacity,RoundingRadius->5]&),
LegendMarkerSize->13,Spacings->0.4
],{Right,Top}];

legend2=Placed[LineLegend[If[appendAT,Append[{myRed},aRRed],{myRed}],
TstatsSplit,
LegendFunction->(Framed[#,Background->backLegendOpacity,RoundingRadius->5]&),
LegendMarkerSize->13,Spacings->0.4
],{Left,Top}];

legends=If[hasT,Append[{legend1},legend2],{legend1}];

circle = Graphics`PlotMarkers[][[All, 1]][[1]]; (* \[FilledCircle] *)

Show[
ListPlot[allData,

PlotStyle->style,PlotTheme->"Detailed",
FrameLabel->{{Style["\[CapitalDelta]\[CurlyPhi] [ps]",16],If[hasT&&!autoY,Style["Temperature [\[Degree]C]",16],None]},{xTime,None}},
LabelStyle->{FontSize->13, Black},
GridLines->{Automatic,gridY},
FrameTicks->{{gridY,If[hasT&&!autoY,Transpose@{gridY,tempAxisTicks},None]},{xAxisTimeTicks,None}},
PlotRange->{If[True,Automatic,{0,length}],rangeY},

PlotLegends->legends

], 

Graphics[{Directive[{rstW, Black, Dashed}], linesR}],

PlotLabel-> Column[
If[Length[title]>1,
{Style[title[[1]],Bold,Black,FontSize->16],
Style[title[[2]],Italic,Black,FontSize->14]}
,{Style[title[[1]],Bold,Black,FontSize->16]}
],Alignment->Center],
ImagePadding -> {{Automatic,Automatic},{Automatic,8}},
ImageMargins -> 10,ImageSize->{900,500},AspectRatio->500/1000
]
]

End[];

DynamicTrend::usage = "DynamicTrend creates an interactive trend where a slider can be used to select an interval of data. The trend has trend the date of each data point on the x axis, it takes:
lists of data lists, 
lists of colors, 
list of names, 
title,
list of times,
y axix Automatic range: if False it is {-10,10}.";

Begin["`Private`"];

DynamicTrend[data_,colors_,names_,title_,time_,temp_,ambientTemp_,resets_,autoY_,wantH_]:=DynamicModule[{allLegend0,allLegend,appendAT,allData0,allData,allColors0,allColors,xTime,shortDay,shortDayF,shortDayL,myRed,shine,rstW,listR,linesR,totalOffset,Hcolors,dn,offsetT,pkpkData,pkpkT,pkpkPad,pkpkTempPad,stdData,stdT,stdPad,max,backLegendOpacity,dataStats,customLegend,dynSel,xmin,xmax,fullY,int,length,xAxisN,xAxisTicks,shortDate,xAxisTimeTicks,gridY,rangeY,meanTemp,tempAxisTicks,hasT,Tstats,padNames,dynTemp,adjAT,splitPadName,TstatsSplit,pkpkAT,pkpkPadT,legend1,legend2,legends},

shine=ContainsAny[colors,{xBBlue,xGGreen,xYellow}];
myRed=If[shine,xRRed,sRed];
dynSel[val_,dynInt_]:=val[[If[dynInt[[1]]==0,1,dynInt[[1]]];;dynInt[[2]]]];
hasT=Or[Length[temp]>0,Length[ambientTemp]>0];
appendAT=Length[ambientTemp]>0;

Hcolors=Which[#==sBlue,xBlue, #==sGreen,xGreen, #==sYellow,xYellow, #==sRed,xRed, True,#]&/@colors;
dn=Length[data];
totalOffset=Which[dn>=5,-2, dn==4,-2, 3<=dn<4,-2, dn<3,0];
offsetT=If[totalOffset==0,Which[dn>=4,3, 3<=dn<4,3, dn<3,4],4];

length[h_]:=Length[dynSel[data[[1]],h]];
xAxisN[g_]:=If[IntegerPart[length[g]/8]>0,IntegerPart[length[g]/8],1];
xAxisTicks[q_]:=Range[q[[1]],q[[2]],xAxisN[q]];
shortDate=DateString[#,{"Hour",":","Minute"}]&/@time;
shortDayF=DateString[First[time],{"DayNameShort"," ","Day","/","Month","/","YearShort"}];
shortDayL=DateString[Last[time],{"DayNameShort"," ","Day","/","Month","/","YearShort"}];
shortDay=If[shortDayF==shortDayL,shortDayF,shortDayF~~" - "~~shortDayL];
(*xTime=Row[{Style[shortDay,Transparent,13],Style["  Time  ",16],Style[shortDay,13]}];*)
xTime=Style[shortDay,16];
xAxisTimeTicks[f_]:=Transpose@{xAxisTicks[f],shortDate[[#]]&/@Replace[xAxisTicks[f],0->1,1]};
gridY=If[autoY,Automatic,Range[(-10-totalOffset),(10-totalOffset), 2]];
fullY=QuantityMagnitude@MinMax[data];
rangeY=If[autoY,Automatic,{(-10-totalOffset),(10-totalOffset)}];
{xmin,xmax}={0,Length[data[[1]]]};

padNames=StringPadRight[names];

stdData[i_]:=(ToString[NumberForm[N[StandardDeviation[#]],{10,2}]] ~~" ps")&/@(dynSel[#,i]&/@data);
stdPad[i_]:=StringPadLeft[stdData[i]];

pkpkData[i_]:=(ToString[NumberForm[Max[Abs[Differences[#]]],{10,2}]] ~~" ps")&/@(dynSel[#,i]&/@data);
pkpkT[i_]:=If[hasT,ToString[NumberForm[Max[dynSel[temp,i]]-Min[dynSel[temp,i]],{10,2}]]~~"\[Degree]C ",""];
pkpkPad[i_]:=StringPadLeft[pkpkData[i]];

pkpkAT[i_]:=If[appendAT,ToString[NumberForm[Max[dynSel[ambientTemp,i]]-Min[dynSel[ambientTemp,i]],{10,2}]]~~"\[Degree]C ",""];
pkpkPadT[i_]:=StringPadLeft[If[appendAT,Append[{pkpkT[i]},pkpkAT[i]],{pkpkT[i]}]];

meanTemp=IntegerPart[Mean[temp]];
adjAT=meanTemp-Mean[temp];
tempAxisTicks=Range[meanTemp+(-10-totalOffset),meanTemp+(10-totalOffset),2]-offsetT;
splitPadName=StringPadRight[If[appendAT,Append[{"Die Temp"},"Temp+"~~ToString[NumberForm[-Mean[ambientTemp]+Mean[temp]+1.5,{10,1}]]],{"Die temp"}]];
TstatsSplit[i_]:={If[hasT,Style[splitPadName[[1]]~~"  pkpk = "~~pkpkPadT[i][[1]],14,FontFamily->"Courier"],""],
If[appendAT,Style[splitPadName[[2]]~~"  pkpk = "~~pkpkPadT[i][[2]],14,FontFamily->"Courier"],""]
};

dataStats[i_]:={padNames,stdPad[i],pkpkPad[i]};

customLegend[i_]:=Style[#[[1]]~~"  \[Sigma] = "~~#[[2]]~~"  jump = "~~#[[3]],15,FontFamily->"Courier"]&/@Transpose@dataStats[i];

max=QuantityMagnitude@Max[data];
backLegendOpacity=If[max>6,Directive[Opacity[0.75], White],White];

listR=Min[#]&/@(Position[resets,#]&/@DeleteDuplicates[resets]);
linesR=Line[{{#,-10-totalOffset},{#,10-totalOffset}}]&/@listR;
rstW=If[Length[linesR]>60,AbsoluteThickness[0.5],Thick];

allData0=If[appendAT,Insert[data,ambientTemp-Mean[ambientTemp]+offsetT-adjAT+1.5,1],data];
allData=If[hasT,Insert[allData0,temp-meanTemp+offsetT,1],allData0];
allColors0=If[appendAT,Insert[colors,aRRed,1],colors];
allColors=If[hasT,Insert[allColors0,myRed,1],allColors0];

legend1[i_]:=Placed[LineLegend[colors,
customLegend[i],
LegendFunction->(Framed[#,Background->backLegendOpacity,RoundingRadius->5]&),
LegendMarkerSize->13,Spacings->0.4
],{Right,Top}];

legend2[i_]:=Placed[LineLegend[If[appendAT,Append[{myRed},aRRed],{myRed}],
TstatsSplit[i],
LegendFunction->(Framed[#,Background->backLegendOpacity,RoundingRadius->5]&),
LegendMarkerSize->13,Spacings->0.4
],{Left,Top}];

legends[i_]:=If[hasT,Append[{legend1[i]},legend2[i]],{legend1[i]}];

Column[{
Dynamic[
Show[
ListPlot[allData,

PlotStyle->allColors,PlotTheme->"Detailed",
FrameLabel->{{Style["\[CapitalDelta]\[CurlyPhi] [ps]",16],If[hasT&&!autoY,Style["Temperature [\[Degree]C]",16],None]},{xTime,None}},
LabelStyle->{FontSize->13, Black},
GridLines->{Automatic,gridY},
FrameTicks->{{gridY,If[hasT&&!autoY,Transpose@{gridY,tempAxisTicks},None]},{xAxisTimeTicks[int],None}},

PlotLegends->legends[int]
],

Graphics[{Directive[{rstW, Black, Dashed}], linesR}],

PlotRange->{int,rangeY},
PlotLabel-> Column[
If[Length[title]>1,
{Style[title[[1]],Bold,Black,FontSize->16],
Style[title[[2]],Italic,Black,FontSize->14]}
,{Style[title[[1]],Bold,Black,FontSize->16]}
],Alignment->Center],
ImagePadding -> {{Automatic,Automatic},{Automatic,5}},
ImageMargins -> 10,ImageSize->{900,500},AspectRatio->500/1000
]],

IntervalSlider[Dynamic[int],{xmin,xmax,1},ImageSize -> {900,30},MinIntervalSize -> 1,Method->"Push"],

ListPlot[data,Frame ->False,Axes -> False,ImageSize -> {900,30},AspectRatio->30/900,
PlotStyle->If[hasT,Append[colors,sRed],colors],
Epilog->{Opacity[0.3],Orange,Dynamic[Rectangle@@Thread[{int,fullY}]]}],

If[wantH,
Dynamic[Histo[dynSel[#,int]&/@data,Hcolors,names,StringReplace[title,"trend"->"distribution"],dynSel[time,int],4]],""]

}]
]

End[];

SelTrend::usage = "Trend with selection";

Begin["`Private`"];

SelTrend[data_,colors_,names_,title_,time_,temp_,resets_,autoY_]:=
DynamicModule[{n=Length[colors],en,opacityList,dataO,colorsO,namesO},
opacityList=True&/@colors;
dataO[o1_]:=If[ContainsAny[o1,{False}],Delete[data,Position[o1,False]],data];
colorsO[o2_]:=If[ContainsAny[o2,{False}],Delete[colors,Position[o2,False]],colors];
namesO[o3_]:=If[ContainsAny[o3,{False}],Delete[names,Position[o3,False]],names];
en[o4_]:=Count[o4,True]>1;
Column[{
Row[Insert[#&/@
Table[With[{i = i},
Row[{Checkbox[Dynamic[opacityList[[i]]],Enabled->Dynamic[!opacityList[[i]]||en[opacityList]]],Spacer[5],names[[i]],Spacer[40]}]
]
,{i, n}]
,Spacer[20],1]],
Dynamic[Trend[dataO[opacityList],colorsO[opacityList],namesO[opacityList],title,time,temp,resets,autoY]]
}]

]

End[];

SelHisto::usage = "Histogram with selection";

Begin["`Private`"];

SelHisto[data_,colors_,names_,title_,time_,Grouping_]:=
DynamicModule[{n=Length[colors],en,opacityList,dataO,colorsO,namesO},
opacityList=True&/@colors;
dataO[o1_]:=If[ContainsAny[o1,{False}],Delete[data,Position[o1,False]],data];
colorsO[o2_]:=If[ContainsAny[o2,{False}],Delete[colors,Position[o2,False]],colors];
namesO[o3_]:=If[ContainsAny[o3,{False}],Delete[names,Position[o3,False]],names];
en[o4_]:=Count[o4,True]>1;
Column[{
Row[Insert[#&/@
Table[With[{i = i},
Row[{Checkbox[Dynamic[opacityList[[i]]],Enabled->Dynamic[!opacityList[[i]]||en[opacityList]]],Spacer[5],names[[i]],Spacer[40]}]
]
,{i, n}]
,Spacer[20],1]],
Dynamic[Histo[dataO[opacityList],colorsO[opacityList],namesO[opacityList],title,time,Grouping]]
}]

]

End[];

DynamicSelTrend::usage = "Dynamic Trend & Histogram with selection. TOO SLOW due to nested dynamics";

Begin["`Private`"];

DynamicSelTrend[data_,colors_,names_,title_,time_,temp_,resets_,autoY_]:=
DynamicModule[{n=Length[colors],en,opacityList,dataO,colorsO,namesO},
opacityList=True&/@colors;
dataO[o1_]:=If[ContainsAny[o1,{False}],Delete[data,Position[o1,False]],data];
colorsO[o2_]:=If[ContainsAny[o2,{False}],Delete[colors,Position[o2,False]],colors];
namesO[o3_]:=If[ContainsAny[o3,{False}],Delete[names,Position[o3,False]],names];
en[o4_]:=Count[o4,True]>1;
Column[{
Row[Insert[#&/@
Table[With[{i = i},
Row[{Checkbox[Dynamic[opacityList[[i]]],Enabled->Dynamic[!opacityList[[i]]||en[opacityList]]],Spacer[5],names[[i]],Spacer[40]}]
]
,{i, n}]
,Spacer[20],1]],
(*Dynamic[*)
DynamicTrend[dataO[opacityList],colorsO[opacityList],namesO[opacityList],title,time,temp,resets,autoY]
(*]*)
}]

]

End[];

CorrelationPlot::usage = "ListPlot correlating 2 lists of data, takes: 
lists of data lists, 
list of names, 
upper value of plot range.";

Begin["`Private`"];

CorrelationPlot[data_,names_,max_]:=ListPlot[
Transpose[data],
PlotRange->{{-max,max},{-max,max}},
PlotLabel-> Style["Correlation plot of "~~names[[1]]~~" & "~~names[[2]],Bold,Black,FontSize->16],
AxesLabel->{"\[CapitalDelta]\[CurlyPhi]2 [ps]","\[CapitalDelta]\[CurlyPhi]1 [ps]"},
LabelStyle->{FontSize->12, Black},
AxesStyle->Directive[{Black,Arrowheads[0.02],15}],
PlotRangePadding->{{Scaled[.05], Scaled[.05]},{Scaled[.05], Scaled[.07]}},
ImagePadding -> {{Scaled[.01], Scaled[.07]},{Scaled[.005], Scaled[.03]}},
ImageMargins -> 10,
ImageSize-> 650,
Epilog->Inset[
Framed[
Style["Correlation: "~~ToString[NumberForm[Correlation[data[[1]],data[[2]]],{10,2}]],15],
RoundingRadius->5,Background->Opacity[0.3,Yellow]
],
Scaled[{0.15, 0.95}]
 ]
]

End[];

EndPackage[];

MultiTrend::usage="MultiTrend creates a multiple axix trend with the date of each data on the x axis, it takes:
lists of data lists, 
lists of colors, 
list of names, 
title,
list of times,
y axix Automatic range: if False it is {-10,10}.";
Begin["`Private`"];

MultiTrend[data_,colors_,names_,title_,time_,error_,autoY_]:=Block[{axisValsY,axisValsY0,axisValsE,swingEin,rangeE,axisValsE0,swingin,shine,myRed,rstW,listR,linesR,pkpkData,pkpkE,pkpkPad,pkpkDataPad,pkpkErrorPad,stdData,stdPad,stdDataPad,stdTempPad,max,backLegendOpacity,customLegend,dataStats,length,xAxisN,xAxisTicks,shortDate,xAxisTimeTicks,rangeY,gridY,hasE,Estats,padNames,dataNames,gridE},

shine=ContainsAny[colors,{xBBlue,xGGreen,xYellow}];
myRed=If[shine,xRRed,sRed];
hasE=Length[error]>0;

length=Length[data[[1]]];
xAxisN=IntegerPart[length/8];
xAxisTicks=Range[0,length,xAxisN];
shortDate=DateString[#,{"Hour",":","Minute"}]&/@time;
xAxisTimeTicks=Transpose@{xAxisTicks,shortDate[[#]]&/@Replace[xAxisTicks,0->1,1]};

swingin=Ceiling[Max[data]-Min[data]];
rangeY=If[swingin>20 || autoY,Ceiling[swingin/2],10];
axisValsY0=Range[-rangeY,rangeY,rangeY/5];
axisValsY=If[rangeY/5==Round[rangeY/5] && rangeY==Round[rangeY],axisValsY0,N[axisValsY0]];
gridY=If[rangeY/5==Round[rangeY/5] && rangeY==Round[rangeY],axisValsY,Transpose[{axisValsY,NumberForm[#,{10,1}]&/@axisValsY}]];

swingEin=If[hasE,Ceiling[Max[error]-Min[error]]*2,0];
rangeE=If[hasE,If[swingin>20 && swingEin>20,Ceiling[swingEin/2],10],0];
axisValsE0=If[hasE,Range[-rangeE,rangeE,rangeE/5],0];
axisValsE=If[hasE,If[rangeE/5==Round[rangeE/5] && rangeE==Round[rangeE],axisValsE0,NumberForm[#,{10,1}]&/@N[axisValsE0]],0];
gridE=If[hasE,Transpose[{axisValsY, axisValsE}],0];
(*rangeY=If[autoY,Range[-autoRangeY,autoRangeY],{-10-totalOffset,10-totalOffset}];*)
(*Print[{swingin,rangeY,gridY,rangeE,axisValsE}];*)

padNames=StringPadRight[If[hasE,Append[names,"Error"],names]];
dataNames=If[hasE,Drop[padNames,-1],padNames];

pkpkData=(ToString[NumberForm[Max[#]-Min[#],{10,2}]] ~~" ps")&/@data;
pkpkE=If[hasE,ToString[NumberForm[Max[Abs[Max[error]],Abs[Min[error]]],{10,2}]]~~" ps",""];
pkpkPad=StringPadLeft[If[hasE,Flatten[{pkpkData,pkpkE},1],pkpkData]];
pkpkDataPad=If[hasE,Drop[pkpkPad,-1],pkpkPad];
pkpkErrorPad=If[hasE,Last[pkpkPad],""]; (* in realt\[AGrave] \[EGrave] max, non pkpk, per l'errore*)

Estats=If[hasE,Style[Last[padNames]~~"  max  = "~~pkpkErrorPad,15,FontFamily->"Courier"],""];
dataStats={dataNames,pkpkDataPad};
customLegend=Style[#[[1]]~~"  pkpk = "~~#[[2]],15,FontFamily->"Courier"]&/@Transpose@dataStats;

max=Max[If[hasE,Flatten[{data,error},1],data]];
backLegendOpacity=If[max>6,Directive[Opacity[0.75], White],White];

Show[
ListPlot[If[hasE,Append[data,error*rangeY/rangeE],data],
PlotStyle->If[hasE,Append[colors,myRed],colors],PlotTheme->"Detailed",
FrameLabel->{{Style["\[CapitalDelta]\[CurlyPhi] [ps]",16],If[hasE,Style["Error [ps]",16],None]},{Style["Time",16],None}},
LabelStyle->{FontSize->13, Black},
PlotRange->{-rangeY*1.1,rangeY*1.1},
GridLines->{Automatic,axisValsY0},
FrameTicks->{{gridY,If[hasE,gridE,None]},{xAxisTimeTicks,None}},
PlotLegends->{Placed[PointLegend[If[hasE,Append[colors,myRed],colors],
If[hasE,Append[customLegend,Estats],customLegend],
LegendFunction->(Framed[#,Background->backLegendOpacity,RoundingRadius->5]&),
LegendMarkerSize->13
],{Right,Top}]}

], 

PlotLabel-> Column[
If[Length[title]>1,
{Style[title[[1]],Bold,Black,FontSize->16],
Style[title[[2]],Italic,Black,FontSize->14]}
,{Style[title[[1]],Bold,Black,FontSize->16]}
],Alignment->Center],
ImagePadding -> {{Automatic,Automatic},{Automatic,8}},
ImageMargins -> 10,ImageSize->{900,500},AspectRatio->500/1000
]

];

End[];

