`timescale 1ps/1ps

// =====================================================================================================================
// This example design wrapper module instantiates the core and any helper blocks which the user chose to exclude from
// the core, connects them as appropriate, and maps enabled ports
// =====================================================================================================================

module hop_m_gty_example_wrapper (
    input wire [0:0] gtwiz_reset_clk_freerun_in        ,
    input wire [0:0] gtwiz_reset_all_in                ,
    input wire [0:0] gtwiz_reset_tx_pll_and_datapath_in,
    input wire [0:0] gtwiz_reset_tx_datapath_in        ,
    input wire [0:0] gtwiz_reset_rx_pll_and_datapath_in,
    input wire [0:0] gtwiz_reset_rx_datapath_in        ,
    input wire [0:0] gtwiz_reset_rx_cdr_stable_out     ,
    input wire [0:0] gtwiz_reset_tx_done_out           ,
    input wire [0:0] gtwiz_reset_rx_done_out           ,
    input wire [0:0] gtwiz_reset_userclk_tx_active_in  ,
    input wire [0:0] gtwiz_reset_userclk_rx_active_in  ,
    input wire [0:0] gtpowergood_in                    ,
    input wire [0:0] txusrclk2_in                      ,
    input wire [0:0] plllock_tx_in                     ,
    input wire [0:0] txresetdone_in                    ,
    input wire [0:0] rxusrclk2_in                      ,
    input wire [0:0] plllock_rx_in                     ,
    input wire [0:0] rxcdrlock_in                      ,
    input wire [0:0] rxresetdone_in                    ,
    input wire [0:0] pllreset_tx_out                   ,
    input wire [0:0] txprogdivreset_out                ,
    input wire [0:0] gttxreset_out                     ,
    input wire [0:0] txuserrdy_out                     ,
    input wire [0:0] pllreset_rx_out                   ,
    input wire [0:0] rxprogdivreset_out                ,
    input wire [0:0] gtrxreset_out                     ,
    input wire [0:0] rxuserrdy_out                     
);

  // ===================================================================================================================
  // PARAMETERS AND FUNCTIONS
  // ===================================================================================================================

  // Declare and initialize local parameters and functions used for HDL generation
  localparam [191:0] P_CHANNEL_ENABLE = 192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000000;
  `include "hop_m_gty_example_wrapper_functions.v"
  localparam integer P_TX_MASTER_CH_PACKED_IDX = f_calc_pk_mc_idx(8);
  localparam integer P_RX_MASTER_CH_PACKED_IDX = f_calc_pk_mc_idx(8);

  // -------------------------------------------------------------------------------------------------------------------
  // Reset controller helper block
  // -------------------------------------------------------------------------------------------------------------------

  wire [0:0] txresetdone_int;
  wire [0:0] rxresetdone_int;

  wire [0:0] txresetdone_sync;
  wire [0:0] rxresetdone_sync;
  
  assign txresetdone_int = txresetdone_in;
  assign rxresetdone_int = rxresetdone_in;
  
  genvar gi_ch_xrd;
  generate for (gi_ch_xrd = 0; gi_ch_xrd < 1; gi_ch_xrd = gi_ch_xrd + 1) begin : gen_ch_xrd
    (* DONT_TOUCH = "TRUE" *)
    hop_m_gty_example_bit_synchronizer bit_synchronizer_txresetdone_inst (
      .clk_in (gtwiz_reset_clk_freerun_in),
      .i_in   (txresetdone_int[gi_ch_xrd]),
      .o_out  (txresetdone_sync[gi_ch_xrd])
    );
    (* DONT_TOUCH = "TRUE" *)
    hop_m_gty_example_bit_synchronizer bit_synchronizer_rxresetdone_inst (
      .clk_in (gtwiz_reset_clk_freerun_in),
      .i_in   (rxresetdone_int[gi_ch_xrd]),
      .o_out  (rxresetdone_sync[gi_ch_xrd])
    );
  end
  endgenerate
  
  assign gtwiz_reset_txresetdone_int = &txresetdone_sync;
  assign gtwiz_reset_rxresetdone_int = &rxresetdone_sync;

  // Instantiate the single reset controller
  (* DONT_TOUCH = "TRUE" *)
  hop_m_gty_example_gtwiz_reset gtwiz_reset_inst (
    .gtwiz_reset_clk_freerun_in         (gtwiz_reset_clk_freerun_in),
    .gtwiz_reset_all_in                 (gtwiz_reset_all_in),
    .gtwiz_reset_tx_pll_and_datapath_in (gtwiz_reset_tx_pll_and_datapath_in),
    .gtwiz_reset_tx_datapath_in         (gtwiz_reset_tx_datapath_in),
    .gtwiz_reset_rx_pll_and_datapath_in (gtwiz_reset_rx_pll_and_datapath_in),
    .gtwiz_reset_rx_datapath_in         (gtwiz_reset_rx_datapath_in),
    .gtwiz_reset_rx_cdr_stable_out      (gtwiz_reset_rx_cdr_stable_out),
    .gtwiz_reset_tx_done_out            (gtwiz_reset_tx_done_out),
    .gtwiz_reset_rx_done_out            (gtwiz_reset_rx_done_out),
    .gtwiz_reset_userclk_tx_active_in   (gtwiz_reset_userclk_tx_active_int),
    .gtwiz_reset_userclk_rx_active_in   (gtwiz_reset_userclk_rx_active_int),
    .gtpowergood_in                     (gtwiz_reset_gtpowergood_int),
    .txusrclk2_in                       (gtwiz_userclk_tx_usrclk2_out),
    .plllock_tx_in                      (gtwiz_reset_plllock_tx_int),
    .txresetdone_in                     (gtwiz_reset_txresetdone_int),
    .rxusrclk2_in                       (gtwiz_userclk_rx_usrclk2_out),
    .plllock_rx_in                      (gtwiz_reset_plllock_rx_int),
    .rxcdrlock_in                       (gtwiz_reset_rxcdrlock_int),
    .rxresetdone_in                     (gtwiz_reset_rxresetdone_int),
    .pllreset_tx_out                    (gtwiz_reset_pllreset_tx_int),
    .txprogdivreset_out                 (gtwiz_reset_txprogdivreset_int),
    .gttxreset_out                      (gtwiz_reset_gttxreset_int),
    .txuserrdy_out                      (gtwiz_reset_txuserrdy_int),
    .pllreset_rx_out                    (gtwiz_reset_pllreset_rx_int),
    .rxprogdivreset_out                 (gtwiz_reset_rxprogdivreset_int),
    .gtrxreset_out                      (gtwiz_reset_gtrxreset_int),
    .rxuserrdy_out                      (gtwiz_reset_rxuserrdy_int),
    .tx_enabled_tie_in                  (1'b1),
    .rx_enabled_tie_in                  (1'b1),
    .shared_pll_tie_in                  (1'b1)
  );


 endmodule