----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Edoardo Orzes
-- 
-- Create Date: 30/07/2023 18:18:34 PM
-- Module Name: UI_phase_shifter - Behavioral
-- Target Devices: GTY Virtex Ultrascale Transceiver

-- Revision: v1.0
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use work.gt_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity UI_phase_shifter is
    Port (
        rst        : in  std_logic; -- connect to not(txpmaresetdone)
        clk_sys    : in  std_logic;
        n_UI_align : in  std_logic_vector(5 downto 0); -- numbers of UI to phase align XCLK to txusrclk
        strobe_o   : out std_logic; -- connect to TxPI controller
        inc_ndec_o : out std_logic; -- connect to TxPI controller
        step_o     : out std_logic_vector(3 downto 0); -- connect to TxPI controller
        stepdone_i : in  std_logic;
        bypass     : in  std_logic;
        done_o     : out std_logic  -- use to reset tx user logic
    );
end UI_phase_shifter;

architecture Behavioral of UI_phase_shifter is

    type state_t is (IDLE, START, INC, WAIT_RST);
    attribute ENUM_ENCODING: string;
    attribute ENUM_ENCODING of state_t: type is "00 01 10 11";
    signal state : state_t := IDLE;

    signal cnt : integer range 0 to 512 := 0;
    signal n_UI : integer range 0 to 63 := 0;

begin

    n_UI <= to_integer(unsigned(n_UI_align));

    process(clk_sys) begin
        if rising_edge(clk_sys) then
            if rst = '1' or bypass = '1' then
                state  <= IDLE;
                done_o <= '0' when not(bypass) else '1';
            else
                case(state) is 

                    when IDLE =>
                        cnt <= 0;
                        if or_reduce(n_UI_align) = '1' then
                            state <= START;
                        end if;

                    when START =>
                        if cnt < n_UI*8 and n_UI_align /= "111111" then
                            cnt <= cnt + 1;
                            state <= INC;
                        else 
                            state  <= WAIT_RST;
                            done_o <= '1';
                        end if;

                    when INC =>
                        strobe_o   <= '1';
                        inc_ndec_o <= '1';
                        step_o     <= "1000"; -- steps of 8, 8 times n_UI => 64 steps * n_UI
                        if stepdone_i = '1' then 
                            strobe_o <= '0';
                            state    <= START;
                        end if;

                    when WAIT_RST =>
                        if or_reduce(n_UI_align) = '0' then
                            state <= IDLE;
                        end if;

                end case;
            end if;
        end if;
    end process;

end Behavioral;

