onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+hop_m_gty_vio_0 -L xil_defaultlib -L secureip -O5 xil_defaultlib.hop_m_gty_vio_0

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure

do {hop_m_gty_vio_0.udo}

run -all

endsim

quit -force
