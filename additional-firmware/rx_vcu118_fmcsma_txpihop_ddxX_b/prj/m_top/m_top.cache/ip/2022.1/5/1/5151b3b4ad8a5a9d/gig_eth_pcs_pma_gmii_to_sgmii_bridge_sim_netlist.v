// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Sat Jul 22 01:58:12 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_eth_pcs_pma_gmii_to_sgmii_bridge_sim_netlist.v
// Design      : gig_eth_pcs_pma_gmii_to_sgmii_bridge
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu9p-flga2104-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* DowngradeIPIdentifiedWarnings = "yes" *) (* EXAMPLE_SIMULATION = "0" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (txp_0,
    txn_0,
    rxp_0,
    rxn_0,
    signal_detect_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_isolate_0,
    sgmii_clk_r_0,
    sgmii_clk_f_0,
    sgmii_clk_en_0,
    speed_is_10_100_0,
    speed_is_100_0,
    an_interrupt_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    status_vector_0,
    configuration_vector_0,
    refclk625_p,
    refclk625_n,
    clk125_out,
    clk312_out,
    rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    rx_locked,
    tx_locked,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    riu_rddata_3,
    riu_valid_3,
    riu_prsnt_3,
    riu_rddata_2,
    riu_valid_2,
    riu_prsnt_2,
    riu_rddata_1,
    riu_valid_1,
    riu_prsnt_1,
    rx_btval_3,
    rx_btval_2,
    rx_btval_1,
    tx_dly_rdy_1,
    rx_dly_rdy_1,
    rx_vtc_rdy_1,
    tx_vtc_rdy_1,
    tx_dly_rdy_2,
    rx_dly_rdy_2,
    rx_vtc_rdy_2,
    tx_vtc_rdy_2,
    tx_dly_rdy_3,
    rx_dly_rdy_3,
    rx_vtc_rdy_3,
    tx_vtc_rdy_3,
    tx_pll_clk_out,
    rx_pll_clk_out,
    tx_rdclk_out,
    reset);
  output txp_0;
  output txn_0;
  input rxp_0;
  input rxn_0;
  input signal_detect_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output gmii_isolate_0;
  output sgmii_clk_r_0;
  output sgmii_clk_f_0;
  output sgmii_clk_en_0;
  input speed_is_10_100_0;
  input speed_is_100_0;
  output an_interrupt_0;
  input [15:0]an_adv_config_vector_0;
  input an_restart_config_0;
  output [15:0]status_vector_0;
  input [4:0]configuration_vector_0;
  input refclk625_p;
  input refclk625_n;
  output clk125_out;
  output clk312_out;
  output rst_125_out;
  output tx_logic_reset;
  output rx_logic_reset;
  output rx_locked;
  output tx_locked;
  output tx_bsc_rst_out;
  output rx_bsc_rst_out;
  output tx_bs_rst_out;
  output rx_bs_rst_out;
  output tx_rst_dly_out;
  output rx_rst_dly_out;
  output tx_bsc_en_vtc_out;
  output rx_bsc_en_vtc_out;
  output tx_bs_en_vtc_out;
  output rx_bs_en_vtc_out;
  output riu_clk_out;
  output [5:0]riu_addr_out;
  output [15:0]riu_wr_data_out;
  output riu_wr_en_out;
  output [1:0]riu_nibble_sel_out;
  input [15:0]riu_rddata_3;
  input riu_valid_3;
  input riu_prsnt_3;
  input [15:0]riu_rddata_2;
  input riu_valid_2;
  input riu_prsnt_2;
  input [15:0]riu_rddata_1;
  input riu_valid_1;
  input riu_prsnt_1;
  output [8:0]rx_btval_3;
  output [8:0]rx_btval_2;
  output [8:0]rx_btval_1;
  input tx_dly_rdy_1;
  input rx_dly_rdy_1;
  input rx_vtc_rdy_1;
  input tx_vtc_rdy_1;
  input tx_dly_rdy_2;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_2;
  input tx_vtc_rdy_2;
  input tx_dly_rdy_3;
  input rx_dly_rdy_3;
  input rx_vtc_rdy_3;
  input tx_vtc_rdy_3;
  output tx_pll_clk_out;
  output rx_pll_clk_out;
  output tx_rdclk_out;
  input reset;

  wire \<const0> ;
  wire [15:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire clk125_out;
  wire clk312_out;
  wire [4:0]configuration_vector_0;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_er_0;
  wire [7:0]gmii_txd_0;
  (* IBUF_LOW_PWR = 0 *) wire refclk625_n;
  (* IBUF_LOW_PWR = 0 *) wire refclk625_p;
  wire reset;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt_1;
  wire riu_prsnt_2;
  wire riu_prsnt_3;
  wire [15:0]riu_rddata_1;
  wire [15:0]riu_rddata_2;
  wire [15:0]riu_rddata_3;
  wire riu_valid_1;
  wire riu_valid_2;
  wire riu_valid_3;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rst_125_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire [8:0]rx_btval_1;
  wire [8:0]rx_btval_2;
  wire [8:0]rx_btval_3;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire rx_locked;
  wire rx_logic_reset;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  (* IBUF_LOW_PWR = 0 *) wire rxn_0;
  (* IBUF_LOW_PWR = 0 *) wire rxp_0;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [13:0]\^status_vector_0 ;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_locked;
  wire tx_logic_reset;
  wire tx_pll_clk_out;
  wire tx_rdclk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  (* SLEW = "SLOW" *) wire txn_0;
  (* SLEW = "SLOW" *) wire txp_0;
  wire [15:8]NLW_inst_status_vector_0_UNCONNECTED;

  assign status_vector_0[15] = \<const0> ;
  assign status_vector_0[14] = \<const0> ;
  assign status_vector_0[13:9] = \^status_vector_0 [13:9];
  assign status_vector_0[8] = \<const0> ;
  assign status_vector_0[7:0] = \^status_vector_0 [7:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* X_CORE_INFO = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_support inst
       (.an_adv_config_vector_0({1'b0,1'b0,1'b0,1'b0,an_adv_config_vector_0[11],1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_interrupt_0(an_interrupt_0),
        .an_restart_config_0(an_restart_config_0),
        .clk125_out(clk125_out),
        .clk312_out(clk312_out),
        .configuration_vector_0(configuration_vector_0),
        .gmii_isolate_0(gmii_isolate_0),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_txd_0(gmii_txd_0),
        .refclk625_n(refclk625_n),
        .refclk625_p(refclk625_p),
        .reset(reset),
        .riu_addr_out(riu_addr_out),
        .riu_clk_out(riu_clk_out),
        .riu_nibble_sel_out(riu_nibble_sel_out),
        .riu_prsnt_1(riu_prsnt_1),
        .riu_prsnt_2(riu_prsnt_2),
        .riu_prsnt_3(riu_prsnt_3),
        .riu_rddata_1(riu_rddata_1),
        .riu_rddata_2(riu_rddata_2),
        .riu_rddata_3(riu_rddata_3),
        .riu_valid_1(riu_valid_1),
        .riu_valid_2(riu_valid_2),
        .riu_valid_3(riu_valid_3),
        .riu_wr_data_out(riu_wr_data_out),
        .riu_wr_en_out(riu_wr_en_out),
        .rst_125_out(rst_125_out),
        .rx_bs_en_vtc_out(rx_bs_en_vtc_out),
        .rx_bs_rst_out(rx_bs_rst_out),
        .rx_bsc_en_vtc_out(rx_bsc_en_vtc_out),
        .rx_bsc_rst_out(rx_bsc_rst_out),
        .rx_btval_1(rx_btval_1),
        .rx_btval_2(rx_btval_2),
        .rx_btval_3(rx_btval_3),
        .rx_dly_rdy_1(rx_dly_rdy_1),
        .rx_dly_rdy_2(rx_dly_rdy_2),
        .rx_dly_rdy_3(rx_dly_rdy_3),
        .rx_locked(rx_locked),
        .rx_logic_reset(rx_logic_reset),
        .rx_pll_clk_out(rx_pll_clk_out),
        .rx_rst_dly_out(rx_rst_dly_out),
        .rx_vtc_rdy_1(rx_vtc_rdy_1),
        .rx_vtc_rdy_2(rx_vtc_rdy_2),
        .rx_vtc_rdy_3(rx_vtc_rdy_3),
        .rxn_0(rxn_0),
        .rxp_0(rxp_0),
        .sgmii_clk_en_0(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .signal_detect_0(signal_detect_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0),
        .status_vector_0({NLW_inst_status_vector_0_UNCONNECTED[15:14],\^status_vector_0 }),
        .tx_bs_en_vtc_out(tx_bs_en_vtc_out),
        .tx_bs_rst_out(tx_bs_rst_out),
        .tx_bsc_en_vtc_out(tx_bsc_en_vtc_out),
        .tx_bsc_rst_out(tx_bsc_rst_out),
        .tx_dly_rdy_1(tx_dly_rdy_1),
        .tx_dly_rdy_2(tx_dly_rdy_2),
        .tx_dly_rdy_3(tx_dly_rdy_3),
        .tx_locked(tx_locked),
        .tx_logic_reset(tx_logic_reset),
        .tx_pll_clk_out(tx_pll_clk_out),
        .tx_rdclk_out(tx_rdclk_out),
        .tx_rst_dly_out(tx_rst_dly_out),
        .tx_vtc_rdy_1(tx_vtc_rdy_1),
        .tx_vtc_rdy_2(tx_vtc_rdy_2),
        .tx_vtc_rdy_3(tx_vtc_rdy_3),
        .txn_0(txn_0),
        .txp_0(txp_0));
endmodule

(* C_BytePosition = "0" *) (* C_IoBank = "44" *) (* C_Part = "XCKU060" *) 
(* C_Rx_BtslcNulType = "SERIAL" *) (* C_Rx_Data_Width = "4" *) (* C_Rx_Delay_Format = "COUNT" *) 
(* C_Rx_Delay_Type = "VAR_LOAD" *) (* C_Rx_Delay_Value = "0" *) (* C_Rx_RefClk_Frequency = "312.500000" *) 
(* C_Rx_Self_Calibrate = "ENABLE" *) (* C_Rx_Serial_Mode = "TRUE" *) (* C_Rx_UsedBitslices = "7'b0000011" *) 
(* C_TxInUpperNibble = "0" *) (* C_Tx_BtslceTr = "T" *) (* C_Tx_BtslceUsedAsT = "7'b0000000" *) 
(* C_Tx_Data_Width = "8" *) (* C_Tx_Delay_Format = "TIME" *) (* C_Tx_Delay_Type = "FIXED" *) 
(* C_Tx_Delay_Value = "0" *) (* C_Tx_RefClk_Frequency = "1250.000000" *) (* C_Tx_Self_Calibrate = "ENABLE" *) 
(* C_Tx_Serial_Mode = "FALSE" *) (* C_Tx_UsedBitslices = "7'b0010000" *) (* C_UseRxRiu = "1" *) 
(* C_UseTxRiu = "1" *) (* dont_touch = "true" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_BaseX_Byte
   (BaseX_Tx_Bsc_Rst,
    BaseX_Rx_Bsc_Rst,
    BaseX_Tx_Bs_Rst,
    BaseX_Rx_Bs_Rst,
    BaseX_Tx_Rst_Dly,
    BaseX_Rx_Rst_Dly,
    BaseX_Tx_Bsc_En_Vtc,
    BaseX_Rx_Bsc_En_Vtc,
    BaseX_Tx_Bs_En_Vtc,
    BaseX_Rx_Bs_En_Vtc,
    BaseX_Riu_Clk,
    BaseX_Riu_Addr,
    BaseX_Riu_Wr_Data,
    BaseX_Riu_Rd_Data,
    BaseX_Riu_Valid,
    BaseX_Riu_Prsnt,
    BaseX_Riu_Wr_En,
    BaseX_Riu_Nibble_Sel,
    BaseX_Tx_Pll_Clk,
    BaseX_Rx_Pll_Clk,
    BaseX_Tx_Dly_Rdy,
    BaseX_Rx_Dly_Rdy,
    BaseX_Tx_Vtc_Rdy,
    BaseX_Rx_Vtc_Rdy,
    BaseX_Tx_Phy_Rden,
    BaseX_Rx_Phy_Rden,
    BaseX_Rx_Fifo_Rd_Clk,
    BaseX_Rx_Fifo_Rd_En,
    BaseX_Rx_Fifo_Empty,
    BaseX_Dly_Clk,
    BaseX_Idly_Ce,
    BaseX_Idly_Inc,
    BaseX_Idly_Load,
    BaseX_Idly_CntValueIn,
    BaseX_Idly_CntValueOut,
    BaseX_Odly_Ce,
    BaseX_Odly_Inc,
    BaseX_Odly_Load,
    BaseX_Odly_CntValueIn,
    BaseX_Odly_CntValueOut,
    BaseX_TriOdly_Ce,
    BaseX_TriOdly_Inc,
    BaseX_TriOdly_Load,
    BaseX_TriOdly_CntValueIn,
    BaseX_TriOdly_CntValueOut,
    BaseX_Tx_TbyteIn,
    BaseX_Tx_T_In,
    BaseX_Tx_D_In,
    BaseX_Rx_Q_Out,
    BaseX_Rx_Q_CombOut,
    BaseX_Tx_Tri_Out,
    BaseX_Tx_Data_Out,
    BaseX_Rx_Data_In,
    Tx_RdClk);
  input BaseX_Tx_Bsc_Rst;
  input BaseX_Rx_Bsc_Rst;
  input BaseX_Tx_Bs_Rst;
  input BaseX_Rx_Bs_Rst;
  input BaseX_Tx_Rst_Dly;
  input BaseX_Rx_Rst_Dly;
  input BaseX_Tx_Bsc_En_Vtc;
  input BaseX_Rx_Bsc_En_Vtc;
  input BaseX_Tx_Bs_En_Vtc;
  input BaseX_Rx_Bs_En_Vtc;
  input BaseX_Riu_Clk;
  input [5:0]BaseX_Riu_Addr;
  input [15:0]BaseX_Riu_Wr_Data;
  output [15:0]BaseX_Riu_Rd_Data;
  output BaseX_Riu_Valid;
  output BaseX_Riu_Prsnt;
  input BaseX_Riu_Wr_En;
  input [1:0]BaseX_Riu_Nibble_Sel;
  input BaseX_Tx_Pll_Clk;
  input BaseX_Rx_Pll_Clk;
  output BaseX_Tx_Dly_Rdy;
  output BaseX_Rx_Dly_Rdy;
  output BaseX_Tx_Vtc_Rdy;
  output BaseX_Rx_Vtc_Rdy;
  input [3:0]BaseX_Tx_Phy_Rden;
  input [3:0]BaseX_Rx_Phy_Rden;
  input BaseX_Rx_Fifo_Rd_Clk;
  input [6:0]BaseX_Rx_Fifo_Rd_En;
  output [6:0]BaseX_Rx_Fifo_Empty;
  input BaseX_Dly_Clk;
  input [6:0]BaseX_Idly_Ce;
  input [6:0]BaseX_Idly_Inc;
  input [6:0]BaseX_Idly_Load;
  input [62:0]BaseX_Idly_CntValueIn;
  output [62:0]BaseX_Idly_CntValueOut;
  input [5:0]BaseX_Odly_Ce;
  input [5:0]BaseX_Odly_Inc;
  input [5:0]BaseX_Odly_Load;
  input [53:0]BaseX_Odly_CntValueIn;
  output [53:0]BaseX_Odly_CntValueOut;
  input BaseX_TriOdly_Ce;
  input BaseX_TriOdly_Inc;
  input BaseX_TriOdly_Load;
  input [8:0]BaseX_TriOdly_CntValueIn;
  output [8:0]BaseX_TriOdly_CntValueOut;
  input [3:0]BaseX_Tx_TbyteIn;
  input [5:0]BaseX_Tx_T_In;
  input [47:0]BaseX_Tx_D_In;
  output [27:0]BaseX_Rx_Q_Out;
  output [6:0]BaseX_Rx_Q_CombOut;
  output [5:0]BaseX_Tx_Tri_Out;
  output [5:0]BaseX_Tx_Data_Out;
  input [6:0]BaseX_Rx_Data_In;
  input Tx_RdClk;

  wire BaseX_Dly_Clk;
  wire [6:0]BaseX_Idly_Ce;
  wire [62:0]BaseX_Idly_CntValueIn;
  wire [62:0]BaseX_Idly_CntValueOut;
  wire [6:0]BaseX_Idly_Inc;
  wire [6:0]BaseX_Idly_Load;
  wire [5:0]BaseX_Odly_Ce;
  wire [53:0]BaseX_Odly_CntValueIn;
  wire [53:0]BaseX_Odly_CntValueOut;
  wire [5:0]BaseX_Odly_Inc;
  wire [5:0]BaseX_Odly_Load;
  wire [5:0]BaseX_Riu_Addr;
  wire BaseX_Riu_Clk;
  wire [1:0]BaseX_Riu_Nibble_Sel;
  wire BaseX_Riu_Prsnt;
  wire [15:0]BaseX_Riu_Rd_Data;
  wire BaseX_Riu_Valid;
  wire [15:0]BaseX_Riu_Wr_Data;
  wire BaseX_Riu_Wr_En;
  wire BaseX_Rx_Bs_En_Vtc;
  wire BaseX_Rx_Bs_Rst;
  wire BaseX_Rx_Bsc_En_Vtc;
  wire BaseX_Rx_Bsc_Rst;
  wire [6:0]BaseX_Rx_Data_In;
  wire BaseX_Rx_Dly_Rdy;
  wire [6:0]BaseX_Rx_Fifo_Empty;
  wire BaseX_Rx_Fifo_Rd_Clk;
  wire [6:0]BaseX_Rx_Fifo_Rd_En;
  wire [3:0]BaseX_Rx_Phy_Rden;
  wire BaseX_Rx_Pll_Clk;
  wire [6:0]BaseX_Rx_Q_CombOut;
  wire [27:0]BaseX_Rx_Q_Out;
  wire BaseX_Rx_Rst_Dly;
  wire BaseX_Rx_Vtc_Rdy;
  wire BaseX_TriOdly_Ce;
  wire [8:0]BaseX_TriOdly_CntValueIn;
  wire [8:0]BaseX_TriOdly_CntValueOut;
  wire BaseX_TriOdly_Inc;
  wire BaseX_TriOdly_Load;
  wire BaseX_Tx_Bs_En_Vtc;
  wire BaseX_Tx_Bs_Rst;
  wire BaseX_Tx_Bsc_En_Vtc;
  wire BaseX_Tx_Bsc_Rst;
  wire [47:0]BaseX_Tx_D_In;
  wire [5:0]BaseX_Tx_Data_Out;
  wire BaseX_Tx_Dly_Rdy;
  wire [3:0]BaseX_Tx_Phy_Rden;
  wire BaseX_Tx_Pll_Clk;
  wire BaseX_Tx_Rst_Dly;
  wire [5:0]BaseX_Tx_T_In;
  wire [5:0]BaseX_Tx_Tri_Out;
  wire BaseX_Tx_Vtc_Rdy;
  (* async_reg = "true" *) wire [1:0]IntActTx_TByteinPip;
  wire [15:0]RIU_RD_DATA_LOW;
  wire [15:0]RIU_RD_DATA_UPP;
  wire RIU_RD_VALID_LOW;
  wire RIU_RD_VALID_UPP;
  wire Tx_RdClk;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Fifo_Wrclk_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_North_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_South_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Nclk_Nibble_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Pclk_Nibble_Out_UNCONNECTED;
  wire [62:0]NLW_BaseX_Byte_I_Rx_Nibble_Rx_CntValueOut_Ext_UNCONNECTED;
  wire [6:0]NLW_BaseX_Byte_I_Rx_Nibble_Rx_Dyn_Dci_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_North_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_South_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Nclk_Nibble_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Pclk_Nibble_Out_UNCONNECTED;
  wire [6:0]NLW_BaseX_Byte_I_Tx_Nibble_Tx_Dyn_Dci_UNCONNECTED;

  (* C_BtslcNulType = "SERIAL" *) 
  (* C_BusRxBitCtrlIn = "40" *) 
  (* C_BusRxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlIn = "40" *) 
  (* C_BusTxBitCtrlInTri = "40" *) 
  (* C_BusTxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlOutTri = "40" *) 
  (* C_BytePosition = "0" *) 
  (* C_Cascade = "FALSE" *) 
  (* C_CntValue = "9" *) 
  (* C_Ctrl_Clk = "EXTERNAL" *) 
  (* C_Delay_Format = "COUNT" *) 
  (* C_Delay_Type = "VAR_LOAD" *) 
  (* C_Delay_Value = "0" *) 
  (* C_Delay_Value_Ext = "0" *) 
  (* C_Div_Mode = "DIV2" *) 
  (* C_En_Clk_To_Ext_North = "DISABLE" *) 
  (* C_En_Clk_To_Ext_South = "DISABLE" *) 
  (* C_En_Dyn_Odly_Mode = "FALSE" *) 
  (* C_En_Other_Nclk = "FALSE" *) 
  (* C_En_Other_Pclk = "FALSE" *) 
  (* C_Fifo_Sync_Mode = "FALSE" *) 
  (* C_Idly_Vt_Track = "TRUE" *) 
  (* C_Inv_Rxclk = "FALSE" *) 
  (* C_IoBank = "44" *) 
  (* C_Is_Clk_Ext_Inverted = "1'b0" *) 
  (* C_Is_Clk_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Ext_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
  (* C_Is_Rst_Inverted = "1'b0" *) 
  (* C_NibbleType = "7" *) 
  (* C_Odly_Vt_Track = "TRUE" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Qdly_Vt_Track = "TRUE" *) 
  (* C_Read_Idle_Count = "6'b000000" *) 
  (* C_RefClk_Frequency = "312.500000" *) 
  (* C_RefClk_Src = "PLLCLK" *) 
  (* C_Rounding_Factor = "16" *) 
  (* C_RxGate_Extend = "FALSE" *) 
  (* C_Rx_Clk_Phase_n = "SHIFT_90" *) 
  (* C_Rx_Clk_Phase_p = "SHIFT_90" *) 
  (* C_Rx_Data_Width = "4" *) 
  (* C_Rx_Gating = "DISABLE" *) 
  (* C_Self_Calibrate = "ENABLE" *) 
  (* C_Serial_Mode = "TRUE" *) 
  (* C_Tx_Gating = "DISABLE" *) 
  (* C_Update_Mode = "ASYNC" *) 
  (* C_Update_Mode_Ext = "ASYNC" *) 
  (* C_UsedBitslices = "7'b0000011" *) 
  (* KEEP_HIERARCHY = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Rx_Nibble BaseX_Byte_I_Rx_Nibble
       (.Fifo_Empty(BaseX_Rx_Fifo_Empty),
        .Fifo_Rd_Clk({BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk}),
        .Fifo_Rd_En(BaseX_Rx_Fifo_Rd_En),
        .Fifo_Wrclk_Out(NLW_BaseX_Byte_I_Rx_Nibble_Fifo_Wrclk_Out_UNCONNECTED),
        .Rx_Bs_En_Vtc(BaseX_Rx_Bs_En_Vtc),
        .Rx_Bs_Rst(BaseX_Rx_Bs_Rst),
        .Rx_Bsc_En_Vtc(BaseX_Rx_Bsc_En_Vtc),
        .Rx_Bsc_Rst(BaseX_Rx_Bsc_Rst),
        .Rx_Ce(BaseX_Idly_Ce),
        .Rx_Ce_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Clk(BaseX_Dly_Clk),
        .Rx_Clk_From_Ext(1'b1),
        .Rx_Clk_To_Ext_North(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_North_UNCONNECTED),
        .Rx_Clk_To_Ext_South(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_South_UNCONNECTED),
        .Rx_CntValueIn(BaseX_Idly_CntValueIn),
        .Rx_CntValueIn_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_CntValueOut(BaseX_Idly_CntValueOut),
        .Rx_CntValueOut_Ext(NLW_BaseX_Byte_I_Rx_Nibble_Rx_CntValueOut_Ext_UNCONNECTED[62:0]),
        .Rx_Data_In(BaseX_Rx_Data_In),
        .Rx_Dly_Rdy(BaseX_Rx_Dly_Rdy),
        .Rx_Dyn_Dci(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Dyn_Dci_UNCONNECTED[6:0]),
        .Rx_Inc(BaseX_Idly_Inc),
        .Rx_Inc_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Load(BaseX_Idly_Load),
        .Rx_Load_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Nclk_Nibble_In(1'b1),
        .Rx_Nclk_Nibble_Out(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Nclk_Nibble_Out_UNCONNECTED),
        .Rx_Pclk_Nibble_In(1'b1),
        .Rx_Pclk_Nibble_Out(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Pclk_Nibble_Out_UNCONNECTED),
        .Rx_Phy_Rden(BaseX_Rx_Phy_Rden),
        .Rx_Pll_Clk(BaseX_Rx_Pll_Clk),
        .Rx_Q_CombOut(BaseX_Rx_Q_CombOut),
        .Rx_Q_Out(BaseX_Rx_Q_Out),
        .Rx_RefClk(1'b0),
        .Rx_Riu_Addr(BaseX_Riu_Addr),
        .Rx_Riu_Clk(BaseX_Riu_Clk),
        .Rx_Riu_Nibble_Sel(BaseX_Riu_Nibble_Sel[0]),
        .Rx_Riu_Prsnt(BaseX_Riu_Prsnt),
        .Rx_Riu_Rd_Data(RIU_RD_DATA_UPP),
        .Rx_Riu_Valid(RIU_RD_VALID_UPP),
        .Rx_Riu_Wr_Data(BaseX_Riu_Wr_Data),
        .Rx_Riu_Wr_En(BaseX_Riu_Wr_En),
        .Rx_Rst_Dly(BaseX_Rx_Rst_Dly),
        .Rx_Tbyte_In({1'b0,1'b0,1'b0,1'b0}),
        .Rx_Vtc_Rdy(BaseX_Rx_Vtc_Rdy));
  (* C_BtslceUsedAsT = "7'b0000000" *) 
  (* C_BusRxBitCtrlIn = "40" *) 
  (* C_BusRxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlIn = "40" *) 
  (* C_BusTxBitCtrlInTri = "40" *) 
  (* C_BusTxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlOutTri = "40" *) 
  (* C_BytePosition = "0" *) 
  (* C_CntValue = "9" *) 
  (* C_Ctrl_Clk = "EXTERNAL" *) 
  (* C_Data_Type = "DATA" *) 
  (* C_Delay_Format = "TIME" *) 
  (* C_Delay_Type = "FIXED" *) 
  (* C_Delay_Value = "0" *) 
  (* C_Div_Mode = "DIV4" *) 
  (* C_En_Clk_To_Ext_North = "DISABLE" *) 
  (* C_En_Clk_To_Ext_South = "DISABLE" *) 
  (* C_En_Dyn_Odly_Mode = "FALSE" *) 
  (* C_En_Other_Nclk = "FALSE" *) 
  (* C_En_Other_Pclk = "FALSE" *) 
  (* C_Enable_Pre_Emphasis = "FALSE" *) 
  (* C_Idly_Vt_Track = "FALSE" *) 
  (* C_Init = "1'b0" *) 
  (* C_Inv_Rxclk = "FALSE" *) 
  (* C_IoBank = "44" *) 
  (* C_Is_Clk_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
  (* C_Is_Rst_Inverted = "1'b0" *) 
  (* C_Native_Odelay_Bypass = "FALSE" *) 
  (* C_NibbleType = "6" *) 
  (* C_Odly_Vt_Track = "FALSE" *) 
  (* C_Output_Phase_90 = "TRUE" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Qdly_Vt_Track = "FALSE" *) 
  (* C_Read_Idle_Count = "6'b000000" *) 
  (* C_RefClk_Frequency = "1250.000000" *) 
  (* C_RefClk_Src = "PLLCLK" *) 
  (* C_Rounding_Factor = "16" *) 
  (* C_RxGate_Extend = "FALSE" *) 
  (* C_Rx_Clk_Phase_n = "SHIFT_0" *) 
  (* C_Rx_Clk_Phase_p = "SHIFT_0" *) 
  (* C_Rx_Gating = "DISABLE" *) 
  (* C_Self_Calibrate = "ENABLE" *) 
  (* C_Serial_Mode = "FALSE" *) 
  (* C_Tx_BtslceTr = "T" *) 
  (* C_Tx_Data_Width = "8" *) 
  (* C_Tx_Gating = "ENABLE" *) 
  (* C_Update_Mode = "ASYNC" *) 
  (* C_UsedBitslices = "7'b0010000" *) 
  (* KEEP_HIERARCHY = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Tx_Nibble BaseX_Byte_I_Tx_Nibble
       (.TxTri_Ce(BaseX_TriOdly_Ce),
        .TxTri_Clk(BaseX_Dly_Clk),
        .TxTri_CntValueIn(BaseX_TriOdly_CntValueIn),
        .TxTri_CntValueOut(BaseX_TriOdly_CntValueOut),
        .TxTri_Inc(BaseX_TriOdly_Inc),
        .TxTri_Load(BaseX_TriOdly_Load),
        .Tx_Bs_En_Vtc(BaseX_Tx_Bs_En_Vtc),
        .Tx_Bs_Rst(BaseX_Tx_Bs_Rst),
        .Tx_Bsc_En_Vtc(BaseX_Tx_Bsc_En_Vtc),
        .Tx_Bsc_Rst(BaseX_Tx_Bsc_Rst),
        .Tx_Ce(BaseX_Odly_Ce),
        .Tx_Clk(BaseX_Dly_Clk),
        .Tx_Clk_From_Ext(1'b1),
        .Tx_Clk_To_Ext_North(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_North_UNCONNECTED),
        .Tx_Clk_To_Ext_South(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_South_UNCONNECTED),
        .Tx_CntValueIn(BaseX_Odly_CntValueIn),
        .Tx_CntValueOut(BaseX_Odly_CntValueOut),
        .Tx_D_In(BaseX_Tx_D_In),
        .Tx_Data_Out(BaseX_Tx_Data_Out),
        .Tx_Dly_Rdy(BaseX_Tx_Dly_Rdy),
        .Tx_Dyn_Dci(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Dyn_Dci_UNCONNECTED[6:0]),
        .Tx_Inc(BaseX_Odly_Inc),
        .Tx_Load(BaseX_Odly_Load),
        .Tx_Nclk_Nibble_In(1'b1),
        .Tx_Nclk_Nibble_Out(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Nclk_Nibble_Out_UNCONNECTED),
        .Tx_Pclk_Nibble_In(1'b1),
        .Tx_Pclk_Nibble_Out(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Pclk_Nibble_Out_UNCONNECTED),
        .Tx_Phy_Rden(BaseX_Tx_Phy_Rden),
        .Tx_Pll_Clk(BaseX_Tx_Pll_Clk),
        .Tx_RefClk(1'b0),
        .Tx_Riu_Addr(BaseX_Riu_Addr),
        .Tx_Riu_Clk(BaseX_Riu_Clk),
        .Tx_Riu_Nibble_Sel(BaseX_Riu_Nibble_Sel[1]),
        .Tx_Riu_Rd_Data(RIU_RD_DATA_LOW),
        .Tx_Riu_Valid(RIU_RD_VALID_LOW),
        .Tx_Riu_Wr_Data(BaseX_Riu_Wr_Data),
        .Tx_Riu_Wr_En(BaseX_Riu_Wr_En),
        .Tx_Rst_Dly(BaseX_Tx_Rst_Dly),
        .Tx_T_In(BaseX_Tx_T_In),
        .Tx_Tbyte_In({IntActTx_TByteinPip[1],IntActTx_TByteinPip[1],IntActTx_TByteinPip[1],IntActTx_TByteinPip[1]}),
        .Tx_Tri_Out(BaseX_Tx_Tri_Out),
        .Tx_Vtc_Rdy(BaseX_Tx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  RIU_OR #(
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000)) 
    \Gen_0.BaseX_Byte_I_Riu_Or_TxLow 
       (.RIU_RD_DATA(BaseX_Riu_Rd_Data),
        .RIU_RD_DATA_LOW(RIU_RD_DATA_LOW),
        .RIU_RD_DATA_UPP(RIU_RD_DATA_UPP),
        .RIU_RD_VALID(BaseX_Riu_Valid),
        .RIU_RD_VALID_LOW(RIU_RD_VALID_LOW),
        .RIU_RD_VALID_UPP(RIU_RD_VALID_UPP));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE \IntActTx_TByteinPip_reg[0] 
       (.C(Tx_RdClk),
        .CE(1'b1),
        .CLR(BaseX_Tx_Bsc_Rst),
        .D(BaseX_Tx_Vtc_Rdy),
        .Q(IntActTx_TByteinPip[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE \IntActTx_TByteinPip_reg[1] 
       (.C(Tx_RdClk),
        .CE(1'b1),
        .CLR(BaseX_Tx_Bsc_Rst),
        .D(IntActTx_TByteinPip[0]),
        .Q(IntActTx_TByteinPip[1]));
endmodule

(* C_IoBank = "44" *) (* C_Part = "XCKU060" *) (* EXAMPLE_SIMULATION = "0" *) 
(* dont_touch = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Clock_Reset
   (ClockIn_p,
    ClockIn_n,
    ClockIn_se_out,
    ResetIn,
    Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Tx_Bsc_EnVtc,
    Tx_Bs_EnVtc,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    Rx_Bsc_EnVtc,
    Rx_Bs_EnVtc,
    Tx_SysClk,
    Tx_WrClk,
    Tx_ClkOutPhy,
    Rx_SysClk,
    Rx_RiuClk,
    Rx_ClkOutPhy,
    Tx_Locked,
    Tx_Bs_RstDly,
    Tx_Bs_Rst,
    Tx_Bsc_Rst,
    Tx_LogicRst,
    Rx_Locked,
    Rx_Bs_RstDly,
    Rx_Bs_Rst,
    Rx_Bsc_Rst,
    Rx_LogicRst,
    Riu_Addr,
    Riu_WrData,
    Riu_Wr_En,
    Riu_Nibble_Sel,
    Riu_RdData_3,
    Riu_Valid_3,
    Riu_Prsnt_3,
    Riu_RdData_2,
    Riu_Valid_2,
    Riu_Prsnt_2,
    Riu_RdData_1,
    Riu_Valid_1,
    Riu_Prsnt_1,
    Riu_RdData_0,
    Riu_Valid_0,
    Riu_Prsnt_0,
    Rx_BtVal_3,
    Rx_BtVal_2,
    Rx_BtVal_1,
    Rx_BtVal_0,
    Debug_Out);
  input ClockIn_p;
  input ClockIn_n;
  output ClockIn_se_out;
  input ResetIn;
  input Tx_Dly_Rdy;
  input Tx_Vtc_Rdy;
  output Tx_Bsc_EnVtc;
  output Tx_Bs_EnVtc;
  input Rx_Dly_Rdy;
  input Rx_Vtc_Rdy;
  output Rx_Bsc_EnVtc;
  output Rx_Bs_EnVtc;
  output Tx_SysClk;
  output Tx_WrClk;
  output Tx_ClkOutPhy;
  output Rx_SysClk;
  output Rx_RiuClk;
  output Rx_ClkOutPhy;
  output Tx_Locked;
  output Tx_Bs_RstDly;
  output Tx_Bs_Rst;
  output Tx_Bsc_Rst;
  output Tx_LogicRst;
  output Rx_Locked;
  output Rx_Bs_RstDly;
  output Rx_Bs_Rst;
  output Rx_Bsc_Rst;
  output Rx_LogicRst;
  output [5:0]Riu_Addr;
  output [15:0]Riu_WrData;
  output Riu_Wr_En;
  output [1:0]Riu_Nibble_Sel;
  input [15:0]Riu_RdData_3;
  input Riu_Valid_3;
  input Riu_Prsnt_3;
  input [15:0]Riu_RdData_2;
  input Riu_Valid_2;
  input Riu_Prsnt_2;
  input [15:0]Riu_RdData_1;
  input Riu_Valid_1;
  input Riu_Prsnt_1;
  input [15:0]Riu_RdData_0;
  input Riu_Valid_0;
  input Riu_Prsnt_0;
  output [8:0]Rx_BtVal_3;
  output [8:0]Rx_BtVal_2;
  output [8:0]Rx_BtVal_1;
  output [8:0]Rx_BtVal_0;
  output [7:0]Debug_Out;

  wire \<const0> ;
  wire \<const1> ;
  wire CLKOUTPHYEN;
  wire ClockIn_n;
  wire ClockIn_p;
  wire ClockIn_se_out;
  wire [7:0]Debug_Out;
  wire IntCtrl_Reset;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxDlyRdy;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxLocked;
  wire IntCtrl_RxLogicRst_i_1_n_0;
  wire IntCtrl_RxLogicRst_i_2_n_0;
  wire IntCtrl_RxLogicRst_reg_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_i_1_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_i_2_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_reg_n_0;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxVtcRdy;
  wire IntCtrl_State;
  wire \IntCtrl_State[0]_i_1_n_0 ;
  wire \IntCtrl_State[0]_i_2_n_0 ;
  wire \IntCtrl_State[0]_i_3_n_0 ;
  wire \IntCtrl_State[0]_i_4_n_0 ;
  wire \IntCtrl_State[0]_i_5_n_0 ;
  wire \IntCtrl_State[0]_i_6_n_0 ;
  wire \IntCtrl_State[1]_i_1_n_0 ;
  wire \IntCtrl_State[1]_i_2_n_0 ;
  wire \IntCtrl_State[1]_i_3_n_0 ;
  wire \IntCtrl_State[2]_i_1_n_0 ;
  wire \IntCtrl_State[2]_i_2_n_0 ;
  wire \IntCtrl_State[2]_i_3_n_0 ;
  wire \IntCtrl_State[2]_i_4_n_0 ;
  wire \IntCtrl_State[2]_i_5_n_0 ;
  wire \IntCtrl_State[2]_i_6_n_0 ;
  wire \IntCtrl_State[3]_i_1_n_0 ;
  wire \IntCtrl_State[3]_i_2_n_0 ;
  wire \IntCtrl_State[4]_i_1_n_0 ;
  wire \IntCtrl_State[4]_i_2_n_0 ;
  wire \IntCtrl_State[5]_i_1_n_0 ;
  wire \IntCtrl_State[5]_i_2_n_0 ;
  wire \IntCtrl_State[5]_i_4_n_0 ;
  wire \IntCtrl_State[5]_i_5_n_0 ;
  wire \IntCtrl_State[6]_i_1_n_0 ;
  wire \IntCtrl_State[6]_i_2_n_0 ;
  wire \IntCtrl_State[6]_i_3_n_0 ;
  wire \IntCtrl_State[6]_i_4_n_0 ;
  wire \IntCtrl_State[6]_i_5_n_0 ;
  wire \IntCtrl_State[6]_i_6_n_0 ;
  wire \IntCtrl_State[7]_i_2_n_0 ;
  wire \IntCtrl_State[7]_i_3_n_0 ;
  wire \IntCtrl_State[7]_i_4_n_0 ;
  wire \IntCtrl_State[7]_i_5_n_0 ;
  wire \IntCtrl_State[7]_i_6_n_0 ;
  wire \IntCtrl_State[7]_i_7_n_0 ;
  wire \IntCtrl_State[7]_i_8_n_0 ;
  wire \IntCtrl_State[7]_i_9_n_0 ;
  wire \IntCtrl_State[8]_i_1_n_0 ;
  wire \IntCtrl_State_reg[5]_i_3_n_0 ;
  wire \IntCtrl_State_reg_n_0_[8] ;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxDlyRdy;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxLocked;
  wire IntCtrl_TxLogicRst_i_1_n_0;
  wire IntCtrl_TxLogicRst_i_2_n_0;
  wire IntCtrl_TxLogicRst_i_3_n_0;
  wire IntCtrl_TxLogicRst_reg_n_0;
  wire IntCtrl_TxPllClkOutPhyEn_i_1_n_0;
  wire IntCtrl_TxPllClkOutPhyEn_i_2_n_0;
  wire IntCtrl_TxPllRst_i_1_n_0;
  wire IntCtrl_TxPllRst_i_2_n_0;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxVtcRdy;
  wire IntRx_ClkOut0;
  wire IntTx_ClkOut0;
  wire IntTx_ClkOut1;
  wire RST;
  wire ResetIn;
  wire [4:0]\^Riu_Addr ;
  wire [5:0]Riu_Addr0_in;
  wire \Riu_Addr[0]_i_2_n_0 ;
  wire \Riu_Addr[0]_i_3_n_0 ;
  wire \Riu_Addr[1]_i_2_n_0 ;
  wire \Riu_Addr[5]_i_10_n_0 ;
  wire \Riu_Addr[5]_i_1_n_0 ;
  wire \Riu_Addr[5]_i_3_n_0 ;
  wire \Riu_Addr[5]_i_4_n_0 ;
  wire \Riu_Addr[5]_i_5_n_0 ;
  wire \Riu_Addr[5]_i_6_n_0 ;
  wire \Riu_Addr[5]_i_7_n_0 ;
  wire \Riu_Addr[5]_i_8_n_0 ;
  wire \Riu_Addr[5]_i_9_n_0 ;
  wire [0:0]\^Riu_Nibble_Sel ;
  wire \Riu_Nibble_Sel[0]_i_1_n_0 ;
  wire Riu_Prsnt_0;
  wire Riu_Prsnt_1;
  wire Riu_Prsnt_2;
  wire Riu_Prsnt_3;
  wire [15:0]Riu_RdData_0;
  wire [15:0]Riu_RdData_1;
  wire [15:0]Riu_RdData_2;
  wire [15:0]Riu_RdData_3;
  wire \Riu_WrData[3]_i_1_n_0 ;
  wire Riu_Wr_En;
  wire Rx_Bs_EnVtc;
  wire Rx_Bs_EnVtc_i_1_n_0;
  wire Rx_Bs_EnVtc_i_2_n_0;
  wire Rx_Bs_EnVtc_i_3_n_0;
  wire Rx_Bs_EnVtc_i_4_n_0;
  wire Rx_Bs_Rst;
  wire Rx_Bs_RstDly;
  wire Rx_Bs_Rst_i_1_n_0;
  wire Rx_Bs_Rst_i_2_n_0;
  wire Rx_Bsc_Rst;
  wire [8:0]Rx_BtVal_0;
  wire [8:0]Rx_BtVal_1;
  wire [8:0]Rx_BtVal_2;
  wire [8:0]Rx_BtVal_3;
  wire \Rx_BtVal_3[8]_i_10_n_0 ;
  wire \Rx_BtVal_3[8]_i_11_n_0 ;
  wire \Rx_BtVal_3[8]_i_12_n_0 ;
  wire \Rx_BtVal_3[8]_i_13_n_0 ;
  wire \Rx_BtVal_3[8]_i_14_n_0 ;
  wire \Rx_BtVal_3[8]_i_15_n_0 ;
  wire \Rx_BtVal_3[8]_i_16_n_0 ;
  wire \Rx_BtVal_3[8]_i_17_n_0 ;
  wire \Rx_BtVal_3[8]_i_18_n_0 ;
  wire \Rx_BtVal_3[8]_i_19_n_0 ;
  wire \Rx_BtVal_3[8]_i_1_n_0 ;
  wire \Rx_BtVal_3[8]_i_2_n_0 ;
  wire \Rx_BtVal_3[8]_i_3_n_0 ;
  wire \Rx_BtVal_3[8]_i_4_n_0 ;
  wire \Rx_BtVal_3[8]_i_5_n_0 ;
  wire \Rx_BtVal_3[8]_i_6_n_0 ;
  wire \Rx_BtVal_3[8]_i_7_n_0 ;
  wire \Rx_BtVal_3[8]_i_8_n_0 ;
  wire \Rx_BtVal_3[8]_i_9_n_0 ;
  wire Rx_ClkOutPhy;
  wire Rx_Dly_Rdy;
  wire Rx_Locked;
  wire Rx_LogicRst;
  wire Rx_RiuClk;
  wire Rx_SysClk;
  wire Rx_Vtc_Rdy;
  wire Tx_Bs_Rst;
  wire Tx_Bs_RstDly_i_1_n_0;
  wire Tx_Bs_RstDly_i_2_n_0;
  wire Tx_Bs_RstDly_i_3_n_0;
  wire Tx_Bs_Rst_i_1_n_0;
  wire Tx_Bs_Rst_i_2_n_0;
  wire Tx_Bsc_EnVtc;
  wire Tx_Bsc_EnVtc_i_1_n_0;
  wire Tx_Bsc_EnVtc_i_2_n_0;
  wire Tx_Bsc_EnVtc_i_3_n_0;
  wire Tx_Bsc_EnVtc_i_4_n_0;
  wire Tx_Bsc_Rst_i_1_n_0;
  wire Tx_Bsc_Rst_i_2_n_0;
  wire Tx_Bsc_Rst_i_3_n_0;
  wire Tx_ClkOutPhy;
  wire Tx_Dly_Rdy;
  wire Tx_Locked;
  wire Tx_LogicRst;
  wire Tx_SysClk;
  wire Tx_Vtc_Rdy;
  wire Tx_WrClk;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKFBIN_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKFBOUT_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT0B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_DRDY_UNCONNECTED;
  wire [15:0]NLW_Clk_Rst_I_Plle3_Rx_DO_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKFBIN_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKFBOUT_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKOUT0B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKOUT1B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_DRDY_UNCONNECTED;
  wire [15:0]NLW_Clk_Rst_I_Plle3_Tx_DO_UNCONNECTED;

  assign Riu_Addr[5] = \^Riu_Addr [4];
  assign Riu_Addr[4] = \^Riu_Addr [4];
  assign Riu_Addr[3] = \^Riu_Addr [4];
  assign Riu_Addr[2] = \<const0> ;
  assign Riu_Addr[1:0] = \^Riu_Addr [1:0];
  assign Riu_Nibble_Sel[1] = \<const0> ;
  assign Riu_Nibble_Sel[0] = \^Riu_Nibble_Sel [0];
  assign Riu_WrData[15] = \<const0> ;
  assign Riu_WrData[14] = \<const0> ;
  assign Riu_WrData[13] = \<const0> ;
  assign Riu_WrData[12] = \<const0> ;
  assign Riu_WrData[11] = \<const0> ;
  assign Riu_WrData[10] = \<const0> ;
  assign Riu_WrData[9] = \<const0> ;
  assign Riu_WrData[8] = \<const0> ;
  assign Riu_WrData[7] = \<const0> ;
  assign Riu_WrData[6] = \<const0> ;
  assign Riu_WrData[5] = \<const0> ;
  assign Riu_WrData[4] = \<const0> ;
  assign Riu_WrData[3] = Riu_Wr_En;
  assign Riu_WrData[2] = Riu_Wr_En;
  assign Riu_WrData[1] = \<const0> ;
  assign Riu_WrData[0] = \<const0> ;
  assign Rx_Bsc_EnVtc = \<const0> ;
  assign Tx_Bs_EnVtc = \<const1> ;
  assign Tx_Bs_RstDly = Rx_Bs_RstDly;
  assign Tx_Bsc_Rst = Rx_Bsc_Rst;
  (* box_type = "PRIMITIVE" *) 
  BUFGCE_DIV #(
    .BUFGCE_DIVIDE(4),
    .CE_TYPE("SYNC"),
    .HARDSYNC_CLR("FALSE"),
    .IS_CE_INVERTED(1'b0),
    .IS_CLR_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Bufg_CtrlClk
       (.CE(1'b1),
        .CLR(1'b0),
        .I(ClockIn_se_out),
        .O(Rx_RiuClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_RxSysClk
       (.CE(Rx_Locked),
        .I(IntRx_ClkOut0),
        .O(Rx_SysClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_TxSysClk
       (.CE(Tx_Locked),
        .I(IntTx_ClkOut0),
        .O(Tx_SysClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_TxWrClk
       (.CE(Tx_Locked),
        .I(IntTx_ClkOut1),
        .O(Tx_WrClk));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "PLLE3_ADV" *) 
  (* box_type = "PRIMITIVE" *) 
  PLLE4_ADV #(
    .CLKFBOUT_MULT(2),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN_PERIOD(1.600000),
    .CLKOUT0_DIVIDE(4),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(8),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUTPHY_MODE("VCO_HALF"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER(0.010000),
    .STARTUP_WAIT("FALSE")) 
    Clk_Rst_I_Plle3_Rx
       (.CLKFBIN(NLW_Clk_Rst_I_Plle3_Rx_CLKFBIN_UNCONNECTED),
        .CLKFBOUT(NLW_Clk_Rst_I_Plle3_Rx_CLKFBOUT_UNCONNECTED),
        .CLKIN(ClockIn_se_out),
        .CLKOUT0(IntRx_ClkOut0),
        .CLKOUT0B(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1_UNCONNECTED),
        .CLKOUT1B(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1B_UNCONNECTED),
        .CLKOUTPHY(Rx_ClkOutPhy),
        .CLKOUTPHYEN(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_Clk_Rst_I_Plle3_Rx_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_Clk_Rst_I_Plle3_Rx_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(Rx_Locked),
        .PWRDWN(1'b0),
        .RST(ResetIn));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "PLLE3_ADV" *) 
  (* box_type = "PRIMITIVE" *) 
  PLLE4_ADV #(
    .CLKFBOUT_MULT(2),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN_PERIOD(1.600000),
    .CLKOUT0_DIVIDE(8),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(10),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUTPHY_MODE("VCO"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER(0.010000),
    .STARTUP_WAIT("FALSE")) 
    Clk_Rst_I_Plle3_Tx
       (.CLKFBIN(NLW_Clk_Rst_I_Plle3_Tx_CLKFBIN_UNCONNECTED),
        .CLKFBOUT(NLW_Clk_Rst_I_Plle3_Tx_CLKFBOUT_UNCONNECTED),
        .CLKIN(ClockIn_se_out),
        .CLKOUT0(IntTx_ClkOut0),
        .CLKOUT0B(NLW_Clk_Rst_I_Plle3_Tx_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(IntTx_ClkOut1),
        .CLKOUT1B(NLW_Clk_Rst_I_Plle3_Tx_CLKOUT1B_UNCONNECTED),
        .CLKOUTPHY(Tx_ClkOutPhy),
        .CLKOUTPHYEN(CLKOUTPHYEN),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_Clk_Rst_I_Plle3_Tx_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_Clk_Rst_I_Plle3_Tx_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(Tx_Locked),
        .PWRDWN(1'b0),
        .RST(RST));
  GND GND
       (.G(\<const0> ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxDlyRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Dly_Rdy),
        .Q(IntCtrl_RxDlyRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxDlyRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxDlyRdy[0]),
        .Q(IntCtrl_RxDlyRdy[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxLocked_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Locked),
        .Q(IntCtrl_RxLocked[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxLocked_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxLocked[0]),
        .Q(IntCtrl_RxLocked[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2AAAAAAAAAAAAAAA)) 
    IntCtrl_RxLogicRst_i_1
       (.I0(IntCtrl_RxLogicRst_reg_n_0),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[5]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[6]),
        .I5(IntCtrl_RxLogicRst_i_2_n_0),
        .O(IntCtrl_RxLogicRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    IntCtrl_RxLogicRst_i_2
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[0]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[4]),
        .O(IntCtrl_RxLogicRst_i_2_n_0));
  FDSE IntCtrl_RxLogicRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxLogicRst_i_1_n_0),
        .Q(IntCtrl_RxLogicRst_reg_n_0),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFDFF000001C0)) 
    IntCtrl_RxPllClkOutPhyEn_i_1
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[7]),
        .I4(IntCtrl_RxPllClkOutPhyEn_i_2_n_0),
        .I5(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .O(IntCtrl_RxPllClkOutPhyEn_i_1_n_0));
  LUT6 #(
    .INIT(64'hF7F7FFFFFFFFFFFE)) 
    IntCtrl_RxPllClkOutPhyEn_i_2
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Tx_Bs_RstDly_i_3_n_0),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[5]),
        .I5(Debug_Out[4]),
        .O(IntCtrl_RxPllClkOutPhyEn_i_2_n_0));
  FDRE IntCtrl_RxPllClkOutPhyEn_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxPllClkOutPhyEn_i_1_n_0),
        .Q(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxVtcRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Vtc_Rdy),
        .Q(IntCtrl_RxVtcRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxVtcRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxVtcRdy[0]),
        .Q(IntCtrl_RxVtcRdy[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAFFAAAAFFFFFFFF)) 
    \IntCtrl_State[0]_i_1 
       (.I0(\IntCtrl_State[0]_i_2_n_0 ),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(\IntCtrl_State[0]_i_3_n_0 ),
        .I3(\IntCtrl_State[0]_i_4_n_0 ),
        .I4(\IntCtrl_State[0]_i_5_n_0 ),
        .I5(Debug_Out[0]),
        .O(\IntCtrl_State[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008002)) 
    \IntCtrl_State[0]_i_2 
       (.I0(\IntCtrl_State[0]_i_6_n_0 ),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[4]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(\IntCtrl_State[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \IntCtrl_State[0]_i_3 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[0]_i_4 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .O(\IntCtrl_State[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \IntCtrl_State[0]_i_5 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[6]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[5]),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h81)) 
    \IntCtrl_State[0]_i_6 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .O(\IntCtrl_State[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAAAAAAAAABAAA)) 
    \IntCtrl_State[1]_i_1 
       (.I0(\IntCtrl_State[1]_i_2_n_0 ),
        .I1(\IntCtrl_State[1]_i_3_n_0 ),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(Debug_Out[6]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6246666666666666)) 
    \IntCtrl_State[1]_i_2 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(\IntCtrl_State[6]_i_3_n_0 ),
        .I5(Tx_Bsc_EnVtc_i_2_n_0),
        .O(\IntCtrl_State[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7EEFFDFF)) 
    \IntCtrl_State[1]_i_3 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[6]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[1]),
        .O(\IntCtrl_State[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF6F66666666)) 
    \IntCtrl_State[2]_i_1 
       (.I0(\IntCtrl_State[6]_i_4_n_0 ),
        .I1(Debug_Out[2]),
        .I2(\IntCtrl_State[2]_i_2_n_0 ),
        .I3(\IntCtrl_State[2]_i_3_n_0 ),
        .I4(\IntCtrl_State[2]_i_4_n_0 ),
        .I5(Debug_Out[5]),
        .O(\IntCtrl_State[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFCE2F)) 
    \IntCtrl_State[2]_i_2 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[7]),
        .I4(Tx_Bs_RstDly_i_3_n_0),
        .I5(\IntCtrl_State[2]_i_5_n_0 ),
        .O(\IntCtrl_State[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \IntCtrl_State[2]_i_3 
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State[6]_i_4_n_0 ),
        .I2(Debug_Out[6]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[7]),
        .I5(\IntCtrl_State[2]_i_5_n_0 ),
        .O(\IntCtrl_State[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    \IntCtrl_State[2]_i_4 
       (.I0(\IntCtrl_State[2]_i_6_n_0 ),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[0]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[2]_i_5 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .O(\IntCtrl_State[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \IntCtrl_State[2]_i_6 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[3]),
        .O(\IntCtrl_State[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hBF3FFFFFC0C00000)) 
    \IntCtrl_State[3]_i_1 
       (.I0(\IntCtrl_State[3]_i_2_n_0 ),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[3]),
        .O(\IntCtrl_State[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h80000080)) 
    \IntCtrl_State[3]_i_2 
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[6]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7FFF8000)) 
    \IntCtrl_State[4]_i_1 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(Debug_Out[4]),
        .I5(\IntCtrl_State[4]_i_2_n_0 ),
        .O(\IntCtrl_State[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8A8A828A00000000)) 
    \IntCtrl_State[4]_i_2 
       (.I0(\IntCtrl_State[0]_i_5_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[0]),
        .O(\IntCtrl_State[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFAAAAAA55AAAAAA)) 
    \IntCtrl_State[5]_i_1 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[4]),
        .I4(\IntCtrl_State[5]_i_2_n_0 ),
        .I5(\IntCtrl_State_reg[5]_i_3_n_0 ),
        .O(\IntCtrl_State[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \IntCtrl_State[5]_i_2 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .O(\IntCtrl_State[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0004040400000004)) 
    \IntCtrl_State[5]_i_4 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[2]),
        .O(\IntCtrl_State[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000044000300445)) 
    \IntCtrl_State[5]_i_5 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[6]),
        .O(\IntCtrl_State[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFFC0000000)) 
    \IntCtrl_State[6]_i_1 
       (.I0(\IntCtrl_State[6]_i_2_n_0 ),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[2]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(\IntCtrl_State[6]_i_4_n_0 ),
        .I5(Debug_Out[6]),
        .O(\IntCtrl_State[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00008881)) 
    \IntCtrl_State[6]_i_2 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(\IntCtrl_State[6]_i_5_n_0 ),
        .O(\IntCtrl_State[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \IntCtrl_State[6]_i_3 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[4]),
        .O(\IntCtrl_State[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \IntCtrl_State[6]_i_4 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .O(\IntCtrl_State[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFFE7FFFFFFE)) 
    \IntCtrl_State[6]_i_5 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[2]),
        .I5(\IntCtrl_State[6]_i_6_n_0 ),
        .O(\IntCtrl_State[6]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[6]_i_6 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .O(\IntCtrl_State[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8F0F0F0F0F0F0)) 
    \IntCtrl_State[7]_i_1 
       (.I0(Debug_Out[5]),
        .I1(\Rx_BtVal_3[8]_i_2_n_0 ),
        .I2(\IntCtrl_State[7]_i_3_n_0 ),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[4]),
        .O(IntCtrl_State));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hCB)) 
    \IntCtrl_State[7]_i_2 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State[7]_i_4_n_0 ),
        .O(\IntCtrl_State[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFEFEFEFEFEFE)) 
    \IntCtrl_State[7]_i_3 
       (.I0(\IntCtrl_State[7]_i_5_n_0 ),
        .I1(\IntCtrl_State[7]_i_6_n_0 ),
        .I2(\IntCtrl_State[7]_i_7_n_0 ),
        .I3(Rx_Bs_EnVtc_i_2_n_0),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[4]),
        .O(\IntCtrl_State[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \IntCtrl_State[7]_i_4 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[3]),
        .O(\IntCtrl_State[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hA66A)) 
    \IntCtrl_State[7]_i_5 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[2]),
        .O(\IntCtrl_State[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFF0FFD)) 
    \IntCtrl_State[7]_i_6 
       (.I0(Debug_Out[3]),
        .I1(\IntCtrl_State[7]_i_8_n_0 ),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFAFAFAFABEFFBEBE)) 
    \IntCtrl_State[7]_i_7 
       (.I0(\IntCtrl_State[7]_i_9_n_0 ),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[3]),
        .I4(IntCtrl_TxVtcRdy[1]),
        .I5(Debug_Out[2]),
        .O(\IntCtrl_State[7]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hEA)) 
    \IntCtrl_State[7]_i_8 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(IntCtrl_TxLocked[1]),
        .I2(IntCtrl_RxLocked[1]),
        .O(\IntCtrl_State[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h000000008FFFFFFF)) 
    \IntCtrl_State[7]_i_9 
       (.I0(IntCtrl_TxDlyRdy[1]),
        .I1(IntCtrl_RxDlyRdy[1]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[4]),
        .O(\IntCtrl_State[7]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hDC9C9C9C)) 
    \IntCtrl_State[8]_i_1 
       (.I0(\IntCtrl_State[7]_i_4_n_0 ),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[4]),
        .O(\IntCtrl_State[8]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[0] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[0]_i_1_n_0 ),
        .Q(Debug_Out[0]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[1] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[1]_i_1_n_0 ),
        .Q(Debug_Out[1]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[2] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[2]_i_1_n_0 ),
        .Q(Debug_Out[2]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[3] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[3]_i_1_n_0 ),
        .Q(Debug_Out[3]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[4] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[4]_i_1_n_0 ),
        .Q(Debug_Out[4]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[5] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[5]_i_1_n_0 ),
        .Q(Debug_Out[5]),
        .R(IntCtrl_Reset));
  MUXF7 \IntCtrl_State_reg[5]_i_3 
       (.I0(\IntCtrl_State[5]_i_4_n_0 ),
        .I1(\IntCtrl_State[5]_i_5_n_0 ),
        .O(\IntCtrl_State_reg[5]_i_3_n_0 ),
        .S(Debug_Out[3]));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[6] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[6]_i_1_n_0 ),
        .Q(Debug_Out[6]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[7] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[7]_i_2_n_0 ),
        .Q(Debug_Out[7]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[8] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[8]_i_1_n_0 ),
        .Q(\IntCtrl_State_reg_n_0_[8] ),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxDlyRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Dly_Rdy),
        .Q(IntCtrl_TxDlyRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxDlyRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxDlyRdy[0]),
        .Q(IntCtrl_TxDlyRdy[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxLocked_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Locked),
        .Q(IntCtrl_TxLocked[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxLocked_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxLocked[0]),
        .Q(IntCtrl_TxLocked[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAA8AAAAAAAA)) 
    IntCtrl_TxLogicRst_i_1
       (.I0(IntCtrl_TxLogicRst_reg_n_0),
        .I1(IntCtrl_TxLogicRst_i_2_n_0),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[0]),
        .I4(IntCtrl_TxLogicRst_i_3_n_0),
        .I5(Debug_Out[4]),
        .O(IntCtrl_TxLogicRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    IntCtrl_TxLogicRst_i_2
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[3]),
        .O(IntCtrl_TxLogicRst_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    IntCtrl_TxLogicRst_i_3
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .O(IntCtrl_TxLogicRst_i_3_n_0));
  FDSE IntCtrl_TxLogicRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxLogicRst_i_1_n_0),
        .Q(IntCtrl_TxLogicRst_reg_n_0),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    IntCtrl_TxPllClkOutPhyEn_i_1
       (.I0(Tx_Bsc_Rst_i_3_n_0),
        .I1(IntCtrl_TxPllClkOutPhyEn_i_2_n_0),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(CLKOUTPHYEN),
        .O(IntCtrl_TxPllClkOutPhyEn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hE)) 
    IntCtrl_TxPllClkOutPhyEn_i_2
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .O(IntCtrl_TxPllClkOutPhyEn_i_2_n_0));
  FDRE IntCtrl_TxPllClkOutPhyEn_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxPllClkOutPhyEn_i_1_n_0),
        .Q(CLKOUTPHYEN),
        .R(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFBFFF00040000)) 
    IntCtrl_TxPllRst_i_1
       (.I0(Tx_Bsc_Rst_i_3_n_0),
        .I1(IntCtrl_TxPllRst_i_2_n_0),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[3]),
        .I5(RST),
        .O(IntCtrl_TxPllRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h1)) 
    IntCtrl_TxPllRst_i_2
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .O(IntCtrl_TxPllRst_i_2_n_0));
  FDRE IntCtrl_TxPllRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxPllRst_i_1_n_0),
        .Q(RST),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxVtcRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Vtc_Rdy),
        .Q(IntCtrl_TxVtcRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxVtcRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxVtcRdy[0]),
        .Q(IntCtrl_TxVtcRdy[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \Riu_Addr[0]_i_1 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[4]),
        .I2(\Riu_Addr[0]_i_2_n_0 ),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[6]),
        .I5(\Riu_Addr[0]_i_3_n_0 ),
        .O(Riu_Addr0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \Riu_Addr[0]_i_2 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[7]),
        .O(\Riu_Addr[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \Riu_Addr[0]_i_3 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[1]),
        .O(\Riu_Addr[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \Riu_Addr[1]_i_1 
       (.I0(\Riu_Addr[1]_i_2_n_0 ),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[4]),
        .O(Riu_Addr0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Riu_Addr[1]_i_2 
       (.I0(Debug_Out[1]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[6]),
        .O(\Riu_Addr[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Riu_Addr[5]_i_1 
       (.I0(\Riu_Addr[5]_i_3_n_0 ),
        .I1(\Riu_Addr[5]_i_4_n_0 ),
        .I2(\Riu_Addr[5]_i_5_n_0 ),
        .I3(\Riu_Addr[5]_i_6_n_0 ),
        .I4(\Riu_Addr[5]_i_7_n_0 ),
        .O(\Riu_Addr[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6F666666)) 
    \Riu_Addr[5]_i_10 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[3]),
        .O(\Riu_Addr[5]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0A002000)) 
    \Riu_Addr[5]_i_2 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[3]),
        .O(Riu_Addr0_in[5]));
  LUT6 #(
    .INIT(64'hCCEEFECCFCFEFECC)) 
    \Riu_Addr[5]_i_3 
       (.I0(\Riu_Addr[5]_i_9_n_0 ),
        .I1(\Riu_Addr[5]_i_10_n_0 ),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0080AFAF00800080)) 
    \Riu_Addr[5]_i_4 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h60EE60EE60EE6060)) 
    \Riu_Addr[5]_i_5 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[7]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\Riu_Addr[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h55FFFFFF00303030)) 
    \Riu_Addr[5]_i_6 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[7]),
        .I5(Debug_Out[0]),
        .O(\Riu_Addr[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h74443000F444FF00)) 
    \Riu_Addr[5]_i_7 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[6]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[1]),
        .O(\Riu_Addr[5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \Riu_Addr[5]_i_8 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Riu_Addr[5]_i_9 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .O(\Riu_Addr[5]_i_9_n_0 ));
  FDRE \Riu_Addr_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[0]),
        .Q(\^Riu_Addr [0]),
        .R(IntCtrl_Reset));
  FDRE \Riu_Addr_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[1]),
        .Q(\^Riu_Addr [1]),
        .R(IntCtrl_Reset));
  FDRE \Riu_Addr_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[5]),
        .Q(\^Riu_Addr [4]),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h20082088)) 
    \Riu_Nibble_Sel[0]_i_1 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[3]),
        .I4(Debug_Out[0]),
        .O(\Riu_Nibble_Sel[0]_i_1_n_0 ));
  FDRE \Riu_Nibble_Sel_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(\Riu_Nibble_Sel[0]_i_1_n_0 ),
        .Q(\^Riu_Nibble_Sel ),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00000800)) 
    \Riu_WrData[3]_i_1 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[0]),
        .O(\Riu_WrData[3]_i_1_n_0 ));
  FDRE \Riu_WrData_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(\Riu_WrData[3]_i_1_n_0 ),
        .Q(Riu_Wr_En),
        .R(IntCtrl_Reset));
  LUT5 #(
    .INIT(32'hFF5D0051)) 
    Rx_Bs_EnVtc_i_1
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[7]),
        .I2(Rx_Bs_EnVtc_i_2_n_0),
        .I3(Rx_Bs_EnVtc_i_3_n_0),
        .I4(Rx_Bs_EnVtc),
        .O(Rx_Bs_EnVtc_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    Rx_Bs_EnVtc_i_2
       (.I0(Riu_Prsnt_3),
        .I1(Riu_RdData_3[11]),
        .I2(Riu_RdData_0[11]),
        .I3(Riu_Prsnt_0),
        .I4(Rx_Bs_EnVtc_i_4_n_0),
        .O(Rx_Bs_EnVtc_i_2_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFEF)) 
    Rx_Bs_EnVtc_i_3
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[5]),
        .I2(\Riu_Addr[1]_i_2_n_0 ),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[4]),
        .I5(Debug_Out[2]),
        .O(Rx_Bs_EnVtc_i_3_n_0));
  LUT4 #(
    .INIT(16'h4F44)) 
    Rx_Bs_EnVtc_i_4
       (.I0(Riu_RdData_2[11]),
        .I1(Riu_Prsnt_2),
        .I2(Riu_RdData_1[11]),
        .I3(Riu_Prsnt_1),
        .O(Rx_Bs_EnVtc_i_4_n_0));
  FDSE Rx_Bs_EnVtc_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Bs_EnVtc_i_1_n_0),
        .Q(Rx_Bs_EnVtc),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008003)) 
    Rx_Bs_Rst_i_1
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(Rx_Bs_Rst_i_2_n_0),
        .I5(Rx_Bs_Rst),
        .O(Rx_Bs_Rst_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFDFFFFF4F)) 
    Rx_Bs_Rst_i_2
       (.I0(Debug_Out[5]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(Rx_Bs_Rst_i_2_n_0));
  FDSE Rx_Bs_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Bs_Rst_i_1_n_0),
        .Q(Rx_Bs_Rst),
        .S(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[1]),
        .Q(Rx_BtVal_0[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[2]),
        .Q(Rx_BtVal_0[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[3]),
        .Q(Rx_BtVal_0[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[4]),
        .Q(Rx_BtVal_0[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[5]),
        .Q(Rx_BtVal_0[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[6]),
        .Q(Rx_BtVal_0[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[7]),
        .Q(Rx_BtVal_0[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[8]),
        .Q(Rx_BtVal_0[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[9]),
        .Q(Rx_BtVal_0[8]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[1]),
        .Q(Rx_BtVal_1[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[2]),
        .Q(Rx_BtVal_1[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[3]),
        .Q(Rx_BtVal_1[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[4]),
        .Q(Rx_BtVal_1[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[5]),
        .Q(Rx_BtVal_1[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[6]),
        .Q(Rx_BtVal_1[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[7]),
        .Q(Rx_BtVal_1[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[8]),
        .Q(Rx_BtVal_1[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[9]),
        .Q(Rx_BtVal_1[8]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[1]),
        .Q(Rx_BtVal_2[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[2]),
        .Q(Rx_BtVal_2[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[3]),
        .Q(Rx_BtVal_2[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[4]),
        .Q(Rx_BtVal_2[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[5]),
        .Q(Rx_BtVal_2[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[6]),
        .Q(Rx_BtVal_2[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[7]),
        .Q(Rx_BtVal_2[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[8]),
        .Q(Rx_BtVal_2[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[9]),
        .Q(Rx_BtVal_2[8]),
        .R(IntCtrl_Reset));
  LUT5 #(
    .INIT(32'h00200000)) 
    \Rx_BtVal_3[8]_i_1 
       (.I0(\Rx_BtVal_3[8]_i_2_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_3_n_0 ),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[3]),
        .O(\Rx_BtVal_3[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_10 
       (.I0(Riu_RdData_0[13]),
        .I1(Riu_RdData_0[0]),
        .I2(Riu_RdData_0[15]),
        .I3(Riu_RdData_0[10]),
        .O(\Rx_BtVal_3[8]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_11 
       (.I0(Riu_RdData_2[10]),
        .I1(Riu_RdData_2[3]),
        .I2(Riu_RdData_2[8]),
        .I3(Riu_RdData_2[1]),
        .O(\Rx_BtVal_3[8]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_12 
       (.I0(Riu_RdData_2[14]),
        .I1(Riu_RdData_2[5]),
        .I2(Riu_RdData_2[15]),
        .I3(Riu_RdData_2[2]),
        .O(\Rx_BtVal_3[8]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \Rx_BtVal_3[8]_i_13 
       (.I0(Riu_RdData_2[11]),
        .I1(Riu_Prsnt_2),
        .I2(Riu_RdData_2[0]),
        .I3(Riu_RdData_2[13]),
        .I4(Riu_RdData_2[7]),
        .I5(Riu_RdData_2[9]),
        .O(\Rx_BtVal_3[8]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \Rx_BtVal_3[8]_i_14 
       (.I0(Riu_RdData_1[14]),
        .I1(Riu_RdData_1[5]),
        .I2(Riu_RdData_1[1]),
        .I3(Riu_RdData_1[15]),
        .I4(Riu_RdData_1[9]),
        .I5(Riu_Prsnt_1),
        .O(\Rx_BtVal_3[8]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_15 
       (.I0(Riu_RdData_1[10]),
        .I1(Riu_RdData_1[11]),
        .I2(Riu_RdData_1[7]),
        .I3(Riu_RdData_1[2]),
        .O(\Rx_BtVal_3[8]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_16 
       (.I0(Riu_RdData_1[12]),
        .I1(Riu_RdData_1[0]),
        .I2(Riu_RdData_1[13]),
        .I3(Riu_RdData_1[6]),
        .O(\Rx_BtVal_3[8]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \Rx_BtVal_3[8]_i_17 
       (.I0(Riu_RdData_3[7]),
        .I1(Riu_RdData_3[2]),
        .I2(Riu_RdData_3[1]),
        .I3(Riu_RdData_3[5]),
        .I4(Riu_RdData_3[6]),
        .I5(Riu_RdData_3[14]),
        .O(\Rx_BtVal_3[8]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_18 
       (.I0(Riu_RdData_3[15]),
        .I1(Riu_RdData_3[0]),
        .I2(Riu_RdData_3[10]),
        .I3(Riu_RdData_3[9]),
        .O(\Rx_BtVal_3[8]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \Rx_BtVal_3[8]_i_19 
       (.I0(Riu_RdData_3[8]),
        .I1(Riu_RdData_3[4]),
        .I2(Riu_Prsnt_3),
        .I3(Riu_RdData_3[12]),
        .O(\Rx_BtVal_3[8]_i_19_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \Rx_BtVal_3[8]_i_2 
       (.I0(\Rx_BtVal_3[8]_i_4_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_5_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_6_n_0 ),
        .I3(\Rx_BtVal_3[8]_i_7_n_0 ),
        .O(\Rx_BtVal_3[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    \Rx_BtVal_3[8]_i_3 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[4]),
        .O(\Rx_BtVal_3[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000020000)) 
    \Rx_BtVal_3[8]_i_4 
       (.I0(\Rx_BtVal_3[8]_i_8_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_9_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_10_n_0 ),
        .I3(Riu_RdData_0[9]),
        .I4(Riu_Prsnt_0),
        .I5(Riu_RdData_0[8]),
        .O(\Rx_BtVal_3[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \Rx_BtVal_3[8]_i_5 
       (.I0(\Rx_BtVal_3[8]_i_11_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_12_n_0 ),
        .I2(Riu_RdData_2[6]),
        .I3(Riu_RdData_2[12]),
        .I4(Riu_RdData_2[4]),
        .I5(\Rx_BtVal_3[8]_i_13_n_0 ),
        .O(\Rx_BtVal_3[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Rx_BtVal_3[8]_i_6 
       (.I0(\Rx_BtVal_3[8]_i_14_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_15_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_16_n_0 ),
        .I3(Riu_RdData_1[3]),
        .I4(Riu_RdData_1[8]),
        .I5(Riu_RdData_1[4]),
        .O(\Rx_BtVal_3[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Rx_BtVal_3[8]_i_7 
       (.I0(\Rx_BtVal_3[8]_i_17_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_18_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_19_n_0 ),
        .I3(Riu_RdData_3[3]),
        .I4(Riu_RdData_3[13]),
        .I5(Riu_RdData_3[11]),
        .O(\Rx_BtVal_3[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \Rx_BtVal_3[8]_i_8 
       (.I0(Riu_RdData_0[2]),
        .I1(Riu_RdData_0[1]),
        .I2(Riu_RdData_0[5]),
        .I3(Riu_RdData_0[14]),
        .I4(Riu_RdData_0[6]),
        .I5(Riu_RdData_0[7]),
        .O(\Rx_BtVal_3[8]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_9 
       (.I0(Riu_RdData_0[4]),
        .I1(Riu_RdData_0[3]),
        .I2(Riu_RdData_0[12]),
        .I3(Riu_RdData_0[11]),
        .O(\Rx_BtVal_3[8]_i_9_n_0 ));
  FDRE \Rx_BtVal_3_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[1]),
        .Q(Rx_BtVal_3[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[2]),
        .Q(Rx_BtVal_3[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[3]),
        .Q(Rx_BtVal_3[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[4]),
        .Q(Rx_BtVal_3[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[5]),
        .Q(Rx_BtVal_3[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[6]),
        .Q(Rx_BtVal_3[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[7]),
        .Q(Rx_BtVal_3[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[8]),
        .Q(Rx_BtVal_3[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[9]),
        .Q(Rx_BtVal_3[8]),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hF701)) 
    Tx_Bs_RstDly_i_1
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[5]),
        .I2(Tx_Bs_RstDly_i_2_n_0),
        .I3(Rx_Bs_RstDly),
        .O(Tx_Bs_RstDly_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    Tx_Bs_RstDly_i_2
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[2]),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(Tx_Bs_RstDly_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Tx_Bs_RstDly_i_3
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .O(Tx_Bs_RstDly_i_3_n_0));
  FDSE Tx_Bs_RstDly_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bs_RstDly_i_1_n_0),
        .Q(Rx_Bs_RstDly),
        .S(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hFF7F0001)) 
    Tx_Bs_Rst_i_1
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .I3(Tx_Bs_Rst_i_2_n_0),
        .I4(Tx_Bs_Rst),
        .O(Tx_Bs_Rst_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    Tx_Bs_Rst_i_2
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[6]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[1]),
        .O(Tx_Bs_Rst_i_2_n_0));
  FDSE Tx_Bs_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bs_Rst_i_1_n_0),
        .Q(Tx_Bs_Rst),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000200)) 
    Tx_Bsc_EnVtc_i_1
       (.I0(Tx_Bsc_EnVtc_i_2_n_0),
        .I1(Tx_Bsc_EnVtc_i_3_n_0),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[5]),
        .I4(Tx_Bsc_EnVtc_i_4_n_0),
        .I5(Tx_Bsc_EnVtc),
        .O(Tx_Bsc_EnVtc_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h04)) 
    Tx_Bsc_EnVtc_i_2
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[6]),
        .O(Tx_Bsc_EnVtc_i_2_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    Tx_Bsc_EnVtc_i_3
       (.I0(IntCtrl_RxDlyRdy[1]),
        .I1(IntCtrl_TxDlyRdy[1]),
        .O(Tx_Bsc_EnVtc_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    Tx_Bsc_EnVtc_i_4
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[0]),
        .O(Tx_Bsc_EnVtc_i_4_n_0));
  FDRE Tx_Bsc_EnVtc_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bsc_EnVtc_i_1_n_0),
        .Q(Tx_Bsc_EnVtc),
        .R(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFDFF00000400)) 
    Tx_Bsc_Rst_i_1
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[3]),
        .I3(Tx_Bsc_Rst_i_2_n_0),
        .I4(Tx_Bsc_Rst_i_3_n_0),
        .I5(Rx_Bsc_Rst),
        .O(Tx_Bsc_Rst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    Tx_Bsc_Rst_i_2
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[2]),
        .O(Tx_Bsc_Rst_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    Tx_Bsc_Rst_i_3
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .O(Tx_Bsc_Rst_i_3_n_0));
  FDSE Tx_Bsc_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bsc_Rst_i_1_n_0),
        .Q(Rx_Bsc_Rst),
        .S(IntCtrl_Reset));
  VCC VCC
       (.P(\<const1> ));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* XILINX_LEGACY_PRIM = "IBUFGDS" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .DIFF_TERM("FALSE"),
    .IOSTANDARD("DEFAULT")) 
    iclkbuf
       (.I(ClockIn_p),
        .IB(ClockIn_n),
        .O(ClockIn_se_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_24 reset_sync_ctrl_rst
       (.ResetIn(ResetIn),
        .reset_out(IntCtrl_Reset),
        .reset_sync1_0(Rx_RiuClk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_25 reset_sync_rx_cdc_rst
       (.Rx_LogicRst(Rx_LogicRst),
        .Rx_SysClk(Rx_SysClk),
        .reset_in(IntCtrl_RxLogicRst_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_26 reset_sync_tx_cdc_rst
       (.Tx_LogicRst(Tx_LogicRst),
        .Tx_WrClk(Tx_WrClk),
        .reset_in(IntCtrl_TxLogicRst_reg_n_0));
endmodule

(* C_BtslcNulType = "SERIAL" *) (* C_BusRxBitCtrlIn = "40" *) (* C_BusRxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlIn = "40" *) (* C_BusTxBitCtrlInTri = "40" *) (* C_BusTxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlOutTri = "40" *) (* C_BytePosition = "0" *) (* C_Cascade = "FALSE" *) 
(* C_CntValue = "9" *) (* C_Ctrl_Clk = "EXTERNAL" *) (* C_Delay_Format = "COUNT" *) 
(* C_Delay_Type = "VAR_LOAD" *) (* C_Delay_Value = "0" *) (* C_Delay_Value_Ext = "0" *) 
(* C_Div_Mode = "DIV2" *) (* C_En_Clk_To_Ext_North = "DISABLE" *) (* C_En_Clk_To_Ext_South = "DISABLE" *) 
(* C_En_Dyn_Odly_Mode = "FALSE" *) (* C_En_Other_Nclk = "FALSE" *) (* C_En_Other_Pclk = "FALSE" *) 
(* C_Fifo_Sync_Mode = "FALSE" *) (* C_Idly_Vt_Track = "TRUE" *) (* C_Inv_Rxclk = "FALSE" *) 
(* C_IoBank = "44" *) (* C_Is_Clk_Ext_Inverted = "1'b0" *) (* C_Is_Clk_Inverted = "1'b0" *) 
(* C_Is_Rst_Dly_Ext_Inverted = "1'b0" *) (* C_Is_Rst_Dly_Inverted = "1'b0" *) (* C_Is_Rst_Inverted = "1'b0" *) 
(* C_NibbleType = "7" *) (* C_Odly_Vt_Track = "TRUE" *) (* C_Part = "XCKU060" *) 
(* C_Qdly_Vt_Track = "TRUE" *) (* C_Read_Idle_Count = "6'b000000" *) (* C_RefClk_Frequency = "312.500000" *) 
(* C_RefClk_Src = "PLLCLK" *) (* C_Rounding_Factor = "16" *) (* C_RxGate_Extend = "FALSE" *) 
(* C_Rx_Clk_Phase_n = "SHIFT_90" *) (* C_Rx_Clk_Phase_p = "SHIFT_90" *) (* C_Rx_Data_Width = "4" *) 
(* C_Rx_Gating = "DISABLE" *) (* C_Self_Calibrate = "ENABLE" *) (* C_Serial_Mode = "TRUE" *) 
(* C_Tx_Gating = "DISABLE" *) (* C_Update_Mode = "ASYNC" *) (* C_Update_Mode_Ext = "ASYNC" *) 
(* C_UsedBitslices = "7'b0000011" *) (* keep_hierarchy = "true" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Rx_Nibble
   (Rx_Bsc_Rst,
    Rx_Bs_Rst,
    Rx_Rst_Dly,
    Rx_Bsc_En_Vtc,
    Rx_Bs_En_Vtc,
    Rx_Riu_Clk,
    Rx_Riu_Addr,
    Rx_Riu_Wr_Data,
    Rx_Riu_Rd_Data,
    Rx_Riu_Valid,
    Rx_Riu_Prsnt,
    Rx_Riu_Wr_En,
    Rx_Riu_Nibble_Sel,
    Rx_Pll_Clk,
    Rx_RefClk,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    Rx_Dyn_Dci,
    Rx_Tbyte_In,
    Rx_Phy_Rden,
    Rx_Clk_From_Ext,
    Rx_Pclk_Nibble_In,
    Rx_Nclk_Nibble_In,
    Rx_Nclk_Nibble_Out,
    Rx_Pclk_Nibble_Out,
    Rx_Clk_To_Ext_North,
    Rx_Clk_To_Ext_South,
    Rx_Data_In,
    Rx_Q_Out,
    Rx_Q_CombOut,
    Fifo_Rd_Clk,
    Fifo_Wrclk_Out,
    Fifo_Rd_En,
    Fifo_Empty,
    Rx_Ce,
    Rx_Clk,
    Rx_Inc,
    Rx_Load,
    Rx_CntValueIn,
    Rx_CntValueOut,
    Rx_Ce_Ext,
    Rx_Inc_Ext,
    Rx_Load_Ext,
    Rx_CntValueIn_Ext,
    Rx_CntValueOut_Ext);
  input Rx_Bsc_Rst;
  input Rx_Bs_Rst;
  input Rx_Rst_Dly;
  input Rx_Bsc_En_Vtc;
  input Rx_Bs_En_Vtc;
  input Rx_Riu_Clk;
  input [5:0]Rx_Riu_Addr;
  input [15:0]Rx_Riu_Wr_Data;
  output [15:0]Rx_Riu_Rd_Data;
  output Rx_Riu_Valid;
  output Rx_Riu_Prsnt;
  input Rx_Riu_Wr_En;
  input Rx_Riu_Nibble_Sel;
  input Rx_Pll_Clk;
  input Rx_RefClk;
  output Rx_Dly_Rdy;
  output Rx_Vtc_Rdy;
  output [6:0]Rx_Dyn_Dci;
  input [3:0]Rx_Tbyte_In;
  input [3:0]Rx_Phy_Rden;
  input Rx_Clk_From_Ext;
  input Rx_Pclk_Nibble_In;
  input Rx_Nclk_Nibble_In;
  output Rx_Nclk_Nibble_Out;
  output Rx_Pclk_Nibble_Out;
  output Rx_Clk_To_Ext_North;
  output Rx_Clk_To_Ext_South;
  input [6:0]Rx_Data_In;
  (* dont_touch = "true" *) output [27:0]Rx_Q_Out;
  (* dont_touch = "true" *) output [6:0]Rx_Q_CombOut;
  input [6:0]Fifo_Rd_Clk;
  output Fifo_Wrclk_Out;
  input [6:0]Fifo_Rd_En;
  output [6:0]Fifo_Empty;
  input [6:0]Rx_Ce;
  input Rx_Clk;
  input [6:0]Rx_Inc;
  input [6:0]Rx_Load;
  input [62:0]Rx_CntValueIn;
  output [62:0]Rx_CntValueOut;
  input [6:0]Rx_Ce_Ext;
  input [6:0]Rx_Inc_Ext;
  input [6:0]Rx_Load_Ext;
  input [62:0]Rx_CntValueIn_Ext;
  output [62:0]Rx_CntValueOut_Ext;

  wire \<const0> ;
  wire \<const1> ;
  wire [1:0]\^Fifo_Empty ;
  wire [6:0]Fifo_Rd_Clk;
  wire [6:0]Fifo_Rd_En;
  wire Fifo_Wrclk_Out;
  wire [39:0]RX_BIT_CTRL_IN0;
  wire [39:0]RX_BIT_CTRL_IN1;
  wire [39:0]RX_BIT_CTRL_OUT0;
  wire [39:0]RX_BIT_CTRL_OUT1;
  wire Rx_Bs_En_Vtc;
  wire Rx_Bs_Rst;
  wire Rx_Bsc_En_Vtc;
  wire Rx_Bsc_Rst;
  wire [6:0]Rx_Ce;
  wire [6:0]Rx_Ce_Ext;
  wire Rx_Clk;
  wire Rx_Clk_From_Ext;
  wire Rx_Clk_To_Ext_North;
  wire Rx_Clk_To_Ext_South;
  wire [62:0]Rx_CntValueIn;
  wire [62:0]Rx_CntValueIn_Ext;
  wire [17:0]\^Rx_CntValueOut ;
  wire [17:0]\^Rx_CntValueOut_Ext ;
  wire [6:0]Rx_Data_In;
  wire Rx_Dly_Rdy;
  wire [6:0]Rx_Dyn_Dci;
  wire [6:0]Rx_Inc;
  wire [6:0]Rx_Inc_Ext;
  wire [6:0]Rx_Load;
  wire [6:0]Rx_Load_Ext;
  wire Rx_Nclk_Nibble_In;
  wire Rx_Nclk_Nibble_Out;
  wire Rx_Pclk_Nibble_In;
  wire Rx_Pclk_Nibble_Out;
  wire [3:0]Rx_Phy_Rden;
  wire Rx_Pll_Clk;
  (* DONT_TOUCH *) wire [6:0]Rx_Q_CombOut;
  (* DONT_TOUCH *) wire [27:0]Rx_Q_Out;
  wire Rx_RefClk;
  wire [5:0]Rx_Riu_Addr;
  wire Rx_Riu_Clk;
  wire Rx_Riu_Nibble_Sel;
  wire [15:0]Rx_Riu_Rd_Data;
  wire Rx_Riu_Valid;
  wire [15:0]Rx_Riu_Wr_Data;
  wire Rx_Riu_Wr_En;
  wire Rx_Rst_Dly;
  wire [3:0]Rx_Tbyte_In;
  wire Rx_Vtc_Rdy;
  wire [39:0]TX_BIT_CTRL_IN0;
  wire [39:0]TX_BIT_CTRL_IN1;
  wire [39:0]TX_BIT_CTRL_OUT0;
  wire [39:0]TX_BIT_CTRL_OUT1;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT4_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT4_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED ;
  wire [7:4]\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED ;
  wire \NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_FIFO_WRCLK_OUT_UNCONNECTED ;
  wire [7:4]\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED ;

  assign Fifo_Empty[6] = \<const0> ;
  assign Fifo_Empty[5] = \<const0> ;
  assign Fifo_Empty[4] = \<const0> ;
  assign Fifo_Empty[3] = \<const0> ;
  assign Fifo_Empty[2] = \<const0> ;
  assign Fifo_Empty[1:0] = \^Fifo_Empty [1:0];
  assign Rx_CntValueOut[62] = \<const0> ;
  assign Rx_CntValueOut[61] = \<const0> ;
  assign Rx_CntValueOut[60] = \<const0> ;
  assign Rx_CntValueOut[59] = \<const0> ;
  assign Rx_CntValueOut[58] = \<const0> ;
  assign Rx_CntValueOut[57] = \<const0> ;
  assign Rx_CntValueOut[56] = \<const0> ;
  assign Rx_CntValueOut[55] = \<const0> ;
  assign Rx_CntValueOut[54] = \<const0> ;
  assign Rx_CntValueOut[53] = \<const0> ;
  assign Rx_CntValueOut[52] = \<const0> ;
  assign Rx_CntValueOut[51] = \<const0> ;
  assign Rx_CntValueOut[50] = \<const0> ;
  assign Rx_CntValueOut[49] = \<const0> ;
  assign Rx_CntValueOut[48] = \<const0> ;
  assign Rx_CntValueOut[47] = \<const0> ;
  assign Rx_CntValueOut[46] = \<const0> ;
  assign Rx_CntValueOut[45] = \<const0> ;
  assign Rx_CntValueOut[44] = \<const0> ;
  assign Rx_CntValueOut[43] = \<const0> ;
  assign Rx_CntValueOut[42] = \<const0> ;
  assign Rx_CntValueOut[41] = \<const0> ;
  assign Rx_CntValueOut[40] = \<const0> ;
  assign Rx_CntValueOut[39] = \<const0> ;
  assign Rx_CntValueOut[38] = \<const0> ;
  assign Rx_CntValueOut[37] = \<const0> ;
  assign Rx_CntValueOut[36] = \<const0> ;
  assign Rx_CntValueOut[35] = \<const0> ;
  assign Rx_CntValueOut[34] = \<const0> ;
  assign Rx_CntValueOut[33] = \<const0> ;
  assign Rx_CntValueOut[32] = \<const0> ;
  assign Rx_CntValueOut[31] = \<const0> ;
  assign Rx_CntValueOut[30] = \<const0> ;
  assign Rx_CntValueOut[29] = \<const0> ;
  assign Rx_CntValueOut[28] = \<const0> ;
  assign Rx_CntValueOut[27] = \<const0> ;
  assign Rx_CntValueOut[26] = \<const0> ;
  assign Rx_CntValueOut[25] = \<const0> ;
  assign Rx_CntValueOut[24] = \<const0> ;
  assign Rx_CntValueOut[23] = \<const0> ;
  assign Rx_CntValueOut[22] = \<const0> ;
  assign Rx_CntValueOut[21] = \<const0> ;
  assign Rx_CntValueOut[20] = \<const0> ;
  assign Rx_CntValueOut[19] = \<const0> ;
  assign Rx_CntValueOut[18] = \<const0> ;
  assign Rx_CntValueOut[17:0] = \^Rx_CntValueOut [17:0];
  assign Rx_CntValueOut_Ext[62] = \<const0> ;
  assign Rx_CntValueOut_Ext[61] = \<const0> ;
  assign Rx_CntValueOut_Ext[60] = \<const0> ;
  assign Rx_CntValueOut_Ext[59] = \<const0> ;
  assign Rx_CntValueOut_Ext[58] = \<const0> ;
  assign Rx_CntValueOut_Ext[57] = \<const0> ;
  assign Rx_CntValueOut_Ext[56] = \<const0> ;
  assign Rx_CntValueOut_Ext[55] = \<const0> ;
  assign Rx_CntValueOut_Ext[54] = \<const0> ;
  assign Rx_CntValueOut_Ext[53] = \<const0> ;
  assign Rx_CntValueOut_Ext[52] = \<const0> ;
  assign Rx_CntValueOut_Ext[51] = \<const0> ;
  assign Rx_CntValueOut_Ext[50] = \<const0> ;
  assign Rx_CntValueOut_Ext[49] = \<const0> ;
  assign Rx_CntValueOut_Ext[48] = \<const0> ;
  assign Rx_CntValueOut_Ext[47] = \<const0> ;
  assign Rx_CntValueOut_Ext[46] = \<const0> ;
  assign Rx_CntValueOut_Ext[45] = \<const0> ;
  assign Rx_CntValueOut_Ext[44] = \<const0> ;
  assign Rx_CntValueOut_Ext[43] = \<const0> ;
  assign Rx_CntValueOut_Ext[42] = \<const0> ;
  assign Rx_CntValueOut_Ext[41] = \<const0> ;
  assign Rx_CntValueOut_Ext[40] = \<const0> ;
  assign Rx_CntValueOut_Ext[39] = \<const0> ;
  assign Rx_CntValueOut_Ext[38] = \<const0> ;
  assign Rx_CntValueOut_Ext[37] = \<const0> ;
  assign Rx_CntValueOut_Ext[36] = \<const0> ;
  assign Rx_CntValueOut_Ext[35] = \<const0> ;
  assign Rx_CntValueOut_Ext[34] = \<const0> ;
  assign Rx_CntValueOut_Ext[33] = \<const0> ;
  assign Rx_CntValueOut_Ext[32] = \<const0> ;
  assign Rx_CntValueOut_Ext[31] = \<const0> ;
  assign Rx_CntValueOut_Ext[30] = \<const0> ;
  assign Rx_CntValueOut_Ext[29] = \<const0> ;
  assign Rx_CntValueOut_Ext[28] = \<const0> ;
  assign Rx_CntValueOut_Ext[27] = \<const0> ;
  assign Rx_CntValueOut_Ext[26] = \<const0> ;
  assign Rx_CntValueOut_Ext[25] = \<const0> ;
  assign Rx_CntValueOut_Ext[24] = \<const0> ;
  assign Rx_CntValueOut_Ext[23] = \<const0> ;
  assign Rx_CntValueOut_Ext[22] = \<const0> ;
  assign Rx_CntValueOut_Ext[21] = \<const0> ;
  assign Rx_CntValueOut_Ext[20] = \<const0> ;
  assign Rx_CntValueOut_Ext[19] = \<const0> ;
  assign Rx_CntValueOut_Ext[18] = \<const0> ;
  assign Rx_CntValueOut_Ext[17:0] = \^Rx_CntValueOut_Ext [17:0];
  assign Rx_Riu_Prsnt = \<const1> ;
  GND GND
       (.G(\<const0> ));
  (* box_type = "PRIMITIVE" *) 
  BITSLICE_CONTROL #(
    .CTRL_CLK("EXTERNAL"),
    .DIV_MODE("DIV2"),
    .EN_CLK_TO_EXT_NORTH("DISABLE"),
    .EN_CLK_TO_EXT_SOUTH("DISABLE"),
    .EN_DYN_ODLY_MODE("FALSE"),
    .EN_OTHER_NCLK("FALSE"),
    .EN_OTHER_PCLK("FALSE"),
    .IDLY_VT_TRACK("TRUE"),
    .INV_RXCLK("FALSE"),
    .ODLY_VT_TRACK("TRUE"),
    .QDLY_VT_TRACK("TRUE"),
    .READ_IDLE_COUNT(6'h00),
    .REFCLK_SRC("PLLCLK"),
    .ROUNDING_FACTOR(16),
    .RXGATE_EXTEND("FALSE"),
    .RX_CLK_PHASE_N("SHIFT_90"),
    .RX_CLK_PHASE_P("SHIFT_90"),
    .RX_GATING("DISABLE"),
    .SELF_CALIBRATE("ENABLE"),
    .SERIAL_MODE("TRUE"),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_SPEEDUP("FAST"),
    .SIM_VERSION(2.000000),
    .TX_GATING("DISABLE")) 
    \Gen_1.Nibble_I_BitsliceCntrl 
       (.CLK_FROM_EXT(Rx_Clk_From_Ext),
        .CLK_TO_EXT_NORTH(Rx_Clk_To_Ext_North),
        .CLK_TO_EXT_SOUTH(Rx_Clk_To_Ext_South),
        .DLY_RDY(Rx_Dly_Rdy),
        .DYN_DCI(Rx_Dyn_Dci),
        .EN_VTC(Rx_Bsc_En_Vtc),
        .NCLK_NIBBLE_IN(Rx_Nclk_Nibble_In),
        .NCLK_NIBBLE_OUT(Rx_Nclk_Nibble_Out),
        .PCLK_NIBBLE_IN(Rx_Pclk_Nibble_In),
        .PCLK_NIBBLE_OUT(Rx_Pclk_Nibble_Out),
        .PHY_RDCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDCS1({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDEN(Rx_Phy_Rden),
        .PHY_WRCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS1({1'b0,1'b0,1'b0,1'b0}),
        .PLL_CLK(Rx_Pll_Clk),
        .REFCLK(Rx_RefClk),
        .RIU_ADDR(Rx_Riu_Addr),
        .RIU_CLK(Rx_Riu_Clk),
        .RIU_NIBBLE_SEL(Rx_Riu_Nibble_Sel),
        .RIU_RD_DATA(Rx_Riu_Rd_Data),
        .RIU_VALID(Rx_Riu_Valid),
        .RIU_WR_DATA(Rx_Riu_Wr_Data),
        .RIU_WR_EN(Rx_Riu_Wr_En),
        .RST(Rx_Bsc_Rst),
        .RX_BIT_CTRL_IN0(RX_BIT_CTRL_IN0),
        .RX_BIT_CTRL_IN1(RX_BIT_CTRL_IN1),
        .RX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN6({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_OUT0(RX_BIT_CTRL_OUT0),
        .RX_BIT_CTRL_OUT1(RX_BIT_CTRL_OUT1),
        .RX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT4(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT4_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TBYTE_IN(Rx_Tbyte_In),
        .TX_BIT_CTRL_IN0(TX_BIT_CTRL_IN0),
        .TX_BIT_CTRL_IN1(TX_BIT_CTRL_IN1),
        .TX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN6({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN_TRI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_OUT0(TX_BIT_CTRL_OUT0),
        .TX_BIT_CTRL_OUT1(TX_BIT_CTRL_OUT1),
        .TX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT4(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT4_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT_TRI(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED [39:0]),
        .VTC_RDY(Rx_Vtc_Rdy));
  (* DONT_TOUCH *) 
  (* box_type = "PRIMITIVE" *) 
  RX_BITSLICE #(
    .CASCADE("FALSE"),
    .DATA_TYPE("SERIAL"),
    .DATA_WIDTH(4),
    .DELAY_FORMAT("COUNT"),
    .DELAY_TYPE("VAR_LOAD"),
    .DELAY_VALUE(0),
    .DELAY_VALUE_EXT(0),
    .FIFO_SYNC_MODE("FALSE"),
    .IS_CLK_EXT_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_EXT_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REFCLK_FREQUENCY(312.500000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .UPDATE_MODE("ASYNC"),
    .UPDATE_MODE_EXT("ASYNC")) 
    \Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0 
       (.CE(Rx_Ce[0]),
        .CE_EXT(Rx_Ce_Ext[0]),
        .CLK(Rx_Clk),
        .CLK_EXT(Rx_Clk),
        .CNTVALUEIN(Rx_CntValueIn[8:0]),
        .CNTVALUEIN_EXT(Rx_CntValueIn_Ext[8:0]),
        .CNTVALUEOUT(\^Rx_CntValueOut [8:0]),
        .CNTVALUEOUT_EXT(\^Rx_CntValueOut_Ext [8:0]),
        .DATAIN(Rx_Data_In[0]),
        .EN_VTC(Rx_Bs_En_Vtc),
        .EN_VTC_EXT(Rx_Bs_En_Vtc),
        .FIFO_EMPTY(\^Fifo_Empty [0]),
        .FIFO_RD_CLK(Fifo_Rd_Clk[0]),
        .FIFO_RD_EN(Fifo_Rd_En[0]),
        .FIFO_WRCLK_OUT(Fifo_Wrclk_Out),
        .INC(Rx_Inc[0]),
        .INC_EXT(Rx_Inc_Ext[0]),
        .LOAD(Rx_Load[0]),
        .LOAD_EXT(Rx_Load_Ext[0]),
        .Q({\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED [7:6],Rx_Q_CombOut[0],\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED [4],Rx_Q_Out[3:0]}),
        .RST(Rx_Bs_Rst),
        .RST_DLY(Rx_Rst_Dly),
        .RST_DLY_EXT(Rx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT0),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN0),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT0),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN0));
  (* box_type = "PRIMITIVE" *) 
  RX_BITSLICE #(
    .CASCADE("FALSE"),
    .DATA_TYPE("SERIAL"),
    .DATA_WIDTH(4),
    .DELAY_FORMAT("COUNT"),
    .DELAY_TYPE("VAR_LOAD"),
    .DELAY_VALUE(0),
    .DELAY_VALUE_EXT(0),
    .FIFO_SYNC_MODE("FALSE"),
    .IS_CLK_EXT_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_EXT_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REFCLK_FREQUENCY(312.500000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .UPDATE_MODE("ASYNC"),
    .UPDATE_MODE_EXT("ASYNC")) 
    \Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n 
       (.CE(Rx_Ce[1]),
        .CE_EXT(Rx_Ce_Ext[1]),
        .CLK(Rx_Clk),
        .CLK_EXT(Rx_Clk),
        .CNTVALUEIN(Rx_CntValueIn[17:9]),
        .CNTVALUEIN_EXT(Rx_CntValueIn_Ext[17:9]),
        .CNTVALUEOUT(\^Rx_CntValueOut [17:9]),
        .CNTVALUEOUT_EXT(\^Rx_CntValueOut_Ext [17:9]),
        .DATAIN(Rx_Data_In[1]),
        .EN_VTC(Rx_Bs_En_Vtc),
        .EN_VTC_EXT(Rx_Bs_En_Vtc),
        .FIFO_EMPTY(\^Fifo_Empty [1]),
        .FIFO_RD_CLK(Fifo_Rd_Clk[1]),
        .FIFO_RD_EN(Fifo_Rd_En[1]),
        .FIFO_WRCLK_OUT(\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_FIFO_WRCLK_OUT_UNCONNECTED ),
        .INC(Rx_Inc[1]),
        .INC_EXT(Rx_Inc_Ext[1]),
        .LOAD(Rx_Load[1]),
        .LOAD_EXT(Rx_Load_Ext[1]),
        .Q({\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED [7:6],Rx_Q_CombOut[1],\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED [4],Rx_Q_Out[7:4]}),
        .RST(Rx_Bs_Rst),
        .RST_DLY(Rx_Rst_Dly),
        .RST_DLY_EXT(Rx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT1),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN1),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT1),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN1));
  VCC VCC
       (.P(\<const1> ));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(Rx_Q_Out[27]));
  LUT1 #(
    .INIT(2'h2)) 
    i_1
       (.I0(1'b0),
        .O(Rx_Q_Out[26]));
  LUT1 #(
    .INIT(2'h2)) 
    i_10
       (.I0(1'b0),
        .O(Rx_Q_Out[17]));
  LUT1 #(
    .INIT(2'h2)) 
    i_11
       (.I0(1'b0),
        .O(Rx_Q_Out[16]));
  LUT1 #(
    .INIT(2'h2)) 
    i_12
       (.I0(1'b0),
        .O(Rx_Q_Out[15]));
  LUT1 #(
    .INIT(2'h2)) 
    i_13
       (.I0(1'b0),
        .O(Rx_Q_Out[14]));
  LUT1 #(
    .INIT(2'h2)) 
    i_14
       (.I0(1'b0),
        .O(Rx_Q_Out[13]));
  LUT1 #(
    .INIT(2'h2)) 
    i_15
       (.I0(1'b0),
        .O(Rx_Q_Out[12]));
  LUT1 #(
    .INIT(2'h2)) 
    i_16
       (.I0(1'b0),
        .O(Rx_Q_Out[11]));
  LUT1 #(
    .INIT(2'h2)) 
    i_17
       (.I0(1'b0),
        .O(Rx_Q_Out[10]));
  LUT1 #(
    .INIT(2'h2)) 
    i_18
       (.I0(1'b0),
        .O(Rx_Q_Out[9]));
  LUT1 #(
    .INIT(2'h2)) 
    i_19
       (.I0(1'b0),
        .O(Rx_Q_Out[8]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2
       (.I0(1'b0),
        .O(Rx_Q_Out[25]));
  LUT1 #(
    .INIT(2'h2)) 
    i_20
       (.I0(1'b0),
        .O(Rx_Q_CombOut[6]));
  LUT1 #(
    .INIT(2'h2)) 
    i_21
       (.I0(1'b0),
        .O(Rx_Q_CombOut[5]));
  LUT1 #(
    .INIT(2'h2)) 
    i_22
       (.I0(1'b0),
        .O(Rx_Q_CombOut[4]));
  LUT1 #(
    .INIT(2'h2)) 
    i_23
       (.I0(1'b0),
        .O(Rx_Q_CombOut[3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_24
       (.I0(1'b0),
        .O(Rx_Q_CombOut[2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_3
       (.I0(1'b0),
        .O(Rx_Q_Out[24]));
  LUT1 #(
    .INIT(2'h2)) 
    i_4
       (.I0(1'b0),
        .O(Rx_Q_Out[23]));
  LUT1 #(
    .INIT(2'h2)) 
    i_5
       (.I0(1'b0),
        .O(Rx_Q_Out[22]));
  LUT1 #(
    .INIT(2'h2)) 
    i_6
       (.I0(1'b0),
        .O(Rx_Q_Out[21]));
  LUT1 #(
    .INIT(2'h2)) 
    i_7
       (.I0(1'b0),
        .O(Rx_Q_Out[20]));
  LUT1 #(
    .INIT(2'h2)) 
    i_8
       (.I0(1'b0),
        .O(Rx_Q_Out[19]));
  LUT1 #(
    .INIT(2'h2)) 
    i_9
       (.I0(1'b0),
        .O(Rx_Q_Out[18]));
endmodule

(* C_BtslceUsedAsT = "7'b0000000" *) (* C_BusRxBitCtrlIn = "40" *) (* C_BusRxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlIn = "40" *) (* C_BusTxBitCtrlInTri = "40" *) (* C_BusTxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlOutTri = "40" *) (* C_BytePosition = "0" *) (* C_CntValue = "9" *) 
(* C_Ctrl_Clk = "EXTERNAL" *) (* C_Data_Type = "DATA" *) (* C_Delay_Format = "TIME" *) 
(* C_Delay_Type = "FIXED" *) (* C_Delay_Value = "0" *) (* C_Div_Mode = "DIV4" *) 
(* C_En_Clk_To_Ext_North = "DISABLE" *) (* C_En_Clk_To_Ext_South = "DISABLE" *) (* C_En_Dyn_Odly_Mode = "FALSE" *) 
(* C_En_Other_Nclk = "FALSE" *) (* C_En_Other_Pclk = "FALSE" *) (* C_Enable_Pre_Emphasis = "FALSE" *) 
(* C_Idly_Vt_Track = "FALSE" *) (* C_Init = "1'b0" *) (* C_Inv_Rxclk = "FALSE" *) 
(* C_IoBank = "44" *) (* C_Is_Clk_Inverted = "1'b0" *) (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
(* C_Is_Rst_Inverted = "1'b0" *) (* C_Native_Odelay_Bypass = "FALSE" *) (* C_NibbleType = "6" *) 
(* C_Odly_Vt_Track = "FALSE" *) (* C_Output_Phase_90 = "TRUE" *) (* C_Part = "XCKU060" *) 
(* C_Qdly_Vt_Track = "FALSE" *) (* C_Read_Idle_Count = "6'b000000" *) (* C_RefClk_Frequency = "1250.000000" *) 
(* C_RefClk_Src = "PLLCLK" *) (* C_Rounding_Factor = "16" *) (* C_RxGate_Extend = "FALSE" *) 
(* C_Rx_Clk_Phase_n = "SHIFT_0" *) (* C_Rx_Clk_Phase_p = "SHIFT_0" *) (* C_Rx_Gating = "DISABLE" *) 
(* C_Self_Calibrate = "ENABLE" *) (* C_Serial_Mode = "FALSE" *) (* C_Tx_BtslceTr = "T" *) 
(* C_Tx_Data_Width = "8" *) (* C_Tx_Gating = "ENABLE" *) (* C_Update_Mode = "ASYNC" *) 
(* C_UsedBitslices = "7'b0010000" *) (* keep_hierarchy = "true" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Tx_Nibble
   (Tx_Bsc_Rst,
    Tx_Bs_Rst,
    Tx_Rst_Dly,
    Tx_Bsc_En_Vtc,
    Tx_Bs_En_Vtc,
    Tx_Riu_Clk,
    Tx_Riu_Addr,
    Tx_Riu_Wr_Data,
    Tx_Riu_Rd_Data,
    Tx_Riu_Valid,
    Tx_Riu_Wr_En,
    Tx_Riu_Nibble_Sel,
    Tx_Pll_Clk,
    Tx_RefClk,
    Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Tx_Dyn_Dci,
    Tx_Tbyte_In,
    Tx_Phy_Rden,
    Tx_Clk_From_Ext,
    Tx_Pclk_Nibble_In,
    Tx_Nclk_Nibble_In,
    Tx_Nclk_Nibble_Out,
    Tx_Pclk_Nibble_Out,
    Tx_Clk_To_Ext_North,
    Tx_Clk_To_Ext_South,
    Tx_Tri_Out,
    Tx_Data_Out,
    Tx_T_In,
    Tx_D_In,
    Tx_Ce,
    Tx_Clk,
    Tx_Inc,
    Tx_Load,
    Tx_CntValueIn,
    Tx_CntValueOut,
    TxTri_Ce,
    TxTri_Clk,
    TxTri_Inc,
    TxTri_Load,
    TxTri_CntValueIn,
    TxTri_CntValueOut);
  input Tx_Bsc_Rst;
  input Tx_Bs_Rst;
  input Tx_Rst_Dly;
  input Tx_Bsc_En_Vtc;
  input Tx_Bs_En_Vtc;
  input Tx_Riu_Clk;
  input [5:0]Tx_Riu_Addr;
  input [15:0]Tx_Riu_Wr_Data;
  output [15:0]Tx_Riu_Rd_Data;
  output Tx_Riu_Valid;
  input Tx_Riu_Wr_En;
  input Tx_Riu_Nibble_Sel;
  input Tx_Pll_Clk;
  input Tx_RefClk;
  output Tx_Dly_Rdy;
  output Tx_Vtc_Rdy;
  output [6:0]Tx_Dyn_Dci;
  input [3:0]Tx_Tbyte_In;
  input [3:0]Tx_Phy_Rden;
  input Tx_Clk_From_Ext;
  input Tx_Pclk_Nibble_In;
  input Tx_Nclk_Nibble_In;
  output Tx_Nclk_Nibble_Out;
  output Tx_Pclk_Nibble_Out;
  output Tx_Clk_To_Ext_North;
  output Tx_Clk_To_Ext_South;
  output [5:0]Tx_Tri_Out;
  output [5:0]Tx_Data_Out;
  input [5:0]Tx_T_In;
  input [47:0]Tx_D_In;
  input [5:0]Tx_Ce;
  input Tx_Clk;
  input [5:0]Tx_Inc;
  input [5:0]Tx_Load;
  input [53:0]Tx_CntValueIn;
  output [53:0]Tx_CntValueOut;
  input TxTri_Ce;
  input TxTri_Clk;
  input TxTri_Inc;
  input TxTri_Load;
  input [8:0]TxTri_CntValueIn;
  output [8:0]TxTri_CntValueOut;

  wire \<const0> ;
  wire [39:0]RX_BIT_CTRL_IN4;
  wire [39:0]RX_BIT_CTRL_OUT4;
  wire [39:0]TX_BIT_CTRL_IN4;
  wire [39:0]TX_BIT_CTRL_OUT4;
  wire Tx_Bs_En_Vtc;
  wire Tx_Bs_Rst;
  wire Tx_Bsc_En_Vtc;
  wire Tx_Bsc_Rst;
  wire [5:0]Tx_Ce;
  wire Tx_Clk;
  wire Tx_Clk_From_Ext;
  wire Tx_Clk_To_Ext_North;
  wire Tx_Clk_To_Ext_South;
  wire [53:0]Tx_CntValueIn;
  wire [44:36]\^Tx_CntValueOut ;
  wire [47:0]Tx_D_In;
  wire [4:4]\^Tx_Data_Out ;
  wire Tx_Dly_Rdy;
  wire [6:0]Tx_Dyn_Dci;
  wire [5:0]Tx_Inc;
  wire [5:0]Tx_Load;
  wire Tx_Nclk_Nibble_In;
  wire Tx_Nclk_Nibble_Out;
  wire Tx_Pclk_Nibble_In;
  wire Tx_Pclk_Nibble_Out;
  wire Tx_Pll_Clk;
  wire Tx_RefClk;
  wire [5:0]Tx_Riu_Addr;
  wire Tx_Riu_Clk;
  wire Tx_Riu_Nibble_Sel;
  wire [15:0]Tx_Riu_Rd_Data;
  wire Tx_Riu_Valid;
  wire [15:0]Tx_Riu_Wr_Data;
  wire Tx_Riu_Wr_En;
  wire Tx_Rst_Dly;
  wire [5:0]Tx_T_In;
  wire [3:0]Tx_Tbyte_In;
  wire [4:4]\^Tx_Tri_Out ;
  wire Tx_Vtc_Rdy;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_IN6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT0_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT1_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_IN6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT0_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT1_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED ;

  assign TxTri_CntValueOut[8] = \<const0> ;
  assign TxTri_CntValueOut[7] = \<const0> ;
  assign TxTri_CntValueOut[6] = \<const0> ;
  assign TxTri_CntValueOut[5] = \<const0> ;
  assign TxTri_CntValueOut[4] = \<const0> ;
  assign TxTri_CntValueOut[3] = \<const0> ;
  assign TxTri_CntValueOut[2] = \<const0> ;
  assign TxTri_CntValueOut[1] = \<const0> ;
  assign TxTri_CntValueOut[0] = \<const0> ;
  assign Tx_CntValueOut[53] = \<const0> ;
  assign Tx_CntValueOut[52] = \<const0> ;
  assign Tx_CntValueOut[51] = \<const0> ;
  assign Tx_CntValueOut[50] = \<const0> ;
  assign Tx_CntValueOut[49] = \<const0> ;
  assign Tx_CntValueOut[48] = \<const0> ;
  assign Tx_CntValueOut[47] = \<const0> ;
  assign Tx_CntValueOut[46] = \<const0> ;
  assign Tx_CntValueOut[45] = \<const0> ;
  assign Tx_CntValueOut[44:36] = \^Tx_CntValueOut [44:36];
  assign Tx_CntValueOut[35] = \<const0> ;
  assign Tx_CntValueOut[34] = \<const0> ;
  assign Tx_CntValueOut[33] = \<const0> ;
  assign Tx_CntValueOut[32] = \<const0> ;
  assign Tx_CntValueOut[31] = \<const0> ;
  assign Tx_CntValueOut[30] = \<const0> ;
  assign Tx_CntValueOut[29] = \<const0> ;
  assign Tx_CntValueOut[28] = \<const0> ;
  assign Tx_CntValueOut[27] = \<const0> ;
  assign Tx_CntValueOut[26] = \<const0> ;
  assign Tx_CntValueOut[25] = \<const0> ;
  assign Tx_CntValueOut[24] = \<const0> ;
  assign Tx_CntValueOut[23] = \<const0> ;
  assign Tx_CntValueOut[22] = \<const0> ;
  assign Tx_CntValueOut[21] = \<const0> ;
  assign Tx_CntValueOut[20] = \<const0> ;
  assign Tx_CntValueOut[19] = \<const0> ;
  assign Tx_CntValueOut[18] = \<const0> ;
  assign Tx_CntValueOut[17] = \<const0> ;
  assign Tx_CntValueOut[16] = \<const0> ;
  assign Tx_CntValueOut[15] = \<const0> ;
  assign Tx_CntValueOut[14] = \<const0> ;
  assign Tx_CntValueOut[13] = \<const0> ;
  assign Tx_CntValueOut[12] = \<const0> ;
  assign Tx_CntValueOut[11] = \<const0> ;
  assign Tx_CntValueOut[10] = \<const0> ;
  assign Tx_CntValueOut[9] = \<const0> ;
  assign Tx_CntValueOut[8] = \<const0> ;
  assign Tx_CntValueOut[7] = \<const0> ;
  assign Tx_CntValueOut[6] = \<const0> ;
  assign Tx_CntValueOut[5] = \<const0> ;
  assign Tx_CntValueOut[4] = \<const0> ;
  assign Tx_CntValueOut[3] = \<const0> ;
  assign Tx_CntValueOut[2] = \<const0> ;
  assign Tx_CntValueOut[1] = \<const0> ;
  assign Tx_CntValueOut[0] = \<const0> ;
  assign Tx_Data_Out[5] = \<const0> ;
  assign Tx_Data_Out[4] = \^Tx_Data_Out [4];
  assign Tx_Data_Out[3] = \<const0> ;
  assign Tx_Data_Out[2] = \<const0> ;
  assign Tx_Data_Out[1] = \<const0> ;
  assign Tx_Data_Out[0] = \<const0> ;
  assign Tx_Tri_Out[5] = \<const0> ;
  assign Tx_Tri_Out[4] = \^Tx_Tri_Out [4];
  assign Tx_Tri_Out[3] = \<const0> ;
  assign Tx_Tri_Out[2] = \<const0> ;
  assign Tx_Tri_Out[1] = \<const0> ;
  assign Tx_Tri_Out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* box_type = "PRIMITIVE" *) 
  BITSLICE_CONTROL #(
    .CTRL_CLK("EXTERNAL"),
    .DIV_MODE("DIV4"),
    .EN_CLK_TO_EXT_NORTH("DISABLE"),
    .EN_CLK_TO_EXT_SOUTH("DISABLE"),
    .EN_DYN_ODLY_MODE("FALSE"),
    .EN_OTHER_NCLK("FALSE"),
    .EN_OTHER_PCLK("FALSE"),
    .IDLY_VT_TRACK("FALSE"),
    .INV_RXCLK("FALSE"),
    .ODLY_VT_TRACK("FALSE"),
    .QDLY_VT_TRACK("FALSE"),
    .READ_IDLE_COUNT(6'h00),
    .REFCLK_SRC("PLLCLK"),
    .ROUNDING_FACTOR(16),
    .RXGATE_EXTEND("FALSE"),
    .RX_CLK_PHASE_N("SHIFT_0"),
    .RX_CLK_PHASE_P("SHIFT_0"),
    .RX_GATING("DISABLE"),
    .SELF_CALIBRATE("ENABLE"),
    .SERIAL_MODE("FALSE"),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_SPEEDUP("FAST"),
    .SIM_VERSION(2.000000),
    .TX_GATING("ENABLE")) 
    \Gen_1.Nibble_I_BitsliceCntrl 
       (.CLK_FROM_EXT(Tx_Clk_From_Ext),
        .CLK_TO_EXT_NORTH(Tx_Clk_To_Ext_North),
        .CLK_TO_EXT_SOUTH(Tx_Clk_To_Ext_South),
        .DLY_RDY(Tx_Dly_Rdy),
        .DYN_DCI(Tx_Dyn_Dci),
        .EN_VTC(Tx_Bsc_En_Vtc),
        .NCLK_NIBBLE_IN(Tx_Nclk_Nibble_In),
        .NCLK_NIBBLE_OUT(Tx_Nclk_Nibble_Out),
        .PCLK_NIBBLE_IN(Tx_Pclk_Nibble_In),
        .PCLK_NIBBLE_OUT(Tx_Pclk_Nibble_Out),
        .PHY_RDCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDCS1({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDEN({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS1({1'b0,1'b0,1'b0,1'b0}),
        .PLL_CLK(Tx_Pll_Clk),
        .REFCLK(Tx_RefClk),
        .RIU_ADDR(Tx_Riu_Addr),
        .RIU_CLK(Tx_Riu_Clk),
        .RIU_NIBBLE_SEL(Tx_Riu_Nibble_Sel),
        .RIU_RD_DATA(Tx_Riu_Rd_Data),
        .RIU_VALID(Tx_Riu_Valid),
        .RIU_WR_DATA(Tx_Riu_Wr_Data),
        .RIU_WR_EN(Tx_Riu_Wr_En),
        .RST(Tx_Bsc_Rst),
        .RX_BIT_CTRL_IN0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN4(RX_BIT_CTRL_IN4),
        .RX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_IN6_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT0(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT0_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT1(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT1_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT4(RX_BIT_CTRL_OUT4),
        .RX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TBYTE_IN(Tx_Tbyte_In),
        .TX_BIT_CTRL_IN0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN4(TX_BIT_CTRL_IN4),
        .TX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_IN6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_IN_TRI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_OUT0(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT0_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT1(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT1_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT4(TX_BIT_CTRL_OUT4),
        .TX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT_TRI(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED [39:0]),
        .VTC_RDY(Tx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  TX_BITSLICE #(
    .DATA_WIDTH(8),
    .DELAY_FORMAT("TIME"),
    .DELAY_TYPE("FIXED"),
    .DELAY_VALUE(0),
    .ENABLE_PRE_EMPHASIS("FALSE"),
    .INIT(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .NATIVE_ODELAY_BYPASS("FALSE"),
    .OUTPUT_PHASE_90("TRUE"),
    .REFCLK_FREQUENCY(1250.000000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .TBYTE_CTL("T"),
    .UPDATE_MODE("ASYNC")) 
    \Gen_7[5].Gen_7_1.Nibble_I_TxBitslice 
       (.CE(Tx_Ce[4]),
        .CLK(Tx_Clk),
        .CNTVALUEIN(Tx_CntValueIn[44:36]),
        .CNTVALUEOUT(\^Tx_CntValueOut ),
        .D(Tx_D_In[39:32]),
        .EN_VTC(Tx_Bs_En_Vtc),
        .INC(Tx_Inc[4]),
        .LOAD(Tx_Load[4]),
        .O(\^Tx_Data_Out ),
        .RST(Tx_Bs_Rst),
        .RST_DLY(Tx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT4),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN4),
        .T(Tx_T_In[4]),
        .TBYTE_IN(1'b0),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT4),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN4),
        .T_OUT(\^Tx_Tri_Out ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_block
   (Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    sgmii_clk_r_0,
    sgmii_clk_en_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    sgmii_clk_f_0,
    gmii_isolate_0,
    an_interrupt_0,
    status_vector_0,
    riu_rd_data,
    riu_valid,
    riu_prsnt,
    txp_0,
    txn_0,
    Rx_SysClk,
    tx_dly_rdy_3,
    tx_dly_rdy_1,
    tx_dly_rdy_2,
    tx_vtc_rdy_3,
    tx_vtc_rdy_1,
    tx_vtc_rdy_2,
    rx_dly_rdy_3,
    rx_dly_rdy_1,
    rx_dly_rdy_2,
    rx_vtc_rdy_3,
    rx_vtc_rdy_1,
    rx_vtc_rdy_2,
    Tx_WrClk,
    speed_is_10_100_0,
    speed_is_100_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    reset_out,
    signal_detect_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    configuration_vector_0,
    CLK,
    D,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    tx_pll_clk_out,
    rx_pll_clk_out,
    rxp_0,
    rxn_0);
  output Tx_Dly_Rdy;
  output Tx_Vtc_Rdy;
  output Rx_Dly_Rdy;
  output Rx_Vtc_Rdy;
  output sgmii_clk_r_0;
  output sgmii_clk_en_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output sgmii_clk_f_0;
  output gmii_isolate_0;
  output an_interrupt_0;
  output [12:0]status_vector_0;
  output [15:0]riu_rd_data;
  output riu_valid;
  output riu_prsnt;
  output txp_0;
  output txn_0;
  input Rx_SysClk;
  input tx_dly_rdy_3;
  input tx_dly_rdy_1;
  input tx_dly_rdy_2;
  input tx_vtc_rdy_3;
  input tx_vtc_rdy_1;
  input tx_vtc_rdy_2;
  input rx_dly_rdy_3;
  input rx_dly_rdy_1;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_3;
  input rx_vtc_rdy_1;
  input rx_vtc_rdy_2;
  input Tx_WrClk;
  input speed_is_10_100_0;
  input speed_is_100_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  input reset_out;
  input signal_detect_0;
  input [0:0]an_adv_config_vector_0;
  input an_restart_config_0;
  input [4:0]configuration_vector_0;
  input CLK;
  input [5:0]D;
  input tx_bsc_rst_out;
  input rx_bsc_rst_out;
  input tx_bs_rst_out;
  input rx_bs_rst_out;
  input tx_rst_dly_out;
  input rx_rst_dly_out;
  input tx_bsc_en_vtc_out;
  input rx_bsc_en_vtc_out;
  input tx_bs_en_vtc_out;
  input rx_bs_en_vtc_out;
  input riu_clk_out;
  input [5:0]riu_addr_out;
  input [15:0]riu_wr_data_out;
  input riu_wr_en_out;
  input [1:0]riu_nibble_sel_out;
  input tx_pll_clk_out;
  input rx_pll_clk_out;
  input rxp_0;
  input rxn_0;

  wire ActiveIsSlve_i_1_n_0;
  wire [1:0]BaseX_Rx_Data_In;
  wire [7:0]BaseX_Rx_Q_Out;
  wire [4:4]BaseX_Tx_Data_Out;
  wire CLK;
  wire [5:0]D;
  wire LossOfSignal_i_1_n_0;
  wire Mstr_Load_i_1_n_0;
  wire Rx_Dly_Rdy;
  wire Rx_Dly_Rdy_Int;
  wire Rx_SysClk;
  wire Rx_Vtc_Rdy;
  wire Rx_Vtc_Rdy_Int;
  wire Slve_Load_i_1_n_0;
  wire Tx_Dly_Rdy;
  wire Tx_Dly_Rdy_Int;
  wire Tx_Vtc_Rdy;
  wire Tx_Vtc_Rdy_Int;
  wire Tx_WrClk;
  wire WrapToZero_i_1_n_0;
  wire al_rx_valid_out;
  wire [0:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire [4:0]configuration_vector_0;
  wire [1:0]fifo_empty;
  wire fifo_read_0;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_dv_int;
  wire gmii_rx_er_0;
  wire gmii_rx_er_int;
  wire [7:0]gmii_rxd_0;
  wire [7:0]gmii_rxd_int;
  wire gmii_tx_en_0;
  wire gmii_tx_en_int;
  wire gmii_tx_er_0;
  wire gmii_tx_er_int;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_int;
  wire insert3_i_1_n_0;
  wire insert5_i_1_n_0;
  wire mgt_rx_reset;
  wire mgt_tx_reset_0;
  wire mload;
  wire monitor_late_i_1_n_0;
  wire reset_out;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt;
  wire [15:0]riu_rd_data;
  wire riu_valid;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire \rx_elastic_buffer_inst/initialize_ram_complete ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_pulse ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ;
  wire \rx_elastic_buffer_inst/insert_idle_reg__0 ;
  wire \rx_elastic_buffer_inst/remove_idle ;
  wire \rx_elastic_buffer_inst/remove_idle_reg__0 ;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [2:0]rxclkcorcnt;
  wire \rxclkcorcnt[0]_i_1_n_0 ;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn_0;
  wire rxnotintable;
  wire rxp_0;
  wire rxrecreset0;
  wire rxrundisp;
  wire \serdes_1_to_10_i/ActCnt_GE_HalfBT ;
  wire \serdes_1_to_10_i/ActiveIsSlve ;
  wire \serdes_1_to_10_i/D0 ;
  wire \serdes_1_to_10_i/LossOfSignal ;
  wire \serdes_1_to_10_i/WrapToZero ;
  wire [5:5]\serdes_1_to_10_i/act_count_reg ;
  wire \serdes_1_to_10_i/p_0_in ;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire sload;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [12:0]status_vector_0;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire [7:0]tx_data_8b;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_pll_clk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn_0;
  wire txp_0;
  wire \wr_addr[6]_i_2_n_0 ;
  wire [62:0]NLW_gen_io_logic_BaseX_Idly_CntValueOut_UNCONNECTED;
  wire [53:0]NLW_gen_io_logic_BaseX_Odly_CntValueOut_UNCONNECTED;
  wire [6:2]NLW_gen_io_logic_BaseX_Rx_Fifo_Empty_UNCONNECTED;
  wire [6:0]NLW_gen_io_logic_BaseX_Rx_Q_CombOut_UNCONNECTED;
  wire [27:8]NLW_gen_io_logic_BaseX_Rx_Q_Out_UNCONNECTED;
  wire [8:0]NLW_gen_io_logic_BaseX_TriOdly_CntValueOut_UNCONNECTED;
  wire [5:0]NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED;
  wire [5:0]NLW_gen_io_logic_BaseX_Tx_Tri_Out_UNCONNECTED;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_an_enable_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_den_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_dwe_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_req_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_en_cdet_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_enablealign_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_ewrap_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_loc_ref_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_out_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_tri_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_powerdown_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_arready_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_awready_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bvalid_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rvalid_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_wready_UNCONNECTED ;
  wire [9:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_daddr_UNCONNECTED ;
  wire [15:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_di_UNCONNECTED ;
  wire [63:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_correction_timer_UNCONNECTED ;
  wire [31:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_ns_field_UNCONNECTED ;
  wire [47:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_s_field_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bresp_UNCONNECTED ;
  wire [31:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rdata_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rresp_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_speed_selection_UNCONNECTED ;
  wire [15:8]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED ;
  wire [9:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_tx_code_group_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    ActiveIsSlve_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ),
        .I3(\serdes_1_to_10_i/p_0_in ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .I5(\serdes_1_to_10_i/ActiveIsSlve ),
        .O(ActiveIsSlve_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    FifoRd_0_i_1
       (.I0(fifo_empty[0]),
        .I1(fifo_empty[1]),
        .O(\serdes_1_to_10_i/D0 ));
  LUT6 #(
    .INIT(64'hF0F0AAAAAABAAABA)) 
    LossOfSignal_i_1
       (.I0(\serdes_1_to_10_i/LossOfSignal ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ),
        .I5(\serdes_1_to_10_i/act_count_reg ),
        .O(LossOfSignal_i_1_n_0));
  LUT6 #(
    .INIT(64'hBBABBAAB88A88AA8)) 
    Mstr_Load_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .I3(\serdes_1_to_10_i/ActiveIsSlve ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ),
        .O(Mstr_Load_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAFFEAAAAA002)) 
    Slve_Load_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .I3(\serdes_1_to_10_i/ActiveIsSlve ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ),
        .O(Slve_Load_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFAF00000020)) 
    WrapToZero_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .I3(\serdes_1_to_10_i/p_0_in ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\serdes_1_to_10_i/WrapToZero ),
        .O(WrapToZero_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_1
       (.I0(tx_dly_rdy_3),
        .I1(Tx_Dly_Rdy_Int),
        .I2(tx_dly_rdy_1),
        .I3(tx_dly_rdy_2),
        .O(Tx_Dly_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_2
       (.I0(tx_vtc_rdy_3),
        .I1(Tx_Vtc_Rdy_Int),
        .I2(tx_vtc_rdy_1),
        .I3(tx_vtc_rdy_2),
        .O(Tx_Vtc_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_3
       (.I0(rx_dly_rdy_3),
        .I1(Rx_Dly_Rdy_Int),
        .I2(rx_dly_rdy_1),
        .I3(rx_dly_rdy_2),
        .O(Rx_Dly_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_4
       (.I0(rx_vtc_rdy_3),
        .I1(Rx_Vtc_Rdy_Int),
        .I2(rx_vtc_rdy_1),
        .I3(rx_vtc_rdy_2),
        .O(Rx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_DIFF_OUT #(
    .DIFF_TERM("FALSE"),
    .IOSTANDARD("DEFAULT")) 
    \gen_IOB.gen_IOB[0].data_in 
       (.I(rxp_0),
        .IB(rxn_0),
        .O(BaseX_Rx_Data_In[0]),
        .OB(BaseX_Rx_Data_In[1]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \gen_IOB.gen_IOB[0].io_data_out 
       (.I(BaseX_Tx_Data_Out),
        .O(txp_0),
        .OB(txn_0));
  (* C_BytePosition = "0" *) 
  (* C_IoBank = "44" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Rx_BtslcNulType = "SERIAL" *) 
  (* C_Rx_Data_Width = "4" *) 
  (* C_Rx_Delay_Format = "COUNT" *) 
  (* C_Rx_Delay_Type = "VAR_LOAD" *) 
  (* C_Rx_Delay_Value = "0" *) 
  (* C_Rx_RefClk_Frequency = "312.500000" *) 
  (* C_Rx_Self_Calibrate = "ENABLE" *) 
  (* C_Rx_Serial_Mode = "TRUE" *) 
  (* C_Rx_UsedBitslices = "7'b0000011" *) 
  (* C_TxInUpperNibble = "0" *) 
  (* C_Tx_BtslceTr = "T" *) 
  (* C_Tx_BtslceUsedAsT = "7'b0000000" *) 
  (* C_Tx_Data_Width = "8" *) 
  (* C_Tx_Delay_Format = "TIME" *) 
  (* C_Tx_Delay_Type = "FIXED" *) 
  (* C_Tx_Delay_Value = "0" *) 
  (* C_Tx_RefClk_Frequency = "1250.000000" *) 
  (* C_Tx_Self_Calibrate = "ENABLE" *) 
  (* C_Tx_Serial_Mode = "FALSE" *) 
  (* C_Tx_UsedBitslices = "7'b0010000" *) 
  (* C_UseRxRiu = "1" *) 
  (* C_UseTxRiu = "1" *) 
  (* DONT_TOUCH *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_BaseX_Byte gen_io_logic
       (.BaseX_Dly_Clk(Rx_SysClk),
        .BaseX_Idly_Ce({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Idly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 }),
        .BaseX_Idly_CntValueOut(NLW_gen_io_logic_BaseX_Idly_CntValueOut_UNCONNECTED[62:0]),
        .BaseX_Idly_Inc({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Idly_Load({1'b0,1'b0,1'b0,1'b0,1'b0,sload,mload}),
        .BaseX_Odly_Ce({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_CntValueOut(NLW_gen_io_logic_BaseX_Odly_CntValueOut_UNCONNECTED[53:0]),
        .BaseX_Odly_Inc({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_Load({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Riu_Addr(riu_addr_out),
        .BaseX_Riu_Clk(riu_clk_out),
        .BaseX_Riu_Nibble_Sel(riu_nibble_sel_out),
        .BaseX_Riu_Prsnt(riu_prsnt),
        .BaseX_Riu_Rd_Data(riu_rd_data),
        .BaseX_Riu_Valid(riu_valid),
        .BaseX_Riu_Wr_Data(riu_wr_data_out),
        .BaseX_Riu_Wr_En(riu_wr_en_out),
        .BaseX_Rx_Bs_En_Vtc(rx_bs_en_vtc_out),
        .BaseX_Rx_Bs_Rst(rx_bs_rst_out),
        .BaseX_Rx_Bsc_En_Vtc(rx_bsc_en_vtc_out),
        .BaseX_Rx_Bsc_Rst(rx_bsc_rst_out),
        .BaseX_Rx_Data_In({1'b0,1'b0,1'b0,1'b0,1'b0,BaseX_Rx_Data_In}),
        .BaseX_Rx_Dly_Rdy(Rx_Dly_Rdy_Int),
        .BaseX_Rx_Fifo_Empty({NLW_gen_io_logic_BaseX_Rx_Fifo_Empty_UNCONNECTED[6:2],fifo_empty}),
        .BaseX_Rx_Fifo_Rd_Clk(Rx_SysClk),
        .BaseX_Rx_Fifo_Rd_En({1'b0,1'b0,1'b0,1'b0,1'b0,fifo_read_0,fifo_read_0}),
        .BaseX_Rx_Phy_Rden({1'b1,1'b1,1'b1,1'b1}),
        .BaseX_Rx_Pll_Clk(rx_pll_clk_out),
        .BaseX_Rx_Q_CombOut(NLW_gen_io_logic_BaseX_Rx_Q_CombOut_UNCONNECTED[6:0]),
        .BaseX_Rx_Q_Out({NLW_gen_io_logic_BaseX_Rx_Q_Out_UNCONNECTED[27:8],BaseX_Rx_Q_Out}),
        .BaseX_Rx_Rst_Dly(rx_rst_dly_out),
        .BaseX_Rx_Vtc_Rdy(Rx_Vtc_Rdy_Int),
        .BaseX_TriOdly_Ce(1'b0),
        .BaseX_TriOdly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_TriOdly_CntValueOut(NLW_gen_io_logic_BaseX_TriOdly_CntValueOut_UNCONNECTED[8:0]),
        .BaseX_TriOdly_Inc(1'b0),
        .BaseX_TriOdly_Load(1'b0),
        .BaseX_Tx_Bs_En_Vtc(tx_bs_en_vtc_out),
        .BaseX_Tx_Bs_Rst(tx_bs_rst_out),
        .BaseX_Tx_Bsc_En_Vtc(tx_bsc_en_vtc_out),
        .BaseX_Tx_Bsc_Rst(tx_bsc_rst_out),
        .BaseX_Tx_D_In({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,tx_data_8b,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Data_Out({NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED[5],BaseX_Tx_Data_Out,NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED[3:0]}),
        .BaseX_Tx_Dly_Rdy(Tx_Dly_Rdy_Int),
        .BaseX_Tx_Phy_Rden({1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Pll_Clk(tx_pll_clk_out),
        .BaseX_Tx_Rst_Dly(tx_rst_dly_out),
        .BaseX_Tx_T_In({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_TbyteIn({1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Tri_Out(NLW_gen_io_logic_BaseX_Tx_Tri_Out_UNCONNECTED[5:0]),
        .BaseX_Tx_Vtc_Rdy(Tx_Vtc_Rdy_Int),
        .Tx_RdClk(CLK));
  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "virtexuplus" *) 
  (* C_HAS_AN = "TRUE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "TRUE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "TRUE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "FALSE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_8 \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core 
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,an_adv_config_vector_0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_an_enable_UNCONNECTED ),
        .an_interrupt(an_interrupt_0),
        .an_restart_config(an_restart_config_0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector(configuration_vector_0),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_daddr_UNCONNECTED [9:0]),
        .drp_dclk(1'b0),
        .drp_den(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_den_UNCONNECTED ),
        .drp_di(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_di_UNCONNECTED [15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_dwe_UNCONNECTED ),
        .drp_gnt(1'b0),
        .drp_req(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_req_UNCONNECTED ),
        .en_cdet(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_en_cdet_UNCONNECTED ),
        .enablealign(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_enablealign_UNCONNECTED ),
        .ewrap(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_ewrap_UNCONNECTED ),
        .gmii_isolate(gmii_isolate_0),
        .gmii_rx_dv(gmii_rx_dv_int),
        .gmii_rx_er(gmii_rx_er_int),
        .gmii_rxd(gmii_rxd_int),
        .gmii_tx_en(gmii_tx_en_int),
        .gmii_tx_er(gmii_tx_er_int),
        .gmii_txd(gmii_txd_int),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0}),
        .loc_ref(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_loc_ref_UNCONNECTED ),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_out_UNCONNECTED ),
        .mdio_tri(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_tri_UNCONNECTED ),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset_0),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_powerdown_UNCONNECTED ),
        .reset(reset_out),
        .reset_done(1'b1),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({rxclkcorcnt[2],1'b0,rxclkcorcnt[0]}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_correction_timer_UNCONNECTED [63:0]),
        .rxphy_ns_field(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_ns_field_UNCONNECTED [31:0]),
        .rxphy_s_field(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_s_field_UNCONNECTED [47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(rxrundisp),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_arready_UNCONNECTED ),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_awready_UNCONNECTED ),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bresp_UNCONNECTED [1:0]),
        .s_axi_bvalid(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bvalid_UNCONNECTED ),
        .s_axi_rdata(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rdata_UNCONNECTED [31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rresp_UNCONNECTED [1:0]),
        .s_axi_rvalid(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rvalid_UNCONNECTED ),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_wready_UNCONNECTED ),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect_0),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_speed_selection_UNCONNECTED [1:0]),
        .status_vector({\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED [15:14],status_vector_0[12:8],\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED [8],status_vector_0[7:0]}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_tx_code_group_UNCONNECTED [9:0]),
        .txbuferr(1'b0),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(Tx_WrClk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_lvds_transceiver \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst 
       (.ActCnt_GE_HalfBT(\serdes_1_to_10_i/ActCnt_GE_HalfBT ),
        .ActiveIsSlve(\serdes_1_to_10_i/ActiveIsSlve ),
        .ActiveIsSlve_reg(ActiveIsSlve_i_1_n_0),
        .BaseX_Idly_Load({sload,mload}),
        .BaseX_Rx_Fifo_Rd_En(fifo_read_0),
        .BaseX_Rx_Q_Out(BaseX_Rx_Q_Out),
        .CLK(CLK),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ),
        .D0(\serdes_1_to_10_i/D0 ),
        .\IntRx_BtVal_reg[8] (D),
        .LossOfSignal(\serdes_1_to_10_i/LossOfSignal ),
        .LossOfSignal_reg(LossOfSignal_i_1_n_0),
        .Mstr_Load_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ),
        .Mstr_Load_reg_0(Mstr_Load_i_1_n_0),
        .Q({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 }),
        .Rx_SysClk(Rx_SysClk),
        .\Slve_CntValIn_Out_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ),
        .\Slve_CntValIn_Out_reg[1] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ),
        .\Slve_CntValIn_Out_reg[8] ({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 }),
        .Slve_Load_reg(Slve_Load_i_1_n_0),
        .Tx_WrClk(Tx_WrClk),
        .WrapToZero(\serdes_1_to_10_i/WrapToZero ),
        .WrapToZero_reg(WrapToZero_i_1_n_0),
        .\act_count_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ),
        .\act_count_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ),
        .\act_count_reg[4] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ),
        .\act_count_reg[5] (\serdes_1_to_10_i/act_count_reg ),
        .\active_reg[1] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ),
        .al_rx_valid_out(al_rx_valid_out),
        .\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ),
        .\d21p5_wr_pipe_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ),
        .\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ),
        .\d2p2_wr_pipe_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ),
        .data_out(\rx_elastic_buffer_inst/initialize_ram_complete_sync ),
        .initialize_ram_complete(\rx_elastic_buffer_inst/initialize_ram_complete ),
        .initialize_ram_complete_pulse(\rx_elastic_buffer_inst/initialize_ram_complete_pulse ),
        .initialize_ram_complete_sync_reg1(\rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ),
        .initialize_ram_complete_sync_ris_edg0(\rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ),
        .insert3_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ),
        .insert3_reg_0(insert3_i_1_n_0),
        .insert5_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ),
        .insert5_reg_0(insert5_i_1_n_0),
        .insert_idle_reg__0(\rx_elastic_buffer_inst/insert_idle_reg__0 ),
        .mgt_rx_reset(mgt_rx_reset),
        .monitor_late_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ),
        .monitor_late_reg_0(monitor_late_i_1_n_0),
        .\rd_data_reg_reg[13] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ),
        .remove_idle(\rx_elastic_buffer_inst/remove_idle ),
        .remove_idle_reg__0(\rx_elastic_buffer_inst/remove_idle_reg__0 ),
        .reset_out(reset_out),
        .rxbufstatus(rxbuferr),
        .rxchariscomma_usr_reg(rxchariscomma),
        .rxcharisk_usr_reg(rxcharisk),
        .rxclkcorcnt({rxclkcorcnt[2],rxclkcorcnt[0]}),
        .\rxclkcorcnt_reg[0] (\rxclkcorcnt[0]_i_1_n_0 ),
        .\rxdata_usr_reg[7] (rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxrecreset0(rxrecreset0),
        .rxrundisp(rxrundisp),
        .\s_state_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ),
        .\s_state_reg[0]_0 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .\s_state_reg[2] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .\s_state_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .\s_state_reg[4] ({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ,\serdes_1_to_10_i/p_0_in ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 }),
        .\s_state_reg[4]_0 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .\s_state_reg[4]_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .\s_state_reg[5] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .\tx_data_8b_reg[7]_0 (tx_data_8b),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .\wr_addr_plus2_reg[6] (\wr_addr[6]_i_2_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sgmii_adapt \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].sgmii_logic 
       (.Tx_WrClk(Tx_WrClk),
        .gmii_rx_dv(gmii_rx_dv_int),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rx_er_in(gmii_rx_er_int),
        .gmii_rxd(gmii_rxd_int),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_en_out(gmii_tx_en_int),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_tx_er_out(gmii_tx_er_int),
        .gmii_txd_0(gmii_txd_0),
        .gmii_txd_out(gmii_txd_int),
        .mgt_tx_reset(mgt_tx_reset_0),
        .sgmii_clk_en(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate 
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .O(\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0 
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .O(\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(1'b1),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ),
        .R(rxrecreset0));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .R(rxrecreset0));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .R(rxrecreset0));
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_complete_sync_ris_edg_i_1
       (.I0(\rx_elastic_buffer_inst/initialize_ram_complete_sync ),
        .I1(\rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ),
        .O(\rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ));
  LUT6 #(
    .INIT(64'hFFFFF5F50020A0A0)) 
    insert3_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I2(\serdes_1_to_10_i/p_0_in ),
        .I3(\serdes_1_to_10_i/WrapToZero ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ),
        .O(insert3_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFD5D500808080)) 
    insert5_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .I1(\serdes_1_to_10_i/p_0_in ),
        .I2(\serdes_1_to_10_i/WrapToZero ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ),
        .O(insert5_i_1_n_0));
  LUT5 #(
    .INIT(32'h8BFF8B00)) 
    monitor_late_i_1
       (.I0(\serdes_1_to_10_i/WrapToZero ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I2(\serdes_1_to_10_i/ActCnt_GE_HalfBT ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ),
        .O(monitor_late_i_1_n_0));
  LUT4 #(
    .INIT(16'h4F4C)) 
    \rxclkcorcnt[0]_i_1 
       (.I0(rxclkcorcnt[2]),
        .I1(\rx_elastic_buffer_inst/insert_idle_reg__0 ),
        .I2(rxclkcorcnt[0]),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ),
        .O(\rxclkcorcnt[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF5575)) 
    \wr_addr[6]_i_2 
       (.I0(\rx_elastic_buffer_inst/initialize_ram_complete ),
        .I1(\rx_elastic_buffer_inst/remove_idle ),
        .I2(al_rx_valid_out),
        .I3(\rx_elastic_buffer_inst/remove_idle_reg__0 ),
        .I4(\rx_elastic_buffer_inst/initialize_ram_complete_pulse ),
        .O(\wr_addr[6]_i_2_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_clk_gen
   (sgmii_clk_r_0,
    sgmii_clk_en_reg_0,
    sgmii_clk_f_0,
    Tx_WrClk,
    reset_out,
    data_out,
    speed_is_10_100_fall_reg_0);
  output sgmii_clk_r_0;
  output sgmii_clk_en_reg_0;
  output sgmii_clk_f_0;
  input Tx_WrClk;
  input reset_out;
  input data_out;
  input speed_is_10_100_fall_reg_0;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk12_5_reg;
  wire clk1_25;
  wire clk1_25_reg;
  wire clk_div_stage1_n_3;
  wire clk_en;
  wire clk_en0;
  wire clk_en_12_5_fall;
  wire clk_en_12_5_fall0;
  wire clk_en_1_25_fall;
  wire clk_en_1_25_fall0;
  wire data_out;
  wire reset_fall;
  wire reset_out;
  wire sgmii_clk_en_i_1_n_0;
  wire sgmii_clk_en_reg_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r0_out;
  wire sgmii_clk_r_0;
  wire speed_is_100_fall;
  wire speed_is_10_100_fall;
  wire speed_is_10_100_fall_reg_0;

  FDRE clk12_5_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk12_5),
        .Q(clk12_5_reg),
        .R(reset_out));
  FDRE clk1_25_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk1_25),
        .Q(clk1_25_reg),
        .R(reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr clk_div_stage1
       (.Tx_WrClk(Tx_WrClk),
        .clk12_5(clk12_5),
        .clk12_5_reg(clk12_5_reg),
        .clk1_25(clk1_25),
        .clk_en0(clk_en0),
        .clk_en_12_5_fall0(clk_en_12_5_fall0),
        .reset_fall(reset_fall),
        .reset_out(reset_out),
        .speed_is_100_fall(speed_is_100_fall),
        .speed_is_10_100_fall(speed_is_10_100_fall),
        .speed_is_10_100_fall_reg(clk_div_stage1_n_3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr_2 clk_div_stage2
       (.Tx_WrClk(Tx_WrClk),
        .clk12_5(clk12_5),
        .clk1_25(clk1_25),
        .clk1_25_reg(clk1_25_reg),
        .clk_en(clk_en),
        .clk_en_1_25_fall0(clk_en_1_25_fall0),
        .data_out(data_out),
        .reset_out(reset_out),
        .sgmii_clk_r0_out(sgmii_clk_r0_out),
        .sgmii_clk_r_reg(speed_is_10_100_fall_reg_0));
  FDRE clk_en_12_5_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en_12_5_fall0),
        .Q(clk_en_12_5_fall),
        .R(reset_out));
  FDRE clk_en_12_5_rise_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en0),
        .Q(clk_en),
        .R(reset_out));
  FDRE clk_en_1_25_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en_1_25_fall0),
        .Q(clk_en_1_25_fall),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    reset_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_out),
        .Q(reset_fall),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hE2FF)) 
    sgmii_clk_en_i_1
       (.I0(clk_en_1_25_fall),
        .I1(data_out),
        .I2(clk_en_12_5_fall),
        .I3(speed_is_10_100_fall_reg_0),
        .O(sgmii_clk_en_i_1_n_0));
  FDRE sgmii_clk_en_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sgmii_clk_en_i_1_n_0),
        .Q(sgmii_clk_en_reg_0),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    sgmii_clk_f_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_div_stage1_n_3),
        .Q(sgmii_clk_f_0),
        .R(1'b0));
  FDRE sgmii_clk_r_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sgmii_clk_r0_out),
        .Q(sgmii_clk_r_0),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    speed_is_100_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_out),
        .Q(speed_is_100_fall),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    speed_is_10_100_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_10_100_fall_reg_0),
        .Q(speed_is_10_100_fall),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_decode_8b10b_lut_base
   (D,
    E,
    k,
    Rx_SysClk,
    code_err_i,
    \grdni.run_disp_i_reg_0 ,
    \gde.gdeni.DISP_ERR_reg_0 ,
    b3,
    out);
  output [11:0]D;
  input [0:0]E;
  input k;
  input Rx_SysClk;
  input code_err_i;
  input \grdni.run_disp_i_reg_0 ;
  input \gde.gdeni.DISP_ERR_reg_0 ;
  input [7:5]b3;
  input [4:0]out;

  wire [11:0]D;
  wire [0:0]E;
  wire Rx_SysClk;
  wire [7:5]b3;
  wire code_err_i;
  wire \gde.gdeni.DISP_ERR_reg_0 ;
  wire \grdni.run_disp_i_reg_0 ;
  wire k;
  wire [4:0]out;

  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[0]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[2]),
        .Q(D[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[3]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[4]),
        .Q(D[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[5]),
        .Q(D[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[6]),
        .Q(D[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[7]),
        .Q(D[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gcerr.CODE_ERR_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(code_err_i),
        .Q(D[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gde.gdeni.DISP_ERR_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\gde.gdeni.DISP_ERR_reg_0 ),
        .Q(D[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \grdni.run_disp_i_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\grdni.run_disp_i_reg_0 ),
        .Q(D[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    kout_i_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(k),
        .Q(D[11]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_encode_8b10b_lut_base
   (tx_data_10b,
    Tx_WrClk,
    txchardispval,
    txchardispmode,
    txdata,
    txcharisk);
  output [9:0]tx_data_10b;
  input Tx_WrClk;
  input txchardispval;
  input txchardispmode;
  input [7:0]txdata;
  input txcharisk;

  wire \DOUT[0]_i_1_n_0 ;
  wire \DOUT[1]_i_1_n_0 ;
  wire \DOUT[2]_i_1_n_0 ;
  wire \DOUT[3]_i_1_n_0 ;
  wire \DOUT[4]_i_1_n_0 ;
  wire \DOUT[5]_i_1_n_0 ;
  wire \DOUT[5]_i_2_n_0 ;
  wire \DOUT[9]_i_2_n_0 ;
  wire \DOUT[9]_i_6_n_0 ;
  wire \DOUT[9]_i_7_n_0 ;
  wire \DOUT[9]_i_8_n_0 ;
  wire Tx_WrClk;
  wire [3:0]b4;
  wire [5:0]b6;
  wire disp_in_i__0;
  wire k28;
  wire \ngdb.disp_run_reg_n_0 ;
  wire pdes4;
  wire pdes6__13;
  wire [9:0]tx_data_10b;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;

  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \DOUT[0]_i_1 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .I3(k28),
        .I4(b6[0]),
        .O(\DOUT[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2D807F017F01FE4B)) 
    \DOUT[0]_i_2 
       (.I0(txdata[3]),
        .I1(txdata[4]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[1]),
        .I5(txdata[2]),
        .O(b6[0]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \DOUT[1]_i_1 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .I3(k28),
        .I4(b6[1]),
        .O(\DOUT[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h987170F170F171E6)) 
    \DOUT[1]_i_2 
       (.I0(disp_in_i__0),
        .I1(txdata[0]),
        .I2(txdata[1]),
        .I3(txdata[2]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(b6[1]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[2]_i_1 
       (.I0(b6[2]),
        .I1(k28),
        .O(\DOUT[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h47807F09BF01FE16)) 
    \DOUT[2]_i_2 
       (.I0(txdata[4]),
        .I1(txdata[3]),
        .I2(txdata[1]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(disp_in_i__0),
        .O(b6[2]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[3]_i_1 
       (.I0(b6[3]),
        .I1(k28),
        .O(\DOUT[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hB44C4CCD4CCDCDE1)) 
    \DOUT[3]_i_2 
       (.I0(txdata[4]),
        .I1(txdata[3]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[1]),
        .I5(txdata[2]),
        .O(b6[3]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[4]_i_1 
       (.I0(b6[4]),
        .I1(k28),
        .O(\DOUT[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1F6F08107FEF9061)) 
    \DOUT[4]_i_2 
       (.I0(txdata[2]),
        .I1(txdata[1]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(b6[4]));
  LUT4 #(
    .INIT(16'h02A2)) 
    \DOUT[5]_i_1 
       (.I0(k28),
        .I1(\ngdb.disp_run_reg_n_0 ),
        .I2(txchardispmode),
        .I3(txchardispval),
        .O(\DOUT[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[5]_i_2 
       (.I0(b6[5]),
        .I1(k28),
        .O(\DOUT[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5996A1179660177F)) 
    \DOUT[5]_i_3 
       (.I0(txdata[3]),
        .I1(txdata[4]),
        .I2(txdata[2]),
        .I3(txdata[1]),
        .I4(disp_in_i__0),
        .I5(txdata[0]),
        .O(b6[5]));
  LUT6 #(
    .INIT(64'h8F8F0000B0BFFF0F)) 
    \DOUT[6]_i_1 
       (.I0(\DOUT[9]_i_2_n_0 ),
        .I1(txdata[7]),
        .I2(txdata[6]),
        .I3(k28),
        .I4(txdata[5]),
        .I5(pdes6__13),
        .O(b4[0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h5B5B0D58)) 
    \DOUT[7]_i_1 
       (.I0(txdata[5]),
        .I1(k28),
        .I2(pdes6__13),
        .I3(txdata[7]),
        .I4(txdata[6]),
        .O(b4[1]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h66AA9A59)) 
    \DOUT[8]_i_1 
       (.I0(txdata[7]),
        .I1(txdata[6]),
        .I2(k28),
        .I3(txdata[5]),
        .I4(pdes6__13),
        .O(b4[2]));
  LUT6 #(
    .INIT(64'h737330304C43CF3F)) 
    \DOUT[9]_i_1 
       (.I0(\DOUT[9]_i_2_n_0 ),
        .I1(txdata[7]),
        .I2(txdata[6]),
        .I3(k28),
        .I4(txdata[5]),
        .I5(pdes6__13),
        .O(b4[3]));
  LUT6 #(
    .INIT(64'h727272727272728D)) 
    \DOUT[9]_i_2 
       (.I0(k28),
        .I1(disp_in_i__0),
        .I2(\DOUT[9]_i_6_n_0 ),
        .I3(\DOUT[9]_i_7_n_0 ),
        .I4(\DOUT[9]_i_8_n_0 ),
        .I5(txcharisk),
        .O(\DOUT[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \DOUT[9]_i_3 
       (.I0(txdata[2]),
        .I1(txdata[1]),
        .I2(txcharisk),
        .I3(txdata[0]),
        .I4(txdata[3]),
        .I5(txdata[4]),
        .O(k28));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'h303FAAAA)) 
    \DOUT[9]_i_4 
       (.I0(\DOUT[9]_i_6_n_0 ),
        .I1(txchardispval),
        .I2(txchardispmode),
        .I3(\ngdb.disp_run_reg_n_0 ),
        .I4(k28),
        .O(pdes6__13));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \DOUT[9]_i_5 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .O(disp_in_i__0));
  LUT6 #(
    .INIT(64'h56696AA96AA9A995)) 
    \DOUT[9]_i_6 
       (.I0(disp_in_i__0),
        .I1(txdata[2]),
        .I2(txdata[1]),
        .I3(txdata[0]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(\DOUT[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0040400040000000)) 
    \DOUT[9]_i_7 
       (.I0(txdata[4]),
        .I1(disp_in_i__0),
        .I2(txdata[3]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(txdata[1]),
        .O(\DOUT[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400040400)) 
    \DOUT[9]_i_8 
       (.I0(disp_in_i__0),
        .I1(txdata[4]),
        .I2(txdata[3]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(txdata[1]),
        .O(\DOUT[9]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[0]_i_1_n_0 ),
        .Q(tx_data_10b[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[1]_i_1_n_0 ),
        .Q(tx_data_10b[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[2]_i_1_n_0 ),
        .Q(tx_data_10b[2]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[3]_i_1_n_0 ),
        .Q(tx_data_10b[3]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[4]_i_1_n_0 ),
        .Q(tx_data_10b[4]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[5]_i_2_n_0 ),
        .Q(tx_data_10b[5]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[0]),
        .Q(tx_data_10b[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[1]),
        .Q(tx_data_10b[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[8] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[2]),
        .Q(tx_data_10b[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[9] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[3]),
        .Q(tx_data_10b[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h7C83)) 
    \ngdb.disp_run_i_1 
       (.I0(txdata[7]),
        .I1(txdata[6]),
        .I2(txdata[5]),
        .I3(pdes6__13),
        .O(pdes4));
  FDRE #(
    .INIT(1'b1)) 
    \ngdb.disp_run_reg 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(pdes4),
        .Q(\ngdb.disp_run_reg_n_0 ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr
   (clk12_5,
    clk_en_12_5_fall0,
    clk_en0,
    speed_is_10_100_fall_reg,
    Tx_WrClk,
    reset_out,
    clk12_5_reg,
    speed_is_10_100_fall,
    speed_is_100_fall,
    clk1_25,
    reset_fall);
  output clk12_5;
  output clk_en_12_5_fall0;
  output clk_en0;
  output speed_is_10_100_fall_reg;
  input Tx_WrClk;
  input reset_out;
  input clk12_5_reg;
  input speed_is_10_100_fall;
  input speed_is_100_fall;
  input clk1_25;
  input reset_fall;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk12_5_reg;
  wire clk1_25;
  wire clk_en0;
  wire clk_en_12_5_fall0;
  wire p_0_in;
  wire reg1;
  wire reg2;
  wire reg4;
  wire reg5;
  wire reg5_reg_n_0;
  wire reset_fall;
  wire reset_out;
  wire speed_is_100_fall;
  wire speed_is_10_100_fall;
  wire speed_is_10_100_fall_reg;

  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_12_5_fall_i_1
       (.I0(clk12_5_reg),
        .I1(clk12_5),
        .O(clk_en_12_5_fall0));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_12_5_rise_i_1
       (.I0(clk12_5),
        .I1(clk12_5_reg),
        .O(clk_en0));
  LUT1 #(
    .INIT(2'h1)) 
    reg1_i_1
       (.I0(reg5_reg_n_0),
        .O(p_0_in));
  FDRE reg1_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(reg1),
        .R(reg5));
  FDRE reg2_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg1),
        .Q(reg2),
        .R(reg5));
  FDRE reg3_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg2),
        .Q(clk12_5),
        .R(reg5));
  FDRE reg4_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk12_5),
        .Q(reg4),
        .R(reg5));
  LUT3 #(
    .INIT(8'hF4)) 
    reg5_i_1
       (.I0(reg4),
        .I1(reg5_reg_n_0),
        .I2(reset_out),
        .O(reg5));
  FDRE reg5_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg4),
        .Q(reg5_reg_n_0),
        .R(reg5));
  LUT5 #(
    .INIT(32'h0000DFD5)) 
    sgmii_clk_f_i_1
       (.I0(speed_is_10_100_fall),
        .I1(clk12_5),
        .I2(speed_is_100_fall),
        .I3(clk1_25),
        .I4(reset_fall),
        .O(speed_is_10_100_fall_reg));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr_2
   (clk1_25,
    sgmii_clk_r0_out,
    clk_en_1_25_fall0,
    clk_en,
    Tx_WrClk,
    reset_out,
    sgmii_clk_r_reg,
    data_out,
    clk12_5,
    clk1_25_reg);
  output clk1_25;
  output sgmii_clk_r0_out;
  output clk_en_1_25_fall0;
  input clk_en;
  input Tx_WrClk;
  input reset_out;
  input sgmii_clk_r_reg;
  input data_out;
  input clk12_5;
  input clk1_25_reg;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk1_25;
  wire clk1_25_reg;
  wire clk_en;
  wire clk_en_1_25_fall0;
  wire data_out;
  wire reg1_i_1__0_n_0;
  wire reg1_reg_n_0;
  wire reg2_reg_n_0;
  wire reg4;
  wire reg5;
  wire reg5_reg_n_0;
  wire reset_out;
  wire sgmii_clk_r0_out;
  wire sgmii_clk_r_reg;

  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_1_25_fall_i_1
       (.I0(clk1_25_reg),
        .I1(clk1_25),
        .O(clk_en_1_25_fall0));
  LUT1 #(
    .INIT(2'h1)) 
    reg1_i_1__0
       (.I0(reg5_reg_n_0),
        .O(reg1_i_1__0_n_0));
  FDRE reg1_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg1_i_1__0_n_0),
        .Q(reg1_reg_n_0),
        .R(reg5));
  FDRE reg2_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg1_reg_n_0),
        .Q(reg2_reg_n_0),
        .R(reg5));
  FDRE reg3_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg2_reg_n_0),
        .Q(clk1_25),
        .R(reg5));
  FDRE reg4_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(clk1_25),
        .Q(reg4),
        .R(reg5));
  LUT4 #(
    .INIT(16'hFF40)) 
    reg5_i_1__0
       (.I0(reg4),
        .I1(clk_en),
        .I2(reg5_reg_n_0),
        .I3(reset_out),
        .O(reg5));
  FDRE reg5_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg4),
        .Q(reg5_reg_n_0),
        .R(reg5));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    sgmii_clk_r_i_1
       (.I0(sgmii_clk_r_reg),
        .I1(clk1_25),
        .I2(data_out),
        .I3(clk12_5),
        .O(sgmii_clk_r0_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_lvds_transceiver
   (al_rx_valid_out,
    \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ,
    rxrecreset0,
    \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ,
    BaseX_Rx_Fifo_Rd_En,
    ActCnt_GE_HalfBT,
    LossOfSignal,
    initialize_ram_complete_sync_reg1,
    data_out,
    remove_idle,
    remove_idle_reg__0,
    insert_idle_reg__0,
    rxdisperr,
    rxnotintable,
    rxrundisp,
    rxclkcorcnt,
    initialize_ram_complete,
    initialize_ram_complete_pulse,
    ActiveIsSlve,
    D,
    Mstr_Load_reg,
    WrapToZero,
    monitor_late_reg,
    insert5_reg,
    insert3_reg,
    Q,
    \s_state_reg[4] ,
    \act_count_reg[5] ,
    \Slve_CntValIn_Out_reg[8] ,
    \Slve_CntValIn_Out_reg[1] ,
    \Slve_CntValIn_Out_reg[0] ,
    BaseX_Idly_Load,
    \rd_data_reg_reg[13] ,
    rxchariscomma_usr_reg,
    rxcharisk_usr_reg,
    rxbufstatus,
    \rxdata_usr_reg[7] ,
    \tx_data_8b_reg[7]_0 ,
    \s_state_reg[0] ,
    \s_state_reg[5] ,
    \s_state_reg[4]_0 ,
    \s_state_reg[2] ,
    \s_state_reg[4]_1 ,
    \s_state_reg[0]_0 ,
    \active_reg[1] ,
    \act_count_reg[0] ,
    \act_count_reg[4] ,
    \act_count_reg[3] ,
    \s_state_reg[3] ,
    Rx_SysClk,
    \d2p2_wr_pipe_reg[3] ,
    \d21p5_wr_pipe_reg[3] ,
    D0,
    Tx_WrClk,
    initialize_ram_complete_sync_ris_edg0,
    \rxclkcorcnt_reg[0] ,
    reset_out,
    LossOfSignal_reg,
    ActiveIsSlve_reg,
    Slve_Load_reg,
    Mstr_Load_reg_0,
    WrapToZero_reg,
    monitor_late_reg_0,
    insert5_reg_0,
    insert3_reg_0,
    CLK,
    \IntRx_BtVal_reg[8] ,
    \wr_addr_plus2_reg[6] ,
    txchardispval,
    txchardispmode,
    txdata,
    txcharisk,
    BaseX_Rx_Q_Out,
    mgt_rx_reset);
  output al_rx_valid_out;
  output \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  output rxrecreset0;
  output \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  output [0:0]BaseX_Rx_Fifo_Rd_En;
  output ActCnt_GE_HalfBT;
  output LossOfSignal;
  output initialize_ram_complete_sync_reg1;
  output data_out;
  output remove_idle;
  output remove_idle_reg__0;
  output insert_idle_reg__0;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxrundisp;
  output [1:0]rxclkcorcnt;
  output initialize_ram_complete;
  output initialize_ram_complete_pulse;
  output ActiveIsSlve;
  output [0:0]D;
  output [0:0]Mstr_Load_reg;
  output WrapToZero;
  output monitor_late_reg;
  output insert5_reg;
  output insert3_reg;
  output [8:0]Q;
  output [4:0]\s_state_reg[4] ;
  output [0:0]\act_count_reg[5] ;
  output [6:0]\Slve_CntValIn_Out_reg[8] ;
  output \Slve_CntValIn_Out_reg[1] ;
  output \Slve_CntValIn_Out_reg[0] ;
  output [1:0]BaseX_Idly_Load;
  output [0:0]\rd_data_reg_reg[13] ;
  output rxchariscomma_usr_reg;
  output rxcharisk_usr_reg;
  output [0:0]rxbufstatus;
  output [7:0]\rxdata_usr_reg[7] ;
  output [7:0]\tx_data_8b_reg[7]_0 ;
  output \s_state_reg[0] ;
  output \s_state_reg[5] ;
  output \s_state_reg[4]_0 ;
  output \s_state_reg[2] ;
  output \s_state_reg[4]_1 ;
  output \s_state_reg[0]_0 ;
  output \active_reg[1] ;
  output \act_count_reg[0] ;
  output \act_count_reg[4] ;
  output \act_count_reg[3] ;
  output \s_state_reg[3] ;
  input Rx_SysClk;
  input \d2p2_wr_pipe_reg[3] ;
  input \d21p5_wr_pipe_reg[3] ;
  input D0;
  input Tx_WrClk;
  input initialize_ram_complete_sync_ris_edg0;
  input \rxclkcorcnt_reg[0] ;
  input reset_out;
  input LossOfSignal_reg;
  input ActiveIsSlve_reg;
  input Slve_Load_reg;
  input Mstr_Load_reg_0;
  input WrapToZero_reg;
  input monitor_late_reg_0;
  input insert5_reg_0;
  input insert3_reg_0;
  input CLK;
  input [5:0]\IntRx_BtVal_reg[8] ;
  input \wr_addr_plus2_reg[6] ;
  input txchardispval;
  input txchardispmode;
  input [7:0]txdata;
  input txcharisk;
  input [7:0]BaseX_Rx_Q_Out;
  input mgt_rx_reset;

  wire ActCnt_GE_HalfBT;
  wire ActiveIsSlve;
  wire ActiveIsSlve_reg;
  wire [1:0]BaseX_Idly_Load;
  wire [0:0]BaseX_Rx_Fifo_Rd_En;
  wire [7:0]BaseX_Rx_Q_Out;
  wire CLK;
  wire [0:0]D;
  wire D0;
  wire [5:0]\IntRx_BtVal_reg[8] ;
  wire LossOfSignal;
  wire LossOfSignal_reg;
  wire [0:0]Mstr_Load_reg;
  wire Mstr_Load_reg_0;
  wire [8:0]Q;
  wire Rx_SysClk;
  wire \Slve_CntValIn_Out_reg[0] ;
  wire \Slve_CntValIn_Out_reg[1] ;
  wire [6:0]\Slve_CntValIn_Out_reg[8] ;
  wire Slve_Load_reg;
  wire Tx_WrClk;
  wire WrapToZero;
  wire WrapToZero_reg;
  wire \act_count_reg[0] ;
  wire \act_count_reg[3] ;
  wire \act_count_reg[4] ;
  wire [0:0]\act_count_reg[5] ;
  wire \active_reg[1] ;
  wire al_rx_valid_out;
  wire [7:5]b3;
  wire code_err_i;
  wire counter_flag;
  wire counter_flag_i_2_n_0;
  wire counter_flag_i_3_n_0;
  wire counter_flag_reg_n_0;
  wire counter_stg0_carry__0_n_6;
  wire counter_stg0_carry__0_n_7;
  wire counter_stg0_carry_n_0;
  wire counter_stg0_carry_n_1;
  wire counter_stg0_carry_n_2;
  wire counter_stg0_carry_n_3;
  wire counter_stg0_carry_n_4;
  wire counter_stg0_carry_n_5;
  wire counter_stg0_carry_n_6;
  wire counter_stg0_carry_n_7;
  wire \counter_stg[0]_i_1_n_0 ;
  wire [11:0]counter_stg_reg;
  wire \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  wire \d21p5_wr_pipe_reg[3] ;
  wire \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  wire \d2p2_wr_pipe_reg[3] ;
  wire data_out;
  wire decoded_rxchariscomma;
  wire decoded_rxchariscomma0;
  wire decoded_rxcharisk;
  wire [7:0]decoded_rxdata;
  wire decoded_rxdisperr;
  wire decoded_rxnotintable;
  wire decoded_rxrundisp;
  wire elastic_buffer_rst_125;
  wire elastic_buffer_rst_312;
  wire initialize_ram_complete;
  wire initialize_ram_complete_pulse;
  wire initialize_ram_complete_sync_reg1;
  wire initialize_ram_complete_sync_ris_edg0;
  wire insert3_reg;
  wire insert3_reg_0;
  wire insert5_reg;
  wire insert5_reg_0;
  wire insert_idle_reg__0;
  wire k;
  wire mgt_rx_reset;
  wire monitor_late_reg;
  wire monitor_late_reg_0;
  wire [11:1]p_0_in__3;
  wire [0:0]\rd_data_reg_reg[13] ;
  wire remove_idle;
  wire remove_idle_reg__0;
  wire reset_out;
  wire reset_sync_312_rxelastic_buffer_n_0;
  wire rx_rst_312;
  wire [0:0]rxbufstatus;
  wire rxchariscomma_usr_reg;
  wire rxcharisk_usr_reg;
  wire [1:0]rxclkcorcnt;
  wire \rxclkcorcnt_reg[0] ;
  wire [7:0]\rxdata_usr_reg[7] ;
  wire [0:0]rxdisperr;
  wire [0:0]rxnotintable;
  wire rxrecreset0;
  wire [0:0]rxrundisp;
  wire \s_state_reg[0] ;
  wire \s_state_reg[0]_0 ;
  wire \s_state_reg[2] ;
  wire \s_state_reg[3] ;
  wire [4:0]\s_state_reg[4] ;
  wire \s_state_reg[4]_0 ;
  wire \s_state_reg[4]_1 ;
  wire \s_state_reg[5] ;
  wire sel;
  wire serdes_1_to_10_i_n_48;
  wire serdes_1_to_10_i_n_49;
  wire serdes_1_to_10_i_n_50;
  wire serdes_1_to_10_i_n_51;
  wire serdes_1_to_10_i_n_52;
  wire serdes_1_to_10_i_n_54;
  wire serdes_1_to_10_i_n_55;
  wire [9:0]tx_data_10b;
  wire [7:0]tx_data_8b_int;
  wire [7:0]\tx_data_8b_reg[7]_0 ;
  wire tx_rst_125;
  wire tx_rst_156;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire \wr_addr_plus2_reg[6] ;
  wire [7:2]NLW_counter_stg0_carry__0_CO_UNCONNECTED;
  wire [7:3]NLW_counter_stg0_carry__0_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0000000000008000)) 
    counter_flag_i_1
       (.I0(counter_stg_reg[2]),
        .I1(counter_stg_reg[4]),
        .I2(counter_stg_reg[1]),
        .I3(counter_stg_reg[10]),
        .I4(counter_flag_i_2_n_0),
        .I5(counter_flag_i_3_n_0),
        .O(counter_flag));
  LUT4 #(
    .INIT(16'h7FFF)) 
    counter_flag_i_2
       (.I0(counter_stg_reg[7]),
        .I1(counter_stg_reg[3]),
        .I2(counter_stg_reg[11]),
        .I3(counter_stg_reg[8]),
        .O(counter_flag_i_2_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    counter_flag_i_3
       (.I0(counter_stg_reg[6]),
        .I1(counter_stg_reg[0]),
        .I2(counter_stg_reg[9]),
        .I3(counter_stg_reg[5]),
        .O(counter_flag_i_3_n_0));
  FDRE counter_flag_reg
       (.C(Tx_WrClk),
        .CE(counter_flag),
        .D(counter_flag),
        .Q(counter_flag_reg_n_0),
        .R(reset_out));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 counter_stg0_carry
       (.CI(counter_stg_reg[0]),
        .CI_TOP(1'b0),
        .CO({counter_stg0_carry_n_0,counter_stg0_carry_n_1,counter_stg0_carry_n_2,counter_stg0_carry_n_3,counter_stg0_carry_n_4,counter_stg0_carry_n_5,counter_stg0_carry_n_6,counter_stg0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in__3[8:1]),
        .S(counter_stg_reg[8:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 counter_stg0_carry__0
       (.CI(counter_stg0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_counter_stg0_carry__0_CO_UNCONNECTED[7:2],counter_stg0_carry__0_n_6,counter_stg0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_counter_stg0_carry__0_O_UNCONNECTED[7:3],p_0_in__3[11:9]}),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,counter_stg_reg[11:9]}));
  LUT3 #(
    .INIT(8'h09)) 
    \counter_stg[0]_i_1 
       (.I0(counter_stg_reg[0]),
        .I1(counter_flag_reg_n_0),
        .I2(reset_out),
        .O(\counter_stg[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg[11]_i_1 
       (.I0(counter_flag_reg_n_0),
        .O(sel));
  FDRE \counter_stg_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\counter_stg[0]_i_1_n_0 ),
        .Q(counter_stg_reg[0]),
        .R(1'b0));
  FDRE \counter_stg_reg[10] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[10]),
        .Q(counter_stg_reg[10]),
        .R(reset_out));
  FDRE \counter_stg_reg[11] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[11]),
        .Q(counter_stg_reg[11]),
        .R(reset_out));
  FDRE \counter_stg_reg[1] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[1]),
        .Q(counter_stg_reg[1]),
        .R(reset_out));
  FDRE \counter_stg_reg[2] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[2]),
        .Q(counter_stg_reg[2]),
        .R(reset_out));
  FDRE \counter_stg_reg[3] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[3]),
        .Q(counter_stg_reg[3]),
        .R(reset_out));
  FDRE \counter_stg_reg[4] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[4]),
        .Q(counter_stg_reg[4]),
        .R(reset_out));
  FDRE \counter_stg_reg[5] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[5]),
        .Q(counter_stg_reg[5]),
        .R(reset_out));
  FDRE \counter_stg_reg[6] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[6]),
        .Q(counter_stg_reg[6]),
        .R(reset_out));
  FDRE \counter_stg_reg[7] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[7]),
        .Q(counter_stg_reg[7]),
        .R(reset_out));
  FDRE \counter_stg_reg[8] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[8]),
        .Q(counter_stg_reg[8]),
        .R(reset_out));
  FDRE \counter_stg_reg[9] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[9]),
        .Q(counter_stg_reg[9]),
        .R(reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_decode_8b10b_lut_base decode_8b10b
       (.D({decoded_rxcharisk,decoded_rxdisperr,decoded_rxnotintable,decoded_rxrundisp,decoded_rxdata}),
        .E(al_rx_valid_out),
        .Rx_SysClk(Rx_SysClk),
        .b3(b3),
        .code_err_i(code_err_i),
        .\gde.gdeni.DISP_ERR_reg_0 (serdes_1_to_10_i_n_55),
        .\grdni.run_disp_i_reg_0 (serdes_1_to_10_i_n_54),
        .k(k),
        .out({serdes_1_to_10_i_n_48,serdes_1_to_10_i_n_49,serdes_1_to_10_i_n_50,serdes_1_to_10_i_n_51,serdes_1_to_10_i_n_52}));
  FDRE decoded_rxchariscomma_reg
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(decoded_rxchariscomma0),
        .Q(decoded_rxchariscomma),
        .R(1'b0));
  FDSE elastic_buffer_rst_125_reg
       (.C(Tx_WrClk),
        .CE(counter_flag),
        .D(1'b0),
        .Q(elastic_buffer_rst_125),
        .S(reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_encode_8b10b_lut_base encode_8b10b
       (.Tx_WrClk(Tx_WrClk),
        .tx_data_10b(tx_data_10b),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_ten_to_eight gb_out_inst
       (.CLK(CLK),
        .Q(tx_data_8b_int),
        .Tx_WrClk(Tx_WrClk),
        .reset_out(tx_rst_125),
        .tx_data_10b(tx_data_10b));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_3 reset_sync_125_tx
       (.Tx_WrClk(Tx_WrClk),
        .reset_out(reset_out),
        .reset_sync6_0(tx_rst_125));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_4 reset_sync_312_rx
       (.Rx_SysClk(Rx_SysClk),
        .SR(rxrecreset0),
        .\d21p5_wr_pipe_reg[3] (elastic_buffer_rst_312),
        .reset_out(rx_rst_312),
        .reset_sync5_0(reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_5 reset_sync_312_rxelastic_buffer
       (.Rx_SysClk(Rx_SysClk),
        .SR(reset_sync_312_rxelastic_buffer_n_0),
        .data_in(initialize_ram_complete),
        .elastic_buffer_rst_125(elastic_buffer_rst_125),
        .mgt_rx_reset(mgt_rx_reset),
        .reset_out(elastic_buffer_rst_312),
        .\wr_data_reg[0] (rx_rst_312));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_6 reset_sync_312_tx
       (.CLK(CLK),
        .reset_out(reset_out),
        .reset_sync6_0(tx_rst_156));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_elastic_buffer rx_elastic_buffer_inst
       (.D(remove_idle),
        .E(al_rx_valid_out),
        .Rx_SysClk(Rx_SysClk),
        .SR(rxrecreset0),
        .Tx_WrClk(Tx_WrClk),
        .\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 (\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ),
        .\d21p5_wr_pipe_reg[3]_0 (\d21p5_wr_pipe_reg[3] ),
        .\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 (\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ),
        .\d2p2_wr_pipe_reg[3]_0 (\d2p2_wr_pipe_reg[3] ),
        .data_in(initialize_ram_complete),
        .data_out(data_out),
        .elastic_buffer_rst_125(elastic_buffer_rst_125),
        .\initialize_counter_reg[5]_0 (rx_rst_312),
        .initialize_ram_complete_pulse_reg_0(initialize_ram_complete_pulse),
        .initialize_ram_complete_sync_reg1(initialize_ram_complete_sync_reg1),
        .initialize_ram_complete_sync_ris_edg0(initialize_ram_complete_sync_ris_edg0),
        .insert_idle_reg__0(insert_idle_reg__0),
        .mgt_rx_reset(mgt_rx_reset),
        .\rd_data_reg_reg[13]_0 (\rd_data_reg_reg[13] ),
        .remove_idle_reg_reg_0(remove_idle_reg__0),
        .reset_modified_reg_0(reset_out),
        .reset_out(elastic_buffer_rst_312),
        .rxbufstatus(rxbufstatus),
        .rxchariscomma_usr_reg_0(rxchariscomma_usr_reg),
        .rxcharisk_usr_reg_0(rxcharisk_usr_reg),
        .rxclkcorcnt(rxclkcorcnt),
        .\rxclkcorcnt_reg[0]_0 (\rxclkcorcnt_reg[0] ),
        .\rxdata_usr_reg[7]_0 (\rxdata_usr_reg[7] ),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxrundisp(rxrundisp),
        .\wr_addr_plus2_reg[6]_0 (\wr_addr_plus2_reg[6] ),
        .\wr_data_reg[0]_0 (reset_sync_312_rxelastic_buffer_n_0),
        .\wr_data_reg[12]_0 ({decoded_rxchariscomma,decoded_rxcharisk,decoded_rxdisperr,decoded_rxnotintable,decoded_rxrundisp,decoded_rxdata}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_serdes_1_to_10 serdes_1_to_10_i
       (.ActCnt_GE_HalfBT_reg_0(ActCnt_GE_HalfBT),
        .ActiveIsSlve_reg_0(ActiveIsSlve),
        .ActiveIsSlve_reg_1(ActiveIsSlve_reg),
        .BaseX_Idly_Load(BaseX_Idly_Load),
        .BaseX_Rx_Fifo_Rd_En(BaseX_Rx_Fifo_Rd_En),
        .BaseX_Rx_Q_Out(BaseX_Rx_Q_Out),
        .D(D),
        .D0(D0),
        .E(al_rx_valid_out),
        .\IntReset_dly_reg[0]_0 (rx_rst_312),
        .\IntRx_BtVal_reg[8]_0 (\IntRx_BtVal_reg[8] ),
        .LossOfSignal_reg_0(LossOfSignal_reg),
        .\Mstr_CntValIn_Out_reg[8]_0 (Q),
        .Mstr_Load_reg_0(Mstr_Load_reg),
        .Mstr_Load_reg_1(Mstr_Load_reg_0),
        .Q(\Slve_CntValIn_Out_reg[8] ),
        .Rx_SysClk(Rx_SysClk),
        .SR(LossOfSignal),
        .\Slve_CntValIn_Out_reg[0]_0 (\Slve_CntValIn_Out_reg[0] ),
        .\Slve_CntValIn_Out_reg[1]_0 (\Slve_CntValIn_Out_reg[1] ),
        .Slve_Load_reg_0(Slve_Load_reg),
        .WrapToZero(WrapToZero),
        .WrapToZero_reg_0(WrapToZero_reg),
        .\act_count_reg[0]_0 (\act_count_reg[0] ),
        .\act_count_reg[3]_0 (\act_count_reg[3] ),
        .\act_count_reg[4]_0 (\act_count_reg[4] ),
        .\act_count_reg[5]_0 (\act_count_reg[5] ),
        .\active_reg[1]_0 (\active_reg[1] ),
        .b3(b3),
        .code_err_i(code_err_i),
        .decoded_rxchariscomma0(decoded_rxchariscomma0),
        .\grdni.run_disp_i_reg (serdes_1_to_10_i_n_54),
        .\grdni.run_disp_i_reg_0 (serdes_1_to_10_i_n_55),
        .\grdni.run_disp_i_reg_1 (decoded_rxrundisp),
        .insert3_reg_0(insert3_reg),
        .insert3_reg_1(insert3_reg_0),
        .insert5_reg_0(insert5_reg),
        .insert5_reg_1(insert5_reg_0),
        .k(k),
        .monitor_late_reg_0(monitor_late_reg),
        .monitor_late_reg_1(monitor_late_reg_0),
        .out({serdes_1_to_10_i_n_48,serdes_1_to_10_i_n_49,serdes_1_to_10_i_n_50,serdes_1_to_10_i_n_51,serdes_1_to_10_i_n_52}),
        .\s_state_reg[0]_0 (\s_state_reg[0] ),
        .\s_state_reg[0]_1 (\s_state_reg[0]_0 ),
        .\s_state_reg[2]_0 (\s_state_reg[2] ),
        .\s_state_reg[3]_0 (\s_state_reg[3] ),
        .\s_state_reg[4]_0 (\s_state_reg[4] ),
        .\s_state_reg[4]_1 (\s_state_reg[4]_0 ),
        .\s_state_reg[4]_2 (\s_state_reg[4]_1 ),
        .\s_state_reg[5]_0 (\s_state_reg[5] ));
  FDRE \tx_data_8b_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[0]),
        .Q(\tx_data_8b_reg[7]_0 [0]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[1]),
        .Q(\tx_data_8b_reg[7]_0 [1]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[2]),
        .Q(\tx_data_8b_reg[7]_0 [2]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[3]),
        .Q(\tx_data_8b_reg[7]_0 [3]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[4]),
        .Q(\tx_data_8b_reg[7]_0 [4]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[5]),
        .Q(\tx_data_8b_reg[7]_0 [5]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[6]),
        .Q(\tx_data_8b_reg[7]_0 [6]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[7]),
        .Q(\tx_data_8b_reg[7]_0 [7]),
        .R(tx_rst_156));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync
   (rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    Tx_WrClk);
  output rst_125_out;
  input tx_logic_reset;
  input rx_logic_reset;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire logic_reset;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire rst_125_out;
  wire rx_logic_reset;
  wire tx_logic_reset;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(logic_reset),
        .Q(reset_sync_reg1));
  LUT2 #(
    .INIT(4'hE)) 
    reset_sync1_i_1__0
       (.I0(tx_logic_reset),
        .I1(rx_logic_reset),
        .O(logic_reset));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(logic_reset),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(logic_reset),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(logic_reset),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(logic_reset),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(rst_125_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_0
   (reset_out,
    Tx_WrClk,
    mgt_tx_reset);
  output reset_out;
  input Tx_WrClk;
  input mgt_tx_reset;

  wire Tx_WrClk;
  wire mgt_tx_reset;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_24
   (reset_out,
    reset_sync1_0,
    ResetIn);
  output reset_out;
  input reset_sync1_0;
  input ResetIn;

  wire ResetIn;
  wire reset_out;
  wire reset_sync1_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(1'b0),
        .PRE(ResetIn),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(ResetIn),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(ResetIn),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(ResetIn),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(ResetIn),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_25
   (Rx_LogicRst,
    Rx_SysClk,
    reset_in);
  output Rx_LogicRst;
  input Rx_SysClk;
  input reset_in;

  wire Rx_LogicRst;
  wire Rx_SysClk;
  wire reset_in;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(Rx_LogicRst));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_26
   (Tx_LogicRst,
    Tx_WrClk,
    reset_in);
  output Tx_LogicRst;
  input Tx_WrClk;
  input reset_in;

  wire Tx_LogicRst;
  wire Tx_WrClk;
  wire reset_in;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(Tx_LogicRst));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_3
   (reset_sync6_0,
    Tx_WrClk,
    reset_out);
  output reset_sync6_0;
  input Tx_WrClk;
  input reset_out;

  wire Tx_WrClk;
  wire reset_out;
  wire reset_sync6_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_out),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_out),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_out),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_out),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_sync6_0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_4
   (SR,
    reset_out,
    \d21p5_wr_pipe_reg[3] ,
    Rx_SysClk,
    reset_sync5_0);
  output [0:0]SR;
  output reset_out;
  input \d21p5_wr_pipe_reg[3] ;
  input Rx_SysClk;
  input reset_sync5_0;

  wire Rx_SysClk;
  wire [0:0]SR;
  wire \d21p5_wr_pipe_reg[3] ;
  wire reset_out;
  wire reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr[6]_i_1 
       (.I0(reset_out),
        .I1(\d21p5_wr_pipe_reg[3] ),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_5
   (SR,
    reset_out,
    \wr_data_reg[0] ,
    data_in,
    elastic_buffer_rst_125,
    mgt_rx_reset,
    Rx_SysClk);
  output [0:0]SR;
  output reset_out;
  input \wr_data_reg[0] ;
  input data_in;
  input elastic_buffer_rst_125;
  input mgt_rx_reset;
  input Rx_SysClk;

  wire Rx_SysClk;
  wire [0:0]SR;
  wire data_in;
  wire elastic_buffer_rst_125;
  wire mgt_rx_reset;
  wire reset_in0;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire \wr_data_reg[0] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in0),
        .Q(reset_sync_reg1));
  LUT2 #(
    .INIT(4'hE)) 
    reset_sync1_i_1
       (.I0(elastic_buffer_rst_125),
        .I1(mgt_rx_reset),
        .O(reset_in0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
  LUT3 #(
    .INIT(8'hEF)) 
    \wr_data_reg_reg[13]_i_1 
       (.I0(reset_out),
        .I1(\wr_data_reg[0] ),
        .I2(data_in),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_6
   (reset_sync6_0,
    CLK,
    reset_out);
  output reset_sync6_0;
  input CLK;
  input reset_out;

  wire CLK;
  wire reset_out;
  wire reset_sync6_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_out),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_out),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_out),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_out),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_sync6_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_elastic_buffer
   (\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ,
    \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ,
    initialize_ram_complete_sync_reg1,
    data_out,
    D,
    remove_idle_reg_reg_0,
    insert_idle_reg__0,
    rxdisperr,
    rxnotintable,
    rxrundisp,
    rxclkcorcnt,
    data_in,
    initialize_ram_complete_pulse_reg_0,
    \rd_data_reg_reg[13]_0 ,
    rxchariscomma_usr_reg_0,
    rxcharisk_usr_reg_0,
    rxbufstatus,
    \rxdata_usr_reg[7]_0 ,
    E,
    Rx_SysClk,
    SR,
    \d2p2_wr_pipe_reg[3]_0 ,
    \d21p5_wr_pipe_reg[3]_0 ,
    Tx_WrClk,
    initialize_ram_complete_sync_ris_edg0,
    \rxclkcorcnt_reg[0]_0 ,
    reset_out,
    \initialize_counter_reg[5]_0 ,
    reset_modified_reg_0,
    mgt_rx_reset,
    elastic_buffer_rst_125,
    \wr_addr_plus2_reg[6]_0 ,
    \wr_data_reg[0]_0 ,
    \wr_data_reg[12]_0 );
  output \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  output \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  output initialize_ram_complete_sync_reg1;
  output data_out;
  output [0:0]D;
  output remove_idle_reg_reg_0;
  output insert_idle_reg__0;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxrundisp;
  output [1:0]rxclkcorcnt;
  output data_in;
  output initialize_ram_complete_pulse_reg_0;
  output [0:0]\rd_data_reg_reg[13]_0 ;
  output rxchariscomma_usr_reg_0;
  output rxcharisk_usr_reg_0;
  output [0:0]rxbufstatus;
  output [7:0]\rxdata_usr_reg[7]_0 ;
  input [0:0]E;
  input Rx_SysClk;
  input [0:0]SR;
  input \d2p2_wr_pipe_reg[3]_0 ;
  input \d21p5_wr_pipe_reg[3]_0 ;
  input Tx_WrClk;
  input initialize_ram_complete_sync_ris_edg0;
  input \rxclkcorcnt_reg[0]_0 ;
  input reset_out;
  input \initialize_counter_reg[5]_0 ;
  input reset_modified_reg_0;
  input mgt_rx_reset;
  input elastic_buffer_rst_125;
  input \wr_addr_plus2_reg[6]_0 ;
  input [0:0]\wr_data_reg[0]_0 ;
  input [12:0]\wr_data_reg[12]_0 ;

  wire [0:0]D;
  wire [0:0]E;
  wire Rx_SysClk;
  wire [0:0]SR;
  wire Tx_WrClk;
  wire \__1/ram_reg_64_127_0_6_i_1_n_0 ;
  wire d16p2_wr;
  wire \d16p2_wr_pipe[0]_i_2_n_0 ;
  wire \d16p2_wr_pipe_reg_n_0_[0] ;
  wire d21p5_wr;
  wire [3:3]d21p5_wr_pipe;
  wire \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  wire \d21p5_wr_pipe_reg[3]_0 ;
  wire d2p2_wr;
  wire [3:3]d2p2_wr_pipe;
  wire \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  wire \d2p2_wr_pipe_reg[3]_0 ;
  wire data_in;
  wire data_out;
  wire [13:0]dpo;
  wire elastic_buffer_rst_125;
  wire even;
  wire even0;
  wire even_i_1_n_0;
  wire even_i_2_n_0;
  wire even_i_3_n_0;
  wire initialize_counter0;
  wire [5:0]initialize_counter_reg;
  wire \initialize_counter_reg[5]_0 ;
  wire initialize_ram;
  wire initialize_ram0;
  wire initialize_ram_complete_i_2_n_0;
  wire initialize_ram_complete_i_3_n_0;
  wire initialize_ram_complete_pulse0;
  wire initialize_ram_complete_pulse_reg_0;
  wire initialize_ram_complete_reg__0;
  wire initialize_ram_complete_sync_reg1;
  wire initialize_ram_complete_sync_ris_edg;
  wire initialize_ram_complete_sync_ris_edg0;
  wire initialize_ram_i_1_n_0;
  wire insert_idle;
  wire insert_idle_i_10_n_0;
  wire insert_idle_i_11_n_0;
  wire insert_idle_i_12_n_0;
  wire insert_idle_i_2_n_0;
  wire insert_idle_i_3_n_0;
  wire insert_idle_i_4_n_0;
  wire insert_idle_i_5_n_0;
  wire insert_idle_i_6_n_0;
  wire insert_idle_i_7_n_0;
  wire insert_idle_i_8_n_0;
  wire insert_idle_i_9_n_0;
  wire insert_idle_reg__0;
  wire \k28p5_wr_pipe[0]_i_2_n_0 ;
  wire \k28p5_wr_pipe_reg_n_0_[0] ;
  wire \k28p5_wr_pipe_reg_n_0_[1] ;
  wire \k28p5_wr_pipe_reg_n_0_[2] ;
  wire \k28p5_wr_pipe_reg_n_0_[3] ;
  wire \k28p5_wr_pipe_reg_n_0_[4] ;
  wire mgt_rx_reset;
  wire p_0_in;
  wire [5:0]p_0_in__0;
  wire [6:0]p_0_in__1;
  wire p_1_in;
  wire p_1_in28_in;
  wire p_1_in3_in;
  wire p_2_in;
  wire p_2_in20_in;
  wire p_2_in29_in;
  wire p_3_in;
  wire p_3_in31_in;
  wire [0:0]p_3_out;
  wire p_4_in23_in;
  wire p_4_in33_in;
  wire p_4_in9_in;
  wire p_5_in;
  wire p_5_in35_in;
  wire [5:1]p_6_out;
  wire ram_reg_0_63_0_6_i_1_n_0;
  wire ram_reg_0_63_0_6_n_0;
  wire ram_reg_0_63_0_6_n_1;
  wire ram_reg_0_63_0_6_n_2;
  wire ram_reg_0_63_0_6_n_3;
  wire ram_reg_0_63_0_6_n_4;
  wire ram_reg_0_63_0_6_n_5;
  wire ram_reg_0_63_0_6_n_6;
  wire ram_reg_0_63_7_13_n_0;
  wire ram_reg_0_63_7_13_n_1;
  wire ram_reg_0_63_7_13_n_2;
  wire ram_reg_0_63_7_13_n_3;
  wire ram_reg_0_63_7_13_n_4;
  wire ram_reg_0_63_7_13_n_5;
  wire ram_reg_0_63_7_13_n_6;
  wire ram_reg_64_127_0_6_n_0;
  wire ram_reg_64_127_0_6_n_1;
  wire ram_reg_64_127_0_6_n_2;
  wire ram_reg_64_127_0_6_n_3;
  wire ram_reg_64_127_0_6_n_4;
  wire ram_reg_64_127_0_6_n_5;
  wire ram_reg_64_127_0_6_n_6;
  wire ram_reg_64_127_7_13_n_0;
  wire ram_reg_64_127_7_13_n_1;
  wire ram_reg_64_127_7_13_n_2;
  wire ram_reg_64_127_7_13_n_3;
  wire ram_reg_64_127_7_13_n_4;
  wire ram_reg_64_127_7_13_n_5;
  wire ram_reg_64_127_7_13_n_6;
  wire [6:0]rd_addr;
  wire [5:0]rd_addr_gray;
  wire \rd_addr_gray[0]_i_1_n_0 ;
  wire \rd_addr_gray[1]_i_1_n_0 ;
  wire \rd_addr_gray[2]_i_1_n_0 ;
  wire \rd_addr_gray[3]_i_1_n_0 ;
  wire \rd_addr_gray[4]_i_1_n_0 ;
  wire \rd_addr_gray[5]_i_1_n_0 ;
  wire [6:0]rd_addr_plus1;
  wire \rd_addr_plus2[6]_i_2_n_0 ;
  wire \rd_addr_plus2_reg_n_0_[0] ;
  wire \rd_addr_plus2_reg_n_0_[6] ;
  wire \rd_data_reg_n_0_[0] ;
  wire \rd_data_reg_n_0_[10] ;
  wire \rd_data_reg_n_0_[11] ;
  wire \rd_data_reg_n_0_[12] ;
  wire \rd_data_reg_n_0_[13] ;
  wire \rd_data_reg_n_0_[1] ;
  wire \rd_data_reg_n_0_[2] ;
  wire \rd_data_reg_n_0_[3] ;
  wire \rd_data_reg_n_0_[4] ;
  wire \rd_data_reg_n_0_[5] ;
  wire \rd_data_reg_n_0_[6] ;
  wire \rd_data_reg_n_0_[7] ;
  wire \rd_data_reg_n_0_[9] ;
  wire [0:0]\rd_data_reg_reg[13]_0 ;
  wire \rd_data_reg_reg_n_0_[0] ;
  wire \rd_data_reg_reg_n_0_[10] ;
  wire \rd_data_reg_reg_n_0_[12] ;
  wire \rd_data_reg_reg_n_0_[1] ;
  wire \rd_data_reg_reg_n_0_[2] ;
  wire \rd_data_reg_reg_n_0_[3] ;
  wire \rd_data_reg_reg_n_0_[4] ;
  wire \rd_data_reg_reg_n_0_[5] ;
  wire \rd_data_reg_reg_n_0_[6] ;
  wire \rd_data_reg_reg_n_0_[7] ;
  wire \rd_data_reg_reg_n_0_[8] ;
  wire \rd_data_reg_reg_n_0_[9] ;
  wire rd_enable;
  wire rd_enable_reg;
  wire [6:0]rd_occupancy;
  wire [6:0]rd_occupancy01_out;
  wire rd_occupancy0_carry_n_2;
  wire rd_occupancy0_carry_n_3;
  wire rd_occupancy0_carry_n_4;
  wire rd_occupancy0_carry_n_5;
  wire rd_occupancy0_carry_n_6;
  wire rd_occupancy0_carry_n_7;
  wire [5:0]rd_wr_addr;
  wire rd_wr_addr_gray_0;
  wire rd_wr_addr_gray_1;
  wire rd_wr_addr_gray_2;
  wire rd_wr_addr_gray_3;
  wire rd_wr_addr_gray_4;
  wire rd_wr_addr_gray_5;
  wire rd_wr_addr_gray_6;
  wire \reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[1].sync_rd_addrgray_n_1 ;
  wire \reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[6].sync_rd_addrgray_n_1 ;
  wire \reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[1].sync_wr_addrgray_n_1 ;
  wire \reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[6].sync_wr_addrgray_n_1 ;
  wire remove_idle0;
  wire remove_idle_i_2_n_0;
  wire remove_idle_i_3_n_0;
  wire remove_idle_i_4_n_0;
  wire remove_idle_i_5_n_0;
  wire remove_idle_i_6_n_0;
  wire remove_idle_reg2;
  wire remove_idle_reg3;
  wire remove_idle_reg4;
  wire remove_idle_reg_reg_0;
  wire reset_modified;
  wire reset_modified_i_1_n_0;
  wire reset_modified_reg_0;
  wire reset_out;
  wire rxbuferr_i_1_n_0;
  wire rxbuferr_i_2_n_0;
  wire rxbuferr_i_3_n_0;
  wire [0:0]rxbufstatus;
  wire rxchariscomma_usr_i_1_n_0;
  wire rxchariscomma_usr_i_2_n_0;
  wire rxchariscomma_usr_reg_0;
  wire rxcharisk_usr_i_1_n_0;
  wire rxcharisk_usr_reg_0;
  wire [1:0]rxclkcorcnt;
  wire \rxclkcorcnt[2]_i_1_n_0 ;
  wire \rxclkcorcnt_reg[0]_0 ;
  wire \rxdata_usr[0]_i_1_n_0 ;
  wire \rxdata_usr[1]_i_1_n_0 ;
  wire \rxdata_usr[2]_i_1_n_0 ;
  wire \rxdata_usr[3]_i_1_n_0 ;
  wire \rxdata_usr[4]_i_1_n_0 ;
  wire \rxdata_usr[5]_i_1_n_0 ;
  wire \rxdata_usr[6]_i_1_n_0 ;
  wire \rxdata_usr[7]_i_1_n_0 ;
  wire [7:0]\rxdata_usr_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire rxdisperr_usr_i_1_n_0;
  wire [0:0]rxnotintable;
  wire [0:0]rxrundisp;
  wire rxrundisp_usr_i_1_n_0;
  wire start;
  wire [6:0]wr_addr;
  wire \wr_addr[5]_i_2_n_0 ;
  wire \wr_addr[6]_i_3_n_0 ;
  wire [5:5]wr_addr__0;
  wire [6:0]wr_addr_gray;
  wire [6:0]wr_addr_plus1;
  wire \wr_addr_plus1[6]_i_1_n_0 ;
  wire \wr_addr_plus2[0]_i_1_n_0 ;
  wire \wr_addr_plus2[1]_i_1_n_0 ;
  wire \wr_addr_plus2[2]_i_1_n_0 ;
  wire \wr_addr_plus2[3]_i_1_n_0 ;
  wire \wr_addr_plus2[4]_i_1_n_0 ;
  wire \wr_addr_plus2[5]_i_1_n_0 ;
  wire \wr_addr_plus2[6]_i_1_n_0 ;
  wire \wr_addr_plus2[6]_i_2_n_0 ;
  wire \wr_addr_plus2_reg[6]_0 ;
  wire \wr_addr_plus2_reg_n_0_[0] ;
  wire \wr_addr_plus2_reg_n_0_[6] ;
  wire [12:8]wr_data;
  wire [13:0]wr_data_reg;
  wire [0:0]\wr_data_reg[0]_0 ;
  wire [12:0]\wr_data_reg[12]_0 ;
  wire \wr_data_reg_n_0_[0] ;
  wire \wr_data_reg_n_0_[1] ;
  wire \wr_data_reg_n_0_[2] ;
  wire \wr_data_reg_n_0_[3] ;
  wire \wr_data_reg_n_0_[4] ;
  wire \wr_data_reg_n_0_[5] ;
  wire \wr_data_reg_n_0_[6] ;
  wire \wr_data_reg_n_0_[7] ;
  wire [13:0]wr_data_reg_reg;
  wire [6:0]wr_occupancy;
  wire [6:0]wr_occupancy00_out;
  wire wr_occupancy0_carry_n_2;
  wire wr_occupancy0_carry_n_3;
  wire wr_occupancy0_carry_n_4;
  wire wr_occupancy0_carry_n_5;
  wire wr_occupancy0_carry_n_6;
  wire wr_occupancy0_carry_n_7;
  wire wr_rd_addr_gray_0;
  wire wr_rd_addr_gray_2;
  wire wr_rd_addr_gray_3;
  wire wr_rd_addr_gray_4;
  wire wr_rd_addr_gray_5;
  wire wr_rd_addr_gray_6;
  wire NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED;
  wire NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED;
  wire [7:6]NLW_rd_occupancy0_carry_CO_UNCONNECTED;
  wire [7:7]NLW_rd_occupancy0_carry_O_UNCONNECTED;
  wire [7:6]NLW_wr_occupancy0_carry_CO_UNCONNECTED;
  wire [7:7]NLW_wr_occupancy0_carry_O_UNCONNECTED;

  LUT5 #(
    .INIT(32'h55750000)) 
    \__1/ram_reg_64_127_0_6_i_1 
       (.I0(data_in),
        .I1(D),
        .I2(E),
        .I3(remove_idle_reg_reg_0),
        .I4(wr_addr[6]),
        .O(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[0]_i_1 
       (.I0(ram_reg_64_127_0_6_n_0),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_0),
        .O(dpo[0]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[10]_i_1 
       (.I0(ram_reg_64_127_7_13_n_3),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_3),
        .O(dpo[10]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[11]_i_1 
       (.I0(ram_reg_64_127_7_13_n_4),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_4),
        .O(dpo[11]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[12]_i_1 
       (.I0(ram_reg_64_127_7_13_n_5),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_5),
        .O(dpo[12]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[13]_i_1 
       (.I0(ram_reg_64_127_7_13_n_6),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_6),
        .O(dpo[13]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[1]_i_1 
       (.I0(ram_reg_64_127_0_6_n_1),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_1),
        .O(dpo[1]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[2]_i_1 
       (.I0(ram_reg_64_127_0_6_n_2),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_2),
        .O(dpo[2]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[3]_i_1 
       (.I0(ram_reg_64_127_0_6_n_3),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_3),
        .O(dpo[3]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[4]_i_1 
       (.I0(ram_reg_64_127_0_6_n_4),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_4),
        .O(dpo[4]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[5]_i_1 
       (.I0(ram_reg_64_127_0_6_n_5),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_5),
        .O(dpo[5]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[6]_i_1 
       (.I0(ram_reg_64_127_0_6_n_6),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_6),
        .O(dpo[6]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[7]_i_1 
       (.I0(ram_reg_64_127_7_13_n_0),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_0),
        .O(dpo[7]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[8]_i_1 
       (.I0(ram_reg_64_127_7_13_n_1),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_1),
        .O(dpo[8]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[9]_i_1 
       (.I0(ram_reg_64_127_7_13_n_2),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_2),
        .O(dpo[9]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d16p2_wr_pipe[0]_i_1 
       (.I0(\d16p2_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[1] ),
        .I2(\wr_data_reg_n_0_[4] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(d16p2_wr));
  LUT5 #(
    .INIT(32'h00000001)) 
    \d16p2_wr_pipe[0]_i_2 
       (.I0(\wr_data_reg_n_0_[3] ),
        .I1(\wr_data_reg_n_0_[2] ),
        .I2(\wr_data_reg_n_0_[7] ),
        .I3(\wr_data_reg_n_0_[0] ),
        .I4(\wr_data_reg_n_0_[5] ),
        .O(\d16p2_wr_pipe[0]_i_2_n_0 ));
  FDRE \d16p2_wr_pipe_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(d16p2_wr),
        .Q(\d16p2_wr_pipe_reg_n_0_[0] ),
        .R(SR));
  FDRE \d16p2_wr_pipe_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d16p2_wr_pipe_reg_n_0_[0] ),
        .Q(p_4_in9_in),
        .R(SR));
  (* srl_bus_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d21p5_wr_pipe_reg " *) 
  (* srl_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 " *) 
  SRL16E \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(E),
        .CLK(Rx_SysClk),
        .D(d21p5_wr),
        .Q(\d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_i_1 
       (.I0(\k28p5_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[3] ),
        .I2(\wr_data_reg_n_0_[0] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[5] ),
        .O(d21p5_wr));
  FDRE \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ),
        .R(1'b0));
  FDRE \d21p5_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d21p5_wr_pipe_reg[3]_0 ),
        .Q(d21p5_wr_pipe),
        .R(SR));
  (* srl_bus_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d2p2_wr_pipe_reg " *) 
  (* srl_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 " *) 
  SRL16E \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(E),
        .CLK(Rx_SysClk),
        .D(d2p2_wr),
        .Q(\d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_i_1 
       (.I0(\d16p2_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[4] ),
        .I2(\wr_data_reg_n_0_[1] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(d2p2_wr));
  FDRE \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ),
        .R(1'b0));
  FDRE \d2p2_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d2p2_wr_pipe_reg[3]_0 ),
        .Q(d2p2_wr_pipe),
        .R(SR));
  LUT6 #(
    .INIT(64'h4444444454555555)) 
    even_i_1
       (.I0(even),
        .I1(even_i_2_n_0),
        .I2(insert_idle_i_6_n_0),
        .I3(insert_idle_i_4_n_0),
        .I4(insert_idle_i_3_n_0),
        .I5(insert_idle_i_2_n_0),
        .O(even_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    even_i_2
       (.I0(even_i_3_n_0),
        .I1(insert_idle_i_11_n_0),
        .I2(insert_idle_i_10_n_0),
        .I3(\rd_data_reg_reg_n_0_[1] ),
        .I4(\rd_data_reg_reg_n_0_[2] ),
        .I5(\rd_data_reg_n_0_[11] ),
        .O(even_i_2_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    even_i_3
       (.I0(rd_occupancy[1]),
        .I1(rd_occupancy[0]),
        .I2(rd_occupancy[3]),
        .I3(rd_occupancy[2]),
        .I4(rd_occupancy[4]),
        .I5(rd_occupancy[5]),
        .O(even_i_3_n_0));
  FDSE even_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(even_i_1_n_0),
        .Q(even),
        .S(reset_modified));
  LUT1 #(
    .INIT(2'h1)) 
    \initialize_counter[0]_i_1 
       (.I0(initialize_counter_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \initialize_counter[1]_i_1 
       (.I0(initialize_counter_reg[0]),
        .I1(initialize_counter_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \initialize_counter[2]_i_1 
       (.I0(initialize_counter_reg[2]),
        .I1(initialize_counter_reg[1]),
        .I2(initialize_counter_reg[0]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \initialize_counter[3]_i_1 
       (.I0(initialize_counter_reg[3]),
        .I1(initialize_counter_reg[0]),
        .I2(initialize_counter_reg[1]),
        .I3(initialize_counter_reg[2]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \initialize_counter[4]_i_1 
       (.I0(initialize_counter_reg[4]),
        .I1(initialize_counter_reg[2]),
        .I2(initialize_counter_reg[1]),
        .I3(initialize_counter_reg[0]),
        .I4(initialize_counter_reg[3]),
        .O(p_0_in__0[4]));
  LUT2 #(
    .INIT(4'h2)) 
    \initialize_counter[5]_i_1 
       (.I0(initialize_ram),
        .I1(initialize_ram_complete_i_3_n_0),
        .O(initialize_counter0));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initialize_counter[5]_i_2 
       (.I0(initialize_counter_reg[5]),
        .I1(initialize_counter_reg[3]),
        .I2(initialize_counter_reg[0]),
        .I3(initialize_counter_reg[1]),
        .I4(initialize_counter_reg[2]),
        .I5(initialize_counter_reg[4]),
        .O(p_0_in__0[5]));
  FDRE \initialize_counter_reg[0] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[0]),
        .Q(initialize_counter_reg[0]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[1] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[1]),
        .Q(initialize_counter_reg[1]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[2] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[2]),
        .Q(initialize_counter_reg[2]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[3] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[3]),
        .Q(initialize_counter_reg[3]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[4] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[4]),
        .Q(initialize_counter_reg[4]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[5] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[5]),
        .Q(initialize_counter_reg[5]),
        .R(initialize_ram0));
  LUT3 #(
    .INIT(8'hFE)) 
    initialize_ram_complete_i_1
       (.I0(start),
        .I1(reset_out),
        .I2(\initialize_counter_reg[5]_0 ),
        .O(initialize_ram0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT2 #(
    .INIT(4'hE)) 
    initialize_ram_complete_i_2
       (.I0(initialize_ram_complete_i_3_n_0),
        .I1(data_in),
        .O(initialize_ram_complete_i_2_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    initialize_ram_complete_i_3
       (.I0(initialize_counter_reg[5]),
        .I1(initialize_counter_reg[3]),
        .I2(initialize_counter_reg[0]),
        .I3(initialize_counter_reg[1]),
        .I4(initialize_counter_reg[2]),
        .I5(initialize_counter_reg[4]),
        .O(initialize_ram_complete_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_complete_pulse_i_1
       (.I0(data_in),
        .I1(initialize_ram_complete_reg__0),
        .O(initialize_ram_complete_pulse0));
  FDRE initialize_ram_complete_pulse_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_complete_pulse0),
        .Q(initialize_ram_complete_pulse_reg_0),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_complete_i_2_n_0),
        .Q(data_in),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_reg_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_in),
        .Q(initialize_ram_complete_reg__0),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_sync_reg1_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_out),
        .Q(initialize_ram_complete_sync_reg1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    initialize_ram_complete_sync_ris_edg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(initialize_ram_complete_sync_ris_edg0),
        .Q(initialize_ram_complete_sync_ris_edg),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_i_1
       (.I0(initialize_ram),
        .I1(data_in),
        .O(initialize_ram_i_1_n_0));
  FDSE initialize_ram_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_i_1_n_0),
        .Q(initialize_ram),
        .S(initialize_ram0));
  LUT6 #(
    .INIT(64'h00AA00EA000000EA)) 
    insert_idle_i_1
       (.I0(insert_idle_i_2_n_0),
        .I1(insert_idle_i_3_n_0),
        .I2(insert_idle_i_4_n_0),
        .I3(insert_idle_i_5_n_0),
        .I4(insert_idle_i_6_n_0),
        .I5(insert_idle_i_7_n_0),
        .O(even0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    insert_idle_i_10
       (.I0(\rd_data_reg_reg_n_0_[3] ),
        .I1(\rd_data_reg_reg_n_0_[4] ),
        .I2(p_1_in3_in),
        .I3(\rd_data_reg_reg_n_0_[6] ),
        .O(insert_idle_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    insert_idle_i_11
       (.I0(\rd_data_reg_reg_n_0_[7] ),
        .I1(\rd_data_reg_reg_n_0_[5] ),
        .I2(\rd_data_reg_reg_n_0_[0] ),
        .I3(insert_idle),
        .O(insert_idle_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    insert_idle_i_12
       (.I0(\rd_data_reg_n_0_[7] ),
        .I1(\rd_data_reg_n_0_[3] ),
        .I2(\rd_data_reg_n_0_[6] ),
        .I3(\rd_data_reg_n_0_[1] ),
        .O(insert_idle_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    insert_idle_i_2
       (.I0(insert_idle_i_8_n_0),
        .I1(\rd_data_reg_n_0_[6] ),
        .I2(\rd_data_reg_n_0_[1] ),
        .I3(\rd_data_reg_n_0_[4] ),
        .I4(rd_occupancy[6]),
        .I5(\rd_data_reg_n_0_[7] ),
        .O(insert_idle_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00200000)) 
    insert_idle_i_3
       (.I0(\rd_data_reg_n_0_[1] ),
        .I1(\rd_data_reg_n_0_[7] ),
        .I2(\rd_data_reg_n_0_[6] ),
        .I3(\rd_data_reg_n_0_[4] ),
        .I4(insert_idle_i_8_n_0),
        .I5(insert_idle_i_9_n_0),
        .O(insert_idle_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'h01)) 
    insert_idle_i_4
       (.I0(rd_occupancy[4]),
        .I1(rd_occupancy[5]),
        .I2(rd_occupancy[6]),
        .O(insert_idle_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    insert_idle_i_5
       (.I0(\rd_data_reg_n_0_[11] ),
        .I1(\rd_data_reg_reg_n_0_[2] ),
        .I2(\rd_data_reg_reg_n_0_[1] ),
        .I3(insert_idle_i_10_n_0),
        .I4(insert_idle_i_11_n_0),
        .O(insert_idle_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    insert_idle_i_6
       (.I0(rd_occupancy[2]),
        .I1(rd_occupancy[3]),
        .I2(rd_occupancy[0]),
        .I3(rd_occupancy[1]),
        .O(insert_idle_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT2 #(
    .INIT(4'h7)) 
    insert_idle_i_7
       (.I0(rd_occupancy[5]),
        .I1(rd_occupancy[4]),
        .O(insert_idle_i_7_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    insert_idle_i_8
       (.I0(\rd_data_reg_n_0_[0] ),
        .I1(\rd_data_reg_n_0_[2] ),
        .I2(\rd_data_reg_n_0_[5] ),
        .I3(\rd_data_reg_n_0_[3] ),
        .O(insert_idle_i_8_n_0));
  LUT5 #(
    .INIT(32'h00008000)) 
    insert_idle_i_9
       (.I0(\rd_data_reg_n_0_[4] ),
        .I1(\rd_data_reg_n_0_[5] ),
        .I2(\rd_data_reg_n_0_[2] ),
        .I3(\rd_data_reg_n_0_[0] ),
        .I4(insert_idle_i_12_n_0),
        .O(insert_idle_i_9_n_0));
  FDRE insert_idle_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(even0),
        .Q(insert_idle),
        .R(reset_modified));
  FDRE insert_idle_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(insert_idle),
        .Q(insert_idle_reg__0),
        .R(reset_modified));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'h00800000)) 
    \k28p5_wr_pipe[0]_i_1 
       (.I0(\k28p5_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[3] ),
        .I2(\wr_data_reg_n_0_[5] ),
        .I3(\wr_data_reg_n_0_[0] ),
        .I4(p_0_in),
        .O(p_3_out));
  LUT5 #(
    .INIT(32'h00000800)) 
    \k28p5_wr_pipe[0]_i_2 
       (.I0(\wr_data_reg_n_0_[4] ),
        .I1(\wr_data_reg_n_0_[2] ),
        .I2(\wr_data_reg_n_0_[1] ),
        .I3(\wr_data_reg_n_0_[7] ),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(\k28p5_wr_pipe[0]_i_2_n_0 ));
  FDRE \k28p5_wr_pipe_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_3_out),
        .Q(\k28p5_wr_pipe_reg_n_0_[0] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[0] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[1] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[1] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[2] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[2] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[3] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[3] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[4] ),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_0_63_0_6" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 ram_reg_0_63_0_6
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[0]),
        .DIB(wr_data_reg_reg[1]),
        .DIC(wr_data_reg_reg[2]),
        .DID(wr_data_reg_reg[3]),
        .DIE(wr_data_reg_reg[4]),
        .DIF(wr_data_reg_reg[5]),
        .DIG(wr_data_reg_reg[6]),
        .DIH(1'b0),
        .DOA(ram_reg_0_63_0_6_n_0),
        .DOB(ram_reg_0_63_0_6_n_1),
        .DOC(ram_reg_0_63_0_6_n_2),
        .DOD(ram_reg_0_63_0_6_n_3),
        .DOE(ram_reg_0_63_0_6_n_4),
        .DOF(ram_reg_0_63_0_6_n_5),
        .DOG(ram_reg_0_63_0_6_n_6),
        .DOH(NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(ram_reg_0_63_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00005575)) 
    ram_reg_0_63_0_6_i_1
       (.I0(data_in),
        .I1(D),
        .I2(E),
        .I3(remove_idle_reg_reg_0),
        .I4(wr_addr[6]),
        .O(ram_reg_0_63_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_0_63_7_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "13" *) 
  RAM64M8 ram_reg_0_63_7_13
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[7]),
        .DIB(wr_data_reg_reg[8]),
        .DIC(wr_data_reg_reg[9]),
        .DID(wr_data_reg_reg[10]),
        .DIE(wr_data_reg_reg[11]),
        .DIF(wr_data_reg_reg[12]),
        .DIG(wr_data_reg_reg[13]),
        .DIH(1'b0),
        .DOA(ram_reg_0_63_7_13_n_0),
        .DOB(ram_reg_0_63_7_13_n_1),
        .DOC(ram_reg_0_63_7_13_n_2),
        .DOD(ram_reg_0_63_7_13_n_3),
        .DOE(ram_reg_0_63_7_13_n_4),
        .DOF(ram_reg_0_63_7_13_n_5),
        .DOG(ram_reg_0_63_7_13_n_6),
        .DOH(NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(ram_reg_0_63_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_64_127_0_6" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "64" *) 
  (* ram_addr_end = "127" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 ram_reg_64_127_0_6
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[0]),
        .DIB(wr_data_reg_reg[1]),
        .DIC(wr_data_reg_reg[2]),
        .DID(wr_data_reg_reg[3]),
        .DIE(wr_data_reg_reg[4]),
        .DIF(wr_data_reg_reg[5]),
        .DIG(wr_data_reg_reg[6]),
        .DIH(1'b0),
        .DOA(ram_reg_64_127_0_6_n_0),
        .DOB(ram_reg_64_127_0_6_n_1),
        .DOC(ram_reg_64_127_0_6_n_2),
        .DOD(ram_reg_64_127_0_6_n_3),
        .DOE(ram_reg_64_127_0_6_n_4),
        .DOF(ram_reg_64_127_0_6_n_5),
        .DOG(ram_reg_64_127_0_6_n_6),
        .DOH(NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_64_127_7_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "64" *) 
  (* ram_addr_end = "127" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "13" *) 
  RAM64M8 ram_reg_64_127_7_13
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[7]),
        .DIB(wr_data_reg_reg[8]),
        .DIC(wr_data_reg_reg[9]),
        .DID(wr_data_reg_reg[10]),
        .DIE(wr_data_reg_reg[11]),
        .DIF(wr_data_reg_reg[12]),
        .DIG(wr_data_reg_reg[13]),
        .DIH(1'b0),
        .DOA(ram_reg_64_127_7_13_n_0),
        .DOB(ram_reg_64_127_7_13_n_1),
        .DOC(ram_reg_64_127_7_13_n_2),
        .DOD(ram_reg_64_127_7_13_n_3),
        .DOE(ram_reg_64_127_7_13_n_4),
        .DOF(ram_reg_64_127_7_13_n_5),
        .DOG(ram_reg_64_127_7_13_n_6),
        .DOH(NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \rd_addr[6]_i_1 
       (.I0(insert_idle_reg__0),
        .I1(insert_idle),
        .O(rd_enable));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[0]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in),
        .O(\rd_addr_gray[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[1]_i_1 
       (.I0(p_1_in),
        .I1(p_2_in20_in),
        .O(\rd_addr_gray[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[2]_i_1 
       (.I0(p_2_in20_in),
        .I1(p_3_in),
        .O(\rd_addr_gray[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[3]_i_1 
       (.I0(p_3_in),
        .I1(p_4_in23_in),
        .O(\rd_addr_gray[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[4]_i_1 
       (.I0(p_4_in23_in),
        .I1(p_5_in),
        .O(\rd_addr_gray[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[5]_i_1 
       (.I0(p_5_in),
        .I1(\rd_addr_plus2_reg_n_0_[6] ),
        .O(\rd_addr_gray[5]_i_1_n_0 ));
  FDRE \rd_addr_gray_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[0]_i_1_n_0 ),
        .Q(rd_addr_gray[0]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[1]_i_1_n_0 ),
        .Q(rd_addr_gray[1]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[2]_i_1_n_0 ),
        .Q(rd_addr_gray[2]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[3]_i_1_n_0 ),
        .Q(rd_addr_gray[3]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[4]_i_1_n_0 ),
        .Q(rd_addr_gray[4]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[5]_i_1_n_0 ),
        .Q(rd_addr_gray[5]),
        .R(reset_modified));
  FDSE \rd_addr_plus1_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_plus2_reg_n_0_[0] ),
        .Q(rd_addr_plus1[0]),
        .S(reset_modified));
  FDRE \rd_addr_plus1_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_1_in),
        .Q(rd_addr_plus1[1]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_2_in20_in),
        .Q(rd_addr_plus1[2]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_3_in),
        .Q(rd_addr_plus1[3]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_4_in23_in),
        .Q(rd_addr_plus1[4]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_5_in),
        .Q(rd_addr_plus1[5]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_plus2_reg_n_0_[6] ),
        .Q(rd_addr_plus1[6]),
        .R(reset_modified));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \rd_addr_plus2[0]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \rd_addr_plus2[2]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in),
        .I2(p_2_in20_in),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \rd_addr_plus2[3]_i_1 
       (.I0(p_3_in),
        .I1(\rd_addr_plus2_reg_n_0_[0] ),
        .I2(p_1_in),
        .I3(p_2_in20_in),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \rd_addr_plus2[4]_i_1 
       (.I0(p_4_in23_in),
        .I1(p_2_in20_in),
        .I2(p_1_in),
        .I3(\rd_addr_plus2_reg_n_0_[0] ),
        .I4(p_3_in),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \rd_addr_plus2[5]_i_1 
       (.I0(p_5_in),
        .I1(p_3_in),
        .I2(\rd_addr_plus2_reg_n_0_[0] ),
        .I3(p_1_in),
        .I4(p_2_in20_in),
        .I5(p_4_in23_in),
        .O(p_0_in__1[5]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \rd_addr_plus2[6]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[6] ),
        .I1(p_4_in23_in),
        .I2(\rd_addr_plus2[6]_i_2_n_0 ),
        .I3(p_3_in),
        .I4(p_5_in),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \rd_addr_plus2[6]_i_2 
       (.I0(p_2_in20_in),
        .I1(p_1_in),
        .I2(\rd_addr_plus2_reg_n_0_[0] ),
        .O(\rd_addr_plus2[6]_i_2_n_0 ));
  FDRE \rd_addr_plus2_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[0]),
        .Q(\rd_addr_plus2_reg_n_0_[0] ),
        .R(reset_modified));
  FDSE \rd_addr_plus2_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[0]_i_1_n_0 ),
        .Q(p_1_in),
        .S(reset_modified));
  FDRE \rd_addr_plus2_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[2]),
        .Q(p_2_in20_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[3]),
        .Q(p_3_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[4]),
        .Q(p_4_in23_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[5]),
        .Q(p_5_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[6]),
        .Q(\rd_addr_plus2_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_addr_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[0]),
        .Q(rd_addr[0]),
        .R(reset_modified));
  FDRE \rd_addr_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[1]),
        .Q(rd_addr[1]),
        .R(reset_modified));
  FDRE \rd_addr_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[2]),
        .Q(rd_addr[2]),
        .R(reset_modified));
  FDRE \rd_addr_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[3]),
        .Q(rd_addr[3]),
        .R(reset_modified));
  FDRE \rd_addr_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[4]),
        .Q(rd_addr[4]),
        .R(reset_modified));
  FDRE \rd_addr_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[5]),
        .Q(rd_addr[5]),
        .R(reset_modified));
  FDRE \rd_addr_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[6]),
        .Q(rd_addr[6]),
        .R(reset_modified));
  FDRE \rd_data_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[0]),
        .Q(\rd_data_reg_n_0_[0] ),
        .R(reset_modified));
  FDRE \rd_data_reg[10] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[10]),
        .Q(\rd_data_reg_n_0_[10] ),
        .R(reset_modified));
  FDRE \rd_data_reg[11] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[11]),
        .Q(\rd_data_reg_n_0_[11] ),
        .R(reset_modified));
  FDRE \rd_data_reg[12] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[12]),
        .Q(\rd_data_reg_n_0_[12] ),
        .R(reset_modified));
  FDRE \rd_data_reg[13] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[13]),
        .Q(\rd_data_reg_n_0_[13] ),
        .R(reset_modified));
  FDRE \rd_data_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[1]),
        .Q(\rd_data_reg_n_0_[1] ),
        .R(reset_modified));
  FDRE \rd_data_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[2]),
        .Q(\rd_data_reg_n_0_[2] ),
        .R(reset_modified));
  FDRE \rd_data_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[3]),
        .Q(\rd_data_reg_n_0_[3] ),
        .R(reset_modified));
  FDRE \rd_data_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[4]),
        .Q(\rd_data_reg_n_0_[4] ),
        .R(reset_modified));
  FDRE \rd_data_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[5]),
        .Q(\rd_data_reg_n_0_[5] ),
        .R(reset_modified));
  FDRE \rd_data_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[6]),
        .Q(\rd_data_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_data_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[7]),
        .Q(\rd_data_reg_n_0_[7] ),
        .R(reset_modified));
  FDRE \rd_data_reg[8] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[8]),
        .Q(p_2_in),
        .R(reset_modified));
  FDRE \rd_data_reg[9] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[9]),
        .Q(\rd_data_reg_n_0_[9] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[0] ),
        .Q(\rd_data_reg_reg_n_0_[0] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[10] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[10] ),
        .Q(\rd_data_reg_reg_n_0_[10] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[11] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[11] ),
        .Q(p_1_in3_in),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[12] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[12] ),
        .Q(\rd_data_reg_reg_n_0_[12] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[13] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[13] ),
        .Q(\rd_data_reg_reg[13]_0 ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[1] ),
        .Q(\rd_data_reg_reg_n_0_[1] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[2] ),
        .Q(\rd_data_reg_reg_n_0_[2] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[3] ),
        .Q(\rd_data_reg_reg_n_0_[3] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[4] ),
        .Q(\rd_data_reg_reg_n_0_[4] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[5] ),
        .Q(\rd_data_reg_reg_n_0_[5] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[6] ),
        .Q(\rd_data_reg_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[7] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[7] ),
        .Q(\rd_data_reg_reg_n_0_[7] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[8] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(p_2_in),
        .Q(\rd_data_reg_reg_n_0_[8] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[9] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[9] ),
        .Q(\rd_data_reg_reg_n_0_[9] ),
        .R(reset_modified));
  FDSE rd_enable_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_enable),
        .Q(rd_enable_reg),
        .S(reset_modified));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 rd_occupancy0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_rd_occupancy0_carry_CO_UNCONNECTED[7:6],rd_occupancy0_carry_n_2,rd_occupancy0_carry_n_3,rd_occupancy0_carry_n_4,rd_occupancy0_carry_n_5,rd_occupancy0_carry_n_6,rd_occupancy0_carry_n_7}),
        .DI({1'b0,1'b0,rd_wr_addr}),
        .O({NLW_rd_occupancy0_carry_O_UNCONNECTED[7],rd_occupancy01_out}),
        .S({1'b0,\reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[6].sync_wr_addrgray_n_1 ,\reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_1 }));
  LUT2 #(
    .INIT(4'h6)) 
    rd_occupancy0_carry_i_1
       (.I0(rd_wr_addr_gray_5),
        .I1(rd_wr_addr_gray_6),
        .O(rd_wr_addr[5]));
  LUT3 #(
    .INIT(8'h96)) 
    rd_occupancy0_carry_i_2
       (.I0(rd_wr_addr_gray_4),
        .I1(rd_wr_addr_gray_6),
        .I2(rd_wr_addr_gray_5),
        .O(rd_wr_addr[4]));
  LUT4 #(
    .INIT(16'h6996)) 
    rd_occupancy0_carry_i_3
       (.I0(rd_wr_addr_gray_3),
        .I1(rd_wr_addr_gray_5),
        .I2(rd_wr_addr_gray_6),
        .I3(rd_wr_addr_gray_4),
        .O(rd_wr_addr[3]));
  LUT5 #(
    .INIT(32'h96696996)) 
    rd_occupancy0_carry_i_4
       (.I0(rd_wr_addr_gray_2),
        .I1(rd_wr_addr_gray_4),
        .I2(rd_wr_addr_gray_6),
        .I3(rd_wr_addr_gray_5),
        .I4(rd_wr_addr_gray_3),
        .O(rd_wr_addr[2]));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    rd_occupancy0_carry_i_5
       (.I0(rd_wr_addr_gray_1),
        .I1(rd_wr_addr_gray_3),
        .I2(rd_wr_addr_gray_5),
        .I3(rd_wr_addr_gray_6),
        .I4(rd_wr_addr_gray_4),
        .I5(rd_wr_addr_gray_2),
        .O(rd_wr_addr[1]));
  LUT2 #(
    .INIT(4'h6)) 
    rd_occupancy0_carry_i_6
       (.I0(rd_wr_addr_gray_0),
        .I1(rd_wr_addr[1]),
        .O(rd_wr_addr[0]));
  FDRE \rd_occupancy_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[0]),
        .Q(rd_occupancy[0]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[1]),
        .Q(rd_occupancy[1]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[2]),
        .Q(rd_occupancy[2]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[3]),
        .Q(rd_occupancy[3]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[4]),
        .Q(rd_occupancy[4]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[5]),
        .Q(rd_occupancy[5]),
        .R(reset_modified));
  FDSE \rd_occupancy_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[6]),
        .Q(rd_occupancy[6]),
        .S(reset_modified));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_7 \reclock_rd_addrgray[0].sync_rd_addrgray 
       (.Q(rd_addr_gray[0]),
        .Rx_SysClk(Rx_SysClk),
        .data_out(wr_rd_addr_gray_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_8 \reclock_rd_addrgray[1].sync_rd_addrgray 
       (.Q(wr_addr[1:0]),
        .Rx_SysClk(Rx_SysClk),
        .S({\reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_1 }),
        .data_out(wr_rd_addr_gray_3),
        .data_sync_reg1_0(rd_addr_gray[1]),
        .wr_occupancy0_carry_i_7_0(wr_rd_addr_gray_5),
        .wr_occupancy0_carry_i_7_1(wr_rd_addr_gray_6),
        .wr_occupancy0_carry_i_7_2(wr_rd_addr_gray_4),
        .wr_occupancy0_carry_i_7_3(wr_rd_addr_gray_2),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_9 \reclock_rd_addrgray[2].sync_rd_addrgray 
       (.Q(rd_addr_gray[2]),
        .Rx_SysClk(Rx_SysClk),
        .data_out(wr_rd_addr_gray_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_10 \reclock_rd_addrgray[3].sync_rd_addrgray 
       (.Q(wr_addr[2]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_3),
        .data_sync_reg1_0(rd_addr_gray[3]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_5),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_1 (wr_rd_addr_gray_4),
        .\wr_occupancy_reg[6]_2 (wr_rd_addr_gray_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_11 \reclock_rd_addrgray[4].sync_rd_addrgray 
       (.Q(wr_addr[3]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_4),
        .data_sync_reg1_0(rd_addr_gray[4]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_5),
        .\wr_occupancy_reg[6]_1 (wr_rd_addr_gray_3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_12 \reclock_rd_addrgray[5].sync_rd_addrgray 
       (.Q(wr_addr[4]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_5),
        .data_sync_reg1_0(rd_addr_gray[5]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_13 \reclock_rd_addrgray[6].sync_rd_addrgray 
       (.Q(wr_addr[6:5]),
        .Rx_SysClk(Rx_SysClk),
        .S({\reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[6].sync_rd_addrgray_n_1 }),
        .data_in(rd_addr_plus1[6]),
        .data_out(wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_5));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_14 \reclock_wr_addrgray[0].sync_wr_addrgray 
       (.Q(wr_addr_gray[0]),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_15 \reclock_wr_addrgray[1].sync_wr_addrgray 
       (.Q(rd_addr[1:0]),
        .S({\reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_1 }),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_0),
        .data_sync_reg1_0(wr_addr_gray[1]),
        .data_sync_reg6_0(rd_wr_addr_gray_1),
        .rd_wr_addr(rd_wr_addr[1]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_16 \reclock_wr_addrgray[2].sync_wr_addrgray 
       (.Q(wr_addr_gray[2]),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_17 \reclock_wr_addrgray[3].sync_wr_addrgray 
       (.Q(rd_addr[2]),
        .S(\reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_3),
        .data_sync_reg1_0(wr_addr_gray[3]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_5),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_1 (rd_wr_addr_gray_4),
        .\rd_occupancy_reg[6]_2 (rd_wr_addr_gray_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_18 \reclock_wr_addrgray[4].sync_wr_addrgray 
       (.Q(rd_addr[3]),
        .S(\reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_4),
        .data_sync_reg1_0(wr_addr_gray[4]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_5),
        .\rd_occupancy_reg[6]_1 (rd_wr_addr_gray_3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_19 \reclock_wr_addrgray[5].sync_wr_addrgray 
       (.Q(rd_addr[4]),
        .S(\reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_5),
        .data_sync_reg1_0(wr_addr_gray[5]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_20 \reclock_wr_addrgray[6].sync_wr_addrgray 
       (.Q(rd_addr[6:5]),
        .S({\reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[6].sync_wr_addrgray_n_1 }),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_6),
        .data_sync_reg1_0(wr_addr_gray[6]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_5));
  LUT5 #(
    .INIT(32'h00E00000)) 
    remove_idle_i_1
       (.I0(remove_idle_i_2_n_0),
        .I1(remove_idle_i_3_n_0),
        .I2(\k28p5_wr_pipe_reg_n_0_[0] ),
        .I3(D),
        .I4(wr_occupancy[6]),
        .O(remove_idle0));
  LUT6 #(
    .INIT(64'h8080808080800080)) 
    remove_idle_i_2
       (.I0(d16p2_wr),
        .I1(p_4_in9_in),
        .I2(\k28p5_wr_pipe_reg_n_0_[2] ),
        .I3(remove_idle_i_4_n_0),
        .I4(wr_occupancy[5]),
        .I5(wr_occupancy[1]),
        .O(remove_idle_i_2_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0E000E)) 
    remove_idle_i_3
       (.I0(d21p5_wr),
        .I1(d2p2_wr),
        .I2(remove_idle_i_5_n_0),
        .I3(remove_idle_i_4_n_0),
        .I4(wr_occupancy[1]),
        .I5(wr_occupancy[0]),
        .O(remove_idle_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    remove_idle_i_4
       (.I0(wr_occupancy[2]),
        .I1(wr_occupancy[4]),
        .I2(wr_occupancy[3]),
        .O(remove_idle_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    remove_idle_i_5
       (.I0(remove_idle_reg3),
        .I1(wr_occupancy[5]),
        .I2(remove_idle_reg4),
        .I3(\k28p5_wr_pipe_reg_n_0_[4] ),
        .I4(remove_idle_i_6_n_0),
        .O(remove_idle_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFF1)) 
    remove_idle_i_6
       (.I0(d21p5_wr_pipe),
        .I1(d2p2_wr_pipe),
        .I2(remove_idle_reg_reg_0),
        .I3(remove_idle_reg2),
        .O(remove_idle_i_6_n_0));
  FDRE remove_idle_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle0),
        .Q(D),
        .R(SR));
  FDRE remove_idle_reg2_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg_reg_0),
        .Q(remove_idle_reg2),
        .R(SR));
  FDRE remove_idle_reg3_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg2),
        .Q(remove_idle_reg3),
        .R(SR));
  FDRE remove_idle_reg4_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg3),
        .Q(remove_idle_reg4),
        .R(SR));
  FDRE remove_idle_reg_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(D),
        .Q(remove_idle_reg_reg_0),
        .R(SR));
  LUT5 #(
    .INIT(32'h77777774)) 
    reset_modified_i_1
       (.I0(initialize_ram_complete_sync_ris_edg),
        .I1(reset_modified),
        .I2(reset_modified_reg_0),
        .I3(mgt_rx_reset),
        .I4(elastic_buffer_rst_125),
        .O(reset_modified_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_modified_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_modified_i_1_n_0),
        .Q(reset_modified),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAAAAAB)) 
    rxbuferr_i_1
       (.I0(rxbuferr_i_2_n_0),
        .I1(rd_occupancy[6]),
        .I2(rd_occupancy[5]),
        .I3(rd_occupancy[4]),
        .I4(rxbuferr_i_3_n_0),
        .I5(rxbufstatus),
        .O(rxbuferr_i_1_n_0));
  LUT6 #(
    .INIT(64'h4040400000000000)) 
    rxbuferr_i_2
       (.I0(insert_idle_i_7_n_0),
        .I1(rd_occupancy[3]),
        .I2(rd_occupancy[2]),
        .I3(rd_occupancy[1]),
        .I4(rd_occupancy[0]),
        .I5(rd_occupancy[6]),
        .O(rxbuferr_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'hFFF8)) 
    rxbuferr_i_3
       (.I0(rd_occupancy[0]),
        .I1(rd_occupancy[1]),
        .I2(rd_occupancy[2]),
        .I3(rd_occupancy[3]),
        .O(rxbuferr_i_3_n_0));
  FDRE rxbuferr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxbuferr_i_1_n_0),
        .Q(rxbufstatus),
        .R(reset_modified));
  LUT3 #(
    .INIT(8'hAB)) 
    rxchariscomma_usr_i_1
       (.I0(reset_modified),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxchariscomma_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    rxchariscomma_usr_i_2
       (.I0(\rd_data_reg_reg_n_0_[12] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxchariscomma_usr_i_2_n_0));
  FDRE rxchariscomma_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxchariscomma_usr_i_2_n_0),
        .Q(rxchariscomma_usr_reg_0),
        .R(rxchariscomma_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    rxcharisk_usr_i_1
       (.I0(p_1_in3_in),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxcharisk_usr_i_1_n_0));
  FDRE rxcharisk_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxcharisk_usr_i_1_n_0),
        .Q(rxcharisk_usr_reg_0),
        .R(rxchariscomma_usr_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000440C)) 
    \rxclkcorcnt[2]_i_1 
       (.I0(rxclkcorcnt[1]),
        .I1(insert_idle_reg__0),
        .I2(\rd_data_reg_reg[13]_0 ),
        .I3(rxclkcorcnt[0]),
        .I4(reset_modified),
        .O(\rxclkcorcnt[2]_i_1_n_0 ));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxclkcorcnt_reg[0]_0 ),
        .Q(rxclkcorcnt[0]),
        .R(reset_modified));
  FDRE \rxclkcorcnt_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxclkcorcnt[2]_i_1_n_0 ),
        .Q(rxclkcorcnt[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rxdata_usr[0]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[0] ),
        .I1(rd_enable_reg),
        .O(\rxdata_usr[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \rxdata_usr[1]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[1] ),
        .I1(rd_enable_reg),
        .O(\rxdata_usr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[2]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[2] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[3]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[3] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'hD)) 
    \rxdata_usr[4]_i_1 
       (.I0(rd_enable_reg),
        .I1(\rd_data_reg_reg_n_0_[4] ),
        .O(\rxdata_usr[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[5]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[5] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hC5)) 
    \rxdata_usr[6]_i_1 
       (.I0(even),
        .I1(\rd_data_reg_reg_n_0_[6] ),
        .I2(rd_enable_reg),
        .O(\rxdata_usr[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[7]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[7] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[7]_i_1_n_0 ));
  FDRE \rxdata_usr_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[0]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [0]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[1]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [1]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[2]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [2]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[3]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [3]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[4]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [4]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[5]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [5]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[6]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [6]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[7]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [7]),
        .R(reset_modified));
  LUT2 #(
    .INIT(4'hB)) 
    rxdisperr_usr_i_1
       (.I0(reset_modified),
        .I1(rd_enable_reg),
        .O(rxdisperr_usr_i_1_n_0));
  FDRE rxdisperr_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rd_data_reg_reg_n_0_[10] ),
        .Q(rxdisperr),
        .R(rxdisperr_usr_i_1_n_0));
  FDRE rxnotintable_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rd_data_reg_reg_n_0_[9] ),
        .Q(rxnotintable),
        .R(rxdisperr_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    rxrundisp_usr_i_1
       (.I0(p_2_in),
        .I1(even),
        .I2(rd_enable_reg),
        .I3(\rd_data_reg_reg_n_0_[8] ),
        .O(rxrundisp_usr_i_1_n_0));
  FDRE rxrundisp_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxrundisp_usr_i_1_n_0),
        .Q(rxrundisp),
        .R(reset_modified));
  FDRE #(
    .INIT(1'b1)) 
    start_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .Q(start),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_21 sync_initialize_ram_comp
       (.Tx_WrClk(Tx_WrClk),
        .data_out(data_out),
        .data_sync_reg1_0(data_in));
  LUT3 #(
    .INIT(8'hFE)) 
    \wr_addr[5]_i_1 
       (.I0(initialize_ram_complete_pulse_reg_0),
        .I1(reset_out),
        .I2(\initialize_counter_reg[5]_0 ),
        .O(wr_addr__0));
  LUT4 #(
    .INIT(16'h04FF)) 
    \wr_addr[5]_i_2 
       (.I0(remove_idle_reg_reg_0),
        .I1(E),
        .I2(D),
        .I3(data_in),
        .O(\wr_addr[5]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr[6]_i_3 
       (.I0(wr_addr_plus1[6]),
        .I1(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[1]_i_1 
       (.I0(p_1_in28_in),
        .I1(p_2_in29_in),
        .O(p_6_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[2]_i_1 
       (.I0(p_2_in29_in),
        .I1(p_3_in31_in),
        .O(p_6_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[3]_i_1 
       (.I0(p_3_in31_in),
        .I1(p_4_in33_in),
        .O(p_6_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[4]_i_1 
       (.I0(p_4_in33_in),
        .I1(p_5_in35_in),
        .O(p_6_out[4]));
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[5]_i_1 
       (.I0(p_5_in35_in),
        .I1(\wr_addr_plus2_reg_n_0_[6] ),
        .O(p_6_out[5]));
  FDSE \wr_addr_gray_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_addr_plus2[1]_i_1_n_0 ),
        .Q(wr_addr_gray[0]),
        .S(SR));
  FDRE \wr_addr_gray_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[1]),
        .Q(wr_addr_gray[1]),
        .R(SR));
  FDRE \wr_addr_gray_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[2]),
        .Q(wr_addr_gray[2]),
        .R(SR));
  FDRE \wr_addr_gray_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[3]),
        .Q(wr_addr_gray[3]),
        .R(SR));
  FDRE \wr_addr_gray_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[4]),
        .Q(wr_addr_gray[4]),
        .R(SR));
  FDSE \wr_addr_gray_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[5]),
        .Q(wr_addr_gray[5]),
        .S(SR));
  FDSE \wr_addr_gray_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_addr_plus2_reg_n_0_[6] ),
        .Q(wr_addr_gray[6]),
        .S(SR));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr_plus1[6]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[6] ),
        .I1(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr_plus1[6]_i_1_n_0 ));
  FDSE \wr_addr_plus1_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2_reg_n_0_[0] ),
        .Q(wr_addr_plus1[0]),
        .S(wr_addr__0));
  FDRE \wr_addr_plus1_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_1_in28_in),
        .Q(wr_addr_plus1[1]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_2_in29_in),
        .Q(wr_addr_plus1[2]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_3_in31_in),
        .Q(wr_addr_plus1[3]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_4_in33_in),
        .Q(wr_addr_plus1[4]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_5_in35_in),
        .Q(wr_addr_plus1[5]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr_plus1[6]_i_1_n_0 ),
        .Q(wr_addr_plus1[6]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wr_addr_plus2[0]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .O(\wr_addr_plus2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_plus2[1]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in28_in),
        .O(\wr_addr_plus2[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \wr_addr_plus2[2]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in28_in),
        .I2(p_2_in29_in),
        .O(\wr_addr_plus2[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \wr_addr_plus2[3]_i_1 
       (.I0(p_1_in28_in),
        .I1(\wr_addr_plus2_reg_n_0_[0] ),
        .I2(p_2_in29_in),
        .I3(p_3_in31_in),
        .O(\wr_addr_plus2[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \wr_addr_plus2[4]_i_1 
       (.I0(p_2_in29_in),
        .I1(\wr_addr_plus2_reg_n_0_[0] ),
        .I2(p_1_in28_in),
        .I3(p_3_in31_in),
        .I4(p_4_in33_in),
        .O(\wr_addr_plus2[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \wr_addr_plus2[5]_i_1 
       (.I0(p_3_in31_in),
        .I1(p_1_in28_in),
        .I2(\wr_addr_plus2_reg_n_0_[0] ),
        .I3(p_2_in29_in),
        .I4(p_4_in33_in),
        .I5(p_5_in35_in),
        .O(\wr_addr_plus2[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT4 #(
    .INIT(16'hFF6A)) 
    \wr_addr_plus2[6]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[6] ),
        .I1(p_5_in35_in),
        .I2(\wr_addr_plus2[6]_i_2_n_0 ),
        .I3(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr_plus2[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \wr_addr_plus2[6]_i_2 
       (.I0(p_4_in33_in),
        .I1(p_2_in29_in),
        .I2(\wr_addr_plus2_reg_n_0_[0] ),
        .I3(p_1_in28_in),
        .I4(p_3_in31_in),
        .O(\wr_addr_plus2[6]_i_2_n_0 ));
  FDRE \wr_addr_plus2_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[0]_i_1_n_0 ),
        .Q(\wr_addr_plus2_reg_n_0_[0] ),
        .R(wr_addr__0));
  FDSE \wr_addr_plus2_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[1]_i_1_n_0 ),
        .Q(p_1_in28_in),
        .S(wr_addr__0));
  FDRE \wr_addr_plus2_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[2]_i_1_n_0 ),
        .Q(p_2_in29_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[3]_i_1_n_0 ),
        .Q(p_3_in31_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[4]_i_1_n_0 ),
        .Q(p_4_in33_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[5]_i_1_n_0 ),
        .Q(p_5_in35_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr_plus2[6]_i_1_n_0 ),
        .Q(\wr_addr_plus2_reg_n_0_[6] ),
        .R(SR));
  FDRE \wr_addr_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[0]),
        .Q(wr_addr[0]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[1]),
        .Q(wr_addr[1]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[2]),
        .Q(wr_addr[2]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[3]),
        .Q(wr_addr[3]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[4]),
        .Q(wr_addr[4]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[5]),
        .Q(wr_addr[5]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr[6]_i_3_n_0 ),
        .Q(wr_addr[6]),
        .R(SR));
  FDRE \wr_data_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [0]),
        .Q(\wr_data_reg_n_0_[0] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [10]),
        .Q(wr_data[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [11]),
        .Q(p_0_in),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [12]),
        .Q(wr_data[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [1]),
        .Q(\wr_data_reg_n_0_[1] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [2]),
        .Q(\wr_data_reg_n_0_[2] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [3]),
        .Q(\wr_data_reg_n_0_[3] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [4]),
        .Q(\wr_data_reg_n_0_[4] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [5]),
        .Q(\wr_data_reg_n_0_[5] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [6]),
        .Q(\wr_data_reg_n_0_[6] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [7]),
        .Q(\wr_data_reg_n_0_[7] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [8]),
        .Q(wr_data[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [9]),
        .Q(wr_data[9]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[0] ),
        .Q(wr_data_reg[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[10]),
        .Q(wr_data_reg[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_0_in),
        .Q(wr_data_reg[11]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[12]),
        .Q(wr_data_reg[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[13] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(D),
        .Q(wr_data_reg[13]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[1] ),
        .Q(wr_data_reg[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[2] ),
        .Q(wr_data_reg[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[3] ),
        .Q(wr_data_reg[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[4] ),
        .Q(wr_data_reg[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[5] ),
        .Q(wr_data_reg[5]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[6] ),
        .Q(wr_data_reg[6]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[7] ),
        .Q(wr_data_reg[7]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[8]),
        .Q(wr_data_reg[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[9]),
        .Q(wr_data_reg[9]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[0]),
        .Q(wr_data_reg_reg[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[10]),
        .Q(wr_data_reg_reg[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[11]),
        .Q(wr_data_reg_reg[11]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[12]),
        .Q(wr_data_reg_reg[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[13] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[13]),
        .Q(wr_data_reg_reg[13]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[1]),
        .Q(wr_data_reg_reg[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[2]),
        .Q(wr_data_reg_reg[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[3]),
        .Q(wr_data_reg_reg[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[4]),
        .Q(wr_data_reg_reg[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[5]),
        .Q(wr_data_reg_reg[5]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[6]),
        .Q(wr_data_reg_reg[6]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[7]),
        .Q(wr_data_reg_reg[7]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[8]),
        .Q(wr_data_reg_reg[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[9]),
        .Q(wr_data_reg_reg[9]),
        .R(\wr_data_reg[0]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 wr_occupancy0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_wr_occupancy0_carry_CO_UNCONNECTED[7:6],wr_occupancy0_carry_n_2,wr_occupancy0_carry_n_3,wr_occupancy0_carry_n_4,wr_occupancy0_carry_n_5,wr_occupancy0_carry_n_6,wr_occupancy0_carry_n_7}),
        .DI({1'b0,1'b0,wr_addr[5:0]}),
        .O({NLW_wr_occupancy0_carry_O_UNCONNECTED[7],wr_occupancy00_out}),
        .S({1'b0,\reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[6].sync_rd_addrgray_n_1 ,\reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_1 }));
  FDRE \wr_occupancy_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[0]),
        .Q(wr_occupancy[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[1]),
        .Q(wr_occupancy[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[2]),
        .Q(wr_occupancy[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[3]),
        .Q(wr_occupancy[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[4]),
        .Q(wr_occupancy[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[5]),
        .Q(wr_occupancy[5]),
        .R(\wr_data_reg[0]_0 ));
  FDSE \wr_occupancy_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[6]),
        .Q(wr_occupancy[6]),
        .S(\wr_data_reg[0]_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_rate_adapt
   (gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_rxd_0,
    reset_out,
    gmii_rx_er_out_reg_0,
    gmii_rx_dv,
    Tx_WrClk,
    gmii_rx_er_in,
    gmii_rxd);
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output [7:0]gmii_rxd_0;
  input reset_out;
  input gmii_rx_er_out_reg_0;
  input gmii_rx_dv;
  input Tx_WrClk;
  input gmii_rx_er_in;
  input [7:0]gmii_rxd;

  wire Tx_WrClk;
  wire gmii_rx_dv;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire gmii_rx_er_in;
  wire gmii_rx_er_out_reg_0;
  wire [7:0]gmii_rxd;
  wire [7:0]gmii_rxd_0;
  wire muxsel;
  wire muxsel_i_1_n_0;
  wire [3:0]p_0_in;
  wire reset_out;
  wire rx_dv_aligned;
  wire rx_dv_aligned_i_1_n_0;
  wire rx_dv_reg1;
  wire rx_dv_reg2;
  wire rx_er_aligned;
  wire rx_er_aligned_0;
  wire rx_er_reg1;
  wire rx_er_reg2;
  wire [7:0]rxd_aligned;
  wire \rxd_aligned[0]_i_1_n_0 ;
  wire \rxd_aligned[1]_i_1_n_0 ;
  wire \rxd_aligned[2]_i_1_n_0 ;
  wire \rxd_aligned[3]_i_1_n_0 ;
  wire \rxd_aligned[4]_i_1_n_0 ;
  wire \rxd_aligned[5]_i_1_n_0 ;
  wire \rxd_aligned[6]_i_1_n_0 ;
  wire \rxd_aligned[7]_i_1_n_0 ;
  wire \rxd_reg1_reg_n_0_[0] ;
  wire \rxd_reg1_reg_n_0_[1] ;
  wire \rxd_reg1_reg_n_0_[2] ;
  wire \rxd_reg1_reg_n_0_[3] ;
  wire [7:0]rxd_reg2;
  wire sfd_enable;
  wire sfd_enable0;
  wire sfd_enable_i_1_n_0;
  wire sfd_enable_i_2_n_0;
  wire sfd_enable_i_4_n_0;
  wire sfd_enable_i_5_n_0;

  FDRE #(
    .INIT(1'b0)) 
    gmii_rx_dv_out_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_aligned),
        .Q(gmii_rx_dv_0),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    gmii_rx_er_out_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_aligned),
        .Q(gmii_rx_er_0),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[0]),
        .Q(gmii_rxd_0[0]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[1]),
        .Q(gmii_rxd_0[1]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[2]),
        .Q(gmii_rxd_0[2]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[3]),
        .Q(gmii_rxd_0[3]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[4]),
        .Q(gmii_rxd_0[4]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[5]),
        .Q(gmii_rxd_0[5]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[6]),
        .Q(gmii_rxd_0[6]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[7]),
        .Q(gmii_rxd_0[7]),
        .R(reset_out));
  LUT6 #(
    .INIT(64'h00000000CCCCA8CC)) 
    muxsel_i_1
       (.I0(sfd_enable_i_5_n_0),
        .I1(muxsel),
        .I2(sfd_enable_i_2_n_0),
        .I3(sfd_enable),
        .I4(sfd_enable_i_4_n_0),
        .I5(reset_out),
        .O(muxsel_i_1_n_0));
  FDRE muxsel_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(muxsel_i_1_n_0),
        .Q(muxsel),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hB0)) 
    rx_dv_aligned_i_1
       (.I0(rx_dv_reg1),
        .I1(muxsel),
        .I2(rx_dv_reg2),
        .O(rx_dv_aligned_i_1_n_0));
  FDRE rx_dv_aligned_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_aligned_i_1_n_0),
        .Q(rx_dv_aligned),
        .R(reset_out));
  FDRE rx_dv_reg1_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rx_dv),
        .Q(rx_dv_reg1),
        .R(reset_out));
  FDRE rx_dv_reg2_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_reg1),
        .Q(rx_dv_reg2),
        .R(reset_out));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    rx_er_aligned_i_1
       (.I0(muxsel),
        .I1(rx_er_reg1),
        .I2(rx_er_reg2),
        .O(rx_er_aligned_0));
  FDRE rx_er_aligned_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_aligned_0),
        .Q(rx_er_aligned),
        .R(reset_out));
  FDRE rx_er_reg1_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rx_er_in),
        .Q(rx_er_reg1),
        .R(reset_out));
  FDRE rx_er_reg2_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_reg1),
        .Q(rx_er_reg2),
        .R(reset_out));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[0]_i_1 
       (.I0(rxd_reg2[4]),
        .I1(muxsel),
        .I2(rxd_reg2[0]),
        .O(\rxd_aligned[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[1]_i_1 
       (.I0(rxd_reg2[5]),
        .I1(muxsel),
        .I2(rxd_reg2[1]),
        .O(\rxd_aligned[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[2]_i_1 
       (.I0(rxd_reg2[6]),
        .I1(muxsel),
        .I2(rxd_reg2[2]),
        .O(\rxd_aligned[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[3]_i_1 
       (.I0(rxd_reg2[7]),
        .I1(muxsel),
        .I2(rxd_reg2[3]),
        .O(\rxd_aligned[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[4]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[0] ),
        .I1(muxsel),
        .I2(rxd_reg2[4]),
        .O(\rxd_aligned[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[5]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[1] ),
        .I1(muxsel),
        .I2(rxd_reg2[5]),
        .O(\rxd_aligned[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[6]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[2] ),
        .I1(muxsel),
        .I2(rxd_reg2[6]),
        .O(\rxd_aligned[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[7]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[3] ),
        .I1(muxsel),
        .I2(rxd_reg2[7]),
        .O(\rxd_aligned[7]_i_1_n_0 ));
  FDRE \rxd_aligned_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[0]_i_1_n_0 ),
        .Q(rxd_aligned[0]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[1]_i_1_n_0 ),
        .Q(rxd_aligned[1]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[2]_i_1_n_0 ),
        .Q(rxd_aligned[2]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[3]_i_1_n_0 ),
        .Q(rxd_aligned[3]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[4]_i_1_n_0 ),
        .Q(rxd_aligned[4]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[5]_i_1_n_0 ),
        .Q(rxd_aligned[5]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[6]_i_1_n_0 ),
        .Q(rxd_aligned[6]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[7]_i_1_n_0 ),
        .Q(rxd_aligned[7]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[0]),
        .Q(\rxd_reg1_reg_n_0_[0] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[1]),
        .Q(\rxd_reg1_reg_n_0_[1] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[2]),
        .Q(\rxd_reg1_reg_n_0_[2] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[3]),
        .Q(\rxd_reg1_reg_n_0_[3] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[4]),
        .Q(p_0_in[0]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[5]),
        .Q(p_0_in[1]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[6]),
        .Q(p_0_in[2]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[7]),
        .Q(p_0_in[3]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[0] ),
        .Q(rxd_reg2[0]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[1] ),
        .Q(rxd_reg2[1]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[2] ),
        .Q(rxd_reg2[2]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[3] ),
        .Q(rxd_reg2[3]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[0]),
        .Q(rxd_reg2[4]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[1]),
        .Q(rxd_reg2[5]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[2]),
        .Q(rxd_reg2[6]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[3]),
        .Q(rxd_reg2[7]),
        .R(reset_out));
  LUT6 #(
    .INIT(64'hFFDDFFCCC0C8C0CC)) 
    sfd_enable_i_1
       (.I0(sfd_enable_i_2_n_0),
        .I1(sfd_enable0),
        .I2(gmii_rx_er_out_reg_0),
        .I3(sfd_enable_i_4_n_0),
        .I4(sfd_enable_i_5_n_0),
        .I5(sfd_enable),
        .O(sfd_enable_i_1_n_0));
  LUT5 #(
    .INIT(32'h04000000)) 
    sfd_enable_i_2
       (.I0(p_0_in[3]),
        .I1(gmii_rxd[0]),
        .I2(gmii_rxd[1]),
        .I3(gmii_rxd[3]),
        .I4(gmii_rxd[2]),
        .O(sfd_enable_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    sfd_enable_i_3
       (.I0(gmii_rx_dv),
        .I1(rx_dv_reg1),
        .O(sfd_enable0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    sfd_enable_i_4
       (.I0(p_0_in[0]),
        .I1(p_0_in[1]),
        .I2(gmii_rx_er_out_reg_0),
        .I3(p_0_in[2]),
        .O(sfd_enable_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFDFFF)) 
    sfd_enable_i_5
       (.I0(\rxd_reg1_reg_n_0_[0] ),
        .I1(\rxd_reg1_reg_n_0_[3] ),
        .I2(p_0_in[3]),
        .I3(\rxd_reg1_reg_n_0_[2] ),
        .I4(\rxd_reg1_reg_n_0_[1] ),
        .O(sfd_enable_i_5_n_0));
  FDRE sfd_enable_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sfd_enable_i_1_n_0),
        .Q(sfd_enable),
        .R(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_serdes_1_to_10
   (BaseX_Rx_Fifo_Rd_En,
    ActCnt_GE_HalfBT_reg_0,
    SR,
    E,
    ActiveIsSlve_reg_0,
    D,
    Mstr_Load_reg_0,
    WrapToZero,
    monitor_late_reg_0,
    insert5_reg_0,
    insert3_reg_0,
    Q,
    \Mstr_CntValIn_Out_reg[8]_0 ,
    \s_state_reg[4]_0 ,
    \Slve_CntValIn_Out_reg[1]_0 ,
    \s_state_reg[0]_0 ,
    \s_state_reg[5]_0 ,
    \s_state_reg[4]_1 ,
    \s_state_reg[2]_0 ,
    \s_state_reg[4]_2 ,
    \Slve_CntValIn_Out_reg[0]_0 ,
    \s_state_reg[0]_1 ,
    \active_reg[1]_0 ,
    \act_count_reg[0]_0 ,
    \act_count_reg[5]_0 ,
    \act_count_reg[4]_0 ,
    \act_count_reg[3]_0 ,
    \s_state_reg[3]_0 ,
    BaseX_Idly_Load,
    out,
    code_err_i,
    \grdni.run_disp_i_reg ,
    \grdni.run_disp_i_reg_0 ,
    decoded_rxchariscomma0,
    k,
    b3,
    D0,
    Rx_SysClk,
    LossOfSignal_reg_0,
    ActiveIsSlve_reg_1,
    Slve_Load_reg_0,
    Mstr_Load_reg_1,
    WrapToZero_reg_0,
    monitor_late_reg_1,
    insert5_reg_1,
    insert3_reg_1,
    BaseX_Rx_Q_Out,
    \IntReset_dly_reg[0]_0 ,
    \IntRx_BtVal_reg[8]_0 ,
    \grdni.run_disp_i_reg_1 );
  output [0:0]BaseX_Rx_Fifo_Rd_En;
  output ActCnt_GE_HalfBT_reg_0;
  output [0:0]SR;
  output [0:0]E;
  output ActiveIsSlve_reg_0;
  output [0:0]D;
  output [0:0]Mstr_Load_reg_0;
  output WrapToZero;
  output monitor_late_reg_0;
  output insert5_reg_0;
  output insert3_reg_0;
  output [6:0]Q;
  output [8:0]\Mstr_CntValIn_Out_reg[8]_0 ;
  output [4:0]\s_state_reg[4]_0 ;
  output \Slve_CntValIn_Out_reg[1]_0 ;
  output \s_state_reg[0]_0 ;
  output \s_state_reg[5]_0 ;
  output \s_state_reg[4]_1 ;
  output \s_state_reg[2]_0 ;
  output \s_state_reg[4]_2 ;
  output \Slve_CntValIn_Out_reg[0]_0 ;
  output \s_state_reg[0]_1 ;
  output \active_reg[1]_0 ;
  output \act_count_reg[0]_0 ;
  output [0:0]\act_count_reg[5]_0 ;
  output \act_count_reg[4]_0 ;
  output \act_count_reg[3]_0 ;
  output \s_state_reg[3]_0 ;
  output [1:0]BaseX_Idly_Load;
  output [4:0]out;
  output code_err_i;
  output \grdni.run_disp_i_reg ;
  output \grdni.run_disp_i_reg_0 ;
  output decoded_rxchariscomma0;
  output k;
  output [7:5]b3;
  input D0;
  input Rx_SysClk;
  input LossOfSignal_reg_0;
  input ActiveIsSlve_reg_1;
  input Slve_Load_reg_0;
  input Mstr_Load_reg_1;
  input WrapToZero_reg_0;
  input monitor_late_reg_1;
  input insert5_reg_1;
  input insert3_reg_1;
  input [7:0]BaseX_Rx_Q_Out;
  input [0:0]\IntReset_dly_reg[0]_0 ;
  input [5:0]\IntRx_BtVal_reg[8]_0 ;
  input [0:0]\grdni.run_disp_i_reg_1 ;

  wire ActCnt_EQ_BTval;
  wire ActCnt_EQ_BTval_i_2_n_0;
  wire ActCnt_EQ_BTval_i_3_n_0;
  wire ActCnt_EQ_BTval_i_4_n_0;
  wire ActCnt_EQ_BTval_i_5_n_0;
  wire ActCnt_EQ_BTval_i_6_n_0;
  wire ActCnt_EQ_BTval_i_7_n_0;
  wire ActCnt_EQ_BTval_reg_i_1_n_0;
  wire ActCnt_EQ_Zero;
  wire ActCnt_EQ_Zero_i_1_n_0;
  wire ActCnt_EQ_Zero_i_2_n_0;
  wire ActCnt_EQ_Zero_i_3_n_0;
  wire ActCnt_EQ_Zero_i_4_n_0;
  wire ActCnt_EQ_Zero_i_5_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_1_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_2_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_3_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_4_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_5_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_6_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_7_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_8_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_9_n_0;
  wire ActCnt_GE_HalfBT0_carry_n_3;
  wire ActCnt_GE_HalfBT0_carry_n_4;
  wire ActCnt_GE_HalfBT0_carry_n_5;
  wire ActCnt_GE_HalfBT0_carry_n_6;
  wire ActCnt_GE_HalfBT0_carry_n_7;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_4 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_5 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_6 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_7 ;
  wire ActCnt_GE_HalfBT_i_1_n_0;
  wire ActCnt_GE_HalfBT_reg_0;
  wire ActiveIsSlve_reg_0;
  wire ActiveIsSlve_reg_1;
  wire Aligned;
  wire Aligned_i_10_n_0;
  wire Aligned_i_11_n_0;
  wire Aligned_i_12_n_0;
  wire Aligned_i_13_n_0;
  wire Aligned_i_14_n_0;
  wire Aligned_i_15_n_0;
  wire Aligned_i_16_n_0;
  wire Aligned_i_17_n_0;
  wire Aligned_i_18_n_0;
  wire Aligned_i_19_n_0;
  wire Aligned_i_1_n_0;
  wire Aligned_i_2_n_0;
  wire Aligned_i_3_n_0;
  wire Aligned_i_4_n_0;
  wire Aligned_i_5_n_0;
  wire Aligned_i_6_n_0;
  wire Aligned_i_7_n_0;
  wire Aligned_i_8_n_0;
  wire Aligned_i_9_n_0;
  wire [1:0]BaseX_Idly_Load;
  wire [0:0]BaseX_Rx_Fifo_Rd_En;
  wire [7:0]BaseX_Rx_Q_Out;
  wire [0:0]D;
  wire D0;
  wire [0:0]E;
  wire [0:0]IntReset_dly;
  wire [0:0]\IntReset_dly_reg[0]_0 ;
  wire \IntReset_dly_reg_n_0_[1] ;
  wire [5:0]\IntRx_BtVal_reg[8]_0 ;
  wire LossOfSignal_i_4_n_0;
  wire LossOfSignal_reg_0;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_2 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_3 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_4 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_5 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_6 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_7 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ;
  wire \Mstr_CntValIn_Out[0]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[3]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[3]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[4]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[4]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_5_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_5_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_6_n_0 ;
  wire [8:0]\Mstr_CntValIn_Out_reg[8]_0 ;
  wire [1:0]Mstr_Load_dly;
  wire [0:0]Mstr_Load_reg_0;
  wire Mstr_Load_reg_1;
  wire [2:0]PhaseDet_CntDec;
  wire \PhaseDet_CntDec[0]_i_1_n_0 ;
  wire \PhaseDet_CntDec[1]_i_1_n_0 ;
  wire \PhaseDet_CntDec[2]_i_1_n_0 ;
  wire \PhaseDet_CntDec[2]_i_2_n_0 ;
  wire \PhaseDet_CntDec[2]_i_3_n_0 ;
  wire \PhaseDet_CntDec[2]_i_4_n_0 ;
  wire \PhaseDet_CntDec[2]_i_5_n_0 ;
  wire [2:0]PhaseDet_CntInc;
  wire \PhaseDet_CntInc[0]_i_1_n_0 ;
  wire \PhaseDet_CntInc[1]_i_1_n_0 ;
  wire \PhaseDet_CntInc[2]_i_1_n_0 ;
  wire \PhaseDet_CntInc[2]_i_2_n_0 ;
  wire \PhaseDet_CntInc[2]_i_3_n_0 ;
  wire \PhaseDet_CntInc[2]_i_4_n_0 ;
  wire \PhaseDet_CntInc[2]_i_5_n_0 ;
  wire [6:0]Q;
  wire \Rx_Algn_Data_Out[0]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[0]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[1]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[1]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[2]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[2]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[3]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[3]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[4]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[4]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[5]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[5]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_4_n_0 ;
  wire Rx_Algn_Valid_Out0;
  wire Rx_SysClk;
  wire Rx_Valid_Int_i_1_n_0;
  wire Rx_Valid_Int_reg_n_0;
  wire [0:0]SR;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_2 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_3 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_4 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_5 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_6 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_7 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ;
  wire \Slve_CntValIn_Out[0]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[1]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[1]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_5_n_0 ;
  wire \Slve_CntValIn_Out_reg[0]_0 ;
  wire \Slve_CntValIn_Out_reg[1]_0 ;
  wire [1:0]Slve_Load_dly;
  wire Slve_Load_reg_0;
  wire WrapToZero;
  wire WrapToZero_reg_0;
  wire \act_count[0]_i_1_n_0 ;
  wire \act_count[1]_i_1_n_0 ;
  wire \act_count[2]_i_1_n_0 ;
  wire \act_count[3]_i_1_n_0 ;
  wire \act_count[4]_i_1_n_0 ;
  wire \act_count[5]_i_1_n_0 ;
  wire \act_count[5]_i_2_n_0 ;
  wire \act_count[5]_i_4_n_0 ;
  wire \act_count[5]_i_6_n_0 ;
  wire \act_count[5]_i_7_n_0 ;
  wire \act_count[5]_i_8_n_0 ;
  wire [4:0]act_count_reg;
  wire \act_count_reg[0]_0 ;
  wire \act_count_reg[3]_0 ;
  wire \act_count_reg[4]_0 ;
  wire [0:0]\act_count_reg[5]_0 ;
  wire \active_reg[1]_0 ;
  wire \active_reg_n_0_[0] ;
  wire \active_reg_n_0_[1] ;
  wire \active_reg_n_0_[2] ;
  wire \active_reg_n_0_[3] ;
  wire [9:0]al_rx_data_out;
  wire [7:5]b3;
  wire code_err_i;
  wire [6:0]data0;
  wire [9:0]data9;
  wire [2:0]\decode_8b10b/b4_disp__9 ;
  wire [2:0]\decode_8b10b/b6_disp__34 ;
  wire \decode_8b10b/k28__1 ;
  wire \decode_8b10b/pdbr62__0 ;
  wire \decode_8b10b/sK28__2 ;
  wire decoded_rxchariscomma0;
  wire decoded_rxchariscomma_i_2_n_0;
  wire decoded_rxchariscomma_i_3_n_0;
  wire decoded_rxchariscomma_i_5_n_0;
  wire decoded_rxchariscomma_i_6_n_0;
  wire decoded_rxchariscomma_i_7_n_0;
  wire [7:0]delay_change;
  wire \delay_change[7]_i_1_n_0 ;
  wire \delay_change_reg_n_0_[0] ;
  wire \gcerr.CODE_ERR_i_2_n_0 ;
  wire \gcerr.CODE_ERR_i_3_n_0 ;
  wire \gcerr.CODE_ERR_i_4_n_0 ;
  wire \gcerr.CODE_ERR_i_6_n_0 ;
  wire \gde.gdeni.DISP_ERR_i_2_n_0 ;
  wire \gde.gdeni.DISP_ERR_i_3_n_0 ;
  wire \grdni.run_disp_i_reg ;
  wire \grdni.run_disp_i_reg_0 ;
  wire [0:0]\grdni.run_disp_i_reg_1 ;
  wire [9:0]hdataout;
  wire \hdataout[0]_i_1_n_0 ;
  wire \hdataout[0]_i_2_n_0 ;
  wire \hdataout[1]_i_1_n_0 ;
  wire \hdataout[1]_i_2_n_0 ;
  wire \hdataout[2]_i_1_n_0 ;
  wire \hdataout[2]_i_2_n_0 ;
  wire \hdataout[3]_i_1_n_0 ;
  wire \hdataout[3]_i_2_n_0 ;
  wire \hdataout[4]_i_1_n_0 ;
  wire \hdataout[4]_i_2_n_0 ;
  wire \hdataout[5]_i_1_n_0 ;
  wire \hdataout[5]_i_2_n_0 ;
  wire \hdataout[6]_i_1_n_0 ;
  wire \hdataout[6]_i_2_n_0 ;
  wire \hdataout[7]_i_1_n_0 ;
  wire \hdataout[7]_i_2_n_0 ;
  wire \hdataout[8]_i_1_n_0 ;
  wire \hdataout[8]_i_2_n_0 ;
  wire \hdataout[9]_i_2_n_0 ;
  wire \hdataout[9]_i_3_n_0 ;
  wire \holdreg[10]_i_1_n_0 ;
  wire \holdreg[11]_i_1_n_0 ;
  wire \holdreg[12]_i_1_n_0 ;
  wire \holdreg[13]_i_1_n_0 ;
  wire \holdreg[14]_i_1_n_0 ;
  wire \holdreg[1]_i_1_n_0 ;
  wire \holdreg[2]_i_1_n_0 ;
  wire \holdreg[3]_i_1_n_0 ;
  wire \holdreg[4]_i_1_n_0 ;
  wire \holdreg[5]_i_1_n_0 ;
  wire \holdreg[6]_i_1_n_0 ;
  wire \holdreg[7]_i_1_n_0 ;
  wire \holdreg[8]_i_1_n_0 ;
  wire \holdreg[9]_i_1_n_0 ;
  wire \holdreg_reg_n_0_[10] ;
  wire \holdreg_reg_n_0_[11] ;
  wire \holdreg_reg_n_0_[12] ;
  wire \holdreg_reg_n_0_[13] ;
  wire \holdreg_reg_n_0_[14] ;
  wire \holdreg_reg_n_0_[1] ;
  wire \holdreg_reg_n_0_[2] ;
  wire \holdreg_reg_n_0_[3] ;
  wire \holdreg_reg_n_0_[4] ;
  wire \holdreg_reg_n_0_[5] ;
  wire \holdreg_reg_n_0_[6] ;
  wire \holdreg_reg_n_0_[7] ;
  wire \holdreg_reg_n_0_[8] ;
  wire \holdreg_reg_n_0_[9] ;
  wire i___0_carry_i_1__0_n_0;
  wire i___0_carry_i_1_n_0;
  wire i___0_carry_i_2__0_n_0;
  wire i___0_carry_i_2_n_0;
  wire i___0_carry_i_3__0_n_0;
  wire i___0_carry_i_3_n_0;
  wire i___0_carry_i_4__0_n_0;
  wire i___0_carry_i_4_n_0;
  wire i___0_carry_i_5__0_n_0;
  wire i___0_carry_i_5_n_0;
  wire i___0_carry_i_6__0_n_0;
  wire i___0_carry_i_6_n_0;
  wire i___0_carry_i_7__0_n_0;
  wire i___0_carry_i_7_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire insert3_reg_0;
  wire insert3_reg_1;
  wire insert5_reg_0;
  wire insert5_reg_1;
  wire invby_e;
  wire invr6;
  wire k;
  wire k1;
  wire [7:3]monitor;
  wire monitor_late_reg_0;
  wire monitor_late_reg_1;
  wire \mpx[0]_i_10_n_0 ;
  wire \mpx[0]_i_11_n_0 ;
  wire \mpx[0]_i_12_n_0 ;
  wire \mpx[0]_i_13_n_0 ;
  wire \mpx[0]_i_14_n_0 ;
  wire \mpx[0]_i_15_n_0 ;
  wire \mpx[0]_i_16_n_0 ;
  wire \mpx[0]_i_17_n_0 ;
  wire \mpx[0]_i_18_n_0 ;
  wire \mpx[0]_i_19_n_0 ;
  wire \mpx[0]_i_1_n_0 ;
  wire \mpx[0]_i_20_n_0 ;
  wire \mpx[0]_i_21_n_0 ;
  wire \mpx[0]_i_22_n_0 ;
  wire \mpx[0]_i_23_n_0 ;
  wire \mpx[0]_i_24_n_0 ;
  wire \mpx[0]_i_2_n_0 ;
  wire \mpx[0]_i_3_n_0 ;
  wire \mpx[0]_i_4_n_0 ;
  wire \mpx[0]_i_5_n_0 ;
  wire \mpx[0]_i_6_n_0 ;
  wire \mpx[0]_i_7_n_0 ;
  wire \mpx[0]_i_8_n_0 ;
  wire \mpx[0]_i_9_n_0 ;
  wire \mpx[1]_i_1_n_0 ;
  wire \mpx[1]_i_2_n_0 ;
  wire \mpx[1]_i_3_n_0 ;
  wire \mpx[1]_i_4_n_0 ;
  wire \mpx[2]_i_1_n_0 ;
  wire \mpx[3]_i_10_n_0 ;
  wire \mpx[3]_i_11_n_0 ;
  wire \mpx[3]_i_1_n_0 ;
  wire \mpx[3]_i_2_n_0 ;
  wire \mpx[3]_i_3_n_0 ;
  wire \mpx[3]_i_4_n_0 ;
  wire \mpx[3]_i_5_n_0 ;
  wire \mpx[3]_i_6_n_0 ;
  wire \mpx[3]_i_7_n_0 ;
  wire \mpx[3]_i_8_n_0 ;
  wire \mpx[3]_i_9_n_0 ;
  wire [3:0]mpx__0;
  wire ndbr6;
  wire ndur6;
  wire [4:0]out;
  wire p_0_in0;
  wire [3:0]p_0_in0_in;
  wire [7:2]p_1_in;
  wire p_1_out;
  wire [7:4]p_2_out;
  wire [7:4]p_3_out;
  wire [4:0]pd_count;
  wire \pd_count[0]_i_1_n_0 ;
  wire \pd_count[1]_i_1_n_0 ;
  wire \pd_count[2]_i_1_n_0 ;
  wire \pd_count[2]_i_2_n_0 ;
  wire \pd_count[2]_i_3_n_0 ;
  wire \pd_count[3]_i_1_n_0 ;
  wire \pd_count[4]_i_1_n_0 ;
  wire \pd_count[4]_i_2_n_0 ;
  wire \pd_count[4]_i_3_n_0 ;
  wire pd_ovflw_down_i_2_n_0;
  wire pd_ovflw_down_reg_n_0;
  wire pd_ovflw_up;
  wire pd_ovflw_up_i_1_n_0;
  wire pd_ovflw_up_reg_n_0;
  wire pdbr6;
  wire pdur6;
  wire \rxdh_reg_n_0_[0] ;
  wire \rxdh_reg_n_0_[19] ;
  wire \rxdh_reg_n_0_[1] ;
  wire \rxdh_reg_n_0_[2] ;
  wire \rxdh_reg_n_0_[3] ;
  wire \rxdh_reg_n_0_[4] ;
  wire \rxdh_reg_n_0_[5] ;
  wire \rxdh_reg_n_0_[6] ;
  wire \rxdh_reg_n_0_[7] ;
  wire \rxdh_reg_n_0_[8] ;
  wire s_state;
  wire \s_state[0]_i_1_n_0 ;
  wire \s_state[0]_i_2_n_0 ;
  wire \s_state[1]_i_1_n_0 ;
  wire \s_state[1]_i_2_n_0 ;
  wire \s_state[2]_i_1_n_0 ;
  wire \s_state[3]_i_1_n_0 ;
  wire \s_state[4]_i_1_n_0 ;
  wire \s_state[5]_i_2_n_0 ;
  wire \s_state[5]_i_3_n_0 ;
  wire \s_state[5]_i_5_n_0 ;
  wire \s_state[5]_i_6_n_0 ;
  wire \s_state[5]_i_7_n_0 ;
  wire \s_state[5]_i_8_n_0 ;
  wire \s_state[5]_i_9_n_0 ;
  wire \s_state_reg[0]_0 ;
  wire \s_state_reg[0]_1 ;
  wire \s_state_reg[2]_0 ;
  wire \s_state_reg[3]_0 ;
  wire [4:0]\s_state_reg[4]_0 ;
  wire \s_state_reg[4]_1 ;
  wire \s_state_reg[4]_2 ;
  wire \s_state_reg[5]_0 ;
  wire \s_state_reg_n_0_[5] ;
  wire \toggle[0]_i_1_n_0 ;
  wire \toggle[1]_i_1_n_0 ;
  wire \toggle[2]_i_1_n_0 ;
  wire \toggle[3]_i_1_n_0 ;
  wire \toggle_reg_n_0_[0] ;
  wire \toggle_reg_n_0_[1] ;
  wire \toggle_reg_n_0_[2] ;
  wire [7:5]NLW_ActCnt_GE_HalfBT0_carry_CO_UNCONNECTED;
  wire [7:0]NLW_ActCnt_GE_HalfBT0_carry_O_UNCONNECTED;
  wire [7:5]\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [7:0]\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [7:6]\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED ;
  wire [7:7]\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED ;
  wire [7:6]\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED ;
  wire [7:7]\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_BTval_i_2
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(ActCnt_EQ_BTval_i_4_n_0),
        .I4(ActCnt_EQ_BTval_i_5_n_0),
        .O(ActCnt_EQ_BTval_i_2_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_BTval_i_3
       (.I0(\Slve_CntValIn_Out_reg[1]_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .I2(Q[0]),
        .I3(ActCnt_EQ_BTval_i_6_n_0),
        .I4(ActCnt_EQ_BTval_i_7_n_0),
        .O(ActCnt_EQ_BTval_i_3_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_4
       (.I0(p_1_in[4]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(p_1_in[3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(p_1_in[2]),
        .O(ActCnt_EQ_BTval_i_4_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_5
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(p_1_in[5]),
        .I2(p_1_in[6]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I5(p_1_in[7]),
        .O(ActCnt_EQ_BTval_i_5_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_6
       (.I0(p_1_in[3]),
        .I1(Q[2]),
        .I2(p_1_in[4]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(p_1_in[2]),
        .O(ActCnt_EQ_BTval_i_6_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_7
       (.I0(Q[4]),
        .I1(p_1_in[5]),
        .I2(p_1_in[6]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(p_1_in[7]),
        .O(ActCnt_EQ_BTval_i_7_n_0));
  FDRE ActCnt_EQ_BTval_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_EQ_BTval_reg_i_1_n_0),
        .Q(ActCnt_EQ_BTval),
        .R(SR));
  MUXF7 ActCnt_EQ_BTval_reg_i_1
       (.I0(ActCnt_EQ_BTval_i_2_n_0),
        .I1(ActCnt_EQ_BTval_i_3_n_0),
        .O(ActCnt_EQ_BTval_reg_i_1_n_0),
        .S(ActiveIsSlve_reg_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000400)) 
    ActCnt_EQ_Zero_i_1
       (.I0(ActiveIsSlve_reg_0),
        .I1(ActCnt_EQ_Zero_i_2_n_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(ActCnt_EQ_Zero_i_3_n_0),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I5(ActCnt_EQ_Zero_i_4_n_0),
        .O(ActCnt_EQ_Zero_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ActCnt_EQ_Zero_i_2
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(ActCnt_EQ_Zero_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_Zero_i_3
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(ActCnt_EQ_Zero_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    ActCnt_EQ_Zero_i_4
       (.I0(ActCnt_EQ_Zero_i_5_n_0),
        .I1(Q[6]),
        .I2(ActiveIsSlve_reg_0),
        .I3(Q[0]),
        .I4(\Slve_CntValIn_Out_reg[0]_0 ),
        .I5(\Slve_CntValIn_Out_reg[1]_0 ),
        .O(ActCnt_EQ_Zero_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_Zero_i_5
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[5]),
        .O(ActCnt_EQ_Zero_i_5_n_0));
  FDRE ActCnt_EQ_Zero_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_EQ_Zero_i_1_n_0),
        .Q(ActCnt_EQ_Zero),
        .R(SR));
  (* COMPARATOR_THRESHOLD = "11" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 ActCnt_GE_HalfBT0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_ActCnt_GE_HalfBT0_carry_CO_UNCONNECTED[7:5],ActCnt_GE_HalfBT0_carry_n_3,ActCnt_GE_HalfBT0_carry_n_4,ActCnt_GE_HalfBT0_carry_n_5,ActCnt_GE_HalfBT0_carry_n_6,ActCnt_GE_HalfBT0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,Q[6],ActCnt_GE_HalfBT0_carry_i_1_n_0,ActCnt_GE_HalfBT0_carry_i_2_n_0,ActCnt_GE_HalfBT0_carry_i_3_n_0,ActCnt_GE_HalfBT0_carry_i_4_n_0}),
        .O(NLW_ActCnt_GE_HalfBT0_carry_O_UNCONNECTED[7:0]),
        .S({1'b0,1'b0,1'b0,ActCnt_GE_HalfBT0_carry_i_5_n_0,ActCnt_GE_HalfBT0_carry_i_6_n_0,ActCnt_GE_HalfBT0_carry_i_7_n_0,ActCnt_GE_HalfBT0_carry_i_8_n_0,ActCnt_GE_HalfBT0_carry_i_9_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_1
       (.I0(Q[5]),
        .I1(p_1_in[7]),
        .I2(Q[4]),
        .I3(p_1_in[6]),
        .O(ActCnt_GE_HalfBT0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_2
       (.I0(Q[3]),
        .I1(p_1_in[5]),
        .I2(Q[2]),
        .I3(p_1_in[4]),
        .O(ActCnt_GE_HalfBT0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_3
       (.I0(Q[1]),
        .I1(p_1_in[3]),
        .I2(Q[0]),
        .I3(p_1_in[2]),
        .O(ActCnt_GE_HalfBT0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    ActCnt_GE_HalfBT0_carry_i_4
       (.I0(\Slve_CntValIn_Out_reg[0]_0 ),
        .I1(\Slve_CntValIn_Out_reg[1]_0 ),
        .O(ActCnt_GE_HalfBT0_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    ActCnt_GE_HalfBT0_carry_i_5
       (.I0(Q[6]),
        .O(ActCnt_GE_HalfBT0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_6
       (.I0(p_1_in[7]),
        .I1(Q[5]),
        .I2(p_1_in[6]),
        .I3(Q[4]),
        .O(ActCnt_GE_HalfBT0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_7
       (.I0(p_1_in[5]),
        .I1(Q[3]),
        .I2(p_1_in[4]),
        .I3(Q[2]),
        .O(ActCnt_GE_HalfBT0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_8
       (.I0(p_1_in[3]),
        .I1(Q[1]),
        .I2(p_1_in[2]),
        .I3(Q[0]),
        .O(ActCnt_GE_HalfBT0_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ActCnt_GE_HalfBT0_carry_i_9
       (.I0(\Slve_CntValIn_Out_reg[1]_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .O(ActCnt_GE_HalfBT0_carry_i_9_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \ActCnt_GE_HalfBT0_inferred__0/i__carry 
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_CO_UNCONNECTED [7:5],\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_4 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_5 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_6 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_7 }),
        .DI({1'b0,1'b0,1'b0,\Mstr_CntValIn_Out_reg[8]_0 [8],i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_O_UNCONNECTED [7:0]),
        .S({1'b0,1'b0,1'b0,i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0,i__carry_i_9_n_0}));
  LUT3 #(
    .INIT(8'hB8)) 
    ActCnt_GE_HalfBT_i_1
       (.I0(ActCnt_GE_HalfBT0_carry_n_3),
        .I1(ActiveIsSlve_reg_0),
        .I2(\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ),
        .O(ActCnt_GE_HalfBT_i_1_n_0));
  FDRE ActCnt_GE_HalfBT_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_GE_HalfBT_i_1_n_0),
        .Q(ActCnt_GE_HalfBT_reg_0),
        .R(SR));
  FDRE ActiveIsSlve_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActiveIsSlve_reg_1),
        .Q(ActiveIsSlve_reg_0),
        .R(SR));
  LUT5 #(
    .INIT(32'hFFFFFD00)) 
    Aligned_i_1
       (.I0(Aligned_i_2_n_0),
        .I1(Aligned_i_3_n_0),
        .I2(Aligned_i_4_n_0),
        .I3(Rx_Valid_Int_reg_n_0),
        .I4(Aligned),
        .O(Aligned_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Aligned_i_10
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .O(Aligned_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    Aligned_i_11
       (.I0(data9[0]),
        .I1(data9[1]),
        .I2(data9[5]),
        .I3(\rxdh_reg_n_0_[7] ),
        .O(Aligned_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    Aligned_i_12
       (.I0(data9[1]),
        .I1(data9[2]),
        .I2(data9[3]),
        .I3(data9[0]),
        .O(Aligned_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    Aligned_i_13
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[5] ),
        .I3(\rxdh_reg_n_0_[6] ),
        .O(Aligned_i_13_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    Aligned_i_14
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[1]),
        .I3(data9[2]),
        .O(Aligned_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    Aligned_i_15
       (.I0(\rxdh_reg_n_0_[5] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(data9[0]),
        .I3(data9[3]),
        .O(Aligned_i_15_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFDF)) 
    Aligned_i_16
       (.I0(data9[6]),
        .I1(data9[7]),
        .I2(data9[4]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(Aligned_i_16_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF7F)) 
    Aligned_i_17
       (.I0(data9[7]),
        .I1(data9[1]),
        .I2(data9[0]),
        .I3(data9[6]),
        .I4(data9[5]),
        .I5(data9[2]),
        .O(Aligned_i_17_n_0));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Aligned_i_18
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .O(Aligned_i_18_n_0));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT2 #(
    .INIT(4'h7)) 
    Aligned_i_19
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .O(Aligned_i_19_n_0));
  LUT6 #(
    .INIT(64'h0000000000005445)) 
    Aligned_i_2
       (.I0(\mpx[0]_i_4_n_0 ),
        .I1(Aligned_i_5_n_0),
        .I2(data9[7]),
        .I3(data9[6]),
        .I4(\mpx[0]_i_6_n_0 ),
        .I5(Aligned_i_6_n_0),
        .O(Aligned_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF60)) 
    Aligned_i_3
       (.I0(data9[9]),
        .I1(data9[8]),
        .I2(Aligned_i_7_n_0),
        .I3(\mpx[3]_i_4_n_0 ),
        .I4(\mpx[3]_i_3_n_0 ),
        .I5(\mpx[0]_i_5_n_0 ),
        .O(Aligned_i_3_n_0));
  LUT6 #(
    .INIT(64'h55FDFD55555D5D55)) 
    Aligned_i_4
       (.I0(\mpx[0]_i_3_n_0 ),
        .I1(Aligned_i_8_n_0),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(data9[3]),
        .I4(data9[2]),
        .I5(Aligned_i_9_n_0),
        .O(Aligned_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFDF0000FFDFFFDF)) 
    Aligned_i_5
       (.I0(\mpx[0]_i_16_n_0 ),
        .I1(Aligned_i_10_n_0),
        .I2(data9[4]),
        .I3(data9[5]),
        .I4(Aligned_i_11_n_0),
        .I5(\mpx[0]_i_19_n_0 ),
        .O(Aligned_i_5_n_0));
  LUT6 #(
    .INIT(64'h0060006000606666)) 
    Aligned_i_6
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(Aligned_i_12_n_0),
        .I3(Aligned_i_13_n_0),
        .I4(Aligned_i_14_n_0),
        .I5(Aligned_i_15_n_0),
        .O(Aligned_i_6_n_0));
  LUT6 #(
    .INIT(64'h40400000404000FF)) 
    Aligned_i_7
       (.I0(Aligned_i_16_n_0),
        .I1(data9[5]),
        .I2(data9[2]),
        .I3(Aligned_i_17_n_0),
        .I4(data9[3]),
        .I5(data9[4]),
        .O(Aligned_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    Aligned_i_8
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(data9[1]),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(data9[0]),
        .I4(\rxdh_reg_n_0_[4] ),
        .I5(Aligned_i_18_n_0),
        .O(Aligned_i_8_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    Aligned_i_9
       (.I0(data9[1]),
        .I1(\rxdh_reg_n_0_[4] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(data9[0]),
        .I4(\rxdh_reg_n_0_[8] ),
        .I5(Aligned_i_19_n_0),
        .O(Aligned_i_9_n_0));
  FDRE Aligned_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Aligned_i_1_n_0),
        .Q(Aligned),
        .R(SR));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    FifoRd_0
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(D0),
        .Q(BaseX_Rx_Fifo_Rd_En),
        .R(1'b0));
  FDRE \IntReset_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntReset_dly_reg[0]_0 ),
        .Q(IntReset_dly),
        .R(1'b0));
  FDRE \IntReset_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(IntReset_dly),
        .Q(\IntReset_dly_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \IntRx_BtVal_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [0]),
        .Q(p_1_in[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [1]),
        .Q(p_1_in[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [2]),
        .Q(p_1_in[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [3]),
        .Q(p_1_in[5]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [4]),
        .Q(p_1_in[6]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [5]),
        .Q(p_1_in[7]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT2 #(
    .INIT(4'hE)) 
    LossOfSignal_i_2
       (.I0(act_count_reg[3]),
        .I1(act_count_reg[4]),
        .O(\act_count_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    LossOfSignal_i_3
       (.I0(\act_count[5]_i_4_n_0 ),
        .I1(LossOfSignal_i_4_n_0),
        .I2(\act_count[5]_i_7_n_0 ),
        .I3(act_count_reg[4]),
        .I4(act_count_reg[3]),
        .O(\act_count_reg[4]_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    LossOfSignal_i_4
       (.I0(p_0_in0_in[2]),
        .I1(\active_reg_n_0_[0] ),
        .I2(p_0_in0_in[1]),
        .I3(\active_reg_n_0_[1] ),
        .I4(\act_count[5]_i_6_n_0 ),
        .O(LossOfSignal_i_4_n_0));
  FDSE LossOfSignal_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(LossOfSignal_reg_0),
        .Q(SR),
        .S(\IntReset_dly_reg_n_0_[1] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \Mstr_CntValIn_Out0_inferred__1/i___0_carry 
       (.CI(ActCnt_GE_HalfBT_reg_0),
        .CI_TOP(1'b0),
        .CO({\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED [7:6],\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_2 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_3 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_4 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_5 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_6 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_7 }),
        .DI({1'b0,1'b0,Q[5:0]}),
        .O({\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED [7],\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 }),
        .S({1'b0,i___0_carry_i_1_n_0,i___0_carry_i_2_n_0,i___0_carry_i_3_n_0,i___0_carry_i_4_n_0,i___0_carry_i_5_n_0,i___0_carry_i_6_n_0,i___0_carry_i_7_n_0}));
  LUT6 #(
    .INIT(64'h4400404400004000)) 
    \Mstr_CntValIn_Out[0]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\Slve_CntValIn_Out_reg[0]_0 ),
        .I3(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(\Mstr_CntValIn_Out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA08A0080)) 
    \Mstr_CntValIn_Out[1]_i_1 
       (.I0(\Mstr_CntValIn_Out[1]_i_2_n_0 ),
        .I1(\Slve_CntValIn_Out_reg[1]_0 ),
        .I2(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(\Mstr_CntValIn_Out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT5 #(
    .INIT(32'h0000ABA0)) 
    \Mstr_CntValIn_Out[1]_i_2 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(SR),
        .O(\Mstr_CntValIn_Out[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Mstr_CntValIn_Out[1]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .O(\Mstr_CntValIn_Out[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4544555540440500)) 
    \Mstr_CntValIn_Out[2]_i_1 
       (.I0(\Mstr_CntValIn_Out[2]_i_2_n_0 ),
        .I1(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .O(\Mstr_CntValIn_Out[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBFBFBFBBB)) 
    \Mstr_CntValIn_Out[2]_i_2 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(p_1_in[2]),
        .O(\Mstr_CntValIn_Out[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'h000000F2)) 
    \Mstr_CntValIn_Out[2]_i_3 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [4]),
        .O(\Mstr_CntValIn_Out[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[3]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[3]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[2]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88883088BBBBFCBB)) 
    \Mstr_CntValIn_Out[3]_i_2 
       (.I0(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ),
        .I1(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I2(p_1_in[3]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[4]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[4]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[3]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFC8830BB30BBFC88)) 
    \Mstr_CntValIn_Out[4]_i_2 
       (.I0(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ),
        .I1(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I2(p_1_in[4]),
        .I3(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[5]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[5]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[4]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9F90FFFF9F900000)) 
    \Mstr_CntValIn_Out[5]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out[5]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[5]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[5]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \Mstr_CntValIn_Out[5]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h08FBFB08FB08FB08)) 
    \Mstr_CntValIn_Out[5]_i_4 
       (.I0(p_1_in[5]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEFF4444BABB0000)) 
    \Mstr_CntValIn_Out[6]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[5]),
        .I5(\Mstr_CntValIn_Out[6]_i_2_n_0 ),
        .O(\Mstr_CntValIn_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9F90FFFF9F900000)) 
    \Mstr_CntValIn_Out[6]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out[6]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[6]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \Mstr_CntValIn_Out[6]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8BB8B8B8B8B8B8B8)) 
    \Mstr_CntValIn_Out[6]_i_4 
       (.I0(p_1_in[6]),
        .I1(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEFF4444BABB0000)) 
    \Mstr_CntValIn_Out[7]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[6]),
        .I5(\Mstr_CntValIn_Out[7]_i_2_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6F60FFFF6F600000)) 
    \Mstr_CntValIn_Out[7]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I1(\Mstr_CntValIn_Out[7]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[7]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Mstr_CntValIn_Out[7]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .O(\Mstr_CntValIn_Out[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h08FBFB08)) 
    \Mstr_CntValIn_Out[7]_i_4 
       (.I0(p_1_in[7]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I4(\Mstr_CntValIn_Out[7]_i_5_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \Mstr_CntValIn_Out[7]_i_5 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hBABAABFAAAAAAAAA)) 
    \Mstr_CntValIn_Out[8]_i_1 
       (.I0(SR),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\Mstr_CntValIn_Out[8]_i_3_n_0 ),
        .O(\Mstr_CntValIn_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[8]_i_2 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_4_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[7]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT5 #(
    .INIT(32'h10011110)) 
    \Mstr_CntValIn_Out[8]_i_3 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [2]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg[4]_0 [1]),
        .O(\Mstr_CntValIn_Out[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6600F0006600F0FF)) 
    \Mstr_CntValIn_Out[8]_i_4 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I1(ActCnt_EQ_Zero_i_3_n_0),
        .I2(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ),
        .I3(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[8]_i_6_n_0 ),
        .O(\Mstr_CntValIn_Out[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT4 #(
    .INIT(16'hEE02)) 
    \Mstr_CntValIn_Out[8]_i_5 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h9555555555555555)) 
    \Mstr_CntValIn_Out[8]_i_6 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(\Mstr_CntValIn_Out[8]_i_6_n_0 ));
  FDRE \Mstr_CntValIn_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[0]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[1]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[2]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[3]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[4]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[5]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[6]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[7]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[8]_i_2_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .R(1'b0));
  FDRE \Mstr_Load_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_reg_0),
        .Q(Mstr_Load_dly[0]),
        .R(1'b0));
  FDRE \Mstr_Load_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_dly[0]),
        .Q(Mstr_Load_dly[1]),
        .R(1'b0));
  FDSE Mstr_Load_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_reg_1),
        .Q(Mstr_Load_reg_0),
        .S(SR));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \PhaseDet_CntDec[0]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .O(\PhaseDet_CntDec[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hB2DB)) 
    \PhaseDet_CntDec[1]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .O(\PhaseDet_CntDec[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \PhaseDet_CntDec[2]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .O(\PhaseDet_CntDec[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntDec[2]_i_2 
       (.I0(monitor[4]),
        .I1(monitor_late_reg_0),
        .I2(monitor[3]),
        .I3(p_0_in0_in[0]),
        .I4(\active_reg_n_0_[3] ),
        .O(\PhaseDet_CntDec[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntDec[2]_i_3 
       (.I0(monitor[6]),
        .I1(monitor_late_reg_0),
        .I2(monitor[5]),
        .I3(p_0_in0_in[2]),
        .I4(p_0_in0_in[1]),
        .O(\PhaseDet_CntDec[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntDec[2]_i_4 
       (.I0(monitor[5]),
        .I1(monitor_late_reg_0),
        .I2(monitor[4]),
        .I3(p_0_in0_in[1]),
        .I4(p_0_in0_in[0]),
        .O(\PhaseDet_CntDec[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntDec[2]_i_5 
       (.I0(monitor[7]),
        .I1(monitor_late_reg_0),
        .I2(monitor[6]),
        .I3(p_0_in0_in[3]),
        .I4(p_0_in0_in[2]),
        .O(\PhaseDet_CntDec[2]_i_5_n_0 ));
  FDRE \PhaseDet_CntDec_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[0]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[0]),
        .R(1'b0));
  FDRE \PhaseDet_CntDec_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[1]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[1]),
        .R(1'b0));
  FDRE \PhaseDet_CntDec_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[2]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[2]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \PhaseDet_CntInc[0]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .O(\PhaseDet_CntInc[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT4 #(
    .INIT(16'hB2DB)) 
    \PhaseDet_CntInc[1]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .O(\PhaseDet_CntInc[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \PhaseDet_CntInc[2]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .O(\PhaseDet_CntInc[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntInc[2]_i_2 
       (.I0(monitor[4]),
        .I1(monitor_late_reg_0),
        .I2(monitor[3]),
        .I3(\active_reg_n_0_[3] ),
        .I4(p_0_in0_in[0]),
        .O(\PhaseDet_CntInc[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntInc[2]_i_3 
       (.I0(monitor[6]),
        .I1(monitor_late_reg_0),
        .I2(monitor[5]),
        .I3(p_0_in0_in[1]),
        .I4(p_0_in0_in[2]),
        .O(\PhaseDet_CntInc[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntInc[2]_i_4 
       (.I0(monitor[7]),
        .I1(monitor_late_reg_0),
        .I2(monitor[6]),
        .I3(p_0_in0_in[2]),
        .I4(p_0_in0_in[3]),
        .O(\PhaseDet_CntInc[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntInc[2]_i_5 
       (.I0(monitor[5]),
        .I1(monitor_late_reg_0),
        .I2(monitor[4]),
        .I3(p_0_in0_in[0]),
        .I4(p_0_in0_in[1]),
        .O(\PhaseDet_CntInc[2]_i_5_n_0 ));
  FDRE \PhaseDet_CntInc_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[0]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[0]),
        .R(1'b0));
  FDRE \PhaseDet_CntInc_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[1]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[1]),
        .R(1'b0));
  FDRE \PhaseDet_CntInc_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[2]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[2]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[0]_i_1 
       (.I0(data9[0]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[4]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[0]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[0]_i_2 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[2] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[1] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[0] ),
        .O(\Rx_Algn_Data_Out[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[1]_i_1 
       (.I0(data9[1]),
        .I1(data9[0]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[5]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[1]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[1]_i_2 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[3] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[2] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[1] ),
        .O(\Rx_Algn_Data_Out[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[2]_i_1 
       (.I0(data9[2]),
        .I1(data9[1]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[6]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[2]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[2]_i_2 
       (.I0(\rxdh_reg_n_0_[5] ),
        .I1(\rxdh_reg_n_0_[4] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[3] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[2] ),
        .O(\Rx_Algn_Data_Out[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[3]_i_1 
       (.I0(data9[3]),
        .I1(data9[2]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[7]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[3]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[3]_i_2 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[4] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[3] ),
        .O(\Rx_Algn_Data_Out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[4]_i_1 
       (.I0(data9[4]),
        .I1(data9[3]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[8]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[4]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[4]_i_2 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[5] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[4] ),
        .O(\Rx_Algn_Data_Out[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[5]_i_1 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[9]_i_4_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[5]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[5]_i_2 
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[6] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[5] ),
        .O(\Rx_Algn_Data_Out[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_1 
       (.I0(data9[6]),
        .I1(data9[5]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[6]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[6]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_2 
       (.I0(data9[4]),
        .I1(data9[3]),
        .I2(mpx__0[1]),
        .I3(data9[2]),
        .I4(mpx__0[0]),
        .I5(data9[1]),
        .O(\Rx_Algn_Data_Out[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_3 
       (.I0(data9[0]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[6] ),
        .O(\Rx_Algn_Data_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_1 
       (.I0(data9[7]),
        .I1(data9[6]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[7]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[7]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_2 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(mpx__0[1]),
        .I3(data9[3]),
        .I4(mpx__0[0]),
        .I5(data9[2]),
        .O(\Rx_Algn_Data_Out[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_3 
       (.I0(data9[1]),
        .I1(data9[0]),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[8] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[7] ),
        .O(\Rx_Algn_Data_Out[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_1 
       (.I0(data9[8]),
        .I1(data9[7]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[8]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[8]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_2 
       (.I0(data9[6]),
        .I1(data9[5]),
        .I2(mpx__0[1]),
        .I3(data9[4]),
        .I4(mpx__0[0]),
        .I5(data9[3]),
        .O(\Rx_Algn_Data_Out[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_3 
       (.I0(data9[2]),
        .I1(data9[1]),
        .I2(mpx__0[1]),
        .I3(data9[0]),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[8] ),
        .O(\Rx_Algn_Data_Out[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_1 
       (.I0(data9[9]),
        .I1(data9[8]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[9]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[9]_i_4_n_0 ),
        .O(\Rx_Algn_Data_Out[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_2 
       (.I0(data9[7]),
        .I1(data9[6]),
        .I2(mpx__0[1]),
        .I3(data9[5]),
        .I4(mpx__0[0]),
        .I5(data9[4]),
        .O(\Rx_Algn_Data_Out[9]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Rx_Algn_Data_Out[9]_i_3 
       (.I0(mpx__0[0]),
        .I1(mpx__0[3]),
        .I2(mpx__0[2]),
        .O(\Rx_Algn_Data_Out[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_4 
       (.I0(data9[3]),
        .I1(data9[2]),
        .I2(mpx__0[1]),
        .I3(data9[1]),
        .I4(mpx__0[0]),
        .I5(data9[0]),
        .O(\Rx_Algn_Data_Out[9]_i_4_n_0 ));
  FDRE \Rx_Algn_Data_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[0]_i_1_n_0 ),
        .Q(al_rx_data_out[0]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[1]_i_1_n_0 ),
        .Q(al_rx_data_out[1]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[2]_i_1_n_0 ),
        .Q(al_rx_data_out[2]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[3]_i_1_n_0 ),
        .Q(al_rx_data_out[3]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[4]_i_1_n_0 ),
        .Q(al_rx_data_out[4]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[5]_i_1_n_0 ),
        .Q(al_rx_data_out[5]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[6]_i_1_n_0 ),
        .Q(al_rx_data_out[6]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[7]_i_1_n_0 ),
        .Q(al_rx_data_out[7]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[8]_i_1_n_0 ),
        .Q(al_rx_data_out[8]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[9] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[9]_i_1_n_0 ),
        .Q(al_rx_data_out[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    Rx_Algn_Valid_Out_i_1
       (.I0(Rx_Valid_Int_reg_n_0),
        .I1(Aligned),
        .O(Rx_Algn_Valid_Out0));
  FDRE Rx_Algn_Valid_Out_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Rx_Algn_Valid_Out0),
        .Q(E),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000057)) 
    Rx_Valid_Int_i_1
       (.I0(\toggle_reg_n_0_[2] ),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(\toggle_reg_n_0_[1] ),
        .I3(p_0_in0),
        .I4(Rx_Valid_Int_reg_n_0),
        .I5(SR),
        .O(Rx_Valid_Int_i_1_n_0));
  FDRE Rx_Valid_Int_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Rx_Valid_Int_i_1_n_0),
        .Q(Rx_Valid_Int_reg_n_0),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \Slve_CntValIn_Out0_inferred__1/i___0_carry 
       (.CI(ActCnt_GE_HalfBT_reg_0),
        .CI_TOP(1'b0),
        .CO({\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED [7:6],\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_2 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_3 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_4 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_5 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_6 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_7 }),
        .DI({1'b0,1'b0,\Mstr_CntValIn_Out_reg[8]_0 [7:2]}),
        .O({\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED [7],\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 }),
        .S({1'b0,i___0_carry_i_1__0_n_0,i___0_carry_i_2__0_n_0,i___0_carry_i_3__0_n_0,i___0_carry_i_4__0_n_0,i___0_carry_i_5__0_n_0,i___0_carry_i_6__0_n_0,i___0_carry_i_7__0_n_0}));
  LUT6 #(
    .INIT(64'hFEEEAAAAAEEEAAAA)) 
    \Slve_CntValIn_Out[0]_i_1 
       (.I0(\Slve_CntValIn_Out[1]_i_2_n_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(\Slve_CntValIn_Out[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8F0F0F7F0F0F0)) 
    \Slve_CntValIn_Out[1]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\Slve_CntValIn_Out[1]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out_reg[1]_0 ),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(\Slve_CntValIn_Out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \Slve_CntValIn_Out[1]_i_2 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(ActiveIsSlve_reg_0),
        .O(\Slve_CntValIn_Out[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFE44FE54FE44EE44)) 
    \Slve_CntValIn_Out[2]_i_1 
       (.I0(SR),
        .I1(\Slve_CntValIn_Out[2]_i_2_n_0 ),
        .I2(\Slve_CntValIn_Out[2]_i_3_n_0 ),
        .I3(p_1_in[2]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(ActiveIsSlve_reg_0),
        .O(\Slve_CntValIn_Out[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT5 #(
    .INIT(32'h8CCC8000)) 
    \Slve_CntValIn_Out[2]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 ),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(Q[0]),
        .O(\Slve_CntValIn_Out[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Slve_CntValIn_Out[2]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[2]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Slve_CntValIn_Out[3]_i_1 
       (.I0(p_1_in[3]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[3]_i_2_n_0 ),
        .O(\Slve_CntValIn_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h80BFFFFF80BF0000)) 
    \Slve_CntValIn_Out[3]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(Q[1]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Slve_CntValIn_Out[3]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \Slve_CntValIn_Out[3]_i_3 
       (.I0(p_1_in[3]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[2]),
        .O(\Slve_CntValIn_Out[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Slve_CntValIn_Out[4]_i_1 
       (.I0(p_1_in[4]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[4]_i_2_n_0 ),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\Slve_CntValIn_Out[4]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT5 #(
    .INIT(32'h8CB3B38C)) 
    \Slve_CntValIn_Out[4]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(Q[2]),
        .I4(Q[1]),
        .O(\Slve_CntValIn_Out[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \Slve_CntValIn_Out[4]_i_3 
       (.I0(p_1_in[4]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[3]),
        .O(\Slve_CntValIn_Out[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h8B8B8B88)) 
    \Slve_CntValIn_Out[5]_i_1 
       (.I0(p_1_in[5]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[5]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out[5]_i_3_n_0 ),
        .I4(\Slve_CntValIn_Out[5]_i_4_n_0 ),
        .O(\Slve_CntValIn_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000474700FF)) 
    \Slve_CntValIn_Out[5]_i_2 
       (.I0(p_1_in[5]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(p_1_in[4]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT4 #(
    .INIT(16'h1540)) 
    \Slve_CntValIn_Out[5]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\Slve_CntValIn_Out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF0F0F0F9F9F0F0F)) 
    \Slve_CntValIn_Out[5]_i_4 
       (.I0(Q[3]),
        .I1(\Slve_CntValIn_Out[5]_i_5_n_0 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\Slve_CntValIn_Out[5]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \Slve_CntValIn_Out[5]_i_5 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\Slve_CntValIn_Out[5]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8B8B8B88)) 
    \Slve_CntValIn_Out[6]_i_1 
       (.I0(p_1_in[6]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[6]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out[6]_i_3_n_0 ),
        .I4(\Slve_CntValIn_Out[6]_i_4_n_0 ),
        .O(\Slve_CntValIn_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000474700FF)) 
    \Slve_CntValIn_Out[6]_i_2 
       (.I0(p_1_in[6]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(p_1_in[5]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444000000004)) 
    \Slve_CntValIn_Out[6]_i_3 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[4]),
        .O(\Slve_CntValIn_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF0F9F9F0F0F9F9F)) 
    \Slve_CntValIn_Out[6]_i_4 
       (.I0(Q[4]),
        .I1(\Slve_CntValIn_Out[6]_i_5_n_0 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\Slve_CntValIn_Out[6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \Slve_CntValIn_Out[6]_i_5 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[3]),
        .O(\Slve_CntValIn_Out[6]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Slve_CntValIn_Out[7]_i_1 
       (.I0(p_1_in[7]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[7]_i_2_n_0 ),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\Slve_CntValIn_Out[7]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8B00B8FF8BFFB800)) 
    \Slve_CntValIn_Out[7]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\Slve_CntValIn_Out[7]_i_4_n_0 ),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(Q[5]),
        .I5(\Slve_CntValIn_Out[7]_i_5_n_0 ),
        .O(\Slve_CntValIn_Out[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hCFC0AAAA)) 
    \Slve_CntValIn_Out[7]_i_3 
       (.I0(p_1_in[6]),
        .I1(p_1_in[7]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(ActiveIsSlve_reg_0),
        .I4(\s_state_reg[4]_0 [0]),
        .O(\Slve_CntValIn_Out[7]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Slve_CntValIn_Out[7]_i_4 
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\Slve_CntValIn_Out[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \Slve_CntValIn_Out[7]_i_5 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(\Slve_CntValIn_Out[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAABABBAFAAAAAAAA)) 
    \Slve_CntValIn_Out[8]_i_1 
       (.I0(SR),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(ActiveIsSlve_reg_0),
        .I5(\Mstr_CntValIn_Out[8]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000AEEA)) 
    \Slve_CntValIn_Out[8]_i_2 
       (.I0(\Slve_CntValIn_Out[8]_i_3_n_0 ),
        .I1(\s_state_reg[5]_0 ),
        .I2(\Slve_CntValIn_Out[8]_i_5_n_0 ),
        .I3(Q[6]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(SR),
        .O(\Slve_CntValIn_Out[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'hCF0AC00A)) 
    \Slve_CntValIn_Out[8]_i_3 
       (.I0(ActiveIsSlve_reg_0),
        .I1(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(p_1_in[7]),
        .O(\Slve_CntValIn_Out[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Slve_CntValIn_Out[8]_i_4 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [1]),
        .O(\s_state_reg[5]_0 ));
  LUT6 #(
    .INIT(64'h4000000000000002)) 
    \Slve_CntValIn_Out[8]_i_5 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(Q[5]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\Slve_CntValIn_Out[8]_i_5_n_0 ));
  FDRE \Slve_CntValIn_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[0]_i_1_n_0 ),
        .Q(\Slve_CntValIn_Out_reg[0]_0 ),
        .R(SR));
  FDRE \Slve_CntValIn_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[1]_i_1_n_0 ),
        .Q(\Slve_CntValIn_Out_reg[1]_0 ),
        .R(SR));
  FDRE \Slve_CntValIn_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[2]_i_1_n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[4]_i_1_n_0 ),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[5]_i_1_n_0 ),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[6]_i_1_n_0 ),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[7]_i_1_n_0 ),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[8]_i_2_n_0 ),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \Slve_Load_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(D),
        .Q(Slve_Load_dly[0]),
        .R(1'b0));
  FDRE \Slve_Load_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Slve_Load_dly[0]),
        .Q(Slve_Load_dly[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0010000101110110)) 
    Slve_Load_i_2
       (.I0(\s_state_reg[4]_0 [2]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [4]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\s_state_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT3 #(
    .INIT(8'hD1)) 
    Slve_Load_i_3
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [2]),
        .O(\s_state_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFEFEBFFCFFFEFEFD)) 
    Slve_Load_i_4
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\s_state_reg[4]_1 ));
  FDSE Slve_Load_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Slve_Load_reg_0),
        .Q(D),
        .S(SR));
  FDRE WrapToZero_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(WrapToZero_reg_0),
        .Q(WrapToZero),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \act_count[0]_i_1 
       (.I0(act_count_reg[0]),
        .O(\act_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \act_count[1]_i_1 
       (.I0(act_count_reg[0]),
        .I1(\active_reg[1]_0 ),
        .I2(act_count_reg[1]),
        .O(\act_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT4 #(
    .INIT(16'hD2B4)) 
    \act_count[2]_i_1 
       (.I0(act_count_reg[0]),
        .I1(\active_reg[1]_0 ),
        .I2(act_count_reg[2]),
        .I3(act_count_reg[1]),
        .O(\act_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT5 #(
    .INIT(32'hAAA96AAA)) 
    \act_count[3]_i_1 
       (.I0(act_count_reg[3]),
        .I1(act_count_reg[2]),
        .I2(act_count_reg[1]),
        .I3(act_count_reg[0]),
        .I4(\active_reg[1]_0 ),
        .O(\act_count[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFF4000FFFD0002)) 
    \act_count[4]_i_1 
       (.I0(\active_reg[1]_0 ),
        .I1(act_count_reg[0]),
        .I2(act_count_reg[2]),
        .I3(act_count_reg[1]),
        .I4(act_count_reg[4]),
        .I5(act_count_reg[3]),
        .O(\act_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFCFFFFFFFFFFFFAF)) 
    \act_count[5]_i_1 
       (.I0(\act_count_reg[0]_0 ),
        .I1(\act_count[5]_i_4_n_0 ),
        .I2(\active_reg[1]_0 ),
        .I3(act_count_reg[4]),
        .I4(act_count_reg[3]),
        .I5(\act_count_reg[5]_0 ),
        .O(\act_count[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFCBF00400340)) 
    \act_count[5]_i_2 
       (.I0(\act_count[5]_i_4_n_0 ),
        .I1(act_count_reg[4]),
        .I2(act_count_reg[3]),
        .I3(\active_reg[1]_0 ),
        .I4(\act_count_reg[0]_0 ),
        .I5(\act_count_reg[5]_0 ),
        .O(\act_count[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \act_count[5]_i_3 
       (.I0(act_count_reg[0]),
        .I1(act_count_reg[2]),
        .I2(act_count_reg[1]),
        .O(\act_count_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \act_count[5]_i_4 
       (.I0(act_count_reg[2]),
        .I1(act_count_reg[1]),
        .I2(act_count_reg[0]),
        .O(\act_count[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    \act_count[5]_i_5 
       (.I0(\act_count[5]_i_6_n_0 ),
        .I1(\active_reg_n_0_[1] ),
        .I2(p_0_in0_in[1]),
        .I3(\active_reg_n_0_[0] ),
        .I4(p_0_in0_in[2]),
        .I5(\act_count[5]_i_7_n_0 ),
        .O(\active_reg[1]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \act_count[5]_i_6 
       (.I0(p_0_in0_in[0]),
        .I1(\active_reg_n_0_[3] ),
        .I2(\active_reg_n_0_[2] ),
        .I3(p_0_in0_in[3]),
        .O(\act_count[5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \act_count[5]_i_7 
       (.I0(\active_reg_n_0_[3] ),
        .I1(p_0_in0_in[0]),
        .I2(p_0_in0_in[2]),
        .I3(\active_reg_n_0_[0] ),
        .I4(\act_count[5]_i_8_n_0 ),
        .O(\act_count[5]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \act_count[5]_i_8 
       (.I0(\active_reg_n_0_[2] ),
        .I1(p_0_in0_in[3]),
        .I2(\active_reg_n_0_[1] ),
        .I3(p_0_in0_in[1]),
        .O(\act_count[5]_i_8_n_0 ));
  FDRE \act_count_reg[0] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[0]_i_1_n_0 ),
        .Q(act_count_reg[0]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[1] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[1]_i_1_n_0 ),
        .Q(act_count_reg[1]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[2] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[2]_i_1_n_0 ),
        .Q(act_count_reg[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[3] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[3]_i_1_n_0 ),
        .Q(act_count_reg[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[4] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[4]_i_1_n_0 ),
        .Q(act_count_reg[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[5] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[5]_i_2_n_0 ),
        .Q(\act_count_reg[5]_0 ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[4]_i_1 
       (.I0(BaseX_Rx_Q_Out[4]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[0]),
        .O(p_2_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[5]_i_1 
       (.I0(BaseX_Rx_Q_Out[5]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[1]),
        .O(p_2_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[6]_i_1 
       (.I0(BaseX_Rx_Q_Out[6]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[2]),
        .O(p_2_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[7]_i_1 
       (.I0(BaseX_Rx_Q_Out[7]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[3]),
        .O(p_2_out[7]));
  FDRE \active_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[0]),
        .Q(\active_reg_n_0_[0] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[1]),
        .Q(\active_reg_n_0_[1] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[2]),
        .Q(\active_reg_n_0_[2] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[3]),
        .Q(\active_reg_n_0_[3] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[4]),
        .Q(p_0_in0_in[0]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[5]),
        .Q(p_0_in0_in[1]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[6]),
        .Q(p_0_in0_in[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[7]),
        .Q(p_0_in0_in[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  LUT4 #(
    .INIT(16'h6660)) 
    decoded_rxchariscomma_i_1
       (.I0(al_rx_data_out[9]),
        .I1(al_rx_data_out[8]),
        .I2(decoded_rxchariscomma_i_2_n_0),
        .I3(decoded_rxchariscomma_i_3_n_0),
        .O(decoded_rxchariscomma0));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    decoded_rxchariscomma_i_2
       (.I0(\decode_8b10b/pdbr62__0 ),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[2]),
        .I3(decoded_rxchariscomma_i_5_n_0),
        .I4(al_rx_data_out[3]),
        .I5(al_rx_data_out[7]),
        .O(decoded_rxchariscomma_i_2_n_0));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    decoded_rxchariscomma_i_3
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[7]),
        .I4(decoded_rxchariscomma_i_6_n_0),
        .I5(decoded_rxchariscomma_i_7_n_0),
        .O(decoded_rxchariscomma_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT2 #(
    .INIT(4'hE)) 
    decoded_rxchariscomma_i_4
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .O(\decode_8b10b/pdbr62__0 ));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_5
       (.I0(al_rx_data_out[1]),
        .I1(al_rx_data_out[0]),
        .O(decoded_rxchariscomma_i_5_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_6
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .O(decoded_rxchariscomma_i_6_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_7
       (.I0(al_rx_data_out[2]),
        .I1(al_rx_data_out[3]),
        .O(decoded_rxchariscomma_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[0]_i_1 
       (.I0(data0[0]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[0]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[1]_i_1 
       (.I0(data0[1]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[1]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[2]_i_1 
       (.I0(data0[2]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[2]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[3]_i_1 
       (.I0(data0[3]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[3]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[4]_i_1 
       (.I0(data0[4]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[4]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[5]_i_1 
       (.I0(data0[5]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[5]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[6]_i_1 
       (.I0(data0[6]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[6]));
  LUT6 #(
    .INIT(64'h0000000400080001)) 
    \delay_change[7]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [3]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\s_state_reg[4]_0 [4]),
        .O(\delay_change[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \delay_change[7]_i_2 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [4]),
        .O(delay_change[7]));
  FDRE \delay_change_reg[0] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[0]),
        .Q(\delay_change_reg_n_0_[0] ),
        .R(SR));
  FDRE \delay_change_reg[1] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[1]),
        .Q(data0[0]),
        .R(SR));
  FDRE \delay_change_reg[2] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[2]),
        .Q(data0[1]),
        .R(SR));
  FDRE \delay_change_reg[3] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[3]),
        .Q(data0[2]),
        .R(SR));
  FDRE \delay_change_reg[4] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[4]),
        .Q(data0[3]),
        .R(SR));
  FDRE \delay_change_reg[5] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[5]),
        .Q(data0[4]),
        .R(SR));
  FDRE \delay_change_reg[6] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[6]),
        .Q(data0[5]),
        .R(SR));
  FDRE \delay_change_reg[7] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[7]),
        .Q(data0[6]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT5 #(
    .INIT(32'h89BFFD91)) 
    \dout_i[5]_i_1 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[8]),
        .I2(\decode_8b10b/k28__1 ),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[6]),
        .O(b3[5]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT5 #(
    .INIT(32'h98FBDF19)) 
    \dout_i[6]_i_1 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[8]),
        .I2(\decode_8b10b/k28__1 ),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[6]),
        .O(b3[6]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT5 #(
    .INIT(32'hF5E187AF)) 
    \dout_i[7]_i_1 
       (.I0(al_rx_data_out[8]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[7]),
        .I3(\decode_8b10b/k28__1 ),
        .I4(al_rx_data_out[9]),
        .O(b3[7]));
  LUT6 #(
    .INIT(64'h0000000000000006)) 
    \dout_i[7]_i_2 
       (.I0(al_rx_data_out[8]),
        .I1(al_rx_data_out[9]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[5]),
        .I5(al_rx_data_out[4]),
        .O(\decode_8b10b/k28__1 ));
  LUT6 #(
    .INIT(64'hEDA3C33DAAABADB7)) 
    g0_b0
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[0]));
  LUT6 #(
    .INIT(64'hEDA5A55BCCCDCDB7)) 
    g0_b1
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[1]));
  LUT6 #(
    .INIT(64'hFDB19967F0F1E5BF)) 
    g0_b2
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[2]));
  LUT6 #(
    .INIT(64'hFCA99697FF01FD3F)) 
    g0_b3
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[3]));
  LUT6 #(
    .INIT(64'hF8FF8117FEE9971F)) 
    g0_b4
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFEFE)) 
    \gcerr.CODE_ERR_i_1 
       (.I0(\gcerr.CODE_ERR_i_2_n_0 ),
        .I1(\gcerr.CODE_ERR_i_3_n_0 ),
        .I2(\gcerr.CODE_ERR_i_4_n_0 ),
        .I3(ndbr6),
        .I4(\gcerr.CODE_ERR_i_6_n_0 ),
        .I5(invby_e),
        .O(code_err_i));
  LUT6 #(
    .INIT(64'hFEE8E888E8808000)) 
    \gcerr.CODE_ERR_i_10 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[0]),
        .I4(al_rx_data_out[2]),
        .I5(al_rx_data_out[3]),
        .O(pdur6));
  LUT6 #(
    .INIT(64'h000101171117177F)) 
    \gcerr.CODE_ERR_i_11 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[0]),
        .I4(al_rx_data_out[2]),
        .I5(al_rx_data_out[3]),
        .O(ndur6));
  LUT6 #(
    .INIT(64'hFEE8E880E8808000)) 
    \gcerr.CODE_ERR_i_12 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[2]),
        .I2(al_rx_data_out[0]),
        .I3(al_rx_data_out[1]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(pdbr6));
  LUT6 #(
    .INIT(64'hFFFFFFFF10000004)) 
    \gcerr.CODE_ERR_i_2 
       (.I0(\decode_8b10b/sK28__2 ),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[7]),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[8]),
        .I5(invr6),
        .O(\gcerr.CODE_ERR_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF80000000000001F)) 
    \gcerr.CODE_ERR_i_3 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[7]),
        .I4(al_rx_data_out[6]),
        .I5(al_rx_data_out[8]),
        .O(\gcerr.CODE_ERR_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAB8383BFA8808080)) 
    \gcerr.CODE_ERR_i_4 
       (.I0(pdur6),
        .I1(al_rx_data_out[7]),
        .I2(al_rx_data_out[6]),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[8]),
        .I5(ndur6),
        .O(\gcerr.CODE_ERR_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000101170117177F)) 
    \gcerr.CODE_ERR_i_5 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[2]),
        .I2(al_rx_data_out[0]),
        .I3(al_rx_data_out[1]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(ndbr6));
  LUT6 #(
    .INIT(64'h0000000000018000)) 
    \gcerr.CODE_ERR_i_6 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[7]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[8]),
        .I4(al_rx_data_out[4]),
        .I5(pdbr6),
        .O(\gcerr.CODE_ERR_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT4 #(
    .INIT(16'h8002)) 
    \gcerr.CODE_ERR_i_7 
       (.I0(\decode_8b10b/sK28__2 ),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[6]),
        .I3(al_rx_data_out[7]),
        .O(invby_e));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT4 #(
    .INIT(16'h8001)) 
    \gcerr.CODE_ERR_i_8 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[5]),
        .I3(al_rx_data_out[2]),
        .O(\decode_8b10b/sK28__2 ));
  LUT6 #(
    .INIT(64'hF88080018001011F)) 
    \gcerr.CODE_ERR_i_9 
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[3]),
        .I3(al_rx_data_out[2]),
        .I4(al_rx_data_out[0]),
        .I5(al_rx_data_out[1]),
        .O(invr6));
  LUT6 #(
    .INIT(64'h00EF0051004141EF)) 
    \gde.gdeni.DISP_ERR_i_2 
       (.I0(\decode_8b10b/b6_disp__34 [1]),
        .I1(\decode_8b10b/b6_disp__34 [2]),
        .I2(\decode_8b10b/b6_disp__34 [0]),
        .I3(\decode_8b10b/b4_disp__9 [1]),
        .I4(\decode_8b10b/b4_disp__9 [2]),
        .I5(\decode_8b10b/b4_disp__9 [0]),
        .O(\gde.gdeni.DISP_ERR_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h001400FE00FE1415)) 
    \gde.gdeni.DISP_ERR_i_3 
       (.I0(\decode_8b10b/b6_disp__34 [1]),
        .I1(\decode_8b10b/b6_disp__34 [2]),
        .I2(\decode_8b10b/b6_disp__34 [0]),
        .I3(\decode_8b10b/b4_disp__9 [1]),
        .I4(\decode_8b10b/b4_disp__9 [0]),
        .I5(\decode_8b10b/b4_disp__9 [2]),
        .O(\gde.gdeni.DISP_ERR_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0002000000004000)) 
    \gde.gdeni.DISP_ERR_i_4 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[0]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[2]),
        .I4(al_rx_data_out[3]),
        .I5(al_rx_data_out[4]),
        .O(\decode_8b10b/b6_disp__34 [2]));
  MUXF7 \gde.gdeni.DISP_ERR_reg_i_1 
       (.I0(\gde.gdeni.DISP_ERR_i_2_n_0 ),
        .I1(\gde.gdeni.DISP_ERR_i_3_n_0 ),
        .O(\grdni.run_disp_i_reg_0 ),
        .S(\grdni.run_disp_i_reg_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gen_io_logic_i_1
       (.I0(Slve_Load_dly[1]),
        .I1(Slve_Load_dly[0]),
        .O(BaseX_Idly_Load[1]));
  LUT2 #(
    .INIT(4'hE)) 
    gen_io_logic_i_2
       (.I0(Mstr_Load_dly[1]),
        .I1(Mstr_Load_dly[0]),
        .O(BaseX_Idly_Load[0]));
  LUT6 #(
    .INIT(64'h00000B08FFFF0000)) 
    \grdni.run_disp_i_i_1 
       (.I0(\grdni.run_disp_i_reg_1 ),
        .I1(\decode_8b10b/b6_disp__34 [1]),
        .I2(\decode_8b10b/b4_disp__9 [2]),
        .I3(\decode_8b10b/b6_disp__34 [0]),
        .I4(\decode_8b10b/b4_disp__9 [0]),
        .I5(\decode_8b10b/b4_disp__9 [1]),
        .O(\grdni.run_disp_i_reg ));
  LUT6 #(
    .INIT(64'h0016166816686800)) 
    \grdni.run_disp_i_i_2 
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[5]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[3]),
        .O(\decode_8b10b/b6_disp__34 [1]));
  LUT4 #(
    .INIT(16'h1008)) 
    \grdni.run_disp_i_i_3 
       (.I0(al_rx_data_out[9]),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[7]),
        .I3(al_rx_data_out[6]),
        .O(\decode_8b10b/b4_disp__9 [2]));
  LUT6 #(
    .INIT(64'hFEE8E880E880C000)) 
    \grdni.run_disp_i_i_4 
       (.I0(al_rx_data_out[2]),
        .I1(al_rx_data_out[3]),
        .I2(al_rx_data_out[4]),
        .I3(al_rx_data_out[5]),
        .I4(al_rx_data_out[1]),
        .I5(al_rx_data_out[0]),
        .O(\decode_8b10b/b6_disp__34 [0]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT4 #(
    .INIT(16'hE8C0)) 
    \grdni.run_disp_i_i_5 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[9]),
        .I2(al_rx_data_out[8]),
        .I3(al_rx_data_out[6]),
        .O(\decode_8b10b/b4_disp__9 [0]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT4 #(
    .INIT(16'h0660)) 
    \grdni.run_disp_i_i_6 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[8]),
        .O(\decode_8b10b/b4_disp__9 [1]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[0]_i_1 
       (.I0(\hdataout[0]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[1] ),
        .O(\hdataout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[0]_i_2 
       (.I0(\holdreg_reg_n_0_[5] ),
        .I1(\holdreg_reg_n_0_[4] ),
        .I2(\holdreg_reg_n_0_[3] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[2] ),
        .O(\hdataout[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[1]_i_1 
       (.I0(\hdataout[1]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[2] ),
        .O(\hdataout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[1]_i_2 
       (.I0(\holdreg_reg_n_0_[6] ),
        .I1(\holdreg_reg_n_0_[5] ),
        .I2(\holdreg_reg_n_0_[4] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[3] ),
        .O(\hdataout[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[2]_i_1 
       (.I0(\hdataout[2]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[3] ),
        .O(\hdataout[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[2]_i_2 
       (.I0(\holdreg_reg_n_0_[7] ),
        .I1(\holdreg_reg_n_0_[6] ),
        .I2(\holdreg_reg_n_0_[5] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[4] ),
        .O(\hdataout[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[3]_i_1 
       (.I0(\hdataout[3]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[4] ),
        .O(\hdataout[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[3]_i_2 
       (.I0(\holdreg_reg_n_0_[8] ),
        .I1(\holdreg_reg_n_0_[7] ),
        .I2(\holdreg_reg_n_0_[6] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[5] ),
        .O(\hdataout[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[4]_i_1 
       (.I0(\hdataout[4]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[5] ),
        .O(\hdataout[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[4]_i_2 
       (.I0(\holdreg_reg_n_0_[9] ),
        .I1(\holdreg_reg_n_0_[8] ),
        .I2(\holdreg_reg_n_0_[7] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[6] ),
        .O(\hdataout[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[5]_i_1 
       (.I0(\hdataout[5]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[6] ),
        .O(\hdataout[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[5]_i_2 
       (.I0(\holdreg_reg_n_0_[10] ),
        .I1(\holdreg_reg_n_0_[9] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[7] ),
        .O(\hdataout[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[6]_i_1 
       (.I0(\hdataout[6]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[7] ),
        .O(\hdataout[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \hdataout[6]_i_2 
       (.I0(\holdreg_reg_n_0_[11] ),
        .I1(\holdreg_reg_n_0_[10] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(\toggle_reg_n_0_[0] ),
        .I4(\toggle_reg_n_0_[1] ),
        .I5(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[7]_i_1 
       (.I0(\hdataout[7]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .O(\hdataout[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[7]_i_2 
       (.I0(\holdreg_reg_n_0_[12] ),
        .I1(\holdreg_reg_n_0_[11] ),
        .I2(\holdreg_reg_n_0_[10] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[8]_i_1 
       (.I0(\hdataout[8]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[8]_i_2 
       (.I0(\holdreg_reg_n_0_[13] ),
        .I1(\holdreg_reg_n_0_[12] ),
        .I2(\holdreg_reg_n_0_[11] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[10] ),
        .O(\hdataout[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0155)) 
    \hdataout[9]_i_1 
       (.I0(p_0_in0),
        .I1(\toggle_reg_n_0_[1] ),
        .I2(\toggle_reg_n_0_[0] ),
        .I3(\toggle_reg_n_0_[2] ),
        .O(p_1_out));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[9]_i_2 
       (.I0(\hdataout[9]_i_3_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[10] ),
        .O(\hdataout[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[9]_i_3 
       (.I0(\holdreg_reg_n_0_[14] ),
        .I1(\holdreg_reg_n_0_[13] ),
        .I2(\holdreg_reg_n_0_[12] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[11] ),
        .O(\hdataout[9]_i_3_n_0 ));
  FDRE \hdataout_reg[0] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[0]_i_1_n_0 ),
        .Q(hdataout[0]),
        .R(SR));
  FDRE \hdataout_reg[1] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[1]_i_1_n_0 ),
        .Q(hdataout[1]),
        .R(SR));
  FDRE \hdataout_reg[2] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[2]_i_1_n_0 ),
        .Q(hdataout[2]),
        .R(SR));
  FDRE \hdataout_reg[3] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[3]_i_1_n_0 ),
        .Q(hdataout[3]),
        .R(SR));
  FDRE \hdataout_reg[4] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[4]_i_1_n_0 ),
        .Q(hdataout[4]),
        .R(SR));
  FDRE \hdataout_reg[5] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[5]_i_1_n_0 ),
        .Q(hdataout[5]),
        .R(SR));
  FDRE \hdataout_reg[6] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[6]_i_1_n_0 ),
        .Q(hdataout[6]),
        .R(SR));
  FDRE \hdataout_reg[7] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[7]_i_1_n_0 ),
        .Q(hdataout[7]),
        .R(SR));
  FDRE \hdataout_reg[8] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[8]_i_1_n_0 ),
        .Q(hdataout[8]),
        .R(SR));
  FDRE \hdataout_reg[9] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[9]_i_2_n_0 ),
        .Q(hdataout[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[10]_i_1 
       (.I0(\holdreg_reg_n_0_[13] ),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[0]),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[14] ),
        .O(\holdreg[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[11]_i_1 
       (.I0(\holdreg_reg_n_0_[14] ),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[1]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[0]),
        .O(\holdreg[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[12]_i_1 
       (.I0(p_0_in0_in[0]),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[2]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[1]),
        .O(\holdreg[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[13]_i_1 
       (.I0(p_0_in0_in[1]),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[3]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[2]),
        .O(\holdreg[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[14]_i_1 
       (.I0(p_0_in0_in[2]),
        .I1(insert3_reg_0),
        .I2(p_3_out[4]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[3]),
        .O(\holdreg[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[1]_i_1 
       (.I0(\holdreg_reg_n_0_[4] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[6] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[5] ),
        .O(\holdreg[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[2]_i_1 
       (.I0(\holdreg_reg_n_0_[5] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[7] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[6] ),
        .O(\holdreg[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[3]_i_1 
       (.I0(\holdreg_reg_n_0_[6] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[7] ),
        .O(\holdreg[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[4]_i_1 
       (.I0(\holdreg_reg_n_0_[7] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[9] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[8] ),
        .O(\holdreg[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[5]_i_1 
       (.I0(\holdreg_reg_n_0_[8] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[10] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[9] ),
        .O(\holdreg[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[6]_i_1 
       (.I0(\holdreg_reg_n_0_[9] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[11] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[10] ),
        .O(\holdreg[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[7]_i_1 
       (.I0(\holdreg_reg_n_0_[10] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[12] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[11] ),
        .O(\holdreg[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[8]_i_1 
       (.I0(\holdreg_reg_n_0_[11] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[13] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[12] ),
        .O(\holdreg[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[9]_i_1 
       (.I0(\holdreg_reg_n_0_[12] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[14] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[13] ),
        .O(\holdreg[9]_i_1_n_0 ));
  FDRE \holdreg_reg[10] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[10]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[10] ),
        .R(SR));
  FDRE \holdreg_reg[11] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[11]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[11] ),
        .R(SR));
  FDRE \holdreg_reg[12] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[12]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[12] ),
        .R(SR));
  FDRE \holdreg_reg[13] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[13]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[13] ),
        .R(SR));
  FDRE \holdreg_reg[14] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[14]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[14] ),
        .R(SR));
  FDRE \holdreg_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[1]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[1] ),
        .R(SR));
  FDRE \holdreg_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[2]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[2] ),
        .R(SR));
  FDRE \holdreg_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[3]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[3] ),
        .R(SR));
  FDRE \holdreg_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[4]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[4] ),
        .R(SR));
  FDRE \holdreg_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[5]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[5] ),
        .R(SR));
  FDRE \holdreg_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[6]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[6] ),
        .R(SR));
  FDRE \holdreg_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[7]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[7] ),
        .R(SR));
  FDRE \holdreg_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[8]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[8] ),
        .R(SR));
  FDRE \holdreg_reg[9] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[9]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[9] ),
        .R(SR));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_1
       (.I0(ActCnt_GE_HalfBT_reg_0),
        .I1(Q[6]),
        .O(i___0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_1__0
       (.I0(ActCnt_GE_HalfBT_reg_0),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .O(i___0_carry_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_2
       (.I0(p_1_in[7]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[5]),
        .O(i___0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_2__0
       (.I0(p_1_in[7]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(i___0_carry_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_3
       (.I0(p_1_in[6]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[4]),
        .O(i___0_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_3__0
       (.I0(p_1_in[6]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .O(i___0_carry_i_3__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_4
       (.I0(p_1_in[5]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[3]),
        .O(i___0_carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_4__0
       (.I0(p_1_in[5]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .O(i___0_carry_i_4__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_5
       (.I0(p_1_in[4]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[2]),
        .O(i___0_carry_i_5_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_5__0
       (.I0(p_1_in[4]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(i___0_carry_i_5__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_6
       (.I0(p_1_in[3]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[1]),
        .O(i___0_carry_i_6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_6__0
       (.I0(p_1_in[3]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(i___0_carry_i_6__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_7
       (.I0(p_1_in[2]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[0]),
        .O(i___0_carry_i_7_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_7__0
       (.I0(p_1_in[2]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .O(i___0_carry_i_7__0_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1
       (.I0(p_1_in[7]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(p_1_in[6]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2
       (.I0(p_1_in[5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(p_1_in[4]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3
       (.I0(p_1_in[3]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(p_1_in[2]),
        .O(i__carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_4
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(i__carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_5
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I1(p_1_in[7]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(p_1_in[6]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(p_1_in[5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(p_1_in[4]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I1(p_1_in[3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(p_1_in[2]),
        .O(i__carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_9
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(i__carry_i_9_n_0));
  FDRE insert3_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(insert3_reg_1),
        .Q(insert3_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT5 #(
    .INIT(32'h30000001)) 
    insert5_i_2
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [0]),
        .O(\s_state_reg[3]_0 ));
  FDRE insert5_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(insert5_reg_1),
        .Q(insert5_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT5 #(
    .INIT(32'hFFFF8001)) 
    kout_i_i_1
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[3]),
        .I3(al_rx_data_out[2]),
        .I4(k1),
        .O(k));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT5 #(
    .INIT(32'h40000002)) 
    kout_i_i_2
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[7]),
        .I4(al_rx_data_out[5]),
        .O(k1));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[4]_i_1 
       (.I0(BaseX_Rx_Q_Out[0]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[4]),
        .O(p_3_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[5]_i_1 
       (.I0(BaseX_Rx_Q_Out[1]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[5]),
        .O(p_3_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[6]_i_1 
       (.I0(BaseX_Rx_Q_Out[2]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[6]),
        .O(p_3_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[7]_i_1 
       (.I0(BaseX_Rx_Q_Out[3]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[7]),
        .O(p_3_out[7]));
  LUT6 #(
    .INIT(64'h0000000000002008)) 
    monitor_late_i_2
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [2]),
        .I5(\s_state_reg[4]_0 [3]),
        .O(\s_state_reg[0]_0 ));
  FDRE monitor_late_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(monitor_late_reg_1),
        .Q(monitor_late_reg_0),
        .R(SR));
  FDRE \monitor_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(monitor[7]),
        .Q(monitor[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[4]),
        .Q(monitor[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[5]),
        .Q(monitor[5]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[6]),
        .Q(monitor[6]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[7]),
        .Q(monitor[7]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'h5555555500000004)) 
    \mpx[0]_i_1 
       (.I0(\mpx[0]_i_2_n_0 ),
        .I1(\mpx[0]_i_3_n_0 ),
        .I2(\mpx[0]_i_4_n_0 ),
        .I3(\mpx[0]_i_5_n_0 ),
        .I4(\mpx[0]_i_6_n_0 ),
        .I5(\mpx[3]_i_3_n_0 ),
        .O(\mpx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \mpx[0]_i_10 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \mpx[0]_i_11 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[2] ),
        .O(\mpx[0]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \mpx[0]_i_12 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(data9[4]),
        .I2(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \mpx[0]_i_13 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mpx[0]_i_14 
       (.I0(data9[6]),
        .I1(data9[5]),
        .O(\mpx[0]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \mpx[0]_i_15 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[4]),
        .O(\mpx[0]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \mpx[0]_i_16 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \mpx[0]_i_17 
       (.I0(data9[5]),
        .I1(data9[2]),
        .I2(data9[3]),
        .O(\mpx[0]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    \mpx[0]_i_18 
       (.I0(data9[4]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[6]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(\mpx[0]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \mpx[0]_i_19 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(\rxdh_reg_n_0_[8] ),
        .I3(data9[4]),
        .O(\mpx[0]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \mpx[0]_i_2 
       (.I0(SR),
        .I1(\mpx[3]_i_4_n_0 ),
        .O(\mpx[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \mpx[0]_i_20 
       (.I0(data9[5]),
        .I1(data9[1]),
        .I2(data9[6]),
        .I3(data9[0]),
        .O(\mpx[0]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \mpx[0]_i_21 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[6] ),
        .I3(data9[2]),
        .O(\mpx[0]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \mpx[0]_i_22 
       (.I0(data9[0]),
        .I1(data9[1]),
        .I2(\rxdh_reg_n_0_[4] ),
        .I3(\rxdh_reg_n_0_[5] ),
        .O(\mpx[0]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \mpx[0]_i_23 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(data9[2]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \mpx[0]_i_24 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[4] ),
        .I3(\rxdh_reg_n_0_[5] ),
        .O(\mpx[0]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFEFAAEFFFEF)) 
    \mpx[0]_i_3 
       (.I0(\mpx[0]_i_7_n_0 ),
        .I1(\mpx[0]_i_8_n_0 ),
        .I2(\mpx[0]_i_9_n_0 ),
        .I3(data9[0]),
        .I4(\mpx[0]_i_10_n_0 ),
        .I5(\mpx[0]_i_11_n_0 ),
        .O(\mpx[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0200020F02000200)) 
    \mpx[0]_i_4 
       (.I0(\mpx[0]_i_12_n_0 ),
        .I1(\mpx[0]_i_13_n_0 ),
        .I2(\mpx[0]_i_14_n_0 ),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(\mpx[0]_i_15_n_0 ),
        .I5(\mpx[0]_i_16_n_0 ),
        .O(\mpx[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[0]_i_5 
       (.I0(\mpx[0]_i_17_n_0 ),
        .I1(\mpx[0]_i_18_n_0 ),
        .I2(\mpx[0]_i_19_n_0 ),
        .I3(\mpx[0]_i_20_n_0 ),
        .I4(data9[7]),
        .I5(data9[8]),
        .O(\mpx[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[0]_i_6 
       (.I0(\mpx[0]_i_21_n_0 ),
        .I1(\mpx[0]_i_22_n_0 ),
        .I2(\mpx[0]_i_23_n_0 ),
        .I3(\mpx[0]_i_24_n_0 ),
        .I4(data9[4]),
        .I5(data9[3]),
        .O(\mpx[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mpx[0]_i_7 
       (.I0(data9[2]),
        .I1(data9[1]),
        .O(\mpx[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \mpx[0]_i_8 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \mpx[0]_i_9 
       (.I0(\rxdh_reg_n_0_[2] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .O(\mpx[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'h01000101)) 
    \mpx[1]_i_1 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_4_n_0),
        .I4(\mpx[1]_i_2_n_0 ),
        .O(\mpx[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000099F9)) 
    \mpx[1]_i_2 
       (.I0(data9[6]),
        .I1(data9[7]),
        .I2(\mpx[1]_i_3_n_0 ),
        .I3(\mpx[1]_i_4_n_0 ),
        .I4(\mpx[0]_i_4_n_0 ),
        .O(\mpx[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \mpx[1]_i_3 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(\rxdh_reg_n_0_[8] ),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(\mpx[0]_i_16_n_0 ),
        .O(\mpx[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'h00000080)) 
    \mpx[1]_i_4 
       (.I0(\mpx[0]_i_19_n_0 ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(data9[5]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(\mpx[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \mpx[2]_i_1 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_2_n_0),
        .I4(Aligned_i_4_n_0),
        .O(\mpx[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBAAAA)) 
    \mpx[3]_i_1 
       (.I0(SR),
        .I1(Aligned_i_2_n_0),
        .I2(Aligned_i_3_n_0),
        .I3(Aligned_i_4_n_0),
        .I4(Rx_Valid_Int_reg_n_0),
        .O(\mpx[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_10 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[2] ),
        .I3(\rxdh_reg_n_0_[7] ),
        .O(\mpx[3]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_11 
       (.I0(\rxdh_reg_n_0_[0] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[2] ),
        .O(\mpx[3]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \mpx[3]_i_2 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_2_n_0),
        .I4(Aligned_i_4_n_0),
        .O(\mpx[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[3]_i_3 
       (.I0(\mpx[3]_i_5_n_0 ),
        .I1(\mpx[3]_i_6_n_0 ),
        .I2(\mpx[3]_i_7_n_0 ),
        .I3(\mpx[3]_i_8_n_0 ),
        .I4(data9[0]),
        .I5(data9[1]),
        .O(\mpx[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[3]_i_4 
       (.I0(\mpx[3]_i_9_n_0 ),
        .I1(\mpx[3]_i_10_n_0 ),
        .I2(\mpx[3]_i_5_n_0 ),
        .I3(\mpx[3]_i_11_n_0 ),
        .I4(\rxdh_reg_n_0_[8] ),
        .I5(data9[0]),
        .O(\mpx[3]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \mpx[3]_i_5 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(\rxdh_reg_n_0_[6] ),
        .O(\mpx[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \mpx[3]_i_6 
       (.I0(\rxdh_reg_n_0_[2] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \mpx[3]_i_7 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[2] ),
        .O(\mpx[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_8 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[5] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[3]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \mpx[3]_i_9 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[0] ),
        .I3(\rxdh_reg_n_0_[1] ),
        .O(\mpx[3]_i_9_n_0 ));
  FDRE \mpx_reg[0] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[0]_i_1_n_0 ),
        .Q(mpx__0[0]),
        .R(1'b0));
  FDRE \mpx_reg[1] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[1]_i_1_n_0 ),
        .Q(mpx__0[1]),
        .R(1'b0));
  FDRE \mpx_reg[2] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[2]_i_1_n_0 ),
        .Q(mpx__0[2]),
        .R(1'b0));
  FDRE \mpx_reg[3] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[3]_i_2_n_0 ),
        .Q(mpx__0[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h96)) 
    \pd_count[0]_i_1 
       (.I0(pd_count[0]),
        .I1(PhaseDet_CntInc[0]),
        .I2(PhaseDet_CntDec[0]),
        .O(\pd_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9669C33CC33C6996)) 
    \pd_count[1]_i_1 
       (.I0(PhaseDet_CntDec[0]),
        .I1(PhaseDet_CntDec[1]),
        .I2(pd_count[1]),
        .I3(PhaseDet_CntInc[1]),
        .I4(PhaseDet_CntInc[0]),
        .I5(pd_count[0]),
        .O(\pd_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT5 #(
    .INIT(32'h96669996)) 
    \pd_count[2]_i_1 
       (.I0(\pd_count[2]_i_2_n_0 ),
        .I1(\pd_count[2]_i_3_n_0 ),
        .I2(pd_count[1]),
        .I3(PhaseDet_CntInc[1]),
        .I4(PhaseDet_CntDec[1]),
        .O(\pd_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h69696900FF696969)) 
    \pd_count[2]_i_2 
       (.I0(PhaseDet_CntInc[1]),
        .I1(pd_count[1]),
        .I2(PhaseDet_CntDec[1]),
        .I3(pd_count[0]),
        .I4(PhaseDet_CntInc[0]),
        .I5(PhaseDet_CntDec[0]),
        .O(\pd_count[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \pd_count[2]_i_3 
       (.I0(PhaseDet_CntDec[2]),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntInc[2]),
        .O(\pd_count[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h69996669)) 
    \pd_count[3]_i_1 
       (.I0(\pd_count[4]_i_2_n_0 ),
        .I1(pd_count[3]),
        .I2(pd_count[2]),
        .I3(PhaseDet_CntInc[2]),
        .I4(PhaseDet_CntDec[2]),
        .O(\pd_count[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F5780A8EAFE1501)) 
    \pd_count[4]_i_1 
       (.I0(\pd_count[4]_i_2_n_0 ),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntInc[2]),
        .I3(PhaseDet_CntDec[2]),
        .I4(pd_count[4]),
        .I5(pd_count[3]),
        .O(\pd_count[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT5 #(
    .INIT(32'hFF696900)) 
    \pd_count[4]_i_2 
       (.I0(PhaseDet_CntInc[2]),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntDec[2]),
        .I3(\pd_count[4]_i_3_n_0 ),
        .I4(\pd_count[2]_i_2_n_0 ),
        .O(\pd_count[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'h8E)) 
    \pd_count[4]_i_3 
       (.I0(pd_count[1]),
        .I1(PhaseDet_CntInc[1]),
        .I2(PhaseDet_CntDec[1]),
        .O(\pd_count[4]_i_3_n_0 ));
  FDRE \pd_count_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[0]_i_1_n_0 ),
        .Q(pd_count[0]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[1]_i_1_n_0 ),
        .Q(pd_count[1]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[2]_i_1_n_0 ),
        .Q(pd_count[2]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[3]_i_1_n_0 ),
        .Q(pd_count[3]),
        .R(pd_ovflw_up));
  FDSE \pd_count_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[4]_i_1_n_0 ),
        .Q(pd_count[4]),
        .S(pd_ovflw_up));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    pd_ovflw_down_i_1
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state[5]_i_3_n_0 ),
        .I3(\s_state_reg[4]_2 ),
        .I4(\delay_change_reg_n_0_[0] ),
        .I5(SR),
        .O(pd_ovflw_up));
  LUT6 #(
    .INIT(64'hFFFFFFFF11111113)) 
    pd_ovflw_down_i_2
       (.I0(pd_count[3]),
        .I1(pd_count[4]),
        .I2(pd_count[2]),
        .I3(pd_count[1]),
        .I4(pd_count[0]),
        .I5(pd_ovflw_down_reg_n_0),
        .O(pd_ovflw_down_i_2_n_0));
  FDRE pd_ovflw_down_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(pd_ovflw_down_i_2_n_0),
        .Q(pd_ovflw_down_reg_n_0),
        .R(pd_ovflw_up));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    pd_ovflw_up_i_1
       (.I0(pd_count[4]),
        .I1(pd_count[3]),
        .I2(pd_ovflw_up_reg_n_0),
        .O(pd_ovflw_up_i_1_n_0));
  FDRE pd_ovflw_up_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(pd_ovflw_up_i_1_n_0),
        .Q(pd_ovflw_up_reg_n_0),
        .R(pd_ovflw_up));
  FDRE \rxdh_reg[0] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[1]),
        .Q(\rxdh_reg_n_0_[0] ),
        .R(SR));
  FDRE \rxdh_reg[10] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[0]),
        .Q(data9[1]),
        .R(SR));
  FDRE \rxdh_reg[11] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[1]),
        .Q(data9[2]),
        .R(SR));
  FDRE \rxdh_reg[12] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[2]),
        .Q(data9[3]),
        .R(SR));
  FDRE \rxdh_reg[13] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[3]),
        .Q(data9[4]),
        .R(SR));
  FDRE \rxdh_reg[14] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[4]),
        .Q(data9[5]),
        .R(SR));
  FDRE \rxdh_reg[15] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[5]),
        .Q(data9[6]),
        .R(SR));
  FDRE \rxdh_reg[16] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[6]),
        .Q(data9[7]),
        .R(SR));
  FDRE \rxdh_reg[17] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[7]),
        .Q(data9[8]),
        .R(SR));
  FDRE \rxdh_reg[18] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[8]),
        .Q(data9[9]),
        .R(SR));
  FDRE \rxdh_reg[19] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[9]),
        .Q(\rxdh_reg_n_0_[19] ),
        .R(SR));
  FDRE \rxdh_reg[1] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[2]),
        .Q(\rxdh_reg_n_0_[1] ),
        .R(SR));
  FDRE \rxdh_reg[2] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[3]),
        .Q(\rxdh_reg_n_0_[2] ),
        .R(SR));
  FDRE \rxdh_reg[3] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[4]),
        .Q(\rxdh_reg_n_0_[3] ),
        .R(SR));
  FDRE \rxdh_reg[4] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[5]),
        .Q(\rxdh_reg_n_0_[4] ),
        .R(SR));
  FDRE \rxdh_reg[5] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[6]),
        .Q(\rxdh_reg_n_0_[5] ),
        .R(SR));
  FDRE \rxdh_reg[6] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[7]),
        .Q(\rxdh_reg_n_0_[6] ),
        .R(SR));
  FDRE \rxdh_reg[7] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[8]),
        .Q(\rxdh_reg_n_0_[7] ),
        .R(SR));
  FDRE \rxdh_reg[8] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[9]),
        .Q(\rxdh_reg_n_0_[8] ),
        .R(SR));
  FDRE \rxdh_reg[9] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(\rxdh_reg_n_0_[19] ),
        .Q(data9[0]),
        .R(SR));
  LUT6 #(
    .INIT(64'h3C3C3C3CC4C4C7C4)) 
    \s_state[0]_i_1 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state[5]_i_6_n_0 ),
        .I2(\s_state[5]_i_9_n_0 ),
        .I3(\s_state[0]_i_2_n_0 ),
        .I4(\s_state[1]_i_2_n_0 ),
        .I5(\s_state[5]_i_8_n_0 ),
        .O(\s_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \s_state[0]_i_2 
       (.I0(ActCnt_EQ_BTval),
        .I1(pd_ovflw_up_reg_n_0),
        .O(\s_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFC02FC32FC32FC02)) 
    \s_state[1]_i_1 
       (.I0(\s_state[1]_i_2_n_0 ),
        .I1(\s_state[5]_i_8_n_0 ),
        .I2(\s_state[5]_i_6_n_0 ),
        .I3(\s_state[5]_i_9_n_0 ),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\s_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \s_state[1]_i_2 
       (.I0(pd_ovflw_up_reg_n_0),
        .I1(ActCnt_EQ_Zero),
        .I2(pd_ovflw_down_reg_n_0),
        .O(\s_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00FFFFFFEF000000)) 
    \s_state[2]_i_1 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [2]),
        .O(\s_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \s_state[3]_i_1 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [0]),
        .O(\s_state[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAA6AA8AAAA)) 
    \s_state[4]_i_1 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\s_state[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    \s_state[5]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state[5]_i_3_n_0 ),
        .I3(\s_state_reg[4]_2 ),
        .I4(pd_ovflw_up_reg_n_0),
        .I5(pd_ovflw_down_reg_n_0),
        .O(s_state));
  LUT5 #(
    .INIT(32'hFF0000B8)) 
    \s_state[5]_i_2 
       (.I0(\s_state[5]_i_5_n_0 ),
        .I1(\s_state[5]_i_6_n_0 ),
        .I2(\s_state[5]_i_7_n_0 ),
        .I3(\s_state[5]_i_8_n_0 ),
        .I4(\s_state[5]_i_9_n_0 ),
        .O(\s_state[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \s_state[5]_i_3 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [2]),
        .O(\s_state[5]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \s_state[5]_i_4 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg_n_0_[5] ),
        .O(\s_state_reg[4]_2 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \s_state[5]_i_5 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [3]),
        .I5(\s_state_reg[4]_0 [4]),
        .O(\s_state[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFDDBE)) 
    \s_state[5]_i_6 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [2]),
        .I5(\s_state_reg[4]_0 [3]),
        .O(\s_state[5]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT4 #(
    .INIT(16'h550C)) 
    \s_state[5]_i_7 
       (.I0(ActCnt_EQ_BTval),
        .I1(pd_ovflw_down_reg_n_0),
        .I2(ActCnt_EQ_Zero),
        .I3(pd_ovflw_up_reg_n_0),
        .O(\s_state[5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT5 #(
    .INIT(32'h00030100)) 
    \s_state[5]_i_8 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [4]),
        .I4(\s_state_reg_n_0_[5] ),
        .O(\s_state[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0040010101010100)) 
    \s_state[5]_i_9 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [2]),
        .I2(\s_state_reg[4]_0 [3]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\s_state[5]_i_9_n_0 ));
  FDRE \s_state_reg[0] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[0]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [0]),
        .R(SR));
  FDRE \s_state_reg[1] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[1]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [1]),
        .R(SR));
  FDRE \s_state_reg[2] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[2]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [2]),
        .R(SR));
  FDRE \s_state_reg[3] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[3]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [3]),
        .R(SR));
  FDRE \s_state_reg[4] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[4]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [4]),
        .R(SR));
  FDRE \s_state_reg[5] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[5]_i_2_n_0 ),
        .Q(\s_state_reg_n_0_[5] ),
        .R(SR));
  LUT3 #(
    .INIT(8'h56)) 
    \toggle[0]_i_1 
       (.I0(\toggle_reg_n_0_[0] ),
        .I1(insert3_reg_0),
        .I2(insert5_reg_0),
        .O(\toggle[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9D409D9D62229D62)) 
    \toggle[1]_i_1 
       (.I0(insert3_reg_0),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(insert5_reg_0),
        .I3(p_0_in0),
        .I4(\toggle_reg_n_0_[2] ),
        .I5(\toggle_reg_n_0_[1] ),
        .O(\toggle[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0080DD3D3300CCDD)) 
    \toggle[2]_i_1 
       (.I0(p_0_in0),
        .I1(\toggle_reg_n_0_[1] ),
        .I2(insert5_reg_0),
        .I3(insert3_reg_0),
        .I4(\toggle_reg_n_0_[2] ),
        .I5(\toggle_reg_n_0_[0] ),
        .O(\toggle[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h522852281042500A)) 
    \toggle[3]_i_1 
       (.I0(\toggle_reg_n_0_[2] ),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(\toggle_reg_n_0_[1] ),
        .I3(p_0_in0),
        .I4(insert5_reg_0),
        .I5(insert3_reg_0),
        .O(\toggle[3]_i_1_n_0 ));
  FDRE \toggle_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[0]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[0] ),
        .R(SR));
  FDRE \toggle_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[1]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[1] ),
        .R(SR));
  FDRE \toggle_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[2]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[2] ),
        .R(SR));
  FDRE \toggle_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[3]_i_1_n_0 ),
        .Q(p_0_in0),
        .R(SR));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sgmii_adapt
   (sgmii_clk_r_0,
    sgmii_clk_en,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_txd_out,
    gmii_tx_en_out,
    gmii_tx_er_out,
    sgmii_clk_f_0,
    Tx_WrClk,
    mgt_tx_reset,
    speed_is_10_100_0,
    speed_is_100_0,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er_in,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0);
  output sgmii_clk_r_0;
  output sgmii_clk_en;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output [7:0]gmii_txd_out;
  output gmii_tx_en_out;
  output gmii_tx_er_out;
  output sgmii_clk_f_0;
  input Tx_WrClk;
  input mgt_tx_reset;
  input speed_is_10_100_0;
  input speed_is_100_0;
  input [7:0]gmii_rxd;
  input gmii_rx_dv;
  input gmii_rx_er_in;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;

  wire Tx_WrClk;
  wire gmii_rx_dv;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire gmii_rx_er_in;
  wire [7:0]gmii_rxd;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_en_out;
  wire gmii_tx_er_0;
  wire gmii_tx_er_out;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_out;
  wire mgt_tx_reset;
  wire sgmii_clk_en;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire speed_is_100_0;
  wire speed_is_100_resync;
  wire speed_is_10_100_0;
  wire speed_is_10_100_resync;
  wire sync_reset;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_clk_gen clock_generation
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_100_resync),
        .reset_out(sync_reset),
        .sgmii_clk_en_reg_0(sgmii_clk_en),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .speed_is_10_100_fall_reg_0(speed_is_10_100_resync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_0 gen_sync_reset
       (.Tx_WrClk(Tx_WrClk),
        .mgt_tx_reset(mgt_tx_reset),
        .reset_out(sync_reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_rate_adapt receiver
       (.Tx_WrClk(Tx_WrClk),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rx_er_in(gmii_rx_er_in),
        .gmii_rx_er_out_reg_0(sgmii_clk_en),
        .gmii_rxd(gmii_rxd),
        .gmii_rxd_0(gmii_rxd_0),
        .reset_out(sync_reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block resync_speed_100
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_100_resync),
        .speed_is_100_0(speed_is_100_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_1 resync_speed_10_100
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_10_100_resync),
        .speed_is_10_100_0(speed_is_10_100_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_rate_adapt transmitter
       (.E(sgmii_clk_en),
        .Tx_WrClk(Tx_WrClk),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_en_out(gmii_tx_en_out),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_tx_er_out(gmii_tx_er_out),
        .gmii_txd_0(gmii_txd_0),
        .gmii_txd_out(gmii_txd_out),
        .reset_out(sync_reset));
endmodule

(* DowngradeIPIdentifiedWarnings = "yes" *) (* EXAMPLE_SIMULATION = "0" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_support
   (txp_0,
    txn_0,
    rxp_0,
    rxn_0,
    signal_detect_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_isolate_0,
    sgmii_clk_r_0,
    sgmii_clk_f_0,
    sgmii_clk_en_0,
    speed_is_10_100_0,
    speed_is_100_0,
    an_interrupt_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    status_vector_0,
    configuration_vector_0,
    refclk625_p,
    refclk625_n,
    clk125_out,
    clk312_out,
    rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    rx_locked,
    tx_locked,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    tx_pll_clk_out,
    rx_pll_clk_out,
    tx_rdclk_out,
    riu_rddata_3,
    riu_valid_3,
    riu_prsnt_3,
    riu_rddata_2,
    riu_valid_2,
    riu_prsnt_2,
    riu_rddata_1,
    riu_valid_1,
    riu_prsnt_1,
    rx_btval_3,
    rx_btval_2,
    rx_btval_1,
    tx_dly_rdy_1,
    rx_dly_rdy_1,
    rx_vtc_rdy_1,
    tx_vtc_rdy_1,
    tx_dly_rdy_2,
    rx_dly_rdy_2,
    rx_vtc_rdy_2,
    tx_vtc_rdy_2,
    tx_dly_rdy_3,
    rx_dly_rdy_3,
    rx_vtc_rdy_3,
    tx_vtc_rdy_3,
    reset);
  output txp_0;
  output txn_0;
  input rxp_0;
  input rxn_0;
  input signal_detect_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output gmii_isolate_0;
  output sgmii_clk_r_0;
  output sgmii_clk_f_0;
  output sgmii_clk_en_0;
  input speed_is_10_100_0;
  input speed_is_100_0;
  output an_interrupt_0;
  input [15:0]an_adv_config_vector_0;
  input an_restart_config_0;
  output [15:0]status_vector_0;
  input [4:0]configuration_vector_0;
  input refclk625_p;
  input refclk625_n;
  output clk125_out;
  output clk312_out;
  output rst_125_out;
  output tx_logic_reset;
  output rx_logic_reset;
  output rx_locked;
  output tx_locked;
  output tx_bsc_rst_out;
  output rx_bsc_rst_out;
  output tx_bs_rst_out;
  output rx_bs_rst_out;
  output tx_rst_dly_out;
  output rx_rst_dly_out;
  output tx_bsc_en_vtc_out;
  output rx_bsc_en_vtc_out;
  output tx_bs_en_vtc_out;
  output rx_bs_en_vtc_out;
  output riu_clk_out;
  output [5:0]riu_addr_out;
  output [15:0]riu_wr_data_out;
  output riu_wr_en_out;
  output [1:0]riu_nibble_sel_out;
  output tx_pll_clk_out;
  output rx_pll_clk_out;
  output tx_rdclk_out;
  input [15:0]riu_rddata_3;
  input riu_valid_3;
  input riu_prsnt_3;
  input [15:0]riu_rddata_2;
  input riu_valid_2;
  input riu_prsnt_2;
  input [15:0]riu_rddata_1;
  input riu_valid_1;
  input riu_prsnt_1;
  output [8:0]rx_btval_3;
  output [8:0]rx_btval_2;
  output [8:0]rx_btval_1;
  input tx_dly_rdy_1;
  input rx_dly_rdy_1;
  input rx_vtc_rdy_1;
  input tx_vtc_rdy_1;
  input tx_dly_rdy_2;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_2;
  input tx_vtc_rdy_2;
  input tx_dly_rdy_3;
  input rx_dly_rdy_3;
  input rx_vtc_rdy_3;
  input tx_vtc_rdy_3;
  input reset;

  wire \<const0> ;
  wire [15:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire clk125_out;
  wire clk312_out;
  wire [4:0]configuration_vector_0;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_er_0;
  wire [7:0]gmii_txd_0;
  wire refclk625_n;
  wire refclk625_p;
  wire reset;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt;
  wire riu_prsnt_1;
  wire riu_prsnt_2;
  wire riu_prsnt_3;
  wire [15:0]riu_rd_data;
  wire [15:0]riu_rddata_1;
  wire [15:0]riu_rddata_2;
  wire [15:0]riu_rddata_3;
  wire riu_valid;
  wire riu_valid_1;
  wire riu_valid_2;
  wire riu_valid_3;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rst_125_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire [8:0]rx_btval;
  wire [8:0]rx_btval_1;
  wire [8:0]rx_btval_2;
  wire [8:0]rx_btval_3;
  wire rx_dly_rdy;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire rx_locked;
  wire rx_logic_reset;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  wire rxn_0;
  wire rxp_0;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [13:0]\^status_vector_0 ;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire tx_dly_rdy;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_locked;
  wire tx_logic_reset;
  wire tx_pll_clk_out;
  wire tx_rdclk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  wire txn_0;
  wire txp_0;
  wire NLW_clock_reset_i_ClockIn_se_out_UNCONNECTED;
  wire [7:0]NLW_clock_reset_i_Debug_Out_UNCONNECTED;

  assign status_vector_0[15] = \<const0> ;
  assign status_vector_0[14] = \<const0> ;
  assign status_vector_0[13:9] = \^status_vector_0 [13:9];
  assign status_vector_0[8] = \<const0> ;
  assign status_vector_0[7:0] = \^status_vector_0 [7:0];
  GND GND
       (.G(\<const0> ));
  (* C_IoBank = "44" *) 
  (* C_Part = "XCKU060" *) 
  (* DONT_TOUCH *) 
  (* EXAMPLE_SIMULATION = "0" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_Clock_Reset clock_reset_i
       (.ClockIn_n(refclk625_n),
        .ClockIn_p(refclk625_p),
        .ClockIn_se_out(NLW_clock_reset_i_ClockIn_se_out_UNCONNECTED),
        .Debug_Out(NLW_clock_reset_i_Debug_Out_UNCONNECTED[7:0]),
        .ResetIn(reset),
        .Riu_Addr(riu_addr_out),
        .Riu_Nibble_Sel(riu_nibble_sel_out),
        .Riu_Prsnt_0(riu_prsnt),
        .Riu_Prsnt_1(riu_prsnt_1),
        .Riu_Prsnt_2(riu_prsnt_2),
        .Riu_Prsnt_3(riu_prsnt_3),
        .Riu_RdData_0(riu_rd_data),
        .Riu_RdData_1(riu_rddata_1),
        .Riu_RdData_2(riu_rddata_2),
        .Riu_RdData_3(riu_rddata_3),
        .Riu_Valid_0(riu_valid),
        .Riu_Valid_1(riu_valid_1),
        .Riu_Valid_2(riu_valid_2),
        .Riu_Valid_3(riu_valid_3),
        .Riu_WrData(riu_wr_data_out),
        .Riu_Wr_En(riu_wr_en_out),
        .Rx_Bs_EnVtc(rx_bs_en_vtc_out),
        .Rx_Bs_Rst(rx_bs_rst_out),
        .Rx_Bs_RstDly(rx_rst_dly_out),
        .Rx_Bsc_EnVtc(rx_bsc_en_vtc_out),
        .Rx_Bsc_Rst(rx_bsc_rst_out),
        .Rx_BtVal_0(rx_btval),
        .Rx_BtVal_1(rx_btval_1),
        .Rx_BtVal_2(rx_btval_2),
        .Rx_BtVal_3(rx_btval_3),
        .Rx_ClkOutPhy(rx_pll_clk_out),
        .Rx_Dly_Rdy(rx_dly_rdy),
        .Rx_Locked(rx_locked),
        .Rx_LogicRst(rx_logic_reset),
        .Rx_RiuClk(riu_clk_out),
        .Rx_SysClk(clk312_out),
        .Rx_Vtc_Rdy(rx_vtc_rdy),
        .Tx_Bs_EnVtc(tx_bs_en_vtc_out),
        .Tx_Bs_Rst(tx_bs_rst_out),
        .Tx_Bs_RstDly(tx_rst_dly_out),
        .Tx_Bsc_EnVtc(tx_bsc_en_vtc_out),
        .Tx_Bsc_Rst(tx_bsc_rst_out),
        .Tx_ClkOutPhy(tx_pll_clk_out),
        .Tx_Dly_Rdy(tx_dly_rdy),
        .Tx_Locked(tx_locked),
        .Tx_LogicRst(tx_logic_reset),
        .Tx_SysClk(tx_rdclk_out),
        .Tx_Vtc_Rdy(tx_vtc_rdy),
        .Tx_WrClk(clk125_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_block pcs_pma_block_i
       (.CLK(tx_rdclk_out),
        .D(rx_btval[8:3]),
        .Rx_Dly_Rdy(rx_dly_rdy),
        .Rx_SysClk(clk312_out),
        .Rx_Vtc_Rdy(rx_vtc_rdy),
        .Tx_Dly_Rdy(tx_dly_rdy),
        .Tx_Vtc_Rdy(tx_vtc_rdy),
        .Tx_WrClk(clk125_out),
        .an_adv_config_vector_0(an_adv_config_vector_0[11]),
        .an_interrupt_0(an_interrupt_0),
        .an_restart_config_0(an_restart_config_0),
        .configuration_vector_0(configuration_vector_0),
        .gmii_isolate_0(gmii_isolate_0),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_txd_0(gmii_txd_0),
        .reset_out(rst_125_out),
        .riu_addr_out(riu_addr_out),
        .riu_clk_out(riu_clk_out),
        .riu_nibble_sel_out(riu_nibble_sel_out),
        .riu_prsnt(riu_prsnt),
        .riu_rd_data(riu_rd_data),
        .riu_valid(riu_valid),
        .riu_wr_data_out(riu_wr_data_out),
        .riu_wr_en_out(riu_wr_en_out),
        .rx_bs_en_vtc_out(rx_bs_en_vtc_out),
        .rx_bs_rst_out(rx_bs_rst_out),
        .rx_bsc_en_vtc_out(rx_bsc_en_vtc_out),
        .rx_bsc_rst_out(rx_bsc_rst_out),
        .rx_dly_rdy_1(rx_dly_rdy_1),
        .rx_dly_rdy_2(rx_dly_rdy_2),
        .rx_dly_rdy_3(rx_dly_rdy_3),
        .rx_pll_clk_out(rx_pll_clk_out),
        .rx_rst_dly_out(rx_rst_dly_out),
        .rx_vtc_rdy_1(rx_vtc_rdy_1),
        .rx_vtc_rdy_2(rx_vtc_rdy_2),
        .rx_vtc_rdy_3(rx_vtc_rdy_3),
        .rxn_0(rxn_0),
        .rxp_0(rxp_0),
        .sgmii_clk_en_0(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .signal_detect_0(signal_detect_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0),
        .status_vector_0({\^status_vector_0 [13:9],\^status_vector_0 [7:0]}),
        .tx_bs_en_vtc_out(tx_bs_en_vtc_out),
        .tx_bs_rst_out(tx_bs_rst_out),
        .tx_bsc_en_vtc_out(tx_bsc_en_vtc_out),
        .tx_bsc_rst_out(tx_bsc_rst_out),
        .tx_dly_rdy_1(tx_dly_rdy_1),
        .tx_dly_rdy_2(tx_dly_rdy_2),
        .tx_dly_rdy_3(tx_dly_rdy_3),
        .tx_pll_clk_out(tx_pll_clk_out),
        .tx_rst_dly_out(tx_rst_dly_out),
        .tx_vtc_rdy_1(tx_vtc_rdy_1),
        .tx_vtc_rdy_2(tx_vtc_rdy_2),
        .tx_vtc_rdy_3(tx_vtc_rdy_3),
        .txn_0(txn_0),
        .txp_0(txp_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync reset_sync_clk125_i
       (.Tx_WrClk(clk125_out),
        .rst_125_out(rst_125_out),
        .rx_logic_reset(rx_logic_reset),
        .tx_logic_reset(tx_logic_reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block
   (data_out,
    speed_is_100_0,
    Tx_WrClk);
  output data_out;
  input speed_is_100_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire speed_is_100_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_100_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_1
   (data_out,
    speed_is_10_100_0,
    Tx_WrClk);
  output data_out;
  input speed_is_10_100_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire speed_is_10_100_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_10_100_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_10
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    \wr_occupancy_reg[6]_1 ,
    \wr_occupancy_reg[6]_2 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input \wr_occupancy_reg[6]_1 ;
  input \wr_occupancy_reg[6]_2 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;
  wire \wr_occupancy_reg[6]_1 ;
  wire \wr_occupancy_reg[6]_2 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    wr_occupancy0_carry_i_5
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .I4(\wr_occupancy_reg[6]_1 ),
        .I5(\wr_occupancy_reg[6]_2 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_11
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    \wr_occupancy_reg[6]_1 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input \wr_occupancy_reg[6]_1 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;
  wire \wr_occupancy_reg[6]_1 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h69969669)) 
    wr_occupancy0_carry_i_4
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .I4(\wr_occupancy_reg[6]_1 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_12
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9669)) 
    wr_occupancy0_carry_i_3
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_13
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    data_in,
    Rx_SysClk);
  output [1:0]S;
  output data_out;
  input [1:0]Q;
  input \wr_occupancy_reg[6] ;
  input data_in;
  input Rx_SysClk;

  wire [1:0]Q;
  wire Rx_SysClk;
  wire [1:0]S;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire \wr_occupancy_reg[6] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    wr_occupancy0_carry_i_1
       (.I0(Q[1]),
        .I1(data_out),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    wr_occupancy0_carry_i_2
       (.I0(Q[0]),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_14
   (data_out,
    Q,
    Tx_WrClk);
  output data_out;
  input [0:0]Q;
  input Tx_WrClk;

  wire [0:0]Q;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_15
   (S,
    data_sync_reg6_0,
    rd_wr_addr,
    Q,
    data_out,
    data_sync_reg1_0,
    Tx_WrClk);
  output [1:0]S;
  output data_sync_reg6_0;
  input [0:0]rd_wr_addr;
  input [1:0]Q;
  input data_out;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [1:0]Q;
  wire [1:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire data_sync_reg6_0;
  wire [0:0]rd_wr_addr;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_sync_reg6_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    rd_occupancy0_carry_i_12
       (.I0(rd_wr_addr),
        .I1(Q[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    rd_occupancy0_carry_i_13
       (.I0(rd_wr_addr),
        .I1(data_out),
        .I2(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_16
   (data_out,
    Q,
    Tx_WrClk);
  output data_out;
  input [0:0]Q;
  input Tx_WrClk;

  wire [0:0]Q;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_17
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    \rd_occupancy_reg[6]_1 ,
    \rd_occupancy_reg[6]_2 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input \rd_occupancy_reg[6]_1 ;
  input \rd_occupancy_reg[6]_2 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;
  wire \rd_occupancy_reg[6]_1 ;
  wire \rd_occupancy_reg[6]_2 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    rd_occupancy0_carry_i_11
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(\rd_occupancy_reg[6]_1 ),
        .I4(\rd_occupancy_reg[6]_2 ),
        .I5(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_18
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    \rd_occupancy_reg[6]_1 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input \rd_occupancy_reg[6]_1 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;
  wire \rd_occupancy_reg[6]_1 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h69969669)) 
    rd_occupancy0_carry_i_10
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(\rd_occupancy_reg[6]_1 ),
        .I4(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_19
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9669)) 
    rd_occupancy0_carry_i_9
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_20
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [1:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input [1:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [1:0]Q;
  wire [1:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    rd_occupancy0_carry_i_7
       (.I0(data_out),
        .I1(Q[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    rd_occupancy0_carry_i_8
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_21
   (data_out,
    data_sync_reg1_0,
    Tx_WrClk);
  output data_out;
  input data_sync_reg1_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_7
   (data_out,
    Q,
    Rx_SysClk);
  output data_out;
  input [0:0]Q;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_8
   (S,
    Q,
    data_out,
    wr_occupancy0_carry_i_7_0,
    wr_occupancy0_carry_i_7_1,
    wr_occupancy0_carry_i_7_2,
    wr_occupancy0_carry_i_7_3,
    \wr_occupancy_reg[6] ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [1:0]S;
  input [1:0]Q;
  input data_out;
  input wr_occupancy0_carry_i_7_0;
  input wr_occupancy0_carry_i_7_1;
  input wr_occupancy0_carry_i_7_2;
  input wr_occupancy0_carry_i_7_3;
  input \wr_occupancy_reg[6] ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [1:0]Q;
  wire Rx_SysClk;
  wire [1:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire p_8_in;
  wire wr_occupancy0_carry_i_7_0;
  wire wr_occupancy0_carry_i_7_1;
  wire wr_occupancy0_carry_i_7_2;
  wire wr_occupancy0_carry_i_7_3;
  wire \wr_occupancy_reg[6] ;
  wire wr_rd_addr_gray_1;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(wr_rd_addr_gray_1),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    wr_occupancy0_carry_i_6
       (.I0(Q[1]),
        .I1(p_8_in),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    wr_occupancy0_carry_i_7
       (.I0(Q[0]),
        .I1(p_8_in),
        .I2(\wr_occupancy_reg[6] ),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    wr_occupancy0_carry_i_8
       (.I0(wr_rd_addr_gray_1),
        .I1(data_out),
        .I2(wr_occupancy0_carry_i_7_0),
        .I3(wr_occupancy0_carry_i_7_1),
        .I4(wr_occupancy0_carry_i_7_2),
        .I5(wr_occupancy0_carry_i_7_3),
        .O(p_8_in));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_9
   (data_out,
    Q,
    Rx_SysClk);
  output data_out;
  input [0:0]Q;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_rate_adapt
   (gmii_tx_en_out,
    gmii_tx_er_out,
    gmii_txd_out,
    reset_out,
    E,
    gmii_tx_en_0,
    Tx_WrClk,
    gmii_tx_er_0,
    gmii_txd_0);
  output gmii_tx_en_out;
  output gmii_tx_er_out;
  output [7:0]gmii_txd_out;
  input reset_out;
  input [0:0]E;
  input gmii_tx_en_0;
  input Tx_WrClk;
  input gmii_tx_er_0;
  input [7:0]gmii_txd_0;

  wire [0:0]E;
  wire Tx_WrClk;
  wire gmii_tx_en_0;
  wire gmii_tx_en_out;
  wire gmii_tx_er_0;
  wire gmii_tx_er_out;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_out;
  wire reset_out;

  FDRE #(
    .INIT(1'b0)) 
    gmii_tx_en_out_reg
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_tx_en_0),
        .Q(gmii_tx_en_out),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    gmii_tx_er_out_reg
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_tx_er_0),
        .Q(gmii_tx_er_out),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[0] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[0]),
        .Q(gmii_txd_out[0]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[1] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[1]),
        .Q(gmii_txd_out[1]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[2] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[2]),
        .Q(gmii_txd_out[2]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[3] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[3]),
        .Q(gmii_txd_out[3]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[4] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[4]),
        .Q(gmii_txd_out[4]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[5] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[5]),
        .Q(gmii_txd_out[5]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[6] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[6]),
        .Q(gmii_txd_out[6]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[7] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[7]),
        .Q(gmii_txd_out[7]),
        .R(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_ten_to_eight
   (Q,
    Tx_WrClk,
    tx_data_10b,
    CLK,
    reset_out);
  output [7:0]Q;
  input Tx_WrClk;
  input [9:0]tx_data_10b;
  input CLK;
  input reset_out;

  wire CLK;
  wire \DataOut[0]_i_1_n_0 ;
  wire \DataOut[0]_i_2_n_0 ;
  wire \DataOut[1]_i_1_n_0 ;
  wire \DataOut[1]_i_2_n_0 ;
  wire \DataOut[2]_i_1_n_0 ;
  wire \DataOut[2]_i_2_n_0 ;
  wire \DataOut[3]_i_1_n_0 ;
  wire \DataOut[3]_i_2_n_0 ;
  wire \DataOut[4]_i_1_n_0 ;
  wire \DataOut[4]_i_2_n_0 ;
  wire \DataOut[5]_i_1_n_0 ;
  wire \DataOut[5]_i_2_n_0 ;
  wire \DataOut[6]_i_1_n_0 ;
  wire \DataOut[6]_i_2_n_0 ;
  wire \DataOut[7]_i_1_n_0 ;
  wire \DataOut[7]_i_2_n_0 ;
  wire \FSM_sequential_IntState[1]_i_1_n_0 ;
  wire \FSM_sequential_IntState[2]_i_1_n_0 ;
  wire \IntLastOut_reg_n_0_[4] ;
  wire \IntLastOut_reg_n_0_[5] ;
  wire \IntLastOut_reg_n_0_[6] ;
  wire \IntLastOut_reg_n_0_[7] ;
  wire [9:0]IntRamOut;
  wire IntRdAddr;
  wire [3:0]IntRdAddr_reg;
  (* async_reg = "true" *) wire [1:0]IntRdEna_Sync;
  wire IntRdEna_i_1_n_0;
  wire IntState;
  wire [2:0]IntState__0;
  wire [2:0]IntState__1;
  wire [3:0]IntWrAddr_reg;
  wire [7:0]Q;
  (* async_reg = "true" *) wire [1:0]Reset_Sync;
  wire Tx_WrClk;
  wire [1:0]in3;
  wire [0:0]p_0_in;
  wire [3:0]p_0_in__2;
  wire [3:0]p_0_in__4;
  wire reset_out;
  wire [9:0]tx_data_10b;
  wire [1:0]NLW_FIFO_ram_inst0_DOD_UNCONNECTED;
  wire [1:0]NLW_FIFO_ram_inst1_DOC_UNCONNECTED;
  wire [1:0]NLW_FIFO_ram_inst1_DOD_UNCONNECTED;

  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[0]_i_1 
       (.I0(IntRamOut[2]),
        .I1(IntState__0[2]),
        .I2(\DataOut[0]_i_2_n_0 ),
        .O(\DataOut[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[0]_i_2 
       (.I0(\IntLastOut_reg_n_0_[4] ),
        .I1(\IntLastOut_reg_n_0_[6] ),
        .I2(IntState__0[1]),
        .I3(in3[0]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[0]),
        .O(\DataOut[0]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[1]_i_1 
       (.I0(IntRamOut[3]),
        .I1(IntState__0[2]),
        .I2(\DataOut[1]_i_2_n_0 ),
        .O(\DataOut[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[1]_i_2 
       (.I0(\IntLastOut_reg_n_0_[5] ),
        .I1(\IntLastOut_reg_n_0_[7] ),
        .I2(IntState__0[1]),
        .I3(in3[1]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[1]),
        .O(\DataOut[1]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[2]_i_1 
       (.I0(IntRamOut[4]),
        .I1(IntState__0[2]),
        .I2(\DataOut[2]_i_2_n_0 ),
        .O(\DataOut[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[2]_i_2 
       (.I0(\IntLastOut_reg_n_0_[6] ),
        .I1(in3[0]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[0]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[2]),
        .O(\DataOut[2]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[3]_i_1 
       (.I0(IntRamOut[5]),
        .I1(IntState__0[2]),
        .I2(\DataOut[3]_i_2_n_0 ),
        .O(\DataOut[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[3]_i_2 
       (.I0(\IntLastOut_reg_n_0_[7] ),
        .I1(in3[1]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[1]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[3]),
        .O(\DataOut[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[4]_i_1 
       (.I0(IntRamOut[6]),
        .I1(IntState__0[2]),
        .I2(\DataOut[4]_i_2_n_0 ),
        .O(\DataOut[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[4]_i_2 
       (.I0(in3[0]),
        .I1(IntRamOut[0]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[2]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[4]),
        .O(\DataOut[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[5]_i_1 
       (.I0(IntRamOut[7]),
        .I1(IntState__0[2]),
        .I2(\DataOut[5]_i_2_n_0 ),
        .O(\DataOut[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[5]_i_2 
       (.I0(in3[1]),
        .I1(IntRamOut[1]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[3]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[5]),
        .O(\DataOut[5]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[6]_i_1 
       (.I0(IntRamOut[8]),
        .I1(IntState__0[2]),
        .I2(\DataOut[6]_i_2_n_0 ),
        .O(\DataOut[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[6]_i_2 
       (.I0(IntRamOut[0]),
        .I1(IntRamOut[2]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[4]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[6]),
        .O(\DataOut[6]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[7]_i_1 
       (.I0(IntRamOut[9]),
        .I1(IntState__0[2]),
        .I2(\DataOut[7]_i_2_n_0 ),
        .O(\DataOut[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[7]_i_2 
       (.I0(IntRamOut[1]),
        .I1(IntRamOut[3]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[5]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[7]),
        .O(\DataOut[7]_i_2_n_0 ));
  FDSE \DataOut_reg[0] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[0]_i_1_n_0 ),
        .Q(Q[0]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[1] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[1]_i_1_n_0 ),
        .Q(Q[1]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[2] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[2]_i_1_n_0 ),
        .Q(Q[2]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[3] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[3]_i_1_n_0 ),
        .Q(Q[3]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[4] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[4]_i_1_n_0 ),
        .Q(Q[4]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[5] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[5]_i_1_n_0 ),
        .Q(Q[5]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[6] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[6]_i_1_n_0 ),
        .Q(Q[6]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[7] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[7]_i_1_n_0 ),
        .Q(Q[7]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* box_type = "PRIMITIVE" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b0)) 
    FIFO_ram_inst0
       (.ADDRA({1'b0,IntRdAddr_reg}),
        .ADDRB({1'b0,IntRdAddr_reg}),
        .ADDRC({1'b0,IntRdAddr_reg}),
        .ADDRD({1'b0,IntWrAddr_reg}),
        .DIA(tx_data_10b[1:0]),
        .DIB(tx_data_10b[3:2]),
        .DIC(tx_data_10b[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(IntRamOut[1:0]),
        .DOB(IntRamOut[3:2]),
        .DOC(IntRamOut[5:4]),
        .DOD(NLW_FIFO_ram_inst0_DOD_UNCONNECTED[1:0]),
        .WCLK(Tx_WrClk),
        .WE(1'b1));
  (* box_type = "PRIMITIVE" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b0)) 
    FIFO_ram_inst1
       (.ADDRA({1'b0,IntRdAddr_reg}),
        .ADDRB({1'b0,IntRdAddr_reg}),
        .ADDRC({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ADDRD({1'b0,IntWrAddr_reg}),
        .DIA(tx_data_10b[7:6]),
        .DIB(tx_data_10b[9:8]),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(IntRamOut[7:6]),
        .DOB(IntRamOut[9:8]),
        .DOC(NLW_FIFO_ram_inst1_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_FIFO_ram_inst1_DOD_UNCONNECTED[1:0]),
        .WCLK(Tx_WrClk),
        .WE(1'b1));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_IntState[0]_i_1 
       (.I0(IntState__0[0]),
        .I1(IntState__0[2]),
        .O(IntState__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \FSM_sequential_IntState[1]_i_1 
       (.I0(IntState__0[1]),
        .I1(IntState__0[0]),
        .I2(IntState__0[2]),
        .O(\FSM_sequential_IntState[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_IntState[2]_i_1 
       (.I0(Reset_Sync[1]),
        .I1(IntRdEna_Sync[1]),
        .O(\FSM_sequential_IntState[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h57)) 
    \FSM_sequential_IntState[2]_i_2 
       (.I0(IntState__0[2]),
        .I1(IntState__0[1]),
        .I2(IntState__0[0]),
        .O(IntState));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_IntState[2]_i_3 
       (.I0(IntState__0[2]),
        .I1(IntState__0[0]),
        .I2(IntState__0[1]),
        .O(IntState__1[2]));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[0] 
       (.C(CLK),
        .CE(IntState),
        .D(IntState__1[0]),
        .Q(IntState__0[0]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[1] 
       (.C(CLK),
        .CE(IntState),
        .D(\FSM_sequential_IntState[1]_i_1_n_0 ),
        .Q(IntState__0[1]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[2] 
       (.C(CLK),
        .CE(IntState),
        .D(IntState__1[2]),
        .Q(IntState__0[2]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntLastOut_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[4]),
        .Q(\IntLastOut_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[5]),
        .Q(\IntLastOut_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[6]),
        .Q(\IntLastOut_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[7]),
        .Q(\IntLastOut_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[8]),
        .Q(in3[0]),
        .R(1'b0));
  FDRE \IntLastOut_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[9]),
        .Q(in3[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \IntRdAddr[0]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .O(p_0_in__4[0]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \IntRdAddr[1]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .I1(IntRdAddr_reg[1]),
        .O(p_0_in__4[1]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \IntRdAddr[2]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .I1(IntRdAddr_reg[1]),
        .I2(IntRdAddr_reg[2]),
        .O(p_0_in__4[2]));
  LUT3 #(
    .INIT(8'h17)) 
    \IntRdAddr[3]_i_1 
       (.I0(IntState__0[1]),
        .I1(IntState__0[2]),
        .I2(IntState__0[0]),
        .O(IntRdAddr));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \IntRdAddr[3]_i_2 
       (.I0(IntRdAddr_reg[1]),
        .I1(IntRdAddr_reg[0]),
        .I2(IntRdAddr_reg[2]),
        .I3(IntRdAddr_reg[3]),
        .O(p_0_in__4[3]));
  FDRE \IntRdAddr_reg[0] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[0]),
        .Q(IntRdAddr_reg[0]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[1] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[1]),
        .Q(IntRdAddr_reg[1]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[2] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[2]),
        .Q(IntRdAddr_reg[2]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[3] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[3]),
        .Q(IntRdAddr_reg[3]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntRdEna_Sync_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(IntRdEna_Sync[0]),
        .R(Reset_Sync[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntRdEna_Sync_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRdEna_Sync[0]),
        .Q(IntRdEna_Sync[1]),
        .R(Reset_Sync[1]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    IntRdEna_i_1
       (.I0(IntWrAddr_reg[2]),
        .I1(IntWrAddr_reg[3]),
        .I2(IntWrAddr_reg[1]),
        .I3(IntWrAddr_reg[0]),
        .I4(p_0_in),
        .O(IntRdEna_i_1_n_0));
  FDRE IntRdEna_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(IntRdEna_i_1_n_0),
        .Q(p_0_in),
        .R(reset_out));
  LUT1 #(
    .INIT(2'h1)) 
    \IntWrAddr[0]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \IntWrAddr[1]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .I1(IntWrAddr_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \IntWrAddr[2]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .I1(IntWrAddr_reg[1]),
        .I2(IntWrAddr_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \IntWrAddr[3]_i_1 
       (.I0(IntWrAddr_reg[3]),
        .I1(IntWrAddr_reg[0]),
        .I2(IntWrAddr_reg[1]),
        .I3(IntWrAddr_reg[2]),
        .O(p_0_in__2[3]));
  FDRE \IntWrAddr_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[0]),
        .Q(IntWrAddr_reg[0]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[1]),
        .Q(IntWrAddr_reg[1]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[2]),
        .Q(IntWrAddr_reg[2]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[3]),
        .Q(IntWrAddr_reg[3]),
        .R(reset_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \Reset_Sync_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(Reset_Sync[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \Reset_Sync_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(Reset_Sync[0]),
        .PRE(reset_out),
        .Q(Reset_Sync[1]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 240816)
`pragma protect data_block
anVhyrdoO9T+Lg1bbuabhb89l4lRufLq74raQ7eWjdpaI18gyaoZGj9B5BdoB8ylbIJvEPCBIIK0
Dqea+HWoN19qSH1ArM75eJw+a7x2B/JOtUoOEZSeR3r9mM2LUj/iL7Cq1FkI7oKPL+Rd1W+H972s
6PirYUrAGYCFeJvDawdvlywow5I599Yhwgk+9plkYVCw1iXlBXkxOiFebS1UVdItzTuuh2+uDX4C
99+nj0XZcqgjdHx4wYkOkEvKjmHlM2YanrQrZK5KVji15XZIXZGFw87HbJ1aBJN1Y+foNhi+mfXS
9PxweRZoLU9C4wGCiztupTVHh6JxHyFVu/IGvol5UyRBi/Q9BeyYKobyPEk7G0NIWUvVr1FEamHA
QxhbkOv68idz3opj3sSbqfYUwLgAay15kFC0E+EWLUpbBWGSPoSP7mTGDV9hX4cUpcZPChcuMDWj
dDah34grXGy+FH8hSwjzaWt4wY7mXy/TGm4qst5tqWx6dWRgUH8IzItIokl4TSwlodS1ceE9R0+P
NIflZGzsKhls+VBrssQYXxnmRY5ELTIjAeOOOXFiiDByz6DDaBIjxnFONZcN726/SelRJRRuORkQ
VSGBfzvc8A4TnvIhTYPSrpVsza2ZyTLEq6aG6poru0CW/1JT4JTgEnih5nvAZtzTFAVlqdGIfW9T
J6WMM5OVyyvGS7ySCCo4PEoNLIVWW7X2nnshNIeCI3tre/ciwjlkhvh7vAxkhdAm1UZ/Qtn60e0c
SxS87nnTK6v5qcluLWmgy6pAUkyXrIorSI/EzxEtaoYkDzKKt4wCFm8tKGc1Gs9VyFyE6WDB9MO8
WgWsKPxKo7EBiXwGpcjM8zJIGTiKMC5Ip19tlmJ94ZJDfbDy+JoOK0bYAM3cUo/AAcuDZSFtZsTq
3Z4sUQdMtibFV6Q9hTvTXOvUC6PhaIMpHi/8pT3dcM+uP+IwAbBi6ACMCmGPhKJBA0GbViAHANlM
2PlPq1/poW8QciLp/cVDqOWLLjgJ+Z7rrUtyqY37JbDjbt6R6pK9mlabDnk2vM6DlmjVunDMpQqN
t9U//1UwStRMCip/hd4UXrOOQFvkQcxRT8Ky/JfSbk+c/ND57F30dnvcikqkHF6CsvVz/wrchTTe
pJE1o/GZtaqkzbqwyqGJGoK/NnAFbvN0LOQ6xpc7tsqx1j8N6XLC1o+2vXIiBESQfAZMUjCMJGLc
SIjLggUkvvHLlVSWN+ySDS3ZnINcDapg9Oj6OjKxLDwnRUPtKZLxZLgFuoNF/xUWSF5fZkziMywF
PKB/9lbG5Bknx1v1k8FHdj2GeSxapOk98YbR6RlTjYDwGXrLCaLHrUO7tT9ls9rGa7uTw9cyRqW0
uLE8334nVQ5FPz8HdAzsU8rVqF6bljZV5Faq6ApqfFrkKK9HxUbKI37aaaDldxA1caJJh9bjZrxL
gr69dvEiaedhuHNely/pl382783fqfJa3TrPpK7A45k8+oom6lXBFwHofNP7eq2oncxzsbBpwATQ
dPpbxTvJgOWrxFt5RAy5Qdwp+YmeVkcPS+29YNFOncipyiTV8f0Sm35+uRVWAdAjXmsn8PS34frJ
RoWBocjZ0kHKw/LQ+SZW+ZkeJCdoh25hLgua6W7tKEEEq5XJtMy9ICVe4uWXG2t0bP3C9JSsf3wL
kdiKvJuYPg+/wk52948Oin0MMK6r4fzzey5bUiIc6eyP6xKC6iVIgp+mlhSlcblrrorRVflb6fE3
+RqmRvo4CSCmnvwV5RLczZq2UlLdPvpH54872x7jVGCiscKd3dO9R3u1W/hrgfozPLsKJ/Fe+R5m
k4jbuN4HIrCipNW2YfhOvclcaTV1485sQPJQSRGskor4jp5eG0m06X+q6HXIjaeOLh/1sUiWfW5t
QdJCm4ojYcht9kLGpyjqyv6TGKs6ACv0ZsV/Ef0L47BjaEwA3HmBAaK1UdaDY52LDErM7xQ5SQ+4
lZ6jILaOWpZlMBdnbcfKgQ9Hd5bx5biBxY4u5gnZfNWIU4xTtVEWjxGCysSN3lBvAu7CW/Stp2B4
Huh+WemY7jmuk5wKFIh3FtMzJBm/dC9yEQSgase4meo42H0NPZ3Cc3CjfwVaxDfXVQ2LlY2shUtJ
6diazITnfS/JrL/gCm14PuJj9hrZE1RSLej8S1uaynBN6kwtmQCvke+P5LblEIIYItMhpH3dFCVt
IXOVcGXUK6Zr37RLpdpucWc9Pdgtv56wAJRZC4pDEUX6tKD3Piu+/9I/6ceDO7accKvDxKsBm7Ny
+cmvEJMQWspPw3/21n79O+PT7N+KZDhJWngjTdGkVBuzq2AvXmyAEwqo6wob1eVmBJ7qgwjiLC2z
JIVsE2Oi+29u5snv+kwZ8lYOJrOuq4tiYjPVjxr1wt17ZT55fX3lP/2ZmDolSkMwRTl4cw0Ypdjs
sjrKaEZiWT8xR7E11eW9Rvb76yEBy0oM++jPZWlm/r3P7NNzDC+PCTBkCNkFAdz2VE2gnuPd16nJ
IOXK8502UycKmAY9XRAiPPrgN6hOvzqbzqLK8IALuiU6n8n2pCLFlUA0GZeURcHkYdVdVfTogc++
QPWxxfdHO1x+8Uj6HYw7oQOllFFjpJFczH6LtSukUf/juOkowBsnndYHw25NnGNcZkSU+YNPrf+M
w5S1RTikdoSCf0QrNUWBR6RU9isAq+ft4BVsfUP6MP4PnvyxGSNoLXtwk6DHToIYVI1ZlPtzN5Pq
2Ccll5KJ6kzm8G17ji0/PKNKDTqveCHwLM2g5E93Ye+bWslVq+3I9JX8as97jvnRvuYvjH01+oj3
KGTviN08IbCU5dvolSqlqZQkTryHdzvc6MdfzTxRa+tH5jK7MGaP7M5yDFYKUVHksSUI8eCINFj7
cCOEcvY/5qqgdUaaxgu2hcGNa6nI7NI5c+PIMqSgIPgdXgwwfVEcNLr4DqdCq8Q4k1XLTFEzYEeR
pQiDilK9aAAeftopn3RJz6fG22VEwear2savNjsNbZagmzz68rmoT62Ud9elSWQ3Csz683Ew0hXE
ZvhNHA266M9SV9kUqkclLQpYDKHqbNce1IVDnIcK1SzRSXaffU0UFWcFAUlYN/TwxSY5aA6u9/Jl
wm8FxH8AkRJODJ0VpFkSeD5dtvBnuc/c9+3etkZgktK8ztLg0jvWNbl4wI/o7GjoH9AFubnv4V7f
KEJ41J5+VMP4uJmPgaqgEF6v7HwUgtJH5KiQLyh2WrNUXeBPGELlhiNwSiGssKFonBf8Tovg8lPl
1aXJNYbjh3xQY3YJPNhZrmR8SPF03dzfPh8KS/q4VpchwBnMB3D41QML3awoLA3YxIwUt6glTSC+
+RCdhr5+mdeqEiKR7+iomaJ1a9XtEgHXAsqjsWQHjvj54W5Og1yWI17pTpgjV4WuNo+ViaokgjIO
V+eE36po5/yTAKQOzvQOhr9zw9obMvh+lJ/+aSQDt9THJY14NE3ZRP3Nd0vFUXMcFrLedAyIwDMj
bkF0PuxyQARFv5nAmvZiJJolI1c7zMyrJaA8yQ7BBieyL0aSn3eEijpeeGGyAN99JjFlpbosoMwu
XFvPwuk/kRuJByyo+gk0LVO1HlQWGk4AeRqj5TFGNlbNDx5dZxidhLnLs9a0LZ/UOuGdU4omqHeF
lefSYgYdV1Sxe4Try5iUm84nHYV2oVxGCk+/h7caeXFpS9eEOXf54YbQQW8rHHGB5pRcssvWiUqJ
/bFQSz1GIZenHbZxI0DHCBHgBr8qKO7IOBtNppdHvXB3/7LjsYFxy1+8Qin/b+ibm0csWOqQpPcL
q3KrROUKAIFrg96oX2EHNbXGuEZBT6/Pwmh6oGev7Cncp+VxJoLFBpF1fgCEqd6JTmfovV/Rykg/
Gp1FjBTiw/GHKBFeQR5NOIKQgPSkIrqfB5K+XNHgLbx/cI0Mxry+2xUKcLZxcGQ1B2lETAi1aEYj
PYniNwq6kf7UxWWqxuXLq2a0oK0L0vlufQHQom1p7oyZuqC1dQZlViuHzVWsQaERp9v6NQQqUfby
fZDvz33qaMJpOMe9QtV9cs5yP8M33PpG5seQ24WWa/Pi8EuqlmGE3zo7t8EfqE3xGjDtvkzEmIHL
iDNONhZSFZk2mbQ+Wo8QaxzmYY3ixefF2I11wAcyEqpiBR3WOUXLgg9wAysO2kVfgDXJBvBsxvAu
E7aVajZVLXwKYA7erzLIIUl21B846uRIIo+IxeODpFXC2W0HV87BBq2VsAvEfaL+LW1kfVV/meDM
u4BFt0GyolYwApcsc3nm7asYPww6+udROx4cxP4qUjpfsUqxoeaeS/yyPpR+mbvqTI1TnBSM/CFz
1vr+nG3MgAo6FrHgeaxdJSYRHnYk3jI7X7hZT/irwPqZkNP0Rp+4mzrEV2M28GfBHh8QWIc1ruZd
is7KLgrxCcbtuSMqLF+RBJW8X7qp/XMbsU+AsJCw/vVYqLCAvfjLIpt0Yd8JrqUMD3n85SdTzNJ0
5xiv9FjM2Aj2dTtot1tYpSPtqZiGQPMq/aVAR421xEmcujdX6kZUrOw93N5k9KQda3Q1Kr7IMNXt
DSaaHCYiqXjfr7Jhm1Dj/l+sqxSJCb4WWZmdvqZvfF9tk9p0TX6a1ZP/jiTk6GWCuO1YqsIzMy5y
Z1Bs8Im/SBjH4TjQUMciFk0vZevMGuniCOUfIut3CjGCZbbDBb7HzLkbbjR3B4YF0cDTTlo0rjW8
sHro//FT02ywE305rxl0tDvQlVfmjt7QbmL8PniegH3PjoqR02qWXLXA21MdgJyJMsqn1pavfA5l
U5twuofr2ddfN6C9NtBhYtfy5f/nHixBEkwi+jDwJQ32csDkj1aWv7YSAl89zajWKTL59/RoNviK
hQeQU1NY0fZLP9eNSSbRQjN/6rlWYSpVgwbzGZRETa7kFxkRwg5yKj/rz9FM5xVHzhgwu1lJvq8+
qdKieFJXJ97kctPNXgpElsvzMAE/ue+ur17tG7tKGaB9I5mwqfJBafFp523zbYAhe3gSQypRppgD
ZmxBGKTj1Zk9kQt52xyZfdHf0XsZMjlcjWnJD7/k+1x6b15m6hu05v6HOFcQCiVQP7ecVlA7wuMb
hu13AyhEvoQ4iKcj9eXM6jCT/F0G/SrckONLnC3CjttcqV5/SoKI3pAGI2T8fUrY+TKTS3ZNF+Vf
BpPsuFgt8iutSBn6Ibh4y833NDVARJBlK+V8uWTtBQvgBOmuCbotHu2gx1E9r/RAEFKLgp7IcwM3
LXc9oqxCRDydcWvr8iiyFAcQJ6143LJ1Zb07RK9fvBAniLRM0RKJQEvh7Gxod3PWlFYcryJtqFB/
xaP4ArUYbnE21TUYZpFC+TLX+czdJ7kNhE1Yirwk2S+AKmtmHP2qukdlzjnBnAu8Rp8nH8ouNixS
Lokb1hUO6mPYxCeEsaqYibhbmCI7n8kLzoJ1Q4qGADkj+RQ99jgqvwLCGawpTcDRiKnofSeXLZnA
thA38GBTigYItpgfmIN7fm89ZZa3guaZ+G3ncXL3Ulj1AlyFvHMgvyzHjygzki0hU21oI/QeaRu3
eYg+V2lyxC9ErUeWuD2XgkAcDE5dNaDOucF5XKcEqyy9VlxihHbmbpN8IeuDkTD7wRO/qAYEf6WP
7EvNSIoj4GIm663Q2/QpXFuS3AMAIwTeDA+OMjcrU4Qfvu+PxxiIxLX+/bt+VD3G6/n9HA3I6gC/
saG7EkuinN9mZMCFczNf9mu+tmBaJWYrnKtWlA5mD7me5ZZLnejV7wXgpj06KHzUXLvF/IUVl8Ry
YDS5XAwwjH/BxTpG3VTLrDp0u2LSABJERebL7kVorhWMaXyQfZsdM83JWJDFlHWsjPbaN8xHE6ao
z/aUH63rdWk8wEwhaFcZydHUDQDxPHaNPtjRbKY/bdxH/PFULNPI/XfRs37EnIuX69S3SN63e6FH
d+8Heh1wgcuW1WWt7D28DFgAUBUgcrgvsBLyyjIBoXx39O/bp9OkX/upxCmwodmSaGX4KaKlZmsV
wXkr5Zk7q7njifp0hTfqneObAeo7s77iqliRVbh/YIk9D5gy5aV5EOv1SRieiTfl11H/qs5OCGNC
rORV6tPsI++ryq8aepfmJf2FWVOGc3ehLJkURgdU37XKk8Auun4ZBjB4Z7Q5+2MfMhbktsT0FEy+
lcdFLgnpBw5qfqjwNsjcTg01p0hgm+c3e2roPv43xL16OOA0/c5vcMNHmVk6bY2neJJWw1qXWJry
wF6rVshVGgKQQXG+zPazM0MqOdg+pO3AFXBgHHrMyjZk4FYoMbGpTcm2Az8lEXRi6QMj0QL4iHBj
k91LjBg+/0JMvfgKm099FuVK6KXbDasArBf1BO1E0KkDF3SJZLlB1G52k4psvVnSYlbGaDs81DaX
v5wEdoyuIwWEUKEhbG3g4FE0HU7vUT4NS8TPsxRasqbKIohCOotVd39iqmRI3pylp2O8aE35n7lL
WjLqaF/Alq+R5oQX69s/6kj3hx19MpPVMlmzP9p/KSI2Q3lCIvvVDyvtjlU4pla0ip15axCDmLE2
gkdu4ArQdBmyWRsUdU5aIzldOiX2aj4tU7ytVXBTa9O6I54/6c8CPmarHRzAkP1fnbchqdy52Wrh
icrwGguGG0Weg5+QskHI0c4QDQ1HoQTmmeJmtasUW/yQaa7xdHXsRXQiKL1V73CCgYtWjV9WKCFv
1ynYeifx9abBCMnXlgAS/kvOtSQkvwu7r3n2WB0WtDrwiwx5lv8aMrnQ7T4Y2rJ5JCoDyTK+LG+h
UkYMN9b1ZdWlwT4A4EHPzjunzlm4tWsZMXCxqewtPLtad0qj1f5qA00BW3vwlvXZlw79IyQjWNvU
UJciL3GiJsovnm/8wIYl5ELliPRN6gsw+1Q6H+QZzerQIs5gTTKyx9FKGIGrXlafDsrKEzek1I01
Qq5lPBsdqalUu2zVAdExBm++wx3zSRnv3YJJ5yBuz2xp586oyqy8ql3Ph7YCqB/dlTTrWSdIA2AW
T+s8I0rqqVZTutl9aJwftTOWiSaZa5+hJRchZ5wUBgugATEp/O+K7NqjR8+HJViU9lfyezXFHX6r
H8xfJcwsYmWH9L0Q2rRI6228RyAqwV3XYIUaceE1lW9oAyFglLxQd8PWzgmOfyBJvdc7Iuam/avy
VHr6ycZQxUZTUTC4ensZjnzpCuoZrIFGLh3SfEmtPLjtSmjJoOV710RIKESkIcGUISGopHtVO9dq
3KI8BBnEsiBEG/Op3LavWmyb7IHQoJB9oCqyAytlwN64Ixf/C8iRY1aqMsZY4XdYLrYRwmZyywyn
ALCb0lVPUP6JDJ0K3bAtEzKSQQqXf/BpX+j9zGocj5bpwcR4kYN95XhXDTIgtfZOYAW5elEzYWo2
LFtEol1X4EFopuQlz/mEJkR86mH2u1mqGguj67GrAlUsUuUtPjGAbi+MAVkgIYZD9QjjzJ0VUN8Z
1dpAdPF9nTpexscLCymWXKbd/Fp1mxExYifzVSRIPVLOEAO4ZRUDi1sKuFqF281mPki1uf5axaCW
bR5jpXtn4Ri9N83PI76LtlAvC3BtGhhQlWPdqGkXLsy9EZVIsiyz3PngQ6lDSca72pIOn9ZXL4rV
JuZzX+phvef5VqFvUfIdo/hbU66fFIQUmOjRNQd8vZtlLcl6p3puxrWwuWM8HguQtbKsvCWOXRCU
MozJys36OX9K2xMMXrqozaPldz0xfgrkm3GZVRrY23Q7oJz0uOGUEtrk/JnL4/Odxmz1KC9teIMt
6Nh6FD+otFeAfCEIWLJX4Jlo04kGLcUHjlEQpkQBgeJhcxiKobsPl8Kx2C2Tjc5qaaIzKAfWzPDA
KWWc2+fPBndLOvJwdU44dtJY3PH6Ye3Z8jzCIhfj3BxSS6hs4m7Skp7JX4VVlii6O1WVQlwz7pAM
/Cudo83GhvBzjrJODpzq/CXly3beiELl0hyUrYkg0q6UAsyatwGgT21fJ23xnLJi1bQvigO/Fal9
YYIE/3BpxTG7/tFAN/oX5DIwrvW0rtPOwMW1QZo/S+iXrZqWxKW9l1vdn1MBdbpG1BijSJVjGqFV
dbaJANHn2ekwAKgi5lCpwiaQnb7KXXchBwTbNksJ5WJh8zHGhz/iJuT7zaAU/IlXa5UGHXtgVUZY
b114Knh076zhDCHErj1JgoMH048EIkvJLxWve3mpl1oNtcNfbxC7MjpAcXExnM/ebi4cUvWE4RHu
Tmu0O6bKEuMsxLxt6bLS397VvJjb8FU4srdeAliYAoKV2gwOxREeDvBpdcwiwY51xT5VC2vFz+tA
D6vjC4CuFdGcjjPS1ehHUelzHxjle/FTg+Kejv7xfa9z0L37aWBEMwpAy4pa+jvwt8z1EhSua60M
22+whL49+3tMSsXZVpm+YQ0NaE/hGG0j6bjO/zG09SnE8zBsApGaIykOem8ptqUkr7T247kzVMAG
0QjgqD/OG95//LPk7F2EjTs+Ph6qYhmguQOuOoD5XU1+UDCmvlBb+DM0kBIr1z07QXcM/FAg8XHQ
EU52mavGOZOgfXvaZKVy2cKImugAdFiRrXTaAE862iUzisaAEbDdfzQqs1LqKCWA87F/J+XJ2XYp
xEP4jvnbGgk5hCRkJvg4KWuv0pozsQMkeZTHUk6mtK8Gu3RulcInfPE5uUfVf5rpaHBKJH1Imx4u
j6FUz7q57tBXnwPksfpUgpwO3yw3hlbfPLLi/iKJYrfCChB51aNH9lDGgEwViSrayJMBRlNBR1kB
kkSGOOtfhxC17F4zz7jhALltE4aVH6vlwyES7spgfXTuHguHJn03UBu0o++9dU1izDEsyD6oYnb0
wnhYUSCnA/bPYQqTu8AR7NAhiixihWPvBPh3kjYCN7p5N7yXmasXd6ItO3Btgi/F5GcRsCtzpAL9
h+vBqiMJbjALM/seM4o8sbt8F9+RGtaE9mho1eFbP7ikfQewITUoL8hRj7sdl0ldV8PnnY1gBjQr
pYmh9AnkRyOhqElAm0oF94ov04S3YcFCQQl4sPM5prs1CalnEwPzTc4P7x+SxOv2PTdZQuvsjE5K
/qStR07JmynTDA3yz5a6B3Pg/E6e5GTmrda6MDecvTkl5D3w6RFWJzhKVZKSyfkDYWhwXpysG5g8
GKB+0YvGhClxxat4491juRupR69oLEyuX/fDL0WRbi6fG4GwgTYFWza2P1/5cegxJvi0w+rNwGk6
/o5CHTWEdcE4N/rHytcqd3jzgi662Ak+NfQ23AliMhF+qT8swYlqn3kR/ZrwkjWjR/kW8pGqiNAT
94/+W4GKLcuVQLxXAJePbETf04Xxl1SLNy530NVnMH/Jt5U52iEU+iTqPAXDxRDOnzLNQcMh01pU
6pIOwNpKCeGFXslCi5ldqjnfcetMaVll+kkzVSdB9in3aGNQ034J5FSMOmH7ueA/FGvSSU6q8d5K
s6nvDYXtTjA7OPuo6fP/VuRJYams+v1QI8lrpXGE9BDBnl73T1fGRX/tU54J4oTN/m8IaLdPJEsJ
M4HfMvb/wMSiqeVJmJvBwp0oiyMFYWWebXyAzBAkX98uoovBjN189u/+KAgGRE+RdffMnTn9KC7f
FrCCDLxYEyHm5ecskV7EhCu1zVcDvST0cGUsJ5ZD9XhxZm+3wxi79J32acB6v76JULv5kYHD00JD
mO6Oe2b1ppM2yoPLTiC5vGZD+wmRRJB2GbPsT1LaGfqPA1VZntoAZaQhv1qUsPmBOEb2g2DzA0xw
eqZqAcSI9r6Ird6/4ufYFhrexBIr3bYO+MJPBOKIuldrZ93BtDQRIF6J67GzUmE2RvrkW+zuA4Nt
SncSbS1dcsIduQ3+wtd0f9JKEurDtrQnI3eXKXOtxLwBztPx253QKNIrKJMJjajkgZ8lO6cJdb1a
1L7oO7aS9gTM57wP+mXTg3tePARSF16+jXiG0MhgjGlsXU7lA0nqn799ETl0a1OsVsvJkbN+uGcF
8NrW8A2w/jnif5Dx2VEnl9tsDSkkOr5FtOa+rGmpXSUuAkSEIbQRYtXtdTzvo4lt/ZlJHgrljWtr
+9Tonq0a9f0+bb2+Xeul66RCU2ngaiSN8Rl0NKo50Ng0PAGeULiVllk0RUckJW9Eelp9yhIrvoTG
T0fgmFwPO0WxnDuWWE0DSAKlJC/pshyPQFPe6LI/vdm5uOWgGQTjmxkN556MZIwkGLgtybRQtSe6
C6k56A5Jrrdi6ytRMWyQaJnO8VGQrNm5Yt80wQ0Glngrjm/FD5rkhCROUb1pdywVf3NzoEFTEUIa
jgM72FpK4mCrzODN35o1F59PreW+4bauh6XxsN3lC+IzlhyTDV/rdsUZrhIvxi3tMy3RxmPz7imT
+I9BYopWNJen7e8lrHRZ0wp/qOV4B5NccvFZ7idwggguqAPRXUGspXip5XH861L/0YHadjIMq68h
xdee6wj/9jgSxeK53Y0oUv7LAdLhYF8/P2EG5j0mpro961pjKH74ZtYKfHjr4qlnB3SR7/SbkUW0
eVrvsVVeCV6z5v3wSz1tgyR6gdZYnYW6VbaVDUOIsuHaNVZrDcpYNot829sNvrUfGRw0oNcm8ON/
f7nrU7rqVgiEBGoIqGmnXbrQlNMtcVaapr8zrSRGxcFMQRd2EUdnN5n8EjZ9lvlGA3YzMmqQUBFA
PTJTx3vNtnwQ+22IA9IG6BjMzw3cLmfa57kN/KmgPzBLuEpSKGGI+0KgipLR8CkzJRjlXB0wW4mr
voEzpY+DfYo5lNK1ciuYtaVsj5rPrCzFdtHZ4tPGZUkLhbS3wX/s8+iGEvsFUA5x+AsjlKbMeNjj
TqnIUSN6hxeTrbilJwITXhumjQthqfuidV5Pca0vWbSX1Jipt84q8Fp18SP2YpnB9icPdUwIaLFc
Fi+PPdqVeJ+r8eMaJeYfPwihyO+LAScJHVUWTFhgddAiBu8A+SxPVX23KcPbNIps/eV3lFNH0+c/
Bhul591/i84J/U+t+GV3cP5WcAL/ioKkVcb5tdHwKWT8jTB4COU55LAMbG1NkjUhaoS6FCMMrNpG
CUQPoHO14bq3xPl831zEmM105Nt/Q86h0uUd9k+XE2iQP7lzJrt0mqvRoQui9lgnN4vj47ywEVrn
L5Z8MJoyyMq4cR/KhM1282NWfjG6tP7yVVa26fbKDc/Y+Jcid2yQo92IwKL5VSxyvgdzH7FoBM9b
6KAquJCIqxHp+wE0ptIX+NqM9d4Ak3l9/FfaVIGSj/MuwFPesVy1ApMLhN0LdtaPCQHPA3U8NV/i
9uhKR1OR5gI8gTS0LegPsqm50mZ9xIpBxU/XHCplHr+ipwFJKiMy61o15phmixfZif9/v/9/sXB6
h8keK8kUL8RXeCRNs/ySN1aLSJ4J2s3Z1HoZAd0pzKM032GhJw0uQii8d2WTbzgTvHZXIlWLzL5h
LEvmhFQlI19LLzLCd/9PXdPH/zl6sH0zm3gesaYWnIpYpfvonydJ2DIwmTvu5R6XhsF7LG1CLGpV
sRwW2QdfnInTh5MmdjyXc8gXa4T6cAlAlk6dwU1J+DssMLCE3Q9C7oYK6oTjYwMkqevwdbFGjUuo
yoouGZe1OfvnYtVnFSWvh5HaGqyiP6qlX8qOPrNj5cQT1OInaPUzmLIw4n6EAoK7v8v2RLul51si
3EEiLU91A+RA7Ze2IQVrIAiih+rx8EbWf6SKVukjWKEoul0AaXQEkVsSRbgsLICScMKkkxhQ6E+u
AsUzBtnN16QrccvpennmglGeC1wR/DyhjwYibunKHzd56GdttQjzZL7NJkoMmCmcImGBALZGRNtW
o+rMi6dVVZd0EG98pNTVgcirZjC63PRDwxcGdqDKaJ8TlOCUCNVJ50Bk9PNf7A1Mjblk3gtsKWxI
HAve1EnIMxP3xJekex2KrGl8pWxZHOEILgU2ocu99IOQ0fVh8yxjw/QSX6+5hAvDQTa8WDEszgKl
3ic/KWnWr1fmOlNe/nffuWzbn8Y6UaJqvYQLsl7WVfPWIMlUZhLbeMEwZ1Nxnn8Px3VDDOZxUIKS
X0XZLTeLLoDjDT0/E0HBP69Ao3CY4saK0gau5Pj/HCFB9Yh20cNSuuiTde+J8IRYyqt00/dpLZVk
PGMxsUKdWuukY1IzL+DP3Jk1IgZqZ19qj3xKds5ob7zsAAUdL9oTkGxKAkWJsX7rePGQkMM47Rzs
5Ttr4Lv2RXfg18ovXARqQHTibV5FgMxyO6rhZuZamsN+UggIpnvtsyffBZCTzreFrpnsObleZGap
Z5qQlYHR4PzetlVdIZ4phGBIVeIessuJ+24vTNrBXAxwfZlPDkGIkGhxT/gddIZhb7ZcHHZrlpMx
dM5319VKSEaNWapNZMTgE2OEW7qlbTdoZKABmDSWLLRisj/CzspvFAFUjvsH0HeYBmACb0gVVS4h
D89e5YpF1uGNYmlUUgX8YoeajuxuCCzIGQbRVCh41BI2D/13h4LEzYex8CZ/HlcDCVZol4EIWH+Q
8cobA1CF9mtdSpOOnCCEN/IvZBHVht8HEPBzXXJeuFoHwr3ZCiW7T5rXrAVGrgYqGe0qlpaKty+b
7LdAkETp1osFVhmy0t3bCP7Q/v7vmHavwRfgRz99fapbRi/E7LivLLhTr3JGDQlfU6qVwd4j7UHk
JkIKAHYIa4hMFlcUUZgQMcaogMHvRv+0ShLUWbh2W4O5RKvsYxxrXMTllz5ZN3fGsX2/qYhjLxKu
Oj172dfRp3+FLPFDyrggs5QlveG27tDwKUmhzYYLTVueaWUeEk2HSa2zdnindgMsJmon8O6h0f9C
fxbF2H9qCDE38U4q8JmkyRH850wyc9ZmwA9TeDnz/j79TEsrGKliXo9HtuVju7qpPw1T1c2eog7h
2TwXbFzx8cgLsBrty64peeLIMmr9NhmIIIGy/E2P+mGUtY6hPo6/Zv0xuh3vgcbEvneuD28c67kS
dqsAvTMUivi/yd2WiyJsSAeoyVH6xk0puo7FSwqoXIhC063xx5GUBgURlF3DkGvbTQuHaEUwgsBF
Cy3JTQGJMSs3fMCsh0q8CKiKtHunJrF0nOR46QBcg1nTRl2uWoLAk0WisLeou7jSOHdCOg6L1na6
A/0hvqqr1RpABGMecyOkAg6trhzZM0RjvtV0SXOLyGFSUg0mkKREcA1+yVU1MZNVP7QmZ8DMRxFi
XxOy/eBftkI+JpE5I9QbPCP397p0xXqPrT736vp243r2tVlEiJ+0DxYyuoyOnIkTbs0zJO7bHMSf
RHlPtM0r1aztC/eZb032vUAtDLwZWSoaETo+nXnyC6om97LVgezYop+fPIvo3sFM4z4zODICkont
cVUmcMic8qhu10y8u0efJJpzBKi7LehHWiyWWgoEOXVEP++RP2p6X6ppxsnwC8RNUt6dxK7L31uO
y8LO2IBGoW8hgDSJBtAfj6SCg416Ci4zvhS8WvINoLtJP2zg/Y0YbUSTge2HT0AAcwZIXqfYK7RC
2obVqr3nESxZoyKY3NiZ3YQrhwPa+gvJ606BMlVDvh8+bFA/tvy1MhRuYLPwb1hd46Q6iZc1Kh+9
VhX6008zAcW+zACLIb6KXcG4ITdqle3cHB6uBnDml5/f5p7NE9yE7pAy75weWqOJ4fzXwBGmHZPq
/58OnE/9RuhlcdLHB3W/RtwaooWyAk0b0j7O24OtqumMnuvMikcWj5WNShZCgmHMzk9L1rsYnMQ9
M0AB8BEPJDe0+EyfVLPn4QRyHuz3dd9V2JIzJsyWCWfbeO0OespZlg7eF+J7URS0UQeXtpHjaHWw
K9kVnNBgrCZeaY1ucJ4h4dY0Tgm/oIMA4+29h+0uEQ6Ud7m1RsH7E24DIlVdrfwjRZAWkkthxLqz
jKYsjvcTsJm8UtBv64RFtt+oN+xnkixmUk6l7vUktmMiZN/074dqTdkoIsGiPA8aE+M+/IpuynLM
skW5xYWhhtdefbXlxipHEUGuTDi03UOWtr0P+isXB5QEbZTZY96V6V+waCd1YOMECne5z5JTQ1iV
d4mc+CSsrFTrqmdFvuILnwVh6IWB5Ie+4po6q/FVGCPo4idJaFpb0t0kVnGkNUpRfdreRfqaf+wC
p8xhKdUl8m8r3jjYIjMYP3G/ftWpQzP73om/PYrtk9YXWkT2BFwOo6nR3l3O5fTUtJfkSS5xc2q2
jLz3VGQ7suceCq5wwpK91BKpEtC/aI7HZdPxZf5wz95Ut7EtcSyHCyTlKsvrw4RGvpdAcQRfxyAz
sxPe6XNmiegNfggaD67SciEf5M+a5oyoITsBeN8G9HYfRZY3HQrNapBm0khMkKU9zFTZhKLgAtwG
SFko6tjxylHtvJXq3OYa9yuqfX1f8CS66YMwjsVJidPrt6MRqofd/lyzUS590JmihhCUH9wbunw+
tqzF8IJpGM5FZzas3DXLoqD6arUYvIiMDgfZqHoMA2cTyueuie2SFPqVDRJqHeiEVuxG+Es02yYt
ov+PWosdlzvfnjkn5PTVan/s9zcB0ApczJIiGTHHew64DgDqymKRqtPneHwJGqP5i8+OpigU9BVH
3Qz01Uaws5XonAmwCGutihlM2uGrHvP9TQ+Zr88r39E0r8vx/eftfa4fF377L7lrSVnAcCXEs6p3
xh6bkMG/5mAeLE6bl+sTIMdYb7d0DLS68mHssF1B6wKQuOaZukgri76MXwFuE1E9W9Fa0KOKepQm
KeSmsXcuut9H5vIZjgX7SlapAOpo8JBo8MGkAA4uJU7/uT1kbpEz0WZKOC41qmeyf0UJwzc4puqs
/IvLqZTqB4R0hL/ZSuui4vQCMTMbNgypN60P4wt/Sd+OjKpdPKqVB/0hk5PVDpiRq038RPMlkBQi
Toh6A5GNDPZmGnFpqwPJU1f/EgcWaW5fwm8Vy0EbgpNwSV9/X11CJsg+Vo+lk1+kY4R+mBgqFDNl
mR28ljcmyFFz5M3tHrwMJEZOskkjrfKq7ze5ocxsvXvS1kaB6PnjMn0Zk21DYAgp3PRmLrajIffQ
PAdmLD4jOhRPLGsenmd1miLKGjgpdVm6OvVS5QqzQCMsgiaeHH3r0rf2yxgF+LnAsg82FO57wcE8
MUSHdAPO7IHICX9KqhLimIy4KhuRyuk0bqZAVXn6wHkdqjlmVgSJzMlM7jiED7P5ZuQzBSLHYSJ9
kUVaRPQwAOANU1IUwqmh/mHDvDi+7LYC/zKBgpgcA33S3JGu4im6VUGNZ8yKTcQsFQVkeOfxdoiK
Q/SWB6KqkPCDksO6DOAULzAddeC6vMcvAPsCFWZyF73IL9dmTaRvYvCYw66NoNYORoxZPlInXM3o
OdvhcQkfaS8vKENQZkiiQHAChUpP4bt3BdSQbSpj44BD1BnvEaoXWtNo5m7FzORmF7Sizy2A9aFg
1CGjbrAKjZ+2iG0ctAVu1iiQg7H8V/qPpM+1lEVAMljlW/IctyqUG36jX76CS8wOyggmNfbpXx8F
cwW/J7VPkbWQ0MwqWrPlCIqDM+pIZTN+QbwEQVZu53MBBgGJducf8eQI3+562DZV/yIk5TkOUGk0
TMUlSknUvGATr+gx6ze9u8maXghYD+wuDjcYf2V3yXJxvinbrqTf5e001e90TYkPdP4aUfXP3Zpe
Txsv9cXahWceKhBuvYFD/CelVaiFbuXu5IHzfoTzHojSypNF02EW396g9rPAajWlxYM5wpVqjaFU
Vyer8YMHPOr3mkhBzGmmGZNUZu+kld5AKmiv/S5TIdsz9y7qnPB6QIyW4r6ZveJIfPMayy8STN66
2BIFGwbBDoiLpfbcQ7N45EaBurX3QB+BTw2UziX0v4iMddtT89Yp/TUEdP4PDsYbHmRjNvFfkuSi
QzRmE6KTopUfWBE6UzcjhpdKcYCotkNLqU/RP7G5qOfcXNy+6PjIzjngKmn7FwWRwGRvX47FuHqm
hqMM5r1OrypMLKh8UjC9/BrPZABYA9AIUWpp8/8rJgk6Ar/M2EQun0HIIx+t8Nl86CG7/Arg8mkx
5zsrYRycizqO5uHdTKPhn+b4ZMpPHT5emLoUtqQeeBV8IoRfeazyXvapKEBH9rvBbiuXZLiqMFZT
ue58Ltd2oo/PT1frvLwVrtAVe4XBj0n/NKO5mAG3Y9utl5Y4285MRa+rI+tTjg6AxNeUUpAnBrYw
kOGziMppQx50Eth7EAPiLIupC4eggvyJYdfrnAh00FI1Upnsm0t20uYXzCV1mYsbVJmFS13OPDnZ
JRlu4kaG0E07cFKCVELNot9fFXhjGQY+1xcIl0011QQY4LkiRXFIQj6ato1AOTrOs+TbIFYAvcf0
eSP6MqjFT1OCCyOqK8PwxZKcbZyJbkgTwJcltgarUyEVc764WnmgKJUTeyJf8oVTR31CnzRD0Qvu
xAk5snIMeEw3Y+DStlJQ/A/o/LMSBjTdbEiFLKTGxA7OCR+SzyPSiif9km3pxhr/xEP5y/YC/obf
3ZZIz9Wc3PlUD3DTIvOooZkskqAoEpe4wKm7XbKDGDkIq/FnfqzUOrdzW2GBtDtHP2vgQQFw6yeE
VEsn0CaJZLnuGhlUaWk+5g5KSW8VgOsxt+v8kaoJsXUZbun9p5ey0BrK2OxCysKlpX3YgvVj+XAr
oaCzg8hWB6M0RvX26m2il791U1DHGNl6IQOYX1gbzHEG5mXak1NYnOa9A57IyfPW9tu+yLkOTlFe
mwUE6Ls+Ig3ujBJFiQDMI2xri+HumUbb8Q+jP0p1iRvNtWpnMD7gPj6S5YIxdxIf81tT3QfK8keM
1tqV8xY5w+XzqI3A+PFE6vqczDtsCtnizRSMKaXZ2IOMNPCMExDNFVRpciMrF5VK6ZsETtGVLkAM
yFa3v7UxvSu65oqA4tngXVg1mKgh8nqXARD2H7P6z1nx46rEPiqiGDl1aKzGqRpoKlyuUHEMQS/X
VKvnzpOqfWTYTjOIlsN9aEz1iVlt3D+G6jZ0tZzuYRC5WCJZ/b9J/zE2v968T0ANThN3HSONVAVJ
RK8Fqc2pgw0Y7spryCCJT9rYCKs3ffFgCflnmEKTXuq+56YukhQjuPA7WjiFOoarZ+jXyAZ/g2ui
clZ6QoZB6UJV52r/5XgjYVHVDreA/XqzPZsyc81S3EwkmQHv2U5lA7NpFP5ioovrduBCG1t5qITT
OP5NXW7dEdfWP4SjZr1Lp6apz29JD3Dc/uEv72k33s7QKE6RrZCp0TSZNwzjOSojLW/Sr292QfMB
NSzabYcZJem/17tn66ArIvXwCorpwujQgaazx9MsdTKU67al8aFHm5o7WHVgJfcC/8F0SmlJbm1m
yM8Lyy9pjs49b3epM9X2/I4zKEaHZdj/FjuaJHop/wAIVq+0UJiKJ//Pu8WxgEqwnwHsHtIv4UYZ
lEcf4pd1VicJw5lp9qnWfQ4jiNlSkisLmrEHV7u1rnnBk4zvgodivqUEdTU3tYmlp0C1qVd32yrN
6g64Do+Kcry1i3B0//SFDQEtn76noCDhwPxPyXUKiRctLwVk94NOkEy/w8UKTC/XkRC4Sjpr4fdO
i60jnYMj9tAFJ6axy3uTZsHnF1a4hOu9jXhNXatMSLahNRy0/wrnLB3GFxxCJ0leo4yseyTL9+Dn
3Jst2PfPDAoiOZ9SeufDfUDwLppryR35b6mTZN8xFK9V5+B8ZCQcZrflTsHe3lxwvc3yfkO77Wao
Sl7XoYTe8NZmYV5/kVG6dT3/h77Jsx3gNGExd33439XnnlX1QqFUAs1UDKzBDJX1O574XPs6kYX4
HEadwhiMEXbQg69ig6kkpr8fCQp6KG1rGkQMmE8Pe1n9+V1l/X004M7//YZd2J1jthS0QJmf7H3Y
4Bzbo4MmJEwnxZ1/1NFPAnpSjiCcWbdlzSeC5Th5ANESmENOWPl4CqKSkr08T0tSDFVQx9GjTyKK
xp5gU4ZJz/BE22P2QfX1DmF5wDGVy7IaGKplwkUkCt133OlNLtpldrlJbyPil7lEabV7dhVbsU1l
/5zOaAig8NW3ORrdm70hTT7QArCd4UkW+uoxtz4iINnNkRfUVhBbapJ8LxjjEioE+pLXe/ePdhOq
SYgZe43vt1RHFVvd2qRxPcuufuSy30LAGssjoWvlBBlj5R2cdh5Kz9pEchutlmQ4nCqM5jCo6lbB
kvUC0P4QIxBtnnV6fqGog7/ivzfqcAk0x0Y3F9hh+eSHUwtrKD/X5SIhmBs9nN1zlk8mUs0M2s6/
Pg/wCyrkiCCXFizvvKmpKFO3O/h7BK8lbXww7K/FGDjBBLmYPJprwIsoIBbUvsG2xr3AWhu+GkFx
q851tY+WsU83kJbz05AmSEt//5vp8BxjhKygERDpGjD51NXXw6JFrX8G8z3Ob0SN5yLce96wQxA8
wQhzaxXy3MHWdkORaW4IVfpZaeqT1KynOD2BUXrP+SetmeDpEpEq57TimAP1rVlI7/XcA8xByYTy
4Os0eCpQYPKqLgjX8qMh+TBkdYh1EN3Sg57G4phUlv5gQ+hH7Hq2hMZWir88RiaTQg0eeuH2H/hW
7oxXIhXR+9DF2jYoo9u//+Q3nlUcLPKF/HxThHgFpc4sqE0JsdLSCVVeRERbJ4h2Yxhon1yPLt9O
OBedRw5TIdQS1o5ex7yv46KynDd+9J0MaZy8nnum8ZIesvIIo+kgUCMOlj1d1jJHMZvTd6yFcPHN
u0lHeYzxOzLqWKUSrY7UqsHo6CLWdBjku+SYHJHkrVaMxzTA/FjzCZuPZ9l9t4Iw7/ywh+srpmlL
1eJ7+bsoxOz7xlhAA+Z98Lh31tyJA3VXqPGOoA2PK+TqGT40V3LWjANY1OmolmxwRb4Uu8agAdWV
/2bxiHi1Dzye6nfSp4ruuZ6FJOvHmKUksX8zlsdN9AhJhwnwOMdTCXoSx5YFaikavse7gLiMl2GD
96vcAQh117BhnTd3WRb++nlPfAFV7JVqG9lbR/HD3Ivw32CWBrjloetqcGuJPoCH3iceKmw0e2yh
dP2Qze7gAc21eKt/dv4bNWnyuNzRw6YK+NX7hx6sAreEp5pdHWk4NM9XXQEHon+wEcz8BoRWm9dY
WANDUBTyjraoF4xYTBE0B2CdNNFhm6WLQUJCWLiYiutrOBWcqhv2LGwZ2vptwu23iLYhCKL0FNfB
T0Dt5DEgsexIAjVwOJqAMUK4ioK1w/v/bFSVYWJSchV/A+Y+6sIOi0KiPbNA/uW7usotuBHpdqUV
00Rl0r552r6pAQzC3Am3QNaHtSWBwh9OYcStiOrLh1sCQds/ji5HTu+Vinp3UVhzNgWXURM1dFll
ZgXAi04r7q8SCG7hV6bqkU50j5L+rEF/KLmpGho9XCLAY6yKcZtVCdVdQ+N4GuQRTFRG0OtBMqj1
6BmtU0Ns6J7BNjsWlGE6r03F+t1Xoxo95ZZ6KMVvytpCwh1t5HHQEq6AJkwHTgmd46d2PVX1P0P4
8jT/n0Y530EGgs0lYw+wqmzJm9JnTwqlPBnwyFGgG9wnf85AiolQRKCVS4q/aZwPhPyslGPgQnWa
Ff9pHusvwxfrkPqLJhtMkyZncOYoiur66yPwFBcW/5Uvf0MeeOZXbKw4I+VaAnN2FYdNMAHIYZxI
gqzC4TkzOhPY4thz6j66Xo1bh0IZrMT9xouQl6dmGgM9XHiRUsVnoTmW7yPDRHGJ4H4bYf3fBnX9
My1XztuA5pl/9u406HKK5pjMOrolC03Zw0XmGQ37b2bHZJagD9PYy0bkjcZExw4fwTnBY2XPkwZk
SlJ4+GHtOLP7Cd8B8exDD9mvHzl84jWxFjr34hJgH5VFNq57pllh4O0iQPYNo84rVgJ5s8wAEDE4
hcjh9lWfqqmX38n7igv5LaN+HGrf/URGpVRBSSe6tIZ5boUe236snjBWcrkW06Esbgec1vxazVEz
qzKyOWL9NWT9fj2SzY4rHIUqulhA0mtz1Cge9DZAsBw4YdctOQPebNjVu/JJfkUYnhtvGb2h3Zgu
B/CyYSMTgmT97Tk4JALqDcuL/X58ayqqxYoVQWTYxd76tCxcKtL1PM7fPLXhI5QuzxZvAGXZsTne
2q7helidObD8OTwvoCEY6EdvCD7KlBDEVqfl37gwJzgYDLfQpp9WvXSDiD7eCPrY9FXsW3oJ09xj
eCV8TTSd+6UJty+ajKKo78zxy0jsyi7yJuyRfi+l7UHvJzvHXAHIcMdu+BJDtY9Kt6XQ4ry+ibck
dRGYPxUz1Pl06IMzye1b5bz5JjOCgO0yYbezXKGb2toUQ8KXud0WKLmjPpvt8kuTVOyCJ06hzh5/
YpK9txKwKf/gPyZdj7EjGM/q1ZHLxtfe7zG7djZtFS1oX0fxRWQGO4NZ3g3wa+cR16YXEWqHL1rS
xftSmGjOiH79x1XkcQpH+4cHsEzruHVRhCMt6jJYDh+nXhRqYRVo2uPyuL0G3jxZ4pxcjZry7gNf
d4RqpCOxshWJ2wDae6EPe20wZECjZQMnRKU5/f+kLl8HJiiqNwnS81XoOLt+d5HIHQTvphHxdR2F
sVZ4iKMXpzxGCb1i1VUKTAYG+iQBUMVZy5gl3iqYDgU7eOvn8PYYSES74FZ/TTlpX8vXOR8w8cqZ
MSUiCnH9nJnbwk/5nrhEjyI+TSxCY3TPMgTXJHRz9XUmVLBxZ18Tlazi9gCu5cbf0d/U+XjSvnA7
FB5igeHJVdGfEBh07weOrvVNt+o6FyQPTtXmYnTmChlWJLfDJZe3gqkEu94EDv8nDwATa4RA3s8n
sgncwMcsh9anSVz5a6BP1mJUmYOV64E+c+lBepRzESeILQ0Y8pjBV7eZd7NVz3Ig35r8zjuuMglN
vcqbQy6rE+ci2eXd6qfWfmSrUyWJXZQ8FuLvAxpBFgSH+hUmZnrl+TTVIEJDxFcNOc9fas3TTIv7
LadH2yYOHSLbv+1rtZOe6zfmNnhsqSv5mV3YcQXnLH/Bk67gKck9d1pUh5emvlwxdkn0VuqNeiYb
+2wv3/8qNWnqh1E8mcU3NP+TVLtTA9ixWkQDT2KqQzz5YQHLKVJDaYmlzYyA1hPsZXUjcDUhPuVt
PYikJTwzHZNAZF0czyQjlVzoU41NweDUD87qY+4yxde6VXpYoeacSzb5YO4Y033+lpWa2SNH6RW4
9j69i7LGzEOXnwiTem4eRnzH06t9HqF7TUK3+oP2puZ6s648Eyi6lnlZqO6j3jrV7N6o9VB9N9N8
ToxdIp68rz+ZUCrXDn8RhLgEpyMFrjjFzDev2N9aqyJQOsM/S2WtNycOsDcxuCQwZC9fNtJ60rcr
SMtqSHvcqcop9HsW5fKXaLT7DjTc6lwXW7hdX1hB03bcKpIkQU52GZklPxDoTpPvI7NKp8loGB+i
K3UTYL1IyxYl1Dh46Fqn578CzcHB5jO9WJvvJiK+XE4tZJ4q+OQR7fKl+y5IPDfIXwG/3DZahPsF
gnLMjHf1PPQerE/z859Byf5remrVsD5gwp95+l26mvG+dr9Weze8jssWwXu2ew+4OLZZugnRgd3N
97G4A+QOdqwIETirGPXEPeLSGOB/kqQ9Kscj6Xnw4L7f9kRhdEnxPx/hohizEf6NT/Hq4ZBOonkH
9FU+PxAmZFhbgaUz9L7ieUBF9UIqSd76fpWegqMZtUmLxs87UkQ9p3yFyoF6x6mHwHR5aRip1+8d
pL8iMc46FZiSsGLW1zcTXStuZMI00oKTRAwYdL64NuEr0CnQ+nQ/iP+6jxc2Y6tJZCWxpkANNl2s
g/GZPcKs2fcz0Yd7XhLNDg1NGKvZB73YlfZZFsFr3eFsSE7B686y/+abjGRRBHVxU+j2phjsFEBA
/rSYdxpYxM1IZnh7ei3IsWSSnzMUjg67+AmeVL9cjuj60dFr4OPSGO3KWNInb3zUlx2H2FQPHNOl
5Y5ayaAC4E52KheYBTPOW+Yd8ds7wof0z1zocgzrOcpAjNnOM8ZwcTrKrpsVytITd+RCh746zudG
Xi3PbxBOtTMFRMjSSnSAVkNy9yLrLEvI1FBv/ARIcNb7igismq9Ze/vRs71c0CGn7xTjubNBHrBD
+afSb0ELbvZNC3IbVU2lG6xy27VB4vBsRQSKWy2kF3kuA3cDconnE6m6/tS4Domd5jGKB/i2FPcy
GeYAqj1mQNjy78whNWA8gB8wgaNwUI+0hkcjEEpwM5RcVcbhiU9QcyW+1uR3N0f4XfJCSkkkh/Tj
1pdPnnyFO81IEQ+SjBSpjRkP27fOAqjmf0j7lBffSmjNrcfuSM/NFHiWv/Yw2AEOuZaeZHjk6dBB
O+ESU7bHWjBVXmPiQ03uGsnwTJTwX+sg9XMYZ/pkK7e8WwHTmj2pgSFOzTcpWOcXLhfozyP5b35N
kKVJpvMkyih2jf4+rS3ty/V/Vsp5inxjMcuThMCsGdjNgbFGmSTtFvV80SkQoZAKdbCJB1O+Oy/n
vfu9BgVz0gqEr95dPNS6KbcvEsKtOPwxXjxPu7l2v2B83F9MqbgUQRqgMD3vyWEdzHw5QREvarha
/BnxCsVA8Kwcm9PYxJ+UFIIM7RCYgd46QLm+lDdA/4HLuJFvWHrWWhk5WdMk5iw6pqzh0Wge7/Sb
h40rpMOwV9LUPlRbgFYkTfMnaYz23osHVaeC0XzesoHLH6+ggGwlgBzt7+S9Rr1VomHDHyAxFsFF
eVCMAO5zWNfQyLKzYNNI26Tx0p9hRgj3pdWTVBMNe9U7NaismdSk83jtZgw/Y853FwTw8QRRxi/5
hFl3CM8iqMvGmEHQgtvFwaduaoBPyLkPuTiWwxFiQSyppLbquuABAIw52PozDZdH5Yvd0yig2SvH
3Qf+lxrS9lEmUG+Yhg2PQrK1eeErvHKkLVjCl4/VYRHl/HUU4ikpK4GX3cnJfk+mBGLu0dxu5mhs
DkJu7ZdNLC0iWxgvpjCBPUTn6vRzHtRrEQoWaN5qFaYWTbzx+Ez8cF5Hj+WGFrBVOtMqcVp5eml2
jlmKn/vccBc2lkkY8pvFV6QeWlftSIP1RPbUWpuqdhg0bEi0XKrFWKrVR2Bdg/VFHmF1BmtBFza/
3iZ7ibfMQYv/bigb2D7kh7hEoTi5uoCavrZ6dwfbE7coDIsPF/6O1AeqQR9e3XgtW6VCCLSOMXfp
QjpBCCiRVbqA8dsm3i9rejMvtQLALT5RbVDcRwz9a2fPCH/sv1lGf7zs08Qv7bAsVaY5yf7+/yK7
6QvCjqqR5cbubkPhIMa9JKbYphgsAjb73TfwYWNuaBGCz9H2ZmUB6lHSdcwtLwjdBoS/wccRTrXm
xW2bsginKVE+oKMlGybYgpGXO1tBjx15vJCFsEgHrexhoMWa/QRXkUo6niggpkhL+SqQDWUmu7z7
JXHRvNtDpzl8HtbJAIyeUdn+RrfM20foM282TZCiMg3SCOaaBN/NH7+A42BNuHm+diafSRccrSP8
39H4jSRJ+PUdf5o18t73fm7foej53oFWD4IhDeUuKg6lx6hq7SLToTDcEZ6ZbdpPjV4kTocWXzcm
GvqxwOydnA3x1a5vCZ0QhYlCdrIEw0SVjjRftfOw7T9UrSmGckOyRjC58SsYAJzOXDU7AbWw7/ED
A3DFLrWGthVWN7jRRYRfjWkR7PS7gF5vnKK2GkhpRa6G4+C3uZ3aPx4O0Ydur+O2aYn9s1iEnOdv
XeropUrUyi21ZzwHNoLd9hcO8DoN5ErBh11SRhqHi5Vb+4Op8vRaP6P9MYLr7Ao/CN1zfK/M3f17
yRys/jJ0ZSMk7mNC6ALog7n5zyRhXnQD9KsfzHBt+ndlv7kjzWSkfDO1YpW2PyHAGtwwXoLMpUkx
ExPIEFYJeJl8CGFzuiT0VBGgfoFWTaoFObvSu7QzA9iZ9n30I32oPkmsLYYpbNezr1Dj+2hb+TYY
kKf6WpQ5O+V35DxOBRar75ZNyIUAYrQp+637um73Sphzk2Gqsacyo02btoPab5ApajVHw4boNny7
dit865De0X/6wh8rT4lwu70pCDQ1sEypD4Ef+YWV5cT3cA2vuDYfLp1Kglmr8EYLM3xuUMcRZJLG
XgpUJwEXCtkmDlrDe/lN08Sf7xBeGxUk0dU9UKiZ/UrXPuKeRkNOtjZlyzONQsLZHrmd3od4F6FR
1HqtLitIf6Qc2AtbyRN/5OY0e9FOar0HhXtBuMYps3d+In8N7vEOGEtpMtXJeQb7H0hPvImex0jq
br9gu6nxOhyY7ItJBHXrVExNZA8TYAMC474l+dxTvDzq/kAmP9xHrg+uHq7Slr3wFYmUMGzEAk7c
M/5wTeg+bxUkU1qFW5VBu3iw60XtDvAbuEe1TaO9qI9hRrnmkP3q4d2Tz+eQrc+WqB5knmhIdn6V
tfREdetmKtRnoW4XHL3kpOhdpF6Q/O3aRsSdXCdj6NhylMsrGOJSVw7A/+kJ+Ynu9E8Zimy1vgmZ
UNUL9hEeoC8dfVdO98b/Lm8+j/2xYY+bJE1oKIJBvtVEfsPel3nn3Dl9Al/qrW1JTyORMgSbQCaF
cbeyhEfuePkPdpYzDZ/WGY3JlGw50ImZtRJJgkYXwRbV5MkYjrg0e1u+Qy4vnRbqrELsvmRjS8iG
JNeGLNI+HJQyrIUNI28ankcgINvtfMncJTU4uvSTDnJeG/lMHDsr7iOHAMQIAkYbiAks5eh51Kiv
WqDFj3557FfPEjuLo3xNlZXd1HVMUOuyHFQ5IQRUVUlhgwWOCohgb7oUt0VgEs+JS+0CRlghUUV/
ZhHX+w/SXnkXkbBrkzuMF+mBGwkDQejVGpVp+EbZyLtP/4wIpVIGW2rw/rgNWtH3+nkBtNmJuJdl
OHROp//yhzzesjr9NNgCvUwq38PTUUBaf0dVdBvbdK+anNl4R1kBamkhEIvl4BNi/KD5bK5J6Bv2
u41FQqn+1VxZ6av2Tozrii3cUO42yFBpbwgigD47+9BLF9sv38WsyJYY2AeE82/0OnCiQ365q9f8
fGa2tjkfVqiPqyCeZQbGlbTp+ryfZYRHelmpdbUXUpRz3/s9bFWizvoslCViNPIUFWRGqJEdJjb5
jkXq83wyr/kh/EMqdaBujsWahplSBQwFUAZEZLY9FZrmfUe8oFXizAJGnQWdDi0DBoX02AJnIcCt
Wz2tf9j+p6ojJgPXsGn6/DqaGYGhfTVcDhswJwsLevstrgrJs7JeJAg3v/by8xN03imn9K+3BzFf
loLZlw5bjkrmcg/2rzxiqpVKlRzLZ6SzwDFuPDlyj9wudRV3Z+oavnXWkdgiyTxRVtRuP25VoWTS
HkFQnrmUxnCxVfS99R52E8C2drIMhoUi9rxErIC0f1OXBzzF+nrDIYsNO3noq+lUsI2j8B6bMiOE
qvfD6NCBK8XPWRRBjMPT3zItHZYJqRsRxDKnO1nX4R3Il+yFmgadHc2biex2TwaJpb5mOdko8xuK
UNXzcUhVjFvVH+h/xTUO0j6asFZ1nzZOUkc7SamyatKxuJCeEczuO/vNGykJIVUxunjJqTz44nDm
tcwqNyPf6PEY8CUS1I54LwQNsjhDqbPPeuCCxuGu8VRRYe9hoWa/IcGGNzad9Hq3O4D4WvrKKJxl
2VkcxRwWqtWk+8dA1RAYvg1y72t2kgkmWgql/5HHNp77rG8Mj0GAKl7bbMIS0OH8rvWoSaZuH1+d
AhRZJaMO2m7XuABAULki13TZ/zO4aBXIOZ+UA0vkhECiRI53FHa1uvrZFPE+s9FX/iiOHcoDZovE
8jmvuht7sf87MZflzM6k5yP7IpzyiD1Grlgnx5+ek3iRgGaNP9JSNYBWegdN/xJDFQUtEJm1bPvB
CjnUB1+YJWi0mhsU5mPqGJY3uuwQ31lzy6P9I7npsNrTyUCkTwGM4IH3c/Pb+lXa7rqKBgRGgoa0
GbVMqq9ezPz5xK9D9YUsacIps/v9nxtxBag2CJOTrMThV/ZSugYcTMuEjJrX/ONWuwLvNcuVNRz3
jPe0tzFeuM50DDR1cTDs841ogzXPwjCyk6Ri+RP1Ep+UagOt6niuZauqt6Tplt/eJ4F2YjhTtpim
MjECasQfclNYyJVX/ZAvLVR/PCdy5EtDE+FuunIP7jquW08fl9YSMG59qoHxTqGBMUzeKG7vhDdf
AX4VgDwEyafdt1+iqOEceTbqa+9Kp/c5wfuD5LvXy32ZLK3YJDuFrVTI94SjXP/6lNgfmX+x/Zi1
RPYO/qRgGJk3FSAvPLfsJIhcsTIgvMBslH1bkdV/NQ/okQzKmHW1W9ieQ4S2evvRwkDh030nvhEa
tY9Inr0gCZCKPO8CIxFP1tDoA7pY2GmEg00OtkiWFBXh2bsMvrhKPNYYpBj25OfJ3dQ2FidUqLfA
M5apP6+DlPlaZSwHhNXqdKXoG0S/qb/wItpfZu5W+p14xX02bk2upNqykInGr3vPsNBitZtk1+fO
BAuCTgrPDFaNLEzBjc1MbkLiCRTTQ4uc6hg0ltjTPFPwfG9xlgH/1wrMOMddEXj0IMCJOBgySQQE
r0A+NdknBjKPvOx0dwQPLwHBocEiQfndOTJDjMGCezmWyfmFAN4v+QKthtav+5A/GIrN2ABqqGFV
jdCuUKfUz6tDXZXfe+i0IYsCIR59AA3jGA38gppc34Aqz+lglBXmAKYVfCPFRkJM+HZhAbWhnw+k
WKIhrARe22hHfdNXNPakIB+o7B/BbegRsu9QD37jMIbQfL9Y29KUYrx07QvQTk1RII7PHba5u/13
Xdd1FF+DUeI7bhvK6NxUmkGKhAQjZLLCUdAdsKy8wsd1yhUfr5Z06C+hEAUxG8311X1u46Ml8ncw
wJ2K6nNvofQuxnu7SS5oFuGD49igXj2FmcBv0ULyVCfoLdh2RokVm5JxfYSj2AivRD6lg5cydhZB
W5X3ysWo3LgrUB6XPZOGrEmlkkz3Q3xejOhFrt47lHRWI7HIwBFsZpZoh5XkK0XG/+bQjSkgNBZ8
Xm7DLf/Th1FWuUW5B7KoMEy6rzUBUXl0Tw/JzC3p2DLLGDKw0DagmQnXn1+1iigcljlYFjMBTMrq
Evvcpp1hqe+PHlhyXyA9WwyMr+yVJLWGY6tku/YD7kIHmVqmD6J74jW9sRUuajRdVabXvFIYpDcg
Vk2svBUfOq+odh67oRV4yD02fGfCcq1nKX4zh7qY1FUnSbWdBGTOSAXgm7JvSKJfcKMdVPsw0Ole
uOY4DqRwvW1hzuucMcO1mCxgKV4jPDrCAAFqp6Ccvv9oXUP3HKUmxsp7a+viJjTFb+cOVvaDL97t
zd1BvyQGlrqgha7bJIYf5Kl0+oGLPLNspKwKLBk3FvbcVQl6moxGjejLD7yV5Ii8OhbsKKhhTCd4
shQDJS00aPsWv06GavWaO50a4tSa7imNOs5okD/LfB6yo0p2p0XLX89YHEtH/6FDSHAsxkZNMp0T
HVScEGKKOber2rpkUOT38bdGs3+yWFQzxDrsOVyfewh83p84XvY1BDhl274BdjWIx+8pL1ZQ//YD
vxtWc6hVwPnaR8l6Lrk7/XFPF5VY4UuJ+bDzN1aK8UEmHWRNRSljOHDKRKgOtYFR+BUp03nS4Go4
3ppxPVdJXaCMm0cRKx7XrfZnQsSLxxumJ8JmivlmdJfWH6RwkT30BA4OQVWj9M6+el/s7bl2x79x
iyeYnX0AiwRq9d65jQwfVqWUQg+kNv2H3GXiS1HDXmFJA60ZafeGNJJTTMdxgHjFaLprAImdKWSm
OLPKjg5jxuSkLDRIIs3SPW/hQSX2xX0d0F9yrbKHOcm9Kwb5Qm6yyasQf8bdkqxUDpD9KtsOHhbS
PaJHjmpv7IEjh2ZS8at73TEmxGHI0a+Ywv57R8W7ceOcGcHNGqFP+GhZwEMZINOtzIjrmur9VIiF
eI109sIprOPXqehAXaqn6bR180xTT6pfMwMTbj5Wfutr00VPEdDYD5eCoCCRSkIrPZzmmsiM1Z7B
u4oVk7eWXcx1Qg3djS4zsB2g4G7Y/lVQBbmSKHasQjHLt80wH6gjv4PeWh+B/IFgWgeHdocXoaup
72MRvzCi2WaY4GNUJZNzEzGgnzJD7G8WPauOBKlvl+EW1bOxGT3bTkpuQJtx6wX0VWkLHglhf0gJ
W6Kr/qSKaK+8AE49wIzoSD6ud3lO1NXe3W2IASqKiIRH/SBNnc7FWg/aASO7rKylINpV5lAiNhgT
yp9uirIsGSsVDQWjkl4OjHQE2fjsrKdIOZfpmRJgjeuZ5d7PZPWcOS32OIeOCeEH8a239AFzjVMG
sr8Xm72yDlGeQSjvbTG09Sqo1m1mDIt9LAauDNHc9geQcVud2t+GWNZ4PJ00gOkTy7CZoBWWn71+
aUtZslbRSq7io+8WX617vNoxKdW0BXjg7yjF74WJFdHBongtlVF7dj096y+v4bSfU9TSNEgbUAU5
5yJE1BbXs5VPPc1+e+pUubPrfPICHm1W27KSNOmPcrYQK7olTMb7YtUHx2JKq7tlX216QAm23abv
K9ugjyCw47xP+VArVQRBPibLUjgS0PVwIVwanlih28xf9C6oY4jh5k8qknl5g59IiEZgkS7c2TiT
9u5GSET9z6GaTEgxYSyjIiAsLghRZ/FGQOnPBVfpoIZwyivm4DUfUaFJGp5hoDVa27CM3xFO+vuK
BzTcEswjHNKccKsPPbcAbjYCVoCiFJNBtcR3CaVRXVbpXnn5+BpS7Dk9K/fwsphzz7zRhYNYaRwO
Up5QuFZXsd9C+Qhfh0VRKIGGFncc9fnlA5kePnwwYh4ZWXe24lg350oJeYbmn9BGpMG03w5MNRef
YTqQ0IRpLZHF/bbOTqPSHChPU96anztSHgUEEyndOnTJRIIBXuxCtblDLpJqnEpNcAB8OGytsCiK
TF0BpM5AzKP2Nhx6tTqANx9s3jEIkrKbZRvKQ4drBylwsLrqtpmI3gXGSXCm8sTI85/W7kpRUYsB
VJaxGEAbcw3nZN4t2nQ2jHvuZKY87wDaSyHfedYcsaUDQbWKbvPFavbUhxdk88bf5YZDJedwQXBu
AEsGYq8vvPT45ACHBKzguYy5541IXLbaNmP82sTXlpT+EE/YOPrpZqVB9kPjhi2wEWqj083mjc5w
1lO3MHP960ZAFhSBYu2KuSTEI7gjZv7SZua9C8LvEKhF2VWbA6sCbGSphP57oS/qcN5AWIxCRKYE
4GxL6NZHPYEqNvtvVbsNvKRIbm/GC1BTbJEj1h/nL+OgOWPT2G0m9mWSWrO7kgMTutf1syZjfhDT
PRCUOxRYaklpKD1cfrPBDh7r9IT8hlSgNim12qqKk8tZiJkJXkfnijDsTbbXyVFabPhKOTmL6uwF
qET7j3QuNGQ1LIfWHAkG3IcPgKy6R0h0NWYfaRRJaamesAZuaEBa19D3wuWbiy17VqkS6+W8vFzq
CgB2PoXGGkGwOIM3k8vE5p9qDzwH+45GJhkPGhm7i9q3fOJUwlerdF9vwS91l657A2J9DeySQndW
pyNOdricYxv6yGITt5zbPSo8s8emtooEonDxMV1pEyYco6oghhSStHlg5SN4XbsawR/I8JyRsLHt
XjbHT9zdIja6Khja4UXH2XyAbv0a8Ol2HrZCoER5KG3EvRPiff8KDUH9cdbZNXtacZi/WxC6b7fk
6q0/rC/PM1sBu8/ZmxcAFBOmfkgb6xEBQ7WrsRdqG7mCVc2j44BkhQbN0oWFicyurP46yTynKQap
r8ZNEH77gcA1fvNgHPf3Bz0iFoxssmPjfomVmdf+qSSr69k5XzhxflBPV84+TWQ9ldCu34KhSC8u
p0xDRF+iNMD4qlCjEB1KSsLzZ1DIC59mMr80770SRwcE0dPPIhqtxSAfP1hjBb9lV/MNEZ0D/Mlw
JQd1S1L0wD2/qmQLP6QDccURap5RKZzVJCEXFqbklUjhYiIsKytU5YlVWHJncVQu1B6HKWBfV0EK
cKYr9JtNyxcjgwNbAzIeh5YazWL341O6aw1M5hWjE5xmYBv5bOWsfXHTuPdLESF3q6YX/L0L2FKO
iOnETzXERRfAwGflSfbOgl9YuOTr/tT5ZUpeFSho7JKeqUl8zEQgNQqaa9f35501mLPUcr8BEmUD
mLZ8afeSMNp7ih3La7P/nUfRTeaqWi/SgAOs2pEzKnCHzAXICpzwLZqHtSznnI+7FWSU0AWIaZnh
XQWRIP9gnEao0C1wUWn6GIhMndChSroOo6TSflagRJP4sKGwElys+6w65x5FbHgqLXxEydKXGPeQ
e1TQvZz9n7rNn2+t3hBmHCKUu/HHzUU9GzdC2poU2639v51UpgZnydGypKBVqBHN/HfoAgoh/D9j
ehMm84/E/vBNcmS+vdm89Smq3RftsE7KZZGsRR1G7M2s+pklC47nnVfpdEU+/lE1QCwDVqo9jdZc
ON1At32fmbuOnVHOZos+iSlDOtLbg8SO2DmBAAjHVhD/QJRDXCdZMMLqhj7F6ADCiCNJ7qvEEpfS
SNEJ296bPZw6pVEYpCTTb+kg4tvRxEjaTsJTjE3oz6bOy1QS89MM3/s6rxtyE3f3BQgAZvfA8e9s
teY7sJbq2jViS3LE9Q3KHZI25cXSYzKviVCK8FimOxlQ9KOB2ekLqQ2C0m5aXpSPYaZTKfvT9UAV
IsHndJMxjR3zN4D2Xk7QQhOJ5EFu4VY0ev/mfaNg9mNnzGPVxJaoQO7z+DsaUNHoPSgXUnNKMy/2
wVo0/6g8a4q8jNxN5aC6yG3prU1SO2FFw4fgnSJ4l5RQzBGRR0IjGEAssXv+XzvbcHKC/2Au9qrf
oYfW77MHhuayUcGwFaR6uU80jWhDlap9CJ58jqsyDvqluLRdA7aAp1OzJtcbd9K+yMyinl5ggJZE
j5fHU3Moz5XjRE/IdnZM+zH8ODdRtEFRY1EqFElKG4fYwYd+iXSxwyIqWdk7etcm7AiUqE6KG/2H
s6Qi5tHIcW8eoJWU4QsFjDuvPL5RDuGjHueeMK+fFxoKfM+jIA980A+hY+5I9Rc6O0g0nAQeIKTY
2c0AhCKzHMUE2T/fsUjW6hMNQ1adaF7U+BxdMR4ZwYvfVQr14ZZ710ftrJZljqAOsxhSlPYU7MPg
sfSBvG3+o+cLTXiclKx+fRz7nswku11JaIjjwmKAZ/zGleVJ+8VM/AR70amP9N0s8W9pAE0CvhGE
MxQTa93yAdWp646o2mGk0iqifymE77cxKrivhtMLB1yclcgPNHH1ZWBodSDR1QCBa2BGyeV046Me
Fatjunu9sC+ptxhzo9nUQPvHgEBwlvCjCSezBhj9dNqkOjUnVNqbp4YvekMzxrwNZzYMFJfL1ZTb
pTwe5awAtCXgChE0d47hi5Yfmi6jBqA1CKzCjPHkRKW3mU6QSuw3pkdQTI6i8oJTdDIAX8OY6+ea
pFcLX4QccFdnVrJfd4S/7My778sUQNOyxOetjuyQ0shpSyQmaDP358vVmWVxJhZRcpV1DSzN8gnD
MrPGYB+gb8SiRwF1XObLgum4o5Mny44ihNCzTSa751ItUfK3+ukqS4m3yOo7Yg7+UIhzAs4Mf9j+
abUQ6CtZehF408yTVhPmgTPLztmIR/+/do9PulLR/ujoX+xNBCjMpEK9LTuUi6PH605k8MdEIX32
3AD883ZZZCpqYnXUUgFXAuT/zj06KJX6OMfnzoYrYHnBwrVDpWV+tQtQIOOGJY0LHWbSKRh+9oJu
EiZmtsHhcpniWJNaiSujMu9C5QqhMWFinsYVruEbOBuika6oSQssFGEFmOBiUcCEnR/MeViTiwVo
MuWyaPWL5TtRsJkfUeKPApdJfR/ytqlNlfbk7n/+EKcLFBYSJANeadVtn68kif19SsESSHgp5h6g
OZHGHWxWFybfbghLnfVvlhfECUmo2QOiiCcxsbVQczZhSwmgxP+GQ4nS+FWu/Xkhbnja2cUVkczU
hb8kg07jXft0V6VXyxZMkhVLCKh0C38Xpi4ThuUP7CJ+cZQswf8McSyNrprfXgXG3imO7k02PuRq
XKIKzMjSGxL7KEnPPVd0MQfQz6cGtrk6LlBZEVWV8XUUSSG0JHZa7W4+Vtep7FuybTqyyw1jt32O
bUxT/Y7H7TJTHn5yTUOAK2+QMCsgQLZM8MUTySWckUYkiM+AViLeq1ha2MhdmHvRMV1ua43dED7K
AgxduXCkq6Z6ha1gwDUxKXzgihw8w7VsMKbWVqpbTBXuPCfHfn+9uxSqf32efGRqTK9sslbwe4Yh
lDhr4KA1QvcB7YBIXTLU5f9Xqx/wACvVcKi1WMnYIA2Fr1c00stRacFOgKHh0yUwj+Kss8Ak9yJK
yFPwsKmbt1W8xDLAIppjv1KZ4NhkiaO7dDwDmEEIm+RqiAXK9QcdsanreNB0VLKSCjchrFXBPKk4
CTnbixUSmCoLEcjqjaEY5KyWwAIkgOQOf6opQ4mnKodzzn/oxOaTj7ReQXMK64za/ernLI04fE/A
lYqd3+fkM8XlCu/8Oipx+Ru7OPXVto2stcj8Jv7BeBPto9MD88hw4ur6/yBtNBRrHJlkiU7V9Rsc
P8i2nukKYjmcU3vRHOtPqEBO9RK5V8k11sO71xiGjEHAwCiKNDqVEzJlI9r+W0cvjGxC4dLzrzlQ
DF8XThiYbiv/jkmLIFGVUWlunNPntDEYNvKc2et6utL0XdqOqXxj4XSeaxzTV2GFdkQ1k43neck4
TtLiF7zSnui0RwwBY3BRpgIZiEUH2YHJEsPpKPY2oPgML2TcIkzUls7uAPY94XdjFAQ6aI9KeTK5
Gcrqv21jPCqDHc0z3fpWhacVJJH+PCRZlTI7DOQAWNY5jn/GPYuL3DApq/NhXei3Y723EPVukZKq
PJx346Qwidx/795aub9A0WaKGdyRz8E47bXI1RmLuUXq1pTFv7dcF/Khmlx3sUNBE1wTqvN2kgIq
3aFpZPYfVklcOBMDY87i08reBIn+jkT7FrbRUDnSQXBxydOGD2qNC3kpa8U4AFTKylMfxHwOPO9m
Y8dMwIUpb1WyemW0uu9v3YDyapw70cXtOC6l7toY98NFtniDt197Ws4NY6l7qOmk7SpHffLxqdVN
W5eqPCYMWud8vAQyhbteBKm7AiGfnS26MRIOC0ewWrCFyAP2x1NZQWd3FvDE+zbINvUJ4PqFcbHo
68Z/9RGgDNMl+tSTRgBHHret2Q4IiPAKk0nFtoViv1PGAZQSp30aylcc07J5MIc2ZMYtJkJaQomR
k8Z9mdSzkm0rqeqIRbLxlM4jzq0yooHlSuQwgnKi66ZcTmOFSUm6QnUketeSpQ13Gc5kN5yR8mCL
YAE9xwgiUDgMn6M447/H7tRhS+lhGMv4DBbh5255rsYhuw/4RkCosXM7tjVLPT9meK5L1rVkkOYH
K91N1mWdgpn5KGMIKkUKBGVMSvToLkMQOgD+PTDecS54gz/t2N4tgWfyxqo55XlTtj+j7Os1lOFe
ItM4Or6BiA8bYGyFCzTwXbNBciKzSWuq57SVa9svLlfPXxlKbPf/tSv1O/OaIi8AlrUZPf3zkzGb
+9fB+MS84sBo/qZtBbvRRwyBSb1mwQuKvXG6CvTI44cu+WhlQj3D+kAhS7D1+ImHJU6pDM6EQXYw
9e6uy/0zoLUyGV5v7L30XgCpdjraz/vBW04SMGV4DonxebSPARwtTzFQacFYsaSyRGbL5XBcK4jL
dhxLasiWXQYkZsvmQpGven301Q/CSRCZflGJ5icNA45enllbPB5iTfwzTIuBPDbul50ah9dvjptC
grA833PXks2FPsepX45tjRWcnHUpMejycthLgg4zCq/O00YGkbLujBCuaHTiuWBOFeMKNYJsGNTO
Uf4IeAH4gnCBEMsN/fvoa5UQi0HNqbdjyGZoIsE1xk2UVLe0x21cLyJkHyZsW1V8HVVw60vKtKpa
3FCt3HJuoF6hCnFk9Gin4WwbSjELXBCfc6pujN9jl9Zu/ZdHZHKEsAsRd+su7IiAfZ48WoG8P50k
dMQHFax67mW8qlGd71DKXmcBKv6r7nWShDsfI5GqHO+PxRKj0W/G2CDgLYycNrlUG5JEL82Xk2Ek
rV8xYXbobw5bVCRiIbeZBvhQYti2MLT7U1JX0L9oMhHLF4HNyG3dUK5NoAgzxYBvC+jpPqSmBPkl
9We5ai/P6Wny4n8S0FIqfln+vqsEQn5D7hnWydL8Bl4M7QbRozZEIVq1+WGw7+I7ZDdBDCB7cmSB
jWvwvFZUFNchz1F9x2/0TFmSSYIlBAhsfzfqc2m3qsOoVsM2M61enn4Dwn85jmsX73qP9L//c/hy
tM4UmmF2d1iAf4Oh8OJ4oK+EvzPEJwA2wrNegg25KzKNI4Fc0b+bZ+UjlaJT3L/BepMrZ8ecjtyJ
SvuDTP3c+O2S583hnK3sNBKy9XJxuPC53BT6JJepmx1Gv4cK8RgAihvwUnB5XHarbQzmNVQd4QaM
oIjM4lk0UE9azAzC6Fvvr7aJBW2PYIlgrG/L4gQqd3O/cHTaa4uNQo8a5wbEl93vR23vRfP4w73B
wOJpv6CwPrsCXkqrs9R+r+oErWDmtrHWLadnbpA8dVekML1+sA6S0lIU2578cUcYfaeD381iROzv
ckmax4LYtuvemOcv3HT34RKmLIAB/f9rmW7jDHr/qeMVRBgk9zdWbYoiElmSq0ArSULkk/at7wby
9rbpj7hPSFu/g+b5DPfsCVrBrWY5jxsq0pHWnd9Hprtg9grYlpnGtThRxyCdYU4kiOAt+AgGmtsV
D+pz9qAj9lbllPOGJN7VEcmD8gFIT/K2B/Cb3hTBUKMvPhOKw5knO5OYhkjFl+kuPR6DaYJSDqqO
TZyF5XiBBPFK9yI920c0GCG/rhFEX8usEz5CYW+ajVwLbTShzPbyX5/3UB2nLs9M3ypKBQXrI3vj
05H3WteSkfBAHqymvp/tIl1l449cXoQHSc4yG8wOfJmRAoUBdHBMmRGZz9uSMfbourWeqMYJeTAA
0Yuxhgrx+sonhv/5aII1kgm4C5yp8sEGNpjwRDs2M1p58qGE932TEjvnvhRGvfvH46Q5T6YFBMge
BI+0IhH8W6giazOekJvNxYOHYmSG1i2voEzWjWQ6Y4Q+8aPv3UBCVXu3HKlaZ3CciRKEbVFf//9x
Jtwl4HkssuPaNFAAg31wIVzzcQCxjEyH+Uru8OzQpnqKw0aAPvScc8HWG9zJhu4sfhY9by7LeAQq
Ya6TyeA4cTmk3ukaDyYpdMPcTVA1s9lhEe1q/OdHEliJFtdUqSPyrOiXkbYyyzWwd9Y2vNR6jiXX
CSbdDkdNTwn8iqHMqm6Q95fHxb2YGmnECHXNO8VPDCDRLniCABairQg2WS00RYg4oNyYYk34GAZK
japR9LhT1+EcTob1kRPX5lzmibEBwU6AdHh3C4BIoePwZsJmN/VUFrX3P5edYI9JM2rZ2Vk92ADB
HYNpss+1GLGRwr1ryRkhxEID1gJRJySFxpibFTkTolDekM74uG97oosQSrjGHg3mACr48H2KQodG
iOHjdxMU9VVyew7OX6keLXQ9ExuMPoyQ1JziA6LZM/Nv+s4XKi5QzBL1+ydnuaJYhIzQsjWZ9A/J
yvYaGJF+xT0SC4winhg2AsxvrZSb3Sx+SSxhF/A4m0HqNVFxLKXNT7CvIjc0tsA/tIli7buR1++M
MFinFRrfuUJAAuBqSZ2mcYuQKnmH+ChWvMz5kIm1awQJnlkD6msjL6ADDhb/dqTgojqM4I5XKvnk
tz5xlwPLL8t0qyDzu//dSiinNKNtn/bR0xNDjezua49PFHZZcCf8Yg4wovbss104FzJBsyfS0Hoh
I6c75rs9sU/r5ida8gxTqfB6wzREG9pPVUprszZc1dMvFsERWgC9y+KodFUpqSEMHfvTuZpAr6EP
VjAIQpAMB2yNvVDkc5FvlUa1I9moKAryjcgjhIhFpWm+GmU77kdqUwYqWg7SA92z568RMayy7OeW
cOjdQxpEL62sKn0akDOdU80YdTOQoT3mhxPoNo/LOIjPmICTwssOVYutj0BtkC/5LyKNkBcfv4dN
iiytzciZecf67KB3uaFGnKzin75CLW4JjrMwplzm6tSvAkBOO86OFvuOX1jOg0frOcGZDZPfRs+H
ilDLUWKyZCQYcc6lHg0bJoInqRLJsGnXwDZyLwvwxLxjRJb4m0Cc4lj4MLvRLGz9bc/ZUiAgbbij
x2VCUjGkoGKabJb+8i09coB/Z0MFJ1Z47rrqDJFk3WhGneTCHBnaveJ5OaJmuDHgBMXCX5crEGca
ls4XM+RutI7gghOfenStXEKVRAHJTFMUyemPVA4SYtG/pG43Mskv4UJMJsM539D8XBkqz+BhoEU3
V77WiOgnzch940v2vDMusDKITSpkxlhsmUFGRFVrD23Qs/BLOgEcQRaASkL/8te905ZUCaoY5CsB
JS4TUsQ4ybTFaFgKgvbr6KlZF/IUqQlegL/ByaLLFmqNxL8bjXEP6h2y7IqDKNvE5Un5I7nbNIGq
1mPqu4xv9wTN4hYV3AxaEgsjJCGpg2yRtQDJ8ukjCy8x4/v04CxVhV2M0uoc2NczFdpMqLLWUQm8
XGhl4PY2al2P6xamtRLaDJQfitRc9aZEhDQP4uyVFC/Kc71fwi9gE4gWBt/D99LIxPbDKxwZb91r
MUtF5bTl1yTM1tmweYQE60Mk1dWqClvuHVbB83wMRU3uE0i9iha4wddzZ2FPRXSyBrMdkUyDPwyb
ln2IckA6CoH0yNW/A5fjS2FdMWW+gzQaa/4g2gofkGQqCcOB9YphhqmV+V11LGI3dGfv7dNAuXto
KC6Hk5OujWlyTO6nhgFysXAzVhluTkDuJmiM7iuUaVeypm+jN1xDW1hoTX6icr57GzTvBME7Db4l
mGrtrrU0pCuFiyKvtCPN37I2Gg4oYJIxPggDMdk+wL3wPolsWdbeIEPjO2exOlE5gKaYIZSq7uUp
AKZxxscqZ84wGURLgjxN71rQd8xy4l4ZbIuIBex8ErtR9izY0rW9kylDIVQV0uVnxUcXgiQkdhNb
RptRd9ZHrq280ufvPiLAuWxVhLEjYNb+Yf2QrYL+Jg+gX9gQ6of7X420acfHQxbblAASC0K13OoG
/xWldXawB7Yz79N5V4CF4Vd7rxuS6j0jPTGsSewo+n9NULZTRQrfsADAzKIakYUAYCnM12VKf+x1
Gc6sQ2qquCtjSrh/gHxFPcCP3FWNAGmFoQSTquC7Y0Kb14MxqBvlvqg76cnXSof15RJdyZdq2KOO
LR4eElIAvDdZMOouIfk2yqCFL1vz8gxzFo/dubnVbwFWI6W9DpTcYoQq60sJV23QEDpbGuu+USVH
ORmRXPn+0baUU1viYyiooe7nyhjs+4z1uvkM6/I6NlcwQf8b7zyapwBED6vPQiTedLNZ04ocA5QA
jtZARM1lcYUCNh6V1YYoYTPdUhv3/US8tmlhnJufJQ4XEHtTJwxLVdSNTEi+xUVL6wVjVw4vly/n
vmmp4KGTw8V5M7lkR1IKzCwe96a33RomsGj3c1cqX9ts95eM6/RUx1tTjejXx469iH4ZPeta02wB
EigBkV7b31Xwfz0b/K4YNQFRokAr2tPiNWI3LFefNw0zMu5rQX7rwiBtedzERvgFtOEoxBuxm6Br
EaF3bH9DE7wfvSm0NphER13adLkp8CQTfNu+S9/GUKVDVKoB4Ksyzm6KWOHKNinrRgaN8ZBJS6Au
KPUGTiHX4oXY1CSnqD1dFfnCpnXNiUxaIVj5BhlNtWgndwydK0H0vGb/A6nTWa2sj3N5Dfgc5wI0
2WPj3RvDqjHxkFK05hD2ZK30T6PMlVbJ+2ajkBvwz0m2RjU7fcnqFLba9PXJaxcXtzsKRjgJPLIu
u2E/AvAEWfGQfQUeA1huNqIjZDNh6lGWy+xrExzXmlvoH54eb3fKXFBN4Dg6nGxy4lDwvb3wGkN8
9LXSd+w8F8El1V2ppTpDkqwrQ3p09Lfhp7PtYJXtizQEZPCMvgw2hVbjSIJsHPtZl+2t+EcmPgGP
iq86ZRPXvywQVfJz8YQ0yr7S12TIM+5hnrClppvUzD/lEsangHgsGdnmUrSKzvWX23V8FR6kCsiY
2fqmkgA9TIBmqgz3dH+vyQPnbEHh9HiwJm/oWNepeBHefhGHTTjdTAsUxpht+9v/2hPln6zA+BRQ
mQB1RjRQe5v+YGcMtba5D8Ja2rPsGCeqKqQE4Nb9Ab2Vel2RPDbYbN8ezCPs2FEtybyRyq1VnV4i
4bW1a+jT8kEE0cf3Xl9UQ0/xRJNfIxK/Ix13ABLvXanM4+5MDJ14wgCW5X+1ezbCQPKBiUNA/qo/
rp/YZbLq1TJ6VF7tOXfmIgHiqTQXtQBMHCCt/JqQ+bRzHX9EXTZCIVfb1zDuL83rCQaaSRaOoYrm
uYNotjjfu3dV9uwnqJQiK+ZxDG6GNpruSAfAkRr3CqeTgizLEm6DtWo0QmuttZoz4v/Q2VWCtkch
qvZ3RnF4qkCqBwACvxKOk4pLqibQzs9dFYMgautgLpr4M/RlyEEPGNmksGGzZbinjhGhBDKqitNY
535qQ/C79Rs7yrpfCf6fUmWxphIQm+a8m8yIo/i45xG76/Ij7y+lC9lEtxyZ1CcmD7+phnffNXQY
mDU/I83vENTes9gQwJg7ObIjolKMbfRyruFL5HUila1e3ACZB45MOMFhXCXx5oIewUtHAznlKOb1
yvf35FGfdkJ17WqmJI0SU+qhWAue5mXzMSiInnJ7DRfqzqJWkEuUTE8aMC53/Jq0Y69zkDz8Y4L7
GDY5pxMGu9ZcE5aWF8wwt3m8LjAg3xIm/unNqlGWg+2O2FXdUiAbFNCSn10aB/d+Rb37LuCqfH2x
jvQiOutYZCqwl9gOQMA18huHUThn/uUGT4o8fOsiug20UMGdwtVjN1D1Kt/ObnjT1MT6/CsLaj/a
mRyf51Ta2oVWAAetzAW3jaXNHYjZ8pyLawfgqbYIH3vmPY2hjwEcVQFxyFjTveXx2O9gpQtj4DFv
j23C77gG5Kul6VfiGfitehjIVCMy79jpp2hR6k5aTYOW5cdWV1USSBh8iBi90q1t7RLA/bqcptqi
1y50qVcGE/qFbiaV3P07WDrXy8UkhkLPOeFNDKsTu2IQBl6kXQQLSzfvGB2NtkknTiKBB3erJORi
f/PJr4zrg28nni/bNeuKk95ML4+pEySVDuNTDDwwpDglFgnHV6vvSmwu+VQWio+ALGdT+f6vxCHe
RLoUx/3gAlDwCslrKyWJb/J1pA8ueDZodrUjDS/oCJKhCFOkx3Os+qI4P7ZsCLM5/LAmICQiTd/B
BOh87528eRl0sypVXz+T1v+X8wIuW1Ete44AJsR/3Qyy1RGdT3KriNdpKn9enBceoEMkG/nOO7LB
4iM6XDFMvnxR8IaYj2QjmT89WgjevTlhFMdWkZ5Kz91thS3HjAMdndD9RzbyyuvxhjWwWf4YhMid
pgZLjrKbg2A9GcdjwQ741TqcscWkh/ZU1uADl2l6h8saU2lPJzYRgNqfiURfn1VFl/9saMqNHZmR
Da5ad9soGDtXLkhlbUIOvSqJcQ6odNiBXv9DDmbkXRa2vLoDkVRs+020Q103wrmXBQGtggZgWSjq
WrlCICI4RidQmfwtaKtsy3SnkFgToQeeiFvV/XeM5rySHVbZsdjYiT9K6rZOYl6ohOnjSaN7xeqK
CzeS+tIpd2jVDJmQzg6yeGGgU3k18H+DfOaPU4V90Nnr9it2tDypd610UBlyWBhdOW9fMXAbk8Yu
mtxIWkjZ6GnDfhy9o+hNUL91cJ2LTv0jOmrT/1yrQ62/PtavEmeHy5un7XdjIkCbBMr4iZ0r7UFj
wlGRjIEcg+LiwCsJMzwNtKsTd8G2ttAryubdRG+ALTKAhX5Z38T4OnAeAMVB080dcSBPMIp53wZN
6AYj1FQTmM7FjTguJw6S18s6kLEBN7x/2Iwr2IEYKstD+n5d99cFcfKlBu17LNy/KdBss9NJ9UyJ
gxONum3WTjGAIp5zTdfKbrRSPeh9l1XL3UR8/YRMpxxT9Xnb7VKbsLn8NvZoYYslUgn8QocbSVUo
b4uJ+HoMMEiCtTCsEONI7ubOMe0I99VBOIaM7faioclxNs8kd3vnzU4q/wQFtDDhtIgVKRmqsGg9
9vR2V1G1AUuhgapV5jK1L+DAaMjOba3vCbql8pSREq+12dHMI068IJCvTbvyHbfjuMoIQdMGwoKw
Y10TDkZV5GKVsdkI6P5yctqBHdTT82b7WWQZ+oVO/I8VshLIFwOMi8YE0+RvLXgI0HMjNSjL6duJ
TyFNN5RPILcoRpOkuOHEt76SzvQHEpcTsjoeJKwR5oeKRgD9wjjq3xOZaJ57YiC/VTSG54CT0e1W
NmT9nP3tygQuYJJ4okouLzj1fEwcnRyg2vY2qdVr9suMqu6ViePbV6r059shWfLPPi6XM09JpK2T
EHE7N1Pr+FutjdsTGGITTdK5XM+4uWirOYg8wlmwh18xC1HJszIgfzbtKAmoVeCq5w2rELNPgq+1
xAdgcXh3v8yRt490B3yK6KabwsglATItYVlCYXNXw7cJx1UAZE0UljaztUJ7ojjhm7M/tbNdlPcQ
HVJkzK/k32/TmO9vHJWQ5eGJoaEOUznBlkIXREeQEKT0c/GFKsqR86PGR99KMgIgRQvFe3dAltSP
c65MWvddJ3nI1JQrw7e4BmdCaMZ9pw8Z/9/uvJqYwYGikCQ6niYIBcd0weB77+9smbLqaC1qwxWF
exiHn1vtWG9lepG+KICDzMYn2EBLR6i5w7e3EGeR65fN5HU8/DhPT6p4UMwUZkf+p3NdSRTOs0aj
PpasX/MfHS4dnOGFjbGaCyygvlbeKmFtuKzvPr8jNdLpOFTXyT97T+yGCIVLiD+vzkPGgV5s2F7z
M6XaJQHW+X+xpp98HCpWt145VUx0Uw5fb2P+DKvAzkgWzgNFP8t9hyjuUcsafNFnLlpqfWQtaz9x
RWHNzxuxt1XotaC9MbQ0fMFT3q8ZVemlZm/ee101+3/G7KpKJr/rRs8qL2f/XYzcJUUOnYvve9sm
QwlDPYpAMQiQjT/6Oi07QFSjcdT1yg1abnLzzBPojlod+t1dVYYinXNKYU/ElH2teLY3DOk0FJ1j
L5gCHH2eI0qv690O8C/yoAX9ybYeFq6Q9CXKt2GsMOEWezDF3JcLjqJO0AfscgvAaWebMouEg29m
WGYU9wXEIBM0vHDWoz6jFNmnZwwUnNaSRzT6FkobPQj8RMncRICNik5Fv4n6knPEunf3m9toRTv7
AsEZBF8sJlYeUI5iXvPaq6LQRsBCZxOqscne/YYBEZUHRsi65PI9n8HvZ46hK2OZEjbiVW6wb6ts
w1hKZGDo6c8CAVQq4H8+Rw3gxmJCb1mrEOBbBBQFkkGNt9VDztHj+i4Muju8IkPZjaPLu5Ih72+k
KJupozvyqD5pNbZAgNQlnACJ+ZUBmyVFAYYdLK7FZGzYSCUqv2RofuaECOp9cUVke0sRgjOYGqjk
NXwfMpGHzEaC2Tmms6X6zbnIcDGfyRClUlOGvYlHLTgofsPpnL8dMpPtt3GUjxpOitqlaP1/czhY
9dnRsizO0jsRSJ2+RRvHJqXn6CmloLeZoqPqP7adZLFDybpnMRmVBgoFcTEPRbxUE+QIzdJ26ywK
SCjFW9xbOgjtpaqQb21Tkf06Z2R/GbfbnDs5/gXqOdtiDgA5UHpb+b9/oFfbyQEy+1p561A+Cdv6
NuMS7dx55x7SEgv8ZthN6dyQC0PxLkAXnQjKnxG5Kyu2CfKLkwAkQRgrR+3RoCw1tKaUvscfDxBi
idw4qqYA9dOz9Jyf2o8B+bItBdVTPHCcD7dKHHIhLguKOHvcGc7HdiPxEb6OFKZVdBi8O48UKceL
NM5W2RMa3Yevt9dzgo1ZQ4di3ID8PfdSnPf2K98CX8OOcvlrI/ukLJ1CN4OROpL4sQzC73PiWdqe
z/SX9uwp03eUxfPDvk4+UdMD8KeRT+Y2nUkrxCkY3UZ/Bp0gzkTSy32dA8JWvoyWKnfdeGAR4yjV
VlcVF0f4uhKc0duoewLV4tmNoZBcUn/bSsPzIRXKJ0GNvGVWTQHl8L8uoi0rFCt2O0gAMitKkvwi
9WjvUi8VvJ71Idjiv0A85rp91cpyfYPkb2hIZiwHxIp6/SlaBRlcBaKq44ng6jfW59p/ntNSpM/+
4wXDhM7+zwmWMPCwLu4032uxtZgCNYTqAQ0vbwPsi832z3gFTf7xhzto9T/ylswut2IuIV3jBdRA
w2G4y4EoKInJYvEXfAGdUhviYJw5keRjfhKVYlHJ911zNUSqYmTqVWQC6C9iHDx+UkoEieVSV2cd
Z6LF1jCkqAhESwEblodZ74pUVukH3T4SYq0BFoBgOLPs+olc8k1lhh/aNE5wQeFUfrDNhw1ypGpg
dVSgM+aAVHGkuvd9OzmXJq+KjSaoVM9rab93fACoZVgC8d6bl/MVdyDZLdvmr6EwMIQz/BYpCwEt
C6asXhfYLl84HldCBJQLY6JCoISIwogJBO0SWqSgZkw/LCRedTFofGiv/g822am9tRfMWSXCudhp
9wLcc+JA0qQipuTVMgLSztzW2vcOXyTXprxZX8HUUI0Ch2ryXUxnbfSINCYUuKbWh1qDPps2i/Ea
+bLNLmkDmL+ng4tVYsHS5twlxcIXVCkzXeNIhqazbui1qmYzEwk45FL/ca3Rb5OGUKiKo+UgsMsW
H4gr2o5k3VvHXb3ASWgj7pIz5W4wuXG6b61jLRVG1tqAYrlP+pj2f0i3K4/DZ5XLAjQZAZjAOzeq
UJdVeO0HHgm+/lDUm2ksJ6u2y63rDO8AxS7xDOR7Ejvk44XposL8sknnKgFfPgqP0SNZr7HwoiWT
gi5ag2r1/7JEQ9l9tchTyDMH+hMg0efGrGkovLor9StcA7qoZ9cpKJ37j/61YiTBpnZDEYui5ZAT
kzhfgo0kZe9jBbH0OnbRW2WqD70sltwUpyw+jQRJ7PMy/TRrp5tRQcqKZ2ZXz+io4LNpwHWRTArd
HI4VX0A56GfPWv3VF4/10EnWe7OPtRBPBzUiKS6gUfNa8bAIu6G9I/OtRjqeanuypYlFTdBnOutx
Q4ICyuuPqB2vdU69BWWXAT9pbVlsgzc68SUDYdmqT87q/uUnwUZXbKK4yJ9yprrrCggZdKxTyxTA
5xITU0is3/f9i0BwTIqwx/hBaJqDmqehw4RFpYOLW16DiFM/tUOjgsg2FtjAobQeXqLDO0ygZLR7
eoOxodGxgQDYrIj5qrCxTNp80XQcDEPGYsnsa5XlY/bKH0Bnk8Wl/0hOBO44sGRNMlthF06dbn9x
y584+wdJ483rz7iLK025Ee+IQWd0Goo/YoeYJ4c/X+9ulQVXDtRsMg1tQghcHa8zCaGODgvBUTyo
DnJhj3ebZdzG5Da3jVaLT71nnJ7h60QXH1/d5sgYMZXruVcO4VE0fk5zYn9aUrUyfBO0Npe0/NM6
Z+jCG+vgHCafHaOo2cPIqX1PCv3Mv8uRrSF17+UEA1KQA8K080I2jGk6XQEianx4/6fkXKZfyL03
POvukitLO2SdwVWpo+/XydKPAUg6+ibOQxd+A1FyJVK9vtIBvwLj1c+CzMdd3RauBAKIVuej7wmP
RRSYHSiT6MZEucq+fyXAAOerCmYoJuQw35NTnluCCmu/2H7snTQcDDNXsglxeNdvHAmsr57f06Ek
0hsPs+61CNh3M9OHSptX8SBjKcD+/eSnRaQgzY6EWo0xh1VrQNx40/4dKidN3gIoVPr8rfHZ8J4y
6aQsv2S9gUWYB6L9i8vStGD1oAmlZs7ss26Hf2vmpkIDz6yqxmVJbVvCIAmgPGtjFuJDDPyUX7YC
jzHW9/WSrtsYSlNYHxCUmM2IXFfgPEl6FjH0YKjIV9pBuOuljZy+YXBh2jhMvJUbzqPv/KIDQtws
bDa3pg8vEsbpwawvE5fHHcqEDG7nFlP2G/W/49Psp6v2hgk6vhi21BooEQL9HNmotbgigrIxMdkH
6IYgGN7eE1/qaFjbe4kpJVbAnczyA60byOYzJPsHnYBpuocMIRAZZwSniCDWtN2xa5XdLBuc94m3
OQfvtt2kAIg6PL3BgxlKy6VMudSfxAPFKiKKxx7CO3GnwPfhAQlbSFXhvxembek70MfdzTRtwR6U
iCVZ+3ykZsocc5GQXEiwpOM+bGmWTTLcLe1eyhHQf8EAEyETfyPpvB2uu27sjhu/KrEDABPHB2tP
o9DJSfN9c1YnUhrm1YGJioFhEIShuVrKi+LCd24Ef7ZXkxU6VemCS3SflZgMNa8puWXfF1Au5vfY
JQ9XKOUktNhGMPG/bzJ8zcFKbM3p8NnheSNYXH3f6vBootkRLQZHI1Bai+Mcw/FVoFr61EHtJbMR
VXCkccJUb4/mAnWDMATRnQm6uNOyNGlSxf4aH/+4K39OBxT4hXFPOt6SdcwM9SDL9uBT6TjYEliA
o54awfNUby0SboraMCa5u09EtwXXqFYthXsWla9UM4Nz9Xr89WF5FtjJV6njCzvNDYhSeAkGve81
+D3JEhsF9PaxR4eZGYe/s80LPkiuGLG2AF5XdN1rfvcnY1sf5qJponjJqa/RCTleI5lPFvVjb+y3
AY79XsD9rOzoYB2KHY5BeHzUM4XNHoMUxSLnF2p+mf1HHQ/RDRnLlqh28vbgYQBTOtqywnRRKqfG
0pTv90Qn+e6bD8yQxkUFJrjeif79tmdfoLyo9nXv76xqe3fs3zNBIhOX8Xq5hHrl6mE3DoRq5F7Y
5l9qJFoHRv/+iEiMste96pjvNe0TLsOqbdMALXDQ43kmkVv8x5LqI0OCOtvnTNPlE1xj5KtSYmir
wpapivgMCSTT38TGvXO8FFfcelwWzsPP6DpDiKXmpUvO2LVYFyZD3ga5eId5gMHNT4jMMtljCJW9
/vaV4m4rvTv/OL+VliMxfKywZJXwOUmr6Lu2C+e8pGPf4gIMXCssx2jYfxMvX8Bx5NiFm3eZr2dz
Xa+Hr9rrBsXw1/gBUpmalp1CB955+SxSxa5ptjImVFAqrgOl5t5otBrQfi17Zq+Pa979W97ywBnX
MlMwi8PgqkxOHUoVHFskw8Vyfm7e7z8HqzpDe95bKFZ+pgytXXp4i0lIFQja7+5yjWnIPfQ3A3sq
Tjj8R8NS++Yymb8K3UGGRdbo/dvlfbIVtpjg+61sD1+1q475wthjlIV+R47vXEFs7bLfX7yfL8aE
pBNNZ42ZQYFOdkxnCsKObUqTU5UlXjMpiQgNPen2e2l+igOWjNZF5Xzg5Ykl7tfHepLwGnjPdbz1
mhk6y0d07PCoQj4ckkdHLjMvh9+RrSuY8oOWk2xezp/HxLsmO7kYKjWp3W+cmuXzx2B0VEToT7yS
/IN7PVIyRWvxXw5yTpEk1qS12l8QZKCF1A0LkPhcXCMLRvHFbhUQkTI96OL+IJFwGEszVMp6ZSx6
C0UWNMzjYD6hi8COTGiCBimAW6AGPY8/xJDPQNZFM7w7ObQYBnThqUZWa3Sb8y124EeTZSvSQDvF
CH5L39wG47X8Q8dQkWUVh7Jet9HKMeBZKY3UXwLiHcAqCwNjANbVHHYNGlO47IIBw5qIAeZOiZCY
vvxITf3wHL3sF/ZyZ3ten1OPIbWTOW8iO54g1cvw9W6+X3LVcxLN9bygRNqQVRm5yxfuPGI06s8z
3v79f2C2RAhOpbtXRh0ms++2Li8yRjcVwRFQ5ECpNpyJgxlaNw8pOXpK0IcsrL4jBCcLxdnz0nzl
EHDC1W3zwZQqd0Ag5JBaoQcQjI0MwV4Ew5mDJP6CBLflu3nUIh0TaKHnl+ej9NjcsOZqe82nnMr0
EMATgjxCF8VnCP+pr4eSFPkv3mze4jfPDcPpeAcHSbkxZMZAHDR7eG1vox/UPo2FoJWuTqSw9w1G
IQ6mBYSwVDbAP7UNi2pIb4ER9ulkFmOy4s62pDCM+HMzm61L+ztZfUhe3rC/BR1tZSr9BIP26uai
HwLEzJmoOoQml3N1i/04yMZimI0fIM5ehw3oZzjzcis2QSyCz303XPjKr15h5nVa/591xCabvVmw
328HJP4zT6Gvlr4RqGt6azPplicIj7FM01tHF3WqCOuNrOUKniReJRwlTYeOpHtrr8h+Js7WdAIn
rtUuwKQNAkKpzEQRJ9jo70kFfkKETGEFR91I8cjsthE/rbgWiO7KVvAk7knv7YG5inlfghurOxzX
qT3l+kN+F9iyqBzWWhI9ixIzBHGnl16zHxm9ogYlVMqSkT5NknuL9Ycssv/ntLOCszzHeqFIVYLo
TnxWdW+FAxmveM8klc7UReBBxDodulWowfGOnFye9hCDVJ1WHqIXVvXigyTZ3WL8b1shWthBIj7e
H2L/ZtdSLUxXlVJbgwLnwH1arG+Api3Hr4BE8eVFBg1RksJwK7ql7OlwAXzqtCk2012uhNHGp6JX
Z1JLC1IgRh9Vy657UcXZNoYxDakBORCdLM6gsnUzXPAXTa9BAbeeqPYWExqEbnYJazDja8oEx0FW
LYWC8st+O+sxJzqf3P8x0xgkyrF4tQXZkvFQZRmZNZaLK+AcF/Z1Wpc5GBqOKFHBnP05MSQUJxnR
2qbE3zJ3A6AqukGn43ehalaoC/Js/fEotZ5LmDnMmeXaRqFXixHDq17TN3gls6wctdiiPKsH8z5Y
Tc6CpIct7+rqhDzjNEaEse+fKOGtcBdPLWGvbxVh1iJ1fceVZ1KsCmwG/g5DtLVGgA8UodYMSl4S
aja1qP1gq3x5LRU3qmuPJ4rTth4GFIHHlOeUtKRd0Bh6R09IVlChk7R/crY7Xmw+NIHjl1NQE2Qi
bsNp33xnzV3ErFRSwVM1eLGrEcJXptjlwKaJEZ3NzyGdY06uQsCorHroDs+nLmZCkzAGo1QNsGtt
+8oxoX96gnSVVfFYioyFfIr+XOC4fZKE5fH7HOeJbELcCdwTdw3B8z+7yNAZpgJIReQs2thnrCCP
yGiumLd5L01aOhVlAu/6+q+SZQqnKySyKr57mybXEr9gMYS+tbn5VF83aVoh9VgeksKB7Ilrcf0k
ugNsxcJ3VcfVGalvtgww8y/TBNGMsBixOGIBfohAZwkH0bEA7DYltLDRrGohmhigH/sBmTU0yu8v
UgZHJRuLD2vGm6o3R0aXCsNIkFdkZrdRzt1TnN8CZdLMkwZQrx3WjfQPSb1EW/Vnk8jxYSHMXdvt
7//RlSEgJHKgUyIxR6C3St5NymDwi/txtyjTwU6nfoIq4heUwwx4IpXe/cVAi80ddK2cbrk5zDfg
iioxThbuMBrXIgpC5apRo74dB9AvPAEeqKf4uMcZ0QHwLtg2CvS48D2VdHMcDbbp9LlFPYzxy0Ic
Rgrh2IVBXtbdYk5Flc39GLyiHxIhvpRag8GdO0JoRz4KzOlY5rdr6BY5qGPy/LdkWXDB6OnZ01iU
x6yNcxsXEtwNEiQSxgOC4mCglrthAUjtHvty760d8cHKIDmmFbIl79wGnZTvvLhv+2GcQVZqztLC
ylozOSM1f72xyJMVZTbJKo3wV9h0p7WNpL/SD0OhbIxVZfuWvA2lxRn9mJhzOurKCT2bJvdvihK4
1THA1rX5PaSj7xhGdGjcAH2+Ax3e1RMKFKlG4C9X+ZTreUtazr/CFjdIkMJQDD2S7KlgTsh0fWC3
cKuKzIOMb9vZFMX8NjnsioUHZjB3Ki2zd6I7GIUsWjgd3U/ENaSLg00fIMgYxLpOaU05EHWbeOyC
JbsIFODiahSL1R0njWeugylxVZ8vpGtYO2aBWVi+ZZndqedIFCeIDlVq2n9mFgecaVbFF9ZZ291G
IQJqTFpGYnhNWZmfwqsDFVTtiYaJGN7/L7M+cXq5pLO4uPykdCLvWHKK7Bx1cHtJO+J8NldpVSsU
Dk+4kiBK63DigdxvbhcPkCIrNRPpqo2hGriCdQLeQddx+oKgTC0SUxcz1TP93NIbRBMWAZ2FxGL7
HlE3Rtxo0y1tpwSoTpfAVMfdE1stV0Y40U6S8o1VI4giNqeHZwj/YWyLA1FncLKT2DIqz/QILjiH
Im0sOS8MbWHxX6cGuq4PNOzEzbvD5xyCv/0wexFceTQHwqZTumPDaQvXv8lkGikF702wOkb0Tc3Q
cq9CvV7BKD5Akk9SkIMHhBPIoX8Tliwrx2KQtKaqDc8HaTZ2McvTdZK9lea7zehQ2/BoycX29Gva
hdEhz9vmJo5M4cItJ+XKLA3F3USew5Nw6pfQaQZwE++9VsT39txfKBQGYEl2aEGSxMgffZOhrXHH
eyX/9lPVuTNeSMhG1JE/loqcfyxE/eDE8vt66Y2KWmL1bKeUC3j/81iCqqzLqpHU9V81LMhHP5cc
uySaEvxC9JwCN0sAP4eUfspRRjAH6uZ2+86T2Kbo76VvlHQMmOacDhs/9U7XLQkPlU19rfbMDqHX
9/uudT+UC5cnadn/LNsomp5IEsqHYQ+EsSKcq/DTldoOBlB3CX58dt3i/UAq/I0rF/Kj1QJrTxnx
x6cWcxxs1BcXGRwba4DamyJVzHJd4iUMQqsmI12Ew5GUqf1Jpis9XAL/WUEb0tCN7dHhtpXrt2yc
TWFJrMv5pZcxoeC/Dr0I7YDpKFFSOGIEmGLs56wVnBSyKWcM9QB5hVtjKJfx9JX2ccciacHNdqjH
xD6JXIcOzINbGtKL8mY2nT2TELMBDz6fdlruSJhvq+XZwkXnvYWzUOrO9Zrn+9hxFw+qPpg5N55h
G+TVwVFPfvhkiHu6X7/r+g1kZnpuSs2bDmoZvqnvXJrdpqAf2hqlqrIrm/YC7rYeosPunCvNzIzY
zdsf+shtFoC2zl1w7QjWSmpCR1diPuI04YDfcD0OsfwjvbYdy8+okYrswJDAjsYTMeWwiW4GvLa2
hp0YKvyfp0aCMiSMmhrEEGK41O24jo80PRfXf5/pahJn9RMuQuu1S/vrko/DJwgASCAAOh4lX4CI
rlC/45J3T46gf3UeMQZuh18QhbatqbP5FoTVWcTFiTk3FwtPshZrAKXCMprDmKOuM/tMTv/KRBNb
m2whgNNi0aqkLxgUTC5KCfI7/Q3O23zflHemtkkaNtKhGxWfuS0KTnnE6M3rZkN5S0kr0mpaEOBf
laKncHi3BjAAYjmhTaQDkGFAHBfkMByLvpopojgBvkKh7NNZcqciUM9D7l1K8HBJQgKlZhJvYoFR
nRrkrObL8hHuLKJBaiA34MnPtz+jr7kEAN0yHr9hsUjwpJNzqJ3WZ1GjMwqXFPPdP/lqri+aF+Qv
6TVCM4WBSIP28mtZaZtP/0tV3Kj13m5W2VG7C36rDHM043rNEL4VwnS8hulSkLgJammKNdaUcha9
244fN3vZdQyz+3m9cqGxPzmJ5HyxBguN+EkkeyUnk4MGTPY8nEYGL+D4Yoxu2zK0NEy0WJnqnEmF
tthrJCFKgBRBL4len6E71bLSphP8BIgeLOQPYN9sXyXbRTkOzCx8uVEEuWrGgKEFKchLlgSOdQZN
ScWC3saCKZLE75nyxjorcoTwJ0rShpE7QvznExH0sQZrf9BTsVfCfr4WZjqWtAHmP4+RsAE++sGN
gB84ywgpug7F1//m+y1SZ5dVOcAunR2mrcqD9ryW/82LcnXqhBazm0JLh6OupdyGCet3PWsXVXk/
7pl2KV8F52CuO8hw49Acl9MUdcsXnP6ALPwuHy5nMpGozeKKwaxU6bkb/VsokIT1nKSzkWTHvGlO
k3URnuDqZcfWLnKf8DNaZpgTARJv8lLyc+NQHqf2omX9kdyZ6kezAMAKUZNIPgTjbBc4Y9nmfYUj
kqXmcIaD4uoHCNu0wjKC7AY8EDDxkVtukkIQS3fW/UgY6tEFA1edR/2y8jqRHkREtsc+tpQ6af66
uJTkMOlhm02BeGR72/1nYB1dbCxbnTEqYeS4lz9KzKQSXJEvupvHu5hw/cZCabzqysNbiIqxRybk
EQ3anNDMAQPMv+nToeYzvTu17O9tjz1l4R69NT0L8khsZXg1JAHIsTNV++zhVNczWmxPbHKVMJHp
TDZ/NOvUVnpTpqIQF9k3AOQAuf+zAqki3K21I+h6gu9GDQnIQahLnFlTo8cs5rPGWoY5WAEC5StE
Z6xcPRUkWT5nh7rlLkRXFGurqCxgQo6dKNs+sfj5nPcuA5btSlz0TsV75WGfmwCZxSh9veh7gRe5
hN5eTYIAGy8arERxa5R5ut/wKij7fSlVQCrR8yjyjdN/m6ncUAr23Fa4TXWvFuBQAyhYDS+/fJ4L
8kHt3LrxwnHM+tX85mrCQBIyUqCoX4gw+Xw5aaHN2Sv3OHCq91RsY8WdQonwNuBqdtD9DP0SI4Br
VbtviBJK/vIX+mzOPSYGGGpPi+fTok9Jnav2seEbpmWNA0t15gFt8gbzyyifT+K5b/FUmJJUwYvS
xZoHX+HHeSj/cpcD5zJ90SamFkbMD2OkAWJc3r+zNRay3FAbQWNvLpM8uiSVqDpkDpaBLAZmoo8i
MtxGWBmM5BLomfnh6RZySQbxoRHlecWkjaz8kFw2xubQzSHmEdGYLHjjP95ZlWq2BafJe1p5YKHo
yZHRC1Bp5WEyhSjCS1TDst+Y3NR9jtWKJABa6LD9AQ1fkqc/Y9ffH1c19mM1fQtsHnGzwiE6tHAF
BI8vBGvmSpbP1+1i83HyxhECZoS8p+BtCFYurGYkaYRjYj56sHzPvmxUfl/PjoFJExJe+perpuyx
lupmQVPDHJjfMROIlTiTUhDn4TExO+KXOSmf5lzbhhel/hxjXNqelk31DAWtErfB3igkqhtmc0G0
UvOdZTFXMtfabRJMUjiRZMZfL4ActQkFI3gKW92NSnSJM/SBacyfphDjqEHmygbHSlPi4w+FrTtJ
9jogwUgUVvJVZMCSx7W6q2jYDcfdD8lvLa26qVgiN7CdY+7NkQ/xhCIAB/hCytXZ5o7saYj4xZsO
F89pOESaTdP+m+svpffMptT/hUwLsAWskNSFS3QCBEiqdAOkkmcnninsux17qO6Xm6hQs6BzXee8
i4PZWSdCSu6vT8FlorvUk2gFmVp8ZBQyCxEUcYLKYabaVb1iWzvihnwzsjViT5unszwwEEGxZL+7
IP6hYYiOdJQfE6VUx6kpVZMe6vBIpnzzUW0HEcKOOxQFXGwaKckpeQdyfoMp2bMynKaNBf66BXkG
VQYSzfQYEHxa/tGtmHvWzeaKM5AImSR6EerLwHyyjv9fP27Lh6EklXC9/7b0lsrCRzuB1PyycGbb
0GXNNy+TYdoGAjhenUIbLwjPwphf3YSXgfUp/+dCsy0XEjgYquKHelun3nclFN/pOjLrYnNa857x
l9lGFXz5GMmYyas8H99adMJtV3sWkdNPi20FDhw473VIL4wbGvHwP0ho1xWa7f17jyBuM/BaWWAh
laDfKi4ZcJRABheNkZ/NIKW6wdEinGgjaKqx9SnlAd7t1ZaBuCew6mwNggWTDLMe9vaMm8ZYk247
eXHk7j1/Kx77rapsteWLVmlDtEtUJKUqlniexhdHZFeIwi/dVZhHLdB0aiWdsBJNGn6c+XK84Eli
U5eTa/C7OOt0BBVj2qZrxWcLd9kIyHm1Of95iGU3JXqpfuySawwUk5puTR0PYzl6ojEOsg6Yf0/S
NOuyRmDBW8FYBxBEe23if0ZGrz/W9x+n3vMAiu6OIumCaRbIX/NCOPaIvDTNXV18de6WYMlozlkY
VALaZ4H+Rtck2FC3NSdmdiJc4BAQ/I+mrW+m7WDJqZJLah7RYYKU95HKkJhR8MTXrSqJN903mOgb
2RleCJmGAFbiv3CG8Q+PF/kbH6C+JzHnCQhbSG/jay0NmkjzgZLcirgsvaDmEtvwMjEAbr25IVXH
uB8wFzTtZX3gRE/eoGG2w/ypiY4DKuGgP4XC249zecugqYhuBVfAqRueMfYe4XyMHzmL2/1PDb24
mHJ2lsOv/6gmKioYffRWSJa6OrdCSxwh1dwSJI6q0G71flBlcVK/SeC0AggQxfJiBPnNuNMiCHAD
p8rACm2NCye2XtBNLXNTsWkvGwDRe32nzgFkFnpHz9a0rP6HVL6ua82f97FFmivDBsjPfOvfIPed
y4yjZ7yQVirPcPTO2k6Ngm0oodrz3pBmjaDTE6XgjnBTDoG6DFxdUx29k/u96tQUtiFM2ZxQB/dJ
PvLlEA5jgUg6W80PKmxlVxJ0XOzKt25gsjmhaI2kWiubULwmbeHugG5M5+VBRAFdlIaOzeMUZQvg
d+xzX8tWU4lz7e+k3o8b0FKqp5w7MPgQVPrtanakDLal0CqpqdfaGzXUS6rayDdZL+1sVclKLXZV
CHj7CdgvbOgJKzJTjP4dEI6o6FrwDXp3rxe0KmIr+uFZnBFaoOSyORaf2cI2zr54ZeKWhp45sF12
QbRokEeOGxVVHahCIPTWgUHbPwya3C25QGQZzPGynOahroNsrQH1O4gczTh8W96/nSc/L6MC0m9u
K2d9RmbSSVFeeR1j2OAzPVxj+0CnY7Qq3qAP7RsEQyJaG3t3PKS+S9DA+1R2jShioptAs4C569/2
o1OOA5cMlNe2qr8Z3hwAymhNef52KPTdgUQ99oARklFRWvkV07gPVglG3SgAxeeOs7dGeTSAYuJc
veS1xsBlYHYPtlWj2rY1w8A0MBuCSSVbYwBWvbXsKV8YFCCvlAarOfq5e1tT5WExXNq/xTnHk72F
8eaAKSATo0b3JZL8n2z2CYvZPtQXAHKThWT7eisasqt8b50vO4KF8G0vqNHQug25HY2z5oK6vyTQ
4xCBr0wf5v2xAS/8dfjGLV2g7xYkO5ndrseFmTgfGCs9T15Uj3/KYd9LmM8Jiupn6p2kEgHkwi5/
+0xpdrn4l6oCkGX+sMGRw9omn3eBRdjgKrFOg1CjesjptjekOLl9GPTZlab2CmCQQuFTSmvozyLS
G7AUhAQsEEM/dJuZzNUwXy5H0cmnKCPid7uHHOIN3GMtW6DpqEpPjxslqXZMfQWTawareV3iN5Cs
Y/asOeZK2RkgE1o4Csen76dwlghSFF5mw1GiDe4IX/EsvfMTad2Eq12NEAehIfuGz1CBRbikmop5
tXza12xQy29tGFmkpQlPd3BKW4BEWhZATD4QHQzasr1FHNEIErqkAPkEjYMGWyZv1Mg72cFo8IXR
Wx9UvReoTS9LrqXrAC6qzceUmkbTZc3hkEtJ4ycKyPDeV+HIVWRuwgikXDr1D7ywW2shyOEK5z7H
aICR8k5j5rP5emJJgyuXil1JkdPyqtIZ+BI+6eeyqEH/B+bkOE8h7T7W6/Terh4ks95WPS8VV10y
QcGQjViqRgLQFtwEubJDQGPw9LDsZbVJMX+xqkvuMEe3TFOhFQ1iy0v05eKFvXq6u3uqrwqTSvL+
wuyRdz2yQHMbE4265ZNe3jWPukKctpqyt88N6NCS1j1yqug0KQdWho3sHe3TKxCwPeUrV2tpK++Y
Fi7QBznwW/JutygO70qbRwLeVtdUtY/aZ9O3UagL9cXseWcR5d6SBTAKAHpE0Akk5Dfs331t6mIf
FGnkFqMBE8hSMYSwZ/AMdC/Eb01MwwvTZPCqQ72VF3JGJAypiEgF4hc/5Wp6RYTJWMVId0oRENeE
NQsZTbS1qqUdn7Kh5rPwmDPeVi/JQ85MhYtIakhAxaNpLeEe9Ubjh+o4N3Fonw7mA3zGL4Yx6M40
+UoMPp4e/dzbcSPwrGf5oUrqtuJJTAHo8GeHgn9tW5NBXw1mP4LHsfHOsNmXibknrHXVkqrXjN07
nRYbzCSnjTb6UfCpFQaIDyG8kUa7CtgTK84+RUQSmfF5437qGXlZeY7mnkTymO2Ud1j/eWkkeFmr
/80qkLDF9LpR0xkRSstbip4IVWRUOXcRIYw4komOw8clPB3A8Drq+sMs11lJXM3HqDskhl+qLxXK
vvguwZo7OyVHevVjSGN4a85bbs/sHvyE6S2+/7RaN3mbg6K+ssphd3WQj1JUa0XEWSlWw1sgkQ85
J+ilwEaFtwg9VlhoBIpeHaMH+0FPNe7iVY7Civms2X1vdU66Bi3icPpslD3tJn4vGRu/RYm6G6r6
U0u7QueFCZqe5iQRwKlIiy7BlWDQJTO/Bmk2iVOKT+sjLA1Qh9ZsW/3/7HpjPhkisE5kPJDbFauu
Eaix1W9+XDGcrMVJfXEzJxdsH36g/I7zoIQY/o6kd9jeGLUIih7PfAdcJbj7XcilDypltB7NUCrv
jw9fGW5QAGiqH33Twbis/ZSrFS3FA4d+/zWZXMU6e+RmaDw3FIV0ttjV7OTJV06vJMLdi0YRR4AU
ficrouTECgxKz0Jg7iD/9lU3r1kd7cdt9RgSFEPw5Gl/2+C6nUZG92UKc3UIgmM1nsnbghTnHmkP
hVOwBxPCwM1pEaLEl+ohvU4i2ecaYpF6kjPhTPrlKU6VV5uMvmuOMN91K9NviGcYXsrKsfMR6+AZ
cpe8qAJaobWkwpJzBD1wt2gh3Ga80tn7I50yTcBviJtfXVuM3T68JLXPv0wZQ9uR0DiCR797Ot+G
43gNjKJ4Mh+uzfxux8pMwnPoZ1lF37QReOkXQInJf6jJB7REyPGSU8x7vj6m5taqiEqyq61v8GYb
+OWvWEPRydIZHLQZmBRvmZP3nGoNqqoe7rvaS81qQW6Ewkczt25kWbLgyvqb+Lk24tyZs5PgdKch
RbMfR5YQH3TEHPcndNCq31akWdbmEibBLpqJBvI58ZlJcifQ8vspIHoOqs67fk08LpEpb0wqyFGA
Nfd4ySFpId+vs2B/K6O9rOdMv5u2NXixfqtE5/MefmlfiM74EZYi1WPcN2zIIVHOCfKAyje/3/Ob
jcgkR5hi1ONrcXUP3OkDHmTZyInIM3TOs+xXdg5tPOEY00lfsSIK9jONGU71Zean+yUECDVga+oN
jwrfcDqXQIRWpdcvpaNCHbrfuj8yg11LbL/TQA9J0Tf5auga9g+EVCSQBDl0Nj/IdOaWyNcpCVKk
MHGBqQai2tqMCRLOtBUc50Q+kHXk4naHDYVmfuxjJm3f3X+P6PhQ6KFD5K+NVRItzLDoHaDFXQyZ
CW8cFDmdFEthjrBYetiC5hURMqwWMRhYEip3/xgFxq303o0m1MV7tnOJ7EPwj0RNiuPXHpkhzdse
J7ASxj2EzqrklMNeHzCo9V9956lXE1y+8UTNfvARDVOUIxhHPmXK6TlnyJbjFAzjlN9IT377WmQL
3WhrGfg/SusR/bNwyY6Om8MDZFIqpamqJxVeoXLFq2NCYQXIXo58ThruEK5g7nMR/30D6kH0FXj7
zIyBdMp2iksTVS7rkeP1e3/P8qKHONzxQYyBDbu68i9yv3yYHgxRh/FxjDQwVOpKgFm9o1kbLory
na9Zy9aVfCeq9qY4Uvay+mLYZ5SZe1laQ7Xi3WjP9/ZbNK6swwevLCZ6OO/5qFHh2dVf0Pd/NUms
rXaUMa4PCiyLT9CAGoxs+T9XJyMjyuCvcgJl1DmYgf1f95G9WTtugxhD6dkMYv345rnG5qaocCOG
MhfUy+rj4JRUh20GmpFh8dWQmfOpzoRA6LcDjwEflVVHfu2xEAVY9fEoTgyEBpvDjfJSuNruqERn
uyhL8l8/ii2zLA/LxtPL7VUGMtqI0wqMN5GyeXa/x9OpQ/mPBI+NIpwoP2gVSzzIR5StGrVmmHl/
gE5M58234Bxl2yZH9gJTvek9FAOoRQtmPmu6m4j3TRAbPmfEgwDahKQ72dtaVh4pGy//NHkzbrFf
PZdecHcxGvrcbby/Hl/2YBJAQ8hPGa7ve0zLiKHkFjhwVVwtEiYL83X9qCM/6hIFZtE2XdGtp2lN
L9TZgb5vPHkeACkyYZGXTSnoleqyWezeStMLg/Rm2qZLsDAz5DRBeRN7ZELvBGJWys2/m+RW+YjS
9TygsW03ZFrMcqeyCcXS2H7/haVWIKPgm385vTuasylOR14R2iy3bB+m6/mO6IOJ3BFVBgtm34xw
M387fM4BMNCRXasOPd6d47OI0Rwk/ZMfMbwD/aSeIdH5RmMJIYB0av49blc4WUVxysumsV9cGaH7
g0vksVtHEQ+fCC3qr7L2Cr1aZ4+5wA9a/eW65CuRW01Ig7WFh42HrVqMwHLdbeQUlZRt6/CpXgEt
nDssgaX4YIhj6vHeF4r/3hKDMeirvIQN+rsuj8MvCbG9TCjiep+cBl1rr7eTNEAweSJkT2GnEDAQ
PJo4uTh4naChANS4KHABrgI8IaeqHzTT9H7D1sfa7SHqaT90r89ZzkKk5YQk4jgjELjzxFd25sBS
bACXZsqbfsfLFHDWihBA8P9yWlNe4arAWGv0dmXUS25QXbE2bQVbgD/BaK5+WtuxSPItlMMDLwJz
rYqGKbSFYGLxnj9XfdT7KE5QqOj4fdbnmS7f3J0X1lCpxrDuCxZLtzOUHhKytF2xZpJXgCOg2LTj
EFVl6atkpOUCFPckQ7YDZv9Hy9S+Jul2xMWApPnPZ8/qbEEjvO6WLAEuAb6BFbGw7KjoRe/M4MuC
haZj2L4srKuIclIWZL+r+tarH9dQXKPmYP5C2D7xx4gYAktEMfsO0AlhL9ubhUZu1UouZV8kPtAF
X52GuMrTN4YuO5x1vsPmm6vVE2ckd4DyJT+AZ11TjDqo3xQkqaUGp3MjOScGPHi2ne6KbNT4mrMl
LxbAbj8sc79RCykNhmHTx7LppwTX8c1xEp40HGIk27Y38vnxPa7dgnlpCwA8zJg33Smn3W4eoJys
eymOmpXjUDFa743mR1hWqRi0VjGsRnwis35Cufy/lFTPcIoOA5FoDdvEzUkSOMpZewRhQVj/8Kkz
heMQz9fvdxafE959jN7t8LgsP3A2t8bPzB/mgdT/RPEWNk7JtED/HkCLaCjBl4hMX91FAZgqzI2t
Ne4lIh+5YFuWaYq7+YbJphBY1Jmkky40qqzmCQDxwpDVEj7d84RGu7B3PafFmClTa8eWw4rvIKnH
RqJrfBMlUbRbp8xlpE07udpe8fRoujul40o48UvYJ8bYp5KuKp9p8BZ56PMluKbgK3mGsDO/jVWI
dg4DHwTCdzxy74Gi8BZp04M8BN5sht9ZW0fwBNGZmdZ09p2MqNaOIYAT0o9CJANSsrV77xw/Zcxd
0lZ4cgUuu9KvPoy3MwEj+7VnFpHjf0XJUzT3RMH+QN/aIutlhBjbF1xDDu/RGWOxRuN/SAH961tK
as6I1Pok76zpz9gy78q5j0iIc2u/rnJ578W0qHsjd+T8C3UrRbI/Oe23G6+u3rs7o9Hkx1zaIckd
PBX62UH2PFAgl7WbA0QaH1ocFGGZvlghzq5+Uw9LHOi1v8PjXS0Kl9A1AA9AqbdCXEiWXXzl+YNW
Ek8a7h9LPaEhHTOzttMaXPaIKyWHrFx+6fZEOOFRBfQy615PmIm2mU91TF2mIVFqRt++aeJ1E9bZ
VVH437bH7OpZ+0zwq2qe2Yd+lVde1S5zWGdh5DsDlPKhWezCwDNIQodF/K6495pXZKfiK5xucDVv
45XQotkDTpCxVLDsXaD12Nal4ZJX7tlu5u9FG+87tSZRwUcxec7/wa/6NdXBj1bPRkDqwkjSM0I9
8GPXfDx+VN+5+RQTBXAwIqOiq5fOGvxnCYrY00lRsoMy8mPLKFThl481DYpC+GJb99J0Oz7NjDEh
jYVdgXNRN9bXR1F+2nXO041zaoSc6nxHXkxEAkNwDVTUcRpBgMi3BA07ruuwTQ/wn2syL5bU+Tbs
sC1HhEKHQ5bYvxmq/+WAb+LHdeyIHhLTc7Pdupo2plSA7PcChpnRBPO/BUVwl+C1cAsNVgs4OG/l
YS754xjCUnE16Vaf9taSDGT/r2e2CXw+DvMU4t4cEHXg3ZW0fivbC60FkEOBFQS2UK9IXt9swHgT
f9JQOiM5mZA1h8uqn+CKVsi98qkZTP2105pyyvT6/X0wnVGfrTLD4EENTDsPu1J+qESlSOT/CN0S
QHzvXj/Q2s8Ojj7EIB6NKZNz6LQVB6tC6bClnBFIt6Px0ctweCJLiJRCuyqFa3qNmnjj3QwYuOu/
gMcJqORei0wFtEIq3lLVXJR41EpCdqkeF9nSwtuP6hY8YZ0zdLSa4jJySoqBwoqK3fdLgslbR0bR
l+l1PsdF7qyUncd4xKAvUd5qXL0B45o0lN2AdIbZ+d5C0H4OrhU6oQWXTGAiCjgEuFk1/o5bzhkC
IILQM9ybkhkNJF67OW5MO+zwszX2y/6HnGVyeNEkYVy/yWCdGaEO5MnRbsG7bhRdmvgh6czApXGW
QIcRTu6OCPT3aRlEc4P4+m/mIg11UQMZZHfdSwlhIOCOeBUK/LJOO4DcAIfO9ZhiMAzDAmxnJGvA
fzcaz9PkUwh2sO1bc4wqOIjSE7Oolqv+hnewdcwMNhYEFEJtIkWT/51LwtSHdy3saL25WWCM0fKf
Xb3d9SdQt9QlY09yn2xAaj4K9WpFSnqYKsS0MVb8gZsd6fxUtW7HNO/ntWJRq3pDkJ96jT4iWkdl
8wz0R5cv9y0LUhNzBtBTE/tLpBSHoONcckuEAj1oxz7q8xa+Dz926pNQ0meThphiM2nhvF7Kl86G
jgUD3qQ4dNOMDx1NcxMoe/uU2bivLPIRFSjVzj39+4mFGeJYrOEGlOFnK0Eu/Sl2HYWYKn2aRASF
KuMYljom6llTfWYe69+IFMfcie15E+CiFLssBYjVLXyfGYDELV076cCWyoLixGcF7ReWIS0Gm4F5
HCJPplT4VT5ssOEjQrernBcE+erNwfy1TVG2YJAAIKHOBLUKisk0hENLKHyj1s/uTVtVBdDqQxGb
YEU/8O3USdFIJLYDhuZKXcmf/rortQUxPePji87AtZAfKEixReYbRyht9dTKNctAOP5NPMgyDXw0
10il4c8oXEUM/sJScZqHPOj+2fMaXemRYTJTk2nU+5mEs4ImXN9SS0ZVgNsF1GWvA0GfKGP+GUGi
RHhJsJNdZFLf6l0LvbiZWVw4i1Zl8+xYw5t9NhI89/PnIk1RzNn3T9bsFQRAajOwyES7u7PERl4R
9liJbOhTe3ZyHaJiDI35EuUIZirNMvSkpJirX8EOWxuhkkzJTHIuD/FzmnTelBDCzI4frw97k/3Z
MzXFRJJiusBkLkC5zLjoBoHHGJMaF65nsttV3plr5TGlmT+dgTqE1KGln3oM6jm+Mx1OmXm1uxbA
3ZoIAyWGX7Vq17xOowgqZSdCM6zvf6CFK4jYAGM3zRlrPh3NtdaY7TLpqFsfb/Uy21e6JXToyIa1
UV+anv78VSUeUprmf/mDZpG8r0MpP67DfEBn5nUHFwIlN/JN5wgAMVGt9bWhOwaJsInoQ6WKPJf2
CrxtYKR1uRbHCsBtC1O0utE5HYKV3O4GZ5cGQOXrEkvnth8CraWMFlQ/GvP4jWPuOrztGKZkCcV7
I4mhTeC5/53V6VdivtCUPf52thlnj2yuTzk6Bp9xPvrV1uC5l0/eN3cg+DcXnoDXfUdqjeUUaoAZ
lHCoJ9jwjN52jwtkgshBrkcutYH95c6A7rpaMbVM+lVLxsJY3EzLZueb1QRJy6+0Ns/qFQtOMrW2
6zUEHTnJksh+VlTVOMQdIl9Ci+5iNhsHWKt5baRKeoWID2VhX+QHRA4FWEkujy6Qyv5FXbfRzk05
Lm3PXg8KavFJoSeFa0pYjDpVwx51AOFXwB/iVgg4ArU884mU30+UcHLSPkX10vHi4GuRBqBwzFNK
Bwgql33UednolI9PF5hczACAw0ExUBxMOQc4XpjqL/iyz1SMVE1kdX6Qsy8kKhUoAqiWkwz33rkB
MDNtMS5+W7+wAIOsCf00vVRafYOFxBRqfdwtU7mvIiJFZjLExdBkZQEl4E2hUm4yR1UAH6yaa/Ht
u/Td25N0jS+KP/h2IUuuFCZAJDg1/rMEIHwH0n4PqDUDfzpiwl5r8sGRzZkoX6vYmbWgp6KASVed
+4vAsyJ5L1wvAfcQBQE6L7uqAI0sH3r+XOZQRLnH01LwfzqbWpQW1KFMTDjSpvFtAv5csGsTgNqn
gsOfOYwJvhM/6VXoIEI82F1sswDyZT7/MK43ri7mRqdyr1/CJH/t9zLPDGPtqxf5tPEleFlp64Zk
benjt2xOxoNz+csn+qilkthZenRU5MNaZZr1D/ZS05bAX/w71uckhxDXbrQL6DHFk395JYf/NkkL
pEGQ6CJSvqKotoLFNpY94C30j+fdutd2kHseSAyQwUnF3yP49SfbylA6guULUrRtQS1wiyJzdcBf
4ZyENkJgnXsnphb8Tq+t6Uk0Q02UY71gQpchyUjKzceo1QE6To5zuz2Cl5PTFXq8OSndBW8WxKsw
jwnuIMH4pzu09Mu805tFeXe6xVXsKiCR+57QvaGW+MHxo6qUnlQfW7dfwe8K3kE/5ObwR33wCSxp
DV53VFbKvo6A+MKO6OPAXIhAQzxhBOfdlH/CMSX06e4B2EtR22cwtB4y+9WohAL1zefsK/nD8vg8
e4JdDy/wzF4iuZ/bIy/corRT5E34slAyU5A8BNUcetsqu+y6sCuRP+GIjKye7nEP0HcgJboo3+0F
fQjX7D/e0pM608FuO0t8bmxUqjhfJAUV544a13SVltP33qLDKtJGtVlCXeJ0zcXilW6dKP76piIS
Q/yfTVikPl6nheKx+g6w5q+xHtuBxh6fJAO1HTm4ctzLnbpbNRvD9bGK+UgZcg6vtJ2N8XZWZ6ZU
a/KEKYMFhBh+f8pxl5lQeO342Tydjqh/xbHQYqKgY+shJ2bz24wb7Pw9Y2ICs11iAWOOFc9dA5sR
oQLh49ZuygKqn/y99L3/wMkLV0dfWTX5qb4M3rQhtHCYig0KDgODbjkXb66eOKeruHUuHZhcJupV
eoNVTctgtm/1OySgEJy463TKwJ3mjfZH1AgLBQFQ3T/wwaoX9DTRVBgv5CRXi3PrBPKuu3Ui7obF
daaCIW3MCRZGAaTkJGzZilLos20ciYdKKhNFmYpCL58TUhpdjKw9ktX/rV6XVX0z1DAeB/WcYUVe
e5SeVwZaZvoOzitthlOmPillaZ9D+sPQR3v4UY6XHlrDDioyq1elXX0HKJBXnJfDyo5X7mEcYAjI
4TUrX9hM598j+zah2U6TAEu/OvM3y0pfbmpsdUt1efNZ6Xn9ltfMcShGN0mbbXjh92rE6eW17ZK1
zYd5gE05pBibG8YgTTsJaZAOnD74pWT2O5q7yMKRrjO2rI6eaeH4A8v/MPiqhPZE2qHzwmT4jCtK
olbauFQTQDSl8EJZcdFuShNjXy+00y7Lj7oNjwXNBrR6esqmP8xtW7Pog0tAZNuOcYxZCf0DL0p/
WubgrEYIDobXR3w69y4rL/Q7d5IQMa2H+shZJyyHKFvx+VNVN3LQQdQpf5UF7YpLOGeIU08ApYCC
rCon1CU705I/Ez0uTwrsALzhsj5sWh3QiGD5iHChlSaLfIqM1u3bXBbuvAFe/RbzUxHwtbJlQGK/
7ymXnUe6DYiffpowWtNaVEBmadktB3DTPQDV68NpLpcCn81vuhOBQRri1mhw0V2irExOQm+J8hA8
cBGnhgyEdogOkUQ+Lsi+Q5o5tAM1TZZ6aJpy0TH1GdwATBwC2wcCHoCpaWXw+4yR7qVzFyZKDRj3
O0MirafS6r9DpSz1745qVozpp7RN1doqShHaxssm5Sn0y1NN0EXEAlLR2R1FdWvmGDC4eBZJ4VAh
jHGqPFOloNSlpPB6KPzXdUq4CCtoR1FN1AmZs/yflUkMmIDGV7MmJPvygokHh1nJu82blyNg2Gp3
Sm3eD9F/QNG9irYDsCQWtwrkgBXDjmCUqNberlT8EEIXT7UVwl5Mfcej/V6/me0C7LXLvHoXlPcj
zg6hYjH0/94cD1V4FNK4la68nIY2F3APgeC1MGXU+NVDhDUq9ojyM5ZmnfiVZr6OviMgB6TKuvgi
QenPItFRa374h0BgvUXxfdi3LWXGIO8jo/bJyxL0B8OiLaW3PgpR+WWQ4OazgRATZ5aL2eGcM4bu
KZizyyDH4iqDS64gCtjhM2CIP6rRlwLQX7uYU6m883lo4Qvaba+3mBab6oBdghx8WcfjkpEFDSxz
Za1CU7sysii9lWwwz1JNg4WfJfep0ckY1iivLNUZubHFKaBohRAlojs41nPmQjCnKsGmcdOXIk9A
3PcYlWzwyq5NbwynBZCItJM+enL9HcK0DbAzWC3riBD1fPzQoJSraSa8i0gbH40jtbjZxseB43fd
S2johVq0hNR/cqbZf/VuCK4xthdQwJojkgRDkuk4eFDy6YUoYKK9YMmTCANDEdvS3naFZxvPDvts
lvuZr3CJRPlDxa5X8rC7gKWNCOdabhEqB+KwfdGu/GUpHY4yKplLP3XJ6Cy10EFLwbRglMNcsYNA
vTpSgbfq/aJjqW2QXdF1AAoqlECXL5gLsLomrI7fsIsgsC450SasEA4zQTiYfi5J1UGMVyQwQB6R
wFoVY/zoCPJEfzGRBR+FyaMUWY1d/sWjnh2nInyvzf+EYRaAGxtEJVG1rjKXBElpIiXXzyZMiVtJ
YNTdu+RcKZMHowdZ3ACW7LvbqPghkNhwTOUML6vciP80J613XHGGTp3aDgJuzs40ck+Ui3l9tvPi
ASStjtSakz0NPZbpcLe/pPDLIvmUO6B4/bK2hLNNJfv7JR/nI7i9YOCYqJMTwGdR/yNJ35IEbhpr
hm6tCvb9VfvG6Be+FQJHuY5GSFYoIEjrP9kaiwzNUTNquIWUUbRxw/cX8ANCMZ8XWa2LQ/N0LdOc
QwQxWmbY6q/r4WmE8ERneSTAx+O4rQazAkB/MGgojCAwDGrRSsXdlPdEAnW/R0XEfcOsWd6bIojs
H1HvgrV1K4L1mGnwSukKAhrWS00tjmyZauoMTDEm+SCWFQdWLGTNkGUWnSL8CgJmusMg7OSoHaas
m/ONYlrnUmqOMkJ1GOagPRVqjLqaDVfssRps7MrJ967ljAdaY0lIZR0rocu7njhUWEipqBbkEl0J
PSsEDTv5GRSXVZ3PSW42iWUogYwfoIthVbm8J1kHEyGnN97+UuAtoyIFS9YxS1uw12sgI3WuQoso
u6pC6N/981ZQkZPFh9HyUWaV+50Zut2Eal1ifXudysuO6XvHeGaU5AB9Z+3MjlucsRzi7OkmeESW
lxfD1kWZioeO8rAkp1W2IYu5JmdURctyMKP0Cz48PByw2qrYfYIGO0dLSgTLeQtrjIMiOEAPlIiu
2rWWa3AAavq+D54huyPCgrh82cmBO3RRPwXRdkb33u4pDtAV6S3zuL9Dx/cW46E3t2d7zCVSSxBe
0w+e2IHpogYXM5xC/ADqFUha/Eb8fStfmgxCEFpzsiAH5syT7yTYCJl5glnk7KQivEcQzkB5Xl5D
KW+7528lXaoUcww7gLiEdQjrTmkrAEzhAxKu1zIZFccyY2VIMHN+y4CG6DxJ+k3K+qjeCpc4Kggu
j9tKm4EB8IYwYpYol3BqI0itHPLnune1s+x1krzbwThEJYxA0SFaDVU2r8lAP/Rk01oIMAmlY410
oqTQHvsH3MZ54w6wquXzsVfmg6RoxJcYzCYMuxrUxr0AcvC4bnl04djTEBVp+Gvi4qKhG4QgPjNJ
avSopC0twPePUphfkH/VAxppjeUbOtzFvpbCe4bc11YHqTHZF77bTIPrH/CP5eZkzMUzVoFA3Eg3
MGeaX0AYZy7CZziqXObJ4rj8oEkWcdvZ2YS90aJcaE+bj/jec2Yd6TAiTrWjgkJ641Eh8vzMrU/H
baet5S+CVyrx0HdUcgUW6Oo98KvWIrch5p7KXw0ir4meE5PyOOAVB3ffvqXz/jsNZ/5rWMrzaMIX
IEhH4z9+ybb4s6XyrsTT3qOAggncHRBudjJ41ibaRmkX/BcSD+6JOx+PodFatzaM7UyyhXVrvxEK
9tVwXA2QRk7MeAW5uil8SSt8TssD3CX9KlKRyMrFlq3iRzEscx5qixBjXjvR2OB/xkMRYdTPtMh7
OMO2PJa6C8JLEtPRSWJDj6dOf1b5ksqnLgs2DgHs8GZXPYFJQGb9c2rnon8WTZl9MwG0O6PcAFWQ
jlE2Pwr/bQ/706PWUREqQRnavZzMlj3Ui05ls4sB4hf7thw8M4LEvdhF+ZwSHBVlJqqj8+AvPJuS
+AXIMuqQuRCqAIX2DHkJ3902QY3wFV2OuO3C1zSnl+0oQIjRz8aVrGjEExoWivN6CzJVogxsdEgv
mqDJZT0T4oVZ0BDH095JZaFjUJr3aGkC3xyLvLxLHh1Nnd0W+qsV2tZvwNxUVIJAkNfOpkjLguPr
MyVpjEMWsW4e+kWada1QfEVHUCK7VvUL5chGx7yl3YxtPJvKPQf+ABIVXW+DwXMTyJ5P5ZtF6EP/
1PN7hcP+7XUeGKRj+Ubapu1N7K2C2ubOZAYWRCQ8uK/5jByylYpYXwwemQPhT2UiKGN5TJFo3nrb
i1S0v5kb0Or10vjw6iCD6q7yASRomedg86vi46VoSEZsPRje4+Oz7irE09qlEj8ke/cl/nisF/ll
MR5W13vax3ZQZigNpWmN/1D+I07hU37ujYfps89r5qn1OkGwM5Ass7+rw/fGv3E4zUsfvPwYpRA8
TTnT9c1H8EFEyZ8VtvhOdr6z59rZB4pzJnQiiopXBCV2SQHgvi07UYy1llM/E+bLMxjCIuOxSCyq
M1XVeWp2hlJrBm8EZkndXHsXlL1KOwinXhWm1/Yug6RU7nVaySUlsa5izXoK7toRXPWFahRo8/4K
BnD2/Mox5BKQIyZUONL6EcQEK1ro6zO7uMvs55jNpTKWwpkddzladjHoFPlRUtiP3sSdokO2hICO
8eKSDsb7rtNC3+o1iz3KQSWRS83BpA13JAVIvEicIbNERvCk9CBekkiZZ7EVXAcaoR3MAuKYhwnf
PH2eFIDa1hMiffeU3NJOYbSgaSUCfI6LcKpJGpa/5WKzMh6YRvohBJl272THw1x1cYOcWGr2ZDj4
XMbXPHbEjVRpJfyz6N/H62BhmxmBNhquIlkLc8TIuow6rz+xaaLiOoETQPuEXrM1xElioQ/5MG/D
LYCpjcAXGCQrEDQCd6ZxyQMkQGadtMujaq7Q9gPpo0l2acXsGFhzPT3fbNluknGOtfnBX0/imw+e
CKda2+RbZK/p+l6vjWmxn4HjfJZFkrgKFsC5N5JsSVx4TzXweij/dV02f3eTkyqwaiu0g2NdIWUR
XWOSn7DqS3/hByW6vRtXBaY/Lxe6cVspa0dOEr4yzb06zZRHsApv75ijF++rcgvEig0w9FqdaDI5
T3lSau4WriUpkxn8UkM1cLG+8SD6OCZJI66ljLD0uTkPuE/s488qmdjqZ3yZuaSXaMp5VPvnTs8+
vJC9jaPpHkw+6Kl3M6kACxjTyA2o+6EwIsVFX/pstYz3u9PUVeUBrrLDD16acRCP6fkIxI5DB7/F
7H6S8GvIUbhwhKfeBMid3gAgazc3eOR/ZTG5coLZPogzV6jWZpestdchld+01lmCg/2onXTgQUEn
yp2a9EJso6Sggz87w9sDQlXGwQKceV37HrGGlGgzN55MxWd3MS+HP0HLo81izSBoESmpHWY5D0V1
89IaMyiNeN05Or3aLZv9O8+jJPTj7i2gwQskCx8f1IQOD9Mv0gUkaHMIhD4maXkrvUu3Ou6IO3cf
k0I3gWW+RK18Z9+cfDQdsZ7BPiGoH1WtuBMCwqxtbaTYddhI2oWlz1mbr7r/eTy+UhhRbqrj0WF+
DlnkKwQiWZ9awiAt9E79bVQ7tA8h77PzGXf1rEjxq6elabcypwrIJ+VWC7xY8jrSbMiJcnySb+UM
nCZo9wsJpx/kar+TE+0fT+QPqctp1t55lNBtn61HFAvkVZCKSmMHjUs3gGaCaYfy1TImWqUfsGRd
IcHvIq8Bbx5XnivXOfifS1XpAkWkH8E7TuMBhzJ0Wd2wEEiwhRZgY9wdTEsecZ1txhIkHJKRDluq
xCbbmtdErqtSojqnksgecNGmF378200niZ8FNwefDdE0UQvliFxvIm1yto2NiJ7TX8+MPIc+jFyP
AWNi2x0W0lGFOGDLrNHE2HNBNgjTL9HkMEcl/XbSVPiDKkjx3RVvVnwBjmbK1JGdWO0EGigPPyVL
oyqufL9sOrhU1SSR3G0ioLW3DmLgnZ3NriLmPVEPZ/QJKn7aJ6GkPjCQwDTkDDUTdykkuJhf+WlX
cutApqu6LPoiknPeLPVIio3mrYDi1wnpme/oeSWUJeJuF3Y4DuUlryeerT17qxfU83oSb+1WpVa0
B3wO+qiQjitCxngVTT+TgNxGXzS3i3zcq3s1xrpsiwyxF7MzTroNrEbdmrYuW29wJMonb6FEtUs4
15lmD+6gnh1NgCINP93o7EmgqEg5+o13UHaXgamqdwuRjD+YDcV/sqEYo4+ZIFbdFPAnApTVIk3Q
X5S6psgv6YP4uLWY+iuWhXov8EbhmRabMM+JeKAUYfj3HzXUY4P+Bd7YMZwjpbhFdqpcScnO/SvA
8KfguYbCAFXZKNjcrM7Mc7iGopN70TvtnqnfK+IDzT72VnufaVe2JEZ1xeNKfaFes9XOVOy2Sbf4
T8scGVEgW3QbYWv7s17Flln/9cmvZvOVBwDFX7rzDmsX+Qtwh4BNyY7TCVql5dgi5IOKGxX0Dmpg
z45wdK0onnRsUtjt13KmzpTfTMOp8QSEp5nzaZC6EB5JNlSzSpKNLvZ6vP29/uSLkkwvtpDnSa1w
iUCYlfN85abCS9dpveHKdt/YLp1fITthRa+M4/YWxq7+G9bU57WvSbFWwTVQ4o/MoUkXq1FBvii0
7WbAd+4bzXuPtDmowwVgXxMQsZtsfwy21BinyrLe4PHHM775uq6wOCcjYAqkmM44LDt4U70hLzha
ET0msumVBqyXgiRhCujn0duK7OJUwn/e1FBh6AQDUNdwSWi8p9tW/db+3cVKXR8SN0Mu9vAnys9k
GyQrnKvTGfCaPao7asWNbEjbLxiTyADKgCfGnM0dYPgQ2TNRxpS+mFMYzK0U6488xEw0psUT8das
6HshVBNCm2H5C0G2MMdAI5nBTHFDiH7dYAf6rLQVfd8GqGtGjSRFb3kXuGcRGmxC/Adh9P23kQGS
plRuv0PiNeDfklqq0iLvt0qzXRytk1iJy9tN3ZN+Ty49SsCu4QBzmK5x9dxIuVtnrS9gNeMDQrFD
OtXR/1YJ/QNlQa4n1apYpmKszAoYzyx8Fh+jekpXgI3NkLPN0OA8/jcSSuzaONNt87hZxHkRDXTL
UWoqaen6BbpoHesQfsjC+5hGy79li+5VJ324pfJXBA37RvmQxy70HZFVHwuB4DbxktNbuKLyB+dD
lVpoMoZgPOHrBAoAqYGa0MbTGbr7SVqhoXAnk0eBBbU79zc0z3K9RiDm3cbVyrs9RTwrg4RcAAdD
qlkImgSA0tHkOdsVznkOp/RIkUZHXtQ/j6bpRXTuUmaTmQTt3ss6ijqDKAUaIGlGF2AX34NpqMAS
utUAiZIvTpuE/aD+LYaFz5fOEoIvUJ2hqSoY8u5wuCE8WdTHi4XEg0JDpD3UCi6e5qRaRGKxIsaD
YbKzKBkTOLKLxnPmCHCMI6mpMZWkuWJBopI8VKqPQw10EeZ8vbtKeu+T2Vw3NXvr/b/ISlOhhztY
06x4yoGr2GZJdQnKKJalTiy6MtSKUUugStHhsLE2JdHxqyKQfS6Sg3aoCA61mEeZ6R7ZvIPSQGHV
1ogbJmL2N5aACmwPZY/1UQFDsiSsgnxPOdAvgWhCWGf/F3dataaG7mkv+mq3SoV8HSneFBM/WilS
f23uaDMGNgzUmtcUGupjqzcmUaivk3w0lWv788ygapgYMYPFQIwvB0d+6UtvuKtpQjGpx5nkfGcP
jpdP4ld/VKXZaLsbiFWxUvDjfwdd5A+UiqocWzjeRq17gsGaoAFbArxdDi1sulrw7T+8jXIhRBdi
mr5XsK5wx6006LGxjCdLxuk0MlyNPLzqBQPCgCM1SEtmv4AFXeb7eZrDn+gBqEPPT6D1ce3VjAj6
zlao/5cRnMom4bP1gyrmRxxKrWXXjEho8ywEd70wXHyyFGU87vR7ONQK9nrd8LczcPEALryy+lOS
gus1Kpnnx8KY3kF6EkOKRzArez6VYd7s7IB5IwkVHKQKtJ7/HNOU3O7zLGf1+ZLDpNyV9JbSM/tZ
xarbXiLPQH4xOppUO8LBBk4Sp/FCJ3xVxQTNE3VbjNHrK38/wQ+8K3saODAQO+eoHMr67HGhloQy
jqe0cxBnV3MlVuiTdBdRpfJysy003rTmThxiolDiHSoY4y3rInLElxWPZYxEX3VtxHzgbiJ56FSX
vstdw1Xsd239Od+iWp21hk47VxC8SnLDZHkppyh9URwH0JOBgBjbNEjwSteBy+VRNTCi8ji8S276
uZpUeHPfgthm1VKK0ARWIJug4Ojrzh63SDOhUDrchKNvhCGCmSb0eh6Jn+x0cVGJy+KoZ6dp3wXW
tlUZe2gJtSBqjuN9kozxcCTPVsDJJPp8ciEr/NyGeQ5sQ/IFimKZ/D4YLU73luybBpvF4PzoBXuX
My/HdK0MtVoHrCMPFXmZ4WXu9pmpRlWHOANh61NWgN/eIJLnSqBBIfJ+jUvMCvfZrf4vzDE5q2I8
VtfcJWObENkn9BaS6iTDD+apto2O3MFPN40by7NChaltuWwzUvOlHAe3cmPPdIfl6bt4ngYOVbEN
mGNnK6OV6a6BvVz+XcdiKHNXzN5fwOEcD+6jPQxw+JrchbMIhTIyhP5AnZCTTV4xj08qqBvG8dVS
3AkZuEmqAyyJhZxzSX4RVT6Iyn+tgBVn5aS9rNXKs4gSDpJCeO1RawWmWMMK5F3zIHpfv6cXExCd
PSOri5XSyHE/ZLC+Eh0OF7QD38SV2rv+FFcY3dzPIoa3Kz/WflqShhhHDdjLVJ4AQdvg2dfzXibI
Y9h50oI4bI6a8/zBcknbSCsJnB0CvB5iFILxQ4b0ffoGOzJDfg9Bd1Z9nPJ0nf0xKTRowX+m3prA
ft8iXMGcakLMwdQkTwxgwuB8qOVJm4LCxi0WGx49RnAhH1RT13rvJE4/hX9nHO47+MX40CnlXndr
bN71k19CM+GhHqXMvrepeQeKT5FiYGPHOKDjoOaPECqLBKfDRtLGiTPgSNyhDXjjIyOqmEZ+yOBg
6XcN3I9jYBGBHdzWLoNlbFs5sNsEH35ONQ+mJ79mTaYCbpLu/9XLqEB+0YOQQiBREDn/GIpZS6HO
GCAkmREAkxfD9HORPC1Ccd44ZY3TXStPIPFUNu7QuwXjtemyCIxsr3jmmojYnDYbWd0R3Yc448O1
j1JAsNX/W6DNI8Adih4nLswrqfe3khBDiSQeqOok54qOW92k6rX7UlPqZ/3UrcoznYj70MZD29/3
491srAXI4ViIze7H1qrXEIPIS+wXLlimIF1x3NeFn+64Ngwxe6vcUEIWYdzqW72DU4UxQ/iiollf
TAz5uI+AvPcfC2cvQWpMGpRurv48qC9C9HOp7iM6cp2ezX+ZxNtzMF59HjS6/ThHe0Hz7rORjVsD
PSSHg/LeAEWQtzLMGm93h/cU/cAwxeBnoBNMj1bo9MfF/e6a2qIZ2c50T81eco4dLfz+3sO4gtIo
R+5AI/sBqoNpSS805XVrf2BEylwldq+wcw2VDdoDnURqpx09Lrle/0bEWt25CkcMA3B5wdDbZXNU
TWtuXZux+YirZv7VgS+lr25zzE4+lZS61apJGD5KWr8MozI4H7FVe2BI1OQP6IxMJWd0lhWiOu2V
wOORXUOuF6brhRall6qvU9blEEadeMUlY+2q2ITUv2qBfZukcEQmijIYFzfXqkLhD7iuSjyhgB5X
0XAzGECegEXHQCBFtjLbjEpakF5yKuu3kas/gtyLwoEgcIsYv1CcNgMRp6qptvOxef9NKM3Sn4wb
kgu6Ddu7bN5q+NMQ6EIct8juBQuNZ7l6b8/p6FhwjRHJPh3be+d420pfctYt71DYHTNKoyI+EKv8
worBTmdlsjgNC0dTvKFFgpzW+JZtXDA5oYFGJv+Y8Ke6iVbcZ8eiX7NOKUv+ZJInjQeU7DFOfs4T
OOu3PmiiEmpcfbXcb8fnN73wB5Q53BJIOsfKV+ubwh3x45cBcwic/IFYgJAxsAC3nLVQzvN3x7PF
Mth7XvL4q1DwjqfPaid+y8V+uU9E7Crd5ANdxfw72KOWBaHKU037kvbsyXxN1x3sIcCo8me/yiZ1
lXRr7c13+AeVqIeaX69XA+Y6tnqpeUxiD1Dn5yGJti8+BWwbCOQfg7Y1vudAHbyS/2yH3gsWmrsE
ZSvTc9K5T9ud5HZ3bTW8gWVjP/NHQIne/3xrPuP94zBifhgoIEtKX4aFt8W5oWcBiGl42R2bsSB9
0TWpQYmWJCGwnT8g9FgAoYWKX1Ppl+//VQlJmDCXdQpEV1ASU/zwVu5TkBgFpzdkJj/zHRVcNWDN
1q66EgLGTIIW8pCVTEkQLii6p2uItT9rm/cnPKguhW6M6Q76/twJoA/dJtTyviWFru6n+iNZXMWA
80zTsBkZ1OpB9TGOWDgACJI8QIzhSdvYCJ6dYJdIAXgjVQ0JZQvXVs3X0zVgrplO+PMDW2IsY3b6
JMzbE5bB9YZAx/dFHYXPDq5gkEavWT628WN5yhgadbh0iuABKwQLgPAe4RdUZwbRB1ywuFzjPVa+
cV4YVXHC+6bvYFmIW04hGoqJ4U82o9RWamZOxERy401sXGOB3wMmVO4BUA9HufZTf6+ZfB/5CRUu
qTcb8tFJ4AjauiGh5L1UjguvzcMN2sdmWswmovEQuctfZiVd1u6hJyCgRI3mz9SE9Nyez7tO/L79
0jThZObq0P/T5RaOEPe0AsTJwjK7Uo66QBDn2jLwXiD+0RL+UmcxtJI5RzDOH4vxqzHZZkfIcrOV
qUvqcKMhN7Ioh8n3tgvX5pvAFEd+wY/VJHFESP0yrjsOx9/TR47sw6rjT0ocKjPj8P+/iWNsglfb
1rTkhR+EUbYDf21AtY4iFhWp7yDxBM9MS0+fONmREt9ZMYQZiiHsoe7I6PNoEjBm9zPq/tM53wnm
wmZRplyW1ERVuVozX2BG6Nz/CEd9TAPrBasM3EDY7++gcGnAi6E1h4x6CJUCzaFARHQ//2WKrRwb
lBhH434BOxR53o9y/Aec4OFjmjgXCOGZPw+pJJBqpvIZDEViW0ErLzsFZTWqDREOoGBgtnBk+5kV
dZOxgGKyeS+TcWvdS0Q5SdrjD0kVOT06l6cPJH1SHPD6ZBPrTcPc5Qh2dIWNvQ993jl04ogqnicr
E4cVOKlNsVRcxgb5gx8VAsKHCSAkf0rHzTgnurMZS04R7B4NRlhpS5/vI1GUH3uAqUqRzHntae5g
v+PAjdS1ln5aPaiebzH8R22lrBZLwoBMOgK5YW7X+kPvCvCR799A984p1ciCGYUJltQ1lT+101SU
KuvMuvCb/PEozRh7rkmnCRPXOT4sewMYzecyOP6c+837OagkMxfNuPqrkWelvjAL1wcMbK44GTwk
8HtXkSo1NC8OaOfCuOjT1h4wMp6+be1pK22cvIgmhCm2Pl1ReZy26ZZASF2EXAWHrjvyMrc8s7k1
lTW53sKAARhsz6bxD+DCgLZrwNdkkxpKL8kRLPMaFPdky3NAWxZZ6qEaW474l3E6ujG9cqu4vsdn
4t3VOp3vDQnXwXL2D5qCRl7FFJUOtIyhr2DdESnl/5M3rTXTa3HKzEPWhKBmF/JC7Q45oKLR+YSL
+1TX5GSzGL5+eub8BJbmBtiBKJrOlaWONieZp70ZDkZhoJJCvVJDtD3WtfIuiCKS+DU+eABbpfJP
DaL1XfRj9ICohrBdvaUJtQSaq6vVTTAA5SG0NIwm+rXX9E8CjsCREiPKCKR5+tzvDQntyk2/d9vM
41s8ukU/gekAhvRZ71x6Z+WUs4nkdr461coc7A5zWawqI8xVEK+m7bffeRZTZgHQSKy42u68d2Bg
eRe4KZ7HWU2S/Ha2jZwCKDApTLXVnMEZJx89KrSBHWiPeAtP7up1elx+Na1s2u7B19i9T0uroGSY
ZyNrG6+E3+P8R+9DZKE31sCX0TkwlNMR6CJ+PHglL6MYQBYFPOwHZWdgFKcW30ZgCA73xbjR2o+D
4Wc8sRnGWA4w4k+lRELM7ALPcYlyonshoOxsoFcsltP0+IBq5kQ8rQylIoKII+SNe3ZBG8nnHc4a
H2Ljec3WGH09GpQ19d6LNnd88g+jYTQTY15xPEV0bc1gx/GHcjuJicpSY1t8uuiPbBijWwI8t63x
/sbgdl0yuYcE5Cw88aAdSzaR9HoIi0InA9xLkHGPTJhVR9nAG3a7VfpM/PCmycSZzk1/s3MbbCmY
1mGsz+YallsOEiseYMHHCz2rXWt3q5FHToezw+vOb2A5+N8x4S+AFFBsGMRIbFC3bDOsD/ANebhT
pIDqqcSzZulon/1qpfkiXtWlhTOHGKvb+3TtMPPCjHjGkUVxDkLOjwiTCIdsFReFDWSsMA8Fbx56
+1gwLdey4ZJY3wSY5GGJMxXd/DF+SJE8kPisr+7IdEFaF3VR/2GY15akJlircgdoM9jZl8TSlHT4
jsnejWU/NIrjxq2VO907T2vVWMTcaI8Zc1yd46srlq5MKnfXvAS/1xAl6Au7/GP6H1Gk1KPVMv4U
3H/GjdiYrPx0G5QVyUg4odGF2q8Lkn/qGbvm633vdVPDHJzWxggINaW37S7t9aIlMDbA5r2gfyFp
89IDqOj71JcjkzWEQTcHalloxuwVFkISrGd8iL0DgytxhKqmjR7H6UmZsi1R1Wun58NACZaRTMrk
kiQNDCDvhkigWYfHXND96U8dyaKxd/7+VcXlP/8N/yRe/Hu5DIjgY43VlLJDKS6Mm/R5mlXtACri
32lzCqHMbNCdgtCSoO57vcJ7bpwhk1c2tXG9FieqGptUHZC1vsyOt5C/DoB3BKdLBPzRQW3x7+bA
V6jMQmOyq4dqFKTgpvoDxalHCsHYVbBvgUmPGvY9nTXHXVKHr4RAhrfg5RByO2Jp4x/X55oFHHsq
ku7KVZGm5Cp/a7K1l84VzJMibULaP5WwNlH1hK+iXj2zSxTPlweA4GQr68Se4v+IP3XktMp+LTkr
sKQhkE6uk+4AxULC/zwkDTaGZ1SsopB+ivqltIZqrifbbNIztFZ96GeP2BPkByKSGu2ORXgohFjh
cup9pZhaQBY/03srcLiumqFDlXjXS/kESm6Se1JWVXt2Tn3eZsqUpY/cYl9NtABV2Kyjriw+RXEE
OOtq0sPPWg8WuvqUzQ7HIHrs6SOjVlKW0s5Ywr/dIoNlzEEtOHrvBPDrTtQBz84J8Pr3EDBW9EMN
LzBymqI6GJ/T6HqX8NVEdGA0BIq31Yjg1nZxlLIDloWFZpWfWuaEgWzRYUMeq9UPM5umg0N5elG5
L7YufWVoj27pzV+77dPbfak0TSnjIDkgZVFX2IcS7K0s1cKs8j2NFLgK59eHUa36/Bx5o/MZQOvw
j+oS5HXbmEVd+5Hc35O2uko4IF5ZZtbmm8Dz7oryEnbwoDOTZz2Pdn68pwS0RnItSH0FONi2eD8U
4+FfIkeYUbSWZOZRT6k1EmbbkzZKUkbPDboCHMcuDfLbfgYpbJyZPOjpNvEF+uCEse42rEEtnvXD
UAVA+fU0upNbd+rJNKlZ0P3T3oTySVKHImff8al1Q8dRw7JqY+R/vIOSdR09b3JrO6OgOIM+ijsm
nTZq2YvcxGnA3byqIDqhOYeuMQUp03cd7npivH8RfF8KznZumbE73uaGLovqgwd85qTAysrmPPOm
R1vsvpV38bexJqnFwhozRM1dGf0S9LZTGcZJ46JhCDjSCD2p+Lwz4EACglh9QwOo+2Lg4dsz1jsN
2I+oLydmm8BD+crZJRwLg52EHBVtrMH4tv/IGqENYgxac8iiDIlbFcsPN8KHjLwvZAkrMxBCscD3
bzzP6yDCTJQCMJcwrOkTW/wJykfCDuH+4ENazYVE/Om24iMTjMDbmLJu/hQhVQwBR8Y16l4jNhO4
9pAPhcpVyBoyqlvtqk0lZfL9r2XE2NrsyTKFRxtyYW7eZdP6LxFowIPk3vBc5aC5ByELEn8A2y+z
BmaPNNhM5wQ80KQJNwqg+M3eR3ygFNYvLo3ijTZxN0oXlQi9/O5r6ULPeafUaTP10zua0ntOA7vN
098l8mR1rH/ocWJG7/T/in1VxoNqB+FPlZKdmFNahfUZ7h4TgOns6XlHqOT6QUWpVJtfRqR7bfJ3
Gea6KUQ7B5IYCJ+qHAMUYD2rJlG2UIu5cD9+WTF12Btfa8D+BfzznShF+IVLedB7wvx1PU+dwnB4
GrFHQQhU3dMudvPE5gflrL3jzA01qnuzITLIzpWa8vK9RZj/5cpsxQQeFyqq8DjZbv929WAuBLI3
jDSJL3BFCyPykcnPSsA6u9e2BggMDz36tv40m446o0Le5zes2PSoHGi7DEb1vXzE6ZHcHKYC3Rxy
aUbo8rzTDedE5lckJDYjXfnlMe8P1QGy3fYq4z/jvX/mvHsVAYQTNQg5T/WWpuEit4Av90IcvQHm
9IaM6F9nc5nvg4c8yPMfTes7SvYMC+AxdmPwCtUiBGuZSUXSIo/dqvWQPKs+m8Uj2px02z0G95eh
S1Yywh3GmsiwgWAITxaoT7hk5TzEeSqlqLJ4+K2LDc/4mTvXWmSn5ZVvf5ZC65CY/erJc8seCaUM
67xW8ltK+Zq3GVDu+QSszjoqKRZpqKMkkZ26Zw2lDALs0BPcmI2JyaUNIfOqyl+tX1XEyz6W8cbK
r4uDa39z7IxQMl2glW2t34ofcZBbytMjDojeTIBqnUU6MiHp9P/vBCAAIjj3qKUzR5l644lK4vtV
OWYkyaMTxOvIXy8ghEQkp+CX2S0q+lCACbl92eIaTwCrgXDN0sXyJdR//2DDnI4cTsuYyoYd23/L
tpY75mF7fDl7HxpPgcfOYIEsjBVl9hxuqW2z0Q5murn+XSPXTCBhvB4nAjmi7dle/3/fxPOxf8MH
T7U+DThoxdBdB0D48AKz15kC+oC06bI9Ppq4zKfEcXFNmEm9rg4E7ie+o/WHVrAb5d4R1r2P51h3
hl9O6pbgTBvtGG1sWM4p0AXGdrTUFqzKNmI+ANj6oS5qmHBCj9U8no7FxP9w/w2k/KITrwkuihx5
LSREn1C115E8HrM+G16/iSoM1fPLP3XYwblZiLo132VwG40T6JAR/UT6R+HDydgvK481ZjOhvFvL
oj1rjC5N8KpktitVrK6vcxUOwWIU2adz9/Gvw23Zy1+URng/SwaGVIsjKSGRHndm4s/TjsIB2fN/
/VvKL1Nz7RCu7n+H9V45FkSs+00sW07PmqoTrE+xeoMQLoErB0Jh1jp8hmFaMRD+1oEK657J2Sr8
79NIkCh5CML6giJ2gyDxBZd8gdmEnbZgOY831/3q705D22vDTHqYzzWH7AEQvo22QbVDb1SASQAW
tIRtxRSRvJ7XGIzCDJlwaDRBssAA0e3agKQ4E+XszBbp6tT8Z7fxZTxRi0H5T0NyCbEs1s1OxpZ0
dQEsrYBRMhBbMLHssmHPt3q0djC/M9aQWFS5r8eQL2y5sXyzFvhAq+y/3YZ46IwadgDQn245BsK8
3WyDo3lJoHoU3NxL06RDd03vxZJbO9Dl3rp2M+m2C1YMcPzWiuLvmuWvmiEpv8bSZPXpRqfJ8oj+
xpF27QfdTNP4Glk+Y1uVxZq0oCH2sQLsxrUEeg60yU94/Go2FQnM3Y4dGdLACWe31f7lWzuuS41i
Vsv4uMbq3hyUkzExm9kpHdYwDZupMfXPU0R5yh4VSYOK7VckeFphtNWJBb1fePeFFRdCXW5ZZcTc
ZyeR8lN997XqwhSuYb0qnEmS/+MaK2FLVq9eP6TcZArhAcdVL30Sbg8PfrzT9MUDnWeu5kIxLoXM
AkpwExINF2zeGNDb1hEAtkmPIi2asC5/U0fDKVH9NuwTbtoGQoIr6ujD6xwqhw59dy/6lUDAkrBJ
/ae4ldDaAFUt5YOX6WOSrbJtEjhkAp9ZWktGZoysay3sy/Winu3zYNzVEb1Q9lfxfxHbuk2feJiR
SRbKAPmp1jouBcUhR1k2ojVBiOxH4Daqe18atLIVsi49wVmKYNcR/O5zavWCvvoeEuIWgxEv0W4w
B0RwjFupPNDllCJgUeTiCkJ7PdjvXt1jELBwLZWiFGJ40s5/TmhsXMFJxq/SO/lG/vwZ72SCb/qI
nkKA29E6rLBKqyPaSxIpBUeBFkE2dG16Z6gl58JGpzhkp0QzBmXDsJmAaDQv2m9/V/haH6vF0xH9
8dxe5rE/VQYh5NwnqZZwyXs3SgFYY94udl5YdbjxZ1yc7b4+UYLyNYbr5ePxa7lq5bdZ526T2gJq
g49cONC7ZViFNGf3ybo9M/Ao/bTL75i7Fspnflpx+hMG74bj5JcZy/rwLREIOapSl9Z7twNU7sXt
cOK2n3hKNtpET5uCTBwI1sMkHGK/PTqlduAVN4Ah3h4gnJH/KuP6ZpAKVTBDk9TNlDWLNx0oUv2g
7EGubm2unycihb0t3fF/Bn0sOn2HHz89qgMlPPPIC9pLqmoAikfBE2RRnBkKefbyoqkWKUsB/H4T
Q3OUWIIuSqMjOUBD7RK+mhuYeX2S8HdndUzrZnMgfK52zBzoz89KVflNcbYa37y7TPdE9sMJNHUW
+Xnkc0Fn9Dm39ir2JfHJtik8ipJFs9tHsD5Ks1s/3L3q9rjFmhhRaSdSD3wQjeyWghPg9LqQ8zpD
9jg+uUYFFfYCN23EpPTwoc2AJM8Ojho/DvZ0QtqGZ7CAVR05GsBT4g6dVttoIUkzJu7AnEvWuemk
4S/QH7J1Wtl9kHO4E/OqZUhx+e8jLLRTGdQwvf8txhTeFgDs0eJ/Wm7izTm3QyeHqqKExCu5dmUt
5pZ7PDFLAet/wENQIhbP09OukRrzA7h+qC2fVwqJ31BwOoUMq49HUQ0KSHAdE9/m/EX2VD9kVWe4
OSY0uQsoBYzOVyWfp0OCsxXse4KgpweX0IjLuuHBrF0XgIOf/YstQu7ufvqFaT7hnWlwEmQzFVVq
3kqk+mfQb5A3wnloF0tWYYExeX0m+5ZUrmDvKYN80CkbT3XEqsx0HoTKbu8eeVQt5WiIyh07GWPm
XppgDGAz6AR7Rc9c/riWISfexDrncxSVq2Hzag/cOxBPpeAd74miVzDB3sht/kDvhJRg0FMxTURA
6IgFa3cEOUgk0i+CN108l6pPGP5lQ3R0BbQZ1gL28l2FXhF9lGVfOr6ES2IssOJCzp8wQqWWdmtF
FX24rP/GYb+3P3cDsqsjEMr1iDHeBSMSYQHjUaU7EtTdAHLZrNIQ5acjM1tiBfGeHSNcYRkxYcih
OPCIbi0sxtl7KRqOyDcKPkfiUSAE7OrVTL4hxONb2kazkFqYydeZmAJHyxYzJP5IFsm8J2fBiFmD
wQQMdIaakYC91w7AbyRqKiUDajufO9ZWgm7cu2lFRLRavWU3dSuEI9uVm9gomtSCAAUluQCYIjVY
JF74TGV4hvgfUnTKW/1FLYw9MNYqaXlNObo99YoV3hKVGHNw88lchl/BBh5j16RWjuK92TvxUyZm
pdkRigTAtT3cSdQYVo3nX5KFDrxGJkMqZXpl35fKKFFwFcBdmFsxRt1I/fqLREAVulA+P4P1YZG8
208JzRFx3osb9yyF+dfhBD15lyL/20xdbUCjT7kdxJ5RlMwmyA3dpZth+aN/aAq0+OSn79bOBVlC
gIaCovcwzrDiOdF1aAjAj2S25n1usASF68YREwV+M9NNdNrMlsXrZugTdO8sH5XpMAhwzYYOuRgc
E9BfD0tXqcP9X0AZxZQY8Pp3GZdUYmtllPY8v0lEh1kTXpZEU4WVdVl6U2DB5EdRUQ7TWVJjlDPU
S6glQRzuMQLDEf1AF2Z/dI0IZkiazILA6TE9H44XZW3lSA6Znjd/0c5wlCS3wWCCoWqT4XB5VWb7
56+f23qdf4/T1uz7xn5FP1PybN+Y/jDsJHd2EoYs4cdmkYyGhoiACWQB3VQgqREc0NiV+zyj5DgE
oCIeiVf4oltGH/0J7pcaxKRehpfQ5Kxd3Im6BDqLyDYpekS5BXw3I2/NNDZQLarAJZuw2mwvVcQP
qUxzyRIOy8n9HllEUVqf3C7TvtFgXOltxdeT8dNdCj0o/aYK4u+wzzRJ8NeD79hZGSTTJZkA9am0
LqsK/ZSdqPiXOHNVW/Mjc54yCspROManIrS7pKn54dnUEr2IxPPt7VpfmJYJcfEc+cNa36Oq42o2
wraH5fGKtQji4v0kQjNXORuqxPl/BN7LbiE6Co8SkarQ8OK2MxLMB6On0j2sPXL/c/+v660MF8a9
ovGwQ0bmTLy+PQU2dlRBTa5fRHr2RZWi3rOn+n4ousczL3F5rmKMyyV3Rbwitq/XrpkXbbXFdc4g
DoMRNpkA1CnJ4jjRBj/YCKpp3sDLRLgkzw12D5id/acFC7NjUNfSf3yePImjbRtvhz9PTfFZKVnS
lYFqOuSNKPUqVZEi24QIiVuaafgRlgxG4CgnzgRZZMNT5/l1rUhZAuIjNA/DNJRgWCwxWh2iFX77
dO/j8i+FrGkIEjc1W8AdQ9bUGHmAmd7w8Lk7SUO6DAXUMB+zEzwzhlCAeBEoiEj5VKiuuiVjpGLH
n2UvSECP6X5nCmgnnMQAGuhoQ1/bdj3cFezwkftEJp3RsqyHXBCTM628UOZTYmXmuBdD5oM+XRA7
M6+xFGiDdr4aFmP9GzO0azoQpxuTFNgkzqk3iQr6rmiSFkdst77s1DkReHyRlK1r4FCc8T8ePHaW
XAg1qxN8+4xZu3HKS/Da0PCX6NfdJKbFQIuICDXKhujZS1d/WLa4gGTjErhY5vmGhDsRRb8D+0yg
UV25n+cEJU1X3jPxbaTOXwoR3e3xWIPbYRKuzGN+lGQq//TduSVK/HWnCbD2lWeHaYz1qgOQ+Peu
dkRr320jjs1yyzEOxCmsAIVKTWw/5eI0jg6tDJPm8dXfJsksiKQV2LnEXtQL1lhDnS/KhJMkBI7C
aRL7D3QgSmYe+jyvsl07cLQVUHBjbczil1H75SJZbFoZXVYCJOKuFBTOc2PP/7UZqxH5eJp5Pfxf
PJKdnumi6rXK7ch9cmvaIT1J4lqj3tnZ3v0eVmXx2Se+ktex+t1SkNclw9vu4+G0IhPWFCk3WH84
jzzWwelwhHF9rgXNb1hd1IMoD5Qcatte8i2/YfFC8SXDgACwp9za0EwOo99rqmrYDof+kpbsNb7A
tD38svhas7kV6nUDIEUtk3ELlca3My23uVUssQiavT4IaRZmm7EMmlMiuNEdSv51HOubDYezMy3q
3xVbYr6QsYEzjVNeGy5jwcU8h6Z8GF6dQAh3EuA9ai/L3dANb2DDnR3Q6A9Fk/esdGOIySyJoOEH
jpjQcAqoxwrS2zSesoFIYLryccsLjfG15MvKTr4/Pnw9Ks5U9UsahhAYHZQOZ06BCKt+kYnq7YO3
+96ajB2vCQ9//AsAHpXXSWXvw3o8vj2aGjGxXkmErpFK2nc83BeDEKpbZblVDh+IgcsS9DUPI9S5
aYB/HFCEvvVVbWIhH8IYSiK90crkfIGrsH2z/1e20tNjiYra76dgvNwQonSGRZskY05A9be7YAjp
t1xxPoqchhE/NFL2aWIKqC2yxBJl7ILVBHaMhaXecIB4RgIy4F2/HKJTJBSvWJVuoiXfjy72iRAQ
27JBLMs2/F6PLD/a0NcdyIV9IzorssMUvlT6mEguL45FMQ/fVeZKXmgJtIqRfMbBiqdQWpRGPrbt
eou1cCDt8AoalrwAHtF3+cJdMNAjnUcYzgGGBli08v78oasXAnBk00n2YqRolXG7SdvPdeaNRWFO
7SIQKdnUCmXt+Y//xGukSKmdTP7SUSs6ay0EPMv6RfYxC7UxNkow+ZuhuLxOZ/E10hfL1KfKdN/O
CZc11Z2iLTAiVac9EWRw8YGJLNHTkn6pRhqgIyrtoxAB2zcQYeZNnecBkUKn2BDVd+v9kMZQMYha
jHvUwXOmV7qA7x6I3WIin1FyQzpiE3UAMfxsPAClNmIq3Vxcn9dSdHh6umFI2XWRA9FPcJuo1agI
iUvjodlB5gt63C5WrV6hH4GANe/R5reOC1+8idpF952yIgVIqgyTSUqFyB/zGKc+jJtLiHYjhMRa
q54uCF44uQqnPvT9Hb1YyHejrtNWmRQJdqQ30q/Snslb+8h53cH2R/zpYVXvrA00unjtsDFSyFep
AGxjISVbi6RL5CEn5sL0sKSkHtoMaiGuKPitkpJGRS6vyiJaDU7OSuxpN+xFlf8ERSof2fHrUoRk
cYcthN4Q3/9Luegnp8OnJ2BxtJwBytnx3NhclbqERqGO7zyl8s9cVQbfX1D3/XgH/JpDAvEoyz10
BmiZsl9MeEhlbmZfbyTX5UHkiqnU5ZB6CSkkTRxRbQyYwq8Oj7mJKfkWiXP9JyC/n4ugZf0eJa4w
TB66fpI9+tzuP1c8HLZ4/tcQBVYi8R8E4DPVvRw/PfQdJMjAwOmxAW+rOth1bx1TGRnCiuQBKa4W
SQqBc8WGAu4cvP4ldP5NC9IeYcWXfcTcxmJF1oLP0vda8MKdr/yuash6Y4KWV1zJz1wioaRUdKnm
1ULq07IOznGBanQFY76nNk9q/pgg/PaHPf0Tv31VuZxC/Q9FFEXhPpHZQoc9VrCuVwfH0x1IOZQX
adkg2FI3HApRHlcWwVYCOY61Cpqj2VSgkqhaxV9ZK+TNXtR1G/T3SAve9xfn9eTIUIX8DrTsLfVN
KJUYduh4TnJY9JiB9E/agDwAsfEnnbkYSiBin63KFDRvUlyblNEMENP68bL6qv2jrPQ/38cEQJqr
9LUuFqjaFnoFqckMAd7GkME3iILpt6umC9BH6RsluPOK5xUzBlZ4TbqjFwxJq8n7Vmyi27i6cEUd
TO5+LoCYyoVkAbgh5qkAh5pnjw36QulkARt/o887V+3KhGatw1X118y+1ahm7yNlX1fLNwyIlKh8
+rclO3DhnuG4ldk4oltyfoexqURfwJ6jLxhjvHNn53X87oMQuO17ycx5KiuOW6d4MYdNKGej5WJ3
deDpZfTKLHBCREOscnktjY+S6vgWHHV18X3r8+YUktIujmlIeVhNBQi3MefH0qLqGTKLqyX66WLW
oZ2QmUL0KvvSTUQnbEFxPwP24vIDtXZxICRtfiah7K2Kwloi/i2Y8pLSmDN66hNpJMMOswnHYH5J
QFtStMoeglc9Hhe9bdL0+T4OCuxGuVy96nlrBe4lNPfMObhKr4Jsb4bSdzKnqji0sr4i/IGb8KtK
iHHOw3TrJCuFyxR+GrCLab8AUP866lSpJkV5/MqFvhNrIUnrsbY5dNkUStZx+b00flsB1kv/wNqZ
0pBwN0t2UvPshQ6GZf45xLgXzMpiPb5WE2xGDILx/0MDL+vKlYq4jl9/YmNdEm6jVpu7TUfStwtx
Lnsp/AKYdOl2iMMAq+wi/+CORBY3pW9JIYuAZiztwHsLyQoLbHdG5IQYsyXcPJBlcxD8+nNzXiem
hM6Wo10jqTEuKarP4eBn4OzSa//uBcaN7AousvOtWFIdGfUL9I1PtDL9owlM+AX3wEJe3GOybOFV
BSvvWviBg8jR5XWvXPztYW8J+fmrjCXSDHMgvi7VvonG1Y9fHDLXllQVq18pwG7dJNrKM7P7OUOb
eOi2tcTxesBEiGWpeu7jJuvqj/JxV6dEeeTiKfSehfa7iBIiPWLL0Vvr62QE+8TuzKUy6AT5JkEg
SPB1z9ru4i/YhNnNtWv4ztZ41DPn8pnSSQ5srjWQOw/l4GlvqEelunGMVQmrdc7FF2K6mX4w7ffU
zZeOUsZ1Dasmkhu06enCMPALcbeAESYJ/ojdXqHyYm7WhkwMuxyk0mY/bJ4A5O+HVnjxMdBnFhl1
QxnW+9PnPQsOIeOAROtQ+Gd/gW7u4Gv5oorkhiV7d07J8m48wCG9NfMpFQ43wAGP1FRZZjoBPiwk
XB4S57z26pwxWfoT7y8kfBo20o0FfCpKa/8qWbiFm8YREowkwbUyz3uHQj2iDhWTMkS29uk+p6lH
+UUe0Psyn5nLoQkz+GiAZJYEN3TZPEN+yxzR8BapXZhduJLAFP8nJIgtxogrGZVrYVYiqTUc4SXl
yKSJE00zHGmm18NwL/X0hWApv0Lc+oo+u6I2YjLV3p9phgTZvHxjPJ+sfLVOiRLlROTFWEcudAtF
2/YbaNl4M+rBD6Txd9QYPlNzWVtUFgUzeH9AL35KR52zC7+L5kQ/nzYceJQ26J6JWXCoBsGUhA56
O5SA77uzp5Uy3TLWyOPdqC7jztijyYECHlZWRbp5Tm+Oyodvfm8HiHpgI+VBklNpT02PPPTSjq+c
U+++o1BLTvsyvR/oBp5nD402WvXr24K1V+qxaQ0nyOgfyZMeTeGvsjztemSEo4dN+VBycCecUSDH
+rSIDwRPftYxQKhLuyNtJR+ZfRqXjIJpvmRN5DHH5k4Slmkrfvui7Tb80+90nZrDUNYqyhv6+Phm
93XG2DazHHsyZb3wqUOpdZnI7LTZLfijN6flzzuZTVxUI+L+20E4RIf5i+1qANUs5zSBqpWZqTjk
nGMiNJ4BZWlrGjO7Xc5ZOoa52Yjk0FqvgcI5v1+VKjH8G/gyvAzeWikTxQ2LAVMosHWQobdFU09L
2lLz2hB9nNflGl5Up+B6BaICksH52dFm6SeAijcOrVubs4T38Q4EaTZKiGw2ULxvOdqob515gp14
G6LHJsuqETUekXyP2+GuOx498PI3sGAU014XJ2euLBeedBCjN0XhnoCP+erOHB/1I1F+TSYc9GeC
gmiMXIKaKI3+TU5wpHgAHQaZtMCSjcLnxkZ+3gcjhl1quPW61NfNj+O/KN51pUVihERUaOgbHtY7
7+Y90fZ3c0kclrFx52smnsMYiVuQsWjI2OhCe9LldEkhPqpCkS/F63Gpau8fykKcYnI62ftM/0g9
dxXhZsbdeCEV5Bfk2wosRQp4gGUI59ti9US0MP4blild4LpWJWLHI7K5WXVAs5+ORZYzMF6bR12S
0weNiEwoTagzsJsZ4UFbQrCsPdkVW3v2cbU+Eavl8dFiZvKQI9Pss3jLlbqQOO1gyMfGhtGzvILE
K7JuEcURuE5HQprLX69OskDd4IZ61sw35fdksXXrmNObcNhuHvCbI1dNqjcFNbYfIg00qgDBwdiQ
Rb5VeHZAtOp4Cj61exn9eGwYiOjiS9RK7j0ro7pldJItzdNGahvRBYuj+54u2IyOqvEsPgTL6Enx
26NoZSTeqlqob4v+tcn7qboQDkkWjY+OqLXSc6hKXLfFyLWyXUMtSSIHef6vpQWi8fA7rMLgcScb
Qgj1FGLPaH0VZdTk4cS5fZQwgvWWLXGeU3cuaOBWQftd3PkB7jaArS207XHpwXXn+8uGo2YFgMlf
UUBC4RL1uAcHsEiUii7XbhupYxk78vAeEKIMoT3/MJqtUQnFhbOMzhTENXbP9rp0MweSFZJM2Gue
5jqnCsOQuKrTs/I4bfa3wVG9m/RPK3CHAPxsfhm32xIvvH/D3lG71rIc6Br0+WTr+ixnjcHCLbfa
pnst45/ytqO/OqqUPD+4T8avP00+ugCgJmYcrXxLHvXar4/eEKvrV1uXsr2T2vkvKbwngxrpXvy4
mNQdzK6U6elf4tXS/YHcGW9ZrtRcFlmISkCJThokhErMBk9//u46W/jWPLolmJEMRL1+VQ8P9nyH
u4UZbTTdtCDdMYbvluPjlznR8balyc9RpEVn5ef9lIzFisIcdO0h9tsLscY5zSwRVJZUvC8yfnQU
FekE/tFTM0nuopqBmQHsRwz1xv1qTxtFGy3VwFdYj7td+SteyrUVmvJ0CAcqOgYmwcfGK2YbR6nW
/m/DLU1aWVsUsy0RvkF5vzSQMz4E5FjiGUTijoyt+rw1Xdl+HOlqM//nW9iBLj7xmVoGVuIQn5z5
NXWMH2O3Afb187AGt3yYw3CLZUAMrTBGGFHvKOo4IoZ5A3PWxKt4W7LA9osXgsIs26v+EdmzMsVw
UUhUCNGPJ5l8tdpuxWdC3O3VmCW29S3DMShiyKGAwhnlUtO0J7OJIsdtu7xz49XTOq0SIbfX3esv
gxWydiimK+pu086xiLBPOPo5Kd8g4kmK3tdgo8VQrAS7ou3RmVyq3dG4mkDp/5p6wHYEpw5/8K1v
kSkNxVy+EYdJI7XjnGwJ80vlBFP3it6pEWRThfvdskPejqRj4lIa0iyfs7SylV5b/Q89TJUhGI2+
1BoKcaiwxTHBcIdFQPac96L12xc634bbQp10OYkRV9xikBNGKWTtlgtQDlintj4eFz/+W3Moasi2
KDl8FHurUC7iaSeLvdyqqzFUGe0yrxffkj0b9XPCY6z2OryjjfvuU47xhLUjVpzB0ld0CefUSgrJ
Ork4mUQ7p8POL2HMqBNhJJZsZ2LpFAAatiB6XYsOS77o3PDz7yX2k125bfROfImD3d+HkeFmwkt9
LJN4fFT7LaaG5KRJpqwi0K2d0spf4o54gDZ49F3nnOUAVCkzEreQgSjQXWHeGUQC3ta5hiR4MnjZ
GS/PlENGoO7nq7eiZA90D6b/u8dHfBaPviWpdf1Bb3JdlvSaBUICROCv1rGmcE1kbmwo6HSRx7l/
ZLxNdsCItC7BSNR8XW5zVC08t3ZarvSKIhnyc6NMzNXUhZrIpYVxYGuw2n33J61UCi3dn6AZWu6E
wIsS0Wcd13ucwVxZrG19D54HawLOYcYQmLlGIBglNcA74y/hCpJpSRTm2Iy14YiqoO98ooEBs3MR
fI8rLx6BAYtTpQ3SLiH20kfnMfmNZe0dfH+3Koev+52JQ45eY1Ku6nTYY6OFF4XNbOhyLZqk4BzW
GCkmqJ38g6m8+E2/jPHQH4FbR8S5FWex2LHRL6C8uH0nHPfxb8Mt6/npNDXsHI4C6/SJlcc2gwRc
7Ti5UjYpv4NkBnVuIJ1dNyt/lqaF+Y4xJBPB2EeZFrnnmgZXsBiYuKEOJpbXsUDbZZ6vM3dHX/tg
ya/wmr0VG/kjivkDIi7ElzmXNaGyaa6iIx6zWpoocgVqHFXoVMw+EpKoHX7AMEYqWv59mS5TpaxW
XxGn6yAd9iAEwEWWY4TXcv9tz4zYTWVshj0Qjq/abd+DoAAd5065LfEtggNaB+pJc7Vpho6GAyuf
JqWwoKImNgHvsNFGsPKShrBgWIvvEWR3fbl94VaxxHMCqGrpeGvZfA+AzzD26f0My8AuW4HVA98H
6ZGI7PmbFACNTOR7ghX5gyRQggjv+GKRo4j/ccujzLK+CjtiLv81Fn5ZJtT7e2icxVcTCXKT43+5
4Guo9WbNKIM7+cum4huW0piJ1g4yZogq74JazK46CggKZ5hZo3KDzBo7kNNt+ojbLFwWY3AUpeIu
cLIm2g+wX7FNqn7S7O6ItEpj1ESZRWTBnUDViYVxWWnc4b2HiMy+YQtIDFRTE/jcADCMsgW2CVJ6
plckn5R02jIvqoVkXEpta/fco0faXVUxMwhRFDVmjztOrKn060Ycq5N744wT2lRKbAvgYOhO2Mlf
NP9Szt3AekTkeZoP9vXrIP82Y4SWcib4ThQNihY9WevzYEAcr5S/AgwduPV0Hb3pVJG6c6Jlpunn
6XYmyuyIhfLUAyC++4CvJ76dpwdvb+kG0ouUr638lNCxRD1hCM6+5D8DaoejY+AxrkxOxfRGm9Bc
cqRf5SzOH4FyEz4xknjuVN//NVQhiYc4jgr5Ar79CTa5ybPZsxcSn/7PqyYJPuIN8A6xcJaTGtsX
jbzs68kp9RcV9E1Y/ZXMVQ2f/R9lqERF/5ACVR9DkZIvu4mvXgfFTguUY8uSBTg48FgezStJnrEq
VGJhRTZlCxmIvgr7s25oNDNpLJd5r/vLqaj1whOm/JLTsQ41XQNRzc7CIdZ2grdxDhxyTGd2UySn
1+icIdbuAG3ZkDJ13jhfuEw3V4keXxCnP0fCSMrfvF86cS2/SIq9gGm6TIbNFt4diEzQC1rA2ec2
DubQNl9VoFKj7VH2+e7hNy8/9AtUdoV6kAEWECM7uyolP1MZPwxZx0GHoD4YHDxFGSKACPqQDWmr
sJxyj0PPJk5Iddk1RAAdm+1ag4rxEueAPodg6jApoDWQ8Rba0ie6xgpxs20N8oRTSK2pTc+PnZHE
Di2rpDloWYGvp4hg9IxQsRWsvZaRgcLnqowgZCd09E8x+xmetztfr1skXAVHuOvnfTtJrfPQy2xs
Z8X7mO9nw+aniPWPJYLdPYlbqJnwyDv5H0jWY0iPpv7kI/fHJgXjRrVVtPhmfI8zyY1fL0ZTnmuZ
A2CHDhJ14p02m6O920irQEAks4f8x18yfzQb9GiBD7ScaIE8WpeDaRXsJzMa0HfG7CkNMvA39Rto
48oKueIv+WhgtQQzSNxJ/7wrJXVE1foXxE6AbttdNQ+wnvq/9zsdjbPHCZnhk8SMc5wen9jRUkyo
lHE0H+qaE9DcPsvc12piDjD7TXaw5wTDzJOmMXukvvM2YpV0F/r93B+dWutbq5Cuz9kigU9ZmyFV
EZOokEYtaQa7uIBrBUbEWTQftgd+9O+kbNl/deWHFZhNhXrPFbbIktEKmC43r2HTuVzTrDZHYrGz
9E99+Xj1D+9rfXfkXx9ItMnOcWpnEuCfSUzpQZnK2hEdMEHB7BCst2nOCZ9HABLyVb4YM2Lcs8Ko
BCGfhkLwpuSefbK0WEXfhU1KmCSYSFPCU2hdguJxEfP7Gg8QDfRDtQprGyLLVQJ7RtKpHWXA/TIk
bY5LYqa2R6U8Aw2/bxu+OJVkXjbiXFHoro62dAnJ9K9NEk46yLchv5R2etgVpBHY3wSUkgwG6x9b
ApH0iuR8PrItnoILJ3ewQZcHK0h/20hiIbxKPYTk/UqTLNKzZP+6VTqTQWjIcKhctmk6FBmk2tUT
pNA+hUjku0azot39SPvz4yEEvAoLsA6C+2WpoB02mYvyIQAehXjSQJulAA1L0iEb2xYBrzdU2rBM
JD6yiPrxcxKwM+GP3ADZJ7zauWLZi1QjNJ3SSZP6wiYEBpXa0ZGgXmmL5exqBKk6S3OII1Yay+AV
ZvvRRKAxvRIG0yptP+7PXc5HzuX2mUruUqq0slXRYEpcUoHvOdwWNfopBmS1tNyEuNdQ3+tVHKUx
xtYlqX3Wn5XyOdtwRRlxSTMtlgZR0YSmzIH4lOTB7M+/RGMRSIffMqUxrcB1Ih+mY+l9LxgnNda/
EZdT4QizH4IITZpf+dGyQYjcULPY+mR9XcBqjPRNTTi34Ej7L9hrqXtDPctgQT5BIrQZ6oEDyQGs
A/vUY0SQd1BYbhvL68V5K1ypOZl8v73BIu1C7De9J4HDYJiuXbVIDLy3hTcVpw9dB7yzrEZI74jp
d2DVGQsvIV16KynBZDhfpqIuuoFzmE5D5L2CmspGYSmDSifmu8VmYiweVGfJfkVWm7V/dh0dlSI0
zJ1hJ18+VVmuIGloV0VIIRz5j70dCuZ+oNczJOR52KArLaraHQg+y39ViKlyHcU/NqCI9AMpqe7L
C3f5ssP+qnDIBQYYJQMBeSq7+VYFdXXfAjAyQxM7wF3claPpPRuunSOahegmnHeGDgPwYAL5p9LZ
wt5XP9HOhaEgkfJCddDE0y0wYxwK6O5yHEcmHgqhR2JrXQ/4C1eL8PZwEHlngbVqYid0zpPZpOvG
WSE7xr2Tn4mLIQYnEEZD21PPQZVEvkVegYzyeG1PI+tvfqMIlt8EhuAbiEcxwPXPMzYfDNGx1puI
At9Finys97X+i66ltbWAQEvdagnBoCEFpFPQ/rMA1pzViyQ2EAgKeWEBqyCpHdEOdHUb6Dzqu2y1
i5+x+xJcMbRoAi+c1NScKwrkr4lY0hBDg6et4KOhrXnHmOgkspK6sGBN2QvM0Mm1xivEh2omvlQa
OYqzN5zX3Pedfd2jBMQuiDPF9Wo9bE3JL4IYqfwqv6z37/+Ox5SuXW835HF9RSUXbVVcXVxqkgrO
keyHKuENKF+VouQOyg+L/thi+jBFJ9eMwkKnWMbu7EMpnwWYsyRyyh653KRpY9czAZDE01KOQS5T
vFSsw8YSKfhDk/Y9hddImJPak2LbrG+BjxAnPwtzgiCnRoPci9Efv1CIi78FH654j6vBqOwNoAvs
jipoYfwUhnUTarrj3criKCqzXFOmqdPlg93m5J58NgXbiQwNu2y/b6hiAP6kCrdRAH3wY6g6ln9L
MmTwYP9JselL3wnNZXaE9BuKgH/ZZw5ITu1oqteZ1Ji7Hp6ek8rh3wr0wCUkz5sDKTftvChyVgN+
echDL9QufTumntRTUGbRY3GRgIlJAiacQ+q2jRuXhoh+c4yyhNkCz6gSeMZfT9Jsq1Z8xqgQ6Gv3
j44Oz/eraRFysBr4aQTOxd08B/QGlIpoD37TMzZ+UCGfe0Mq4j4y3QiKZsJF+cy9NVAi86Ns3+dy
nBoupWW5qcBfYJhx3afLW6QpzpAVVpB6VbYFjZsMNQQoW2wnAvX+jGyS2J4w3UjWXIyvz28OJQUQ
fWDK+fZIjPNvbMzYckj3o4uUVl3ckbsMbtvHWpqYNPyois8fx3zHcT3yRRhBi8IEBi3Oz475E/CJ
Ffsv8FSODfpGvGaZu3eG+eXpQxXDaQ7H4dIMBwb/SPcS1DL2ifcC6nescf7DSLfUFdTxpeCsgGgF
9hhQ5ASjKErTZagGHUyNAaJ4eDsn1WnNw8NzccqJjXpnIR+TkbOE4xAMiNYpExksRUrwY8kvdljH
HUCiGRAEu80CEeavBYX6XZKvuO22h3qE+jTCH2XOxSEyGhGdg+H/JCwzgr00oMG1JEMp9b1Di4/j
vSQoOXp/4nkj/f3sb/wKtC0pe3oQsY+Fr7u/r8JgDAlnrZdh0V/oh8cUuBMIO2o+xP0vDln8DzeX
LXZ6hIE2eWpE2/xk7bMVR8Himw/xoNID9zDEFs9zcOFUKpZKx4RYWqE2HUJML4S1OYxwRnE5LNnp
G7ZHSJgQRWQqlvmZmf4yd5fFTUsbLy1oM7zLFEX3s/rtOn9GuH7UPB7U3iG9Xg5y5fEwQlQj2F/q
wSJc3T/cHAsDEkwkNvGonl2RDj242vyj3qhG8/BNczuXSJDdAyS8kz0R1qIJF6N0nrj9t4oouSvC
SES7/zauPya5R+Z/IPMRpa6EYO5tv9ibk/WfitSzEbsFBvhNl7QzJHSfNRLNyAsYPu75OsVlMvd1
Ii84zGpqFD326GZ0ybFT/4p4xv1I7WQQ3c0u34waMa4qDWK2PWGspHNnnaop5XKM9jMxj88dxlfd
NKopC6IsrcVqWOck5X2/00iaQ4711mUiqNBrKusJzdzPp1KdrxizbaLgDgFfeC0nynxZLD5kW0vF
Irw7xSZlrBlONUI7kY1iDbbKrj/k/fzyzwQATuhpD3dteLg+kbfDoUyiJN5tjupiLnMQV1kibyWl
2281oKqd7K3u2Y+oRKfStB+ZclXO0HTtdxN3+0MVtp84lILI/bspl8F7kOQBqMfg+F151ZQPmVly
SfSfMYcdecmYOa6yOyATVX26sZRms+d9/yb8AB/Avp1GZJ1IFyOT5XDBvcpjJVc46HriUCuLfwgc
q8I0K7s+Xd0fjg15hxkeLgC5otZb5y7HOzK8kTM8MSTklZnQpNBIrkp+flQRpHGk6imwQTo7l/i5
4in/1wJo44Zl14CMd9aG7b+eVt1SU9D4YDbkZ3pzzHOH9CATi2ulrJF20US7bpYrolXTY/KufKVl
b04t1pGrRcr+qGI/BO73z/JsB5xCQDEMRXVfH6zDWDquEuhTtXs/W2mFrogjVCj5nCmv0nK9e7+H
BGYnZNO0IN23DlQdWPmFgg83ynzfyO8IDAyFzmjVl7tuf7+ei6ojXhhO4ykRa0P2aoxR6iYlW6eQ
ziNxpC1xtOR+4qbyyLgb8KvNClSUzxc0Ia/W62iW9bGSmcpAd5T16rCsrbcJoST96Zf/LsfndM9v
ekxqM2yLIcVr7Pv19+KwW0puOQrXQDa/CWFV1KlJxnfkE62wVJjbWBaFoZQFrsfNx+KMYP6ULJyT
SI4WOAMs73xmyIXGfF+50f2+6Xey+WyeVbh38Bp/XNdkDJ4icV3PxgJnxaf+1tRQNnQQv+7KX8OF
k8KVshxIiyVMAObxzaRVOWK7Kp56T0k1uOVYcursDbSKJRGiQKxvWu2G4skilUEDbjnkBSTkZNsm
K47viO94Dt4NW3nTCu08Im8+Js5uo7qbGAZVH6BrPEHaG42HVCLy5y4orTVtde1Zg8kmy/VkCY7d
K5w8FJNMMCzAhzCKImgyO8+84HP8IeZRiyaIYJPqPhzMAqjqETZO/gf8jgfHKd7HUox/5ROGS0AA
BJE9fDUnkbRt117do89SqKCzWJcGBwO9Ua0MiKXXWQIiPFfoIlqwP2M8d1rMTE/4uDA174rFqE/c
1MZC4Jbz6iCmu00Kh5726fGZKZAWmRDlnkn4ORot4sy9CsUyHJ7kVJ8wmCGE8IQ+gyRHZkVVV8Ah
7WQ1jzzykzCWgXuGvYsSUHR+lQOCepLXyrtNW4TadRAyFjuPRGJVOLVFCy0cDy1mreuqk+GtDT4a
bsNuhoOBz8miRoT+n2FPBwzNAfxPJBGBg7Zx9PvAqnbahfLPlulWK8azK4KCwO7b5qW0bWIuG9Ex
yjmwBOWv/VRCyuDs/QFCr1rXrYTIc+kFaUVxIGnKCyDZX2nY9k2dDt9ELuLXfSkfRkupv74oh8cd
ybV8pj1z0cVY1Qtrk0RaHLYvh0vjZZKRcyYZ+4q4ZX4Q4rNYnFIift7ICO1l0q1GvR5bkyckfUp2
Cglm5bA3sPlbZevQs0+CEhZXa7qPyqUl+5jnsTo7UOHDOqSjSdzixjzRqJSeb83NyGmtDCsT2OKS
DprgUtc9Ee6x44xi1NlhuKd88l8ITa5IC5fiiI3WKpuBVs+wrT0pimwDTSM9fXavyl8MJszMHYxO
ajkuvb1koBXzMHK0yBbK27cFkdXACNWWk51f0PvNspaTvlbU7dbO8h37jUiOyjeOoEBJhxEn4YxV
FdSCJyeTODzxIp7f8yBshybrtbRdI8yXsOWGt4yEY25Ed1TQnhYJPh9AePW2pvx2rSLUu+cXsYdC
Pgo/FD/tpwB7z5yCpYRxaIdKY8xGEOsdEjsaqMAlV3/HRaMy+I7s5fglRLP4+/ZfOM8YGvheQRsM
sHz8JadsePEBDMkHs4OA3gfQBCnC0yaChMdZut9OuOcFVZeoKJGX6x+6KVIVbRP+cB9eeY52NSAU
cYzGdR0GkvuqXGAFB3dA34WazgRsPgU77RV18rBFuURXk4Vm1AW1oV3yGlPo+LtJs4DJvt71j+j5
XwXZoR+CxpQ+Ok2X0I2aeFtL2tTzlLoJDCWKCIJZMpfMS9e17s6joIkW4RENVLSxkTXoXYVTlfAB
shSvJARcIgk0sVuv6aYEVGBh624H3P669i1xvQzBSxuNw562qWhLGKgexDG/3rnIcpAkX3sYxY2g
d2DdV389KEdhvznIQAjUnU1rRnP6lK8yzBAdcLwqeBvf2g/lFrAjonKLx+Puy4X3uPTPBcsw7NYy
ZUNGzDY3jloNdQ9FGCgQs3RFNbOlLGgHEQyyR4gt/Fl5GfQbIDuw6tY2iMBiPdYg2FxnpNyKwAlK
opFi5xRyXyfvUtzTkarqbuwvwoMLqz9TPczhs4S4unpJ90O7v9vHJVLf40LIAwjpmVX0ETki6mhG
6FNDl+zgunHSRrLDXZ3Uytl7HHWdKj3jUgAI+zZgtZTGrQil3FGfNaFaDBKnYDVdCsJwCzjSNGg5
xNTUD/WxkSsH5BS8Dj23cYjmQHxG6VYyCtqyNo/iRpwzMdLymYp2O+QCfnc4Nchhd+SLW9Z07eU5
F/XPx+pbDL9E5n/z7Q3vvPqq9IfXHa5rgmBuE6ieTlJbNR5VGr61z+YDXwyKh7VGqYz6/qvS813u
pxh04SJMRfhbEVEs39jW3lJDJjbed0ctd1VnvkZJqcBss0rzRMEw2WEyVVTeAs2K8elgH3vPNnc9
Ax//be4ffawgm7vi8AQuhLF/mR6GWu3IbUCeE3rS+c6JInfTPYKWx6HGiWmR9lQjUwVPpy91iH94
+Mkzkd5RPBnxt6fX3gKOdAP8gHsld6afQDBWV4KKZ9BJv+szxJ/zTEOsPB7km7R2RNtwaA0Bc580
hkUZstwXnItYkr9VYzYt+L+yGAxt2fQ5dw3c5xY6IQ11n2hq0dIaKQWrgPK4yLT5L4eS2FFtCBx8
ii7X1Q21EAm2XDru0fXlAHOhdaEBBUhNOv7IM7cLl4I7ZS7x6akYf5htHGPuA1Z4gslcc3v9oDS8
MUyTQzYLWPxt/oX3c9TX2CX64uNUdIqNFc0EDQK2uC4n7ISLomxvRQ4vSf2y4ykMdQFXchWXUItP
hwQoq+6rK2vRK4rxowR1h0Wjp9TneeH1yvlxjvMKURykI0zueXyRJC+XRmjilREZwrJg/EtBVptx
JtEblzqMjUxcDFnSFJrcxBDvUZWNSc10FfUtu1vzMmYL2uDW0iY4PXloPP5NL90gy0CrJyLQQ+dr
BRc89KEt4T0OapFLzzIZu9rognmlrWxUm8z+mJImR4P8f0UETMMD999+lHhGCn3+2JS8t2uc4NcC
DLtLx1Xda9izSXK970/pMZB8obeHmcy56eBWcyl1euj36MRDKjJvNi+kEHZ3orx94FPo3Qv6QCqD
ShSwwm85g6pBhqdJUXqKEfS36iUOzilJoUPQgARe2II5IY+Z+Hmv/nWP3crGeVdkVmWKbxRQsdse
fuvE+mjiujEW5fiCil+wX1vZHNTEZZ6IBoBhD2BRa2wJ+G3DUSM9JRtS+zBs+bpTdITUD/zUYIsx
WvxUl2pPxuWNJLCF683Jq0NoCQ5b1e5sqZpIyzSERaRifxtnqqcV2QB9hF5zUX+DZCi7G1gCGFxk
mXYn0fSqIVdUMOY9wkqH+3sjp516STOFaPFebMhBOMIEmiXDvfAGacJii/U8QeBqiLI04DCrY4nq
oUJMIxj8UbydHPO8wlFtmJwWmJPBZvgZIii41P+cXVMu1l13thDI55oEqQRIfuk0YLOyr8Lvwci3
XywYVxcVXK2h/bnJ1ZpisZq4BFCJWcld4RPWl/9/xQr0e+kf+Bkk1ro7YNzxp73xlwNO7qTMdRWZ
nUuWjcXhqbuxltf3YRH1qIDcs9NJEx0d0j66J/EygEZUr4WRR6+vbVB6NGGERkhOPSJUyIAuYj8b
hF0kKYrg+wQrtZSTZULlgZFsmaFp/ZktBCbBegAMoDZvKmecxGM4gkSMRHy+8AobLq9jHd14E8MW
zfm9P9o1l51H+TC8vRuorc8LcxN93uAU3B/WGsKXa9tPexBTp9L0xLMBx/F88KJVM0jxiLAYdDLM
rRt4RW8eQLS1ukDRGzmpQsT1FYiTpg5zUy8JXNQBPLO3ytaZBkiL2oIanL+vPjaL0uK+P+8GWl9e
agGc0aP4la3FVypVdcPF42biX6tdKxPmux6Vi+2QtptmKzcJ+E0DmbMOf/XX/r+BToltvlPuk5NJ
/ELuMlfoKFoUqnECjBeWIye0lZmo7zyWYz7sn6eG9HpaJP7965h/bOWZ/+KNqDdiE6nnXPu9ExX0
jmeHmsoKF0Uij8YYyH161z7Fr145cxKNVNIsyGgjcse6YLeNjULAfhGh4mr449KuQosqeyJdn5pN
z5LPOfKNvHZdTMzOgN4aQVNRz8NDiDv2DFDe9e3OuweqjjdrnhjQ4r9Mr6Usv9+Mn9gcqXUWmN4N
G3AdinJLmjh47J2QTpQN6GChrX5uK4MkIeJrd/EX9LbFXg/Ybe+jJUQvkry7uJ+vaL1l3TDL8fhc
fVkOTqz/HHh3m94gU4MnUYAHW5yFbtrwEzdmmsPvp4euNLnsB6PgXpiCRShqvZ78FhOPE05g6FCb
IQm+ldVF0djKUkuqcV3Kv0yV8kfZZvy0uf4mh6Z024oKbKgcQa3K0XAVtvCia4vxj9DWx2HUUx5v
2FKCyH/3O6IPlCItnEzoY82WzT5Gu5t7Am0sensM8r4C7STJ5qw/fYHr9j09AfQPFlFq/kVOqp76
xPTx3RGBOYGpAYGv5oQXIFiOJGvXNVYgylyvl6SVd9VJsVV/5Le1bxOH5V3LCDHgbr4ozlHCr9wx
WfOoAYJtkdV/3Oo+Ec/YcvYiSdpe05LofAVuVf+fzzzYhT30UIXFseAC8jKABJCKwlFJP4lIsx+r
iA5OpDekiDIySxU+svVfsJvvUi1dB5sbngTUJEuzsfMXKE+WHcHBM2TC0AuGBCCjoyB0oiWsJC1A
I/B/IE4NQ8m5RKLGz3kWTnMa+U0Cbe93VN3w5YYM2L2fCPBsmjQXvbL2CLi1jmG4bwc9Z2pSRbfn
/UvgMb2XXPIvrfegaUJvaWSzmlDgV8P3rpfjlVVf8rlILG8N3vPFOijFeTLF65g6Y4CgHp2qPui8
peNK5Tk/iJc74InXAloxmmnJeRCSOso6/M6exi85O5JdAZKaH+zQTORwPFuwP9t6BolvgPWRfoMI
wspOEz1sDZ4tjMNgBn7OFXXNLSnHYOUamHwv/Ot3Yj882ufBa/pYJgoIZ8yux53JNGPFaVdxEC6p
1HObp86349SPrZuJ8i6Gr2ApWE5twI2+6Q4XG7XyjVeR+HmLGI6ugSWKR5VBUU0LaSI7grnwIs2x
cdhvMpvJviC3R3AlBNzymgSD5elDnEmXZtLyFvp0ddIa3nzhNEOUMwQl+jHaOSbdProkRuJL9VGL
jACzl8KkhOimbSbUuhK30Lz7EiXhnGUimvAIWzEzjh7JWJfabFxcqy9ddRIWNeHu0K+TEr/XSo9V
R577FzYA7GalzHGh3xwAuCLk+LZvPkIX2x1QKTzw5x4lMDfh1vi9t5uJkf5YQwg1HOK3dXxGXyzB
fkDRIQKx5h/iCNw6E8IqLgmZSH0ge2w2F2ja+7DLjrcjfdMSRPh5l8334IOBPq3rgqd3MQ9nMwZD
VreyPCxhl1ctcZIzlbf14imSTZfFaN319lQSXy1uTvc4e6phrgza/sjoTuuCqIgYIK6ckUhIMj0P
7LKS/49p8dY0nXtuEbKfMNl8uH04MYa1WldLNTIzv6hn66KoGGRRXGeHp2iRiYDEe9c45cpxBMna
OkgHJ+C8IfgBr3EI57nBuqKCesaAQpM5EPKlERQtMhnNJwgUReOKZXmBuKy2uKJ/DZy1+jRWiSVj
5LELvrJXCWDHwPXyAoIbDnVz/VKHq+wL01e3Qf4lGR2NIbaLf5eISGM+FJIOusUk1hpIkssR0lap
zHcWzcxTgIXK46QO0v0DXi35fvD7YzKh7D/0pt8aBnsEPtP8pJjISG5npVMGp/Te4mNkKV8EgmvB
lF2s199WAqXkltRRlSNEO2QhTZD3IL244tw/w0VYacKgO8UKxN+RsU+QvdW4hCdMNbtrlGWSN9vu
SPK5lg7/e0P+MJSw3VqXLAojl81+u3xZg4EPA4U0mGUTqhvcADDTypP7LZyAxZiAM7llPvVvslvb
lXcPHedj83jTVsRzjssRmJK1ZsTYYzJzbHBBOOyqilLY8Lrwkpt6YKB2wUiu9Mq5kv+ujM2iXE3W
JBAj/Xr7ZUTnEZh7yozs+iaGN+tLPoEycrEFsjF36RGv0m+HiUycE/H9Q9/jERmgBu/sOvjq0JY/
909ARn+EnWNDCvFFrnXO5Kw6INHhaZsAa5D4hhF+QlmmRyMvPXuiTtqOQWWxUfJAuvNEQdYUfNxP
ChDEasrUEeqTPip0EIk8Xzrn3b9eL+mnzyqc7OuuMvBCvgsqxfQFKtsnb0eX4djHt3XKgIDrg/wY
TZkFZ+Nyv2+KrKu2HuetlPuUe1AT6ckBScr2vEC165T191Y/wD5iPgQDeQHKz84h9WImSJ3Jp8qX
djilAVDw8bAclbyU+bzMW15zNw9a24HSrp9mKvw/J6OOig/+4zgJzcs4pURqf74sfvuT4ObBTNaG
1N6f/AiIvdhyEfJ2JYQYSHpyAstfqsnvHrP28FbFcIv9XetudmRmDDKhtl8Z11YSgX9uqimCzV+x
Wx2ypQZZnKOAnD7DR53LdsfWNMIJSbm0XOgJXMbD2PQ77E4rJyS2JcJIkBGVozHlnCA82EUb/wBN
RqnJ25B5N7rZEPmwFi3VT91kfapxzo10IssAQdKp+vvsjv4JH6bjuu2bPNtdLFeT5ZhD6EEJb6xP
SVhNlcgQwCmCAWmNO7IFumMycy5qYZa9rphc0l3mT2HJ47Ppu/pMuho9pofJQ9EMAJllik2OYEg5
gtM9wkABV39zrQOaZYCpdYpPxE2WpU7zwswyMg0+bAYhvNPenPL9Bc75QuT/SdcsDgjJVE2DIH9s
A3IC50S627ufHe+oJH6VYJ2S7J2I3SYG+yV7HfUUPjkkE29LSi/bj1NXYN/lL/kxV4bVdIa11wlG
W3EpA4cQiFknfxU9SPIHbgZy3xeH5HRysvSsc5pXqvSGgPL8DKwNa9LlyrfIGr2vU45z3utSyprZ
SRHdtl2cXCfexNRhAgvv24tE4GLUmJYabEhhz8RDCr6tcGKfJ/qzCzTgalMNhOED9OdsXyBmfEGN
Rn4pEiWASlSC5AEN52vxfRNjxktuB65k3fORs1vZeSZ/B3bik7+SMbD2QX/T5ZXiSnPIRkKM3LXb
IdhGpnJz5qPkOsvzMnusPEX22rpSnpOdbK5IOcQlr4QG+SUFl7j8g2obPKuj0cgpU9kw12ouk2/M
inUVdNYQhbgY8yBruq//0loaz/obow1m2HcOwfjJ0VtlHijHBks+JRLvyM3XdbRnKyBWZg3AUInS
niUCRTel9dmwQbXQoEOyZSqZDLVJ6k10nkHgHx6/qLAi3rQzdKOv0+IruA5FNu+4Z8ts4R6A51JE
HALzK182127tiySEtO5G7QV9hBXwHC4nrx4h6GtjkTxmwBgmq2Z/N+KNf6lStISv/0Bbb5izmDgO
iw0p6hAhdXdbnwalUpP2eBBuTH2cf1Cl1IsdoX4VxvkHutvShly4fo0wpuMaUHF8eTL0XsHhdBpS
IAP3cwVAhn2HULVaen0G3Z2Lwy66wDuRpNW4wz4OlMtNpa+vxP6yaVdZuwtfVfp5fV6TgFCKjhwf
W2h0THV8sfoJ4lW8+xx6Of4T60SnnAI9Zu9Tm/eMlvpPHoVSLmNVRknIgwkqgAFGnq3G9Bx+ylr+
k1oJ1ADnm8fefbTSGMnnDJw1b5Z/N40WC4Hgvnr+lkDUb7+Wv+sR5SaqwWtd7Pj1mjoumMYtmowS
gSvGLRs653w8uXKFMDMXmtfVH2/TeaLz4vPd/bDp4DRdDSQp5YG7Qvr9ziDKcG08IG3AQzUVf5JE
4b++6iHH8HZFk2erFZBQa2dvnRMs6Cy3H9FFaCzKERjP+QQZPT+JHxGssz9MPBmbvyt8FUlGgTEp
PBob94Fkm4+Ma+6ZCIQsw0G4YLBkZDHZDTukl+3ZiYV1sKBxuLjf0SQelDvz6Xq2Xh8z6VH+SLDQ
OqWkojAYPEjU+jFyoKzuB3IyfG+Ql5XLz2RcBRsiJtZvUXsHfhd6SFb3AEldNg/YJoPADc6pzRNE
DIGgD/ac03x18M5wVBexg9xUDaDIsWhw+zuB3fdkY5WaAElvzAn830giVcDXf7/tLiY4EoIwN0An
W2fHe4yg/g9WlHEVAjvqFrqXc4MjZ0FrgGD7Am4tx+snJ8SmsOnarKMyN1r6cWIT51GOu/a3UloA
jKrjnV8gAttpu61o4F2S0qfJwht8ctPIDbhxnV5AH6m6BWh1hm7piLZbuW0upw+4iEahTwJWgknM
33aahayiqZphylphh3Db61B4ehhVANlXnJRm0va8E7S/szbhmIHTWrq5YJRYMEb7+2G2EnBbamll
Mvz1EJnnBoLZjxfBbgEPLcK4MM6B9kyKsMJITYew/E1/ozFmcnmCRjlWSaQfAHHVxi2KD3MA9CNd
FVNL6ujr+QNuxD9BfYVThQBbgYG0ivmaoXbLn4DRrtWFR+vrl/Q27/mHhDmN2xkU+rHxOetHZQli
M+OotoPMrY2wyWa19uO//07ffq726g7+SLQUrhW9/gV8DdlDTpE0sl5bEVT1SUszjLrZbikpXNwl
Fg+QuCqAdXI8QRXCFE3A0JxZj7uxoCXum2aFdvZCT0pYGiVQK0VGErkN0qivZPTZQi42hD6i66ef
pWTHS6pJyx/5vbMwLXR1Fq13K3526ugcrC+KGXChdO9G2MphBIaDZ0mEZ6NSee8i2yUboWmmCgSZ
i3NsWpJUv53P7gTojjiA2olZKJSqkmpiIpqagkBV5mJzKTjP655BSPVL2LV8+lb0RFkQvrOoGqty
kyuAflpnUac9ECHqohgGKjz2dfsMPXPO6qKgFNKGEALzmnuM4M1aRq1Wul31skTIcFijfC3hq2dp
QzFuXPsjQKGDcdjR7EBTecVggnF3pItg6IBVwoji22ScUhxjukYnRoE5WfzGZGrBjt+vLrLJlyPR
BucWzQ3sGCbLJeRxqObNTMf0G05c/tj7QKb3eGdCR3uSMSA3kgloQoe1Ugf+ylJr5O0JSlZ+cHsx
M5dDwnF0HCo9tCls+CFiO4sIDeuR+talRcy25tlJ1xUieJLr989rhVuw/HDQ1HDURp0P9NgO0uiU
yD6qO811B3dSE+xOnRK5r3dyyiyiJU6iSDPly3A0aE9SLyK0QNSZOXgB4jF5FEMc+VU4xBKNRXj1
JxaWDjXDqK6xfJRgDe8zwmxnuq/mFe1/8zwDH2JtUi2u3yBDaNe8rq0i9HESIYWaT9nbiCMjUa4j
rVUAoduP/ycWGJgTwE3/oQrbIDoRuDLrHVtTgqUH9bU16nXMK6wkNyBj2I7qXWaQCSkcMYv/vF0L
opVvWQ86y1JQVktwsmxMZ1hkhxwNfpRdE3eLLAZ43PrA/nak/XBoPBDbZJU7DOBBGnUG8NBCj/Ky
erC5pUE6gNmHp7Hd9hv+rLvrk/YCF0rnS30Wg2pGna5hMxvh/ChoWSF0tUySURtGt1WB9k8vcIUt
THsk2vxDiEBPtN0nVNrfVzdiSLR30k/09ytxvSYmY3N8kKoeeuxi3n/ypjKo/U+BXhUTApTQ2bjV
PxpBeHZNsCjmcR5xtrtST69riqDQ9KUCbJIvAeRf1UzLGZItgmY+8j4Ps9MVMkZBRYKRWuH4k+lY
P/A0XAKvvoreE2PI3ZzzE0IUa48M0O32MWbORVzjiWanTAP2gQJb6A62ClBSd+Cg57zRCV63jVxt
HviON0NRQeAflc4DF89tKYQjr06KL1GKscrxxz+/cor9jatNxyY6sjvDXlmqNQ9qdqv/5eppqZuP
iFrimBRLqcuUKJ06dEdbs+e5Hnr8CHA2wfOrhpTokWm0zYedbReE/s1npHUCNmyLeahXtoaWguAi
DmPqFXsekKCIm7PViBm5uY4pZDzmf7d5HZ/RyGF+TvdzmfpZTnPYMf5GUoPyNI56veNoTTegxId4
b67wmTF9Goqb/fmSdjInsdq/r1RgFb1uWnSBUJ4ex1vUaWLQC7oMlYRQnbIfDsyL/EQHz8YrelQ4
xMeItGy1iJtU2HvacK5mZmuEUWlCuvpXwru9B4o1F1cc3S5TPWvZ2f+DMLrOKjX7PNYqBCzG9C/2
+EYw3D03QEWwc/N99samjaN//xGBBmq0ZZ3VvPbYQgsc1BGXOJH80wszuccYQ2H4yNGv8WBPiF6/
i2pJPHXFJ4j11xR188IK+Ij2vb30OrBHAj6XNTAy/s7dL7Nc6/KdWMTENYtypAnXRt87ZER9wUC0
XX3cDom7dPChYE2+uf665tTPK8UCsgFexiO440PAs8NB0/IFXVSCAj+wF9d9f/6WLxdgkkqadUNj
NK4vkfiIPqONEwD4KFjb82hwmykZ9U9yTLV7A6gzQutMBb0POR+4sUWJDuzzWTCsvU9qHIRLFY2k
NLqQWLuPFGY5ERl0b1o4113Y/GpG/ZJ03BAriMn0IckCrA1qeTP1zqSCb4JJnzSiW7hTC5ePPVbw
93Rk5YCRv8T4q/Yj5cYkC2jPiQ+1XKdGUFkY6t1m5wO0eJZ99/5AKnxh0KT4jyU3EnGAGFoQ5Fe0
u1wtytJMohO+mWtgeYl7lfDhxTk4CwHlsatKl3urkcL0dUvR5kRuASXLhvPlPdc2pLv8Ir2+ZPPv
9ypvepHyhx1V/QxLGAn6wTQ3yuiZo/uNEZYZ3PDjGeiyg45985+j0XPDnDbOnMgrwkQ3Yoc6p2oy
r3paE6odlOppS5MPKwc1M9zjuO+Qp9u1URgOMTn5YtTZcKakUEY/98N7HHLoK8rG4ho9zRAv/vji
BIwQXHfhpp6N+LT5ZYiULdHetKA4l5agd8TeldGi8YSR7jmBGZh01Aad4fXfoToKQiw21iP8m59K
VzoqtP1dfoHcNQx4TxtvhDRa1s7U/hyRROTJQB/SJWLP3Susb96C+SSFoedJ9BYNKdR/6bH0qoFQ
QD6cNdnrU19ZhWk47QxUsSeoWmnsYdoikXW6ZLyB5+wZT7i66oQn9/rkK3PvIUKh3Nn2E5whnnxC
wWaN0diYRhx1QSBxuasrjM1KgbhKHxQBFqf6bDapPgViSVyDyX07Fz1poqNYvt0NKt6MR31wnXKI
6nyvFcNrVe4QDx1+IVVe8/xOGi0JWn1CK6kbfbBP+GZdjMjvaRzgDwxbnChtzmUKRvJEZvmhOCxY
MEu13oedyGCivPjwXtWahIGJ7nEonDT5K/5w7jri7jFB6rBI75N27LycGycx84yZkwUiLE0ONG8X
kbWc01KVHsB8EUtmjSfeelUw+V8cwuBKZ9fNMUJz6M8wt9dwEHK4OPzrwiQSwNpV3bXqRrx4QWN2
tVBuW6zHTz7L4G9dfTot12s8y/Oij6ieDp63CyhIKNahgYGzK3fynqfv9OfOStnXa6ah8c/oSapr
AGsqDM0cXocc4911wMZcONApdw8GmlLP5ZXXaXKbk6pT6AW3bvi11gqTzrU3ExYYCgxvcEmMvwl0
k3O/ZkZHGdQoSeBEnS89lj222T3Fk/+clg5a/jd0otjfFibbCGdHqJCaAYHJINv4S5jF7MjyN6vO
vdTco+DtWlZsuhYyw1LQL7zKUr7NEJPZJRNgNm77fmOYLRY6+qOP3yoNZBAzq7JDfnxIkf2ZutVm
omw7+DkhoEZrYSfmRfODUk2wu63Cv3ZVHmM9IYhciyBJOyLc6/PG5R7Evn7ufyzg0PaQ24msiaAC
z4FkR4k2MZAwEfNDPIySAMx/+16lkWHNGoX3o8pwMg18HaLOBK4BZbht3l8c8eiV/a+g6tjbjWC0
8Jlk8cYPEW7AWn1kvhhjXuyFnlAX6/a1O1bfCtEs+YmNEV2iykcqTYqkmgeD4Na33SIAVPO8Ddqk
S554rtKYRi2b9cuxtDkHWB3ONJgmi83qO9UzfAfYaneFvynwSRrGGURrkxE3xNw49Rni+AEEiSnJ
+N+N50hmEdVsdaMBYAPrpkTgYuneR90d6GClUKtNLgfJx9zjeLmiWE+hW6B3WFB8bx+eIrj3X1tS
Dcktwxq+Xmpk9xqTd083St1/Y/Lc71hBVW3ozWvuFFY3LcE8gtIoAG19n2uxxZ6IHHwOuT7lHpDX
O4C23QhUHF4M17XRWi1DhzUkb6lwoRNeVHZ4FwEEj2/+pYLqLLo/Ckv8WmFXIoOmoguuP5P1pvqS
Wk7JFgWnkVM+Lnd0MyWzhVWFp2hxrSOhcJ6l/fCsoozOt5qcsFOi+Xn1W7Gu5a+20+mQpk6ux41q
55oC+EAjKtjIEOh8hEoc/LUi+oJJNUUCZFS3xHUmE36Q4+08OUMrwoUp6FEF/2Vw3RzMHplU+elh
B2iGElTEoM1z7Qd9jVcJOGyc7acnbHnEem1eFQFQVpTi2AUylGraadOu8wpi2yckBjCYPcqEidN7
b/Pc+Zbp3aRoLXcx5tfj1IjiyHHf8PxtBD51CvH+kIceI9Oau1TcI4ygdLv1F5kr10eB1m38IcTu
G5c3LExWhnvTXBICjYVUpXrrjOd9ArziSFnGHrCLMZjotiTneMf/HtDk0O0NVyqgU1azBHv700m3
MozADC/2ezvTKYXgYBuDTR4F+fBC/SzUbeSMMz1mG9pjeva7BjEmHt6mXsoX53qkG+7oGyWUb49K
uXoBfJJhq0KsmbYoJnGbpur8I8r9YZy+Qdwuq+fqRelmQZmfUoegOQks39emNH/Ua0WW90Gr4LP2
/Gbfz2FgPNEJHckUEz71iWCl4yZljhmFLVylG4e3DoEwJm4shjKKXPOhW7hQ7deAoyxyZKCukiYs
ad1l2LssAsCxGpnGow8Q7hfTmQWLsT50w0k7hz2dn6jdEZzP8LqugmN7mka5AtkqPYuj+6lOYCWv
A02/+cCnAOyDkIxnIEhv+bHPChGoE+dja+huMXnwsi/VO2FxMr79JP/IPRONQwue5pMt6PXxeZe9
TehvOMY4NvOIVQebdW1ann9FilwzMa9UJhC6+lYnPTLTgcH58Ajpy22VVsvVVOf89ys/Z7X+gSul
BKJdBfcqggzddpT9+wJ7cdBrPU+p54ZneeQSWyilbYE7pDYo3x7DTjdUcHIQGxnpTwEFPW6qR5I5
hHiS9Th7TksM3gJIQRhAloZu9p7Ua69lqqQ47B7p8PTPuHgFURqcEI3nwe9J8jnlcxIT8j28OSFQ
TEyTE8jckX7BdNsehqmVlbqrZL3RxZkXYJG0bJxwbRMwsT7S8L2tztYN/ghSy3fJ2EBcOZ8fRjI3
ZTHF+IyHOoxFi9CU2JTvGLiaiNHgS+WfhNSdH4B2e0B70t5q0speXykgfk5SAmeukcM2Em7MH6Cu
D4q2nYO9Ps/KeZKpOgc3agLWUCEatXOBF3IPKsZXD6Bx4XWSWGXULcXvM0zgUyn2w50gzqLzDKwJ
oUKSN++U1JtSZ0ri53l77DqCfmuA3mpJ9I8oyQuiQG6Hh44gMCX/BIorcdbKDhF1drjGCbUf/xoV
Ur6ySRt6ZDSSRwObH56UVBN9vyJTytD5I09GQpuzZumT3kG09P/fEjLZ1y9AokjNCDnMGHeRZbej
QoLyozl2t9D04nz5JJPw28zvwhj2dH4t+nUnND5WgxJCqxa7LqczI9WX5N8eQ7mG21ijD0DXMVZc
BwfWvdKesKy/aOc6Pa99cI0V4AlpWt/hA3bpgMMFK48yqyiWq4gB7qBHiYV14ACeSobnRKSm/dAj
NIRLJA9llZZ4tX2xl9sFxV3QCvf1ZKz7PQhmFcDKl3WybeXuo6vASxyUzQ7JEJCnlXa7O/BvEJRy
0Vtp+BuepCcRzcKoUig/r3KSZgEU7YdcDKW8lRQmoWphTEoKWkDCojjHlVr+MNf+cK89yv9rCCfq
mQUGKFaBw9PghXg/u2/rNnKoJFtypupl5PO77Hp9DKVJeQ8pBCW3R/ne7Rq2bouyAWzAnFc7o3Sb
5ey9i1uh3aAxgTwbWz2sC4bliWnkOc+q5wBylvq5JDZmMbrpz9Rg6ItrVzt5P7Xo1Hu1VUTabxWL
GmWNxIubvh+FRFx+cxykxzE6xt/iUX0+MtrLf/f0zOf4NNxRB1xGhnBE+tCpuPR5D9IyycKxR4Mv
B+XBY1CYtHwvocsyx9NRi4sdcR3CkL/mPxRuy+kDWt3Ym0jjZ9nm4MSG97czzQ0Vkz1zips4lJ5Q
cm+sGpvdolv+ogljuipR1RLqysIcnEmCDH41TzUzHPyGsOWHBVItbHDntsUTbI6ORpUF+k8coDtf
eGYuJ4F8BJ/a55Nx6XLRj2ayCcNvw++xu/9GlVLpDIQ/eOKZVJ4kqoC8RDQpSEKAyoeepEPpbfQ0
t6XovGkGkBsMgXFhoPumGUr6PG8dex0g4RNCaT3GT/SkFehVpO032zzJHB2Bx1E2byKx9kwg/jxD
mkog/rDAUI9yv3nFxzict1OkAIup465UONhrRZ3KS7b/mU1lPqHHlQCgrKmed6Biqk8tfnbnawpE
rXkEnhJIo2SOHgL1iXamNmPv6iuoTyLtGkKZk9ukHa4WRMvQMKvuDIdFaHTrEouCPxTFFFAW+00b
WxEMVA2Zc/GyP+AcWDiWb9ATUZX3gY0xpygISr+23pkfWZoK64ZO/DypC1pj2Glv9WaZEQlHhzbX
+eutV9aiN9zc3a5bSWzrU0B17MfC2gnJ4/KcAD5lnfCGKZxGfrhg1g4S+qwNp0Q+jnCyw6nxJWne
g87ChBKPSMwvCT/xtuSl6NbpkzdfXKefEvxiQo1Bpaj2CIZcHMMrCtz1DBIo5GXLWEtYGQprrt4K
OpQEzBBgdp8G5F2z6hs13wnQGs465+T+Q8ph0jx/J7pIUx96DFgUUrLcDkuB5MFHUd1I5QOul0Lg
82BiKTSQq48iwe/Fnlbk39rL063cZqEB6eS/zeoBFB2BdvhduaspEm8UY0pxva6UmKhUhf60tM+e
O7U40BAtgWsnRB2yazwfH66pj25ioTTDR5M1RN49oX2KiR37sHRKkChNwa8CPCQUD93ubE8HnlSk
B/wfXMEs4lb9lmbANS8z71iPzsc0cA4x8AEJGCxi5raiinUb2VXWoYFF2a4HcUYGDQFDp2IsjWpl
KtYgApoj9pzIIUFbf8xvAQTN6iOxd0qaY+D3AXC4yMTnPRBTVIpGVNob6MRb1wOeEZbP8o3+Kw50
Yyajo+f8kG79GqvzcnvKh3qtLBBGGhd5E4UnViXlJNv16DSIa8ryy6gTBVaL/lkQKvqwhA4833D5
eBr4pIwpMCMvcLecUHyzvyy7/gUQPjFJQBNUcsTNnLnKoZnp6quP3+Tvkjw8jvjksnu+tGYwmWiA
Hu74yAZLhLjPmHSngmxoKhEV+oO6hSLghmTWdRIxlltUwwKzlpGMDuINdypUMQbyWnsVHSeinJ/7
Mq5X5tKLQtSMM5AswIRZOJpzNZGeU9qnaqBTdYkx/fzbrS/2DViYGaFbVPQoTDOPTgPtHq/brQ6g
1hBfV7T8o3hBsXCd5wqUx9KZMvViNl2mV/DjIbnhR+TGUKfmQTfVX3hs03Hu9gpPEqW+t+OqT4av
tBL1GMDYmUH1NPiDce37qJj8PJGODrjWqewi8U9AsjSeEzeilkt0OA8CQbuQ28pkQTWvNT3iQGzz
R6KsD46mo2t8xC0tgwwq7kkUmYBlakDlEcfM5ehHSod2Hb0FdQ3ZpH0ViD5wAMC5cmvYC45xgWRn
PhjKc7EW/s9whcXYiwaMG6yXGL2AfTnXztStyPM3f7b2ec0dW5Oo/KXQixrdFjoWxZO25+XT5Uvl
meT6UFvwbdJB8nT1fJa3UqWClls7FDeRssG9uhkqitTsUIglgOleXFaCtiMtga39iTC5LxfPLfDN
9KaNKptXZIag/EFQesSRG7B3b7hcuQguv9Rr2942QLYfYp2tn4AygyNpNIJlkI6vigq3LJc/EwVc
L7VADCIr4hpjg43AiecP3UHVlthIhqZ2/aFn/ZEprHkZiXx+sa/8NYG+8filMd3ATVYBB3m0bLTK
fR6APzjnF7YjnkeUBZOSiNKEwukFzmET8rrq0lwG36oLTdBdUOMUnMrYtj9JfiBo2kyF/imaN75J
vdUU+xraEevto5ZmnNhLw69qHSyb4c298kUuQIXRk7XPaEkH6YbXA7kDgzs4HLHsyS5TfBX0cf/u
3MfvbPwKLHU+oocKgnxyO6ws24siNfJHVskHrzBwWywbQAFvrV7Q5F+2qGNIaaX8r+onBeUu0lkK
mEa8bJbDnlbz4LCiiBV1oHBhM+v1oqXhOmq6wHwCaup/f9uN+IkuLCNk1xpOSkcH7Lz87IBC1Rtu
MIOWqYZOnClULe+maNz8sZJAClYw29WdkkCWGC97OP1JiqRx4siq+a3i9nuXl3hEYxhb8o8xAe1R
HLzn4gK8BsTiHOT6ba4GoZ4qRCPFpw7sjLVCbJmYyOR1xxuiYXIVrGIJ3r0aquNxULVy/skKnlbT
M5/jersftZn23Ga4+w09OVsXQJafCYsC8lJ4XMa5Qk9b2n9E9hwakcuy5+XxO4INnL8TjMnDJxy7
LUwl7ISh0pX43gTSBjiIAzXzryu7bpH5k2HvB8ftqXT0KK3q54NAAkF38ZELroYCrhyrKvUVlfjx
sk/6s2zfiQ+/ZxQ5PQJoz0SSDAcdlG8csPB23lXm7j1M7ZTWm9mdsgSZeKLOts3pOE1iDgC5Od8y
cBfkwYs3SiBUCn/cssnvsnkoIsx/cd95qy4+H3yNAY0jItHzMvvPjBEi+gTydngJDOylcRwuMKp2
OenyTliZ96T+SZkrmEG3Cf7RpRM+74twQF4boUtbQ2DvKTiBr1aOmWLI7F5CLGbi01USu2kxsu4Z
8nMnmbZ8Tn8H9wQ1m/YWWXdsfRizkZth/76fsW5h4vaVmcIovUcY4ulW7+Bk3j7s0pE244a6Fu+/
EsCXqzKfpeuBCg4sNLP4Lvhn0wXG8w+Sth9MnIQY/Iju5HZ83YNrg/xgGOq9tUjBLV8qMK3iCQuV
WIgT8tgZaXMwMy2URXAJrjDvBjkr0Pxi0m5hIz7OonDO419xBEWcCQTpqhd46FXUkrU9eKmzwDQU
/wo5aKF/ogYnaKz6BGJkWSp6cWk3p6+B/V/4ZkpCQecWxqCkD/sTcbIy0sSl64fhz/9cSzFGuwe4
jXcSPu0ixFwXZM7vRfr1vNmLUGPgXI+vSmWl61xACbWfdVvDsQSaykEZhJUM+Dtkwah2a1byJZ8c
GeoK+nxrVlmml6CF6oOLbCgqn01k+UqgZelGjNDF52Cpo0H8LLxC50t1clccRxwmF3uwpTSLxzrZ
H0h6j1R9rs35Wv/tyKNzdTgnf6FKL+8BcU9ZL1GVCBevvNZaht5XDAFTuz0X/LWUXRx6wAo6vnnh
hFSmB+YurUXJLuT/yUBeQvlMDrUxuPoB+X78o8SH1dgX6NzrR03kpBBGJLBz3m3CTzisHcIjPBRI
c/UWIx+bpGXexthAg19sbzKZ2aRuKrQw2G/NRnNPLIGOyvnItVlZUI3cH6SwdC43tWd2tf3iV6A8
niGBAPQpNfAQvYsI9DHVlwaNptBQws1IpqWprgmgddyxz4TwSURtqT7m8ph9yhCK5Zczd+MHezcO
g3V6/SFU3i19N1pF0xmE9LpMg35wWqpPKEsN61Mlub2cy93gI+TFKvK2o0zIAH0vqaBiZHK35+sz
0ra9da2LlYIBvkFAXMTi0amvfNGRp9D+bS3ZSW/Shva+CRvvJV+qLABe+Brai6rhs/agdKRFSxGR
p0gdi6BFPuJOUiAls4SSvq3WmetpM1V4mUhc6FjihOPlnZq9eTuCAav7p0QmN+QcQOjm9XsgviQE
qqjwIvhyTY7z+oMRyS74qVHsi6twWSk79wnTflaap3BgFn9Zheklry5VpTFrPZG/vkkbD7m2QS3z
goxxRb5YIV7WotUk4dNPP9YLpWkDCv7btlJEQT9nw3mqKx3A/C5ij4me8ZO4++Btg+eVL6tO1cez
8Q6hYBDtZ6tmAoro9mHym/Ck4tFsLrxWoMTjxUWqy+b3dHuhRAmpNVDKjJPUlyh810mlTh5jskqz
QZ24L90VK04SH3x9yCtWOjjzJsxg1h591keVM8aVptSXaCFqhQK5mvtQi4jTqQtUNYh2OnZFEpVf
6UoK+TBiWP2w6y1lbQT22/gIpe3YHoQwKZTj1YynlmKA7/X/Gcd0Rm+1u4SYftfDZqqqZCUB5X+/
QDCVN3Ezm3GJmbYsKJfkPw//r6QPaCfjxC5ubpv+ViEBWyx3qTwFEMVO8gVT7KbgDulik1vOGA7u
5T5pBcAvrp8UWZaE8PgJRRDhdpRzjQjjGADvwm2l4Hr1pix7iw4tsP0aAZuveWX8cImBLJ5XtvFp
Z/xcLzolVBXeZtstHHQ6X6SiEN4VXJ764sNMNbAvS3aOh0R/CTEcof2aZGTPjQEwDmYy4CcXKE3h
26HGFEZaFItqQ1Ww5WLQLSyrmOl32uC9/2neENmmjIteTsJsoAsUzrlEYssujLNkt94ZTCJARxiO
j7sZck3UV5I8lf13zHST0oavgvk8nuhI7E7HFpod3XgwV9F/aUowbSRqVWJmTqI9jU4yKlIRTxr9
E920ZDEj3uEscnakegwfGFPRWZUMWWUj/zb/3iwat962tW/WoRj68Wbij8ItY1bThM+tnjGAmxeX
3i6UloQazMGqfr9dCcO5jDpkr9iAtKD6F2qmUgaI6oOzWPdaftXFdjZbUfy+hKGbWd9hNgetEd9A
F/j0TBogEDqzvVD8d+xDdE0ic4raRDrYZfRdOESHVpaev+KDP6frJkAX7x5WAy8GX4+JRQQYnxrp
7GjFZSl7uOpRYqr84XHnfim3nXmOmWrapjiwVoRBYRkAfXg/r8v3IuIdqu2BLL7yG1h+JSiAIFGq
NQIb0ICD//dF6g17jcIcclsvjepa1DPNaQxnx8Ab75f9cBe74HaUtdrPDbOwQXf16u6RAQahRJzI
KCwHJEZXSzDbAAbMiWTYnvvHhlf6XnTBVMQi9DMgGNoYazYdtnIhbHAGpJM5jIPJXJcNPTPZ1GVW
fhzVZBnhvjYmt4FWRN+OxcBAlD4uYxRJ4Kg9G8cT5ZzQ6rBlChBi/zjcQmrlQ17yb0GZjJe0q6b+
NTuvFxPeFs5bW8cxIU597WQbSaZFojvbGagT7T3M4vOcpAiVccnDkSk1KUmk8vqbwbIAfIPBemf8
eNDmHzUWpWnmf4080eleB2AN3tzooEJoCB2t49xb+RUU7pl64vuHcY3xmpvtRaEFu9vJJRRlex8j
KF30AWpqhE3jR+Y4JZDZeVT70vrjmE2PzR+NWtkwgNUj/7b9wDA5w0/FwHI8vYcHkydqxtErFZg4
s6kU21rmZyA3OEME8tihsFO0fmp7Rx5XS7lmTZRd47uY3t8Yc/C/uNe1XgnfLXEfvKHcPytZfgc9
u1teYjjzIypd+RhOaijSEL1QkZRS1blMR9DWcNnyZZiL6tXIXHt6Oe1m43p0L/LO2nfOPTFmTkx8
wEP3vnYN8ss6nn8ddgcKTt/ceYPehQBOmTSJtn8h1Bhse4AInomUOrpuW7cz0H94reSt4oA+0bqs
QCekieuOL7450iEepVuJ/EnuP0twBs4L+voL0pqxWuPElNV1lcPqCHkM67wtf7fTo1Sgxu99qm0U
PP4KDo2zPkiPNrdASU22S262YOxJFrw7gN41nzqIpRgrv/AYA61r1EpqTdOyHIS8g5SNsTXdApgf
gY7vl18kQ5lia/KxeGMcIQeVnCtHRBunbllbqYn9qSkerwH3CcaxdrzoC8xjHwTkx2OloSJQ66vf
x3EdMeuF+0WxOj/l8uwfC3RmJpyzYHrfouVLHhyUpFfR9HkGtFF/Zs2v2pTc3mvlalGpUh6aCe8r
8YXhX4e92K8FTwFI3enubkhYvqo6nfUI8x8YKcGnOXTf6TqLJVw83IKmX8WvCAS9A3Suvgk2Zbun
DKkGJis6+E4FpDSCv+N10+EZ0OlBSX2Zzf5stb77Hsq0MBJ36W8RmBj30os8s82AV4hQFyWXFVcP
UvtHxiNaf6c6Ui6ZiwLGWJ1jwmMUGmXjIakbwDkTUGdy+fEFjwySSzmkHOmy/OZF3cKkG9Lsm6tk
n8aEiyPvnKWpb2cuNZTLMuKK6UrVyztvq0oVjIxltuLD1iBWu5tZluAhmhPchfE20ZyEwc69XIk2
iC71oWkWanD9N1HM3YzTnFhdAZXl6EUg0HQF3NHv8uF1LDnxqnqVxNFuA/RugPs4zpdeJ+owOV9O
iVqpeJcKnKjeklc/hIHG6lRW9SpuMaOvBSFTjNFqrRDLJWqFI7uxxnIqcXtzafAOManuh778OMaa
1M24x95vlV4DUb/zIJChsufgw50BmRJeV0gmQHEGCqq65wLT0j842Ay5kmU8pc/auDmsv3MEm8xu
LckTavsxxTsCMbvMaojPdWovusk7nR37PaLDqdk8fH1ePEU4d8d3325KF1K1Idk7kPslngSXwjLo
kTFoEPlC6bUGfwlxDeSw7RM5f8sZ75StLGrLQNmCOF9IQdrNQZ3sKcrpnXwDXEvqkfsfb2WHl4tK
rtieiYYMPRiMG7p3htP0FAXpu+Pxd2x4wh5g5/a53+CEOELTyxnYOa2Shrj98ZzSkccoX4VBuEBZ
+9VOIDSlqYdtXLYvblCHXFJ2iuluJxYOOk7QZmdpPtMSzYLKzYsIJdGs+pAhqq4agIQeC+xIFqtH
JHfVN+HZQHpPsXH49UiSEYfF46Eq3fnJ6+50Ghg3vkMgEPQiyVjrljWuLN9B8Kv7dfz7lOk/e8wU
WIHCPYAp0CcblaBcsmMJBVvT6XOwaeaUeJalDuOG79p2c+I1LG9WHOXnOuHKK2LAsy6ddBRxoymO
SzVDNZa0tgVgCjkWfoMmuAGMfbwsNSXO5tMoF4ZcPGs+ct1UZyjivU8n4ioRQ6QA0xv4XgYc7jOW
Yd9zyw3eexYsTB8JjI0b8GQpjtI0xxcJAXEln0WDVSdk6OJ8PPb75fkH1m+sw53vG0g+XMetydpd
CHTb+2j+ICOTMjW/1ALbIBJJtG/qHgNvxQpXLn3I6qKxsglYipk0EESlyvNpctgq7LPHSIwXD7bc
ZRY14D6PAGD5KD3FIgg3VMteRgJDrrxn9vBHXQp/CZ/eSvWoRLaG44i/ec9lp6qIB2VJLPVtV62+
Ffi5z93xd+LZXf4n5Aue/K4YE190DBbWfjhy6vPHxjks4na0fasbC4bHKdXeCGGqW+MG20AnQkwF
ZEPCga3YefYOYi0Effsr+2zX0mK6fHHUBFBb6G5Z7JXfg4XGVVCTFF1izGfI7AnBQIzTGHExEyLz
gZDaUqGpKUy0DatlYWwTIRFN/bNKvrvLbd/kO0d0kwtqum73z5NPW099cd9i2s9w8SuA/X0N5rCr
DuXQ2NPB7b8l37c/1WSkdLEKhdBrKUMejYNbQQF0zTH6PSiwHeiEKP6acbc3HxB9H4rowHzw5bfV
a5KTpMrRiysB4oa9eBwB0ZJuj4OPK4g6964wIXaOAt8oyO32i0SxFnkmcSp5ar92zSqWosiCm9dF
wM7If+pbe2Uehqfxhgx/keTUcZMIAK/R5IuCYb0SXqXlfLin2Icoto3hF0uGtracOueODV1scNSj
gA5hRCu2b7DarmLsMVK94XsoGnJVUPpatufubgVGyETAq6g9upyqxY6F0nrekhLA6hsso8iARdo2
85YhHrR5SOc7lOZx5lJfVRPmru6s0Zn4mgzCYYbAw2ZKNONc6vb3UamAr3Ra95MuXxGt/b3jqUK2
OCvZLh7qlJsvst1ORf9f135ExXsY/BUncOUxHQYRsRblwktIUDDsLG1zHdrvjblBS83F6GyEBHOi
lZYoI0bFRR4qoGvai91FCx9QtjBBt8jVZbcEBwTe20JURn/TKbZ+45AahGhheC63ZVcPv660bqk3
X6ppcX96J2g3taJCwPiXhZTUcyyfebvnio4rEVciYbBHwFvvsix/qNdl3Pvlan4VWyirKRworsYL
j+LJdXsaeGdJUIIqivpc0zs7CKlIeaN4I0H7P0mdvuiE/oOe4BA5hILSc+XKSlSXBO3xOkvSrybC
vf8ygb6sLbHhuiqfUfXitoQPvy5zsIZISMLms+3epZor7P5UHqDIrX0cDaRFWVcYQSSsRbHvKuU5
WvrospcEA3banPLAVypqfrOz4J2E5emBUcKmb9dk5dCAru0EeyIfeSkwCEWhn8V1R+8YuE2QNLJR
b4Bj3PToy+1Qb+9Uh3p+3AU/841b3K4Tfe314KfNAfqNbJHL9ngJ9ljvPYo+J8KcqC2EEVWo0npd
2YhGgbxjlM/xomDvoGgLhFrz3k3fWHUkC504CFTgM7iONpuTVaqLkg+BAmDgbrW2RoDNJAM6A5Nb
jjqlJx8AHYg/mbA14Ow2YVil6ffk4yoARjehHtd9G1Cf4eq8TcXzDSCXIgCm2lrAwcTofVB41l1F
ULrG8gnowgdYbPC1vRrVqOeGKl1uu/n21nV5NqSpspP+mIlVOxon6IpeCTz5OnmxOoJtP4GvXEJf
awynHPCKaitM9R1JYCMzsmLf3YVzF/0dkz9j7/bxC+vpYe4YXrWqizO8z02/OQuKyY5rlcMqFMxm
KN69HNxPE7GtSKub88XH7jWeAareFG0byOUJNp3X+v00UtE1Izt3n27aAaTICAlmWjCrPyw1v9Z9
Urr/DuW77WgA+9sPFvCR+zFmrwaO6Xznv6V1OXVw6HkHDB5VgPZSZvcb/bvins9Q+W3o7POm6EiR
lpAEL6Q/oUfUQSMZA6io06+cFaes+vPn7mCdOhT+b7x+DDSjzBEkI19xuJcZ9zg23Ic5VFLiewFM
rmSk5AgGtMLUguxEsyjPt0y7mFjsQ3Ngsgj6rJI0G9Sj3CEtObdu20E50C8h/N8SISt6dE5g8Qmd
DJklsYBJTMfGvl4WQCFO7rTZ4sbHF9OzaKf8LsusLQlVqZc6T+fvae43IDp+8oCz3LQNesOSPVPv
Sq9U8XChASb+KRaVBCa+JtwfhJ+XLtCBLFzuM+gHOUHKOe8VdcSz82Zi3DCaBMF3Vga8MewgN+61
YJnPI4Fuowt8K6rgxj5vTlq146CXYP3haag2oLXg6SgI6QoFmg+QfLsdJjCr0D6oxKOLAt7ddvMY
7lsqVfXI5FiMLlg0BTqi+bdCc7D1kItPkYR7PzxhogNrOhFd+7DxjorEI6zvanGRoaJjvDyN+FDu
OEiC1CkbFUno4KUjeLwupiFg4Mih2FYdSgql9w/K3VI+pccfzXHEjd9KKRbnERU91qRVflbifPa8
fMuIlfYu8v94AYI7o6uhRegZKFjrk+U4SWD02vBVxnzYK+PbwKY8Q9uYcJbkl5neahGGUI+RwiGm
2Vx7IW7RU1wNykqBufCMgho6lwLozlcQzhIiQRigtnkwSNtXQ6L6nl7HiQfymywxbr76Dsm6lZRC
KPnCsAmJa1fHh7JUbP5OXxgNZE/uhXmEKk24/pRBfwK/VQSjKB+59abwM60USHFWYT2RJAYqtXlq
r6oWhJUc9q68D64FAELVMagghpqCFNg1TSOUJAos3ztmPVlWdUHJukgvQwYvEEDVEtHX1j/mkScQ
ESNL0eTPTMG0UaFXFqaWQwIkkVaG11hQjPVMuDzzRn8bJwYo+zzUQzVLfn9GQpgd74CmaafDvbtv
kfKhKoOLVeCOSsmgzn7hizIqD8n3dPC84DpHU6XBNUVPKjhQaBJ6w1CrGllrNtFVtRBnxP8bHrCy
d2Jc+ceHOLOsiZo3uRXf/uMOjByGrtC2K5MreYoIrGZaEWMyDbXxCENKi7J8Oj6xP24YC2EhhCPf
1Q5ESCn2xVWOpt2rQlHvAFEyCodzEEZwKYqdARIdHux6vzCP4IQ+B96+YREZcSm2K51gWVzjMwh+
AI3fttcXmJ/Dp+fUgqWrTnKioGjJpmmVSbghlapmegtPDLLDw7eS59L0r8kPs4hvBr40dYYCvxk3
/u/wPV4QDGgFhNeURKQgbpVHQQ0UPHtmlj8EbWro1F0agQvIPH+829MbIrOTufX0MZNTMF/Dqlah
keGl9cRZB1EK8PAYKEbxaK6LfgB0W7Zjzka4MU0nkpGpxxF72BrCieL88JkuQyaKaemN7cSk6Wnr
fitSWTlyZZ6OhHlUOtbPx1nar1gaoVxU8gmqukyMHrfjumdJkzFYy/47RBoBUKMkE3rN//LXTbNC
s583Etu8H4Wl/pg+uouoL6Q5hkfdt9JWbKvuav5HLyOmPhapac2w/rwmHns/2HbwydT2VUhREyEz
o/HS2E+SmKL0jVwI9D0uimU63uiCi3eJuWnEdpJ92wg9jJkxpFxlo7kUkUFQ3/Mv6A8BHebsP5n9
XBka9THZUKBEM+j6dHdkRviIQBFqDqjoAMXz0y4u+Eoluc3LPdu8dEjLOj8FO+Smp6X9ZqUxqSzC
QpX1qHaDi5uxbJrpDRlgBgymA87PeI8e1bHwtdFwPCHwHz3oOdS10Tw6EpzlIIdQQfdK42CPyibg
TXUJSbeoAWrmTiITdWCtOwYsir+4Q+1hVEgI54vpRQzC2BNYaBEflmZMNBFogLBvtJzRsm2skb8L
veu+hEuyFL7XTJyYA/CSSjev7z6ih6nbiphkmVqBOPYNRCaDNFEa6xCX02RSSIu4U7QdgdsRB7ZU
KIyyD4WVQ0Slv6eYPvJdGoAiTxhO6KR+H9gPtBqKcqz3BMi5w9TwUSZk5RYOho06eC+Tn0W2Xlnq
rmtCF2uZ6x5eUQkKrX12YceuON4BfCHrfr5LxfnGPqc3/RxRFhOVmJTMQap1RE3N0K6EswjQ9Hbx
LU3MQKq4xHZG6OHpZZsMC2Jc6WI1Ho7l9CImAtmtDjGGvuBP8wyu4KHVUcf/kBlgfNRUIYVAeSmf
mdgexsrGKwbly7UC8leyI6NFKJZdDZtb02bK4rxefbyNrQ77i3vSD7vNOCGW1Fn08SzdeehrT6Wn
oZfsGcgCYlcbGMOLeMzE6V4gHkZmp3psHHhL6aju6TTwKXwiyAJVaTpe8sU8cQwfrrOaMm/7F7bs
MafAe4jVLsgL6rv8CGlqTZUOc5QL5dRQxRlZ/WNkkrqqrACStuZLWtoZ0hw/WiSzbpdm/vDqX0yn
FbuXp/RktcDpOaKR1ZRoGI11CbbSJqiRqQsk1TUSGBb56ucU91BR++gzGWjmOHmdMUnaQTb5zfoo
hel02FZLleNDQXDLw738JsQ//a0sfyBe2mHSKxIaHHMWXQrId/2pxBUB8GuRJ501jBnLFtBAHrng
86lthQzd1o21Oka05DKUDxlScE7iD84FNgqf1VRMAx7Fd6w9KLgWH2No2lVArwuug94j9AV8VwL2
qcD64PBngYjAcKdcVdgAWezOBVzuajxrLTPPd7e5dcdFra803zfAmllMZ5k3XIRPAxVnNYfr+cS6
9HAH7Uoy3Zrx58Z0hF+brWGZ1KN2no+EWbQnHk3ahmsZzUY/sadgSrS1rLHTrE7OG+sv9kYu4kAZ
uKET6ZXBJXkn1+3ojXUMvPi7PagbSi0NS/A4JVv6+qiVDi27iE1XxS9Jn2uUUFyvOftwRWZQagdC
+lzHgR3sd7HUYv6t1/aEX62TwG92+834+dOYM+xwNcKOUXJ1DIyt7N69NBa2X4HypBq3RCKnLX5B
wIjKZ3YGXIEypg5jjRanQ5rFjDEDJ8AkrYE4tz1Bgi9ufywM29X1iSoxRWT/uFGVIFAb0xRLA5Nt
A0QrQx5yHdsy1V1r5VVSjXIFgRe4tTPB+xb7OX4WCfb49nX1VSDB0A2DGTTLnGWueL94XaHYn3NK
QzTtLgA0su2mNDE41LetUehNMMfXD1S7Y08NwmsDMq2wGwcLxkKBjuNLb/kMJ6L+G3IqXcx/ac17
ZY5nmdzWOXh/9N+XCkVy7/t5W81vOq1JAKucuZ6Xf/tH37/VlrThdHahuNL5neFOjTVwWcs8EDgy
zOXJRcNLhD1vJcBEz8MqfqaEk267kw6SqjQ/tssFLRhYHlPyGpMHM/nj/v9XGzwTrUjrFnrVebBg
CW86JPBu/bQGVwnivKOvb1pWkwe7eldPLc21QgenbzvSq6HTICo8gGzslE+OB5W3zVi8pWmsxlck
1GsZkeA5sQ02CbMFUHvrwBGwOPkjESfwR/D3bCSZdLnfkRrG9tJy3cNfW86/kRThmsu0Qlt33oDy
PGGfgj6Va+/LyZq1gP0TDcG/gOolKRPHFcRVAeQAY5gp9j3nI55Yq0OZntpSCwm5/pKVcS/mv/OI
D2VOl6SECTJIGpba5WXT97xFDTTfSsrYBVC37Bv4q6UsxVZRuDVN+txvJrK59qZahe9K4dUnTPsh
eHYYoX5LPTtm8dQiVPpRAFC/Omlu43NadlfkyWMqjkqv/NwLjHzcR7hCKELWE10J9SfYMS/5pHdK
auCWSEgU6t7EC3lH/a/k0iYxw4SS/zX4FaA6UU7eaCDYrN4htFrOoRtrKSOs/ACGhhVmzTXnxe/O
qD8kR4BPr3l80wAFWLFMvJFPLs1JifRcPg/6xFXECWUm+juURyk5rXhfkZQyNR2AJ3VwjiutlieI
DHLxQqGaBC4Xm2gunK4cBiydwhN2CHAd3eJEEE9uLIaWCViN6N6L3l1y300ARqf3pAZ5t28Q6uZJ
3RjEFyc1Nh3zT+LOszimWvYRFpo0HafwtG1Wy2FFPD6jrGaoaxLZPvedvldARnxZ9viFV3wOSIOd
yrh+Vm1zCwlFFtxHgQOJ3PmHhMRFEO+ccnxqngNbAl31fpRSGvqGRTa4Ozfiw73V2prhz7wCTMQt
s6YUn5du0EMvsprWGuDdWIG10HIPeUomx5yejMZjIEIDSNVNjU7EQxzNeEZZfKB/7ptPGmQmQxk0
H63RRXLPiHMOYNV+ZJPjJ6/YOe1lzQofn0KK/HGkmhFubBl1dhaGsxeYBGcGkRuWaFEX+dLPKRwF
0QClUesidR9Ds6Z4jg/glUog0OBZ6A5mF0Y6xpmEq1vRs3LZPG5H8o94csHzpG2ex61u5mXrqPBP
aTvCaCfoVk8/MuAt6bniTcBqErQZ0+htmz+0SOPX1G+muh+9Mm1t40kxEXhlaIi5NmdI/HE9Gsu8
YYdRVROY7iWjxMN2zJXfmOlET+QmM7Gio92NoXf9wMNBj1hq6WhG2ZO/WJo5QgJU5xiGfD01zo4D
2S8TGHLxo9ax5mA82201Hb96yXtpCeYTmkVMmVp0FcgVPB9ISy/LzYuZPTN4a9+ClFEol032Mg05
oBf5Fo5jehC6YILWlyonXbWBdldvjtI1niLlDrEg3OWHkxZELh775a24ME/PDkAOjeRU9qk1zc3u
0BvVgD0b14mHRomVUCa8Pn6VNl9ZY1QfwM6hxKRjZsPhxWvu3kAiXuc4GsHmT+3tHZNuUd4VLo7t
5ICZNBJ8ahFsdO8SP7Yj4hojj/i/ohLQkFQqb4SX7SQiYud0g9x1zpI0zRvsP+YdNgb1mnltkHc5
ZB8+0pDgnYLbfLo1TssIyGRX5yVJdeUA66oionihMZ37vOQczCx+g/JOboD8+KvKwltEVW0ao++o
OH011/6Yz5+nimKAyATaP1uOWHBQSJ0sNEkCPMD7HZ8sXDEyJhLWh7DU+Wtyk7sFDO43rWm0v6XR
CG4hkd+kje+699otMLyvn0ZggU8t4c5lOY+TGoJqoYQDpUmg2pBGEpnR4Q99NCutwu6cH4NOQm8E
CG1aK0JMTF8VpyVccmydanR0AI5mcmp8AGHNXhCvX88oXKrHfn9JY9kT50Ln0AzntL2zRX7z+xyN
w3tu+Pga0fsuOvrwGZcNcdEGAvJUccunzBBiPjrYYz0xq/Ypzeb7uW9fn+o53hNWpKXyH3vixjUN
r0IFcn+VfEx1HY37aHuvl3wiZrxKCUCZg8Sh0qwRPcRk1DTLJt77AgbOCV3VDcZ7LqYvw8a95Szw
7C9uJhsIFRiVRlMabcjmKpr956ySKuyJyOwNvttq3Hj5uiz1Z9OZingWRd4nBD/NDjvgGeVGtxXq
b9Uz98KsN/Z7G2IttdAqQdl1/YPZlUVyWivOtOPq9aWrvry4IlbUpDak5evs/5RgiWOee2sLsDaH
MQHAGGe8SLHzFub9iOpf9rIPbBQF+/atHrQs6xO1J5cI5cqtuLA9A4kz8MnONZ1GVQbsDbyX/tkS
7BQr4ukRnVNxZgGAtja/p8OEmAOAs6L0ffy6hUmMKcAISIBQ1PWl+/uTVcuntISJ0RTWL2isFEn/
0DL59nWPYNJhAeJxza+hf7izwyty8srWx5KCR3ZYPyxp3wMFUYp2yJMinURLvzepU9DbOw1/aSDO
AhFCRFBknGyJrNp6F2KrgeVYFw+NTVgEoMARMOOgEa/MDGETd2SsxU7t2nah1X08Ut+aOII+FiOj
yoxltG3ovNA9FsellzgtdfBJBpMQe97mVyl/h3dJqXg9X8MuqBoKpTnWcPn3VZ298lNVyS1Or8Dd
UoDAyusb+kjutNSDswe0NdkO+v6bMIUvavWIaulECA5+xrOEGVp2j0MYtt0pwig6cnhOXSPb8fwk
HXtjYtYR/FQnM9A7xiWOcb13voQSt7RQ1aDEhB/Ty3K03YupUB+66uv0vCLl82ASjgeyUOF98eVx
vKltF2lkzcv51j/LOsl/UJJkMZb/t8BcuCtz0xMRC3/QWlrwM6cCFnXThmuOCqcKEbznyrINLwyz
22IfuUfz1xWGE6NZbOaRgJrJqsspE+2kL4LiCe+5Yot21ro+Uk3R+3x9/AcBQwcuWe2mS00YHrul
uN9015ujIi1848eetVWIobKhr/cht96DMOZMh3POeyiLFMHJG0uMKyV36eReE6jKl9Rhq/ZNQrRR
2I7IkZptNRJQPGrrz6cBPQbACG/PBJ+b3p2XVhrAT/rilpqGAxmBF3KkkJIdM8oHZRc5PWlGr8br
r7LJ2fzefum7ObSgBEQO0bu7cKJeochLZBn/wZaHeZ1qFn3w19nC44EWAhkY1s47tUnOxRPTIyxZ
f7Ln5EXDw2WErLwpZDfieEpOAlf9vi8B9ohOIex6YGemh28D5bCdNAF9MiJU2AOCqqQiBeSMHZpR
vSwOyBJ3/5CHtxvKr3lZ06QQajq2CyEOyx6mfGwZOnUz2Th3sn4EmD3/FCHL68LF/TK/RLwVSkjq
1B3auODyAG8N4qXIkERi1vnDOrOgiV7BW7iiGHPR6d/D+LzNmjxNuJOtkIHxG26FxLcu0CTxk4gI
ydn6bjanvjIgy3v0qrblZqJO22Is8XjydU1PxWqVvY9aKChqosQXWccGp8PCP5kt+osLHNeexIN8
Z3gtXGdMCqeNzk73kMSpgVCGTr8OrNzHoe/beu/A0XwiRSubfywDmJv3d7g9YmUixUwnxAaIhOa2
OOgVmDE4cI5uBtUIb0zN43rMAXkyXqRWedaU/zSn6//bCM8UIMh3vCGIFQoeKL8yROVm78TE854x
LZGcLquVDYdLf82EEKv3uRV5j+N1GnI5z1qlLr0x99FXZLEOrGXMoZV9HLsFxGHSaBvEMUGfMkCf
dTz/MhByBngt6P7y6OvnGWqNv7au8FUaUMQW3aa9E7YYmfOjQZSogj16nvM1thoZqHWhbkHUDSiD
h1+2kHYIeSM18My1qKTdniPfLHp1jG/x8PmHw5jpXU45tZ/4w0EHfJ5yB3ZJURbuXYDgUGMNVpEb
Mt08SPv0UeQDY3PPAmHsZIyi6UiDlXYpC1aisjj4Qg6cU4pV5UP0XFS4vdTyhWhPJpoIaSOzTcLl
8yQYYpdJYAPoi3X2mS2jl3VZdAkboORIGU2GCiZbEFmHObvidOrnUHAXjjqEk6w88ldW6jYxG8ZW
bYHORkW1CJcRJskVkBU2p94q5lEvFLMeGX1WhOSjBP4fNFSFUFyq/GusRNyON9x1wXBOrIhVTOCh
aDIcRlnAPbhFE2AIeS4sWXm5foPMFrG09HK5x03w9+246cvZBc7xvRfNSfqL+ntIqkLFmehfZpyf
umkvsaOwRxwdyL+pXo12TCEegye8L6Jx9RcRYjqPyq9COza5wCGVItASmIu3KJ9y+AXHXDUhVxRt
oR8qmxRCLDj/nfkCpEDoqmRHdg+8DYfMRYGt8xCr666Vn6KuMptTA3oWmRzvT3WoTzjU202WswEc
JmnS48TKaQDWKjCVdClliSf9lxsoDYGjmlgmkF018bvEO51+alVEQejF+qc2r2K68Hphm35tcjt2
Bq9iMjedYyDH234LdQCtuncuvSqd/5KG2emCcx1skZOWrHjYl6ngMoLRmdrITspRxJ7rtiYZgQSs
V4LI1ZbEpbrty0hLM0boRzebjdwwn4+n1f+tN/0WkhqL6zdKIpH0rbnGRjdGlDUdJuJkGj9xoGIx
Jl87QNciG5NIs6kG94s3dDI3s6YdEsPYoXHrxL3VUZcRGfAYnsiVuynhDWlY8qXlZQreFoeCG4I1
W5ly7MPr6bE1EPd1+RloXM1dD+N/DY6Yed6L0SqEwoP/C75XxGsP16YRlrm4KlJLf/WuEZqJxGmD
ytv7z6ATlYi1fo7I0lcqI9Lgr4OJEWbUvE1f9VqfLjcfL9isXqErADnSUNYo4w7qpyhjkpl4+naz
l68e7+w97qg3JCic3XNwcmxp3NwpdF4dkAg5Z4RqBoOzcTapvWU4DsQKc+00MYv7s7ly9hzU93Jb
b4U0wt77FkIYBbGdVVMzTHhBqFV7QJ+Q4yOZHyjBStjVXtlyGNqmcIa2KZi3zY0isPwby8yhOQPo
xQXSNYtd5KVhznMa3v7ZumcKWyx75UJc1jfOeHw5Az5MSwx12et+WeE8lDliXgLM/nrNuinmbjOh
DAOzNJIlM3laUSkFf8Iyz2dcXwzjdKqrrU3lNnOd2vrVveXV7JqM3o/FOy6p+tuTjxyO1fV+1JRH
7kcyDWnbS8a2S2+c2kGM5JOISbxdfzBDOTmKicX170mef63Tf4kX0l1EWc4dwp2KIprlgWE8dq7j
ltsUYfyahjlbcWLKyCA4x+WDZUIn7G/7XgDb0Kq41twq1Woe2NEedt/qYJfz+Fbn/ljnVNEBZGio
uLPBu8hYgbMOej+WX55Mm3JdkAP113BdZzmwKXFxU6tfAb19uVMZpI0zgMOfIJsFlRjlmIOEF99i
z+Q3qWap74oAlyuuZiQt0AefHWDvhYuprAePjlVMP5i/DNs4TfUn1ivOosHqbXSRa++y+5eUcfU+
HXVIOPQP9Awqmu+lfjNLXF9VimNEmbRJ7vOj4ScJ1EXrQLAB2sBlef1JFwUfltNwKqTxQ5LxZHMd
8lyPHj77r2AA2zpG9g0KCTOk8AG6t3nAXD1q0IZRJOqfO1T5C7wjJBhBTctUj9pUW+JYLh5OQQS4
j4j8T/LspKPDR8dJoUbRRFJth6Bu7Zp3uBhdz5AgHnqYtlJuUPi7sAnQPZfQUsG9seNEiCAR5d0d
dEVXzUM1A4OiHJ6/i44bT4ztM9mU0QCFn+oBIyZMLEQxhSXLqniS2l0ZE7NvQx6XYiYiMpmav1IJ
BYF2XOntPEUspKTyFfR/KLhjEgGvvmYGzeYNA1C9bOadGtemq0wz59bwoz+vUJrAhaUPoUDUHZEU
SOGwZAYGJXJnkSajMWn9t66fe91fM5CLrA4eUz+zUg5kCQwdRyiooBcUDrn1nwRd3Uv3Eu4Q35rU
GxfKkeZn7DXHyACyvK4QQ1blS+nUbaFN+GnKYk6+vDxrAxoTnBOl8j72JJb93lOutx85TeDSL+y+
UoRFUVpsnEJFJY+QbIYVsthRycxKpuoxq4KJ3oUF6Ds/9FEDCZGPtqjw/BAOyOimoN1Y+LPdTzRW
hm+yFTLTOGHxFodwnzm6Er9XA3XCW56gvGw+UNlJ9RXRr4JXECxlXAGyyshXaq/701rpwWFtsw7I
J1zWHWluyv0Cb0J25wlF5YL6apyIhuIP/zPmL44WwBUwprQQ+tRM+CZvmPFIVTa7zE1j6xoArOzA
5dP3wy7F83Xa/gYTFPxLi8rb7gwUoHbOPTMVglcIfkC6EWB+CAhAbrJMxA6XPtvjYVfdsAiGyz2N
/sZtqRTbyP8d/wfqsuNui1afCCxllbijtZ2dHNRCN0msZnqmXyq1tGLZ6eBLIoghcjunoFRdJEFe
76PP66gQJye6vX4t+vFCPV4RTLP6RX617ce0vE6ODuE63YM6a/NfWWUqPyBfqBbBjykxE2B8EcQJ
WtR/s91iwN2X7QOjxhCcpqHkz5Fxpz2mfn8IpFiL6sXg6welkh4EUdIG7LYX0DvaFHAfOS4W//m7
Na7zQxGi1PiiPGiylXYZDNdcMEkU2PFauW/zm18eLlPI6nsgQumT7jlbFQtmHxD6aJ0PjWiSd1mj
vUUYGUPahFspT39cNI7H6i7FQylK9XY1QJgWi9jMrWNOc37LNdW22CF6bWMLA3bvJ7LSvUqOvk7i
fZ2gPkiimU+FKy1IhyuRT8ZeiEDuvvexAJhC+akCtI6DkE4mQwUB9H4rlcOsqlZZ/LOfVKv7rWV5
PI8qnezHHrygMa2prlq+qrr+s3unGGLq/m4f66Rz3YuLwOCpe9vQns2LmWYEfKUEdiVmvvsFDSJM
7BMNhGAZ8POodHKZWhcveVz6EaYB+dbaXYncDnSAHLx2on+4MNIMUo+dVIHLwRdm+6GOMN/wj6Sp
ou8qYTmXTHl0hSAIuA9wW4gdeyryo9+wM76lqMTB/TCwcNealZf3a0jRNvvhXIhgy1xot+llyhOU
A9gPwCUp3BNvMJ3ekir3n38l1JhrOd6ficXuwC/xe7OdjlGOz40HhQNIbEvnFvLywUC0PbS+lbTZ
g35fkRBmCqapTC+mxIdYd2hsonJ8bD25mkcX4slb/p8k8ssW8Br4yNtKeaRmo8nF3VpJ4TQBlWMS
7tAIZXXBZ02gJe6ilV5BplaxiZkLY9slMBiyhYEI8JvCyeq+Kb3G6e7oqCCiXIEf2fKpCaMBzNkh
WfKxEf6XwHKPZ+2OpILbJqsI3XjOg08tsI85Q+zfJKutyRsO32QjxatatZ/CA8RpANnL2NJqsAV4
jO2xz6a7mkDsWpENOQNfY8boS+oNfrmes6ik8gUkwwFEnTQpKfjmASoI7lgmooTfX/qiXHbIUdxE
C4S3TrMJ3I7sbTxZEwAzKy01Mnvamf+aRCDNStBtjj8wDzwPuwMT93yv/kx4pdDD2g/UDXV3fFI2
Irl9NK1lt4AVFny0nFhTz3QNwmO46y/6Od+M/koLsuOziLCHyxJvztLAssbj0aWpZ4KPSNYNXtvE
K3MrXWgfriIxeiS/FNohwK/oZBkKu4Z9pqU6xzeZJJNV/XQhDgxS/al3kZzZ+rrAG3P8MA8La29i
wWMwVOi5jvSP0+y/ufk8GmsIH+m3sokVEtI1HwJVvOqwGm7goY/Z8pxHvpU7+Wegt/GPN5JS/psH
JM5eYxuNf5J9y6POl8veGLilY69Z4QdFV1mTPixA2yLQxiTMPPFR98nC14mawLK5Y7CPqtDlw+En
LOZZ3oCR/DEeDhUjXNHXbxskRSK9evslAXJlgOHGgzIkfgN03goAFCpn8YYzOS+OCaumol486hrb
Uoc/rZbju/vgZMxLMZzbHp3mBZNDVYC6jzFl/kdqlPxoZv3YgcUrudsIkXxT+AplYmeaKIUwNRrn
IFNWjcadddeu5k7sbXfa3Mm5mBNXerFhh/5kH0yp9vhSPSwmydBHUQHR5kXKLqhJof1DvXFqYsK9
6nIisHO9xcPxXTWEgHB3PMKPq7MqQ4USJI4Y/JXriC+55ucscEHVZgHN181e9l2FV7UMlL8pgfel
XLvFViPvUwIpwTr4uPRhcBKJlTeikclZRzCGt0KoS8FOmgIIFywagxQmGvynX2Zyl34TfRrvus7W
F70+YCkixI1F4Ae0bOcZhOJPd1biBzdfUdTS0ilH11iG7HKg4Fu78uD8c5bQ3U/7iWfP5M5fhNtk
bKC+32d6lNfOGmPP3PPVtkvVMOx52wdIg7tROePzZLR69DckkD1alC9RAeRJ+dJtuNZcjxxh6M8c
pTrAO/OhpxM85BdppJW/g0/xWceV7n9lvq9GT8sNdp0NdFQio3fqUvKAVcwa6TU52tT4zQFrsYQA
l/h+WRY2q7Pm1Dp4ZmF4ruIcvAIBt9kjVOc72xiGlRHiawNLviR2yGzHf3NyJ/SAVxCE444S/Zvk
VKyAh5LVHnZNq9+bQTgz0yfDzkPIcb0QcOVzylA+0wyNWOga+0TjNG/rT5f4tpGa0sPL6+Ve9E2i
5Qjtj/fSe67hoHhHzllhHJoRxogvkMAzrhMgjaARUZuii1+G/OO6O5vLj9ywH00W9d8Wvp93kdLf
scYvPzt+4s6dclIKODSIPF/2h7i7G4eTtiHvapwVE+xhFpwJtVkxk+lRZGR2t1FU1YTpfrzqhHei
0hSiywCzElLzbH12YoE1cZomhZPWRTUnzjMP0zFehwtl2jhKNMsDFAsH06V67nimsXrhziPG0+tR
jy9sOs34qF0Tb2UVJT8OJILuHFtUa49JlbNufGBOjHWNn4PpmQDwia0lhI8GCiPtkyQxwLJRp9p4
6hMc1Xoe+qd+wK29Gd+HHmhKqzJi+++9X2V3zgekFIVhiLqEoaisdMgYW3YruaQdvVesa1h6bLBT
Uws4dLanyzrho9paXwXsrpPE/ARgXp/01oTEkxua5OG/UeyALDI4zNKP/uosPP4etJ9o1Z1UtZqz
785+XZb0sjrcBzkTbA9xk9PziRlETkGtz4+QYbeeOWyl2w4a5nasDQfzqYAraz4QfP7OHTo8Dacx
CXJuuoi90qA/haMbwXzUkwzPWQbN8yKhTj2sbt5RNT6yITJQzpL3xbYFC07A1cpnYMADBJMNLaLy
asJwEI2wGyfUNhqF6btCmkKh0DQkH31HKSv1DLp85ZxMnXhdyLwxgdIs4TxDH3x420LbFVvOXmIB
ZafSDblWh3i6Q4WuW8FzLPUGYlX00y/dM00ej0PHCW4RBurvIpNxRBC/cp+5bgcu/YxqQdyQJ/UZ
oitXMnPpRTT77G/AqmRx2fo8PQb5dAULxB/fbrOl/sQRbz47R1aFTwZV1KRBU/t2eazQDqsP+3hM
vB9ALuS9SRPJ+FNZ3SSOMflAYH9+NhsKS4O8D8epXlF5TzMZO+3OGwWjTCzF5JA+T5kglo0mxnJm
EjPdmcx9puyb2Uobcb/HqX2yBe/eL1U1Y2pZTQecC8JtirnrVBpTh50Lm2Fr3TAPf0SG1vLh9MuR
gpfTKru9y78pUZs+H6FoKZ8zJ2fLfWKnKGL+fxbRdLZ2mSyuzdEWUp8RnXy0oHXeVejrgNIWz5j2
qL+cPvvnDN9W5YTttHfpweE4LwwfojRSVTabZNI2z5TLFF44I+sIuiFmEgscNwzs6xlBTx57xfqc
JXnVpKNgtff8x8vHeAHWJ5T6nLKHgZ36ZYG9s/x2Bb0BLsuRu3kSYflaVswo6FChZfZuHLbnJQTo
TncKKaqyqRiDGucVeijZ3yY84lTFNrohdHbjnTIqiCOlNmsh7msIYxzHjDEOCmBAMxCs7CMzsXKh
NuxcHfyICZ8kS+RIv8XfzC+RjIiwHdN9yuA7rXc9JpqBI8fqkMjsmagAspSFRyQmcER5N5q2H/xp
qNoly5zwgUHrJ7tj76yVpmttpSkph406VUdFemyEPFbCgCV8gleGORgvk4N98X5hsB0OJ9NhvUxJ
aEUee4QOehJRmwk8fLDrKvm+y1we9oJk2gJmlwVmFyniAjAxMbcbbEgqTy7iMUGaM12jZjY0R5El
LlGgmuloSEAY+odvMomBC5UaWdDtaJDYQ15CYcyf1VshuWxnDzU/GcqA9171glrBXddLB71Yl2xK
ENi6hTFaJXa4ybzs6dB40U/2AbRgL8lvvLCPcE4VHjXt22P3z9KNaEBMKo4vawxS/zNroWQgyisL
AJmJ+iqymvAlhiViyHikheSx69bdWZ/Z0rozeoSUbj7kAgti9zeZzXT6z/IpCqaDXZyeCiGPjMVU
1nQP7rII6ArdLX6zyIviKA7uvB0uFyxCWKXklDXYroQsvsRxLooNMsVaMLhDlACOc3hVeen6Qgyy
8hJn4AyK+jJEaX85OXFemDQBll6DdZr+msRFkzqY0gYYZKVcB+zsYVETmYKf6Vove9Qi05Fp7OtJ
zVv7XZ5jOyup4ecXt0mld0cvkiNfIc4RymLunPMJ8b14oEGor0CeKJqpn6f5IB5AR9VVNayi2P+h
lc5zBEZ5SaM3/Cj9sG8ngBB5BhKeYFyKoaoZepCNN8xfm1ULUPCieCHSzESBJCGW5KwtWOLslZ1S
Tlt1ZLoBWiz22pARNakm+r0dMN3K2jh106twmRFZ/i3DAGq38YLud+fXO4YhW0+FgWuoBcYUvQHl
0KVzJ1OboKrQS1KNmO3aKjgxfKI1E4hZ5pTku8jxEit7xr0BXI1pyWEapz3xwsyajpgTfnOU8TLv
jITMzgvkuujFJVWZkKYjm5D7fW6mUIW1diXCiLVzWoIvCPzxl44/0m1dA9PTKwk4XpSwN7p2atKt
ZZlcoL76Hvx9blq4MPGQQjf5srYmC3SOYBHyMKzyTJjxljoc/2DiI1+9rOcYe6PaYgaXJAWgZVoW
1Vur5Z9I235CVaghJuGnuI7POCYUZy7jISVwcMjd4MNgOM1vW7+59GZTV42duL+cUys2g8dFDWXZ
yEqxWpU5Ty8/jMCR3Hkoj9YsmyLc9ZYs1mORbxDpvrmrNRRX/8IpAWYwxlt3HVNYNKW+n9uhbCJQ
b+JaTmD4DsLVFM3RSglulq0+6ZrQGBpG8GGJ36UtCvuEQd14IT0yf3wHOG/3IAJwz58VWGmJXAuP
gHNQsCI5oxKFZM/AG12ERouJ/OSB5zdw13q7txnBXo0AjQhUdPuSCIqUmpKYCsNio17FzF2JZ02D
/cpDavh28l6QWdSfl4R9vulRNxPF3yGg7swKXJAhnh3GJvdfspsCCLESQPnYBWh4aGDJ2nUqZgsC
LAwDuU1BDsgZ0rC3IiX0fdTfiRS0KVUVYb6ImjMxHw+JqHIfMEKF86v++/v2pHD/H/yT/uk/FmU2
sGUyYgny7odoqzuBRONwZ+Ky50GqWuETkshrC49fkRHxuNE/kOav7LOVbjC7SH83e8Uoymr9Wic7
AOuiwVaej9sw2iiiIEjNy9xLq2oVXwGufD1eMuHgM8J6xsVdssqZ5f1d0PjDKP4Aqv7isiOGdqD2
DGEPT8IaTPJpXSc9DY8oyFRiL01k+OiHi/phw9KE4TLzKHvm5cgAlE/so5k/fdMAr3/qqC0KglIJ
L21DLc/aVH1R4KU6/uyP2JDDHscXknOcWDxe5JEpT2H2GOgxpfLd+IE8WN9s46vJY7WajxEHiqLi
5bTAxc/6YIo209MGyTDKaejaIUUK4bEr3IIoTz891OULM3l54BOV3Jh2pGC1YrHGBYqI01TAzj9+
ios02ZQnT2NPplPUIPG2bxKhBe1W2HJpfwKL45X2ohDNQxGifEKdbiKvKeiw3G2gbG/wb1vJ5vki
Jv5FreE/96rrIlEgvXdr6TAkkxo6CJ42E17S5bze/2IgkbC4z2LO5LRqXjGXphx/jjfJtXcjV+Ob
+LkL3077hWOopUOW0fzqXS8ILoPO+FpczJiEIL9DIp0+PLMyG18JDDNVHdljpX5wIGQGPs3Mb0Ge
J5T7NUobH0kN3gVw/JDaIKaYSq/GGe+49vjUbtGLK1PmTNJvry3TRq9F9NJu4SlGZOG0EhbCHMJs
FOk7HjLJgHbNj7pEdvYImDaFnAOG/dm0G+BHmvCbxZpYNQmPNWKh4bQWE32mdQW6kiW6s7JTmtC/
wlecRhmDR1My5RMQzRfJDbMYbsPKsRXYqxBUNg/cyXF2FY4ELntwSnn/Z/uXnrrTTbiBShpfMczL
zQYYa0sgbjbhoqjAxVv0oxwPZk9viodOw6VYI99SiMPEIQTEU9X6bllKASlgF/DtGoOi9eOr+qC6
1xuZlKp57b5fHoS3HFeDEveIb5xoq4kjjiAsEisKv6MnAUto5eC+CV/WRPW0FPj6jdedqVQN9Ca7
WWf3OibRTiXo1OFtPMgjR7QkjlxM92kB9wBXje46sO1hhfE4prW3CxAACFnaHgIc+NiiRN24Z+IE
+nqGnsRQSRuTp17eC5pa5lRCw2FiLeRKNAc+NrwkVDgeE3byigww62u42zgFCTh9+83dRm8pgNjb
aECCXLSQOBl0FI4OtAIXy2rO4Eiqru3PGTS/CXRcBJUJ8YYr6jXmlXqIGrmjQRW/uL+vZZ11RfO/
RPlv1Zd3KaHmWNnajlRzNJvPe4Kv85icSojmjbEebRi+Tewdz5dDu2HRwwDo8Fzh8tpru8hfpYAy
gJMOlJ86r+yri9cTzwbiKtqsNmQE9ihEr/DZdTPx4DcBMPhxEMvkIBMuoAEwyLP4eNzNSrYY+psD
iLdm+ZNODw9mHHPyExMCOHrTpWcluATpjYmoDIK4xLt6ZKhd5Oz+NCi+heZbrDc9mr2Cu1cXfKcR
QX2HXJfV3/u93rkRPmNOh7xrsCafKGkgcylpnjo7otnE62SXv6wphMNnKIcMCeVxvvPywtYOzoS+
vdN5X0ivtjZQx8bcyukHRS3FliMawagO+f9H+FcO2YyQ80N5+Fek4Kmcvu3yBqRmkaMgeoN9zlh9
SC0E8tNVAPIsoDOO12LLU64BUJBCHkYI60+pB8BNL6kOy21nzgPDXqsu0OiInBgoO6Grfq/a6+lB
pZaMxqNqtX0grPl5lEhBNQKX9Thjfrk5Opf+Y8dGP9d4sPCSumTmff9r3dCtwUqxsr0rBCe3MNK6
482dyWrUSitOsR8qCUz85oCmpIzH3UJhfVLmVEt1p58IQ+2mag9atCkxCkk5cn/tYwrdPMy5rKj6
+J4BSpIcu/hK/q9LsIQDx6opqo2PNELy29T3/87YJU6KiZXfCb5WBBX9N3Z3o0FDrHCbhQsvLgnC
Z29HMZ2DW/scQd+/Iw7NPrm5Czk5AINbN3qbc1rK9JPhePoKylDo24dQtD6mFrlXgVyar2avYZl4
409/yjdTVF9yHZCRi4GGEN96iZOHDfN/8duw02kdAGDbas2ZvK1IdyFw2V/QJjck6f/Izj7iBteA
d0YxuX6BRmWQT6Us9bAAhJEU8O8jT3kmpTkesxL9UaUrOcnWtrv9vhKPzhR4RPGz+GDKwoWd+hIV
+cFglTNB/ByGRc8lpZQPwcxPq7DcdpI1w2VI8fMpJ8dmRbK1c+99hvQAr1AGhSg/jH3uBjsf/Pod
maWfrMhE2D5njkijaehjq7YF3fRGXy0zHgfHThgBE76uRpJaMtx6+FGR6eSSC7Cs5uGEiTxEzKsk
wN/bW1edYT7sQ/bSymE63Gb6Ct5T11Il4EhSdBMMSoCiCo5Fs+kzvFmAMBpMLFBNEltFKgvae8wZ
v7W8Wt7yBhOtKF/70FcdicwBqm4ArN+PEID7X6uDtlDBvtzOfHE3p1vUriO+EIYkHVx3ur79zBB3
Npx3+f3/dLFbr5hvHFvX6ZefujPuFS18ThqTEWEmN0oVf+QtFL3ThKMrdu5C7m9m3nizEf4W67p3
Pl4BeZkWL65TSaCc2X7kIDmJ7b16JOdHBrgWs1xclON5+J/h1zSVNcgZGGJQ15ozBkqoUhj0kHcH
oM+HG2tUKjeqgbjvClM4m7LJetei5h4lv1yNXnfgdqNg2aeg2cdeigbRgejVUauBYZ7o6DC8XbNV
VKSiFxq5f9ATYowMDWqApcbuoc6TgH3jrFGTcCtiuQTDf8NE/bgLu2z7jkSCZ7CJ4LvwhcxCHp+S
0Jt2ru2btybpXyRUgtrZ1i7RicKnpo1WCEONYKCbbShRlFq3gfUeLcMpV7iD/ay1vQX4DcrlRThZ
V0NYV2eFIh8Bxt+Je3CmDTpqg/ovsHT6lNLXBwsLYSyujtfSBNUPjv4EDTFKOEm1pwlJF2VkFVBI
lYMXoSOQdSjRjD/WXicmjZHaDi5jCMAHsmhlY8+YDyfHR72JnoqDnpR0FGH46T97drJeVP+wVJfw
ErE1pu3b4cmusknftmbaz8Mt5NpexY6C4cXVHfzUgp2KBQRh5ar6t54BbZ6TtoVcxzzAAcLKiGP7
CLzmNXkksqnIWRqcPgFWuwAtHnBGktyqNOENehCHrwhLr+W8c83guk0s9xj16m0YgtgIdyV+gxKH
qKXRWyPBSPhn9ongeMWhIPnvva83nilUspej+SZBeF8kRionXauQf3WByD4vou/ylkvl7TK24CDB
yGcDoLcvvoz2+aOY/L4tvSB3X5NrxgiC0LaEfALwsDjR1dvFu9gyW04g6UJQEL4EIBmUCkJ/Gd+6
9zmWhoGUpLIMoJ80ZolP4OeZ+Fw26+ayJOVD+ZQzVuUOj0VIy6/+2Dq6WEM+FlZGGpmXENdI47Wr
OA0Ji8JLBxcedK9psGdETn61jMVs3jQp2Y2sQhHFt8rlwq1VYByp58SQM1vtv1eMQGVyTYW7bc1w
HRNbtANBUomgk9BQyPTCdC7/wZvgKVqZo9n4uhQQ7VIZehy+cfM6Q+MrB9MbfLHXFUrHlA0ZsrNM
GR9/9FenL7fJIZoqdg52JqMlHwpPYXlBJSprwM0BDKDZu/0gGKcvj2/b2F32HS0yhBr2UJOAuls3
rEYgztc0dNb7YSoaCqClp2cWbZWgJyMjgWzGzX0gPmtI+Vm44MWfkCIJwdOKfDxhs0Vqc5zFKyRT
hp7lSQowvr7y1fIigu9z7cdMHBm8t18q5cuquHeDuCD2tAMo+TbVoakS7N3PG2MklyAwIa8jzsz/
zHPIe9KAHbwj61pgMj97vq6APPj1ssZcWS6V6LwU5s/qwRL+XuqX+fX6GamsXT6kCMVD8M7W8kIN
EWNBSHLIfjREbJrAxSu0OWsPkUmGSqS8R1rsj6a9BCkOXe+4MNpQyLe/XeRsY/UQvFa35QXYtVTY
55joOv8XxUrNu5VSHIYin9sHacsivuM1SYxQRW3QRKxPRvJ6HQhWMc1T2dMl44GRtT5utrVhTbNl
7zWLvyvldNEa27cmtnS/DB48YmAeBWggXTQjIaCHasjeXHMGLoJdgJrgxnKRYDPg2W1KLylgyLue
VJYVgRLP6IV7Z71A2OiJXz53G41Xw8pron3w/E/SZxGlwu2x/aLAl6z1UuyLpTDoQcXqI8fNY8S3
gEXTauaQaqkryApSXycXVRzVdi25Qi2jCrihbhUMgn6X0xT3Gvv8NgiFwQq7qsFVQTXliMJ5g+ei
Detr9kXRKjf7Pn0Ga2jRu1bggkfy+woN56LBoJrjBB1cpI9ly3sCxu78IOzqGmLxFF7XRLpexvXm
MW7W066CNkw/BRFtyXCF6h+OkBZ9ieEk9LZNC5JRHwpSt+WNZAbfBClZI1dH+K/+ItPWn2PasuSI
7JMOfMiCV3VPkPFuV7Wow0fTFkR+prZutj6dVG63TqQ14FEJ/yFj3n15Lh2fw8sU/uxg+RUZO9QW
x0QAuHt6qONgSVZ6sBhunzCth6gIqm9nywRREzlpQEHBJf6Swz70Li8fpC6PYKS+Sj3NLh6thHdu
Px2W0MS4THjq2u8WDIC7auTENUt22dHH90Y41c9OhzmzZfCXsVUI+zqUcE8MWyN8OwNbmUnRa+z6
oZZ50kHBHJjfTmWGQOWrou+qgPFr14/b7JPgeOzhQvFBlrfKTeJLX15gA+kCAYcsUc3U/MbtqjpI
jfsEbAMzoEytNDnvXF4dfbLvCEv9fikCs6qjmzbWPGiom+FeAW5cIgvfe07B+K33Sv5lODi7vfc3
Zsw7fyFCaheSVY0wsNw7pDbHmfcaLpWznmqqwuHPvCf1mqP4rGEg/6aQ6sGaEdTBvjL6DKS2aHNW
4y/bSt0v040uPk97tYFLQLUqVm25VK8fyR/UdVaPjvjBptn+Jd+9wJCik97MTw2lOCxvTCDs3dt6
YiPsTaz9HwZ+OT0VzwFPzw96ueCRAMRM4+WJAXU0SXudFUeYmGPDRUw0psC5gG4TZpaA6Ent4euj
P82Au/Rtk0ecmAPXeY1dZxU49d5fjVLSn9a9j5H5z5Xa9bbR5nZUvd93BtFclsJxDvaz6lForynZ
WewvB+51HC/HH38CnowDhUcV+TWGstYkom36axxHG2NO3wn6PlFoQBelBsyPxw2LU7VaEvzx1eP+
cGyLD+24Bhgl5JtID2kpSUi+lNNOUIKV7Bkotkes8X09NjjsJob3ybpB4UkD8tViJbZNRbOmAiUN
0Ef3qmDru9xqzZvGO7Y3yK/1moi955n0zTeEn0A35sC2+H4BT8IehCALfCpnn1nOdJLslzKE1JlJ
oylCNPuaPJD8KhYN+fBDvxmdok1pPv0VT5apYD5bCRjIaUJv1TKVFqvundL1+97ad/Fakmu77KXG
9OkhFuVJZid5yTho5lwAvAmEuGPE6KBN15pjd4zMbe+XzdPLUa3vOJDPH477UiFRDtupYpV6wcqW
ux1we06TiIBMic48TYIxCoOBRW0UY2Zv1XwjfOVjErwdsP4MkVhfTRFmQqn3HFekluXp0R3J9DMb
iCfSW2J5bqW5QpyZ6fBTQqQHxTBwYPJLk/kvmOHYuvPAiefUlW1TrkJiev3ks/HHnZ8fzfyHAEs3
ZwVZdwKZCOHIDNX+BsFdfxQ2uTaB3eSmYbpgQtMqBkLRumcqcJolhlaxRfn6HxYmug+ABf58ERlf
Aj9W7rbaYOTEb5cFyWT5J/rjwicFLK8zeBtEQprOgxy36QxI42W2LClGmaHaz5QA0/doCOMOd94p
ifS469mlUc6/kHF4pIALrmfKsFNHVIDmJRXUwFwmUFCPnltJ/fy6wtQlMnK/M9UXxA92zfnCAIhF
9UjI6lL6pWqxMKrfzShf5UfzB9ls+XTjVOOPp58Rhypv4yVF+qhWJtzoE22chHXlgiA1jFbIvpGY
cMRcEQpgehLMVdDiTFlh7Kk/xUro3bYW7boxPbbhfVRKZUBbRJn8ABI6nka1LAJ9oMa1SQDoJqdV
VrZf4U1uMoR7RAymWUReU4qBIMlBKj27omEjZQKGbRr0sdGiVzgAkHwKc4MJ8bZjRsYg9uuQHMcC
ACApeagpjpU3trEnzD5U1QkmQIdUl4Rl4MzDgXarBh+4XFLobFhkrzm9LdmWJLIiDvymE0Cjyqpr
Jyy0Ij/ST7Bq165iXgHgEJAADQwIX8Cchc0U/BmDYIpYTYunZNWrA3rvu/3w3L18k0NF9KfTgs5Z
OuSmqSOzXMpqsbOZaxV3wi06aHibOQPDZyT4DjFliVxi2VACIX7qgJEqMq+sKzIxCDzwJxPpwa+1
fzwZb34Ts0uwrimHbfFHQZ6UK5ckoa5FCIjX0XilwNnI29l/czgCCFiQPN/kbeZlZqWRug0ihjAq
X5lNMxHwU8Y795Ey2wP7uNJGfMJ+3fmpENpPK/xAehbLLcz+vNS9mkFv5+/sWePOgjn24fpe1QgN
NrDUtMqK0vyrSveGu4wokhWPljyjdvFY4+zscO/DQtF7sMGXOU1UNOrzLFOcZhc2O8TIUg9X/L/z
0loiKGQEMbi/POQU4XEdeAA3eUbp4OaYJdKqZs/MMP1EuIBHlxlrbT0zXN/V/CfFF4R0Z96BFk4H
IEtlw9E5Gck0UcYvxJpD7NY7uc8LU3fkrTJeHX9yOxfiz8Pf+og8NtI74DeFhdlQhx6Qh0zr9/bV
HQhbZfV/OZ/MooxMKMY4ewPyQFjboQms2oZ12HdgWivFfudpbpHe5JoKiKmCBE67cD4V7qvf9VSQ
qRc6tNVDO7znCYA+dxAf+52OiQ0R0ewCcjuLoBb5nTaXVPkXf0YNIQJ+FNVJv83j8YHSxosu0pmK
PVs0+AsTKHicDs3Hn6xpZ4VmxMWA72XCUBsBemCpT2kiHt3g+3SjCwt2IESgjsQw1T+JEpTN2frr
q6iiZjTihR6nNJ9aq6ff8eNrNknXlrQP2QZXHxPH+Oay4OzYxoBJ6UweJoRuM4KXU+3hUAQKhipw
t8r8L1GqMX/UFvW1TG2o3traP5ApeLiUCysGVNFQEsDb3va+3yU2fXccpil+XVtcQqjT3X0poLbO
CPuHQ+laBAez9ssShWaR9sEBluCmdGL2GJGW8jfoyuzq5TblS8yVr1COMCAIuFt/gQzIfRUp4z/I
TgPg/J8F/nKOMwgMtYOOUXVnnsGM+zwKpmBkmFLkaHo4uz197etCj/z8fneUgOgYqS4dihrG1cBH
984jf9VXCpydr5TH+x1Cm4tiGfATjm0+stqEwARdRxLgiiOnGEDq1sD+8D2UYM9XWJNn76rjN2Hr
GxnVqPFR1I+zOH3Qf/Z/ifFD6R13lzsTwTjAZJ5tXv6hdqg1KOLQlJH1lAaMJR4svmi+aQnaMqlc
y/8XsjPC7m4sMt38cY7eHZTphnFplYnDlocSBwWVSJJ3kg/j9nDbObQ8LokIjvce+q2uMD7d2/Xt
JV5Uarr+rvuLwhPPeIGZz8mIGH5WVPQMzcDI5CWn2U9zU+YtY6GzsRuXE6FdoLZXvqlN8BN3dWUo
Yhon9EKeNdE0UxcGeEXTOhz3Gj8+h7npjzO4c89yZxzy0CYqyiDayi2lCuSeTEo/eT0iPWfWioBe
noJ5d46qz3nDJut1OvQNb6lMpDQ+nWg90TonGQepY/He2TunvpZnom3cNDD90KKIjWAE7/Y0ZOSO
3PTuf8/h3lvduUdGwyCQUdgtLbY+dfYvKY6StQ0bTr3UXyVSfhJ44V+MTtT6+sUH7+46JLYG++7G
1BDxFUgybA1EXAYa7+s4Kz33kCrHmw/WqjMwihbOmno6xtelD+Ypx6rGPHapDiDlWirmbppdMbFy
BbzupYowbayCOJRTmoPDnB7r5ZFP+9Lx08tmwaTxt9BN11KKzIJZYk3lQSZlH+28p1TqnN7l2BIy
RTuWcFOclnhwSR2w/XHiWgh2wHphvbgrz7kIT+cvpVzHwWH5c0v0935gyZEXbwxN1NYyoz7EyWC9
AvjUKCl+U/XaLmsVUzw0qAbzN6fXqT1PZllwn5o+TBvJbyIrCZsHpeN87Xes6Vy9KvzGDPGpe3FE
esZ2Fupde6nbI9g4YhnZmHOuJselEI4SK5HdLCYHe4EVxSUdj6xmQToU0F+IzVl8EK/jk+urqkrk
lWPSbRZLU/UBjVx4sQS+vxD7W/YzZ3OIXSCqvOjNA5Qbp6H5+l6MP1ZlBSmT5hfeotHg99Dzo9XS
dA/bfgLVETtqtyu24SzGpKStGCVLAyfrvW3UcT3bigHjVRVB2ev6INvUVOW6iab/FWFO4D/OWJDn
XERYkTdHRfyvk/KDnG+fo/4wb7ngFkvcNSwNl1UEJooMFRkMxNHMXdO3AhezjOCaX+vHobHGalOa
7ij6TsSv/gyKIdSmpQqjr6VfCn6iINRCBzu0+xrbFLLLevWhesi2VBu1d2G63RCxAyRhF8pCGB9T
A1Q2uJn+MvxnPBIxL0tzuLpdZO9QVaL7npLf6VDX1N+6OkBGPMBf59Jv8TUS4T15C7BXVOuMrf8x
cfeYBY9yONhhd1wuxKWdjNa5LlHvTJJ/vDx3YENhadqA6p4O3BM3oaaEkwhZKGSjodd/eBA+AzyJ
BtG+Hr9wuCt0TuWrQ2m8WsCu+t6wqPi8qSE6cVYlnh0x91ntNgGHwUQGLqrGU3BQX+1DzomDIg2W
XT0C0bNl1aU3axpeQE/mHw+Hpl1iL/p8d0LkSsdwyuyTz0p4S6qzVlUpYTm+e3nKwRZbYI93KZAH
Iru+wtRw7Ni+I/F9NbkKqv2sFVYIaBvORluZV5wSCmP8YXl/7qsilyR92TfQI0y9Rz6yWLJkpQNG
7h+JS5IcmUrPzaQA5MviKCft+ckFJYU70K6hozJ4PZdC/Dag1mKX/1EcvMuv3lRGQdgGoqEhb6Iq
HGpErp28XPsVKbw2QAzKvdA7a8JjuF78+TkZBQKh/59EwTOUNekSUKEk2T4FFgjkjRbQSrhVJsGh
JsgbG3BlLg4359IG6N2D3IYU6WAgMZF8H+unCZMtdKUORA5d2WIMhde2wfKzmGVHuLBvLZJrw/+i
SE+pSK8tRP12EsincePqfRDRCBu/MM+oIq6W1iuw+xn1R5QLakCjHTYOpIunM3arCfaJGMktZsDc
BS/Hbhs3uOwEBmMeNi4fUspZgpQpl1g8VUirjrGbsa3Xr2mvZOft5A1aDXtZEci/2jjkalqEWl0P
VECIEblSzVXbmACb6AHmSxki8hidQJ/l96vAaK2V5yEx+hXp9WYPb4ummPOSRLOp676vcFn6K95e
hey62cKgAx3Uxq5vu7/V1yOVLill5ccIGYh9Sp36Zr9/y07jT0LKe3v8exxp2/TiedhixerWCWDc
J1AB1FcIzURawi4DfGLQYKM4JYBq54eUpUtaWZebhMonyX2BgzGt/neRibxsStZsCZZeMfhklJ7i
dsVRH884CKXnRWM7aGEm3A6NL3uSwN3BwuRIDUHcKd1gZjKgv2iH9SoyAvyAwejb3vTYEp89y8wv
mmvrPjpgMaQcCAHPp7LksBJJXV0BLCVgVw2eMOBnSnEewSKEoFT9Z9/fp+M3rNxsIkq2Af6aAw97
r/01kTPwjCC+FJfJnmXRdQxtOVzzMcVjZfP5Q9yrnrvE/uyvaBx7SiA97ee1VS3PeBNLFOxWJL2s
GVKZiJpdS0X9rvHLA7HaHshp6P1QbmIyLVJnhHfWbd5rKR0EdLZjCkAJ5vJno2Bu6c/rQZR4z+my
mPpJlu9mtIKFViMhZtaWPhB+qlcB2M3MCq1j6PL5TeCLszMGmZfTe7nUweCmReRSSBecQdptveds
RjCHwg5rvyUuiQ0yOFznsX01MSr/P1P/qlsdzOWcDTwLnRPq71YZVS2Yos9DhmTfsJqnECf7E0+h
ivWcptQ9Gt2sv0akzKOTZFABJ0dVrBrFKUH3dDgbQYrdPZDlYLt5yvylsbDos6Oy1CnACJ6gp2wH
Gw/13qZ5goyDtnQsiX++UF9dbCu6TDgX/aYXPRSxde1m+CamUdJTUXtcYxVUVGa5ODMi+VM6G/5g
8Fp/QBRbI8QdKx1iTa/WNctm/uSNBqooOJ1uR8lT0DSzFNZ7HIhi1qe4PKjTEBODedXNRVCGW0gi
iWfvwNFI0LJnKHbDq76uW4kxA+AUYyvIhik31kTKxQJU8ecZ5g9SogfGmie34x+8sbdMuyXt00Tx
E0usOLzQgO6ogk+zgDsDqKkVzKJxaPnczaYTuOWSe0vOCoau+qAVr5FsV9gvGILzmj6WT76fZU0A
MAc81Wjv2po6iyVqWmWzqH6Nrk2RazlRgdp+vhflHgWCG9fvbDwFCzXS8MUCEt4Df3nWoUjUUzfw
U3Bhem26YJ+t515TCyVyoCyQsGRwsIazAawQrFmieImZAMgZz4V9tLMLCpxq0rNby69YmV4yTfCC
KGSm7f0IOMF66dWP9SF5q7VvAWnmQdD4BhOdiRJEDeVOv2AnQg26+dv/iaRkCa//pJR8Up5+cdcJ
ojYEgXEO2IrYDfuOV3AhTUeBbhbyT4XpHBYfNIktgUnTADeCkVhZTVhfO1GXcNE/KGMyhvU/VXuE
C+poYBTpMOGFDb/sug59A4Fgraw8m2JV3/S2qN8i89VaJUssd2UlQd0+qvB79VhIdOcbQMKFS5jD
X0j4j1JEFM/PN10H2c5tJqcA9QlL1gcG526mtuXDjvHXfI3JdI9cBBRd61/imb+zJ7JDQ9p/qCop
fmaD6OcitTfAwTanuCXorXOwEUQFh3Q77zjsl9y+ctlFjRAjmXqrJIKhXPpI32VnAs5VadjInNtO
1gV3ygqSyY+8CSzSCo0GlMBzZAuVkOS6IHceUHlrRhcYfcAIO86gQzVsniSblhIm11Bmqtcbq3Dz
c9hVTkN1Q2/ysMwL07YlJwcATJGIfihRAoWDW6DHyHunmMCU7isawx9Bwra8P4uBTgSBHqWK1dYm
Lb8jcg3fYsxiJde+gmI5DtOPLA+Iu9BQzKqiyOKrmykZegNGZWsSCcnfluWyEO896FxvrP0IYPu2
GCHSVp65rrh/b8KypfWWlP7T4mX/nZrlQeik6PVkTnFCh74yghxZf622QYqqqvkp5CnYunipvQoF
Ak61Ptwx/x8bXHxtGDfm36PHSFrVhKPPkshdOL1EXq1wa0p8YCbLDe4atf4ral5uDd6IZ6tVv+RV
cwmKDtd658d8fgkgIUWCKcyYXn39GZIZLUB25suK5zUK72nVpXs/nVwqknGp1A3azFxEUawaeOPY
l+vZL7rnI3yCx9BVOStOtmsiZ/uXeqoT4jeS2EeB4wKdlFiG0VMLNdvXwh2gnCd9blvO1RTPwgtL
ppHIPdWWva91FT/X/fbP/F0/lpNGUIbo1POQRfAbnXjxQh6MSiQeg38+khRlcoRlGpaVH+THp7dk
LzDAaa8OIkg49B4FmsCBDHIwCTXYT+vNj5n5kLT3Dm4HuhbjO4kdhT7xZzrxdJ6oQrz3olALsOpb
9SxDM3CcYJRyXCoaMTR0PyU7vaWts0QzHapNZ0Ui8a2MBBw+3jCkvcRW7Rx5wicjmCU9DK5KbFgl
antYx2VUd3POUdAhSSSjOmObmF/Na+uak66z07bElA8Y6W8UglZ7PmC4UFmQqpNpJvX5ME90NOTF
h6kTd2ysB6Mn9++uDiePk7m0xwpFwH4Ft3+sN8873TO+lGLB+vRwu+hTHhqiw//k02cPuswfoPEQ
jqM2tNE1q8n5agZ4XK/aNsLKiLksYaI+xZLW9wo6GuK1SxsOR047kFz9VibpDQbOLoN9lA/cyuKG
PaCo8irmP7Qwe7TCfLuaoeyuL17f5m1pOhJJlDDfz1jJRyJpHU2AXIZYv+sIxiH5UjQz+/TeSFl+
LuTOYr29drk2/tR2yj2eaufxQ1BifmYSMD+1ip0id/AW0QypftLpOChBaimXn9nqLOtaTQeGKG4U
1Y49P8mCvAc2tD5c3omCb/okOjBZiOVGhAe/SnB3E6Nx+HgK3D+bdZuIQfmffQGvWK6ubEgjpr87
1Wi7lh35HVryWZVYRkF5y+hxrXAi7pJECXz0P6N2BFjzX1zM1sFcmJmm/oUx8BcvXVVTF1R/UFjd
jFu8d8zfqzRqVvKhgBNPua/z43QTDwW6YnHQ2NaCeEXYlfM2GuuYYwxNbd+H0W0syCudIQ3AFmoQ
3DbKQXg4Io7MJSo0stnwVChyrbOyNyFZElvO8rm5J1/edqSB3PzvAP3glqCxuOguJjCL7f+irEcf
aQB89Uho/L2xzCGHDTpHnTGgGuiX2B/5l8xNcuvNMQi/fgTs4gFOpUuGlegd7xOpG8j40jgxukrs
/DiJg1sp2KIFSH2r971xm6a8ofQEsfE0unpWW3wKV3UlWLTNUhDNhDJP27mPEdrLgY3AQ2Awigbt
x6sQZvhMV9GcEnpH4Zyv2VFhhK9kDUfoPSaDOLudQLicsDzIVBbvwBDAjQBHmC4hGimO7b0wzxmo
ESUdnQ19QWDdxoQS208sFAaj8gSDO5VGQu+2ESoQS0MqJoQ7nLEc7wHHUgOBiYuJOxfH6TyVLF/U
E0Vx5vxUMrI1yMNpnqORTteb3MfUDTxQB4NworS2nXdVtjJ0X6yN7+zNVtPViqi4OeV2oXKEToy6
aeKGMM2pC2oX5Oljjqn44uiR4Gq/jkva/6pGfD1MYVKUYLc7ZgZxyCUmWZ7oYAnqzAVd8mRW+xJX
+Vqumni0krY6t7tnusHCsI9NSXsD/Od1tRxhtL72PkMYVbX6NuQQQM61ahvxxgfVqRyboazBYaV4
qMU7Vlbrt2lOpJ/ToZ2C4cWofPLHxe/lT7/62MbwCGfx4jF9dESNFbY3xK/ljiah3pnie2/TUc1W
p4n2IcZmML+u82+oSyTUG587Cirasv2U5xIucU/Ml5ss/PR6XGbtljTdkCmnw8gQto1mhdUOTDts
bo5oG29yTnq+NnFX1tY6dbcoWXtXKDj/7A6OsNLh2PPAwxXZukpMmZIWQugFbcZ6cchSZ2MfLLYq
U/yTNkoZNTpAZ2euSjhY8KxCtXzQPEPJLMDCyixg0zPy4OT/ev8Sc0BGN0VvG5SxPwfW9mfScdvE
JX+n0zSGPKA5wfChvcg4jindces8LQTMQzwcDtM2jumeUAwtdCdi+9PN4LELouUzYmKza8pA7d9y
mnxuomHK4/UhMkv4XO5yTSqRmILOO7mo0RxwRtjzRwp6HiS1EqZ42Nj6hJvFZ6F7aMWQJQ/YVy7T
Tyncf7SAPbhygrDnvnbfPMz7KdhGP1MfgIY1qixvEgrU2UugzCz8RJEYIaGbkrFCmjV2FR/jkMXN
qeuFaci4ezUuQIW8pUBWxagYBv2ijnhWbE4830wLhp4IA8afNqc6fPHNb2yhEDzc7S/rmDs9fZY5
/pPe6aMNvI+QPyAqDzElQJ2Zs7YSxRgve3d9Gn2+jezNb7FKNt8aQVQwZD2scK7146rI0EW/zSlD
U/ed8lEaNMBG+C0Y4N99JWdD4IKsJiR48uTIBcn0s2Nn72xpYst8RgMJnSmsphzkw8OmLKw9Yw6X
i0RUL2qN3BuL+bzZh0ZA3ZZa8p3PoWLgSWNXJunzF1BF2fogwKVAlozW73NKJz355skqAXAiTgZm
0/i8WGg0TY1uTWth6GUpHD0cr6BSdfVuhmE9SDyCgs+C+2WDIwPxz9xs2boQKQ/VfqwgJiGh3+mT
8/O9LFS69pIidtipDvXypfXLgEkab76trh0Dss0LLXyWIPv1mkn0wCjCMUVvVCsfndM3vA1y6L5A
RqiQSX58N5h323/WTTmWr8FhFmD7GK2+NvmyExo7c7KSInPYL6GOA6WQJSluoNO1GxTXENikdXpf
MIZG4mGSouwtHo4XjFJenQIx5xMbVAWsVqU3PA6W06+82rp8iSYYGKuFKfiUv05/pdk6Oibyk5+b
NAzfTmC2mL+GrFTwlCtTHBHb+1QkHUXNGk4q4uZ4XlIYeJSp2tb1b79TP86yXwsFjxkLuAd/1IEt
euOH+TKQ9H374ODD7bPu3VqX5/ByD9LiOf8n6L4bxn4Jn1aMdbhDioY4soWo8ArgM29Z2rjMeYLs
DPMQRdPQP7ielww5Y2ahMAS/uhq3FzMNaZ8AmqMnX7r4/fwuvTVsgvdlEY9lN+9pCq9QevXiladh
Jo9iHR0nQ+gP1k9oOCrPzAsNJDqZvSU4gFFnBKDWxml4iWhM8Jbnysk+CRNE9FjH8a7a5ugo4BYP
s/2H8Y6t/Qx60D6/bVKYsPCe3Tsa83tAlProYn8NQYzgBivIiOMSrAMITaKmhqWaTAkDHz+mgZpS
fN5ApOoOZkchmOW6xDuMmExrUslRhpWxCCXH5buFxUnSFXh1sGTLQu237OEt36iIjN+W+6W4T/4V
FYS6bJA8LWUFinzSEI92cMCVu9bFcjvF/DIYPmvpgDDilBhLnKk1JjkZu9sL7KCTv0vLMqTTcoHK
vjVj9FelJIU7ZMB7deo7oDefD2imA/5WgCZ5Kf7E8gaPWHn8fzvTGHddc1fpVlYYc2+aARKi2Gtw
I8AYcEqF+0x7KA/lVsQs2GRiNKpAvhs8ntdnGFnFRekYAz1VbsTLWxcVrrqlEtwdiLjdIPQGVXiL
s4TQAWYfusp8uXQI+oFC9QfMH0n+0Spv5FyeFUL6NQpN/3qJjhIRheMnPSrjYSK2DPRnpM7VNjxe
BtZKGaJhKRWweJ9/zVbh30I//qAxcqvh/5fcyfkzn+VR2IfjM3w35yKz9MN2weH0pgDny+Loa8vH
mMIYLczXR7nzj4P7N4mqSfc2huk7diWftHNyouMEkPBfNKV50AgJCNvZ9/8+WlIZhUA1+obcIFpH
DnQ5o4v/zu8z8YjUK3yRQzbi3/5etu8kEDtPwVRTXDx6/QxNvD4shH3QpdDBQ+90R/zeeztOwfdh
XKQWoiz3UbzoRXguVfUDspEtslfTsJoEqSRKdsQ0wBdC2wUUvCJbwpmrCLiz0zQEYwZrqoauSyyk
hQ4/perOi/vTpql0YK4kDZ7NrXietfKJe16OaMfdYJ97/+oBumwFPOjR2tDbZf0zTL28aJcIAM9Q
EWPRQIzhJAHu4KzO3gSdb0aiD/0ztAElgVL8j+62vxjhhNC52ZO9iv+unQ53ToJbttokE0JGPej5
yTDdtPAOZrbydfvhdwaBXUsMb4RCR60YuYaBOXPzLejrRMzj6kIYsSsspQnNaZKQLMryiWIKwt59
PLJR50ynVxOHEcOLybCCCrU94Pxy0hDB2M+Y6yvtjRIrH3DyQlg59yuRlFvPvcJhrl3DSxnfqBXn
yHOJ5vJ9TPvncljzCUE0UECITr5kpmvTHUGmur2K3i509JTted01HOnojbmCJaV4JTSBzhoSbWah
1c2/suXafOoftSAM+YMBqKsGKgLa4nQ6FLicD+P8v26apXes3WMigxmfG3q2cY5qSmYiHwdu0xCE
3EnMaK3SWn9AWoFFNTXhIT2ndjASP3JOwTVUr2jpkgXy69f4B+1bATGl8hR5d5fB2wAFd+M7059v
MxQU+8tkS8AuIdf8Ur3HGXnoqm/U4+6hgGduqdZteyaV3vmSIiH8Y44YBjwZR61x1gidsj7r6U0R
vQdbjMz+JHxsv9YTtowQYi3mtNa7qtizUvhW/iCH2GPIE5MMA/8VWEyZrvkVdM++l3US5Zwa58se
/6cS3dWvuBRvLlXJ6V+pT4DQ3lTCIPO/F306Q9uM1p3IJ8unH+C3wID3t3f/Sloqhb5TmfY4kdj7
dIgdVT+RIuroIF/o4lFoRyx/uIADnYEdg3U3OApE9Eg/2sf0el/eDgx9HZYYSY1QVHxP9iCaImnk
gMgjK0hOUEitFxqGU5EE8r8kl4gvhIKwTJqOFkDllJJCVgnTiwhck42/w7q9bgNBTb79OzTRxlSX
Rt8EVEqLIplBJeU4sEAmcu/qD1+HdT68PUehUZ+TkHlygja0Z0ws8PrzNrkG+PrL+F641cx6gXtB
pJQwwI78QNUmlWJKcwBVhkqV9BLSGNxEoJO77/ENENBmO/4Naedau1tWdW/yF9KTeh7ztwCpgnjk
I5bb+u+Tat1znIU5zEzAtRk2P7uBrGJgfX5P5OQzwFGkc70EIO5yu2tRLH7ke+G0IDL82RfFI2qN
xXGGxgOymL/8ACiZPE9H8ZoPajTA4/z9PqIUd4FBC1GXHxajyumlxfiKHI3+x5Rht5wmXZtApQ/s
VOMZed+B8yBiYNk1VvGTCnAhDZ0czv8SAKH3Kt5PS/gWekMQgICHicjZbD6IsJHvn+pQZiOD4z2D
2H0Ig4+P7EkWoN8iZP6tBVWPoy1EON7oIqa1KBRLu2i/4l9QobP015ZP4Qy2IR5PqwzTddKE95Un
Us7uxpW0O8BerQKgq3vw7nHdq4qR0C9ptl2eIN1DqsDWWmb4dNsEUufb2UEoIe1P7CHET1ygynss
8solHIgvN8csbfljgWmXkcC7ecLjwfDmlbQ3jA0X1FcpZM//EE0DfOoMk0qdqh7ZkArnKkikeB3e
9hFSvZWP2bET35emNsUDlE4+zMyLxsvaQE6432hSzkrZ0qZJSwUEbrfA9mxVCUIP310oLs5LoH0l
JrJT8O90fUH26H6jTm62fPq/CT72eI7PcyWzNYm2dX6gmZV/y/6hhfK8gaqDVP5P+qwJLegM4jTv
BDNd3O6ktpH/F7AV2h6GSP8OkHj0LGoQdMC7bazBHBi+8YOV0fSu3G9DNbAIw48fAO+VukXi569I
7k7kRaIH5Q1SmdIQFLwsfGVDSgMPA0bu9V+x91gwePxdS2oMNHffKAy+WEp3VyHG+0RdRSAKekMC
O75TWZA96kh5ShZpkEefccPUTz/WwKs3GefZvbT5apvNH1w2vILp84M55V6yI+t/2EqFQoyDWEyi
NLF/WoNMP+P4G9GAPd8TLRw3UD9G2Br+Wqf5ioIOxFisXupjBINluSIen38OicqNenH3vX86U4ij
8BaRfz9lOQPsd0OE6ymqCKAHmIUMlMe/m5E6fHmsLDd8ml3oHJzwRmaziDzW4US8a880KuKj1d8s
aIa67h2IaWPYREryTrUWMXDjNqjMUvfiw/bD/IgF4+a6XQVqSfUvck4hMxZLpqshNk+ZFq0cpOij
DqMCupxgXhziF6RTRNRlFC4RTnDlo+cqSC4YE9DllwHjUyaszqD0ahaHhzyvmJ7nlBtZbATI0sAF
eQy8BzA5IJsJEEvAya/IuCqEU9/O9q7l6MkWEt0zAc0w04AqAB4WbDqaLWQk5gQ7zWmTNhbmpO2P
uz5SqJgfTVl86xoIpk7/QnQEnszdI4U/jGU/SzYQb9TyhhE7XYwsPi9RgEc5lXIrhoyY8iivTQjE
4UtQEkHxBNRaI5MKeMqswUABLHV+8K/6S7BwkRHBtLjNQsk0pB5RyfLTdTzw5PU9WuNlAcyZEPST
86bju1I2gjHPG2HsCfrrUOyDnEnNaOZ19ZZrUYYkomxc2MpBJQTU/BDrSUlaTjsbQ9qbn3AuHJjX
u/xGT/3m29UcF08akgASgzIFdI+VnHMb96R6QOEJkTcV2eBDiyIltqD5d2U9FlMynJWYJc0OANKe
k9dIQ7bFrvTYrJ/pWr6YUv9N6LtrE44FI9F8zqQCbEWhpOLyP7JFfFUuh4KY5UsGYQB/HZPPU/EF
Ierz72V/1hgzg/9G9tpV0swIKkG0pCprES39d9Cf/MpjiByltXO/Iq0J6PyC7X7yrH6IHyIvL0jc
QSwe+XTamxh+477D+htGQkhMCMj+P5QqP5LL4oAiWLtpaS6pedzlyCuLFAqA2Tdf/ldz4PR7egWB
gMPGFojOfLqT4BxDn3JTfAJERpPlz3FUlbwGAFY5hmeQ5V4epnuzl5KE4HOO97nYUa++XroQQ/fX
BjS8YDlizso8Z0Mwr8RO0FzPN8VPmApkQ7SZY7qLGBHb/jRtuUjLcidHAGCNqM816TTZnLqH8KFy
1nFgzWjqqVvqRTZGNgrRIw+ioO3TMBOzukczJwkkwvWLNUhzd7ZB3uq8+TgcWx1hbDNNbrlWt+bf
uMvhA16m/0lsE8/gxh8NQk8ysEgWs6v0QT4mcsN+xCRpqUB/w26hDJ7jWMijsySjq4qej4wftVKZ
/xxai9cRv2rqCo+k8BPAY3H67X00ZxS1ZprIpubQyFnrmE29hTgRFzyJbJy1p/IF8L7c6Dhkb8Pu
jTuvtmdzblTnOLjgthzzjJd4h9RdfLguISMp6ek1CpONIi5MqcapN4EK1Jo/W5lqJIramGPtZKNy
5mqpkDhQbr6bhAhbKcslP/UCc1DMSeZZdIkYCJ1Fbx94CMatkpw9fdYQqB9VYDu+OY1GzvzE+YEh
D0urBtNhB8cNlO5bLbwEcdwKgPMvD3xrpeMeDxwWvLh9N9g1cWdjO0Nrd8AGA3BBzUijDvF9dEtX
cZx78aL+KBbp/vZRV2BEIEZmWl57dOJwu8OROPhyS48QWuRlWr6Jl+e1qOdmxZTHtglpq29iQ4TC
VGW9Gc9aZkn/F/rIlL3g7YunNcENombwy+HI49Mq18IBdD51y6lIYCodFcyCRNzi69ZE2iGthpTV
1/Ob9oeHrHyX/UDZFB/JnyLQgpnGtd3cDuhcu0/qoIKbxLQXBxyftV1A+FotWSqz3/38TApCREDx
tXA0mUNMQ27k4K5v8tQ9YfPg3aRuPM551vAab5S6PI3HyTTdyjjTPPNJ0maum4gI0eeoKODM6VIN
64pKWkTaFXKJuIXCnfQ7RZd10qf++EImWEsuaIUwUF5CvkqZHjtK41+Y4VhaQ0HqR6qLiuAqkGCj
YzqriwtBv98gh3XhpMJWmRVIAP6ThC6Nzj6R4/NRLCDFen14bS9jGt5iE5cxn7ckF5/L7DpB0a3t
sXeUgQVs5/H7s6X6+nZ7K2hSe9BYKfjJyhb6wfM6isE5hqgdO+p21lUTL5rIwc8TsBFhhFpfYQcJ
5qH/l1Pq3D2Q1DJqySZWHfehDb96E6rUmDLCW1c4Z9WyJ58lziDHXY592DKPp5iPCwNuEdxV/xHA
LRqLNTozcECDfcc08TMDLqaCapFAKkVJvuMyMw/KsEmQ+LA9ANaboPlpNV4f4/IrYq8wjsgS8koi
2pRJiF8AJDy7eXkRYLiMp44bXmxgnClUI/IzUcvLbKOlph3ayEp2S/T0SDhhnANwnkckTbUli02F
ERjjW98GR31hnPGeK+lU6GFTCZJMOu5LMXYLgTvX8Vu7wGCn0U9xjmsuFGh822cRJ036cot0HkaH
WTTctukLYimrgu2tijkMnDNWIqVX9IGG3ePTlJTHGn8bBdRR2DoRnxKwqsms7d0vH/mbAVW/1ayq
nTgRyfe1v7GUGBlGYjGoavfhKtT3QPDkfSlKDPuvfJm+misUbP8lGTZTle8O12fPVcrxLpADY6B1
1ntsiefRbwlbofv8nNTjOqAQTNBHB89qbvrj+PSfQiWl60FW4lbCgxrpoeF3ZgfK3CSpqatrSgO/
5NCi3kMhO+6/Am4qKBqjOWzAqR95u2X1aeSIRjOlkmjnjAhW59btS/y4FULw37vhcyqRNecipQM9
DgSss9WckFpfleS515rDHwKvDbLpXdy8t1Y3Y0Q+WN463w170g4GtKiqzypTTuBy3NOc0UbR+VKp
y9IRnkZ18+hDES4QBtc1SoxH+gC8LFRCPg26MRrmIL8JnoAUqzY11qqFbfB96MZrZitX0hRn6kyP
IyAj64uvCUinSRW2/XHoT0DrzkZg53gLfAMJlvZan/TGSTkB7FaK/4YOm34PiCt0FEFbL8XNy3lj
c9CKdc+yV++pCK3EDVnltdxG+a4VRvS9TDkrM+U+6fIFRJsMly7GpnKHk0BKRg9Bkn/v2Vx3IQ5x
xB5gFJnpV0ITzGRIS61jSKj4n7wH6/DbjhfM3PClB8BZtylbNBYNNT2lpgxsX1vGfOhnr+k/mycM
ZKHiWcugLUpkInPw+rv97ThDRDF/0qdB9k3MwhiYzVUXJITDRkLUq5G3ZXfuHyyFdyhwF+7m8E3P
SI7bSCOrhBn272oi72GI4Genc/sSSr1kviNf+3SoWZ6UQaCfcwiT5oaeaNiwLBaJQrY/5CudoJ/0
gRpz9jINPCLjA2fvOLgyNWEO0drM1a15xQ6zVBPyvlaRld27njFwQbIJH+o+sjI6rTtZGVL/l12w
XsyWRoEPiPCh0nXGQHeGcUEgfY8r4PFMsetsVkm6IA+sMMFI/9nHABF+3kYs/JyRa1BtOg9Gmo2t
kyt9zBBeUZ9l341P+BHG+06yGwvcn4WpxOXsrCaRl6EsmajVGG2zUTvqHsSRVbBPoZcckqmjBWx2
OZ1U8HI73KGrluSI35S/HdKKmLp5n+NBZsGJkRElYDf5OrfYOVg2542j415DBR11l810LGpKdqlo
mevhKAHPcT2FLMi5YY1GdkNbfXnnINZLMi24roJB+BZAEYJJcSOw3QnI3p/x/9UU5btpSJN57fwD
JQUSVwczKjA7P8oHlUEFM5eTZAzsZbMXw772fz8gRYcwO3Y/hzHF6X4p/YOAA0JrZkZ2weihk7T7
dJlTxhyX2dvitaP2kzb02ftg67XKfcl0W4HEWyvjxj4+1mOLjxkrisQHAL7SJwO61cnCrr/r9QTy
eEgp1UN3vRol2+5rUUbV+DU3F1siQcFHfnhwaEImqTo/iOjm6WiN8SWd91iXOuFnyAmVZ7+utJj3
Dcw8N6Xj/RBoX6wWJDlUb5pg/eO0TuY6HBD/1eOW2Dj6+qyo9rlYihDwkQj8UgawOzR9gRtnyv2g
RnqbYGJpMqqhu/iNlZITpGZfFsWpPX8RJkF1ouKfmJ7PmCLd6GLX3sOXkuZucNqhGtlWOB3llTY8
aTOP9jlszk+Pcb61zRas882Nc0pvkvofGqyiN+IGwJeHWaGbuXG+zWYC2Jhrd/wBASyLla/I8BSf
a8MYeoB4K9fhZkQ+6Kwrz4pAurSjIuBNy52oOzGqfy8+Jivw5edsDEche6f441pmJaG6CG5PxnIZ
XPFZd+p0mYOHzHSgav0j5xNQUoljeltiMap0Bc+sEmRsyV5rq47Bxt6vxrPl2tUDjOmoAhu0yMA6
yrQdDw4yc6VNcLd6uBgJ4waiFsDus3V1Tpke0w/CZTu+5loH4qHHZIoiwrbt6rNgUQ75YGSjf/hm
Qs8U7fxSZ2NvQQ+icbbEItCU9r87PMnW59sEXjaVM6rnECp69Mp+tlKYS7HYIO26nhLqwVF4OTQc
FcShy+jecycJl149252+1HxWkr3fBy+vO0OtOn7HJYLRct6QryfcOhszyZHbPQEqR9ZV5JB+ZtUt
wpIUsPiYawuL5ukve7twpcSQhCIL5uIa2QaWv+kct5fUUK24uLMuL0tsRsQeq0ukm14TwVA8MN5f
1sjlx5N57CMkUQn6tF83ArrWqzCQj1mcbTxIbuDQili2jQHmcWqlzKorInrgyVRdoUEzBDqnnFp+
EakQEKJmH9cZt5/vanWvlqe9qxWsxGpxHOZNM50OmuezohmOpe84yoLE02UVXy/WLBrxp+sZtG7O
R542echs3s6DuFAhpaP2Il7nKG/b642eH0I9Slfo2C/qRnFXtNp0RpxLfLnfnPp5h1tmonZhvTOJ
y6FLn9T8k62BHwegXkqtruL55vxBWaIcDe3aPB3zDq6vtT7iXCUHVEH3Jdn3lcAcFK30em4s4oct
adq/U5OsVTqa/CXoSE7+VI/SBynJ8Oy6kZlWpbYLR+2EBV6LJLlj22Wai+C3nmtY8EF9E1T9vWZK
6U6vzAVoWAAsLYCT1BqYtk7bwkCwZU8y7/JScYPWJe9ufi+azwfbVTlMGyag8pTot3y17gsePElf
8jaU7xXv3W+CDq0DQ0OaeAMw7rNZ9jtMLZStfkepkA1vNc7qyyLjFm9wXmTKv/mHwDsIZ1Mavvjv
esnkqN4oBd1C1FNJc4exrT051B6hquVDoM5cJb7+3GMCdQ2xH8YSTAKzShBjvuZb80sIwOM/wfQ9
u4EFYytZLrRk6NEGx5BlLsCjQ7a2eA7vK6LrE7FBBnROvFy/eL/ia+GE6B5RBSXpPJRGQ97DTCVs
aQTjKJWcaczezAkaRIu6TNBDOdOIcA7ct9lMolENzhnLSHH1OWOD/DOTfhVVQyGOjD10RhlRQJmd
jfT9+4p3baEQwHtq8iZC1uFt5mm+o9sMv5FANPrueMJu6lJdtQpuUdhT++9Vpm4wot2hlWZKSu0Z
LPrGfvdQotNRW64jqdKC7YaA3G3C4e/hLJ5JBhpxfCZBk5H2b0bOOTG8UwZYLbGrmGtSFFT0/0Pf
4BDN01pOzbpmcRzolsNFQaSOUb3qLkxmEyPQ2bu/iLJ8p6z21srMzcECKZMQ/czuv2ItO34a+1v2
WeONy5jBLY4OmqDg5XbA9h26Dvgp5CbzJHWhWcf1uJ4WSTeCw+cUF0jPWtZJ82b1QLxChHUumXaa
yqJZsCVldZRCWg/L9RC8H/DwNqeafwIJcE3Mihg3jA2lDDItEbLXDsF4j+0RY0D1MizQEuis0EAX
O96OtJc6a0njwWpGyFYWahBIDBDZt1XE5tGfZEDxckYR9bqztRJsNXBDl/UehQQDcXSOU+fORsj/
ngBelgm+oHl9sarh6K1yq+/0I+HfAs0anyM7w3raKwrGXnBOegUSUm1GyJ/w8CKSUAJoKxbibwRx
swQ0scSDXZ0ZmnoCDSx6/08q55ozWh27LSxS+Sy6dVg9d85NhqKH5Ki42fZom2odHoUz5q5BLs3M
R9kMTr2DedwTssasYd2bnJN8m8fimY6bglaNERx33c2DYstkw5G9v/JdEw1fHN2mFA4+YFyp90Kq
wumVLE5CPJIDsL7HvcWxiZbKjZ7BTfxD/wIKPPsNwMn0x4EqGzU8+uLfD7ANWvAPHhy+a1mSSh2e
Vu95b6mGrIjIcF80brHjiFNtGpltRdmyjjJenMzYJtOeTqDV+0ISxStL5qqn7mS3W7l5LS6N9EYT
gwAY8FUtHMQq+NbLVLNHS/0TSE1qE6tBm/xvu6YqP4L9JOjyR3fwaergsPv8bKphrJZf6a8A3TGI
bH7zWz5Ae6pmva7y58Td436Tc2/6Q+T9pweyJApwHvI7SXWp63oT72MJKmur61il+zUtYCJTnUyH
tAaQCQYxjF+JWF7VjYEBIR7OFsDkeCWCASkvbgHqn7EbOSsaU089udWzwI+iJ5q7oMkju2B4OHN0
orPcyMy+uGGko6HMmvcMWcji9g4k3HOKXHxMVi/bx9C/5syjqw9ogOam/j62TLaUzZhQrYhQUm5D
ZHhrmPpd+isjIKurqHOjMseuMV+l6W5y7JNdur1LGTUkV1TZRe5Yhpu4TrvOgwUuEngeI7POqcBx
iN83MfqtM1ofb5O+4xkd8C96Mr2wf3EntStZUvYg6zq+hpb/KABtn4ezKu6dNsdYZI6v456z2bLZ
xziGvR5Uje+zEOYH1vwFP9S8CnTsQTj4B/7uU64XOJC/qqpGG1T8hmBYOu/z5FxIVduUANVii94Q
0J2JPGaSYIV4hBy2UYtmM/W8wbthpxdQWrYZqNNO6DS3kj3BlkTJ585cnv6CRed9sTq/7zZf+iSS
tWBbbs0eJ55oG8j5oxovhHyu+dMwtmwGiqOJ/blgQW+ASlau3I7iQ2DMps1BRDbZo5+hsgNInDLH
Bv4b9ufBP/iSYL7DV8YWwGO9FWlOGsvNwPenxavy69o0eUXj1D15AIztXJyXIBFvGTnMBUEsrIp3
Iz58X7lCoFkgOVDeKMj0k7aL+RbmCxEEz0EKxmtEE6yYwm8CTD0XU0GAVw9qChRE4Alqi3nnCCUt
RJ+w2Ho0dArIZFfXLGNr9vor76mXunia2feQZEyn0pF6EB6PsEtIm8SuRyvqR+j9E3l7C/GCNoG5
iX81CtbT68oDaMzovS6xW9IxSQzr7le/rOCP+Fc44tSa7h9pCJaieOwjC2KM16+zKztjggKT3UlA
Ecnl3kJjgPWNWAcMHPQoEThkytu5LujzSsB5PjfPi2EI4aVfjAx4PAgm0+5HAZy0KpEPHVrbf37s
Js1Cz4VlHCWj70njhUhgXuJi9LM3q+eKBk0s4mqAKoL6VwAfA26HT4vdzZjqP1XFCMSI1VTlgO60
4EGySPB9BtCfcIuCDiLV0swxg50yTiYAzabrLKJ5fWai9E3EdCjdKCsW031KzjkV1IgpwS9H5Mls
gSX+WOYS/a0u3bdVojS1wuraQ9JyCqNiDWJxtBvJe9SOL3EgHjC7gWrhBbY3AgOum4NSm+VyiWrM
N622/blx6F8buXajlYQ0IXATT0mV/GvElxsbAWG+5tNMLgufjDAl9tT98uhcH6rh3ePCdDaKPV2l
G5Zjajctak7dGhZEim/aXm+lThpoZSPcFoVKNFM7SsDS4QxIcN8FaHw+Etqt8rdgIEWvau0eEmR3
vY/2P5jEPp5gOOkeFiw6Bqfd/ybfdUC9EucOPp2AazvzSS+TikfZnTGK88o9ywmfFPTQUHAxaYM7
eqLZKG6WGlaU8whbCVMdrArwOD7psxGJ28O+vawFEANguOtO32zAjGbouFUr9N955GOrq/AC0PWS
sZw61k61cMjfSPUIYDXfll1M6DOtgjM8PJhFh7BA9djdAatrxNjGwlnouUjM7BTkQi788AhGnewP
JWLz0DCodqtKmxBQ8upcIEbq34o8sxI8PhXnj/M7K6DBmp90MQHwi1jhlRYu90GqbP/2ShFZSE4O
to799XWd+pES6QelAVjW2hfnUfY/awPCONZ49vHI4IEWItnJgRjUz6zDrWJmayRqxrYrOBQbE9DC
Fvvwxa5fSDdHeDh5x05amm3IcCAE65ddOsaofk4xrzYcwvEKmc1oB4TWNIZVuhyz7xpxdVy427/n
ZCG4g23WIUKmrTqVvTSLkg/sc5O/QPyBFtdRLj+3c6ASpqdy/4OJmesuNkMAuNGro8p+XYcQhjWe
6KMYWwhoIklJ953C7V/tXMFNZVWV8VD9OX5daoe28Jun1Mr9xS9iAAHgizP2ExLeM8bXlTc7EZVc
ovxRVKDbKUNZDyAZL4yGcJCXQE9Z00HznoEGOWWvnQGCqY0Rb707QU4ljiTDBKvPVp+58wfzSj/5
Blwl/ZM6PNlGqhKCPlDfNQhzqcWZnURNSeRgupAvHk5ILd+mtS/drivfXv7Vml8Sx6p2VwC2Khjb
Vz7hWIijeYxQJdZ+KlC5KpMy3WQ5vAZ06+VyOEpF+Rbu1BYnMhBqezbXDkZmvEXi/pxshgwD8x7f
3o4xBuTYGw7wit4b0i9epd6vLl1OCbOphtq4TUG3CebmBBH17CuhUnOzoJ/z1k72MnZE0P3fLAhU
zlbS4rS2lpyCAp79lQsKr2cgmihHKpnWUIRtNJCGtlRcroPtGkWykQ4OZh9920uw0xyq3QW66EJc
s49COd9qjqkVEmzJuI/QfChbQjdXRO9DH/pcTnlsKSwWFt5LgB86O6Lq70o+eGrYevJdzEjDz7wf
lWxd+T7dfDlAb+3P+tTjzPer4UCu8LEVcOFCpfZ/tb7Gqzbxwomo5gCtyCmI1h/Mx7baRmeUaC2G
g8XrD6f8Quiwt8VuSBkC1eeWy9UIAVkUPZEneMI2Zg1XgjallK44i/4UMvJjOkEDNZURBwvIF5xk
VnY0KOa89XZkXTuwJDO5l3Nb/zhpA3SLwUN/agVXTKD9M2CQ5m4VOjKGNbn0blUYIkowvyjf4+Cc
nMGmqxs9/Hcfmq96m0bYKEfP0mXmZzDFONABV5JwgIFGtBquXHo+pRyuFtHH0j8T3drCL7cppmN3
VcmgUDZu6SJEc0D+CKq5g0umKgdI62HJ1WYWVttOgcdPRYiy20tmwWV1Hi0O1yvqehcyYvleB6pk
AeUOz1QDV1r7Bb7CPGKT8f41pwLBnCibalw+NXVoZz81biVTX9SSuWcPqTMxm/txdW9ePXnG9he5
d+dSr3TNfx9KwxF1SNKgOMdfCV/V0ewjxIgq9ZcwzWhrqUCCCaEvuM3PwZQWKsmLb3VwTBOHp74z
xHoSfy6Sx3z48x3XCkOWYLhHehIQGnELTVV73SUz7C88iDT5W1e2sscDXnadrteNEfQojsU7q6XU
nSPjg9u19G2RcIBF3eddQ2luww7QIUEuZ2jvE7Zch80pqRB1klquylB1n77i9PZ0hQRo7OvcEWsR
6FtfP+m2yPTaB+o7dUu6iKUeUFIxUVe48aevQp0emzbr0M76kAd14HCpBdZ/6tHvggU+aCVtRtpP
eE80ec/D9TBaV5QOfm8LK38MPV+4hpaxwDCyVC93YNdomPBv3M5HEkaeKePUqml1OK5VpdB7u775
ofStMPaEnzL7MCCLE90YPMYgSK3pwP1xeIAsH2Jg5O/X11SeH4Cdf4gD+5233HEmNOx2hUoa6hCj
F6D2tHK0z0pFBKrv+i6zqS674xSu/86dVr1w/iHdwq/LqOMjS035zhE2HGIa0LWzVGGCskGf1xhm
2W/NH3IddV9m1v854PWwDE859w5XBUSo7MLfVYTv/2uYBbKtgNlpmN700ielBZtJQCRp9mjjreAa
EOBgDrnbWsmH6uS28moOI9+IVGrmKlgd1P2gq9nXgZtrEo895j44nHcZFgdtWGf441QGuGhpnuP7
f3wE9R87GXmyrs8vODe37+E/8B4EiCtof2K4IAkgB+2Lan6rIWfIjO5xKkapNedtQAsI1eycWp7S
Kc0ejJ8W1tDIbxvu85RCsYiOJxB3bc8FZGX32WicNkoaExvLv69WNRbDt9Zc/g9iWzRNc5YkMm6+
IYg+kIZBXsjrVD/krqsEjrVmVkXFXZLAVAaFDbFJpk8UJPvf7l5Pa8KBevHjQX9f6pyAyPOgf9Vl
r6wOqhU0SZgOrm5Z9Du3wtOw1eEj9qFQeXW59HDEbo21ko9RoIfr1MUyKnGJ+PRDCRF0WrULqj5t
TGEvJxiz4/ZWYnTCGAZi6hHzU2fdpCyY3e6NK/rjsj9IlbY3FqQykXv2V4A/v90bo1eZpQFH0ovm
xWhTbB+QHxwgDpd/cTu9+YjzfJMaVGs5Tg0LaXWh5sxSEib6G74izFOsAPe2FFZbCFaEeF5tSMjl
3Wgh4X98h+gRCFam20ogkaZlvAyGKamCXPu1F16dsQF3jeZIqvp+vnjPCf4yzGL8mKburRyz4Frm
58ISgeMMS6qUT4dHOyfOBcuEFtH5a9jNY+DxmokgmBCFccp3vN6oKEt8Op0mdpdk/7NdV2Nx17lQ
P+v7tF66MQqR0znLzjl9Eifj5EnrfXSDJpqRzFL0BOANYDz/WbGYO7FyN5Ka3y0Vw9iMiP3qSyFP
3oH2qpIAdpiiTf0XPosrWmdMt+tONkEzVhJTW8tf2IIKStX6Gu8Ls7yZapXkIw/SC2D+yufM10el
QfwLmJ1NgC7FEt/70AcvmNNY6ztouhF/LTLloeBG/Epk2I2PSApfRtFIy4yRBYinUJeTpBt29NWl
paad6Bx4Q5HXUneZ85grqIt0bpjmpM+//HDp5kUMGWwi+2BHJORmmEgHTExTJhtNLJ0LCueWUJHN
mDyKN4kbvAeV6yWmYU3rL3OLyWjNmTz3dwV6mEx3jgCqsq3fpFVJainLD32npwDowWy149hlH1ox
6dcN+XuOV9aq4Ujdugab5MLvfrbXauvyibOW2MfMFS7M8d/7gVN/VDjKzvY4ncJ5KfWrPPCvHrFn
nJZBiQB5ll070eiNiu/qyC5jp9K0ZteZGwcs30xtKhKXVcDlcB9bNBXCDKVsqAUKQ0cd1E1BhinE
wgsY4fVvEdAbG/ksetLaYkFUOIBbseF8ph2tryMeNZc1WPR7XQ+z+Fd4hvaIWFvBgk1CjtZOqA0O
SAKbFnemYwU3+MzNRxHnRmmIlRWk/YQnu42Z4w3IrN4T+qTMzIPqtc8Wn04m429/mJDplMmlVcUb
XFX2iOBdfML+zKY5Xmwv+E4TNXpeJe5dz67woCqybrWG97/+LDNOaNBWL2WVhnYMakgHED6BGsX0
ACEEqW6xGOHjCU8DrApbD9CAuMa3ZVihEkF/7bw1G7aJEE04GNgXrcX6jtCEVXI1OcCjVTZVV/dB
NydQHrOD104oHWjE6fQTybQsIGy/1vmAvdNn2HdvRn4sgmGZGkl3TzO59d851F/ma0QV83Zr6gxI
aXIW8ZlK6MwHoTE4vYhwYayys7QiHkBdjhDb8/QRvKwTQO8r3sEB/zq+NriH8Q1bDWd7pG9ku0SD
R7jFC1Lk7Zhkgs2FJS5Hc+Ptx+qf5fgPXyAy/kMqtO8HGAFxq9BgVDOVrhl0MQ0qGMVqDHx5ULPG
5BfwZM5ZptBQdxUa0lNmmfDdCjqVDCF9zQ3h6jarW3bGtbRTHzv/n5SPKC/acx0Ncj2fc10FJoRA
6hSXFO15TdAaidl7+grmMGPqF9tMCQzmDaiGrZkxPvWg6SWuR5yHbNNkh9gyiZz4DYCJA4KXKrST
rTiXLzrum7mjn7VOPLcHs+jI49dhWggAhbfpt3DTma/k4CSE1LAUwjBfXBBH+dHentNMzwgqDBxY
FT46rpt4J6QIgIAlIO2x77HyHMUl3ZAGkAXu9uYLqq/w0ABOQtGdaH/crAMwJnXT8HRbVLgVHcl1
SBhvnGBYIxSrRDO82Ry5gfiHv4S0uc3sAgToXmdEU3iop5eTaAM/WTo12cFN2zTh+lg7vp+EaQt0
ffFimEWPLvONpgX+j00SOOuo5EBjQHyvJWWLHqcxUzuPVS6w3eXBb/JM2LOmG5YDcp+iyQnCAuJA
SBx6X4v+EpJGqHpl+NUVoYe5mD9E1OZpub6psOohcrA74W9lLE8iOn6mKBPx8oG9iq7GCptWuqlv
aW6vWPEGGv9ptqBPvOh9fLyaKDquwVpnN7QNr0A+cFgsSPLOSLugqvsPaHV1Vm+Vk5Zi0KPCcCle
46vIf0xgGS1Ru9gZLwk7wabkxgVptrwqCjJt9nHlt6lRW+PKfYzp2g/wRTSoviouANgaMxQZiR35
XUWaDizEyfrBhsxiKVbJ5XOcAjQeiO7eYGqR1pQZsKK+9FBVpuNIssfAZfkP+/3rlQ/TWzg94aWj
M9Ag0DPI/LUUEqZ4ip4rsKWSaRwOiJQyd7rgIHLDBxYbopvxCmPdGiRwiOZCck7Ypd2dyis75FQ4
0xanJ2o0qzflhIjmM2J0WeNLhmJ7CXcNuOXkT2oMdPmN9RWMeJ/o2ltEXVc5H6+ec4C/XNFTwAUW
89SFGm5zLaTKTn3pWydcsep4jITW0k3+G+15IF+/HEeWKuj3I8suUbzQeZZjFg+/dF3LhKMc6KSl
s+eBjdLmx/S/rs1i5LmcyBeR5rjNB8bZuQnXh6CT24Trh0wsQXuQGWoyxrETxeA0wjJ6lsAb/k/5
EjLYpXgq6nxnKzdpfgETPJIwprRayhV4VxvpCmy/AccK76XeDOFsWjNVP++YgZGZLl2Tx1DfdFCD
Klqv2lE4c4w7d+ljV4jetqbgJ8TbvWYnArG7GWK1JvBrrq9IUPoWa7cyxrbxwGrJS4ewtHkOAq3h
GxTJ+H6Flhb8zTRXKrf1LdEigaGnxasUR3TswJGW04zmDCW295NNY297Su2ZRVAIz1lp5yyCQvh6
/lP6zMJpKFcj1cUthPfE/t1ct90CVIOKK56SC6AQUHE8uu+D5P/R0F4PwxPTliIGSRZocWPWqpro
rQ4QDwJGuEARo+yRpcgXtOVKshGPldDGMRtMFCmcTHiSfDaVjG9epyO+ynNNMDwBl5g7808nRQVi
DbYJzAHCDEStug5sPtAhmhOYMiF3zXp7q+3rjgQu03DgPLacpkv0YHLd0esWC/zZ7WW9puXubM2i
7I+l1F9pEoYQANwzM1qK8YAwbTzRVvArbLSr0GCClGWK6quG5Q+o0vb2m8v8WX/WaaV7eYeIlH5g
Lhtt/uU4QJ4njzx+Q6OZb7EOvisXcZ1Z8Vpe+agNmUdj3AcuYe4LfpL8QFnGtCKrFQYyQwUAxgUh
oZBdbwj644m1Lx2xEV1teWG80r6kncFDPlIBjQoCo9khceY5zmFNrkZB2smngHoN/zG1zNKoWL0J
BCUwvdmpQs9q4UW00t3ppMuvlHF9L+u/5jXoPiS+k7dOJO/0TTIA6P3sW+3iliFx6tOeuHBkwzI6
eAafP+3b4Bjl7e7ec8uJavQjLV7yT1IrqbX+Qcwf1ifBmsrphkTDInRgvDuENbRJiGbLkvmYjt2k
mkYyOKM+d+bdHzAlhcBYpmfx36G4ZHZfA8BEd0CfQn4yxZJP0JOaGJ9wtvkU8n0VZQ/2UQoz4sl+
EnjcBBItqtAgJhOPkAwDqwcO0EDI1f1OiBiraCw3X9pn7Bed+LQaYZ67G/WLNg3SPkEZ8KShAfhr
3LpL8sy0vBw6X3YxySKasu6f8NbghjbQ4h8mOFU3mGEjZHX1Ewu46nwdgTELhOwm0Nt3qeFuj8Fa
QFNNimLs4Mv3eu2EN+f2a1wrdy2IJgn/EvWqhBoziyEVMR0/e8FQEjwNdvQs2rvWDQjkrVrBUsYg
2GSUD4S904K9YGHkev2+MpbAMYUjfjUHJX59kk0diyu0ZpuPwJDxEguiP5XCPNYs8hbXC1NJ/AKy
WGU0r3uZlmM/vnCe0+TMzGLb7LxPj1eN9aY2hW2PUZO2rETvXsA0wkQkF198Y5XtgMEbfdQlCTZV
tknctWbPdzGAncA6aZKSALCi/79YRMaqm+rwNJz31fPdemjCZcc1gOfoJzqxfWv4L3ZrVH+lyNOu
jPRFHHzmrH7qRviOmZyK7DGO3cy8eNqyFP07T73gca3FV26o5Ol912YYWV9fnQhnm4Xk9bPozhDh
QKHDadr0+uhshJKgU5H9ujPLjcmqyzfOlyKmI+Rc20nRefNBnAtx6daHZ9op4tIOAV7BJ/+gxHVW
4B9UMdcZNME4euNr08rMvK62IkkhOxwaNBHq3NOBTcRDbqqJLOSbay0Yv6EKekCoMg60PK3r4kCL
0WmIMY3wf1dkgawF6m0x2Wpau6747boOS309xqPqkfSD9D4jd0wVQM0EEVZNKiHZZPALZ5rRtMxF
jpA60NZk0XZbMSxRcsdtXsskCQn3QDrU/SheMYU50Z59Be0Xu8t6bhZYStI1oJIxTTXjo2PtQ5HF
ErKxHrDqH9pTCDdhuUXatiSOpEFOWD0dyG1s3CGttCmK49Swj49ZHvY2+my9eWs8AqlTCiQVbGIX
bSTvSRKmm7BNo05HXfIE9Eav25TRGIKvwuqUU3jYKeD3I3IKy9/vdMImVBQsbV8k1GsYn0fPVr9H
RO/krf8YDPnoa22hUUACRBdPf8cFOm4LNET2ebYd+OfRCixGeiY2ObEJfVjIUZQcgCaJQwWuoFnN
4ISPKDVio3lk4LUH0eS1fwnItcQr3xXyQoPmEShF680DZhnoiIL2Rgd0avv6pl0pJZMDCdNJUXrp
crp9k7ok4DFBthKA2+FHR1hZT031nhOTZVHtwFpYZZFTXie+B6Lna/jAHcXIZOYuHKUC7PXNip0n
7ZiEhGgwcN1shdr4Yd8BLBBXg5DBmqscYa+AP4fr7Zuy5uKzULYj3wkKRFHHq514GPzFoDwtoT4I
ErJOE+A0xqxnqeY2goOsovWabXRRQrz2kXKmLueuSvpdGIrJWexK9H4bMi+On+2i5tQYvzMQy2yr
YrCRsHLqbaf34Nnzx/BitvDcTnhnA6ETJ7VEfUcNFb1pq1pwMZExvpUYSWcPv2NpNAf6S4r13z0i
X1PFMz4PfKSceTmDVYXcuxR13GPQzi26pVsmBvFxspxZs6g+oNMG/tZpt5b5yNw3Cqnzpj56vyO6
SMbJu8fkRVDJr21oJlgqjri5kP85R3uHmK6hqHDS3/GJCKZJKM+3u4kuGPcUALqUEiYKXctDmC8d
heQj1rqMSeFzL6Gg93G1A/HlyT2ZIY3MJd/mc8yqQhtpLzl0lDrz1a8tC1Y9PuXToy6gMHmrYqec
erh6eFghIEWyARuBZa+Nooi4Ne0HjDbs1udO92E+0M0ou8Kmfp2NodLxk4Z/4Yz3/WCkRTa1q4O4
B4BZ1WjiBSz/suftPxsNCF/VC4kbKUAUXRDyMldjkDFnwtr7WU+TSELCk0/V3VT4x8+25ZP3f0TQ
tvyVTjp8xRoyf5FT/zrmuqpWmiXrnDRMQCzBEckYAP0kYBE3WW9SuaIjC7f50zzbdGmIGkZyZViI
Rovz2NJRJcjzsMTEGuJe1vOVMqa1iHBvgazad9kXwZUUrgN86kkNoWDFdP3FwPpIEePBdh9oe7f0
V1Cr7dytlKUnMDTWj1BWfpOpNbUgcB8YWcsf8zJR2tsFEgzUVJeEJCamfUdDZHHchfETn4dCGpdp
WOy4ZJ1Evjo+VEacuorsFNOecpGsqjIu42GZDvI9dM541nEJEK8RC5X+CuGRg7v85DD9S8T0ekTV
FO/mRk7I5SJR7xiqcLcc6Mi/x4SiG2yUpAGVxpB1rRjbvIxoPJdO4GxZQFlO9S4NNupAO+yQQjxU
VUwXAT1PySvsMeyCL9pdW/4RAkgDAS5TRjkROR0sZr95z+7ewA4wAJYhJQki8S3OjTecXEP4BeA4
sZinD4xs/gDVW81uEXStyToIEiQBj2HTOO/c8C1sNBf0tH3zYO1X9cDNnspmbTE4cXiGL+BKy0cK
Lnw7YieCnUmbHu2ui4/FhI8hP37yq1yLHR5Y/gshmLvOG7yBn4FeTdzJunSl7XxTwWgHLgjWAGGF
wt22PhXczjwoe7eU5SxzzWLxHQGtqVLgym70ewazQRz/8JXIcPcIgQVl5wUfBz9yuTrE2b79T66y
bj0iQzZQoUjZQSDc6c67nyBHVockzC3AV7eh5GtHqzs2z3MX20fvPAQo8SebBTX2DEpipxbyvbx9
9NBPDWt9qNcjd3A1PfSOtVYidwEJXbugzLgTMUqM/2dl3q6w7T+cyIMigl104c4GyHVUmL3DOKYD
fAWgHKww0eqaeo2kN65iuJU8MefwVtPsRwAkM9Lqx0rcpDnxA8DUVtd7gkWwDLsSjyS258WdStVh
EpeMROcXzNABWY2EJZaeGEWVl3nMCX84pwO+GFk66tsfGecD9Wawv/mDQcDxnlW63g211aNdbm6H
QSo8oaWJ2akJs5Fnulopn2NE2iM4PsZz1hpVwcBsOL5sJ//zNh1+Tl3EqPvfxgh/n87vH0y+LtjF
/rPvkCdfMfKksp+qpZJEyxHKCA23d7IFzuwMYprtmdL2Ln/aLhdFYTLpx3wVYcTTkjNYPBlHoXW0
f+BOS5MLmqFVUQNeO9dHIEyGyo6q6j48DYcEzBb5gfiVlN/uLS+7g1GdxcY9ZoGRh3BmTEa/Y7IY
jxHbV+sdhq0YWRHa9c7ZBFFYf7g9ljvk1mjbCqMMSHfbIc2q0Brwat7o1Kmv5vi5hrmBeMj9S9+E
OAyVc8UgPyUsL+P577brccaBYhWp+hu77i1ftTnGju0O15wGxDt7O/dXPpMvlKdU4yZqFRKbi+w4
r/0+us+eb04Cv7vQalVrlx+ub1JLQZjKJp8CTcX/t1sMAHEXQzdjI+K09xCeKwPVNMVqbdUoOniW
3mA7vcV5VIzx0YpQfFpKKfG7kAKiaTo23rwL6ayfn/5iUL6oz61k0jo9G2ZWAOI3rawEvs4xv1P9
EbaKVnwVkUpv9DSeDNGsZR9wd1ngC9Az3zuUxr1kewLXaa+TBGbj+WKwgXodn2qhVUni2J1co/K6
mug9AEvCK7g0HEBx5rG3jFXtYq9Sa2Q0be5dJQQCr3nSHVa3BOxCEVdGV28kRomGRGaqPZ96JjJc
djq7KQyRuVSv+MGv04hkRmvGeYfWp6T+GtvPyw5SFtU2YwJ3Kl6v3wDiD32P+Tf9TezC5svT/Bmt
LfXjTl/HZBf1Zd4yfOpKlyGAGYas4eq1t05GNWfggmDWs5/cIKMoLFvI3kagX6+fCn0aj+yfl0gR
e74MymQbe3jc+r/zZHqHMNFb71dLOdpaEEeFHdwkle28ICVIT6w9fQVWMXm/BX8SApNL6OCIZ+HT
NfqPWzCrqj+j+e05mAu3rrHkMYNuS16ZhquyKdi5GR8ZB+4OiG0KDzB2a/i3JRWjk5ysB3L3YMuJ
4ZJxAwSvCFJSFLKJ8sJZZk4nRQ0ygrqiIDyE1bM5X/uNkmwS84LbWZX/A2v+csXBoDcTBT7yDyvC
gW101NMVo9KhoLmEyvIMnI8SmDAyAQqRSFdrf+W8yCmZsHCiZke8BtUn6DMLWWelJeEyM+UIEJ5Q
0Os9bvRay36mDARvXVpKhxLkeifcyagOm3ZM3P4vtLeHgMkALOrvif+SHpM/j43DcW9kTxLPuryr
9rSGKsJHwMTwCaYbKYu331xudAghMPYn8VF0TrnFyDp6PFlv9tUlCthGP1aM5+3J2bHgSPu2D3bW
H9au4uZTrdxEQrXRfA4c6UKLCKSHImVGEzxMyHYNXFJgTqxyRfQeEUhHrBDI4dEO9iPb14FA/9xf
OIh/KJvus5CF9xJn7RijhPsXr6bAtyid0P3Pr9POY61Mn/zZr0R2lMsX8Y1bCtlm7lsYA3NFztAd
R0IAuRo7XaEniyNqg5kx6nZ3duXiZAt53JgN1PXQgAisyBoPi0y4fBxGFhLmNSg1KblPVUBxJL/E
61IyljFHFH1dmyoG4wbdfT0HRWtbqJACaYHHK4L2Sz8lEL5ju+ZH279FL38HKulgQSJepniNftmn
LjL1tDWNLa/WiA4VyEBamT+vqCIJ9SCSxDgdCacRmzxn9NIRLoEgJkpLJTparMUPE6ZsKgA9eeeK
nFQ+/9aOTsTmHrdD9Hkqfb1eirnKVfHGGDpZj+t1G9boydhyHcsEtfoeSpNejMFyUBN6Hyphv6lR
9tQSU9+OuG3GGVud/RmOJPOvWuMnQ9EksBG1Ax36vywQAYcV6I6qqwrMdk1ui2cKUdwksFxiRjp6
5m6hL4VfkRD8w+TolZVNJQIbf5Oz10FX27QqG2O81gBtS/JqJgD0vNIk+KoDmzvqf4DEcTMI00yh
6Rtv4zebiapsoSeOiSv6Z9PbC/SAsL0xGcI1pEDB6uemJF2Krh6Fgu3j3jE9Al98ywA7DepskMWa
I4Ns5TJXlzQeaTbeMfoJUOnypaa3JhGit7Eb3tPqOskwEUsJrIis1FXsiPrag4NNJmwnaUgw8uIp
3znzH1baAB1mYLvh9guTyZyn448SL3ugiPjJ+Cla1RMQ8W2I30qQg0SeebB2yqtJnji1bsLinKqH
5SkIPO70jmxOfJbe1+ew3GH3SXTgl981OQbBuDTCCqQw6nM9I+5F86Kd3+jgLlK2WwVqupNIco51
pu71Kp17243+BN5UFPbr37jM+18Gw+B3k5vPgQfKzfJs3RKMp3E9g/A4G3L7Y3bfGj7Ua1MFZYA/
KI67zdovAc87dWL4uJYCGo7MbkKU+EecSuJ90pNGyA59je45YUuH43oDIJlK5YHNiH7XFAfHo8f3
ta+pnqjeys+WPL5D2Un1lj6v3opYjeKaknmUOE6R3ImRHPVm5gJzL+wFGF8rlYG+8ay+loI9Dxxa
TMPpnAiZ+bbzf1KhhFhgQEyflWdHaKaPaBFoVQMm6+l+QgSSCyQ1vTVdXe+2u1x0u8w/Oe4tT4Kt
biJiNPqbFNCZ10q3NIqfz8vparO+ObPdA47iVQkaGySQq7iPhxO2VecR8UJUj8jko7660sdbzKJ4
gSS0fKXpeCWkWBpVBcyIhmSSVDFuvb5AMaDa46rR3dIIz+5IGjHjXLAcCQAAFbodmEhArfQG9kkn
vmMIWYMxka5xE47qdxmENIL/k77onBpiwR+j6O+hez9k1lNLuV7FqjWqhvNzpdtfmy7GaaZKDamP
ub0JGfKK937s5D2vQj3/mNN9EcaM1cjyEfMC1elLJsTv6HAsr7X2J16XtPyd7Np6qOfMo/+9nFuo
IM321mq7vPf61Mjn9ljd7oXxuEqGcpfbdf86oGJIpOwkJxUNUQIFBovAVym8uFqINsooxSzKFnkz
EKsYA9L+zUOWr7hr9FXSVBn6jb2EpHV3ckhHmoUMFM2bTpmNKMIGQJLa6XHqUNq3kzz0gNksoJx+
H2LDVBzNB8gWdl6Brs96gN86uy3ZYYaN2Vd42OZT/E41+UgSZrQELB7OigL4hXdZlNhEu5PF1NgO
8Z+hH+qXn8hYsk8XKTy8KuYaK4zfRe8CZyY8UBye/JZLYt/5+DdLOvJxi55fVooM+XmyOz3F58jk
hKMJGxlrVMtDg8/MkR/1OoYjulzJSKwHaswpu4P15TOtK5/NewqzjAHlnrCM/tjArCNMmVJ25npq
BW8Lcy4c8WD2ERLgSS9G43o3BqvFXYsqpNRcSvQJwoRLQ4u531Uo8hnKHlnweWSbNS/b6x1PB+vM
MQ/IhakEQZbCQlO6uJT06HB3nAEPO2nopJXW/9X8rw0a2MzyTRR7W9YbJ/OIFCxxKijY71IIoeFD
p7iAMoRxAJrEkpQq5QwPUFVFs5+RXS2iX2VwhPR/QmLVpw2pcU7aqXqkAWsT1WLEzDM8XiDL/XsZ
deQMDHq/0egFmABl5RsMoNVO6L5x3klprJDfuYL+ugpIipCRi2bPNdisfhOwEzqKgH8LAeV7lb/0
X9YjiXWi1q5sZ2BVgY22Y6cI+C/7/1HiI+eT2HpZHYjXNDy+c6GiflGWpe7KFosiGld5kcNjCUEx
JcDZ4oOci+JVitQJsFf4NdmicD2kuo2vsGEuFMLeRy4qb3sYln2yrZkTj59GUe2C692echXqkJvc
RsGQHh/lOh6hFtZpQJFaAy1hR5Xyi67YdQ4CWv8abXYkOPLWRj9bm5RVlbe4IhaBWgJRK33cnQit
O5Nq3Hkq0w76J7OzOvP+jQDRV7g6UQ9S03qb/ztwwKutly+3VfViaN2Jfuv0JXwOveHaUazTKby3
hUP6XXJ9cIepa7fekLC0MByjbF0IwK5R9HVoQnWrWvN1L09XOxZnl9RuH/pzJhZCBhmkYFKy3eKs
5ybyvRKnalhHz1sQHmMc5/IYWtOIBT5HSW1/xaFviemhG7mpW2WubRz7waBotVVd1PWY3+oPe9ZX
byRw/voTHic6NUIL4MyAvxih5XPBka7b8S7n7RTiDJe5Duw6RgySuIFnsPKAdgJQM3cW2hL0KsbK
27for4UZNi1Zqu99svh9LjwVXvwL6ut18eer9L75hE8WdW3psqnq/nR9gHTCMwtmVTAAQu6jqlR0
ucygCV7eoGb+Mc3wM5A+4nzGLIMHdacq/q3MIr/AtIMbta9GpVpC90jGfKvyImTT6OEJGRaSe+qn
d3XzVQ5Yl0v02+NdtOworhu7a6T4TKV4Fcii0M2tksCZvG/VQ0LUfxFFGfXYL17NA2GQnbMyefNK
KEUVFLbNCp3ScEk/y/Ecdz5B0xpQNfBPbmWgwJk0+7VaNq4LO648Ldn5QnfQovqGUFgu0yzRUcJl
GUuYwkGTZGnU/vC38TMCY1wWggqtmZ9PyXbSOCiNsjs9EIMvcOhwH+9hyjiqmc3dAO0YuxMNAEco
T1GZC7W26RJYcRc0/l2BA9IggVUpPrbLCOqZkuLTUd8vEaeEUxLpugPRO4M/PaisWXbEfm/etVi4
G1tk3KDE2nOuRQl2UM0wsEiA3Uo2NguK1xC+Xj4BSroCPlWdYuWutaQqfLKn1AgnXDg0MPd8S02z
PHqSxYDlz6oOLwGCPOeh8Igb9XXIxjTRM4ZhbMytxN2w0/B4tepWd3SSJCE+xMZjcamIx5AR2KJ5
hgi+EFqfLgvuxXwqm9FFY4AZc9JZqUqIN7HRlJl8iSpsUmfVHQbhW0VpS6nZsoWJ5AI4Mn+dI8ju
3eR4lfrzW3ybaD2HrfJnJ8XmOhseoN7fqW5xdwVaEuSQ5TurXHOsK5mwFNBVIfpeCw15jPNsWuS0
QlUIS/8w106OZwWexhCc5gipXRwjIy2RO3orEUUlZ29Maf3+Ix4nLtV/DyUEHHnJAjsGuL4wie8X
vUAmA3tEUSDf+m7xceSMnZCOHqhDGzy4DwVUa6l9YcbKJtuYhHkkytGY8uK1TttGuBA+C6soUrP8
noZHxSvz6pYeZ9JWcCp7NgOKI2iMMU+ZsqqieewyL3LIGta/zS+2aeArgFQgawQTq9chi0bmguIr
L4E43/xINT03YHj+M8pSZB9W0lpuIY48zAweaKCbJ6KVuoYA39QEWEx0rV8VJqZLJHaCVKt+NCCs
hq62aRjQrrfRUsq3a7pJzDiJThAfr4CE8Otrf2xThTEvPMRpm1X+Or9HQuAf4TTJPx5jWjhX+ICr
tAR+F6VnTz6Gzt2G80UQc420gTaFDEEN8YrhTwv7Q9Rab1nG4G5r6/M6ixa4mDynYRdSbfDlcWzJ
GTX3WUwP8bmoVPTnZlNSa3HtJUKKJ/jmVymnqezykLJH3AwzBRB4vmZtFgSzN+/6fkHkTj/IQsoC
/7sAReuy7tDj88wcTq21ssRUhNHVoRLVLa6GDF4PrKMEoEDur5JfEIXz/TAzCYUrWCRd3qs/miBb
XxTuLxMyL+qoKaRGM2sI3V8YyMUtDK4TAI2FmxJbFLXZB44O/0cegzvJ4plgGCMieLmxlfCtZm8t
cAKoTN+giUgjk73y1hc+nwlJCihF8Q4OiZQwGWpk5D5yrd+6V1UPxJpmVmJd+ucx4TcAxtNQifjx
/I7Ho5EsHAm7OCYChnQR9xPvDCg8q1ihklo+911RLtZ8HPrZS2l+/drEYkzfWxhn6wWK5t+XrZKE
WQAhlbWc0FPVMNcS5ua/agm9KoU0tjo3yS2KAYkcSnU8pFrQJpiltkIRE2uf2UoXgbn5c6f3DbzK
oJ2axQCvcPmFCdaEpF3/DxhJhrAm1g75zpg8QKYLh3wvFZeA96YgQ1MPI/LFzblFwoJgDTXdOXn6
3TMgbp5CfOpGNqBxgP+0xirbqKsyk/YZ6wsFYZwm1ZjlAiEdU55ltXZeRxmrhs8tbXu4SIMKWI2I
ilEOQPWsQgodz65Az8b0Ytk416KXB6uy0X6TeCfIUjy2UstwbWBO3XT7swc7PS3SuShtsMsFLuGR
3aZLeuUcDPYJoIDOpa9p15/jeVaGq5Ed2tGtqkODf31tYzjI0qXSX+Y9K6VqmvVv8CZdMLQmeVyV
YDuROlM2Z+yGBIpXCGqEpwhizlcuHc2vPv7DCmzehFA4m0+Lwdqx3pdUWbnNJHIJih0BrVZ9IL8T
AYUK8aeSCgQaV3qQQrh80+gJeZP0cW9o2RMV3Fn5eibO/XEq/ja9FsXDHrGrAO0QNqVvaRBIwdAT
8yy+DJNDRm6Gr+oFiP9efdBqOmevbT1+K6T4m/97cXxjbu/4XyCW3WWO9jQAWXDjn7cu5rZPuhLW
jjTBQW0Hj9/JWdT/U6np9B1k6uuFJz7FqaD3A9QDF7olNemQ0QCgayqg0jD607B9DhKh/RrzlWSt
nd1jCNITDDVOPra9yKJFS9zzu1ol3V9E4+OtZjhOgIOs98z6psffCz7Ymwe8O55/a8HrapfIHdu6
NZ/JEkGYKw+9NGqmMzGw1Lp0J873LFFbFUBodqY8ZVq+1YdqFuHwupt7W2Ajc07V7u74Q+pgV9l6
wOy+kRWqQ6+hceVHU1BOgqh+pRu7DZyqk9xbbP/Gdv53e8hGu+wuPjtYlhWxypewDbexHbX4wn2F
5n5fNSeRgpHxfXyl11t2KBY/nc9sGTSpwzL6fW0MP94iCw6UoI7RdJi1DhgxyBeaozHwG8lnRqKQ
2N1mUP5Ng3xbYPDC5PQAJ6zhdX3AQFbHDTjlDAN7GYIw7W2yRaic4ceXvbKKhWt5sk4hZqXqqqOZ
V4E/R2Farxddqinu6pp+udm8Da9PdXtwYPN1EgY5oNkUduUKSBF4s6wzcxsFU1oNW7KTf3wLwgsl
gHcVyUgG4bcLDzHMMOmccq8djsJx65yEw6dKrY/PjU2k1CddnuSS3dIkvZuPKHmzasjT2PGhx4un
gswPjTyj0nOv3i9/ko2cgpquWf8Ag5KpmfqENIIE4PNEbvbf1WBAjXrjns3/Jn2Ee8D6ccNKvqhw
5t3eXbdjRisOIVBz+Fy+/WZU1eqmZRX504vDPvwrmwUhs+RuRpaY3bYmOVUcykT2ibK4C1NWENLq
/qOADOmLSd+6SRi005ABtYgFPzMZJ09pEC59bBEP9ObtFla3iXB16h/KMJW6yomHRPdJsqlLU2QZ
hFL8LacJ142NVBiZi9bPc1zr7S/0s8khuXxaZB8EGYuhdnvpIaPUgYKfqPc7dITI/RQ/BFOfwr4y
7sGSPFf52orWwBwg78S9qzz3NsKxvozFMHebV/oVm7e6z6cDDZMlGz9586OPaX1r0W2YRASFy4Cw
g1CXF1Z0r4mabHnFiyRW9tU9FZexKKf2E36spLgmAAY7Ie5olbDPUKFmJrZK7aJPfCsb7HrLRD+Z
wC/+GuJHW6rrf6tHFf/q7xvMmyNFqOkC48yaTb+kCWtwHvFguU+S4De0p3vX8mwaWDJCRkIvAu8B
2qeuJ9HWgQ8TfJvdYSMYr+DIdQOD5PtTZLjSh6/EeYrevxawSrS0L07+xu+XOa4drcEohfl2FwnU
yjmrC4cs8fI/x/W+JtetStKk5RMroD/f9SBjknoBTRQp/CkbScRrFFxNGG02pd3ngiC7k/9BDIPr
o8ESfcpuh9YiCREzB8J17FBZJv6lpyjD+moNeTsa1HtR/z5C63uWvlfJZv3+d7yAx/X4Llg8YnAU
s4z6TGp0eHXFMvwFb/4/p0mzQH5rmoc1LEVpjbGK2MTVncbDWY5r5n1MxRSc9KEnj5ro1NPwz6yX
y4Iq4whV/PT2UrAyd3AZXo27Fy3xsYqccpltNg8ijqm7r0nq+IbF0TZ6l5FLGqwvSZ6jeSVDCDel
9tkktZ6avUSxGmk8fhtWWmg8bi2RphF4XHs6lvEUTZ0PkFgmlbieCkgsG2BHFSMDGuVk2RuZqoCD
bG7u6uuJczNS22SlxQJHB9FedbxGp7JbbvkaxwA4q9h7MrACoEVCPfed+Kb5bUdJcMiuzUVOw3q0
fJQ7xvEl935XE4FGfienJcQs4/xTBoLtz9bY6vsKgfnguLNsonyc2sx0wt4MBoQsgzEPfZD6CA3b
fOskplf02cg8LtZAl7gdblN5crU6QW7DmZC629Zy7LpBIGwRlv9JevxAa0F6yrmyO6p+/vN+1zig
MBS5S8bQyG02iIgXeIEb8H+dlfsgMmu2c4CQsLTex1CKB2Ao5BHBbHrtUKszhpW1yVYJaXZ240qe
W0E25Nvr9xJLlWdTpAVOGSAW7zLbomRpupudjleQjZLews2FbHWh/DeSgJ+6woSZs+t7EWaqnP2E
fB+szsKtN26/VCoboFUIo0CqQbCf/60bM5Tb/N/Vh5ojJk1vN/Bc4P6ydJVJI6axM1MUqycREX95
JdXv3QnUpaaYyVJ/SmU5m0labiNui4ZfDDc4Miy7Q004a20tzATOgVDX4EW3mmWHWsx6sqDtrhYK
Uw3uQJwt1xiSBybP7vbsq8PYHiwJ6Dnn//MjPfxTcKYqe8W0TFLFLMb/2GLsXajBuF011Y1cqj/h
SoE9UbjeG1j5oc1s7X2njmkOvJIMs0NFuTrMNxG2qJgfw5mHYVKCNAnL3VRgUy/CCpzCeGZCbS+m
BSfbniZ4g4gxzAy0InX6ByDYGXozP/hwd3lIX7qGCAL3ypwdADl6cj+25dPVbeBhVILZSKWxf/US
B//H+qNh4YbItdxMgD2HfXlzScZwLDg0kxhh69mQmYMG0g67IWq0dwMdo9chef3o1qBZz8Jq3kR9
6aI34MoYhEAaSWU10gm0LByA5ZI6pwuKwspCRtMRBBBA0mBxva9NF8SDn/NO6ZnsJHRrrCe7+Wpe
vlcg3yc5jcb52GQBDmwAPfggPgAx2snOVf/NAjm3oYdzDhJHLDNEpYEyzVJI27Ti3BsRG6fP0xxa
WkM3frDhWqRQ++FCUov0Fs0eImkyi2B/X9olAKBQOsZp3TSWS4yBZXkMCfg/M3IMRf/U1Ynvme6+
l98xHOISehrASxC+JKePeKiHgPqhKSsaBXaYQTggOZR6qNO+9a2OMkR3dFPwsEJgeb8EENkokpRT
a0j3isUrinn5rIrvUbRo5ziKjfmSriZIV2iqgpU5Dkt20BuGwGw+EtRcbm1mZ9FW0HSACaWPUZoA
uneynI9qzUEE3TrjVXl/IgZCMFTKtnc8nEB8U7tFUwKbif5R/KKlMG0qO8Oxywm3kyz+jDuIYHuC
CaAdQ7QTi770cD39tWvDV4hFgR7jPE+hn5ZkYAaG3+AXbuVSX1UpmTEI6GLK+6NuveoV6UbBAU4y
cRxxmD5VSeV6essPs4TuQEaOn+OhHYDZCH/AN46Zfp85POyqrn7n9gPtlo+CeaYCvxRvo3amc/cq
yjCaJhJ7fx3IaKeCFWkAF7yKSLwNlAEtg1dessCfsAQ5Wk9Cb7mrtALoyLJzmc0tIa0CvI2zfyf5
RXJO7RvUgbcLwiNPclvV4FvyS9YJ47bHkFGv3hs9YKyoGCKljkcmyWTQA1c6MEZtx9GBOQ1yDR4k
6/T4vR8mNZwYY1X/yDyTnHXeZq+ilvCVBGXMSWO/jRD8N7vMRnC61N5UbYJxmU6uFBzmhlmHJsCe
1xe43P7EV3XtMXpb4voMuXkpSFIJwCBgwaVP30vh+uKwolOXm3/QpFXqUqYA5R5uOHPpnuDXRfrp
8Jxtrvssv2TsFJCWnnCpJecrHyFKC0kgqRv1+BDjYflL5PqFP0mZvvpjRd9YT9u+8ZxUqPBjTYGu
3/Pv1ru/brSRTHFc+GUqRilfpquwbE+6kzj4KuVD1BKFdwzTjG/OBTrBKCPRZ4Chl0ZFAno2ry7k
2/1/JYvLjKgZNvBFiLolnoOs+ep9ev75jCvWj1Cil+lT50eCgKkuVCxkONwgglb+VdhnyVH74q+7
Xe141Kj2WU5pt1jcIAhY7FVBoIcdfqptMDs2rGT12URAD95cNISKRiK0R+1O6m2/+8eAPlhhu3iH
rbN8CfO63a7lMsWgNfryePbepvPOm41yR2je3vpYTkI7WqXlQalyZq0ca4iBvYubrgyOiSCzYYDs
1xTYGxYC08P0NvqNqITExp1k005s8YXhEHJ7SlX/kogjCxFS8dAvwE7TWWullHou6rcy5IyGOovW
ey3Od2pVuXGTG9pyn8v+t1Up+QwConze+oHan95ZbYWhB2G6h/ayD43DCOrkd6RutEqPHL35kmTA
aV/yvMct7hEAsfrDcuCK2dX7vSSMiIDCfiD7Buc5uHjl9Yquo8t+qn64P/ZBaKnhY4m4eBeFOQN9
sNt6fpCC5FIR3qkiz/X2QsQ/Sg+B+/AJFKb54OmSurMtBa8hRELMpl1rAoK5p8AHDsmr62vWTR3l
oqOKGngMkpo0jC+7quUJegxebnxMVrBahbxucsFDpzK6dqAR03q/dsHpECZF3enmukkz1Ul0ZABo
dQp9IlSDED9UN2VN+Z2N9ByMzszjBSg3pnIbXPDar7IA8W8CwQpYrZDbn2o4n1PS/2l6PUi1awty
G0qqsAnb/OEbhH5ciJgjckTmxKN8v7lSspxELV9xML8BmXHjUUXb7XIO8M3jmlTC7SCNDDeHBwsS
3q3hkJQBurvRUOWUV1qi6Xg1XKYg6RXaGd5N3GERFI1DrwVIaf9Vm9RhFZJ41AjNdYuz6OcaMY1e
Kc/b3o0AWmnQqcHT7fV9+fj3EnfQ3QBW5Sz1f0qXehBLi8Fhq4GZE2wFUvlMibPMzzJ+yjdqgVS8
LAzVF6X3y8xHa9xcUVKoSbOn0qdq3QMsv0gux293d3gv81GfEr76pm1xM3sb7W1bPqtI7QfqsStb
ZRjKrcQbW5RscnURykTSbBLXO9YdHpTPbpVXORr8yqXvN/XTsPM6iaJSk/0KD1lUYGrspMQwx/5e
xpAL6EGuFKnJbElbqSH70LKGzjeylUx+ZDWMYCsWSN7qS8Uw6gdbEXQei22FuvschHVBPRDBwuJ9
Mv2s02Yzl+EJHGmmURObwuVATuiuzyM/KBMsm+c9k3W2CrvgtDRKOFv3jkbxKKq6MNnYZmJI4MQP
SkI2u+k6eE6Nq3Tq4qPr1pod3/yRCcgBBKAOcSUpo0B+REu9UaB1WkF0O7+dek3+8WcIFvEfJ2GG
7X6AL25J7stQ4/4ZIlbF3MAT75XeNXKKHGxCeHmLqkB1lbwFz12G75uyv3g40KqmwzjLN7WZzLtc
Qk03WQ/7cOwU3Ou13ZMSo+fVwW8LId8UI8h5qucAHhzZpP/Q4crs2Fymi423N3CEGZRYF01GVLnn
RH3bLg1AmH7kkd4otT2c0Zjv5dZO4VqVpgvzfNt5L7FoyRGYq2uHRsGkh2wZtnLSqluok7/KOUbK
8+NhAXMU+3bWgoFnnIdDn7PYRCQn6MFxs0ILwhJTa6ODQKByyJoq1kLIi/d7bx1+6hs+tuf5wOnr
112pFPOpT3WBr1tY5Qw7Z28Qhzn17r0ssKxgem2SNzZhg4DDU0hxyC3TxNOcOJUnAJ8F0iSwes0J
jiXBzPTW2gfrrmrPr+njv/+DbyNgQtTN4vQe0AncmjEJTnmMkP4dxrPqqHd0Eya12cQK3SDmWqhm
Kt7nKtQgWENJeky74vy/MJ744zIySRou26fxpERChlFlVhwq5X/7FAC7wvJyZ+0XAS4JyM3V1X/t
oHJYMk2K7pL7hHy2Uf+7Uk2gcVTf6+nyT2zN463R+NZQFZS+rjcvfCfv3ZXNTXf9BHMZAMjGBXKS
mO/4Yaw+go5y5pV1atemPCzzMsk4Nb7CdkBv2dxo9GpBnfQar2SdnJLJLLIjuS034YBVA54t+hBZ
/bKXmF2no8wukx84mtgMkRua8ChvuyyES4ufuefAKbGxBUDl/SEw3Fw2jrKs7H6fP1cf4wGBhdLB
Nego+B2Bgu17gGwul6WVxeHXq3hSYU/wsN01Jqr6jHA7TrKROnsLt1nfVO7v+Q4nRWo6NKQgHFav
nXWSVXIC10qwYAXvBA7sV+fgMOuDW+Au3/DsNsMz3yH+8as0OteXg2z3vrfyECW+j8Mt/CighHfV
mHUsdMJlTtT4YsNSSjjTZEOOScW7bvB0iDzh8grmlfCIfmjTOWMe+0t5RM8IDdCB2G5DeqEDMD8A
sq1DFHWrVt3bKCDA1K3wZQz0y1iMGmY8/L/QGmZSG0teAnlTdXCxFVtPM2KyM0cAHV+BIqivPUUb
K2HXqvMaXugTOXyTFQwdVbrpxapouY7UwcgXDhs4OJsmGYmBDgSGplnFlPBmlW95pvRieTnOMssk
gApDWqAoZqqYjZilLTFJWPj8ihp3bCwvElJ0zqI3Ab4uvH2qnpQIswFJXwmD155KWCr8c50lKu7x
HoM8Kiv1huNxXUItG6Hp4n4xDxW4njgS91lQp0to1DtcVZ8z9AB1qcEc/2LfVcEtUglJhP98VtxK
DTKyY2O1oZHxWDHGAdZm/mXnk+KPIYoM0CdXP9XQhamoLx2dX8B/NjXk9+8yayh3qCzJ8zVMTlPe
G7map0X1pkLddmGsTtyknhmLdfy/Ekv5Etv0pgdC+2w7q0YpYYjzn3ICC8W4/iCY5PV/mYRUDlD1
eJwKELuw5o3kVP8gVwXp95QaoztJFHq1eZUHb5XSfd/o/xPhasqF5EslJca/xOnqSz0DxkGAoKp7
n5JWrZtlU1w2kLLe6MzAP01+UI07KX8uM7rd9LLNQ2ZXgcvMEqMve6GeQn+bHfI5xn/ZgYberE9o
UQviuOqJ2wtJ7fw3TlIS6HdATXxok2QafFwtk0QKDVnbpgMj69+d/F/RiFYmSQdsOOFBlKHEXQBs
zaCjOD05JDDAYtssCLHsJnsepZ+U1cEEDubrVzYupK6M4w4O4cjFIHooIfIkR2l2QiOwfH/1oErO
gf3fOfC5eNMs0SbCYcplg9r7OJ1UdPda8ntY3ZhoEhu0n84Bqgqj9m9NMKcQun9N/Vc0SMJ5AY2C
zHaBA6EnJ3xVfZ2x/Iazz9TLvtJiSDtcXMuYIhqWSpHg6DW8XdEyqeOF4FBHgpotZVeY5elgX2Dx
cXMqQ4lNCLRf5BNOVsvjm8nvEfNIbMyGLFOXu3aAZY5j4nWD7JlsrVxxbRQHCKBhD3A+JUiVW6JS
8muGr3JD+4YTKu1laH5uD92kvVR/6PPPcz5I1BAHH4ipX3NE6ed32EJCwpfiXxDgll74H3+rDX6e
4tTss4Xz4cODVvoGP5SCkrjodBaFNmjP8ZQ26k7ThafHMI48wtSorMAMXmOYTOh74xvBjwpKQWRA
A6tahfvpjNZQ59/1/I1coYqP5IGXWQop/azS84d6X5ZoUkE2kMK4jgAOzMXQHUmH8T8NC2uS5SI2
oTJ8u864SHOqdva4Hkl1kcJmO8g6UQCLbqqMfYcEA0Ofe5uAXju+KqD1LdJPXjOT3sZiHjuCkNIZ
h0qkvmXseyLPvMbwLIkDw8LjkfliVjxv2zztRt1lr6GRus8kPrked8FC+T3Fkgv2cOrIIPdd9viz
OicjC09aeSfmBhcZnnvDi99b6gbetbRDnUdljrwHkSS5bZ68P3Odh5kfeSkryfcCdEJHLnCHomky
zeYmKh6hgaN9monsQQrtoeDyDUipvON92KR9TRiwL5CQCTFObPSFKI4W7S1GSwuLMp8Tz1FAAeCc
AGLxbBNV1m8wxdiof1C+ns4eAYCL6XClntUJGIx4z+0ksAIRnEHxCoU9Pd8+8TgjEIOtJWE/yuB3
mBcadPpMda+MBxb4PupSr8jhVfvT54AjfjByloI2sIix6sAVa5SBjp4Y+pFEitM4Uok63H9SLePF
YoTqybdbWWOK+gT7HIXKhPEI2impnIjMj0rEm8yDgiqOToQw48NwsncuLSRDZXLW8RbSUuD72pbJ
/MyMAVunTInBp/Je8o0I4UobPiduXUDEmXISCxeXmzirwu9P9QiPq1W80ufQVbD22fvMnm2/RUk3
iJFAaG0LubJrif+pYo51bH9aU6nX7kHDZ2OjfiCn93lhP7yUIqPkHtdmyKWxK4JSNokpNhznOv8+
anBnLoIfvKwmytL7jHvrgEOU9AmnUICC0juYPFJy1xLN1yuuJg2lMhE2Ji7spRe9xrp7stTFED2F
LJvKhtr4VFfROYjlW61t2TQgIU7f2psE2G3jlo3GZ75q8AfrekrrayvKQLMivA4RtH+rty9jp6lO
WXZqP4IdCzYpD1BZx8pqyZoTnfmhuLh0B60wfNvs2BVgF+L01XB/UnF8aG0QdarkE+LHzv4Pedsi
sNHf0JsEuPzyaEndUuAGAYI3xcoBD5c7i9Ez0j0IE215b1MCS8hP2cijuAHfTWmqHI+cSOFyFoqx
+iXbCK7xH0qry0jAKR12vW3V0yw9PdRq2XQVr21lpmi+nexDxKk7dgAALSc8phUTO1zbEAQwHZxx
fYyuSJGN4FuXfy6elBwJmEc6gsZmfuaxTH/zhDxYo120rUtkxn01A1D5BE8S4Ju8G5bJdCssLSZj
lBga2Fu0e/+avUCbWr4LVNSM3tXOnnW91sM+7fwUmCjWwbdG/kN8XENACBq928M1D+U+kFl/19pl
nTiV2OKZ7PJON+IGF2vbAPNEZvYo+5wWSkb/sCRoESbwzEnO0asZDxUE49ykE0fH//FI9x2uOVsK
cmVopvbyU0PE3Qrst0ChelNOTotEjz3VwLvWuZES/4qaLW6h9o54oZKw91aJA91+BMGUKF8CjXrw
vz/wyFzRwIrJa4YjOQGIjKZw68W0PeUTr1i980cPeYvZkRe/C75AAZTlg/p10bk3QYK9WMA7KOuT
BhF6tzeUd6+UcmSaRdRNDK66slhRgabo7jD0GrkDQuWncTYkEBRKb27NiEa4KqDDEjZA7qM2zYUr
w1yImVb9z5fLKiPMceK5eNw+WOvKKzc7E4lrJ7UDXcc1/lJqHTMuXfDOgyLCzBBAOQ/ZtsyBIMd1
eK4loyj3Ei1iiu5p5/oVNH7bDIqD2ZIXoPVwgSM3ZXeOYKRdIq4fW0Dat6wgdhrdYO2oi3dX9Q1K
RmwhsCIefaBOp1KSKfD4hYhs19RMLu+R0zb/05n5AS+9DMp3XuA+TooMwmMVmLnJ6W/QoQf2rX9q
kcj73ZsMaJPhMhm29hbceUoWII09rAklrShYg7cDJUPcgToNpbUMGZ2hDiYKnLEL8kV1v5ah73+7
WZ69bpMGWsTGbiLPrrQNoYDqAbLlZMCjWxsO5FI4iI0cEYy2QlSlIGE3Oft+3e1E6OdJnClWDQMW
UIHW97Cr2pn7sSAVRZVHz7BTD1C7gshu5T0Dqxz0LCoSlSFNxRZVvTRmPsCqv1IpGqjc/rSzGlMV
AXfsLM7rUxC9eSOA4IjocBPMQmHf+gIC6/zP2rsD1MZGoVVb6C5k7EOFO6P2IWRq53bRDKO4UyLT
/kasSVJQSPCU1FNEjFvkbJDmDgrEiHw8zh/AMMTJcvhXvwvBmsOZFfWE9vFbD29/N01knMKQgOUh
8NGa4bDaNknC85xTfEVXuFnARxGOvBmGDS4efU4h6FPc0w4xolHx8y54Ndeof06CisKxlHcEdtEg
jnPz2MfIeRwLdc8gTPkeqbwX2SqcVYoUL5jl8CYe25qcZX6m/kcg4a58D8lwdgLm11SuWw/Rcqt4
q4SMmsVIAKaBRuINc/EVW8u9ecSx+vqq7ueiQshKcvTyn7NL7LBWOy/VtXfZKtIbNOty+Ml3rrs7
prr5drg79untPvfDH4JEz/XxezTPGcPMuk3cqml4s92TZYo6IebKC9T1w9Q7yO9m67+iZmCHJ4cc
yJ01XDm3dTJSlrYeBOag11Tsj8yWcZ3YqTLIJngFz5pahnnf7Qym4p8/yRXzptLEQTHWKWF9gp17
jBWn40W4lPhWD7pA03uh25hC9ES0JcMMoODIRisxDTfTnM8IB0ij0RfTPDsMAD6Vw5aW4JORUFHU
3Hx8rf69c+Pqh11zlhmcyKQCqU8ulUdus/jq2sxFObcBRyyMCXrvsTblBptvJj/o5cvHmuhv8QNd
cQfJ/caJecHt5PTNQc9V87GpuYoQ5xbPDI2OTbdMfV78nQQpOudta2ozZgJX2Fatl8Z3oYuuvxnn
KSS/l1/brFAgeN0QrLF4XZw9KBkgEfNqIHWZMU9MXFU7Inl1Vu3KJJUZ5UAc4G4K/i6w1jHKdgpk
HcKPayKNsX6rKa6b4VNpWohf1MJmk1sW909PGcFuh6KpvdJiYWvNULHn2F8tfgnt7b3kvGN6nOQM
+2ud3vVO+RwCRsQSbWgQZaOkpDLOGzNSwkG539fKrdlD9mz9HCOn00cXMA3OtJ6cUq7Xi/A08Ikp
4712Qug+dXI1MtleSG1ZhLsXUfZuBVP8HShH4ouNc4T6io2DBRQsARdm8Wgd8tmiUuo95JeAEI1t
ISc0k7MW+nsH4zDMlz8f3IKQEBqUvuyFFFcNVsoCG+JD/S5Z+Lz7Zqa1dgPP4SB9YfFbXVpnNtu5
isOC+lRd9jucf7SPk+snI6glrg45pv2SM9UVG2gJ3WWD/TyJccb+J/67I/K9+zhURyDczKbgiTu0
obgdBvL+FCd8gyOd8SXsqbPjVUqSmyHAuBNLVhLpaj4yceyfYFHuPP7Nu1inroEkglENJws1EmkC
xRBz4vs74kFTBmVmOVXBhKtmAdshZrf5VGp0mJlCuSh3PHjqB5uUaqshpvHLtjV1nbi+w1hyl+0g
wfZ6tTes+FsfQIIHdRMaxmWEnguNrELCvsLPNND8C2v/awbC+Ar0mvCLUzmSR65MXVwcxSRKPGg5
Ci+h8wvezOTGIGxd8Zou3OIg8KHyWWqTgDhyK8y35RgRjdAJURPOshnfqWPyuXdxKgJVDkv2kkjI
PDME+FHRZnENSjEIeMEGebjWmkjL+fB22EJi1bzmbatUqSaYTI/04ZqUkVbG3SrcsBLMWqR645XV
xrvaCK2feCdW+8q7vNJt3pReasKUQu7PA4VcwATUs3QsKRXtrjCjVuZ04kU6qGYPT3ERIxou9Skr
5a43yKE8dP7wqtstZ8MvIKD08oMWGbls6Fpr5kkfH0OYu8aMP0rqawj4VoYv1ydIWDMRI8spvU3d
rh0kqiXsRHxTfiqw16qdnCibAjh16gHYVCxDnmKN11OphDE/eP9vFiNclR4WJzC50ENnSUJ2wreH
Sh65PSJcn21f4Tcn5SbbGzjKPxEglC71mCT4UXMNLy4ePEYSa1YecTcAkZdhRWBkUPGVtM+nxLSp
KQBQriQ36Vmdaq3wQqLJ7xOgpphu0yUvGE2bp6DwotHEmMQBU/CMuWL0uHVw4+aSI5V9Z41EFxOs
kwr6RcklgyUNNM33PlwI0j0+bIgarA3hIl2RK7Ag7dYeo8luTkrqy92j83bIkWsKkgNMXoJKJAb/
ZN5W6Yk+wl6PdKPFF5R5qC/V67G6TKb19xfWOOxQmoYCztP1FiMxmkaewryRKJ7s/Z6aQgLToCAL
vB4j7uTI7wPkVInc7WbjnrjCUfMbsrIWImLxVrSZ4G/6DzuvZqs/N0eYtZhFHtseZuJIHaF4NcDN
38FoZlEVGATnMWa0JrhEqcj0UQm9kOcnf4FgeyLjuGw6mZoIGR2xs9vMXX47zbonWD9nkFA8FGQD
+82b0+mM9r8C54NVqpO5HP2Nd1Bi2ZFcqWbZZ3oGQaUeIMnuq7rRvpl4O3M6ZtHMSySBthek0/H5
ESVCXc6tofLZSopjc1nB54kyzrLwjmLygqAGgUOUW98PVGNKcYQkb1pWbB9oGQAgELNoEpE0lpBj
aWsommDl0Aj45tF0NVJnF5caXSTdRX99hWhEsEshEsUpWdCT3F+h0V8i1XNtjB9yQjsNRvVkigED
lMxE3JdnrL7FMm5Ii+QtMH4YHm6jFI3kXqME2l5yiN8KataQ66/E1EWjOhquTD7LOuz2xH/rDqxN
kN7cWxs7PIAtmEqQJ8zlS4h8y0u107lJQb5baUjCaWEXxM/5u5Jbrc9MvdypcIqm5Z2KG4marY+z
Tkigsv9pFZymYvHkG8jvHypRA5S/ILJ+oWFOrpy0oziGX1IsM+qPrXBj/dp/IJxpbmv0Oga0RLHC
57fXu3la8qI9yqm58jOs5A88bvkae/NaraUgyWHpV12ByIkMg2hXeW+uAqqpoR+AhL6trVP3A4lg
FeWpCsd+HznBcV0EjFRVotXZ+0pyLIktVAJtiH15OLOEAQklloksBCd6Uwcu33w1Frh2HMAH0BD/
MlJXFRLL6PTT6kB4m1v8Km8ksITDnJAzWEC+FW5d/qh39x9STSJfyZzDIBPpp3BKQiSqsMuD0yaZ
3OcNULB4XeGMr4jRdT72R2+BwT0RZVMSxmxO3CwTgluUkjp5GntmYojEUcBcax3c4ovSNuAeZK0G
LdmXKGPZKwUcOqpmtoClPp/sCH7wCzuQeGEomXtmZZ04ua1fdz4JK66nVoViJPuTHfL+v2A56Ezf
TSyjEO9ODISxb0YK27foTnT3p3KGp405ctXeNGk1zQe2KENpMfbQifa7lJtCo0rBNl0FueJeusMu
cr0zGFY/QFTFzRfjFq+qJaoE1it/c/TjkXBMUe5AogMEYXH1XEU5cW5pF+ARLZPIgQUjZStNAGhQ
8d69yzkpVlKSpQnZRp4EBwx/tqcJcnPxuY4sZMlZqoD9Lzd2Bf6xbnFwIUC+pTnHh8tIXdroDmQG
jKLQg3/nBqep97SFsNs3MvQhRUqwLJj2F6BuAsgAQqJsgVbWjKXq9Sw+0E6a5Ayv/Pm63uhtgFAC
MN3/FRvnhF5M3jVhoHdFUoyeLIqcJTjIutlP55ZHAWdWSJAx/KBAj0m4RLUblxXZtgTA0Hd9vOjt
ZIlS37pm5JMNjBDOtbwmMATKitnbjrLPqhIZgpHBQgXS/aPnzpsajujXJEgbcgFxYmLXATvA6+rk
OU4iWOp7TuXa20Gb45EUJvlUUeOP6LiNb5q24geeBFPj7oHsD3hfuSd1U1a/SECwDdTb9uhvDE+f
11jDLCwTtlSuEHBkPrvZF9FDcDApfcKyD4/GgmspzhEmavrQFUlH+ZLwW3M26n16dlHg63QSzkfy
FLSzKpOCUfR7QHk7wbhiNysviKjWYUnRJfe2OxMPTum7RGr9lXPfPGHAY5J5lHvSwRokyeEDBd/q
7KiZd1UhY0jRT7NyRskvPWt6EnZG/MWpXcONWmMDPXrKydAM+cPF/8HnGB8T4JuO2j42poCFFKM6
/DjxfWTYQuTuTLE438zTNBu7gpaJBWJKOszqzte0i0MlvBwmi/57PUbJkt65FAFSp747/ygnYkLq
zit6ZwmJWSrRpsRGe949BLvGw+BH/aTBwmb1DkntqJdpECRzVQu05cn/fN/pIZoxu052FCAJMc3A
eHCuIyAl7WytRdfH8/n0x3SV1p/Fl+wc7+GxaaeDs1CJ84y6NaOlFplKj/grmCVmcmRUJDPbOt+v
8iwedCdQ5R3GIl7vq+TH5yXhYd3Z423bSMRHePKQwSekM+5mlYgBKIKo5BDrxgHUjSOK7rXIMVXt
RmX4NYQPFakemPxwfVGeJpA0ueOr8pECzSJ+jw/C4/tAYtL3cYDXoRdp9zNMn/0Y+paSWk9lOZUP
sW6IEIVarR6b5sMrsv0mXGaquUdNxgyNxNGJ6aFlFYmvcCzOXV2/j/ZSsbSjPrkC9idFFTU4lmEL
dm1KhXy+2GgA3aDDkiAbMDkPbm04acTLAP816M5T+dTxnMyMf/yRwNmdo2bBtFD3FNi8UoT4l/g8
bk5BEH/2mf8VlESEpGz8JC8rARJL5NOZKxh4DG5glWzbA9KoUvCG3LqZBbTgJuSKFllZ7oQ77Lw0
aSg8jDnzveYd3t0q8e/8kb8AsgjM4y+UDyQC7TcgXC4a2zWX1aWVtQ5aiCOcqh9WDgWjbH4igZgS
m12Kh6l2sOpIB1Q5mRavsMZuz5pfvL/pz5MxvSoCNSNa3QZgt2KAhnibD4hBK8RhnS2iIqeIX1+z
m/8td1Rj4QFvTtfOVwptu4Z4DTpkx06YsVTkc6MsGF+DypBLGS4AbV+6oEndwgk+r7oapcUoLWMG
IBNO9q5kXiAy1/LbRRZfSEuJ0qudEIGGv/UzsA5mnHFSyXLMm/h08tDILNCy4pB+dleigiqKb0Nh
o/76QyqUGgHdmlvntKbit2y3sr7+y4qs7cqiUGR6UcSXM+38GaHHeivle7FZoIwdzxzd6GVyypAA
qjqnP+mmmFsxyrVwUF8fLxc3pF2EmDi408Bf+zd4tpZkNnSIIRCdnO5/Wxp7Iu0odynolBpwFkUg
tmP4uE2Gr/eE3XyA/qn1tTPv32W/z8a8FfsdgN1BQneicyYvI7Rp+jTffRNbJNAi/eTQ9av9Zjtb
Yyerjl6XoLMJKOYkghnRnZVzB3/OUVVqM0WW7eLiLvN/cG8blcufBSpTGCw4j/6a0iY9BHJjzOZk
Guu0Hv+ebKGoMxJnTL36ycTshGt2XJY4Yyrvfp20b9ABXlHzE2yGsFmaw6IGdTtqEIIBp7yWxXet
NXgffOyyuYZ+i93XUSmO9AkSPKwkQX6lJrEmWGKlJQ9GZtKxNYCv54sDxjC0Dv2UoHZYnJ4phIIf
oyBMSSPVMsMcA2hstbE09QsUBe7ijMn++gwVoGfkTwz4OKPHty4YrYR5b0gpjQ3Y0PBciGcqcBBI
48mzo6otm3HxugEjfq6db6R5RmaxKaMxRnAsf4n+L6bckPpwaTMGZapjeo4BeUqegsVQucLrt/E8
jupAJ3pG1SL0Tkn37Ug5tsrhNUcIu9m1EWb2frVPn/1Bi6lJp7dc9fBFqh4gVx0F5vRdPzQQ15Ca
PuOGcGJxYZ4in/ju7JJhDUOKaJehHKAasqEBOpuTHWMhZDhlAN7wRIe5WOl68omAToW0GlR+p6uh
xThSAAX3reKFbwBlQUsxhZwmGGh5HB/+25tIK9rtdTncwQbqPxyOnMrRS5DujK5/hDz849o1cSEo
GgqViaJfRVgte4pdBLOqs6xk4bzrGEzqxjZhFEjGK9PLFshwswrwZu8tfGeVfDjXlaLc7GpJxF2R
t6kFHKvfI70Fn+Z8GsxATpYmO3qIlolyM+XzmKcQZr5GexwiB7bSBE10VdYzJqzGqnIgZRbtBFcP
8vWQoxEOC4c9vttZ0a4YsxFtTej3zbuV4y5800tAy7pLENLgzUSAd6YiFcYdlTcNEvRJ6FHxK510
jyk4XUV04ARz17VOta3Cu/nIaTWVZ7VIpgCgQEj4s5S3ZOiGZvCTSJihJ7q/nIC5zlGjSSsf2em8
xQBWlfxufIN/sjeestSehg3p7GK6iWvo5OqmFZFibJ1NvBEdkS21Swm/IdkrCAkpJvVSueeAvy8j
dXnvdvWS8KYi8oH4rfU/OryrzZiEEf2Az9il0q8022iBSc8CHWqqMyt59Ueb6q3cP7sYLxu5DSbS
TLKXoXLo6kavSHll6Ge0ZuavIsiv/onV8mp+fbHYbRIMqmLtoEabyrFOwbQSD86QGrk+ssTFQeNt
9FBv3rJ1x7Yc4LDYoEcpI+zVh7nQ9Vz9Pu5n1wM+To38Wl+4Yy15dOskFoX/hwuVdvzCwgS18kGC
xM6fqPH3H+1cKkWuWlBsKvv6/3UCKEODmqDemDLhs/V0X5zn0/8R6f4BKRcywllQ03I8FRNkf2qR
m/0B7oB2MQUJLWaG1NbYnmH7LrouFPE5TdeBgAjHeAHGv+qNYAO5JZDyASSFhYHC2t5c2uRI5YfD
G5ALBoiGZ9UGrD/ibLyluEIcESAUoWfwsSuxKRldG2NU3i9CBRVxH8VXEH3TkiOtkAwB3tl/KWz0
YNCyIqQ3/gxvzY8FhqI8MBVxkBYzr8VVKLjujmsaIdYe8HKZWowH3vou0vfFNG3TQZIYP/PVus3z
OJRlZk18OEMII0bERk+CwskHFrcE+ePPci4w46pIspTym9xmB3aUTtvemXzcw8mJ7OblpfV2lAhJ
H0acYMV2dbOsPxd6WeJ64mIfmGwRX++NR705naps6uUXCF3jQdzqnOftpQHBpqCHVWa1jRpMF9VX
uRlZpspT0Nc2CubVlyhBZI0BaTtwkVcsNfN3sAcwqxHz/4tGfmDAkghRAh7QduhiO0EBttQ1DzbK
5Q+cAIeD03JDQRY/lvlt1g9sdOwES9++CWk1DadRDctmnhHugfaM3EEVB7QVTKy2T9WX3MkcUcHy
HZqz/IniLOtAIdF+T83PluhSJNOTnyhZVQ+gq3HFFX8CU3sqQhDx0V9cRi0eFF656CIS8R4NvI6k
/xFgG6hX+9z+V4ULcnTWg/7CgU0neF4QSVHvLJ4QAqWT3ZeRwiIasw8QaW8bcQcS8qODUAXJH1JU
SPfvgHEd2JD09K0GzwY91TPw/wgCB5Ok5Ruk8GPoS4h1JwzpsJphyWJ/h/qNjkgU3OO2Cs6qUAgX
3mlUhs//+ebw6SHJvu2zqKqkOIlBLcShqmMZQzBYF5pjkWdfkELCmvmIHpqeNOX1mAcYSDvVnGz0
HsWXlQVbldDKtmeYa+LrIamfGKnMeJj19jHvHMwKBgQWxTtICfupyQCryfU6PHOErhD20QkNbaBg
hEyDm358E0wCQUBuXKUv3vecFxadADfv2a031dTCJEExR5ndPRjNv+OSwryfoMqf2H54WoHPM8I+
Fv6XrSe3vvzHc0iUXPut20QaQzowrVau0qevcx8cJeMa0/RSuW/mE0JpZPJlZOZGeFzs33/HP/db
1+2EJL183235IgVlLZXe8cplZ2N2guj00nhW2Ku0OIuCSg7ZNZQHvBeclO+02Pnwts3MRcQNcwiD
kqOpzfJcGpmKSgbNNLNDyGplYK1eUyOo3qFl8JekGto1NA1YtokN31bOAPuCVeR615OqipptJQk4
1vFotbdEcsNLdDo1+ISNU4aTOSMZ8y5vKes2xyTwkFen74CVkBjz44hwjR49OpbzP45rMXjQY6bw
CKi0H3vaOFF7b+bYdMkZWpH6D8enFAPWleszxzpd41Ygj6Lp27jiGv42kJJPBGK1H+Al4EZo4Yoz
wu0TTrb6vOBv2ASwX23eHxRsXKIuU47PKgm+P5vqBJUGyL6L6SZpE2J2tDtXZLULpuzBIlRoxvqD
rjJgAWOh3cZ7HUXoTX2SeR3QLfrW3E0614+Tqk/CPrQHiEhTTUw6Yr4hxYDlkY3on4lqDWcsJBls
vrS1o01igz7adZ1gjeT8PBqnyomq+bucN9DKHuTxmIN2CwKvONWsL4xhQE2in6tLT9EqOXmOuIG2
VNLi5fxlv3jY9bQBHSXRMPWfi5v3tn8KgRkVDsIscmNTtoVedWlIxDw7yvjjMQ5gmdsltm/1DpYH
S/3ddcYNOrYvTQxNePOeUxbAk+PA2hJvEY8sT7czrh5e8l42joqKkOArroJJEBJqT6981TysaST/
ZtpxSC38VktCPBbV0EeLlThGh8+lGkBYhflmtb/wmiv/J0br9QKOL/rrs88XtKm+r6NyaNzU0Vpa
S82BgvX2Peq8kg7M7KHJc+uQGIODZCCu7Czqzx4mKSab83AncnY4Fq+C/azFJbPS66MR44adEqk5
lES2XgjODLRd+UUFLgW2i9NIseAMLZZ9NdvUu+0mwcRc+PZLxV5HHtKzT4TTADVHZhLPDbp+x4hY
1XdPKPt4CkSX4nfUsxn++uNdRyUOibG/dhXWyuQNedIMEBNZGB8qab53F0fXDXeIS8WWxacy1PcW
iO5W9CoXQK+5Eof3J/HN6JG3U/OXRYeVMcPha0CLFl1uF2o8v86/qkvstiUxHJfFKreNDZOTcPuf
PZecOp2K096A3omYLRrsN3B42H6BjuiqT4ArkRE8xJb1B7CSUwDZzU5lgEOIJ3RaTLBL+OEVKJ5L
azte13+SE1IOk+/ShWVBxIHGTi0oMqCLW1zklF1iBdDHRmgSbywYEfXWM6BC4VXGPWY1O8Cq3iL4
egNAPG3791auZaalhRwnHPUZyhqd4xiNIvUAkAIeYUKX/pq+iLxFJSkKeCtdDUL5YeJA/EB3FjrO
zvBAC7tGinDcYuMypW6KfOnUSyUfxkov1PwvgTFM7OpsDoLwWoukoZD8IpgrgylgrWu2Yz1FZMDN
+DHuNlBumcHV4tk3haE0FTLpLaxsNCUAMiYdFT6JuDSopz4oZu6K/YW+neWd2VLhGdJT/KVUpH+R
0ps2WatBagBj4ISXnYC3T1Ccx0GjVDI5VOdZ6PMudA0wiyqpk30BQv9Ad7zeCgzJ7hT/D41g9KrA
ragseFbneK7p9iWzij/N5LwFtomgOQu7XmcmTGN/WcJhl/AMew6jTHWp4S+MN1zsHlbBm8qyP0MJ
+Ew4By1PC4O+rU2rwpexWihr1qP0qzzF24fRdbqw3i58DGYO8xVv+tx/Fn2fmbGEbSwOHZzuWnWW
nf+kAZSKHE0JzU3ZArw+bt7e2iH3AnMSQKAd77/Y3CCBP5LwnnM5PKbAD5WuVw2Vi+ESM18u69Wa
NI5Pv/G7XKTc4c7FOdP1SwbKbeYR8p8YU8egL87a1vzXaFS/c14SEYhyZf63mwOPgv8vkDo4JrJI
llBVvx4hBdr9I68Won8xJP/IUt/WcRypDzEuqQlvUoOwSYNJLEPIDU1uzB1QRKA0RtOaQD0e4lWh
S/MlE9sqrkPbDK/dEA/eVal+HdJO++UrA8Ka2JpLkWjViKmnHgk20RfcPcwl4cQlEKdNzF2k6JF/
upoGX60TAUbM9lUdUDEFB/x0fbzJqnPaRUuhr22vU1NfoSQtxxgBqR/6sWzlJFipI/WYGrJC5HIB
0fk5EHP2wZ9Zxo0nkrusF8czb5us+wQIlx3jywpCxUycxnBMZlcjwoUs3OtcFjijfmHHI3BV3+rH
rgr6LxWoaJ8KrMHrUgHTmDxPoGb6GhQih1TgCCiGLU0wPTlknz/pxjP4BZyc+4+l1qsIwCmnxqC6
1nQukIpW4AUvn55m5YpJ2ZiGHJ4x4vPjk2+V4UNIJlp4uGaKxZWz4FlznwevNkFsrAF+j0Yihx6f
Bjb7LhGCR/ztW8Tee/O81tisS/5A3kELCtNSow3sQ/cD5xyK/M6T4601YhNGZgkT3xSAoxdmbY/Y
BTMJY+W+NFrFNEB5FlNlotpELwIIs6Dg6kcIRxyBCgu3inmnCc01Kz7D+q1bLQngM9OTdRe4dRSz
MyvOJ3seubit8tY2r3m+KzIj6MCY15nbSaQY+WYAO2AlqAj81m6HfmOHvrNw8KSCfBNtYVD1ttow
zOZGIyqgCiLyToMS7Nqa50iUbJXJ4sA55gv/UXIWeZhw2dBBlufI/hVEoqfw+/zSJjH2jdMX514S
ZdrDl+a3mF/MK/6ZAUIZ7pPc5U0kwepOIb5L0s+NNzLWcNdSjXZt1drlZHm/2/JTG9gBKWzboBCl
QfeknYepgj91rtr9PcEuNN12landeKqkydYcpXxZ2IwdML27Bj82za/7GInBxpNPMfxTXJkHEOPc
/s0EEzHUObabThSYKxL/3xYk4HBSDlc/oSO99NHpUuwdToi9N2WQ93m+l9MA9xDqCpL4MtQqaDiu
7u21GGm3zF7wxGwiqSkD2KTKL7qeCuusV2um7S8Ru+Ju8qKBowkInstRWRijYR2Jg9ygYb5PzZMU
hJkPraMeNX38o1zFZ8/8OtT+dAVjEy3Rtd2+gVe9fFicXyeHCBcuVWcNRIp5G+yk1OqSKilQ1kI9
PsVHBDdGTHMeWNbExH3KKl69mnfMQuyCTEkBjs1EQnMWd2mgo/GuXupSUgpFvWcNKjQ2vArLsc+z
JDWcQuw7124BcciqLO5/l+8BLuvNuAkh7Og3lgRHX9jz8P9cDFDcElsZCtxQ4fvikPImxRBxIHoU
p2l1jYDscriZIhH1BlJZuH6FtzqK3P0hK3SZpSkIEkipeuFJRRu6o/w0SL/PWIlJ7mihUCyvsIdT
LwSW7QQeyQvc4fCMr3DDC8xzpce5Om69jw1k/yNS65Xh+k5nmOoucdAGwBdcTPknXmHy5J+geahB
d8AdZAEwBAWY0uxOOwbFj96/4VEykzV4ieb/vPqr0xym22uq5JF4o3Xee7tVU3mbLRoPRwgoyWbz
idRT8YCmovOJNJdwu3jlPGkNtJHCAS1x/WPXAfUIfnRGA4ydzU8j/i1JI53VBRGsOusVBRBhadt4
xRLr1kHU0401oP8OTenEdRrPK6mcvUnb/4OJnfusHbjvQAA0jmWyXgtjKuwGW7i3M8vx2lXXI8XQ
tuMxJPQfrJXlG9lCFtlBqbHwS4nbFJIJ1m2QmXcgCTFxM8K3ripUFrXPbj/eNuaiMcur+IsyV2CS
V0gEBbC0nsunSaY9M/rph5PgJRV+Vlj6bBTeF0qGOMfN9RSerhNNrSkVNMbD8vlbWOKJdyezhFVc
Sxr22nLvksogXcSWMwwc2j8E/wIXBOeQ+BFEO63dPVGmtOlNm9tvaFPzGU06K/aj2llO/1rOqsVl
yMQHmeJN+ZCCXVFaKLUcWrSVozhcmZtXh0qbQ1jH+1xGY8cILZcvPEAXvdxURhy77ww2uoSlt4cN
SiMn2edcjZZ26L4b9dpDdpZsTsUk++sS1qit6dFSli3pcn7T60XWhVWQJwx+bYxCCLfUJ9qsAvO2
ecYgaEtSgAr/jfZCMh4QArGUhS0UgoDQ+lhjp2sD5xT7P0C/iZTN+Kw/ULxQ9oQck7QW+dLrx+qL
g8kYT5M9BnBVBjVmrtaCTyjXEKFrkulSsO2u62733KRfQyH4Tzjox/HALUbdD65vjsSNghIl9qtj
65L4gFp4hFhbfbYKYOudmweBmHnzzSzbXgJuweHrwOtKa6dy9h9Hcnyyp54/73rdsWsdQ0vaZv5C
WaEUcuYj3VhT+MqeXRxSvtUxxvw8+vT9UjmHdusrQPJpfpNrRegFA22JcC/8kIO0rePPQYOb22MA
HEFnmH8fEK4R5rU06mmZU7PLccxQuaSTDuuHGJS55/9de5Kb80VG3TERFMt62pBLjqUkKsT95QVj
ozSeKbzCWYcUt7B3bXLdexFFtOxCOuWV3mUlM3RmaWa2LaKlEA7XnVlSxykk9psekM3nGyV6BAhn
kmDiFzpSUYvNanc6H65HbhgtG9z+C/hfte8o6rpcpVJCK3NkThzYPi31L+1yu2998PiSE6/PtIbP
8pWNpyVhWF8o9VE4mkAkQzfbvy6VQX6qzZruRUbCPgbEcPep7K+s+BLwotGZ1hPugTyqd44rynik
amesqZ777eMVAwl1XMcZ2q4hTuNoanwoaW/37eKEvlItUF5rxM4DASQBH32e6wStkju2bw4VVy9W
kHNq8usnHT1+inamqlE10tJXAs1fQjLgaFcDXn2E0CFLdhGLGHW4OH00lcKjpuYRqu7sCjg/O9Mq
Z0QaPVra+bjJvwqPLS7j4hvFHOAxsV2P0ouEdUhhhkpIL7rDx0NO2YqGEJ4mC5aA7nAahqEu+Vcm
zFmLKeFEt2t9lzZxFmTs8zXQ/NsBVN60WRNqVHkAlYL5Sp6+Hy9mpPVPorkgi9fIgmNg0DTiJiTD
wph12FfDsExAEOEpX61I5n6Ttjw5pa1HqvAz7AJ0G/ZBVg5Caw7JFEzbnKG1Lg6LPQ5UF+8AadeY
TSy+0BcaZJE158se9N+uqUHjYacwh1+dh9v9lGLrrEZ3JG04IfQbjysROmImX9vGy6RHBlF5eRPy
ZcF91YcoMue+O5mlXTCrEft3E+geyKg4oLUyGSzAjOEX+hdzyJqFEk+kYqic2rCGWcxzhloy1ED3
PosSISggZWkGOlYxKSGfjZU+YeI0mmIsV973CtbF0js2cDzGNKHy03Xv/BgU56XbExd/lCOUNrc1
/RpVQuc96dzNDYhUWvoJ7VTIais5nhKtJpvI/l0GRKkU4i4FpAXbuLJ8tVQ6Mprgd/mVR5ROPQca
PCdcEoKgIqW/o8pqHRmsWYJXK002UiXGnTn3+D85QXRG8hiS/OpEHe++qHZJyKRKM8yeYzf/MW7W
eZnelh2mAipxjqYrMH2bc5LMQ9DttTwouOB6kDS0rASgZ/Ee0lay3j7ozEJDvkeg5YEctV4ZxMg0
v7hkmkwKtGO6WUcxEYPl0d3SYGzbimQKWSWXIsIBDj20aOHfv9XsezahPUUCFFf73JpmM2zztHX6
GdKbfVKxP3h2aYbmDRU8Ot7ttiRMtxTfI0opR9F2sMLDTFy+pHve77Y/nO2VJN/VADJEMgR66NAc
JrcRbFxMJnEVM2XXXCa3qRienowGuXJO2TYaWb2QnRAN5rbzrCgkMLttxgoKxNY8uLUniTDDF+nw
3Vr9ZJEybgTvieh5yf3rhbXQXF5eBINsSp3hdqCzaybxbq5o7yi3fLTbOx2k8WG81inTqXsp0NAb
IgGu5cg7FOBFJoOojH7lIqBRsK4NP7vtYqs2Ny9iQFJWoVNLa9a8+9J+KBI1rUOBLDaxdP5cEgRl
BUK5yJmZmqDBowF5HAMA7VdtCKX+KnECuCvwPuc+D2mSyAHV9pHbcEL/twPYSTrUv736BIXvbvaq
oqgtT0ib7DJb07U4O+UPj5I0BtAmeMokowjPeYK366yosrFwd9j9L5I3gQm/ZdN9GlbCobChlpEC
PfSGSaeNTYLLba3ZdPHoAoBEsNdpUiosR9Z2lsNgwVWS3nAyXGh/Vkdk8rxEK0Q4IdMh74t3iPUF
/qSCWpW9/IHuNp2RKRJpMXbv8tomsL7kyIop1A6Cl/g7NcaV5eDH63WLHuWNeJ/LW/RxVX6Vi1Po
xPpRBvtY22IgxnBRl4LMCDVfQHQTs4XYQIGpFE3gODOD6U8Eiwt+soysKEjJVbrDeAWOHrQBcLaz
v6hOo++wQlSkVtSoI5OT1ljT3HmJgb7Y9/fzl5fbRB4tt4eZuUZpKc9zkrQkOk/NedGbMmgKXY08
+apv6LZjAwg9n12Y8EdWzTOma8J4sAO6S6huvsYCxBo0oGUlIYcnnkLsSGGNkyGle24u3FhaSDaN
VY4yBheApui4g+b3Oz/GzRI72TGtclK+2WcZhHCtZC6r5ptX0zKnhpbQxlzsbfgLTQDWLaJLcDOH
+8ZemdrEdE3ShqdaOIPDLIC+JpdXb/z94Qs8KstfZh2QA64ugK0PkgSesJxxTIRSZBArmcsO/Lq7
dBQdZ9K+EsjmnKrfJ0HFGKv6v8xCtdsDDTVrVzA5pRu22uMdTycdO5r+Paho31Ism7EiRp600OmE
JhLBHPFK6Z5qe5tYeqS/AaHsYP3uRyCq3huvCct1ihVKSB+YgqJkDL81YA+LlbVhmDLGoKsxAVP0
k/rdcj+Q/b7h0l/AoNDkgKO5GykQnGw/PYkh9wFS6gSHhAQykpj3L/NVISxPXr16/P3Il3Wedw6G
KCs3uPVkgo+FH+K87TvkWBtmejm7z+OvTuBD1VpX2yf7rk4nydJfpzzG4NmaSLtluj3V9hIwzuhX
1Jjn64Vr0RtHl1sPOupOxfDs6yiWDZMd8t1dQvxEVDaWxkrfXu9iiW+73sHpkKAZyP/T/BpRmmXh
OL1kinj+XtSThRgMwr6bHssPBDRDJuGjiHIT6ug3gekgNbw+D7sMQViS5YtKlRYcRYYCkJ/qodnL
tnI1ggnPn9Byn7bDuAM3hiJfpcsOoGAAOLRqb/DrpSFlk2t7hnMYDVzT80MJFjFJcAgIDahPJX5S
Qey5ThZk/OjK4jJcEAcV7GZhMY1BV2rRc5/sqydTZqfmI2f5lDWulXlM6owfgzUp1nNs3PxWrAEh
dsQtEl+Lj8sMYz1g84jxXPf/HwSMgjcE0+SCdjb2cCW1m1O+ng05FjwyJZeMZ7g5gwDKwmHXSyKL
6Zd3+Zdpwf3vyzqoKaxLAz9LwS9224ECeVJ6Jh2IgVQCngz/Ta3RLi9jvyn5XO3rssPYKe/QrnLQ
neHNN6mLfbzLWyTY5oGxG+oQRsa7dnEBrUUceOYZcvdRgLI7zNAjVv9gLToaTm9AFNT06XZ6c5LP
CqIeoawTSATAQCMNumQmJ/wSGVM4NryJzCQ0dSVT3Aj2M66PKNLrHxKf384cJ6/cM8MXq1cA7AuR
jZVtI/P7U7ctZcZQYtoIVvbZz79kaARNACUu93zfMrcoWiIBZlyKQaxvqqcZOO36W+NGqYaNzsd7
3fegpi71CSI36s60Q/1GD10Ht9hGQpCK7ZllpnjCFo16lkLFjS+7SmQp6bR9a4lt2S+0HvVV7aJD
ZhusCUXIZxgnCsjN4/lL3HRpbwI3kQGZcGEjZzF4pRxTwdvk7qr0Vuh2wkSofiSK88WIV0PRVxob
rlFULhhxFItEey2QR3UF0LTxxV0Fb+c1Yjyo/b9s0iQYUD9611gabHXM/TYyCOZKu9orhkHl8rzP
5KXgrdK4PSmNnxo7qBtjIDLRhjswegzCFmSHP0K8X155PbVxM9hKtGqNJ51n++CAHr484X0gfthr
5Jzet0DcOjn/rCXoV5BBq94HtXvkatlS+jnl/IRNmn6YT33PckarreLZN6sGN9NJS/EloCq2FbZ2
/bk1Z3RVbQaqT7hmT9UDsi2FO/kPDabLYNvn6dzQzUqDwvHCq7SraWy+FIwHpqupQvHECod1j41h
AmgUSJ5YW7ww4KQ2gT1sMgL7BJg+G6r9g+lrW6wBbmSAioGmAnOXM2ulp+R00T7ZY3vheg+L7cin
+bkHZEeq+TH7rVmsNbjyjkgMSXI4jQ+nXR+EZFwmfpxFoUCFpIfIdpJq69gMO9TSt/3rsdFajHnS
o08H9YMG56DjqvecdEEZ92WKI/wdjgnsksa8i05g6VET7rpMYqx7MHQNY5v+T4Tt4xH+ncE7ZHLe
DQ1Hqkd8mGVTGfxbKjMgkR5vq0k0EmJqR81kGtyJ0t2N4o9CU9CxGnCJxxgvwpr9RlieRwKlWIM0
Kl3t2EeA/cAAXIhkCQLlsf3cGpPt0xX0t7CffV5fFDy5SRQ35W2tmPZ+j+OL8C6ByMrKiKhkxAqS
X1AH99VzBr+aCE6tgxo3wvfE3GBRmvV6/YNL1dMtzJtbzTYUA5mvwn/dAsWgsxgcqD8E63HbrrFX
IoAkjZ+9RDpyKOGHzB3ZjMsL0OskAXN6lB9/C3A/eBrf8iSBKoPY8ts8hCRysVzPrZLabGwKUqJA
Y7OtqVQEbHI4g4Q4nshfkpV2/wuRJppAmAPnNt2de8eUpdVR+Im0dLvI68507qFqp1GtA7AmUopK
oE4JRLGy7zQqrB97uWNmSiqZLu7yIFNnHAp9axZtOJx7OlGAnaF8sWtfb2XCkwoEw+hlT5ZSRR/A
zEQthiBhPMSxh6f5SHL8O1VuuUZntwr+IuSjfj7/1QhH9A5FZRYlTo2yCYDO1vwklo+mh2Qw3gue
r7Ut56K8Pe3rXhYExSq1iHyuFo4IegjBhmGmJVQFrQO8F0AbvBzogspsRSV7XbAmVWLu+fJAt9xJ
F17hibDawGy7lrwodIakbNKID3LXsludeiq28AYCs3hGlDEBPwK39laiFisssiL1ZigB15ZvI9vh
6rnqXn/Wbrngu93LZwa6OTbju9xw3sFzj+YeKFa2oTYdwKCtk05ZCZiPHD8ONneTY1NnwPQGclz3
EmQmpdLXH67S1VAnb09Yl5DatXYBYdBJU0uEsujx9o849fiZCYeDNTUZ821prck5boXYhKDzey7T
zraUWxbihxt36tzENR6BI2CXwqqJghTfKrACNZIOLJFdGCH2olD8/zPEWFr48HthoJpglokKDdD6
aN46Dqqn21a4XWtoDEbt8MDwO18l2qAlAiPHuajDcVvjQUMxitGHkRrGhTFECYlWKyEwJ2YPllMA
LxhMJOAzr0OQMJNTSWhyBTHHWBT5rdlUptmBuyjlekAGrhhYXCNrHdKBCfpnmDv9vnFJ1NsJbtx3
8nbpJY+06VtCXh4ACRsJd63t+yYqlDFEzLlXJq8tCvahNXaPQ7q3OlMDFmPZifYTQznD9uOoRI33
DOwuMsRYjyV25JUW131VJSrFd3XRQRaQc5vqGNqh+5EJyiFnPTBqomibIAXyXFtpkpW4y209zH5R
t4RGvbEj9cXkMO3a1xb2fkUip/KjfPO7IEYRg2KLzwxWgRczopUb9BqjJvGxtEo3SwHZPEwrd5fJ
zZwUNGNV8sQDmB4GfPR9SvFhCWzf0qkJhDMnR8CLyLdH5CMMFfsh2Us2EbNBr5ZNCUeGMBTN/GIH
y5+dG3hVzQjd0CCMOK/NZF74Xp8dX8MqGnH2LFlXYRLE4ZLf5ldx2AG60mqrvZKylax3lvhM3MWb
cfSQWlbzWTWN+BoSgjP9uvz73giR7ac0I/jXLdYiiV6Sg91IllrahwqNTBLoTa7DRxkPQh5zO1yi
7MN7RdvuptzfkA2mZgBrdPwhl+KJ++L3CfMR6rZfgVsb3VJBcfnpJoqlBzKMqDcbvrF9U3sVAEsN
wJcu9i6Bk6XbLZL2Epx7spJe6JHAYB9QnO1SfHDq4YppgvOqmbOFhLoLU/zhgGLVJJAybwUi7d2e
5bV6dtu1IqwKzWvuZlEtwoToKZk619s4A8TezN3maCH/hJnPrF8lKpB7NoXJYUXPdaoGMWmecUk5
ZVesReRjU6J+rEGQE1VzgX7XWr2glaSgpQEOg57qAzo74+LIuvV1WCklGxX8ooYU1y7AdVNLgQG9
7PMihPhLUwOYgDT9DLXv92F9W93akikyGlCUJJxOPoYRynuorZ16q1FlRJpQYqHjhUXk+WcDt2d9
HMS7bmFUYphNiQrb7O2m//3nQkr7V7yKXeLSwxGBD4sy0cgP0fXZPXDIVWlb8lUneWYaM9gOedJI
Pr79z+8YeD9U2F8SKK3Jt738+QmEVM/qoMI2hFsEtHwpUYOAmRmR9h5Ysyy8JYhk+Ub2y9GIpAKz
5HjxXzNpApoAv522ZEgSJlkXae8G+irWq4NDUffumkIeSydiDx26LTrZQrRXB/K5oMc/XlSQPsH/
pgNyZZcGXaWN7qQErA/gQRT0btlQnqTywqt37OlAk5GwF5/W4Idai5okJf0C62VTGVOevILu0tyC
y3/zmCxOxrgzG82m9N+QjjeHWXMrRd370NnO9D1iMkxZePg9Tryo8oHt1lj9lY3VKn0G7ZkbNSyo
ijTI1JmVoLjX1gDh5YpsNoneDAfcq6/qbwiXXQp6tXLtQjnNgDK795VV/BVSkCWqHNkemQBdaJFq
8bIeWMZkBSSUg5d4Svca4J62kbJbpf2US5Fe+lVnr1CIWSIimcznDv1qzJMA7Q8bd0c/jtDsfi0c
GaU0QKnHb54DupLHBV+iFfNTm/vFRz/BOAmzqNcEgIiYX2FutyjYMN80YualQhO/Vk/OLglZvNk+
u4hAzv6BO/J/YQS0nRdW44VN2ay7jJgpMqZvUUurRttLAeiezBboY1lnkpg8pKJTaVyXzBKb0f4i
m/ZB46P2NUvW3xIrsKekIbjbhtUZk3UNLBzJYuVJ5u2qtImGZ2RzCWU9YWOWzYGzPr0EUJKiM0jZ
FgX+JtfZUX+vL71a9QZbNd4DLDzQYq/tSSW6a5lASyvyn8zf8Aw4wWqpcbj/2GFHtBRy9DRra8BE
JE6vwLH7RwLBl9e3NGGfqFDwwKKjo0sMobMCpyxX2SbXZf6i4rDuWOkVZCPy9o6KrhPkn7tFeurB
72azf/ZG3qXldPmx2NvpAr4CNWsuHPHtd5FktHmC5pZ1rcG5GfX9mSjG59AxmyGy2Oa3DEvT2TVW
ATkx2VOvv1UadSA1ZGAEFc+Kg0UPDvx3fHcKS+kQKJyF9EZyzBqyTLaraOAGt82r3pdIh+DpqycU
IB65EqB7uxt6finKkWXiOSVtaAgyEZDDaJNh0syXCAvSIYlpnvnRAR+eRguSfAJwANibcRnBTxMQ
0VpWZZ3qj+ikNiS8I0SqW09Zxe9bChnQrvA5RNlnQqUQnGa293ktZ6A5YqsXDMP2R7rb/EvHduR3
dzpZIq9Q37P9vKMWUBaSj6+6a4rd0OK9///RK+BZeC2ZZL1GfhyvwLHyxWZ2j0TIFWTnovnJZR8q
SfceIwy+EBB/a16gqWV5o32J5MEnjrI+RDyUsZHLQmkB4m4ejG04CxDRDu6V2zWHqxg2KeW8yiwk
yV22EZYfLU2E7ufbw2lCACDz3QIEfk0w9Zxun7UwbLFYTv/an8nw0arP5q/CM6ekRMDPR6qZWxSl
opDOvW10qT84lsHZPLqDMlbs7eEl0OZHz5jx+REG8Hh0p01D6E5I+zANOXwWHMkIMBDpR21BBD/T
A9gzfeTIR/5g/avnmCp08w4v7Bn4qQMMyw387H6eJNKL6mp6szsqxvUWbWv9Lk6X6Mh3TenuMFmS
wT5S1UjzH6/Iq6ObFvlagFC98wQXBzdGYg76/vhNECbAsIRpi2MSOAIEiKNRgko2mJWNPR2iOw/m
XSvZcj4UJgeBzk4zNCaCNlLPZfPCb7+48HX31YUKDlgz/Juy+eQ/XcBNJfLbuZpiVZ83gjpj2bht
NQIpVjUVpajlYAKXQYQyKW2etgL14yLuakOzm1H60OAv8SsP8YgeWRo7F9KNzxDBFd6+sNyF1hAe
zR1jYr0rY/4XmdzKoXy5xQKJ0STDAot/WZqnImuaEwZx4anpWFX9J3g2XoYbG4UqP7NvsmtqOuX/
Vu/Wc02I+WI2QiSXH0cc8NwLbWwKAwk6Ks/0KwmR9lmBWaowgn7LFiU7nTfj48XwuWAMafPJvtO6
xRO3lmcw/MESfW5KvKKv33rKL1CcMcO838sbkzi3yy+9g0oulhigny189BkDxMXPKWpsDEH5/aXb
prJaJCnzbaiR81k9EmTEzloh0zDf4mJAujIZNgxaV+h7Qh4jdUQtSR1jVPjyP4woSaEzx/isKd2y
LU/nVM+r6KIDpp+aLPKzpBfreQY5iMQBrFuJMlELg8ez2e1dGnyut3Jo0ghGg5YlZQ4RT1qbs7jw
2MLbHZbsh+H9BlHCktD/MdyHtFDDrSH4VEqtgwLF2Sloyy4Jkzi8YnNkyfbN90A3OctlHWeF6lbj
EMCp4FEc0Hzz5VWIgEXF8WUucXUTIdcCoJac7SXHzuTXn7xUMVYrUj3mUYM6LmsAg9fgkV+IDQrY
GGyAphZItnQIH4vvZ6EJLChq268D4x8xOGEDRpc1aIlbRaz3n+jV7ELJQ2KmQ6Y/WXsAilMFt6Ef
/ona7CGtUOIZ7gJl+YmTPx4ePgykLVUQ/0c1FVT30XQ6Fy3SgxPujC/dpR6cCe0Fo+rb5K2MZ2Ha
FKEHUIWf+wMAz7rHRX6igfHHVkF3oYuoLXr46JNa0IqZ7OSDRD+xL003jS8Y3EfU8c5trxk7GRL/
DYd/Ic5ITHfxWWTTcYp3dgnCwszYsd3DgHEzwN94a4x37Dc1SkOEdC6ssUD4MouIjBUAwZ/Y4Imm
l+jz20Ue0kKHopRwI/aC8yiEWuqtmnSHuMYNDWuwA+eyn/7hg2pSlijKsZ+a+3+jR/F7cvZ5S4tP
FNlJpsyVkqFcxHe1P0StmRhVNUh1Rc88I29e4R/LrO9gUSg63Cg95ouRx7DFzYjC7T/uSW7J/Lnd
pd+mFDVOWopH2L+a7Oz2C7jYovThslkHMaPKDj32y2HxqgQVPgR4OnB4Mv/CgZqdvAvI2DLw/pxC
aNF3Ujxfz1dpgwOu3qlphcNGmXiuXmIm+UjhHFC8DE5D0M03p8AQQ5kK32OXRCGjx0+Tl0edaawg
P6+LE4833allXyj5cb0i0/tBDK5syMcgvvAs9gnjztS6OSrClZg2Ucg6tlcBjnsRyWI60li7TnPX
je466c7vlhDs6QD17cf2Kpre5taO0J7tnIVR1KmTG5GsTpcXOOhdt6nXYLfEyaPpitDH2th6HN01
IvBslY/ag8sln33GXxnU1vaIxiZG+RdlB9fl1Fl2bL2eYy7VhBIcZCLn3gGuFwRO0DG9DcY5N8kH
zOLrqgwPpn3JG7Cc6CO91mQgYww+UGofpets2vFL/+9lpaXa6dgmR6H/BsFxXINPTCfogy2vHa1O
moq4/QXYYwboxDsRMCVyCoIJYvZLu1EeF3J6YZFVo2NbfiO0ITVIHyyL4/Cp59RohQARiLO1Asjm
7MWz/ZAiZ3XW+uUUqDM5YjaOilogYrtHAXL8KVCYo1qY15HO7zNcd+WYtOJF+AyCs3fE3CrwCfxG
D10Jhz52lFsVAVeTBzUYbHivf01al/mrE+Kowzr1jvgFCF8WXew4iRnMxNoVcNnBtWeiA3ebdE2F
uQpdHjZLWWjIkYkajdBRBFDNoLZtCC0fbmrD+FN9V5Ki5QxLhTUNI40pOT22it2giuurK/uLVqnB
G09FV4dW+dkZNtT9gtIZUaFDTU38JBXJAygNIz8oMYW/dpPHVGMylaUWzbHYoHR1MepMZJVbXA6v
JDBoblDESKZBTMr7Mj7aiRC+m1UZffDoCB1mboTyCeiz8R1SjEARO9lnLWQs6GKIQRuop4Jmq1cx
Oifo5f3S3/fCbbSRseeuurSfrZKxVTD3SGMp6ajNN84LyhlDUm4jtUfo3NnkeymwziXLEaaIbOod
KPUXJP0zMVUvMQC+DTILEV+PbL5QB0rK+ynnohbedtRpr69lPuB4syKpZu2pmFEFGKDEn2KdIDlb
fVtrmyT+aLRzDPRSnGmKw69LP2elo4QfzESH+epyUrl7106sfCfj0p9uYXZAZqMk5WH34AkT4Ijj
DWC6wokwKg/fOacj4Tu/E/xxs8YwU226SF3Ror2uarX1ihC3puXRVWR9PG1WQeP/k1UD0t2pR42Q
x+MyygdwdnH+4CLQbXxMFVmIzo2IjCqbVNVq4xLW+Qoj/44kY9GSBomZznzKzMMnqg51m/6kV9mj
Wq9AWRXhiKI2Su/CnNckaDP5ITQ8Sk7o1HcXEaNzbHnFNkQ7qV25X9gkkIUQtUHV8drcRyJcQSlE
3VME83dMKhp8WZibG1Spqteit1Od7qejBTMKUzt1CXYsVJnmDgijNSkgG3KnwfrBL/cJhZ+hW+KI
jLOHaIKTQ7A946cQwkhWwuW4ZpD3EZWURORCSE0aLikCQllJebkhj6K7RN5r6gUA1vX2KpKf4cMV
SVsXZn53MJo9pw3KjgeECBx77GMvTPnHCFP7pgsHfBXlHRnPC9SiCWSKfFSqkoS2l1bWkcTXE50U
W07rU8IZL8wSCpjDPA3mUA/AlBFS0xW2xfyQfTGiJIMQ//MIxDLRCuOILIYgz0RJHtds9QnL8LX8
WKjykHZyub+/i6m78Tj6R1M+psgTBj81Vy5US7fvW+rwaoOhmDia5i8urX+WhfHWBnRqZz2wLmIW
gmOLyox9j1WgwyGPgNwBdHkuROrF4Z4oIrRJD48KWw8U6kQ7PzoMcTGR93CrJ05GOvGmZWjNsg3m
oLTF1cyaUIK8cpx717kwgFKkmp3V7hsCvr+XsM3NZxEv70+aFCbMOd3qkPXP8slgc7okAMVMZsr2
x8FAMrAuwlm0bxlkL0GrMatsXJG7b0JyyGrYNVQddHY/213bXRDxDoNXMeG/PDKeRbNWdcYu6gMo
HxWb3c+uzbriyZI1xeYzfc4YPYDnHnZbOUYL3e/cuMOxkIZulTdY/ZTTnjU78V3DsGrDDSfjP+X4
hwjPH8/jMELsmiZrtaddfaU2hPZoK08qxIpRUX9+LgO7Qx5HmzCuHCww8/v3yQK8tRqecLXH9jBY
66EyrMmrDu31F3qrmItImpmgsh8TFtGL0q37tusQU/ZCOGirTMbv2WBSYQChnJ7c63Vo5y0STXq8
pdJOMbLQXbbFAck5ieX4sNPRIIYsZ5koExo0J0JE31uKCWphARteyARPlMlIUOZVBqE/Ol4vTnLU
n8DrbwVx3GY2bimzSqRXmVyzY+IU+mfW9tAhI3wY0HbXUUhV18EshtVxaKPc1b/Z2cGkwzscvO2N
4dV9fEOPMDfzwZMWAHUv8SFx6gaSum4k37JsLP+PJekQ/GJkgGFtQTHFIY/wbGvK0CAZRJokdsog
g41+IYPQIxk7Vi030uB29XL8lYS0MWiGAPUdRK+bgJjrKH6ZkbTKhGMfoc2pRhRk3qKKDGI6nNuN
gmCdhqjpyeAtpPpFRklS3/AqEfKQqH74FfzatyPNFIvvnnTbRmlGrM+B8cEdtV3/CroEEoh2gzJ7
GTFPI+gvslNE7OWI/LH+6rcCIwCh0WsCma8fXY9VCXqiOG5L3cTD1MbUKuxRtCgZ3Ejbnn9ahXSd
YZJR/IY6qo58aJYapCchpY1x5JLkjgt1IBtQLxCfqT2gUuw+G0ItGwhWPNP8Vfof8PHZNO4CLbcw
F6RVdinjfhU5I0CFp6qvMA49W5y7MDEUJonvwjVZ4y85CjR22VYyXwg59WGf90tr5PZn4HA6vpdM
d7GGw+Qh3IDB6wXaOu7MCmimiAYdva7DA/jYFJlcMCpwAS6S5sMQxO0rqUD3W7lCQJVwBPOQUUl+
jti3UEzrHZWqC18ar3M8+y2Ox8aJ45k+HS1+HSlVdvjDx2ALjuc+RbS8Qzp/5RiG0G4UbYHofZBt
BR0HS0Z6K65CnmvtfQCbHQVOp/ZGvwKV0XC8YqAcEJWYfeBWDY3GHeU7tsjS3iL+ZONxklQB0jOp
l8rdbTMuoZTpT9Q2JHAPZg3GCZYrqZJ3ScpNqIQrS0SsiY58VVco+8mWrR4WWKazLLa/0+1j5oge
hfvFpmQDY3XeFJYA0yG54fEZ3PMgWfdHvximl4qgLUgSgNMK20hCPRyF9KKaLGO+4YFYr+7a244y
sjwxGpy4rvQyfRNxfzP/sToC8cONOVf8g9+gaKU1vZePWksqd9PRS1zYFDyImyjttxslD6btRZ4I
4HvHVo9a+lHmF23jNrutSX7lO22Zyh0sVfLOU890NhLsHso2D/LIoqn8skwTKkvbQegMboPGGYl7
3K5Xw+ELIXd9pgnq1Bu5Z0uabuTgIlfUi6g4iZ5+YvCcjkMTtPwys+BuvN2csnVePUkGIK+DRnKp
2jSn6JxRHn7EhiaGJ/KxIh79/IhQ1Ltr45Ah6Kq6NHpQ5q+XkFXXY67TE1PaANLBn26A3nhVoDna
mR7e5LIUcXDKADBOCNDDwQgNNDxGpu7EYNRY+7qT8nn6WoP8wbd6tNEiV9nDHQ23zqYGKMoUypjK
rWWEkU+KmnysmC4VrTXc6qvMfm+4eBsgVVnrk9e0XEC5Q+0M6E3lyvOWeh61GRqUpH6PfzRIoo94
xWiVZcUxh70gdJkOojTKL/nXRCPDY2rUVWmZ2VIw9oFvifs0yyyUDyLltU2LTqEbAr9Ub/FiVT2T
ldFKpVWmcOo+4N3ZT2fjvF0qaDAi7wapR6HnmLziyE31UxD0IYBW0OlQ0TgR97RZxtPf17BKB73a
WqWwGldBbFNE4YmlzkE0Oq+Esv6bDGzh3WdIAaD9pv89O508K3UcnuPEMntnLXGJR3RsIxITuBU/
5JYoL6OeB0h0vHEaB3oGVXx55F/F4HD5u1Tu0FPADrdXNgebbtoqFD6wDSEm2gB/1Du54mehH3bM
ssMiuzJKYkeSoTG8bHen2HhKnr9GOWinDy7RHG8ZfgrJIZpnDxgxShKptWQNdjXntkwAiybup1Nc
2PvN/xCa0A0MsJBAU9jhk/rPoQaFyVn+ow9TJP9lKHOBuoLZbO8czYSbb1ofS6pCUIKyTI1wMcE0
M933lRUEHgzsKLAyRVrD0Q+v+rknOmYcUQQAzoWXBVkzRN/g0EIntZfrbUAUTQ491JEzwSpgFIZg
gCaUuNsTolXuNZAOBmrWVQMyH/+siTPdyQnMFuPyh/VgizF3GaVQhI8kStDrasn0hBcW5luuOmaN
kamA62M0rZwiO2xUrOA0/StpzFvqHVkYY8Y1dqZwmUjNPKc9WL6iU4//ddbUDl3bFcbTIwSDl0Zy
ETgA/gsPPvfEmEiwbutdt8X08mnIUfmRQXHL3I+zPdh4NFwRFdaB6NKA8OIH9QCleF6j58dZu8f/
HJwgy7oTj1ij8m8Ko1wTuzFvI3EtaylfoHI/vx5mR2uR0Ab7LxlxNFZFu15fmXjAsmP/WclYX9Qg
VGM5h8eZW17r9oUWJ5NfXfe3zfDZvYstU23o82h2CN7rtXSCZn5M7G55X2LeHgJXw4FlcRj2lDpn
bbTqrBroUbc6vGR74Qtx8Y8mOYPE4ADBBhfdlL26AOHqC3PUszv4+B1lVnPzr4jHYp1jsF+VXwOH
P7r7ABLa70F4Uw6HGDcLIPsQJ4Ot+QYQ9REgu9DV0Edn2QflC1itge3QIJTQ0RDbUyXmogc+J4OK
cMR1Q1t9ETcFVBSr7ud1p8ZenfWzdJFaR4UiqPlom7rx5Mp8ZjcVxEyw/pRoKG4oDGh0tpA3hkf4
UDzv9XKs0XcO5rBPxM6EaocLPOb5h7DWytwbmkR/41huVZhNajUQqkEb2uFVa8LccdlOX9SPEi2N
ejseHfSSR8TUachlqejtRneDFkYKmGq4/0qR36x3188aQlTw0b2awCLg1rdr0ifZcjIltP/rXay2
6WW8fBh045I8MkMXiSLYUTbcstVRXbhmgTTJ0kYZ4VFcLG+aO7/oRNUTj8/FNpE4VB9PdziLmhwX
7kwvUmKThqhbqIHbF2u6/iHywm+F6FjgaPumDKC5RezG69ubkw9e6j2EwfFWBbvN4/LvBCdzDzHI
to3+hP9GBwt4iDScmtOzA8om28eGOIgwsWZ5dBTjuXhMCNiq5KhssUGS+cYaN+A2869XQ4I615Mh
z5pvtpzpc6+iUdLjisTXBljGFi91STAXCydka4yptDdnnky5OMJyzvz0hjN4BHfCQXMw3LTQu0Lg
rms9XuLaDWNi7puX3zOAGjFAYzbPYQP4kmlZ0NYw2aboOZIje8O2ainy44iSB9EiSuZhuDtqsF9O
Zy4LMtPBzY4Tl3Ka5DgC1w/vljqU2MPAWjTTsHQSUUWG6O15G+whr2MMP8SfLznIKbM1OzHE3JYe
Q/fJbn9zmE8MchIFPIs/qt9xfrTX10HWmOUcsUePwECIvuuU+Z35uKduLzvmKk6BMge6rRnNLxhm
3woluQ516jV7Nvz1TpMTSsv3MGO41Juj0vr/uUqTdHPeU6S5p/GSkptnt0xRriyXcbB94r3ditIA
aXbHin47wfUADmO1V4cvHUikbAwThrV1dxn+e38JXiazHirlY4YFrlUZyyqLmRfXcOJJiHfMK/zs
wUpQvZytCMr0uxixC3qG64fGFsQIOwYRPUg3BTgiUTYka/GUgblB5HPGOcLWqeXmKjktloIdie2N
FiQ2sfFQyt4HYN0qyrKiTHizEN5UUJb8Zs6QPjvikN4+oQGkDSelSrQQK79hWEMvd5SIHP3uRsiw
Rjjz7bmyqvhHVIi7jz5Bb51s630XFnhFRvYbRD1O198TsOiJZ49UNG4KeR4YtyfEgcoSqfNplnFI
y7O9RFWSaGluiVaHFk5h3BMGb+NnnvYC3QHdhPwhu2a3mu3tuPq0Mz8R087p8Nd+Y4/aqEd1FXqx
/9SUwZVumHN6lAy3mZYV0K/ptJOS4N/lnqN5P0bCDF3P642uDotnGsmj7TerV2u1X1YEvkXssCYq
ts8H+Xwgh4Bhv+PAmLDLC9dE/3WIc77qC0nIWSaDNFLDfkivUSOyxhQ2S4jXuu9Coa/gw4LPPMZp
4x5ysvfimfPqnomWf49WsohdWOjC/FLL7O9E5Wq6q+Qlox6/3es6T+ez/JQyiyU6vUnINXK7THlw
389bggTOlYOKJVsLzr/K1AJNXcCQwbQr+GBXSCKM6PHi6D8uvE2VOlluARE1nj76CuEdzYZTVcC7
XH4uRm0Uwl8BsTuDTOQuDifR9S145nn34QV5yAB/qyg+PYnkNcJI/oLsOx+UxD8umZ619OcrpoZe
4Dw6QX/SUyoZoe21/iAZGvmGnjlxLdVl7S3Xme0SHqWIYaKl6AH0WHf8K8M0GppeZ05bvd0KZyex
8JBjoxilfa/ax3Voo1/LjGZt1jZdAnc+g9e9FkjrQmzBvlIh4MGxIXAnw1cbmsMbL3En3g3Jacbw
YAoOtb5iwl0XMo6Oct6pVLchmJNFsP/YFDmAbvX1cfRenUFPlLVgCOo5wo77ArwkoayFOze5bZP5
6sMr7L+TS2cLnd02pxQWQO03kQ9STe5xkI7r2Kq0Mtn77Droh+bLF+t03CPsRHIbfT0h+fwqkRXm
JVNcxtUNN/AGa/hiCWlyFZaSBnoH3veoncOPQDojo4iuOvheKRV2IWfJhUG+mrenqk379nsOmn2B
byYYl8yMDCi76RZbVg/7FsBzxeyU2Ztp8eHYGeB9nHKD8uksQFc7hK5ejJAsgnRJ6uorqtgdBg8+
TqdpKuQsxrSnl+gPbhld3MTMpLWZcgipXFVAYymAyW7Y/0OjPyFTQ39irrJ5ewDPFIQ4EGN8nWDl
ps6w8cIoXwL2UqIP7g+Av2Yw7tqNPSS9mO41ST7U9II6hbzoEjz9VE1J7ENLeW8IZt3u3Fo4eKn2
+FRLp0kQA0kMpRZysB/hx3G5B4mwd65bfjFDnDaT25WsTPNOY6CHLCqBeehlsbqbX3uHISdFA4dd
0cKFM1P19YMskxDS2jJNeUAlBgwYOBPLEa12Zcnz8CdFxY1DPeNmMXdx2RhWQl3k1TTWys9K1gBS
FM1BoDAe6Olql5qWB7IAT0vT7F+ByOC1g/tYqOSj31KTbZXRZRuYQP5wKB2dwE6vdoW4tZUjeTqN
ScDmEORxx0ss4h3HuukCoJQzdOrtJuz0bFAP/yuYOEfgu7s0hHRyo78/a3H/SZRYDEm8IwjnqIHt
2zbznqGZ76woh92S4OMEhzbVgsSlHg8C0Fg21xuj6DPKSOLcEuWKJphKhvVlZUDDkSWUyrkV+cgA
cQfb6SdxVj0CSEpBPCd6lI1vQ19Hcw3KIcQMd2XSyx6pJqeDl/DCCQa4atiuCWEh/6MpO7yvEaMJ
FiIF+KjbAhmP14/qhh4HKL/+U9Bce/YXeFsxlgpuI7PJMq1hj6nqUshaNjcewlZy+1iwIslgZ1H2
/EqzxjnS66EIWzXzqlG44I0tja0fr04oGhhFZLd7VlqxlyGm+9TjWwtKGDXOlrGx5Xys2UY6ojZP
wKU1agN/eRwSnaui3kyaaks6k6BlqLoXbyivVvSk8HjU+0F4lZ5y301Oq+E3kuOUX6jjwXsH3oDW
d1yMX4ilSXytDimFvl0DL/hwjfHMJ/WcjgdgfZTe/usFfeEqvQ/dSuG4tdVLffBinOzzBZFNoC/J
bnw71UFz+5iRlTKEzXZveuJjPVL+kM0tcSE5hrSIY0AmSHmvtnQ/hnLKHkdkD/Xgm4vwC/xCW/rh
Pua5PkjxI0iyOumHcxIh1lYhz+gegtWa7ffTYPOo3rEvstSG63iUTnKZeKOSovXnZ1TN7fmta22E
bLkVO2dsSkGgEsE1h/4dDjFyaYtyhket/cJauosoraZkl3RK0NvnzihSAGxSXy2SWd9PB2ntcWZs
OZa5m7DwuFOekOAjaqXxSaZbhYZOrZpK/cmcIg6Q4ugYnAoUOGVXnmTBFokCnnucMF/Sf/wx1Io+
JPmQqIkuAE1vjjRQiILlLFNMl24xOdVzNMUeYtc7JdQ9QqctFmc4fOD04pPi3pcAzpH7iRFEHFC2
oxNRfdBlivJqpXfRyEVgwQbKEgjyFOuYUrkKKQVRtG9Q6Y5OwhNlRHvoZy04qW/XupTK9ipjFW0T
xWUxEc4848/1JP5jC9VHDKBVXN05bvO4ZGXROf829onCv6OENmV4v8ojloSC05PIygqzDDRwAWB/
aaYfOB+QWJ0k5LpqcG0PKqtj4FczVdcbcBwCyiejkvxqfGEEzZ9D1ECZzZw7tltdzmspvp4474wI
2Z38iq4Kzp9KtWDJ6IIvwftLfuW/O9XL68rqBGp4M78u7bYmXsQ7uhKj8L/B1GBYrMZ587ZvudzA
DqsOioV8PP0HTigk4V1/wDnCafS+Cc5kPtedBvWcbIUAv3sFMa2LmdKUBE37WUZ7oBl8gvwAAZUL
tqzoICNb3NI2v4TUzcv2t6xZ2CUxY+lgJmmEAgSwQPeYcn2mGo5YpVyAU+Me/zQL4U8Xk+kbmwJt
RKE+QVOvk2HSgLYQRk81Z7ADROGjySUDX1RJkmwJ0LIt9hb4XyDUx76+0RLQINZOqV/Bn3AqtXON
lryiM7TkJHvSa8qLyghKxnJWcJKsawtFoPmD0iqEsjO0QWe2hCYGpnK09gVH/OEbzQKJKrAWiqa1
jNbkO5L+k/uuMUMLfvjY4JLvexERcF/quhdVxngvs0XgXBdRUK7Z0p9DeAX+ZEef6vEIfFovTqNQ
baFmtsI0C3zCk2K7cxvDJtGn9yIFQQx+nLv+8pzF6dbOYhVjo/tLcyQsyDp2DDyL70BgSwzS6YKL
00eHE114UPDjNrMLUbVgZk95D5SokAE3KWPDN691rEfVZRy2CBQDNDOQ3YcIDnt3GgGzuWsVnXWQ
UR9wTbX3jaTGhrowD8cWkhI2lMNJ4DUd11dWU2pBUeEsD/B0cVwpp34ggLu3GX1kC+RmkFL6qO0k
DRkKgqmj1bcLssSgPQls/BbW0jimBNKstlL1sUmGVb0k1t1nkuQFELD5RNC+qWiNbDOczHsWyL1L
cHuBZ+FtdPh5O8J61+e9fmj0/J5Vsp0yQjTI/FlNo7rHncwdcsPMUBJudJJQOJp8VsrMCKveKuiy
iVU9/M46XrGMA7FVSfPn2qcEkoS2dVT8aojZjof9OESlPbcK0BeLcjAlAeS6/YcQoO0/Tb+vpbkG
aw8Pyh2wwuNEhtEjkjLPFyU1sOwBlJLRGSh3XalxyXDb0TfU3TQFFOcVSFDp5PdoXw6yOvHyRK3w
eImaQ9ZNVOXf+C8p8xvz6dCem2nWbt8Iag2hKqcGosUzrbIE7WcSzb1aCxmU2ItGKNg68I6TJUhK
OOpI0cVBNGxluQSo6H4C8E0m00e0DVLXvHLdCQqKZyoZGZqk+mx5FQol95OG7umBRFyWDEpbhpG2
l6TZ6O6X6csAuz0ryRyKV64n31UE9hofrhV+RPI0QYTG7PlnVMVwc5RbA+bl3vzU2FJ2XmP361u8
p2cdduYyWlUzNHxMvV1cjr4PfZSSnOHMudNxorhAWbmhCe9evXZa2uT3CkaRnZFC2O/f0YmrxI98
udoJSM6XSWRwB32SvgVQkA4xbVaZSkoU6Kn8LhLLJ1OUdeYiC4DDo2PxsLc1MlXp30xmOJe0Sn7F
lSP+T6DCMFHvGiLFlWOba86Ig2ONAjmUA2ehVscZcYSbsSzTTE2u8fHeGa/ui1GE796Hs1u5eYpQ
colIod3XPNoqI1hlgDZVouBeZ3CWtZ6eEcAT+IdQ4ga5xDunuaNzeG3BIGrKqTP4tMsNmdQ9ydzZ
7bYapIzEAo6AB7ICyw9ces5wxNrBbSS570s7Ikyi9R3+Q1AWgo6fWC8TOyeAriVhj6fEF29JeCHv
1MHvnqVOwOWRKIFQSu1Dlf7UtMmlioWaALvM8M4ZrhJqEV6EeQkSO68qbhCT+6aIC5AHDZvE0HOd
XS1yn+QAevzp9c+zhxqJmaRde9+y+kccjUQwEDVE5szdBOKi3iXyS1+kOg4t2r4On8tUBSP9ZQSv
RpYpQj4GtWA4l6tjDciWVg8WSskH09eSY/oyJKg4BA43BF4GbUmOXSJoa9yL3glkSYuCekYkNzhx
8ol5SOXeBZWCC6yuNsTgnraxYDn7PvDAMGRlMvibZ3XM4nWh544nDIMGoId8MOK1LMfUtg8vsVE7
48i2Cc10C6ESCeO36MoIncQqNgwdT2cpEZHVYODLxXmjnIPY2rIdVNTxhO/aIU74MUJwn6eY1SAV
4/YAr5+v3oS8w1MzgKQ5zUmxbXUJUlqWJnFU4uVzP7m208ko1zxi1Ys5xBo9O0phTDIJ58dL5EQd
Ewf/PE2XUuPIo7DYjl9k/GjVWduCJ17Z5200p8pMAIj4MqaOUcM140Qj6aps3CfEc90wioQhkTF6
T4COm1m1QTAZvZXfWwqh9mAFyPPE2AxEH2beGvxWOXPFo/EOdXFgxal4JzGS14rsGJq0rtsvxhTE
eLAZt96HKswQyhmEzNJQO/dc6YmtpWXQO+GRFoMVoXYUWMrusKOIlN3RUTaKXRceSAOs6AvTSpNP
uTfkb9jLquT0vTnU9EoHgxTgoJIbFNCp53SwRAmhBt9l7X+gEdVG1rWDmT/vdEFsn1ZFotFeL3O8
ebJh2nkL10ME0UDyHdGsQXCV9jysjwmxxrpa42LMxDvZDT71y/xQV/bbiX3swJXNk+yB8vYDyTZB
EFrLyNKnQmP5MaUi8nvqkY/PEm4aUKP6s4bTvcQuNyL4IJZdtLIlaa/3ITn3gKkjqO89K9QeoMrV
BlDpLBIR3/+y5Tl1eWebTJ+alX89FAeTugfu1SQx+36wiHQGcO3AnCA/IOqWhOTdHFy/dPTrzzqy
UZ7QqgMv+u1Nv4Ma8b2Q2yRjnoUrfUku68WjAMKqzb0OjPr9T38AM5UBhIrkAhSJ2I36Dk1pXRWx
95V1xH82rnYRMJlslvLGwnVnwtl2xKMNOUDNgh87Wq1AfTsx88eWB0570PzTmZ8yR9PqxAULRzs6
2eGPwU27L+xf1fI/ohERyTumFpacuOmtbl0jyTZY0ae34821gCHfIWX8arOHwplP9q/4bx/GhqTj
jKjOD0z1xG0uw52nLIFAOH7i95v/evLs3sjvfziBGsnlPhUdmVG/QZJB1Dxjn+Wr2YfDSuuee1zK
npkJPvps3L6LqZMyDs8bzB6NG7O7n6FpLHFQyPfEPGQXJvxXmtR+FONINfCFMuH5VSh1l9OXsYhD
Qp2ZVrEk/fsf1V/lZijbUjrrv+RtxpJb0glxQZbQFNEFeEFvEcFNN22Ukl9uGkl1W6IHQnxWyEsP
0z79rhIZbOSLoZLKQ4wMtnexDScuBecPIW+Agg+pPg2vsYNwd2CeuoNOTel10PvtDvSmND0yPl8j
cwouB+6qhLpBr379IPmagf/DKKfcbqZ3+b1B0YBijVrW23jHCPkvRHBobTAdrrLaDtBiuq+dpOwt
2jMhh6KyA5ldTxKPQ/f20apMsmV0UZoplW693EUp4cSlo7bdtVKJSo3GD7zRELtAiS/ET2irVnWj
8wkSUGJUNYYIQ1I41NGz82ALmrTcuuGE6GvTtvQQ+TlMEZQhU9jDC9TgkCCZUL3qYPYEsdrdqHgh
T2p3L3hCAVyj4l/V28kRYvKQlVHC+piVyhvOBuChQcWHUK0WWy7WQvIZiJoMiwX1w3pMSxLvV6e1
WhnIw7SNw104V7C1/1T6I0hLpXawqLYdnBNj6g16obfaoENNblly3g3JjVO5U78ya/Zbjjwf2R0Y
k1rFWcw+cRSF7DXTqqpQ8mO/syXKRccr7ZtZZxQqgZnGFkX76xS6mTFAgCtKHObbkhCujjKOc6vV
1ppXnhPqlo7XmDmM25QtubXxF/a/8ItRWQUwdPydWeTrlcxYjAvXQMl+Gpb+yitUFwaS8yIVL5cw
ONzaQQxC/RShcJsWEQFI7ehsPsyV/vEzZHaCwSR5CDJlG+KRdnH3K4Oq9ADkPk7Le7/M/SKEbNvt
Xf1LdfsSrniXlRVnHsmsjAjcZHDGsyBEhI7cd/BQ/ChlPTyhhtQMa5XxRdRKe9fIeuLV8cuIjtt7
30pUHyiU4h4dtv8wYhSQaD/FYw3MYKyIYIK1p77VrR30RZWrNiFMkudsn41v3KylqNIVDtgZJGHy
KaTFo4P/tVqCVPu2CBXQCl7bDtPHNTzeGqkaXtEyLTPyDwQ834Hfk/dkYaCq+fqqMJXYBkF7Ev2V
xQO3VTjqySEKb5Wmvl+KfJL7JjmpZkhYUWR7GSQ7FlNDxzMkr1BgG92IMfNv6xN60S1kHAZ6hISe
OJQpxdMokFqYSc/yRYncMU5LVB5uMTSF1gnyH5jhF+Gkinwt77CuXiUNxsV63ji7j6idEeOmgLnC
ta5E3UUFhsjr2nwdTuSHzxtsyWkPisixuxjbCp6ToIJJAREnKRa5ZRPdKN17s+mmFNO94OcTUevw
ECb8j7gqvOlt512Oiqloa0k8H5lUyNjUcf9k62hd/uN+TVz0bzj+4tKkapTo5MTepaf83Imu4x6J
ICMR6fUNpsR7sj1oCx2sWGbLjyiaEJ/Q3yWEZWxZrw/8HnX5fYhJzdk2RK3nliPuZNAOlbrGn04m
XSlyI5lSIEKSLGWBiSgfhXoFHJSeoDMomK2Mve0CfamxdmckXyiL7lOH9tKesRd8aMJekRFsqPHJ
zARji+u/q2bngfs80WUL9QVo/JZWkjoDgZP4FBGIHNYv8L5eJkrJc+7bEOhdlSgXKQiPD6JP7wqR
9r4peaL67ZxZt8RTwE2qMFtIfxF6OVw+n3rAOCYLBkyenwvtctYBEL5NwEVTZPHHpog1D/rI/XpS
cqGX56ozJrmDYK4/wkw9g4323poOc4AT5C2BYCsZgrKD21mF8TcpAjdWdLZcQawykLXIZoG/eOLH
4Vh5+B2eHHyYwo/v0iABii2S+tUzezhMzDTmP9Thedle/QrDmczQPIsMBFGpsDDMu3j7otlPFGZQ
k7cIT5cJKfeunNeM27nhyXM7BNkDY2eJf6r+mXWf+3ZhDbCFxldBOfsemUwp+qJ7K2ORvxsiaIzF
YQqf3zSeEaFgWu9EK0kWH0oedXHeLTHeYzQWivhv0Z/N0hKNVYoY0lys7NLHnBeib6yewSFpat9y
TEV7wOyppvfQXVw6FpHo4SNWDwYP1RqBFCL7zIs+1oj4xiXyqi2JJ16lzf1OzdcMBWN+aqIEjA3d
aNz40egP8tuCNy8TTAdx/kPiK0SsYVLXheNfiBVqX/eq2299BEcgv/nVn6t/NtCUgWb9XzHxmYiP
q3GvldrfoyFALRBUokOipuEqdXXt9qYF7mR6VAM22ReAE5tUpH4VRTit5WHd1QPY+2c+78tCT4pE
4MH6d/8h6N2G/qHMhQ+avI5KuRuYTbM8BTxdwTWm2AuQiLV2R91mv1Td6BoStbwTI154HpU+olNi
vyoOkN1Fvxu9+5QybP1jHzAp1AqHcfp2qvIPTw1VYeKxTigZ+CQZ9jEHzMB0M2JYWuaBrSjkIZ0t
kDBDFMg51ByYfjzv0xRTjq+2mAm+Dw2F43xOA5v9Y/q+KeChPQYQQqx9w0JVPLWdZOiJdUYM5a5l
DWWkvixSK9yN3UEkrayWGu5PTrtC8/fah8bcb2/zWTbms3dkul/HxopghaU14TSQV0Jy/mv4iTsW
EoYMOg0Y41cGb2BxeR6qfjpFz7qfX08lo2qZBYGa15qbb20gKxmwo/U0Ho0K0Sd8KZkv6fV9H3Oe
0LepWVNzwynTB+VfJqsq6dG4qYeVbXW99tX3OPbyMxHiC6GJ5wOXnKnGtLdwXr3fc8w0mP3UTWwY
GN6fsgurTfkw7Jc+PDpXzQS3YZQq5CKBmlr0BKjLl+H2pqO5OyK4GbY4SkUIzbbTwJBMTTxT3ARH
luK0fHSdgxTnCQKeiwCyQHdTMzzV3B873HXUSt2aQqAFDPEFSNr90rCWTYXWq8nC/LicYU1sJ0E6
M/HPjQblNXYaEKy9awpFGE7d+CWo9q1AOXKldyvgDgibgGTeV9YYpvQERMcf4Ow0QXhU4OdFJujx
k0Jmtj3AUbtrERCny5s/ici13llVbYbbZ8SOjzW9a4OGTWu83zi2Eu+wxgqpY33Ah8jcJaVp+ySH
TOyJgQEQCw4EnzeCth/OL3ymatLkS9bkdCFQAHHyIgKI5OGZatrSfyuPqCEmXOEh1Og3uDHJ9Zjz
ShNFFT4S1cxfSsefFuzw/JiR3clq6PO5ahwLEqZDrExWPY0Oow8oggsl4TzlHRuqU9b+mHmh0HjG
Xyvxv4Nq4V34TgP7za7SXqwKhNy9BthfaedAlkTIOQa+n+TDaDzX8LYuR2GIHquKJfbv/3TnvlC5
VW8uT0IjqKN0nSJNCLqt3vNhE/Q35OEYK+5NkwQ2MIyPtoFetWuvVoyStR6MTk/43wEcNHi4raJl
OVl4LmwmjxQiluqivYHmG2gjQGI39vSweXHcZJQnE/pcb3mdaPNDiJ7DorosVRcoSDcRmxhyXjP8
MPm9dVhdBSm7d8TESksNMpOEzyWA8a6vZFYns46F4d9nZDfsBpz08RsW7D0s4txcTjUDX416iQNV
YpcUJOuG4D6mKEDzJzCW0CiDkO9G++VtTDUV1snXdCit5Pk69u4XkXfIhJdmNH/xzrhhlMrXBN7N
46SV01dGKcPv8o7yk0AXxSL3GF2GDCDRDGP7fdV9u2k9jxgtRJsldXRAM+/TAosOV9k4CbW+qomI
PWJJXjYY9BSByp/+YWwQ+j0R4KXKH7JG5oUIkgppJhK3JTK7Q+peEUjToQkL/bm9GeP9bHl1pL/p
iQlN+6IOFZRSlikyhZQ3b80Z4rM2M/96lMRTpZnE+suUA7mKu0lN24NP+Xkd5+s50EgmmDh2nNgk
VPJZRZZxZtXhwQH1/8jdRXzg0SvXhbvcxYMvP/QxzIsepIS8jCqO22wRxxsfkvsDLRvIRo0cOybI
CdlPqih+lmWOnuHRzULDN431ecLDPCLWkihkrbNx34aUcHxaHF6BOmX5+9WnAMU13pUFvolQvPjA
jhgVgAwCBAVTIa8HHKNlkJlS7VKRPeNfVhdYjlx1quWwoIckQgovRi5ss+tQtA5tIk9QedvWSy1e
JB5WGVJXQAacbjD3ElHNwK8ghwE6RfWEcPhr/R/oXmueYtJsrbPQyj39z+7mPx5ltQaKYQk3mT/r
wW75yjkcg+CQlCxksXP8CxbtKnapkB1bZ3PlDHMVBQ6xOhd3LFRJogRLViZKOdk5ujrjWwucNQG5
8f0NXq9mgRaQOA8hz1wHxsaOALYxMK884oDuW3z2sRBGJ6raJQ689IOoIMDVehyfVJq5OVpiSkzu
V2ejzzswyJ8z/jp6FSgHldHurGL4yd1RCTJIvFf9g2k0q7IxYrBojiRw+ZmoH/FsZr0PCNENUmC0
jHMXBSw5IZxeZcwRQS3AY2IvTCDa/4wbhT1WuDoGT42yodOlhD3tH3uNCF1sMov2dpplqdMhnfkI
IOQYB+4dfFGj0f87qyYcDD/2q6vOwFz2Fu7NXy9pRmEzxo1KnLKLaVzrEmrVqpLwJjcU1IxxXvs8
Ci0yhCPWDCjpMFevqDG2YjnyEpcs5b+0A43RF9F5C2h7uvrrcgCBgjI0c/S4LusY1qT04pJL6DBS
ltusX1ht+TS2NxC0cB+MElhTxX7dfQnyM8Xk36pGe9uY8RZ3gwxW3PiHv85Pm2BrKPwaOxrr2/X/
DIxZNnWdp7awfJamyXwHqEAC0jqCebGHz/qTjVyAmq8nrNYIgFnNrYK4hNYchSCupQ9qcC/bzSqO
DPYmQ8MVnVkd1KwwdtmQENKRHS2g5aVtuUifHq5+C2spbPkH/DOd6Ez7+c9rx/I73ZJuob28GWR9
70KSu8gxzSay8XlqgQ6GkTP2oC20p+1/cFL6Jo/g4/9buUwuy2Lgg1zXvgsA1uEHPmuDArXRNdQX
haZ1A7RqVq5eV9EbYeJreOE6YZW1kWWeKEl4dd5O9TEJ+ode4gD1M7he1LFu5M9+czUQ42jVJAiL
9GB3QFyrQNB9dVppf5O3oPOw2pwq37XviqncvCnw9b33g/lgjyqISa5vSkiUKxz1Albl/Mdsw65c
vGArkWEVOu9IZ3z5SRauhWEejsZpuDFdBWbWFOey4ZwNonJWmKbDlmCFGhtoYeTGx3c3VBd9HzkJ
D6U78vM/Tl/Povn36yaMcvWRqdkl5y8yumpooc8CK0sCCH0O9VB2YmiFM1dRUp+a9ZNAxUBRZ5Yn
1xskSeLRMhoP+W1Dx2t2i3T8VZ+gT5NyE2csJ120ZQfFmf0It68mN40pnmwpKRxCOOzMSz+Fg3yM
r7EOnLAwe8HStGptSGfSjjMbIxvstnK/uZjvjjIUdyqRxfYvnNZjoIG/x5rtym3oVPmQo9ZXbFVk
BaqWc1P2krNk7k0rOr5B/xV4RLyVBCM79ofsBy2MekX3/ErOz0rYWImGbM5ddJjwAIe+pGVOq3bL
YD8tt9ogDDYCmvyZRbInvxE/dChutizC/D0nlM+LWbBR62irxBkx66JPIYgvfrCtNZWQ66irqJ8A
lYdeyFt3m5VZZpQZ6IWR9QdmNj3UG1IOXXYfsPG12mz177B/3/MXoo7Ub2QFXpkxVYIEO9GYT4XN
kS9ULOXFQq4P2Z+pcT9zVMy23a5UiefuTcoJ6ovTJM79SOqZ/w8eAJ3Zw8sFKXb+iFUu4ikEHX7f
YC0333gw+1/cSSJT86484lrrTaTPPtGOPtMt96BQkoGkYeG61OgkALeLNCmbedslV/ZR+NqFNPet
9ZG5Y1rXi0pXuSX7zcHi4RMOsMA8TZT/uTZwKIe/Zzs+CXK/qIGlhNt0gT6fOpCEzSJVv6ADy0hg
jEH3yNAsqB328v8oxEb/p28/su3h/w8TEnx3XuilQbRzgaKdEKMZXuFauap+HYXpoE3JVC6FZ2hN
rj7cjzMI/wFYnrtoXyXEsTq3zEedGCDVNBbhunWhe3oLDxvvon3ryGLexRZQ53V7bryxqM+Ev944
TtZ5zJxtcEjKwEUCoG0I6oekaZ2topYoE42zVIJvM1i0vAOa9lSlbyVqQvSmOSDKEVKnbL/OJNp1
BMGZvG6/k7EfhFNN6240ilT4mPmPwXeAdjFZPgbhuL+PbEhAtG5P3xbVKejy5WAQF7ShEgVWx4cE
RO4DT3zD1GEKyHT6QR/dCNJQGrU8qru6t9XxHFvmdSXmMtiMcdN/tBAQ1mIYggXoRuzG16JZ6oxG
qL/0TT14x3AjgvRgwdw5nnkbVexaKgxOxPSkJ7NYE//KLl/AEIp55YEUK7WSfnfCXYhtQu6epnBq
WPVexhJdeFQDp891uAw+BB62MVxNyTSFitmlu2hVwX3XEs0Jzs374FaItMQJM671oJaFomGUAIYl
l9zqCXQDODNdODdiFoea1CRoMJiPmBOjucyiM6YiXdVWKkEZGDsD/Rtnwq/9/EVnlSkGQKvni6h5
/neKuDon3n7MnbyhValtRYAZwpYSfVDLS6n0Y768AKE5rfriI58nUt9I3X9O3rtyENSq4/milGaD
n4MrkNwEr/ii1Vnz2E1tPiUSK/AYZNW11uHizIx9qXQs2ZdNDHNj495znUxJTv9XL7dGABs2uHz7
6EhZfh8I0O/XLhTSCc6EU8uyzjGNBXyYG0KqAdVRxJhhND/xSxT05+rzSuZndTAN3DfWYMaZZl5S
pNJyMjYSyPm+GtaWXAL6ML4ad/9k9kfyBqBMEoijtdBUWTsLEa8KiX3OaZiGqdopulCa6JJWP3YL
GBm37KAAzZw5GHDE+0wBlbHph9cWvf0GBOP+aBpOcPDVMXrmPGmgdzB4+sNiesXGFWxiT5XzHmWF
X+X3TfQJtq82nNz4Dk2XlpWP0bFkIQ13a2Y9g7lR/8z5I5oPTJd27t7BbeF+FeUj5ld1Pag8JIgL
hOcVBGu8T1Z93ytpL1yFWZWmNaweeFRrkHTO6bIU+i8ivNlOQJx247rfCWeTdeZus+cXFAcQ8Il0
Dm5yRaCeBTcStqlQGo2czyfQ5G5aOOG6MsIYih33IVbQQADGIIQLt7gva8VBz1TjWG6dLBBFkgf9
pCHRVuxDFUwU9c/gVUmphfyTyt/Rlf2lFvyboePNl6v2zmOe18SuxZa2PLyx0o387AgsgvFEDqML
wnPIYbatpNxLkGT2YwPBXKVEDPWBSVy2lfrBDxA9Z0dciWZaqWQB5fiaxc3yRJTslhIY/15tEG4Y
T8e+km/6JFpUgO3CzHcDhla3NwJttkvK7nIFKS3Rx+GixWPwJ3VIWtynBpgBXxGiuzbjxuh1lo74
9gV7IOjjY/smMWaYf6oXbplmxWagx1JqhIJDpjP45Gvq1H9IO+B8isE+Cp5zYE0H5zSbilvhkRNj
DWGCRhl7xyZ4UfsGo5GcHLRTjcWVo+U7QRYPrPqlvNNx/7y6gQ5g01fHcHXSoDGoTooLhTFLy0fJ
hWfBufAyLlY2fmgmf/eS4YROOCRIhXQ7xIFodNrJBLR8a3Vocf16I1S1t7v56IurkRvrvAGXH2Tm
U4Fyc4+/JyeXLXTqLUW4y/FpW1yXNDW4wQNN5f901ijtt5GZfIcOzgKf3hqwQwTVF9uS5Jc4tJHT
JZOsxr9NFc4wWN8GSVgoDneoELKWXCi+zW1M4PqgZulBUrzjV4M7gUyBZB2LBcsi85tstZ4mtczt
UVW5UPMH///enYtFJiNPDdQG21LUoel/wQjLPeH8/A4LU8Umy3rpDPGd4zKN9vFjOFmpMmPxaB4w
9vGfIQr7b1I9KVZTqxNsUKpFkL11AsmlCCt894S55KqNnY0Vlz7/sTI9G4dflMrFqhMxdYCJEDsA
u+7rvaJNhsE/bKQUJxrPyfKSEfMkAJKep1VMt6kPR1WkmeKSEKQ1XijGEbMq5lWYegdHlFOGMG8l
Vtml67YKuP6x7b+3xqdaot07zBWdTZoQJqqJfmxx/izHt8+G5pFGLoLNe3P0ZUjWQilsddvOg/jJ
YmbXFQt26RMp4dGoBJgK2DOaIJizc+QbxIz+NOs891z/GmFF5J46MokEKJue5yMxMDhCb/hayV8n
C7tYQIoWNPkXr4MmKtB1TkizTnDec9MGVxyDNCn09DpXqDeeU5yvvqpHGGRBIVfz2RrsoguhWAiu
31yq2jjVMs2wj2nT10uA0TBY+gWIX7ciMNqH+InHB+ml3EICaOaeQonxTBWzh0kILeI9drlsUHer
33Iv7K4S1wWVaJUGsMVHf8BTjQnzJIOe9O+Det5yWy1XYZcbnUcDuexuNHgsppPDWILT56W4uaox
p9JEjzC9lzBSl54SbAUlyeqOPlKtDSPSDwNHVNra1PKEUQuKXiAj9iYHGh3oTvGeg9Pazetm5iqS
Z7UpEXyzZL/4mgDJukWCSy1OeHfjXq6C7+ES85tvINshs3SgXto7Ux1/Un2XkQ1mpHF4Eg2xuzEK
XgDtP4+8YYm3lKilTyRa0Dc1yjAsvSEoi5AceP4DYQ3WdaZKL3OpekC7CP1L1Ai1MwMDnjk02xAU
v7U6EOnO/YtlxHGYdkg05kRP0bkYsisek0z/a6oRZxU36qxJElcyWaxcXFobL4KBI7dhD7u2pAT6
N5Ct97w/otckWmlpOCMbXuReT9Xhd3hM01c5xdAiya8It1tfx9mA2kjT9RodkyQDpJ/f/Itx+oFP
0bqRCL4x0uuoia8oY9PmASIAG4t/wxq7rQIpeyBDrQmmgkRClenaJsNz7/kGnA9yrde2Dkn6bO0B
RFMysv5LFNXGr6u/c/1AqzWVMI4vHDoFDjvIWAM+zclmavhGvRxx7CjO1z2SRrsgSwZHvFFMq0tN
RSNjdwwLnF9+2ZKBi+xKnUhQhzfGMeqL54kQ0pAB5vrUD/nVZ1Mf/Cu5WNYPWsoROjKfVyeDvErQ
uXS4NJao/jfU03I/WZ4uJ+JOwGnPyX1vw27f5mvwLUew7Tdvb2UthjekQGJImNdLSjHMmcu5SGud
nNGE6RHdab4Yvfv0Gr+oa9OtwezxdanxWnnomx1jKAKgxSLMCnn527DyPCAna0Ip3n5da9MSmyTu
bsekfXcEO1uWVELKZaHyMl6F38YtcFD6O544n+wTyIc6zS3y4g8ak7mIMV/34Bm7jSy7kG3XZbkL
OxOUW0627qv6SjJsfRdN//kZtzE1dgZ0gJxVCrJeemuKV76f7AzogU6CFtQPqeyksE8we3j4SFn9
PIojwSlIyU5Prrh3fuXr/p+vemGONkAacxAWZ2lCp/ycHDqffktS15SsZphUJv7ba+Hi1llOkW1f
6LTu5XekeHBWIj2AHfmjAj+z/jhMEq5/KwYTldsHo+1zCoMBrZgDXvF2Ioio+QGFrNN0dPgGHmZS
9h5ZAeIeQA6sxzM4A5LhsABxJbxU0LyccQvllPsIRwWgueYMopsapvA82VwY66EdQcZZxwOjVTtL
RAYjSNSDQtaIHTis3WhUpKZiRQvAE0qdL0zuowEVrCpUqv5EqCnyuI3LIcHKYmq3Gbonanz1ItSI
5aDpL4FlFea7MSVdXz4/g0cm8QXGm/3m08zRgffND/jtaPUZLxP2I/u+FvMgx7aSNuyl4ywV8dEi
b9V3VjO6scEFrZpcoqKHOm2WlGRwFJi/nFQjGoFUCr9/roahusMF7SlyRhlH8ljSopR2/uiZpTer
sad5AJGj+rcKVa3Ly9FfA4S7MyWGVtmrIKEGAZuoTLvUYbPG7Whasvp8g08BFyTNtQsAHsmapCrl
pH8+agKm0iKcrUM2vGIp0FClNruXXzErcJbvDb7Eq8/mWKm6rZjo55t+G1W/h+nvK3K1YtuKNpNU
x49jXHD8lh1Dc2HXgB8yC4Rgc5Bf//zbFGAjiaUBrXzrmxaanpbgK54Sof43A9Cc8gKYOXSjzRUZ
Q8Tx6/ixrMoYbdcsSHBcO5sKtuZr//j/5f37Ht23a4ww+CaChYpwphr9knRs/RlvrYIYUX7bC5Jm
ReVWRUmXiZPJ97OS5PChFGmkrJa32j/WOTBd+sdaHmabgLYoO0M4kVP3FPinX+ObCLU/DyqC17fd
GWZMXcOXfDmMZ6PBwrm+wC7i3m45essbeTUH1FZplh4UVdWGwJFVZUKXRObqoN+4Tl+uQkpEnwkV
gNFDWdwXP8zOap2fwXC4lAKCBVCl1MGNuFd1210nnRdBpZtiOOjMkVoix5FyKd2IsCcQmW2v7qAK
ScNoN6LsrGMBwHXOfA6td8mDq6yQoqYGFTCHtPqrB7VkaT71DyhwpjpnHAYLr7tEfuxGxB5scpSL
ZoMSLLnTsl4JpyutaxbcDoaCIbb0TO9xI5l30IEZeS4CCwdN5Iql23UuXS8UaL/coH4U1Nc43k5G
tLYQhz7QnLTWHRbfGuDoXpbKH4E8eG1/kNAgmSzyV72gdWwaU2AjcJKTjJhY7UceoF2I2FExDZDd
K61xYsrBSIeJNYI6KRVCKLD9Y790Eg7xYN/8I61pgPNQIV8crXwph2qtKKYGIdjz8BSCPFmypDOu
trxI2rWGc8M0mxamfk1JiBE79z5lorDpahiFJGIDyna5jNaTaQcc4I7l/FD8LcKUUYE7hK6OAAK9
CZCm0wEa4jHZcos4h5qPjvS11wA9gkkv+fXPjSUe3uvRp9694Z15sVR3lPnoHVOMHAhjdoWXfdPC
4+RwiFRbAY6w18sjW39OJ2cqQqSWxq1q5829ayWITuZzQaCpbeApx5LF8v2ogsxASlKqCUDtPYjG
G5EYktIv8GfnXAj/4/AdPMoVYR9WDpHXAo4d0tD0jv5mODI631kg0IkxWF0KCIQCeH1wTpkZvZYO
pzeIPCVYJ+KcEvr0mRnIQKDlKzweIiiiznpshiCuISmZaZgF5izVeNoza5bOJm4OFitQCfz6yYVR
ejVomZz8b8K8CFUBdXbYuzZPSiBmdgJx/pJ0pnRmPaADAVp9QMVLlXexUDnLe+PN5DazM0QqcwXj
d5bzXcSFljM0Qk20uJOI95zjh3CmllqnfTwlv9wSAkHhJoh1WFJtmvcM7L6HtHEifRv8bY2gYCup
eXVaqcUdH595AnZpH/GD5mtjQnp1JHQd8+dAmWDalGSr0+yY0O1diDPAQT8onv+WfLLvXtOu/R9I
dkT6Cp99GFyxe1+PNafcm9m9Z4te//Fz06R7nj7kyxKI+UDkSFmsgoC2IREe2aDYdmZtRvYK3yww
SSo43WUUjrf6nGUvmmaMM/u+1Ke+g+I1mnr5b72sl9laQjkEBUFWudjIvzPfpjJgFI1fZlpn12YM
ojLnNuumJIswL6ujgsL+sCA+mqLpywPwjBxtdoF4dTM6vBC3CrlBnrWjuSslcLiyfEciukAECgg5
tDGx42Q4k0aOcIxcu999bKu6m0eNAE371AeVH4KslhLOFEtbm2R+OJuTCVx13n6IH9KL78s0lkY/
a/yyZ6u8b3Bgi+5TjGjtsUztJ4SXKiDtCo9j7iiifP9rYPZPweVkt2i5UFk6ht8oJfcA/Zy5gnEy
eONo4dg5lmZ44nsvzPBUi4PIFa2fDGxiIcfNA3U9yO0MPIQbpvFxwbv79WmAk4wghOSAruHTkJoC
PeoXAJZ7ZVFgUi11HNt3J+mDu1NinDeoZ1iet13iJ45fISI53ykH/b+5sP9cncx16wpZhlCN8Jaf
PH4AU3sSJ2nZo02gJsdILGahHatKAZo2dBQG5Zk0/rpHGwHJ/ARLZGUQgWFP2fQUSyIpDM0O/TWL
cTCUXrKzBdg/FiOh/2oICDeNZm/9hhibr4HFA8BqeW1raPwaEp8cPEZVOTGjJ4ZF8RYClFMKJQr5
PVRi2MdoCSntXpHyq8wlXnyV2+U4gqSXXlFUMeMny7d9oCVkDy6i7pmE7sn9jK8Zl96WBkw2LBi7
ED+SR7m4uTfgFU637Zx8PuhlxKkp2DEn8sn9095EtAzhDgIX+5udRh93PkYRoCE1U9NRVpOt9+mb
IH/G7/swjTwcJ+/potchEMmfyumhg8ZQ4p/Q5ywup3GhMm+kadPHQZnwKGnRRysvkvLHA4m3lxFS
9uUE27OMxhsgtg/DZ5+M2ADoGgZCNAIJs3Dwpi5enhg2GXsVm01plq2eDxQVSkRmvw7jMGs+jj9X
2sZ1LyMZ9vi4xSmbuuhmtw1NZmwoFwhmI9nUQYAMnJonmZ/JsfuvjiH+qJ5CjMEeHGKzkbx4ltIF
5afv6IGd3UzzHFXUhfOUDGmN6+SKXSZBdPQdBz/fJm6jB2SdGN0H76kF5ytUFEIYoyE9WOoOl1RI
gqSwFW/UhtL2dfwXZY+cTGUAeNmoAD7WM2gwj2MhnTSc7qprdsfyQlfUG16VooKpUVjVcQsDFamo
OFxkZ7HiBS7VZKofVqblkXpBm0xNVJwkiFguamhxcBL85Gjrkeu2grM9lCVKKG+hUuQ+QcM5mGge
J13TjYeSbqC+UaDnQhm6iAR+yY6pkC0q5k+H/GDdZ9tS5Kf/YI6/tZN0unAcq82PzLpe1vTf499G
vsFuJey1wpfIFcCL6mmCzhCe3utTdFJi6jBrd3LUt21rc9RQe9pignctx3P6RcDyZYHC1GQC+nNx
Z8PjGnFbYfbNrhA5UtnaqUCtTq7n44cJ8l25pgoX1aH8iqWcGV+aqDl6lSyhFwagtj9zl6p50Pt+
sobhw9yibsKPitwT/nsaoGXSi2Gnytk91ia1EEwDN8nf5MENVvD5hMzmKMOxNiDkJMfsNfUSLIdB
QLiziw7KEMxRwE1JpFEX8UDZ83TWUDtEz2TGp7dOjfiTugw58BXKwweOLD39VidfJHhE3Ml8JZEV
G8BJ/IN9uQ55NoYkqV45NeZW+4bevGmvoEpcuJdv3L37pC7GxnjLC0qFTTJzselqX+SZlyJCr7SS
gxYStepwf3jBF8zmB41ueT+yZIP1rOq6hbLTHgV1hQXFD3A2wgYBH53yiHv/l6uRcIO9LRsJ8fVb
cPGYcJwdZT69E5Hql+EI1IczfRkp05shwa8wd4Kx6rPjhD01mK/fiTIidfHFhKikIH303pr6pIO6
4GZxDzxd2kw/NA8ggTMQsVUp+ezGplV3YXeA+eWeSdm2krzr+5dJgOCjKJdkxayYG5ZXdJd7aZy3
CucaA7dyVuSKRw8NEXV6xBGY8DC8G3I+icdkxZsbhZY0n8MasP5yPJ8SC8aTpEe+GcGdWjkFilRR
CxqH+1b/1JUABbSayGHI+Fx0M/Gc0w/7CEEjKN1snMW542QQKwFwj/MjjvNmrgwnNcpDt1O65bnv
2M8H1lML9u7SBsPZ5pz4YFpfVOC1+jA4U7zvvjRWaFo/xYFkF9KTddIesZ4CHGLDjlvn28HFxGMo
B4xqiyCaSfnOy9QkrWTvm/ie6IW/0RhenIQzqZN7Jh39/RtQ7BrsaVrZtLuktGOSyJ2vV/UqlbFj
cUocZz+cggr/Irb9UvGZHyQ0/AoQ1PR0HZW2rlc103zU1su577KCMOwKBxhxy87YTwUKWFU8FVPY
Ukh+GREG67DCTiEG0XWUExulGFy7eZob/s0Xzz83GST/tPW+Ovpo/kV+3R6mXAqOfXY7v3xoQnAO
Wl0LoZ3eFqQjnsJLL+SNsiLskBEeVixTUEPUjDnCKlyz8PvcDgOvHwJ+EiPbtgNvD5sHGPZAiFeb
K3P3WhJfiKJinwH2a0iO4SSfK0ueDxzCK0d14heP2+cjRKJBwAYcFz/cznq/Xlvca+U6QFs80Z70
TqtRqAHJ6r7WGNkrRV/RTdgg/HnfndH/mc7437uony9xalaHEJcgaVzi1Zw6o91l4AcoN7NJ8Mim
3UGYFNm7KDm/sZNqpO/6/uT77Xb712IfzdMBYj3rPWW6Cx5WX0y426HotGgWbr6tIx0gn6kzRk6H
68dATz7oZo8+sxuxq0X7eKi1Fx9mS2mrVdDffnrBtOgPSj/Og5SA9vVYFRRwHGB9P5u0x0234xRt
CdBUbG6/ugFFWEkWp6DYZIw64OK7yuq+72RWUknPOGvecAfciSD9kN9Zg7WvzY6Zdmsb43eHSIf9
nOWxwIWDshNy1Od4OwmFP2DZYDEdRQGR3yb28QQO93H3ilzAGRZExSrRyB9MqMKhXumGTzw5R4rv
YV1Q09z81VPft4shpJxpB9M+RzolL4b+m27UQiDGhZLdOnBCsSVGAh3J5X0lRQi0SeUE3UA51+r6
ixq1L5zTLrXDVSiZ69gdGY50sCdTXfjLaroIshDkNSOLPWDXUzBoGFwuvWUxMQu2R/A8z7+G9z4l
IgS684/XkvFHW3ObKb2RRJ9DYsQDjN1Lt0UjLSU9I4dXyunGBMBWVMb5g9ffztzAkPvLIUJzzN1h
5/EFAOVxUClAxmFLSsAYHPPOoCfaabx0Sgj91skG7vJl7DN1IO8A1+xz4s3x1TlL3j/A+r0qkkIZ
H/Py5EnFAzp12kcyFrUXtaCy90P1L0POGz3DQOpq9FIfCwEMuoMmQqM7d1o5OI9fEl5lpnDTFJlD
TeV6QXyolhgcK1K104OX3wzR0lzLt67s4RNG+hfItW5HjCGSbtXadvTajJzW+dTDkQr3Qldden7M
oX5iXkhwA/itloWej7iUgj3RVV3a9HjnFwI4dI3L1fA3rSJ/FhbKsrGdJ6M9l2Q5MnrRVy6F8x7S
oZOjsMDm3tLApM9MDsDQb5Yncs+PwsGw1VVgK1zl16eVqv0nOR3s1/3T5PFPGJQqm8IyrCdUARma
7uBxWCLKplFYqWaaeRPsYC/ZmF6Ymaw4uuqSQX6KqAznLoYrAPc2E7AD4aseij6fWJ0EXwNfeHfG
tng8px1wgVZ9GA1MI5SCei302xINgJk3k5BpB5Xs4HFWIaPxoJqYAO9N4ezvae3gZawsX024qWLw
Dt6hCVrQIaFWm0FkblxfLYAv5FEisCGMXt0GTegdWlP2aGaAPaK2zB3ndyt2x93e0EChfIPxRRvU
U0e8qRne+gaVlqB6gz9ScES6y/AjziOhol177xGDJ87WSr+JJ1NSzM/Feft2+TOiz2Y4cO8ISR7g
+hFVcg2KZnIoftzTPoNpi7jz2gsQzQQugS6yK7Lf2Bmsvo2kAq3bhjWtKOusjwp5Ntr4IV/vMjUL
UNTGLsyJ66hKQ9lc/8IqpmT876gJt3geMkFZTyFyW9gSPzzacb9qHZFIsfh397GeNB8jn0K/k11D
nxCTLQ/uxAfsc0mclK7N4x8y/rU6Cf5nePUfUVoHtuEBb/UWTZsrFA9fs7K98dYn4jJfmF2tQn1Y
dtiEYMCNDcOHdOWSZMxNyq5KSHKlAXG+rspglGSVw5jyznDXxO4Jcye95QxHV5TGJl9Q0POGTeaH
5NBsa45i6R44F+b5dMIx0ea76sY/SfDY7IAMuSXkukaHcxCaNSCy7wqCvCdZnMvnGTdpLA89q4X7
7jxeds7WpfK9ifqd7wZEwlcBQtiN481mKrGb6g+31+IVmI2AHbXBZJR233PEmXlPsFlA480a2Ypq
xjYrj2trAK9E9sqZQcb6UW0j0D50rzMNQrVaB1QAh09U7hZMRzR7HHmgbOldWx++X/TWjPfSPOMw
0pxXgC5KUuh8yrc/zlZdjyrREBMLPRS58DaEngitOwcGvsUahHPj80pfn1ZpdOgutA2jIPc4PPuH
6lckjWolbQ75de9MD/FoFUIYlXsa0v82bM1gdi8NsfMiY4xn67CjrTAGUVqN/gMlBRGuEGMTjVLd
iHN1yLKtIQtG7NTLOwFkoH2XuJu1Y/9ipdUIV/eKbsJjNaAdQH9/Q3VGTg1kC6/RZZB3tz9F4+ZW
D3Rg5E3B1MlxxK8VPxaM+gLAzEb+tWZJpCwWxsPoOddSRBOhqQRbEHniCwUfHseAQmMVcE78ikfv
nE9yLovbJ86CU56Q/0S2GaDPFaomU9jxzyGWc+z/bpcgPQPD0bt1oqQ58s4IugCcJVHsHiy/USA7
WIHirA1doRnzq6tQC++REL7neJUp4J6YjUuw231iZMQF/+58T+E2gQqgzWrBS1yaGrC037Y5ksPA
UAnxNPEnQWgtYGgTYXi5l8nB67EQ+2wibmRyGP3pwy3ZNZoDhH421WclojP1bMYGNTpGS061afG9
Bo5VvBdsfQ406WgL+1/AWnnfoUugQaePuV1+mBndl7KUf17we9sEEnOE9eKujtHp3fNxEQCULkNe
jLMmhLA4imMcN7zhiVGP99mdApJvtBPc6BNLERfPN3/O7ct8C7VKqOF1rEZ/y/BRZpedvO3yUZBx
OGCd/fY44rINM9YhWiqDzxqy/6ma+v7NiPabJ+xu/ZY5LFKzSaUnEGuVk3eejT+O9PZkgIU9Iw+r
0eRF81ULRgR8eN27E5TwbbirXoRJJE9oTRXb2vo7ihtIqrhFF2/MxgoST0tZSslBsy/J6DvjtGvx
596P5L6L/V/FUpnTIDshFMuL589nkuiO0IpfLnSHWq9BhbelnDIfQsQQsi19uFnJq3z9oB/2u8qH
lp1T4k1XMWdC5BS+XpN6FhaDLL2/d04jiJBGsy+M/7SfOwgrmo1bjoHqnC4Em6QG+pPRZubpDMzm
naZX1XK9npr7ytZ/kfvhyxQ373YvrjbpEeRMrXU1ZozFSW6ACSc/VYH20Df/iKNwmhluGQzW2Msu
07ApQ9QBmV31LJGv6PtAdfSIdNpPDmXdKhbpGG8N/DgVqcjPitO2r9O5ksgZ/JSzI9KX0m5Fhva/
nM1fzPoCoqAMQy6VbsGWXLqgf6KpvZn/4dwJYyPk4U6zOGtcnSbHDUKr/uVXXvb8h/Cz4hGXIUqb
7r8NG6b0by2vZ/0OMca/rcc0tHnctgT2ysU0DLIUwEMu4SW/xn70FKrHxFl0g9g5ZNTKyDNhlxAD
PBC0IQjLPKzLgpKAxvAQRD7QCRCO4zswKvIBdZdyMisMGXEGAHfGABeWJpUQ5YrmaaJ6WrqRO/0z
FLiiiE5aUkSltSVuLAcmdb2oOvW0fzCJsHIcnDPe3jLeyzZ7rkeVFr88snPESZiCfZLX6WUtM8AB
HFx0ibKypPMgD8828JaFXuXNVYDSqEjwuMSlpdBkdYbDYTvQiacpa8Nqw+hsH9gBhXUtp4FhihAJ
3RrX5+ZT/xdaTTXW861rpriIACg3/3S49osUSIRlH+7Lpy0Xmg/GxFLKBHDbjVmNEuQPgoKIRuoG
zVgxNuXGuq7flPbqjzaoRy6yD81jxyzq5dMK7voOqdTUogY4FKurnQnQROBVTlT3loeHvDk/07cw
rp9fkRjXWHL/ATy51jC3auFQkn1PQYPOibmAKz3Gq8xn4XeLZFUCq010169gh04oKooAFyjjH7rF
5XjLLGSX9Fkg6ftMQR5u+xnEISz6+vtUOqB0BejBhhg+76Kr8+GPHGArPOC9U9VDAPInwXw/ZNsq
Fu1PWT6u0sQuh6NQms+bv+p7SOdC9pp2gcz0ZRNk2wnXffn8FomH7cuI5SjWx2nPSevoEF2mXLlL
1yHBEXt7Fkh1RrmjaMBtn7wOend9oAqrupD8liix1QHdFImDBSJyGvz2GM8InTOGLsCMbi4JSs9n
Ht8oJGckK/PVLmsWHJkUAyx/MCwop+YLkTxYyS0bPdYCVtzoIvBnCLBmYawSPAByxeul7DqBpenH
yf4vFG1LKjWoOpCHiMPVIQKWBN/s8Wcuc+Eq/dN+gUpdRy7QoBKjUqN11kdJu008gwrdV0lm2hz/
53w1n6whOGk/fQs3j8gP17f3KkwCPSJoDz57DVwzUEvejcT4XFp5E9EtZz6aqdddOjPUsHbneSh8
89BlpbFpCZyeLyWBuULZIT/alD6sFAw9w7SaQPxSUklBobUOHG0qtS9WwXbwjPQsaYOtwh9bjWl+
l36fy+mUoq3THFtEIj50UTwc72j9jeKcuBE8iznOd1IUQ7CyadyentpEjZ7LxCqWR1I6Y7CVv5vC
vwIucqxLXe+1R6v+oNmsNqWFTg+HI73iXqn1qMiyX9WC+p+ce8vqSd+1f08cjYh9FNa3w6DtZX4o
8I5MT9prjwDebB3tuFmv/3+hxwtsQBhC6Gnbm+7oUXjAH3TCAQqKiIYSZrT+/Iu4GQvsyTE1JpJ6
OYMgocShfohZiO6NHyxlXqn3g6Z/ZLFY/uyqXurI3skjm4Cl35o2ths0VmVQ+mWnDl5FuQLt1KQG
CEyf0cWn8gurA3R7D7vX5BOU/DkHMHcguWJ8Lrin+ndW9FxhGqxUIOZjwi2AKKAg+JrcF8UFANsc
zev/q0J2jirRfTslRVWVhb9wncRz4+wjRV2DYEwBIcKwBM2pTVcaaBzZo401Q40YkWgByxQmmpTw
XWVqfzsVg5V/9ZcHc5jdDbM1k1NNHFAacuIoMbJaz9xhJs6MEI3ovWhUF/BwHZFt2jM6jhfEgLYq
YQS01U2tsTy1duu5dzctY/iFPVxgwGbUs1e7MH4fGtUMfVoYZ0xPq86YFM1rwgFAsXcbRQynlKIB
5UpvrlMVk81v99dfL6ZKA+wYRQNpUWPdtOu9Q5ydCR1f7A/Y8ijFbXeYuZzR/w5SZrxN5JpafVME
lWYt/fZapXuX4cxXuEThkA3wpwO82iVIxNuxgTeYiaR7Hsgmxc5okBA5KZ7GXIsXtn3ve+89TrOX
9NxLN8TlKWFWAixK2CurBXIudOHtC3DASpPnQg8IHDx2wqGov8Z0e5+y4xV/PXT1XMxscl883Wc8
7b1UYwx9GXBXsaiQucd+trNfjbA9V05nt+fHQdavRd/ooVEfDHnSxh4Ke+6M1a7sGW2OosYB9/6H
UHpMhepPtpJrM6h3I3pp+5EwBgv5uw8Ye7ajI5EqvPwSk6oYJY9yniGwmPDwwUZMAh9PHNw3dHGH
uB94+F+/e/g2gesXK9W2mpOXOIG7Lp8GrN7VLItzaTjgeCuasqphP3rFSOaFRfbml9i7YfbHGMHo
ZgaZ93h/S7361R8E1/eM4zW26CREK4Safb+DXS4Cl49eU/Rv2BJVEDu/Olt3d5QoeqPBDnMvdlOs
3KxZhZVs3cdknGrf7k5tvnWhjLGOniW74t7aPj68fKq4oKngg4f7G6ZoQBgRa5h7ROtU473kNV+y
DlmN+55seEdIvvaPPSixaGiONMlTXEloGhfNpv5FWPWxK877G5GXqoCudEtxApF+PkCs2dWPzZvX
mPkPcXnSikW8MkGfQwKZDaFG7oFmtLK/c13FXMEeWwgRnfbwnkLkWRfG954JwVYbUhIqPV5+3aVI
9M4nnD/bhv9kfENuBRnlgk5ueFg/mD96KoOPVobsOdF1+FXkDFfCITFtYo1vVXSFqriUYkJK/ac7
1EqAcCTpWv85QQ33y8+bh8kBCYvWT65iyOp9stX0t59D7I5XDtBIGNWBfYUO4ObogAZy8edVQFTf
cortSsXuPisXuNZ+t7e7+ni2+2B4zKoPXhNXCCSVboebb4BCDC1ficC1mzzovWGDSnJHekzca2OT
tcPafyTZQ7EQmpJnvAaktlnLKWQGpg6Waz7EJuloymVNkL8fNxC3LRmI+8eVOB6FlqfmRQXmk7Yg
pV1OF5rtLGEgodH1AL2ajHo1ylhvKl7N5f2IvqIanClF1cr238h9uJV3gx+VNHnCOYstNEWWbXMG
wXnPyYfQIFVY4CyxDNovZ5hyUC5B7vOpyN96FcdkAyVKRFwOP7ksQ5tCm7vPBA5MlHwWKFt0+8wN
7aZvefTu91ktYYNWJLDVb5YUBv1bWpun3H+SS9GyTw/I/0+/A/WvkePwqk6ue9EfBuT7fINic4Qo
d9yq5qn60ryvQTeN2HciADjJsASPVOB0VZEVR2tNw1LM76skh4loH6M6id/BoxrO8X3PVxlV9FpU
6VreJqBwJKD0VBZJt9mCAwX4bz9+f2ssKwZlbGCPsyba6mRvAiRZyxz+NhmfgFajG3Am6KgPdE2Z
i34uYyU2rssVh36kEIsFHtytR0L3lwqKWAf+pkyvncaYLHcMysKqXOnOsg+rtkkzYB3Ldl0YMUOr
wbFz2xCUP1L+Jf16VF3Jkh2fPoN3KJqmzS9myrft8W7cwnDMfCIodJt/F9HEw/xdM8qg5w1AaqhD
KidZHF5VV/kX/i5bdaXSYpCxE+mrYES/jUk3rwoi6H1+ONgTuL4YHjrLFrCtECoW5B2bJCWwnfMv
+J1MPdC0MC42Fm0cON1dxK+Bg/pl8V7a/Uy0GYSYwjXLxOaUkE+0nydRJRly7AmWbidjvXz+TSYT
ILvWjapfTvXqqe6XxjTxijpJjtZiMMVvrePs89I6FxfzklxFCmCCWRiVGEFRhF/9lqrvHPUY63D3
fcg52CY16OSirYcJgaGvj6+XOpGL8/eWGJzpgESsdW0cAkxJkF0Ldot1qq+8pEowc7Jb0FyMNvyQ
05BHk0/okQ506hIF3+6mfk7TR//jnl6rssuy3URSD1A1efeR1viQNulcctIN9icWq4mVRHgW+LAT
Jj/2cTYFrkN+DLMbroOGF2ypfdKLwseozi6FLSFv29VFz+22n2wzTOlO3ruX4hYq8Yiu/WvSOYwS
uFLULYKRzhi2UoVQiZG0OBtid92QVtbuN0tqJyYUURK+7WjhWUAx7QJx9JYteEkqQfLtdrlVm+Zz
TYR7KHsXuvBx76zmRYWAGWfeR0X73umlrpUZ+OQNcJejNMS2sOuzKFyB58Bx+7Kgv8K4p97Gn7dB
gHfrfKaeahMyy0SjOQqDULvleT1iaaaNPo8kR8PEnIcC3wG1Q8uqTRfeNXtcVcZ3VvtnqfEbS+Cl
3IQASw5SdJ0rR6hM3r8Ny3/4UH4LrkI1UQXJY2hYVkjRbwg4YmdmXqZ8JBEdHWVMmlUtgJQqIaqo
ht63m9URNp8n2V57vvBhvrjh8bushfl3YS3KAsQFL/QZlmJ5nIiFuxbppfLVIsO4S/Vt6EZiKzZC
gRZdhluk8hAdKSs3lFTu2Q1ESq3S68v/1XVpitmgenC6OH7ldAofL9X1np/YZlLS38A5XEFpcDWO
QkCCGPdh2Not6ZbcKdI0a+MGJmzvki8P+nBeZBr6BuluNNPPilqrR69THoiOE3FR1i6xOh9rvnCG
RDfxYdAlYb/c1ecZC9KDdFXfJg5EXJ+2nSIL3+VzYkAHHYGADUcc0Y27npwiUiPi7GfFK87X+6rK
vwn7A011kFtjrByFgRyuoQW+I18kuqX8G5aIb9vHzy02TloHIj4O8Eq/2jfLj3KmJv1s7WIcq0L0
cNczKv5sHTNlpV/FUp3LXKhqRkp0fPw5lNYummYTX9T48u88zs1WTQxt9YDhcHGrAY+c/1F5n00O
FZ/U2NxUsa8KZi+mXYr9ow98H0mtHtpB7yNXdWQ5zD/qDjt1gA5FCJYjFuyZCWdIQQXpg8y7CcK2
vgOrgCTdLBFO9pLFxaEnLNPtjKnsVhqHZyqFZsKI3uiMcSmUyl5HewQSRxeymsiIwwOJ9C2K98te
5o6sg/gvjlcy8yMVumKU7nm/GQeWh6rAcL7bK/sj9DgCUgGUWrjd8QXJ/DwblQvYfZcc45YAFfH1
rXv5QqmMoXjYViD9U/ieD0dtSZ3lPWTZI+aHNvpKzeVbZQrjx0KBjxXK5BfwaTs0860RcOEojPsF
Rgn8B9ZSw7jiPX589XAoh0TwTb/I69f4gIWEbaDpCGpR0NujahCZX8QJY8UEXcct3qO4Hy/KhqO5
+E28qIpONC7BWw4/EuEin3luWNKa5OwPN1IJBl3Y+n3f0OJ1zVCgN1pRAzlTvgZnkBUAxtKpyhYM
bhw8Q1mOPMmhgOnJgw0hW9BD3S2q9gBSW9M+Vgwsc1OqkRKBo4/DOSWHBGeJo3WdWjtUQ1Q5L0un
Z3jHTRWAxz1iPR2mnK9jlHduTtrCOxNh5AC8EfV5V7jD6y+wbFSGPLb+3fS0RKYEkb4AzYsx9Ftg
ug0Zkm7O81l+8nEcU+OJaHiqkofKDbYIHN+1AZk3eAM4dCCglAsCoHaAh8j5uKY1Vuiw6oZFM7KI
zu54uWvZHp0wYwJydNrEPaYb/2edonoUEFPH7ufQ2gG2RncG0PGkm9BrVGmDCZPlIfwm9zb122KA
KHh1oMIzJrJK8VzGorwJFMWaP4MQhZSHfF7Yxc1xAM6fhSe90wu0R2s5l/GivLDnThP0z8ulpQej
Kz+7T0UZDoujKu/hXcd2ODjS8ffOzmPG3M919W5xzuu+/RFc1uYvQh5uB3rmAdlx74ii7/9cihkm
tINPqVW6dV4OF8XOxAsA8KBrU/fu9YLj75Ld4S34sqOmgPrIQl28rAvAGBSN/+O5tKuPtm3WR04I
/5sl19wg52/uaUfNrDmD0Rpjv4amTP7G1UbWBq0KJGAos113wRBsQ2wXlR4rm7hlRV5Fqnyy7WiA
CdQJuROwjhiBcjVR5TcHvizBWDMcXvmUTwncPhjnL0yOHOHqfIKS2bj52v/pq+4yy8CattNkA3qF
FbxIf6cBnzftoEPdGWeNcOEXdXcNIkbU57sjnEeh0GcOneRN3nu4k0t67OJ4RrQF5pNxgvK26pg1
xdczpm2Yep185BJvCRI43LrPpgSFMSaIKO6Z8a7vNjAGwT/cfYXGVzQQyiXaviTJMMBRKXUiS54W
hRK2syulZk/SWAL+uIU25eYrREFeLEq3JEdoMJrZPJUfLU62a2K984ztCZ1gvnQ/qPPngx/JXNui
AX9eQeJFBRlRYNxXBJ0tK3a8SdIeNFA6Ka61jBiKdVCc/swChLmcLvxEiPoqL/kVio7/6/Q7CuPe
On7UriaNteLehdLNyFYjy3DwmtN1KbJFY29W8sdhLexSITlbTV1uLRPewJoxayBENzq6XWtN03G6
Q7Bkn8/7Q2AEF9H957srOrsXEGOogvUaYOJdhOiVDoo2zvzfUoVUtd+4hCBVxtLSRBRojEsjtrev
mLM/VEmSlP++6MHWLw/K9fdqLmgVPXLAzT6Nd9nmLi4azmodifyM1C5qYMEGF3Kw9FODNv0cI1CK
nm7yIVvcbXM4tifvZU9ZrljmjPZxFyc8oHkhv7sMb7aDyy6miauyRTLUqVDCWd9oQSDLIfEH8Z9y
FulJdNrclj9ZPauinAU5xRK4C2ArhR/A5H3Gt0lshVUEjTYjbnzKumEAw/Wu3OUqSE6tlCTwyurV
ZpqfcTOUslk1grgKYzg1YMxtT0d/0wIlZh58wKawrGYIHeQqSmgj8Rwp9JKSCM6Xp/hWqAX4xfq/
kM8qDjtX4QLJX92nkjUwXBvce56Of685vEi5wsTqYewB52qBMIjco5aIX0UoOX5tm4uQND6+Qp5C
rQzvo1J3aO6eeaRgCHLSh5WMdZLYZ6Dm9EwVrNgNHd2e7OZd9ozfDIgJKnk1z+ImvnyYG4wJPqfm
AqMfqAkQDaIyU0r5+8chgcjc7X2B8BH+m3haQeOCxM4eeCiIx4ABZKfWcH2Qel5EkZbOKVcuMdlx
23LHkyG8hHD1gKeumDr32Xt9NSKK0Ej2J4jKqJxZHOyjAay+WqSe5fQn7wSqD3USkq5lr0kFqor1
2jrQuVVaNxTu3Q0xMKdWTvwEftlcMfWJNA8PsNa4EA4m/bz09fwJdbgpQNkQiO4BWK1yGUN9YcHm
PeFahb6UySdthVW//eBwPmcGANYbymFtKqf5Y2I0hZqAzZik/qckVwdgaDLsKAhy9gH0SZqI3zSO
JdU9i7BCkDl9Bs5hm9Qr3QoiHSvuoeRatgNfGhhUnt1RlW5/qqP8cRMrcOLLDm7Y+CTnLYdAMuu2
fGFc4vOy5Kx4uDVvWY1b9INBlHltTWKhxD/gDuke0BYLTY5fEFgx7iTlTajqUruuRa0s7LnSw3pv
EWsy5n5GV7CbDlCxLyGyD8bkNQpT1rJ/h/zEzyyoYHwP5FIs+SxMDBa9FW7fXLFiI2dRNMkMJAYg
LF7uiIZoW3cgPCVNjZPNzTkKx7EXEi+l7U4rP1GrNr36WuN+cKFYtPHGM3WqnaDB2EwrE+AGZNR/
PzRLQo3OvDE2XakKAQQIf8Dm+WH5Fuz3pHN+BHw1lANVWZanzfDLi91Vuj7mD903bOS3/WQ4QXai
CmbVDNKRCub/CbsAIUbJhEnmQlVr0a4a5ZXWnAwAFE4A+aRY5wPJ4w7M4PPHSNTxBUogkOb4IVUf
1OgtqwC86gA2pmaUs04nH3bXStnbsXAsiUUeZ7Bn0gHaF0HlsUKzfXpBcV/6rcnuaQcml5eK6poG
KTentkXWT8UX0YGuSIl0puMWrAh74jnCLGm91xrRLpAqwk74HVsknqNoxXQ77cNowHX5pKkVSTEX
2HVtSZ9/0jYq9u826lvab28uRMTrCKUxw+JXCpKpiZalPsYyPuX/18MuZw/OlHbi8UqEp/EwE0AZ
Z5iVtyIwGPtA34GtQmWJ0W+Y54CA7x8RX0l6J40b4c+ZER111u7cCRFdZcK5Y/HBbh4v+hhZtM+V
5RPtH0preFrGBfydbz7x2SdNXTokaKdYL6K4MwQnsTxT06MvyUS1ecjSk6j/fkustcus9xPE6eQb
c/JTN3YAeQYS4ZEqlKeol+HKV70Zaj5UD8DLJO5v2mXxhwNmDwc3gvj0NRwewYo+pQI8a/74XwES
fepoL5mW1EINjWZlt2JFjcbAfTHh/sZxAd1b9Z8qL/NL+cqUIVkZfKOCy1artXRB2Su+HfcfO9By
mQ8RNzXMzlIoDyGFHU9epY9TUstryutekVsCqRa4Euh5upPdxmgRG70f0Nd7cKrNkELgIwf0+pT6
amJ7/nimvn4UZKdtaAqk7m9ZiP2MiadVBFoWxCFW0fDaUBA44eCBwXNFFwXYYu5KPMOTGhM+9vc6
Kn3dR80axTiEYB2LcuqHwAm7+KQGJt4TppOR6wK+9QfqU5H+bFU46FD1VmJw/LTCTlm2Z3iZf397
yBSSmunpLRdZkd9Ae9Kw0ffrVzVc/4fil+9Ahq4PbjZqYghJOZeV8LmET05yPzFwmTg9aLCNi847
3+V7lYhSz0wT/FaSQYCT85D+4i8ouHTXmPkQzQSFkbXk6okoiCJqQ3X0qLXr0YMFtmDQ3FN0FbAX
KSzsgbFaZOoYkPlEyWeKXRRVnwrB0l8S5bGouPb/lXOT2dzOWhIOnyCp33o5nDSURmgPWMXX/oDY
DbUgSJA8a+y/7xHRO/+Ar1T0DJLSKenBD/lIrjolGTqiZsjqfrt1+cZPpU1IaBFH+SdvGUb1hcb+
UaxIRbqOTppbxFovd/SQO4PshQsuCz94FfV/QpGX3lQSPmmDFtBIJm/qqsyHDuW8owlknQea6hD+
gArBBzNlc1QdjE/npYp662Mb1p6TdoArBIZ4EuTj2wXE+5CzYogB8zibbEgRCAsVvKQkWMTTDI9+
jnEFFmAAjbudApH87eGIa2LukI9bOMt3r4OXKuMbiQ3WBbn31LIoeAABo2d2WIp8PPw6fTaWbaiC
K9gRDo0kDqs6tQFLWfpQuMF1tpXdPXfDXhQdRkuIqwZp8ZIBy2K9nIW4wwPxtVSGDKtuJ9j3yL0r
TtMOVSZKX7SnqQw54TPXWtcrCwsc2+U0NPJ/YUyPfDHBVn7dWENGQQW0DM6GGh2jNl6xo0P50pWJ
HWk0imiS4CqMAVfwFnYoJ8n1nextcZRzpB3bP4Id43w9dt6HLKKoSF+vchJ1o7WoX1mO1jT5d1GF
DjyNBo4RzOrNS3mHRLB0y5K3PcGF26MmKhBauSOem3NLvCXOZT83NRI1o4QYPuAaVM3O9ztyK91v
2gBlraYofJdPik1L2gQCrVV2P5Rz0z7xUl3wOkNS3D1p5ci0kF3ROCZmpZjaXoA5TtNk9jA0+T6k
WMR9hCWedh/QmBklt4ZdJrdVPxYMrbvnWEy9PKO9tLIg3GwhtGMD7woxJFOEO23ylerrhriS9ORB
/R3iAdLaVov+bB8XBwF53+LT4mhvDM2yHQVdDCb0jQTyVR8mHoEOzvO5WIywBRNWL+bcHXIOnVCb
8KCeYMKHceikSJX/MWk6pd0mtDdqpEFEJnp8bWLUPaFCvJwhENzTVVOFpvpzNlVrWOpGO1E/Iboa
sresOLJn9XP7UdViB5ehVlqCgV8eRHLYsAy+ajAGAziIImFuDDoi71RBDvdjYnYjS4EIUTvaCcdB
gwFbbWyhSxCN/i2f/eNBi0eJDzaD9FiQRXZHgNRE/tqbD2LBW82IfaL9EB36j0JeDWaPBkYzhf/Q
ExV+sQPPuqMoVV1mP2tIBCI107lHGQDTGZTe1az3HaA77CtWKWFR5Pbkt3E5Bdap7jKt7Csna9d0
9l7ONMUrqeIpjp6rSb+IRULC+nGWQQkwD1ffvNcluBbEznMQ37csGbtpO3xzBHfYTlqOrSHIhoBB
TrvzFiNp+pie8p+rFAw9bEjrC5DNjJPXohxoQuOtHlZ4fbtT97Zuj1Y7958Epzp4FObUeMQd9TYV
b/VbIEGIjKxwE94LClyInt501MxowkzaNtbmlg3RRK49IKjsgwxyBG4qymbPOJ+OY3DXTFNaQGEH
8WrSTEWi+yFBjRceuKgutdVEAd7qjSej+RAN30C4ndz4nTNJKATnP0stHT4zOYqGZ0c3BveNdn/o
tj5T4k6/j1Bec9+/Gb1b3LGkQixllZtDFyJ+KUVN5TwNeqlSooo651SDeB1Mk5HGqG42/+qORUm8
4Nte3u7I/J7YY04XMVgUAGJDh13i23QXdJaNR9tl3c+HEjjrmy3KbK6rbmwe4pEGi9rfEktluWKI
uFjiIJvplPhAKALLkRrYufAr9pyry91Y04FBfOD4Yep7SOlUSY+SPPjzBAR1o48r/igPL5S5vQIB
onYcP8lhXOR0Zb5ay8e1fRVIx5ZlgIqMcE+AZGTXTrWHQ8v3nwXULwIvVbfpnGrOYMblb1PWRjgC
ZAlaephBmwJpjWEZwGhI8bzXSS+gYIkkezCr5tmoD6HMnKePfF2cJKUQlsdvy8mjRs9Wi8+hAHAc
MqQ7Pn7KRPEC1Ww6uZLtfGTqUfuCc0AFRRQA8Y1vN58/H+so1CBkhVztWszpf8dWwrGnAdd3TXxo
twjDXXhJexbJV3Q405GE2ufnnmyVvBMHTspR+VSfBTk261KC53yGlixTt248KIBI0xLEHFc8rSeQ
Ms5WU9PKpFVRosMtCcMqqgoaBQzelJi93/0az/pAaAh2vveXlYF/aGqHxkf34KkjnzN9Mfp8+SMa
+vVNZJK70KnG9AnU99yi3OfKGCXpO8vWSOwwDTB40+gQyCxz7BTI7L7SuRl7zzbnbeQ1/dsOA7Xc
lkrwpvPIikA4ccnOw+JE8rrhyLhS4XUnmVhu+pi7TUcl8ng7uIM1yEfc2SakIvwW4kSjjQlzFJGa
iLnPh9en5854i5XBnWpJkq7FSvFtggJurMuhO1vW6olkfz2megJoGCfCuI5SN4lbxYlMJb/Wcbz+
fJbrv+EYtYMkuifYk4CARxnNtuy5Deanwuh8oh1gJPto80zGucX2FXW56/wtlRtWvvkOZ0KYi5XQ
HlG8krY3OqEpiGt1X2sHID8tVFh5KLMMlE3AiX++jR4LZZWcUyOhbNm7d2Ei/eCGMnQxSWcI2b8E
b8kDT+uvZDdkX5U4bZI/XRxC+RMrKm89JukptfMDDeH0tsrL5kZm1wkImR+QA77suQZZAvqNZNnt
9VORDPsWaJ3l0fvw9BjdroKaFOtPkl+COwh3khxipxOmWRtZJl8nEX+cqbaEcaw13X+hXJ67qpW4
X1u0mxePQyrD2doO/A4n7VTfsNJalnJhmvuaPCS3zFyttv/yHx3/bcWhwUk+sumqkv1y5wtzb1vF
ZUKoH17PuPqGO/wSO8cerQfb9uejNtrBcyTKG/TFLGo8rZHep+22m9WxAkCQdtY4RTZkQ6ji+OOr
psFU0po+orO3yVMnGbz/WfXIDUR5jWydplfEqmEdSWfIUWoqHR1CEy3kl4emUrGye9q8Jg/txBcS
G697YLkurPYGja2yRlHvADCuSTJUJ4jXZHo3HjMY+TT+7vqv8PccxWc/lgEZRWerATEj2FSNx40I
ZJtjWfuutPjdKBP9aElkT3WgxMERJHannk5uKeAPAsgOXxvLJSO9MWombA1DaNHQIak6iwKSPnyP
ji1/Dwcv4M/GQmUrizSRPH5DbI9gkkHenYn5z+CI2InGJ4/HZ/DdCpjkWAgRjO/DCf652u2vosrc
9HDfAhRaLJ8flSaXlskkCecigfuWnXcOT8SDjxrwY6sDfr3EtR/WUxweBPc+c9Tb+mf37KBeR/sH
Y2T3fDxNyTDoRDVtr5sA44ljZCzFpmKklEJCh1pXJN7pA99nVEkBen/6ejX3yKeSMhOkP/yGOAd5
eFeH44jhipZTY4hm61JWu/r14RyLbkx3AwhE6tR1jsnx/cSbk1TEfBxjTo/Rzu9cLDsqoNX3UHcB
Gm59qOvqHlepieprzufo14cFuWMsq1msEeAc07srB1CMFQTe+7//2+0l+9a/2useR0y6/taTj0o+
ELy1whGwNPkbTabHPn8qKm8LRDGYSb2NI8N5+56jbXX2EML37aInZJc6Ghud2ilXKRRIDUoE+lGq
1KhPfuLjfEV8jxvwXnQI2X+gNirXeLSJst0hkxA15BR830BDeB+DaIajGw3e9rl1tEDD/FfKIkS7
j6SdVAEVPIHYbOsV/HvINt/UkmDG2b3bPOHlpmEGd9G2pwGjk0PZQbtbhnkQsorKyFbXGcok8HGV
2P5dwl3X6o13+OZpg5bJjV5eVRHA8qusvh+jFgz6xOB1iSfifHJdq+W3uAP+pW6hYI4MApz++kb7
RjuevOWBkjUon+/ahU2XJai/jVwYqyLndIFme4vzPxD61hNasYUAjf6B93yiJukAorPKpPiVjr6c
a1sEsXVzuq064ttHhx24WT1mO8WGo8pVGxGH3w2jkZwtgiTpjNeE5h4ZlDti4oKAf3w/bX+it8Ev
l3p14Pec/zd2GkvmOWBY/9ifUX7DSq2W1llKU7DdTVzr4tG8W4hXjW+6O7276NhIXbYSLH9VxpPi
3EFguj9kHbcQrzV8q5igzx/OXIaCKLV65SW1mkvpepQ8isqCFKqfbFnhq6kzEEa4nptcEdCjUTsW
rt+3owH30D7bMhPoDMKmiq4x6bFj4cuBXMghAdZI7oB3SCJD0TnQu9UbYzlTpk3evBvoTsQX9AIJ
O/o589YANSjQFcGsW6oTbKHXpckFQuy4uN41SncY6+42PgQxzBDuOT2hF58/2izHHgTJwFExHP1i
zKwhtsaEKLIU5/HG7wvEcbMN7p282tYF5TJGU2NvaIYUeMb47JnvSHI40P+C9HRdxNxXNHoxag/r
+v6gJ0uczjiGwVV1rT5vBPLQ5ywBPJpFQJCaqrJ39YaRamCnGm3z5o6CQZTCCHn+zIQPDHiXfNPW
mBg6bOYEsDO1e9sljqkr814V7/tDIpckHBeRpiD+XLhQL5jpzzUS9AcJfAhVVSrH/PB4TCK0aTJB
MNaqPngL6fXvf9rypqydFLEHPF38n33pFpRCzzJ7jXEytgtryd//ixANgcJ9cSS6RRZ2KPeKaG0V
rht7PWOSAc+/Sglm0rDja50dtDT4SAu8NvMEAKp+IPDu/woNMVsYQQFlASaSG2+ro0dxPvUmxRsk
MHWx8YWX7yFXh5+m1pxAWzS55aRlph5+Q6zpomRcGcDopNzHzp00enjPV3d5Cbk2tTfJ59+OFxFX
bxiG6kailgjjHDqQZoDLjIMlTy0sxCrHFU+MXuZhWt8S7JlsPwByrtPSY9H/GgrrB7APn1Vf7YPF
hqmB2/fPvQ6mj9uULgZNJR8AEyCUSytpHzWXhyQJW6VOw+2QfvWGYCOVEqgGrEEDtvpIp5Wt52mA
IqOuQT9J7T1x3xyXf5vGHogV5OU4oN1fqr+QmNqeRyP2r74h1aO3IOU2s2UJTEwSCEDHARkIpiF2
ZzZBxV+vWjc8g7Pft3lbWW5KjieF0xSgRuxOmJ6oPSRRDyzvKWzRGhb5KgUFjV9IJeARi1b6tSC7
Fur8bU1pqrBVLCtpl06YS0WbE18YgpLHWSQkI4IlIloSGw0X6l2Y3I9GRC8K5SoTYx2hwC8tCuHN
XGpefvHEbURANJu5YC5lyvsdU3zjTTxqEkyTSTUt1ye9XV6m2pKEce8uwtcaNXj2y8UgkOMdMD4z
dgDMjlfGi+CoNpEcoBX4QtoI40gCaus+GZcSy1lMlJX8VTnU/oBmDnHWHLAS1kN67IENwqDFqYYU
4uYZcMpJzcCbI0OFlbK5+xfxddcIDX0pblirv1ZOLASmZykBwH2kumEenNkbeoBwaDjCXlckXSOU
RhEQGfooOeOSstcWQJe0QqrRv7tZX9017zzZ4ZmifCEvGbBQpT4dIbCRz0b41NIUH8YaQ2VFfFI0
FK2C92GG2dIMo9/xiQHVY7bERs8hnrtyUAG6HEE3hAgajV/5PyElA9AirUZfv83JkhcSBPqr2CyR
FGxbGVWqw8ApApqKG6pC6S/ikzQA/GyD46uErEp907L9TOGK4gWnx8b5Ibpysp9N0WILxvwIN83N
UmUItiva2eYK5mTEtm/4ARvs7eYTXVP03uSrX8fUwaWcwhB70oq3SyFlaxVDvi8UgLEcxSeHnSbY
6pvJtpinfmzmkvXjNMv9iseLtJpwTpd5Bq44T9W4uNwCKKw09p5PsO3Wty1wRTKGYws03pA+2oST
ZWeFg3UgYi/T+rqu7rquW6IGTLwVnkfT/OlW+XBy/08LS33KubWZRQLYv0I/kIe2h72/C9wZznzA
7Xn1RSHGJqC5rsA+oM8RTns6HdyUcloMf/lV7AynbPk+euHmj2S2jAMZs3QaVKmk+NagWRxgvtG6
lib/mMlzI19Gj9XPciiTroVFvVSvcDJzVhFaKydtdgiXfK3GWpZbC2+2v8RLXCchTjpzcnYgkRVz
uVgj53UWVrGczldgx6xfBqmeLWmjBKYqxGEhsUKsuSUPj7/tvlqYqEtaDr9FWIta7zxxv2W5YJ2J
StwgAjqdFjdbDFAYsRKV77nJXe/Oba6q/lcxMXpS4VIz9BHQRrZ1kdaBshPwxdd1Xnyln9F6kGBv
nTpJBuSJnYvJ+MA8iGvhBLLkiMFtVcnUocDbcE8fBfv3eAGvU55UPii+cTnI73DCS+z3Xy59Io0E
KxtMM7d7pC0LaGSNlnuDp7opbKkGiz/d2tTfVZqqcNdHdLoMyOlKlb3YfNOGZKgFJL0iXSTNL/aH
hwv0H6WXpU6A02Drgk0PoXfEP/j28TH0mCxbXGTn68smpJyounKsbTb1EVXqUOqf4ZJtc2IdOt9p
XOlY6beMVddIHeLMj0OIyPACSONKOCNxGLQS3/s5/RbkBLl9iMasxPk2vZ+58Gf69gLhBd9U78rb
IJrdWCuZPIC5ZgXrGfrYQ9/HWf6DByq7qZdUzcl+RErgBymgyrLslJHBSN1P3jzYtkjwnZttUnbp
bjsRlioi2MloFWTwxxGa8AlPZ2MXSuMYQBIxBHI9Un1aSLqBmy/e6ztJ9NOipRp2rdfmz6vukza0
tcdihvO90ysobAzuntEH8VXE3DVjMh4aCnpJhICuM5IW2VXUUoNTMu0jgGJXng7/6/uZ26rs4or7
Ujrvfc0Zc1t3xYvoIR2e6ST2JfgM/Ai70hW3GAarfx5ytllH5XVParmycEW/G/DHQiNryNcT/xbM
DIjHdGQGHrfANJY1Y3Sjahe2d4GzpciwpcHnCL2Ar0bB4u+xEIjcuKJ0UqXab+YTwx7U0GPYpM7g
NtTRqv/wU9Mms9c5JMHmVy6ubcHFsia2lo1NlSoVN6nRB9DOIQ9DyhqGbwxIuTJvfpCjuQ/mGU+3
v6+r7AqkmYpQljpZgRGcMfSX19QQ3F+MrcFyRZxgn4/OarWy7BTgTNrqmGYH6FyrVIIDDr0AISg4
UGopvkyh9ZxqGjsfDRMstya2t14+YvnTj0fNqghcHE93wjwzducCg8LKuLJbGQjNQoMqKt8TW9qW
NWN5ForF7YrTbCwSQ81Ivvue/K3PBE/CshLQ0stDTPCaz7dnqURq+lO87nLY9VmwiHXnMxft0UXo
C9YXkoo7ftHsdcnABj3SvkjwO4zbcufO6lNgT96wMZSRKMm/IC6wtlsIlKvvAEHwT2TX27JbBLje
tkbLSf85aw5Vz8oHRqFGbYCnDsvvTv/a/Zac+2JL/qmnFnbeoN2LuhGqAGHMosFmhKh8Ky838x0X
CPdcqgwcuWLs5MS+oDbrOzmTdbdhzjgZnj9biVm3zowLLPAgJSmm49nVGV2RRcNudyPbDyM2ZQy1
oMhTpYVnGSrzH7mKzmKBSsn0HYlhktD5rfYgZ6Hj+36IyeLRx53gQ4Wtg7SZcKnKoepMuwn2cvXw
JFo9brxPYC4des0Bj9Fw7BaeULVv+asIyPdxF3eabDKjaQ6ka21kr9Z9/G3D73RbD6u8bBZsrHEa
SMhofo5GVQGsuXwjd9GlncF6xlBgg55Antk3TzMftSqjzKnB1RhZ0Bt9wx8Re8R6Po9uH5oVhQTg
0ffMxkk1TAkWSAfiPspIOTHm6jA1qlG1DOvH5CFWkctpGukyzxq4/FtQtDcTKsqWGKnEBMfF+bCU
6SMB9LvNC8r7H5L1EOVAKTyVdEk+3eelcCxy2+N5ZgqANcYaFnNyXilxt7WWlKKlnQJMhFJ0nb1T
PrIOLw5ArPamdztTq4W8PIs0Ng+ApTqvJE6VUQLaE2PJcGQy9D758O9kTawnBuCREZERrTZteqAz
kHZr5QFOaii6YUqSCQBs4lqvM+9I1++TODT1sOqBqNWb4eaU28+L8H3HcPvG8oOfx+RR/agDPOfv
7BYi//G9n8MhNByp2IT5SN+DhfPybZHPzmURAMIa2SGZwMhG0IzIiCjyVaO5MCLTop1cXe2H5tfz
oUdQ2joo4i+rM3TS0OAg9Wk3pGVAQkKlCHYYl79j3F/qs0utQLRqgtW/uwwSCAnc3buOMMmcsbRL
w/i0tLpjyBPzvEgBTcwjewMZBk1xKa/eQ5jIVepwDXCUCAXvg5xAeUDIENFRAkms69xZB7WwM7W8
U+RaLdKTbH/TslwfxwEKF5AklhdCLRzY35nk6mSpWntDS8N5obx+I3KXyHPmfbl5nYAmv/ZV3C4J
OA1b8owuEIKdp1OE+HP3WKThRPskTQnUHU0+rzvsnSrqrN20btlXXLIkRNP6onx3mURqq3mR9+pm
uVOk0zoGgG9FzcYZmZ0nm/308VeBSY6DNtEOT4kRFS9bfxjSKtW/VUUXIaQXpMfm5IcL8p4O+2EY
0BzHxxKxqvjnvw8K/TIEBWf+9MNDUV6UJF53hpCNL1WaR/xTu5uMOkLpBUoEiRlYjVwNKk6c1ADX
/T+CNVJp298JPiJkulppPLXmGRd60iSGn2oN8jpLW6114dR5cMGNYH353epMXvWdY74uIe9hkDyx
ClEN9+LGJfsi8oIzx5Igc+zslbUl/e+1+aYHjk8wFR9KkY1PwAxfxUa+VDVrPm9j/PPFpbsxoHj6
K48tWYqawgJsMhfcsTUuJLzTvWD3Rb0N1CMyKKF0yeF3YXgOqyhiWGLFwWDaFuqkRdYnyGpTv7sP
RXMrg1funZWasBJ17/3QNdcaO9V5DG1LCP7176q44wr40FlXZSRKFCcOLX/W+ejtPsuTwYQpvo9v
gAu8TLkQcwuunmbyag95HT3EczNynlMg3ZrXSENaDUvMgNVZCv0X/QM+h8W7KqBJ9yxzTM49imNu
dUJxJ0ceacbOg9ghWbTcys0J77+NoZtnU1b4sKyPPt7QJrXd/gRMhfGKZe558z7m2wUODY96BRex
/2w1p4/CPD8fxP7nxEr69l87Es3/zDZt6gg2/ag6stc4omns3goynogn9n8Np/yCD1iObSgREfQ/
PtJVV5UEygvMn9NLJLA6S1xQDDi60J2NHjEvTLa6AAFEo5juWLJKiOHnOmEmvXqQ0ji3qc3K4ebc
aT7z4+rcyB069MAQI9Wl4GjpaAvarOgTkn0QmYx3t1i0UwGkhAgYnSPckIszLQPAFBKnx5STTr8M
i7ZsPLKhaMZ0tGMExRrlpdccR6WWOjvmiIjcH3M3OwLAOten7LzOLmwXHRZ0bKBtFNsfLKpwngS6
a/GTyhfnxvmmnP0GKMDnEWnajBQ5qe8lNtsOuKWYTSnIqhs5tqB3t1upo0VXZ9PrE3DyJ6DS3NSW
2YFJ8+z2avKesUHFgtxvxJpeIQ7ZPINLWuLdzgqU42v7NWypnGOhvHtoMc9CJTXyBts0Lx/xkQzH
jwM4kZtm7wxMlGML2OXhjQeNHordQeoy9gzvebjslrt3IlmuxFEdbdo/UkU6nmDrqIYeRHfibSkS
qSq+wg5ZtatFXz2cJX1DYuNMcT67CVQtyIQA+9VcKWJxJRXuHovlPl/lfHwZ/at7SKC2GzVPHVmr
mfWEUHOKla8HInN79NoodKnRAXN+RXIvM8Ih8wkhr5wmttt82pYOCy8sel8ks6vaHmSeC066zMvT
qx96pkln3HMTaeMhqbqM3bRqhOQuLO063d99i9vQZXJXFMbI4PgW7FfWHeZNrr7TJsdwyj56/7g2
v6t4Q5cqHU2Z79grpo8urCcavDdBP5uNDhq9W4SJ7PaVT5PPXYG8RCDgax84VBzxZVX6nAnu7A9x
CvnYDhrgYalzNFhvQlnuvtiYeooe6PWuU8dvQontsbG+sBB1c3BHi/WCORrKBoH1Zi7GvW0FWYNy
8YPjLPSIXcdZoPFtpQH7gPDqDZL14FdWVB7xl7R7cNZuXm5wJ7XnmCrdXsTTj9VLRR93oK8nNNOh
gHqPlMdy4GuMik//qibs87UlNw5BnGz6SkMOZuT6EigBw5z3a4MKMUA7Tq9CmsEfUvRK9J7wPdJm
bPm5ZtndlL38XwsaPlXy6UL/Xew4wyqIaxsx1xE33m54GZiHBqUHVp1v0BJ8TecBW0eSlFeXekSk
2rMhCxkXzqXFbtimhCtSwb63qg7ens2TCCC8UZ3yltMrvV16miY6r8/Hmis6yIHXS8Hez4t/fTUw
jTd/gCdwldVuFjxlFUGrOxQMEdB6bMXVor6+zmV0n+mIyRQsMXHY2udsMWnY9IGZJJxTHGCIEu0n
R0m3cX4ZXiGobVEYCc/crr0jzxnjdlsLCVHafBZdHdxszhaonJKLpYGHYe8QdPBuocZ3q5jJwpJE
xvTAEaMb6I1EIIvjBoNyAdu4qtmEv+Ny2acFhAASzHZs+s10KXU3XIMTwr6BRB6hLiZyAU9TLWJP
81RiTDq/fHuTEvx2ys/Go47BvoWFik8xXOtuT7qtaNRqwztvQo96MQKy3ll6SGpg1sBzQapvpszB
shgxgfMV6ubneZOQODoc4SNTQCly0A/xH+LDx17or4bxfG1A9iBRJOrEd1KxUfF/PvgRhrP6bszV
iNwNFSwkfcLmv6e0m5ASZ8AKVSmcRDVYrcyNf62tZ89TQ/lM0x5hSHGBd967CwUt8tx5adtk+r+h
IqMqigBGVSUwVjjrj/L/4cYfcIStmhPrm/BQRueYOTzBk7QMXOT0vtlLJrAZxvdy9oMr+ztU9ByJ
ZZvJoeb7DxDFp+oGTewZwfLm3Tr7Q1C9+Y0BvdubZ9KUaWgLDhCdZyrljuMykU/184d6/ZP5WCPH
N7EDSKTGA5nrZSBs6q8tTKI9HXCz4WM9leE8x9PCkP9IqH80VN1OtKTVjNE86pyPISw49IMX3pmu
b7VXgvlPmvItYmfbw35fd8sBRJ/tgzq1lApuIctyQpsS03Ci9oQgRo+YQ4kndlhxsSfQcNtBanVB
p6rv7IBLx8jCb4WlDw3o5TUbUYMLfbZQcSn0Qpcrz9Xf+b+0AWWUJtulFtI8L+5Mq0wIgjZT9AJS
0c/jvBWcYdz47sR0KoFwpYFneM2nHipBja+Pz3xaIgltopq1Lcav+HekACHSVkMtwUaHtVTCop1x
qRCJlJiLBoVD2gqL1sH60o57UkX40vD+Ng4s28Rx+q48w3ONf8bvRvx8Lrey66uI85unj5ZiSG5X
YogZBK6Wcd6B8OZ3SzDKMpvh25/fPUeiqeL7tEeR129zROaFtVY5AehhtrtTXQjbAuzQQ+5JI9m2
CuOnvVxxaeK0Ad60E8/mGqzyj8MPm8Y8V9m6TKH4vLt3o3gcm1IRPqbsOUzrVW/OFN4AuUYNkdAa
c3J62OEx10Chk+nMBMjqCzDlKXExsH6Zh2sqjmWkbpuebshyIJsKYUR5fyYXbd2PBwu3rTMfhr02
5DHmQssiDLkK02Lhv2z8E+85XyFLBxlfOqXvgmEFfu83FZFfRg0xgpayYkn8BN8WNTNDPggOx8ue
upC8sckLETyjQOArrc3a7zXyEYJWFtmKBciXorT9XeOVlvixTvukdvBugPs3yuD/Bw7peUoAFbDn
+QBWkFrvAgaTB/sQhvLqLFkR3cHp+DJrVHbCMlmlOS+XBUx3lCxKcM8ZsfQbcKs3FAP6X9RnNSq7
SbPUM5ssB0fUgJ/HdFoceNVWhQmy4kaqxYOkGCxGH1jMJzJPvJ/H1P7l1abtKhx8NHnQ+tQq5jtj
W/tnaZGVxxXVzT65FKFpsTMsoT7XOyhbFJTQYuAdkA3bX+B57KQAfJ43sz5Cwwlvp3u6xtehjK40
JBWRWQ+hrxc7DI9JPuOz6ANc3MbSXOLi9Ddl6S5UwxrmDYehMbXHo0dbInsq5Ddf1VRSvwc0Umo/
dfQ7/se1Do4JLYjjZXlaCwmbAwBwJC+Do1qi76HHQOmTuEpXwk+wsB8hR1geFadICZNUU6QEcFnC
03ohL+I/yBtkOlCWgC8TEXGTmKeKCYxbywZYT5ZX5Rq/h8vGFsGaLHmu1BL+KFiRIp8NCcLX8h1+
DypqPvi3H4hKG+MpvaXaDRRye+XFl4Ntoou/cGB/pcBKR6lUFJVDRn6h7MamheaEuAgc9g0YPHEO
5jmhJQLm+bT4wy6D9F4m9CkQTUSxH17JKdO3ppVNlBSfJ4Ivs9Rsx9ZKQUMzYkLLHdTP9YyfrZSy
zSVK2fbTXE+IXIhqNQqqxsHcOWjX+/oth5xek1eHCDutfeJcKKsbAzVSdHNpKKWz14xyft0V9mD+
kop78nt4ur61Kr74K75dDnvA0eL9wiPVh0BKoDPozsH1pAZtEzgQFNFplQI/zbbtuV4NdOorEjwO
7MTHIzCsqbvwqRbJ4tyKTfdqNhaGR9+cFAtkxJUE6HDoCNiHrfjwncfxTaHTyeRlxe6QvqeI+CLu
Sn4OzCNnjXgUN0SXIr2b9m8XVgUAlyid9p/8YEKurgAXnyEwC2lSopwJs6MU36v/UuBLt1MUFX5O
bc5CkXgOjX3xYk5969m3MgRQPVBF+fdmrJshDogN1l1DVYT83/zXllucx2648uX5+GidiiuYm1hW
q6JHVRE9pAgwFiLSsv90XpmUtDdtUBe82gKnevdklgp3zeK2ZdevV/vbB66JZNsI+KeS9oSzisru
HlMUGIT3ZT2DM5aA0x0aIZaz0facraQBbmW2PpXuO8lMckHc0/zVzIBhIW1L6TGmg7k0Ru2KrLX4
1F8OWEdKeP7Svc5LpBmDNy7UZ2SZur2IVahgZxRVi/p3eDaeysYhO8n+DFm8XsiCKO4X7G3Fn7UO
gtPR7L7V+IEZ/Eoet28RZBneVyAmaOs6u+v3jLUvAcLuZ4FsBBrTu8trmxjXgVluxEys81rR/8X2
SNEkHgsqHaSpVbc+p+fm0TGh9H7jh1srTN1d9YD0k04HvB/rm2k5Mzc1sj1KPPaq0RvfOmEuLsMp
GH4A5LSuzobyUbtNjbPZVa3oy36H91qUT+SrhVHhX35n9MyP+x1yUFm9iN3LQmkA77wzH0Ng6zIW
xBmRa7/YMQKPyauXq01or3Av+iw5TfLV18xJS5jeC1KvxhSh7aQZ6dzbQl7mjRSwAkr6+8DaddJ5
FDyDqBJOqosbwZtrsztpt8lN5vkFLiC/siq7Y2BNabnSnX98TDC7uU52i7S6JV6cBSMdJ0fI1Xfo
ZjdrxgifGrkvscKFCVB4cUqaLkCXEkofW9GexA7evKeiBS26+QAZacBIxD/qQzSFea9bwCC7cRqF
gS+CQdICVrYkawEj4OIumEnHocC4SRbtYcGEvHb/dObSS+NH8+1xkxLqTtupm5XiycyxJsvcP/RX
wpYG/djHsc3nDJ42IkZsYZXAftNCQoBExa1ttsPyMFRpdDoqi9QxLu6C13arlOqjjunHvAfBqqG8
qoMeB5d3Ag6kzR8kT56AepCuRvWLbRwvPso905glZl924wWAewRVEm5LBpDi2RaMYjCWHcG28j5A
JOqk7L/2A5c5tPyW/INQz+yglKLPaz1JMIpXwoJMptyBOE8uQmHi0wDHdNKhMPewhxCTlOpK4Bmg
huKDS9qVDfZBFuI7egcj3N+jEHMNGG+Qv+RgJ32lQRSjSHvVSBuMV7oP0a+5kNXf+nWIx8o6xR3X
SdxFoRFvn7+la3hPRr6B2C7dcr2h6HtewAoarYl+8DyqjYIw0uB3/W3KEXVRYx0AL/RkbBbaW1jG
JuTkp4XMdVHGw03ihh7+nBCgtwHJDBxPkLVNEaiwWd+SmulkKTmC75W5L+BQRdRlvqift6zKltz4
ElNYegXqhu3zdLmD19zdi/1lCiaRiNBJeSUobfRH0Z2SXmZAMwMfXhkX2BpK/x4g/sR3fCdr981m
oZ5dhnP/FgvUx13785y+wv4bpz5UgfAKyWXnbVgrto2oL/Gacxw07CBidRuMo7tDgbuFP7Q+7nTL
OxV4twxgLTnBnh4Fo/F4QWJLUfiqvcJznJrxGVwdqMxZK8P8xbBy4oLl/qHYCl0YhSGCtMIXONzq
ulGh4lxD0VSMtiYHG3P6tDdavAPxqCBUgtfX+SrLoVoHVSaIm2Zn+FQ/wJAMWJOXvRH6a4iqKx9k
q5NB8dL00831MGb6wY3bP/dC7rljFn0OnsruOIwkYLITUNXdkpT10AJBmh04EjL6Q8KakFvoosjq
26/nR5w3yikEAue//xu0xbBe5hgu+AYJ9Ng/12s/ZbD/Rtj0GvXWB35EvIfYVbHifrTcjsAF65cU
5BzEfc8ihpLomJE9oqhk4yLJz9C+2UhmwF82vjyygqwMW7gOpygBvCDIf7Sh29KjEBDFziAzOd4A
BxP7hhOjV70cqJ/tT44uXj9havfmWwUr1T/ChkyQSz7VILVEMM/jo6BEH3VJol21A80e55XfWwBZ
p3bKnZymnPVIglCyohsE1emNzylyzeJ/VqjKngI0+rkYbyj0XCj4Cyd6HjHv0YBClCMlO7cRQ6Q+
MlUl3WekmNrvhB1KAzVvGbVqUmeif3qhBvXDljpoYgtOHeJFl58hGq6tBSLoVfXEpLteIevpTI7Q
EXDaFUKdi/UU2i66u9pUjUVamWeRqOxcvB9yg4IKOUVd+PqcGOlQTpIdv6nZt/GjQsA8dVYfg+9f
kn4V++eigvx1ed0JsgBxY2T9vruXN95xdpvQL+h+YKJ/fQppAikrNfYCwwjZEtf+/eXX0r0LdiSh
jnOkZDriXh924r0EqwTaIXp4t67pA3i4eaTf/uqj8SAg0WKlYQgMUy4O+rehOROgT8Jg+9A95iuF
pfAp+2hNsd5V7DpJD/xzHVHYIfbK5k2O9UGyX1I2IIzlBkvKxCuXUofdg8+9sfKyrPScUk5qBbI4
xO3cGNe3A1UdhZn3GCcC+DV9eAbV1h8ka14fs46JrdFShlO3JE9OwV86wYiYPOScfOgKiwHIzNAV
KoNS6j5uiM7E2RvxIi/n68/lINMpBoE6KCwkp2n2V6v2yUuwyel2nv7flJk+trC7ABJGzNkSmrZZ
HRZIoSoGNZ1KadEoXrioT6wAnPB6KA4S0YXpqXlGjeVw9UMqBs4CFHCWXzHJVPr32QAYZ5u5+IdT
DszEA5zTA7mstkZiWiZT0dm3+bh4viPEG3KlkBvohmzGsQ0/J8i50JJCHIsr5kW7GK+ejRbIsEgI
U7JaAQkSakxor0fk9lh+FENWbHzqnTC0hmrx94dzWQNLLuXJP3g5I2HmTKgQcEB0IUa0zkL5vBVU
Y1G7xMFu7+pBlpEc3cZ3bDtEU5+Q6+jaOSSie535g+anmLHchRYFEtnvNxn4DlTtGISgFNEgzNsE
G4/mJ4uQ04hSGWB3agR22VYDkA8HFkDEgNZglFa9iQd9ieagTB49CJZAzITEqj4TQRYWLSnBA2n4
NVazhqY3EMJ9xyAUM+RoRVpGPmWwBob7xNjYBifZpzklJP43fZyWpmUTfxJ8sz8JtB+22n45wVp3
1tOjxznDws9KYUjfLCMw0axwSZj/osu7TfMA8hJrY4b5WVo+IxdaX9OaaerH30BvrMYQ4i3GY0Ff
zGR5c9u++37MTMlVsk7WtX/UVmVSAa/n5mcijcRHgsnPP0hEnT3Z16xiq94LmBRc2522+PS02jnE
erYnCHNFtl9xhBraszFe+vzgK0ivsHn+ayO/4Sc68Z7OZR5YitM0RF87DpauraqJ+r4YUAYiMoq9
ZQ4qRzYpxLr5Cxb5s8ocuty3iFvHTLNJ7nnZYlMuoE8ZUgT7S3z1BQkueS5Eo1tNmUzZPKBw9Pjb
UKR5bt+1/P/mOFIXYG/c1eGJVvJdd/y5ax8DohhPEEMA2aFYbPbqtXsgzNDBAKVdPRVQyBBJxtEe
T61X7RISopobNx04XgTx5pbLgLQirxlE+TdenN8Xjd9iWrchuVmKQe6WHBatndK8/JK0CECp0q2e
7SWjz1ZKSA1D0X7mNwEc653oV6u8Q5szpDmhIBDTwsbmqzpoUSE10Aq+NA9c6NKwds9dJos+e8rw
QUmxvT0IjZc6cDzBHowNzhX+CcKgsevZPViNC5hTAEONMo/DzW05rHJr4WqxNaRO5R5fZq0BVBrL
z76D/WnfdmKPw4u7V358u18PfQf9lPbT6piYpgrfFJSHcdfXn3bfncf3B+eqNJgwrrP2KEY0l73m
tGMC4L2st6pdc4vs1X0xDkBr9qpbcVRzMSMyNpoDI+g2vRZ86wrXukLEQR4UwEtpSoprGAIoPbRJ
wikYRoY246I25OyDrpHmTX3D7KP+aMeVLm2DGiKDRm87RzCU/AY1znUC2/NvntAtqNWSlfj4z8UB
zJc8Jn3jc9a5kPll/0w2nUdtj9cuRpWitH7HQIXwNQaye8TRzwd8aYTN/gUbieclmMefiNJdnR16
/mALpGoX5hNaualZOO6N43KQuCQ1W4nHYj+gf8SqLNnRxrRUsgjLYL7B28IqJD/2/CKbUqI8NEVJ
XYGGlV5DBwoY1h3nJ34w3N5wZCoZV2SyTn3uBsz74w1toaPv4QmJVGmgHsefOQUHvYxdKk26DNGt
tvLRZxq2vl08QuyxHbBN9NmbK//h43/JqLgwpnbjxhEEE1JVGcaKGJ8lIY5/6sFx+Mkq4kuyF9Cn
HKue23ju913C0LvoltgxrZtdlQd2K2xmBbgWii+4TSyXR9ML8CCEaxBpyBTrP+Lxp+9kmNrNnyKV
dDwpxHz+d3ZWrxBtDrGctwHcDGXzGy9lCS8C373hBkNZtMU3DEuP+htE1q7x5yA5l2+siKqRUnPv
u7uguPVCGSIiNc8IKC4iXpvyhfRzJlLuLQuXI9E7rfC93Wl1vSJi3wlZoHMxqzmUfutr3MTVAzXS
dD9akkKY0gydFyESjLDuHCTWIXwZWrnxzh96OkzEnG2yOnNrKKF1WN2e3z8+UIwB7auDht3JNJoW
4iHVIPwPQAoVjxtEb/jjoLxg4/78o6ZkOpZfsJCiEPBr2pf0+5NBvZR/oFD8nBJg3WMqI7nyZvSW
/qHMy01JAsXN0NV8cgK4aA0DDxWwGhMbAK7aCAVVrqImQSpJag9LO87aHVRu0xE4Qeju9lkP33Xq
NGZAAwalgsi/+4nfOTU2rGkIF7GbSnaq3vlkS4wQfg2LQDlHKwoJz/TYAdT5NKhrgyFvna+/81tm
CoDZu1SfAVb/lwYhX2LRIFFHtoG4bosxLZQ056n6fIJH5VkAG4aJYk6H011/JAe4XkiX//7qSbrX
QOG7nc/YflAof6T1hK6P203mGg3VUgRnxFqf113zFQtuf5kYfqf+ToP9YixnEXZMtPXfbSC8yZYA
vWrQjWc27f1RDiJROxrX5RjRTVlLbroaBEfrJh44A22HMg5fbvPfneyXGMYp6ViKXwCW+F8v6brJ
/qzSoAu+uqWxZm5/Jhqh6XvQ3NFWqXJB+xdDM5IBnPzBDQuopmDaPoo7Jurn6XW0zhCQrvWPZXX5
Toz9G6/OmptGNQrY+TRVegHusSNHJBZJfFwBtL8Y10oe5N6z0K15cyT3R5fIAG9zUIAfVOK6dK5l
K3P2oNRJYQVGEPFefl1iwqrhIK1hcxAZPh5bv3lnBFSB55FKGchKfs6yZZkEfaoHej4PAeuwE/16
+f+z7jL1n8bmRv0KOuewKC6Hmy+qHG2xqEHkYMgal3/ASSbfFwZ5ahH2bQHVo5dl+SgmFC9udHnt
ft2TpNzMqg2ak8MXeH75CiksLxLfNlklVR/JdhnQqux4EZz/ibfVj6EGMIG/1Bg2v41Tp2JJcUlA
HfM5z/y1D6kjsjluLtcWnzClAzMbIn8DISrc3Z5aGP2vFoTlwBoDiCyFLNVIa+RikB4643vRLeAD
qvA5kWi2OOl8EGnZbxv3zjdf8RuYAVKTWJr1ihJW1Bf14dQCXUTJeaSXinVNKz2Afct6DZqxHOhB
Hn05bC5IJ1yX+3wlovdbIgwU/TzCc9pTvcSOqdz/D0UfMHsBLPm1guPSU/i0d9dFztj+q0c2X+UC
LCIDegRaK5ROBarHyaYRifB7vfHfPsc5JuJGZbmEITj1SNrLNj6KNdGQ3+qm76suAWaERGaJLc0T
XUDd9LmHPDJQgmL1fy2sco8HAi3G+h8z1TbqDmXWJB2Geovbr+zKAQOt79yUoi1pUApgKPJWLAkS
SnN63oJPfjLZT5/d3vd2j9dfT+jg63ek1CgDe3pl8v8RntocPMiahmyZkOD70b7q8pW1T+bdfw0C
6iaBY+hr5GzLvm6cgXETkP07Eb36Bh0vGSO+1uZwW2cJXbXPm/r5w8TOylDiygUnG31Uu3Ks3eCM
5R27XnematLgNI44yOfY7/COnfmpkYVgOD9Cp+FZX7U/gNSigKdM6mtRZPjnIVxv/hpytG+ICJC2
oUim7TZ7HCHVKH3dsYNAdT4N4bSwAzNqtV/eMFZ/+RYrwaaSU5ly2xp/Y0eRsZTUKU9AdGm4igNa
iQWCdT/bg0ArryqLDg8Cz3gn+8EHZiEcDr3D1rbwmRICEFC2Z7upO2mu69vs/bBfPY9H/HrjW3Ie
WBvYvY11sJzyyAvFGllF9dthytO22d5GxJsde58fIEcsboL4N4TNnpygXgbLp1uui9ykIPORDRe/
Wt+xTs6vOjonuoomsIDo2QfZlK/O38z5H0A6NboHMP4rLCZuX651g0VrulbRtfjmBOGV5LmwzxiT
DQoSuMD+Hc+hXM6dhhUpre820diOmjIKsGTvMVQ3QCLEZQuTy7TEult1OS9ZMmgngcdZpOPdcQIs
gFxHD/+GndnhxczC+zs8hEyQf4xXi6Na7/fTzCpe7HaAJIlXaYH/99ozOof6U2qh1LmLxQihayIr
9qCO1Ra7tax+NlNU7onq7oh6NeOL4e42YHo6CAjY8EAqucAlyvl7qwGEdyclISb3/VG4B+plpb45
o+Mnu2rgo1zs+fpEkUILMyFDdWhTIqc1p8FbWSJRZ6nUKoR9QI+myEjQgusVdF5FBEOqeryHs+wf
52XxbXj50gaw0MU4kfwZZa38DkIQKOPIU8qIY+JL6uP0Icfh9XbNxz6XjDp4dkIrivc4JhtK89um
NCZ3CFHH7iLjN6Izw7OdZMzwBNpLsDTlmA4wJ9ozxPD6lGrdunXAxLy5dKd1oHGrjM7/h5TMOSl/
PXR1RtDEZzT62VpifflXPvIqL6vzn2TvATJT6xFwVVSn+6QhN+WMGXqdI50AKmMzNymxcmh3W4fp
Az2k/HfQRcoB1yRGDY/5q0lO5OBNoFb1pupkIB+4LtSI6k48LXhYjIJki4fBAL9lMgyI88a6f2Ii
NiIZl9ZsBFuJM7+qWA4m2qnHTJBNAyhk05onLi80oTpkU4XeZbjs8KsIEYD87pIcvoD0XjS3Vetm
LIcb0aWnHVYPnE20YxxAkMVTx4Iw1wF5o2N9cutrUKD8pSRFcqfjRXsI4fC8qGsaiaNUbexSu6FU
Eu83eeDR+4yFklHG30FOSTpeVr2+YFvsrlE0prurV4WvphQCI0zUsvYmWECDI+V2CqoK3p9fglfA
6GNMlAhqeoZKQHOSmSnGfA4fxA4Qw/JcnLEDmdZRZCLtIGBbdqO7KEl57JwKCVB8CevsknEcTJzK
7U0+8iu9y7C+BxS+p16/Zzzx9gu0dYDREOGUKa8ILvHCbD48zcWIAGSmPBk+nWAMJgZ5d/C9vuKb
nEtYWKInvCPcLQa2y2aDtFs9O21P+2YU3xyeio2aDPJWp2lzbZZOa14DB5+sFHIdAIlajVLwCGZj
9pDLJ7md/81eOqqtdIg4fXP1U+PHil6p7MF7rB/AptmgIzCqW1Fg9xbcTA9DM/69TbTuXMoRHIS/
MF692HsRhG62kiPP9a9heMwga+SXubB2hi2Xsr+bfR3Km+cZkb598s33UB+bUbcA9Ukbrkec+SbQ
RKgZ6L4Cm+oDvwh/gf+0rMPdP+zzS6ZuHkqiaCxXAlfZQW52M2GjjvE4DjFK2hV1PX0jCoGTvqdc
zes3yosETkZQdTHzMhCp/QQ/ZHBCQ7ZeaoCCIyGztLkCLKTXbO3Vu3VoScQgvOSxr5EyLN5WGMpv
YyD48KAJZnRipqUXCrg9uSxbwlg+YHA295Ok0f2eybMkiEnD+yfFcfClnn4QsVHt6+h5KrupZVt4
XVbaD5Oo50F5LSxwxsWnDnMLU21Q+baWG7BffxPVRbGuSnUpNE/7TwUq5kdV0t7oSD5zqlcZXk9e
AljOOvg4x148KJHh7dz5T7Mhp0og1QxNhchZ0MwOGCJUjpoW+I1m5nOcmL+S1tZxGnCLD8gyXKsg
1qtMmBakBzCEsCaHykAW17YmrGYGFGAM3S4BBNCZxxHiOEcPM0Pobms1ctcdcCXj3YEcpbGpnTEL
ZYp68VGfKVB1dMBIDqDebW62PXNHeHZO9N+rHNQRe/p2Ey+SsC7DNTyAwLNzBA0TgzIx85L5lNt3
sExpEES364R4IZ0nycxcDFBnXITk55+mgmhXWo0kvjaMyZ7QdkmS1qtGFzNCWvKzo++WxwOs+AYq
H9RCuPsfh+FmPaYtjJESaixho2EsWoyWGHsvQBw++qzVYr0L5a9g0b30vy5OkFY/dOEouNqx23Me
UR3gtnb9hrFYzCLg5M7S5qjK9Hg1tQt33xVpAhoh2ZeRdsg6rVtViNAsP6Ut/IjwjUx7RusOyiTY
ZDKMtIS3UseTmr161NxAmhCfE45v8v2WqrxDFaY+iserk07oUd+M7O8uqQrbmhoOgu7s3KPlBbqA
kmD7DuG5bfu4gJsZJfrPCf5C/5eEwav/6jhUGOXboGraJ0cTr/i4xiqGi1XaRr3TxRA6Vpyl6irT
I1kwSsF58ZSW/E3sG4pGVSWxMZpMugz+i8hlIVSrOGlEtkhN4eIJUfGsW00TImjuV2yJfRzoHnbf
koCGP9C9RcAsXBDeeRRF4uc0B1DFX7tiMbkQVcLRObV4NTToFQ98bgJSwLXesksUIAOwlfaYTry0
UrexlZDVK+ySm6u3/Z/vSPk7uITYjVNs6nsFDX638hpBY4ehyoGskHicxnnpMxws5znBwU0kn3it
15MzQJoBqAyIM90/TCGVTFFaySNHsRxFTdzteqxwKJMUnHChM0/FkSnfaOR+I/sE8tCXvFiCtLal
4BVP8FV1Jpkl8Ks0AnZxL0zr5T5+6WEloPG1Iq892hl/Xg/TkQYEmoMlyjXyqOz8H1sUI9ZmVowR
Mjgm7mPK+qOua9uChNnIfF5Q5Qw43xb6b1i/HEKntdvXefx9r8yT0QET2jDIN2W3WTCJMxpynbWp
bi8wR/MXl3alCuDjEstQh8dtkatH1XWkovLi3kRy3GYHVuJGi+Nan/wYiD50HnzC8zIcDISgT5/R
nJeguua0iWACqz1pEZ2IIBTp7rIIm/Y5MWxJQe8KL7RW4A7tMONldlxc9QG/3VvHnO9hBqNxZRjo
cRxbGUdh7wc6znW+ogW5v/MfGTip7WWeqyu0oAenULq68ZFoqJKPFG/V7bAAEupgIU9X0fPnByEO
rnO4T35Y+zAShI2RJL724boSjgVIwuu6BEI07rauYHqSdVo2/n4CbmnXQgYpjGVg4MWF5oq3/Rgb
Ez597Ux+zNRTdr5U6gSXJZ3ix6u3p0ZDLtqNmUwXJ/a2lrWqswBJyJ5xXJvhr9bx1Ep8V5vT9+c0
2wPYf/ycQWMcnTrIQkMSrjy0CK4hTYDmf7WWueehpGH8qeTActflkxVQ2ypVbsJ0Pjw0Di9XhnDK
+5BA8LUEvHuCBOPVbn2abr5x1HfK2ZMh/6+0B808dR/hqglQ3MluylOlRfswgO3sSgUTEF5ywPF9
kDiEF6Kl7VZhqvQ2B6Yov5KIx8Ev8HcL2XHfP4jhMCU1WZ3oGSvafG8/Eb5ClJ1UTf2MSzx3mWIv
DLMq105Ak14EasdsZip/H/saKT9H2NIfaJp3BU1p+0E1N8vchQ9QeX2HUvtYUHZcMMA+dTyN1qC4
+Y8V6127ZYbbIgd2GWlOIxzOUJsAEI+pScUOPnAWkqIE2abIfUI4npygnucMtRD2Bh5A2+aw9thn
QhFHEut7o219/na7UPQQWeb8FEsLcojQOa37T3hK7Hkl3/UZIW7mxe9E2ItgQAinBmzxYUVgbAiv
I3zTsdMJj9As46yzdFGDR+egZ5zH7ds33iolAYLtAGATb1y3fgiQe8RKH5qYM1oyzsz5emHi+LZp
HNlziIvRkt7Lvnqm5cRlasLcyEA0k91XnnxnHuNG2pUm962AZbeZ3hrugb2clromEBMcr7/296Uf
pcCAugXIllsXnWQHPGKAGR5eoy4+Q7KaojMaUn5WNCxnx1W74aaiJ5qzCQQpI+C/C3D9S5p9ryJB
8UuuaCJ0SEHMt9l3cuZhF71zi0qonai0fm9BZ0OFF2Dmm83dRvw4WDxQV/SHezSSMSYWady6oXUp
lvYfroGUCDmdz5DpjxFiKLaZ6SPXuf58LDbxohWO9zHpfRDt4CSRrtXN0RUgZeNLuCwmuZuwdcxh
GmVfkw+Pge8byZ5GcTvBhTdKU50wGM4HFJIebcP2akAu+cugAR5LRcu9f9uKLtp1Kpfe76K6i+yZ
sPaNmMypRnqd19BwRuvPei8v2/GK0P95YScnenTRKYPicmpJeqo6X+WRxQ8yDINfQSUqqfwKo8zk
I5n9oOzWsWO4uIKPUe12dSRE/f/pB2jI3EwDNHzy2p0U065dUkHQVc2x2hRHlcGwUIbuMQpt6oeD
b2mKDuokLq5Lrw0ALS67BTRv2HB7jtP49yqcli2meFx9hhdn5IFwSJ+LwU71arJM+LwH3ZLtR+Ht
C+IpD5oCRoCiRhwBGjCqwTJBF0gJvjhB54muyBiPG8IUGcLR2tlzVDgpVnWG+cGdX3yowSCOviey
pNjV+fqJ24tsXJ8hEUQs3qV2WUXEBEP3ESzw7NsHsF7ETR0K4VcNBCioSsv7qFdPXjBX283Z2jN4
KTPuzW3jqat8UToJyBNfG3QruecsVLlzbt0Cr+klxzygMvn9DiFv92961mmSsHgQP9bA4CBVS0uN
kwqHOvDvLgyYIocViC+BoMdj9IE1/lLvBbczuhfUTucsAWkGZWz7+2pmDNNNTZOG2x3vwyDQzeFo
zWohZeo61L1tWdHcRcQ5orqLEwyJSb5HRiILZwUZC5y+vjbCfdbwunUGz0NnFbCZIZni5R0zDjEa
HW6+nMUHCHlPI+iRFgAi75JEfn+hE0JjmeP9a76bK4Xdv2/1nOLKJ6IEGuPmaXyIwYD6S8knipD6
aKiSpN+5nIED9le/aOqPiCd/Nu6EEUgt0FC73ZypAHUIfLycxtSx5nU2JspxI3duSSzl9nrZ5lki
rutEhrHebmPi968E4o0IV4dN/6SAcWVcQ/8mO7FlfOwQMzqi52Ce4mzNzWwfTnOqoDhtvJGlMOxQ
WKDe+eI5UkJkGaeCdlvnvVa6eKyLzCbCZhCq+Alqpp95rRaBhpPrr6yljR3MwF8qMyE4q5dU282e
7dYc1xi/WLEmH0KLID72pXGpmHSdAWdzGFRUdOYYsEeY0QeqeCBz1Vyg/5FHkhWDTSc4PzijCVs6
cG1usZaZUwD5EujPw4tUuQEmUhAqwEMmptmY+ssf8wu+Y1YDcipDjIr6Qe8Tb7RaQEmyFXHk7C2m
oKZkAD6kCxWChocXs0RXk1DRQ6auUR7Yzec0x/F5+Q2xSxx3yfqVOUgrZCEhJH4sIfUSCkxUmnhl
Q00iYhsAxWZ+kCmomv0zJ7To8fNCkyzBL+plLpOl/u01psfpmG2x1iooeA9obFLqvCsKf5SdGPmM
3fIPJIx4AckMIzVYdOuzpm6lqW/l3IxpiLDiUwvaXlmcQOUzOwovubEocWIbKEYbaoSV/VJdy0Zr
7HusD3FmEJqmKAldDKhJVHT5A2Xa4ETQrvl9H9JHG2HoqB9BeYdJ4Uy1oj4sLVm/WQ5SUlErdY31
GfXoLA8c0th0ymmOp1ClAC70NjQgWerCkdeltSddq1b3eOj+1eMS/hm8rpKYHleP4rZHZ3j5xnjA
suHzDl6zwFRdZa1xdsofhMJ0tECDEGphUYS1ACtrVZbSnjrJtq3Z/Z7hezUYJE0vHQyF1RGyeHi0
VRH6k1pUs0u+F98VwK67jdxzvY9Afm2vZLXclYN7a86xw7Kb2EZ7KssZAZ4scd3CChx41Mpi6KKs
R4UtYIDpGx9WpmHZ5fFdG35pn89jk3pMDuUG7Y4FitK3yqKYyQ+NNPO8GaFUHJ9g8TBbGRLqeJWC
QZIFOBvV/jDkIkLEhGC7+uJy03cuQXJpojMYCZJJzGMjyefUP72mIDAMLl/QSF+YSTfopDC4Elfw
zN4I5S7cMIGAKj51BlbHjihCM4nqbOyoouTmYyqsdiiMAxwDylpETg0bHp7fstpRxvCuIOuMHOM6
fq7O+zu37oxftARI/ssJ5rg/kCc8rWDUVxQG+UwDeQrsxbSX1jWw1l88CD6RXWAHCjuOBcgXxKQf
GZBY2nRUkBH0zjhxAI0ZYtixGiAFH72quEcRn9lWyQhQdKujcF2DOcrqSO8yEJGGVNg3tldVUfGR
wIAB3nXTCcQu1zEG2uYUl0JFv+VR2KiYUGQIeOAK//sKQLr+reN3xpvy+7xhCmsksq7WTnP6ZZpW
nOrvkC/1Z8p/ovpNGmnXqfBZQesoribX/TeiUMhUgDykt5tq4gYloF3RauLSqP2YV/Jzq6wGrfzG
HTPHaa5sOJ0B0p2OS3xpJWLQOT/QPd4x5DH2GB9kBDwhH/6nzoH5ezlUBCOw4JCz7z6/lsUYuefI
UAIgA/NcqcwAmzUlw10x2jx/qfgsoEINZTetVMI0bjGpk0UItBGDew+E1TCdUGbc44OfE4sgyABT
vND1aqLOniJl3DiPrf/q8NX7p8go4fJDQiRwpiXVmIMjxEX5P7AMcvKduvKOzxSvxgawsLObxf5y
BhlKbExXSzSiq5bLBjXzp7tDH48X3s1SYSHeyWYnuM9lr8pzMX4qcr/AcenkVxQ8eB7gmpqc/CqS
TwQgKSE80QYPLLuLAmi3ug9jyHiehZ9njRiCVVMFz8LnIRP/AsTY8p9Y6n73tmfEtGZNAtg9Zzsj
kQOj6gME0MwzyMGiSqrXW0FZ4XrYuBckH1QDpaUoPn7J+GlOKsow45Xaxzpv684AqsqK42cTs4nU
dIRs5hGXhJHINsBuojA8bzMXAMk151hDg0zu/Cp8ufuHsYyFgpW7cS78yEqGM/qo7l4QD6HXqkeV
lOaGsBhU3jgGxLB7hxj3YAPf8mT9WsYsDlkKybUJTn7ZsOzBkq+9szfecgUfdP0dBO4NxQmZPyHV
G9PV/N0CPHQhlovyyDKT0JwlI8hTW45Hctkhbb3AGLh5z4gaPgUs32FXznhrBltRRJY5lYkFtXdQ
+u+qs4TIAzh3ptJAN7DE/ZNP9VTGTXtwV6smdPJyOn3CRR7O90uotkw0ADyuutpwHh4KS9v7yqlu
GbNPXlutGy0MBP/8hZVsoDfzuPU4qxf+DYnlM+wmfZp40qazfDXz5pOashiKWxPmIcO0XBj9IXUr
OoT5VG5JdL5br0JznNJbCymGayeW08CShPFLH3i/82NDJYcv/5VzxrNruMCoWRwF09l+roMJCiGu
KlSPC1sjkE2TQ2EpKg23G6OWmcr646tFOE6a0MTIvQc1LX7JXXKJ/8+6NBhSUCBRFaoeZL1ToPJA
hqLF2N1ZZC76jblSSgWQIPOgrVVAtMjda3dU2SArJWmk84qcJMmpbVo5E5RbP8SrcNMTWKLU2mfl
bNpcAOIrq5qnGdX8+mec3kxp2nhFVDs8SufsT1R/S3H0Fs0evylTv3UDm/Om3gbyvnQWLHTZKbU9
8mgeJBRom1MmZnW6NngnlLMrN2vy3WE/H9azGpw1Gp30urlm5q05MTFRchWqhAs7Bs76abOfH23/
j2PkaDl7SOug0B/Iv5W406yXBhdAzNtH43Q99yKep8ml8dWCyZoXBhUR8pUpTkDZr9Hz5fjMvh/F
bJd7mOrPKaKfggWyjyDeIr3ZUY9jC/Kc41N1CjA5yReCw4uykc35yrcVCF+YBg5Z7Eb7mywr3w9N
H4rrRnYenOFOmai9umalWm5YA7u6ZYBv8u4df2OS9T2/j/uQIUuQJrzTr+NuClOE9XYsGHeJlOTt
nozRW7gJTWdcVunMZCdVYOizWyPWxgytQ637XD+IHMQBLpDX/oq5zniuDEc1GuUqSWQFYNX7BJpp
4wLrBda66EbQh4poxu6cwy29+Ki3he47soBIyqCq+QcoixvfwTH4UPwKQP04lICVvfVxUw9tiY4w
C0GvkvZUfmdiLSPXQF6ush9rZz8/Nl4RAPsK+GiBbp63N2WVZdp6XRozUXIrXjYrx9VIREZ/siMz
4WlxN0fLNNR8jKUR2FkGPLGjU0pLWTc/QSQKyjz49WgxjeJTKfAGfT4LBKohyF9tGPLi0X2NtW5N
9QNJuc2VIH8xYtAeVKd/aipZv7w4YO7gzsi6P1hS+ClX+gRB/kQ1y5CkezEm9OZz4rrcFPFnkD71
JHWyu/3vE/0ntCSX9lKO3naaWatNU/AYPf014sQ9Pruw8T2iepDqZPT7kXBeXRiypmwFF+/icAbM
cC0QtLhBQ8rtpk6VkHRJkQuTnZNq+U/i7q+7uHk9Sv8htHFTzY7ZkZdPxE4Jmt1o/nisY+TUyJ35
EQ882roq86aI0t2mw48xmNLtDihxvHNHOr8+nNrk7ok7VsqeA7ZuR9WCVc5ee9e+ztrGJ19BxCic
SzDlBXSD3iVzpsYRZ3/OT7PlVnbj0tncFMPQkPL4bMjuEqF+b0pRLg6ONj6LRNg/GR3jaP08oXns
M/44mkle+uf16TMBT4DzWgScqtdvwnMWMZ/H6A1MLjRFg5OAZ9m06md1s7vaAFvdWD1DT3Q5YRmc
OhSux19wxmKsr0ZnyTLRojcCGcQeIGShTesK/4X0yTyA5qAxOFE0VbIUccSOSCkp3iZ6u8u+0ggz
YpEZ0c5siZ+ccBSSGG5fAz9MPDJswaSCCi7m7z0AyNqN+EniGtYESHg3yGRoFT1NQgVLdGAk4Fch
prPiJhq3Z72mMJDHUNOXERmlKT9+VcenVez5+v0sc22z7leN3ocg45biNUTCAQSYzVsaeYY1JTqB
Zi5CggMVKvWRTaU/a2JO8V5EeCRunvWZOvwxImEHjhoVozV3j3+ED5QlNXc30HhjCks6L4SZ7Ze1
Ns4mc+/kh3yWqxyyNRR7vkyIXSTGGi7jyt/jqOy51OEtT10rxlfYnYidtX6fnmivUHwUYqlehXkj
UUSvAPh5F/norEfBdVYPFuPziNiTBAKOdH22juyg19ZUB5cEbjVDpl2XZ4y7UEPgmmKHlYZo5i5R
2xDxX4y3+S7k/xB4tacnfr0d1bN/eE3/nDDOJHrNkUr3EXBrGJKVJbkw2CyMtmo7P0cdMjbpOtNJ
UfKbE9E9VYTKadgnTA1sl5rp1bq83Sfx4wHuATkYsupA+F2Svl/D8x3vdPUPIxx/ywXA3stqnv11
jl1F4myv1J4ITa05S1lXiEdkkMsylgx48Ym30uoca9/6Enh/AY+qv+IuNmT4xWs3rA6rzp5mEmy0
l3WqNEvglA48vLY7x2nySb1pcaGRfh0A3+BFZbYZEp7iNotoOj5frsiQCo3bxYjaRt2xMw9WxdIb
nTxK/i+5hELkox0VtXaHsSZ+ooWL4LCBgXtqgubNLVAgioKbwNumigfE36nA+cBXdn2UydwbYVmY
TOuTTQ3rUwqSnELEGciTf7bJPkEp3niBoA4EXDu7ez2tCvuBxAsQtpIQIaFsZaTMV/t3/3t4NQo2
zLvPqWI6+yRUa+0SXxnNyV/Y7n5Wv3MS9+3NcdHkJTCq0IkaWSzga+yv0bWysWalXQbzK7vBfu1G
8ahYHHMHe6f5xu8qmBt9HASgGYhPmcyKMpupWJsnpMjpf8scnITZJXjL7VmVI3OHlOeJ2oo/XEIP
ovEXvP1m4iNSoAyjyYU/1mqoTjvpRYmJVTmovZxBuTsa/4HSdThU3z6VCt0g2+odmIHsHIJp4T1y
fzqqqBkVpkLSVOc/T1tr7/1ntg57NM+0mWgB781TGG067qxh2YO9J3uKhEuN2To7eqBcn7E0LLz1
W3kBJ3akeI5ehfeqIzVF/qj8ycawdQgPJddk20Dhh3WcCkr/DrUGziCvdUyUCM4eNT8HDAziyFnr
Qc1gKxG/c+/cqEISsc+ce69R8h3XUZ2Qr4qN9hn35XHkrSRE9cyKA83K0mXFX2nZeP0BZzhaR6XU
Xny1aoddzjXD9SEeFtTh3Fk/vvLLbivWPb0icmI+8a9aHFyzZYDqJTBcHiRixqKr/kQKryjvllZF
fvLrHde/NNYxTMNyN5VTS+siEAIOBWfWkZso8ausD7CRXQBZt9dEw6UxgsDXYd3aolUDSvO+JZDj
AYAolAU6AGuPqT2PxlcsCr90opWjWI2L2fKVAoYTwSqA7cWt/Qzqx8xFvPnv5OdkghT+u2joWKLv
iQyNuzVXG9pGnBBb9gWQVO7P8EBjm+kTAO1AVsTATIHdCTDYjySbC3oS/AX8GhSKQMUtsCe0/M04
Iif+v+Q83EMVnz7JiyQgoSMIOjy0yN/O7vo1YZi/ZjckjW9OCn9ei/bS46pvDsud5qpxa3rBhkhM
N9aDyEAQZNFvpa3JbJaICBSgGArhk2vbG97raceEb592zRcNVvnKFLrZRwhnm2Kz8E7CsmVJyJ/0
Ac4qHdAZKUUKgg0dCzPLTp/qU+Y+o67P1DNj0h6kVxmggD0FDGwwRjgQ9M+V6Qh2c/fGhH/I8D6u
RAglv54zFp49F2UqyZyx3cT6TW+gEfRHK1G7l6p96FuHChHNKW8pHUx9jQBNtvow6iKhLFnZmM44
Ir6ur8YGM4/cmj5ZS08PgYOrU+U3Ks4UQWnRHjo6f2xWVWWMUcKjW77MmaI88YZBVZti6oI8jGsZ
ooZFzT/E3gHX5WpC82Zt29O6I0SRgbpJ2P9eRT6QppUwBJ9wyPjcY6uVjipoaIhUbLherKSbNouc
NGvYaEDWbRX7c7d+BSOqtyA+LSjhQKoAKG4FBRitUNIHOAAwTfrY3XEfXlShnxG2ph5uNXYrUPQM
GkFk1FwVKCRsYtP1B+t4gqGoH4CiK2oZPI0tJjwDvfZ2btR+F7Y014XrSk+43YV7WH7Maknm56xi
7CL4gNs5HanM1Q75DBfe6281T4JGjsCY3Xdv3PZCJAmGydBUGwvS0Tyq6sL4fpxhbEOamcw+kccK
UU1lh0gGvZlKlskjtYkmyrVnC+g/scAm+14fuF3nHg6aKk3+85uMQkVZkk0Ilb5c4w+/O4ERbZkI
iaV60geC9W8BZZlgU6HDYxXIbIM/y/vLO5bigp4xtL7pT0PHW03N6Va+lrI7h19Bzws4RegrX//+
/IdN34C9wuYqPCBcwBhp+jFliefOZQOuV2jfQop9hTuUTeyD0YUdjIcK6eFHXM3zfu1PEYibIsq3
DUBYawnzdpRuviCFUvLU+VZCATzWj+mOfGPGDHw+JwMq4Cndpld9gfL1p+MrIFA/fTPjZBUXLyro
h5CDwhT/hA4iD6wbOhQ32TmhPe/hpcCqG7RO2GfDsk6HXo0xHt3oWTzd+LOP42BCObKTKnxaSkBL
4y7XBhf/KAYrVHGEjiV67rWzSbKcBtHFZeKiXdKChW5wiG31aVmoQQHShPNnYsDOH2ysfVk7kgbg
YxPt12yjYoYuEDWT4AvRaUY2pmdSXjK46J0oL8c4WULQSLQrMwj4uep4FLObBFTQyqBOD37oZE7q
11H3S7yP6qZHhw/m0CL1dQN5v8Du2YTqy29f/xbE/4s8sFNDaKWjmQYotuupyYiXyP0maz7FfRy7
MpwXhk+uVtsnHoxz7hYfZziCmwSHmdyBSt5xe7kkxXbVFM1+t0Q8VEyEqAjfSIjpEgGYozaPgTmg
XMGMmTuSlHW3O8czm6IET+AShsLpg5X6D+8Q/oJ7SqZZaNuqHPewWHemDI0PZEXYJ5BcntkcMBtr
y2/+T/wcayDjMxjrPgy+FJ+MW3NlaC/EeKVpthujek7jg1auW6LDNsSOfZXaA+jg3ifj4/CgT76S
WDnLhCs0CgtidwUnonTi9UnZixCXoVGs3VSDVVwtJmlPzfnxjvpirJSzimguqKF5yEgDq1phm4BO
mo1cDrzAn9nKubJzAuhSKUeAg9k5R8LFUrb9u+4ECFLJfYpJtqQlEt7jIbb/P0m17cwNxiqKUP77
YWUJqGDYLpD9eUXzoLGX342ACkrn6+d7vbZzXDd7WgQ6sQ60p9fapbmnAlvSlYIZqKUi69QIw/Tu
mIR7snfFr/2RztodvQhfnZnXDdYCBz3dIzCLDCRugGhJkqwk3ZN1PApON+RbkSkhvZ8pKZBSRe6f
9RnkK1KHOW2egtW+VkwgHYIO2YCOlj1glGaWc1tHqIM6WJtcvVJGN9ImnXKaKHfEpcuK43rRSEDS
UTm1Ub06u9GWn1SMeIfAZb/0jtaYcEBRjTq3s/ypsAUOMpa2Gt3HDYUknzG84zYrCg0IXG9yd1Jw
pEUu9fe+Um529mNC8e554UTIfPMs5SMDvhBALd/ACI8rfDDQsPbM2xu1ybp62nd8vzkun+WA+jj5
iGmuo6haGIp5lh6f6m/1XiznUWm9xlX7O4f0XbfGUKbgAMDwDTBc09Fg91W30zAjEYCxQqTtgfqg
0ONa9+ORR/205AuejrMonR8zCL1s280Bj+a1/8fJYGnPVjt+8MnEEEDnHeAla2cYkh0rTq7EDEmn
VK0ttgaMqheSifquhH/QIE8uq0XAvLvqVScgu80scgKLHWGmk+JC3NGBwevkNFYIcnOv8lLNOA/S
yWYvRwJMetQZ1mHWJ6YB7beUDrCn6SR3fMJDr2ZY9PfHSEEe3AOUZkDlFvtX5+86mxyZ1rs2f/22
4L4hGGNtMqri5DMlhuYnm1CrUEhlM4+vEc1c73v4OnxfWvOJN6SVe8Hxg2jpDYozr8HJZLN1BxM9
HjuuOWjSWlKH52Cr49iFviVM80nKVObpQVVJAxWKOSjsXq8eAJhmraPGUzdWjw4pDfYX6vP6XurY
NwWZj/Ky7tesMRJbocNR9mVaNon8hTmSNLwQx96A1TMHG3roDG6rINWt9pazw3nbh5FSJphKHjNH
JflLcM+T9AubmjRWhpF3itpRf2GDHupqfBlLmhQ5OcAjyZ0EKcW9ByBB+Q19D2s9AwDiwP/zTSks
kdxO45dDIJ/JcSdZuLSzicOMrlrbfZAeWFllqJp1BLZSRFL9QqGINiBzu8oGFRxp74AUjO3HlQWz
tK0VgHJCPxSlAp4ZFyxLEKLvEvSWy2qOxduVBMkPorDzYbetAqAWa4ijq6NlXkqNvAHxdvfyalTt
pc+Qd3P3yqEM/qgpmpjfXhzwTBZcl+8GDrLjsFTktzYmcunMopjn8EcujMjgMIu+0VRAlfpairW6
lOm8tFp5RDHdtd9DyhWsuwsWXZRYdLlSCTKS4+rmiTot5y3ip5BdQAzDbDfTfrgiAllScnk/xha+
SzYbA9uh5Zk8ueUWvwGkGr0CkyEiFi9yBsreDCQgRVNq95Or+FfmVBbk8/gxOKDJDjE8X+YmwUKT
sbQC9XfC1GbaIdjcc/ZLUxlDacEBdlDFIsg3z9tkyOLa5+fcT7doQM4L5BvEm1diClOKnT/VI+I4
tMwEFSdrhpFc0/zI2UZauniMk2yJnqfwIt8x8QMVblMXiUjOZZgQ3SArqY7QXtJJed9eKHkE+nVk
ZxjpbLcYQYl/JYAbuUQtGVgqTgXqXS5lc9vB5cpTe7oLbecQV0FodEa2THXf+HpkUFzLow4S/FYa
FRojqGm2aQ2/DxV5mb6JSjDtnDTY31XeTjuzBv87ZD6Gn51qLxBmKHXh6quSEI/K8F1K28xNe7ZK
mGFwfzwpzcX5NxkXTo3snd3yojzWK1vIzd8zztKW4siZuJNdSicO9pMVBBixCmzT7ppsKYEVor2Q
lnmevwhVPpSFmVPZAx+9mrPhigY7CRgxUn+agmrsJy9ghsOnGAp3n2VSbAKJKfXqRuzXeio9uL2T
M6whzZ7N85LhYCMT3P/MkOWZREZCh6XzLgVFbyTGRM/v1qjSR9BhsLCcOl6B8oH32ZKDRx/hoss0
6TXH9jsPFRXwKc3UDpOEK4dn5sUieaQiyjcZCl8tXodqOD01LrS4yjY1kHUQ8m8PoEYWUuP0JUhe
N9tySpyex0kS0HoOledQCqBUgWNMnzniGM+qjbBghe9m9eV9uTSxa+SP5ecv/+pUpO5PbQRUxsti
NUrk3KwWWG8hTeXU+Fs2kzcI1EiFr1y2qDxxpq6GsTSjpKIITo1j2wxntKCWtdFQZbm+k51VVNWA
bjm3uvjgnMKU3QFULKHn5SnfrFJDMkzFZETO/w22vXtJ5qnKS89Pfl0mA9rouD6DQnT/5YK0kqGS
WXOyMiwXu+bi1GX/O8PacLeZGg+bJDxbM0No2ylh3mxAlH4vlkJym19zq7bR1lVhYFkLX203t7Y3
7BqcZO3iYh0KpHZRzjLylCdo8f6TPIoJ+wUG+eilrnMBDYN4B4GdyHIM1C7b+nBR5wGaX7WB7huE
iFcDexsRzAdFts2ZaqL23e/KQymECZuft5S6dbH74pN1S0sbTJSbVZVUY8avSsrtUWw7520zMKpy
e7YlwFAZtGAWlWtVxZ+pOG51iXOym6oW4ALtDBOY+Qyw+0tBNPPG98Bkf/5faW6RQtMum0b5JyAD
Y8JZRUuBj8ZDAb596yrN7Qzc64p2mH+lH54HangvVZ7bTahgyZ1/dZj6i2DcTb8YdhOEjgcFYAuD
imhIQk3P1rOWgZBn3E8Rq+ypnhWJD6NSR9dU5KXMLJo3b1V8aSjj/pW5jXOdvSIBbMZPFEYyh86I
5znbnH7NoaPtN8WDZuMA4Xi5H2CI7qdo0VH37NHlYf4vDf78NGoiiz6kH1YTcQ7LaRbWmucmve17
oczL4mBmuO/GGVo6lSRZjv+nWIPsLCThdF4hnsi/UGR8r9sQIurL3jbIIXcbhXvPrbwIn/WBvKwI
5oS2AVmLX3RNX1+JQQ5Y5rXsz72MI2k5H/8gHscHKt4EE9qNVVBeSe9jHJ5+EAoudQLCIUNA5/5q
b1ua8utQIAAHAU/cCtrPMXk2f1L8Av9RFVt0x2bVz1Ws2Mvr4u225uLhQxnU7DkhEeZXxJbFVv6X
Bhp5QvcbvRTUzHaNVYnUt4c/eC7NQ9oWlWZ9lG5E3snU13ivkyoTBZJOWf2tkwSfKdi/4wEJkrtt
1P2SNqXItyKHcJSDzUswcfDDUitR5K6RgYNvcxuxTf1gMa1BnkBLJT9nj+nCkDafUKx1u3nogUh6
5Em4+wXJY7bogu0Co/rTg4GH3yi9QGEboElENLv6EfHJqbIBYohgS54t7RE0ArJaqL7f1My8MbY9
CUbWqB4oO8Wzoo1U4kFWxXQ+VuB3Q1RLTO5PtIntbrI2/UINmz88h1cEzkNx25Xi1xN9C2nP9u+B
upzEGd9vtCQzDLgAoLtRgB35q047ztNRqQBop4IMwCKnj9ySWpEq4pMkzsgzSe1qeK/qE/f9wpOz
IL+7uIwoMo37ZQLMOZLmRktlvpnvJ0ZL2RDlWs356FSSMo9e/5Cfkt9+Tvklb/dE0dqyOsGZRvK5
uiATwLYMFIA3S2Uo21zzKMEEWmLifynUimqREshBP0JX9Bsd66/cBZMMVK2yBtEHpzX7mLfEZZFx
IQd3+u9tzONJdy3sqk5H9usr1EgEOHBLZOKnHeVKyN0HPVSFO/wV+qd+zdqSQ97iL0IpZ1uWQiYA
6TkAEnH1Vw0EqmSll1lNCHVofCHWL3Aggjmexl7KJ4IbdWNHm547BBlcsovSbjGgh0lnyX8x3d/j
iKnCx9IX8FJpAuZncIca2AEBc28EC5ZfGR2+nBkNnPcw6sKGSFxlowFSI/tk0DOvd0W3OZsUMdBa
EhyLoYzZhDVmDJtv6aiuJC8JvbUHTUegWk77aZjhM1ys30ZML7/CWjdwCqS+h7xK8qs3rlEclFnH
68aIqgIF9atge0rzn+oAY3qlwv8HSQ7h9yE3yHptuix0PSuJH2zxYW871SLylPv5f8TxTE9/znNo
n8UMePctsAdNbYQlNreAt5b1K2zvNE26/RnMXuZOS0E7h4o4r4pRBefOsJpD5Uw4/5MeHcNs6RAi
ug/D6WTKw8LV0wbOX8TI6zC+4iUWmO3s8t3PKCJHGvZcy4RIFN98v6U9Ns0kZDnEu2uSI+6/IP3M
208ahyhmYAQOWyU4gk5ETMcyunF66xejAAV20I+y0gqD681f0+XyT/yLn1DMLnVaUG1Kg4/pJxvi
Xc5v1YDMqt3Lp3w1kx5fMcoq/xmbqBZsrKZQA0/npvKoVXWhRLcg2ewmMpPQJA2N719hmQnbjJw8
WRhCTr4iskDZiAVK++Z3BuJZlxnzQpFNQRYbDuW56t8M6shcpgfxFhwtbA4ey5AdeLLv7V/9doMQ
5U+ZHuT08lIg3zqD37RFqFiRRIKngPdVvQMwsr157SlRMH8GY44HRb9d6yaf6h9KKeWhsjKe6ncp
iDwLI1jbFXGS4grafEsyNqZBbqDi1Sp5Li7ubn6CKKTVlF2hTS+oeykXKn9nEi1D+dpHYbvtZbEr
1NdSPYB4iOKyHoLDEdIjA3hP6v9bMmd7PLxNRDGQxeAstPp5/pJyZR51+0F3kenxvKwxWeHC2Jjp
sJmMAHFw6rwflOS6hhqKMpgdxeCLY2dqqOVGLYL1uJCBTfKJpQERb79KMlaowis4COip/Hxm86CL
3V6HBBWPWLDBCAgv4e4D0S+rYXH909UUk3otj0MHf8XPyG84F/BvPXGbqbkN0NYrGu9Z4wXfNRgV
sphLLYFEl6dttgIZ6xVWU00c9JtyrcKk0Wc3H1VeUd6a1fe34HyEAScQJTGTT2Cz26Xu/6VzVBBj
wvc5cpMPyU7SmqcnTEO9+VY3axtrBPPUrL9dEJCw8jgbvfBS7YSo3qtdP7rTQZIiZPuAtZoODyN4
msNgUnNQj4nO3B2df3eZ6f+JZg2q/z9F/P6gluWBW+q26QNauHhRJ9yuyiq2UkaXjGsqelnvd3bM
yeRzSZJExv6W/hMH8HEYIzaS6hXkCSXV6Pyy/pNEVlQVcFi12J1fEXUx9Ccpu/Ey5WhND3Xs5hgG
7uZsv4HC/kr4gdAQg8u78JmD+bXhwI6Vg1cea7Cx5+OKd4YrKHD6jd0USUSbbPGVk3p9f+FlDW0N
e+tCqUwmhDmxJh2rMjNM9cIoYB/SCvy3fX6aD2bKCEYeO4L3is5olQQVE0Ch9krupGcZzXF33g9P
9NiMucY2Knem1emT9AjzdyypWCoVNdxlUcx2vMsQNl5AIxScCFaxzHnt+yaAu+Wvd+sktGu0OIsU
8Dv7G068VbWpUeHmbQPzh+d+EgzCUAY3xY2o+b43wmYj2GNGLXzEQQcfMOJURpDlsuUTRM2f+vjX
4QF4HUeXGkmdSnQ6RdoJXBT/fvQE0tSqKBxpIpK1kNYkXToYyjOmAGYjmG7GS8l6ELKvjCeiuDmm
+YB8+66XSPd1WsskPB5pJa+OTkgAk7g/54eluihyr0WFY5SAsOPRUaiOjiJV31J4BPZ52ygqjiHt
UfgtmqrmUSRdIfSOMeLpzbuhpZuPZNwLaY0xUxi8bpKuHbTcnLjRLKuT3kJSfBb06AQ5RBSOmWWD
i1nhZoCwxTNLkVHIC9teLBhB7ZOcWFS/M0R2oSBSUSF7kzELSHyxTHAJzAVz1QiPVj3dIPejLnrV
7w5hRbz+DGk2/5l2MxVjn3xtH5JcQigdj7umkcBtU3f65ShI5AuOUP+JuLmBRNBWxoLD1biXsDHM
KQymO3gOm9fFqo6hmieoHjiLtB4QTniyFzzfew2nx8zGbrfAJXpA+kyn89jneZYhWO82O4aDHnTh
aGsuqEryWI9dbcXsdw6dgGzGKjZR7bbD4bbVqF/VqC7dr/mO7RxE0nfZQ4WrKnW47/Qtvubqdr8x
Qt4RIrJnEvSqy6Qk4aMu+wkgfII24FaSEKb6j/51BpbP9GmFTzhGteLmu2kXJ2flMUHfVO+jLcVd
PFDC+0bJpuK/szxbYHLUI7VVuc2F2DRkGFZZffNPwgOJimPoqofmgqEbHH6SGAhPjZ+9Sh0F83MM
cJwj/b0XYWbBS2ojYXgQKEDTExct5MTsKb2MZSYF809YS8RNAGtByI7e3WIBxTbnSxMU5a6XdTTz
T4FCwVmAC64H0H4KHGuXl3Jj5quGDCrmRDlgEmDzA8062Rl1e3UZ1GAYsTMfP6B2jhcgJmRESkvk
XdJSzUS2k0t083GlstXqG47ENEJdT1ffrRabWqYEEOJ5DkyEHkDzsjXHbPu0TOCWXYrZnnZ1gXOD
9AR3OYR3cj5+qSyMaEInekuqTeXXGbSICgiCztMVwK8Z3Ec5ge8upE13wbRUdM+8vhSH3s18erWE
OencwrHT4nH5jh2DEO34crAgSdY2DhMWmfjcXTSgwfkgQJAKA4/6SglgSJIcT2fFYHVWKeFH95gu
lOnCr0dpjAY35uaGF6H8XNakUp7fNB6TY86Swq3wVZuAocdU5c/bfAcyYtY4LetllHqa6G7GC3mr
9L10n2cfC1TniHfx36YKAyP2XdJiqzqhS42bJowlLDr+Ky+VbrXrupmSVc0Zdq35bbuf3lmTZ2YT
WlDfnYOKgbDhYvM8xOai6TeTCu3UZs8IXLMEFsdoYrkSGUAfYZU8ezfOlLuufAc+UxkG6odeMjWM
FtVbEZRYyERgqAqSoog+4wUUHc2lIRvGqMX8DALce+eCKvYwqRj7wjTV7EegBRhUcs6oD3ojckrq
6l8q7OwiG7z3jDmGUST6zXumVy2+hExQfzn2iWAvfu5JTiTp+4XfSlvDsNnrOVxzVW5WB2KUKj52
vHNY19W00XKDOzJSwc4CrNwqWkev0eoYOwbK54LWabTC9pEUFbS6LppET+yCauL9T3Tp946dfEVI
s14PBsjB4cQdoSyfRdkhDfOjFJw6qnTQYMCvscH35wKpXDc9OJaKrsNG2HYSoHh9tAmQPCxTizUB
wfKon8UXZbV6oRewVT2lT4e0aAiPqu09BSSgG7ACW2jPjUCwuqcIlWKzfp2ZU4P9ONJ0vjGMUrWx
6hwkCayFtqCJK2W4A0v5xUc9NoBRAkKVi/E7VzfKWtATUTftS8wqbHyCH0YuWcNdFhDEZZlQCcEK
eiuCTNusG/xU9I7ylRgN+hUuktW7L3tEt9N6C+9/eyOQB8IS0N20cXpqnPWBTcboV0AbW0uJ0TkS
9ttBxgyVs8pmJiaHnzoeirOPnyRACRnOvUloiv3rOdvREb42+YgrUB+APe3CndHXhQOKuLHFo4b3
igQ4NYFIkCa7QDlqpGNF7S4WKOp4S1uFBADZ18RwMoPY5tXvGbhBAhDJOdmdKkVpgdNNifA0p0zS
LryD0QgVMjZBxy/JWavI4DtTFfzCQXDMkGtll3XB6lTL/r6CRKeY931Whs61bPWMQmpbSV463EFp
4oIGnK2RTZVJwi3gSoFsO98QIF+lDwxKrQ+eJ4Qj7VjDOcMIE2XY0ie6COw/e3FEaQwKiLkwc8gc
a2IUjZLXGF32Onj2YfZkIIj9LRRi9wgbpOWe/qOPIXayOowwbJT2zIo9/+a3QSjnvIXv+bhAhZ4h
hItWH0h4KUebm99VuPzQo8pUk+6rtOpoHXQSZ5plRiK8hj/zVLP/b7uXWiGfJpTU0kpqKgzyeF3O
tS7IAMkkNsfmJiNyTrGez8PcXMYXSvwzR+/Q3bFxkYf+BjX46u5N+WlCbbA+99eml+yVPrUyUQSs
lYtFmth7RD2NZcMkOTwwFfWPDuj614ByzuXEKyEhhYQEnayFy5H7a557ixL3uC5CptWkvmgOhYC+
wpflbZLI3+vG47ctPtlH2wRcToWck7oxMtFyGgsrBs4pv0IUwROF14DujFtvRunjRFu6KT1XqD7W
YszA63LtDu4Gc+7PaiXu8WbrbmYJGwH7fv/cRAhT82RSFeHYZXCRV9/8/Pp/v9SDJ9nL3YrMoBXX
Lz1Lg65+9CNx/AbzeVjn55urj4ERj/RBsHH06oA9P0tfguTvkUMLP4Fy29CBr7n50XwDDUAfa+VN
R0UHiZeyyevNchTl/jpw5S/GyV5FznNkAUL+R/Zgs+1GLrpW/uQVOODc0zbo1MNr2eDgdnd5DFTh
RfMgLG1n2CLoM4c1lzdJ7HwqqllvXw2EH+wDSXfsh6rnnIFrK8/aaYQbMXFJDfD8KSW3+wQA0eB1
w6BOhvW/146ig49YTYzgipq6I9HOLR2gt6ThkyDtWvBGf/h87k72OMMcuq+TvSLvAoHoiWK1LwNq
+Dh9F+jRLbEylubZstMPxEWoYxrt5VvvmTIHXyfAU0qxTmavnJmE9/IisJzzL5dWVzJi7uwJrXgh
zjhZXv4bKBAwV+cxCSvnom7QnQwXvztnGaTD+56qavCnUGsp315hVdF1qXMe0DliPQqHoFQ60QAT
T9b5DFCvA5hI3z3sFnyirzIC6JRZ2ATNKzJsjzFODYr6DK/4tHbr4qYjhZddYA5dq290G1hy5GEQ
Wft5zV+xDe69dbEWHrNFO8KFy49erPKSE6qWu+71kQXSEI3HN16dY41OTLs8ysAMYw+nqt6Ahf0H
EmP/IH0yCaMRonSyRcsYPQzdCnOwGGeV4xrgIGpNKSWOnUBUx7NbsMWpk6Nm81VPEYGVh/JZvJos
iyVZekBpE/80vZqmofgc7eyLFM9FCXSTZ9nQwq5Za+F/QNO7F0yv3pF4j7vN5ct7Br2+41DqMdIc
OCiWRJA57N+f4+pvGymTXIhQwC/YvgyIQf6S6Ovc3P++ZeqqoDB2YxqTRYziLWOi9xbbBWYJAbX0
yHRA6nMKvn/SkuiIOCdbIR2ym+liVSZyUXs5593lm3J7yPd0UA0os8SCebsgtkEvFmmUyEqRTTyy
FE9w/Dqjuhbd1ExFhLkK6VF5zrAgYwh03fF/4jl0fOeR/5nUQhSHs+GE+NyBh4wzh3feCgixMdFm
GISMFgktw8rmc3kMoEMHKzda2YreqvMxvdSYUWrk/NfYgAwyQ9lGCbANIFcbLE7/1KdRdHZLO1Or
dqljW4F2ltGK8lCg13o2mn5XNZGTjM8ncSaYchwxQWgw0+jX1rZqR+HEOvSx82U5Qf8X510n0+Eg
zZKmbslnC5x/XlJgd85Pk8Y00oi/eAk9U1FwWfh3SA7YbAxVjnrpnQyL7OQv4Kjw1XT3Yt+aDETq
vjw6GIsm6zAjsjfpp2s5gUjEy/JOIv2wmeehlTz4VB/i5qfMNPN55ukfZdR+Sd+YXtxyLfYIeHG8
Voo6couXJLpd5Cz/TFG57f4yCAu7TQXPzJ636a4hSDKt8lR0jHrvAzR3tyW5fx1X9y9dKWao8dlq
Oj+TFD6rUuo78M/TA7KFSkiYUih/Z32xeAHIELJpLaY2cHO+z4tInxTZlVQ4pzrF0JvhLvZzxXk6
ZwoS9Vd41bvQB42jDoG3NQ2HCnHJxtC/7Ct49plxogQS8+Cv0zVmolk93SK6xY8Ez23DRGDPO/Ly
zyo2azB/P73yydiAA8LOIpo1Y/DV9h2IQ0hg2pGATrtWz3kf1fHQGokc/0ktABb/kKXaWnKaxAML
eOZKz98FGLTfFyTrsqd7ydtwUHac7fEZlUDeZvbtW02n7jKqrQX+xG4XqspTM7vW3n7JsqF+2n8S
By05Yq8+EcQquOooMKgPIOJliFF9Se1Rp4Et6EKMk0bJZIXFhWKjcL/yd7KY7rwNRYoZZc094wb1
vAeMLq/U5L07XeH05+CvbkdJ23JRvW3Fp+HawwGR0AoLJO4DqnKi6O57xsqMiT6C1fTQfBewauWW
5M4z5fvxu7tm6YT6hL1CLRG/aSyPze2l0w5g+JRQ4BqouMmwWA8Rt3R2BnQPY9MNvGswCyXrzXnZ
VRu9ob+w2Vs7JbO2mEBUMqBO5U0Q/UT0RJDg36dzqPnGRX5srxD9NprSkAx9WMqPgSOSF+EBuEdu
JNI2F+JXW1XojoMvY9suLD3jOAlqXdZWbOOJjIucBMTDDRlaLHwaG9gYpr/475dzUTe1IcG/Rpds
zspJN8qulVda+0Cx3dQldOMxZMD94zdxr2WY5SJe37Z3do/qYfhdDvgW8R+5qt0UGo0piNWTkw0H
7yitkJiuj8c1L0yvkaNzwIydRXmyZOLTA4bq1ch7zdyDWsfwbF4xM+XFiNB6ox80uKRTkDiRXOhP
ngxJXck/FJi3aMzVTCkKDveta0+xTrS3/udRijxwEuOJKW669ZgkkEWvKMaY4+CiievSTVqvPi4H
HB/Fu+cRqIUsOzg0b6BY1Ianq/T6lwk8vQpK6S5U9w0n0rOMzziX6A5jt2AF0OUtaAA42Ejv3ufK
8sK90mzxjOP79NHaGIRPzazqnb9gWqoFhcvsncEkiAcWWoG4kB7RDVzw8QOUXCjrvgBzyTlg4Z/n
udPEW8mKo7r6Vjl4nOZlFLYyh3NT3bmrQBdx1rgibUhLfowrI1X3meVv02tUDgnK/fuvCiFBvsOX
QaKu93DW59g9phFq+EmDlJH6axui2oYBgegv6VSJ7Rz5H7jir4xInBjiPJgOqLI98NaKXu4PoB5d
kiKUA2Nz6PdS92R14S7p9/aRT3UIYIiRh2a2ulJ/pz8gCnifcRsyYmlPzECY4WK7Tf2H801OKKC1
aNAnE9PuQzdCFvJmKOWglmPir8Ieh+3Bj5JKPo7jSEqC1GO2WEI0JF4yAsVEOcdfrs4vjoyp0HCI
fLsvoXVKwd8O24qEXJ8FAabZQqxF4EaxWATc77GB0Q9LJlSkMtClut+o6R2N4B8kUYGbCsvUdAjf
qxJRPkMEoQRpX3dk4cPChrFZMI+AfBE9Q7hE7E66sIswDAtVoA8nuFoAyn5ROwN7UhsrNHKlm4Af
fchyallVHWuVWvrz9tWUgTlUDR4FrcUP/7D7gxmWG5hhiNVQ0x8DHeH+bFQY+lAHZQwWnsL0MkyA
3eZgDbcdVqzXG60IdXILbVEx+UqxmVb9QhHNEkJbO0N4tgelfu1cjlH4e0sfF9MWUtljf9K1BCHx
vVx+StI+/YfdLphMbm4CvafB9TlKml/dJb1gGjV7B6cDW1puABEoqtzNZrlLAPoAllMmEQAyGeoc
zBJHwWRhryt9drk2gDI8nBYXd6BtTYvNxfZWINr5c+jGAyF4rFI8ZBn1afAJf7TBLo2wt59qOPdc
ldCNPAbQnHz5CD6LeA86sYlzD0wOiqxl78fZmlv1ACxCwGYNWTRHxz9wCDe63C4HAlRFi7qj1JJb
cymt3tGcsCoSwxuCJye1Nt2egsCbhEr53q1mLM2XgKtmznpmMQxIR/LfN0cKZcz1WxjO6ZJ2Hz+w
1XyQBaTdD3z+XAVx9HVJing3dE3/qHOBFUExvF+8VBp0SWcposV12+ARVcAce9P88jHS8enBoPM1
TOtvbhBf0292RexGnJuNmzH3+dOqOuvm4Bvjbj5LL59QSwe2NRP1uI1ZQk3Wvq/0VhiAxlhWLdYX
uMqxCNO3co0DHcKiaEseAzKxOa0vy7u+qC0twPYs0YbFIqVfk6+xeR0S7tPnklZYJBSuTCUjUoet
fKNKL45m9YIyWK6pZcZ5eDjxZPo9qUpP3hZsokkz0fOVYU917nFMXpUNwoRYWXogm1wB0bOstp1W
zbjLYXsfbq+QZzKofRKdgKfRnz9JJ5MdV1oypaTuN2OhEgiEl6kned2yGV8Lwo7FNQe3ROviwX4z
somRsQEAft36KcrJopLKWG8jgJtw/uOR4q2Zokyu9b7LtzpkeT4hOPvWZ10lgj5COiAIX+yRov4v
h3M0PB/pVykQs/OzDQzA0Yjj+ladWo59zDXbe0LycN+qhdQDIcBuFQ8K/GrqdnprwPZMnyABVCRd
Pt/xcCnY6nQSxNlUPeDKw23GZSVepKdhmT5ka4QDeB9GrAlJo9xXlek43djbH/FxJSLVVSt7Veqv
kFvY7dKLk26qQ6kDS+mcm0Qn51WiIOQXHHdzUniNaUJ7K4ceHn6oSzCOYPtkg2lJ1g7yiBpNf9fk
mvj82BfT7K3/n7dY6jvC52WYQx4pnbUiJQcSIqOkOurw+goPYzuvIeiwUwCQmQy5Ywbg2Kbh7sLq
zyrAn+QGFrqi0AR+BgBxRvc5HMizS+bqIFNVchzKyaPsw4WEr6em0AJgwVSCQalASDdQec/en8Wr
J7OS579n5sFt8aCBOjhiOVfRLRe6RmHGOBN7J4AaGw9AHHVH1c3fPa1Ph/n5HDJTeHwY2mVViklk
ZauH/ujMag62XktYmGIW/f3PvjBKobcIMf5z8ekcwmGFVNnq2QJCmeCckWQJb/Sku4f2UZwBPGHs
OJdvZGcG8XkgUHrWH/1FZTkKPmSYPl9hiGE518Q2xowisgAIw73KUCIvYqLrPwJQfE5WPjh2prkx
FVMl2xsiYUTQHkm2/8Ouw5FcU9iRUqhSoTVwOrZ0SGU7aHxg8DkU8qXSRU2A1qImB1CfRXlVruTO
eN8r4uC0KUjZ7GLaF2/yS3U0DhUS7BEFxOWHZForb9tx3jhf9OClolzYusNRxvdHMhziGEZr3pbq
Wrz07hXW64pkPYI/enXjmBAv2qMipMC8pZPCMgAF52gn2YIBlYx5sMM2DLZst2dhmN42TZz8qjH4
c48rvaNSiD9TnJcjOslJ4wA8apdhvOfcBKx2wrq6xakhWnfvhD79Li2KSE/Lo2aOgYUa+DGemeDi
lyzoMqoqYaBst8kaLxsRsoPw54SjxB2ef3zCIc+NM+4oksDCOAUsMnPjDasSJ+71xgax0ksXvcB0
79eCUkDB5wDV4imwVzoSwcYdSDnCCZJkzUioRTtZl9QL3OpAe6DsSRJwHgGOGEIjHSmd+yNH1VmB
3SQpWQ8LymqL8HX5yeCRD9n8w3BDpfPNmZ9i+71JtOLFHNR8hQWGCSAs7QtFtaNeVJ+yOoHMrXFo
kDgbQCmhuI8tsfSlAHL4ohrDtN+drCdy1k79fUHSbPs1dSZZ3+AnxFNu0BzHB2NDggTj/9mgLE7Z
ixI/wsV5OSSzHy9YqQm1M+PW+q/rd+6dPb8F2EI6f9lfk7qHQRY5gEIASa0t2U6TjSD6scc1SMvT
36seBa1k0uSE8xNJGuBHQTW0g3Xq3JJii3VsV9pgHPKCnWeGhtqFSgUIpPrENaHdMJOAIwnXaevb
OuMB/27+E4B9hnhztrO84eetP7HKEXfSyK0c/d63JKZk106KKEEVDpOgrcPT2jaJT3hn6J0R/SXo
6b+chCNfnZBaYam8TfSsSmpyDnTzae+9Jp1IuQL25y6kjj1eYREf5LefY/hJ3bclxei3tKX/uL/k
FVhX+qxqQMU0t5/wp/rf99p5h3Iz5Un4Jeg69OthnSKNcL27sUL2Kxy32O9z1gNeJztrrsd2fRTW
Bbw9T3IwHbp5MX8h/n3RrP5rtn09M0nSOHs5vQ6MqxqjRDfQFLJO/B2kUXQ//jO1xLYx97K/d2Xr
q4/5EJ3QoB+3mHAqAW3uEShYW2huOHckENeIO3LZ+2aBds+MweVTM3WCZBTeOG25qXzORYbUJsIl
Ju5z8A6mp2nk770wuunt9DvlLMxVWQFkP9YH4GPnwWXXTppMJ4ZkVCXYlq8CpWwsj18yUq6fkbGO
xcfJso3MLAS0dojdlhPki4JEt4KK+t3bbBNmgeGKhZ/gHq4DSRGqhm4laXR5hHHR3IQobSycjSMw
UTNodIbdCaPwUEdO1SIA1vulHojvess4UNqn16DZm3ktIF2BErZfefqUllM/mHkZ7u81umv52z7a
rG0epYZHy63vGyelWn2md0Y8174VxhV1cBVZQ9KOcwdsz71HRH/xXeKUkImLjHM6n5ikgPr3P+Q0
B0qLSAJIQ+TV4g/Ajt4njzWBNzbj7Y20dO0zaRiIJjS8VDDSa6UO04HyJWCijxX19XxWP+h1+xfj
m5jo3J3F4BdwidxvfcOjinO6Xfg6RokntvAzIt1F/QiQPJpXO5kxXhhIBQTJ8SNRMVNlN54rRV2N
HXo53KEu+7Hjn/FCu2YJLHuqoIL0zg5J1tdPMZLXo0wK3X1XG/qNSyI7IXF99JQDCrbhIpZpLHfR
2xEHM4be9IhZK26DUaJgJ2hMi/3O4OxJbWvoIMktVC6hzsDNLPnNoCoXK8+fcDS+1ydMUxNUcqSP
vIlgb4HFlenEeU/Y/n/LoNjMzgpq5atRLi23ZucGSbWlI6tyFuyds5+XKSCtzuSlq/NsqyHXdb3Q
B3Hc0AD5qLNWzY20HmkCMU7BycPsONQMB8DC0xmo38lzsiMvZVTuGK8x24AGDTyV7p4Y4AKBa/ev
dR2G31wXmtKaJ+lRFBjWs6zpkcbSTkT/tMmJNrkoFAEpDfiqSGQ+BqnROlm8ICEuZwdu1b1DtmJ7
bsICkXjJKZVssRpdymWz31qGS0dwYyUyu88wGr8toKKYPBT2eH62oMtBV7NIAFHwKvt8tJUeo4Ac
YOXwXqJpttV4a0tTu3DOcjBQQhJzbZK0UOIH5A7AmGKhMT8omwdX2WEZarI1piRnGcTbG5gIZlsu
peelZOsXs5XdtMbYpTyiEMy1fX3fpiW4MoUCxD3P2M8Tvn+bj6+Xi02Q45qRUalSl1RwXaxBoE0y
9rR3HgAK1w1TRNbQ8MArVOnF8QeMnPqma2POEJcGfMT9XZcefOSOWZiEXtsvEzJcQ3fSs2ynrGmn
mBm84LSjITKqtmnM8jAHIX0RGyeCIQk9q7QcWA2yTge9WHdqTnB/0GBhf2sM/wy2jMp2uSCL8v9o
5qpKuGcNNCKBEazUnGEcxj8DUs2koM6Z71AwLoT9kjSpopy6+6dhaCXtpztJSI8YaZ6llACa9M6g
ArN8Wo1LtDSTufw6jq6jo01oJX+cuB0ugBlGSTrltONBcmDoBI+7V/Fnv+9BzzCjuL7gCArXSdYc
X20RdQ3GLpgCIkfuZkGOnMG+ITMxshnUagaEkDf9+bNN0N26qLdJCF0K7tk0BpwNfV/00xLlq6Ns
N0dplv2nTB3s9caEZl4W3F/G8pTeRKKPqtIIPQT0hOpk55kbfs7bx+uQivq25kFckQShLD4D2YJA
zl+6PLAJwH2rXZg/+CL2c2VVxkK+lfJfkvFHyaJ5Psbb0rm8CzVoROaJb0KDdA9/9ZJejEEJLRY3
IZdOdah1XyrhbKDDTtg0o4eptnkvs5aNIsUb+NUhXJdxoaDioiYUvGYF/H4oUYPnWCWdFCW0nRen
EOOyLwnK4le3dfnGgU5SAI0ZiBq1tAANKsD+/psSVH/idlsI+s7qLh+ZSNYo0Z6Unm2SeJTA2egQ
6rZN0RtvoKtjyq+jVP/Adhw5fJuVvSgLRuwRUjA+qJ/kk3nM3XsClHRwK0YRAsNTd9r+Y1XLaLV6
DJn2Ze7o+6ax7TYpZlDZQgHbSEAdJ/7xA8Wk5mnIW3eufzkVXauXJRnni8NcJtqvflY/j+MIh+gu
2pL8fBy/bLyFGcdoGJkVMYd2RFTOEddPVMgwORHZnWMQmd9zqPs0c+Yah1B93JaGOKPPdCerkCJ6
jI9HkWsuIO8HWiLYTcgrUHuyr23pS7t52MtVXgBVslyQFKdEKtCD242GWkf6blvvmqzcbO8BXtRH
hsMnnhlZbiqk+TN7rJG7VwXCiOOapdvf7bnu/I9v3kME8UDcxBGZz0frJr1meJyy2g0v/W2G41jG
eutWmn7tymlpG+gjsaDDnc9EWNZ/XkL0mMXiqt30E3xNb10tt5o7nyFBwGRoKR4kZk30KlmNhmqq
4enGE2I5jKg9LKQdsrLgIlxU5swhbZjnBrdXRsJ6u5aIJ+vquToMMHCCueuF44JdEToyxU9YIDfq
4Lh19EFkdYBC6tzo80+z+hfidq1wHZ0Mp9uezbzBnzc6M/WjryGkqRkm+kTCTVPIUKRBqMlytLeg
2CTbpquzpAbHFl/OTkk48DudtVNw+rYqTJeAtMZHnXW4YZyHYkB/eEBpeQBXBDV+nQkqpvLcYlre
Gb3kPCU4PhZeBD/NbzLFmjtbpxUhY+ggTwHA1b9Gi404MAoCSxOaz+lq+jHAvgIjUSTcIZKqSCwW
2LkTQJSJBuj9m18zGYQ2GeXI7CeXQ86PkarudHNDaUpVOXOYoh11sIQhvmYqtBr3Y0xXtvPlFUdE
QadZjj8FkUp/5aM+SGmOOMDT2awZmU4W4NbNrtUB1tqWP9L4ZUlv2hTXxN4pOb+mASLCVXmqfKeE
KWd9W6Q7P3KS9SWlkW+vrzrF0XL6vFfvLapLPCQ42O2nB0bzMGi9Nlu2ubENYnRFrQ1ohDduXveJ
DtB/V8xpZMs3yv/Aj8B2ivuPbDoHreWruGAsPKsIpX7sXEqlNbVb7lmt7WWyTW2cEERa+2/dkIkT
t+M3YoSbpax3czl0ZNzcOtn+VpYXOFKkVPdn3FgZsPyCDpPMA8p8hoh+6H2zHfejomhZW1onn+l0
M6uXSFMMMOAozPoi/IaO0JLJRUJ8F3wI5fQXeqErXEEb8SuXp1N5SlQ/vc+MMFSh7krsZbgDohdK
paqyEpbsQ8h2ubQo2UkCwQLydKgNSgDHIJWeZdm5YEQIEH8nihRVR4xfboaiPF1SfpXt2vetqPBh
dmU1E2iK/xeJDsX+3H/TdRq+Je017e23BXQ3F/b4tyPJVMioib0cnuka/kqMhduc6wHhCZcs7EdH
lEmpq0r51lu30Jj4I90CVAgJ8lkfhOH9fXnBkq3ILAt64TngHqREKC8KLZZoTEkp4kgpK8aM1hEQ
NWWBDa3HbAyvFKZdB2i26R3TUdBN4GKKMREaYm5yJgaJa9HKy56hgb5qZ4Qmi5vc0JSSDLdX2gZv
erwzoldHOEjpfs50n0jktYOnfocKs0dQaPkIyS5whTEWiqa7Readt4bPjD7UioffSfUasynIACDs
u9TpTSiljlMICdPl7fSy7+1M2SWp/TtFna0zCTsn/M2fJAf9wi0W9EwRlagqs4AEqXalHkiSceHH
qcCY16jqjHz+vag1tjg+D50S5+Y1TAbnTUSO9tYT/FsIXkgqPAexUdg2shCnx/wNyr8iBG1jHPNP
zpDFlmSBm14Y47kpc6dLS/80Hh5hhuU5tvvHQE5nWEmPfOGS3yplvfYqSVQ4PRb446+SWRCJ3OzB
Wz6rF1LOzU1+BRtBcY3pV25ohpWNsTF4mipVrfyltkNaqFdUu8l3jb6TdA7PaeQkIJl/sVfF9F5N
jV8ZNZDQ1DbzsKQ7Tzd02gxKRokdvNA9/vXOOye7utXQqiYbILF9gHu0WNTnfJX6TbSvzxobsv6X
MhJsq2dnJO+aCs7zqbzhTmqNaN08BKCmJTfjmkaHWwasG3dXdJee8DnfkwLliDMGWBzV09qw+2db
RCCsGUNQyRxcD0bGxW0luQgxMtQwr4w/TrYFa4XLtTAmQKuG/hC95vZuQdaDkitvQC+m23oscRSu
e68odPvz+j4acqkO9KORtwf+hIn1rpTOCqFwVVcYNNWpperFn0i3zAFKgd4URV7gQb4R7xhGs5aq
AdRztxIJeBFJzX7A/Rz/S/pqp+9CTgj7BfaM60jhY5CHpn6K4koZMk+o6XkZuhfEXkluW/VlRudO
XiftoZgKDCuSVs9F2ULAvmvliSU+VdpuefRUqHEQQbGUKqioQFzE2v2pfNjM5ye8F8we3D/EAMRv
zGJvhp5Z7WkMrTFJ4ECSPM8pp5fWaTDA+tYJQVvpJIwFIQoA7Bx4UgldRxSC72MCyj9ls/dzH91j
FARH7IV3s5oNY/LxLsVoxhXGbT5wFApg95AtoClQ8iY2LBkQs+Tc94Cjb5HWYccuP4raCbxwVgu2
cZczCmahCJWDq6jguHCiVY9jvxL+ncFbJtcAetjgDkayqWJKET4WiYCTXkCNg66zfMvCXtm95fwA
3i6KAPt6us2ytDzOGswSgOl9LSeCPh8NFdjEMe2oogee5My288mdAPndkndZFtqKuchP4TWtwt8n
p5I4CIxQShwxedkkp22SIgShiOS/JwvZ7Os3BEdGY6H9+fE6fmfRYPR0GFTRLzLe9xz/oZqukFJz
GM21RAE4FrV+3q+5ggf+JjHxapTc2lw0UWBXaW6foX5HiThZ2coDL63gJqqyx05GVhtEkIgJrSG5
Nw0V/tsDs8N2RDzgsRwogXVQGBdzS57s3P1kWCm0T6MXIxTz+uWixHr6XkY1bEupbHcWVNRNkNfH
xsHGYA3N06/ryqT/IdI5m2zXsZ5s7PVdi4pwJR4Y9IWrcdA1d0vWJ/jHPZr089ncFDxyUKJx/Bc7
CVJeY5prtb7FXnO+JBN1N1YEznWs7+ZNjrDqIB2QsDLW2hsg1+S6AR0ranQdOsVuiLVdS7yK15pd
rVvd1MbxPlAOExmDOFjdxkn7rpPVLGUAVHek1TV6aA3iyYAyMH0M/OhlNa7siksJLAhfjkbO4xdR
ChNlY7Wsu/1g+UxiyA3sToBx1l+SVu1VPkP1XsZM/tbah6GAXf3EJZgTVHQNZcKz3WC/3BxHX9t6
HaTy5HXrLmI1KNyKYZ5Hqf1hoUfDFupLIvrMNGe40BHoldEO3wisQKs4WJJk148H4L90ztkpRrIa
SJochT5FiLMAAIKNfKQMEi2dB/Y33rLV1nXuiz0iyMUIj8J0a0FSnbSeCm4ILMSPWFUT5+CDXrl6
FCvRAR/RcqDNVAjrTWIl0QyxQnlV8u+M8uGG+Wglz1yAqCknvZz2+E79eBo1SFV1kENVEBgNajer
A2AvRokgTNsHL4g4YSbpf5l0xNz2QxmQTntQ9ELIH7bqW4IIjTQmybj2d4t7p2XB/gllEGYX62j2
97BHb2icOkfJF76k5IMkYG1HhtHfV6Rkyq9iOu3KNnTT2zgFJec5aFTQkeiLa8rrhJk+1NDgSV0L
NHmegd6js3Mg0DBbRBqNlanH5YKjLsScOA/l8zwJKm0Wh5slvf0c66zIPXA5ywM4FC1vD4wb5tUW
IejgXHe+9VUPgUEeQF4r/J0Tn2mRIyVkhbVnyiv4YjuA6vRBtuOEDnJEU+NxqMB7GxjC6AJYofJw
/traYr+e/DvQmeU4tOBUZNhUiteS9SmqxpY/GUPmOzdU5rurink0Pn5Vt526To83k4eS66B8PlcO
3ZJblMzLqfZ/0w2xDmt9Imk0ULXCUrcwIWnjlHCNSoQBRrWeIAjOi5XDK8ikdO3hWiDqolrPcBUn
JEom/e4usUSBhrzRWTO0QpeR6xFQaiBpIEwuXUlj7e0Xw3++qrlr3WEkyWZEDpPs6GfYkuV3VGgi
8Vlg15aG5H9sA10ufAAg1vXSoYp8K5obfwGh7hoAea0kKY+2JdcE2k+qkL6EJoQqNjoN3xv92apn
SFHRiXh3M4YiIqNIFw1w5mitBu1IDMeGEaa6mUnFYFyR4D/958YIczRSvBWGKFMrc52nlAjqpjDb
VxKAXX+8MSbPZnUVR/PwYAbJyP+L0W9QCOBOwRNT4W4Wn5r8m03yqgjZTscDhylwW25GNAU7TU3s
itnbY5VR21dF1zHiycTfdnHGecZD6If5LeNnGIu0EMonm3+2YBaZMuQakF7eMuTxTgLJIsfMiQP9
llw+1LpWiEvH7Do1P1goRGbskgQ8M3RtLrM9jk1NrMEPIZfBKY1T5liRR/n+eq/gaTcN/bpVrPJP
igYYg4mq/Z3TSUBdNnmC3ILnLIXVLYFULeUgzpLGzmSmTOi2teciWlRPsX8Fd7cnW3NfkDZ6f0sh
WzoyptWg3bnIf2zeMdAli8ra/zjIgb5irpsbEIOc2wnS2jVaReVZmHKUHM2gMWiX1zMQL8hVDXJr
JBwlkI/Dpkg0hVKP9wSlzrG4cxecw/xQxURZolFBrsflrPqftXsRPMR2IByWpcSiEnJ5ah6pUC8u
vmOnjbmRLO7Tirgo7zdAs8UucHQXGerr0WEfpi7LJUxzlZMNwdbyturz3L4Nq07ioadM5KCNqz/U
eaDcxypXGUIQYxhszLZxmkxnYwQDBuvbPishSNploMiXzXqUAiF7qjcRVi/rRlxmVEs0FAi5iX4I
pr4ALqf4wER4seUxU3UtfSlGyyVqrfh7yEKAOeRKuVMeCu2ooN25GPED4tC3g3GW7DsKYRpksWAw
l/XIHTJkpWw+GyY6xC9GvWhg9wZySChnj84IfmnawvJqnmrINknpXdse3ujjeUS/cRDW4El8xG9y
JH3+1QkCnlL6z7YLNS8EvHnk3Wf+rDtoWrR6JEP2YeTkqANhFGznDM3KT+sf0PkHMHX3yk/No/qE
MyphRFbbzOkyYaU0cozRas3e7hbNjx5Vj235ajuo5LqdgDjgHMtRiihNgzJBpBbBWw46r1X6fEvJ
4pwwH+jVoNcSlEUhPuwgYoKwXsI1HlRlGxJRfysAwVLz+f49X7qc1KPkODjgHJezJxmxKvLe2KfT
GVVfiWcDakJRJdAG9klPniRjR21Qmkwgmckod2Rc4tKSh3AaLsR4pV76AinBnfBmnikIxGMNaat/
NKd0MaukZ0sDKGXvCUAHaei57rW1jghdpuXB95U5lZ3qqJzepN4oeb07PXVTIqsSF7g673eV81OD
lIbPAUqz7M+A6ttgaZqpjZXloaYZAXBsEHESe5aBHdYNbXayuTllVnD6iUEpGzZEPBoMQ+3oSNlD
1hmtZEl38wI+7JnIPdFkbEUre+LXJgcRzy0EM+6mVqaOlprkFA5IWfu5rU36a84IloXf4ltJmhH4
7dtnoa/qCsVGdTM+tGjydNmKW/lx+VyewACshwtxLnnyz1NkRxsDJtZ9gd/fTr/Z+RWcOr7KhTnA
eDFH5B1r7P20qBxYxR+04f/Q2GfBvqKFD7GjLvZF2BfzctSxd0s+AWBZdZa00/BThe7EffEZ6cyq
7Ix8umL2dF0rp9jugnLlCgAt+pGvv3OL5ZrRa8gR0Do2fXddV/vBvUg2AZQ91QGe4hwYAsFMFeVn
Jp89tWBXSmRdzmyP6eWkgHLMQlX8D5/hKmRBROFtUJPPN11wdj/evtOfoydugDsdxkb9rMmtyOEV
bL1UJh00otmsFumnRDl+sPrJQMvwm62pN5BdFZpcoY7N+eEjcYvmdcaCT9oEf4M064k1DqZrdbng
9C/UuNPkD19x9e3pLiz6M171D9EsdqpxdUH1UOiOCFvEwaPfzYWDNZWi5ojD5P6YvEME+ZSoHbj2
Xba5ukMlTON5TVmuc9nY/LYfyqoh8xMFXjwiRT9EMdpzMSEFzWay3gEEmqdGm/dAw1R8lcVGw07+
NXUIq4mBDJU6D4TrjktxUcgBeCmYKIa6EKaBMLlC+2qKP43/+RV3jWwgVOCB/0GEUc2glRlDAisX
aHXv4H2ncSS3gH0wDbDAMOgJTa4PStMqJCUX0Z1ZzTpmKo/j8yjaiyqAU9V2x++tjh12tdK9CeOM
sDcpTpG2x2wko6w89cLBqw+LECeL/NLHUFXm9g4s6VM0d7pWNWeYjzDNGGzlPBg0C/hxcpcBmqw9
L0MZPSzt6KqW6b1Ha5wn3Hrrwb7rpyed+7xSg4eLUq6ebXMLfSJgwHwkhuGPdpQs9tbRWo2G0gCB
l61el9xX4r2w/BHs2xobuupREpuGXGkG08eA3LU9TWbnTQucjmxUOV6r27tAbTZoVzmxyuWmzTAy
hNO4r+3SSPZ4Vm32ALjgk5gKy1rZ1LLbNnyAHn1l6tXlQ3OUOv5PFVf6YYv15TEXLRrS2+U8hUwo
OV0fUpGyTHJaAcK5S3nmSZxrGlzPEAJJRbnnjIljlgAzd3f+TfcnUGmSsFfgPKoWfmSKP6UIFxs5
jKqKUe/9hRVOG9ZeH/U1J/LITLlVnkH5tvBDFKg/oCDqJlbiRGcRJkdQQAtPEQbNSdgRc3kZXcMW
K47H5EyoX0uHxofLkDdQechmiDkuknyEvr+PFxIBE7v3de3y65F2vPem130mz1GBDe7oPsgLpLls
dM3VmeQ09SKwYkXK/V7PRdkZWhRLsyePwhFKP0fsp+xLVr0IEIPGS/SWn3AkPXm36RqpgvUBKnj9
tog6DNf3/GXKMI/082enT4/ABjhLbsZ0Im2F7IilvTzpt6PfLduvlnLTPh2hrjZ0M8qtViApAMgl
7u7NuUap/GgdVo2tl/ukdrzczvy3K/I3Tz7ZQ9Y6CLNrv/Xx3Ju1qFP4H4a8HLFm5xUDNoLO7EXd
gRUAWe7Z3q9lo3XXsZSzxeXklJWxLlRtvCzH6vxjKXQzNR+pFjLTH4OumkTTQwBC306ubA3tLZLT
+oTM/1bMYsK6OVgE1JsKi+qT1R0r6GB/xgRvZNSRVVmo5EpOcEDxsmbWLvleEtcg/FVlqOKJK1mt
PK1d0P5UBbkGOKymNv6u5HRfZaEPS2DcddWFws8Xns1C1K8/43CPmPQUaeYpNtsytld10ictVZjN
pt3vNnn6shzZvcLypfDSy9ByHpvEs0TCj6zsPq/DvoS0RF0Z7TI+duAMRbdqwnkUfPKcehcV0czr
PUjXt5segjFUSX+a4eEo4az2VRQiuMJv3l49CHJMbuaK5ElavPwwOPJfLy8RgTUd17RRxZti/azK
2mUvhb2FLG9DEiXV+6q/Jxzwraad9O7zmjsUSse31CY3ek65rrAzkzO9EdM1DgH5LoFLUpyeNsX5
5etH2ByIEaGobNxU/kR6k1yICgYDSmGCICFO385NcGL30bcA1pqUZXH9q/AnERTQEnPPsKJ8lzVd
LDlN9xAilAXpZ8sY8UJAqkpvwdWtGAzcupO/ROYGkcsVgvAq1+9LV1C/ZboTQsmWjBFXu2NlzdBB
/Q2w0QvpZfZ7EHq80ZuMLtEPVIR/hf91vU8np53mcNKtLheASOFoJBrE+JVQOzcNtOAqV3S3bvgz
d9yCJWqHliYqzEljT8NFSnO8e6THVbBhVb8HYFrcQEy0IjHDgk3lNapop/aAU5lwHWMjDM4+75wa
huxl0NQZcOjJ1blZs6kdCfVEDwVdfvpkG370LxxHNLv0iAyGHCr/vFzFT0lUIJHCikbc2Alo5xqo
wy/VyS2w7moB0NC/MaDM/mJZ0gyww2ZrhBU9H6xUmM3S8GLUPs9FGovHSA97S6SrPQ2zt6y4SeWb
FxlGJLWFysB0WSQG0vJDavIg85HPTLiaVxSeeZnyPLtyv4CMvlgiT8Qx6sG3twmPbMXGSUAKZUF9
+pBsTBxXAWX9ySdzB/Q3DtWWtE3UEHdJM4zo84ELclVJmM6MaMgl/dIk9pfcnHuSnFySGvo0Mwdq
6C3OWbayVJz95Rrihv2kS+EDv95EeJfPVqix/0i41qx3jR0nfdeWjQGoSyg2f+CGcF7J0N1c+pPM
FBhR0jfd6TY+oIHV6i+N2QPqvIoHp/NpKuTyQkyYxVfEHEbgeizjRwqHz+x2NKUmwquLKOu/UDHh
8nCXjrJQH4jkjpgbbzr+/MD4kZ0lTHT+sMhItTNv/PptFh0hzvMppzs7PmgEPydKoecCZ4wkoMux
slFfbAfkFCWXLGZkcLXtS3udHeNauxlpWhJ2fyzEUkt9mp5yVITcajDZh54D1kvfEX0BNHPnpmzA
TgqW3JnQzezPT9tbxlATrK/cDbcg2BWulYAHSzvZ9E099PtTL/5faCX7c1CHpUyyVbkwIv6mPxMS
KxCrn8SGDCtbqRB+4qPsCKYkRPV9YUibUiLA0Cv7XN0YxKe/ArSqozTgYrvnW1OexziMbPG1P/yN
3+ZVZMFZBx0fQlYiSLrOFF1EwLnZDqEfCK+7m7Ptk6lSY+FEaw6QwobFbk8CwnV/j5RcZOwBRese
3g4lkhaKqVGo6m9fQm8xRIfbB9SbP7o6bEhauDGiCfiEKGlvs973kkbaXYY4/fgJuyhH3vChMBMD
OsFdtAPgBXvrjHV38LCfwgE3sKlH2v5iv0cOqbu6Xvwio0ifAv40hMbdSOi/Ltw66Ww5UDYHMKE5
IHOs/SeMGs1QfXxKM6MB+/PggHZ01vBIa5XwokShCV0rbTkgli/AT85k8ft0iMW5XuCMX00o3im1
rgWnABf3ajT1l09xEeUNEWQHj6iWybJzzeMy1giVsecaXXi2TGXT+ZLgAFSvksv/IFth5T3lERKp
EiUGWNxaxtOOu20jbanFvUmUNCRZ+bVndnLcWjBezQkUw88BIpnln4gFOj5tYqMqr/ELG8VQREhA
AV4+W0kJ2nYHYjo2TCF/2b8e/CJMGMy8PpWb+/iVhohnE7hWt2yJkWbsPQyiRspvBdnN/Cr12WLG
PJ5gvOs3jg+5gvmk6gqw7irCxdx79B9EqcjxEhzE6s0u+L2s5yjWc+JM3N7H004BdwHzsrlCFFGa
vuhkClmWSFiJhBXN7CwTRls+xsEgsMvXlj9bOd/lEPhyY+ScPMW0KSIsIhB4uzG/vSbRfjd8UjGW
Fq4SNtUZoZx01Uc9YUdLPzTMQb+NUmeZC+FHgS7xdhmEncrtX/T8Wj61mDxAZIItgsASSpz/Zq1u
ARRKepwz1bkcAij3lLZbGWwh6obkCmPdaNDI4n6oorbE0Hq5e9s0jlw5d9Py+bGYSccj/AlZ2x0m
YDmsNLKjzZyJzPNs8VZGPG9jsi74CnHRhC7o0muV67yPJ6/43YamNUXna1CaJlxTzoQYFBSPBrt/
xi9I9OFG3EYfGz+eenwpsB7nSys5lZzA9HLbzFygM3bG4kUJ4Po7QYOCVbL1qI4plXeWX7lD6KKD
x3ZJBT5qizxnFGqQ0IbePVv/scH7SxnEmnqCjLk3uFhRQzLBwfaslKgMcg5pGPzgWN2fLkoYRg9r
pgVxAMCEQBwRSTTDnusLx5p6Jy0cS4yS2vnEit9uZPQTYMVHMmhTtfLh2cgHncNHuhIkt6ATgZrU
E8Env6E+eKsab0xmbxc6m/8hdGDSNCEziOdFjCEqNfaL1zAO7fYdPX4I9iSlQkve+sE4YZUWY3hz
0vuFg1y8uEoU3P78YbRtPw2/oUsoK0kI6pEEc87KYjyYs4zM11I7mglb4rhY33KzQfDzegNy6Ugw
ki51Acm/5a5OfbZOH95FUKXpatyTe62RDOTXxggw1ZRGh+9cJ1twCxksOdvOd21Qoe/md/1yNIJz
gAB+nAMycY67pj8HOQAL9toVupBCtObl0G4YB63tqe+7mYuSAuuZ8q/uCIjjW7U46jSS4NEt1VSb
70NXRvd6dHW/bAzbNU/pWRh9EWo46zCIq+450qMl63Zyi7C+X2HcnfwCuPXC/GpoipwU6ZBKVeeI
s1b6iYG2Z8IXi+2QxqMCp/Vn2CTiPhSh6Pa7k2R+WUS83krZeoDDKBVLdOhbeCeOBg2jWzNuiwUo
qU2Oqc/m3P9oq6Xj8c4AenCKqUBSnIRBQyvchEOGNuZMObgm5Rfxt4eouSz4KCgoPMZQ0383dTy9
lxVK/SdV9k9ySo9kJhwVTHaQ0hPA6bmVF+Ei1Kd/V2Ju7QevUs2k0VLG8RFfEVx7mL6m+yPG/vsg
j4E92sgCQKHL9vMPgV4xWJt6EbRwpDC3zJyyCmRkKfl3EQhEaBxqJsp/Z3oQ/1k01AzGsZmiUIVD
ynrAy2b5AFCmtMRz9os/3diA6pw1VoANXRciX+/XX+gEbo9GFMm1qeVlSYJw+oQnrb0ePvfM7rWf
B4ztvfmxLN/Mh+NodEkI6m8e7E+nj0PiAljXY2ekVb/BNG4cGHvSzzsRnNXu8dAOoa3wuxT4CTR1
YDNOlZJdF0+YfaKBuWxllXa8pFeId7ojDNseSwu83QxBOfqP4QEIwSgaNz2ZHo05ix3IptQ+Mbv6
eVqKJLCLTFZA1PzCPmHQjgLQqCn4OJ9SiNPZ3ufU6JZGNJCgmgxA1JT60iw20qDg+/GCq5GtL0Zd
Z/HaR4LYKwB1g7lxCcWx0VKiPy6eDqtg1G5gJLbbjr0ht7hbkRzLLY0B6dDid8HlTWVuc5aMMkfg
HQOLZOL5xSrsTSykd996OgG+U0ODtpo+SPAQ/GfvUYsz1A/PT6mRvcAL4t3Im9gjMnQZxAX4QRvY
4yO8NliMwd4i1cK2MHcS4IuKHS1F0N36uAjV6NsNHevzvCh5UaEm7Mbvm/uDtgvNX1AczaVaCcZx
krJ2O4X2GmtUYGo98fa02QZg2ZyWwnwXkitQepzf4r9gfFvAuc1VNXAmEq5h9wqrF9uDC5af5E35
pIZptsxkqeHAwRHKUNnI4pnvXM/PDlwEoeKVyKMD33IBhWKm8LZ5a3V6qhqOyvm1Ov4RH1eE5fW/
Pcqd0CDEfeuFv6IXYCik5tCb8/onkl4BghoDExY2gFZHXj0lu5wnLI6jVi4gCllr6JUxUI6XAWwg
uzQ20Ze9EqwAk+GObp4NM2g92ACK3LnJjY4S0OgRwt8XsNJjf+ygdDT36+qbndH2O2YX1BunA3TJ
jBq15+AJ1XilnVl0WmLCL97bQlLyV/LsxDoVWzQ+o/v8KPdmYbkoCLmNWbTI6NAdOFk+3VLfedAc
kKd08BlN1VcmZMlCFbQCuF8d4eHRa0ui2cyBW2duxGAsjIjG4wEMlh+JxHNVdhb8qvgDR1yGQCfB
BDS2GCoHqmZHIQxbYJQrBw9zMVk05rNPDno8HCiM+8I4P0G5nvqw87vEKqjunVY/WohR44GH1E+z
VuaKATI3Mk8M4y2CjMxFr1YV9Ya5ndFP5boaY0ytsgxeow6CJvvHIEE3px7mx1jXnARRlvSCtx29
avUbHpKwf/lxI0ewPcVGL+SPzjWQlyUUM696j/dQ2LpCMLQFZDcmieBDKbNpaBU1zF0b6xtoiN/H
6ftJAoatSFbEdTnianRuAtlA5eBANHdplDpoc2L4VTxTvNnz6EqXy9sbuugSMcpkTw2rzz7txgkS
le3jKnFA6mYv07YgBekHnxjHhvR8b7ZOv6FiU1CM9j/oobkRrhyTWKIlJrX5kb96hqFoI6n1ETcP
MmnRALeEI8nDYImsv1ysrD9U7bz0mhshU3q6UMQgDdeilepxuOmN2+q+MK5SnONz0biqr3Cd7IId
9v19EAk4tZnpheAQwxXB2Pyhl+XqskoT38SDmK0Is9meGxNvbc8lVz8NaYGco/5Vh6YhV4bfHQ/3
HPz1lEb8iJ8pB/JgWdHCdmZdzmf4fjUmY4fdby13a6fvUIOFViE228Us0sBEnblkT9etJeyb/2lQ
C4vEBS9bCHj4hqmB8lYV7S4MzDBFvXyRO3o3px4PZEwPvQT/jR/IvkPZxpBEE5NasSEHEAtAICf5
aBTkEpvP5U6I66PH0GClZ2pFWkIxruq87BvVcowtVmRjhKkLMi44+t99wHvEetOK/lSuvSLL+cei
kxB1ZplDuN8y6awfWy2tmI+5zSfIqvHNCk/iWvGOpVIPr2D/nUwehuf1pfSkYPIEeAjPyNPyqGoR
4hxEFKUJ6ng4kSguxfvzZT7LtBk+MIU1SZQtJqG9hVLPwvRFugBsObcmXZb7Ja3ScB8IjXtu0yNn
eEfgOG85JyNwr6MncXGyHnOYlQoXpshdit1ojmsMmgykWsam4f26kDavWP602uwIHZnrBErV92e5
sK4OkC3mU7UAharAQk/+gTMZHrfrKEntoVcKDbLsEI/j32kRnvfktrRhitXhs6av5X53+KUwjTlw
0O4MOV5pQ2IOWC639xcGAZRojCuak0odOWllDF9KKEcLsOytUBs9RnRkpZK+TSTwK+gi16YYTWPy
sq4dksENwPI1lWKChCZs0a9QM8a3VvP/Y11kjZBXiy6ouIrXrL+V3J7eQpJkgjX9b0vpr/eKaLeD
OyBlgK0KJUyYoIRnwyQkRGcx0uW2AQF/914kFdV9A36pL6atnXI8fTKuebx9AhFFzVohdu03ITQJ
3lvjXOuXstRGZgDL5bT2u6LDzjgpy6K8GBWlmHkqEyvreEozgix0lw105k+/7uiNiougPDy7XdtN
4Wyp9PAIcJcEUmjKcM/AzxFbRMRxcsCyGihcJeBOMlhLDMCKVG4NA9mmVkEbWmwMHolluZ3zZSXP
5LUisj0MiiICF87c0L85Fg0D6KeePHa7lnJvAx7GQ7O23d0nd+8EraGEzHj6U8awnt4b9YuVK2UV
nQ4Zx6dtY0RT839qp9/NUS6hesOZrMn1wcRnT6k30KVNdGQbUQwpNljPNYYjij2DcCvpjXPLViCx
/UkLyKDbLsm/bZVKdO4+Ppj81LBebxY2321ejzOu9b+ebCVV9qdB0xEWr04dvf2M1wtQPcV88aZd
Ra//dVWxNuOzTqftdjtNe0CUg0d1DIzoH1G7oZRLbk5NmrC4L7N8d06XhUDifNM7xn8tyseb7/jE
Q7rNaPQD4A8TcZJ7M+sDtQqf2/0hqaU9YinanN3iVzdby6/cPu1f4SpXJZQyZmQW0gGZ7Bt/RlcP
i8Aj3+c+NkbulegwYdIndHA0vRSC9NYo+IejQVFWtdjtPy4dqPKVCOvksoZsZs+2YcqCv6y6GvrJ
egmMJTlpYfcBbhTglmacT6NkrQfdIoJiBTOmgrmxiDwOzC/qCJAB8CdXpzRodZQvNAnY8m/f48qT
sh+OJ9+DtN5HOxSaHIp9aAF9pHOZVFBtkQrxgfvWTb63xRlZgPynEt/ceKLwMnBDJvv3VEvHIW/Y
BLYZ8eSgvp6dhxkSN3Np6u5O7Sg3JZQ6xTVqQxYRjPvDYL1Iq26+/T+/C1djBPmA7ZlMRX82A37w
S29LJY0RGaImio2Fb7t08nf3z/GlpMdCwce8irF6JsUl4/w621Xi9HB4G1JNw32Hw7Zm9A7Lhfma
3Fu7mSvbdl02J2IAYabmDZOBuSajTmtfjOdfcILGSqeZqa43+sUQg1SrIuE2VizYhD2ew5hNzqpX
CEumP9Ag3/fDGLGG04N2XebT6ZX4R6dnBY1pyR+MzH7fHtKpTq3eholYiCQF0ScbuCNpuPEVQ9PC
QxYcx0SLH8NFLBV10wtpfBSp02bBErvOAjz0g+RCLo4pdq4j3xYxmtn/IG+WHwGrSKTRLR44P+cs
swfZ1klhWNGhbg+qH+2Hc4SLPJAAVLnrTtxagANjKy9lzAeUDDr/BoN9y4qbHGKHBvzOJp5TJsT8
X0Gin94IYRkYgaFbPa/KuTtXVRqrbGCfcOgKM45wB2MjmYlNswZS9Hqe1HOkO20VUJjMca5duS3c
hF9oA8Bz1IKB7Jhd8kiErL1mUrfBSqD8he+mtWWHJ+UTot/zLFOpE14WBNnPPw/J3XzaXAm+fVxb
k3b963CdH/uPkFueOOrqb2itMgrBIYOrco5SZbXiZRYAr33VlwpSAm/pi2n7MmswzsI5CdKx5EPJ
kBA7rfsBtP29kITseQdiAre5Cbz6evLS0DJ1eplREoFPRPhhYRLZmKnTvEsb2GrLw8Gisq2oLPTr
4q0/rwWVjNlJ+3sz32IWioOx1l45z7VWiLldNqbbCWvYc0PzCiW89R5I5x1iKLuZn/E7Opb2/iIi
3C1tqexyx8nZedJHlu4PXDucd0wkIzQjdaLnpmn4jsOJcV0hXnOy6RlMU3MSlLLFyjlVV4WpRzMo
q351T5M48fra3PwglkCDuA2sXcs/nuhieOpO4n2smYALDEgsfTWc0mDNRcWBTN2Ctd1qrfmngANq
SyuGrLU+2nXoL7WTYFAr5PTH37fb/fkf82XPj1LXEEZGE6Q/ngBCMCupQ1gvMdMAAKF2Ydemkl+t
+toVDNb82WZInuYfld+9BTOq/jXMgUQgwD1de231KsTR11h86VyB+0JVv6cZkBA22cSeIfH59xFY
ieTiUYXKZuQZfZMVMDGyBOiLSZUH2nj7l3chEUrmUWbuIiOuNYtDHHCefb5JhKSdSCoHhXCb3Z/N
Y3AvGlWUDw3TknNH7v811CZc5whkGMOZQ7SR6nU8VqgMac9+gI6PHkEtlzKZufWf+1Ge/EZYKghe
zWajWQQ5bbqKL4RDQweVPWpF5MHAWcIwoIxW19kxQRq2EefoHKb+8Xsg2WPacetk6i618hMsrTTn
te4Dqh+7iHQNvTGmqDl7fPIq8WZyiOVXuuNFrgkeF1XHlIcW+5BR2C6IsAa+vexzYWWCG46wyJws
e6om2zyOFZfPLVnfPVbcF7JENLikjOBygA2NPT12otDue9bOnlVVvvSza7Drl66Jlka1AuvhAiVh
rKg5MpQNXTgDpn+Girm/Sm+IKwma8zgffyp72tKomHa61zVv2ukS1sgyoS0UQm5EvCo6vw/B28AQ
/DGo0PpWoMwtOZx0Egolve51XLFmCZ4UwPk9PmPmFAFzYLcESQ4yhbAR2oCVTwWwSmbnvWwP6zOL
L3A44UwgfYq4vS9QfgzdsoWuH130iRbNpDIAP1jrOpNhRQAqRBcctzGkFK0tBO8O76AB1G3q6Lzc
GUav4UezFZ2v3VSaXw3OjOIEXm19fO4SinqGLcSItZLFMOE+tp0GuuphFf2jjOVHfyB0Qq6Ewz6r
Kd105U+Kvcw1THeR5ljSKSqouaqLg1ojNEIsY3nYz4BuUG/L76KGAWIOA6zCfrkHJONyxTCa6en7
quy8jk2SixAdyT35etLvbFhLI/qFHUYDKv7KnAmzbySxhSnjdW614ES8JwRNszQy93v8Q3PM6gmU
WngLb/pWoOIkSomVm+vFcGTMFkE/DqC6blW6UdeOuBbxbvorW5UJnKWcGcwtdCbIQ+Y54iAYveDj
1/LOrZh30aym7fv0ueZlIkBfv4V9HGy0HcfzGsUyKvJyjoEVRBBagnr3F9cs6xVGunReNoe/PdnL
NZNHIK1dlUTh2Rmj+IT3FTDNXlT4Xh61aji0d0mDaqiCPQ9HAXYqU6yHLxJ/PmF+NZDOrlohqHbw
KHHu+RhO+I1JZuiAQ8dkNeYTM+xY17ZFsSKZiKgzss8hmiOPyjzEKNa0pn1/5HAB2TuQFz68vxLR
tqz+WwL4fmZh3iMBgoXHp2Vi7o+9pA6iDU2DmqOYzAEDhhzSA3vXI5S0JYihsBJ26R5f2nymIy4m
hiqNK+jhu/R0ftE9Oxh9avzTnZgTuZIBUM8avYC1gAFl+EDFZN6yn+KVuHRlneToz5KoNxZhMQCF
/v81iIWGMRsoeKcPrTGdgSFtm1PIBHfkQP1fuyADDoTcP0xoAlPh+QQ9uKJLi6/xM+H+QroDfhUS
9LtuIsQxNtFwkp+OGGptUSPufNmFUOwyHYl70E50PKzsxaXp2gt7zgyn28Mw/+HqfScWh014NQUK
4xJzfgJsKkAruqA77i070BrbFXSUtnSU08+24I0dSjLJQUDUaHZiaz6nZ5A5rOXYbXG2w5GimrZ+
7ELAQK8jPK9NfLv8vx+WdpWNxHUVHV0RZX0hFSka4jxJDg+lrAjQyIMFckOv4SSzYaCqNuknOtnx
kY1KTmRkUQFMcuQ02MI3WruLgut4kpH9qzjZahwJ0Bp/VI++S2FOyN8nRGxq8S6bXbyInczNh858
dvz+LDfPMdSnsNjiqVPHdGLmddRXuHyeqXQOYWMVJq40S8j+xi52VxIDyTDE8kODRcmfmDs6CT46
bjh05tVhQIMaIqO14TQfU6Qq8drw8WNDRjB8r9ilyBPRgDAw2O/vx09Wzm+sCil+gY9x6C8v2rZf
lzntmzHD29kUJvdo3IZmswvm3bj//YRsTxK6pCSoKJiqVQzB38X+eMB9ZQ4cEYEANE3W6DKvlJcB
I7NL8ctyiKNqz3Lkd4NZaiSoKQWXRVyyhNmOdceaKdd3wasl+R9njTrNvDSWJGWI8GGI+qRCBcRd
02RU9Cg3L2h/M8v6Y459G9LTH1LXGxc9oilzNQwfvLnueUvrNbPoNTx5iPHDAMaB6rcc4xZDbe/s
2rGTAX8OE51APLzOngzXr6Y9036A9kRVms+hoU+ryzY1ubuA10IUFmS8+npVlgFMiJCgY947uAQl
XZk2vdd8X4/VCG8agodiLUo4z5g8Uwb9Nb8REq73xwxgnbZ3vFUHQwlt3GKQU5wTV7rP4kIryoHV
xCp/WjCJD6FizeFZH4uOfbtmUhwtTf+6aP8T+VTo445nzjg6MXbvlT9RCeDpaEztUtoOUJjPecoC
uTf+udKOdlg7avT3aVNaJ6njJp5Jpbj8beDAGeg7v5D8FMwopZW8oZ0rXkQc/mlRRqCfyjj3hBti
eb8OdAhu/L+aunCF7hXQ36IKpTDrbYHWkry2LMka88FeT29/3BGTLMN2IyCs1FB+lqg7TDgoRGRg
62g4s2P0qz9xLab3xZk13Ux1F7a0f6ZcjXN2k/dpCbPFL4+sW+nCCExUZ212JL6LxbjhVj0a15SB
aTF1uBEC2pktoZv3TYFRdKtYFMXDom/g6C8TzOtDbv3rUly76ZEhWlsQ7qhOC/VNkieMP9uqB5rC
fURIrP/PxWTiDQdofOitONRrE23SC4BTS328uCfGOYEor8ToopXABdypB6bbe+ozusHfmij+/jnl
OwVYo01yN07PUXcFeZONgDuZo8dYUg4nk3lLX1HG8I11hEkFBhrOkgM6RVSX9fp+ZuLDtK1262BR
p9epBaCILdPjkx2qnO2RTUJoh9NZgT2ZXAh+a/W23vspOYL8//cl2ID9FWiRBCF2u6kLkV1oHGnC
AzeOfnYKR/uo4t+jwUBNedaqk8nkJJZMWuKNM18MnyySfPmMmIvbYYy+EH98ZU2Je+9YnSIUtQot
l/7DOCOyPFXhyjTkCPhwHflX8+fYInipjAkOnfeC3fdxhcdB4xsxsU6M6CY7rDtHcqcCbFPQf8yu
MnpLVzV+vT0piXPtajLgtrO4OyA6gCvfbkwh5d8pRgNFZBk0Gis7QwNPNLI4udrfjd32+iAeM3KO
wDXFY8PCt1DQDPQJgEYK4KfciTzPt3xncGbikxyfv3Pj/cnfz38qAtVnrA11djxCmGRTB5GShZRY
P+2m7DsnuX+oYSoISHgX6zKGogb6TYkLItZ26Iq6ep94QQeDZ7dG+fP/3L+X7Ehj3I0wS4saDIRi
Slgdjd2CRfByqDlokxJujIXK5EHeCjTA0V8NHtcQwWPF+3yaivMRaZs93qQXEmccUVyC9DxoKZNp
n5F4RD8VwbqCkonqfmwssfWmDXUg4cOQXM5yVL68lzPCc0AXphZCRoF52TcgK5flzj861is7Gb4h
8nm5WnuatDCe+Ehf2Nj03LdPM1KaOYUcbtwXry37WZdzXrNVJR7LSUJDPMUuBM4vbwdzwggsuZgs
cwADwGaR2UcjbZmIb5duxjJsT2Ru3JR1PFxnQbX4+HTObc5xSch0vbTDoKiY1YwqDHhkwDvcZKBr
J1mqsHH/U2JnFTX9/MEpACZLmwVIJ9d2KiLLY5j1IpfxYSzq9zYXOlF9txS8Z6nA3Mj1nVCHJNAY
sc0IunqPaF3ldidZZSVrWxhoblham32XXF56+/VVuuo8AXY4ai3StwzUGkSbyh6bUMeT4LaC+YbG
RSy/ykE41D3WnXC+moGJ7K2DuBd4FbCFnc1X08gP+aDEgBcmLWmQEcqrUbFRxNCzSyYyLnKy0UUX
jyDLwxhWr8WcGhkeTKMPNGPZlF1Ts8KsLeB2WJuzWkLllTMwJYf3lPCD2po5lbLpaupWHyxTPo/+
UD4AVHXHNEcOj9rLwYBXyIJVCyIhKGgGR/whTorTqc/UjXeF6jCb9ioEjFQFUQbDiTOSEsRCvriI
vQirSETu9Xsig5C52nayJaamFjYVZNHPAHxV4DAbcGrQksD0RXZ/xSeIsULZwykzKJ+mGZZcHm6K
4Mp6aWTQdJJ1hOqM86dyJn27wZ3K4rPrbBJLcoADjW0Qkt0BGUmf6szsUleiJtjNfclZVcI+zYD2
d9wEBlTbYc28KVgAxCQNM9EF5aiusUOjVIoqdkXgISnnh2cqN++4ynEiHOPF3ue5DbetTuymAZZQ
0b1+C8gS11eibs3e+2DeBVOAQy8j63erHNcjQrYfaUg9tR14lGPtyYwW53q+YZIBfz8CK4aCtTbb
amINhdbhy/1dr/W/l8GTOYYPv11OwBbUt4eCd/OQC7reR8EINWfiH2BFXR5MZD+AGy/1O6/91bV7
EHS4anV9Dd0w6L5Xz69GYvX6Uq9zYZUiDcCqadspgr4vyq4cUJE8mlo2wfTBOt+UuiEUhL4Exi6A
B2EzqAKHawWm09JWwunpljrUn/fQRzAt64/5jbxdeYr0CNa5T6q5cVuKjvYJwReAPlCN64IsbChm
VP4GGiqfG5mOWF+yNaqU+MWTi5gSw1b4fWdDEK9Juhpnn1c7OUB+NJ48ACmoH6ZZWvqi24fvaEnd
urAzlDalEaG1aZlrB0oM/8zpfsR8OAo7aFv0WSQ7/9Aa6dge/e+yK0HwRsYvXJagWAMWQ9XINWne
ZYB+Xl+UaMnSY8ap8AVmcdM5qMrDxW3sIlOmYt7/WIsqh3SdpfwaUYvfyniO9rPZe6guTUn8lK1/
8MXGuAiTdS9F1RJiJeejDYqfCeOHsDXWE/dvGxY5EoLfYuhJv0EKHrVd8jnQ3IK7D7RgrCUq/8+6
x7vmzsv+RofA8NgeD4C24s8dyne72UCYtHz75JmIy1GvIHNpSfrvFLaYPbbHL2qGSX+yHtd46IFk
e/xcRDO07U2WAo3h0MqVUHO/Ahe96c5rGRKufPqCLJBGDB/i3A1cZQ/t6sYUv77Hpa295ifsn947
XJFgquTVIO33iwij8XI0Tlud5W8oyPfKAhPA3olwxPziLW/9VbF5O3Vey+7VHPMKx/yKLuRzn9uc
03bRlFbKSQqRfiXz4ywkvckyikF3xLGeEmcea7kXJyPWA8R3hw+TNS7vp5ws8iTEYi2qXDlR29Lr
HvhFJ06eAJ22afFYT9uMS1XkJqKf+VT9Nfz7ZLRJpZwP7eMlDL9qv5vYGlIqzm+7PQ6sehZ9059o
h8qvTSODgaoAdCPLtGUyvNStAlRW4oln5/0PhMm6me1L65Q8kpnJo73+/d9RkW90OhrZOt5kJw50
yOiQTWsmLvO35DAvqfUUbAK1DhnWZugfml9lhwR+TMM45cpwlWyOa5AqSi75QQF8MyvDebvWW57W
8CL2UHK/Gu+u3Ed2uZe5yIHEAOKdoH3lz6nVJtjw3ep9+9V14q6BrCVf1kQ4qVVr5N4Jm4zrcxKP
nsViiNNeazqc8NBAjFElDbCBUQt+joGj8lgiJeSiCFay/TW25+J0mwmwvB1+tNYpP1ArTTG8wkXx
3WGIeqnaCmdjEIxP9GlZg6fKLNrGKqBDGu2nne+H95iMMR8lTVuvtiQ9uGm0tYDMjwpuYyHQIcIY
zQX0XRd+rNB5sM4LMeXQAtzLpai/rsZohCO6+dVvm2QQb+VgZTEMEbJVb9pe/bixCFETNJSxaBvz
2aVuWvPgE947LP+sBmgBypvwfW3SsjNJ2EkHKdHxx29kJA5IeZeR+Jn/CWjzazOT9QoAAFMR1XaT
5j/EFreUjPFHioo9KQqS1L2LUKkIk1YZTSpM/hP20xUkX5KzJfcjyHmfbUtxR/LGOkcifUxh3HHC
3HZTNPMYV4WAH6uHhniAq7vgXPu3U8c37UenDsvC3T/taUuArkHhsyCu9EPVQp96xyu+kRUd7GBS
Mu//eZPlN8u666pGTSI1u4posV4DlcRKIBy3QtErLbcK3MAgVQyF+UL8LMZ3iW86rqCfWBEb8/Fp
F0F1ssC1zWMouTNfXAChgR+DjJxgAC2tVuLd5y6fbYzwsq73sqafUbc8xfcz+VPgnmjAq7lDThaw
/6r0KPwo18lhBZOHsVc23cxo2X42z/lqTSMV0FnDTDK+ywuNwqeZ2G+23iDe58oG6TvO1Pxfb/Mj
R0V7HQYUe02tYn9W38kImjf/u8GcAb/o5EkONN+9Ty8OMIXlrPCch0LShOb7JvjZiRvOOLMxXo1D
N5UTQRI6OZ5Ttp82vr1mg5RLroVpQtF+4wzJn2CnE8bgozMLjsN64ncWls3f96cZUTAhFsvkK9B5
Pk5MJGLfoEW4zTu75iavszakFNKUnGIu4X/6pGvbjJBWdUpv5QR/M0frC4RwQKzD3EK7PI2+M55U
/cjFtbHJTtE3xU+pdnkCTSsvOmR3YOsOJUZ50J1+kSJdz+u5VzRtGycEiyrnWXstiudwUo8wLHx1
05+RJbfoO+4NM442oMYoHKdD9TobX0Grj9ZW05EoAMX7KL9C56X3wVx5VdIwT6upddYg3fP1D6D7
HTXpJ6oPJb4OsakIatqMGhu5zMYKSQJahzQ6bydqn6K3nRHJD9y88otVJKPI2ssPvoZPnaHe/GCB
xyMD7yhJovFkZQdsGnX+mtTo30ewwdyrOLQybLkDu/f/hxHDZVFuiB3bTLvOdApW6FoFHn6Hsbcr
7wNLUrRBvg9gVbZ3qyt9AnQfyUZHVhcm5ew8qb9UALCgxZ0qG92WfbRKPttpfHTiLIXaNwxxR+b2
VbZq0NaNVBGgOJ15MAjC0I0UaZsHlVYrqWKYeGXhjRWHRFQhWzwYp8QbGe9bGYtintfisqYz25GT
1NVxMNrI/0/3EpFIvK0anHo3txLYkdX7CyVVf0qRRgXFz5d2gs1/Nl5sUPE7gPvjymvFNcQtqeQA
sMYEc9BXFZghFkN/xoIF+wadf7/FmqkGi7+2Fx6loe4RS7aej0lk0UJtv4fQmJKVyzwUP6auQQ/d
oD+kIqdHCHICemXfcAq4pEsE0QPOjM8tP+OCJbJTIgJKpEL2GXuBnd4uuaSQEMldfrseodvrBMVi
kZu3ucRWR3O8+lfuFsKrMRKAygjvMIFYU/vfgIod7tms3vKOIArwUHDBoS0Mr2Wq/zDBfJNu8weG
orNxIXQ+RT1l1RsLIKyC+lJwNWgFXPA8Q4Y2B5WP3n3SUtyS8EfruZdOXNqZ7ZE2y3RkyjLEu3hR
LCUG/IeEq1DX+OH6m8PbHfHeRDLwdiGDepne1nYx9Gi1fqGch95B7NG6ag6Il48MvZsTa0CITnuq
jAAcP5SfINRmkHQbj1tNWLsYidNmxTX+1CnLFcrmFBQXl51imHJM5DaDvILvorHofi9BrVtku7Hm
SR+W26lvElfPE4GG7SBh1R49oE5oFuViFS/pp0LvrWR/3CKstDLBMT6bnw0+iQpf8y/YX14ek8MV
Ak6GO41L4TmAT6nPf/l5zfKD1zSMCArXT9/L8iZOTEAzTtBMm2XEajW8c0+RRDKP4NYr0QBfeC6f
12jxl7GpY28RKmerZAEcoR8WPj84rUqHC1uVaVKZCa4LkyJSN4SXEIuVaxCZy/iOcx7CXD+z/owL
/9WXG+17Fan8XYnH+ximCWfElH0uvdPKoOwET+gEMzwPRdUmtCeBU+fEv0KswRj9g2zpsWZPpH2X
rYuNr+nV6lQjf9wy6BNSBSymJDq0ugbmEpg4QVcjRKIPlJYlieCl3T5Gnxkm1WWh0rJYqRNuVayW
6wITdSW0UfaDiCbiJZd8P5Uks34fV0AnHmhT8D9hPqnWXkqKOSPCAO2hZ2pW0UiQ9seiPN9E3Cdy
fYEflDfdjgl784w80jHAWNSGsSggSRjXxuSje2hwG3rz+6ReZrkZB6LelYZqwGBvSHgMoOd3q5Ju
Df9ZDCMBrkGqDs507r3VGeHD5yn4Xqe5J7HEg55ZYkMoK7wOA/TEX3/k+rguDr6ZbZgxNaXcQmRx
UPM+/n0bMKxtCIA+iUm/VLztr5EdN3EfQblFxPro60BdumkkfMY3tfq10tEKVMtT4YymQgQ/FkLU
9XDiOpz+AeUV1i5lH4HMBWbi9Y5cPehDWcPStjgOskLVMgSFam+srVyyzdt7ieOuCy23S4fE+6Rm
R+1mQc13m11nV8iuKXaLmi71ktbOVOSZasDlSUWHiz21aL9UZw0NcKVE2nLW2S+Zs+t7iddcqUe1
amm4z73K2t/LZ7U6JBAH4ql3oMX0kYSPTdjCvVPz7NKTRinnGQXFEvWDjdLSBRaNo+6a5NeoyM5O
94fExdjKn419xTmH5Dptny5s2qTUWBAM3ELSFf+iatTgSUWbfDROVDXigL7uZtELXPASG1SL9Fxp
UEgtSTfhUJweas/xUW3BgVFug6/RZtYJUNsnY0fFFoVUXDChW2vYMjMP1AYlDkezUi/Q5wYsrO7u
kAWw4ubHVeXVOlpUqk73k9mABOz19DMBRPO5CxKJvethKiZ3LtH7wwc9UNmBsY08dtjrj4wgBV+8
F2Kf+RSj7NsmYlTnpxiwVNIgCrj2wKUTx5QgNpDSUrtGOzetE79FgDu3oWMBVbA46+JaesBo6nIe
4tcUoM7FIzZ3vZiUI0Dg0YYyEcFNFVv5hRBU57jZQ6Y7EdaRmgHl1/uk03Itnrm6Aem83DFQU3az
bCV53Ehrd+PRu16ALKKIZ1Tj/VAjk8QpbSvdD7Sp/MLP94Nu9m9TW9ylQ/Rqh6h4EKbYOzGV4lbQ
Ob3nPzIcYFAC5+i4MmY9BWQOAweBLYIzE+1kf6BaEShz5nDLYWfi0neEo8FO50I76+f+FRQv1eU0
3fhY2vzOxj1xQzZpM6/kMt26lXXXK5cX6UIfv4NRlsLak8NQbHsoAd1FSCrFwlgN0/UmEvkn8vCf
c0mLPfvV50Nvlm27mKlmMa8LYsuZVtuWMWOQH9E13fGqc0/Gk5LQ6EnL444lQ7xqLxRTWzpJQt24
MiqErouwgKP/SoGf1AiUn7myNkt1Mg4P/suFePtHTszIZEtnbQMJ6VXouaN6cAPrpcpWPmaU7p70
fSpUZ3X+KPJW0/nUky75O0Z4XJyTNS6bNSH1W0585N+gSQ9FyvIM476TTR7+3CdY2lG4k3dhdxuV
brij798b/JLtHAaNnW8HT3OzIjeoW6G8RikRXQ6Py+ufxGMGUqbYnQFq28kdf9UjwHSTM9594MVU
akUrwyHQ+UCw2T2vSGzQX4JhQw61bh5WwJH358OdVFqFxVao0q4adY9rJq9BKvNVamYZJJ2MfcRh
DdYUflrONFfJsLstwxFJT+kspcuzqu0SfzgFz03IaOhwPurgQFaWsLvCk3p4p1PxswCPEjjw5lap
k9sgbUyBIfN68hurO/ydLE6RwwcbKXoCcfV4t7x1WX8WAyq96oAH1oJ2JNBvZ3ygeJ1dzXjfVoQt
TpRhxLeHidrgmJlzFtf6Mbo1EfD4QJw+TZGQD0usZeCgUaADzQXVyRYWZFQJZ6JMgPTzTWL1yQQo
pLVSiHdmxcJj3clc/aUOq3e5yRzgW0veaRNKnrGVMTfBEX+Ox7o9LIsH/mI7pQR0ee9nmodDBnZJ
pphGTyd93Yg1HbXHNTrdfWCOGkLCIICq53ksz5jI4j4cIoLpQaeUfzaFUmeCPt3Un10TUoSy4lCY
vb3nEVZ4g0aMueGq0a31WHiv/F1DZM+rtsI4A+n7ecEAYQPdWYc65QgP4vbtmkuF+cphR2MfV7Tp
pCDruLJdn5XBOCxCUlvwg+K8Lw/4p2lEbB7YMnlgo6GBpQCVhC9U4d2SVdtyM/XIyv7ifiIIkKY3
qCXrLDcK5IriFjkLlm66gs/M51QWuf1OY0ZvoS/hd9O4p9Z3T9DDDCvLtRGaGbdcBtDd32RYwVZo
Y/yvU1uvKpw0hryODVkg+xjf/+SBGbipsbrozdz1F6MNeSUVO2NG+thMxkYf0rlAVTBtwVE1O9ls
GkrZo1qHIhtodRNSvZbJNJeb3I/+Z3qM+5WlPWgVa5jujC7S6I31Ad7xMxfkcEQlviC8cQre6JBe
l1k+ZjhJMbR8wJv7oxMdh+V3mk3bU9/M8CGgCq34f13gjML9VLhOIvKc4+TxZxb5feG1ENkgHYQU
qY5Zpf0iLqP4CBQf/9M+JDZ7juotE4/BjvbAG4mDK8k8YfsZAnrdFD7L/rn7KNvp5zYxd5JjNbTL
YVC6au6KvgDBanl0rBbP1B7SyCNzaDsbNogZYizrbvJ4HmoURbCoiCrgrkRytLaHNbZfCMbijv3T
NQniFgx6DSHMfPlPK6iK7wVdcReUbqCzl7xLo3SN8QX6BcvobXkLhKXbqvcNHj6edGg5G/Hy7C//
iKyg7WBaGOKSdlSNz0Bf/FX5X3SAtcteL5fFiZlFtbzlCmSSUlxz0zNAjWfM96nEOYtQjko5WOtX
FjaZ4HEEh5l0OKV17KeWFPoJ/d/tIzwFVTqQl50AW/+ki/HnjXpfKd5S8NFnnMbtkIue4JrH3yaC
OQKeiC0+XvVg5AZLD3Dg6ge4dZJgEhamw1lxcRLL2smH0HwMc+23isk/xmNl2mbnMkAd5Y6qb6T7
qbiJYmEz9VdRao+qGkBSbfKHApRNJ9Z0A+U0Md5wwasOikTQGSWbeNThgKoqk618/1x0dK0y8DYW
QUxZ0teYw/snWosuxdjcBzm7M7h4oHoV8nRAn0Uj9emgjGwXW8vY336KFWyyHA+DeOQ1N4o8Ll22
5HVB16A6v84K3uanSHPXOnWbrBNvwo/tb1N5jYiLj386mr6o4AXNFqHCay1HRP5NAdcQnNnt+4u5
z9Yv/fOVIQELR6Laa1/yEeDR9SvwOHrL8ZRF2GN/EKhLR0ub96tU8RDlMGVuQ1OyscFmFb+shQ4M
wRMIYDDxG4yv7cI2Xly65ysmJTOPcvYNyM1CKFM1FoW6SUZzBhbCB/8PpQ8wE1dFfQW6/5h/Fsdg
2Mu1ABiz2MBFRfRYGTad3DsO2SCjYL1b9zds6OQR+OkSmk4dX9JgaiQscpdmCT4gqu8+UcodNFXH
kgWbGwSBFdLbt6LS6EqxjljXKtq+5Ry2lzYaqEfPYtrkrh7AOBCVeqMlB0XPJdwJsvsjW0widqxY
qvwqgi1/DjUm73EGclVURd+GtzZSeSoUGysgjXIBbFYO7zRV9EC8pmbYhP/4hf5s7+9lqtXZjxh1
EcVaG46gqzm/eR1TAvh89r3q2s2OnWUvFh+zVxiAs/IfLZ+B3Ca4Lsb92eAJ/pPk8pFHcdoTEuYn
2vuj95FzbfpJUKoSMboa6A3Ze2kSQtnfe5/it6Rns/Oij3qJhjo8MQPIBvpsn+VHbhmK0ZNXlnTT
WdUq8Gwpgk/jqZ3ymJqu28pzmOYwbjH6nJUlt96r15ExNpgqkSF0YYylhXYfaqyG3Jgn7tD94g1n
8DGYLG08OxQGeRnEgQn9dC9Xjdgl4CEXm8Tf1I15pagMXzxQtTOsvA24tp/0MsSwspkVd8FFQ2i7
GD+a9GhAuk92Upaw7OjnoKNz+avyfgyOfHv/nLFL/qcu+idcfE7v0rrYOGPfbW/NQq/3QieUG0iK
QOFHDWDcggtk6O8l1RjJNBwAruGa03T/q/M41JlG+VszwyGL4tw10dCpa/kC1L+h5xImTNcsEmRl
PcCLsp6EsuJKkH4BiydtUYGS5LatQ8lcN+rfdYza7UtRQdi0oW86cIpuIMBX7WBe9jCE2Do9JLdN
zW9SW0lXfs3oIuDhKD+XFjXfR4cH8uSXuj3W1opYXo1u6fNiCTAngX6w/uoe2fmS5svtP/9Tgiuy
H5s+itpZ5EDPSMu15Jwy33b1bMflPXxcvBM3WvLmrKbnNMT7axUkFXOXM5mH9ySp0KoqkRWZKpPu
l2Tbf/yRyiF7O5qBuZXyKDcj4jBJ3PHx5fe5XgTSxNdg6RIs5yeNiIcACqPrDZIrr5929Y5PoXQn
g0XQ5aI82ICLShdD3kBXSGTIv2xYrLogWC8HD74pOoluI05kOryhIgZube6gyLU+TlTbY6jVbdg0
lh22xV2w9DkCLVD6kAMdEGs1+MpA+Z5tZfC9HEMtGMDKn1AAoLwkFjsTRv275YUGT6M5AU5OREV7
snafyOweMTrDp3P0y9zKXYYj2tt4cMaF76KDhqAIy0ei0RE392c3tND5DOUDwOvsitL4gvmwN/Ju
pnWD6CNNXeDkWFumtObaMgeMZg+kPmXyoK2xtyiFf7/JDxJk7NMN/j3wPBxu/v/fuKawG6h0TWoy
9oj2ivjrqtNmMhoLnID7Yb5p8yWBnsu/ntE2tyFdY48uBVjnrJ6mlIumfcMuD2L4ATqUnmlIpqhL
LiRjYLW6h8qumPSa0iyJgoN5i/xYM2Um7BoVI90RJUNXP1VLSkrVIBdlJborKS2LVDnjmf0I1bG0
GWpsWh9vtBN+9iMfqJ51h8a4R9CB9yuq3LM5GOkTGmEy5pWK6jZmBtXm+kq9kd3uAMxwOylRKTnH
cym45iyv5vnjDz0TGW2VAKz/isUy1c/yqFH0wlmXZFW3AcB57PSdgmlvA33D9tPh7GknZnhBIvOg
LpXt0ke6QXHW8ANAwMG0rv4l7DkYqcYb3A1oDfYfzwFNvwID3gYk8TxgIEKKWYVWzGnAuA2KI4Wk
f+IeQfKVvtDIpJ2OCVRVFyUBc9lhMVqajLPs8XNyjOobiOmoIf3jjoqEEwNG8XZHpNw9/Tf344ls
zZwy2Q3jzu2W2Mcmgz/n33mYSsfKEdVzM4r/i/HuBPDFX3b1iAMRtm6AnZ3V78v746Kuu1jgW91A
f4r3MsaF3+/YQpxDmMndYjDZzz2/U0B2FCCRfbkQHLLnTJsBHx0PU5CJT83hjmWed2RnBSkZvsQl
MeyTLyMw8ZRfkRPNPL2FvVEG2udmksSSKCBSKIT1hk/TSXj7Enu30o+UdD362fdnNa7xWOnI08J5
FDL3Xme3m00Z50f5aZGuh6xaSojJ4MTlyCPqTwnRuKYRrxxfcZ+k1dJO+dyvdNIQiqKgqmBUCEVc
0WsZaFs/inN3FA6+U7cOetVAKEeSC2kt7E0mVzOxv6Q4qUW8PqAtvn0kffFXhabncGAqZvZbK3Jj
Ih9iQMpFupXY8A7i5WRhv5srOSqiGWJ/2w/qGQq8mUfkw6xWLiAOOu34EY44e7MpH4RZ0wNzDn6x
locK5Gi12CwbRUemSYn5iSq1hMu9nzVkkBRMz2lVa/A8048GjxcFZRgbPUxbmQlNYbXX/j7htdaz
AYJQdXATY/PwS/tgTQBydAom+i/ih5RY5HWwCyk722a8AuZqsQ1VpG0+k4SQPqJ/G9RmyvhwItdX
Hx16IrUFjBlLKSr5swByKZIJGPdKSkl2l3hdLFRHiNF4gBTKrNzo0t+Yj37Q9SfCOUxYMGXT2Idb
HoWxCjGh9ydjZmOTCU9F/OziGEbiZqiYUGH/W2ptBMKl++FW0w55/Kw7CPIY1uqeqNkF+d31NTvy
2TK+d9qkCSRtlyY3FldlQ5ptthV+gxJy7gwjkokcar/GUoRg5PJxzHiyunDdGdJvKF7wq2kD0RGn
Xksov2hYIj/JoYWmRJM0O0JWXYluofOsT4zWuosF899Sk5hD2eBqLbyWGWv82aZfy52dTvYXqxG6
M0WNjxVUldRKeckLw/4cYbUydeVPJVYRkNFS+Pz6lVIaRVO1FqTXxj4LxARNaePJVAmYBHbmklvs
NFX+o/GnJV37DKE8jfFohRjPE6mCX6tt4PfBoNr/6aD43IiWAUciVI/xsHh94QcIZl8uxgogbmcp
JCWywwh/A+M83dd1//bFSxvM4YK80W5VDex+EGfxSKBiLDNtPc256vWIGS2gdIpOgMtFU5pu19lC
fyB9KJHaAlGIo6DtaUNqZSgeW7whTrHyB90w2VTFi14LAHr05ayxQ2Alh3Myluiy0/ZlY1b+1bwr
cVFvrCDlKEtEmcDN7xQttcr0c2XAdk1LuznqaTstWX/v5Q1/Z/AC669RonlVtV+UjrsAVrmQiqS6
JmvgvM5FzEyd/0kwdepB8MenfdclMdl2ec3qhD3U4jCHBrFMpIQFN46oYiwRyqdBPaQom4OUjEMi
Ny6gS/lCEEfgct+2DYiN6UOHVva9hNFnupgL2r/vrvJEUfyrLo0dmLCpU4avF9UH01qhfRLD7+8h
r2IF1L8RD/Ww1ARkm3qzkTiyijdLNQbZROz5/d3OyFnxxPvUgdF2uDCB0G2VJLVb7KNdVZD4yrf8
0JSe9k67g9YOi7XDSAOcj/xrLYexqjp0gmHVvXMHdsjUB5gSqElpOXAT9hQ7+sm1H9iMw6vx45ZA
QkC3yQ7qq66lFtH+1o8wjY41w4LfRMurOwu/HknPqXA/nKNRv3nkGKxgbmqfPwppP79IzIACustr
5Lx3MFslZpd3JFn3Tv8l0e6j2HabtCeKdHDJz+n1y9LmzQzB5S/PNxYV4YsrcP3kCcLxBS9Zg7Sk
a/FgEHjuzlYrggQLJZ/SE6/L78aVPKYzgEK5O+bvmAhV/tMo8tE41FAGMvmILREtRwGaAF0KnO23
zcDG+iF8SWndoIgFHjCmUJMCAmgiwK/Bc7J4mzcSZ4Hi2+/FPEjd8i4gog0p6Vis2BU+UJP7sER4
Fbxiw7Xbx8Cfu8PnF+mZUPOlaAq+QYC1ArQfddFnvCfUK9xh8wQMWVRmFo9dpAjpzbEyabmgEzC9
RgxytTwCXavtsW0gcqeSwL0vmqi0dTx8ppEPONw1OQF4pMRXTdmhNQZRzkJkPNlDQggbk5m9bg0X
ktKguADH65ldXpHy57uJEjoaanmTyI7M6j3Ysw0wPnxYIJ5KJoZ8umBqnwzuaco24t4zJyyosbTU
+KJQHFi7gcc5MkdHBpDSFn9OvGub6q0tsvyaojNoCeptiIVbpuQ4JpEeJnbp5IFWpcO90JpaPgOm
MJdIVg2ZuUUbi/jcFtEU4tbi4yFGLdVbDKGhZqjGMDYPWm+C8ffVa/2RfOE4Sf7RJJILmeK3Hapn
7mcHpEoyiBAGhlJCTe62KNEceKWuhwH1FrorJYc65Te3jhLoy8sZDLGjCaKXvOogODOAEYIk70wM
2qeAed7FxTz/INPBo9GaP1G0HasShARlwhDFQv1KxzKyfRbEB04iKKUWaRShqSxYjgkRiKR/AgHw
W3rDSISNEXEYNhhtH3KY6nIzx1OWYMYOVCU/6hyIc8pg6WYVFbZxMjKsYatJusBgZK0tYxRAnKp7
1n2mcFIi4mAgYqnNmZKowpqo8JP8haU02bauEt9BIztAiBsVyUzhMWALnbZ6uokhbwc/cDH/KiKu
YwG86zHlGHJn/oFYmY/KykndVa1eajB4YPOrPvYf44ysLAJguXT0yTYL+x348QzoDzVhxCaS3j+l
X1+pLjAcsdMkWO8DH+oUD9Ykc/bVP8TbjQde5qLwgeMBdPWdVc6N1rc65/p47C2Nk+FfUlfawJ+C
K1BIHztajqkQTfA/jx0GQVanF8ZERaPHv+TG+ztU+uqHm1mm9yN1nYHU+uCJChTZOTMm0BL9f+X8
zf3+CfyM160jZdW9kk40YjTbqL1kO/8X5FzbxOtWShMKGuGLh95TtcmKjKvPVnNUrhDfmN7BXMjr
LqxENX6pt2eTu8HEdxX/kCiVjE8h0tS9n+1+ZvesZcl6k0YIB1U0Je9/Ey+l/cONnzLI9rpwcy94
GbSOzmhFzAh7AsNEHFaddZTlQm/M9REz9r8B44b75yNFKUcUaUa/TaNFIlIqK1JQGgNKZ8IOLDJc
heJigKfOVysUtffq5Q8FJsqxXFtBdTG+A3RmNDXqLVkX4V/gVzUnYnAWHeB35dYu3Aw8dbdnyPfN
lpSf/dcchzDotywse7p6Sb+SIy1KP5ztWMhXIylcafSoTERr93HeqMGAPnaeLnyyxOQeSJ+hw5ED
H0GzGYp2WeW9E384yDSlaU2gVlS4vssTkH/aJdTLc/nF9miAe//TIJ2/LANVCDCvfrdGLLQzbZOI
GK26kfWPCtVyuwzJUlw1rn4qbS7Bdr1fH8Y8gGupz1ubG+39CJPSdZ8OqbXcOIHRaj5VHquk4dEG
69XaVxzRiYAKpM5jvD2Yfrf5OGfRhQ8Y1leFAwOWTqRA3nJx4Q6bgeP7YpUIu+zoOJ1uCSVCLsCt
as1ecFySi48xD1VInO0h0GJPJu0qTUS6wcZ9CrkDLQIPQ+PdHX5LET+rW1fetigC/zoaznPohAQu
er3hdAgVaBJRw9OGRCbgatp8h3UQE9F/Mu7cZiH9WtPsH+rHdtWtijLqdt0tJUYd6RylbbICpxw7
rXkPAvaYKn7XD8lOur62ydSQN6UDr4AlaQJb9/iq8wewn0ynMWP/vLocpCp3w0PImCyWN8YGJZ1u
lIhaNSMPvtH0e4MKoaHKMPuF/IXE7XyELGzAQEXa5ueq82OPl5iJ2fKB4yhS5hqX+p0Cl5qwIjUx
lFyI9Odl5xgQBzylQS/x37Cpi4pn+fHHHJGR/vXwoinYvS0Cyy50obrEbfM94oZ0ho0fJ8ABzwwd
XKfRFyseKq48KOnPuSbbtNmm9wGtQzrxO0y5shkwBv62YBfcgOlqBJpGFxv04q61dwyvNnsMeq36
SZOFWhK9bcS6AHMNxb1h2TGZv51EVfqY9Nr9Py3sQ6lAIJoQ6eto8ULePKI7tJ3z4eL52Kn3qpKV
cnePnYxk91nWU4RDhu22AyVCfV9PEwZ3WWNAfDvUXgo0byxgdAyHEzrMaxojdHw6HJRAd++EAeRn
QO9vGFpbBEaAXhxGhmYMwSiBRt0oSTYu/RfmqzLNdyGk3QnsEN0iKSbxkwkSRsUM59j+Zl1WjsWx
KbSPr4JWKiAlGS7rYl9MP+ilqRlpq18BdOnRgjBzi9VmrDTKA1cb4EleC+wWRTcPc4TUIEQUBbIZ
VxDEmHrY5uVxtEpu8B3rKStZM0BeRWvKuzpv/B61I5pVMJr3QPqNCWqr5N1K62V/U6pUlCuXHQRC
xesEBSGRhG7e6MPm4hw1BZo6dGc0OfyRKLYzCBiE84ZrqI6ODXArMtU4Nr/JO9lXK3q8LPPbZxIH
Qj2tuMbTdjFdB1c5O0+g2CSmVFRR+xeoLmXYBpu6pbrvpuiDyWHC2SD6X5vhhH4sLP7vH7VoLJyy
ceskpVbVSNIf11gs0yzbZKeowPqWIzh/BoSl/OGoIx87ibIqBCRadySCXfIJGKisqsKxCAQUerxY
Rp1+8zOb8VmJOxJNbDH1ksiOtBox9hjzGgZEDtw9YZEiG8NH7DwdW4U/d6ES71BWll4rLlrx5aSE
3vQoAMtukxa9s+OfJj2FtXCutSAQ+2r9OYouY/nauCe3XKbUuhgoR4m+CrDLHPaDArt6SNwaXSr/
KTTwRftr7XPEjG+SNIqFvxW05Z8HIBccU+llv7SmOOP2dlN18iJjJ+7G1BragdvRFvhm42Isms0X
lPOaDUTLnVsViQdJsPasmxiwWX3IqCpxmtgDQNkIw77i59ClNXQRQslgBKLaMqjxrjK0/0lKAUly
IOTjkOpI10K98d5vQsFuK8iCpfhtWfsuJpcsd6+NfUcLR2F2dSljLuDhV1HjClq8NAmWeMwUuotp
xbIP3cAd/p5e93alh0xjoX0A4VKYV1LI9ROmxgc1g6aNFupXPKUgz6vkE+BHIdkuMoD2yD6GGCwx
NHXkm2l9aS3Jz1CnIGsk2lI6eY5uQi9/CGxyPEPAloJW0HdSvpcIdK8x/YH1HR/7+jF5fQxVH9oj
aHEc4e0WVqP53awZns2NrpXMpADhYQB5PT2WRGHGh9daRHbnvA+drvu7YqgKCyGT8/bL/2oB3W1K
ie0WbeEKyqCeMRcTgg1g+VUFb8oreGY4VpU9g2fHrLf4QtcHlfuxbyYklhad/hE2Zve0XJoE0l0f
bj34i3Tf5+C/WBSLJTQp8zuI379RJC7cXDshNous0uKRXG/skfsdV2QUGBc1yrdQIMqOFJ/xEIK2
8CLOZ/piQQM9csp7sTVHzSiK+PwB49rK6B/ynWo/oyNrW149rHDN7dscutBFpzuIdvNvlsFFfNp7
jo8vKitJJ/UCmalNGph5gD2snLP7yp3/oyOw/ayctCeyHeY9ESVbJMZNeW4clDjKaqE4TjD+deiO
wQM/UHmwVyxA5EK9i+RHE/IBJQOOQdW2BLr/+VnwexwO2ot9sx/AHKR5R/79j/cE+8lENLK379VM
WdP27s1/OeS2BvfKiDXgCKSSFhSrFYloLTbwtkN0s0FlfBIPB+vHI47+MXBE7BLcFacGgnTSviXR
SgMopg68qew3OzoVYHhLXtQuusgVgLNfMqqX8ReUEqYkpDCwfypKVT2ApL1zICg2BePbFXuleuLw
SaKx+Ux35RJyWlf5JivcDRlKzuefDu4DQk8SJtWi5pniSOFlj62jVqgczVQ7hgLp6IU3BYrrEWqy
f/wQcuLxRSIBu916CLPC5Of4I2onwV282ETdoqmQi4iBLlRZN19dzPF3YZuu0moNM+NxjqT+zCXx
qEbbjIx9G+UcfOCZ6l2x9x+Li0w396RcHCRzDGa0r7da8B9GtDsZWWNSOZDmJdvb1Y57TlRNbxeN
nfh0VnkgPNi1QECVDfBu961a9SL6nRe3PKXMCJ6KbHzxU8TXuLy4pwyABT25PcP93QHKXHnJm/N3
Ys/uyGca+2Fha8wVDC6Gjy6s72Ywbsv1mLVYoeKWmDZOQzdqJkXhzfuG9OdLZlc9KJgQrzYlN59+
em0Xk07K2vw7BmKQcO2/cywWvWj0pTyz4tZq1Vf80QyLC96Of5LoiKFBS4AI2j9HJdIk6SqMpKYS
UNIi/Bjy8kegTpmpvc9D8LjFG5TpgkBc846vLcu9MqR5cJFHdRbNUa+oqaPs8QAS5vH7V44v5yDv
XPy1OxfrpbVCLjH3kRv1wFb/kGBHaHtSPzOkPIIINLm65fHY3yQuT0k1yyG5eP+OqVJSdt6w0JPB
lt8jhae0rUC/SwJejKIBG4uZKJcsA5kGYu6prK4PYdzY2p4BpbHwmRb8U4fbh32mT8IBHzf09WJZ
pwbIRvQsd479TAeibJ3GUPjRKNHDRvYFrJz22/MHuReHjeRZX0fAaKEfEPn3RgH3tAd025+16zNN
+UiEHdSI5VwolcNH1CEOhONq+OuIOgYaY13i3tnVWCDuVcS6MObb6w0jrABzw8eIlgQoki/elFIi
KrRHosaTc00rrZsalSFaTEFPoLYaQ/OWLcJ8t5oDg1usU1elN7qE9GesPHutrD4J83ECX0tXF0Ru
ukpQNL4LejnuU5jubPn5L4cwMLKRqVY6hkWpSwT8BnM/fDVaGHu75VqHSX51lQgmCx9EYHtBGRjG
x1XPae/4CY/kMzQTOQafWc99YXZJ9Vv3CBJyLxi2bBIYFPGRPfKumROMu1UVnXUIIQ/yNXe5CmP7
wvP6RY7LNJBhekafZdXgpbB3t51TrEd9SWvtVfGMC234WxoeKpHeLYrxa+wtz24IwEGk7su5OUyW
ZBCTSfJkmlq0+lPMivJdmYdtA3z3I0Vh5iNkTB+O8pnL0jNl6jlmdLAL62Paval8VhhO2dTMPiMs
J9/0gIaF90BWswUpuqUidC98twJZKKM6IyEU7k0JeEIYYYuwCGFiRKrk1HWtdFEALlYaR90PFBLC
vUdF0Jya4M2nW/2V/00NbqHiRHxGpEdeWySOyIPk3V0DcbnIxCsdw84pO7SOgbxS7/blVVbWN8Zm
idGxdi+uHX7Wx84spSjhp0ZG0Q11dqJfEiupiNSs9cuMk0TDxYFXS04ldRZprGb1s7ky6vltltiu
QkguuuJOvpSZGRqq4DO2x/d1z5kCl7HaJcyzoBybzLFu5/jfZz6UaC0T631go+e9+wSDF5/59D4L
HaGDm6F2vAO/rgtPVkCCmQYo9PNLJqdFdX1eC3AkXlVtLltvA+DgVYWYqF7jYvJOGNiNPwER9UIt
fMtzghrGVLEHoMe7rN2KSF+QahhlH+VQNQs7XH+k0DbdD3+u36H/NDlzcQGhBy4FMHFuDJLwY3qw
zNiZubSSIrd31pbZkEpT+2tr2ZvtOSlNdIQWywluSlsgIT1W66ILE1duBoaHUNVrrgK92aRcNObT
prA8cFnkx5NzzlFziothKzCx1nj2mV4+sWcA67twrA0kDmWDw+uYETt5+sbb4x9DEC8iXzION7YM
ZF/GvVszoSdroxsvvzHcs/RHhs5+mrRWWtC9eibWdBcmpNF9/CHx0F9THacqvgvPOo0Cvfjz1oK3
jWXc1iRIWbmdxEuoDZBahVN+1VXJqJJr1zyhCEtwTj4IqoMfh/6ZgdTDfeSrOkSnuYrTO3dITRXO
XZUeXUdumXS3jODLgiC2iCLaK/un9zNxtA3VP4Ywi4rlG0D0V8l3NwEGk2FEZgmH5n6udiBTk5EF
Oj2WSjunBSMdU+fCjRcQ8HmggDU8Sfh5KzBrTxAy/TR2lUh0HFzsLCb9HJ8PmTOKJoPngcPUtEl/
zRsunQkioo0CSP5YoFb9k9akgzfi9A8vPzgWlxSb2+bUK1wbRpD4v/u8vPU5N/zZKiT5O6nbsDMI
CAghdFzq0XI3iEU2YJKn2SIoH5rvfyeugbjBMvSEwneG7dmH2kCgv+9PHhftrzSWgD21x9PPRr4F
cLeoZoJlaBJHn8f7aY7IgMM86zjnnqiPrF9TyfDMEJRYml+cSPmGUxm0IVl00Z54QbPXB7exuUnM
fnTLc1OLnCozMavgYR+Z54yMqRtRpScqy7negNkVmUqXmiKteTIZC87y1LGKNj4nnIIPr/FE0IS4
i/+IE1iqW0W9X/LKgmLyM6F5+F0WaqNxF8t2VpyolxPswj6LlD65uAcFKoqcsEBHruQWk/AFIsTN
VNhTBMKTgOt7CVdipZa7l7aAfSFb6rOv2NeJDCVQKTFsRXhymvoBc215JU7hr5Azn7kQ7NUxYAv0
6ssIkCNaHnvtm7YK7op5/oPJuiDVYAXsJYkL2mrz/TPWL9q2pSGyeKPjgwR5ZzlHumL3+8BzsKuY
HtGuwn9Da5e1NFCdCFYYXH49qnDzWVssQvArOoHemw1+y4I5naAKPZ3TJksPEc3kJGt+8dDOO9Ed
Z1h48H1E+7zDmj1t0wT5enSeb6aYE3Tr610SMo2y5OVkuEHBumLBiWIDb8MBR9iqjlFaezF0EZ7n
tQkEiXAGH8Lbkt3eTDMj0qmP42WRnJ8Qhi5D2EEZQTb/HBHuilKz9SScmQlsPV7Rb4eARlG29yYx
5L9fBjMxy0csiTXg0R8WWZLPUJpBaDt/lCzN31geNHvT1oifW0rbA0zyCuw07ChXJbaJRXoBRFt1
RecF/+lkbPNTC3wEFPr1BvqshKnBavmap7MFuaud7qYh5eM3SVi8NRD0zbt3PlO+QnXu8gcByP+v
DqWo9eyXfngRa58pKrFSmDpVLIeuSMSKLff8kRMS3fiCQeSits/CejlMmgg7sr4rPWjKSXjeqG1+
ADh+zMQHqL8x7Dugaf4tEeZ8Iwa7RV9sBsF1i/qvUaaPgaE+Gn/Ix/vLU8dBZCQAC1IWEtK5TcwR
pKNnwRDiVwz+6ydy2W/o+WDG+lxoVYcfJccFDORud9aoAG3Z7B0gt9fn7K632Nn9t2WI0NfJVIGN
mrS/t03Zr30FQdBX/eohgH3IOVpLmWTjk+8dIctuOoD90ewGq30IZYNW590FU/rEDU6QaAKxSeJq
x8DL5T2buMinb18xs/o4WhOQh137CJKBBDlRXVMxoPlWPSwsYcBv6Pyzo6u/9Bu4O0BK05iY1J10
EOQeldssrnVJFHot+DoZ2DhbaLwYc7ldrOiGZOJtBbQqWEpT9qR6Ow0akvjlAaUAsCiDmZc6PVls
RywC2zFJrd4tMCygiJHVlTQxNnHDIhsb72Psd4hEXEnuY4saaGBDznct8Grl/gitOeGXIKrKC2gq
0OX1x7PW5f3MbnRduCThfaG1jwJAxqA8jghYtSwwnZgboAab7cIGIIlakbA9tSOIPJYLsKIaKT+H
elnaksJwiu88qP2IJ8FvQ/G5wg6tO5DUzJi1BTZB3ORO0XSAny4UWP0iqnYP6S2b8J5ZoFicYROy
N5bqXrcektmGmgdvfHs0D6jLGmH3RYwrlbDikcFnLtShF6wCXVh9bkTTmhO0TqAAT0uj8vtGONv0
Kr4tZX6Y6JWe6v3cHh1UNz+ytJ5PQ6ND5o59WxDREfKP0UWk5qjtpTYhLtn4IIU04GMKSgPK+0ru
AfxDHi+m4m+ABmEDKhW1nl9NAH3oClpyKFsf1beqF0AGx8UkV+trHo7a1YfEJHDxyreRqDZyrfUu
r/vof8+uGujcSZyuhp8eER3qOYbLPT0BNUnXAZ8VZ5AIrBcp9stzMGIARqX6hM0JuBSTo1vZba/T
WjZontLp+NuJmTLwBrfviY2z/zC6Kqnw6Zdb74swEyC78JcfF9ky5RraK7JczAaprmjWY7hALMqC
zlZFDf8X7lxlw3PX6YEtXIu7YzTTwC/8/1FOfeMaWqmJx5T34e3lBqPFAB/E82M+1jVPRKXCxZuE
vhLDW+eJc/ywyYn0uqsTwoleMaCMoRlqN6hGbwJa37jc/QI1PGaNIODETZ+xDnuu7LmT45hfNPlz
DJiTb+ixkRlFsVV4vC0X8QKMS5zIIFc93rK9aWMM4GZ9CM+BNfLoPH4azIOedy5fWyEa9xmXGCHg
ifb+GWkr+sz+gKTU6iwkX887TeuZW+9IBpuXOCREvwP0A3lXCto+e9BjkUOiA+6TsQB1ZVw5qLo7
WxuL+h1JdkN0BCrISoVL8dwXLLBmOJnRg0UeIw9BRT6UPWK2daJe4E1T80H6E5e2SW6ocnGxrGlS
bdMqOeL90nerpx9/Xx2JwnX/UgnQGDdyX9yb2be5bYErYZrefjPweWwaa+2IE8PPL19OeJxtM06n
QRVTNtp2HLjLIZdblIkOGqG7qcwdpCzIsI6Y9MN2HU4ao+x2vzZfDWd3YjrQ3ckM2tY/ultcBD8C
bdtNIgYAwj9clOpp5IhQlSh1KIM3fYzPomWifj7xhEspzH6DXqUs0Gl/fexYQyvYsXpz2hSOpwJy
5Yv2EHCABGid8/6E3nBsqP/Hjs6dTc8RW2aGg33ikIgvSvVdGxHL+CA5mPgbrvoP+j/XgpV8MwGu
fEJcVXyUyWoVeu8ISObndunyB54MPnsOqcExo69b9p7eIZ771uwL8QMNTLxi+0aS/OHClueWEMNE
T6eOxKfPWYlgJKl7eWJTIE/lbNvudsqbrl8s6BzDNUKJs2cR4Wz/O/airnYpUoBMddeogL5VUMIh
DFmb3H98uHwG1egmgt2JSfTQdj9ink3xOTvd2tY3xHuRvVOP4cV1SAB6i3H6GIeK7AKTJn664xlg
k6ot7er0ZPeALkPcso86iX8wz0ZvoXLpm/vBY1sxPXJPAac3znOIdFT3aH82n1zSyL9UVAG94TCW
HFMgYe+Mf9UEOaoGEUPpU31KL2lbBVdeFzvHgZoLUUu+EW/+MA6/y4KJmJOOKRaDAHrlkR4y/o7X
DmpENK6La4HSTMG7ER/Shex94SMU4wxduGhQCx6NGX47Px71KQIkHt98XvUxENaqfJgtIr6dP/dk
6RoWoyQt6akGTQbAwRKwLXthKQ1BXUQk9rCMQLw7XHjwacQ41Tmc3PshZ2LzLcSIlE1km4ZOqwku
FxlCnIKJdXg3BOp1oWkkCAe8Wcf4M1XQmDnt8kyqxx/56TeEDr2il9U9AHDLeLfazHYsSBGFx6hQ
+3hhvFp90az86HOpD04rS1TR1FK04TSMx9rWNm2dQp8xZbDmOpSEn1vPbnil4JWNpu5mcUYVifQU
ejJX5GXCIjmfdFyZINMOyeK5m5uPrmI4sxCr7LxXnF3OE1orskzFx9T3xJv6kKUW1IeVYuoEi7Xt
6QTe6K14AsiqD4okRfwMK/bk8tGE1imwFrRwVU6qCcMV36oBRsyEjIilubwlC7NX4iGWkIeF8uq9
bjeSmpjOxuT6pnOcqvXaYL34pY9NaxfI0XYX6SfF0lnWLapudqZG5MrHp10uevbrHpa2bEgVMF1V
bKGQzctnzPVraO31D/hGdrc+Dv05jPf7nStOpWVWZUXCgWdR77S9GW3OlOsif+jCRlJo6ZYNocs2
9ygzayIteAFeRWrblQAnaUA+u3OV8eGhWs6G24AJ/5fYcNjjeMH/nijl2gxkvTsyPF6/LllrCdVI
2bZLa93NKoZ3xnDeyJeXUZxv7fMbOb0uxxFO7biqs2ZdDqI9Pw33a9VCUYJxxYKJmh74Vjl+TvBV
F+g8FznDNyEvEeTk3x9nAJ0umVK6zqZV8p6DeQArX7EqRHuRXP3tIj2+Gmg4/OXYEflu4+dVEc9j
N+PEDKjHpmDH5F/KyntbiKaxSEmebmH2+7FqmlE0NE3eVOhwxNini2Pk9qO1FhGax4VrV6QPv0na
pD5Ua+8SdB2/Lf8nT1nDyDgSqatYfAeKNKdiuARBpaAk31luA1P8uzT75RE/2d3fLlX0rafiX9/U
89GTgEdLwTELIxPqs3yXGc46YdcScFe1J+5VXKMrUEEsAsE+WyTpCm7DF/26k8XoxvEKOCnnd7KI
T6kSCtDbEFQmk/Xo346V+zNM8lfdHLN3PkSaAVZwmny01Sjoc4GDXQe/5KUX3m7t6gCMpOAeItk6
0rxvl+r/vDctxvVF/UqiseMRYA1JnnTStRLytxfCCNuq0UPcZK1Cwgg2IdFSvZ4egkcIfihjplN0
C3h2imJqyF8O3mSV0me6znB8AamPqK3P0E96uwUsSXkqFAjpMuynH5P3oxcQuFbYBuREBjU2gSm3
B0Ti0mjXDBLdRCnYDlltRzi2MkpPj7MKgKVa6bbwRcZlxQMnceAc49Y9C3TZlTnPOeQiWsTy3XMY
z8xsAmjktQime8kfMdbOxC1Yt/v4e5JcNp7uY+bh0rFErDx88yS33N6NgxZ3BRfYjY1O+24c5rWy
fBRrY5uZMhQRhrkITeMxeTYwEyMqQ8+FTHSlQDMJ4xOYxK3myop/l8WZUURdGPrIOInJwyEx8kzh
dSrazSN3u04ECiEjxcRD4fOK/DLIgxKFsKAb7VqmNxpI3E4DJ8DL64xiUxxGYOXiwg7VZSf2oi2f
sNwTmWSU5me3sb+j0388waU90QBrZ8pNTv+/+ytPvXQUREfe9x7N4iKy2vHTeTDN4ocvfm3XVR1b
mHVykgJOgbj8aOU9BOsvprUZ5uO7lISeew24UmYuURUMFGHaZMvQOO2ruqB9mlwDmWrOkwENpPWE
fwbaeLuF2jXRS6QDaKnTsqEvzj1n3k4cw2nbzTkJRnKxIVishKhsutH11mlamMh/QW39G1tjyZHB
WqXjobxEbv/w8FpdCLGufPPWtK1/499I4TkJk7ABUFuVF/ZYdrGgZ2bwIZNF4Uq6BZUR+qx9EsZM
yI4nbo9JqBf9y5BsSd5Kdwxl4/jUmeZ1NL9RS9elY8t+bg+W+ThtHMN0lYTbDLI6K3BeHWBmiqPg
yRd2iIcnx4MDBcbhXfur2Zudf5Kr/BTFIk93ynEtUMfuFJ/jdVakw2cjnRY6rUARw/23ikkVvdHJ
xb3qaDBZAddNkAJYMcrPTjDOgzPedoqFsCg2Pr/pM36HHVZ5yYgWEDY1gJHW4c6rzWvdM8z22Kip
9G+wIcfIRaEboZLLpQmbZG2XhwPaeRpaoaVrsBWKkx23Arko15yEuC28w7ZdB49JCyKAKnejMkbt
ovm3Ibwc8VuVPXAvn2RT1YeTnNWVuMc+XfCLiDW9TyYSsJ6rCBo/ypeQAvWRv1W8l1MXghtQFJ8Q
n3QDw+rgpHxTRhf3b79RyAtgKJy7BTShz4MnU6meeBcPtGcL41kB45BXOxU+7qcoEBo9ysMmwYvv
ESc2Ok/LEWK4aDJMw1+UcIxV39JMOVNNmE+ITq6dTfwWpEuTZ63Rhg7EvxFf9F+ddj6kfOxGiHM8
2aSS1tgfSZ7Xa19EwemSo3fK55DpTb9CSVC8KqqrHn1tjj8fGoqEzlXrg5HTsNn1gtjoG47V9s+s
md6F9y07YeAPJEkqeOQwAQynNuKMbWSmCYks9WAPuCRE49zNIGc/mU1MsncjzobJ1vsw9RpcLsH7
GGKbJqSaFksZZz7Ao9WK4uni3k70zu6aIGwxIznsDMLzQA77ewT9NfC0GmvEwz7PfE0e7YsZH6k4
numGM3e6Xpvk9ZQw1xiQXfrupMdyBPIumvzM+lt1fyfxXVAv/Z4/CWWrfyGJTDaepr+KM30mfJxu
jm/2L2Pdfw9OSwx6yAbZF/EtpjCDs8nhz7IVNorPG9l84SZTCI0a2f0ek2VsPQOu/2wktjYDs+pd
ljMdwOb+R1QxO7Bk+SG9ZTfu/8ANLUa5KGKWAdQRwoKYcF2lxfHLhB2WX9cuLiate/AJLlKWo3jm
RLunbFPpCJvcHyjBViFejjQ7JZb6FpaQXorK9/SbiZuRn9Kjvs0u4O4TGdaxfAd36gjFdDaSrW4l
oQ3vkd6zRecpbHXyszN1fq16yA8o6Hb58KmAsTjjIrFzSfpw3FH652lD2Y7YKc6Sq2C2PHdjqTJ2
Luc/JuyazFit09HaT2n/0h//Gf0mxTsjtLC0/3/nf8hgMnLjg0xfyoLDfxT0WF/yWhuyT3w16H1O
4jFIKujJWq5fbA/Oj3SrEiz1sYvRBDzo1Gf2TokmgTBuUZ19nXJckv3T+hF1lOU6yz1uO3lqi4AJ
/xRF7HiaF/1wRaxAjGiW5oDcfTcQiVQYbjDkEVsjhwuFjxCLVgCZLJ0tAOBoXki3zvxuAXWvpeCi
4l2D1KKfv106RmQ/n4b3fh4YjZsN9xM+znuZY3Bmnx1TcgMGNimLRjMEKlDei/AEFi6sPdjC+K2z
G+BqxV4kby0++XRpNKl0KfayYp+uD3pC9ZdL7JjB9B0cLOJnePUE2jb8bosiWvnylSYHr3WXiZ+u
Bqf3nU41xcY4+RgleaH4dOyhtlbQfZd4lqpO/j0gyzIRZpuiCN7R1wQbgVFxXV0zx3r0bxMPlgeL
04GaR9UAuny6f8/luAqMyGqqmOSzllP/8Y2dxy1XANJw/BJPwIFNDK2wf7yl4MikO3DmxuEcyVJN
SuBWVBPFT2YH6h4ilxN3/5TL6G2tmadVuuD3vjTPKRVQD4q30wd5+tCcrG4ZFY+KtyQJxpMsq17V
r7Pmo90PxzlvChwYRAdkKjrRp89DTzDEFa/V7i2NPou8PGNt1O2NsC6fa6OfeNMxSDUfF3oT0F2K
5ycTpAX+r9fYJba+GcDGvO/bPPNEwmULJQG+L1QgwqZGtatKUAJmA336c9N2WYkF1+ZX/rvxwh6H
9o1EV6pldETp/bsadnlQFUTjPCHvOXqKgPp0XKi8tB3Ux5UHhU4gg55aGl22ghNL7kGqqjEICMMN
A3ihQOu2d5aCrSRXOCmf5LBsRh4N/IWpYXvTPL80ti9WaUG8BmU9jozhwgWfb4Do2ZFjAgLyzPcK
MfiMBGVYmyCm7z5AR0DF8VrJDxBV9KOtPXqmuHoC6sgxQBNUHxbAvrr5clTfjULKHzsSmIn1BmB9
in6S62MSFsd0o2bOzo7VJRRpKmggVOvCZLWGREdCcqrp+2KJl90jYMeSuZpWaYpDWQYOQaAqL5up
hAvYR30sFg1phhAaP6y0POZ5hQFDRYGKkio+lv05SOGgEqOn6IGHklbdWg3MYHcQSp9H7ahqENgL
NRb74pm53GYQPN4vhRJRrLLnoXtLmGSzMW6L0Oa8o5QeIKkIu67IUapy/YHYw5CICnRFjD8R0PJI
hXr7pp3RGq16j/RClGFte5rg1k2MZjmyKewHWJtdLKfXxODZBH2/XVZMJ546Ewums6PmkMHP6Chm
RxI/cGxj56ui02rmDVDk0X5RF+wsh62KhuZPGc3LoUe7Zd2N3PUlW4YpXu5igdDpS2lcZIuYtp6K
HfdDo/jZEbyfRUMAI/VJgQTJ5zqh8LoyQwl4MUAG65uCyvTX+UnoEUzTUOREvjaNi8l5Q3f2kN1i
2KcFcukeXdvyfdQv7c9BC83pxc6r79gyBB87I3pplE05cMZ7w1ZjC3qTYdWPpKsxxozvXZIp/iq7
i8wCK7dnMuRRgrRkBZlTmh5B9tHDz0OwVMopOhzLwbM2vVtIdJykDFzXNECi55na6th18xNBDeiK
EgsaKMQ4YS0f2wKz1ZlV4mWhgMicXf9a+5z8oG4Vi6n2xrnI0E5/WBOE2jKE5dbHJbJyJeOqX36T
4KwY47bXMFI2hJbUmNUdfJYSYX82twb+V48zcfRiXTCmvFkPhXITisthxoCvk36ACuti3NWqWZe5
fQw4F5Fakv08usCJNkdij7JhnGfVXBiTaenOTj3zlxPkPRFLe81CpdNCWc+YsBpRIlcaCSEpdzof
hTfgR0KWvd0W3lZWC3W5p+pjUFqSfxh8Ta9whxwGMEK7o3dl9e0VtrJzGlTxoMbNMtYRs9CSMAej
yFrK7cLyZo7/kmApladFq/GuI6u2VBu34PoCHxzzhO4a767Z88tDOrXbGnHlpfrtAsK+7JZWkYyA
Ygi1qE48IdbMPwj9yVNWtL2pyJJhroaJWhwbBF6R0nhHh/dP+L1WbZDfHGsKchYfPeqQAxGXdI33
yZX//yqgXod0I7laDL3BQrkEcfnFon/wtvcPUcIFcG6ZMSTQiZ5c11qioEhoEd56RCifO2FMLCHI
fTrec1HBSe+vGLwRMEMzqAJ0wcSACuNEX3+ChkD/AKRsCjsflsNy2rUaEOXY/K7sW3oZpY+iLq5B
v3m6JZPAUXG9ZCtkAiObvukjEQ5IbH9BohmKObmTGr+tBQH82S07tvikkKvbJIabxBYFe3i6jyrQ
ItpSWXtmnRLV8IHLbg5Tav7iC5liKb/v+Qvve2uMjf3sg7iCqqpYebENZ381XX4zf2ytsRNz9DJ2
YW+Xcx68cYEHLtpWq0DAxGaTW00UQmoEdzf7VBa/Nk6KG+Y1MNgbhtkt+2myXe36S5bUGgQv1gUB
iDUd6iKr6ZgP87f1AHZXnvs2q36z/a+F0nMWJkd7BXshTKVUu6c4sGc7lW1cVLLDtt6Fjeg+cPZt
7aSr2TqRKRaAVnXKELeXUx7BSIIzYMpKtleFzEGPAr83/SRQdis69jkfXDNrz2xW4IQD/SntGmpE
UWDt0s8ypwJL1bmOut0mgwinMT3of21qQ5ekSml5JiiO528smmPI4RAmZuqOY+Hh9IMYGXZ7Yo6Y
wKTTEfzg4t5V09HQqAVltu+gULfVjCRiuEWeDnLNvMXMDpTpfemrnaMwImbBQmPlnyz7YpivlOfu
t5YtO1WftBZzMHlvyJP3UziAaQUjg3xy5yrAmdo63zEt7d/m4aIpoSnTt3Wh/bAZOeh7IUxA5n5c
Hf7apSm0j2vyQlxhB3OGNzyt49yu6JaT8b/7vAzLITqu4Wn9qhRhHsTlDoeuiENgK1U6EH/cXpx1
oHVzh9vq0dTN99rij/O3xPvQW0i1vskgXeE0DSeVj94vbauVmwA3s4vy59sJz9yxS+xpGxqe5Vb4
UdowT/XgAXnBf/7bcg6TTyiChq0WBcsh3GxCpjp52P7al1zdu2ntp9swa9CwS17F9q/stdGkyB3k
/rx/zBB5ZCcdDbVeTEoFj/tLSfezq2OCb8roLriY0r15lkbvHNRLfgINM02GgPwLa0u1eel430Y1
gvcFSbcELCDMqy2a0eP87r/s7PRpdnes1EPtVvwhU1hX6tq8eGCpLp/RqIzT8BZ/03m4HXMri4bJ
nggdtV6woDeuCozBQJFMT41uyX0n1fUHItfX7MG+W8wr7TX8afqQU3C6cc3GLEGieoYVAMqiACLl
ZWLDJFaobzPu27r/NsS90anIXgmyYJXLZh9BF67TPobQnsjxN8fxJPkbQE4KDmxgWO+GVeLSJg0b
Iq3yptTcWL79rPoD8k5YaXdiBCnLwKJ/gBhgYDW1E6CfWsK+LN/o7d7/RC1LGAkEIC+Yyv5Mxc3K
bJ/DOP04Tl3tPuc8+NkOo7dJBMyX0hJPGwtnPHJHa3qaLvbEm3w9rn7YjKAEYYf9jpDBiAwyKBOb
5LkePp+2gVxlZsB67NSRUzo/bH5CgrXP8YyTmqCOKW2trpa9kw9CM1pe94NUIA/aChqA5ueeveqY
VDazyDWahxMBlt6qWxbYA6pBOCDLxauyfyjdMZkkGLJOWEN1CLx6tk9fWieg72F7ag7RndWJMNGH
dj0PyAt+aztsa4pt8ik34GmyqNQ/KDUDRqdw/WyW6y41LN8/IYgyYpbvtwwYtG8j6UidUykL3JWT
macxZSDqZha6TCpjjVaWjmc7CL7XKtSteAOKq3tI/BdUhp/3Zpa4Khncf99PPUwoEdcQob5Xe69c
6UbnuBiqWj54iMCkO9WGdKUPV76j28/93/utDY2QA0ef6R2z12Zi4jaUran3FWzbzczJIgmEIkbQ
AIO71h3IfqCa8jYZ844/5sVYM7QsC+e4nKpPjwWy7HIe7rGLw6+fO0MOzH04m17OvvtS5BVJeXD3
S9ND0HNMgoYzBZvoxPjiidYy+sXpSjzbhm/Hb2c7g17get/9c+vwVJfeXoWH//s4wlaqoh96kfTg
/uAAaT5tIAfLddb1TyIYmX6Lz/mfyfPthhhVF1+27bDOL4JXC/U51qxOmR8YcenQ7EOxAw351Ctz
0wonN9Lrh0pfMwcNtkq4o5ZmOo9pIZBhBunSeh4P/LjdhbTAbRodC71LJgZeDcjLK9EtkdSdeRNG
BK67QHcmFi4xEVPps2+IJ4Llx0zkDhPScrDFjs5b4ZVQ8uNsrJhthgdoRfS7X+XUHVp5V6Zeakbp
nceIgdFgij3CCyFzoWpN22mogNvBlsUwK85E4zA79pM4Fb24RhVb26iOmGDU56ILtFLk5tRsOreR
gbLgB1WDuyZ+JPHw1Nitm1UVyZtmYosV6hT/LtEpmwALS52HHngFe1ZnpMF6sAW5sNK5lSQEXVtg
JjPXnfyR+jml5yLmAo8oSE3jqY84o0WFDyr+LjcGQePGntMwsHOY/horqlircIOcIjJQJHBvm6FG
Lz5cbNLen84pvfg7AnprIezIUI4K7Wx5vFwNw7waKAGethajuoYAupnGZKsXFWi+OCCcyoUDclg/
6NmXTkVAfyUhHoGhidvMdPFeHgnN9mz7QnLeK8zHvNCXSsa2Eoq93Iv/nL4eoK1pVeBXmAor1Jaj
qEuZCZP0+4swp77hm5akLBu2iRU1qotaLT9b7NjDS9vlLEaa66+gdnIFUMHsDJWk
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
