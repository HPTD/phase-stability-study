// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Sat Jul 22 01:58:15 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/eorzes/cernbox/git/rx_vcu118_firefly_pi_phy_hop/prj/m_top/m_top.gen/sources_1/ip/gig_eth_pcs_pma_gmii_to_sgmii_bridge/gig_eth_pcs_pma_gmii_to_sgmii_bridge_sim_netlist.v
// Design      : gig_eth_pcs_pma_gmii_to_sgmii_bridge
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu9p-flga2104-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* DowngradeIPIdentifiedWarnings = "yes" *) (* EXAMPLE_SIMULATION = "0" *) 
(* NotValidForBitStream *)
module gig_eth_pcs_pma_gmii_to_sgmii_bridge
   (txp_0,
    txn_0,
    rxp_0,
    rxn_0,
    signal_detect_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_isolate_0,
    sgmii_clk_r_0,
    sgmii_clk_f_0,
    sgmii_clk_en_0,
    speed_is_10_100_0,
    speed_is_100_0,
    an_interrupt_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    status_vector_0,
    configuration_vector_0,
    refclk625_p,
    refclk625_n,
    clk125_out,
    clk312_out,
    rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    rx_locked,
    tx_locked,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    riu_rddata_3,
    riu_valid_3,
    riu_prsnt_3,
    riu_rddata_2,
    riu_valid_2,
    riu_prsnt_2,
    riu_rddata_1,
    riu_valid_1,
    riu_prsnt_1,
    rx_btval_3,
    rx_btval_2,
    rx_btval_1,
    tx_dly_rdy_1,
    rx_dly_rdy_1,
    rx_vtc_rdy_1,
    tx_vtc_rdy_1,
    tx_dly_rdy_2,
    rx_dly_rdy_2,
    rx_vtc_rdy_2,
    tx_vtc_rdy_2,
    tx_dly_rdy_3,
    rx_dly_rdy_3,
    rx_vtc_rdy_3,
    tx_vtc_rdy_3,
    tx_pll_clk_out,
    rx_pll_clk_out,
    tx_rdclk_out,
    reset);
  output txp_0;
  output txn_0;
  input rxp_0;
  input rxn_0;
  input signal_detect_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output gmii_isolate_0;
  output sgmii_clk_r_0;
  output sgmii_clk_f_0;
  output sgmii_clk_en_0;
  input speed_is_10_100_0;
  input speed_is_100_0;
  output an_interrupt_0;
  input [15:0]an_adv_config_vector_0;
  input an_restart_config_0;
  output [15:0]status_vector_0;
  input [4:0]configuration_vector_0;
  input refclk625_p;
  input refclk625_n;
  output clk125_out;
  output clk312_out;
  output rst_125_out;
  output tx_logic_reset;
  output rx_logic_reset;
  output rx_locked;
  output tx_locked;
  output tx_bsc_rst_out;
  output rx_bsc_rst_out;
  output tx_bs_rst_out;
  output rx_bs_rst_out;
  output tx_rst_dly_out;
  output rx_rst_dly_out;
  output tx_bsc_en_vtc_out;
  output rx_bsc_en_vtc_out;
  output tx_bs_en_vtc_out;
  output rx_bs_en_vtc_out;
  output riu_clk_out;
  output [5:0]riu_addr_out;
  output [15:0]riu_wr_data_out;
  output riu_wr_en_out;
  output [1:0]riu_nibble_sel_out;
  input [15:0]riu_rddata_3;
  input riu_valid_3;
  input riu_prsnt_3;
  input [15:0]riu_rddata_2;
  input riu_valid_2;
  input riu_prsnt_2;
  input [15:0]riu_rddata_1;
  input riu_valid_1;
  input riu_prsnt_1;
  output [8:0]rx_btval_3;
  output [8:0]rx_btval_2;
  output [8:0]rx_btval_1;
  input tx_dly_rdy_1;
  input rx_dly_rdy_1;
  input rx_vtc_rdy_1;
  input tx_vtc_rdy_1;
  input tx_dly_rdy_2;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_2;
  input tx_vtc_rdy_2;
  input tx_dly_rdy_3;
  input rx_dly_rdy_3;
  input rx_vtc_rdy_3;
  input tx_vtc_rdy_3;
  output tx_pll_clk_out;
  output rx_pll_clk_out;
  output tx_rdclk_out;
  input reset;

  wire \<const0> ;
  wire [15:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire clk125_out;
  wire clk312_out;
  wire [4:0]configuration_vector_0;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_er_0;
  wire [7:0]gmii_txd_0;
  (* IBUF_LOW_PWR = 0 *) wire refclk625_n;
  (* IBUF_LOW_PWR = 0 *) wire refclk625_p;
  wire reset;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt_1;
  wire riu_prsnt_2;
  wire riu_prsnt_3;
  wire [15:0]riu_rddata_1;
  wire [15:0]riu_rddata_2;
  wire [15:0]riu_rddata_3;
  wire riu_valid_1;
  wire riu_valid_2;
  wire riu_valid_3;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rst_125_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire [8:0]rx_btval_1;
  wire [8:0]rx_btval_2;
  wire [8:0]rx_btval_3;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire rx_locked;
  wire rx_logic_reset;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  (* IBUF_LOW_PWR = 0 *) wire rxn_0;
  (* IBUF_LOW_PWR = 0 *) wire rxp_0;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [13:0]\^status_vector_0 ;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_locked;
  wire tx_logic_reset;
  wire tx_pll_clk_out;
  wire tx_rdclk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  (* SLEW = "SLOW" *) wire txn_0;
  (* SLEW = "SLOW" *) wire txp_0;
  wire [15:8]NLW_inst_status_vector_0_UNCONNECTED;

  assign status_vector_0[15] = \<const0> ;
  assign status_vector_0[14] = \<const0> ;
  assign status_vector_0[13:9] = \^status_vector_0 [13:9];
  assign status_vector_0[8] = \<const0> ;
  assign status_vector_0[7:0] = \^status_vector_0 [7:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* X_CORE_INFO = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_support inst
       (.an_adv_config_vector_0({1'b0,1'b0,1'b0,1'b0,an_adv_config_vector_0[11],1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_interrupt_0(an_interrupt_0),
        .an_restart_config_0(an_restart_config_0),
        .clk125_out(clk125_out),
        .clk312_out(clk312_out),
        .configuration_vector_0(configuration_vector_0),
        .gmii_isolate_0(gmii_isolate_0),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_txd_0(gmii_txd_0),
        .refclk625_n(refclk625_n),
        .refclk625_p(refclk625_p),
        .reset(reset),
        .riu_addr_out(riu_addr_out),
        .riu_clk_out(riu_clk_out),
        .riu_nibble_sel_out(riu_nibble_sel_out),
        .riu_prsnt_1(riu_prsnt_1),
        .riu_prsnt_2(riu_prsnt_2),
        .riu_prsnt_3(riu_prsnt_3),
        .riu_rddata_1(riu_rddata_1),
        .riu_rddata_2(riu_rddata_2),
        .riu_rddata_3(riu_rddata_3),
        .riu_valid_1(riu_valid_1),
        .riu_valid_2(riu_valid_2),
        .riu_valid_3(riu_valid_3),
        .riu_wr_data_out(riu_wr_data_out),
        .riu_wr_en_out(riu_wr_en_out),
        .rst_125_out(rst_125_out),
        .rx_bs_en_vtc_out(rx_bs_en_vtc_out),
        .rx_bs_rst_out(rx_bs_rst_out),
        .rx_bsc_en_vtc_out(rx_bsc_en_vtc_out),
        .rx_bsc_rst_out(rx_bsc_rst_out),
        .rx_btval_1(rx_btval_1),
        .rx_btval_2(rx_btval_2),
        .rx_btval_3(rx_btval_3),
        .rx_dly_rdy_1(rx_dly_rdy_1),
        .rx_dly_rdy_2(rx_dly_rdy_2),
        .rx_dly_rdy_3(rx_dly_rdy_3),
        .rx_locked(rx_locked),
        .rx_logic_reset(rx_logic_reset),
        .rx_pll_clk_out(rx_pll_clk_out),
        .rx_rst_dly_out(rx_rst_dly_out),
        .rx_vtc_rdy_1(rx_vtc_rdy_1),
        .rx_vtc_rdy_2(rx_vtc_rdy_2),
        .rx_vtc_rdy_3(rx_vtc_rdy_3),
        .rxn_0(rxn_0),
        .rxp_0(rxp_0),
        .sgmii_clk_en_0(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .signal_detect_0(signal_detect_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0),
        .status_vector_0({NLW_inst_status_vector_0_UNCONNECTED[15:14],\^status_vector_0 }),
        .tx_bs_en_vtc_out(tx_bs_en_vtc_out),
        .tx_bs_rst_out(tx_bs_rst_out),
        .tx_bsc_en_vtc_out(tx_bsc_en_vtc_out),
        .tx_bsc_rst_out(tx_bsc_rst_out),
        .tx_dly_rdy_1(tx_dly_rdy_1),
        .tx_dly_rdy_2(tx_dly_rdy_2),
        .tx_dly_rdy_3(tx_dly_rdy_3),
        .tx_locked(tx_locked),
        .tx_logic_reset(tx_logic_reset),
        .tx_pll_clk_out(tx_pll_clk_out),
        .tx_rdclk_out(tx_rdclk_out),
        .tx_rst_dly_out(tx_rst_dly_out),
        .tx_vtc_rdy_1(tx_vtc_rdy_1),
        .tx_vtc_rdy_2(tx_vtc_rdy_2),
        .tx_vtc_rdy_3(tx_vtc_rdy_3),
        .txn_0(txn_0),
        .txp_0(txp_0));
endmodule

(* C_BytePosition = "0" *) (* C_IoBank = "44" *) (* C_Part = "XCKU060" *) 
(* C_Rx_BtslcNulType = "SERIAL" *) (* C_Rx_Data_Width = "4" *) (* C_Rx_Delay_Format = "COUNT" *) 
(* C_Rx_Delay_Type = "VAR_LOAD" *) (* C_Rx_Delay_Value = "0" *) (* C_Rx_RefClk_Frequency = "312.500000" *) 
(* C_Rx_Self_Calibrate = "ENABLE" *) (* C_Rx_Serial_Mode = "TRUE" *) (* C_Rx_UsedBitslices = "7'b0000011" *) 
(* C_TxInUpperNibble = "0" *) (* C_Tx_BtslceTr = "T" *) (* C_Tx_BtslceUsedAsT = "7'b0000000" *) 
(* C_Tx_Data_Width = "8" *) (* C_Tx_Delay_Format = "TIME" *) (* C_Tx_Delay_Type = "FIXED" *) 
(* C_Tx_Delay_Value = "0" *) (* C_Tx_RefClk_Frequency = "1250.000000" *) (* C_Tx_Self_Calibrate = "ENABLE" *) 
(* C_Tx_Serial_Mode = "FALSE" *) (* C_Tx_UsedBitslices = "7'b0010000" *) (* C_UseRxRiu = "1" *) 
(* C_UseTxRiu = "1" *) (* dont_touch = "true" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_BaseX_Byte
   (BaseX_Tx_Bsc_Rst,
    BaseX_Rx_Bsc_Rst,
    BaseX_Tx_Bs_Rst,
    BaseX_Rx_Bs_Rst,
    BaseX_Tx_Rst_Dly,
    BaseX_Rx_Rst_Dly,
    BaseX_Tx_Bsc_En_Vtc,
    BaseX_Rx_Bsc_En_Vtc,
    BaseX_Tx_Bs_En_Vtc,
    BaseX_Rx_Bs_En_Vtc,
    BaseX_Riu_Clk,
    BaseX_Riu_Addr,
    BaseX_Riu_Wr_Data,
    BaseX_Riu_Rd_Data,
    BaseX_Riu_Valid,
    BaseX_Riu_Prsnt,
    BaseX_Riu_Wr_En,
    BaseX_Riu_Nibble_Sel,
    BaseX_Tx_Pll_Clk,
    BaseX_Rx_Pll_Clk,
    BaseX_Tx_Dly_Rdy,
    BaseX_Rx_Dly_Rdy,
    BaseX_Tx_Vtc_Rdy,
    BaseX_Rx_Vtc_Rdy,
    BaseX_Tx_Phy_Rden,
    BaseX_Rx_Phy_Rden,
    BaseX_Rx_Fifo_Rd_Clk,
    BaseX_Rx_Fifo_Rd_En,
    BaseX_Rx_Fifo_Empty,
    BaseX_Dly_Clk,
    BaseX_Idly_Ce,
    BaseX_Idly_Inc,
    BaseX_Idly_Load,
    BaseX_Idly_CntValueIn,
    BaseX_Idly_CntValueOut,
    BaseX_Odly_Ce,
    BaseX_Odly_Inc,
    BaseX_Odly_Load,
    BaseX_Odly_CntValueIn,
    BaseX_Odly_CntValueOut,
    BaseX_TriOdly_Ce,
    BaseX_TriOdly_Inc,
    BaseX_TriOdly_Load,
    BaseX_TriOdly_CntValueIn,
    BaseX_TriOdly_CntValueOut,
    BaseX_Tx_TbyteIn,
    BaseX_Tx_T_In,
    BaseX_Tx_D_In,
    BaseX_Rx_Q_Out,
    BaseX_Rx_Q_CombOut,
    BaseX_Tx_Tri_Out,
    BaseX_Tx_Data_Out,
    BaseX_Rx_Data_In,
    Tx_RdClk);
  input BaseX_Tx_Bsc_Rst;
  input BaseX_Rx_Bsc_Rst;
  input BaseX_Tx_Bs_Rst;
  input BaseX_Rx_Bs_Rst;
  input BaseX_Tx_Rst_Dly;
  input BaseX_Rx_Rst_Dly;
  input BaseX_Tx_Bsc_En_Vtc;
  input BaseX_Rx_Bsc_En_Vtc;
  input BaseX_Tx_Bs_En_Vtc;
  input BaseX_Rx_Bs_En_Vtc;
  input BaseX_Riu_Clk;
  input [5:0]BaseX_Riu_Addr;
  input [15:0]BaseX_Riu_Wr_Data;
  output [15:0]BaseX_Riu_Rd_Data;
  output BaseX_Riu_Valid;
  output BaseX_Riu_Prsnt;
  input BaseX_Riu_Wr_En;
  input [1:0]BaseX_Riu_Nibble_Sel;
  input BaseX_Tx_Pll_Clk;
  input BaseX_Rx_Pll_Clk;
  output BaseX_Tx_Dly_Rdy;
  output BaseX_Rx_Dly_Rdy;
  output BaseX_Tx_Vtc_Rdy;
  output BaseX_Rx_Vtc_Rdy;
  input [3:0]BaseX_Tx_Phy_Rden;
  input [3:0]BaseX_Rx_Phy_Rden;
  input BaseX_Rx_Fifo_Rd_Clk;
  input [6:0]BaseX_Rx_Fifo_Rd_En;
  output [6:0]BaseX_Rx_Fifo_Empty;
  input BaseX_Dly_Clk;
  input [6:0]BaseX_Idly_Ce;
  input [6:0]BaseX_Idly_Inc;
  input [6:0]BaseX_Idly_Load;
  input [62:0]BaseX_Idly_CntValueIn;
  output [62:0]BaseX_Idly_CntValueOut;
  input [5:0]BaseX_Odly_Ce;
  input [5:0]BaseX_Odly_Inc;
  input [5:0]BaseX_Odly_Load;
  input [53:0]BaseX_Odly_CntValueIn;
  output [53:0]BaseX_Odly_CntValueOut;
  input BaseX_TriOdly_Ce;
  input BaseX_TriOdly_Inc;
  input BaseX_TriOdly_Load;
  input [8:0]BaseX_TriOdly_CntValueIn;
  output [8:0]BaseX_TriOdly_CntValueOut;
  input [3:0]BaseX_Tx_TbyteIn;
  input [5:0]BaseX_Tx_T_In;
  input [47:0]BaseX_Tx_D_In;
  output [27:0]BaseX_Rx_Q_Out;
  output [6:0]BaseX_Rx_Q_CombOut;
  output [5:0]BaseX_Tx_Tri_Out;
  output [5:0]BaseX_Tx_Data_Out;
  input [6:0]BaseX_Rx_Data_In;
  input Tx_RdClk;

  wire BaseX_Dly_Clk;
  wire [6:0]BaseX_Idly_Ce;
  wire [62:0]BaseX_Idly_CntValueIn;
  wire [62:0]BaseX_Idly_CntValueOut;
  wire [6:0]BaseX_Idly_Inc;
  wire [6:0]BaseX_Idly_Load;
  wire [5:0]BaseX_Odly_Ce;
  wire [53:0]BaseX_Odly_CntValueIn;
  wire [53:0]BaseX_Odly_CntValueOut;
  wire [5:0]BaseX_Odly_Inc;
  wire [5:0]BaseX_Odly_Load;
  wire [5:0]BaseX_Riu_Addr;
  wire BaseX_Riu_Clk;
  wire [1:0]BaseX_Riu_Nibble_Sel;
  wire BaseX_Riu_Prsnt;
  wire [15:0]BaseX_Riu_Rd_Data;
  wire BaseX_Riu_Valid;
  wire [15:0]BaseX_Riu_Wr_Data;
  wire BaseX_Riu_Wr_En;
  wire BaseX_Rx_Bs_En_Vtc;
  wire BaseX_Rx_Bs_Rst;
  wire BaseX_Rx_Bsc_En_Vtc;
  wire BaseX_Rx_Bsc_Rst;
  wire [6:0]BaseX_Rx_Data_In;
  wire BaseX_Rx_Dly_Rdy;
  wire [6:0]BaseX_Rx_Fifo_Empty;
  wire BaseX_Rx_Fifo_Rd_Clk;
  wire [6:0]BaseX_Rx_Fifo_Rd_En;
  wire [3:0]BaseX_Rx_Phy_Rden;
  wire BaseX_Rx_Pll_Clk;
  wire [6:0]BaseX_Rx_Q_CombOut;
  wire [27:0]BaseX_Rx_Q_Out;
  wire BaseX_Rx_Rst_Dly;
  wire BaseX_Rx_Vtc_Rdy;
  wire BaseX_TriOdly_Ce;
  wire [8:0]BaseX_TriOdly_CntValueIn;
  wire [8:0]BaseX_TriOdly_CntValueOut;
  wire BaseX_TriOdly_Inc;
  wire BaseX_TriOdly_Load;
  wire BaseX_Tx_Bs_En_Vtc;
  wire BaseX_Tx_Bs_Rst;
  wire BaseX_Tx_Bsc_En_Vtc;
  wire BaseX_Tx_Bsc_Rst;
  wire [47:0]BaseX_Tx_D_In;
  wire [5:0]BaseX_Tx_Data_Out;
  wire BaseX_Tx_Dly_Rdy;
  wire [3:0]BaseX_Tx_Phy_Rden;
  wire BaseX_Tx_Pll_Clk;
  wire BaseX_Tx_Rst_Dly;
  wire [5:0]BaseX_Tx_T_In;
  wire [5:0]BaseX_Tx_Tri_Out;
  wire BaseX_Tx_Vtc_Rdy;
  (* async_reg = "true" *) wire [1:0]IntActTx_TByteinPip;
  wire [15:0]RIU_RD_DATA_LOW;
  wire [15:0]RIU_RD_DATA_UPP;
  wire RIU_RD_VALID_LOW;
  wire RIU_RD_VALID_UPP;
  wire Tx_RdClk;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Fifo_Wrclk_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_North_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_South_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Nclk_Nibble_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Rx_Nibble_Rx_Pclk_Nibble_Out_UNCONNECTED;
  wire [62:0]NLW_BaseX_Byte_I_Rx_Nibble_Rx_CntValueOut_Ext_UNCONNECTED;
  wire [6:0]NLW_BaseX_Byte_I_Rx_Nibble_Rx_Dyn_Dci_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_North_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_South_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Nclk_Nibble_Out_UNCONNECTED;
  wire NLW_BaseX_Byte_I_Tx_Nibble_Tx_Pclk_Nibble_Out_UNCONNECTED;
  wire [6:0]NLW_BaseX_Byte_I_Tx_Nibble_Tx_Dyn_Dci_UNCONNECTED;

  (* C_BtslcNulType = "SERIAL" *) 
  (* C_BusRxBitCtrlIn = "40" *) 
  (* C_BusRxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlIn = "40" *) 
  (* C_BusTxBitCtrlInTri = "40" *) 
  (* C_BusTxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlOutTri = "40" *) 
  (* C_BytePosition = "0" *) 
  (* C_Cascade = "FALSE" *) 
  (* C_CntValue = "9" *) 
  (* C_Ctrl_Clk = "EXTERNAL" *) 
  (* C_Delay_Format = "COUNT" *) 
  (* C_Delay_Type = "VAR_LOAD" *) 
  (* C_Delay_Value = "0" *) 
  (* C_Delay_Value_Ext = "0" *) 
  (* C_Div_Mode = "DIV2" *) 
  (* C_En_Clk_To_Ext_North = "DISABLE" *) 
  (* C_En_Clk_To_Ext_South = "DISABLE" *) 
  (* C_En_Dyn_Odly_Mode = "FALSE" *) 
  (* C_En_Other_Nclk = "FALSE" *) 
  (* C_En_Other_Pclk = "FALSE" *) 
  (* C_Fifo_Sync_Mode = "FALSE" *) 
  (* C_Idly_Vt_Track = "TRUE" *) 
  (* C_Inv_Rxclk = "FALSE" *) 
  (* C_IoBank = "44" *) 
  (* C_Is_Clk_Ext_Inverted = "1'b0" *) 
  (* C_Is_Clk_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Ext_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
  (* C_Is_Rst_Inverted = "1'b0" *) 
  (* C_NibbleType = "7" *) 
  (* C_Odly_Vt_Track = "TRUE" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Qdly_Vt_Track = "TRUE" *) 
  (* C_Read_Idle_Count = "6'b000000" *) 
  (* C_RefClk_Frequency = "312.500000" *) 
  (* C_RefClk_Src = "PLLCLK" *) 
  (* C_Rounding_Factor = "16" *) 
  (* C_RxGate_Extend = "FALSE" *) 
  (* C_Rx_Clk_Phase_n = "SHIFT_90" *) 
  (* C_Rx_Clk_Phase_p = "SHIFT_90" *) 
  (* C_Rx_Data_Width = "4" *) 
  (* C_Rx_Gating = "DISABLE" *) 
  (* C_Self_Calibrate = "ENABLE" *) 
  (* C_Serial_Mode = "TRUE" *) 
  (* C_Tx_Gating = "DISABLE" *) 
  (* C_Update_Mode = "ASYNC" *) 
  (* C_Update_Mode_Ext = "ASYNC" *) 
  (* C_UsedBitslices = "7'b0000011" *) 
  (* KEEP_HIERARCHY = "true" *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_Rx_Nibble BaseX_Byte_I_Rx_Nibble
       (.Fifo_Empty(BaseX_Rx_Fifo_Empty),
        .Fifo_Rd_Clk({BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk,BaseX_Rx_Fifo_Rd_Clk}),
        .Fifo_Rd_En(BaseX_Rx_Fifo_Rd_En),
        .Fifo_Wrclk_Out(NLW_BaseX_Byte_I_Rx_Nibble_Fifo_Wrclk_Out_UNCONNECTED),
        .Rx_Bs_En_Vtc(BaseX_Rx_Bs_En_Vtc),
        .Rx_Bs_Rst(BaseX_Rx_Bs_Rst),
        .Rx_Bsc_En_Vtc(BaseX_Rx_Bsc_En_Vtc),
        .Rx_Bsc_Rst(BaseX_Rx_Bsc_Rst),
        .Rx_Ce(BaseX_Idly_Ce),
        .Rx_Ce_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Clk(BaseX_Dly_Clk),
        .Rx_Clk_From_Ext(1'b1),
        .Rx_Clk_To_Ext_North(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_North_UNCONNECTED),
        .Rx_Clk_To_Ext_South(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Clk_To_Ext_South_UNCONNECTED),
        .Rx_CntValueIn(BaseX_Idly_CntValueIn),
        .Rx_CntValueIn_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_CntValueOut(BaseX_Idly_CntValueOut),
        .Rx_CntValueOut_Ext(NLW_BaseX_Byte_I_Rx_Nibble_Rx_CntValueOut_Ext_UNCONNECTED[62:0]),
        .Rx_Data_In(BaseX_Rx_Data_In),
        .Rx_Dly_Rdy(BaseX_Rx_Dly_Rdy),
        .Rx_Dyn_Dci(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Dyn_Dci_UNCONNECTED[6:0]),
        .Rx_Inc(BaseX_Idly_Inc),
        .Rx_Inc_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Load(BaseX_Idly_Load),
        .Rx_Load_Ext({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .Rx_Nclk_Nibble_In(1'b1),
        .Rx_Nclk_Nibble_Out(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Nclk_Nibble_Out_UNCONNECTED),
        .Rx_Pclk_Nibble_In(1'b1),
        .Rx_Pclk_Nibble_Out(NLW_BaseX_Byte_I_Rx_Nibble_Rx_Pclk_Nibble_Out_UNCONNECTED),
        .Rx_Phy_Rden(BaseX_Rx_Phy_Rden),
        .Rx_Pll_Clk(BaseX_Rx_Pll_Clk),
        .Rx_Q_CombOut(BaseX_Rx_Q_CombOut),
        .Rx_Q_Out(BaseX_Rx_Q_Out),
        .Rx_RefClk(1'b0),
        .Rx_Riu_Addr(BaseX_Riu_Addr),
        .Rx_Riu_Clk(BaseX_Riu_Clk),
        .Rx_Riu_Nibble_Sel(BaseX_Riu_Nibble_Sel[0]),
        .Rx_Riu_Prsnt(BaseX_Riu_Prsnt),
        .Rx_Riu_Rd_Data(RIU_RD_DATA_UPP),
        .Rx_Riu_Valid(RIU_RD_VALID_UPP),
        .Rx_Riu_Wr_Data(BaseX_Riu_Wr_Data),
        .Rx_Riu_Wr_En(BaseX_Riu_Wr_En),
        .Rx_Rst_Dly(BaseX_Rx_Rst_Dly),
        .Rx_Tbyte_In({1'b0,1'b0,1'b0,1'b0}),
        .Rx_Vtc_Rdy(BaseX_Rx_Vtc_Rdy));
  (* C_BtslceUsedAsT = "7'b0000000" *) 
  (* C_BusRxBitCtrlIn = "40" *) 
  (* C_BusRxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlIn = "40" *) 
  (* C_BusTxBitCtrlInTri = "40" *) 
  (* C_BusTxBitCtrlOut = "40" *) 
  (* C_BusTxBitCtrlOutTri = "40" *) 
  (* C_BytePosition = "0" *) 
  (* C_CntValue = "9" *) 
  (* C_Ctrl_Clk = "EXTERNAL" *) 
  (* C_Data_Type = "DATA" *) 
  (* C_Delay_Format = "TIME" *) 
  (* C_Delay_Type = "FIXED" *) 
  (* C_Delay_Value = "0" *) 
  (* C_Div_Mode = "DIV4" *) 
  (* C_En_Clk_To_Ext_North = "DISABLE" *) 
  (* C_En_Clk_To_Ext_South = "DISABLE" *) 
  (* C_En_Dyn_Odly_Mode = "FALSE" *) 
  (* C_En_Other_Nclk = "FALSE" *) 
  (* C_En_Other_Pclk = "FALSE" *) 
  (* C_Enable_Pre_Emphasis = "FALSE" *) 
  (* C_Idly_Vt_Track = "FALSE" *) 
  (* C_Init = "1'b0" *) 
  (* C_Inv_Rxclk = "FALSE" *) 
  (* C_IoBank = "44" *) 
  (* C_Is_Clk_Inverted = "1'b0" *) 
  (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
  (* C_Is_Rst_Inverted = "1'b0" *) 
  (* C_Native_Odelay_Bypass = "FALSE" *) 
  (* C_NibbleType = "6" *) 
  (* C_Odly_Vt_Track = "FALSE" *) 
  (* C_Output_Phase_90 = "TRUE" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Qdly_Vt_Track = "FALSE" *) 
  (* C_Read_Idle_Count = "6'b000000" *) 
  (* C_RefClk_Frequency = "1250.000000" *) 
  (* C_RefClk_Src = "PLLCLK" *) 
  (* C_Rounding_Factor = "16" *) 
  (* C_RxGate_Extend = "FALSE" *) 
  (* C_Rx_Clk_Phase_n = "SHIFT_0" *) 
  (* C_Rx_Clk_Phase_p = "SHIFT_0" *) 
  (* C_Rx_Gating = "DISABLE" *) 
  (* C_Self_Calibrate = "ENABLE" *) 
  (* C_Serial_Mode = "FALSE" *) 
  (* C_Tx_BtslceTr = "T" *) 
  (* C_Tx_Data_Width = "8" *) 
  (* C_Tx_Gating = "ENABLE" *) 
  (* C_Update_Mode = "ASYNC" *) 
  (* C_UsedBitslices = "7'b0010000" *) 
  (* KEEP_HIERARCHY = "true" *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_Tx_Nibble BaseX_Byte_I_Tx_Nibble
       (.TxTri_Ce(BaseX_TriOdly_Ce),
        .TxTri_Clk(BaseX_Dly_Clk),
        .TxTri_CntValueIn(BaseX_TriOdly_CntValueIn),
        .TxTri_CntValueOut(BaseX_TriOdly_CntValueOut),
        .TxTri_Inc(BaseX_TriOdly_Inc),
        .TxTri_Load(BaseX_TriOdly_Load),
        .Tx_Bs_En_Vtc(BaseX_Tx_Bs_En_Vtc),
        .Tx_Bs_Rst(BaseX_Tx_Bs_Rst),
        .Tx_Bsc_En_Vtc(BaseX_Tx_Bsc_En_Vtc),
        .Tx_Bsc_Rst(BaseX_Tx_Bsc_Rst),
        .Tx_Ce(BaseX_Odly_Ce),
        .Tx_Clk(BaseX_Dly_Clk),
        .Tx_Clk_From_Ext(1'b1),
        .Tx_Clk_To_Ext_North(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_North_UNCONNECTED),
        .Tx_Clk_To_Ext_South(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Clk_To_Ext_South_UNCONNECTED),
        .Tx_CntValueIn(BaseX_Odly_CntValueIn),
        .Tx_CntValueOut(BaseX_Odly_CntValueOut),
        .Tx_D_In(BaseX_Tx_D_In),
        .Tx_Data_Out(BaseX_Tx_Data_Out),
        .Tx_Dly_Rdy(BaseX_Tx_Dly_Rdy),
        .Tx_Dyn_Dci(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Dyn_Dci_UNCONNECTED[6:0]),
        .Tx_Inc(BaseX_Odly_Inc),
        .Tx_Load(BaseX_Odly_Load),
        .Tx_Nclk_Nibble_In(1'b1),
        .Tx_Nclk_Nibble_Out(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Nclk_Nibble_Out_UNCONNECTED),
        .Tx_Pclk_Nibble_In(1'b1),
        .Tx_Pclk_Nibble_Out(NLW_BaseX_Byte_I_Tx_Nibble_Tx_Pclk_Nibble_Out_UNCONNECTED),
        .Tx_Phy_Rden(BaseX_Tx_Phy_Rden),
        .Tx_Pll_Clk(BaseX_Tx_Pll_Clk),
        .Tx_RefClk(1'b0),
        .Tx_Riu_Addr(BaseX_Riu_Addr),
        .Tx_Riu_Clk(BaseX_Riu_Clk),
        .Tx_Riu_Nibble_Sel(BaseX_Riu_Nibble_Sel[1]),
        .Tx_Riu_Rd_Data(RIU_RD_DATA_LOW),
        .Tx_Riu_Valid(RIU_RD_VALID_LOW),
        .Tx_Riu_Wr_Data(BaseX_Riu_Wr_Data),
        .Tx_Riu_Wr_En(BaseX_Riu_Wr_En),
        .Tx_Rst_Dly(BaseX_Tx_Rst_Dly),
        .Tx_T_In(BaseX_Tx_T_In),
        .Tx_Tbyte_In({IntActTx_TByteinPip[1],IntActTx_TByteinPip[1],IntActTx_TByteinPip[1],IntActTx_TByteinPip[1]}),
        .Tx_Tri_Out(BaseX_Tx_Tri_Out),
        .Tx_Vtc_Rdy(BaseX_Tx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  RIU_OR #(
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000)) 
    \Gen_0.BaseX_Byte_I_Riu_Or_TxLow 
       (.RIU_RD_DATA(BaseX_Riu_Rd_Data),
        .RIU_RD_DATA_LOW(RIU_RD_DATA_LOW),
        .RIU_RD_DATA_UPP(RIU_RD_DATA_UPP),
        .RIU_RD_VALID(BaseX_Riu_Valid),
        .RIU_RD_VALID_LOW(RIU_RD_VALID_LOW),
        .RIU_RD_VALID_UPP(RIU_RD_VALID_UPP));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE \IntActTx_TByteinPip_reg[0] 
       (.C(Tx_RdClk),
        .CE(1'b1),
        .CLR(BaseX_Tx_Bsc_Rst),
        .D(BaseX_Tx_Vtc_Rdy),
        .Q(IntActTx_TByteinPip[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE \IntActTx_TByteinPip_reg[1] 
       (.C(Tx_RdClk),
        .CE(1'b1),
        .CLR(BaseX_Tx_Bsc_Rst),
        .D(IntActTx_TByteinPip[0]),
        .Q(IntActTx_TByteinPip[1]));
endmodule

(* C_IoBank = "44" *) (* C_Part = "XCKU060" *) (* EXAMPLE_SIMULATION = "0" *) 
(* dont_touch = "yes" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_Clock_Reset
   (ClockIn_p,
    ClockIn_n,
    ClockIn_se_out,
    ResetIn,
    Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Tx_Bsc_EnVtc,
    Tx_Bs_EnVtc,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    Rx_Bsc_EnVtc,
    Rx_Bs_EnVtc,
    Tx_SysClk,
    Tx_WrClk,
    Tx_ClkOutPhy,
    Rx_SysClk,
    Rx_RiuClk,
    Rx_ClkOutPhy,
    Tx_Locked,
    Tx_Bs_RstDly,
    Tx_Bs_Rst,
    Tx_Bsc_Rst,
    Tx_LogicRst,
    Rx_Locked,
    Rx_Bs_RstDly,
    Rx_Bs_Rst,
    Rx_Bsc_Rst,
    Rx_LogicRst,
    Riu_Addr,
    Riu_WrData,
    Riu_Wr_En,
    Riu_Nibble_Sel,
    Riu_RdData_3,
    Riu_Valid_3,
    Riu_Prsnt_3,
    Riu_RdData_2,
    Riu_Valid_2,
    Riu_Prsnt_2,
    Riu_RdData_1,
    Riu_Valid_1,
    Riu_Prsnt_1,
    Riu_RdData_0,
    Riu_Valid_0,
    Riu_Prsnt_0,
    Rx_BtVal_3,
    Rx_BtVal_2,
    Rx_BtVal_1,
    Rx_BtVal_0,
    Debug_Out);
  input ClockIn_p;
  input ClockIn_n;
  output ClockIn_se_out;
  input ResetIn;
  input Tx_Dly_Rdy;
  input Tx_Vtc_Rdy;
  output Tx_Bsc_EnVtc;
  output Tx_Bs_EnVtc;
  input Rx_Dly_Rdy;
  input Rx_Vtc_Rdy;
  output Rx_Bsc_EnVtc;
  output Rx_Bs_EnVtc;
  output Tx_SysClk;
  output Tx_WrClk;
  output Tx_ClkOutPhy;
  output Rx_SysClk;
  output Rx_RiuClk;
  output Rx_ClkOutPhy;
  output Tx_Locked;
  output Tx_Bs_RstDly;
  output Tx_Bs_Rst;
  output Tx_Bsc_Rst;
  output Tx_LogicRst;
  output Rx_Locked;
  output Rx_Bs_RstDly;
  output Rx_Bs_Rst;
  output Rx_Bsc_Rst;
  output Rx_LogicRst;
  output [5:0]Riu_Addr;
  output [15:0]Riu_WrData;
  output Riu_Wr_En;
  output [1:0]Riu_Nibble_Sel;
  input [15:0]Riu_RdData_3;
  input Riu_Valid_3;
  input Riu_Prsnt_3;
  input [15:0]Riu_RdData_2;
  input Riu_Valid_2;
  input Riu_Prsnt_2;
  input [15:0]Riu_RdData_1;
  input Riu_Valid_1;
  input Riu_Prsnt_1;
  input [15:0]Riu_RdData_0;
  input Riu_Valid_0;
  input Riu_Prsnt_0;
  output [8:0]Rx_BtVal_3;
  output [8:0]Rx_BtVal_2;
  output [8:0]Rx_BtVal_1;
  output [8:0]Rx_BtVal_0;
  output [7:0]Debug_Out;

  wire \<const0> ;
  wire \<const1> ;
  wire CLKOUTPHYEN;
  wire ClockIn_n;
  wire ClockIn_p;
  wire ClockIn_se_out;
  wire [7:0]Debug_Out;
  wire IntCtrl_Reset;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxDlyRdy;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxLocked;
  wire IntCtrl_RxLogicRst_i_1_n_0;
  wire IntCtrl_RxLogicRst_i_2_n_0;
  wire IntCtrl_RxLogicRst_reg_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_i_1_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_i_2_n_0;
  wire IntCtrl_RxPllClkOutPhyEn_reg_n_0;
  (* async_reg = "true" *) wire [1:0]IntCtrl_RxVtcRdy;
  wire IntCtrl_State;
  wire \IntCtrl_State[0]_i_1_n_0 ;
  wire \IntCtrl_State[0]_i_2_n_0 ;
  wire \IntCtrl_State[0]_i_3_n_0 ;
  wire \IntCtrl_State[0]_i_4_n_0 ;
  wire \IntCtrl_State[0]_i_5_n_0 ;
  wire \IntCtrl_State[0]_i_6_n_0 ;
  wire \IntCtrl_State[1]_i_1_n_0 ;
  wire \IntCtrl_State[1]_i_2_n_0 ;
  wire \IntCtrl_State[1]_i_3_n_0 ;
  wire \IntCtrl_State[2]_i_1_n_0 ;
  wire \IntCtrl_State[2]_i_2_n_0 ;
  wire \IntCtrl_State[2]_i_3_n_0 ;
  wire \IntCtrl_State[2]_i_4_n_0 ;
  wire \IntCtrl_State[2]_i_5_n_0 ;
  wire \IntCtrl_State[2]_i_6_n_0 ;
  wire \IntCtrl_State[3]_i_1_n_0 ;
  wire \IntCtrl_State[3]_i_2_n_0 ;
  wire \IntCtrl_State[4]_i_1_n_0 ;
  wire \IntCtrl_State[4]_i_2_n_0 ;
  wire \IntCtrl_State[5]_i_1_n_0 ;
  wire \IntCtrl_State[5]_i_2_n_0 ;
  wire \IntCtrl_State[5]_i_4_n_0 ;
  wire \IntCtrl_State[5]_i_5_n_0 ;
  wire \IntCtrl_State[6]_i_1_n_0 ;
  wire \IntCtrl_State[6]_i_2_n_0 ;
  wire \IntCtrl_State[6]_i_3_n_0 ;
  wire \IntCtrl_State[6]_i_4_n_0 ;
  wire \IntCtrl_State[6]_i_5_n_0 ;
  wire \IntCtrl_State[6]_i_6_n_0 ;
  wire \IntCtrl_State[7]_i_2_n_0 ;
  wire \IntCtrl_State[7]_i_3_n_0 ;
  wire \IntCtrl_State[7]_i_4_n_0 ;
  wire \IntCtrl_State[7]_i_5_n_0 ;
  wire \IntCtrl_State[7]_i_6_n_0 ;
  wire \IntCtrl_State[7]_i_7_n_0 ;
  wire \IntCtrl_State[7]_i_8_n_0 ;
  wire \IntCtrl_State[7]_i_9_n_0 ;
  wire \IntCtrl_State[8]_i_1_n_0 ;
  wire \IntCtrl_State_reg[5]_i_3_n_0 ;
  wire \IntCtrl_State_reg_n_0_[8] ;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxDlyRdy;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxLocked;
  wire IntCtrl_TxLogicRst_i_1_n_0;
  wire IntCtrl_TxLogicRst_i_2_n_0;
  wire IntCtrl_TxLogicRst_i_3_n_0;
  wire IntCtrl_TxLogicRst_reg_n_0;
  wire IntCtrl_TxPllClkOutPhyEn_i_1_n_0;
  wire IntCtrl_TxPllClkOutPhyEn_i_2_n_0;
  wire IntCtrl_TxPllRst_i_1_n_0;
  wire IntCtrl_TxPllRst_i_2_n_0;
  (* async_reg = "true" *) wire [1:0]IntCtrl_TxVtcRdy;
  wire IntRx_ClkOut0;
  wire IntTx_ClkOut0;
  wire IntTx_ClkOut1;
  wire RST;
  wire ResetIn;
  wire [4:0]\^Riu_Addr ;
  wire [5:0]Riu_Addr0_in;
  wire \Riu_Addr[0]_i_2_n_0 ;
  wire \Riu_Addr[0]_i_3_n_0 ;
  wire \Riu_Addr[1]_i_2_n_0 ;
  wire \Riu_Addr[5]_i_10_n_0 ;
  wire \Riu_Addr[5]_i_1_n_0 ;
  wire \Riu_Addr[5]_i_3_n_0 ;
  wire \Riu_Addr[5]_i_4_n_0 ;
  wire \Riu_Addr[5]_i_5_n_0 ;
  wire \Riu_Addr[5]_i_6_n_0 ;
  wire \Riu_Addr[5]_i_7_n_0 ;
  wire \Riu_Addr[5]_i_8_n_0 ;
  wire \Riu_Addr[5]_i_9_n_0 ;
  wire [0:0]\^Riu_Nibble_Sel ;
  wire \Riu_Nibble_Sel[0]_i_1_n_0 ;
  wire Riu_Prsnt_0;
  wire Riu_Prsnt_1;
  wire Riu_Prsnt_2;
  wire Riu_Prsnt_3;
  wire [15:0]Riu_RdData_0;
  wire [15:0]Riu_RdData_1;
  wire [15:0]Riu_RdData_2;
  wire [15:0]Riu_RdData_3;
  wire \Riu_WrData[3]_i_1_n_0 ;
  wire Riu_Wr_En;
  wire Rx_Bs_EnVtc;
  wire Rx_Bs_EnVtc_i_1_n_0;
  wire Rx_Bs_EnVtc_i_2_n_0;
  wire Rx_Bs_EnVtc_i_3_n_0;
  wire Rx_Bs_EnVtc_i_4_n_0;
  wire Rx_Bs_Rst;
  wire Rx_Bs_RstDly;
  wire Rx_Bs_Rst_i_1_n_0;
  wire Rx_Bs_Rst_i_2_n_0;
  wire Rx_Bsc_Rst;
  wire [8:0]Rx_BtVal_0;
  wire [8:0]Rx_BtVal_1;
  wire [8:0]Rx_BtVal_2;
  wire [8:0]Rx_BtVal_3;
  wire \Rx_BtVal_3[8]_i_10_n_0 ;
  wire \Rx_BtVal_3[8]_i_11_n_0 ;
  wire \Rx_BtVal_3[8]_i_12_n_0 ;
  wire \Rx_BtVal_3[8]_i_13_n_0 ;
  wire \Rx_BtVal_3[8]_i_14_n_0 ;
  wire \Rx_BtVal_3[8]_i_15_n_0 ;
  wire \Rx_BtVal_3[8]_i_16_n_0 ;
  wire \Rx_BtVal_3[8]_i_17_n_0 ;
  wire \Rx_BtVal_3[8]_i_18_n_0 ;
  wire \Rx_BtVal_3[8]_i_19_n_0 ;
  wire \Rx_BtVal_3[8]_i_1_n_0 ;
  wire \Rx_BtVal_3[8]_i_2_n_0 ;
  wire \Rx_BtVal_3[8]_i_3_n_0 ;
  wire \Rx_BtVal_3[8]_i_4_n_0 ;
  wire \Rx_BtVal_3[8]_i_5_n_0 ;
  wire \Rx_BtVal_3[8]_i_6_n_0 ;
  wire \Rx_BtVal_3[8]_i_7_n_0 ;
  wire \Rx_BtVal_3[8]_i_8_n_0 ;
  wire \Rx_BtVal_3[8]_i_9_n_0 ;
  wire Rx_ClkOutPhy;
  wire Rx_Dly_Rdy;
  wire Rx_Locked;
  wire Rx_LogicRst;
  wire Rx_RiuClk;
  wire Rx_SysClk;
  wire Rx_Vtc_Rdy;
  wire Tx_Bs_Rst;
  wire Tx_Bs_RstDly_i_1_n_0;
  wire Tx_Bs_RstDly_i_2_n_0;
  wire Tx_Bs_RstDly_i_3_n_0;
  wire Tx_Bs_Rst_i_1_n_0;
  wire Tx_Bs_Rst_i_2_n_0;
  wire Tx_Bsc_EnVtc;
  wire Tx_Bsc_EnVtc_i_1_n_0;
  wire Tx_Bsc_EnVtc_i_2_n_0;
  wire Tx_Bsc_EnVtc_i_3_n_0;
  wire Tx_Bsc_EnVtc_i_4_n_0;
  wire Tx_Bsc_Rst_i_1_n_0;
  wire Tx_Bsc_Rst_i_2_n_0;
  wire Tx_Bsc_Rst_i_3_n_0;
  wire Tx_ClkOutPhy;
  wire Tx_Dly_Rdy;
  wire Tx_Locked;
  wire Tx_LogicRst;
  wire Tx_SysClk;
  wire Tx_Vtc_Rdy;
  wire Tx_WrClk;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKFBIN_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKFBOUT_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT0B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Rx_DRDY_UNCONNECTED;
  wire [15:0]NLW_Clk_Rst_I_Plle3_Rx_DO_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKFBIN_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKFBOUT_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKOUT0B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_CLKOUT1B_UNCONNECTED;
  wire NLW_Clk_Rst_I_Plle3_Tx_DRDY_UNCONNECTED;
  wire [15:0]NLW_Clk_Rst_I_Plle3_Tx_DO_UNCONNECTED;

  assign Riu_Addr[5] = \^Riu_Addr [4];
  assign Riu_Addr[4] = \^Riu_Addr [4];
  assign Riu_Addr[3] = \^Riu_Addr [4];
  assign Riu_Addr[2] = \<const0> ;
  assign Riu_Addr[1:0] = \^Riu_Addr [1:0];
  assign Riu_Nibble_Sel[1] = \<const0> ;
  assign Riu_Nibble_Sel[0] = \^Riu_Nibble_Sel [0];
  assign Riu_WrData[15] = \<const0> ;
  assign Riu_WrData[14] = \<const0> ;
  assign Riu_WrData[13] = \<const0> ;
  assign Riu_WrData[12] = \<const0> ;
  assign Riu_WrData[11] = \<const0> ;
  assign Riu_WrData[10] = \<const0> ;
  assign Riu_WrData[9] = \<const0> ;
  assign Riu_WrData[8] = \<const0> ;
  assign Riu_WrData[7] = \<const0> ;
  assign Riu_WrData[6] = \<const0> ;
  assign Riu_WrData[5] = \<const0> ;
  assign Riu_WrData[4] = \<const0> ;
  assign Riu_WrData[3] = Riu_Wr_En;
  assign Riu_WrData[2] = Riu_Wr_En;
  assign Riu_WrData[1] = \<const0> ;
  assign Riu_WrData[0] = \<const0> ;
  assign Rx_Bsc_EnVtc = \<const0> ;
  assign Tx_Bs_EnVtc = \<const1> ;
  assign Tx_Bs_RstDly = Rx_Bs_RstDly;
  assign Tx_Bsc_Rst = Rx_Bsc_Rst;
  (* box_type = "PRIMITIVE" *) 
  BUFGCE_DIV #(
    .BUFGCE_DIVIDE(4),
    .CE_TYPE("SYNC"),
    .HARDSYNC_CLR("FALSE"),
    .IS_CE_INVERTED(1'b0),
    .IS_CLR_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Bufg_CtrlClk
       (.CE(1'b1),
        .CLR(1'b0),
        .I(ClockIn_se_out),
        .O(Rx_RiuClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_RxSysClk
       (.CE(Rx_Locked),
        .I(IntRx_ClkOut0),
        .O(Rx_SysClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_TxSysClk
       (.CE(Tx_Locked),
        .I(IntTx_ClkOut0),
        .O(Tx_SysClk));
  (* box_type = "PRIMITIVE" *) 
  BUFGCE #(
    .CE_TYPE("SYNC"),
    .IS_CE_INVERTED(1'b0),
    .IS_I_INVERTED(1'b0),
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    Clk_Rst_I_Bufg_TxWrClk
       (.CE(Tx_Locked),
        .I(IntTx_ClkOut1),
        .O(Tx_WrClk));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "PLLE3_ADV" *) 
  (* box_type = "PRIMITIVE" *) 
  PLLE4_ADV #(
    .CLKFBOUT_MULT(2),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN_PERIOD(1.600000),
    .CLKOUT0_DIVIDE(4),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(8),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUTPHY_MODE("VCO_HALF"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER(0.010000),
    .STARTUP_WAIT("FALSE")) 
    Clk_Rst_I_Plle3_Rx
       (.CLKFBIN(NLW_Clk_Rst_I_Plle3_Rx_CLKFBIN_UNCONNECTED),
        .CLKFBOUT(NLW_Clk_Rst_I_Plle3_Rx_CLKFBOUT_UNCONNECTED),
        .CLKIN(ClockIn_se_out),
        .CLKOUT0(IntRx_ClkOut0),
        .CLKOUT0B(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1_UNCONNECTED),
        .CLKOUT1B(NLW_Clk_Rst_I_Plle3_Rx_CLKOUT1B_UNCONNECTED),
        .CLKOUTPHY(Rx_ClkOutPhy),
        .CLKOUTPHYEN(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_Clk_Rst_I_Plle3_Rx_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_Clk_Rst_I_Plle3_Rx_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(Rx_Locked),
        .PWRDWN(1'b0),
        .RST(ResetIn));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "PLLE3_ADV" *) 
  (* box_type = "PRIMITIVE" *) 
  PLLE4_ADV #(
    .CLKFBOUT_MULT(2),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN_PERIOD(1.600000),
    .CLKOUT0_DIVIDE(8),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(10),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUTPHY_MODE("VCO"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKFBIN_INVERTED(1'b0),
    .IS_CLKIN_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER(0.010000),
    .STARTUP_WAIT("FALSE")) 
    Clk_Rst_I_Plle3_Tx
       (.CLKFBIN(NLW_Clk_Rst_I_Plle3_Tx_CLKFBIN_UNCONNECTED),
        .CLKFBOUT(NLW_Clk_Rst_I_Plle3_Tx_CLKFBOUT_UNCONNECTED),
        .CLKIN(ClockIn_se_out),
        .CLKOUT0(IntTx_ClkOut0),
        .CLKOUT0B(NLW_Clk_Rst_I_Plle3_Tx_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(IntTx_ClkOut1),
        .CLKOUT1B(NLW_Clk_Rst_I_Plle3_Tx_CLKOUT1B_UNCONNECTED),
        .CLKOUTPHY(Tx_ClkOutPhy),
        .CLKOUTPHYEN(CLKOUTPHYEN),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_Clk_Rst_I_Plle3_Tx_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_Clk_Rst_I_Plle3_Tx_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(Tx_Locked),
        .PWRDWN(1'b0),
        .RST(RST));
  GND GND
       (.G(\<const0> ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxDlyRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Dly_Rdy),
        .Q(IntCtrl_RxDlyRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxDlyRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxDlyRdy[0]),
        .Q(IntCtrl_RxDlyRdy[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxLocked_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Locked),
        .Q(IntCtrl_RxLocked[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxLocked_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxLocked[0]),
        .Q(IntCtrl_RxLocked[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2AAAAAAAAAAAAAAA)) 
    IntCtrl_RxLogicRst_i_1
       (.I0(IntCtrl_RxLogicRst_reg_n_0),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[5]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[6]),
        .I5(IntCtrl_RxLogicRst_i_2_n_0),
        .O(IntCtrl_RxLogicRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    IntCtrl_RxLogicRst_i_2
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[0]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[4]),
        .O(IntCtrl_RxLogicRst_i_2_n_0));
  FDSE IntCtrl_RxLogicRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxLogicRst_i_1_n_0),
        .Q(IntCtrl_RxLogicRst_reg_n_0),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFDFF000001C0)) 
    IntCtrl_RxPllClkOutPhyEn_i_1
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[7]),
        .I4(IntCtrl_RxPllClkOutPhyEn_i_2_n_0),
        .I5(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .O(IntCtrl_RxPllClkOutPhyEn_i_1_n_0));
  LUT6 #(
    .INIT(64'hF7F7FFFFFFFFFFFE)) 
    IntCtrl_RxPllClkOutPhyEn_i_2
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Tx_Bs_RstDly_i_3_n_0),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[5]),
        .I5(Debug_Out[4]),
        .O(IntCtrl_RxPllClkOutPhyEn_i_2_n_0));
  FDRE IntCtrl_RxPllClkOutPhyEn_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxPllClkOutPhyEn_i_1_n_0),
        .Q(IntCtrl_RxPllClkOutPhyEn_reg_n_0),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxVtcRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Vtc_Rdy),
        .Q(IntCtrl_RxVtcRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_RxVtcRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_RxVtcRdy[0]),
        .Q(IntCtrl_RxVtcRdy[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAFFAAAAFFFFFFFF)) 
    \IntCtrl_State[0]_i_1 
       (.I0(\IntCtrl_State[0]_i_2_n_0 ),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(\IntCtrl_State[0]_i_3_n_0 ),
        .I3(\IntCtrl_State[0]_i_4_n_0 ),
        .I4(\IntCtrl_State[0]_i_5_n_0 ),
        .I5(Debug_Out[0]),
        .O(\IntCtrl_State[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008002)) 
    \IntCtrl_State[0]_i_2 
       (.I0(\IntCtrl_State[0]_i_6_n_0 ),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[4]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(\IntCtrl_State[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \IntCtrl_State[0]_i_3 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[0]_i_4 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .O(\IntCtrl_State[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \IntCtrl_State[0]_i_5 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[6]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[5]),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h81)) 
    \IntCtrl_State[0]_i_6 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .O(\IntCtrl_State[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAAAAAAAAABAAA)) 
    \IntCtrl_State[1]_i_1 
       (.I0(\IntCtrl_State[1]_i_2_n_0 ),
        .I1(\IntCtrl_State[1]_i_3_n_0 ),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(Debug_Out[6]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6246666666666666)) 
    \IntCtrl_State[1]_i_2 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(\IntCtrl_State[6]_i_3_n_0 ),
        .I5(Tx_Bsc_EnVtc_i_2_n_0),
        .O(\IntCtrl_State[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7EEFFDFF)) 
    \IntCtrl_State[1]_i_3 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[6]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[1]),
        .O(\IntCtrl_State[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF6F66666666)) 
    \IntCtrl_State[2]_i_1 
       (.I0(\IntCtrl_State[6]_i_4_n_0 ),
        .I1(Debug_Out[2]),
        .I2(\IntCtrl_State[2]_i_2_n_0 ),
        .I3(\IntCtrl_State[2]_i_3_n_0 ),
        .I4(\IntCtrl_State[2]_i_4_n_0 ),
        .I5(Debug_Out[5]),
        .O(\IntCtrl_State[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFCE2F)) 
    \IntCtrl_State[2]_i_2 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[7]),
        .I4(Tx_Bs_RstDly_i_3_n_0),
        .I5(\IntCtrl_State[2]_i_5_n_0 ),
        .O(\IntCtrl_State[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \IntCtrl_State[2]_i_3 
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State[6]_i_4_n_0 ),
        .I2(Debug_Out[6]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[7]),
        .I5(\IntCtrl_State[2]_i_5_n_0 ),
        .O(\IntCtrl_State[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    \IntCtrl_State[2]_i_4 
       (.I0(\IntCtrl_State[2]_i_6_n_0 ),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[0]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .I5(Debug_Out[7]),
        .O(\IntCtrl_State[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[2]_i_5 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[2]),
        .O(\IntCtrl_State[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \IntCtrl_State[2]_i_6 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[3]),
        .O(\IntCtrl_State[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hBF3FFFFFC0C00000)) 
    \IntCtrl_State[3]_i_1 
       (.I0(\IntCtrl_State[3]_i_2_n_0 ),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[3]),
        .O(\IntCtrl_State[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h80000080)) 
    \IntCtrl_State[3]_i_2 
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[6]),
        .I4(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7FFF8000)) 
    \IntCtrl_State[4]_i_1 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(Debug_Out[4]),
        .I5(\IntCtrl_State[4]_i_2_n_0 ),
        .O(\IntCtrl_State[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8A8A828A00000000)) 
    \IntCtrl_State[4]_i_2 
       (.I0(\IntCtrl_State[0]_i_5_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[0]),
        .O(\IntCtrl_State[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFAAAAAA55AAAAAA)) 
    \IntCtrl_State[5]_i_1 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[4]),
        .I4(\IntCtrl_State[5]_i_2_n_0 ),
        .I5(\IntCtrl_State_reg[5]_i_3_n_0 ),
        .O(\IntCtrl_State[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \IntCtrl_State[5]_i_2 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .O(\IntCtrl_State[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0004040400000004)) 
    \IntCtrl_State[5]_i_4 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[2]),
        .O(\IntCtrl_State[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000044000300445)) 
    \IntCtrl_State[5]_i_5 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[6]),
        .O(\IntCtrl_State[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFFC0000000)) 
    \IntCtrl_State[6]_i_1 
       (.I0(\IntCtrl_State[6]_i_2_n_0 ),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[2]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(\IntCtrl_State[6]_i_4_n_0 ),
        .I5(Debug_Out[6]),
        .O(\IntCtrl_State[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00008881)) 
    \IntCtrl_State[6]_i_2 
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(\IntCtrl_State[6]_i_5_n_0 ),
        .O(\IntCtrl_State[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \IntCtrl_State[6]_i_3 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[4]),
        .O(\IntCtrl_State[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \IntCtrl_State[6]_i_4 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .O(\IntCtrl_State[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFFE7FFFFFFE)) 
    \IntCtrl_State[6]_i_5 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[2]),
        .I5(\IntCtrl_State[6]_i_6_n_0 ),
        .O(\IntCtrl_State[6]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \IntCtrl_State[6]_i_6 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .O(\IntCtrl_State[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8F0F0F0F0F0F0)) 
    \IntCtrl_State[7]_i_1 
       (.I0(Debug_Out[5]),
        .I1(\Rx_BtVal_3[8]_i_2_n_0 ),
        .I2(\IntCtrl_State[7]_i_3_n_0 ),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[4]),
        .O(IntCtrl_State));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hCB)) 
    \IntCtrl_State[7]_i_2 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State[7]_i_4_n_0 ),
        .O(\IntCtrl_State[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFEFEFEFEFEFE)) 
    \IntCtrl_State[7]_i_3 
       (.I0(\IntCtrl_State[7]_i_5_n_0 ),
        .I1(\IntCtrl_State[7]_i_6_n_0 ),
        .I2(\IntCtrl_State[7]_i_7_n_0 ),
        .I3(Rx_Bs_EnVtc_i_2_n_0),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[4]),
        .O(\IntCtrl_State[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \IntCtrl_State[7]_i_4 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(\IntCtrl_State[6]_i_3_n_0 ),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[3]),
        .O(\IntCtrl_State[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hA66A)) 
    \IntCtrl_State[7]_i_5 
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[2]),
        .O(\IntCtrl_State[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFF0FFD)) 
    \IntCtrl_State[7]_i_6 
       (.I0(Debug_Out[3]),
        .I1(\IntCtrl_State[7]_i_8_n_0 ),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\IntCtrl_State[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFAFAFAFABEFFBEBE)) 
    \IntCtrl_State[7]_i_7 
       (.I0(\IntCtrl_State[7]_i_9_n_0 ),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[3]),
        .I4(IntCtrl_TxVtcRdy[1]),
        .I5(Debug_Out[2]),
        .O(\IntCtrl_State[7]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hEA)) 
    \IntCtrl_State[7]_i_8 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(IntCtrl_TxLocked[1]),
        .I2(IntCtrl_RxLocked[1]),
        .O(\IntCtrl_State[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h000000008FFFFFFF)) 
    \IntCtrl_State[7]_i_9 
       (.I0(IntCtrl_TxDlyRdy[1]),
        .I1(IntCtrl_RxDlyRdy[1]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[4]),
        .O(\IntCtrl_State[7]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hDC9C9C9C)) 
    \IntCtrl_State[8]_i_1 
       (.I0(\IntCtrl_State[7]_i_4_n_0 ),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[4]),
        .O(\IntCtrl_State[8]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[0] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[0]_i_1_n_0 ),
        .Q(Debug_Out[0]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[1] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[1]_i_1_n_0 ),
        .Q(Debug_Out[1]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[2] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[2]_i_1_n_0 ),
        .Q(Debug_Out[2]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[3] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[3]_i_1_n_0 ),
        .Q(Debug_Out[3]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[4] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[4]_i_1_n_0 ),
        .Q(Debug_Out[4]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[5] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[5]_i_1_n_0 ),
        .Q(Debug_Out[5]),
        .R(IntCtrl_Reset));
  MUXF7 \IntCtrl_State_reg[5]_i_3 
       (.I0(\IntCtrl_State[5]_i_4_n_0 ),
        .I1(\IntCtrl_State[5]_i_5_n_0 ),
        .O(\IntCtrl_State_reg[5]_i_3_n_0 ),
        .S(Debug_Out[3]));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[6] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[6]_i_1_n_0 ),
        .Q(Debug_Out[6]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[7] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[7]_i_2_n_0 ),
        .Q(Debug_Out[7]),
        .R(IntCtrl_Reset));
  FDRE #(
    .INIT(1'b0)) 
    \IntCtrl_State_reg[8] 
       (.C(Rx_RiuClk),
        .CE(IntCtrl_State),
        .D(\IntCtrl_State[8]_i_1_n_0 ),
        .Q(\IntCtrl_State_reg_n_0_[8] ),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxDlyRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Dly_Rdy),
        .Q(IntCtrl_TxDlyRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxDlyRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxDlyRdy[0]),
        .Q(IntCtrl_TxDlyRdy[1]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxLocked_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Locked),
        .Q(IntCtrl_TxLocked[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxLocked_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxLocked[0]),
        .Q(IntCtrl_TxLocked[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAA8AAAAAAAA)) 
    IntCtrl_TxLogicRst_i_1
       (.I0(IntCtrl_TxLogicRst_reg_n_0),
        .I1(IntCtrl_TxLogicRst_i_2_n_0),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[0]),
        .I4(IntCtrl_TxLogicRst_i_3_n_0),
        .I5(Debug_Out[4]),
        .O(IntCtrl_TxLogicRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    IntCtrl_TxLogicRst_i_2
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[1]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[3]),
        .O(IntCtrl_TxLogicRst_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    IntCtrl_TxLogicRst_i_3
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .O(IntCtrl_TxLogicRst_i_3_n_0));
  FDSE IntCtrl_TxLogicRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxLogicRst_i_1_n_0),
        .Q(IntCtrl_TxLogicRst_reg_n_0),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    IntCtrl_TxPllClkOutPhyEn_i_1
       (.I0(Tx_Bsc_Rst_i_3_n_0),
        .I1(IntCtrl_TxPllClkOutPhyEn_i_2_n_0),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(CLKOUTPHYEN),
        .O(IntCtrl_TxPllClkOutPhyEn_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hE)) 
    IntCtrl_TxPllClkOutPhyEn_i_2
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[2]),
        .O(IntCtrl_TxPllClkOutPhyEn_i_2_n_0));
  FDRE IntCtrl_TxPllClkOutPhyEn_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxPllClkOutPhyEn_i_1_n_0),
        .Q(CLKOUTPHYEN),
        .R(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFBFFF00040000)) 
    IntCtrl_TxPllRst_i_1
       (.I0(Tx_Bsc_Rst_i_3_n_0),
        .I1(IntCtrl_TxPllRst_i_2_n_0),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[3]),
        .I5(RST),
        .O(IntCtrl_TxPllRst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h1)) 
    IntCtrl_TxPllRst_i_2
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .O(IntCtrl_TxPllRst_i_2_n_0));
  FDRE IntCtrl_TxPllRst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxPllRst_i_1_n_0),
        .Q(RST),
        .R(IntCtrl_Reset));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxVtcRdy_reg[0] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Vtc_Rdy),
        .Q(IntCtrl_TxVtcRdy[0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntCtrl_TxVtcRdy_reg[1] 
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(IntCtrl_TxVtcRdy[0]),
        .Q(IntCtrl_TxVtcRdy[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \Riu_Addr[0]_i_1 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[4]),
        .I2(\Riu_Addr[0]_i_2_n_0 ),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[6]),
        .I5(\Riu_Addr[0]_i_3_n_0 ),
        .O(Riu_Addr0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \Riu_Addr[0]_i_2 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[7]),
        .O(\Riu_Addr[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \Riu_Addr[0]_i_3 
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[1]),
        .O(\Riu_Addr[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \Riu_Addr[1]_i_1 
       (.I0(\Riu_Addr[1]_i_2_n_0 ),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[4]),
        .O(Riu_Addr0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Riu_Addr[1]_i_2 
       (.I0(Debug_Out[1]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[6]),
        .O(\Riu_Addr[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Riu_Addr[5]_i_1 
       (.I0(\Riu_Addr[5]_i_3_n_0 ),
        .I1(\Riu_Addr[5]_i_4_n_0 ),
        .I2(\Riu_Addr[5]_i_5_n_0 ),
        .I3(\Riu_Addr[5]_i_6_n_0 ),
        .I4(\Riu_Addr[5]_i_7_n_0 ),
        .O(\Riu_Addr[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6F666666)) 
    \Riu_Addr[5]_i_10 
       (.I0(Debug_Out[1]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[3]),
        .O(\Riu_Addr[5]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0A002000)) 
    \Riu_Addr[5]_i_2 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[3]),
        .O(Riu_Addr0_in[5]));
  LUT6 #(
    .INIT(64'hCCEEFECCFCFEFECC)) 
    \Riu_Addr[5]_i_3 
       (.I0(\Riu_Addr[5]_i_9_n_0 ),
        .I1(\Riu_Addr[5]_i_10_n_0 ),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0080AFAF00800080)) 
    \Riu_Addr[5]_i_4 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[6]),
        .I4(Debug_Out[3]),
        .I5(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h60EE60EE60EE6060)) 
    \Riu_Addr[5]_i_5 
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[5]),
        .I4(Debug_Out[7]),
        .I5(\IntCtrl_State_reg_n_0_[8] ),
        .O(\Riu_Addr[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h55FFFFFF00303030)) 
    \Riu_Addr[5]_i_6 
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[7]),
        .I5(Debug_Out[0]),
        .O(\Riu_Addr[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h74443000F444FF00)) 
    \Riu_Addr[5]_i_7 
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[6]),
        .I2(Debug_Out[7]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .I4(Debug_Out[2]),
        .I5(Debug_Out[1]),
        .O(\Riu_Addr[5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \Riu_Addr[5]_i_8 
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[7]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[4]),
        .I4(Debug_Out[5]),
        .O(\Riu_Addr[5]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Riu_Addr[5]_i_9 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .O(\Riu_Addr[5]_i_9_n_0 ));
  FDRE \Riu_Addr_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[0]),
        .Q(\^Riu_Addr [0]),
        .R(IntCtrl_Reset));
  FDRE \Riu_Addr_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[1]),
        .Q(\^Riu_Addr [1]),
        .R(IntCtrl_Reset));
  FDRE \Riu_Addr_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(Riu_Addr0_in[5]),
        .Q(\^Riu_Addr [4]),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h20082088)) 
    \Riu_Nibble_Sel[0]_i_1 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[3]),
        .I4(Debug_Out[0]),
        .O(\Riu_Nibble_Sel[0]_i_1_n_0 ));
  FDRE \Riu_Nibble_Sel_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(\Riu_Nibble_Sel[0]_i_1_n_0 ),
        .Q(\^Riu_Nibble_Sel ),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00000800)) 
    \Riu_WrData[3]_i_1 
       (.I0(\Riu_Addr[5]_i_8_n_0 ),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .I3(Debug_Out[1]),
        .I4(Debug_Out[0]),
        .O(\Riu_WrData[3]_i_1_n_0 ));
  FDRE \Riu_WrData_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Riu_Addr[5]_i_1_n_0 ),
        .D(\Riu_WrData[3]_i_1_n_0 ),
        .Q(Riu_Wr_En),
        .R(IntCtrl_Reset));
  LUT5 #(
    .INIT(32'hFF5D0051)) 
    Rx_Bs_EnVtc_i_1
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[7]),
        .I2(Rx_Bs_EnVtc_i_2_n_0),
        .I3(Rx_Bs_EnVtc_i_3_n_0),
        .I4(Rx_Bs_EnVtc),
        .O(Rx_Bs_EnVtc_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    Rx_Bs_EnVtc_i_2
       (.I0(Riu_Prsnt_3),
        .I1(Riu_RdData_3[11]),
        .I2(Riu_RdData_0[11]),
        .I3(Riu_Prsnt_0),
        .I4(Rx_Bs_EnVtc_i_4_n_0),
        .O(Rx_Bs_EnVtc_i_2_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFEF)) 
    Rx_Bs_EnVtc_i_3
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[5]),
        .I2(\Riu_Addr[1]_i_2_n_0 ),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[4]),
        .I5(Debug_Out[2]),
        .O(Rx_Bs_EnVtc_i_3_n_0));
  LUT4 #(
    .INIT(16'h4F44)) 
    Rx_Bs_EnVtc_i_4
       (.I0(Riu_RdData_2[11]),
        .I1(Riu_Prsnt_2),
        .I2(Riu_RdData_1[11]),
        .I3(Riu_Prsnt_1),
        .O(Rx_Bs_EnVtc_i_4_n_0));
  FDSE Rx_Bs_EnVtc_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Bs_EnVtc_i_1_n_0),
        .Q(Rx_Bs_EnVtc),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008003)) 
    Rx_Bs_Rst_i_1
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[5]),
        .I2(Debug_Out[2]),
        .I3(Debug_Out[3]),
        .I4(Rx_Bs_Rst_i_2_n_0),
        .I5(Rx_Bs_Rst),
        .O(Rx_Bs_Rst_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFDFFFFF4F)) 
    Rx_Bs_Rst_i_2
       (.I0(Debug_Out[5]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[6]),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(Rx_Bs_Rst_i_2_n_0));
  FDSE Rx_Bs_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Rx_Bs_Rst_i_1_n_0),
        .Q(Rx_Bs_Rst),
        .S(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[1]),
        .Q(Rx_BtVal_0[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[2]),
        .Q(Rx_BtVal_0[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[3]),
        .Q(Rx_BtVal_0[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[4]),
        .Q(Rx_BtVal_0[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[5]),
        .Q(Rx_BtVal_0[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[6]),
        .Q(Rx_BtVal_0[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[7]),
        .Q(Rx_BtVal_0[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[8]),
        .Q(Rx_BtVal_0[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_0_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_0[9]),
        .Q(Rx_BtVal_0[8]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[1]),
        .Q(Rx_BtVal_1[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[2]),
        .Q(Rx_BtVal_1[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[3]),
        .Q(Rx_BtVal_1[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[4]),
        .Q(Rx_BtVal_1[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[5]),
        .Q(Rx_BtVal_1[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[6]),
        .Q(Rx_BtVal_1[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[7]),
        .Q(Rx_BtVal_1[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[8]),
        .Q(Rx_BtVal_1[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_1_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_1[9]),
        .Q(Rx_BtVal_1[8]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[1]),
        .Q(Rx_BtVal_2[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[2]),
        .Q(Rx_BtVal_2[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[3]),
        .Q(Rx_BtVal_2[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[4]),
        .Q(Rx_BtVal_2[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[5]),
        .Q(Rx_BtVal_2[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[6]),
        .Q(Rx_BtVal_2[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[7]),
        .Q(Rx_BtVal_2[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[8]),
        .Q(Rx_BtVal_2[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_2_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_2[9]),
        .Q(Rx_BtVal_2[8]),
        .R(IntCtrl_Reset));
  LUT5 #(
    .INIT(32'h00200000)) 
    \Rx_BtVal_3[8]_i_1 
       (.I0(\Rx_BtVal_3[8]_i_2_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_3_n_0 ),
        .I2(Debug_Out[7]),
        .I3(Debug_Out[2]),
        .I4(Debug_Out[3]),
        .O(\Rx_BtVal_3[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_10 
       (.I0(Riu_RdData_0[13]),
        .I1(Riu_RdData_0[0]),
        .I2(Riu_RdData_0[15]),
        .I3(Riu_RdData_0[10]),
        .O(\Rx_BtVal_3[8]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_11 
       (.I0(Riu_RdData_2[10]),
        .I1(Riu_RdData_2[3]),
        .I2(Riu_RdData_2[8]),
        .I3(Riu_RdData_2[1]),
        .O(\Rx_BtVal_3[8]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_12 
       (.I0(Riu_RdData_2[14]),
        .I1(Riu_RdData_2[5]),
        .I2(Riu_RdData_2[15]),
        .I3(Riu_RdData_2[2]),
        .O(\Rx_BtVal_3[8]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \Rx_BtVal_3[8]_i_13 
       (.I0(Riu_RdData_2[11]),
        .I1(Riu_Prsnt_2),
        .I2(Riu_RdData_2[0]),
        .I3(Riu_RdData_2[13]),
        .I4(Riu_RdData_2[7]),
        .I5(Riu_RdData_2[9]),
        .O(\Rx_BtVal_3[8]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \Rx_BtVal_3[8]_i_14 
       (.I0(Riu_RdData_1[14]),
        .I1(Riu_RdData_1[5]),
        .I2(Riu_RdData_1[1]),
        .I3(Riu_RdData_1[15]),
        .I4(Riu_RdData_1[9]),
        .I5(Riu_Prsnt_1),
        .O(\Rx_BtVal_3[8]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_15 
       (.I0(Riu_RdData_1[10]),
        .I1(Riu_RdData_1[11]),
        .I2(Riu_RdData_1[7]),
        .I3(Riu_RdData_1[2]),
        .O(\Rx_BtVal_3[8]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_16 
       (.I0(Riu_RdData_1[12]),
        .I1(Riu_RdData_1[0]),
        .I2(Riu_RdData_1[13]),
        .I3(Riu_RdData_1[6]),
        .O(\Rx_BtVal_3[8]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \Rx_BtVal_3[8]_i_17 
       (.I0(Riu_RdData_3[7]),
        .I1(Riu_RdData_3[2]),
        .I2(Riu_RdData_3[1]),
        .I3(Riu_RdData_3[5]),
        .I4(Riu_RdData_3[6]),
        .I5(Riu_RdData_3[14]),
        .O(\Rx_BtVal_3[8]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_18 
       (.I0(Riu_RdData_3[15]),
        .I1(Riu_RdData_3[0]),
        .I2(Riu_RdData_3[10]),
        .I3(Riu_RdData_3[9]),
        .O(\Rx_BtVal_3[8]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \Rx_BtVal_3[8]_i_19 
       (.I0(Riu_RdData_3[8]),
        .I1(Riu_RdData_3[4]),
        .I2(Riu_Prsnt_3),
        .I3(Riu_RdData_3[12]),
        .O(\Rx_BtVal_3[8]_i_19_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \Rx_BtVal_3[8]_i_2 
       (.I0(\Rx_BtVal_3[8]_i_4_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_5_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_6_n_0 ),
        .I3(\Rx_BtVal_3[8]_i_7_n_0 ),
        .O(\Rx_BtVal_3[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    \Rx_BtVal_3[8]_i_3 
       (.I0(Debug_Out[6]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[5]),
        .I3(Debug_Out[0]),
        .I4(Debug_Out[1]),
        .I5(Debug_Out[4]),
        .O(\Rx_BtVal_3[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000020000)) 
    \Rx_BtVal_3[8]_i_4 
       (.I0(\Rx_BtVal_3[8]_i_8_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_9_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_10_n_0 ),
        .I3(Riu_RdData_0[9]),
        .I4(Riu_Prsnt_0),
        .I5(Riu_RdData_0[8]),
        .O(\Rx_BtVal_3[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \Rx_BtVal_3[8]_i_5 
       (.I0(\Rx_BtVal_3[8]_i_11_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_12_n_0 ),
        .I2(Riu_RdData_2[6]),
        .I3(Riu_RdData_2[12]),
        .I4(Riu_RdData_2[4]),
        .I5(\Rx_BtVal_3[8]_i_13_n_0 ),
        .O(\Rx_BtVal_3[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Rx_BtVal_3[8]_i_6 
       (.I0(\Rx_BtVal_3[8]_i_14_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_15_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_16_n_0 ),
        .I3(Riu_RdData_1[3]),
        .I4(Riu_RdData_1[8]),
        .I5(Riu_RdData_1[4]),
        .O(\Rx_BtVal_3[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \Rx_BtVal_3[8]_i_7 
       (.I0(\Rx_BtVal_3[8]_i_17_n_0 ),
        .I1(\Rx_BtVal_3[8]_i_18_n_0 ),
        .I2(\Rx_BtVal_3[8]_i_19_n_0 ),
        .I3(Riu_RdData_3[3]),
        .I4(Riu_RdData_3[13]),
        .I5(Riu_RdData_3[11]),
        .O(\Rx_BtVal_3[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \Rx_BtVal_3[8]_i_8 
       (.I0(Riu_RdData_0[2]),
        .I1(Riu_RdData_0[1]),
        .I2(Riu_RdData_0[5]),
        .I3(Riu_RdData_0[14]),
        .I4(Riu_RdData_0[6]),
        .I5(Riu_RdData_0[7]),
        .O(\Rx_BtVal_3[8]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Rx_BtVal_3[8]_i_9 
       (.I0(Riu_RdData_0[4]),
        .I1(Riu_RdData_0[3]),
        .I2(Riu_RdData_0[12]),
        .I3(Riu_RdData_0[11]),
        .O(\Rx_BtVal_3[8]_i_9_n_0 ));
  FDRE \Rx_BtVal_3_reg[0] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[1]),
        .Q(Rx_BtVal_3[0]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[1] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[2]),
        .Q(Rx_BtVal_3[1]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[2] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[3]),
        .Q(Rx_BtVal_3[2]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[3] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[4]),
        .Q(Rx_BtVal_3[3]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[4] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[5]),
        .Q(Rx_BtVal_3[4]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[5] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[6]),
        .Q(Rx_BtVal_3[5]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[6] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[7]),
        .Q(Rx_BtVal_3[6]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[7] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[8]),
        .Q(Rx_BtVal_3[7]),
        .R(IntCtrl_Reset));
  FDRE \Rx_BtVal_3_reg[8] 
       (.C(Rx_RiuClk),
        .CE(\Rx_BtVal_3[8]_i_1_n_0 ),
        .D(Riu_RdData_3[9]),
        .Q(Rx_BtVal_3[8]),
        .R(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hF701)) 
    Tx_Bs_RstDly_i_1
       (.I0(Debug_Out[3]),
        .I1(Debug_Out[5]),
        .I2(Tx_Bs_RstDly_i_2_n_0),
        .I3(Rx_Bs_RstDly),
        .O(Tx_Bs_RstDly_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    Tx_Bs_RstDly_i_2
       (.I0(Debug_Out[4]),
        .I1(\IntCtrl_State_reg_n_0_[8] ),
        .I2(Debug_Out[6]),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[2]),
        .I5(Tx_Bs_RstDly_i_3_n_0),
        .O(Tx_Bs_RstDly_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Tx_Bs_RstDly_i_3
       (.I0(Debug_Out[0]),
        .I1(Debug_Out[1]),
        .O(Tx_Bs_RstDly_i_3_n_0));
  FDSE Tx_Bs_RstDly_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bs_RstDly_i_1_n_0),
        .Q(Rx_Bs_RstDly),
        .S(IntCtrl_Reset));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hFF7F0001)) 
    Tx_Bs_Rst_i_1
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[2]),
        .I2(Debug_Out[3]),
        .I3(Tx_Bs_Rst_i_2_n_0),
        .I4(Tx_Bs_Rst),
        .O(Tx_Bs_Rst_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    Tx_Bs_Rst_i_2
       (.I0(Debug_Out[4]),
        .I1(Debug_Out[6]),
        .I2(\IntCtrl_State_reg_n_0_[8] ),
        .I3(Debug_Out[7]),
        .I4(Debug_Out[0]),
        .I5(Debug_Out[1]),
        .O(Tx_Bs_Rst_i_2_n_0));
  FDSE Tx_Bs_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bs_Rst_i_1_n_0),
        .Q(Tx_Bs_Rst),
        .S(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000200)) 
    Tx_Bsc_EnVtc_i_1
       (.I0(Tx_Bsc_EnVtc_i_2_n_0),
        .I1(Tx_Bsc_EnVtc_i_3_n_0),
        .I2(Debug_Out[4]),
        .I3(Debug_Out[5]),
        .I4(Tx_Bsc_EnVtc_i_4_n_0),
        .I5(Tx_Bsc_EnVtc),
        .O(Tx_Bsc_EnVtc_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h04)) 
    Tx_Bsc_EnVtc_i_2
       (.I0(\IntCtrl_State_reg_n_0_[8] ),
        .I1(Debug_Out[7]),
        .I2(Debug_Out[6]),
        .O(Tx_Bsc_EnVtc_i_2_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    Tx_Bsc_EnVtc_i_3
       (.I0(IntCtrl_RxDlyRdy[1]),
        .I1(IntCtrl_TxDlyRdy[1]),
        .O(Tx_Bsc_EnVtc_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    Tx_Bsc_EnVtc_i_4
       (.I0(Debug_Out[2]),
        .I1(Debug_Out[3]),
        .I2(Debug_Out[1]),
        .I3(Debug_Out[0]),
        .O(Tx_Bsc_EnVtc_i_4_n_0));
  FDRE Tx_Bsc_EnVtc_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bsc_EnVtc_i_1_n_0),
        .Q(Tx_Bsc_EnVtc),
        .R(IntCtrl_Reset));
  LUT6 #(
    .INIT(64'hFFFFFDFF00000400)) 
    Tx_Bsc_Rst_i_1
       (.I0(Debug_Out[6]),
        .I1(Debug_Out[4]),
        .I2(Debug_Out[3]),
        .I3(Tx_Bsc_Rst_i_2_n_0),
        .I4(Tx_Bsc_Rst_i_3_n_0),
        .I5(Rx_Bsc_Rst),
        .O(Tx_Bsc_Rst_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    Tx_Bsc_Rst_i_2
       (.I0(Debug_Out[7]),
        .I1(Debug_Out[2]),
        .O(Tx_Bsc_Rst_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    Tx_Bsc_Rst_i_3
       (.I0(Debug_Out[5]),
        .I1(Debug_Out[0]),
        .I2(Debug_Out[1]),
        .I3(\IntCtrl_State_reg_n_0_[8] ),
        .O(Tx_Bsc_Rst_i_3_n_0));
  FDSE Tx_Bsc_Rst_reg
       (.C(Rx_RiuClk),
        .CE(1'b1),
        .D(Tx_Bsc_Rst_i_1_n_0),
        .Q(Rx_Bsc_Rst),
        .S(IntCtrl_Reset));
  VCC VCC
       (.P(\<const1> ));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* XILINX_LEGACY_PRIM = "IBUFGDS" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .DIFF_TERM("FALSE"),
    .IOSTANDARD("DEFAULT")) 
    iclkbuf
       (.I(ClockIn_p),
        .IB(ClockIn_n),
        .O(ClockIn_se_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_24 reset_sync_ctrl_rst
       (.ResetIn(ResetIn),
        .reset_out(IntCtrl_Reset),
        .reset_sync1_0(Rx_RiuClk));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_25 reset_sync_rx_cdc_rst
       (.Rx_LogicRst(Rx_LogicRst),
        .Rx_SysClk(Rx_SysClk),
        .reset_in(IntCtrl_RxLogicRst_reg_n_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_26 reset_sync_tx_cdc_rst
       (.Tx_LogicRst(Tx_LogicRst),
        .Tx_WrClk(Tx_WrClk),
        .reset_in(IntCtrl_TxLogicRst_reg_n_0));
endmodule

(* C_BtslcNulType = "SERIAL" *) (* C_BusRxBitCtrlIn = "40" *) (* C_BusRxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlIn = "40" *) (* C_BusTxBitCtrlInTri = "40" *) (* C_BusTxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlOutTri = "40" *) (* C_BytePosition = "0" *) (* C_Cascade = "FALSE" *) 
(* C_CntValue = "9" *) (* C_Ctrl_Clk = "EXTERNAL" *) (* C_Delay_Format = "COUNT" *) 
(* C_Delay_Type = "VAR_LOAD" *) (* C_Delay_Value = "0" *) (* C_Delay_Value_Ext = "0" *) 
(* C_Div_Mode = "DIV2" *) (* C_En_Clk_To_Ext_North = "DISABLE" *) (* C_En_Clk_To_Ext_South = "DISABLE" *) 
(* C_En_Dyn_Odly_Mode = "FALSE" *) (* C_En_Other_Nclk = "FALSE" *) (* C_En_Other_Pclk = "FALSE" *) 
(* C_Fifo_Sync_Mode = "FALSE" *) (* C_Idly_Vt_Track = "TRUE" *) (* C_Inv_Rxclk = "FALSE" *) 
(* C_IoBank = "44" *) (* C_Is_Clk_Ext_Inverted = "1'b0" *) (* C_Is_Clk_Inverted = "1'b0" *) 
(* C_Is_Rst_Dly_Ext_Inverted = "1'b0" *) (* C_Is_Rst_Dly_Inverted = "1'b0" *) (* C_Is_Rst_Inverted = "1'b0" *) 
(* C_NibbleType = "7" *) (* C_Odly_Vt_Track = "TRUE" *) (* C_Part = "XCKU060" *) 
(* C_Qdly_Vt_Track = "TRUE" *) (* C_Read_Idle_Count = "6'b000000" *) (* C_RefClk_Frequency = "312.500000" *) 
(* C_RefClk_Src = "PLLCLK" *) (* C_Rounding_Factor = "16" *) (* C_RxGate_Extend = "FALSE" *) 
(* C_Rx_Clk_Phase_n = "SHIFT_90" *) (* C_Rx_Clk_Phase_p = "SHIFT_90" *) (* C_Rx_Data_Width = "4" *) 
(* C_Rx_Gating = "DISABLE" *) (* C_Self_Calibrate = "ENABLE" *) (* C_Serial_Mode = "TRUE" *) 
(* C_Tx_Gating = "DISABLE" *) (* C_Update_Mode = "ASYNC" *) (* C_Update_Mode_Ext = "ASYNC" *) 
(* C_UsedBitslices = "7'b0000011" *) (* keep_hierarchy = "true" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_Rx_Nibble
   (Rx_Bsc_Rst,
    Rx_Bs_Rst,
    Rx_Rst_Dly,
    Rx_Bsc_En_Vtc,
    Rx_Bs_En_Vtc,
    Rx_Riu_Clk,
    Rx_Riu_Addr,
    Rx_Riu_Wr_Data,
    Rx_Riu_Rd_Data,
    Rx_Riu_Valid,
    Rx_Riu_Prsnt,
    Rx_Riu_Wr_En,
    Rx_Riu_Nibble_Sel,
    Rx_Pll_Clk,
    Rx_RefClk,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    Rx_Dyn_Dci,
    Rx_Tbyte_In,
    Rx_Phy_Rden,
    Rx_Clk_From_Ext,
    Rx_Pclk_Nibble_In,
    Rx_Nclk_Nibble_In,
    Rx_Nclk_Nibble_Out,
    Rx_Pclk_Nibble_Out,
    Rx_Clk_To_Ext_North,
    Rx_Clk_To_Ext_South,
    Rx_Data_In,
    Rx_Q_Out,
    Rx_Q_CombOut,
    Fifo_Rd_Clk,
    Fifo_Wrclk_Out,
    Fifo_Rd_En,
    Fifo_Empty,
    Rx_Ce,
    Rx_Clk,
    Rx_Inc,
    Rx_Load,
    Rx_CntValueIn,
    Rx_CntValueOut,
    Rx_Ce_Ext,
    Rx_Inc_Ext,
    Rx_Load_Ext,
    Rx_CntValueIn_Ext,
    Rx_CntValueOut_Ext);
  input Rx_Bsc_Rst;
  input Rx_Bs_Rst;
  input Rx_Rst_Dly;
  input Rx_Bsc_En_Vtc;
  input Rx_Bs_En_Vtc;
  input Rx_Riu_Clk;
  input [5:0]Rx_Riu_Addr;
  input [15:0]Rx_Riu_Wr_Data;
  output [15:0]Rx_Riu_Rd_Data;
  output Rx_Riu_Valid;
  output Rx_Riu_Prsnt;
  input Rx_Riu_Wr_En;
  input Rx_Riu_Nibble_Sel;
  input Rx_Pll_Clk;
  input Rx_RefClk;
  output Rx_Dly_Rdy;
  output Rx_Vtc_Rdy;
  output [6:0]Rx_Dyn_Dci;
  input [3:0]Rx_Tbyte_In;
  input [3:0]Rx_Phy_Rden;
  input Rx_Clk_From_Ext;
  input Rx_Pclk_Nibble_In;
  input Rx_Nclk_Nibble_In;
  output Rx_Nclk_Nibble_Out;
  output Rx_Pclk_Nibble_Out;
  output Rx_Clk_To_Ext_North;
  output Rx_Clk_To_Ext_South;
  input [6:0]Rx_Data_In;
  (* dont_touch = "true" *) output [27:0]Rx_Q_Out;
  (* dont_touch = "true" *) output [6:0]Rx_Q_CombOut;
  input [6:0]Fifo_Rd_Clk;
  output Fifo_Wrclk_Out;
  input [6:0]Fifo_Rd_En;
  output [6:0]Fifo_Empty;
  input [6:0]Rx_Ce;
  input Rx_Clk;
  input [6:0]Rx_Inc;
  input [6:0]Rx_Load;
  input [62:0]Rx_CntValueIn;
  output [62:0]Rx_CntValueOut;
  input [6:0]Rx_Ce_Ext;
  input [6:0]Rx_Inc_Ext;
  input [6:0]Rx_Load_Ext;
  input [62:0]Rx_CntValueIn_Ext;
  output [62:0]Rx_CntValueOut_Ext;

  wire \<const0> ;
  wire \<const1> ;
  wire [1:0]\^Fifo_Empty ;
  wire [6:0]Fifo_Rd_Clk;
  wire [6:0]Fifo_Rd_En;
  wire Fifo_Wrclk_Out;
  wire [39:0]RX_BIT_CTRL_IN0;
  wire [39:0]RX_BIT_CTRL_IN1;
  wire [39:0]RX_BIT_CTRL_OUT0;
  wire [39:0]RX_BIT_CTRL_OUT1;
  wire Rx_Bs_En_Vtc;
  wire Rx_Bs_Rst;
  wire Rx_Bsc_En_Vtc;
  wire Rx_Bsc_Rst;
  wire [6:0]Rx_Ce;
  wire [6:0]Rx_Ce_Ext;
  wire Rx_Clk;
  wire Rx_Clk_From_Ext;
  wire Rx_Clk_To_Ext_North;
  wire Rx_Clk_To_Ext_South;
  wire [62:0]Rx_CntValueIn;
  wire [62:0]Rx_CntValueIn_Ext;
  wire [17:0]\^Rx_CntValueOut ;
  wire [17:0]\^Rx_CntValueOut_Ext ;
  wire [6:0]Rx_Data_In;
  wire Rx_Dly_Rdy;
  wire [6:0]Rx_Dyn_Dci;
  wire [6:0]Rx_Inc;
  wire [6:0]Rx_Inc_Ext;
  wire [6:0]Rx_Load;
  wire [6:0]Rx_Load_Ext;
  wire Rx_Nclk_Nibble_In;
  wire Rx_Nclk_Nibble_Out;
  wire Rx_Pclk_Nibble_In;
  wire Rx_Pclk_Nibble_Out;
  wire [3:0]Rx_Phy_Rden;
  wire Rx_Pll_Clk;
  (* DONT_TOUCH *) wire [6:0]Rx_Q_CombOut;
  (* DONT_TOUCH *) wire [27:0]Rx_Q_Out;
  wire Rx_RefClk;
  wire [5:0]Rx_Riu_Addr;
  wire Rx_Riu_Clk;
  wire Rx_Riu_Nibble_Sel;
  wire [15:0]Rx_Riu_Rd_Data;
  wire Rx_Riu_Valid;
  wire [15:0]Rx_Riu_Wr_Data;
  wire Rx_Riu_Wr_En;
  wire Rx_Rst_Dly;
  wire [3:0]Rx_Tbyte_In;
  wire Rx_Vtc_Rdy;
  wire [39:0]TX_BIT_CTRL_IN0;
  wire [39:0]TX_BIT_CTRL_IN1;
  wire [39:0]TX_BIT_CTRL_OUT0;
  wire [39:0]TX_BIT_CTRL_OUT1;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT4_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT4_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED ;
  wire [7:4]\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED ;
  wire \NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_FIFO_WRCLK_OUT_UNCONNECTED ;
  wire [7:4]\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED ;

  assign Fifo_Empty[6] = \<const0> ;
  assign Fifo_Empty[5] = \<const0> ;
  assign Fifo_Empty[4] = \<const0> ;
  assign Fifo_Empty[3] = \<const0> ;
  assign Fifo_Empty[2] = \<const0> ;
  assign Fifo_Empty[1:0] = \^Fifo_Empty [1:0];
  assign Rx_CntValueOut[62] = \<const0> ;
  assign Rx_CntValueOut[61] = \<const0> ;
  assign Rx_CntValueOut[60] = \<const0> ;
  assign Rx_CntValueOut[59] = \<const0> ;
  assign Rx_CntValueOut[58] = \<const0> ;
  assign Rx_CntValueOut[57] = \<const0> ;
  assign Rx_CntValueOut[56] = \<const0> ;
  assign Rx_CntValueOut[55] = \<const0> ;
  assign Rx_CntValueOut[54] = \<const0> ;
  assign Rx_CntValueOut[53] = \<const0> ;
  assign Rx_CntValueOut[52] = \<const0> ;
  assign Rx_CntValueOut[51] = \<const0> ;
  assign Rx_CntValueOut[50] = \<const0> ;
  assign Rx_CntValueOut[49] = \<const0> ;
  assign Rx_CntValueOut[48] = \<const0> ;
  assign Rx_CntValueOut[47] = \<const0> ;
  assign Rx_CntValueOut[46] = \<const0> ;
  assign Rx_CntValueOut[45] = \<const0> ;
  assign Rx_CntValueOut[44] = \<const0> ;
  assign Rx_CntValueOut[43] = \<const0> ;
  assign Rx_CntValueOut[42] = \<const0> ;
  assign Rx_CntValueOut[41] = \<const0> ;
  assign Rx_CntValueOut[40] = \<const0> ;
  assign Rx_CntValueOut[39] = \<const0> ;
  assign Rx_CntValueOut[38] = \<const0> ;
  assign Rx_CntValueOut[37] = \<const0> ;
  assign Rx_CntValueOut[36] = \<const0> ;
  assign Rx_CntValueOut[35] = \<const0> ;
  assign Rx_CntValueOut[34] = \<const0> ;
  assign Rx_CntValueOut[33] = \<const0> ;
  assign Rx_CntValueOut[32] = \<const0> ;
  assign Rx_CntValueOut[31] = \<const0> ;
  assign Rx_CntValueOut[30] = \<const0> ;
  assign Rx_CntValueOut[29] = \<const0> ;
  assign Rx_CntValueOut[28] = \<const0> ;
  assign Rx_CntValueOut[27] = \<const0> ;
  assign Rx_CntValueOut[26] = \<const0> ;
  assign Rx_CntValueOut[25] = \<const0> ;
  assign Rx_CntValueOut[24] = \<const0> ;
  assign Rx_CntValueOut[23] = \<const0> ;
  assign Rx_CntValueOut[22] = \<const0> ;
  assign Rx_CntValueOut[21] = \<const0> ;
  assign Rx_CntValueOut[20] = \<const0> ;
  assign Rx_CntValueOut[19] = \<const0> ;
  assign Rx_CntValueOut[18] = \<const0> ;
  assign Rx_CntValueOut[17:0] = \^Rx_CntValueOut [17:0];
  assign Rx_CntValueOut_Ext[62] = \<const0> ;
  assign Rx_CntValueOut_Ext[61] = \<const0> ;
  assign Rx_CntValueOut_Ext[60] = \<const0> ;
  assign Rx_CntValueOut_Ext[59] = \<const0> ;
  assign Rx_CntValueOut_Ext[58] = \<const0> ;
  assign Rx_CntValueOut_Ext[57] = \<const0> ;
  assign Rx_CntValueOut_Ext[56] = \<const0> ;
  assign Rx_CntValueOut_Ext[55] = \<const0> ;
  assign Rx_CntValueOut_Ext[54] = \<const0> ;
  assign Rx_CntValueOut_Ext[53] = \<const0> ;
  assign Rx_CntValueOut_Ext[52] = \<const0> ;
  assign Rx_CntValueOut_Ext[51] = \<const0> ;
  assign Rx_CntValueOut_Ext[50] = \<const0> ;
  assign Rx_CntValueOut_Ext[49] = \<const0> ;
  assign Rx_CntValueOut_Ext[48] = \<const0> ;
  assign Rx_CntValueOut_Ext[47] = \<const0> ;
  assign Rx_CntValueOut_Ext[46] = \<const0> ;
  assign Rx_CntValueOut_Ext[45] = \<const0> ;
  assign Rx_CntValueOut_Ext[44] = \<const0> ;
  assign Rx_CntValueOut_Ext[43] = \<const0> ;
  assign Rx_CntValueOut_Ext[42] = \<const0> ;
  assign Rx_CntValueOut_Ext[41] = \<const0> ;
  assign Rx_CntValueOut_Ext[40] = \<const0> ;
  assign Rx_CntValueOut_Ext[39] = \<const0> ;
  assign Rx_CntValueOut_Ext[38] = \<const0> ;
  assign Rx_CntValueOut_Ext[37] = \<const0> ;
  assign Rx_CntValueOut_Ext[36] = \<const0> ;
  assign Rx_CntValueOut_Ext[35] = \<const0> ;
  assign Rx_CntValueOut_Ext[34] = \<const0> ;
  assign Rx_CntValueOut_Ext[33] = \<const0> ;
  assign Rx_CntValueOut_Ext[32] = \<const0> ;
  assign Rx_CntValueOut_Ext[31] = \<const0> ;
  assign Rx_CntValueOut_Ext[30] = \<const0> ;
  assign Rx_CntValueOut_Ext[29] = \<const0> ;
  assign Rx_CntValueOut_Ext[28] = \<const0> ;
  assign Rx_CntValueOut_Ext[27] = \<const0> ;
  assign Rx_CntValueOut_Ext[26] = \<const0> ;
  assign Rx_CntValueOut_Ext[25] = \<const0> ;
  assign Rx_CntValueOut_Ext[24] = \<const0> ;
  assign Rx_CntValueOut_Ext[23] = \<const0> ;
  assign Rx_CntValueOut_Ext[22] = \<const0> ;
  assign Rx_CntValueOut_Ext[21] = \<const0> ;
  assign Rx_CntValueOut_Ext[20] = \<const0> ;
  assign Rx_CntValueOut_Ext[19] = \<const0> ;
  assign Rx_CntValueOut_Ext[18] = \<const0> ;
  assign Rx_CntValueOut_Ext[17:0] = \^Rx_CntValueOut_Ext [17:0];
  assign Rx_Riu_Prsnt = \<const1> ;
  GND GND
       (.G(\<const0> ));
  (* box_type = "PRIMITIVE" *) 
  BITSLICE_CONTROL #(
    .CTRL_CLK("EXTERNAL"),
    .DIV_MODE("DIV2"),
    .EN_CLK_TO_EXT_NORTH("DISABLE"),
    .EN_CLK_TO_EXT_SOUTH("DISABLE"),
    .EN_DYN_ODLY_MODE("FALSE"),
    .EN_OTHER_NCLK("FALSE"),
    .EN_OTHER_PCLK("FALSE"),
    .IDLY_VT_TRACK("TRUE"),
    .INV_RXCLK("FALSE"),
    .ODLY_VT_TRACK("TRUE"),
    .QDLY_VT_TRACK("TRUE"),
    .READ_IDLE_COUNT(6'h00),
    .REFCLK_SRC("PLLCLK"),
    .ROUNDING_FACTOR(16),
    .RXGATE_EXTEND("FALSE"),
    .RX_CLK_PHASE_N("SHIFT_90"),
    .RX_CLK_PHASE_P("SHIFT_90"),
    .RX_GATING("DISABLE"),
    .SELF_CALIBRATE("ENABLE"),
    .SERIAL_MODE("TRUE"),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_SPEEDUP("FAST"),
    .SIM_VERSION(2.000000),
    .TX_GATING("DISABLE")) 
    \Gen_1.Nibble_I_BitsliceCntrl 
       (.CLK_FROM_EXT(Rx_Clk_From_Ext),
        .CLK_TO_EXT_NORTH(Rx_Clk_To_Ext_North),
        .CLK_TO_EXT_SOUTH(Rx_Clk_To_Ext_South),
        .DLY_RDY(Rx_Dly_Rdy),
        .DYN_DCI(Rx_Dyn_Dci),
        .EN_VTC(Rx_Bsc_En_Vtc),
        .NCLK_NIBBLE_IN(Rx_Nclk_Nibble_In),
        .NCLK_NIBBLE_OUT(Rx_Nclk_Nibble_Out),
        .PCLK_NIBBLE_IN(Rx_Pclk_Nibble_In),
        .PCLK_NIBBLE_OUT(Rx_Pclk_Nibble_Out),
        .PHY_RDCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDCS1({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDEN(Rx_Phy_Rden),
        .PHY_WRCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS1({1'b0,1'b0,1'b0,1'b0}),
        .PLL_CLK(Rx_Pll_Clk),
        .REFCLK(Rx_RefClk),
        .RIU_ADDR(Rx_Riu_Addr),
        .RIU_CLK(Rx_Riu_Clk),
        .RIU_NIBBLE_SEL(Rx_Riu_Nibble_Sel),
        .RIU_RD_DATA(Rx_Riu_Rd_Data),
        .RIU_VALID(Rx_Riu_Valid),
        .RIU_WR_DATA(Rx_Riu_Wr_Data),
        .RIU_WR_EN(Rx_Riu_Wr_En),
        .RST(Rx_Bsc_Rst),
        .RX_BIT_CTRL_IN0(RX_BIT_CTRL_IN0),
        .RX_BIT_CTRL_IN1(RX_BIT_CTRL_IN1),
        .RX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN6({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_OUT0(RX_BIT_CTRL_OUT0),
        .RX_BIT_CTRL_OUT1(RX_BIT_CTRL_OUT1),
        .RX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT4(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT4_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TBYTE_IN(Rx_Tbyte_In),
        .TX_BIT_CTRL_IN0(TX_BIT_CTRL_IN0),
        .TX_BIT_CTRL_IN1(TX_BIT_CTRL_IN1),
        .TX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN6({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN_TRI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_OUT0(TX_BIT_CTRL_OUT0),
        .TX_BIT_CTRL_OUT1(TX_BIT_CTRL_OUT1),
        .TX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT4(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT4_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT_TRI(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED [39:0]),
        .VTC_RDY(Rx_Vtc_Rdy));
  (* DONT_TOUCH *) 
  (* box_type = "PRIMITIVE" *) 
  RX_BITSLICE #(
    .CASCADE("FALSE"),
    .DATA_TYPE("SERIAL"),
    .DATA_WIDTH(4),
    .DELAY_FORMAT("COUNT"),
    .DELAY_TYPE("VAR_LOAD"),
    .DELAY_VALUE(0),
    .DELAY_VALUE_EXT(0),
    .FIFO_SYNC_MODE("FALSE"),
    .IS_CLK_EXT_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_EXT_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REFCLK_FREQUENCY(312.500000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .UPDATE_MODE("ASYNC"),
    .UPDATE_MODE_EXT("ASYNC")) 
    \Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0 
       (.CE(Rx_Ce[0]),
        .CE_EXT(Rx_Ce_Ext[0]),
        .CLK(Rx_Clk),
        .CLK_EXT(Rx_Clk),
        .CNTVALUEIN(Rx_CntValueIn[8:0]),
        .CNTVALUEIN_EXT(Rx_CntValueIn_Ext[8:0]),
        .CNTVALUEOUT(\^Rx_CntValueOut [8:0]),
        .CNTVALUEOUT_EXT(\^Rx_CntValueOut_Ext [8:0]),
        .DATAIN(Rx_Data_In[0]),
        .EN_VTC(Rx_Bs_En_Vtc),
        .EN_VTC_EXT(Rx_Bs_En_Vtc),
        .FIFO_EMPTY(\^Fifo_Empty [0]),
        .FIFO_RD_CLK(Fifo_Rd_Clk[0]),
        .FIFO_RD_EN(Fifo_Rd_En[0]),
        .FIFO_WRCLK_OUT(Fifo_Wrclk_Out),
        .INC(Rx_Inc[0]),
        .INC_EXT(Rx_Inc_Ext[0]),
        .LOAD(Rx_Load[0]),
        .LOAD_EXT(Rx_Load_Ext[0]),
        .Q({\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED [7:6],Rx_Q_CombOut[0],\NLW_Gen_5[1].Gen_5_1.Gen_5_1_1.Nibble_I_RxBitslice_0_Q_UNCONNECTED [4],Rx_Q_Out[3:0]}),
        .RST(Rx_Bs_Rst),
        .RST_DLY(Rx_Rst_Dly),
        .RST_DLY_EXT(Rx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT0),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN0),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT0),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN0));
  (* box_type = "PRIMITIVE" *) 
  RX_BITSLICE #(
    .CASCADE("FALSE"),
    .DATA_TYPE("SERIAL"),
    .DATA_WIDTH(4),
    .DELAY_FORMAT("COUNT"),
    .DELAY_TYPE("VAR_LOAD"),
    .DELAY_VALUE(0),
    .DELAY_VALUE_EXT(0),
    .FIFO_SYNC_MODE("FALSE"),
    .IS_CLK_EXT_INVERTED(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_EXT_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REFCLK_FREQUENCY(312.500000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .UPDATE_MODE("ASYNC"),
    .UPDATE_MODE_EXT("ASYNC")) 
    \Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n 
       (.CE(Rx_Ce[1]),
        .CE_EXT(Rx_Ce_Ext[1]),
        .CLK(Rx_Clk),
        .CLK_EXT(Rx_Clk),
        .CNTVALUEIN(Rx_CntValueIn[17:9]),
        .CNTVALUEIN_EXT(Rx_CntValueIn_Ext[17:9]),
        .CNTVALUEOUT(\^Rx_CntValueOut [17:9]),
        .CNTVALUEOUT_EXT(\^Rx_CntValueOut_Ext [17:9]),
        .DATAIN(Rx_Data_In[1]),
        .EN_VTC(Rx_Bs_En_Vtc),
        .EN_VTC_EXT(Rx_Bs_En_Vtc),
        .FIFO_EMPTY(\^Fifo_Empty [1]),
        .FIFO_RD_CLK(Fifo_Rd_Clk[1]),
        .FIFO_RD_EN(Fifo_Rd_En[1]),
        .FIFO_WRCLK_OUT(\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_FIFO_WRCLK_OUT_UNCONNECTED ),
        .INC(Rx_Inc[1]),
        .INC_EXT(Rx_Inc_Ext[1]),
        .LOAD(Rx_Load[1]),
        .LOAD_EXT(Rx_Load_Ext[1]),
        .Q({\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED [7:6],Rx_Q_CombOut[1],\NLW_Gen_5[2].Gen_5_1.Gen_5_1_0.Nibble_I_RxBitslice_n_Q_UNCONNECTED [4],Rx_Q_Out[7:4]}),
        .RST(Rx_Bs_Rst),
        .RST_DLY(Rx_Rst_Dly),
        .RST_DLY_EXT(Rx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT1),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN1),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT1),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN1));
  VCC VCC
       (.P(\<const1> ));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(Rx_Q_Out[27]));
  LUT1 #(
    .INIT(2'h2)) 
    i_1
       (.I0(1'b0),
        .O(Rx_Q_Out[26]));
  LUT1 #(
    .INIT(2'h2)) 
    i_10
       (.I0(1'b0),
        .O(Rx_Q_Out[17]));
  LUT1 #(
    .INIT(2'h2)) 
    i_11
       (.I0(1'b0),
        .O(Rx_Q_Out[16]));
  LUT1 #(
    .INIT(2'h2)) 
    i_12
       (.I0(1'b0),
        .O(Rx_Q_Out[15]));
  LUT1 #(
    .INIT(2'h2)) 
    i_13
       (.I0(1'b0),
        .O(Rx_Q_Out[14]));
  LUT1 #(
    .INIT(2'h2)) 
    i_14
       (.I0(1'b0),
        .O(Rx_Q_Out[13]));
  LUT1 #(
    .INIT(2'h2)) 
    i_15
       (.I0(1'b0),
        .O(Rx_Q_Out[12]));
  LUT1 #(
    .INIT(2'h2)) 
    i_16
       (.I0(1'b0),
        .O(Rx_Q_Out[11]));
  LUT1 #(
    .INIT(2'h2)) 
    i_17
       (.I0(1'b0),
        .O(Rx_Q_Out[10]));
  LUT1 #(
    .INIT(2'h2)) 
    i_18
       (.I0(1'b0),
        .O(Rx_Q_Out[9]));
  LUT1 #(
    .INIT(2'h2)) 
    i_19
       (.I0(1'b0),
        .O(Rx_Q_Out[8]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2
       (.I0(1'b0),
        .O(Rx_Q_Out[25]));
  LUT1 #(
    .INIT(2'h2)) 
    i_20
       (.I0(1'b0),
        .O(Rx_Q_CombOut[6]));
  LUT1 #(
    .INIT(2'h2)) 
    i_21
       (.I0(1'b0),
        .O(Rx_Q_CombOut[5]));
  LUT1 #(
    .INIT(2'h2)) 
    i_22
       (.I0(1'b0),
        .O(Rx_Q_CombOut[4]));
  LUT1 #(
    .INIT(2'h2)) 
    i_23
       (.I0(1'b0),
        .O(Rx_Q_CombOut[3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_24
       (.I0(1'b0),
        .O(Rx_Q_CombOut[2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_3
       (.I0(1'b0),
        .O(Rx_Q_Out[24]));
  LUT1 #(
    .INIT(2'h2)) 
    i_4
       (.I0(1'b0),
        .O(Rx_Q_Out[23]));
  LUT1 #(
    .INIT(2'h2)) 
    i_5
       (.I0(1'b0),
        .O(Rx_Q_Out[22]));
  LUT1 #(
    .INIT(2'h2)) 
    i_6
       (.I0(1'b0),
        .O(Rx_Q_Out[21]));
  LUT1 #(
    .INIT(2'h2)) 
    i_7
       (.I0(1'b0),
        .O(Rx_Q_Out[20]));
  LUT1 #(
    .INIT(2'h2)) 
    i_8
       (.I0(1'b0),
        .O(Rx_Q_Out[19]));
  LUT1 #(
    .INIT(2'h2)) 
    i_9
       (.I0(1'b0),
        .O(Rx_Q_Out[18]));
endmodule

(* C_BtslceUsedAsT = "7'b0000000" *) (* C_BusRxBitCtrlIn = "40" *) (* C_BusRxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlIn = "40" *) (* C_BusTxBitCtrlInTri = "40" *) (* C_BusTxBitCtrlOut = "40" *) 
(* C_BusTxBitCtrlOutTri = "40" *) (* C_BytePosition = "0" *) (* C_CntValue = "9" *) 
(* C_Ctrl_Clk = "EXTERNAL" *) (* C_Data_Type = "DATA" *) (* C_Delay_Format = "TIME" *) 
(* C_Delay_Type = "FIXED" *) (* C_Delay_Value = "0" *) (* C_Div_Mode = "DIV4" *) 
(* C_En_Clk_To_Ext_North = "DISABLE" *) (* C_En_Clk_To_Ext_South = "DISABLE" *) (* C_En_Dyn_Odly_Mode = "FALSE" *) 
(* C_En_Other_Nclk = "FALSE" *) (* C_En_Other_Pclk = "FALSE" *) (* C_Enable_Pre_Emphasis = "FALSE" *) 
(* C_Idly_Vt_Track = "FALSE" *) (* C_Init = "1'b0" *) (* C_Inv_Rxclk = "FALSE" *) 
(* C_IoBank = "44" *) (* C_Is_Clk_Inverted = "1'b0" *) (* C_Is_Rst_Dly_Inverted = "1'b0" *) 
(* C_Is_Rst_Inverted = "1'b0" *) (* C_Native_Odelay_Bypass = "FALSE" *) (* C_NibbleType = "6" *) 
(* C_Odly_Vt_Track = "FALSE" *) (* C_Output_Phase_90 = "TRUE" *) (* C_Part = "XCKU060" *) 
(* C_Qdly_Vt_Track = "FALSE" *) (* C_Read_Idle_Count = "6'b000000" *) (* C_RefClk_Frequency = "1250.000000" *) 
(* C_RefClk_Src = "PLLCLK" *) (* C_Rounding_Factor = "16" *) (* C_RxGate_Extend = "FALSE" *) 
(* C_Rx_Clk_Phase_n = "SHIFT_0" *) (* C_Rx_Clk_Phase_p = "SHIFT_0" *) (* C_Rx_Gating = "DISABLE" *) 
(* C_Self_Calibrate = "ENABLE" *) (* C_Serial_Mode = "FALSE" *) (* C_Tx_BtslceTr = "T" *) 
(* C_Tx_Data_Width = "8" *) (* C_Tx_Gating = "ENABLE" *) (* C_Update_Mode = "ASYNC" *) 
(* C_UsedBitslices = "7'b0010000" *) (* keep_hierarchy = "true" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_Tx_Nibble
   (Tx_Bsc_Rst,
    Tx_Bs_Rst,
    Tx_Rst_Dly,
    Tx_Bsc_En_Vtc,
    Tx_Bs_En_Vtc,
    Tx_Riu_Clk,
    Tx_Riu_Addr,
    Tx_Riu_Wr_Data,
    Tx_Riu_Rd_Data,
    Tx_Riu_Valid,
    Tx_Riu_Wr_En,
    Tx_Riu_Nibble_Sel,
    Tx_Pll_Clk,
    Tx_RefClk,
    Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Tx_Dyn_Dci,
    Tx_Tbyte_In,
    Tx_Phy_Rden,
    Tx_Clk_From_Ext,
    Tx_Pclk_Nibble_In,
    Tx_Nclk_Nibble_In,
    Tx_Nclk_Nibble_Out,
    Tx_Pclk_Nibble_Out,
    Tx_Clk_To_Ext_North,
    Tx_Clk_To_Ext_South,
    Tx_Tri_Out,
    Tx_Data_Out,
    Tx_T_In,
    Tx_D_In,
    Tx_Ce,
    Tx_Clk,
    Tx_Inc,
    Tx_Load,
    Tx_CntValueIn,
    Tx_CntValueOut,
    TxTri_Ce,
    TxTri_Clk,
    TxTri_Inc,
    TxTri_Load,
    TxTri_CntValueIn,
    TxTri_CntValueOut);
  input Tx_Bsc_Rst;
  input Tx_Bs_Rst;
  input Tx_Rst_Dly;
  input Tx_Bsc_En_Vtc;
  input Tx_Bs_En_Vtc;
  input Tx_Riu_Clk;
  input [5:0]Tx_Riu_Addr;
  input [15:0]Tx_Riu_Wr_Data;
  output [15:0]Tx_Riu_Rd_Data;
  output Tx_Riu_Valid;
  input Tx_Riu_Wr_En;
  input Tx_Riu_Nibble_Sel;
  input Tx_Pll_Clk;
  input Tx_RefClk;
  output Tx_Dly_Rdy;
  output Tx_Vtc_Rdy;
  output [6:0]Tx_Dyn_Dci;
  input [3:0]Tx_Tbyte_In;
  input [3:0]Tx_Phy_Rden;
  input Tx_Clk_From_Ext;
  input Tx_Pclk_Nibble_In;
  input Tx_Nclk_Nibble_In;
  output Tx_Nclk_Nibble_Out;
  output Tx_Pclk_Nibble_Out;
  output Tx_Clk_To_Ext_North;
  output Tx_Clk_To_Ext_South;
  output [5:0]Tx_Tri_Out;
  output [5:0]Tx_Data_Out;
  input [5:0]Tx_T_In;
  input [47:0]Tx_D_In;
  input [5:0]Tx_Ce;
  input Tx_Clk;
  input [5:0]Tx_Inc;
  input [5:0]Tx_Load;
  input [53:0]Tx_CntValueIn;
  output [53:0]Tx_CntValueOut;
  input TxTri_Ce;
  input TxTri_Clk;
  input TxTri_Inc;
  input TxTri_Load;
  input [8:0]TxTri_CntValueIn;
  output [8:0]TxTri_CntValueOut;

  wire \<const0> ;
  wire [39:0]RX_BIT_CTRL_IN4;
  wire [39:0]RX_BIT_CTRL_OUT4;
  wire [39:0]TX_BIT_CTRL_IN4;
  wire [39:0]TX_BIT_CTRL_OUT4;
  wire Tx_Bs_En_Vtc;
  wire Tx_Bs_Rst;
  wire Tx_Bsc_En_Vtc;
  wire Tx_Bsc_Rst;
  wire [5:0]Tx_Ce;
  wire Tx_Clk;
  wire Tx_Clk_From_Ext;
  wire Tx_Clk_To_Ext_North;
  wire Tx_Clk_To_Ext_South;
  wire [53:0]Tx_CntValueIn;
  wire [44:36]\^Tx_CntValueOut ;
  wire [47:0]Tx_D_In;
  wire [4:4]\^Tx_Data_Out ;
  wire Tx_Dly_Rdy;
  wire [6:0]Tx_Dyn_Dci;
  wire [5:0]Tx_Inc;
  wire [5:0]Tx_Load;
  wire Tx_Nclk_Nibble_In;
  wire Tx_Nclk_Nibble_Out;
  wire Tx_Pclk_Nibble_In;
  wire Tx_Pclk_Nibble_Out;
  wire Tx_Pll_Clk;
  wire Tx_RefClk;
  wire [5:0]Tx_Riu_Addr;
  wire Tx_Riu_Clk;
  wire Tx_Riu_Nibble_Sel;
  wire [15:0]Tx_Riu_Rd_Data;
  wire Tx_Riu_Valid;
  wire [15:0]Tx_Riu_Wr_Data;
  wire Tx_Riu_Wr_En;
  wire Tx_Rst_Dly;
  wire [5:0]Tx_T_In;
  wire [3:0]Tx_Tbyte_In;
  wire [4:4]\^Tx_Tri_Out ;
  wire Tx_Vtc_Rdy;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_IN6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT0_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT1_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_IN6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT0_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT1_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED ;
  wire [39:0]\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED ;

  assign TxTri_CntValueOut[8] = \<const0> ;
  assign TxTri_CntValueOut[7] = \<const0> ;
  assign TxTri_CntValueOut[6] = \<const0> ;
  assign TxTri_CntValueOut[5] = \<const0> ;
  assign TxTri_CntValueOut[4] = \<const0> ;
  assign TxTri_CntValueOut[3] = \<const0> ;
  assign TxTri_CntValueOut[2] = \<const0> ;
  assign TxTri_CntValueOut[1] = \<const0> ;
  assign TxTri_CntValueOut[0] = \<const0> ;
  assign Tx_CntValueOut[53] = \<const0> ;
  assign Tx_CntValueOut[52] = \<const0> ;
  assign Tx_CntValueOut[51] = \<const0> ;
  assign Tx_CntValueOut[50] = \<const0> ;
  assign Tx_CntValueOut[49] = \<const0> ;
  assign Tx_CntValueOut[48] = \<const0> ;
  assign Tx_CntValueOut[47] = \<const0> ;
  assign Tx_CntValueOut[46] = \<const0> ;
  assign Tx_CntValueOut[45] = \<const0> ;
  assign Tx_CntValueOut[44:36] = \^Tx_CntValueOut [44:36];
  assign Tx_CntValueOut[35] = \<const0> ;
  assign Tx_CntValueOut[34] = \<const0> ;
  assign Tx_CntValueOut[33] = \<const0> ;
  assign Tx_CntValueOut[32] = \<const0> ;
  assign Tx_CntValueOut[31] = \<const0> ;
  assign Tx_CntValueOut[30] = \<const0> ;
  assign Tx_CntValueOut[29] = \<const0> ;
  assign Tx_CntValueOut[28] = \<const0> ;
  assign Tx_CntValueOut[27] = \<const0> ;
  assign Tx_CntValueOut[26] = \<const0> ;
  assign Tx_CntValueOut[25] = \<const0> ;
  assign Tx_CntValueOut[24] = \<const0> ;
  assign Tx_CntValueOut[23] = \<const0> ;
  assign Tx_CntValueOut[22] = \<const0> ;
  assign Tx_CntValueOut[21] = \<const0> ;
  assign Tx_CntValueOut[20] = \<const0> ;
  assign Tx_CntValueOut[19] = \<const0> ;
  assign Tx_CntValueOut[18] = \<const0> ;
  assign Tx_CntValueOut[17] = \<const0> ;
  assign Tx_CntValueOut[16] = \<const0> ;
  assign Tx_CntValueOut[15] = \<const0> ;
  assign Tx_CntValueOut[14] = \<const0> ;
  assign Tx_CntValueOut[13] = \<const0> ;
  assign Tx_CntValueOut[12] = \<const0> ;
  assign Tx_CntValueOut[11] = \<const0> ;
  assign Tx_CntValueOut[10] = \<const0> ;
  assign Tx_CntValueOut[9] = \<const0> ;
  assign Tx_CntValueOut[8] = \<const0> ;
  assign Tx_CntValueOut[7] = \<const0> ;
  assign Tx_CntValueOut[6] = \<const0> ;
  assign Tx_CntValueOut[5] = \<const0> ;
  assign Tx_CntValueOut[4] = \<const0> ;
  assign Tx_CntValueOut[3] = \<const0> ;
  assign Tx_CntValueOut[2] = \<const0> ;
  assign Tx_CntValueOut[1] = \<const0> ;
  assign Tx_CntValueOut[0] = \<const0> ;
  assign Tx_Data_Out[5] = \<const0> ;
  assign Tx_Data_Out[4] = \^Tx_Data_Out [4];
  assign Tx_Data_Out[3] = \<const0> ;
  assign Tx_Data_Out[2] = \<const0> ;
  assign Tx_Data_Out[1] = \<const0> ;
  assign Tx_Data_Out[0] = \<const0> ;
  assign Tx_Tri_Out[5] = \<const0> ;
  assign Tx_Tri_Out[4] = \^Tx_Tri_Out [4];
  assign Tx_Tri_Out[3] = \<const0> ;
  assign Tx_Tri_Out[2] = \<const0> ;
  assign Tx_Tri_Out[1] = \<const0> ;
  assign Tx_Tri_Out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* box_type = "PRIMITIVE" *) 
  BITSLICE_CONTROL #(
    .CTRL_CLK("EXTERNAL"),
    .DIV_MODE("DIV4"),
    .EN_CLK_TO_EXT_NORTH("DISABLE"),
    .EN_CLK_TO_EXT_SOUTH("DISABLE"),
    .EN_DYN_ODLY_MODE("FALSE"),
    .EN_OTHER_NCLK("FALSE"),
    .EN_OTHER_PCLK("FALSE"),
    .IDLY_VT_TRACK("FALSE"),
    .INV_RXCLK("FALSE"),
    .ODLY_VT_TRACK("FALSE"),
    .QDLY_VT_TRACK("FALSE"),
    .READ_IDLE_COUNT(6'h00),
    .REFCLK_SRC("PLLCLK"),
    .ROUNDING_FACTOR(16),
    .RXGATE_EXTEND("FALSE"),
    .RX_CLK_PHASE_N("SHIFT_0"),
    .RX_CLK_PHASE_P("SHIFT_0"),
    .RX_GATING("DISABLE"),
    .SELF_CALIBRATE("ENABLE"),
    .SERIAL_MODE("FALSE"),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_SPEEDUP("FAST"),
    .SIM_VERSION(2.000000),
    .TX_GATING("ENABLE")) 
    \Gen_1.Nibble_I_BitsliceCntrl 
       (.CLK_FROM_EXT(Tx_Clk_From_Ext),
        .CLK_TO_EXT_NORTH(Tx_Clk_To_Ext_North),
        .CLK_TO_EXT_SOUTH(Tx_Clk_To_Ext_South),
        .DLY_RDY(Tx_Dly_Rdy),
        .DYN_DCI(Tx_Dyn_Dci),
        .EN_VTC(Tx_Bsc_En_Vtc),
        .NCLK_NIBBLE_IN(Tx_Nclk_Nibble_In),
        .NCLK_NIBBLE_OUT(Tx_Nclk_Nibble_Out),
        .PCLK_NIBBLE_IN(Tx_Pclk_Nibble_In),
        .PCLK_NIBBLE_OUT(Tx_Pclk_Nibble_Out),
        .PHY_RDCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDCS1({1'b0,1'b0,1'b0,1'b0}),
        .PHY_RDEN({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS0({1'b0,1'b0,1'b0,1'b0}),
        .PHY_WRCS1({1'b0,1'b0,1'b0,1'b0}),
        .PLL_CLK(Tx_Pll_Clk),
        .REFCLK(Tx_RefClk),
        .RIU_ADDR(Tx_Riu_Addr),
        .RIU_CLK(Tx_Riu_Clk),
        .RIU_NIBBLE_SEL(Tx_Riu_Nibble_Sel),
        .RIU_RD_DATA(Tx_Riu_Rd_Data),
        .RIU_VALID(Tx_Riu_Valid),
        .RIU_WR_DATA(Tx_Riu_Wr_Data),
        .RIU_WR_EN(Tx_Riu_Wr_En),
        .RST(Tx_Bsc_Rst),
        .RX_BIT_CTRL_IN0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN4(RX_BIT_CTRL_IN4),
        .RX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RX_BIT_CTRL_IN6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_IN6_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT0(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT0_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT1(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT1_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT4(RX_BIT_CTRL_OUT4),
        .RX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .RX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_RX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TBYTE_IN(Tx_Tbyte_In),
        .TX_BIT_CTRL_IN0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN4(TX_BIT_CTRL_IN4),
        .TX_BIT_CTRL_IN5({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_IN6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_IN6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_IN_TRI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX_BIT_CTRL_OUT0(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT0_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT1(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT1_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT2(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT2_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT3(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT3_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT4(TX_BIT_CTRL_OUT4),
        .TX_BIT_CTRL_OUT5(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT5_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT6(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT6_UNCONNECTED [39:0]),
        .TX_BIT_CTRL_OUT_TRI(\NLW_Gen_1.Nibble_I_BitsliceCntrl_TX_BIT_CTRL_OUT_TRI_UNCONNECTED [39:0]),
        .VTC_RDY(Tx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  TX_BITSLICE #(
    .DATA_WIDTH(8),
    .DELAY_FORMAT("TIME"),
    .DELAY_TYPE("FIXED"),
    .DELAY_VALUE(0),
    .ENABLE_PRE_EMPHASIS("FALSE"),
    .INIT(1'b0),
    .IS_CLK_INVERTED(1'b0),
    .IS_RST_DLY_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .NATIVE_ODELAY_BYPASS("FALSE"),
    .OUTPUT_PHASE_90("TRUE"),
    .REFCLK_FREQUENCY(1250.000000),
    .SIM_DEVICE("ULTRASCALE_PLUS_ES1"),
    .SIM_VERSION(2.000000),
    .TBYTE_CTL("T"),
    .UPDATE_MODE("ASYNC")) 
    \Gen_7[5].Gen_7_1.Nibble_I_TxBitslice 
       (.CE(Tx_Ce[4]),
        .CLK(Tx_Clk),
        .CNTVALUEIN(Tx_CntValueIn[44:36]),
        .CNTVALUEOUT(\^Tx_CntValueOut ),
        .D(Tx_D_In[39:32]),
        .EN_VTC(Tx_Bs_En_Vtc),
        .INC(Tx_Inc[4]),
        .LOAD(Tx_Load[4]),
        .O(\^Tx_Data_Out ),
        .RST(Tx_Bs_Rst),
        .RST_DLY(Tx_Rst_Dly),
        .RX_BIT_CTRL_IN(RX_BIT_CTRL_OUT4),
        .RX_BIT_CTRL_OUT(RX_BIT_CTRL_IN4),
        .T(Tx_T_In[4]),
        .TBYTE_IN(1'b0),
        .TX_BIT_CTRL_IN(TX_BIT_CTRL_OUT4),
        .TX_BIT_CTRL_OUT(TX_BIT_CTRL_IN4),
        .T_OUT(\^Tx_Tri_Out ));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_block
   (Tx_Dly_Rdy,
    Tx_Vtc_Rdy,
    Rx_Dly_Rdy,
    Rx_Vtc_Rdy,
    sgmii_clk_r_0,
    sgmii_clk_en_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    sgmii_clk_f_0,
    gmii_isolate_0,
    an_interrupt_0,
    status_vector_0,
    riu_rd_data,
    riu_valid,
    riu_prsnt,
    txp_0,
    txn_0,
    Rx_SysClk,
    tx_dly_rdy_3,
    tx_dly_rdy_1,
    tx_dly_rdy_2,
    tx_vtc_rdy_3,
    tx_vtc_rdy_1,
    tx_vtc_rdy_2,
    rx_dly_rdy_3,
    rx_dly_rdy_1,
    rx_dly_rdy_2,
    rx_vtc_rdy_3,
    rx_vtc_rdy_1,
    rx_vtc_rdy_2,
    Tx_WrClk,
    speed_is_10_100_0,
    speed_is_100_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    reset_out,
    signal_detect_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    configuration_vector_0,
    CLK,
    D,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    tx_pll_clk_out,
    rx_pll_clk_out,
    rxp_0,
    rxn_0);
  output Tx_Dly_Rdy;
  output Tx_Vtc_Rdy;
  output Rx_Dly_Rdy;
  output Rx_Vtc_Rdy;
  output sgmii_clk_r_0;
  output sgmii_clk_en_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output sgmii_clk_f_0;
  output gmii_isolate_0;
  output an_interrupt_0;
  output [12:0]status_vector_0;
  output [15:0]riu_rd_data;
  output riu_valid;
  output riu_prsnt;
  output txp_0;
  output txn_0;
  input Rx_SysClk;
  input tx_dly_rdy_3;
  input tx_dly_rdy_1;
  input tx_dly_rdy_2;
  input tx_vtc_rdy_3;
  input tx_vtc_rdy_1;
  input tx_vtc_rdy_2;
  input rx_dly_rdy_3;
  input rx_dly_rdy_1;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_3;
  input rx_vtc_rdy_1;
  input rx_vtc_rdy_2;
  input Tx_WrClk;
  input speed_is_10_100_0;
  input speed_is_100_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  input reset_out;
  input signal_detect_0;
  input [0:0]an_adv_config_vector_0;
  input an_restart_config_0;
  input [4:0]configuration_vector_0;
  input CLK;
  input [5:0]D;
  input tx_bsc_rst_out;
  input rx_bsc_rst_out;
  input tx_bs_rst_out;
  input rx_bs_rst_out;
  input tx_rst_dly_out;
  input rx_rst_dly_out;
  input tx_bsc_en_vtc_out;
  input rx_bsc_en_vtc_out;
  input tx_bs_en_vtc_out;
  input rx_bs_en_vtc_out;
  input riu_clk_out;
  input [5:0]riu_addr_out;
  input [15:0]riu_wr_data_out;
  input riu_wr_en_out;
  input [1:0]riu_nibble_sel_out;
  input tx_pll_clk_out;
  input rx_pll_clk_out;
  input rxp_0;
  input rxn_0;

  wire ActiveIsSlve_i_1_n_0;
  wire [1:0]BaseX_Rx_Data_In;
  wire [7:0]BaseX_Rx_Q_Out;
  wire [4:4]BaseX_Tx_Data_Out;
  wire CLK;
  wire [5:0]D;
  wire LossOfSignal_i_1_n_0;
  wire Mstr_Load_i_1_n_0;
  wire Rx_Dly_Rdy;
  wire Rx_Dly_Rdy_Int;
  wire Rx_SysClk;
  wire Rx_Vtc_Rdy;
  wire Rx_Vtc_Rdy_Int;
  wire Slve_Load_i_1_n_0;
  wire Tx_Dly_Rdy;
  wire Tx_Dly_Rdy_Int;
  wire Tx_Vtc_Rdy;
  wire Tx_Vtc_Rdy_Int;
  wire Tx_WrClk;
  wire WrapToZero_i_1_n_0;
  wire al_rx_valid_out;
  wire [0:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire [4:0]configuration_vector_0;
  wire [1:0]fifo_empty;
  wire fifo_read_0;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ;
  wire \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_dv_int;
  wire gmii_rx_er_0;
  wire gmii_rx_er_int;
  wire [7:0]gmii_rxd_0;
  wire [7:0]gmii_rxd_int;
  wire gmii_tx_en_0;
  wire gmii_tx_en_int;
  wire gmii_tx_er_0;
  wire gmii_tx_er_int;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_int;
  wire insert3_i_1_n_0;
  wire insert5_i_1_n_0;
  wire mgt_rx_reset;
  wire mgt_tx_reset_0;
  wire mload;
  wire monitor_late_i_1_n_0;
  wire reset_out;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt;
  wire [15:0]riu_rd_data;
  wire riu_valid;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire \rx_elastic_buffer_inst/initialize_ram_complete ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_pulse ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ;
  wire \rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ;
  wire \rx_elastic_buffer_inst/insert_idle_reg__0 ;
  wire \rx_elastic_buffer_inst/remove_idle ;
  wire \rx_elastic_buffer_inst/remove_idle_reg__0 ;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [2:0]rxclkcorcnt;
  wire \rxclkcorcnt[0]_i_1_n_0 ;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn_0;
  wire rxnotintable;
  wire rxp_0;
  wire rxrecreset0;
  wire rxrundisp;
  wire \serdes_1_to_10_i/ActCnt_GE_HalfBT ;
  wire \serdes_1_to_10_i/ActiveIsSlve ;
  wire \serdes_1_to_10_i/D0 ;
  wire \serdes_1_to_10_i/LossOfSignal ;
  wire \serdes_1_to_10_i/WrapToZero ;
  wire [5:5]\serdes_1_to_10_i/act_count_reg ;
  wire \serdes_1_to_10_i/p_0_in ;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire sload;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [12:0]status_vector_0;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire [7:0]tx_data_8b;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_pll_clk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn_0;
  wire txp_0;
  wire \wr_addr[6]_i_2_n_0 ;
  wire [62:0]NLW_gen_io_logic_BaseX_Idly_CntValueOut_UNCONNECTED;
  wire [53:0]NLW_gen_io_logic_BaseX_Odly_CntValueOut_UNCONNECTED;
  wire [6:2]NLW_gen_io_logic_BaseX_Rx_Fifo_Empty_UNCONNECTED;
  wire [6:0]NLW_gen_io_logic_BaseX_Rx_Q_CombOut_UNCONNECTED;
  wire [27:8]NLW_gen_io_logic_BaseX_Rx_Q_Out_UNCONNECTED;
  wire [8:0]NLW_gen_io_logic_BaseX_TriOdly_CntValueOut_UNCONNECTED;
  wire [5:0]NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED;
  wire [5:0]NLW_gen_io_logic_BaseX_Tx_Tri_Out_UNCONNECTED;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_an_enable_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_den_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_dwe_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_req_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_en_cdet_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_enablealign_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_ewrap_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_loc_ref_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_out_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_tri_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_powerdown_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_arready_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_awready_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bvalid_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rvalid_UNCONNECTED ;
  wire \NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_wready_UNCONNECTED ;
  wire [9:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_daddr_UNCONNECTED ;
  wire [15:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_di_UNCONNECTED ;
  wire [63:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_correction_timer_UNCONNECTED ;
  wire [31:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_ns_field_UNCONNECTED ;
  wire [47:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_s_field_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bresp_UNCONNECTED ;
  wire [31:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rdata_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rresp_UNCONNECTED ;
  wire [1:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_speed_selection_UNCONNECTED ;
  wire [15:8]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED ;
  wire [9:0]\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_tx_code_group_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    ActiveIsSlve_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ),
        .I3(\serdes_1_to_10_i/p_0_in ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .I5(\serdes_1_to_10_i/ActiveIsSlve ),
        .O(ActiveIsSlve_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    FifoRd_0_i_1
       (.I0(fifo_empty[0]),
        .I1(fifo_empty[1]),
        .O(\serdes_1_to_10_i/D0 ));
  LUT6 #(
    .INIT(64'hF0F0AAAAAABAAABA)) 
    LossOfSignal_i_1
       (.I0(\serdes_1_to_10_i/LossOfSignal ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ),
        .I5(\serdes_1_to_10_i/act_count_reg ),
        .O(LossOfSignal_i_1_n_0));
  LUT6 #(
    .INIT(64'hBBABBAAB88A88AA8)) 
    Mstr_Load_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .I3(\serdes_1_to_10_i/ActiveIsSlve ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ),
        .O(Mstr_Load_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAFFEAAAAA002)) 
    Slve_Load_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .I3(\serdes_1_to_10_i/ActiveIsSlve ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ),
        .O(Slve_Load_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFAF00000020)) 
    WrapToZero_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ),
        .I2(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .I3(\serdes_1_to_10_i/p_0_in ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\serdes_1_to_10_i/WrapToZero ),
        .O(WrapToZero_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_1
       (.I0(tx_dly_rdy_3),
        .I1(Tx_Dly_Rdy_Int),
        .I2(tx_dly_rdy_1),
        .I3(tx_dly_rdy_2),
        .O(Tx_Dly_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_2
       (.I0(tx_vtc_rdy_3),
        .I1(Tx_Vtc_Rdy_Int),
        .I2(tx_vtc_rdy_1),
        .I3(tx_vtc_rdy_2),
        .O(Tx_Vtc_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_3
       (.I0(rx_dly_rdy_3),
        .I1(Rx_Dly_Rdy_Int),
        .I2(rx_dly_rdy_1),
        .I3(rx_dly_rdy_2),
        .O(Rx_Dly_Rdy));
  LUT4 #(
    .INIT(16'h8000)) 
    clock_reset_i_i_4
       (.I0(rx_vtc_rdy_3),
        .I1(Rx_Vtc_Rdy_Int),
        .I2(rx_vtc_rdy_1),
        .I3(rx_vtc_rdy_2),
        .O(Rx_Vtc_Rdy));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_DIFF_OUT #(
    .DIFF_TERM("FALSE"),
    .IOSTANDARD("DEFAULT")) 
    \gen_IOB.gen_IOB[0].data_in 
       (.I(rxp_0),
        .IB(rxn_0),
        .O(BaseX_Rx_Data_In[0]),
        .OB(BaseX_Rx_Data_In[1]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \gen_IOB.gen_IOB[0].io_data_out 
       (.I(BaseX_Tx_Data_Out),
        .O(txp_0),
        .OB(txn_0));
  (* C_BytePosition = "0" *) 
  (* C_IoBank = "44" *) 
  (* C_Part = "XCKU060" *) 
  (* C_Rx_BtslcNulType = "SERIAL" *) 
  (* C_Rx_Data_Width = "4" *) 
  (* C_Rx_Delay_Format = "COUNT" *) 
  (* C_Rx_Delay_Type = "VAR_LOAD" *) 
  (* C_Rx_Delay_Value = "0" *) 
  (* C_Rx_RefClk_Frequency = "312.500000" *) 
  (* C_Rx_Self_Calibrate = "ENABLE" *) 
  (* C_Rx_Serial_Mode = "TRUE" *) 
  (* C_Rx_UsedBitslices = "7'b0000011" *) 
  (* C_TxInUpperNibble = "0" *) 
  (* C_Tx_BtslceTr = "T" *) 
  (* C_Tx_BtslceUsedAsT = "7'b0000000" *) 
  (* C_Tx_Data_Width = "8" *) 
  (* C_Tx_Delay_Format = "TIME" *) 
  (* C_Tx_Delay_Type = "FIXED" *) 
  (* C_Tx_Delay_Value = "0" *) 
  (* C_Tx_RefClk_Frequency = "1250.000000" *) 
  (* C_Tx_Self_Calibrate = "ENABLE" *) 
  (* C_Tx_Serial_Mode = "FALSE" *) 
  (* C_Tx_UsedBitslices = "7'b0010000" *) 
  (* C_UseRxRiu = "1" *) 
  (* C_UseTxRiu = "1" *) 
  (* DONT_TOUCH *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_BaseX_Byte gen_io_logic
       (.BaseX_Dly_Clk(Rx_SysClk),
        .BaseX_Idly_Ce({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Idly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 }),
        .BaseX_Idly_CntValueOut(NLW_gen_io_logic_BaseX_Idly_CntValueOut_UNCONNECTED[62:0]),
        .BaseX_Idly_Inc({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Idly_Load({1'b0,1'b0,1'b0,1'b0,1'b0,sload,mload}),
        .BaseX_Odly_Ce({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_CntValueOut(NLW_gen_io_logic_BaseX_Odly_CntValueOut_UNCONNECTED[53:0]),
        .BaseX_Odly_Inc({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Odly_Load({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Riu_Addr(riu_addr_out),
        .BaseX_Riu_Clk(riu_clk_out),
        .BaseX_Riu_Nibble_Sel(riu_nibble_sel_out),
        .BaseX_Riu_Prsnt(riu_prsnt),
        .BaseX_Riu_Rd_Data(riu_rd_data),
        .BaseX_Riu_Valid(riu_valid),
        .BaseX_Riu_Wr_Data(riu_wr_data_out),
        .BaseX_Riu_Wr_En(riu_wr_en_out),
        .BaseX_Rx_Bs_En_Vtc(rx_bs_en_vtc_out),
        .BaseX_Rx_Bs_Rst(rx_bs_rst_out),
        .BaseX_Rx_Bsc_En_Vtc(rx_bsc_en_vtc_out),
        .BaseX_Rx_Bsc_Rst(rx_bsc_rst_out),
        .BaseX_Rx_Data_In({1'b0,1'b0,1'b0,1'b0,1'b0,BaseX_Rx_Data_In}),
        .BaseX_Rx_Dly_Rdy(Rx_Dly_Rdy_Int),
        .BaseX_Rx_Fifo_Empty({NLW_gen_io_logic_BaseX_Rx_Fifo_Empty_UNCONNECTED[6:2],fifo_empty}),
        .BaseX_Rx_Fifo_Rd_Clk(Rx_SysClk),
        .BaseX_Rx_Fifo_Rd_En({1'b0,1'b0,1'b0,1'b0,1'b0,fifo_read_0,fifo_read_0}),
        .BaseX_Rx_Phy_Rden({1'b1,1'b1,1'b1,1'b1}),
        .BaseX_Rx_Pll_Clk(rx_pll_clk_out),
        .BaseX_Rx_Q_CombOut(NLW_gen_io_logic_BaseX_Rx_Q_CombOut_UNCONNECTED[6:0]),
        .BaseX_Rx_Q_Out({NLW_gen_io_logic_BaseX_Rx_Q_Out_UNCONNECTED[27:8],BaseX_Rx_Q_Out}),
        .BaseX_Rx_Rst_Dly(rx_rst_dly_out),
        .BaseX_Rx_Vtc_Rdy(Rx_Vtc_Rdy_Int),
        .BaseX_TriOdly_Ce(1'b0),
        .BaseX_TriOdly_CntValueIn({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_TriOdly_CntValueOut(NLW_gen_io_logic_BaseX_TriOdly_CntValueOut_UNCONNECTED[8:0]),
        .BaseX_TriOdly_Inc(1'b0),
        .BaseX_TriOdly_Load(1'b0),
        .BaseX_Tx_Bs_En_Vtc(tx_bs_en_vtc_out),
        .BaseX_Tx_Bs_Rst(tx_bs_rst_out),
        .BaseX_Tx_Bsc_En_Vtc(tx_bsc_en_vtc_out),
        .BaseX_Tx_Bsc_Rst(tx_bsc_rst_out),
        .BaseX_Tx_D_In({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,tx_data_8b,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Data_Out({NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED[5],BaseX_Tx_Data_Out,NLW_gen_io_logic_BaseX_Tx_Data_Out_UNCONNECTED[3:0]}),
        .BaseX_Tx_Dly_Rdy(Tx_Dly_Rdy_Int),
        .BaseX_Tx_Phy_Rden({1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Pll_Clk(tx_pll_clk_out),
        .BaseX_Tx_Rst_Dly(tx_rst_dly_out),
        .BaseX_Tx_T_In({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_TbyteIn({1'b0,1'b0,1'b0,1'b0}),
        .BaseX_Tx_Tri_Out(NLW_gen_io_logic_BaseX_Tx_Tri_Out_UNCONNECTED[5:0]),
        .BaseX_Tx_Vtc_Rdy(Tx_Vtc_Rdy_Int),
        .Tx_RdClk(CLK));
  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "virtexuplus" *) 
  (* C_HAS_AN = "TRUE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "TRUE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "TRUE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "FALSE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_gig_ethernet_pcs_pma_v16_2_8 \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core 
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,an_adv_config_vector_0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_an_enable_UNCONNECTED ),
        .an_interrupt(an_interrupt_0),
        .an_restart_config(an_restart_config_0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector(configuration_vector_0),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_daddr_UNCONNECTED [9:0]),
        .drp_dclk(1'b0),
        .drp_den(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_den_UNCONNECTED ),
        .drp_di(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_di_UNCONNECTED [15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_dwe_UNCONNECTED ),
        .drp_gnt(1'b0),
        .drp_req(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_drp_req_UNCONNECTED ),
        .en_cdet(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_en_cdet_UNCONNECTED ),
        .enablealign(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_enablealign_UNCONNECTED ),
        .ewrap(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_ewrap_UNCONNECTED ),
        .gmii_isolate(gmii_isolate_0),
        .gmii_rx_dv(gmii_rx_dv_int),
        .gmii_rx_er(gmii_rx_er_int),
        .gmii_rxd(gmii_rxd_int),
        .gmii_tx_en(gmii_tx_en_int),
        .gmii_tx_er(gmii_tx_er_int),
        .gmii_txd(gmii_txd_int),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0}),
        .loc_ref(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_loc_ref_UNCONNECTED ),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_out_UNCONNECTED ),
        .mdio_tri(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_mdio_tri_UNCONNECTED ),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset_0),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_powerdown_UNCONNECTED ),
        .reset(reset_out),
        .reset_done(1'b1),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({rxclkcorcnt[2],1'b0,rxclkcorcnt[0]}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_correction_timer_UNCONNECTED [63:0]),
        .rxphy_ns_field(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_ns_field_UNCONNECTED [31:0]),
        .rxphy_s_field(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_rxphy_s_field_UNCONNECTED [47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(rxrundisp),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_arready_UNCONNECTED ),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_awready_UNCONNECTED ),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bresp_UNCONNECTED [1:0]),
        .s_axi_bvalid(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_bvalid_UNCONNECTED ),
        .s_axi_rdata(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rdata_UNCONNECTED [31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rresp_UNCONNECTED [1:0]),
        .s_axi_rvalid(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_rvalid_UNCONNECTED ),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_s_axi_wready_UNCONNECTED ),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect_0),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_speed_selection_UNCONNECTED [1:0]),
        .status_vector({\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED [15:14],status_vector_0[12:8],\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_status_vector_UNCONNECTED [8],status_vector_0[7:0]}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(\NLW_gen_lvds_transceiver.gen_lvds_transceiver_logic[0].gig_eth_pcs_pma_gmii_to_sgmii_bridge_core_tx_code_group_UNCONNECTED [9:0]),
        .txbuferr(1'b0),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(Tx_WrClk));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_lvds_transceiver \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst 
       (.ActCnt_GE_HalfBT(\serdes_1_to_10_i/ActCnt_GE_HalfBT ),
        .ActiveIsSlve(\serdes_1_to_10_i/ActiveIsSlve ),
        .ActiveIsSlve_reg(ActiveIsSlve_i_1_n_0),
        .BaseX_Idly_Load({sload,mload}),
        .BaseX_Rx_Fifo_Rd_En(fifo_read_0),
        .BaseX_Rx_Q_Out(BaseX_Rx_Q_Out),
        .CLK(CLK),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_20 ),
        .D0(\serdes_1_to_10_i/D0 ),
        .\IntRx_BtVal_reg[8] (D),
        .LossOfSignal(\serdes_1_to_10_i/LossOfSignal ),
        .LossOfSignal_reg(LossOfSignal_i_1_n_0),
        .Mstr_Load_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_21 ),
        .Mstr_Load_reg_0(Mstr_Load_i_1_n_0),
        .Q({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_26 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_27 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_28 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_29 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_30 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_31 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_32 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_33 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_34 }),
        .Rx_SysClk(Rx_SysClk),
        .\Slve_CntValIn_Out_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_49 ),
        .\Slve_CntValIn_Out_reg[1] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_48 ),
        .\Slve_CntValIn_Out_reg[8] ({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_41 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_42 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_43 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_44 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_45 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_46 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_47 }),
        .Slve_Load_reg(Slve_Load_i_1_n_0),
        .Tx_WrClk(Tx_WrClk),
        .WrapToZero(\serdes_1_to_10_i/WrapToZero ),
        .WrapToZero_reg(WrapToZero_i_1_n_0),
        .\act_count_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_79 ),
        .\act_count_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_81 ),
        .\act_count_reg[4] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_80 ),
        .\act_count_reg[5] (\serdes_1_to_10_i/act_count_reg ),
        .\active_reg[1] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_78 ),
        .al_rx_valid_out(al_rx_valid_out),
        .\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ),
        .\d21p5_wr_pipe_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ),
        .\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ),
        .\d2p2_wr_pipe_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ),
        .data_out(\rx_elastic_buffer_inst/initialize_ram_complete_sync ),
        .initialize_ram_complete(\rx_elastic_buffer_inst/initialize_ram_complete ),
        .initialize_ram_complete_pulse(\rx_elastic_buffer_inst/initialize_ram_complete_pulse ),
        .initialize_ram_complete_sync_reg1(\rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ),
        .initialize_ram_complete_sync_ris_edg0(\rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ),
        .insert3_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ),
        .insert3_reg_0(insert3_i_1_n_0),
        .insert5_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ),
        .insert5_reg_0(insert5_i_1_n_0),
        .insert_idle_reg__0(\rx_elastic_buffer_inst/insert_idle_reg__0 ),
        .mgt_rx_reset(mgt_rx_reset),
        .monitor_late_reg(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ),
        .monitor_late_reg_0(monitor_late_i_1_n_0),
        .\rd_data_reg_reg[13] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ),
        .remove_idle(\rx_elastic_buffer_inst/remove_idle ),
        .remove_idle_reg__0(\rx_elastic_buffer_inst/remove_idle_reg__0 ),
        .reset_out(reset_out),
        .rxbufstatus(rxbuferr),
        .rxchariscomma_usr_reg(rxchariscomma),
        .rxcharisk_usr_reg(rxcharisk),
        .rxclkcorcnt({rxclkcorcnt[2],rxclkcorcnt[0]}),
        .\rxclkcorcnt_reg[0] (\rxclkcorcnt[0]_i_1_n_0 ),
        .\rxdata_usr_reg[7] (rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxrecreset0(rxrecreset0),
        .rxrundisp(rxrundisp),
        .\s_state_reg[0] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ),
        .\s_state_reg[0]_0 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_77 ),
        .\s_state_reg[2] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_75 ),
        .\s_state_reg[3] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .\s_state_reg[4] ({\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ,\serdes_1_to_10_i/p_0_in ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_38 ,\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_39 }),
        .\s_state_reg[4]_0 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_74 ),
        .\s_state_reg[4]_1 (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_76 ),
        .\s_state_reg[5] (\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_73 ),
        .\tx_data_8b_reg[7]_0 (tx_data_8b),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .\wr_addr_plus2_reg[6] (\wr_addr[6]_i_2_n_0 ));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sgmii_adapt \gen_lvds_transceiver.gen_lvds_transceiver_logic[0].sgmii_logic 
       (.Tx_WrClk(Tx_WrClk),
        .gmii_rx_dv(gmii_rx_dv_int),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rx_er_in(gmii_rx_er_int),
        .gmii_rxd(gmii_rxd_int),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_en_out(gmii_tx_en_int),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_tx_er_out(gmii_tx_er_int),
        .gmii_txd_0(gmii_txd_0),
        .gmii_txd_out(gmii_txd_int),
        .mgt_tx_reset(mgt_tx_reset_0),
        .sgmii_clk_en(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate 
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_1 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .O(\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0 
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_3 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .O(\gen_lvds_transceiver.gen_lvds_transceiver_logic_gate__0_n_0 ));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(1'b1),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ),
        .R(rxrecreset0));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_n_0 ),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .R(rxrecreset0));
  FDRE \gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_n_0 ),
        .R(rxrecreset0));
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_complete_sync_ris_edg_i_1
       (.I0(\rx_elastic_buffer_inst/initialize_ram_complete_sync ),
        .I1(\rx_elastic_buffer_inst/initialize_ram_complete_sync_reg1 ),
        .O(\rx_elastic_buffer_inst/initialize_ram_complete_sync_ris_edg0 ));
  LUT6 #(
    .INIT(64'hFFFFF5F50020A0A0)) 
    insert3_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I2(\serdes_1_to_10_i/p_0_in ),
        .I3(\serdes_1_to_10_i/WrapToZero ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_25 ),
        .O(insert3_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFD5D500808080)) 
    insert5_i_1
       (.I0(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_82 ),
        .I1(\serdes_1_to_10_i/p_0_in ),
        .I2(\serdes_1_to_10_i/WrapToZero ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_37 ),
        .I5(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_24 ),
        .O(insert5_i_1_n_0));
  LUT5 #(
    .INIT(32'h8BFF8B00)) 
    monitor_late_i_1
       (.I0(\serdes_1_to_10_i/WrapToZero ),
        .I1(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_35 ),
        .I2(\serdes_1_to_10_i/ActCnt_GE_HalfBT ),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_72 ),
        .I4(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_23 ),
        .O(monitor_late_i_1_n_0));
  LUT4 #(
    .INIT(16'h4F4C)) 
    \rxclkcorcnt[0]_i_1 
       (.I0(rxclkcorcnt[2]),
        .I1(\rx_elastic_buffer_inst/insert_idle_reg__0 ),
        .I2(rxclkcorcnt[0]),
        .I3(\gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst_n_52 ),
        .O(\rxclkcorcnt[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF5575)) 
    \wr_addr[6]_i_2 
       (.I0(\rx_elastic_buffer_inst/initialize_ram_complete ),
        .I1(\rx_elastic_buffer_inst/remove_idle ),
        .I2(al_rx_valid_out),
        .I3(\rx_elastic_buffer_inst/remove_idle_reg__0 ),
        .I4(\rx_elastic_buffer_inst/initialize_ram_complete_pulse ),
        .O(\wr_addr[6]_i_2_n_0 ));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_clk_gen
   (sgmii_clk_r_0,
    sgmii_clk_en_reg_0,
    sgmii_clk_f_0,
    Tx_WrClk,
    reset_out,
    data_out,
    speed_is_10_100_fall_reg_0);
  output sgmii_clk_r_0;
  output sgmii_clk_en_reg_0;
  output sgmii_clk_f_0;
  input Tx_WrClk;
  input reset_out;
  input data_out;
  input speed_is_10_100_fall_reg_0;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk12_5_reg;
  wire clk1_25;
  wire clk1_25_reg;
  wire clk_div_stage1_n_3;
  wire clk_en;
  wire clk_en0;
  wire clk_en_12_5_fall;
  wire clk_en_12_5_fall0;
  wire clk_en_1_25_fall;
  wire clk_en_1_25_fall0;
  wire data_out;
  wire reset_fall;
  wire reset_out;
  wire sgmii_clk_en_i_1_n_0;
  wire sgmii_clk_en_reg_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r0_out;
  wire sgmii_clk_r_0;
  wire speed_is_100_fall;
  wire speed_is_10_100_fall;
  wire speed_is_10_100_fall_reg_0;

  FDRE clk12_5_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk12_5),
        .Q(clk12_5_reg),
        .R(reset_out));
  FDRE clk1_25_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk1_25),
        .Q(clk1_25_reg),
        .R(reset_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr clk_div_stage1
       (.Tx_WrClk(Tx_WrClk),
        .clk12_5(clk12_5),
        .clk12_5_reg(clk12_5_reg),
        .clk1_25(clk1_25),
        .clk_en0(clk_en0),
        .clk_en_12_5_fall0(clk_en_12_5_fall0),
        .reset_fall(reset_fall),
        .reset_out(reset_out),
        .speed_is_100_fall(speed_is_100_fall),
        .speed_is_10_100_fall(speed_is_10_100_fall),
        .speed_is_10_100_fall_reg(clk_div_stage1_n_3));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr_2 clk_div_stage2
       (.Tx_WrClk(Tx_WrClk),
        .clk12_5(clk12_5),
        .clk1_25(clk1_25),
        .clk1_25_reg(clk1_25_reg),
        .clk_en(clk_en),
        .clk_en_1_25_fall0(clk_en_1_25_fall0),
        .data_out(data_out),
        .reset_out(reset_out),
        .sgmii_clk_r0_out(sgmii_clk_r0_out),
        .sgmii_clk_r_reg(speed_is_10_100_fall_reg_0));
  FDRE clk_en_12_5_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en_12_5_fall0),
        .Q(clk_en_12_5_fall),
        .R(reset_out));
  FDRE clk_en_12_5_rise_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en0),
        .Q(clk_en),
        .R(reset_out));
  FDRE clk_en_1_25_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_en_1_25_fall0),
        .Q(clk_en_1_25_fall),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    reset_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_out),
        .Q(reset_fall),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hE2FF)) 
    sgmii_clk_en_i_1
       (.I0(clk_en_1_25_fall),
        .I1(data_out),
        .I2(clk_en_12_5_fall),
        .I3(speed_is_10_100_fall_reg_0),
        .O(sgmii_clk_en_i_1_n_0));
  FDRE sgmii_clk_en_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sgmii_clk_en_i_1_n_0),
        .Q(sgmii_clk_en_reg_0),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    sgmii_clk_f_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk_div_stage1_n_3),
        .Q(sgmii_clk_f_0),
        .R(1'b0));
  FDRE sgmii_clk_r_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sgmii_clk_r0_out),
        .Q(sgmii_clk_r_0),
        .R(reset_out));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    speed_is_100_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_out),
        .Q(speed_is_100_fall),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    speed_is_10_100_fall_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_10_100_fall_reg_0),
        .Q(speed_is_10_100_fall),
        .R(1'b0));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_decode_8b10b_lut_base
   (D,
    E,
    k,
    Rx_SysClk,
    code_err_i,
    \grdni.run_disp_i_reg_0 ,
    \gde.gdeni.DISP_ERR_reg_0 ,
    b3,
    out);
  output [11:0]D;
  input [0:0]E;
  input k;
  input Rx_SysClk;
  input code_err_i;
  input \grdni.run_disp_i_reg_0 ;
  input \gde.gdeni.DISP_ERR_reg_0 ;
  input [7:5]b3;
  input [4:0]out;

  wire [11:0]D;
  wire [0:0]E;
  wire Rx_SysClk;
  wire [7:5]b3;
  wire code_err_i;
  wire \gde.gdeni.DISP_ERR_reg_0 ;
  wire \grdni.run_disp_i_reg_0 ;
  wire k;
  wire [4:0]out;

  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[0]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[2]),
        .Q(D[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[3]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(out[4]),
        .Q(D[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[5]),
        .Q(D[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[6]),
        .Q(D[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dout_i_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(b3[7]),
        .Q(D[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gcerr.CODE_ERR_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(code_err_i),
        .Q(D[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \gde.gdeni.DISP_ERR_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\gde.gdeni.DISP_ERR_reg_0 ),
        .Q(D[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \grdni.run_disp_i_reg 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\grdni.run_disp_i_reg_0 ),
        .Q(D[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    kout_i_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(k),
        .Q(D[11]),
        .R(1'b0));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_encode_8b10b_lut_base
   (tx_data_10b,
    Tx_WrClk,
    txchardispval,
    txchardispmode,
    txdata,
    txcharisk);
  output [9:0]tx_data_10b;
  input Tx_WrClk;
  input txchardispval;
  input txchardispmode;
  input [7:0]txdata;
  input txcharisk;

  wire \DOUT[0]_i_1_n_0 ;
  wire \DOUT[1]_i_1_n_0 ;
  wire \DOUT[2]_i_1_n_0 ;
  wire \DOUT[3]_i_1_n_0 ;
  wire \DOUT[4]_i_1_n_0 ;
  wire \DOUT[5]_i_1_n_0 ;
  wire \DOUT[5]_i_2_n_0 ;
  wire \DOUT[9]_i_2_n_0 ;
  wire \DOUT[9]_i_6_n_0 ;
  wire \DOUT[9]_i_7_n_0 ;
  wire \DOUT[9]_i_8_n_0 ;
  wire Tx_WrClk;
  wire [3:0]b4;
  wire [5:0]b6;
  wire disp_in_i__0;
  wire k28;
  wire \ngdb.disp_run_reg_n_0 ;
  wire pdes4;
  wire pdes6__13;
  wire [9:0]tx_data_10b;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;

  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \DOUT[0]_i_1 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .I3(k28),
        .I4(b6[0]),
        .O(\DOUT[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2D807F017F01FE4B)) 
    \DOUT[0]_i_2 
       (.I0(txdata[3]),
        .I1(txdata[4]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[1]),
        .I5(txdata[2]),
        .O(b6[0]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \DOUT[1]_i_1 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .I3(k28),
        .I4(b6[1]),
        .O(\DOUT[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h987170F170F171E6)) 
    \DOUT[1]_i_2 
       (.I0(disp_in_i__0),
        .I1(txdata[0]),
        .I2(txdata[1]),
        .I3(txdata[2]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(b6[1]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[2]_i_1 
       (.I0(b6[2]),
        .I1(k28),
        .O(\DOUT[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h47807F09BF01FE16)) 
    \DOUT[2]_i_2 
       (.I0(txdata[4]),
        .I1(txdata[3]),
        .I2(txdata[1]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(disp_in_i__0),
        .O(b6[2]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[3]_i_1 
       (.I0(b6[3]),
        .I1(k28),
        .O(\DOUT[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hB44C4CCD4CCDCDE1)) 
    \DOUT[3]_i_2 
       (.I0(txdata[4]),
        .I1(txdata[3]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[1]),
        .I5(txdata[2]),
        .O(b6[3]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[4]_i_1 
       (.I0(b6[4]),
        .I1(k28),
        .O(\DOUT[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1F6F08107FEF9061)) 
    \DOUT[4]_i_2 
       (.I0(txdata[2]),
        .I1(txdata[1]),
        .I2(disp_in_i__0),
        .I3(txdata[0]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(b6[4]));
  LUT4 #(
    .INIT(16'h02A2)) 
    \DOUT[5]_i_1 
       (.I0(k28),
        .I1(\ngdb.disp_run_reg_n_0 ),
        .I2(txchardispmode),
        .I3(txchardispval),
        .O(\DOUT[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DOUT[5]_i_2 
       (.I0(b6[5]),
        .I1(k28),
        .O(\DOUT[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5996A1179660177F)) 
    \DOUT[5]_i_3 
       (.I0(txdata[3]),
        .I1(txdata[4]),
        .I2(txdata[2]),
        .I3(txdata[1]),
        .I4(disp_in_i__0),
        .I5(txdata[0]),
        .O(b6[5]));
  LUT6 #(
    .INIT(64'h8F8F0000B0BFFF0F)) 
    \DOUT[6]_i_1 
       (.I0(\DOUT[9]_i_2_n_0 ),
        .I1(txdata[7]),
        .I2(txdata[6]),
        .I3(k28),
        .I4(txdata[5]),
        .I5(pdes6__13),
        .O(b4[0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h5B5B0D58)) 
    \DOUT[7]_i_1 
       (.I0(txdata[5]),
        .I1(k28),
        .I2(pdes6__13),
        .I3(txdata[7]),
        .I4(txdata[6]),
        .O(b4[1]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'h66AA9A59)) 
    \DOUT[8]_i_1 
       (.I0(txdata[7]),
        .I1(txdata[6]),
        .I2(k28),
        .I3(txdata[5]),
        .I4(pdes6__13),
        .O(b4[2]));
  LUT6 #(
    .INIT(64'h737330304C43CF3F)) 
    \DOUT[9]_i_1 
       (.I0(\DOUT[9]_i_2_n_0 ),
        .I1(txdata[7]),
        .I2(txdata[6]),
        .I3(k28),
        .I4(txdata[5]),
        .I5(pdes6__13),
        .O(b4[3]));
  LUT6 #(
    .INIT(64'h727272727272728D)) 
    \DOUT[9]_i_2 
       (.I0(k28),
        .I1(disp_in_i__0),
        .I2(\DOUT[9]_i_6_n_0 ),
        .I3(\DOUT[9]_i_7_n_0 ),
        .I4(\DOUT[9]_i_8_n_0 ),
        .I5(txcharisk),
        .O(\DOUT[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \DOUT[9]_i_3 
       (.I0(txdata[2]),
        .I1(txdata[1]),
        .I2(txcharisk),
        .I3(txdata[0]),
        .I4(txdata[3]),
        .I5(txdata[4]),
        .O(k28));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'h303FAAAA)) 
    \DOUT[9]_i_4 
       (.I0(\DOUT[9]_i_6_n_0 ),
        .I1(txchardispval),
        .I2(txchardispmode),
        .I3(\ngdb.disp_run_reg_n_0 ),
        .I4(k28),
        .O(pdes6__13));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \DOUT[9]_i_5 
       (.I0(txchardispval),
        .I1(txchardispmode),
        .I2(\ngdb.disp_run_reg_n_0 ),
        .O(disp_in_i__0));
  LUT6 #(
    .INIT(64'h56696AA96AA9A995)) 
    \DOUT[9]_i_6 
       (.I0(disp_in_i__0),
        .I1(txdata[2]),
        .I2(txdata[1]),
        .I3(txdata[0]),
        .I4(txdata[4]),
        .I5(txdata[3]),
        .O(\DOUT[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0040400040000000)) 
    \DOUT[9]_i_7 
       (.I0(txdata[4]),
        .I1(disp_in_i__0),
        .I2(txdata[3]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(txdata[1]),
        .O(\DOUT[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400040400)) 
    \DOUT[9]_i_8 
       (.I0(disp_in_i__0),
        .I1(txdata[4]),
        .I2(txdata[3]),
        .I3(txdata[2]),
        .I4(txdata[0]),
        .I5(txdata[1]),
        .O(\DOUT[9]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[0]_i_1_n_0 ),
        .Q(tx_data_10b[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[1]_i_1_n_0 ),
        .Q(tx_data_10b[1]),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[2]_i_1_n_0 ),
        .Q(tx_data_10b[2]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[3]_i_1_n_0 ),
        .Q(tx_data_10b[3]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[4]_i_1_n_0 ),
        .Q(tx_data_10b[4]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \DOUT_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\DOUT[5]_i_2_n_0 ),
        .Q(tx_data_10b[5]),
        .S(\DOUT[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[0]),
        .Q(tx_data_10b[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[1]),
        .Q(tx_data_10b[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[8] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[2]),
        .Q(tx_data_10b[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \DOUT_reg[9] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(b4[3]),
        .Q(tx_data_10b[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h7C83)) 
    \ngdb.disp_run_i_1 
       (.I0(txdata[7]),
        .I1(txdata[6]),
        .I2(txdata[5]),
        .I3(pdes6__13),
        .O(pdes4));
  FDRE #(
    .INIT(1'b1)) 
    \ngdb.disp_run_reg 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(pdes4),
        .Q(\ngdb.disp_run_reg_n_0 ),
        .R(1'b0));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr
   (clk12_5,
    clk_en_12_5_fall0,
    clk_en0,
    speed_is_10_100_fall_reg,
    Tx_WrClk,
    reset_out,
    clk12_5_reg,
    speed_is_10_100_fall,
    speed_is_100_fall,
    clk1_25,
    reset_fall);
  output clk12_5;
  output clk_en_12_5_fall0;
  output clk_en0;
  output speed_is_10_100_fall_reg;
  input Tx_WrClk;
  input reset_out;
  input clk12_5_reg;
  input speed_is_10_100_fall;
  input speed_is_100_fall;
  input clk1_25;
  input reset_fall;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk12_5_reg;
  wire clk1_25;
  wire clk_en0;
  wire clk_en_12_5_fall0;
  wire p_0_in;
  wire reg1;
  wire reg2;
  wire reg4;
  wire reg5;
  wire reg5_reg_n_0;
  wire reset_fall;
  wire reset_out;
  wire speed_is_100_fall;
  wire speed_is_10_100_fall;
  wire speed_is_10_100_fall_reg;

  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_12_5_fall_i_1
       (.I0(clk12_5_reg),
        .I1(clk12_5),
        .O(clk_en_12_5_fall0));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_12_5_rise_i_1
       (.I0(clk12_5),
        .I1(clk12_5_reg),
        .O(clk_en0));
  LUT1 #(
    .INIT(2'h1)) 
    reg1_i_1
       (.I0(reg5_reg_n_0),
        .O(p_0_in));
  FDRE reg1_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(reg1),
        .R(reg5));
  FDRE reg2_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg1),
        .Q(reg2),
        .R(reg5));
  FDRE reg3_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg2),
        .Q(clk12_5),
        .R(reg5));
  FDRE reg4_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(clk12_5),
        .Q(reg4),
        .R(reg5));
  LUT3 #(
    .INIT(8'hF4)) 
    reg5_i_1
       (.I0(reg4),
        .I1(reg5_reg_n_0),
        .I2(reset_out),
        .O(reg5));
  FDRE reg5_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reg4),
        .Q(reg5_reg_n_0),
        .R(reg5));
  LUT5 #(
    .INIT(32'h0000DFD5)) 
    sgmii_clk_f_i_1
       (.I0(speed_is_10_100_fall),
        .I1(clk12_5),
        .I2(speed_is_100_fall),
        .I3(clk1_25),
        .I4(reset_fall),
        .O(speed_is_10_100_fall_reg));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_johnson_cntr_2
   (clk1_25,
    sgmii_clk_r0_out,
    clk_en_1_25_fall0,
    clk_en,
    Tx_WrClk,
    reset_out,
    sgmii_clk_r_reg,
    data_out,
    clk12_5,
    clk1_25_reg);
  output clk1_25;
  output sgmii_clk_r0_out;
  output clk_en_1_25_fall0;
  input clk_en;
  input Tx_WrClk;
  input reset_out;
  input sgmii_clk_r_reg;
  input data_out;
  input clk12_5;
  input clk1_25_reg;

  wire Tx_WrClk;
  wire clk12_5;
  wire clk1_25;
  wire clk1_25_reg;
  wire clk_en;
  wire clk_en_1_25_fall0;
  wire data_out;
  wire reg1_i_1__0_n_0;
  wire reg1_reg_n_0;
  wire reg2_reg_n_0;
  wire reg4;
  wire reg5;
  wire reg5_reg_n_0;
  wire reset_out;
  wire sgmii_clk_r0_out;
  wire sgmii_clk_r_reg;

  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT2 #(
    .INIT(4'h2)) 
    clk_en_1_25_fall_i_1
       (.I0(clk1_25_reg),
        .I1(clk1_25),
        .O(clk_en_1_25_fall0));
  LUT1 #(
    .INIT(2'h1)) 
    reg1_i_1__0
       (.I0(reg5_reg_n_0),
        .O(reg1_i_1__0_n_0));
  FDRE reg1_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg1_i_1__0_n_0),
        .Q(reg1_reg_n_0),
        .R(reg5));
  FDRE reg2_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg1_reg_n_0),
        .Q(reg2_reg_n_0),
        .R(reg5));
  FDRE reg3_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg2_reg_n_0),
        .Q(clk1_25),
        .R(reg5));
  FDRE reg4_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(clk1_25),
        .Q(reg4),
        .R(reg5));
  LUT4 #(
    .INIT(16'hFF40)) 
    reg5_i_1__0
       (.I0(reg4),
        .I1(clk_en),
        .I2(reg5_reg_n_0),
        .I3(reset_out),
        .O(reg5));
  FDRE reg5_reg
       (.C(Tx_WrClk),
        .CE(clk_en),
        .D(reg4),
        .Q(reg5_reg_n_0),
        .R(reg5));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    sgmii_clk_r_i_1
       (.I0(sgmii_clk_r_reg),
        .I1(clk1_25),
        .I2(data_out),
        .I3(clk12_5),
        .O(sgmii_clk_r0_out));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_lvds_transceiver
   (al_rx_valid_out,
    \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ,
    rxrecreset0,
    \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ,
    BaseX_Rx_Fifo_Rd_En,
    ActCnt_GE_HalfBT,
    LossOfSignal,
    initialize_ram_complete_sync_reg1,
    data_out,
    remove_idle,
    remove_idle_reg__0,
    insert_idle_reg__0,
    rxdisperr,
    rxnotintable,
    rxrundisp,
    rxclkcorcnt,
    initialize_ram_complete,
    initialize_ram_complete_pulse,
    ActiveIsSlve,
    D,
    Mstr_Load_reg,
    WrapToZero,
    monitor_late_reg,
    insert5_reg,
    insert3_reg,
    Q,
    \s_state_reg[4] ,
    \act_count_reg[5] ,
    \Slve_CntValIn_Out_reg[8] ,
    \Slve_CntValIn_Out_reg[1] ,
    \Slve_CntValIn_Out_reg[0] ,
    BaseX_Idly_Load,
    \rd_data_reg_reg[13] ,
    rxchariscomma_usr_reg,
    rxcharisk_usr_reg,
    rxbufstatus,
    \rxdata_usr_reg[7] ,
    \tx_data_8b_reg[7]_0 ,
    \s_state_reg[0] ,
    \s_state_reg[5] ,
    \s_state_reg[4]_0 ,
    \s_state_reg[2] ,
    \s_state_reg[4]_1 ,
    \s_state_reg[0]_0 ,
    \active_reg[1] ,
    \act_count_reg[0] ,
    \act_count_reg[4] ,
    \act_count_reg[3] ,
    \s_state_reg[3] ,
    Rx_SysClk,
    \d2p2_wr_pipe_reg[3] ,
    \d21p5_wr_pipe_reg[3] ,
    D0,
    Tx_WrClk,
    initialize_ram_complete_sync_ris_edg0,
    \rxclkcorcnt_reg[0] ,
    reset_out,
    LossOfSignal_reg,
    ActiveIsSlve_reg,
    Slve_Load_reg,
    Mstr_Load_reg_0,
    WrapToZero_reg,
    monitor_late_reg_0,
    insert5_reg_0,
    insert3_reg_0,
    CLK,
    \IntRx_BtVal_reg[8] ,
    \wr_addr_plus2_reg[6] ,
    txchardispval,
    txchardispmode,
    txdata,
    txcharisk,
    BaseX_Rx_Q_Out,
    mgt_rx_reset);
  output al_rx_valid_out;
  output \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  output rxrecreset0;
  output \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  output [0:0]BaseX_Rx_Fifo_Rd_En;
  output ActCnt_GE_HalfBT;
  output LossOfSignal;
  output initialize_ram_complete_sync_reg1;
  output data_out;
  output remove_idle;
  output remove_idle_reg__0;
  output insert_idle_reg__0;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxrundisp;
  output [1:0]rxclkcorcnt;
  output initialize_ram_complete;
  output initialize_ram_complete_pulse;
  output ActiveIsSlve;
  output [0:0]D;
  output [0:0]Mstr_Load_reg;
  output WrapToZero;
  output monitor_late_reg;
  output insert5_reg;
  output insert3_reg;
  output [8:0]Q;
  output [4:0]\s_state_reg[4] ;
  output [0:0]\act_count_reg[5] ;
  output [6:0]\Slve_CntValIn_Out_reg[8] ;
  output \Slve_CntValIn_Out_reg[1] ;
  output \Slve_CntValIn_Out_reg[0] ;
  output [1:0]BaseX_Idly_Load;
  output [0:0]\rd_data_reg_reg[13] ;
  output rxchariscomma_usr_reg;
  output rxcharisk_usr_reg;
  output [0:0]rxbufstatus;
  output [7:0]\rxdata_usr_reg[7] ;
  output [7:0]\tx_data_8b_reg[7]_0 ;
  output \s_state_reg[0] ;
  output \s_state_reg[5] ;
  output \s_state_reg[4]_0 ;
  output \s_state_reg[2] ;
  output \s_state_reg[4]_1 ;
  output \s_state_reg[0]_0 ;
  output \active_reg[1] ;
  output \act_count_reg[0] ;
  output \act_count_reg[4] ;
  output \act_count_reg[3] ;
  output \s_state_reg[3] ;
  input Rx_SysClk;
  input \d2p2_wr_pipe_reg[3] ;
  input \d21p5_wr_pipe_reg[3] ;
  input D0;
  input Tx_WrClk;
  input initialize_ram_complete_sync_ris_edg0;
  input \rxclkcorcnt_reg[0] ;
  input reset_out;
  input LossOfSignal_reg;
  input ActiveIsSlve_reg;
  input Slve_Load_reg;
  input Mstr_Load_reg_0;
  input WrapToZero_reg;
  input monitor_late_reg_0;
  input insert5_reg_0;
  input insert3_reg_0;
  input CLK;
  input [5:0]\IntRx_BtVal_reg[8] ;
  input \wr_addr_plus2_reg[6] ;
  input txchardispval;
  input txchardispmode;
  input [7:0]txdata;
  input txcharisk;
  input [7:0]BaseX_Rx_Q_Out;
  input mgt_rx_reset;

  wire ActCnt_GE_HalfBT;
  wire ActiveIsSlve;
  wire ActiveIsSlve_reg;
  wire [1:0]BaseX_Idly_Load;
  wire [0:0]BaseX_Rx_Fifo_Rd_En;
  wire [7:0]BaseX_Rx_Q_Out;
  wire CLK;
  wire [0:0]D;
  wire D0;
  wire [5:0]\IntRx_BtVal_reg[8] ;
  wire LossOfSignal;
  wire LossOfSignal_reg;
  wire [0:0]Mstr_Load_reg;
  wire Mstr_Load_reg_0;
  wire [8:0]Q;
  wire Rx_SysClk;
  wire \Slve_CntValIn_Out_reg[0] ;
  wire \Slve_CntValIn_Out_reg[1] ;
  wire [6:0]\Slve_CntValIn_Out_reg[8] ;
  wire Slve_Load_reg;
  wire Tx_WrClk;
  wire WrapToZero;
  wire WrapToZero_reg;
  wire \act_count_reg[0] ;
  wire \act_count_reg[3] ;
  wire \act_count_reg[4] ;
  wire [0:0]\act_count_reg[5] ;
  wire \active_reg[1] ;
  wire al_rx_valid_out;
  wire [7:5]b3;
  wire code_err_i;
  wire counter_flag;
  wire counter_flag_i_2_n_0;
  wire counter_flag_i_3_n_0;
  wire counter_flag_reg_n_0;
  wire counter_stg0_carry__0_n_6;
  wire counter_stg0_carry__0_n_7;
  wire counter_stg0_carry_n_0;
  wire counter_stg0_carry_n_1;
  wire counter_stg0_carry_n_2;
  wire counter_stg0_carry_n_3;
  wire counter_stg0_carry_n_4;
  wire counter_stg0_carry_n_5;
  wire counter_stg0_carry_n_6;
  wire counter_stg0_carry_n_7;
  wire \counter_stg[0]_i_1_n_0 ;
  wire [11:0]counter_stg_reg;
  wire \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  wire \d21p5_wr_pipe_reg[3] ;
  wire \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ;
  wire \d2p2_wr_pipe_reg[3] ;
  wire data_out;
  wire decoded_rxchariscomma;
  wire decoded_rxchariscomma0;
  wire decoded_rxcharisk;
  wire [7:0]decoded_rxdata;
  wire decoded_rxdisperr;
  wire decoded_rxnotintable;
  wire decoded_rxrundisp;
  wire elastic_buffer_rst_125;
  wire elastic_buffer_rst_312;
  wire initialize_ram_complete;
  wire initialize_ram_complete_pulse;
  wire initialize_ram_complete_sync_reg1;
  wire initialize_ram_complete_sync_ris_edg0;
  wire insert3_reg;
  wire insert3_reg_0;
  wire insert5_reg;
  wire insert5_reg_0;
  wire insert_idle_reg__0;
  wire k;
  wire mgt_rx_reset;
  wire monitor_late_reg;
  wire monitor_late_reg_0;
  wire [11:1]p_0_in__3;
  wire [0:0]\rd_data_reg_reg[13] ;
  wire remove_idle;
  wire remove_idle_reg__0;
  wire reset_out;
  wire reset_sync_312_rxelastic_buffer_n_0;
  wire rx_rst_312;
  wire [0:0]rxbufstatus;
  wire rxchariscomma_usr_reg;
  wire rxcharisk_usr_reg;
  wire [1:0]rxclkcorcnt;
  wire \rxclkcorcnt_reg[0] ;
  wire [7:0]\rxdata_usr_reg[7] ;
  wire [0:0]rxdisperr;
  wire [0:0]rxnotintable;
  wire rxrecreset0;
  wire [0:0]rxrundisp;
  wire \s_state_reg[0] ;
  wire \s_state_reg[0]_0 ;
  wire \s_state_reg[2] ;
  wire \s_state_reg[3] ;
  wire [4:0]\s_state_reg[4] ;
  wire \s_state_reg[4]_0 ;
  wire \s_state_reg[4]_1 ;
  wire \s_state_reg[5] ;
  wire sel;
  wire serdes_1_to_10_i_n_48;
  wire serdes_1_to_10_i_n_49;
  wire serdes_1_to_10_i_n_50;
  wire serdes_1_to_10_i_n_51;
  wire serdes_1_to_10_i_n_52;
  wire serdes_1_to_10_i_n_54;
  wire serdes_1_to_10_i_n_55;
  wire [9:0]tx_data_10b;
  wire [7:0]tx_data_8b_int;
  wire [7:0]\tx_data_8b_reg[7]_0 ;
  wire tx_rst_125;
  wire tx_rst_156;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire \wr_addr_plus2_reg[6] ;
  wire [7:2]NLW_counter_stg0_carry__0_CO_UNCONNECTED;
  wire [7:3]NLW_counter_stg0_carry__0_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0000000000008000)) 
    counter_flag_i_1
       (.I0(counter_stg_reg[2]),
        .I1(counter_stg_reg[4]),
        .I2(counter_stg_reg[1]),
        .I3(counter_stg_reg[10]),
        .I4(counter_flag_i_2_n_0),
        .I5(counter_flag_i_3_n_0),
        .O(counter_flag));
  LUT4 #(
    .INIT(16'h7FFF)) 
    counter_flag_i_2
       (.I0(counter_stg_reg[7]),
        .I1(counter_stg_reg[3]),
        .I2(counter_stg_reg[11]),
        .I3(counter_stg_reg[8]),
        .O(counter_flag_i_2_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    counter_flag_i_3
       (.I0(counter_stg_reg[6]),
        .I1(counter_stg_reg[0]),
        .I2(counter_stg_reg[9]),
        .I3(counter_stg_reg[5]),
        .O(counter_flag_i_3_n_0));
  FDRE counter_flag_reg
       (.C(Tx_WrClk),
        .CE(counter_flag),
        .D(counter_flag),
        .Q(counter_flag_reg_n_0),
        .R(reset_out));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 counter_stg0_carry
       (.CI(counter_stg_reg[0]),
        .CI_TOP(1'b0),
        .CO({counter_stg0_carry_n_0,counter_stg0_carry_n_1,counter_stg0_carry_n_2,counter_stg0_carry_n_3,counter_stg0_carry_n_4,counter_stg0_carry_n_5,counter_stg0_carry_n_6,counter_stg0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in__3[8:1]),
        .S(counter_stg_reg[8:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 counter_stg0_carry__0
       (.CI(counter_stg0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_counter_stg0_carry__0_CO_UNCONNECTED[7:2],counter_stg0_carry__0_n_6,counter_stg0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_counter_stg0_carry__0_O_UNCONNECTED[7:3],p_0_in__3[11:9]}),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,counter_stg_reg[11:9]}));
  LUT3 #(
    .INIT(8'h09)) 
    \counter_stg[0]_i_1 
       (.I0(counter_stg_reg[0]),
        .I1(counter_flag_reg_n_0),
        .I2(reset_out),
        .O(\counter_stg[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg[11]_i_1 
       (.I0(counter_flag_reg_n_0),
        .O(sel));
  FDRE \counter_stg_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\counter_stg[0]_i_1_n_0 ),
        .Q(counter_stg_reg[0]),
        .R(1'b0));
  FDRE \counter_stg_reg[10] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[10]),
        .Q(counter_stg_reg[10]),
        .R(reset_out));
  FDRE \counter_stg_reg[11] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[11]),
        .Q(counter_stg_reg[11]),
        .R(reset_out));
  FDRE \counter_stg_reg[1] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[1]),
        .Q(counter_stg_reg[1]),
        .R(reset_out));
  FDRE \counter_stg_reg[2] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[2]),
        .Q(counter_stg_reg[2]),
        .R(reset_out));
  FDRE \counter_stg_reg[3] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[3]),
        .Q(counter_stg_reg[3]),
        .R(reset_out));
  FDRE \counter_stg_reg[4] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[4]),
        .Q(counter_stg_reg[4]),
        .R(reset_out));
  FDRE \counter_stg_reg[5] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[5]),
        .Q(counter_stg_reg[5]),
        .R(reset_out));
  FDRE \counter_stg_reg[6] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[6]),
        .Q(counter_stg_reg[6]),
        .R(reset_out));
  FDRE \counter_stg_reg[7] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[7]),
        .Q(counter_stg_reg[7]),
        .R(reset_out));
  FDRE \counter_stg_reg[8] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[8]),
        .Q(counter_stg_reg[8]),
        .R(reset_out));
  FDRE \counter_stg_reg[9] 
       (.C(Tx_WrClk),
        .CE(sel),
        .D(p_0_in__3[9]),
        .Q(counter_stg_reg[9]),
        .R(reset_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_decode_8b10b_lut_base decode_8b10b
       (.D({decoded_rxcharisk,decoded_rxdisperr,decoded_rxnotintable,decoded_rxrundisp,decoded_rxdata}),
        .E(al_rx_valid_out),
        .Rx_SysClk(Rx_SysClk),
        .b3(b3),
        .code_err_i(code_err_i),
        .\gde.gdeni.DISP_ERR_reg_0 (serdes_1_to_10_i_n_55),
        .\grdni.run_disp_i_reg_0 (serdes_1_to_10_i_n_54),
        .k(k),
        .out({serdes_1_to_10_i_n_48,serdes_1_to_10_i_n_49,serdes_1_to_10_i_n_50,serdes_1_to_10_i_n_51,serdes_1_to_10_i_n_52}));
  FDRE decoded_rxchariscomma_reg
       (.C(Rx_SysClk),
        .CE(al_rx_valid_out),
        .D(decoded_rxchariscomma0),
        .Q(decoded_rxchariscomma),
        .R(1'b0));
  FDSE elastic_buffer_rst_125_reg
       (.C(Tx_WrClk),
        .CE(counter_flag),
        .D(1'b0),
        .Q(elastic_buffer_rst_125),
        .S(reset_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_encode_8b10b_lut_base encode_8b10b
       (.Tx_WrClk(Tx_WrClk),
        .tx_data_10b(tx_data_10b),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_ten_to_eight gb_out_inst
       (.CLK(CLK),
        .Q(tx_data_8b_int),
        .Tx_WrClk(Tx_WrClk),
        .reset_out(tx_rst_125),
        .tx_data_10b(tx_data_10b));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_3 reset_sync_125_tx
       (.Tx_WrClk(Tx_WrClk),
        .reset_out(reset_out),
        .reset_sync6_0(tx_rst_125));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_4 reset_sync_312_rx
       (.Rx_SysClk(Rx_SysClk),
        .SR(rxrecreset0),
        .\d21p5_wr_pipe_reg[3] (elastic_buffer_rst_312),
        .reset_out(rx_rst_312),
        .reset_sync5_0(reset_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_5 reset_sync_312_rxelastic_buffer
       (.Rx_SysClk(Rx_SysClk),
        .SR(reset_sync_312_rxelastic_buffer_n_0),
        .data_in(initialize_ram_complete),
        .elastic_buffer_rst_125(elastic_buffer_rst_125),
        .mgt_rx_reset(mgt_rx_reset),
        .reset_out(elastic_buffer_rst_312),
        .\wr_data_reg[0] (rx_rst_312));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_6 reset_sync_312_tx
       (.CLK(CLK),
        .reset_out(reset_out),
        .reset_sync6_0(tx_rst_156));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_elastic_buffer rx_elastic_buffer_inst
       (.D(remove_idle),
        .E(al_rx_valid_out),
        .Rx_SysClk(Rx_SysClk),
        .SR(rxrecreset0),
        .Tx_WrClk(Tx_WrClk),
        .\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 (\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ),
        .\d21p5_wr_pipe_reg[3]_0 (\d21p5_wr_pipe_reg[3] ),
        .\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 (\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 ),
        .\d2p2_wr_pipe_reg[3]_0 (\d2p2_wr_pipe_reg[3] ),
        .data_in(initialize_ram_complete),
        .data_out(data_out),
        .elastic_buffer_rst_125(elastic_buffer_rst_125),
        .\initialize_counter_reg[5]_0 (rx_rst_312),
        .initialize_ram_complete_pulse_reg_0(initialize_ram_complete_pulse),
        .initialize_ram_complete_sync_reg1(initialize_ram_complete_sync_reg1),
        .initialize_ram_complete_sync_ris_edg0(initialize_ram_complete_sync_ris_edg0),
        .insert_idle_reg__0(insert_idle_reg__0),
        .mgt_rx_reset(mgt_rx_reset),
        .\rd_data_reg_reg[13]_0 (\rd_data_reg_reg[13] ),
        .remove_idle_reg_reg_0(remove_idle_reg__0),
        .reset_modified_reg_0(reset_out),
        .reset_out(elastic_buffer_rst_312),
        .rxbufstatus(rxbufstatus),
        .rxchariscomma_usr_reg_0(rxchariscomma_usr_reg),
        .rxcharisk_usr_reg_0(rxcharisk_usr_reg),
        .rxclkcorcnt(rxclkcorcnt),
        .\rxclkcorcnt_reg[0]_0 (\rxclkcorcnt_reg[0] ),
        .\rxdata_usr_reg[7]_0 (\rxdata_usr_reg[7] ),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxrundisp(rxrundisp),
        .\wr_addr_plus2_reg[6]_0 (\wr_addr_plus2_reg[6] ),
        .\wr_data_reg[0]_0 (reset_sync_312_rxelastic_buffer_n_0),
        .\wr_data_reg[12]_0 ({decoded_rxchariscomma,decoded_rxcharisk,decoded_rxdisperr,decoded_rxnotintable,decoded_rxrundisp,decoded_rxdata}));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_serdes_1_to_10 serdes_1_to_10_i
       (.ActCnt_GE_HalfBT_reg_0(ActCnt_GE_HalfBT),
        .ActiveIsSlve_reg_0(ActiveIsSlve),
        .ActiveIsSlve_reg_1(ActiveIsSlve_reg),
        .BaseX_Idly_Load(BaseX_Idly_Load),
        .BaseX_Rx_Fifo_Rd_En(BaseX_Rx_Fifo_Rd_En),
        .BaseX_Rx_Q_Out(BaseX_Rx_Q_Out),
        .D(D),
        .D0(D0),
        .E(al_rx_valid_out),
        .\IntReset_dly_reg[0]_0 (rx_rst_312),
        .\IntRx_BtVal_reg[8]_0 (\IntRx_BtVal_reg[8] ),
        .LossOfSignal_reg_0(LossOfSignal_reg),
        .\Mstr_CntValIn_Out_reg[8]_0 (Q),
        .Mstr_Load_reg_0(Mstr_Load_reg),
        .Mstr_Load_reg_1(Mstr_Load_reg_0),
        .Q(\Slve_CntValIn_Out_reg[8] ),
        .Rx_SysClk(Rx_SysClk),
        .SR(LossOfSignal),
        .\Slve_CntValIn_Out_reg[0]_0 (\Slve_CntValIn_Out_reg[0] ),
        .\Slve_CntValIn_Out_reg[1]_0 (\Slve_CntValIn_Out_reg[1] ),
        .Slve_Load_reg_0(Slve_Load_reg),
        .WrapToZero(WrapToZero),
        .WrapToZero_reg_0(WrapToZero_reg),
        .\act_count_reg[0]_0 (\act_count_reg[0] ),
        .\act_count_reg[3]_0 (\act_count_reg[3] ),
        .\act_count_reg[4]_0 (\act_count_reg[4] ),
        .\act_count_reg[5]_0 (\act_count_reg[5] ),
        .\active_reg[1]_0 (\active_reg[1] ),
        .b3(b3),
        .code_err_i(code_err_i),
        .decoded_rxchariscomma0(decoded_rxchariscomma0),
        .\grdni.run_disp_i_reg (serdes_1_to_10_i_n_54),
        .\grdni.run_disp_i_reg_0 (serdes_1_to_10_i_n_55),
        .\grdni.run_disp_i_reg_1 (decoded_rxrundisp),
        .insert3_reg_0(insert3_reg),
        .insert3_reg_1(insert3_reg_0),
        .insert5_reg_0(insert5_reg),
        .insert5_reg_1(insert5_reg_0),
        .k(k),
        .monitor_late_reg_0(monitor_late_reg),
        .monitor_late_reg_1(monitor_late_reg_0),
        .out({serdes_1_to_10_i_n_48,serdes_1_to_10_i_n_49,serdes_1_to_10_i_n_50,serdes_1_to_10_i_n_51,serdes_1_to_10_i_n_52}),
        .\s_state_reg[0]_0 (\s_state_reg[0] ),
        .\s_state_reg[0]_1 (\s_state_reg[0]_0 ),
        .\s_state_reg[2]_0 (\s_state_reg[2] ),
        .\s_state_reg[3]_0 (\s_state_reg[3] ),
        .\s_state_reg[4]_0 (\s_state_reg[4] ),
        .\s_state_reg[4]_1 (\s_state_reg[4]_0 ),
        .\s_state_reg[4]_2 (\s_state_reg[4]_1 ),
        .\s_state_reg[5]_0 (\s_state_reg[5] ));
  FDRE \tx_data_8b_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[0]),
        .Q(\tx_data_8b_reg[7]_0 [0]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[1]),
        .Q(\tx_data_8b_reg[7]_0 [1]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[2]),
        .Q(\tx_data_8b_reg[7]_0 [2]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[3]),
        .Q(\tx_data_8b_reg[7]_0 [3]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[4]),
        .Q(\tx_data_8b_reg[7]_0 [4]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[5]),
        .Q(\tx_data_8b_reg[7]_0 [5]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[6]),
        .Q(\tx_data_8b_reg[7]_0 [6]),
        .R(tx_rst_156));
  FDRE \tx_data_8b_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(tx_data_8b_int[7]),
        .Q(\tx_data_8b_reg[7]_0 [7]),
        .R(tx_rst_156));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync
   (rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    Tx_WrClk);
  output rst_125_out;
  input tx_logic_reset;
  input rx_logic_reset;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire logic_reset;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire rst_125_out;
  wire rx_logic_reset;
  wire tx_logic_reset;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(logic_reset),
        .Q(reset_sync_reg1));
  LUT2 #(
    .INIT(4'hE)) 
    reset_sync1_i_1__0
       (.I0(tx_logic_reset),
        .I1(rx_logic_reset),
        .O(logic_reset));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(logic_reset),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(logic_reset),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(logic_reset),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(logic_reset),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(rst_125_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_0
   (reset_out,
    Tx_WrClk,
    mgt_tx_reset);
  output reset_out;
  input Tx_WrClk;
  input mgt_tx_reset;

  wire Tx_WrClk;
  wire mgt_tx_reset;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(mgt_tx_reset),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_24
   (reset_out,
    reset_sync1_0,
    ResetIn);
  output reset_out;
  input reset_sync1_0;
  input ResetIn;

  wire ResetIn;
  wire reset_out;
  wire reset_sync1_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(1'b0),
        .PRE(ResetIn),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(ResetIn),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(ResetIn),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(ResetIn),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(ResetIn),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(reset_sync1_0),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_25
   (Rx_LogicRst,
    Rx_SysClk,
    reset_in);
  output Rx_LogicRst;
  input Rx_SysClk;
  input reset_in;

  wire Rx_LogicRst;
  wire Rx_SysClk;
  wire reset_in;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(Rx_LogicRst));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_26
   (Tx_LogicRst,
    Tx_WrClk,
    reset_in);
  output Tx_LogicRst;
  input Tx_WrClk;
  input reset_in;

  wire Tx_LogicRst;
  wire Tx_WrClk;
  wire reset_in;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(Tx_LogicRst));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_3
   (reset_sync6_0,
    Tx_WrClk,
    reset_out);
  output reset_sync6_0;
  input Tx_WrClk;
  input reset_out;

  wire Tx_WrClk;
  wire reset_out;
  wire reset_sync6_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_out),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_out),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_out),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_out),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_sync6_0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_4
   (SR,
    reset_out,
    \d21p5_wr_pipe_reg[3] ,
    Rx_SysClk,
    reset_sync5_0);
  output [0:0]SR;
  output reset_out;
  input \d21p5_wr_pipe_reg[3] ;
  input Rx_SysClk;
  input reset_sync5_0;

  wire Rx_SysClk;
  wire [0:0]SR;
  wire \d21p5_wr_pipe_reg[3] ;
  wire reset_out;
  wire reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr[6]_i_1 
       (.I0(reset_out),
        .I1(\d21p5_wr_pipe_reg[3] ),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_5
   (SR,
    reset_out,
    \wr_data_reg[0] ,
    data_in,
    elastic_buffer_rst_125,
    mgt_rx_reset,
    Rx_SysClk);
  output [0:0]SR;
  output reset_out;
  input \wr_data_reg[0] ;
  input data_in;
  input elastic_buffer_rst_125;
  input mgt_rx_reset;
  input Rx_SysClk;

  wire Rx_SysClk;
  wire [0:0]SR;
  wire data_in;
  wire elastic_buffer_rst_125;
  wire mgt_rx_reset;
  wire reset_in0;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire \wr_data_reg[0] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_in0),
        .Q(reset_sync_reg1));
  LUT2 #(
    .INIT(4'hE)) 
    reset_sync1_i_1
       (.I0(elastic_buffer_rst_125),
        .I1(mgt_rx_reset),
        .O(reset_in0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_in0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_in0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_in0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_in0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
  LUT3 #(
    .INIT(8'hEF)) 
    \wr_data_reg_reg[13]_i_1 
       (.I0(reset_out),
        .I1(\wr_data_reg[0] ),
        .I2(data_in),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_6
   (reset_sync6_0,
    CLK,
    reset_out);
  output reset_sync6_0;
  input CLK;
  input reset_out;

  wire CLK;
  wire reset_out;
  wire reset_sync6_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_out),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_out),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_out),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_out),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_sync6_0));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_elastic_buffer
   (\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ,
    \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ,
    initialize_ram_complete_sync_reg1,
    data_out,
    D,
    remove_idle_reg_reg_0,
    insert_idle_reg__0,
    rxdisperr,
    rxnotintable,
    rxrundisp,
    rxclkcorcnt,
    data_in,
    initialize_ram_complete_pulse_reg_0,
    \rd_data_reg_reg[13]_0 ,
    rxchariscomma_usr_reg_0,
    rxcharisk_usr_reg_0,
    rxbufstatus,
    \rxdata_usr_reg[7]_0 ,
    E,
    Rx_SysClk,
    SR,
    \d2p2_wr_pipe_reg[3]_0 ,
    \d21p5_wr_pipe_reg[3]_0 ,
    Tx_WrClk,
    initialize_ram_complete_sync_ris_edg0,
    \rxclkcorcnt_reg[0]_0 ,
    reset_out,
    \initialize_counter_reg[5]_0 ,
    reset_modified_reg_0,
    mgt_rx_reset,
    elastic_buffer_rst_125,
    \wr_addr_plus2_reg[6]_0 ,
    \wr_data_reg[0]_0 ,
    \wr_data_reg[12]_0 );
  output \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  output \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  output initialize_ram_complete_sync_reg1;
  output data_out;
  output [0:0]D;
  output remove_idle_reg_reg_0;
  output insert_idle_reg__0;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxrundisp;
  output [1:0]rxclkcorcnt;
  output data_in;
  output initialize_ram_complete_pulse_reg_0;
  output [0:0]\rd_data_reg_reg[13]_0 ;
  output rxchariscomma_usr_reg_0;
  output rxcharisk_usr_reg_0;
  output [0:0]rxbufstatus;
  output [7:0]\rxdata_usr_reg[7]_0 ;
  input [0:0]E;
  input Rx_SysClk;
  input [0:0]SR;
  input \d2p2_wr_pipe_reg[3]_0 ;
  input \d21p5_wr_pipe_reg[3]_0 ;
  input Tx_WrClk;
  input initialize_ram_complete_sync_ris_edg0;
  input \rxclkcorcnt_reg[0]_0 ;
  input reset_out;
  input \initialize_counter_reg[5]_0 ;
  input reset_modified_reg_0;
  input mgt_rx_reset;
  input elastic_buffer_rst_125;
  input \wr_addr_plus2_reg[6]_0 ;
  input [0:0]\wr_data_reg[0]_0 ;
  input [12:0]\wr_data_reg[12]_0 ;

  wire [0:0]D;
  wire [0:0]E;
  wire Rx_SysClk;
  wire [0:0]SR;
  wire Tx_WrClk;
  wire \__1/ram_reg_64_127_0_6_i_1_n_0 ;
  wire d16p2_wr;
  wire \d16p2_wr_pipe[0]_i_2_n_0 ;
  wire \d16p2_wr_pipe_reg_n_0_[0] ;
  wire d21p5_wr;
  wire [3:3]d21p5_wr_pipe;
  wire \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  wire \d21p5_wr_pipe_reg[3]_0 ;
  wire d2p2_wr;
  wire [3:3]d2p2_wr_pipe;
  wire \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ;
  wire \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ;
  wire \d2p2_wr_pipe_reg[3]_0 ;
  wire data_in;
  wire data_out;
  wire [13:0]dpo;
  wire elastic_buffer_rst_125;
  wire even;
  wire even0;
  wire even_i_1_n_0;
  wire even_i_2_n_0;
  wire even_i_3_n_0;
  wire initialize_counter0;
  wire [5:0]initialize_counter_reg;
  wire \initialize_counter_reg[5]_0 ;
  wire initialize_ram;
  wire initialize_ram0;
  wire initialize_ram_complete_i_2_n_0;
  wire initialize_ram_complete_i_3_n_0;
  wire initialize_ram_complete_pulse0;
  wire initialize_ram_complete_pulse_reg_0;
  wire initialize_ram_complete_reg__0;
  wire initialize_ram_complete_sync_reg1;
  wire initialize_ram_complete_sync_ris_edg;
  wire initialize_ram_complete_sync_ris_edg0;
  wire initialize_ram_i_1_n_0;
  wire insert_idle;
  wire insert_idle_i_10_n_0;
  wire insert_idle_i_11_n_0;
  wire insert_idle_i_12_n_0;
  wire insert_idle_i_2_n_0;
  wire insert_idle_i_3_n_0;
  wire insert_idle_i_4_n_0;
  wire insert_idle_i_5_n_0;
  wire insert_idle_i_6_n_0;
  wire insert_idle_i_7_n_0;
  wire insert_idle_i_8_n_0;
  wire insert_idle_i_9_n_0;
  wire insert_idle_reg__0;
  wire \k28p5_wr_pipe[0]_i_2_n_0 ;
  wire \k28p5_wr_pipe_reg_n_0_[0] ;
  wire \k28p5_wr_pipe_reg_n_0_[1] ;
  wire \k28p5_wr_pipe_reg_n_0_[2] ;
  wire \k28p5_wr_pipe_reg_n_0_[3] ;
  wire \k28p5_wr_pipe_reg_n_0_[4] ;
  wire mgt_rx_reset;
  wire p_0_in;
  wire [5:0]p_0_in__0;
  wire [6:0]p_0_in__1;
  wire p_1_in;
  wire p_1_in28_in;
  wire p_1_in3_in;
  wire p_2_in;
  wire p_2_in20_in;
  wire p_2_in29_in;
  wire p_3_in;
  wire p_3_in31_in;
  wire [0:0]p_3_out;
  wire p_4_in23_in;
  wire p_4_in33_in;
  wire p_4_in9_in;
  wire p_5_in;
  wire p_5_in35_in;
  wire [5:1]p_6_out;
  wire ram_reg_0_63_0_6_i_1_n_0;
  wire ram_reg_0_63_0_6_n_0;
  wire ram_reg_0_63_0_6_n_1;
  wire ram_reg_0_63_0_6_n_2;
  wire ram_reg_0_63_0_6_n_3;
  wire ram_reg_0_63_0_6_n_4;
  wire ram_reg_0_63_0_6_n_5;
  wire ram_reg_0_63_0_6_n_6;
  wire ram_reg_0_63_7_13_n_0;
  wire ram_reg_0_63_7_13_n_1;
  wire ram_reg_0_63_7_13_n_2;
  wire ram_reg_0_63_7_13_n_3;
  wire ram_reg_0_63_7_13_n_4;
  wire ram_reg_0_63_7_13_n_5;
  wire ram_reg_0_63_7_13_n_6;
  wire ram_reg_64_127_0_6_n_0;
  wire ram_reg_64_127_0_6_n_1;
  wire ram_reg_64_127_0_6_n_2;
  wire ram_reg_64_127_0_6_n_3;
  wire ram_reg_64_127_0_6_n_4;
  wire ram_reg_64_127_0_6_n_5;
  wire ram_reg_64_127_0_6_n_6;
  wire ram_reg_64_127_7_13_n_0;
  wire ram_reg_64_127_7_13_n_1;
  wire ram_reg_64_127_7_13_n_2;
  wire ram_reg_64_127_7_13_n_3;
  wire ram_reg_64_127_7_13_n_4;
  wire ram_reg_64_127_7_13_n_5;
  wire ram_reg_64_127_7_13_n_6;
  wire [6:0]rd_addr;
  wire [5:0]rd_addr_gray;
  wire \rd_addr_gray[0]_i_1_n_0 ;
  wire \rd_addr_gray[1]_i_1_n_0 ;
  wire \rd_addr_gray[2]_i_1_n_0 ;
  wire \rd_addr_gray[3]_i_1_n_0 ;
  wire \rd_addr_gray[4]_i_1_n_0 ;
  wire \rd_addr_gray[5]_i_1_n_0 ;
  wire [6:0]rd_addr_plus1;
  wire \rd_addr_plus2[6]_i_2_n_0 ;
  wire \rd_addr_plus2_reg_n_0_[0] ;
  wire \rd_addr_plus2_reg_n_0_[6] ;
  wire \rd_data_reg_n_0_[0] ;
  wire \rd_data_reg_n_0_[10] ;
  wire \rd_data_reg_n_0_[11] ;
  wire \rd_data_reg_n_0_[12] ;
  wire \rd_data_reg_n_0_[13] ;
  wire \rd_data_reg_n_0_[1] ;
  wire \rd_data_reg_n_0_[2] ;
  wire \rd_data_reg_n_0_[3] ;
  wire \rd_data_reg_n_0_[4] ;
  wire \rd_data_reg_n_0_[5] ;
  wire \rd_data_reg_n_0_[6] ;
  wire \rd_data_reg_n_0_[7] ;
  wire \rd_data_reg_n_0_[9] ;
  wire [0:0]\rd_data_reg_reg[13]_0 ;
  wire \rd_data_reg_reg_n_0_[0] ;
  wire \rd_data_reg_reg_n_0_[10] ;
  wire \rd_data_reg_reg_n_0_[12] ;
  wire \rd_data_reg_reg_n_0_[1] ;
  wire \rd_data_reg_reg_n_0_[2] ;
  wire \rd_data_reg_reg_n_0_[3] ;
  wire \rd_data_reg_reg_n_0_[4] ;
  wire \rd_data_reg_reg_n_0_[5] ;
  wire \rd_data_reg_reg_n_0_[6] ;
  wire \rd_data_reg_reg_n_0_[7] ;
  wire \rd_data_reg_reg_n_0_[8] ;
  wire \rd_data_reg_reg_n_0_[9] ;
  wire rd_enable;
  wire rd_enable_reg;
  wire [6:0]rd_occupancy;
  wire [6:0]rd_occupancy01_out;
  wire rd_occupancy0_carry_n_2;
  wire rd_occupancy0_carry_n_3;
  wire rd_occupancy0_carry_n_4;
  wire rd_occupancy0_carry_n_5;
  wire rd_occupancy0_carry_n_6;
  wire rd_occupancy0_carry_n_7;
  wire [5:0]rd_wr_addr;
  wire rd_wr_addr_gray_0;
  wire rd_wr_addr_gray_1;
  wire rd_wr_addr_gray_2;
  wire rd_wr_addr_gray_3;
  wire rd_wr_addr_gray_4;
  wire rd_wr_addr_gray_5;
  wire rd_wr_addr_gray_6;
  wire \reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[1].sync_rd_addrgray_n_1 ;
  wire \reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ;
  wire \reclock_rd_addrgray[6].sync_rd_addrgray_n_1 ;
  wire \reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[1].sync_wr_addrgray_n_1 ;
  wire \reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ;
  wire \reclock_wr_addrgray[6].sync_wr_addrgray_n_1 ;
  wire remove_idle0;
  wire remove_idle_i_2_n_0;
  wire remove_idle_i_3_n_0;
  wire remove_idle_i_4_n_0;
  wire remove_idle_i_5_n_0;
  wire remove_idle_i_6_n_0;
  wire remove_idle_reg2;
  wire remove_idle_reg3;
  wire remove_idle_reg4;
  wire remove_idle_reg_reg_0;
  wire reset_modified;
  wire reset_modified_i_1_n_0;
  wire reset_modified_reg_0;
  wire reset_out;
  wire rxbuferr_i_1_n_0;
  wire rxbuferr_i_2_n_0;
  wire rxbuferr_i_3_n_0;
  wire [0:0]rxbufstatus;
  wire rxchariscomma_usr_i_1_n_0;
  wire rxchariscomma_usr_i_2_n_0;
  wire rxchariscomma_usr_reg_0;
  wire rxcharisk_usr_i_1_n_0;
  wire rxcharisk_usr_reg_0;
  wire [1:0]rxclkcorcnt;
  wire \rxclkcorcnt[2]_i_1_n_0 ;
  wire \rxclkcorcnt_reg[0]_0 ;
  wire \rxdata_usr[0]_i_1_n_0 ;
  wire \rxdata_usr[1]_i_1_n_0 ;
  wire \rxdata_usr[2]_i_1_n_0 ;
  wire \rxdata_usr[3]_i_1_n_0 ;
  wire \rxdata_usr[4]_i_1_n_0 ;
  wire \rxdata_usr[5]_i_1_n_0 ;
  wire \rxdata_usr[6]_i_1_n_0 ;
  wire \rxdata_usr[7]_i_1_n_0 ;
  wire [7:0]\rxdata_usr_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire rxdisperr_usr_i_1_n_0;
  wire [0:0]rxnotintable;
  wire [0:0]rxrundisp;
  wire rxrundisp_usr_i_1_n_0;
  wire start;
  wire [6:0]wr_addr;
  wire \wr_addr[5]_i_2_n_0 ;
  wire \wr_addr[6]_i_3_n_0 ;
  wire [5:5]wr_addr__0;
  wire [6:0]wr_addr_gray;
  wire [6:0]wr_addr_plus1;
  wire \wr_addr_plus1[6]_i_1_n_0 ;
  wire \wr_addr_plus2[0]_i_1_n_0 ;
  wire \wr_addr_plus2[1]_i_1_n_0 ;
  wire \wr_addr_plus2[2]_i_1_n_0 ;
  wire \wr_addr_plus2[3]_i_1_n_0 ;
  wire \wr_addr_plus2[4]_i_1_n_0 ;
  wire \wr_addr_plus2[5]_i_1_n_0 ;
  wire \wr_addr_plus2[6]_i_1_n_0 ;
  wire \wr_addr_plus2[6]_i_2_n_0 ;
  wire \wr_addr_plus2_reg[6]_0 ;
  wire \wr_addr_plus2_reg_n_0_[0] ;
  wire \wr_addr_plus2_reg_n_0_[6] ;
  wire [12:8]wr_data;
  wire [13:0]wr_data_reg;
  wire [0:0]\wr_data_reg[0]_0 ;
  wire [12:0]\wr_data_reg[12]_0 ;
  wire \wr_data_reg_n_0_[0] ;
  wire \wr_data_reg_n_0_[1] ;
  wire \wr_data_reg_n_0_[2] ;
  wire \wr_data_reg_n_0_[3] ;
  wire \wr_data_reg_n_0_[4] ;
  wire \wr_data_reg_n_0_[5] ;
  wire \wr_data_reg_n_0_[6] ;
  wire \wr_data_reg_n_0_[7] ;
  wire [13:0]wr_data_reg_reg;
  wire [6:0]wr_occupancy;
  wire [6:0]wr_occupancy00_out;
  wire wr_occupancy0_carry_n_2;
  wire wr_occupancy0_carry_n_3;
  wire wr_occupancy0_carry_n_4;
  wire wr_occupancy0_carry_n_5;
  wire wr_occupancy0_carry_n_6;
  wire wr_occupancy0_carry_n_7;
  wire wr_rd_addr_gray_0;
  wire wr_rd_addr_gray_2;
  wire wr_rd_addr_gray_3;
  wire wr_rd_addr_gray_4;
  wire wr_rd_addr_gray_5;
  wire wr_rd_addr_gray_6;
  wire NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED;
  wire NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED;
  wire NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED;
  wire [7:6]NLW_rd_occupancy0_carry_CO_UNCONNECTED;
  wire [7:7]NLW_rd_occupancy0_carry_O_UNCONNECTED;
  wire [7:6]NLW_wr_occupancy0_carry_CO_UNCONNECTED;
  wire [7:7]NLW_wr_occupancy0_carry_O_UNCONNECTED;

  LUT5 #(
    .INIT(32'h55750000)) 
    \__1/ram_reg_64_127_0_6_i_1 
       (.I0(data_in),
        .I1(D),
        .I2(E),
        .I3(remove_idle_reg_reg_0),
        .I4(wr_addr[6]),
        .O(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[0]_i_1 
       (.I0(ram_reg_64_127_0_6_n_0),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_0),
        .O(dpo[0]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[10]_i_1 
       (.I0(ram_reg_64_127_7_13_n_3),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_3),
        .O(dpo[10]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[11]_i_1 
       (.I0(ram_reg_64_127_7_13_n_4),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_4),
        .O(dpo[11]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[12]_i_1 
       (.I0(ram_reg_64_127_7_13_n_5),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_5),
        .O(dpo[12]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[13]_i_1 
       (.I0(ram_reg_64_127_7_13_n_6),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_6),
        .O(dpo[13]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[1]_i_1 
       (.I0(ram_reg_64_127_0_6_n_1),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_1),
        .O(dpo[1]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[2]_i_1 
       (.I0(ram_reg_64_127_0_6_n_2),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_2),
        .O(dpo[2]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[3]_i_1 
       (.I0(ram_reg_64_127_0_6_n_3),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_3),
        .O(dpo[3]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[4]_i_1 
       (.I0(ram_reg_64_127_0_6_n_4),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_4),
        .O(dpo[4]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[5]_i_1 
       (.I0(ram_reg_64_127_0_6_n_5),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_5),
        .O(dpo[5]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[6]_i_1 
       (.I0(ram_reg_64_127_0_6_n_6),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_0_6_n_6),
        .O(dpo[6]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[7]_i_1 
       (.I0(ram_reg_64_127_7_13_n_0),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_0),
        .O(dpo[7]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[8]_i_1 
       (.I0(ram_reg_64_127_7_13_n_1),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_1),
        .O(dpo[8]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \__1/rd_data[9]_i_1 
       (.I0(ram_reg_64_127_7_13_n_2),
        .I1(rd_addr[6]),
        .I2(ram_reg_0_63_7_13_n_2),
        .O(dpo[9]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d16p2_wr_pipe[0]_i_1 
       (.I0(\d16p2_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[1] ),
        .I2(\wr_data_reg_n_0_[4] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(d16p2_wr));
  LUT5 #(
    .INIT(32'h00000001)) 
    \d16p2_wr_pipe[0]_i_2 
       (.I0(\wr_data_reg_n_0_[3] ),
        .I1(\wr_data_reg_n_0_[2] ),
        .I2(\wr_data_reg_n_0_[7] ),
        .I3(\wr_data_reg_n_0_[0] ),
        .I4(\wr_data_reg_n_0_[5] ),
        .O(\d16p2_wr_pipe[0]_i_2_n_0 ));
  FDRE \d16p2_wr_pipe_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(d16p2_wr),
        .Q(\d16p2_wr_pipe_reg_n_0_[0] ),
        .R(SR));
  FDRE \d16p2_wr_pipe_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d16p2_wr_pipe_reg_n_0_[0] ),
        .Q(p_4_in9_in),
        .R(SR));
  (* srl_bus_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d21p5_wr_pipe_reg " *) 
  (* srl_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 " *) 
  SRL16E \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(E),
        .CLK(Rx_SysClk),
        .D(d21p5_wr),
        .Q(\d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_i_1 
       (.I0(\k28p5_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[3] ),
        .I2(\wr_data_reg_n_0_[0] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[5] ),
        .O(d21p5_wr));
  FDRE \d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d21p5_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\d21p5_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ),
        .R(1'b0));
  FDRE \d21p5_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d21p5_wr_pipe_reg[3]_0 ),
        .Q(d21p5_wr_pipe),
        .R(SR));
  (* srl_bus_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d2p2_wr_pipe_reg " *) 
  (* srl_name = "inst/\pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 " *) 
  SRL16E \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(E),
        .CLK(Rx_SysClk),
        .D(d2p2_wr),
        .Q(\d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_i_1 
       (.I0(\d16p2_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[4] ),
        .I2(\wr_data_reg_n_0_[1] ),
        .I3(p_0_in),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(d2p2_wr));
  FDRE \d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d2p2_wr_pipe_reg[1]_srl2___pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_0_n_0 ),
        .Q(\d2p2_wr_pipe_reg[2]_pcs_pma_block_i_gen_lvds_transceiver.gen_lvds_transceiver_logic_r_1_0 ),
        .R(1'b0));
  FDRE \d2p2_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\d2p2_wr_pipe_reg[3]_0 ),
        .Q(d2p2_wr_pipe),
        .R(SR));
  LUT6 #(
    .INIT(64'h4444444454555555)) 
    even_i_1
       (.I0(even),
        .I1(even_i_2_n_0),
        .I2(insert_idle_i_6_n_0),
        .I3(insert_idle_i_4_n_0),
        .I4(insert_idle_i_3_n_0),
        .I5(insert_idle_i_2_n_0),
        .O(even_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    even_i_2
       (.I0(even_i_3_n_0),
        .I1(insert_idle_i_11_n_0),
        .I2(insert_idle_i_10_n_0),
        .I3(\rd_data_reg_reg_n_0_[1] ),
        .I4(\rd_data_reg_reg_n_0_[2] ),
        .I5(\rd_data_reg_n_0_[11] ),
        .O(even_i_2_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    even_i_3
       (.I0(rd_occupancy[1]),
        .I1(rd_occupancy[0]),
        .I2(rd_occupancy[3]),
        .I3(rd_occupancy[2]),
        .I4(rd_occupancy[4]),
        .I5(rd_occupancy[5]),
        .O(even_i_3_n_0));
  FDSE even_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(even_i_1_n_0),
        .Q(even),
        .S(reset_modified));
  LUT1 #(
    .INIT(2'h1)) 
    \initialize_counter[0]_i_1 
       (.I0(initialize_counter_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \initialize_counter[1]_i_1 
       (.I0(initialize_counter_reg[0]),
        .I1(initialize_counter_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \initialize_counter[2]_i_1 
       (.I0(initialize_counter_reg[2]),
        .I1(initialize_counter_reg[1]),
        .I2(initialize_counter_reg[0]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \initialize_counter[3]_i_1 
       (.I0(initialize_counter_reg[3]),
        .I1(initialize_counter_reg[0]),
        .I2(initialize_counter_reg[1]),
        .I3(initialize_counter_reg[2]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \initialize_counter[4]_i_1 
       (.I0(initialize_counter_reg[4]),
        .I1(initialize_counter_reg[2]),
        .I2(initialize_counter_reg[1]),
        .I3(initialize_counter_reg[0]),
        .I4(initialize_counter_reg[3]),
        .O(p_0_in__0[4]));
  LUT2 #(
    .INIT(4'h2)) 
    \initialize_counter[5]_i_1 
       (.I0(initialize_ram),
        .I1(initialize_ram_complete_i_3_n_0),
        .O(initialize_counter0));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \initialize_counter[5]_i_2 
       (.I0(initialize_counter_reg[5]),
        .I1(initialize_counter_reg[3]),
        .I2(initialize_counter_reg[0]),
        .I3(initialize_counter_reg[1]),
        .I4(initialize_counter_reg[2]),
        .I5(initialize_counter_reg[4]),
        .O(p_0_in__0[5]));
  FDRE \initialize_counter_reg[0] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[0]),
        .Q(initialize_counter_reg[0]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[1] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[1]),
        .Q(initialize_counter_reg[1]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[2] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[2]),
        .Q(initialize_counter_reg[2]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[3] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[3]),
        .Q(initialize_counter_reg[3]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[4] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[4]),
        .Q(initialize_counter_reg[4]),
        .R(initialize_ram0));
  FDRE \initialize_counter_reg[5] 
       (.C(Rx_SysClk),
        .CE(initialize_counter0),
        .D(p_0_in__0[5]),
        .Q(initialize_counter_reg[5]),
        .R(initialize_ram0));
  LUT3 #(
    .INIT(8'hFE)) 
    initialize_ram_complete_i_1
       (.I0(start),
        .I1(reset_out),
        .I2(\initialize_counter_reg[5]_0 ),
        .O(initialize_ram0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT2 #(
    .INIT(4'hE)) 
    initialize_ram_complete_i_2
       (.I0(initialize_ram_complete_i_3_n_0),
        .I1(data_in),
        .O(initialize_ram_complete_i_2_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    initialize_ram_complete_i_3
       (.I0(initialize_counter_reg[5]),
        .I1(initialize_counter_reg[3]),
        .I2(initialize_counter_reg[0]),
        .I3(initialize_counter_reg[1]),
        .I4(initialize_counter_reg[2]),
        .I5(initialize_counter_reg[4]),
        .O(initialize_ram_complete_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_complete_pulse_i_1
       (.I0(data_in),
        .I1(initialize_ram_complete_reg__0),
        .O(initialize_ram_complete_pulse0));
  FDRE initialize_ram_complete_pulse_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_complete_pulse0),
        .Q(initialize_ram_complete_pulse_reg_0),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_complete_i_2_n_0),
        .Q(data_in),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_reg_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_in),
        .Q(initialize_ram_complete_reg__0),
        .R(initialize_ram0));
  FDRE initialize_ram_complete_sync_reg1_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_out),
        .Q(initialize_ram_complete_sync_reg1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    initialize_ram_complete_sync_ris_edg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(initialize_ram_complete_sync_ris_edg0),
        .Q(initialize_ram_complete_sync_ris_edg),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    initialize_ram_i_1
       (.I0(initialize_ram),
        .I1(data_in),
        .O(initialize_ram_i_1_n_0));
  FDSE initialize_ram_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(initialize_ram_i_1_n_0),
        .Q(initialize_ram),
        .S(initialize_ram0));
  LUT6 #(
    .INIT(64'h00AA00EA000000EA)) 
    insert_idle_i_1
       (.I0(insert_idle_i_2_n_0),
        .I1(insert_idle_i_3_n_0),
        .I2(insert_idle_i_4_n_0),
        .I3(insert_idle_i_5_n_0),
        .I4(insert_idle_i_6_n_0),
        .I5(insert_idle_i_7_n_0),
        .O(even0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    insert_idle_i_10
       (.I0(\rd_data_reg_reg_n_0_[3] ),
        .I1(\rd_data_reg_reg_n_0_[4] ),
        .I2(p_1_in3_in),
        .I3(\rd_data_reg_reg_n_0_[6] ),
        .O(insert_idle_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    insert_idle_i_11
       (.I0(\rd_data_reg_reg_n_0_[7] ),
        .I1(\rd_data_reg_reg_n_0_[5] ),
        .I2(\rd_data_reg_reg_n_0_[0] ),
        .I3(insert_idle),
        .O(insert_idle_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    insert_idle_i_12
       (.I0(\rd_data_reg_n_0_[7] ),
        .I1(\rd_data_reg_n_0_[3] ),
        .I2(\rd_data_reg_n_0_[6] ),
        .I3(\rd_data_reg_n_0_[1] ),
        .O(insert_idle_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    insert_idle_i_2
       (.I0(insert_idle_i_8_n_0),
        .I1(\rd_data_reg_n_0_[6] ),
        .I2(\rd_data_reg_n_0_[1] ),
        .I3(\rd_data_reg_n_0_[4] ),
        .I4(rd_occupancy[6]),
        .I5(\rd_data_reg_n_0_[7] ),
        .O(insert_idle_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00200000)) 
    insert_idle_i_3
       (.I0(\rd_data_reg_n_0_[1] ),
        .I1(\rd_data_reg_n_0_[7] ),
        .I2(\rd_data_reg_n_0_[6] ),
        .I3(\rd_data_reg_n_0_[4] ),
        .I4(insert_idle_i_8_n_0),
        .I5(insert_idle_i_9_n_0),
        .O(insert_idle_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'h01)) 
    insert_idle_i_4
       (.I0(rd_occupancy[4]),
        .I1(rd_occupancy[5]),
        .I2(rd_occupancy[6]),
        .O(insert_idle_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    insert_idle_i_5
       (.I0(\rd_data_reg_n_0_[11] ),
        .I1(\rd_data_reg_reg_n_0_[2] ),
        .I2(\rd_data_reg_reg_n_0_[1] ),
        .I3(insert_idle_i_10_n_0),
        .I4(insert_idle_i_11_n_0),
        .O(insert_idle_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    insert_idle_i_6
       (.I0(rd_occupancy[2]),
        .I1(rd_occupancy[3]),
        .I2(rd_occupancy[0]),
        .I3(rd_occupancy[1]),
        .O(insert_idle_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT2 #(
    .INIT(4'h7)) 
    insert_idle_i_7
       (.I0(rd_occupancy[5]),
        .I1(rd_occupancy[4]),
        .O(insert_idle_i_7_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    insert_idle_i_8
       (.I0(\rd_data_reg_n_0_[0] ),
        .I1(\rd_data_reg_n_0_[2] ),
        .I2(\rd_data_reg_n_0_[5] ),
        .I3(\rd_data_reg_n_0_[3] ),
        .O(insert_idle_i_8_n_0));
  LUT5 #(
    .INIT(32'h00008000)) 
    insert_idle_i_9
       (.I0(\rd_data_reg_n_0_[4] ),
        .I1(\rd_data_reg_n_0_[5] ),
        .I2(\rd_data_reg_n_0_[2] ),
        .I3(\rd_data_reg_n_0_[0] ),
        .I4(insert_idle_i_12_n_0),
        .O(insert_idle_i_9_n_0));
  FDRE insert_idle_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(even0),
        .Q(insert_idle),
        .R(reset_modified));
  FDRE insert_idle_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(insert_idle),
        .Q(insert_idle_reg__0),
        .R(reset_modified));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT5 #(
    .INIT(32'h00800000)) 
    \k28p5_wr_pipe[0]_i_1 
       (.I0(\k28p5_wr_pipe[0]_i_2_n_0 ),
        .I1(\wr_data_reg_n_0_[3] ),
        .I2(\wr_data_reg_n_0_[5] ),
        .I3(\wr_data_reg_n_0_[0] ),
        .I4(p_0_in),
        .O(p_3_out));
  LUT5 #(
    .INIT(32'h00000800)) 
    \k28p5_wr_pipe[0]_i_2 
       (.I0(\wr_data_reg_n_0_[4] ),
        .I1(\wr_data_reg_n_0_[2] ),
        .I2(\wr_data_reg_n_0_[1] ),
        .I3(\wr_data_reg_n_0_[7] ),
        .I4(\wr_data_reg_n_0_[6] ),
        .O(\k28p5_wr_pipe[0]_i_2_n_0 ));
  FDRE \k28p5_wr_pipe_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_3_out),
        .Q(\k28p5_wr_pipe_reg_n_0_[0] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[0] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[1] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[1] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[2] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[2] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[3] ),
        .R(SR));
  FDRE \k28p5_wr_pipe_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\k28p5_wr_pipe_reg_n_0_[3] ),
        .Q(\k28p5_wr_pipe_reg_n_0_[4] ),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_0_63_0_6" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 ram_reg_0_63_0_6
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[0]),
        .DIB(wr_data_reg_reg[1]),
        .DIC(wr_data_reg_reg[2]),
        .DID(wr_data_reg_reg[3]),
        .DIE(wr_data_reg_reg[4]),
        .DIF(wr_data_reg_reg[5]),
        .DIG(wr_data_reg_reg[6]),
        .DIH(1'b0),
        .DOA(ram_reg_0_63_0_6_n_0),
        .DOB(ram_reg_0_63_0_6_n_1),
        .DOC(ram_reg_0_63_0_6_n_2),
        .DOD(ram_reg_0_63_0_6_n_3),
        .DOE(ram_reg_0_63_0_6_n_4),
        .DOF(ram_reg_0_63_0_6_n_5),
        .DOG(ram_reg_0_63_0_6_n_6),
        .DOH(NLW_ram_reg_0_63_0_6_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(ram_reg_0_63_0_6_i_1_n_0));
  LUT5 #(
    .INIT(32'h00005575)) 
    ram_reg_0_63_0_6_i_1
       (.I0(data_in),
        .I1(D),
        .I2(E),
        .I3(remove_idle_reg_reg_0),
        .I4(wr_addr[6]),
        .O(ram_reg_0_63_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_0_63_7_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "13" *) 
  RAM64M8 ram_reg_0_63_7_13
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[7]),
        .DIB(wr_data_reg_reg[8]),
        .DIC(wr_data_reg_reg[9]),
        .DID(wr_data_reg_reg[10]),
        .DIE(wr_data_reg_reg[11]),
        .DIF(wr_data_reg_reg[12]),
        .DIG(wr_data_reg_reg[13]),
        .DIH(1'b0),
        .DOA(ram_reg_0_63_7_13_n_0),
        .DOB(ram_reg_0_63_7_13_n_1),
        .DOC(ram_reg_0_63_7_13_n_2),
        .DOD(ram_reg_0_63_7_13_n_3),
        .DOE(ram_reg_0_63_7_13_n_4),
        .DOF(ram_reg_0_63_7_13_n_5),
        .DOG(ram_reg_0_63_7_13_n_6),
        .DOH(NLW_ram_reg_0_63_7_13_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(ram_reg_0_63_0_6_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_64_127_0_6" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "64" *) 
  (* ram_addr_end = "127" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "6" *) 
  RAM64M8 ram_reg_64_127_0_6
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[0]),
        .DIB(wr_data_reg_reg[1]),
        .DIC(wr_data_reg_reg[2]),
        .DID(wr_data_reg_reg[3]),
        .DIE(wr_data_reg_reg[4]),
        .DIF(wr_data_reg_reg[5]),
        .DIG(wr_data_reg_reg[6]),
        .DIH(1'b0),
        .DOA(ram_reg_64_127_0_6_n_0),
        .DOB(ram_reg_64_127_0_6_n_1),
        .DOC(ram_reg_64_127_0_6_n_2),
        .DOD(ram_reg_64_127_0_6_n_3),
        .DOE(ram_reg_64_127_0_6_n_4),
        .DOF(ram_reg_64_127_0_6_n_5),
        .DOG(ram_reg_64_127_0_6_n_6),
        .DOH(NLW_ram_reg_64_127_0_6_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "1792" *) 
  (* RTL_RAM_NAME = "pcs_pma_block_i/gen_lvds_transceiver.gen_lvds_transceiver_logic[0].lvds_transceiver_inst/rx_elastic_buffer_inst/ram_reg_64_127_7_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "64" *) 
  (* ram_addr_end = "127" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "13" *) 
  RAM64M8 ram_reg_64_127_7_13
       (.ADDRA(rd_addr[5:0]),
        .ADDRB(rd_addr[5:0]),
        .ADDRC(rd_addr[5:0]),
        .ADDRD(rd_addr[5:0]),
        .ADDRE(rd_addr[5:0]),
        .ADDRF(rd_addr[5:0]),
        .ADDRG(rd_addr[5:0]),
        .ADDRH(wr_addr[5:0]),
        .DIA(wr_data_reg_reg[7]),
        .DIB(wr_data_reg_reg[8]),
        .DIC(wr_data_reg_reg[9]),
        .DID(wr_data_reg_reg[10]),
        .DIE(wr_data_reg_reg[11]),
        .DIF(wr_data_reg_reg[12]),
        .DIG(wr_data_reg_reg[13]),
        .DIH(1'b0),
        .DOA(ram_reg_64_127_7_13_n_0),
        .DOB(ram_reg_64_127_7_13_n_1),
        .DOC(ram_reg_64_127_7_13_n_2),
        .DOD(ram_reg_64_127_7_13_n_3),
        .DOE(ram_reg_64_127_7_13_n_4),
        .DOF(ram_reg_64_127_7_13_n_5),
        .DOG(ram_reg_64_127_7_13_n_6),
        .DOH(NLW_ram_reg_64_127_7_13_DOH_UNCONNECTED),
        .WCLK(Rx_SysClk),
        .WE(\__1/ram_reg_64_127_0_6_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \rd_addr[6]_i_1 
       (.I0(insert_idle_reg__0),
        .I1(insert_idle),
        .O(rd_enable));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[0]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in),
        .O(\rd_addr_gray[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[1]_i_1 
       (.I0(p_1_in),
        .I1(p_2_in20_in),
        .O(\rd_addr_gray[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[2]_i_1 
       (.I0(p_2_in20_in),
        .I1(p_3_in),
        .O(\rd_addr_gray[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[3]_i_1 
       (.I0(p_3_in),
        .I1(p_4_in23_in),
        .O(\rd_addr_gray[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[4]_i_1 
       (.I0(p_4_in23_in),
        .I1(p_5_in),
        .O(\rd_addr_gray[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \rd_addr_gray[5]_i_1 
       (.I0(p_5_in),
        .I1(\rd_addr_plus2_reg_n_0_[6] ),
        .O(\rd_addr_gray[5]_i_1_n_0 ));
  FDRE \rd_addr_gray_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[0]_i_1_n_0 ),
        .Q(rd_addr_gray[0]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[1]_i_1_n_0 ),
        .Q(rd_addr_gray[1]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[2]_i_1_n_0 ),
        .Q(rd_addr_gray[2]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[3]_i_1_n_0 ),
        .Q(rd_addr_gray[3]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[4]_i_1_n_0 ),
        .Q(rd_addr_gray[4]),
        .R(reset_modified));
  FDRE \rd_addr_gray_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[5]_i_1_n_0 ),
        .Q(rd_addr_gray[5]),
        .R(reset_modified));
  FDSE \rd_addr_plus1_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_plus2_reg_n_0_[0] ),
        .Q(rd_addr_plus1[0]),
        .S(reset_modified));
  FDRE \rd_addr_plus1_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_1_in),
        .Q(rd_addr_plus1[1]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_2_in20_in),
        .Q(rd_addr_plus1[2]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_3_in),
        .Q(rd_addr_plus1[3]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_4_in23_in),
        .Q(rd_addr_plus1[4]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_5_in),
        .Q(rd_addr_plus1[5]),
        .R(reset_modified));
  FDRE \rd_addr_plus1_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_plus2_reg_n_0_[6] ),
        .Q(rd_addr_plus1[6]),
        .R(reset_modified));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \rd_addr_plus2[0]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \rd_addr_plus2[2]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in),
        .I2(p_2_in20_in),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \rd_addr_plus2[3]_i_1 
       (.I0(p_3_in),
        .I1(\rd_addr_plus2_reg_n_0_[0] ),
        .I2(p_1_in),
        .I3(p_2_in20_in),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \rd_addr_plus2[4]_i_1 
       (.I0(p_4_in23_in),
        .I1(p_2_in20_in),
        .I2(p_1_in),
        .I3(\rd_addr_plus2_reg_n_0_[0] ),
        .I4(p_3_in),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \rd_addr_plus2[5]_i_1 
       (.I0(p_5_in),
        .I1(p_3_in),
        .I2(\rd_addr_plus2_reg_n_0_[0] ),
        .I3(p_1_in),
        .I4(p_2_in20_in),
        .I5(p_4_in23_in),
        .O(p_0_in__1[5]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \rd_addr_plus2[6]_i_1 
       (.I0(\rd_addr_plus2_reg_n_0_[6] ),
        .I1(p_4_in23_in),
        .I2(\rd_addr_plus2[6]_i_2_n_0 ),
        .I3(p_3_in),
        .I4(p_5_in),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \rd_addr_plus2[6]_i_2 
       (.I0(p_2_in20_in),
        .I1(p_1_in),
        .I2(\rd_addr_plus2_reg_n_0_[0] ),
        .O(\rd_addr_plus2[6]_i_2_n_0 ));
  FDRE \rd_addr_plus2_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[0]),
        .Q(\rd_addr_plus2_reg_n_0_[0] ),
        .R(reset_modified));
  FDSE \rd_addr_plus2_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(\rd_addr_gray[0]_i_1_n_0 ),
        .Q(p_1_in),
        .S(reset_modified));
  FDRE \rd_addr_plus2_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[2]),
        .Q(p_2_in20_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[3]),
        .Q(p_3_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[4]),
        .Q(p_4_in23_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[5]),
        .Q(p_5_in),
        .R(reset_modified));
  FDRE \rd_addr_plus2_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(p_0_in__1[6]),
        .Q(\rd_addr_plus2_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_addr_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[0]),
        .Q(rd_addr[0]),
        .R(reset_modified));
  FDRE \rd_addr_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[1]),
        .Q(rd_addr[1]),
        .R(reset_modified));
  FDRE \rd_addr_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[2]),
        .Q(rd_addr[2]),
        .R(reset_modified));
  FDRE \rd_addr_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[3]),
        .Q(rd_addr[3]),
        .R(reset_modified));
  FDRE \rd_addr_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[4]),
        .Q(rd_addr[4]),
        .R(reset_modified));
  FDRE \rd_addr_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[5]),
        .Q(rd_addr[5]),
        .R(reset_modified));
  FDRE \rd_addr_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable),
        .D(rd_addr_plus1[6]),
        .Q(rd_addr[6]),
        .R(reset_modified));
  FDRE \rd_data_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[0]),
        .Q(\rd_data_reg_n_0_[0] ),
        .R(reset_modified));
  FDRE \rd_data_reg[10] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[10]),
        .Q(\rd_data_reg_n_0_[10] ),
        .R(reset_modified));
  FDRE \rd_data_reg[11] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[11]),
        .Q(\rd_data_reg_n_0_[11] ),
        .R(reset_modified));
  FDRE \rd_data_reg[12] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[12]),
        .Q(\rd_data_reg_n_0_[12] ),
        .R(reset_modified));
  FDRE \rd_data_reg[13] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[13]),
        .Q(\rd_data_reg_n_0_[13] ),
        .R(reset_modified));
  FDRE \rd_data_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[1]),
        .Q(\rd_data_reg_n_0_[1] ),
        .R(reset_modified));
  FDRE \rd_data_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[2]),
        .Q(\rd_data_reg_n_0_[2] ),
        .R(reset_modified));
  FDRE \rd_data_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[3]),
        .Q(\rd_data_reg_n_0_[3] ),
        .R(reset_modified));
  FDRE \rd_data_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[4]),
        .Q(\rd_data_reg_n_0_[4] ),
        .R(reset_modified));
  FDRE \rd_data_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[5]),
        .Q(\rd_data_reg_n_0_[5] ),
        .R(reset_modified));
  FDRE \rd_data_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[6]),
        .Q(\rd_data_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_data_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[7]),
        .Q(\rd_data_reg_n_0_[7] ),
        .R(reset_modified));
  FDRE \rd_data_reg[8] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[8]),
        .Q(p_2_in),
        .R(reset_modified));
  FDRE \rd_data_reg[9] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(dpo[9]),
        .Q(\rd_data_reg_n_0_[9] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[0] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[0] ),
        .Q(\rd_data_reg_reg_n_0_[0] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[10] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[10] ),
        .Q(\rd_data_reg_reg_n_0_[10] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[11] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[11] ),
        .Q(p_1_in3_in),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[12] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[12] ),
        .Q(\rd_data_reg_reg_n_0_[12] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[13] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[13] ),
        .Q(\rd_data_reg_reg[13]_0 ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[1] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[1] ),
        .Q(\rd_data_reg_reg_n_0_[1] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[2] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[2] ),
        .Q(\rd_data_reg_reg_n_0_[2] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[3] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[3] ),
        .Q(\rd_data_reg_reg_n_0_[3] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[4] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[4] ),
        .Q(\rd_data_reg_reg_n_0_[4] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[5] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[5] ),
        .Q(\rd_data_reg_reg_n_0_[5] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[6] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[6] ),
        .Q(\rd_data_reg_reg_n_0_[6] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[7] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[7] ),
        .Q(\rd_data_reg_reg_n_0_[7] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[8] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(p_2_in),
        .Q(\rd_data_reg_reg_n_0_[8] ),
        .R(reset_modified));
  FDRE \rd_data_reg_reg[9] 
       (.C(Tx_WrClk),
        .CE(rd_enable_reg),
        .D(\rd_data_reg_n_0_[9] ),
        .Q(\rd_data_reg_reg_n_0_[9] ),
        .R(reset_modified));
  FDSE rd_enable_reg_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_enable),
        .Q(rd_enable_reg),
        .S(reset_modified));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 rd_occupancy0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_rd_occupancy0_carry_CO_UNCONNECTED[7:6],rd_occupancy0_carry_n_2,rd_occupancy0_carry_n_3,rd_occupancy0_carry_n_4,rd_occupancy0_carry_n_5,rd_occupancy0_carry_n_6,rd_occupancy0_carry_n_7}),
        .DI({1'b0,1'b0,rd_wr_addr}),
        .O({NLW_rd_occupancy0_carry_O_UNCONNECTED[7],rd_occupancy01_out}),
        .S({1'b0,\reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[6].sync_wr_addrgray_n_1 ,\reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_1 }));
  LUT2 #(
    .INIT(4'h6)) 
    rd_occupancy0_carry_i_1
       (.I0(rd_wr_addr_gray_5),
        .I1(rd_wr_addr_gray_6),
        .O(rd_wr_addr[5]));
  LUT3 #(
    .INIT(8'h96)) 
    rd_occupancy0_carry_i_2
       (.I0(rd_wr_addr_gray_4),
        .I1(rd_wr_addr_gray_6),
        .I2(rd_wr_addr_gray_5),
        .O(rd_wr_addr[4]));
  LUT4 #(
    .INIT(16'h6996)) 
    rd_occupancy0_carry_i_3
       (.I0(rd_wr_addr_gray_3),
        .I1(rd_wr_addr_gray_5),
        .I2(rd_wr_addr_gray_6),
        .I3(rd_wr_addr_gray_4),
        .O(rd_wr_addr[3]));
  LUT5 #(
    .INIT(32'h96696996)) 
    rd_occupancy0_carry_i_4
       (.I0(rd_wr_addr_gray_2),
        .I1(rd_wr_addr_gray_4),
        .I2(rd_wr_addr_gray_6),
        .I3(rd_wr_addr_gray_5),
        .I4(rd_wr_addr_gray_3),
        .O(rd_wr_addr[2]));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    rd_occupancy0_carry_i_5
       (.I0(rd_wr_addr_gray_1),
        .I1(rd_wr_addr_gray_3),
        .I2(rd_wr_addr_gray_5),
        .I3(rd_wr_addr_gray_6),
        .I4(rd_wr_addr_gray_4),
        .I5(rd_wr_addr_gray_2),
        .O(rd_wr_addr[1]));
  LUT2 #(
    .INIT(4'h6)) 
    rd_occupancy0_carry_i_6
       (.I0(rd_wr_addr_gray_0),
        .I1(rd_wr_addr[1]),
        .O(rd_wr_addr[0]));
  FDRE \rd_occupancy_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[0]),
        .Q(rd_occupancy[0]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[1]),
        .Q(rd_occupancy[1]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[2]),
        .Q(rd_occupancy[2]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[3]),
        .Q(rd_occupancy[3]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[4]),
        .Q(rd_occupancy[4]),
        .R(reset_modified));
  FDRE \rd_occupancy_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[5]),
        .Q(rd_occupancy[5]),
        .R(reset_modified));
  FDSE \rd_occupancy_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rd_occupancy01_out[6]),
        .Q(rd_occupancy[6]),
        .S(reset_modified));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_7 \reclock_rd_addrgray[0].sync_rd_addrgray 
       (.Q(rd_addr_gray[0]),
        .Rx_SysClk(Rx_SysClk),
        .data_out(wr_rd_addr_gray_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_8 \reclock_rd_addrgray[1].sync_rd_addrgray 
       (.Q(wr_addr[1:0]),
        .Rx_SysClk(Rx_SysClk),
        .S({\reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_1 }),
        .data_out(wr_rd_addr_gray_3),
        .data_sync_reg1_0(rd_addr_gray[1]),
        .wr_occupancy0_carry_i_7_0(wr_rd_addr_gray_5),
        .wr_occupancy0_carry_i_7_1(wr_rd_addr_gray_6),
        .wr_occupancy0_carry_i_7_2(wr_rd_addr_gray_4),
        .wr_occupancy0_carry_i_7_3(wr_rd_addr_gray_2),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_9 \reclock_rd_addrgray[2].sync_rd_addrgray 
       (.Q(rd_addr_gray[2]),
        .Rx_SysClk(Rx_SysClk),
        .data_out(wr_rd_addr_gray_2));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_10 \reclock_rd_addrgray[3].sync_rd_addrgray 
       (.Q(wr_addr[2]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_3),
        .data_sync_reg1_0(rd_addr_gray[3]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_5),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_1 (wr_rd_addr_gray_4),
        .\wr_occupancy_reg[6]_2 (wr_rd_addr_gray_2));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_11 \reclock_rd_addrgray[4].sync_rd_addrgray 
       (.Q(wr_addr[3]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_4),
        .data_sync_reg1_0(rd_addr_gray[4]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_5),
        .\wr_occupancy_reg[6]_1 (wr_rd_addr_gray_3));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_12 \reclock_rd_addrgray[5].sync_rd_addrgray 
       (.Q(wr_addr[4]),
        .Rx_SysClk(Rx_SysClk),
        .S(\reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ),
        .data_out(wr_rd_addr_gray_5),
        .data_sync_reg1_0(rd_addr_gray[5]),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6]_0 (wr_rd_addr_gray_4));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_13 \reclock_rd_addrgray[6].sync_rd_addrgray 
       (.Q(wr_addr[6:5]),
        .Rx_SysClk(Rx_SysClk),
        .S({\reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[6].sync_rd_addrgray_n_1 }),
        .data_in(rd_addr_plus1[6]),
        .data_out(wr_rd_addr_gray_6),
        .\wr_occupancy_reg[6] (wr_rd_addr_gray_5));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_14 \reclock_wr_addrgray[0].sync_wr_addrgray 
       (.Q(wr_addr_gray[0]),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_15 \reclock_wr_addrgray[1].sync_wr_addrgray 
       (.Q(rd_addr[1:0]),
        .S({\reclock_wr_addrgray[1].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[1].sync_wr_addrgray_n_1 }),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_0),
        .data_sync_reg1_0(wr_addr_gray[1]),
        .data_sync_reg6_0(rd_wr_addr_gray_1),
        .rd_wr_addr(rd_wr_addr[1]));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_16 \reclock_wr_addrgray[2].sync_wr_addrgray 
       (.Q(wr_addr_gray[2]),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_2));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_17 \reclock_wr_addrgray[3].sync_wr_addrgray 
       (.Q(rd_addr[2]),
        .S(\reclock_wr_addrgray[3].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_3),
        .data_sync_reg1_0(wr_addr_gray[3]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_5),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_1 (rd_wr_addr_gray_4),
        .\rd_occupancy_reg[6]_2 (rd_wr_addr_gray_2));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_18 \reclock_wr_addrgray[4].sync_wr_addrgray 
       (.Q(rd_addr[3]),
        .S(\reclock_wr_addrgray[4].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_4),
        .data_sync_reg1_0(wr_addr_gray[4]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_5),
        .\rd_occupancy_reg[6]_1 (rd_wr_addr_gray_3));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_19 \reclock_wr_addrgray[5].sync_wr_addrgray 
       (.Q(rd_addr[4]),
        .S(\reclock_wr_addrgray[5].sync_wr_addrgray_n_0 ),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_5),
        .data_sync_reg1_0(wr_addr_gray[5]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_6),
        .\rd_occupancy_reg[6]_0 (rd_wr_addr_gray_4));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_20 \reclock_wr_addrgray[6].sync_wr_addrgray 
       (.Q(rd_addr[6:5]),
        .S({\reclock_wr_addrgray[6].sync_wr_addrgray_n_0 ,\reclock_wr_addrgray[6].sync_wr_addrgray_n_1 }),
        .Tx_WrClk(Tx_WrClk),
        .data_out(rd_wr_addr_gray_6),
        .data_sync_reg1_0(wr_addr_gray[6]),
        .\rd_occupancy_reg[6] (rd_wr_addr_gray_5));
  LUT5 #(
    .INIT(32'h00E00000)) 
    remove_idle_i_1
       (.I0(remove_idle_i_2_n_0),
        .I1(remove_idle_i_3_n_0),
        .I2(\k28p5_wr_pipe_reg_n_0_[0] ),
        .I3(D),
        .I4(wr_occupancy[6]),
        .O(remove_idle0));
  LUT6 #(
    .INIT(64'h8080808080800080)) 
    remove_idle_i_2
       (.I0(d16p2_wr),
        .I1(p_4_in9_in),
        .I2(\k28p5_wr_pipe_reg_n_0_[2] ),
        .I3(remove_idle_i_4_n_0),
        .I4(wr_occupancy[5]),
        .I5(wr_occupancy[1]),
        .O(remove_idle_i_2_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0E000E)) 
    remove_idle_i_3
       (.I0(d21p5_wr),
        .I1(d2p2_wr),
        .I2(remove_idle_i_5_n_0),
        .I3(remove_idle_i_4_n_0),
        .I4(wr_occupancy[1]),
        .I5(wr_occupancy[0]),
        .O(remove_idle_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    remove_idle_i_4
       (.I0(wr_occupancy[2]),
        .I1(wr_occupancy[4]),
        .I2(wr_occupancy[3]),
        .O(remove_idle_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    remove_idle_i_5
       (.I0(remove_idle_reg3),
        .I1(wr_occupancy[5]),
        .I2(remove_idle_reg4),
        .I3(\k28p5_wr_pipe_reg_n_0_[4] ),
        .I4(remove_idle_i_6_n_0),
        .O(remove_idle_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFF1)) 
    remove_idle_i_6
       (.I0(d21p5_wr_pipe),
        .I1(d2p2_wr_pipe),
        .I2(remove_idle_reg_reg_0),
        .I3(remove_idle_reg2),
        .O(remove_idle_i_6_n_0));
  FDRE remove_idle_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle0),
        .Q(D),
        .R(SR));
  FDRE remove_idle_reg2_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg_reg_0),
        .Q(remove_idle_reg2),
        .R(SR));
  FDRE remove_idle_reg3_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg2),
        .Q(remove_idle_reg3),
        .R(SR));
  FDRE remove_idle_reg4_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(remove_idle_reg3),
        .Q(remove_idle_reg4),
        .R(SR));
  FDRE remove_idle_reg_reg
       (.C(Rx_SysClk),
        .CE(E),
        .D(D),
        .Q(remove_idle_reg_reg_0),
        .R(SR));
  LUT5 #(
    .INIT(32'h77777774)) 
    reset_modified_i_1
       (.I0(initialize_ram_complete_sync_ris_edg),
        .I1(reset_modified),
        .I2(reset_modified_reg_0),
        .I3(mgt_rx_reset),
        .I4(elastic_buffer_rst_125),
        .O(reset_modified_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_modified_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(reset_modified_i_1_n_0),
        .Q(reset_modified),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAAAAAB)) 
    rxbuferr_i_1
       (.I0(rxbuferr_i_2_n_0),
        .I1(rd_occupancy[6]),
        .I2(rd_occupancy[5]),
        .I3(rd_occupancy[4]),
        .I4(rxbuferr_i_3_n_0),
        .I5(rxbufstatus),
        .O(rxbuferr_i_1_n_0));
  LUT6 #(
    .INIT(64'h4040400000000000)) 
    rxbuferr_i_2
       (.I0(insert_idle_i_7_n_0),
        .I1(rd_occupancy[3]),
        .I2(rd_occupancy[2]),
        .I3(rd_occupancy[1]),
        .I4(rd_occupancy[0]),
        .I5(rd_occupancy[6]),
        .O(rxbuferr_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'hFFF8)) 
    rxbuferr_i_3
       (.I0(rd_occupancy[0]),
        .I1(rd_occupancy[1]),
        .I2(rd_occupancy[2]),
        .I3(rd_occupancy[3]),
        .O(rxbuferr_i_3_n_0));
  FDRE rxbuferr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxbuferr_i_1_n_0),
        .Q(rxbufstatus),
        .R(reset_modified));
  LUT3 #(
    .INIT(8'hAB)) 
    rxchariscomma_usr_i_1
       (.I0(reset_modified),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxchariscomma_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    rxchariscomma_usr_i_2
       (.I0(\rd_data_reg_reg_n_0_[12] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxchariscomma_usr_i_2_n_0));
  FDRE rxchariscomma_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxchariscomma_usr_i_2_n_0),
        .Q(rxchariscomma_usr_reg_0),
        .R(rxchariscomma_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    rxcharisk_usr_i_1
       (.I0(p_1_in3_in),
        .I1(rd_enable_reg),
        .I2(even),
        .O(rxcharisk_usr_i_1_n_0));
  FDRE rxcharisk_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxcharisk_usr_i_1_n_0),
        .Q(rxcharisk_usr_reg_0),
        .R(rxchariscomma_usr_i_1_n_0));
  LUT5 #(
    .INIT(32'h0000440C)) 
    \rxclkcorcnt[2]_i_1 
       (.I0(rxclkcorcnt[1]),
        .I1(insert_idle_reg__0),
        .I2(\rd_data_reg_reg[13]_0 ),
        .I3(rxclkcorcnt[0]),
        .I4(reset_modified),
        .O(\rxclkcorcnt[2]_i_1_n_0 ));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxclkcorcnt_reg[0]_0 ),
        .Q(rxclkcorcnt[0]),
        .R(reset_modified));
  FDRE \rxclkcorcnt_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxclkcorcnt[2]_i_1_n_0 ),
        .Q(rxclkcorcnt[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \rxdata_usr[0]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[0] ),
        .I1(rd_enable_reg),
        .O(\rxdata_usr[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \rxdata_usr[1]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[1] ),
        .I1(rd_enable_reg),
        .O(\rxdata_usr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[2]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[2] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[3]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[3] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'hD)) 
    \rxdata_usr[4]_i_1 
       (.I0(rd_enable_reg),
        .I1(\rd_data_reg_reg_n_0_[4] ),
        .O(\rxdata_usr[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[5]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[5] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hC5)) 
    \rxdata_usr[6]_i_1 
       (.I0(even),
        .I1(\rd_data_reg_reg_n_0_[6] ),
        .I2(rd_enable_reg),
        .O(\rxdata_usr[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata_usr[7]_i_1 
       (.I0(\rd_data_reg_reg_n_0_[7] ),
        .I1(rd_enable_reg),
        .I2(even),
        .O(\rxdata_usr[7]_i_1_n_0 ));
  FDRE \rxdata_usr_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[0]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [0]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[1]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [1]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[2]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [2]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[3]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [3]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[4] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[4]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [4]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[5] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[5]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [5]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[6] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[6]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [6]),
        .R(reset_modified));
  FDRE \rxdata_usr_reg[7] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rxdata_usr[7]_i_1_n_0 ),
        .Q(\rxdata_usr_reg[7]_0 [7]),
        .R(reset_modified));
  LUT2 #(
    .INIT(4'hB)) 
    rxdisperr_usr_i_1
       (.I0(reset_modified),
        .I1(rd_enable_reg),
        .O(rxdisperr_usr_i_1_n_0));
  FDRE rxdisperr_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rd_data_reg_reg_n_0_[10] ),
        .Q(rxdisperr),
        .R(rxdisperr_usr_i_1_n_0));
  FDRE rxnotintable_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(\rd_data_reg_reg_n_0_[9] ),
        .Q(rxnotintable),
        .R(rxdisperr_usr_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    rxrundisp_usr_i_1
       (.I0(p_2_in),
        .I1(even),
        .I2(rd_enable_reg),
        .I3(\rd_data_reg_reg_n_0_[8] ),
        .O(rxrundisp_usr_i_1_n_0));
  FDRE rxrundisp_usr_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(rxrundisp_usr_i_1_n_0),
        .Q(rxrundisp),
        .R(reset_modified));
  FDRE #(
    .INIT(1'b1)) 
    start_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(1'b0),
        .Q(start),
        .R(1'b0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_21 sync_initialize_ram_comp
       (.Tx_WrClk(Tx_WrClk),
        .data_out(data_out),
        .data_sync_reg1_0(data_in));
  LUT3 #(
    .INIT(8'hFE)) 
    \wr_addr[5]_i_1 
       (.I0(initialize_ram_complete_pulse_reg_0),
        .I1(reset_out),
        .I2(\initialize_counter_reg[5]_0 ),
        .O(wr_addr__0));
  LUT4 #(
    .INIT(16'h04FF)) 
    \wr_addr[5]_i_2 
       (.I0(remove_idle_reg_reg_0),
        .I1(E),
        .I2(D),
        .I3(data_in),
        .O(\wr_addr[5]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr[6]_i_3 
       (.I0(wr_addr_plus1[6]),
        .I1(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[1]_i_1 
       (.I0(p_1_in28_in),
        .I1(p_2_in29_in),
        .O(p_6_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[2]_i_1 
       (.I0(p_2_in29_in),
        .I1(p_3_in31_in),
        .O(p_6_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[3]_i_1 
       (.I0(p_3_in31_in),
        .I1(p_4_in33_in),
        .O(p_6_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[4]_i_1 
       (.I0(p_4_in33_in),
        .I1(p_5_in35_in),
        .O(p_6_out[4]));
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_gray[5]_i_1 
       (.I0(p_5_in35_in),
        .I1(\wr_addr_plus2_reg_n_0_[6] ),
        .O(p_6_out[5]));
  FDSE \wr_addr_gray_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_addr_plus2[1]_i_1_n_0 ),
        .Q(wr_addr_gray[0]),
        .S(SR));
  FDRE \wr_addr_gray_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[1]),
        .Q(wr_addr_gray[1]),
        .R(SR));
  FDRE \wr_addr_gray_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[2]),
        .Q(wr_addr_gray[2]),
        .R(SR));
  FDRE \wr_addr_gray_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[3]),
        .Q(wr_addr_gray[3]),
        .R(SR));
  FDRE \wr_addr_gray_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[4]),
        .Q(wr_addr_gray[4]),
        .R(SR));
  FDSE \wr_addr_gray_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_6_out[5]),
        .Q(wr_addr_gray[5]),
        .S(SR));
  FDSE \wr_addr_gray_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_addr_plus2_reg_n_0_[6] ),
        .Q(wr_addr_gray[6]),
        .S(SR));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \wr_addr_plus1[6]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[6] ),
        .I1(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr_plus1[6]_i_1_n_0 ));
  FDSE \wr_addr_plus1_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2_reg_n_0_[0] ),
        .Q(wr_addr_plus1[0]),
        .S(wr_addr__0));
  FDRE \wr_addr_plus1_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_1_in28_in),
        .Q(wr_addr_plus1[1]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_2_in29_in),
        .Q(wr_addr_plus1[2]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_3_in31_in),
        .Q(wr_addr_plus1[3]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_4_in33_in),
        .Q(wr_addr_plus1[4]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(p_5_in35_in),
        .Q(wr_addr_plus1[5]),
        .R(wr_addr__0));
  FDRE \wr_addr_plus1_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr_plus1[6]_i_1_n_0 ),
        .Q(wr_addr_plus1[6]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wr_addr_plus2[0]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .O(\wr_addr_plus2[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \wr_addr_plus2[1]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in28_in),
        .O(\wr_addr_plus2[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \wr_addr_plus2[2]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[0] ),
        .I1(p_1_in28_in),
        .I2(p_2_in29_in),
        .O(\wr_addr_plus2[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \wr_addr_plus2[3]_i_1 
       (.I0(p_1_in28_in),
        .I1(\wr_addr_plus2_reg_n_0_[0] ),
        .I2(p_2_in29_in),
        .I3(p_3_in31_in),
        .O(\wr_addr_plus2[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \wr_addr_plus2[4]_i_1 
       (.I0(p_2_in29_in),
        .I1(\wr_addr_plus2_reg_n_0_[0] ),
        .I2(p_1_in28_in),
        .I3(p_3_in31_in),
        .I4(p_4_in33_in),
        .O(\wr_addr_plus2[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \wr_addr_plus2[5]_i_1 
       (.I0(p_3_in31_in),
        .I1(p_1_in28_in),
        .I2(\wr_addr_plus2_reg_n_0_[0] ),
        .I3(p_2_in29_in),
        .I4(p_4_in33_in),
        .I5(p_5_in35_in),
        .O(\wr_addr_plus2[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT4 #(
    .INIT(16'hFF6A)) 
    \wr_addr_plus2[6]_i_1 
       (.I0(\wr_addr_plus2_reg_n_0_[6] ),
        .I1(p_5_in35_in),
        .I2(\wr_addr_plus2[6]_i_2_n_0 ),
        .I3(initialize_ram_complete_pulse_reg_0),
        .O(\wr_addr_plus2[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \wr_addr_plus2[6]_i_2 
       (.I0(p_4_in33_in),
        .I1(p_2_in29_in),
        .I2(\wr_addr_plus2_reg_n_0_[0] ),
        .I3(p_1_in28_in),
        .I4(p_3_in31_in),
        .O(\wr_addr_plus2[6]_i_2_n_0 ));
  FDRE \wr_addr_plus2_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[0]_i_1_n_0 ),
        .Q(\wr_addr_plus2_reg_n_0_[0] ),
        .R(wr_addr__0));
  FDSE \wr_addr_plus2_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[1]_i_1_n_0 ),
        .Q(p_1_in28_in),
        .S(wr_addr__0));
  FDRE \wr_addr_plus2_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[2]_i_1_n_0 ),
        .Q(p_2_in29_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[3]_i_1_n_0 ),
        .Q(p_3_in31_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[4]_i_1_n_0 ),
        .Q(p_4_in33_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(\wr_addr_plus2[5]_i_1_n_0 ),
        .Q(p_5_in35_in),
        .R(wr_addr__0));
  FDRE \wr_addr_plus2_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr_plus2[6]_i_1_n_0 ),
        .Q(\wr_addr_plus2_reg_n_0_[6] ),
        .R(SR));
  FDRE \wr_addr_reg[0] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[0]),
        .Q(wr_addr[0]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[1] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[1]),
        .Q(wr_addr[1]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[2] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[2]),
        .Q(wr_addr[2]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[3] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[3]),
        .Q(wr_addr[3]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[4] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[4]),
        .Q(wr_addr[4]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[5] 
       (.C(Rx_SysClk),
        .CE(\wr_addr[5]_i_2_n_0 ),
        .D(wr_addr_plus1[5]),
        .Q(wr_addr[5]),
        .R(wr_addr__0));
  FDRE \wr_addr_reg[6] 
       (.C(Rx_SysClk),
        .CE(\wr_addr_plus2_reg[6]_0 ),
        .D(\wr_addr[6]_i_3_n_0 ),
        .Q(wr_addr[6]),
        .R(SR));
  FDRE \wr_data_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [0]),
        .Q(\wr_data_reg_n_0_[0] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [10]),
        .Q(wr_data[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [11]),
        .Q(p_0_in),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [12]),
        .Q(wr_data[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [1]),
        .Q(\wr_data_reg_n_0_[1] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [2]),
        .Q(\wr_data_reg_n_0_[2] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [3]),
        .Q(\wr_data_reg_n_0_[3] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [4]),
        .Q(\wr_data_reg_n_0_[4] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [5]),
        .Q(\wr_data_reg_n_0_[5] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [6]),
        .Q(\wr_data_reg_n_0_[6] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [7]),
        .Q(\wr_data_reg_n_0_[7] ),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [8]),
        .Q(wr_data[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg[12]_0 [9]),
        .Q(wr_data[9]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[0] ),
        .Q(wr_data_reg[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[10]),
        .Q(wr_data_reg[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(p_0_in),
        .Q(wr_data_reg[11]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[12]),
        .Q(wr_data_reg[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[13] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(D),
        .Q(wr_data_reg[13]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[1] ),
        .Q(wr_data_reg[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[2] ),
        .Q(wr_data_reg[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[3] ),
        .Q(wr_data_reg[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[4] ),
        .Q(wr_data_reg[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[5] ),
        .Q(wr_data_reg[5]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[6] ),
        .Q(wr_data_reg[6]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(\wr_data_reg_n_0_[7] ),
        .Q(wr_data_reg[7]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[8]),
        .Q(wr_data_reg[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data[9]),
        .Q(wr_data_reg[9]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[0] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[0]),
        .Q(wr_data_reg_reg[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[10] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[10]),
        .Q(wr_data_reg_reg[10]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[11] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[11]),
        .Q(wr_data_reg_reg[11]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[12] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[12]),
        .Q(wr_data_reg_reg[12]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[13] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[13]),
        .Q(wr_data_reg_reg[13]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[1] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[1]),
        .Q(wr_data_reg_reg[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[2] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[2]),
        .Q(wr_data_reg_reg[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[3] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[3]),
        .Q(wr_data_reg_reg[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[4] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[4]),
        .Q(wr_data_reg_reg[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[5] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[5]),
        .Q(wr_data_reg_reg[5]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[6] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[6]),
        .Q(wr_data_reg_reg[6]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[7] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[7]),
        .Q(wr_data_reg_reg[7]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[8] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[8]),
        .Q(wr_data_reg_reg[8]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_data_reg_reg_reg[9] 
       (.C(Rx_SysClk),
        .CE(E),
        .D(wr_data_reg[9]),
        .Q(wr_data_reg_reg[9]),
        .R(\wr_data_reg[0]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 wr_occupancy0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_wr_occupancy0_carry_CO_UNCONNECTED[7:6],wr_occupancy0_carry_n_2,wr_occupancy0_carry_n_3,wr_occupancy0_carry_n_4,wr_occupancy0_carry_n_5,wr_occupancy0_carry_n_6,wr_occupancy0_carry_n_7}),
        .DI({1'b0,1'b0,wr_addr[5:0]}),
        .O({NLW_wr_occupancy0_carry_O_UNCONNECTED[7],wr_occupancy00_out}),
        .S({1'b0,\reclock_rd_addrgray[6].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[6].sync_rd_addrgray_n_1 ,\reclock_rd_addrgray[5].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[4].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[3].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_0 ,\reclock_rd_addrgray[1].sync_rd_addrgray_n_1 }));
  FDRE \wr_occupancy_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[0]),
        .Q(wr_occupancy[0]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[1]),
        .Q(wr_occupancy[1]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[2]),
        .Q(wr_occupancy[2]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[3]),
        .Q(wr_occupancy[3]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[4]),
        .Q(wr_occupancy[4]),
        .R(\wr_data_reg[0]_0 ));
  FDRE \wr_occupancy_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[5]),
        .Q(wr_occupancy[5]),
        .R(\wr_data_reg[0]_0 ));
  FDSE \wr_occupancy_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(wr_occupancy00_out[6]),
        .Q(wr_occupancy[6]),
        .S(\wr_data_reg[0]_0 ));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_rate_adapt
   (gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_rxd_0,
    reset_out,
    gmii_rx_er_out_reg_0,
    gmii_rx_dv,
    Tx_WrClk,
    gmii_rx_er_in,
    gmii_rxd);
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output [7:0]gmii_rxd_0;
  input reset_out;
  input gmii_rx_er_out_reg_0;
  input gmii_rx_dv;
  input Tx_WrClk;
  input gmii_rx_er_in;
  input [7:0]gmii_rxd;

  wire Tx_WrClk;
  wire gmii_rx_dv;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire gmii_rx_er_in;
  wire gmii_rx_er_out_reg_0;
  wire [7:0]gmii_rxd;
  wire [7:0]gmii_rxd_0;
  wire muxsel;
  wire muxsel_i_1_n_0;
  wire [3:0]p_0_in;
  wire reset_out;
  wire rx_dv_aligned;
  wire rx_dv_aligned_i_1_n_0;
  wire rx_dv_reg1;
  wire rx_dv_reg2;
  wire rx_er_aligned;
  wire rx_er_aligned_0;
  wire rx_er_reg1;
  wire rx_er_reg2;
  wire [7:0]rxd_aligned;
  wire \rxd_aligned[0]_i_1_n_0 ;
  wire \rxd_aligned[1]_i_1_n_0 ;
  wire \rxd_aligned[2]_i_1_n_0 ;
  wire \rxd_aligned[3]_i_1_n_0 ;
  wire \rxd_aligned[4]_i_1_n_0 ;
  wire \rxd_aligned[5]_i_1_n_0 ;
  wire \rxd_aligned[6]_i_1_n_0 ;
  wire \rxd_aligned[7]_i_1_n_0 ;
  wire \rxd_reg1_reg_n_0_[0] ;
  wire \rxd_reg1_reg_n_0_[1] ;
  wire \rxd_reg1_reg_n_0_[2] ;
  wire \rxd_reg1_reg_n_0_[3] ;
  wire [7:0]rxd_reg2;
  wire sfd_enable;
  wire sfd_enable0;
  wire sfd_enable_i_1_n_0;
  wire sfd_enable_i_2_n_0;
  wire sfd_enable_i_4_n_0;
  wire sfd_enable_i_5_n_0;

  FDRE #(
    .INIT(1'b0)) 
    gmii_rx_dv_out_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_aligned),
        .Q(gmii_rx_dv_0),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    gmii_rx_er_out_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_aligned),
        .Q(gmii_rx_er_0),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[0]),
        .Q(gmii_rxd_0[0]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[1]),
        .Q(gmii_rxd_0[1]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[2]),
        .Q(gmii_rxd_0[2]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[3]),
        .Q(gmii_rxd_0[3]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[4]),
        .Q(gmii_rxd_0[4]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[5]),
        .Q(gmii_rxd_0[5]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[6]),
        .Q(gmii_rxd_0[6]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_rxd_out_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rxd_aligned[7]),
        .Q(gmii_rxd_0[7]),
        .R(reset_out));
  LUT6 #(
    .INIT(64'h00000000CCCCA8CC)) 
    muxsel_i_1
       (.I0(sfd_enable_i_5_n_0),
        .I1(muxsel),
        .I2(sfd_enable_i_2_n_0),
        .I3(sfd_enable),
        .I4(sfd_enable_i_4_n_0),
        .I5(reset_out),
        .O(muxsel_i_1_n_0));
  FDRE muxsel_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(muxsel_i_1_n_0),
        .Q(muxsel),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hB0)) 
    rx_dv_aligned_i_1
       (.I0(rx_dv_reg1),
        .I1(muxsel),
        .I2(rx_dv_reg2),
        .O(rx_dv_aligned_i_1_n_0));
  FDRE rx_dv_aligned_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_aligned_i_1_n_0),
        .Q(rx_dv_aligned),
        .R(reset_out));
  FDRE rx_dv_reg1_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rx_dv),
        .Q(rx_dv_reg1),
        .R(reset_out));
  FDRE rx_dv_reg2_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_dv_reg1),
        .Q(rx_dv_reg2),
        .R(reset_out));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    rx_er_aligned_i_1
       (.I0(muxsel),
        .I1(rx_er_reg1),
        .I2(rx_er_reg2),
        .O(rx_er_aligned_0));
  FDRE rx_er_aligned_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_aligned_0),
        .Q(rx_er_aligned),
        .R(reset_out));
  FDRE rx_er_reg1_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rx_er_in),
        .Q(rx_er_reg1),
        .R(reset_out));
  FDRE rx_er_reg2_reg
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(rx_er_reg1),
        .Q(rx_er_reg2),
        .R(reset_out));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[0]_i_1 
       (.I0(rxd_reg2[4]),
        .I1(muxsel),
        .I2(rxd_reg2[0]),
        .O(\rxd_aligned[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[1]_i_1 
       (.I0(rxd_reg2[5]),
        .I1(muxsel),
        .I2(rxd_reg2[1]),
        .O(\rxd_aligned[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[2]_i_1 
       (.I0(rxd_reg2[6]),
        .I1(muxsel),
        .I2(rxd_reg2[2]),
        .O(\rxd_aligned[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[3]_i_1 
       (.I0(rxd_reg2[7]),
        .I1(muxsel),
        .I2(rxd_reg2[3]),
        .O(\rxd_aligned[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[4]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[0] ),
        .I1(muxsel),
        .I2(rxd_reg2[4]),
        .O(\rxd_aligned[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[5]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[1] ),
        .I1(muxsel),
        .I2(rxd_reg2[5]),
        .O(\rxd_aligned[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[6]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[2] ),
        .I1(muxsel),
        .I2(rxd_reg2[6]),
        .O(\rxd_aligned[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxd_aligned[7]_i_1 
       (.I0(\rxd_reg1_reg_n_0_[3] ),
        .I1(muxsel),
        .I2(rxd_reg2[7]),
        .O(\rxd_aligned[7]_i_1_n_0 ));
  FDRE \rxd_aligned_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[0]_i_1_n_0 ),
        .Q(rxd_aligned[0]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[1]_i_1_n_0 ),
        .Q(rxd_aligned[1]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[2]_i_1_n_0 ),
        .Q(rxd_aligned[2]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[3]_i_1_n_0 ),
        .Q(rxd_aligned[3]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[4]_i_1_n_0 ),
        .Q(rxd_aligned[4]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[5]_i_1_n_0 ),
        .Q(rxd_aligned[5]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[6]_i_1_n_0 ),
        .Q(rxd_aligned[6]),
        .R(reset_out));
  FDRE \rxd_aligned_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_aligned[7]_i_1_n_0 ),
        .Q(rxd_aligned[7]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[0]),
        .Q(\rxd_reg1_reg_n_0_[0] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[1]),
        .Q(\rxd_reg1_reg_n_0_[1] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[2]),
        .Q(\rxd_reg1_reg_n_0_[2] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[3]),
        .Q(\rxd_reg1_reg_n_0_[3] ),
        .R(reset_out));
  FDRE \rxd_reg1_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[4]),
        .Q(p_0_in[0]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[5]),
        .Q(p_0_in[1]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[6]),
        .Q(p_0_in[2]),
        .R(reset_out));
  FDRE \rxd_reg1_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(gmii_rxd[7]),
        .Q(p_0_in[3]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[0] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[0] ),
        .Q(rxd_reg2[0]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[1] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[1] ),
        .Q(rxd_reg2[1]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[2] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[2] ),
        .Q(rxd_reg2[2]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[3] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(\rxd_reg1_reg_n_0_[3] ),
        .Q(rxd_reg2[3]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[4] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[0]),
        .Q(rxd_reg2[4]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[5] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[1]),
        .Q(rxd_reg2[5]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[6] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[2]),
        .Q(rxd_reg2[6]),
        .R(reset_out));
  FDRE \rxd_reg2_reg[7] 
       (.C(Tx_WrClk),
        .CE(gmii_rx_er_out_reg_0),
        .D(p_0_in[3]),
        .Q(rxd_reg2[7]),
        .R(reset_out));
  LUT6 #(
    .INIT(64'hFFDDFFCCC0C8C0CC)) 
    sfd_enable_i_1
       (.I0(sfd_enable_i_2_n_0),
        .I1(sfd_enable0),
        .I2(gmii_rx_er_out_reg_0),
        .I3(sfd_enable_i_4_n_0),
        .I4(sfd_enable_i_5_n_0),
        .I5(sfd_enable),
        .O(sfd_enable_i_1_n_0));
  LUT5 #(
    .INIT(32'h04000000)) 
    sfd_enable_i_2
       (.I0(p_0_in[3]),
        .I1(gmii_rxd[0]),
        .I2(gmii_rxd[1]),
        .I3(gmii_rxd[3]),
        .I4(gmii_rxd[2]),
        .O(sfd_enable_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    sfd_enable_i_3
       (.I0(gmii_rx_dv),
        .I1(rx_dv_reg1),
        .O(sfd_enable0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    sfd_enable_i_4
       (.I0(p_0_in[0]),
        .I1(p_0_in[1]),
        .I2(gmii_rx_er_out_reg_0),
        .I3(p_0_in[2]),
        .O(sfd_enable_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFDFFF)) 
    sfd_enable_i_5
       (.I0(\rxd_reg1_reg_n_0_[0] ),
        .I1(\rxd_reg1_reg_n_0_[3] ),
        .I2(p_0_in[3]),
        .I3(\rxd_reg1_reg_n_0_[2] ),
        .I4(\rxd_reg1_reg_n_0_[1] ),
        .O(sfd_enable_i_5_n_0));
  FDRE sfd_enable_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(sfd_enable_i_1_n_0),
        .Q(sfd_enable),
        .R(reset_out));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_serdes_1_to_10
   (BaseX_Rx_Fifo_Rd_En,
    ActCnt_GE_HalfBT_reg_0,
    SR,
    E,
    ActiveIsSlve_reg_0,
    D,
    Mstr_Load_reg_0,
    WrapToZero,
    monitor_late_reg_0,
    insert5_reg_0,
    insert3_reg_0,
    Q,
    \Mstr_CntValIn_Out_reg[8]_0 ,
    \s_state_reg[4]_0 ,
    \Slve_CntValIn_Out_reg[1]_0 ,
    \s_state_reg[0]_0 ,
    \s_state_reg[5]_0 ,
    \s_state_reg[4]_1 ,
    \s_state_reg[2]_0 ,
    \s_state_reg[4]_2 ,
    \Slve_CntValIn_Out_reg[0]_0 ,
    \s_state_reg[0]_1 ,
    \active_reg[1]_0 ,
    \act_count_reg[0]_0 ,
    \act_count_reg[5]_0 ,
    \act_count_reg[4]_0 ,
    \act_count_reg[3]_0 ,
    \s_state_reg[3]_0 ,
    BaseX_Idly_Load,
    out,
    code_err_i,
    \grdni.run_disp_i_reg ,
    \grdni.run_disp_i_reg_0 ,
    decoded_rxchariscomma0,
    k,
    b3,
    D0,
    Rx_SysClk,
    LossOfSignal_reg_0,
    ActiveIsSlve_reg_1,
    Slve_Load_reg_0,
    Mstr_Load_reg_1,
    WrapToZero_reg_0,
    monitor_late_reg_1,
    insert5_reg_1,
    insert3_reg_1,
    BaseX_Rx_Q_Out,
    \IntReset_dly_reg[0]_0 ,
    \IntRx_BtVal_reg[8]_0 ,
    \grdni.run_disp_i_reg_1 );
  output [0:0]BaseX_Rx_Fifo_Rd_En;
  output ActCnt_GE_HalfBT_reg_0;
  output [0:0]SR;
  output [0:0]E;
  output ActiveIsSlve_reg_0;
  output [0:0]D;
  output [0:0]Mstr_Load_reg_0;
  output WrapToZero;
  output monitor_late_reg_0;
  output insert5_reg_0;
  output insert3_reg_0;
  output [6:0]Q;
  output [8:0]\Mstr_CntValIn_Out_reg[8]_0 ;
  output [4:0]\s_state_reg[4]_0 ;
  output \Slve_CntValIn_Out_reg[1]_0 ;
  output \s_state_reg[0]_0 ;
  output \s_state_reg[5]_0 ;
  output \s_state_reg[4]_1 ;
  output \s_state_reg[2]_0 ;
  output \s_state_reg[4]_2 ;
  output \Slve_CntValIn_Out_reg[0]_0 ;
  output \s_state_reg[0]_1 ;
  output \active_reg[1]_0 ;
  output \act_count_reg[0]_0 ;
  output [0:0]\act_count_reg[5]_0 ;
  output \act_count_reg[4]_0 ;
  output \act_count_reg[3]_0 ;
  output \s_state_reg[3]_0 ;
  output [1:0]BaseX_Idly_Load;
  output [4:0]out;
  output code_err_i;
  output \grdni.run_disp_i_reg ;
  output \grdni.run_disp_i_reg_0 ;
  output decoded_rxchariscomma0;
  output k;
  output [7:5]b3;
  input D0;
  input Rx_SysClk;
  input LossOfSignal_reg_0;
  input ActiveIsSlve_reg_1;
  input Slve_Load_reg_0;
  input Mstr_Load_reg_1;
  input WrapToZero_reg_0;
  input monitor_late_reg_1;
  input insert5_reg_1;
  input insert3_reg_1;
  input [7:0]BaseX_Rx_Q_Out;
  input [0:0]\IntReset_dly_reg[0]_0 ;
  input [5:0]\IntRx_BtVal_reg[8]_0 ;
  input [0:0]\grdni.run_disp_i_reg_1 ;

  wire ActCnt_EQ_BTval;
  wire ActCnt_EQ_BTval_i_2_n_0;
  wire ActCnt_EQ_BTval_i_3_n_0;
  wire ActCnt_EQ_BTval_i_4_n_0;
  wire ActCnt_EQ_BTval_i_5_n_0;
  wire ActCnt_EQ_BTval_i_6_n_0;
  wire ActCnt_EQ_BTval_i_7_n_0;
  wire ActCnt_EQ_BTval_reg_i_1_n_0;
  wire ActCnt_EQ_Zero;
  wire ActCnt_EQ_Zero_i_1_n_0;
  wire ActCnt_EQ_Zero_i_2_n_0;
  wire ActCnt_EQ_Zero_i_3_n_0;
  wire ActCnt_EQ_Zero_i_4_n_0;
  wire ActCnt_EQ_Zero_i_5_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_1_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_2_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_3_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_4_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_5_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_6_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_7_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_8_n_0;
  wire ActCnt_GE_HalfBT0_carry_i_9_n_0;
  wire ActCnt_GE_HalfBT0_carry_n_3;
  wire ActCnt_GE_HalfBT0_carry_n_4;
  wire ActCnt_GE_HalfBT0_carry_n_5;
  wire ActCnt_GE_HalfBT0_carry_n_6;
  wire ActCnt_GE_HalfBT0_carry_n_7;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_4 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_5 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_6 ;
  wire \ActCnt_GE_HalfBT0_inferred__0/i__carry_n_7 ;
  wire ActCnt_GE_HalfBT_i_1_n_0;
  wire ActCnt_GE_HalfBT_reg_0;
  wire ActiveIsSlve_reg_0;
  wire ActiveIsSlve_reg_1;
  wire Aligned;
  wire Aligned_i_10_n_0;
  wire Aligned_i_11_n_0;
  wire Aligned_i_12_n_0;
  wire Aligned_i_13_n_0;
  wire Aligned_i_14_n_0;
  wire Aligned_i_15_n_0;
  wire Aligned_i_16_n_0;
  wire Aligned_i_17_n_0;
  wire Aligned_i_18_n_0;
  wire Aligned_i_19_n_0;
  wire Aligned_i_1_n_0;
  wire Aligned_i_2_n_0;
  wire Aligned_i_3_n_0;
  wire Aligned_i_4_n_0;
  wire Aligned_i_5_n_0;
  wire Aligned_i_6_n_0;
  wire Aligned_i_7_n_0;
  wire Aligned_i_8_n_0;
  wire Aligned_i_9_n_0;
  wire [1:0]BaseX_Idly_Load;
  wire [0:0]BaseX_Rx_Fifo_Rd_En;
  wire [7:0]BaseX_Rx_Q_Out;
  wire [0:0]D;
  wire D0;
  wire [0:0]E;
  wire [0:0]IntReset_dly;
  wire [0:0]\IntReset_dly_reg[0]_0 ;
  wire \IntReset_dly_reg_n_0_[1] ;
  wire [5:0]\IntRx_BtVal_reg[8]_0 ;
  wire LossOfSignal_i_4_n_0;
  wire LossOfSignal_reg_0;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_2 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_3 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_4 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_5 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_6 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_7 ;
  wire \Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ;
  wire \Mstr_CntValIn_Out[0]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[1]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[2]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[3]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[3]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[4]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[4]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[5]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[6]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[7]_i_5_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_1_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_2_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_3_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_4_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_5_n_0 ;
  wire \Mstr_CntValIn_Out[8]_i_6_n_0 ;
  wire [8:0]\Mstr_CntValIn_Out_reg[8]_0 ;
  wire [1:0]Mstr_Load_dly;
  wire [0:0]Mstr_Load_reg_0;
  wire Mstr_Load_reg_1;
  wire [2:0]PhaseDet_CntDec;
  wire \PhaseDet_CntDec[0]_i_1_n_0 ;
  wire \PhaseDet_CntDec[1]_i_1_n_0 ;
  wire \PhaseDet_CntDec[2]_i_1_n_0 ;
  wire \PhaseDet_CntDec[2]_i_2_n_0 ;
  wire \PhaseDet_CntDec[2]_i_3_n_0 ;
  wire \PhaseDet_CntDec[2]_i_4_n_0 ;
  wire \PhaseDet_CntDec[2]_i_5_n_0 ;
  wire [2:0]PhaseDet_CntInc;
  wire \PhaseDet_CntInc[0]_i_1_n_0 ;
  wire \PhaseDet_CntInc[1]_i_1_n_0 ;
  wire \PhaseDet_CntInc[2]_i_1_n_0 ;
  wire \PhaseDet_CntInc[2]_i_2_n_0 ;
  wire \PhaseDet_CntInc[2]_i_3_n_0 ;
  wire \PhaseDet_CntInc[2]_i_4_n_0 ;
  wire \PhaseDet_CntInc[2]_i_5_n_0 ;
  wire [6:0]Q;
  wire \Rx_Algn_Data_Out[0]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[0]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[1]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[1]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[2]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[2]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[3]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[3]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[4]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[4]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[5]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[5]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[6]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[7]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[8]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_1_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_2_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_3_n_0 ;
  wire \Rx_Algn_Data_Out[9]_i_4_n_0 ;
  wire Rx_Algn_Valid_Out0;
  wire Rx_SysClk;
  wire Rx_Valid_Int_i_1_n_0;
  wire Rx_Valid_Int_reg_n_0;
  wire [0:0]SR;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_2 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_3 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_4 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_5 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_6 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_7 ;
  wire \Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ;
  wire \Slve_CntValIn_Out[0]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[1]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[1]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[2]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[3]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[4]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[5]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[6]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_4_n_0 ;
  wire \Slve_CntValIn_Out[7]_i_5_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_1_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_2_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_3_n_0 ;
  wire \Slve_CntValIn_Out[8]_i_5_n_0 ;
  wire \Slve_CntValIn_Out_reg[0]_0 ;
  wire \Slve_CntValIn_Out_reg[1]_0 ;
  wire [1:0]Slve_Load_dly;
  wire Slve_Load_reg_0;
  wire WrapToZero;
  wire WrapToZero_reg_0;
  wire \act_count[0]_i_1_n_0 ;
  wire \act_count[1]_i_1_n_0 ;
  wire \act_count[2]_i_1_n_0 ;
  wire \act_count[3]_i_1_n_0 ;
  wire \act_count[4]_i_1_n_0 ;
  wire \act_count[5]_i_1_n_0 ;
  wire \act_count[5]_i_2_n_0 ;
  wire \act_count[5]_i_4_n_0 ;
  wire \act_count[5]_i_6_n_0 ;
  wire \act_count[5]_i_7_n_0 ;
  wire \act_count[5]_i_8_n_0 ;
  wire [4:0]act_count_reg;
  wire \act_count_reg[0]_0 ;
  wire \act_count_reg[3]_0 ;
  wire \act_count_reg[4]_0 ;
  wire [0:0]\act_count_reg[5]_0 ;
  wire \active_reg[1]_0 ;
  wire \active_reg_n_0_[0] ;
  wire \active_reg_n_0_[1] ;
  wire \active_reg_n_0_[2] ;
  wire \active_reg_n_0_[3] ;
  wire [9:0]al_rx_data_out;
  wire [7:5]b3;
  wire code_err_i;
  wire [6:0]data0;
  wire [9:0]data9;
  wire [2:0]\decode_8b10b/b4_disp__9 ;
  wire [2:0]\decode_8b10b/b6_disp__34 ;
  wire \decode_8b10b/k28__1 ;
  wire \decode_8b10b/pdbr62__0 ;
  wire \decode_8b10b/sK28__2 ;
  wire decoded_rxchariscomma0;
  wire decoded_rxchariscomma_i_2_n_0;
  wire decoded_rxchariscomma_i_3_n_0;
  wire decoded_rxchariscomma_i_5_n_0;
  wire decoded_rxchariscomma_i_6_n_0;
  wire decoded_rxchariscomma_i_7_n_0;
  wire [7:0]delay_change;
  wire \delay_change[7]_i_1_n_0 ;
  wire \delay_change_reg_n_0_[0] ;
  wire \gcerr.CODE_ERR_i_2_n_0 ;
  wire \gcerr.CODE_ERR_i_3_n_0 ;
  wire \gcerr.CODE_ERR_i_4_n_0 ;
  wire \gcerr.CODE_ERR_i_6_n_0 ;
  wire \gde.gdeni.DISP_ERR_i_2_n_0 ;
  wire \gde.gdeni.DISP_ERR_i_3_n_0 ;
  wire \grdni.run_disp_i_reg ;
  wire \grdni.run_disp_i_reg_0 ;
  wire [0:0]\grdni.run_disp_i_reg_1 ;
  wire [9:0]hdataout;
  wire \hdataout[0]_i_1_n_0 ;
  wire \hdataout[0]_i_2_n_0 ;
  wire \hdataout[1]_i_1_n_0 ;
  wire \hdataout[1]_i_2_n_0 ;
  wire \hdataout[2]_i_1_n_0 ;
  wire \hdataout[2]_i_2_n_0 ;
  wire \hdataout[3]_i_1_n_0 ;
  wire \hdataout[3]_i_2_n_0 ;
  wire \hdataout[4]_i_1_n_0 ;
  wire \hdataout[4]_i_2_n_0 ;
  wire \hdataout[5]_i_1_n_0 ;
  wire \hdataout[5]_i_2_n_0 ;
  wire \hdataout[6]_i_1_n_0 ;
  wire \hdataout[6]_i_2_n_0 ;
  wire \hdataout[7]_i_1_n_0 ;
  wire \hdataout[7]_i_2_n_0 ;
  wire \hdataout[8]_i_1_n_0 ;
  wire \hdataout[8]_i_2_n_0 ;
  wire \hdataout[9]_i_2_n_0 ;
  wire \hdataout[9]_i_3_n_0 ;
  wire \holdreg[10]_i_1_n_0 ;
  wire \holdreg[11]_i_1_n_0 ;
  wire \holdreg[12]_i_1_n_0 ;
  wire \holdreg[13]_i_1_n_0 ;
  wire \holdreg[14]_i_1_n_0 ;
  wire \holdreg[1]_i_1_n_0 ;
  wire \holdreg[2]_i_1_n_0 ;
  wire \holdreg[3]_i_1_n_0 ;
  wire \holdreg[4]_i_1_n_0 ;
  wire \holdreg[5]_i_1_n_0 ;
  wire \holdreg[6]_i_1_n_0 ;
  wire \holdreg[7]_i_1_n_0 ;
  wire \holdreg[8]_i_1_n_0 ;
  wire \holdreg[9]_i_1_n_0 ;
  wire \holdreg_reg_n_0_[10] ;
  wire \holdreg_reg_n_0_[11] ;
  wire \holdreg_reg_n_0_[12] ;
  wire \holdreg_reg_n_0_[13] ;
  wire \holdreg_reg_n_0_[14] ;
  wire \holdreg_reg_n_0_[1] ;
  wire \holdreg_reg_n_0_[2] ;
  wire \holdreg_reg_n_0_[3] ;
  wire \holdreg_reg_n_0_[4] ;
  wire \holdreg_reg_n_0_[5] ;
  wire \holdreg_reg_n_0_[6] ;
  wire \holdreg_reg_n_0_[7] ;
  wire \holdreg_reg_n_0_[8] ;
  wire \holdreg_reg_n_0_[9] ;
  wire i___0_carry_i_1__0_n_0;
  wire i___0_carry_i_1_n_0;
  wire i___0_carry_i_2__0_n_0;
  wire i___0_carry_i_2_n_0;
  wire i___0_carry_i_3__0_n_0;
  wire i___0_carry_i_3_n_0;
  wire i___0_carry_i_4__0_n_0;
  wire i___0_carry_i_4_n_0;
  wire i___0_carry_i_5__0_n_0;
  wire i___0_carry_i_5_n_0;
  wire i___0_carry_i_6__0_n_0;
  wire i___0_carry_i_6_n_0;
  wire i___0_carry_i_7__0_n_0;
  wire i___0_carry_i_7_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire insert3_reg_0;
  wire insert3_reg_1;
  wire insert5_reg_0;
  wire insert5_reg_1;
  wire invby_e;
  wire invr6;
  wire k;
  wire k1;
  wire [7:3]monitor;
  wire monitor_late_reg_0;
  wire monitor_late_reg_1;
  wire \mpx[0]_i_10_n_0 ;
  wire \mpx[0]_i_11_n_0 ;
  wire \mpx[0]_i_12_n_0 ;
  wire \mpx[0]_i_13_n_0 ;
  wire \mpx[0]_i_14_n_0 ;
  wire \mpx[0]_i_15_n_0 ;
  wire \mpx[0]_i_16_n_0 ;
  wire \mpx[0]_i_17_n_0 ;
  wire \mpx[0]_i_18_n_0 ;
  wire \mpx[0]_i_19_n_0 ;
  wire \mpx[0]_i_1_n_0 ;
  wire \mpx[0]_i_20_n_0 ;
  wire \mpx[0]_i_21_n_0 ;
  wire \mpx[0]_i_22_n_0 ;
  wire \mpx[0]_i_23_n_0 ;
  wire \mpx[0]_i_24_n_0 ;
  wire \mpx[0]_i_2_n_0 ;
  wire \mpx[0]_i_3_n_0 ;
  wire \mpx[0]_i_4_n_0 ;
  wire \mpx[0]_i_5_n_0 ;
  wire \mpx[0]_i_6_n_0 ;
  wire \mpx[0]_i_7_n_0 ;
  wire \mpx[0]_i_8_n_0 ;
  wire \mpx[0]_i_9_n_0 ;
  wire \mpx[1]_i_1_n_0 ;
  wire \mpx[1]_i_2_n_0 ;
  wire \mpx[1]_i_3_n_0 ;
  wire \mpx[1]_i_4_n_0 ;
  wire \mpx[2]_i_1_n_0 ;
  wire \mpx[3]_i_10_n_0 ;
  wire \mpx[3]_i_11_n_0 ;
  wire \mpx[3]_i_1_n_0 ;
  wire \mpx[3]_i_2_n_0 ;
  wire \mpx[3]_i_3_n_0 ;
  wire \mpx[3]_i_4_n_0 ;
  wire \mpx[3]_i_5_n_0 ;
  wire \mpx[3]_i_6_n_0 ;
  wire \mpx[3]_i_7_n_0 ;
  wire \mpx[3]_i_8_n_0 ;
  wire \mpx[3]_i_9_n_0 ;
  wire [3:0]mpx__0;
  wire ndbr6;
  wire ndur6;
  wire [4:0]out;
  wire p_0_in0;
  wire [3:0]p_0_in0_in;
  wire [7:2]p_1_in;
  wire p_1_out;
  wire [7:4]p_2_out;
  wire [7:4]p_3_out;
  wire [4:0]pd_count;
  wire \pd_count[0]_i_1_n_0 ;
  wire \pd_count[1]_i_1_n_0 ;
  wire \pd_count[2]_i_1_n_0 ;
  wire \pd_count[2]_i_2_n_0 ;
  wire \pd_count[2]_i_3_n_0 ;
  wire \pd_count[3]_i_1_n_0 ;
  wire \pd_count[4]_i_1_n_0 ;
  wire \pd_count[4]_i_2_n_0 ;
  wire \pd_count[4]_i_3_n_0 ;
  wire pd_ovflw_down_i_2_n_0;
  wire pd_ovflw_down_reg_n_0;
  wire pd_ovflw_up;
  wire pd_ovflw_up_i_1_n_0;
  wire pd_ovflw_up_reg_n_0;
  wire pdbr6;
  wire pdur6;
  wire \rxdh_reg_n_0_[0] ;
  wire \rxdh_reg_n_0_[19] ;
  wire \rxdh_reg_n_0_[1] ;
  wire \rxdh_reg_n_0_[2] ;
  wire \rxdh_reg_n_0_[3] ;
  wire \rxdh_reg_n_0_[4] ;
  wire \rxdh_reg_n_0_[5] ;
  wire \rxdh_reg_n_0_[6] ;
  wire \rxdh_reg_n_0_[7] ;
  wire \rxdh_reg_n_0_[8] ;
  wire s_state;
  wire \s_state[0]_i_1_n_0 ;
  wire \s_state[0]_i_2_n_0 ;
  wire \s_state[1]_i_1_n_0 ;
  wire \s_state[1]_i_2_n_0 ;
  wire \s_state[2]_i_1_n_0 ;
  wire \s_state[3]_i_1_n_0 ;
  wire \s_state[4]_i_1_n_0 ;
  wire \s_state[5]_i_2_n_0 ;
  wire \s_state[5]_i_3_n_0 ;
  wire \s_state[5]_i_5_n_0 ;
  wire \s_state[5]_i_6_n_0 ;
  wire \s_state[5]_i_7_n_0 ;
  wire \s_state[5]_i_8_n_0 ;
  wire \s_state[5]_i_9_n_0 ;
  wire \s_state_reg[0]_0 ;
  wire \s_state_reg[0]_1 ;
  wire \s_state_reg[2]_0 ;
  wire \s_state_reg[3]_0 ;
  wire [4:0]\s_state_reg[4]_0 ;
  wire \s_state_reg[4]_1 ;
  wire \s_state_reg[4]_2 ;
  wire \s_state_reg[5]_0 ;
  wire \s_state_reg_n_0_[5] ;
  wire \toggle[0]_i_1_n_0 ;
  wire \toggle[1]_i_1_n_0 ;
  wire \toggle[2]_i_1_n_0 ;
  wire \toggle[3]_i_1_n_0 ;
  wire \toggle_reg_n_0_[0] ;
  wire \toggle_reg_n_0_[1] ;
  wire \toggle_reg_n_0_[2] ;
  wire [7:5]NLW_ActCnt_GE_HalfBT0_carry_CO_UNCONNECTED;
  wire [7:0]NLW_ActCnt_GE_HalfBT0_carry_O_UNCONNECTED;
  wire [7:5]\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [7:0]\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [7:6]\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED ;
  wire [7:7]\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED ;
  wire [7:6]\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED ;
  wire [7:7]\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_BTval_i_2
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(ActCnt_EQ_BTval_i_4_n_0),
        .I4(ActCnt_EQ_BTval_i_5_n_0),
        .O(ActCnt_EQ_BTval_i_2_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_BTval_i_3
       (.I0(\Slve_CntValIn_Out_reg[1]_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .I2(Q[0]),
        .I3(ActCnt_EQ_BTval_i_6_n_0),
        .I4(ActCnt_EQ_BTval_i_7_n_0),
        .O(ActCnt_EQ_BTval_i_3_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_4
       (.I0(p_1_in[4]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(p_1_in[3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(p_1_in[2]),
        .O(ActCnt_EQ_BTval_i_4_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_5
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(p_1_in[5]),
        .I2(p_1_in[6]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I5(p_1_in[7]),
        .O(ActCnt_EQ_BTval_i_5_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_6
       (.I0(p_1_in[3]),
        .I1(Q[2]),
        .I2(p_1_in[4]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(p_1_in[2]),
        .O(ActCnt_EQ_BTval_i_6_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    ActCnt_EQ_BTval_i_7
       (.I0(Q[4]),
        .I1(p_1_in[5]),
        .I2(p_1_in[6]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(p_1_in[7]),
        .O(ActCnt_EQ_BTval_i_7_n_0));
  FDRE ActCnt_EQ_BTval_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_EQ_BTval_reg_i_1_n_0),
        .Q(ActCnt_EQ_BTval),
        .R(SR));
  MUXF7 ActCnt_EQ_BTval_reg_i_1
       (.I0(ActCnt_EQ_BTval_i_2_n_0),
        .I1(ActCnt_EQ_BTval_i_3_n_0),
        .O(ActCnt_EQ_BTval_reg_i_1_n_0),
        .S(ActiveIsSlve_reg_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000400)) 
    ActCnt_EQ_Zero_i_1
       (.I0(ActiveIsSlve_reg_0),
        .I1(ActCnt_EQ_Zero_i_2_n_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(ActCnt_EQ_Zero_i_3_n_0),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I5(ActCnt_EQ_Zero_i_4_n_0),
        .O(ActCnt_EQ_Zero_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ActCnt_EQ_Zero_i_2
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(ActCnt_EQ_Zero_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_Zero_i_3
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(ActCnt_EQ_Zero_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    ActCnt_EQ_Zero_i_4
       (.I0(ActCnt_EQ_Zero_i_5_n_0),
        .I1(Q[6]),
        .I2(ActiveIsSlve_reg_0),
        .I3(Q[0]),
        .I4(\Slve_CntValIn_Out_reg[0]_0 ),
        .I5(\Slve_CntValIn_Out_reg[1]_0 ),
        .O(ActCnt_EQ_Zero_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    ActCnt_EQ_Zero_i_5
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[5]),
        .O(ActCnt_EQ_Zero_i_5_n_0));
  FDRE ActCnt_EQ_Zero_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_EQ_Zero_i_1_n_0),
        .Q(ActCnt_EQ_Zero),
        .R(SR));
  (* COMPARATOR_THRESHOLD = "11" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 ActCnt_GE_HalfBT0_carry
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({NLW_ActCnt_GE_HalfBT0_carry_CO_UNCONNECTED[7:5],ActCnt_GE_HalfBT0_carry_n_3,ActCnt_GE_HalfBT0_carry_n_4,ActCnt_GE_HalfBT0_carry_n_5,ActCnt_GE_HalfBT0_carry_n_6,ActCnt_GE_HalfBT0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,Q[6],ActCnt_GE_HalfBT0_carry_i_1_n_0,ActCnt_GE_HalfBT0_carry_i_2_n_0,ActCnt_GE_HalfBT0_carry_i_3_n_0,ActCnt_GE_HalfBT0_carry_i_4_n_0}),
        .O(NLW_ActCnt_GE_HalfBT0_carry_O_UNCONNECTED[7:0]),
        .S({1'b0,1'b0,1'b0,ActCnt_GE_HalfBT0_carry_i_5_n_0,ActCnt_GE_HalfBT0_carry_i_6_n_0,ActCnt_GE_HalfBT0_carry_i_7_n_0,ActCnt_GE_HalfBT0_carry_i_8_n_0,ActCnt_GE_HalfBT0_carry_i_9_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_1
       (.I0(Q[5]),
        .I1(p_1_in[7]),
        .I2(Q[4]),
        .I3(p_1_in[6]),
        .O(ActCnt_GE_HalfBT0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_2
       (.I0(Q[3]),
        .I1(p_1_in[5]),
        .I2(Q[2]),
        .I3(p_1_in[4]),
        .O(ActCnt_GE_HalfBT0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    ActCnt_GE_HalfBT0_carry_i_3
       (.I0(Q[1]),
        .I1(p_1_in[3]),
        .I2(Q[0]),
        .I3(p_1_in[2]),
        .O(ActCnt_GE_HalfBT0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    ActCnt_GE_HalfBT0_carry_i_4
       (.I0(\Slve_CntValIn_Out_reg[0]_0 ),
        .I1(\Slve_CntValIn_Out_reg[1]_0 ),
        .O(ActCnt_GE_HalfBT0_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    ActCnt_GE_HalfBT0_carry_i_5
       (.I0(Q[6]),
        .O(ActCnt_GE_HalfBT0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_6
       (.I0(p_1_in[7]),
        .I1(Q[5]),
        .I2(p_1_in[6]),
        .I3(Q[4]),
        .O(ActCnt_GE_HalfBT0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_7
       (.I0(p_1_in[5]),
        .I1(Q[3]),
        .I2(p_1_in[4]),
        .I3(Q[2]),
        .O(ActCnt_GE_HalfBT0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    ActCnt_GE_HalfBT0_carry_i_8
       (.I0(p_1_in[3]),
        .I1(Q[1]),
        .I2(p_1_in[2]),
        .I3(Q[0]),
        .O(ActCnt_GE_HalfBT0_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ActCnt_GE_HalfBT0_carry_i_9
       (.I0(\Slve_CntValIn_Out_reg[1]_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .O(ActCnt_GE_HalfBT0_carry_i_9_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \ActCnt_GE_HalfBT0_inferred__0/i__carry 
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_CO_UNCONNECTED [7:5],\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_4 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_5 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_6 ,\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_7 }),
        .DI({1'b0,1'b0,1'b0,\Mstr_CntValIn_Out_reg[8]_0 [8],i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_ActCnt_GE_HalfBT0_inferred__0/i__carry_O_UNCONNECTED [7:0]),
        .S({1'b0,1'b0,1'b0,i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0,i__carry_i_9_n_0}));
  LUT3 #(
    .INIT(8'hB8)) 
    ActCnt_GE_HalfBT_i_1
       (.I0(ActCnt_GE_HalfBT0_carry_n_3),
        .I1(ActiveIsSlve_reg_0),
        .I2(\ActCnt_GE_HalfBT0_inferred__0/i__carry_n_3 ),
        .O(ActCnt_GE_HalfBT_i_1_n_0));
  FDRE ActCnt_GE_HalfBT_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActCnt_GE_HalfBT_i_1_n_0),
        .Q(ActCnt_GE_HalfBT_reg_0),
        .R(SR));
  FDRE ActiveIsSlve_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(ActiveIsSlve_reg_1),
        .Q(ActiveIsSlve_reg_0),
        .R(SR));
  LUT5 #(
    .INIT(32'hFFFFFD00)) 
    Aligned_i_1
       (.I0(Aligned_i_2_n_0),
        .I1(Aligned_i_3_n_0),
        .I2(Aligned_i_4_n_0),
        .I3(Rx_Valid_Int_reg_n_0),
        .I4(Aligned),
        .O(Aligned_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Aligned_i_10
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .O(Aligned_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    Aligned_i_11
       (.I0(data9[0]),
        .I1(data9[1]),
        .I2(data9[5]),
        .I3(\rxdh_reg_n_0_[7] ),
        .O(Aligned_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    Aligned_i_12
       (.I0(data9[1]),
        .I1(data9[2]),
        .I2(data9[3]),
        .I3(data9[0]),
        .O(Aligned_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    Aligned_i_13
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[5] ),
        .I3(\rxdh_reg_n_0_[6] ),
        .O(Aligned_i_13_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    Aligned_i_14
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[1]),
        .I3(data9[2]),
        .O(Aligned_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    Aligned_i_15
       (.I0(\rxdh_reg_n_0_[5] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(data9[0]),
        .I3(data9[3]),
        .O(Aligned_i_15_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFDF)) 
    Aligned_i_16
       (.I0(data9[6]),
        .I1(data9[7]),
        .I2(data9[4]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(Aligned_i_16_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF7F)) 
    Aligned_i_17
       (.I0(data9[7]),
        .I1(data9[1]),
        .I2(data9[0]),
        .I3(data9[6]),
        .I4(data9[5]),
        .I5(data9[2]),
        .O(Aligned_i_17_n_0));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'hE)) 
    Aligned_i_18
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .O(Aligned_i_18_n_0));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT2 #(
    .INIT(4'h7)) 
    Aligned_i_19
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .O(Aligned_i_19_n_0));
  LUT6 #(
    .INIT(64'h0000000000005445)) 
    Aligned_i_2
       (.I0(\mpx[0]_i_4_n_0 ),
        .I1(Aligned_i_5_n_0),
        .I2(data9[7]),
        .I3(data9[6]),
        .I4(\mpx[0]_i_6_n_0 ),
        .I5(Aligned_i_6_n_0),
        .O(Aligned_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF60)) 
    Aligned_i_3
       (.I0(data9[9]),
        .I1(data9[8]),
        .I2(Aligned_i_7_n_0),
        .I3(\mpx[3]_i_4_n_0 ),
        .I4(\mpx[3]_i_3_n_0 ),
        .I5(\mpx[0]_i_5_n_0 ),
        .O(Aligned_i_3_n_0));
  LUT6 #(
    .INIT(64'h55FDFD55555D5D55)) 
    Aligned_i_4
       (.I0(\mpx[0]_i_3_n_0 ),
        .I1(Aligned_i_8_n_0),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(data9[3]),
        .I4(data9[2]),
        .I5(Aligned_i_9_n_0),
        .O(Aligned_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFDF0000FFDFFFDF)) 
    Aligned_i_5
       (.I0(\mpx[0]_i_16_n_0 ),
        .I1(Aligned_i_10_n_0),
        .I2(data9[4]),
        .I3(data9[5]),
        .I4(Aligned_i_11_n_0),
        .I5(\mpx[0]_i_19_n_0 ),
        .O(Aligned_i_5_n_0));
  LUT6 #(
    .INIT(64'h0060006000606666)) 
    Aligned_i_6
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(Aligned_i_12_n_0),
        .I3(Aligned_i_13_n_0),
        .I4(Aligned_i_14_n_0),
        .I5(Aligned_i_15_n_0),
        .O(Aligned_i_6_n_0));
  LUT6 #(
    .INIT(64'h40400000404000FF)) 
    Aligned_i_7
       (.I0(Aligned_i_16_n_0),
        .I1(data9[5]),
        .I2(data9[2]),
        .I3(Aligned_i_17_n_0),
        .I4(data9[3]),
        .I5(data9[4]),
        .O(Aligned_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    Aligned_i_8
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(data9[1]),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(data9[0]),
        .I4(\rxdh_reg_n_0_[4] ),
        .I5(Aligned_i_18_n_0),
        .O(Aligned_i_8_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    Aligned_i_9
       (.I0(data9[1]),
        .I1(\rxdh_reg_n_0_[4] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(data9[0]),
        .I4(\rxdh_reg_n_0_[8] ),
        .I5(Aligned_i_19_n_0),
        .O(Aligned_i_9_n_0));
  FDRE Aligned_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Aligned_i_1_n_0),
        .Q(Aligned),
        .R(SR));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    FifoRd_0
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(D0),
        .Q(BaseX_Rx_Fifo_Rd_En),
        .R(1'b0));
  FDRE \IntReset_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntReset_dly_reg[0]_0 ),
        .Q(IntReset_dly),
        .R(1'b0));
  FDRE \IntReset_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(IntReset_dly),
        .Q(\IntReset_dly_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \IntRx_BtVal_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [0]),
        .Q(p_1_in[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [1]),
        .Q(p_1_in[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [2]),
        .Q(p_1_in[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [3]),
        .Q(p_1_in[5]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [4]),
        .Q(p_1_in[6]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \IntRx_BtVal_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\IntRx_BtVal_reg[8]_0 [5]),
        .Q(p_1_in[7]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT2 #(
    .INIT(4'hE)) 
    LossOfSignal_i_2
       (.I0(act_count_reg[3]),
        .I1(act_count_reg[4]),
        .O(\act_count_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    LossOfSignal_i_3
       (.I0(\act_count[5]_i_4_n_0 ),
        .I1(LossOfSignal_i_4_n_0),
        .I2(\act_count[5]_i_7_n_0 ),
        .I3(act_count_reg[4]),
        .I4(act_count_reg[3]),
        .O(\act_count_reg[4]_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    LossOfSignal_i_4
       (.I0(p_0_in0_in[2]),
        .I1(\active_reg_n_0_[0] ),
        .I2(p_0_in0_in[1]),
        .I3(\active_reg_n_0_[1] ),
        .I4(\act_count[5]_i_6_n_0 ),
        .O(LossOfSignal_i_4_n_0));
  FDSE LossOfSignal_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(LossOfSignal_reg_0),
        .Q(SR),
        .S(\IntReset_dly_reg_n_0_[1] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \Mstr_CntValIn_Out0_inferred__1/i___0_carry 
       (.CI(ActCnt_GE_HalfBT_reg_0),
        .CI_TOP(1'b0),
        .CO({\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED [7:6],\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_2 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_3 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_4 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_5 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_6 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_7 }),
        .DI({1'b0,1'b0,Q[5:0]}),
        .O({\NLW_Mstr_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED [7],\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ,\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 }),
        .S({1'b0,i___0_carry_i_1_n_0,i___0_carry_i_2_n_0,i___0_carry_i_3_n_0,i___0_carry_i_4_n_0,i___0_carry_i_5_n_0,i___0_carry_i_6_n_0,i___0_carry_i_7_n_0}));
  LUT6 #(
    .INIT(64'h4400404400004000)) 
    \Mstr_CntValIn_Out[0]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\Slve_CntValIn_Out_reg[0]_0 ),
        .I3(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(\Mstr_CntValIn_Out[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA08A0080)) 
    \Mstr_CntValIn_Out[1]_i_1 
       (.I0(\Mstr_CntValIn_Out[1]_i_2_n_0 ),
        .I1(\Slve_CntValIn_Out_reg[1]_0 ),
        .I2(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(\Mstr_CntValIn_Out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT5 #(
    .INIT(32'h0000ABA0)) 
    \Mstr_CntValIn_Out[1]_i_2 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(SR),
        .O(\Mstr_CntValIn_Out[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Mstr_CntValIn_Out[1]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .O(\Mstr_CntValIn_Out[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4544555540440500)) 
    \Mstr_CntValIn_Out[2]_i_1 
       (.I0(\Mstr_CntValIn_Out[2]_i_2_n_0 ),
        .I1(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_15 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .O(\Mstr_CntValIn_Out[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBFBFBFBBB)) 
    \Mstr_CntValIn_Out[2]_i_2 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(p_1_in[2]),
        .O(\Mstr_CntValIn_Out[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'h000000F2)) 
    \Mstr_CntValIn_Out[2]_i_3 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [4]),
        .O(\Mstr_CntValIn_Out[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[3]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[3]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[2]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88883088BBBBFCBB)) 
    \Mstr_CntValIn_Out[3]_i_2 
       (.I0(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_14 ),
        .I1(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I2(p_1_in[3]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[4]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[4]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[3]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFC8830BB30BBFC88)) 
    \Mstr_CntValIn_Out[4]_i_2 
       (.I0(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_13 ),
        .I1(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I2(p_1_in[4]),
        .I3(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[5]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[5]_i_2_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[4]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9F90FFFF9F900000)) 
    \Mstr_CntValIn_Out[5]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out[5]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_12 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[5]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[5]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \Mstr_CntValIn_Out[5]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h08FBFB08FB08FB08)) 
    \Mstr_CntValIn_Out[5]_i_4 
       (.I0(p_1_in[5]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEFF4444BABB0000)) 
    \Mstr_CntValIn_Out[6]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[5]),
        .I5(\Mstr_CntValIn_Out[6]_i_2_n_0 ),
        .O(\Mstr_CntValIn_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9F90FFFF9F900000)) 
    \Mstr_CntValIn_Out[6]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out[6]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_11 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[6]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \Mstr_CntValIn_Out[6]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(\Mstr_CntValIn_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8BB8B8B8B8B8B8B8)) 
    \Mstr_CntValIn_Out[6]_i_4 
       (.I0(p_1_in[6]),
        .I1(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEFF4444BABB0000)) 
    \Mstr_CntValIn_Out[7]_i_1 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[6]),
        .I5(\Mstr_CntValIn_Out[7]_i_2_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6F60FFFF6F600000)) 
    \Mstr_CntValIn_Out[7]_i_2 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I1(\Mstr_CntValIn_Out[7]_i_3_n_0 ),
        .I2(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I3(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_10 ),
        .I4(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[7]_i_4_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Mstr_CntValIn_Out[7]_i_3 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .O(\Mstr_CntValIn_Out[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h08FBFB08)) 
    \Mstr_CntValIn_Out[7]_i_4 
       (.I0(p_1_in[7]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I4(\Mstr_CntValIn_Out[7]_i_5_n_0 ),
        .O(\Mstr_CntValIn_Out[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \Mstr_CntValIn_Out[7]_i_5 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(\Mstr_CntValIn_Out[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hBABAABFAAAAAAAAA)) 
    \Mstr_CntValIn_Out[8]_i_1 
       (.I0(SR),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\Mstr_CntValIn_Out[8]_i_3_n_0 ),
        .O(\Mstr_CntValIn_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEF40EA40EF40EF40)) 
    \Mstr_CntValIn_Out[8]_i_2 
       (.I0(SR),
        .I1(\Mstr_CntValIn_Out[8]_i_4_n_0 ),
        .I2(\Mstr_CntValIn_Out[8]_i_5_n_0 ),
        .I3(p_1_in[7]),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT5 #(
    .INIT(32'h10011110)) 
    \Mstr_CntValIn_Out[8]_i_3 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [2]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg[4]_0 [1]),
        .O(\Mstr_CntValIn_Out[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6600F0006600F0FF)) 
    \Mstr_CntValIn_Out[8]_i_4 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I1(ActCnt_EQ_Zero_i_3_n_0),
        .I2(\Mstr_CntValIn_Out0_inferred__1/i___0_carry_n_9 ),
        .I3(\Mstr_CntValIn_Out[2]_i_3_n_0 ),
        .I4(\Mstr_CntValIn_Out[1]_i_3_n_0 ),
        .I5(\Mstr_CntValIn_Out[8]_i_6_n_0 ),
        .O(\Mstr_CntValIn_Out[8]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT4 #(
    .INIT(16'hEE02)) 
    \Mstr_CntValIn_Out[8]_i_5 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .O(\Mstr_CntValIn_Out[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h9555555555555555)) 
    \Mstr_CntValIn_Out[8]_i_6 
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I3(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I4(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(\Mstr_CntValIn_Out[8]_i_6_n_0 ));
  FDRE \Mstr_CntValIn_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[0]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[1]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[2]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[3]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[4]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[5]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[6]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[7]_i_1_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .R(1'b0));
  FDRE \Mstr_CntValIn_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(\Mstr_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Mstr_CntValIn_Out[8]_i_2_n_0 ),
        .Q(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .R(1'b0));
  FDRE \Mstr_Load_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_reg_0),
        .Q(Mstr_Load_dly[0]),
        .R(1'b0));
  FDRE \Mstr_Load_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_dly[0]),
        .Q(Mstr_Load_dly[1]),
        .R(1'b0));
  FDSE Mstr_Load_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Mstr_Load_reg_1),
        .Q(Mstr_Load_reg_0),
        .S(SR));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \PhaseDet_CntDec[0]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .O(\PhaseDet_CntDec[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hB2DB)) 
    \PhaseDet_CntDec[1]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .O(\PhaseDet_CntDec[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \PhaseDet_CntDec[2]_i_1 
       (.I0(\PhaseDet_CntDec[2]_i_2_n_0 ),
        .I1(\PhaseDet_CntDec[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntDec[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntDec[2]_i_5_n_0 ),
        .O(\PhaseDet_CntDec[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntDec[2]_i_2 
       (.I0(monitor[4]),
        .I1(monitor_late_reg_0),
        .I2(monitor[3]),
        .I3(p_0_in0_in[0]),
        .I4(\active_reg_n_0_[3] ),
        .O(\PhaseDet_CntDec[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntDec[2]_i_3 
       (.I0(monitor[6]),
        .I1(monitor_late_reg_0),
        .I2(monitor[5]),
        .I3(p_0_in0_in[2]),
        .I4(p_0_in0_in[1]),
        .O(\PhaseDet_CntDec[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntDec[2]_i_4 
       (.I0(monitor[5]),
        .I1(monitor_late_reg_0),
        .I2(monitor[4]),
        .I3(p_0_in0_in[1]),
        .I4(p_0_in0_in[0]),
        .O(\PhaseDet_CntDec[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntDec[2]_i_5 
       (.I0(monitor[7]),
        .I1(monitor_late_reg_0),
        .I2(monitor[6]),
        .I3(p_0_in0_in[3]),
        .I4(p_0_in0_in[2]),
        .O(\PhaseDet_CntDec[2]_i_5_n_0 ));
  FDRE \PhaseDet_CntDec_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[0]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[0]),
        .R(1'b0));
  FDRE \PhaseDet_CntDec_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[1]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[1]),
        .R(1'b0));
  FDRE \PhaseDet_CntDec_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntDec[2]_i_1_n_0 ),
        .Q(PhaseDet_CntDec[2]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h6996)) 
    \PhaseDet_CntInc[0]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .O(\PhaseDet_CntInc[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT4 #(
    .INIT(16'hB2DB)) 
    \PhaseDet_CntInc[1]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .O(\PhaseDet_CntInc[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \PhaseDet_CntInc[2]_i_1 
       (.I0(\PhaseDet_CntInc[2]_i_2_n_0 ),
        .I1(\PhaseDet_CntInc[2]_i_3_n_0 ),
        .I2(\PhaseDet_CntInc[2]_i_4_n_0 ),
        .I3(\PhaseDet_CntInc[2]_i_5_n_0 ),
        .O(\PhaseDet_CntInc[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntInc[2]_i_2 
       (.I0(monitor[4]),
        .I1(monitor_late_reg_0),
        .I2(monitor[3]),
        .I3(\active_reg_n_0_[3] ),
        .I4(p_0_in0_in[0]),
        .O(\PhaseDet_CntInc[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntInc[2]_i_3 
       (.I0(monitor[6]),
        .I1(monitor_late_reg_0),
        .I2(monitor[5]),
        .I3(p_0_in0_in[1]),
        .I4(p_0_in0_in[2]),
        .O(\PhaseDet_CntInc[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT5 #(
    .INIT(32'hFF47B8FF)) 
    \PhaseDet_CntInc[2]_i_4 
       (.I0(monitor[7]),
        .I1(monitor_late_reg_0),
        .I2(monitor[6]),
        .I3(p_0_in0_in[2]),
        .I4(p_0_in0_in[3]),
        .O(\PhaseDet_CntInc[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT5 #(
    .INIT(32'h00B84700)) 
    \PhaseDet_CntInc[2]_i_5 
       (.I0(monitor[5]),
        .I1(monitor_late_reg_0),
        .I2(monitor[4]),
        .I3(p_0_in0_in[0]),
        .I4(p_0_in0_in[1]),
        .O(\PhaseDet_CntInc[2]_i_5_n_0 ));
  FDRE \PhaseDet_CntInc_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[0]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[0]),
        .R(1'b0));
  FDRE \PhaseDet_CntInc_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[1]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[1]),
        .R(1'b0));
  FDRE \PhaseDet_CntInc_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\PhaseDet_CntInc[2]_i_1_n_0 ),
        .Q(PhaseDet_CntInc[2]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[0]_i_1 
       (.I0(data9[0]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[4]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[0]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[0]_i_2 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[2] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[1] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[0] ),
        .O(\Rx_Algn_Data_Out[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[1]_i_1 
       (.I0(data9[1]),
        .I1(data9[0]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[5]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[1]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[1]_i_2 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[3] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[2] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[1] ),
        .O(\Rx_Algn_Data_Out[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[2]_i_1 
       (.I0(data9[2]),
        .I1(data9[1]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[6]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[2]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[2]_i_2 
       (.I0(\rxdh_reg_n_0_[5] ),
        .I1(\rxdh_reg_n_0_[4] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[3] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[2] ),
        .O(\Rx_Algn_Data_Out[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[3]_i_1 
       (.I0(data9[3]),
        .I1(data9[2]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[7]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[3]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[3]_i_2 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[4] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[3] ),
        .O(\Rx_Algn_Data_Out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[4]_i_1 
       (.I0(data9[4]),
        .I1(data9[3]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[8]_i_3_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[4]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[4]_i_2 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[5] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[4] ),
        .O(\Rx_Algn_Data_Out[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[5]_i_1 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[9]_i_4_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[5]_i_2_n_0 ),
        .O(\Rx_Algn_Data_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[5]_i_2 
       (.I0(\rxdh_reg_n_0_[8] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[6] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[5] ),
        .O(\Rx_Algn_Data_Out[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_1 
       (.I0(data9[6]),
        .I1(data9[5]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[6]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[6]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_2 
       (.I0(data9[4]),
        .I1(data9[3]),
        .I2(mpx__0[1]),
        .I3(data9[2]),
        .I4(mpx__0[0]),
        .I5(data9[1]),
        .O(\Rx_Algn_Data_Out[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[6]_i_3 
       (.I0(data9[0]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[6] ),
        .O(\Rx_Algn_Data_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_1 
       (.I0(data9[7]),
        .I1(data9[6]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[7]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[7]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_2 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(mpx__0[1]),
        .I3(data9[3]),
        .I4(mpx__0[0]),
        .I5(data9[2]),
        .O(\Rx_Algn_Data_Out[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[7]_i_3 
       (.I0(data9[1]),
        .I1(data9[0]),
        .I2(mpx__0[1]),
        .I3(\rxdh_reg_n_0_[8] ),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[7] ),
        .O(\Rx_Algn_Data_Out[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_1 
       (.I0(data9[8]),
        .I1(data9[7]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[8]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[8]_i_3_n_0 ),
        .O(\Rx_Algn_Data_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_2 
       (.I0(data9[6]),
        .I1(data9[5]),
        .I2(mpx__0[1]),
        .I3(data9[4]),
        .I4(mpx__0[0]),
        .I5(data9[3]),
        .O(\Rx_Algn_Data_Out[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[8]_i_3 
       (.I0(data9[2]),
        .I1(data9[1]),
        .I2(mpx__0[1]),
        .I3(data9[0]),
        .I4(mpx__0[0]),
        .I5(\rxdh_reg_n_0_[8] ),
        .O(\Rx_Algn_Data_Out[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_1 
       (.I0(data9[9]),
        .I1(data9[8]),
        .I2(mpx__0[3]),
        .I3(\Rx_Algn_Data_Out[9]_i_2_n_0 ),
        .I4(\Rx_Algn_Data_Out[9]_i_3_n_0 ),
        .I5(\Rx_Algn_Data_Out[9]_i_4_n_0 ),
        .O(\Rx_Algn_Data_Out[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_2 
       (.I0(data9[7]),
        .I1(data9[6]),
        .I2(mpx__0[1]),
        .I3(data9[5]),
        .I4(mpx__0[0]),
        .I5(data9[4]),
        .O(\Rx_Algn_Data_Out[9]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Rx_Algn_Data_Out[9]_i_3 
       (.I0(mpx__0[0]),
        .I1(mpx__0[3]),
        .I2(mpx__0[2]),
        .O(\Rx_Algn_Data_Out[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Rx_Algn_Data_Out[9]_i_4 
       (.I0(data9[3]),
        .I1(data9[2]),
        .I2(mpx__0[1]),
        .I3(data9[1]),
        .I4(mpx__0[0]),
        .I5(data9[0]),
        .O(\Rx_Algn_Data_Out[9]_i_4_n_0 ));
  FDRE \Rx_Algn_Data_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[0]_i_1_n_0 ),
        .Q(al_rx_data_out[0]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[1]_i_1_n_0 ),
        .Q(al_rx_data_out[1]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[2]_i_1_n_0 ),
        .Q(al_rx_data_out[2]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[3]_i_1_n_0 ),
        .Q(al_rx_data_out[3]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[4]_i_1_n_0 ),
        .Q(al_rx_data_out[4]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[5]_i_1_n_0 ),
        .Q(al_rx_data_out[5]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[6]_i_1_n_0 ),
        .Q(al_rx_data_out[6]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[7]_i_1_n_0 ),
        .Q(al_rx_data_out[7]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[8]_i_1_n_0 ),
        .Q(al_rx_data_out[8]),
        .R(1'b0));
  FDRE \Rx_Algn_Data_Out_reg[9] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\Rx_Algn_Data_Out[9]_i_1_n_0 ),
        .Q(al_rx_data_out[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    Rx_Algn_Valid_Out_i_1
       (.I0(Rx_Valid_Int_reg_n_0),
        .I1(Aligned),
        .O(Rx_Algn_Valid_Out0));
  FDRE Rx_Algn_Valid_Out_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Rx_Algn_Valid_Out0),
        .Q(E),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000057)) 
    Rx_Valid_Int_i_1
       (.I0(\toggle_reg_n_0_[2] ),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(\toggle_reg_n_0_[1] ),
        .I3(p_0_in0),
        .I4(Rx_Valid_Int_reg_n_0),
        .I5(SR),
        .O(Rx_Valid_Int_i_1_n_0));
  FDRE Rx_Valid_Int_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Rx_Valid_Int_i_1_n_0),
        .Q(Rx_Valid_Int_reg_n_0),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY8 \Slve_CntValIn_Out0_inferred__1/i___0_carry 
       (.CI(ActCnt_GE_HalfBT_reg_0),
        .CI_TOP(1'b0),
        .CO({\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_CO_UNCONNECTED [7:6],\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_2 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_3 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_4 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_5 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_6 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_7 }),
        .DI({1'b0,1'b0,\Mstr_CntValIn_Out_reg[8]_0 [7:2]}),
        .O({\NLW_Slve_CntValIn_Out0_inferred__1/i___0_carry_O_UNCONNECTED [7],\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ,\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 }),
        .S({1'b0,i___0_carry_i_1__0_n_0,i___0_carry_i_2__0_n_0,i___0_carry_i_3__0_n_0,i___0_carry_i_4__0_n_0,i___0_carry_i_5__0_n_0,i___0_carry_i_6__0_n_0,i___0_carry_i_7__0_n_0}));
  LUT6 #(
    .INIT(64'hFEEEAAAAAEEEAAAA)) 
    \Slve_CntValIn_Out[0]_i_1 
       (.I0(\Slve_CntValIn_Out[1]_i_2_n_0 ),
        .I1(\Slve_CntValIn_Out_reg[0]_0 ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(\Slve_CntValIn_Out[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8F0F0F7F0F0F0)) 
    \Slve_CntValIn_Out[1]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\Slve_CntValIn_Out[1]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out_reg[1]_0 ),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(\Slve_CntValIn_Out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \Slve_CntValIn_Out[1]_i_2 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(ActiveIsSlve_reg_0),
        .O(\Slve_CntValIn_Out[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFE44FE54FE44EE44)) 
    \Slve_CntValIn_Out[2]_i_1 
       (.I0(SR),
        .I1(\Slve_CntValIn_Out[2]_i_2_n_0 ),
        .I2(\Slve_CntValIn_Out[2]_i_3_n_0 ),
        .I3(p_1_in[2]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(ActiveIsSlve_reg_0),
        .O(\Slve_CntValIn_Out[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT5 #(
    .INIT(32'h8CCC8000)) 
    \Slve_CntValIn_Out[2]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_15 ),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(Q[0]),
        .O(\Slve_CntValIn_Out[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Slve_CntValIn_Out[2]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[2]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Slve_CntValIn_Out[3]_i_1 
       (.I0(p_1_in[3]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[3]_i_2_n_0 ),
        .O(\Slve_CntValIn_Out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h80BFFFFF80BF0000)) 
    \Slve_CntValIn_Out[3]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_14 ),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(Q[1]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\Slve_CntValIn_Out[3]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \Slve_CntValIn_Out[3]_i_3 
       (.I0(p_1_in[3]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[2]),
        .O(\Slve_CntValIn_Out[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Slve_CntValIn_Out[4]_i_1 
       (.I0(p_1_in[4]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[4]_i_2_n_0 ),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\Slve_CntValIn_Out[4]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT5 #(
    .INIT(32'h8CB3B38C)) 
    \Slve_CntValIn_Out[4]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_13 ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(Q[2]),
        .I4(Q[1]),
        .O(\Slve_CntValIn_Out[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \Slve_CntValIn_Out[4]_i_3 
       (.I0(p_1_in[4]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(p_1_in[3]),
        .O(\Slve_CntValIn_Out[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h8B8B8B88)) 
    \Slve_CntValIn_Out[5]_i_1 
       (.I0(p_1_in[5]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[5]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out[5]_i_3_n_0 ),
        .I4(\Slve_CntValIn_Out[5]_i_4_n_0 ),
        .O(\Slve_CntValIn_Out[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000474700FF)) 
    \Slve_CntValIn_Out[5]_i_2 
       (.I0(p_1_in[5]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(p_1_in[4]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT4 #(
    .INIT(16'h1540)) 
    \Slve_CntValIn_Out[5]_i_3 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\Slve_CntValIn_Out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF0F0F0F9F9F0F0F)) 
    \Slve_CntValIn_Out[5]_i_4 
       (.I0(Q[3]),
        .I1(\Slve_CntValIn_Out[5]_i_5_n_0 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_12 ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\Slve_CntValIn_Out[5]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \Slve_CntValIn_Out[5]_i_5 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\Slve_CntValIn_Out[5]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8B8B8B88)) 
    \Slve_CntValIn_Out[6]_i_1 
       (.I0(p_1_in[6]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[6]_i_2_n_0 ),
        .I3(\Slve_CntValIn_Out[6]_i_3_n_0 ),
        .I4(\Slve_CntValIn_Out[6]_i_4_n_0 ),
        .O(\Slve_CntValIn_Out[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000474700FF)) 
    \Slve_CntValIn_Out[6]_i_2 
       (.I0(p_1_in[6]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(ActiveIsSlve_reg_0),
        .I3(p_1_in[5]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\Slve_CntValIn_Out[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444000000004)) 
    \Slve_CntValIn_Out[6]_i_3 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[4]),
        .O(\Slve_CntValIn_Out[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF0F9F9F0F0F9F9F)) 
    \Slve_CntValIn_Out[6]_i_4 
       (.I0(Q[4]),
        .I1(\Slve_CntValIn_Out[6]_i_5_n_0 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_11 ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\Slve_CntValIn_Out[6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \Slve_CntValIn_Out[6]_i_5 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[3]),
        .O(\Slve_CntValIn_Out[6]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Slve_CntValIn_Out[7]_i_1 
       (.I0(p_1_in[7]),
        .I1(SR),
        .I2(\Slve_CntValIn_Out[7]_i_2_n_0 ),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\Slve_CntValIn_Out[7]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8B00B8FF8BFFB800)) 
    \Slve_CntValIn_Out[7]_i_2 
       (.I0(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_10 ),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\Slve_CntValIn_Out[7]_i_4_n_0 ),
        .I3(\s_state_reg[4]_0 [0]),
        .I4(Q[5]),
        .I5(\Slve_CntValIn_Out[7]_i_5_n_0 ),
        .O(\Slve_CntValIn_Out[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hCFC0AAAA)) 
    \Slve_CntValIn_Out[7]_i_3 
       (.I0(p_1_in[6]),
        .I1(p_1_in[7]),
        .I2(\s_state_reg[4]_0 [4]),
        .I3(ActiveIsSlve_reg_0),
        .I4(\s_state_reg[4]_0 [0]),
        .O(\Slve_CntValIn_Out[7]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \Slve_CntValIn_Out[7]_i_4 
       (.I0(Q[4]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\Slve_CntValIn_Out[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \Slve_CntValIn_Out[7]_i_5 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(\Slve_CntValIn_Out[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAABABBAFAAAAAAAA)) 
    \Slve_CntValIn_Out[8]_i_1 
       (.I0(SR),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(ActiveIsSlve_reg_0),
        .I5(\Mstr_CntValIn_Out[8]_i_3_n_0 ),
        .O(\Slve_CntValIn_Out[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000AEEA)) 
    \Slve_CntValIn_Out[8]_i_2 
       (.I0(\Slve_CntValIn_Out[8]_i_3_n_0 ),
        .I1(\s_state_reg[5]_0 ),
        .I2(\Slve_CntValIn_Out[8]_i_5_n_0 ),
        .I3(Q[6]),
        .I4(\s_state_reg[4]_0 [4]),
        .I5(SR),
        .O(\Slve_CntValIn_Out[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'hCF0AC00A)) 
    \Slve_CntValIn_Out[8]_i_3 
       (.I0(ActiveIsSlve_reg_0),
        .I1(\Slve_CntValIn_Out0_inferred__1/i___0_carry_n_9 ),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(p_1_in[7]),
        .O(\Slve_CntValIn_Out[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Slve_CntValIn_Out[8]_i_4 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [1]),
        .O(\s_state_reg[5]_0 ));
  LUT6 #(
    .INIT(64'h4000000000000002)) 
    \Slve_CntValIn_Out[8]_i_5 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(Q[5]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\Slve_CntValIn_Out[8]_i_5_n_0 ));
  FDRE \Slve_CntValIn_Out_reg[0] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[0]_i_1_n_0 ),
        .Q(\Slve_CntValIn_Out_reg[0]_0 ),
        .R(SR));
  FDRE \Slve_CntValIn_Out_reg[1] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[1]_i_1_n_0 ),
        .Q(\Slve_CntValIn_Out_reg[1]_0 ),
        .R(SR));
  FDRE \Slve_CntValIn_Out_reg[2] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[2]_i_1_n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[3] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[4] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[4]_i_1_n_0 ),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[5] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[5]_i_1_n_0 ),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[6] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[6]_i_1_n_0 ),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[7] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[7]_i_1_n_0 ),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \Slve_CntValIn_Out_reg[8] 
       (.C(Rx_SysClk),
        .CE(\Slve_CntValIn_Out[8]_i_1_n_0 ),
        .D(\Slve_CntValIn_Out[8]_i_2_n_0 ),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \Slve_Load_dly_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(D),
        .Q(Slve_Load_dly[0]),
        .R(1'b0));
  FDRE \Slve_Load_dly_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Slve_Load_dly[0]),
        .Q(Slve_Load_dly[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0010000101110110)) 
    Slve_Load_i_2
       (.I0(\s_state_reg[4]_0 [2]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [4]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\s_state_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT3 #(
    .INIT(8'hD1)) 
    Slve_Load_i_3
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [2]),
        .O(\s_state_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFEFEBFFCFFFEFEFD)) 
    Slve_Load_i_4
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\s_state_reg[4]_1 ));
  FDSE Slve_Load_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Slve_Load_reg_0),
        .Q(D),
        .S(SR));
  FDRE WrapToZero_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(WrapToZero_reg_0),
        .Q(WrapToZero),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \act_count[0]_i_1 
       (.I0(act_count_reg[0]),
        .O(\act_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \act_count[1]_i_1 
       (.I0(act_count_reg[0]),
        .I1(\active_reg[1]_0 ),
        .I2(act_count_reg[1]),
        .O(\act_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT4 #(
    .INIT(16'hD2B4)) 
    \act_count[2]_i_1 
       (.I0(act_count_reg[0]),
        .I1(\active_reg[1]_0 ),
        .I2(act_count_reg[2]),
        .I3(act_count_reg[1]),
        .O(\act_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT5 #(
    .INIT(32'hAAA96AAA)) 
    \act_count[3]_i_1 
       (.I0(act_count_reg[3]),
        .I1(act_count_reg[2]),
        .I2(act_count_reg[1]),
        .I3(act_count_reg[0]),
        .I4(\active_reg[1]_0 ),
        .O(\act_count[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFF4000FFFD0002)) 
    \act_count[4]_i_1 
       (.I0(\active_reg[1]_0 ),
        .I1(act_count_reg[0]),
        .I2(act_count_reg[2]),
        .I3(act_count_reg[1]),
        .I4(act_count_reg[4]),
        .I5(act_count_reg[3]),
        .O(\act_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFCFFFFFFFFFFFFAF)) 
    \act_count[5]_i_1 
       (.I0(\act_count_reg[0]_0 ),
        .I1(\act_count[5]_i_4_n_0 ),
        .I2(\active_reg[1]_0 ),
        .I3(act_count_reg[4]),
        .I4(act_count_reg[3]),
        .I5(\act_count_reg[5]_0 ),
        .O(\act_count[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFCBF00400340)) 
    \act_count[5]_i_2 
       (.I0(\act_count[5]_i_4_n_0 ),
        .I1(act_count_reg[4]),
        .I2(act_count_reg[3]),
        .I3(\active_reg[1]_0 ),
        .I4(\act_count_reg[0]_0 ),
        .I5(\act_count_reg[5]_0 ),
        .O(\act_count[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \act_count[5]_i_3 
       (.I0(act_count_reg[0]),
        .I1(act_count_reg[2]),
        .I2(act_count_reg[1]),
        .O(\act_count_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \act_count[5]_i_4 
       (.I0(act_count_reg[2]),
        .I1(act_count_reg[1]),
        .I2(act_count_reg[0]),
        .O(\act_count[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    \act_count[5]_i_5 
       (.I0(\act_count[5]_i_6_n_0 ),
        .I1(\active_reg_n_0_[1] ),
        .I2(p_0_in0_in[1]),
        .I3(\active_reg_n_0_[0] ),
        .I4(p_0_in0_in[2]),
        .I5(\act_count[5]_i_7_n_0 ),
        .O(\active_reg[1]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \act_count[5]_i_6 
       (.I0(p_0_in0_in[0]),
        .I1(\active_reg_n_0_[3] ),
        .I2(\active_reg_n_0_[2] ),
        .I3(p_0_in0_in[3]),
        .O(\act_count[5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \act_count[5]_i_7 
       (.I0(\active_reg_n_0_[3] ),
        .I1(p_0_in0_in[0]),
        .I2(p_0_in0_in[2]),
        .I3(\active_reg_n_0_[0] ),
        .I4(\act_count[5]_i_8_n_0 ),
        .O(\act_count[5]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \act_count[5]_i_8 
       (.I0(\active_reg_n_0_[2] ),
        .I1(p_0_in0_in[3]),
        .I2(\active_reg_n_0_[1] ),
        .I3(p_0_in0_in[1]),
        .O(\act_count[5]_i_8_n_0 ));
  FDRE \act_count_reg[0] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[0]_i_1_n_0 ),
        .Q(act_count_reg[0]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[1] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[1]_i_1_n_0 ),
        .Q(act_count_reg[1]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[2] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[2]_i_1_n_0 ),
        .Q(act_count_reg[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[3] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[3]_i_1_n_0 ),
        .Q(act_count_reg[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[4] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[4]_i_1_n_0 ),
        .Q(act_count_reg[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \act_count_reg[5] 
       (.C(Rx_SysClk),
        .CE(\act_count[5]_i_1_n_0 ),
        .D(\act_count[5]_i_2_n_0 ),
        .Q(\act_count_reg[5]_0 ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[4]_i_1 
       (.I0(BaseX_Rx_Q_Out[4]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[0]),
        .O(p_2_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[5]_i_1 
       (.I0(BaseX_Rx_Q_Out[5]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[1]),
        .O(p_2_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[6]_i_1 
       (.I0(BaseX_Rx_Q_Out[6]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[2]),
        .O(p_2_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \active[7]_i_1 
       (.I0(BaseX_Rx_Q_Out[7]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[3]),
        .O(p_2_out[7]));
  FDRE \active_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[0]),
        .Q(\active_reg_n_0_[0] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[1]),
        .Q(\active_reg_n_0_[1] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[2]),
        .Q(\active_reg_n_0_[2] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_0_in0_in[3]),
        .Q(\active_reg_n_0_[3] ),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[4]),
        .Q(p_0_in0_in[0]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[5]),
        .Q(p_0_in0_in[1]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[6]),
        .Q(p_0_in0_in[2]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \active_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_2_out[7]),
        .Q(p_0_in0_in[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  LUT4 #(
    .INIT(16'h6660)) 
    decoded_rxchariscomma_i_1
       (.I0(al_rx_data_out[9]),
        .I1(al_rx_data_out[8]),
        .I2(decoded_rxchariscomma_i_2_n_0),
        .I3(decoded_rxchariscomma_i_3_n_0),
        .O(decoded_rxchariscomma0));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    decoded_rxchariscomma_i_2
       (.I0(\decode_8b10b/pdbr62__0 ),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[2]),
        .I3(decoded_rxchariscomma_i_5_n_0),
        .I4(al_rx_data_out[3]),
        .I5(al_rx_data_out[7]),
        .O(decoded_rxchariscomma_i_2_n_0));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    decoded_rxchariscomma_i_3
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[7]),
        .I4(decoded_rxchariscomma_i_6_n_0),
        .I5(decoded_rxchariscomma_i_7_n_0),
        .O(decoded_rxchariscomma_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT2 #(
    .INIT(4'hE)) 
    decoded_rxchariscomma_i_4
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .O(\decode_8b10b/pdbr62__0 ));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_5
       (.I0(al_rx_data_out[1]),
        .I1(al_rx_data_out[0]),
        .O(decoded_rxchariscomma_i_5_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_6
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .O(decoded_rxchariscomma_i_6_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    decoded_rxchariscomma_i_7
       (.I0(al_rx_data_out[2]),
        .I1(al_rx_data_out[3]),
        .O(decoded_rxchariscomma_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[0]_i_1 
       (.I0(data0[0]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[0]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[1]_i_1 
       (.I0(data0[1]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[1]));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[2]_i_1 
       (.I0(data0[2]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[2]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[3]_i_1 
       (.I0(data0[3]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[3]));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[4]_i_1 
       (.I0(data0[4]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[4]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[5]_i_1 
       (.I0(data0[5]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[5]));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \delay_change[6]_i_1 
       (.I0(data0[6]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [4]),
        .O(delay_change[6]));
  LUT6 #(
    .INIT(64'h0000000400080001)) 
    \delay_change[7]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [3]),
        .I4(\s_state_reg_n_0_[5] ),
        .I5(\s_state_reg[4]_0 [4]),
        .O(\delay_change[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \delay_change[7]_i_2 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [4]),
        .O(delay_change[7]));
  FDRE \delay_change_reg[0] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[0]),
        .Q(\delay_change_reg_n_0_[0] ),
        .R(SR));
  FDRE \delay_change_reg[1] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[1]),
        .Q(data0[0]),
        .R(SR));
  FDRE \delay_change_reg[2] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[2]),
        .Q(data0[1]),
        .R(SR));
  FDRE \delay_change_reg[3] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[3]),
        .Q(data0[2]),
        .R(SR));
  FDRE \delay_change_reg[4] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[4]),
        .Q(data0[3]),
        .R(SR));
  FDRE \delay_change_reg[5] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[5]),
        .Q(data0[4]),
        .R(SR));
  FDRE \delay_change_reg[6] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[6]),
        .Q(data0[5]),
        .R(SR));
  FDRE \delay_change_reg[7] 
       (.C(Rx_SysClk),
        .CE(\delay_change[7]_i_1_n_0 ),
        .D(delay_change[7]),
        .Q(data0[6]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT5 #(
    .INIT(32'h89BFFD91)) 
    \dout_i[5]_i_1 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[8]),
        .I2(\decode_8b10b/k28__1 ),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[6]),
        .O(b3[5]));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT5 #(
    .INIT(32'h98FBDF19)) 
    \dout_i[6]_i_1 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[8]),
        .I2(\decode_8b10b/k28__1 ),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[6]),
        .O(b3[6]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT5 #(
    .INIT(32'hF5E187AF)) 
    \dout_i[7]_i_1 
       (.I0(al_rx_data_out[8]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[7]),
        .I3(\decode_8b10b/k28__1 ),
        .I4(al_rx_data_out[9]),
        .O(b3[7]));
  LUT6 #(
    .INIT(64'h0000000000000006)) 
    \dout_i[7]_i_2 
       (.I0(al_rx_data_out[8]),
        .I1(al_rx_data_out[9]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[5]),
        .I5(al_rx_data_out[4]),
        .O(\decode_8b10b/k28__1 ));
  LUT6 #(
    .INIT(64'hEDA3C33DAAABADB7)) 
    g0_b0
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[0]));
  LUT6 #(
    .INIT(64'hEDA5A55BCCCDCDB7)) 
    g0_b1
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[1]));
  LUT6 #(
    .INIT(64'hFDB19967F0F1E5BF)) 
    g0_b2
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[2]));
  LUT6 #(
    .INIT(64'hFCA99697FF01FD3F)) 
    g0_b3
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[3]));
  LUT6 #(
    .INIT(64'hF8FF8117FEE9971F)) 
    g0_b4
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[3]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(out[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFEFE)) 
    \gcerr.CODE_ERR_i_1 
       (.I0(\gcerr.CODE_ERR_i_2_n_0 ),
        .I1(\gcerr.CODE_ERR_i_3_n_0 ),
        .I2(\gcerr.CODE_ERR_i_4_n_0 ),
        .I3(ndbr6),
        .I4(\gcerr.CODE_ERR_i_6_n_0 ),
        .I5(invby_e),
        .O(code_err_i));
  LUT6 #(
    .INIT(64'hFEE8E888E8808000)) 
    \gcerr.CODE_ERR_i_10 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[0]),
        .I4(al_rx_data_out[2]),
        .I5(al_rx_data_out[3]),
        .O(pdur6));
  LUT6 #(
    .INIT(64'h000101171117177F)) 
    \gcerr.CODE_ERR_i_11 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[0]),
        .I4(al_rx_data_out[2]),
        .I5(al_rx_data_out[3]),
        .O(ndur6));
  LUT6 #(
    .INIT(64'hFEE8E880E8808000)) 
    \gcerr.CODE_ERR_i_12 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[2]),
        .I2(al_rx_data_out[0]),
        .I3(al_rx_data_out[1]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(pdbr6));
  LUT6 #(
    .INIT(64'hFFFFFFFF10000004)) 
    \gcerr.CODE_ERR_i_2 
       (.I0(\decode_8b10b/sK28__2 ),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[7]),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[8]),
        .I5(invr6),
        .O(\gcerr.CODE_ERR_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF80000000000001F)) 
    \gcerr.CODE_ERR_i_3 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[7]),
        .I4(al_rx_data_out[6]),
        .I5(al_rx_data_out[8]),
        .O(\gcerr.CODE_ERR_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAB8383BFA8808080)) 
    \gcerr.CODE_ERR_i_4 
       (.I0(pdur6),
        .I1(al_rx_data_out[7]),
        .I2(al_rx_data_out[6]),
        .I3(al_rx_data_out[9]),
        .I4(al_rx_data_out[8]),
        .I5(ndur6),
        .O(\gcerr.CODE_ERR_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000101170117177F)) 
    \gcerr.CODE_ERR_i_5 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[2]),
        .I2(al_rx_data_out[0]),
        .I3(al_rx_data_out[1]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[5]),
        .O(ndbr6));
  LUT6 #(
    .INIT(64'h0000000000018000)) 
    \gcerr.CODE_ERR_i_6 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[7]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[8]),
        .I4(al_rx_data_out[4]),
        .I5(pdbr6),
        .O(\gcerr.CODE_ERR_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT4 #(
    .INIT(16'h8002)) 
    \gcerr.CODE_ERR_i_7 
       (.I0(\decode_8b10b/sK28__2 ),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[6]),
        .I3(al_rx_data_out[7]),
        .O(invby_e));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT4 #(
    .INIT(16'h8001)) 
    \gcerr.CODE_ERR_i_8 
       (.I0(al_rx_data_out[3]),
        .I1(al_rx_data_out[4]),
        .I2(al_rx_data_out[5]),
        .I3(al_rx_data_out[2]),
        .O(\decode_8b10b/sK28__2 ));
  LUT6 #(
    .INIT(64'hF88080018001011F)) 
    \gcerr.CODE_ERR_i_9 
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[3]),
        .I3(al_rx_data_out[2]),
        .I4(al_rx_data_out[0]),
        .I5(al_rx_data_out[1]),
        .O(invr6));
  LUT6 #(
    .INIT(64'h00EF0051004141EF)) 
    \gde.gdeni.DISP_ERR_i_2 
       (.I0(\decode_8b10b/b6_disp__34 [1]),
        .I1(\decode_8b10b/b6_disp__34 [2]),
        .I2(\decode_8b10b/b6_disp__34 [0]),
        .I3(\decode_8b10b/b4_disp__9 [1]),
        .I4(\decode_8b10b/b4_disp__9 [2]),
        .I5(\decode_8b10b/b4_disp__9 [0]),
        .O(\gde.gdeni.DISP_ERR_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h001400FE00FE1415)) 
    \gde.gdeni.DISP_ERR_i_3 
       (.I0(\decode_8b10b/b6_disp__34 [1]),
        .I1(\decode_8b10b/b6_disp__34 [2]),
        .I2(\decode_8b10b/b6_disp__34 [0]),
        .I3(\decode_8b10b/b4_disp__9 [1]),
        .I4(\decode_8b10b/b4_disp__9 [0]),
        .I5(\decode_8b10b/b4_disp__9 [2]),
        .O(\gde.gdeni.DISP_ERR_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0002000000004000)) 
    \gde.gdeni.DISP_ERR_i_4 
       (.I0(al_rx_data_out[5]),
        .I1(al_rx_data_out[0]),
        .I2(al_rx_data_out[1]),
        .I3(al_rx_data_out[2]),
        .I4(al_rx_data_out[3]),
        .I5(al_rx_data_out[4]),
        .O(\decode_8b10b/b6_disp__34 [2]));
  MUXF7 \gde.gdeni.DISP_ERR_reg_i_1 
       (.I0(\gde.gdeni.DISP_ERR_i_2_n_0 ),
        .I1(\gde.gdeni.DISP_ERR_i_3_n_0 ),
        .O(\grdni.run_disp_i_reg_0 ),
        .S(\grdni.run_disp_i_reg_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gen_io_logic_i_1
       (.I0(Slve_Load_dly[1]),
        .I1(Slve_Load_dly[0]),
        .O(BaseX_Idly_Load[1]));
  LUT2 #(
    .INIT(4'hE)) 
    gen_io_logic_i_2
       (.I0(Mstr_Load_dly[1]),
        .I1(Mstr_Load_dly[0]),
        .O(BaseX_Idly_Load[0]));
  LUT6 #(
    .INIT(64'h00000B08FFFF0000)) 
    \grdni.run_disp_i_i_1 
       (.I0(\grdni.run_disp_i_reg_1 ),
        .I1(\decode_8b10b/b6_disp__34 [1]),
        .I2(\decode_8b10b/b4_disp__9 [2]),
        .I3(\decode_8b10b/b6_disp__34 [0]),
        .I4(\decode_8b10b/b4_disp__9 [0]),
        .I5(\decode_8b10b/b4_disp__9 [1]),
        .O(\grdni.run_disp_i_reg ));
  LUT6 #(
    .INIT(64'h0016166816686800)) 
    \grdni.run_disp_i_i_2 
       (.I0(al_rx_data_out[0]),
        .I1(al_rx_data_out[1]),
        .I2(al_rx_data_out[2]),
        .I3(al_rx_data_out[5]),
        .I4(al_rx_data_out[4]),
        .I5(al_rx_data_out[3]),
        .O(\decode_8b10b/b6_disp__34 [1]));
  LUT4 #(
    .INIT(16'h1008)) 
    \grdni.run_disp_i_i_3 
       (.I0(al_rx_data_out[9]),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[7]),
        .I3(al_rx_data_out[6]),
        .O(\decode_8b10b/b4_disp__9 [2]));
  LUT6 #(
    .INIT(64'hFEE8E880E880C000)) 
    \grdni.run_disp_i_i_4 
       (.I0(al_rx_data_out[2]),
        .I1(al_rx_data_out[3]),
        .I2(al_rx_data_out[4]),
        .I3(al_rx_data_out[5]),
        .I4(al_rx_data_out[1]),
        .I5(al_rx_data_out[0]),
        .O(\decode_8b10b/b6_disp__34 [0]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT4 #(
    .INIT(16'hE8C0)) 
    \grdni.run_disp_i_i_5 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[9]),
        .I2(al_rx_data_out[8]),
        .I3(al_rx_data_out[6]),
        .O(\decode_8b10b/b4_disp__9 [0]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT4 #(
    .INIT(16'h0660)) 
    \grdni.run_disp_i_i_6 
       (.I0(al_rx_data_out[7]),
        .I1(al_rx_data_out[6]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[8]),
        .O(\decode_8b10b/b4_disp__9 [1]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[0]_i_1 
       (.I0(\hdataout[0]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[1] ),
        .O(\hdataout[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[0]_i_2 
       (.I0(\holdreg_reg_n_0_[5] ),
        .I1(\holdreg_reg_n_0_[4] ),
        .I2(\holdreg_reg_n_0_[3] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[2] ),
        .O(\hdataout[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[1]_i_1 
       (.I0(\hdataout[1]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[2] ),
        .O(\hdataout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[1]_i_2 
       (.I0(\holdreg_reg_n_0_[6] ),
        .I1(\holdreg_reg_n_0_[5] ),
        .I2(\holdreg_reg_n_0_[4] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[3] ),
        .O(\hdataout[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[2]_i_1 
       (.I0(\hdataout[2]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[3] ),
        .O(\hdataout[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[2]_i_2 
       (.I0(\holdreg_reg_n_0_[7] ),
        .I1(\holdreg_reg_n_0_[6] ),
        .I2(\holdreg_reg_n_0_[5] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[4] ),
        .O(\hdataout[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[3]_i_1 
       (.I0(\hdataout[3]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[4] ),
        .O(\hdataout[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[3]_i_2 
       (.I0(\holdreg_reg_n_0_[8] ),
        .I1(\holdreg_reg_n_0_[7] ),
        .I2(\holdreg_reg_n_0_[6] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[5] ),
        .O(\hdataout[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[4]_i_1 
       (.I0(\hdataout[4]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[5] ),
        .O(\hdataout[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[4]_i_2 
       (.I0(\holdreg_reg_n_0_[9] ),
        .I1(\holdreg_reg_n_0_[8] ),
        .I2(\holdreg_reg_n_0_[7] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[6] ),
        .O(\hdataout[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[5]_i_1 
       (.I0(\hdataout[5]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[6] ),
        .O(\hdataout[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[5]_i_2 
       (.I0(\holdreg_reg_n_0_[10] ),
        .I1(\holdreg_reg_n_0_[9] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[7] ),
        .O(\hdataout[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[6]_i_1 
       (.I0(\hdataout[6]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[7] ),
        .O(\hdataout[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \hdataout[6]_i_2 
       (.I0(\holdreg_reg_n_0_[11] ),
        .I1(\holdreg_reg_n_0_[10] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(\toggle_reg_n_0_[0] ),
        .I4(\toggle_reg_n_0_[1] ),
        .I5(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[7]_i_1 
       (.I0(\hdataout[7]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[8] ),
        .O(\hdataout[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[7]_i_2 
       (.I0(\holdreg_reg_n_0_[12] ),
        .I1(\holdreg_reg_n_0_[11] ),
        .I2(\holdreg_reg_n_0_[10] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[8]_i_1 
       (.I0(\hdataout[8]_i_2_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[9] ),
        .O(\hdataout[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[8]_i_2 
       (.I0(\holdreg_reg_n_0_[13] ),
        .I1(\holdreg_reg_n_0_[12] ),
        .I2(\holdreg_reg_n_0_[11] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[10] ),
        .O(\hdataout[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0155)) 
    \hdataout[9]_i_1 
       (.I0(p_0_in0),
        .I1(\toggle_reg_n_0_[1] ),
        .I2(\toggle_reg_n_0_[0] ),
        .I3(\toggle_reg_n_0_[2] ),
        .O(p_1_out));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \hdataout[9]_i_2 
       (.I0(\hdataout[9]_i_3_n_0 ),
        .I1(\toggle_reg_n_0_[2] ),
        .I2(\holdreg_reg_n_0_[10] ),
        .O(\hdataout[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \hdataout[9]_i_3 
       (.I0(\holdreg_reg_n_0_[14] ),
        .I1(\holdreg_reg_n_0_[13] ),
        .I2(\holdreg_reg_n_0_[12] ),
        .I3(\toggle_reg_n_0_[1] ),
        .I4(\toggle_reg_n_0_[0] ),
        .I5(\holdreg_reg_n_0_[11] ),
        .O(\hdataout[9]_i_3_n_0 ));
  FDRE \hdataout_reg[0] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[0]_i_1_n_0 ),
        .Q(hdataout[0]),
        .R(SR));
  FDRE \hdataout_reg[1] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[1]_i_1_n_0 ),
        .Q(hdataout[1]),
        .R(SR));
  FDRE \hdataout_reg[2] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[2]_i_1_n_0 ),
        .Q(hdataout[2]),
        .R(SR));
  FDRE \hdataout_reg[3] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[3]_i_1_n_0 ),
        .Q(hdataout[3]),
        .R(SR));
  FDRE \hdataout_reg[4] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[4]_i_1_n_0 ),
        .Q(hdataout[4]),
        .R(SR));
  FDRE \hdataout_reg[5] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[5]_i_1_n_0 ),
        .Q(hdataout[5]),
        .R(SR));
  FDRE \hdataout_reg[6] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[6]_i_1_n_0 ),
        .Q(hdataout[6]),
        .R(SR));
  FDRE \hdataout_reg[7] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[7]_i_1_n_0 ),
        .Q(hdataout[7]),
        .R(SR));
  FDRE \hdataout_reg[8] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[8]_i_1_n_0 ),
        .Q(hdataout[8]),
        .R(SR));
  FDRE \hdataout_reg[9] 
       (.C(Rx_SysClk),
        .CE(p_1_out),
        .D(\hdataout[9]_i_2_n_0 ),
        .Q(hdataout[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[10]_i_1 
       (.I0(\holdreg_reg_n_0_[13] ),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[0]),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[14] ),
        .O(\holdreg[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[11]_i_1 
       (.I0(\holdreg_reg_n_0_[14] ),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[1]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[0]),
        .O(\holdreg[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[12]_i_1 
       (.I0(p_0_in0_in[0]),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[2]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[1]),
        .O(\holdreg[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[13]_i_1 
       (.I0(p_0_in0_in[1]),
        .I1(insert3_reg_0),
        .I2(p_0_in0_in[3]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[2]),
        .O(\holdreg[13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[14]_i_1 
       (.I0(p_0_in0_in[2]),
        .I1(insert3_reg_0),
        .I2(p_3_out[4]),
        .I3(insert5_reg_0),
        .I4(p_0_in0_in[3]),
        .O(\holdreg[14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[1]_i_1 
       (.I0(\holdreg_reg_n_0_[4] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[6] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[5] ),
        .O(\holdreg[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[2]_i_1 
       (.I0(\holdreg_reg_n_0_[5] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[7] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[6] ),
        .O(\holdreg[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[3]_i_1 
       (.I0(\holdreg_reg_n_0_[6] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[8] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[7] ),
        .O(\holdreg[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[4]_i_1 
       (.I0(\holdreg_reg_n_0_[7] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[9] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[8] ),
        .O(\holdreg[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[5]_i_1 
       (.I0(\holdreg_reg_n_0_[8] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[10] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[9] ),
        .O(\holdreg[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[6]_i_1 
       (.I0(\holdreg_reg_n_0_[9] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[11] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[10] ),
        .O(\holdreg[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[7]_i_1 
       (.I0(\holdreg_reg_n_0_[10] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[12] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[11] ),
        .O(\holdreg[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[8]_i_1 
       (.I0(\holdreg_reg_n_0_[11] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[13] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[12] ),
        .O(\holdreg[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \holdreg[9]_i_1 
       (.I0(\holdreg_reg_n_0_[12] ),
        .I1(insert3_reg_0),
        .I2(\holdreg_reg_n_0_[14] ),
        .I3(insert5_reg_0),
        .I4(\holdreg_reg_n_0_[13] ),
        .O(\holdreg[9]_i_1_n_0 ));
  FDRE \holdreg_reg[10] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[10]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[10] ),
        .R(SR));
  FDRE \holdreg_reg[11] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[11]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[11] ),
        .R(SR));
  FDRE \holdreg_reg[12] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[12]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[12] ),
        .R(SR));
  FDRE \holdreg_reg[13] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[13]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[13] ),
        .R(SR));
  FDRE \holdreg_reg[14] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[14]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[14] ),
        .R(SR));
  FDRE \holdreg_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[1]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[1] ),
        .R(SR));
  FDRE \holdreg_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[2]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[2] ),
        .R(SR));
  FDRE \holdreg_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[3]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[3] ),
        .R(SR));
  FDRE \holdreg_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[4]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[4] ),
        .R(SR));
  FDRE \holdreg_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[5]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[5] ),
        .R(SR));
  FDRE \holdreg_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[6]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[6] ),
        .R(SR));
  FDRE \holdreg_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[7]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[7] ),
        .R(SR));
  FDRE \holdreg_reg[8] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[8]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[8] ),
        .R(SR));
  FDRE \holdreg_reg[9] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\holdreg[9]_i_1_n_0 ),
        .Q(\holdreg_reg_n_0_[9] ),
        .R(SR));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_1
       (.I0(ActCnt_GE_HalfBT_reg_0),
        .I1(Q[6]),
        .O(i___0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_1__0
       (.I0(ActCnt_GE_HalfBT_reg_0),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .O(i___0_carry_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_2
       (.I0(p_1_in[7]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[5]),
        .O(i___0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_2__0
       (.I0(p_1_in[7]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .O(i___0_carry_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_3
       (.I0(p_1_in[6]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[4]),
        .O(i___0_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_3__0
       (.I0(p_1_in[6]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .O(i___0_carry_i_3__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_4
       (.I0(p_1_in[5]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[3]),
        .O(i___0_carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_4__0
       (.I0(p_1_in[5]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .O(i___0_carry_i_4__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_5
       (.I0(p_1_in[4]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[2]),
        .O(i___0_carry_i_5_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_5__0
       (.I0(p_1_in[4]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .O(i___0_carry_i_5__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_6
       (.I0(p_1_in[3]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[1]),
        .O(i___0_carry_i_6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_6__0
       (.I0(p_1_in[3]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .O(i___0_carry_i_6__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_7
       (.I0(p_1_in[2]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(Q[0]),
        .O(i___0_carry_i_7_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    i___0_carry_i_7__0
       (.I0(p_1_in[2]),
        .I1(ActCnt_GE_HalfBT_reg_0),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .O(i___0_carry_i_7__0_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1
       (.I0(p_1_in[7]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(p_1_in[6]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2
       (.I0(p_1_in[5]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(p_1_in[4]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3
       (.I0(p_1_in[3]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(p_1_in[2]),
        .O(i__carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_4
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .O(i__carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_5
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [8]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [7]),
        .I1(p_1_in[7]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [6]),
        .I3(p_1_in[6]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [5]),
        .I1(p_1_in[5]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [4]),
        .I3(p_1_in[4]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [3]),
        .I1(p_1_in[3]),
        .I2(\Mstr_CntValIn_Out_reg[8]_0 [2]),
        .I3(p_1_in[2]),
        .O(i__carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_9
       (.I0(\Mstr_CntValIn_Out_reg[8]_0 [1]),
        .I1(\Mstr_CntValIn_Out_reg[8]_0 [0]),
        .O(i__carry_i_9_n_0));
  FDRE insert3_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(insert3_reg_1),
        .Q(insert3_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT5 #(
    .INIT(32'h30000001)) 
    insert5_i_2
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg_n_0_[5] ),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [0]),
        .O(\s_state_reg[3]_0 ));
  FDRE insert5_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(insert5_reg_1),
        .Q(insert5_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT5 #(
    .INIT(32'hFFFF8001)) 
    kout_i_i_1
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[5]),
        .I2(al_rx_data_out[3]),
        .I3(al_rx_data_out[2]),
        .I4(k1),
        .O(k));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT5 #(
    .INIT(32'h40000002)) 
    kout_i_i_2
       (.I0(al_rx_data_out[4]),
        .I1(al_rx_data_out[8]),
        .I2(al_rx_data_out[9]),
        .I3(al_rx_data_out[7]),
        .I4(al_rx_data_out[5]),
        .O(k1));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[4]_i_1 
       (.I0(BaseX_Rx_Q_Out[0]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[4]),
        .O(p_3_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[5]_i_1 
       (.I0(BaseX_Rx_Q_Out[1]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[5]),
        .O(p_3_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[6]_i_1 
       (.I0(BaseX_Rx_Q_Out[2]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[6]),
        .O(p_3_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \monitor[7]_i_1 
       (.I0(BaseX_Rx_Q_Out[3]),
        .I1(ActiveIsSlve_reg_0),
        .I2(BaseX_Rx_Q_Out[7]),
        .O(p_3_out[7]));
  LUT6 #(
    .INIT(64'h0000000000002008)) 
    monitor_late_i_2
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [2]),
        .I5(\s_state_reg[4]_0 [3]),
        .O(\s_state_reg[0]_0 ));
  FDRE monitor_late_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(monitor_late_reg_1),
        .Q(monitor_late_reg_0),
        .R(SR));
  FDRE \monitor_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(monitor[7]),
        .Q(monitor[3]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[4]),
        .Q(monitor[4]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[5] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[5]),
        .Q(monitor[5]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[6] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[6]),
        .Q(monitor[6]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  FDRE \monitor_reg[7] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(p_3_out[7]),
        .Q(monitor[7]),
        .R(\IntReset_dly_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'h5555555500000004)) 
    \mpx[0]_i_1 
       (.I0(\mpx[0]_i_2_n_0 ),
        .I1(\mpx[0]_i_3_n_0 ),
        .I2(\mpx[0]_i_4_n_0 ),
        .I3(\mpx[0]_i_5_n_0 ),
        .I4(\mpx[0]_i_6_n_0 ),
        .I5(\mpx[3]_i_3_n_0 ),
        .O(\mpx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \mpx[0]_i_10 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \mpx[0]_i_11 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[2] ),
        .O(\mpx[0]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \mpx[0]_i_12 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(data9[4]),
        .I2(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \mpx[0]_i_13 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mpx[0]_i_14 
       (.I0(data9[6]),
        .I1(data9[5]),
        .O(\mpx[0]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \mpx[0]_i_15 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[4]),
        .O(\mpx[0]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \mpx[0]_i_16 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \mpx[0]_i_17 
       (.I0(data9[5]),
        .I1(data9[2]),
        .I2(data9[3]),
        .O(\mpx[0]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    \mpx[0]_i_18 
       (.I0(data9[4]),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(data9[6]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(\mpx[0]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \mpx[0]_i_19 
       (.I0(data9[2]),
        .I1(data9[3]),
        .I2(\rxdh_reg_n_0_[8] ),
        .I3(data9[4]),
        .O(\mpx[0]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \mpx[0]_i_2 
       (.I0(SR),
        .I1(\mpx[3]_i_4_n_0 ),
        .O(\mpx[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \mpx[0]_i_20 
       (.I0(data9[5]),
        .I1(data9[1]),
        .I2(data9[6]),
        .I3(data9[0]),
        .O(\mpx[0]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \mpx[0]_i_21 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[6] ),
        .I3(data9[2]),
        .O(\mpx[0]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \mpx[0]_i_22 
       (.I0(data9[0]),
        .I1(data9[1]),
        .I2(\rxdh_reg_n_0_[4] ),
        .I3(\rxdh_reg_n_0_[5] ),
        .O(\mpx[0]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \mpx[0]_i_23 
       (.I0(\rxdh_reg_n_0_[6] ),
        .I1(data9[2]),
        .I2(data9[0]),
        .I3(data9[1]),
        .O(\mpx[0]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \mpx[0]_i_24 
       (.I0(\rxdh_reg_n_0_[7] ),
        .I1(\rxdh_reg_n_0_[8] ),
        .I2(\rxdh_reg_n_0_[4] ),
        .I3(\rxdh_reg_n_0_[5] ),
        .O(\mpx[0]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFEFAAEFFFEF)) 
    \mpx[0]_i_3 
       (.I0(\mpx[0]_i_7_n_0 ),
        .I1(\mpx[0]_i_8_n_0 ),
        .I2(\mpx[0]_i_9_n_0 ),
        .I3(data9[0]),
        .I4(\mpx[0]_i_10_n_0 ),
        .I5(\mpx[0]_i_11_n_0 ),
        .O(\mpx[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0200020F02000200)) 
    \mpx[0]_i_4 
       (.I0(\mpx[0]_i_12_n_0 ),
        .I1(\mpx[0]_i_13_n_0 ),
        .I2(\mpx[0]_i_14_n_0 ),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(\mpx[0]_i_15_n_0 ),
        .I5(\mpx[0]_i_16_n_0 ),
        .O(\mpx[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[0]_i_5 
       (.I0(\mpx[0]_i_17_n_0 ),
        .I1(\mpx[0]_i_18_n_0 ),
        .I2(\mpx[0]_i_19_n_0 ),
        .I3(\mpx[0]_i_20_n_0 ),
        .I4(data9[7]),
        .I5(data9[8]),
        .O(\mpx[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[0]_i_6 
       (.I0(\mpx[0]_i_21_n_0 ),
        .I1(\mpx[0]_i_22_n_0 ),
        .I2(\mpx[0]_i_23_n_0 ),
        .I3(\mpx[0]_i_24_n_0 ),
        .I4(data9[4]),
        .I5(data9[3]),
        .O(\mpx[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mpx[0]_i_7 
       (.I0(data9[2]),
        .I1(data9[1]),
        .O(\mpx[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \mpx[0]_i_8 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[7] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \mpx[0]_i_9 
       (.I0(\rxdh_reg_n_0_[2] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .O(\mpx[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'h01000101)) 
    \mpx[1]_i_1 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_4_n_0),
        .I4(\mpx[1]_i_2_n_0 ),
        .O(\mpx[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000099F9)) 
    \mpx[1]_i_2 
       (.I0(data9[6]),
        .I1(data9[7]),
        .I2(\mpx[1]_i_3_n_0 ),
        .I3(\mpx[1]_i_4_n_0 ),
        .I4(\mpx[0]_i_4_n_0 ),
        .O(\mpx[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    \mpx[1]_i_3 
       (.I0(data9[5]),
        .I1(data9[4]),
        .I2(\rxdh_reg_n_0_[8] ),
        .I3(\rxdh_reg_n_0_[7] ),
        .I4(\mpx[0]_i_16_n_0 ),
        .O(\mpx[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'h00000080)) 
    \mpx[1]_i_4 
       (.I0(\mpx[0]_i_19_n_0 ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(data9[5]),
        .I3(data9[1]),
        .I4(data9[0]),
        .O(\mpx[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \mpx[2]_i_1 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_2_n_0),
        .I4(Aligned_i_4_n_0),
        .O(\mpx[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFBAAAA)) 
    \mpx[3]_i_1 
       (.I0(SR),
        .I1(Aligned_i_2_n_0),
        .I2(Aligned_i_3_n_0),
        .I3(Aligned_i_4_n_0),
        .I4(Rx_Valid_Int_reg_n_0),
        .O(\mpx[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_10 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[2] ),
        .I3(\rxdh_reg_n_0_[7] ),
        .O(\mpx[3]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_11 
       (.I0(\rxdh_reg_n_0_[0] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[2] ),
        .O(\mpx[3]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \mpx[3]_i_2 
       (.I0(\mpx[3]_i_3_n_0 ),
        .I1(\mpx[3]_i_4_n_0 ),
        .I2(SR),
        .I3(Aligned_i_2_n_0),
        .I4(Aligned_i_4_n_0),
        .O(\mpx[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[3]_i_3 
       (.I0(\mpx[3]_i_5_n_0 ),
        .I1(\mpx[3]_i_6_n_0 ),
        .I2(\mpx[3]_i_7_n_0 ),
        .I3(\mpx[3]_i_8_n_0 ),
        .I4(data9[0]),
        .I5(data9[1]),
        .O(\mpx[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000022F222F20000)) 
    \mpx[3]_i_4 
       (.I0(\mpx[3]_i_9_n_0 ),
        .I1(\mpx[3]_i_10_n_0 ),
        .I2(\mpx[3]_i_5_n_0 ),
        .I3(\mpx[3]_i_11_n_0 ),
        .I4(\rxdh_reg_n_0_[8] ),
        .I5(data9[0]),
        .O(\mpx[3]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \mpx[3]_i_5 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[5] ),
        .I2(\rxdh_reg_n_0_[3] ),
        .I3(\rxdh_reg_n_0_[6] ),
        .O(\mpx[3]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \mpx[3]_i_6 
       (.I0(\rxdh_reg_n_0_[2] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \mpx[3]_i_7 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[1] ),
        .I3(\rxdh_reg_n_0_[2] ),
        .O(\mpx[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \mpx[3]_i_8 
       (.I0(\rxdh_reg_n_0_[4] ),
        .I1(\rxdh_reg_n_0_[7] ),
        .I2(\rxdh_reg_n_0_[5] ),
        .I3(\rxdh_reg_n_0_[8] ),
        .O(\mpx[3]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \mpx[3]_i_9 
       (.I0(\rxdh_reg_n_0_[3] ),
        .I1(\rxdh_reg_n_0_[6] ),
        .I2(\rxdh_reg_n_0_[0] ),
        .I3(\rxdh_reg_n_0_[1] ),
        .O(\mpx[3]_i_9_n_0 ));
  FDRE \mpx_reg[0] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[0]_i_1_n_0 ),
        .Q(mpx__0[0]),
        .R(1'b0));
  FDRE \mpx_reg[1] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[1]_i_1_n_0 ),
        .Q(mpx__0[1]),
        .R(1'b0));
  FDRE \mpx_reg[2] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[2]_i_1_n_0 ),
        .Q(mpx__0[2]),
        .R(1'b0));
  FDRE \mpx_reg[3] 
       (.C(Rx_SysClk),
        .CE(\mpx[3]_i_1_n_0 ),
        .D(\mpx[3]_i_2_n_0 ),
        .Q(mpx__0[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h96)) 
    \pd_count[0]_i_1 
       (.I0(pd_count[0]),
        .I1(PhaseDet_CntInc[0]),
        .I2(PhaseDet_CntDec[0]),
        .O(\pd_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9669C33CC33C6996)) 
    \pd_count[1]_i_1 
       (.I0(PhaseDet_CntDec[0]),
        .I1(PhaseDet_CntDec[1]),
        .I2(pd_count[1]),
        .I3(PhaseDet_CntInc[1]),
        .I4(PhaseDet_CntInc[0]),
        .I5(pd_count[0]),
        .O(\pd_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT5 #(
    .INIT(32'h96669996)) 
    \pd_count[2]_i_1 
       (.I0(\pd_count[2]_i_2_n_0 ),
        .I1(\pd_count[2]_i_3_n_0 ),
        .I2(pd_count[1]),
        .I3(PhaseDet_CntInc[1]),
        .I4(PhaseDet_CntDec[1]),
        .O(\pd_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h69696900FF696969)) 
    \pd_count[2]_i_2 
       (.I0(PhaseDet_CntInc[1]),
        .I1(pd_count[1]),
        .I2(PhaseDet_CntDec[1]),
        .I3(pd_count[0]),
        .I4(PhaseDet_CntInc[0]),
        .I5(PhaseDet_CntDec[0]),
        .O(\pd_count[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \pd_count[2]_i_3 
       (.I0(PhaseDet_CntDec[2]),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntInc[2]),
        .O(\pd_count[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h69996669)) 
    \pd_count[3]_i_1 
       (.I0(\pd_count[4]_i_2_n_0 ),
        .I1(pd_count[3]),
        .I2(pd_count[2]),
        .I3(PhaseDet_CntInc[2]),
        .I4(PhaseDet_CntDec[2]),
        .O(\pd_count[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F5780A8EAFE1501)) 
    \pd_count[4]_i_1 
       (.I0(\pd_count[4]_i_2_n_0 ),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntInc[2]),
        .I3(PhaseDet_CntDec[2]),
        .I4(pd_count[4]),
        .I5(pd_count[3]),
        .O(\pd_count[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT5 #(
    .INIT(32'hFF696900)) 
    \pd_count[4]_i_2 
       (.I0(PhaseDet_CntInc[2]),
        .I1(pd_count[2]),
        .I2(PhaseDet_CntDec[2]),
        .I3(\pd_count[4]_i_3_n_0 ),
        .I4(\pd_count[2]_i_2_n_0 ),
        .O(\pd_count[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'h8E)) 
    \pd_count[4]_i_3 
       (.I0(pd_count[1]),
        .I1(PhaseDet_CntInc[1]),
        .I2(PhaseDet_CntDec[1]),
        .O(\pd_count[4]_i_3_n_0 ));
  FDRE \pd_count_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[0]_i_1_n_0 ),
        .Q(pd_count[0]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[1]_i_1_n_0 ),
        .Q(pd_count[1]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[2]_i_1_n_0 ),
        .Q(pd_count[2]),
        .R(pd_ovflw_up));
  FDRE \pd_count_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[3]_i_1_n_0 ),
        .Q(pd_count[3]),
        .R(pd_ovflw_up));
  FDSE \pd_count_reg[4] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\pd_count[4]_i_1_n_0 ),
        .Q(pd_count[4]),
        .S(pd_ovflw_up));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    pd_ovflw_down_i_1
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state[5]_i_3_n_0 ),
        .I3(\s_state_reg[4]_2 ),
        .I4(\delay_change_reg_n_0_[0] ),
        .I5(SR),
        .O(pd_ovflw_up));
  LUT6 #(
    .INIT(64'hFFFFFFFF11111113)) 
    pd_ovflw_down_i_2
       (.I0(pd_count[3]),
        .I1(pd_count[4]),
        .I2(pd_count[2]),
        .I3(pd_count[1]),
        .I4(pd_count[0]),
        .I5(pd_ovflw_down_reg_n_0),
        .O(pd_ovflw_down_i_2_n_0));
  FDRE pd_ovflw_down_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(pd_ovflw_down_i_2_n_0),
        .Q(pd_ovflw_down_reg_n_0),
        .R(pd_ovflw_up));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    pd_ovflw_up_i_1
       (.I0(pd_count[4]),
        .I1(pd_count[3]),
        .I2(pd_ovflw_up_reg_n_0),
        .O(pd_ovflw_up_i_1_n_0));
  FDRE pd_ovflw_up_reg
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(pd_ovflw_up_i_1_n_0),
        .Q(pd_ovflw_up_reg_n_0),
        .R(pd_ovflw_up));
  FDRE \rxdh_reg[0] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[1]),
        .Q(\rxdh_reg_n_0_[0] ),
        .R(SR));
  FDRE \rxdh_reg[10] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[0]),
        .Q(data9[1]),
        .R(SR));
  FDRE \rxdh_reg[11] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[1]),
        .Q(data9[2]),
        .R(SR));
  FDRE \rxdh_reg[12] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[2]),
        .Q(data9[3]),
        .R(SR));
  FDRE \rxdh_reg[13] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[3]),
        .Q(data9[4]),
        .R(SR));
  FDRE \rxdh_reg[14] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[4]),
        .Q(data9[5]),
        .R(SR));
  FDRE \rxdh_reg[15] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[5]),
        .Q(data9[6]),
        .R(SR));
  FDRE \rxdh_reg[16] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[6]),
        .Q(data9[7]),
        .R(SR));
  FDRE \rxdh_reg[17] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[7]),
        .Q(data9[8]),
        .R(SR));
  FDRE \rxdh_reg[18] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[8]),
        .Q(data9[9]),
        .R(SR));
  FDRE \rxdh_reg[19] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(hdataout[9]),
        .Q(\rxdh_reg_n_0_[19] ),
        .R(SR));
  FDRE \rxdh_reg[1] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[2]),
        .Q(\rxdh_reg_n_0_[1] ),
        .R(SR));
  FDRE \rxdh_reg[2] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[3]),
        .Q(\rxdh_reg_n_0_[2] ),
        .R(SR));
  FDRE \rxdh_reg[3] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[4]),
        .Q(\rxdh_reg_n_0_[3] ),
        .R(SR));
  FDRE \rxdh_reg[4] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[5]),
        .Q(\rxdh_reg_n_0_[4] ),
        .R(SR));
  FDRE \rxdh_reg[5] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[6]),
        .Q(\rxdh_reg_n_0_[5] ),
        .R(SR));
  FDRE \rxdh_reg[6] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[7]),
        .Q(\rxdh_reg_n_0_[6] ),
        .R(SR));
  FDRE \rxdh_reg[7] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[8]),
        .Q(\rxdh_reg_n_0_[7] ),
        .R(SR));
  FDRE \rxdh_reg[8] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(data9[9]),
        .Q(\rxdh_reg_n_0_[8] ),
        .R(SR));
  FDRE \rxdh_reg[9] 
       (.C(Rx_SysClk),
        .CE(Rx_Valid_Int_reg_n_0),
        .D(\rxdh_reg_n_0_[19] ),
        .Q(data9[0]),
        .R(SR));
  LUT6 #(
    .INIT(64'h3C3C3C3CC4C4C7C4)) 
    \s_state[0]_i_1 
       (.I0(\s_state_reg[4]_0 [0]),
        .I1(\s_state[5]_i_6_n_0 ),
        .I2(\s_state[5]_i_9_n_0 ),
        .I3(\s_state[0]_i_2_n_0 ),
        .I4(\s_state[1]_i_2_n_0 ),
        .I5(\s_state[5]_i_8_n_0 ),
        .O(\s_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \s_state[0]_i_2 
       (.I0(ActCnt_EQ_BTval),
        .I1(pd_ovflw_up_reg_n_0),
        .O(\s_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFC02FC32FC32FC02)) 
    \s_state[1]_i_1 
       (.I0(\s_state[1]_i_2_n_0 ),
        .I1(\s_state[5]_i_8_n_0 ),
        .I2(\s_state[5]_i_6_n_0 ),
        .I3(\s_state[5]_i_9_n_0 ),
        .I4(\s_state_reg[4]_0 [1]),
        .I5(\s_state_reg[4]_0 [0]),
        .O(\s_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \s_state[1]_i_2 
       (.I0(pd_ovflw_up_reg_n_0),
        .I1(ActCnt_EQ_Zero),
        .I2(pd_ovflw_down_reg_n_0),
        .O(\s_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00FFFFFFEF000000)) 
    \s_state[2]_i_1 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg_n_0_[5] ),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [2]),
        .O(\s_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \s_state[3]_i_1 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [1]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [0]),
        .O(\s_state[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAA6AA8AAAA)) 
    \s_state[4]_i_1 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [1]),
        .I3(\s_state_reg[4]_0 [2]),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg_n_0_[5] ),
        .O(\s_state[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    \s_state[5]_i_1 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state[5]_i_3_n_0 ),
        .I3(\s_state_reg[4]_2 ),
        .I4(pd_ovflw_up_reg_n_0),
        .I5(pd_ovflw_down_reg_n_0),
        .O(s_state));
  LUT5 #(
    .INIT(32'hFF0000B8)) 
    \s_state[5]_i_2 
       (.I0(\s_state[5]_i_5_n_0 ),
        .I1(\s_state[5]_i_6_n_0 ),
        .I2(\s_state[5]_i_7_n_0 ),
        .I3(\s_state[5]_i_8_n_0 ),
        .I4(\s_state[5]_i_9_n_0 ),
        .O(\s_state[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \s_state[5]_i_3 
       (.I0(\s_state_reg[4]_0 [3]),
        .I1(\s_state_reg[4]_0 [2]),
        .O(\s_state[5]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \s_state[5]_i_4 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg_n_0_[5] ),
        .O(\s_state_reg[4]_2 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \s_state[5]_i_5 
       (.I0(\s_state_reg_n_0_[5] ),
        .I1(\s_state_reg[4]_0 [0]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [1]),
        .I4(\s_state_reg[4]_0 [3]),
        .I5(\s_state_reg[4]_0 [4]),
        .O(\s_state[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFDDBE)) 
    \s_state[5]_i_6 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [4]),
        .I2(\s_state_reg[4]_0 [0]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [2]),
        .I5(\s_state_reg[4]_0 [3]),
        .O(\s_state[5]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT4 #(
    .INIT(16'h550C)) 
    \s_state[5]_i_7 
       (.I0(ActCnt_EQ_BTval),
        .I1(pd_ovflw_down_reg_n_0),
        .I2(ActCnt_EQ_Zero),
        .I3(pd_ovflw_up_reg_n_0),
        .O(\s_state[5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT5 #(
    .INIT(32'h00030100)) 
    \s_state[5]_i_8 
       (.I0(\s_state_reg[4]_0 [1]),
        .I1(\s_state_reg[4]_0 [3]),
        .I2(\s_state_reg[4]_0 [2]),
        .I3(\s_state_reg[4]_0 [4]),
        .I4(\s_state_reg_n_0_[5] ),
        .O(\s_state[5]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0040010101010100)) 
    \s_state[5]_i_9 
       (.I0(\s_state_reg[4]_0 [4]),
        .I1(\s_state_reg[4]_0 [2]),
        .I2(\s_state_reg[4]_0 [3]),
        .I3(\s_state_reg_n_0_[5] ),
        .I4(\s_state_reg[4]_0 [0]),
        .I5(\s_state_reg[4]_0 [1]),
        .O(\s_state[5]_i_9_n_0 ));
  FDRE \s_state_reg[0] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[0]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [0]),
        .R(SR));
  FDRE \s_state_reg[1] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[1]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [1]),
        .R(SR));
  FDRE \s_state_reg[2] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[2]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [2]),
        .R(SR));
  FDRE \s_state_reg[3] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[3]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [3]),
        .R(SR));
  FDRE \s_state_reg[4] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[4]_i_1_n_0 ),
        .Q(\s_state_reg[4]_0 [4]),
        .R(SR));
  FDRE \s_state_reg[5] 
       (.C(Rx_SysClk),
        .CE(s_state),
        .D(\s_state[5]_i_2_n_0 ),
        .Q(\s_state_reg_n_0_[5] ),
        .R(SR));
  LUT3 #(
    .INIT(8'h56)) 
    \toggle[0]_i_1 
       (.I0(\toggle_reg_n_0_[0] ),
        .I1(insert3_reg_0),
        .I2(insert5_reg_0),
        .O(\toggle[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9D409D9D62229D62)) 
    \toggle[1]_i_1 
       (.I0(insert3_reg_0),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(insert5_reg_0),
        .I3(p_0_in0),
        .I4(\toggle_reg_n_0_[2] ),
        .I5(\toggle_reg_n_0_[1] ),
        .O(\toggle[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0080DD3D3300CCDD)) 
    \toggle[2]_i_1 
       (.I0(p_0_in0),
        .I1(\toggle_reg_n_0_[1] ),
        .I2(insert5_reg_0),
        .I3(insert3_reg_0),
        .I4(\toggle_reg_n_0_[2] ),
        .I5(\toggle_reg_n_0_[0] ),
        .O(\toggle[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h522852281042500A)) 
    \toggle[3]_i_1 
       (.I0(\toggle_reg_n_0_[2] ),
        .I1(\toggle_reg_n_0_[0] ),
        .I2(\toggle_reg_n_0_[1] ),
        .I3(p_0_in0),
        .I4(insert5_reg_0),
        .I5(insert3_reg_0),
        .O(\toggle[3]_i_1_n_0 ));
  FDRE \toggle_reg[0] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[0]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[0] ),
        .R(SR));
  FDRE \toggle_reg[1] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[1]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[1] ),
        .R(SR));
  FDRE \toggle_reg[2] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[2]_i_1_n_0 ),
        .Q(\toggle_reg_n_0_[2] ),
        .R(SR));
  FDRE \toggle_reg[3] 
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(\toggle[3]_i_1_n_0 ),
        .Q(p_0_in0),
        .R(SR));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sgmii_adapt
   (sgmii_clk_r_0,
    sgmii_clk_en,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_txd_out,
    gmii_tx_en_out,
    gmii_tx_er_out,
    sgmii_clk_f_0,
    Tx_WrClk,
    mgt_tx_reset,
    speed_is_10_100_0,
    speed_is_100_0,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er_in,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0);
  output sgmii_clk_r_0;
  output sgmii_clk_en;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output [7:0]gmii_txd_out;
  output gmii_tx_en_out;
  output gmii_tx_er_out;
  output sgmii_clk_f_0;
  input Tx_WrClk;
  input mgt_tx_reset;
  input speed_is_10_100_0;
  input speed_is_100_0;
  input [7:0]gmii_rxd;
  input gmii_rx_dv;
  input gmii_rx_er_in;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;

  wire Tx_WrClk;
  wire gmii_rx_dv;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire gmii_rx_er_in;
  wire [7:0]gmii_rxd;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_en_out;
  wire gmii_tx_er_0;
  wire gmii_tx_er_out;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_out;
  wire mgt_tx_reset;
  wire sgmii_clk_en;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire speed_is_100_0;
  wire speed_is_100_resync;
  wire speed_is_10_100_0;
  wire speed_is_10_100_resync;
  wire sync_reset;

  gig_eth_pcs_pma_gmii_to_sgmii_bridge_clk_gen clock_generation
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_100_resync),
        .reset_out(sync_reset),
        .sgmii_clk_en_reg_0(sgmii_clk_en),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .speed_is_10_100_fall_reg_0(speed_is_10_100_resync));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync_0 gen_sync_reset
       (.Tx_WrClk(Tx_WrClk),
        .mgt_tx_reset(mgt_tx_reset),
        .reset_out(sync_reset));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_rx_rate_adapt receiver
       (.Tx_WrClk(Tx_WrClk),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rx_er_in(gmii_rx_er_in),
        .gmii_rx_er_out_reg_0(sgmii_clk_en),
        .gmii_rxd(gmii_rxd),
        .gmii_rxd_0(gmii_rxd_0),
        .reset_out(sync_reset));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block resync_speed_100
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_100_resync),
        .speed_is_100_0(speed_is_100_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_1 resync_speed_10_100
       (.Tx_WrClk(Tx_WrClk),
        .data_out(speed_is_10_100_resync),
        .speed_is_10_100_0(speed_is_10_100_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_rate_adapt transmitter
       (.E(sgmii_clk_en),
        .Tx_WrClk(Tx_WrClk),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_en_out(gmii_tx_en_out),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_tx_er_out(gmii_tx_er_out),
        .gmii_txd_0(gmii_txd_0),
        .gmii_txd_out(gmii_txd_out),
        .reset_out(sync_reset));
endmodule

(* DowngradeIPIdentifiedWarnings = "yes" *) (* EXAMPLE_SIMULATION = "0" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_support
   (txp_0,
    txn_0,
    rxp_0,
    rxn_0,
    signal_detect_0,
    gmii_txd_0,
    gmii_tx_en_0,
    gmii_tx_er_0,
    gmii_rxd_0,
    gmii_rx_dv_0,
    gmii_rx_er_0,
    gmii_isolate_0,
    sgmii_clk_r_0,
    sgmii_clk_f_0,
    sgmii_clk_en_0,
    speed_is_10_100_0,
    speed_is_100_0,
    an_interrupt_0,
    an_adv_config_vector_0,
    an_restart_config_0,
    status_vector_0,
    configuration_vector_0,
    refclk625_p,
    refclk625_n,
    clk125_out,
    clk312_out,
    rst_125_out,
    tx_logic_reset,
    rx_logic_reset,
    rx_locked,
    tx_locked,
    tx_bsc_rst_out,
    rx_bsc_rst_out,
    tx_bs_rst_out,
    rx_bs_rst_out,
    tx_rst_dly_out,
    rx_rst_dly_out,
    tx_bsc_en_vtc_out,
    rx_bsc_en_vtc_out,
    tx_bs_en_vtc_out,
    rx_bs_en_vtc_out,
    riu_clk_out,
    riu_addr_out,
    riu_wr_data_out,
    riu_wr_en_out,
    riu_nibble_sel_out,
    tx_pll_clk_out,
    rx_pll_clk_out,
    tx_rdclk_out,
    riu_rddata_3,
    riu_valid_3,
    riu_prsnt_3,
    riu_rddata_2,
    riu_valid_2,
    riu_prsnt_2,
    riu_rddata_1,
    riu_valid_1,
    riu_prsnt_1,
    rx_btval_3,
    rx_btval_2,
    rx_btval_1,
    tx_dly_rdy_1,
    rx_dly_rdy_1,
    rx_vtc_rdy_1,
    tx_vtc_rdy_1,
    tx_dly_rdy_2,
    rx_dly_rdy_2,
    rx_vtc_rdy_2,
    tx_vtc_rdy_2,
    tx_dly_rdy_3,
    rx_dly_rdy_3,
    rx_vtc_rdy_3,
    tx_vtc_rdy_3,
    reset);
  output txp_0;
  output txn_0;
  input rxp_0;
  input rxn_0;
  input signal_detect_0;
  input [7:0]gmii_txd_0;
  input gmii_tx_en_0;
  input gmii_tx_er_0;
  output [7:0]gmii_rxd_0;
  output gmii_rx_dv_0;
  output gmii_rx_er_0;
  output gmii_isolate_0;
  output sgmii_clk_r_0;
  output sgmii_clk_f_0;
  output sgmii_clk_en_0;
  input speed_is_10_100_0;
  input speed_is_100_0;
  output an_interrupt_0;
  input [15:0]an_adv_config_vector_0;
  input an_restart_config_0;
  output [15:0]status_vector_0;
  input [4:0]configuration_vector_0;
  input refclk625_p;
  input refclk625_n;
  output clk125_out;
  output clk312_out;
  output rst_125_out;
  output tx_logic_reset;
  output rx_logic_reset;
  output rx_locked;
  output tx_locked;
  output tx_bsc_rst_out;
  output rx_bsc_rst_out;
  output tx_bs_rst_out;
  output rx_bs_rst_out;
  output tx_rst_dly_out;
  output rx_rst_dly_out;
  output tx_bsc_en_vtc_out;
  output rx_bsc_en_vtc_out;
  output tx_bs_en_vtc_out;
  output rx_bs_en_vtc_out;
  output riu_clk_out;
  output [5:0]riu_addr_out;
  output [15:0]riu_wr_data_out;
  output riu_wr_en_out;
  output [1:0]riu_nibble_sel_out;
  output tx_pll_clk_out;
  output rx_pll_clk_out;
  output tx_rdclk_out;
  input [15:0]riu_rddata_3;
  input riu_valid_3;
  input riu_prsnt_3;
  input [15:0]riu_rddata_2;
  input riu_valid_2;
  input riu_prsnt_2;
  input [15:0]riu_rddata_1;
  input riu_valid_1;
  input riu_prsnt_1;
  output [8:0]rx_btval_3;
  output [8:0]rx_btval_2;
  output [8:0]rx_btval_1;
  input tx_dly_rdy_1;
  input rx_dly_rdy_1;
  input rx_vtc_rdy_1;
  input tx_vtc_rdy_1;
  input tx_dly_rdy_2;
  input rx_dly_rdy_2;
  input rx_vtc_rdy_2;
  input tx_vtc_rdy_2;
  input tx_dly_rdy_3;
  input rx_dly_rdy_3;
  input rx_vtc_rdy_3;
  input tx_vtc_rdy_3;
  input reset;

  wire \<const0> ;
  wire [15:0]an_adv_config_vector_0;
  wire an_interrupt_0;
  wire an_restart_config_0;
  wire clk125_out;
  wire clk312_out;
  wire [4:0]configuration_vector_0;
  wire gmii_isolate_0;
  wire gmii_rx_dv_0;
  wire gmii_rx_er_0;
  wire [7:0]gmii_rxd_0;
  wire gmii_tx_en_0;
  wire gmii_tx_er_0;
  wire [7:0]gmii_txd_0;
  wire refclk625_n;
  wire refclk625_p;
  wire reset;
  wire [5:0]riu_addr_out;
  wire riu_clk_out;
  wire [1:0]riu_nibble_sel_out;
  wire riu_prsnt;
  wire riu_prsnt_1;
  wire riu_prsnt_2;
  wire riu_prsnt_3;
  wire [15:0]riu_rd_data;
  wire [15:0]riu_rddata_1;
  wire [15:0]riu_rddata_2;
  wire [15:0]riu_rddata_3;
  wire riu_valid;
  wire riu_valid_1;
  wire riu_valid_2;
  wire riu_valid_3;
  wire [15:0]riu_wr_data_out;
  wire riu_wr_en_out;
  wire rst_125_out;
  wire rx_bs_en_vtc_out;
  wire rx_bs_rst_out;
  wire rx_bsc_en_vtc_out;
  wire rx_bsc_rst_out;
  wire [8:0]rx_btval;
  wire [8:0]rx_btval_1;
  wire [8:0]rx_btval_2;
  wire [8:0]rx_btval_3;
  wire rx_dly_rdy;
  wire rx_dly_rdy_1;
  wire rx_dly_rdy_2;
  wire rx_dly_rdy_3;
  wire rx_locked;
  wire rx_logic_reset;
  wire rx_pll_clk_out;
  wire rx_rst_dly_out;
  wire rx_vtc_rdy;
  wire rx_vtc_rdy_1;
  wire rx_vtc_rdy_2;
  wire rx_vtc_rdy_3;
  wire rxn_0;
  wire rxp_0;
  wire sgmii_clk_en_0;
  wire sgmii_clk_f_0;
  wire sgmii_clk_r_0;
  wire signal_detect_0;
  wire speed_is_100_0;
  wire speed_is_10_100_0;
  wire [13:0]\^status_vector_0 ;
  wire tx_bs_en_vtc_out;
  wire tx_bs_rst_out;
  wire tx_bsc_en_vtc_out;
  wire tx_bsc_rst_out;
  wire tx_dly_rdy;
  wire tx_dly_rdy_1;
  wire tx_dly_rdy_2;
  wire tx_dly_rdy_3;
  wire tx_locked;
  wire tx_logic_reset;
  wire tx_pll_clk_out;
  wire tx_rdclk_out;
  wire tx_rst_dly_out;
  wire tx_vtc_rdy;
  wire tx_vtc_rdy_1;
  wire tx_vtc_rdy_2;
  wire tx_vtc_rdy_3;
  wire txn_0;
  wire txp_0;
  wire NLW_clock_reset_i_ClockIn_se_out_UNCONNECTED;
  wire [7:0]NLW_clock_reset_i_Debug_Out_UNCONNECTED;

  assign status_vector_0[15] = \<const0> ;
  assign status_vector_0[14] = \<const0> ;
  assign status_vector_0[13:9] = \^status_vector_0 [13:9];
  assign status_vector_0[8] = \<const0> ;
  assign status_vector_0[7:0] = \^status_vector_0 [7:0];
  GND GND
       (.G(\<const0> ));
  (* C_IoBank = "44" *) 
  (* C_Part = "XCKU060" *) 
  (* DONT_TOUCH *) 
  (* EXAMPLE_SIMULATION = "0" *) 
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_Clock_Reset clock_reset_i
       (.ClockIn_n(refclk625_n),
        .ClockIn_p(refclk625_p),
        .ClockIn_se_out(NLW_clock_reset_i_ClockIn_se_out_UNCONNECTED),
        .Debug_Out(NLW_clock_reset_i_Debug_Out_UNCONNECTED[7:0]),
        .ResetIn(reset),
        .Riu_Addr(riu_addr_out),
        .Riu_Nibble_Sel(riu_nibble_sel_out),
        .Riu_Prsnt_0(riu_prsnt),
        .Riu_Prsnt_1(riu_prsnt_1),
        .Riu_Prsnt_2(riu_prsnt_2),
        .Riu_Prsnt_3(riu_prsnt_3),
        .Riu_RdData_0(riu_rd_data),
        .Riu_RdData_1(riu_rddata_1),
        .Riu_RdData_2(riu_rddata_2),
        .Riu_RdData_3(riu_rddata_3),
        .Riu_Valid_0(riu_valid),
        .Riu_Valid_1(riu_valid_1),
        .Riu_Valid_2(riu_valid_2),
        .Riu_Valid_3(riu_valid_3),
        .Riu_WrData(riu_wr_data_out),
        .Riu_Wr_En(riu_wr_en_out),
        .Rx_Bs_EnVtc(rx_bs_en_vtc_out),
        .Rx_Bs_Rst(rx_bs_rst_out),
        .Rx_Bs_RstDly(rx_rst_dly_out),
        .Rx_Bsc_EnVtc(rx_bsc_en_vtc_out),
        .Rx_Bsc_Rst(rx_bsc_rst_out),
        .Rx_BtVal_0(rx_btval),
        .Rx_BtVal_1(rx_btval_1),
        .Rx_BtVal_2(rx_btval_2),
        .Rx_BtVal_3(rx_btval_3),
        .Rx_ClkOutPhy(rx_pll_clk_out),
        .Rx_Dly_Rdy(rx_dly_rdy),
        .Rx_Locked(rx_locked),
        .Rx_LogicRst(rx_logic_reset),
        .Rx_RiuClk(riu_clk_out),
        .Rx_SysClk(clk312_out),
        .Rx_Vtc_Rdy(rx_vtc_rdy),
        .Tx_Bs_EnVtc(tx_bs_en_vtc_out),
        .Tx_Bs_Rst(tx_bs_rst_out),
        .Tx_Bs_RstDly(tx_rst_dly_out),
        .Tx_Bsc_EnVtc(tx_bsc_en_vtc_out),
        .Tx_Bsc_Rst(tx_bsc_rst_out),
        .Tx_ClkOutPhy(tx_pll_clk_out),
        .Tx_Dly_Rdy(tx_dly_rdy),
        .Tx_Locked(tx_locked),
        .Tx_LogicRst(tx_logic_reset),
        .Tx_SysClk(tx_rdclk_out),
        .Tx_Vtc_Rdy(tx_vtc_rdy),
        .Tx_WrClk(clk125_out));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_block pcs_pma_block_i
       (.CLK(tx_rdclk_out),
        .D(rx_btval[8:3]),
        .Rx_Dly_Rdy(rx_dly_rdy),
        .Rx_SysClk(clk312_out),
        .Rx_Vtc_Rdy(rx_vtc_rdy),
        .Tx_Dly_Rdy(tx_dly_rdy),
        .Tx_Vtc_Rdy(tx_vtc_rdy),
        .Tx_WrClk(clk125_out),
        .an_adv_config_vector_0(an_adv_config_vector_0[11]),
        .an_interrupt_0(an_interrupt_0),
        .an_restart_config_0(an_restart_config_0),
        .configuration_vector_0(configuration_vector_0),
        .gmii_isolate_0(gmii_isolate_0),
        .gmii_rx_dv_0(gmii_rx_dv_0),
        .gmii_rx_er_0(gmii_rx_er_0),
        .gmii_rxd_0(gmii_rxd_0),
        .gmii_tx_en_0(gmii_tx_en_0),
        .gmii_tx_er_0(gmii_tx_er_0),
        .gmii_txd_0(gmii_txd_0),
        .reset_out(rst_125_out),
        .riu_addr_out(riu_addr_out),
        .riu_clk_out(riu_clk_out),
        .riu_nibble_sel_out(riu_nibble_sel_out),
        .riu_prsnt(riu_prsnt),
        .riu_rd_data(riu_rd_data),
        .riu_valid(riu_valid),
        .riu_wr_data_out(riu_wr_data_out),
        .riu_wr_en_out(riu_wr_en_out),
        .rx_bs_en_vtc_out(rx_bs_en_vtc_out),
        .rx_bs_rst_out(rx_bs_rst_out),
        .rx_bsc_en_vtc_out(rx_bsc_en_vtc_out),
        .rx_bsc_rst_out(rx_bsc_rst_out),
        .rx_dly_rdy_1(rx_dly_rdy_1),
        .rx_dly_rdy_2(rx_dly_rdy_2),
        .rx_dly_rdy_3(rx_dly_rdy_3),
        .rx_pll_clk_out(rx_pll_clk_out),
        .rx_rst_dly_out(rx_rst_dly_out),
        .rx_vtc_rdy_1(rx_vtc_rdy_1),
        .rx_vtc_rdy_2(rx_vtc_rdy_2),
        .rx_vtc_rdy_3(rx_vtc_rdy_3),
        .rxn_0(rxn_0),
        .rxp_0(rxp_0),
        .sgmii_clk_en_0(sgmii_clk_en_0),
        .sgmii_clk_f_0(sgmii_clk_f_0),
        .sgmii_clk_r_0(sgmii_clk_r_0),
        .signal_detect_0(signal_detect_0),
        .speed_is_100_0(speed_is_100_0),
        .speed_is_10_100_0(speed_is_10_100_0),
        .status_vector_0({\^status_vector_0 [13:9],\^status_vector_0 [7:0]}),
        .tx_bs_en_vtc_out(tx_bs_en_vtc_out),
        .tx_bs_rst_out(tx_bs_rst_out),
        .tx_bsc_en_vtc_out(tx_bsc_en_vtc_out),
        .tx_bsc_rst_out(tx_bsc_rst_out),
        .tx_dly_rdy_1(tx_dly_rdy_1),
        .tx_dly_rdy_2(tx_dly_rdy_2),
        .tx_dly_rdy_3(tx_dly_rdy_3),
        .tx_pll_clk_out(tx_pll_clk_out),
        .tx_rst_dly_out(tx_rst_dly_out),
        .tx_vtc_rdy_1(tx_vtc_rdy_1),
        .tx_vtc_rdy_2(tx_vtc_rdy_2),
        .tx_vtc_rdy_3(tx_vtc_rdy_3),
        .txn_0(txn_0),
        .txp_0(txp_0));
  gig_eth_pcs_pma_gmii_to_sgmii_bridge_reset_sync reset_sync_clk125_i
       (.Tx_WrClk(clk125_out),
        .rst_125_out(rst_125_out),
        .rx_logic_reset(rx_logic_reset),
        .tx_logic_reset(tx_logic_reset));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block
   (data_out,
    speed_is_100_0,
    Tx_WrClk);
  output data_out;
  input speed_is_100_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire speed_is_100_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_100_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_1
   (data_out,
    speed_is_10_100_0,
    Tx_WrClk);
  output data_out;
  input speed_is_10_100_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire speed_is_10_100_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(speed_is_10_100_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_10
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    \wr_occupancy_reg[6]_1 ,
    \wr_occupancy_reg[6]_2 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input \wr_occupancy_reg[6]_1 ;
  input \wr_occupancy_reg[6]_2 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;
  wire \wr_occupancy_reg[6]_1 ;
  wire \wr_occupancy_reg[6]_2 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    wr_occupancy0_carry_i_5
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .I4(\wr_occupancy_reg[6]_1 ),
        .I5(\wr_occupancy_reg[6]_2 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_11
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    \wr_occupancy_reg[6]_1 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input \wr_occupancy_reg[6]_1 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;
  wire \wr_occupancy_reg[6]_1 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h69969669)) 
    wr_occupancy0_carry_i_4
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .I4(\wr_occupancy_reg[6]_1 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_12
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    \wr_occupancy_reg[6]_0 ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [0:0]S;
  output data_out;
  input [0:0]Q;
  input \wr_occupancy_reg[6] ;
  input \wr_occupancy_reg[6]_0 ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire [0:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \wr_occupancy_reg[6] ;
  wire \wr_occupancy_reg[6]_0 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9669)) 
    wr_occupancy0_carry_i_3
       (.I0(Q),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .I3(\wr_occupancy_reg[6]_0 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_13
   (S,
    data_out,
    Q,
    \wr_occupancy_reg[6] ,
    data_in,
    Rx_SysClk);
  output [1:0]S;
  output data_out;
  input [1:0]Q;
  input \wr_occupancy_reg[6] ;
  input data_in;
  input Rx_SysClk;

  wire [1:0]Q;
  wire Rx_SysClk;
  wire [1:0]S;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire \wr_occupancy_reg[6] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    wr_occupancy0_carry_i_1
       (.I0(Q[1]),
        .I1(data_out),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    wr_occupancy0_carry_i_2
       (.I0(Q[0]),
        .I1(data_out),
        .I2(\wr_occupancy_reg[6] ),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_14
   (data_out,
    Q,
    Tx_WrClk);
  output data_out;
  input [0:0]Q;
  input Tx_WrClk;

  wire [0:0]Q;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_15
   (S,
    data_sync_reg6_0,
    rd_wr_addr,
    Q,
    data_out,
    data_sync_reg1_0,
    Tx_WrClk);
  output [1:0]S;
  output data_sync_reg6_0;
  input [0:0]rd_wr_addr;
  input [1:0]Q;
  input data_out;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [1:0]Q;
  wire [1:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire data_sync_reg6_0;
  wire [0:0]rd_wr_addr;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_sync_reg6_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    rd_occupancy0_carry_i_12
       (.I0(rd_wr_addr),
        .I1(Q[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    rd_occupancy0_carry_i_13
       (.I0(rd_wr_addr),
        .I1(data_out),
        .I2(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_16
   (data_out,
    Q,
    Tx_WrClk);
  output data_out;
  input [0:0]Q;
  input Tx_WrClk;

  wire [0:0]Q;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_17
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    \rd_occupancy_reg[6]_1 ,
    \rd_occupancy_reg[6]_2 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input \rd_occupancy_reg[6]_1 ;
  input \rd_occupancy_reg[6]_2 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;
  wire \rd_occupancy_reg[6]_1 ;
  wire \rd_occupancy_reg[6]_2 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    rd_occupancy0_carry_i_11
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(\rd_occupancy_reg[6]_1 ),
        .I4(\rd_occupancy_reg[6]_2 ),
        .I5(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_18
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    \rd_occupancy_reg[6]_1 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input \rd_occupancy_reg[6]_1 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;
  wire \rd_occupancy_reg[6]_1 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h69969669)) 
    rd_occupancy0_carry_i_10
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(\rd_occupancy_reg[6]_1 ),
        .I4(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_19
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    \rd_occupancy_reg[6]_0 ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [0:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input \rd_occupancy_reg[6]_0 ;
  input [0:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [0:0]Q;
  wire [0:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;
  wire \rd_occupancy_reg[6]_0 ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h9669)) 
    rd_occupancy0_carry_i_9
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(\rd_occupancy_reg[6]_0 ),
        .I3(Q),
        .O(S));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_20
   (S,
    data_out,
    \rd_occupancy_reg[6] ,
    Q,
    data_sync_reg1_0,
    Tx_WrClk);
  output [1:0]S;
  output data_out;
  input \rd_occupancy_reg[6] ;
  input [1:0]Q;
  input [0:0]data_sync_reg1_0;
  input Tx_WrClk;

  wire [1:0]Q;
  wire [1:0]S;
  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire \rd_occupancy_reg[6] ;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    rd_occupancy0_carry_i_7
       (.I0(data_out),
        .I1(Q[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    rd_occupancy0_carry_i_8
       (.I0(data_out),
        .I1(\rd_occupancy_reg[6] ),
        .I2(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_21
   (data_out,
    data_sync_reg1_0,
    Tx_WrClk);
  output data_out;
  input data_sync_reg1_0;
  input Tx_WrClk;

  wire Tx_WrClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_7
   (data_out,
    Q,
    Rx_SysClk);
  output data_out;
  input [0:0]Q;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_8
   (S,
    Q,
    data_out,
    wr_occupancy0_carry_i_7_0,
    wr_occupancy0_carry_i_7_1,
    wr_occupancy0_carry_i_7_2,
    wr_occupancy0_carry_i_7_3,
    \wr_occupancy_reg[6] ,
    data_sync_reg1_0,
    Rx_SysClk);
  output [1:0]S;
  input [1:0]Q;
  input data_out;
  input wr_occupancy0_carry_i_7_0;
  input wr_occupancy0_carry_i_7_1;
  input wr_occupancy0_carry_i_7_2;
  input wr_occupancy0_carry_i_7_3;
  input \wr_occupancy_reg[6] ;
  input [0:0]data_sync_reg1_0;
  input Rx_SysClk;

  wire [1:0]Q;
  wire Rx_SysClk;
  wire [1:0]S;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire [0:0]data_sync_reg1_0;
  wire p_8_in;
  wire wr_occupancy0_carry_i_7_0;
  wire wr_occupancy0_carry_i_7_1;
  wire wr_occupancy0_carry_i_7_2;
  wire wr_occupancy0_carry_i_7_3;
  wire \wr_occupancy_reg[6] ;
  wire wr_rd_addr_gray_1;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(wr_rd_addr_gray_1),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    wr_occupancy0_carry_i_6
       (.I0(Q[1]),
        .I1(p_8_in),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    wr_occupancy0_carry_i_7
       (.I0(Q[0]),
        .I1(p_8_in),
        .I2(\wr_occupancy_reg[6] ),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    wr_occupancy0_carry_i_8
       (.I0(wr_rd_addr_gray_1),
        .I1(data_out),
        .I2(wr_occupancy0_carry_i_7_0),
        .I3(wr_occupancy0_carry_i_7_1),
        .I4(wr_occupancy0_carry_i_7_2),
        .I5(wr_occupancy0_carry_i_7_3),
        .O(p_8_in));
endmodule

(* ORIG_REF_NAME = "gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block" *) 
module gig_eth_pcs_pma_gmii_to_sgmii_bridge_sync_block_9
   (data_out,
    Q,
    Rx_SysClk);
  output data_out;
  input [0:0]Q;
  input Rx_SysClk;

  wire [0:0]Q;
  wire Rx_SysClk;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(Q),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(Rx_SysClk),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_rate_adapt
   (gmii_tx_en_out,
    gmii_tx_er_out,
    gmii_txd_out,
    reset_out,
    E,
    gmii_tx_en_0,
    Tx_WrClk,
    gmii_tx_er_0,
    gmii_txd_0);
  output gmii_tx_en_out;
  output gmii_tx_er_out;
  output [7:0]gmii_txd_out;
  input reset_out;
  input [0:0]E;
  input gmii_tx_en_0;
  input Tx_WrClk;
  input gmii_tx_er_0;
  input [7:0]gmii_txd_0;

  wire [0:0]E;
  wire Tx_WrClk;
  wire gmii_tx_en_0;
  wire gmii_tx_en_out;
  wire gmii_tx_er_0;
  wire gmii_tx_er_out;
  wire [7:0]gmii_txd_0;
  wire [7:0]gmii_txd_out;
  wire reset_out;

  FDRE #(
    .INIT(1'b0)) 
    gmii_tx_en_out_reg
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_tx_en_0),
        .Q(gmii_tx_en_out),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    gmii_tx_er_out_reg
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_tx_er_0),
        .Q(gmii_tx_er_out),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[0] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[0]),
        .Q(gmii_txd_out[0]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[1] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[1]),
        .Q(gmii_txd_out[1]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[2] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[2]),
        .Q(gmii_txd_out[2]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[3] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[3]),
        .Q(gmii_txd_out[3]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[4] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[4]),
        .Q(gmii_txd_out[4]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[5] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[5]),
        .Q(gmii_txd_out[5]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[6] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[6]),
        .Q(gmii_txd_out[6]),
        .R(reset_out));
  FDRE #(
    .INIT(1'b0)) 
    \gmii_txd_out_reg[7] 
       (.C(Tx_WrClk),
        .CE(E),
        .D(gmii_txd_0[7]),
        .Q(gmii_txd_out[7]),
        .R(reset_out));
endmodule

module gig_eth_pcs_pma_gmii_to_sgmii_bridge_tx_ten_to_eight
   (Q,
    Tx_WrClk,
    tx_data_10b,
    CLK,
    reset_out);
  output [7:0]Q;
  input Tx_WrClk;
  input [9:0]tx_data_10b;
  input CLK;
  input reset_out;

  wire CLK;
  wire \DataOut[0]_i_1_n_0 ;
  wire \DataOut[0]_i_2_n_0 ;
  wire \DataOut[1]_i_1_n_0 ;
  wire \DataOut[1]_i_2_n_0 ;
  wire \DataOut[2]_i_1_n_0 ;
  wire \DataOut[2]_i_2_n_0 ;
  wire \DataOut[3]_i_1_n_0 ;
  wire \DataOut[3]_i_2_n_0 ;
  wire \DataOut[4]_i_1_n_0 ;
  wire \DataOut[4]_i_2_n_0 ;
  wire \DataOut[5]_i_1_n_0 ;
  wire \DataOut[5]_i_2_n_0 ;
  wire \DataOut[6]_i_1_n_0 ;
  wire \DataOut[6]_i_2_n_0 ;
  wire \DataOut[7]_i_1_n_0 ;
  wire \DataOut[7]_i_2_n_0 ;
  wire \FSM_sequential_IntState[1]_i_1_n_0 ;
  wire \FSM_sequential_IntState[2]_i_1_n_0 ;
  wire \IntLastOut_reg_n_0_[4] ;
  wire \IntLastOut_reg_n_0_[5] ;
  wire \IntLastOut_reg_n_0_[6] ;
  wire \IntLastOut_reg_n_0_[7] ;
  wire [9:0]IntRamOut;
  wire IntRdAddr;
  wire [3:0]IntRdAddr_reg;
  (* async_reg = "true" *) wire [1:0]IntRdEna_Sync;
  wire IntRdEna_i_1_n_0;
  wire IntState;
  wire [2:0]IntState__0;
  wire [2:0]IntState__1;
  wire [3:0]IntWrAddr_reg;
  wire [7:0]Q;
  (* async_reg = "true" *) wire [1:0]Reset_Sync;
  wire Tx_WrClk;
  wire [1:0]in3;
  wire [0:0]p_0_in;
  wire [3:0]p_0_in__2;
  wire [3:0]p_0_in__4;
  wire reset_out;
  wire [9:0]tx_data_10b;
  wire [1:0]NLW_FIFO_ram_inst0_DOD_UNCONNECTED;
  wire [1:0]NLW_FIFO_ram_inst1_DOC_UNCONNECTED;
  wire [1:0]NLW_FIFO_ram_inst1_DOD_UNCONNECTED;

  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[0]_i_1 
       (.I0(IntRamOut[2]),
        .I1(IntState__0[2]),
        .I2(\DataOut[0]_i_2_n_0 ),
        .O(\DataOut[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[0]_i_2 
       (.I0(\IntLastOut_reg_n_0_[4] ),
        .I1(\IntLastOut_reg_n_0_[6] ),
        .I2(IntState__0[1]),
        .I3(in3[0]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[0]),
        .O(\DataOut[0]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[1]_i_1 
       (.I0(IntRamOut[3]),
        .I1(IntState__0[2]),
        .I2(\DataOut[1]_i_2_n_0 ),
        .O(\DataOut[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[1]_i_2 
       (.I0(\IntLastOut_reg_n_0_[5] ),
        .I1(\IntLastOut_reg_n_0_[7] ),
        .I2(IntState__0[1]),
        .I3(in3[1]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[1]),
        .O(\DataOut[1]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[2]_i_1 
       (.I0(IntRamOut[4]),
        .I1(IntState__0[2]),
        .I2(\DataOut[2]_i_2_n_0 ),
        .O(\DataOut[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[2]_i_2 
       (.I0(\IntLastOut_reg_n_0_[6] ),
        .I1(in3[0]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[0]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[2]),
        .O(\DataOut[2]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[3]_i_1 
       (.I0(IntRamOut[5]),
        .I1(IntState__0[2]),
        .I2(\DataOut[3]_i_2_n_0 ),
        .O(\DataOut[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[3]_i_2 
       (.I0(\IntLastOut_reg_n_0_[7] ),
        .I1(in3[1]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[1]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[3]),
        .O(\DataOut[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[4]_i_1 
       (.I0(IntRamOut[6]),
        .I1(IntState__0[2]),
        .I2(\DataOut[4]_i_2_n_0 ),
        .O(\DataOut[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[4]_i_2 
       (.I0(in3[0]),
        .I1(IntRamOut[0]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[2]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[4]),
        .O(\DataOut[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[5]_i_1 
       (.I0(IntRamOut[7]),
        .I1(IntState__0[2]),
        .I2(\DataOut[5]_i_2_n_0 ),
        .O(\DataOut[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[5]_i_2 
       (.I0(in3[1]),
        .I1(IntRamOut[1]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[3]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[5]),
        .O(\DataOut[5]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[6]_i_1 
       (.I0(IntRamOut[8]),
        .I1(IntState__0[2]),
        .I2(\DataOut[6]_i_2_n_0 ),
        .O(\DataOut[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[6]_i_2 
       (.I0(IntRamOut[0]),
        .I1(IntRamOut[2]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[4]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[6]),
        .O(\DataOut[6]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \DataOut[7]_i_1 
       (.I0(IntRamOut[9]),
        .I1(IntState__0[2]),
        .I2(\DataOut[7]_i_2_n_0 ),
        .O(\DataOut[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \DataOut[7]_i_2 
       (.I0(IntRamOut[1]),
        .I1(IntRamOut[3]),
        .I2(IntState__0[1]),
        .I3(IntRamOut[5]),
        .I4(IntState__0[0]),
        .I5(IntRamOut[7]),
        .O(\DataOut[7]_i_2_n_0 ));
  FDSE \DataOut_reg[0] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[0]_i_1_n_0 ),
        .Q(Q[0]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[1] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[1]_i_1_n_0 ),
        .Q(Q[1]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[2] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[2]_i_1_n_0 ),
        .Q(Q[2]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[3] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[3]_i_1_n_0 ),
        .Q(Q[3]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[4] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[4]_i_1_n_0 ),
        .Q(Q[4]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[5] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[5]_i_1_n_0 ),
        .Q(Q[5]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[6] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[6]_i_1_n_0 ),
        .Q(Q[6]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDSE \DataOut_reg[7] 
       (.C(CLK),
        .CE(IntState),
        .D(\DataOut[7]_i_1_n_0 ),
        .Q(Q[7]),
        .S(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* box_type = "PRIMITIVE" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b0)) 
    FIFO_ram_inst0
       (.ADDRA({1'b0,IntRdAddr_reg}),
        .ADDRB({1'b0,IntRdAddr_reg}),
        .ADDRC({1'b0,IntRdAddr_reg}),
        .ADDRD({1'b0,IntWrAddr_reg}),
        .DIA(tx_data_10b[1:0]),
        .DIB(tx_data_10b[3:2]),
        .DIC(tx_data_10b[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(IntRamOut[1:0]),
        .DOB(IntRamOut[3:2]),
        .DOC(IntRamOut[5:4]),
        .DOD(NLW_FIFO_ram_inst0_DOD_UNCONNECTED[1:0]),
        .WCLK(Tx_WrClk),
        .WE(1'b1));
  (* box_type = "PRIMITIVE" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b0)) 
    FIFO_ram_inst1
       (.ADDRA({1'b0,IntRdAddr_reg}),
        .ADDRB({1'b0,IntRdAddr_reg}),
        .ADDRC({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ADDRD({1'b0,IntWrAddr_reg}),
        .DIA(tx_data_10b[7:6]),
        .DIB(tx_data_10b[9:8]),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(IntRamOut[7:6]),
        .DOB(IntRamOut[9:8]),
        .DOC(NLW_FIFO_ram_inst1_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_FIFO_ram_inst1_DOD_UNCONNECTED[1:0]),
        .WCLK(Tx_WrClk),
        .WE(1'b1));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_IntState[0]_i_1 
       (.I0(IntState__0[0]),
        .I1(IntState__0[2]),
        .O(IntState__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \FSM_sequential_IntState[1]_i_1 
       (.I0(IntState__0[1]),
        .I1(IntState__0[0]),
        .I2(IntState__0[2]),
        .O(\FSM_sequential_IntState[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_IntState[2]_i_1 
       (.I0(Reset_Sync[1]),
        .I1(IntRdEna_Sync[1]),
        .O(\FSM_sequential_IntState[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h57)) 
    \FSM_sequential_IntState[2]_i_2 
       (.I0(IntState__0[2]),
        .I1(IntState__0[1]),
        .I2(IntState__0[0]),
        .O(IntState));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_IntState[2]_i_3 
       (.I0(IntState__0[2]),
        .I1(IntState__0[0]),
        .I2(IntState__0[1]),
        .O(IntState__1[2]));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[0] 
       (.C(CLK),
        .CE(IntState),
        .D(IntState__1[0]),
        .Q(IntState__0[0]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[1] 
       (.C(CLK),
        .CE(IntState),
        .D(\FSM_sequential_IntState[1]_i_1_n_0 ),
        .Q(IntState__0[1]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:000,iSTATE0:001,iSTATE1:010,iSTATE2:011,iSTATE3:100," *) 
  FDRE \FSM_sequential_IntState_reg[2] 
       (.C(CLK),
        .CE(IntState),
        .D(IntState__1[2]),
        .Q(IntState__0[2]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntLastOut_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[4]),
        .Q(\IntLastOut_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[5]),
        .Q(\IntLastOut_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[6]),
        .Q(\IntLastOut_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[7]),
        .Q(\IntLastOut_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \IntLastOut_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[8]),
        .Q(in3[0]),
        .R(1'b0));
  FDRE \IntLastOut_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRamOut[9]),
        .Q(in3[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \IntRdAddr[0]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .O(p_0_in__4[0]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \IntRdAddr[1]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .I1(IntRdAddr_reg[1]),
        .O(p_0_in__4[1]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \IntRdAddr[2]_i_1 
       (.I0(IntRdAddr_reg[0]),
        .I1(IntRdAddr_reg[1]),
        .I2(IntRdAddr_reg[2]),
        .O(p_0_in__4[2]));
  LUT3 #(
    .INIT(8'h17)) 
    \IntRdAddr[3]_i_1 
       (.I0(IntState__0[1]),
        .I1(IntState__0[2]),
        .I2(IntState__0[0]),
        .O(IntRdAddr));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \IntRdAddr[3]_i_2 
       (.I0(IntRdAddr_reg[1]),
        .I1(IntRdAddr_reg[0]),
        .I2(IntRdAddr_reg[2]),
        .I3(IntRdAddr_reg[3]),
        .O(p_0_in__4[3]));
  FDRE \IntRdAddr_reg[0] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[0]),
        .Q(IntRdAddr_reg[0]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[1] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[1]),
        .Q(IntRdAddr_reg[1]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[2] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[2]),
        .Q(IntRdAddr_reg[2]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  FDRE \IntRdAddr_reg[3] 
       (.C(CLK),
        .CE(IntRdAddr),
        .D(p_0_in__4[3]),
        .Q(IntRdAddr_reg[3]),
        .R(\FSM_sequential_IntState[2]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntRdEna_Sync_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(IntRdEna_Sync[0]),
        .R(Reset_Sync[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE \IntRdEna_Sync_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(IntRdEna_Sync[0]),
        .Q(IntRdEna_Sync[1]),
        .R(Reset_Sync[1]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    IntRdEna_i_1
       (.I0(IntWrAddr_reg[2]),
        .I1(IntWrAddr_reg[3]),
        .I2(IntWrAddr_reg[1]),
        .I3(IntWrAddr_reg[0]),
        .I4(p_0_in),
        .O(IntRdEna_i_1_n_0));
  FDRE IntRdEna_reg
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(IntRdEna_i_1_n_0),
        .Q(p_0_in),
        .R(reset_out));
  LUT1 #(
    .INIT(2'h1)) 
    \IntWrAddr[0]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \IntWrAddr[1]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .I1(IntWrAddr_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \IntWrAddr[2]_i_1 
       (.I0(IntWrAddr_reg[0]),
        .I1(IntWrAddr_reg[1]),
        .I2(IntWrAddr_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \IntWrAddr[3]_i_1 
       (.I0(IntWrAddr_reg[3]),
        .I1(IntWrAddr_reg[0]),
        .I2(IntWrAddr_reg[1]),
        .I3(IntWrAddr_reg[2]),
        .O(p_0_in__2[3]));
  FDRE \IntWrAddr_reg[0] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[0]),
        .Q(IntWrAddr_reg[0]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[1] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[1]),
        .Q(IntWrAddr_reg[1]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[2] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[2]),
        .Q(IntWrAddr_reg[2]),
        .R(reset_out));
  FDRE \IntWrAddr_reg[3] 
       (.C(Tx_WrClk),
        .CE(1'b1),
        .D(p_0_in__2[3]),
        .Q(IntWrAddr_reg[3]),
        .R(reset_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \Reset_Sync_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_out),
        .Q(Reset_Sync[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \Reset_Sync_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(Reset_Sync[0]),
        .PRE(reset_out),
        .Q(Reset_Sync[1]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 241024)
`pragma protect data_block
NemwHFFRd828bwYGow++se//gdRVXMeRTcOPc/gSLYqg5t3k/BU3S5AwbLnvfECKJLyr2kBCycKA
ioeZwb74csJCyLncabtbYzViDVE26LCaJs+SgYW3GzoAd9UvC3OtIKAhz+vdmPevUOfvCjlavH0Y
HLUn8ax+0gbFKCi/vP7Fe09jmJ/qblgeU1kxfv5slIZ7o5w9S3l2r+RdVeJO7V9TQhf1bFGVefz3
4bym1zSAf42RNy9UzVornL6rVdGfDc8oVbVWej25vBRUv9S5dnRfflE4GQQjJxoGWz9jBkphv2PI
l1oYGDcvZbXMRMsuhJ/eeSbW1AERop/wXVz+6syQFy4aQE8XdmjWMpnQU8DTQ3D5b0nlGUCQqiHE
ifhIoOJnldvg9g2LS7lcTzXDPRj84pwF8/VBAADLqcryhkBXoN19EV4VIpau0aFqND5nyQ0imukL
nGaiT32z00lVXUIJhxigDD+txV63dhDc6xGHQGhTbA63HiLPR4XOinP0z7NbvCJj/eeDb5b+liQy
l27SLfFGMFyC9B7hJjHCbFhCbhBmIC/+dcd+17E6EUWADwAq1QBsysxmViGq/4uU8BXGALcMk3VO
ZDeYAN5biwPqP0bWcFvU5vaFvdfLbfBegsKumi1woq19zX4VEx21u0yxzZ5mVctyCbzrNWHCwnru
/v0HpWCOBUTE6Aj4X6pLwxKQBMGXjiPIMwGZ4FOtK9jkMzmNynlT6Zrnpb/C787rlHoySVOu11ph
wbMUD937XCrbMltKIUZ5G/CJ1/i0DsuKAM2go06VJtKEHKCuQrBIy+HimDVIGClq+4cQnIoNnOa0
+dQSOuxgMm2Kgxu2aNB4No9yoeEoBG82LXqr8Am9XI2k574cIsaezKLVYYSxBQCeoyzMaBSjQdrg
JdTCTbi1v+swhFhuj9b23+DubPYWXUjhx/lSH2M9Ijogcq9wYommUq4CfKPTsT9TUmkEzZHZ/d9m
imdCviLn57qZyPWJQsC1V5aSM8M/Zt/Wgh2vTbIkxGFAQ2YyVwy1QQyYk5N8AKZE4DfSNX/hsXuO
vLet7BXP9/BDqGh0GAynMRmlyPbLBx/rd2ex/mu0ZoK13AMZY0bXEZF6YrIZlpBQI796M8ikx2Q0
Ycyer/+DKIIcuHXCrNn5SntrJAGBXepYBC9dRivdl/T+ygxw71Q0YIWbvJRpNzGlFB459EHtSt0c
Hog8LVsthU2IitLE0M4aaulBlnpcT4v6Ht59qj0uLOmYA+tXMjYjVxplsXe7/sXlizvE5zShdTOm
lys6g/07SoL1fOIejvSDo1Mhz6SrUheIKdromD4DH4I6M8lrirxDwakfJWacOi5JmoQ/FwOibqx5
KakJkj0K7lyWNBfQjA3dQyggTbqwjW9okS+5478c/7eILBRT+ZRFIClei1I9QZrolh2/BfeukBYN
Cw6TNzUPTGd8GY1YmcRIJwBKnDjE0G0/Q9VdDZdn/Ik3fkl1SsFmLrVLxi5qOEenHWxNmsNGPOWn
kPn08vXVObbeaqSG3jj1BBnEFBHrppRtsZKKHg6pKi3VOlPLfCleBisiqiZOq1SjHqfixhHg8vBt
ISr6FffW1fP3yj6HdfwaTcpQ7UqKMOdtv2wjQ/VSlCOcFukGp7/MdnEzERIeYhDUtQblF+ShXqfn
in/DnDLCALaTKObMR3DeE1BAiii+pG/Of6iW42b64cS70rlAVlVt7Be/39YWvZleL4ES9fBJ5faJ
gQglVUKTVQPyAwr+m7WOR/73HpocKDIE9CDrbxRVMxLfSwpy5ePLOjBP/7EBzWbXGBQ0PsAjZaQl
aUq/f/GzmpTkAH9Z+4XsZ9RawaQ9u+vFJVMnK4yRntcauAmynNj7aCIrJf5hWzYG/LmFYTFC3OiH
RGRDd9LiY/s5zQK/n5kqUUPXkJYnQJJL94i4MVDFtP1bBqkiHBiiVX0feEu+153McStqjg5wCxc2
MPY/Wcb9putnzOEJdlYlO3UIb6CdxY+jzwEh/Td2chMB7Hwsbmkz+LTtH1PYpnWj1KDzt2ccGbfS
5ZMYZZqqG1qYxd7GR4xzv5ptjZDpwUXFaFw9X1fqYKLrtk+7T2WKs7UrPPTIalhrgg7Jxz1yo84L
oAH2eAGoyYrK6OrqHXAnTYWCjSIe0Woag7RQGy6Ql0cSva+zEpJfYXv+BXl5QY6TGh/xMmcj57UO
UrXDKnsazCh2Nsom6V2V5Qf2PRpDuxanVf6lxwCuZ4kZi0Xf+NVn6jOiVPi0D3siLO+DGnuUkqBg
jsyLkBb5ZBEOG7KVMR4AmJXwxwObg1SlPTHrR4YDvoGX4sWGpmise7KuSJnQbFE2ORD98pmhLQ3z
9CTrLBu0BY8kYok8PNwPkAsbJBkYxsIlefHrPgCO6Ux2W5aL12bGUhK05aDtfIPwB5GTAa2u2CyI
OeTsSlIeeFxLnEaUez1DUyC33JTYVQ5C9ZAwjRZgi2hcaFm+zr09hLcYuSrKtMGPxu7jPNtADTEE
AbC3NG0V3i/uBUK2UAze+OK3Yk8siL/iJj9RIUXhIuFn3Lw1PDquYgXJzs1xQ60PEGYOialO3IZ6
C0owYu9nREbqd9eJFcR+U61EK/u5wwcVVfQjNnq61OeYmOh+nEzxQyPYMwbHfD8LKTUOgGIkhVp1
BEf+tSjAVPV4ITNZ0IxUDVFLsmmav4d42+3MMiC4TMWpQLWyz86SMI2igu+ie7JRytgqGg0o2keT
WfMFHSyZfk+b/v/e8xF8m7j4pQqt9q24QL5cOolUA2Y3qEThLLmVdAJ8sHLt8r86XJpv8TpNp66V
PNHO3kFd3vniBAH2oVIRk6NM1pXSvofOlWj1HpGk4eUybxsHeFRsIJYuiZtK6QSUCbm9vXeYERvQ
+XePKAm27AMY2e6cBMzMRkKgGeHy6vjtIhph9ZI0nhvWXORAa3ZiLIVImfyCx0fRavuVUkPZEx+N
PzlgIWfWWwYO44XcIWZy0T7ok44f3xpXuLDwvlnofy55aH9AStdHelXKLVuU9iGiXrbmr009LgRF
7z0gax7ZDzvKkDEyiFRgtNmqNMj3UPKitiRYA4/OLXNORgRCAns5vbb1jr00ZLDO+m76A723VwQF
GDn2NeB3yZlTEM8+kIKCRNBl2DDqzfux6nzViLR+A+6O64lUi3Obn9nR76xTSkE2opEt6JLpBKTl
iw/DUjI4usuW9dVnPVq9dACQ0lf4llMVAK79jN1wvHn0ZEPIBXlKbkilowvSYHv///2PoYvXALcH
nPJeZpEX4krhvKFBqIMhxyvwGhKoAnFluZ0g1imSrPoAmouBOt7p4JY8cFHv4sUsfgy+lN4v65QH
Ax3plA/BK0I2upIWZkh5w2uzA3fOuZlw5WwN/byMbtkChLtgcaGtlg4HqSATktg+xqSaBUgqML//
8K+kIgZyXlEzHqj7gq9CrBBNVQxDjFBoOjbA78Q5tTsQYSWyjr1e9kT6n6BgaIlV3d6V3OWbvenP
U+gkXxfyfphNv1jImn3f5IJIJrAcc/lymoPQyrK5Dwlfk7w4sesU9Z0av2QDjnRaCcLiaO4R2BhP
0Oat0nEcp0/8p2F4ZW0HDaJorMbC4FGgKO9rFzVLqGEDH+9GkT9lODnED4dwEKLrhMpRX0nBtFX0
IavedlMv1GHVpT6OkV6d3yjP1RU+tgG0cr5qKJ+ZJdGOo+YghmuucbQVguqHaC82MX6gERVhwkXB
4XxyWeScTPPzgntcudvPojlxIB9rvvKry9gh3ot4Pys5LVmQ4cPyKzcOkw+lZ67JdVLPRW1McsWj
IwcLIng66CtA+GC8auaeKN4Lhf3ndN9Ffn4MGQUvV/AMJs/6IvA0E5F4csbtPzO88KSIcnLJ1+4P
WmfJvl8+i5JWpuueG8r8vFRz2K5P5jDjo0dB48D9rp/QtjDtRtaqRz0qvgPDUDtPbfNXoNqsL+od
0vB80vgD6UznsL/R/qrVW/P8k403r+jDgho1VkjrgylbpEzxpkXUgF8gJLBfG9k8T+E023Pez523
iJ7uKnLoIwNueFgqO/OwbGgz2a1+2TvA3flpZMNLKGLcFBCCmbnvSoz93bjTtiCCbcHdXETFQzVf
U3y8EOTmeY9sQ8x+O2uW0DIFdZvfYWK6Pk6bUjWTJG/gXEYmaaHs4/gsH2r/BDBLzUsleIo/A5wl
/X+AeqXmVavkvqVuh6ri4Qrs24b3s0iScPGdkNOwffcSd0BP+g7UxCOUQdurMJlPVUoGr+6XO3yA
b0rqP12Lqn5My5062a33bWeaqgBKjOvw9NpH5Hxu76NoMs/vRolVS+8rYnyUUEoV5aTXD+P3RarA
pmtFG6D3ZuPLMQ328r4Wvf/CiD/c9TGG4CmaIpm7y4vlb5TKhsBBA8o6dPbyELvJkSfGWXMIg35H
L64tcP4yz0OpO0m5fZK1RnF8weEf3vzkRdwKJ3V8t5ML6Kmtphm04hKWYll+OSAkUTDgay9FWrc6
fdKwvfYdFln6/UcUT9bED+bTynRGZErkXtuOY6d2qSZw+GIVNMlybl9Wkh1uOXJaWRIpIyyfUT67
X2ejgMa8z+ok/a0aZBTSqVBeZhH/Ovcj8cEXbq9Zhlgt+yilThxz6888tTS9b3hcfXIoi0YcUqRS
pZCfJ0VLwawublAwOnWaFL6kd40tides230JTogj5duNL+Cresl/vNnJw66cUxttRF+hbzWal2he
91jIrtDBe7eCXS9x20WZDmEGa3prGmMEtUNt8Ac9prW+Ovol7YCW29O7SEik+9fzsCigtRa6hmuQ
89uLYRxWGvdSqUakPP6ac6n6BoXUqJK8UhwKDsKjKH9ze224StYV/BezwMkmj6rBDHrvS8A+U5nf
88+g2Nwy9+Y4OnBSDG9zQ+zOYtPMqAr/2CIzuDd2wPrZf/Kapmh3EzgvssUIhW1gDmvMHKOjGaLj
B99wwFxfNKRRqDVJmG+xi1q+swpgVi8ogBk5ncHkrvit+qGuYEITcePF1XTDFrFz5gp/8km9KxZv
afv0weu/1QHXrAfxWJtMuhyAIJii37JBM/eqajtJz2XVe9m9BBm/HnRyCi214Hhjo3wqrQ5wpjSI
Z+qyB+6ooOp//mJ6P5NAMIaWPHImkHwvW+hZuim/b2XJUvq8IgVs5B9S8D1Tu477nObHDvMppMqx
QsZXIhTCjVh1eNZZxvnlHG4mtj9m+o5U4G1vYfaiPBiA/bWLu62ClVrRcj+0jq51ax7JEowIDD9b
lL73ebZVB/8hpcJWc/Zmx5YN12Wk0B8dqRhHnGMLAKaKHCS0s4UFTcd7Sc7OqwjNoV6FEmqQuVTk
NJ254kWI8o4XOQf5EelwCXxLb0hM7jgzjy333QVfXFK/2j26edTykeBdWSJZbhJDisH4pt/l3s1k
HPZsxgSXt+NM0Y+ZQ9X3SsxVlkrshfSD8/8fWdh+4mAhVpyQ25A1dYW91lBVvMIf2vJFtOuL7OTp
JgoJq9o7l/LSS4sNPP77PSYLZm53P/FmWst1pUD0EJGIa1HnzsMvH5U3xdRVT3/CwhrMwSPuccP7
rpm8/HydHceekTyCpZJP7aDQSmlCXmdPBH7h3EC4IhNpu8EJbTo6pAmJz3KHK6YY3ZAqxE+n+yTO
wROrqu272u33e2+3lOneBiZQtI73RgQr7AvjXJYlbjs2N6vbopLQX6+AeHmmq047LKh+HM/wAcWV
rCzUQ31523hUTbx7lus8lCJ1EV2kyK7Hi8hNJ0r15NB4OZ4xO0JkRb46+ACxrYNO2H3pydIeTchc
DvBy5TvnfRsHF46S1HaunyPoAe8jyNlr55/zhsW1/5eSo4lytUVI8G0zqE3WhXhYP9c+dqfNMx+E
vFzJ9z1UdrdpPBj9tjJ6Vuy3xbSQtyguAl5NXZsSXGH8zat+u6EXdeH517PZI+TyuNm+zcpCqCDI
4jLfpFZXJkxSMblrRf8S4AJP1nkF0IsuDldhthU7tBnrYV8Ju4bO0z0g15hsuMKezK7zBuxQi4JZ
2y7T6/vBKBenoko57neePHKOQSYHOTswtMZec/QrmS7nSxDXHO1U/iJ8C8uciVPfboCv2FXILyHY
SmW42L83HN3cqnf16se0rxg2yatPT06LFQdhidemRhMW/yda9sj+WQzru45Egdxm3y7kqDyikyFq
pal0BnREmj9QFXMrSSHcYLthj+R13PPVEzsuJa47ud1LkipY5a3KO4/IgXQny7rC1RA7LdkrmJQQ
b7clptz1HucvzZmpRfiTM9dMpb/0tARdeEg9YstP/tqiHjKXmArGOGzLZgayeUwrlOuwRH87f2nO
IcvmYWc7S4/OETH+5ZojeYlKkUUa66MwBnSeuhp/IfxV74tW2oGI2XKCLY6C+sNagX89IMfpA5Io
0coeoTC4EOns15FIwBv6GH+5Vluk6rJDmuQXt1bul36hgoc01EoZKpH2z0YK2jIkr2MbDIuaHQwl
hFqgN5oWmdZMYQL71yzDJzloyKI7xZqwMXGyPvQk12fPPDK8QF5nDathDH4rCGaSQPxiXzVceSwo
6MKYufMTmNvvXPhEWLRMH1+W6mwCAhaIGtzTEA6iSnrlhL6lR1rX2I9BkeSyXlax8pHUhjtMjvWZ
64qRT1N+7G9PvWwEJ+ywxhPkictL2sfDS0TZATuVmfsDgjt7dPObpVe2ejOpnK/zCky4EHKhcPGW
8OB2FHP84E/uCaaMDIfUlDK2zFOPXgfwSrZcd71nC1HJ6IuDFHM/eMEEEA8hwWI+uuUdAoMhss6H
EQiJZWnjOOQ2awbssfOcELgsIS251tn/9NQJT1ViBe97RJPAAoL2eFZSmk37uvydN6ou1HTP9AVC
5ps9iqUuaqE+awcBLZqe/UozNRdaEognAFaIBBovJzar7Aumbh1OkAbpYOppv4/hxitZIlM3LwI1
UCwLlgj3PRYzikZkd8+qzYV6iX3hXpv6M2lTE+QzJ0jkNnIIlLQZsJSW2Vim5ApdLy3+KV5lc0kj
PohoawQ2mi2BlBdY31cdq+xYGWGFjGlPICPj4758QlzZT2uLvlgZ2hwQnEJybDC5s+af5GGIK2Sm
rNhrYD5NF3aAa+s0KbrpcF8jhvD+ZJEjxSS2SESMPAG+uwM+jfrZR+KWcU6p8EX34Z3pvGLEy7kp
+DbrmL/6+NZ3UCUh/oMZmb+PSvJvnTHiGd1FMSloUeHq2eLzoppc5rB9Hq26hTYbsQwx/q2qfsE6
ztV063KWIeh5sBAdcDaySuRsxtoOei7IFGWk8YGE321MjzcAXeJA9V+0NJzArOmtotOGVbqhjTs5
5CqwOO9jhErlXmUbhksIsLV4AkeLmFnXXMI+uF1AVldcGvo72DcTnp6OHaE6xc9sIE2yuNsxui/f
UAd1vGJcwIWz0jhdQfCtvPTzWRPNcBgjNVmtJDl01gQW65EGRhSX0NpJG3RUByPF5X71/94tANuN
o+ha8CJpknVeBCFbjeIYDnN9OX90ZXnOJtZ34JqDUkgtG0nQJILY4qOm31pHS7wbHbZ4K3uZmVWL
qVSefElZ9hOJqh2Ux+lTfaYh60hWmq3BptbmjBUdIVGu2zvgF8tW9xh23nbvUlmzubTZGVPhIZjQ
xRkhDnYGmkIUG433xdDhTtiy3sndPyy4IJ6j92oHr74aUzv/ReyqPq/3+9ZBPMOEigr64E55HvLf
/H6QcVwr1ZHLpKQ41l6G4BBBnoEYx6gDgkpnPzCt0HuFLtccWYmekHYBKJOhKm6ZNhbQRYDDj7KU
bCpJWnStW0Jk2hvYNhrVihOnmFzkx91ipxq4b//nQD+LUZEJ8NwOJXyCpuzJofnGDjpuq5xkEzz4
RF590UgC/iFuvJzDavGnzy1vhqSuz5VYx+cHhJxMi+qHCEHLFUzEGpyj9eks9ogkuEPhCOxX9EiP
y3ex9QngQnSxWlXVgWpJlgDtyL8Te4KIQ2HBO+yDSyzxjci29iMlrxwNm0G+x4pz6D4Q+QLUWZfW
o8rnV6hVe3G45JSOPtUU18VfrAlN/Pr612XPkcwsOe8tRTmV+KK6UkgTzWpMHJkhmrbT9UnMoPoK
J8SwB7ZFvth2jZNi6QhD5z8tAqGARySu14uflAxvXlb0+yHKPBq05916PsRBXkCSOv81DCNUxkvk
fG8JAlRMuvag2nyCY9VrAJvxLfZtohbRW/Yivju16AMKMNTVPYjmy31muDQGIXMQkLz+DkQsxJzC
Dx0YMEBeFqmoaEk+iJn7N6id8496v70Pb2KYWiEp7tmFpuWiKUa8xla+02cYsHrDVoKUxDd0R0O5
/CfeS4wQH2YczA/IFZSEg848VDUgP84QNotqLEPIFENK1GcL3d65l6IDvOcL5bVF5KGbLhYObDzl
WGBTUqLoEK/M4TPVCZulrX1xuda0UqplFWscgRchKk9AsbWVHZHu+LCjaKNTnoKJHutVK5B4lqkd
P/2BsRt0BUtplLoNY7hCuNacm56JcJzfqPVE/WYHUjDTlq0mH8JsBX8MBENJsuEZZP5S1pkw0OLG
gKz3vT9vOaOkbueW3dIx923tuAMhd8B0v0BezYu9mbgwtOYip265uK+a6ELlHW05pLhczun67gLv
e8qF9jdhTxI9uKheZ9o+aqo07ziNvibtJybiyEMzBFRNbwY6s2NCDRPEzy2kxuHqdd0xo2tsb+G1
fg8hoqu6lis32e0HFhtmRzu+kqy9xA1MTtigHPjXOordLeTgFWSvL8xWFzCcIHl5j7IHA5Qv1U8u
1FL94m/g+IazsJKEn0rg19rDXCMBjZGodVwc9PqW7BKyQqb3bYD2SwNQNhwRKVLLAUzWEx6VbXbT
Q8UyN+9ui5biP5J8bvtCzyew0ILpcNCH2b85Az01EElRm3/sMiBhLZusKarW4Za3jGD3W8usLgMX
cHJDmKu6NkJsV3rLAPzTQYk87QiwkoP9pws0s5YJWxg2BSEhu8wRob2I8pcpFotECAjGxCVipCHK
MDc/EX3R0MdQ7UYFu+g4ccuIIeb3dCY3rNkRJhgIIEKhdnPjIW/s8QEUeRs5V+jEzpDUKULpVOZL
xUA605HVTkEsEfCCqoTEXTzcCvKtxuKSQD6JnJMyRzeh8NuzinJbyX8q/W5nQIfad7BG6s6Ens9H
Iry2oBoP6RkCLjlSQELws0+otDh/kvNATtM7vfEMBkKYwYBhG9N84CvCmcADh27joPoxkibytbny
bZFcuLa0iKp9bbpBfcrXIFEm/hRiCBbUp8UEi08rLXDOH2QWs/P7pK/WDGHcP7QC/xVqBFmnF0j5
Wm94RuOqPRVif8QpGe1R5IidsVnpEJVFBx46/LNfEJZtZrRHXXwIZwXK1otDEUhxXBP2ZO9E0scW
ZeM2R7BlLA8CytFaYn49HnIAm7EjITZgK2RJoes8UcCUvwzSX8g7+17OK+LgY6XTPkEyx8DU3/pl
iy/b3k0nlsktmE7b/SUpHvQnM8iNjqAWKaemnH/r7i26CsFWf1ogTmO3GRn14hj+qFjccUB0cuSu
RBAOlaUGEpmV86LLOEonZeKAq+aZhJvKdSjJn/oZP4ynSgE4co8KRh2nqpGsULDdJ2h04Dk+C9uf
8/BlNZbScE44WOghHycNBcTRya2SYyuWWsQAgR3b1s4MLP4xCs9XPpJ2sxFq2IPa21nmCo0pD5LE
I3ONR7A1IBt9R242c64FlJJHS1hI3VY7M28JgCbKFjQyZJpoDajOstkRzSBYkyOCPDmab55klLI+
Tn/9skbQszCwBaVhAu/6JiTIMOBx+grEmniTzZF72iNdWKkbUokYkgIHuhDXR3lH9rWuh+R59loA
2F6xnA12DbTn/t5LkIYyczZrnzt7VwwtallfHGlQ5MtMuM0MEEcQTZzzuMGMYCkIT2/yDYuFoE0p
ZxgTPKop+dZGLNqqIN7CGomADzge7Z41cQ2xxeLtvsByu1gx+QttyWEYC/zdKx+DD1pjGTQi1PYc
sEUdrtlR1LsRyFdfl/QOAzP4SdcdQW1NUYlb7lC0O6T01v+qsRvI679AAv5Ui0WfJ+MoRvEHqqp+
dkmlLACoJQg+TYrSvoeJ/DUPDCyKCvuzobplnd47Fa41JN97o3AvOL8iYsaDsXXNK7uQlfwA6wNk
4qoNbedPk6aoonVDfYrBWQsz5t5WKetjCxEZ0JvgkNRZDEorEyQ0n7EM89Xo8IhM2hUrL126uwll
xf8xPmnkaHCx9bvoXNJrmenrrbOYokXEOz16X8SJBAbmMbRkWDtQP16zfBltazZpJw3hU3Mj2qmC
yaYPKV2Tj5ro6OYp61+7Gftyjjqmya2R1XXjuMQnTEC/e53QMNw2ai3YUTB4O55XtGkszC9lVKgo
0SELfl508LhW4L2jaOv9vv2vBd91sBNnmRAB7sI+R8rfWeLPpol0L08utrIjicxN9LISdyQnORLt
c2t0+tajvWLC7BwUOVCKGc5WOnl3cx1d+x7CUYAQ0QIPDkmFvgfeGRxxIb23k1Vn64lZJNglwDQ+
o0PK+kAe3QjgH+jlGXYRmBUBTyg8jB5zIJNhBS6/hyblw1eyAiVMeDhQjLN42PIzTdSuQjPG9L5Z
9bjzNk5k23UJf12p9jAAhjlRi6ym5z0+qePURoMLy5Ul3qDyVTugRwNGwPk7pKmSjBLCyhL7OEC1
yQwA19+M8Q2Gmegy1wWfP5qZ0U5mh3/j4Lzwpl9EHwpEFI93NFQP5RNME12qkEJUXFEmt3tBytRO
F41GJKDVFV+ZhLVv6Joz3aSOG6vYwlsEaHNFoubI3CKsoVkDQi0R4G+6/oBix1aP8nlnIHwWWS5D
vKum4LqIx2JBruyuZNiZ2iOnXJXSdVJ1+Dje6VI/SHwRTcJjnTK9oJ2QNj0uZkM/RtMiqJLUqbkW
xyDYkh56EQrADe+lWE2Lg95U3TZeHP7NRxpr4l9iccHCQQ1oRRJjBp7hwdz3hKcjK8TwScTw7ZVs
5bJxa8YVaZv1U4ZC809rMh4TmC+CUAUw2EGNK7I6A7wva/wruUflVvQC3rIicvmvTJGUTOTmorMX
0e96dYffzb3ZPnHBujgB19HIQPrvldJQKPXCUATEnTa9IHNpZaWKylGUtzrPxh4CDgNXNFJ1Rlae
OwQEXMG2/NBu9tdORMn3RLoTE8wyG4zLRSECEQHayd/5QrZ9LrNY+4BQYGkbpLL5YfCy8qmATzqM
r4m0AH37IToXMV2LK1zlLOcroAAUjh6iynotJ6WAAwfQewdWqj/4KeYGdDugtCwiOsCmUyVRJbbw
q6XSBefapEo9zXcxS1vNVuIhzAp7crJTQZFtwegq+A2zs9pBCb6BgfBxXyKSbYCY3iKww0YtHVA4
cZA2omWV2J0krmbVzyQU7Rj2Op+D+2hyIccZAsHDaW1gwlkwOPYmu/lteX5lVKtJ2Un1oHEkZHyY
/C6AcNjmZYL3wBpDi37Dz94UEAeGpJSF/oFfkNhagSiflAXvzc7iqNkdvlHMWWSM4zpi5Gkcg2JZ
sI0UbyiNA8UrZpkOOXJpc3G7FJWtbeGHugNlUrP9nml6ORfDQbhI5dvEW6f+kcAWaO1m1yM4E93J
8aMcGqCPCUZujAjDV7av0S6LqpKOwKSG7asJ6znII49HREr2RX34Uchk2iHTK6P6sTcGPIskIanS
t4dl807bZ/5vncuns798IhedKYWNdL9zOspiQTlWmQvY1zNPnCxnAagra6v9wGTTsnnTN6GcIkDq
mNdgZCq1TJbIgVff1ABEUDPqxc9RoWZFNnDAKVY0zHQXd4d4hzHkvvUAQ6S/TtP/E8VDqPhvBrjC
A37/wEmT3DPaK6yxCb9S1o3xxRFiRlJdvm5VlJ9+SYyLlcFOZopQcmOf/6iE5Hfl9V4+t/krvr8H
oJQXSbRQPJXj00kNzPcy2QSjVskWJ1RK64vqNoaXBeYU8Edcrp6SdmFXY/s0SS+aT7HWHJIynDJ7
uOk84efkmQ6WkW0nQMNeYWztc07suy55Av3VnGpC8a1p5bbBsAW2dkrjEd4t/E0s3rPpNXYG6TM9
qQPHAN2w1tunnlZQMi6lFGVcQKoqWaRx0tB+CPR5GySPKdcDwvG68ySC3TMHEb/y2UcJrJoU94Sm
D97EWplcoJKy0V2lQxI1pFr0sQYHA1G6n2AWU1tRV9lNDRRn1gCJl42O7vOgr2ShnSGseovRwJNs
Kq0pv3P9D4g9vrXaztuuJkziN9nfo1rPqeExX0sBgoR8HoIqd3gfkjWBNsStXvK9Nygp2gAlY3BH
x0dXU8a8jsLWFD3MZzS188aLW8JlkkFzftvZSRUL6iVWPr9D6Y2MTsfiIVHC3nCXxn0ChWs/stme
s/9+lsYoaJWGSfOLnk5RNJHJHDQJtGH/7I/c+K29LMhh0o5eYXP3CsI/gcNhLRlq6oEyKUCYiXKV
Icf0FQJ7Q36jYeq97diJvkEI0v70H+s0NcgZJ7aMTtBl3h4r1rlA6BgPYOoo2Dhdu7v0dZBCzO7J
hQ887UPnZV1v1ejsThZwff3z8gtFjybuzr4H7mpRCfRhnScVZ+X3Cjql0A/naFJhB3z5bDC+mJkt
30F7tRIXoIz8JEx5DMhPvN9f9ivTbOKnJ2Ks2SBVeJIt7HntT/gHw8Wy8+QaLfFKaAlJSGYjHt9D
LY6t8oW8zSj1Q4TYNq4pw+BCozy06g3hZoP9SP/Ejp8OZpzHyMDQoMceqDLJEhsYj6ioE4U26aWD
oP4tdszG6XJAb1wC3+BJmRb00WICF9ReF4B38Sk6LuLAP95KWsWG/Vjp5qOdsafG5Ey4ubIVYcV3
TNG5YC0jIh5NTGhD1ikXHMxwcZJKodd3OAOzbF34qq4H9MeQMNDCSFJnKprmysU2xY5rM+hSbEdv
ZT5QjfsubsS2wEvPkiySky9L4GiWVeBxAtS+PjhF68n7N6+9WqYz1K441HUTTQbuKvV+ZYka8qlb
Z8FUPpK+X1nDy3+BEVK2PduqgthisEEujMcdmSmCGAT8dq48NNgAkvRDGeckCoWcNTI5hUhSWuEE
LLpkXD3k5l3KQwBqjc+9TgxIHBjRjo3Ckx2BUh1CtdBBEPCD9AMwwLctcsK8bGVuv1NLevLQcAoW
IWBdvt15rsHSDTkUdQcYE5Kxam0bRppZ/6l9u3lo+fRy8cGWenSwoSBv8RoPw5AE1H6SBYNijac9
e4xRZzsERZPk+kG97+V3z9zQzK8pJaViV+nKfipw401zrUunQtGs5babOmu9QchSYzuh3BpBAQol
ocViByeH1wvzfwJxHwQDSvNq0NOc82lHzZYzquNEUaDoAiDoh3F2bALa4RwXqMf22kV78f2V4lpU
0hGmVVzyO/ieFGLFaRhkYlsHEhIqpj/mRQgqmwzi3HseUOVLKlOuqqagOxub/jA0fbRWy7khQw55
IThMaiOASBvI+wahhKTLtPHK9EZRVjlUBNAmfo+xgtKimhg+erW6/THh1cqhxnne6noEc3zpBNe9
vXwyLFtlYw9IvsLFjR+gEoftZux9j9sP/NwfO9BU2wkaCt8YKwUZJpuzqYoOWm2+uBkZH2XYg4Lb
uVaQ/qGjH/nNi3UvgRFMj8o0wR1+64i/mgivNGPtsIpOVZKSMWrWjLT6D8yVS38BbFCRYxpqujEX
e9ST9Zo5lP+n8FvS1pwkJoAXeUVhtjw+h5KagrELWillCp7TXQhSqXz5uipxFwHsbhqqsQEpEZd2
JdnDAeNzXVS7H7rlvldFeglDKT81jZTfQ8ee0lEt7x6sZKYsD+FS057b7FvllXrRfDoIdV7LpZ2I
3AB/Yhk9gjZVehl8BkvYLwEqq6rirDJhJ6+T+E0JThHZ9v/Eb6gguu4DrcJaCiAG/l9s/excItg4
iQAPP+80fJyFfolEiaYe5OMJiHRqyHQe/6tcuPLPTE9qA5oz1YfpiY+SZffg9W/8VWnWNGW1XWaU
yMIiGkyJVoqEEHMaRnxFeabx1RdFWCiUGEs2ppTNnl2zSEypd7IlDb7HbkRg1RHcWfirhjAo1yHX
nb7JZcrxoyPFojYExA/g2EUlqHGgXOeOpF2/QHgSULSlkReCt9TxTWEsrHpu/0oUqFyiz3lejY4i
CX8pBhs8RsUpqXe6gQjJ/18nQtqmfOQIIDSbB5dc5AGufrFo52UitiILsCnZ32WsQpwk7mQ1AS68
o9Fp/vmcoJv1PMihLK5uruheXeifI4phyhkfAIT6XMG1vYKQiIhofjJWGpeMozY9Y5KGif0wzCAa
YiFrYq123gPqdfo5+fROFnmBq7vRpjLPgNkt2SAjJM5jUXjrL0DO+j0KQssIgTz0Ux4OstvXbRqs
rPIW59QAGkvfT+yxYdq9qRFotyPcFgXjrGZkt1GFlDHTTiBsK3tNVxUPRLBPMrPTrg2LTBatPg/7
4xYPbQH7kxbnfVTT+B8cjPg7kVJZ1lg+rBQagZP5L4B4RvXWvKMXy2T+b6tzZ4tFrmn3V0wktf8c
ZQ9jjstOcEzxr1AHjXjJXwNDc8yS88A656PA/RjSbQ55McjHo2ccd2MnM622U7TPL4qPNEqy2OBx
Hvmwo6WdgyUbVOQOFm8RAzUyprNd7ZXpOiTiQQL4/j0/cwmrUqyn8ZQteSCe6ZVfebLbo2PpIkrG
sJMovRnlTm6Zl4dubULRxutvdOqDtmVZZlNVl85sx+W5Ss5aLE4PguafJqbfb0U8AItNnpWJrrM9
qns8/HmiT+A02AUyrqUgFbS4zwZGGIdeZNWMrcwp1JRGk9PCcj2WzPH6h6cBH2s/+/8o0aCF1P/O
Z40AagC6KjcSf8zkCrEe79k1iScLWjwCAHm5tTFXjk24XiEN2cUyzYQlfo3SYVeRDoAcLc6O9NsO
bv+2nStvombozFtWCYJCQ4G6okoz41rnPzOblYXg/Z0KHWc7rg/MhcbrIc5bIi8IbdDKfTJOdjU2
TTdRIaDT/StbRr/3PWF56zM2AxJrdWvGlr19KFt/VSOQLjmgUAa+LdErPbt6FWUPs+J33NzPPt9m
4ZBVYF39PnT5YAR8clND5Hf/sbYkFb6oyBQjYxKNOvLBNX3YvH7gzbssMibwBAmSni4Y3yGcACp2
UKwxCz10RX7n1LpEQsszJVhSZnUR3RnvlOlnhabktKOq+0pMlIXCQVHOOr9cRfEff6N0I1U0WXhv
NO92sV4dbvqvEYNuYZPOMvUHOZ31TqMcj1TAPz0BNXP8eUsYGWEOZrE6L9FkUvKFzFzSaxlVvza/
zZzP6aji6ce3sbFCnNd0jQe2x/MBND+RoE2UGWlcVLjyGWz+TtkCFMm6K2PPLwYlyWCO5tDnlhfw
9sCVJ/9WS9wZ9nS2mCndqkxstbs99GJJOnJkfQ1sz8nxzHnV3VDeMZvWjbeUtQyxsfBjw199P7Xd
RYzjQ5pkQ3NKDIGGtNl9MvCyZxNnx6VFm5pXIp8PK+xHLF3CyNZy1purxGnoQHSuMYgmyKWq4SLp
YL+Z6Ayql1nNiMmPT+MxsjaF9Nf8KTzzPWrSzR3DtaOjfM5FuwLhxotjdBQwr/geBO9LeBI2VtqK
83vZ0KIK/rR79qv37NJjzLRhXv+mGnXV9TTuORxQ+djC40JT2ZOVFXsYIEToxvkoOypU/1jTABSz
dtV6BcKsYJDJ6IO3PWeHRoyMxtAL4DmdvH1ch2YPCeS1vFhDVZUxKzz61werpYUU5RJ+D7Rkhhd8
e0ehH3/94LfO5SBk0pudP6JRFc4Hb8Wn5xokZU20Ljx+V68JO7LPIcP6/6XBtQfbgIcZp2Iq4JxM
FCpiQeOim4ugzetX7BMDdiG9rUQLtlwLh7+MH/VHnyRWmVO2fZhP21FV5ERGJkl0QOuFPTLQtPNi
sfuWwJl9pGTTbBSLysHC4Ria9pDSCNetOnLAWx0Cbjn+7qLQPWLsoVVjv9nrpeE31bQuTRCca81Q
hW0sPmrDoNqGZQuefFsWMfWQsyDmbF1NCNSptaXhScaaqvnNpYbpXNTyPzCuiirCojzDQtSnidHj
8bXkoR0IZT1QAj13ZqFM8dQpPWgyqXSV2EgVOSYG7colxLg7b6EOZUNaq8xRUNljGCv5YXUOfcqL
/xSrek9ewYXloZi2bFYlIBMsVkDt5nk4ZLBohXECz+wy7M+QUos4Lv/3RrY+OK9CMjc7RYsag3vc
f49NwxvgYmOZpD+mrl5ZKTvAk/akPPluyOD9YQVtu2VnUgccrA/ZcaxJaTvlUdZxIUlmMZ12Ut9O
9u9tw/HjWsCyrjT2L7SD7RPbi/ukKcVDjzkOsC20C+g2bkV5IAki0PFLGnqXqJKf9yGETPrAYHY5
cABKE8CfLgeGTouZe4iuz9hoBF0wDxj3x90VK7sPdgLdmM+xoXU9geHrm/E1z0wpA8tGBc/HtAeo
JNVhMnhTR2qREDw15HN+dlH4F3erdBHPi1W1pNBZGSNAMk0uaQosvscvzl8WGS0EhPrd9fIaWy0B
bwTrVVxJK4RsV6yC9OaWA1Yzvi6CRhSmOXGObwwnVzSjmmIan2NyVkcLwCHdH0kgY7+wvWpvtDIy
WJJe4uKR8EmcjMfRkRxb5HVrDT/tLTaBY1C7f5Ui0jEV/Z/GOv4SaxhRQSbxT5K9Ex9tbfDbQ4dX
DZuPmbJ7P4ueU8Odt7EArjslPpU8eje8JCOWa3+o+TeKR0mCmBxRaOOEmi4IeGjxThs9c+ULk2ep
9TtHzxLngLSKkALiR6VIc9RGKWF8ViGKdrLqGLXQB4UJ2wpEkPqkYEkPDMVBya5lkrC0PYh9u0l8
UU9CeJRM4aS30yZVDsqom0WUwVmBGBfynfy1jWDDnxdI+ow1ArLKax89z7EWbVLssN13E8ucS5Ra
MCiaTuONrtH/DCm4NC8GUJ3VIEDMumJjSKzbdj2+tRN9SzYoQwVbIznN6QAdg5ftZ/xkNnT3sgVP
uUXIizqxNOF4NgeuyjPakcat6klbH/8BR/gNFnfOjlUqC5VyHE2tEcg6GesXpFqZv8b73oLzp8d0
OGV1Ly17A3OUgTyBIfWwQ8hFvk3sReHEQ2xcYWboLd7qMzSXaY7OlMJ+OHm330DXxMP8Dh6ocVZo
T+ONbX1YqAVpzg1+9/u34x2DktmDifdKDvpL03L+n2ZQuBRjGPNoB2ItMtj/VileY+AwmBlGMTxx
Fgg6qaJFHrLLJ3+gbObQ2DMqkGxdtAGvL/c+3dwLNrYofus8LD+xIUyI2Cp6uznEphTWjTVgToYq
lD+XubtawWeZ9VNxQs5tKvKp/aiLUrWn3Hj7fqK5M/US2B5G8g1sOqUbtbyGlcMytopFrIMqIvbS
AjuJJD2FAQYkHEC09v6LAvROH/hqpRi3/IWaQsdS9z1H9uTHeRz9ICSKO75g1Mq+Au8D9AJereYc
cHEXeiC00Ql5tOA6YMXkprnP6YhY1cMVwevdjIU3P+dWjHBtdE9F4lRV6ntNJj18LT7bKPPR6Vgk
RO+XpjEFxFgEqlpUqaoj9SXlJ9UuJFmwgxnDN3IoDIh+D2BF9wGwgdvI+HYjmL8KF9mL/HWdtwFq
zH9SytDJpAX0rj8YK424SoOv3P7GWCuvPyFbPmLW3Xvs5XrQ4QzovLcmNPVvGpuhVA4HtCirYzek
tb1O49uD8Gm+mo6od56sW4AsjzILFnZN8NhRKdvHWVwUNOsSZ+Uv2VBzLSj0Erfh9gAQl3NDefO6
XgQ5wYMGQjoOVkObuKEj24tcXfpKjiP8ODP0QnOhgfcxO1x02Y7C770FcVKWxmrNpQrRB4Q2TSfI
IAuN/zp+JqtpNN1JHIdPVksUHFuuhgS7rsekNKbW7CpphFmOJQxxQhuPS39gtrgisll/sAyii8/L
tPOcQx5easZfj4N6YOuhyE71uMUg3s8mmjF5Zo8BJ1uAluyBt0C8YySatWLjjcOchHfHnZU4KAsw
F4wdkvO9tc6ePFFgpbszA66aPYXX+RIhoAjiZPBBsX0p182uMuNorJ6kiVj4y4nrj41Bz/mWTuFk
+KMGl1FtKhRK3+9KXbW1NGszrhGJaI720XLf219vzUURld+ndHS46p004SIuP3XcUhYZczUVQC77
58hZGhwG4qxWQ8CDmT6e8jDUqqf2v1zTB/N1fx040OIw+EC1JfekabXdIerScoPKu/N54d2gQkIk
ONe+KE/S/DfXzpA2YdZsEoltM2nHnw93HYmnuscZeg5BwUMMmnavCO0bsF3Uh2Vh82I7xGNuBTeD
Q9j1v6i79dR3RqDCKa0UHTe6pbuvpgse6OKNxkAwwFZR6txp5mhnUDSFop2Es2b2MN9IpgEG8uEx
se9QgQVPWGhRbwpdACKGEaZ6RRFb6mfrGb4xUyckSbLvLUS3vJIGitHDaLeeh2ZL60Ot69C2orNt
C6qNx1/XrjLtEX6y61tR64aEVFrXRw4ESLNq5d2lgZNGGIPLGSNA92rPW4vFVh0u7PPG9E8K7XR3
sib4tqFk8p+cdPDNuXxWDlnvLjnm5AOWIeHqh0+3ftYl0LjZ5RqU1VkZ2A18tXUugF3Xbk1EhKwJ
CctaCvddWF4xHt7Al05PUdmF49JLEOsj99iuW8t9GufgMnBPeg2hWKBAhcGnVrDCQ3KqBmImYS4G
dQmqycRAAr0PWtOxSri13ABDNcPKQWGlv0WGgFkZzIV8yEw/no7HL6RnI4wFiEZkGRUGwddGHna2
MHMYxTCU2rKVgWUpitysiemyebXcgT69XA1af2SsMo3g6a99trJtyXE8w3fPSLhVOo9Eo9V5WW/x
YY4enluMmaWIAH92o43+jyLFoDOBnlXc+K0vpm7Cznh0g7gpo/F7Zwimoe7hO2RZ1xZVnwk8b4pI
tQilg8FdlKSxdudaluu8Gj9tAH8+/aLhJKjQ/cWeuGY+ALMl9mqlItZ0D5weJpRdC8gnD/RVd1ta
zNFxfS2ygACfrDGoJjlWuQIv8pbqW+y1FRLmaownHUy+zWQsDibdF7I/MqzPWp38L+IehduU9yBB
pXa4VWBG53uS3UE25xjF9513Mp/oQybQHO4E9DJxyMXjqjKpqsQm3DGKbBamgjXVbSAtFM4zBLKQ
4OdvoEKC3Rm6nUSOTK7OoNoQcN54T0VwfYvksaaVZcrI0kZ2S4Xu7wzV5pmayVW78XJIj6eu2kE6
9xdlWlpGujmmA9PV8fuZ7r2RBlOPsQk73uZ+g7bp09F++n0nYfNzxXtakb8So5z2TF91uvr5r9iW
qb+lvHRIPAQAdM1+opOJn9SQ5JejnDcqW4eSaDmKzHIodCMS7jt0TTelig+KFiJjcPzfaUJwqfAj
n8gqAAMHUoAoiuTMYE8m8fsskwassIzC9sNGAAT1J8HOjQTDzmlD4WYiznzRKi0n2PC2nLRUYtYg
ra5dQDs4beaNLPXkCkMSzga5zY4Mc/AVLRoifDlkAA/G+TzYZ3D/MqFU/XIOLov6NKuVP8cTizsI
MDNhnsCjo0CEJNqa7GSxzlKa9wOec/Su1W07Ia/Ps61pkKNSILlRakHygQWEI3ffN3fQbZBTtmOQ
rWbE3C51Rt5ZcNTFx5DLmfIx6/bY+1IPHP2gYtNMprB4nHx4ZiS9ZzdNfLZit3bbRUFX2lD0cHRT
eXU7U/ILj0uekTGn5bVSuqufrYBVG0CTKSPQEvAYAJHsMr9fV5+XGW6ap0fHvx8UgVYAQt1csf6U
rLrmKi6iaDHYtuOVKZ+UV34Wk07je+P7IgbgFevZ08BmcSfejqJbFbY9VB90Yi9uwdaX+cCWTDxs
0nRMzR1mD3O2nc5j/JyTtwGfbN+WNF0UAfQns0t36B/xz7pgDb3W3euxQJTcB2XDl75FgR1xEAyJ
tLofuXWZWDPtQ8Lou+fgzYWQibJFq+bszVg9U7EP/Va72V/L2398vwxCL0kiHzgnaYptUEsR1BIE
VY5OPghT3LiTBblJ0cC+X5/FNEH664ibeijOTdWOwaZ5aT910hHUTFpwWrH0imhr0PC50ftLYznN
LcPdjhpn4ewHCpKWukDFQF4LSgT94H9M/5oPB7RAIJoiM/bZSvj9wavOmbvBnqZx4DM2gEqoKbGX
R2R0GlM0HeP4M00AM4CAybgl2fOJe8S5ul5g+D940dVWRe5mGeNLCOcPZjU25dNbDnXubLzcq5xF
fKCc3sFPIBjSiKNJ0tUVqv6Wy5MLKeEEfXVo1zQNRbxb4n5/k1kYLE+HYP2Jby0lK36/af/U6E05
ifidctaneZANRBTKtqc1RSXuv92IqJ1lZMyNOiqyygGHxzl6ZogwRTGvPxH+K/22oQgsje8Ya5mb
xUM+44vpUjQLZqHIq+vixn5/XBbl8986bS/C6a4eeiin5g7jaN1V8SBfCNPnZ/TzOJT+iaXwF/ki
TuqsNMGTEksxwtHAUme8oRF3d8kIzsBF3DxYMVSgkGek+e0Fh+40u7JiDOUYz52uN6ZANlse+X2K
UZyUmGrldPmigvhkBtpvlSD/+1iKgllvdDLusWpzqS6vDmMzqkSYflgdHsBdGo/pJrVPK4Pj12Qa
K03WMjfkqq4XVYU7A7BzdDrTXFFEKqRLKvYRigUxDkL/XxvaYl/thyYh4xxymuAERD1R68ftZBLu
GCKk2a3yJYxyv0s4XeVX7nOL9x1fJnU2p9/DIzC9JP14Tr1IzxnYNwqcAkEYcHAdejt3kZ0gKeQT
3jczLykIV0H25+BaKRKGHYQ5PlwDhYJgEnHyaHU62T7c9HoON3qQEdk45QCwzwF3XTw2A0x+uYZ5
Q8CvSfg5cnex9bd0QszXuXeFl7FAMRAkAT8FBP3qS0mrb62JmLN3ySi9tYBcin3pnubANjBFFlSc
axeP0aEKQpUEHHkExY/fHS0xaVCM1od5bPaIHQVO6OSPCDISpQ6s0+chppu5eubbp/OEBXAlMj/c
O2uSNyzOHkJYho2uKuVxCMI3jJ4pcGzhXwBAcGUP7K7fJ3otxNK3VgnY1vtWW8GbcUg1AP8GrGei
9zQ2US40eZQrejx4g5297lkuBo5LFRf4D6X1MtLSBICSfQHd+47EhDm0TMEDIVrrjDdwNDfYPZba
DR1sxBnhmUCxdFAgjx4tUApt9LC10nVHZjiSab8YyvtctAzjKIiyxJfIaE4iPugv1J8xk6rcvfBf
sR4IWiTEwAiLLfzp8J/5ugjX/ZPCEZ/Avo/5TgM7B4s9LCBHI/c3yh732YSw/YIOxXINd4t7Bhzp
9L6zN/qQpsQS39YmAJsqqhIPqOPO2FtzOV/UFCJF41W7SNXrN149pQnXy2vvchPqjOHmcTgeIyiG
czNA2CVF8rLhbKRRmbD2DWwPxPutnZZSXj3A4kvn4XeQFAIoI2sg1FxuUBO4d0zCN8HVPv1Rkqbs
PjmXKeKNfOnCHIhDKdrrTXrRWjC9KNopv7YnuacIWS0xPe0dodRwCFe9gDhr09Nfag0FobEdGKPs
eplEFQlxA/D85g3Y3OXARjRC+RdtZ9/7aM+swl7bAZ0f1fA7tgym0ASZyzM6XpK2Ma6db4sRw+Qn
T4uWwts3Zws+JLWXdCJdAFiYbzVE0lgHI/0LE4JNOhTR5j/nW2tEW0XWDIXVaJZ9AG/9AO43Z9O5
JugGM19M4+2UfMmcnZXMdhWrlnfFls2Rb7qTRMDTzmKOc0Meg6ZLwJeUDLH17SOJVI+rcZ/4CzJ3
AvSj+7DQNLOoLRTrsd2vUMs5ksuS0FGFszQjmet01sjIchwOYUX5ec5twDnhGDO8lwcQuyRbzqHp
mhiNlzCJ3p0vsPxxbkM5w1HksUxibpJiDy+lqhVMF4p9cRIrimNKSnS5g0Kii1aE886hao4LiFyU
C8UPbw3UZ8HC7XbzrSKN1hF7L1wOz8390yJYNmlffr4TjjjTmMQXPKgBN2wb2z2BtV6u1lLZ/OAC
cxNxhoJEJtEISh3iSBeByT19qrXwLd2/7JqYn4OWoa9zdr4Z0zq6WlWxQySo5TPByt9foL88AUzO
bIoeLZ1R9ugI4D7MFMmv7syxGhZt++aXB+7YXdv1Zz8Th8d/T8ngI7hdB7hll1QUfFtH1aypYQ78
sVA+MFe3H6rYQdvC5RZo2tXcRdQ3F3QO8SMusOR9z98aw69+cC/X+mOYaXVHS1bLVP5xfp9kLNQ7
uR7usWKyzwEvyd6QpmZgAdDt2f5rQr2ziGmj80dWmm26MOVYGErGjcjqN9JgvbsX166jjP1fzPs7
Qk8f/El2YqhE8s4jX1pVN3/a8oLWRJbh422Pm4SO8AkgZFoRfRgJVZ8yMvtfXfL13aAF6R1yhpZv
FQHIRtiuO2iMXXW5PKm01/zJkfro67NENqkBwya8Mzk4HnG5R+Zw1sVn0h/Y9AjZBAivOTNIfjhN
GTMGBlxAb7eVKhea1Hco3eE+4Ff2zcWDDYb76J0TtAe2dc45WhZ0qt5Nquf7u6cUEyY0vBY/oUZO
IEeQMA7jRqXiHa3wxE2Fj7QwwbQUDD248XzmUIhXm4BUOUmdzTmKTCHs78WjbffCnsCBelIZlu+s
kMKcyvrfwfkcR7SpwPVIL7lfxCnzYZ7dKYxBZpkTrIWzd5e4Qqp/Dt0neLy7l1dB/3U6tXcDy4NP
V7VAShn49TyQ1L2ry+myOirHOhG8LbxjeqTe2AsBMFw3XMsKlaHBYKXnnUEGgU5kjt90uJd8VZRi
Rv1mLYuIASCsEt5QFPFv/fxIsCY4t7fK6n8bhh3N0KGkQgSzp8sSXNtJpdNAdz5afodoYp05mk1C
x/UVy2xMYj8RBNceriqmYsEe+4rsBc3j8XLDCGG93qG7FLmu6wJ+xB4GtnksgBtNE4eEcRmv2ako
rPJfFYFlRKXJszfEfen8o4CB4vc9VaHE2G3DWwSmcaXgQrZLUv8RcGMvRi3OO8DJyiTQvNxshmzd
lcUd0TBvtIdhf1pLkOWWzcukwqycSGNOfMvuT6nieiD9HCmhZh6axQcevI9qXU81RNAR2SEX1QqI
DxVdJtsnvs+F38OclGDRt4N+MQ6AhFXrUol6PGya6BtSYFb2OplxYVl0yUTPJexeTMdHJ8Y7Mgj/
bClzV4z73fBOLJkJaHP+TZI5WzcphzGeL2x+8CdZjzK96wk/YFL6UqXuu6KpMfr3XgJT+KyLMiy+
aFcVKf3ji/jdGddx8ny6l5Vi9b+Nm0ZhHlHfnspWt7duUnKJNUdwXWFWReiLk+U3qTY/4qrfqRqr
PZUqtKzavurT60bOO1yhwgv4Lb+h2ZMS4SjnTLXrTcwk4be9/Nn+CJhm25F3drihA2pGxzDILU8k
wu2ZM2q75tBoh0GEP6d1JEq2jTW4Wd9/Fx+tUsZCXtTwP08kaRKdeEqYBQuICqf0IYBbWhR4PJ4D
5WvwSWxaO60dv98Qm3FYa+pqG1w6kdmAphvV9BiLWNUGsZgo1caIz1AZ1uIYoIhKJsBrZjJXyb96
2Z7tAF5x+/nMIlmumeJDpF2JQf2pvvwABhDQTNe+U3U5Jy6hjNBBMIW4W+EVukDeY3YuIju7lmzD
NGe1KliREDvhSMtnfiISQdemzuVwHil9BMECmiSzgvuBUy+U+1fii1oEa+Jd/9GQ8H5E9EfhQZn2
LtgACViVMGXG2u4EiW8UctP0AffG9/0dNl+2p2v2O/2m0wGtmNAPRhLH0q5vakv2KbqZJ6ezZnog
1FJ4AbLkeOoCfAuEJuHeCzyGKEjnocH5v7FCkXXiRr96fNwp60Rd5PRSIyID/YyqaI73oR7YzPf8
ctonyTecEAzpcNuAsZ96ij+xWDnzsPwKoe9s9jdg23p/26KT6CPWa1nBbOl902AhuJIiuC8QaRU9
LyC2uaoGKSHFSRayH385a6/kMYKjBVVsiaBS/Q1ulUMM+1QNRUpBdlhYpbEVynE846FK6vbPMvdS
NG1EOrpJ+jq6ERQ0RMjiOmIINwQbxjv/YXIOlu+tTiEkR6WHayiAZfM1EHbWdzNhGCxkFU3luas1
F7MI1YK0KgSNnvG8b7IZX6Yo3Lhdv48rL6uqpykZh5Z7qDPphLOOQ4/f54LH6frGhZfre1G0BTVa
lxidSad+edLc3U/RdUoNJNNX79g2ejfhNwdL+GXUfz/sL7crhr8PeNP2cmyZ8aZOaVR39BgyA3/i
Y8Cfeee/2QquX6QLlItPB6u9/b0YpQ2GtT1p7kK/tor7kX48BDlpeKHGPG2jTq1lmhJSrQjJ+7jN
4laNcZpjZOYqGX+iUcupktFop7i2QeBY6lECWx8+TvGN+yzNbuQq4CkHHrxrfK/W6FK1LcxbW/yN
H5r3niYvey3ZMc63IpvC1B4kxyKpAJfgiU1Opz2V/SO5xDqScnYo1INvtatLkeCRJFZza1KRfrga
dLdFocUxPhFK8nWvgUWP2pzHCJ9FHU1JH+YyOgALEi8rQ28vAOMwoWgO3inzpVZG+SLp3KjgyiVc
Ts9xfKoOLp8Lksr6h25neE7uXXftg0maYWqGSRLCwYOgCqo6YlLlGAU1y2mjLN+F8rThf5u9tJSf
x5QXvT8T4hK4Xer19w2wHzuj9jDqZXp7JuPWJ0xUbdCG8RacHpCceLhcVrNIRfRGhdZSEucdk6J0
sWVvp0tpBEppuBxzw7YC4HpD7jTX6baRHm708wSFSQxfSGQqn0gr60/7VhevTImBdJAcPDEchZZJ
HNYBzgbtefaPgv9IMs5hT3SX3rdWbt8UxNS0CQs2QrrDo+2n+Ve+CFEJylI5A05wZG54MWg0Ux/Q
PKLhJ11rKo1lku01dp7sMed0s+Y1mCsSs5QBddDwamahv7SmmGike4dyRhT99x6sFJCbyMtGZLFK
cArAND9PPIH5Z0/ayvJntmWQveXk+Qr++Zaa6Hdk925MmDKlEPaNLeLOOR8Y4OMa0VmFsyJ7LTSG
YezX8VVMd8FNQBfKdcgBsfdGdTPeTTx3+UWjswLRq/qHhc903FMBR384NxBVitV6qYOLGYh0NrSR
YDJ1XYhXVp2e/UkwWNHjj6EW2lGm+SVUpLfLWdtUZQkeCJpSJfgSe/IeFstovAFyeCISs2DOJI8u
AHWXOX33tR4XYDdhvOrgUyD1lNyUbZ75tFD/DdcoZNri+kxR7JSG2YoEt3RN+3K9pVOL21POLzN7
bWXlmWG5zVbY8jSdj7c8ky0h0BR5BCfpxHhjT7UmxKfVOD13PjEdMlnOIE4cx4SICW0stRyYMc5H
YkjgMAg7WON8W1mCrmwC0YEJBOWFPwmzEtFMgmd+Sq534+6AX8DyHOcbz2/wNOu4YsBgrKdglE9h
f9hKMnM/ABd50wWFjbuLeDEOmfAqAfU85BjpcsRpKquOgQkSLJKDLFTmYwB4DJOU7aFkHRq2M1bZ
SSHY39NjwJkwiYhM3azA/qT2qjbgzDugd5NnVCp784NyGXdj98hKjfQXQ9KcLA/KQh8sCxNjfRBt
dvpi6jUpEFsBd2X7pX/LklzlA1kkxb2Wo5BFzlnE4/pGkI1BhtmZ1EzDsB7Ous90vPAqxms/Thqw
nxRM0698h+OgSv6p6RSkIYhk/7RxqB2o6tN5hhME7cBv8abAHjikCfYBgkWac41s2Cy1NS+zV58V
DVEh3YAI0W2sU124TIm+Al7JXOgXRKOWlzS7i5ZpSrarttOP1aMwirAV0Xaq+M2HSZi6PANUwMuJ
H3FPAczRjER3riB/tT0wY1qkDZt9EHrDjmKs0GBJRbCfzLSKgUMwxrvWagHwVq0uyUOUOqYMlEQj
QAi+Xp4VFmJ3zmR2p8CH+QhSOGOgoXH0tU7wz69obZPHPdX6P9CXXyZv0ZxtRJ2of4XO+eo6dhop
Ms2NX1h3Tg/ACnBSjAIaToXjc9XbDwYhBnaRYbMEQyzoCFZ3ITZ+VFZ/h9kgJ4JQLCgpgxc9MwlI
Tz5QdcmtO61xSdCkWHDYt9EkU7ixqPU7gMAxBLfgAxGLw+D2XmW9rztV6dG0daCKEz0Xp0ou/kvN
QZj90cetK78Ubb2Uy4KI2gBvWuUrrk/mzYpEXd+f2wskqt9iI0dVGlgAiIaZrwiEtMx0UeYtec5E
It6f84r2h9PbnjfAunkUGOC8FwGTmfUZ/Ejo5Oin4F5VNrlb3QrH3f+GN4f8z+DMJJcbfyxOAmx0
2glbgJ0LFwEyrnE6w7Biej//DuFfnVaTtZgSCiOINy+gil7Udj4pqvLHLAZKWkr/qduLSrU7mhxK
iM+CbQX5yr9Q713660UvaKp6xh4RtSqhdDHFpPrpy0okOdkXGpTdWmNOWo+kO0EGcb9+jOrBk5Za
XULGEnMovK0nkq2lZuib8IOsL17OBn3Idiy0t2cW28/6lcldqW18QinFRzokq7YCoIa7cr4zN5FU
ws4LvTUcqaxNcIWiCTnYbMQQZSz8OBQ+jeMHhn9D9GsqdX+13pSyVUZ/7qOmMOqpDKpCxzXP2e95
TkbGyUxcDZRFM4TLi70L5Jrp57+woFSsIaytrUxvc9S0fOE8qfwyGdt1BuUG8xjZ2gM4TDMmiFcg
nPR+2EgmA30UOQlvNZo8s5sTYDOKGkitPErYwltpt6LSzse3gAkWpEU2UDg1ZR8Ok7uMZ4SWkkbb
hli8HVAR/4is1Ll9CJnKT69ziwSg9Vd1VuXAVVxVC2Is+agp8EwMMrKxtBv1Ui+zwBDiBq8UF/Aj
PhpanN9HuowbfR1RuX2U9e393vqKLPQOwr+k+jWK0QpJSHmXFZnhPaYYFyHOGpf8VALm/gqkxNiT
Hs3G16jwsmCvInGx2b+voZNE0FWN2hWBd85wxDZizpcWflk3CQ1NNlAjnZAZm8rCZ70T94rHbLXM
/UBK2r20sfbYcMbzkaImsEs02s4LukWwaj2+WlBwkXCMd0IolhYdH20Iqsw+dHwbed2snGHYQ+tz
5dE3jBD4Bor/3rFTAhzBfYr7W/WzwEE4Vp/L2vznrLdhDtLTYD4GaycDcDtCy9UnNwoaZb6d3s9H
vmIvSj0edPJczl7vsfq0rYG5dVYzEuSkpOW4bwOkVyMLbL4m4ydbs+QKRySsZwlBTLDzVqUOaLh9
sB1nlUzDrb0CE6JA/hGf29GJz94NTaqZXtrpu+IAemnoiCcy9+N1RvY1Wd1N9rvItCLRztCqM+dP
JoTu3smr8dyk7PDiMaXaY3aP7nN5rWH3Xl0EqjTYQ/PaoqPmmmGjkn/lbgT8gdODhBvzg5dcuql1
VZGhoFSHYMmRfcNnHFWJDlhUSh5uiXa6uKqM9F5K/W1uiz+yxLE9LF0ugZnC53jUT7kCBM3IIIPt
PRgj1wkH5MIrP0MhFjxTXyG0GWl1OQYW1rb7Y1+ALNoTTjWMxZtU+cumj/iknsu9Iv6lYvBJZedK
QGy9+8e4Y5TWft3nNKW0MtmqNhKapPzIhnznXVB474oWytqMiuhtTeIg44U+55lj3H3aHyUh3TMn
IT/ARKbZA6kA2CPxv72dHx+Ua0qa1jba3ewxE4NKU0hbuG/lXZuNsDdBVTiNmmr+bOWnRaC019Sg
CPr31F+M1Vsx8D5RLHeHv1nDqf3j95VEL38REvysY25Xi7yRjyWn2u4cl8n5D8wmp/H67mP5Aqs9
mMQ23CoqQrnHk89ziLNdMLQf3tYytWohJWZTib5+Khtf+Drp3SMsSxhqxPB8de+qPcgmOmC6ss4g
bxBppFr8C6EmK0ayud+VAK6HPH+KLuXZLZKgu3tIQyPeUVekW3jVqxhUAdn994s1Zxn87brsUImc
t0Abf92O7qJetLEo/PvlYE/8Rm7/krfsqIL3Y1dMgcDV6c50T0e5boiKjzyojdSsHCyOuNXN24/6
h/u2Iz1HDTV4QMh+xtZRW4AMhOz/iMBjkkPDoB4LnN0gQJXhlPFxzCG7ywnxCwfs5NbFSIQV0I2N
40ebSi8z5FdxivY+GQZ0o8yNmfKwwzFinH/CZcx09lV7Zs0FgLjQQOdoOzqs7cLlgg4mq4laYr0k
WStvH8qKE0qe5HVUnTiHD13fJYRyXe/4T7kXA+d7qqDGjL+W7ByLxDTp5dbhMTkWWAnjDs0e9zay
nJX85D4rXgZJIUYC5fLVuGlt4sEPlKUmzW17IrUsK8pW5sp37oTTGoplXVB4/xjv5YI2LtptGnNd
zxs7xP4o9Gr6XOdxvJoLQaOTshK106MfzBHCIsY8QQrfKzdMOsQBbkhFIglleChIpVjkRlRUuJsZ
BAbWo8qXU2V2j4GKL/wWL9B1Ot5GK20suJxyA4CX50beIRC7EW/yZGis9OHYe0KFg1XPFCAEqamc
njoJ2M2WfoCtgplu9YrhvTttKnnxJi3kEtpHKB7fYbIbMPYxtF41Ztgs5DBqsVgX8cxApTfFUvsi
LbcUC3ntfOz7Qbb5vsaPWGehgnbxr1VLvdi+gP85oLNn3lBmBK7hqvuel8owWbPMYMCIwHOdUlNx
FMN1alw5F0eS411HeHPSZkDZdiCIkRhvBR3X44vzG4dU3RV3tpqhx//Sr8CACN5uoCuwlWM/YcII
KEnWzulsfaEzTOek8g/ptv9CdolMkG7DEpbqWC4Lwn1q7nLDL03feQDP7uYpWLMSuMSFTfYWBDVO
hk8wpJ35eLKabEAB6v1/aCMHpDrk1sg7bkM4bKRp06iKoopMDpKHK4tv16sVMnZUob+xKYRPTVw4
mHVIje3+w8NvCFYKO1JvMDMFswy4PO8mq8SIRnXpDAyXeseJjvNrAmu6BKTqpG5MnOS7PqdYHd78
Wa5Ac7b68L9ioLYmqaaEgSnY4H8G1QFT4LDsaxOE7N3Dhj1RojDBWtR5uYmpNOEfYU3g2F0NPeiV
XmMmX0bsiA2sQXdlj/DcZVGxUPenMm72pkcL22tdXr/1U5oRpbP0nlIIvhktErBNHKdHCkFOGjmB
ly8GgSFPVsEzCx2rHpZhFjUIfxfno48d1xHMypw2mQdZ60uLoOuICGBrVH3JOpND2BfrjMZ9RXvM
tZ2qFa33ta3vgjHEqZQjCwm0OuXWWoYK8olkhK6VvAeGGK+lAddrmmhHBYYLO+86bj/I7UYPmte5
njISRLGvzVsWo70p4wz/oa0wbW1vCzDKhLdedgiQRcvPVK8sHN+CKOzZ+44LjbS4JGwK8ZhRVnl1
1eYJY7W2cq1drggM1Sj2IzqbsLQMW0ueYPTUAu8CTYe11gWJgX6tx8ym9IQAzfraPgauTQb9X19m
ASuWMrsct/gpbKR2BOfGN18qwKD47W6aHAOgr0BkUzpAEkPH3GuOsW9jWJCWjuJCwYxV2CNfsoox
44QKyFfHyAh3F3KOo64DKpgPGgWDQ/rmSPJorENGOxqEq00ruH5vD0hJbQ8pLeNmp4yX6KOhztnj
ZCtKrOdHnf0vDQXKteyA2usaIMZOf1ZD5dMl5I3s+fRGkCASmE0KtNy9BZzRR4FCmIgammlJVKtN
K7BIBfr6DhKIOo+DcNR5jnzqcyGd+FTSIX2AYXtsfygKnsq3itfK1QnVYv+4Ql/ZsaaiO7iqlXYF
ohGr+sHYjhgntcguzQO2Q7lauFn/OG0whrj2Y0P4ZwnHtp3+cr6gN0Luli9tUocP3pVzk578WwAo
GGpHNUUDJCXdSKSP0rl8TjpODPamIHzYebm3gkNbbW5z1fwqXhzCjVv2SbG2msgslE/0c9UI5xo3
7KVd1qT4IJGBNQIFdzkoOSl/WWJfJwVJ5csTUFi8VORoWx8GK957mS3zai1ag/BLI5V8uy3Km3Nd
oYbwNvoOoocsKbqbXuVeYBWVkNSbne5ktWknuL7b05U0s3zxYjPLcFAEFr30QJTyGLGPUlrAZZ6U
0Y9gWPeQQUn2PLbN1IfCH/jBq2Xbj3KNx7Az8TPJgXnkydgODWlPGgBVESwFkuyOJ57XKevIwUNi
HwzY7BairjBsbyFteEfdqBSOg+XAwcMOmmvSurIbtlYbg9/fO3sSivpvrqeUH/mSqql6ALwmerbs
APwzrVUCPhChidutqbK1MTKolC2bil2neHx840M3b5zxtZUMyawdeqcjwDOdVFUJwTRi/gulLx9x
0nLwlpMIGPHF8VpSzWaTk8wapAsOPWsoD5H34EvhEZ/Z5oVHwqsDr37W7UOCv5EMOQWKgfpXCn1x
qoWbHRAE6g7iL+RAFiW2pIhQ5GdSIhFMDgl7o7Z4VX2SNTereSE2+0iESujTwFBxEt3AykqjuZG7
P57aaI8a4tvk0bm+7q+ggh6dS+4EuU+LObWSyyOvVSTo03ing/YEeNaKxU8sDCZcPGES5jFyuXda
1MsOUiFmOWlx455Whx3xZ5YBc9oDIx/Yp6S1OK+vrrXrvS2WpTAJt+hXmO+EGh+rrzqVtJDvrlh+
C5+cEsGnU2xwIF6s4I+k+LKuoZ5Gp42EGDS7dcwOeylkgQUz/A3JNhc4GyWLt5b2+UhDgLPVEex3
DMwAtTRuWP4WmVluACTEb8B6PVdLyRRUW3t2e0au58AB3Xg4ycObs5P149UlGN4rGLXfyNaR8XGm
hs7CLS6bnrw03Hi6nM23DHuRJq2/OMrfxVFx1D9pE3B+3hFr758QqdPxMQ0DnB7EmMyVh1kXwURm
99yyQK9BmIdQ76khp3Q8oZhPOICPoUjJQCFIY8Q06n00BRyR/IzxP5JmiuRHQmoKIX0i4XbFM5AG
BTjvxpCx06A8OHtTfV3PqJ2EFfb/4RlcmdKDbL46Gqpb3EXek2VokLpie9y8tmeKVQXi5Br7giIw
jExI1cM+I+aqmxjcCMlaUJ/7X3VTqE3tpLxRLi9Uc4fY3s7fbkgHAPMN+qR0FSL2b+6xMdbOO+8A
kvMST2h1PRviPDedtV65/PsAJWcYI4U1h77PfTQmqLTguAKBUYeZ1qP7Se2y08sIfa8MEcT0F7Ij
GW59FkkueboarXZa+36FjVZ9qPD1Oo7//oNlP6WHRRXm4ddwijlfCeaP1IrJ83UfmIlsnNVk5/Fh
IRkWYgYSW1AN4U0AO3OaUuxu+9CK+oNwE1YBRoC+SbHyaT7geafdv8R43sFpZhSwPa0vwMbSyxQs
V5qJYCr3O3Y68+3OvvWiD0OP8dy7EPQPPmIoob1IEjXI93XlD2NDU9oAJo4t+c+LeqO9P61+Hdyx
/sdSEcyDtrw4ytN7UEAFI9oyyHRcg5nGsdbEjqHNsOU9FuT1Y5RjIg1mtnCxz46jLlGsWCQqvOX1
tRxpzeUb3xBHu5+sXP7+J6GeuPjTeGq+dJ3Ev9bkvtUc9G2Mv7HVaL7zV23cqzRXnFTXefEOa7BT
FEY8PmVhz5iVmUA8r2/k9gjXGFZrntCUCthWBeYTondfbCqTv0MJ2SFVVk0DWbEw2kqv0YekTxyk
Glen/y0fTl8KwgkKWzi1ziNJo67h2BWwYoSulroMN3F3aqgtd2lrACN5G7UJKs1B88nhyjLC3ldw
KS5j9YeerUmglCXOwTwlLzggVDE0Lp5DGoy7YnfDBbjH1MUTyjOvNEcXc7SN6HxTR0MHGkMpGY9m
EqCMFBNc3iQXwwLf0Zkq30L6VOO9KRvjrE6wz1rVuvCLuN4n4YXqQi215xdCZ/LxS8pHygVVGijx
S7pRuHupXLW9xmbODGMBlOYmRToXqe0ncKjnhnAVytaaY8Ca6Qwf/0O+Dd2ySrrZ+57kLPp9uvHb
PTXmBzUlKKeYQ1INOiOHm2fiZ5ZQ7OFbwdDAZlsN2oU8P/QIjvwfws2yrQtoyDr5KSOtt1UX95c0
RH8tdeH7D3GHPj/my5L79DJwBQhJ3VsXhj8KUINdRbVVojnd9d5TIC/ozqJ59+oh6AJ4fDL8QRCs
NV4TZTaIRZkxpfV2yfOoxSeAIKT2glp63Rc3kCIku/0kAFzHP6DVxYYiQsk40ojF2R7AiWrkXDVh
v+x+qik4MovktVFcn4Ggq/RaKq/xlHw6auS9UYcdYWowl0XwD77J4Sa4sSdnE0IY37vKdvWd1PjG
awrKg/xofTKgAuUidN94x/Y1V2e0jtymlg4TOXr0JUh+alJaPv8yQFxuVWvkaUPl9j7eQnJyL3wz
X365SULIB3Fipmv1UoG9KQr2K82faxBnn3BWrd0G9h4rd5m+TCvxpYWelCKZONA/UVLhggH4noiH
5h5LCuX/lV4eFJnyvY9TGFP5YFXhYWF/GEccv4HHRBqVNpECoSSltoIr/oYNnpKNt8tl5rEZ9Q5B
oZrJaNcCoD3W4tkgZOAmCx71bke43fajTKlyGkBBH7TpXdPxaayPBUEeSO0DNXDDnc9ofjsc+tTf
Ipmpt2FA9RiBFCbqJkg9hIdMrb6oW8bOIYhctCI5K/S8WsI1wwRUdEJX/7yRjNqGCoCWR1Y3P9g1
8cbgK+XSeeOFhASpLWY+hTCkw68Squ1um0kciG/MGD0oiiSFDD37WyyO1mBRrh1SO3CdJ+S+7qYi
oWaCrKdDLwQcBxeteeXwwbo8B6ZOfa4Gx+HSJU0QSRqv7Xs1pz8LwV1ZdefEp4p7zELVPWlieZKL
5Uq38s3k6qbfPU0yBprOUJwOsn1s4SK9cqksHBp050rmbTpNlhAmq/rqotn5yC810EFn9wrF7HWl
J4HSC3M0DeReAH+RgPFcOAvRRcE0sz9MY+FBA0DiS+Sqjh3DomJGDPSRfICI84ZM2wbmi9MRfdgi
5tZTUZijGV6iCLB9vXQC6/TMGRMODFrGcgdNN62eBS+RbZ/pmRsLtsp+iYtKpVbbXe7nZHK1n1ZJ
uGRPdqRwNsMlIN3YGqWtRADT0quo5rw+hUmOfjvpHPUlgDrGGnARY4rzSCIckoqfj+pq4tB0xwtC
HPgPw4AGHshOCT9doOJoSGeTkkNiTsrwXEMM+7p3XGZlSMvOPHdCuHrFOOm8/uL2vP1fxoFrBAIM
86XN92wZlcKFYKCdZbM9jPyDX+PZ//GDo/98SVFY7sicBTFlS1ArXFLsaoaZKGQojAKaBW+bg1sZ
aOWZ+ShLIT/7M1Kx02wUDBt/Rk7seFsKIWfTlfXJq7VO5VeJ+CxM/5Z8s+KIqNGwMXFK0NUIBDyh
sbUkM/Mu2+XdKQdM5KW1AAvSXXhww9Z0or05xrEjV++PYp2YW0qzIXAArXtjY9xt7uwxi2xpfJsA
DHGt+zI6/4Uv/iPLCabtdcsvRYSufx/x3TxFpMWmbm8aBnrlU0Vjry0HdMuSWT1Zx8Xj1qp5Gyw7
woY+8ckz3/Tth+d8H5ZPyaAnt5ng1us7CezAYJSC/5fRkTxMGElrD1w0NBnygIwDOdFeENPMECZN
GvuwTm+ngiBVNO/gUB6U8LnrJzu+nmGPQmj7HbPAr8k1u4PyEOGFo7LrYJQGW/fLSl7GDV26bA/r
cgpaseQRjHzB42BK/FCEiebeJkUPG9jpI6EGmtroJ14CUVAl5q3i32YcJ7XpH/HdqlnFc4EfEcZN
8xbtLTYsLUrvSuFTMNCeBMVb2/jjjA1kInCXP3deAtKm3C/v0ztFyDVRWh02kICfXFhFwDRj8qNo
s8rUgpLINRjBb0HUY6GtDtpH0FuQ8bPSyPlEhZEHqrTfUNI+M7HAzJ3+KBnK6CddzYf5VayaHKtk
UWPX8662aIEKRRctcdOXHhTwSD+ihTNOh/7/V9RKcaPe9ez7cVdlq+CwonhHTQ1IzrsCFjpv7Xz9
Zx4cy/iYQMS59ZuuriBb6emfd4bFkPN/d1qANOoL52yItYQzq0kukuW96jpMFPtdV1mXuw9p8OOA
TqN7kFR0SLscRVrOdL4SjFhvpEQA02qrw4epctNHY+YDbVSdwoc1qq1WaqVdfTa5ge7ciRbQVGUU
AQox6E8ypBYuaEfQ/Z0EX8xEtSPPLkJkxXcGMntHG+YU6ZkWhFbPU0/Jf+TRBFqrXNWYpWBk2pwO
yK7mD8JTzDjH7h9owldzHeDmH3WRHLUoVedbAccSalGS7lU0IUju42MaVO6zSHkBqcXsz8XP5FJC
IfXHBCr6QzkVTRfGfCu8qp81OhknVy9cqz88lf+gk3SGkvhuJNKGNz0mm32lXrbkNf8VNJlErgSF
HXMjL5apdIoYL4SzJQUbzwIOfaN8ZtrvHphh1DcQXVfawXfA9ZhQgNXqwF9xUPMdfRDE42hntsmy
W7EFtyhv1B1sgr7lj+2myT0mSkv9pdryNa5RVeOAARPW52jb4yOoL41cu3w66LUGfa/6WkL7GAYz
mIJ7ki+YbtZgR2zYRZN93kbdpjTBfV5VPtuiQkbR7EowubrKW5Uisq6pciCTrwXNBvy/RLfjISfM
bdzaRbm3bk7yEioeGUVWXCho9A04nADF+C1Cv9iUMaBHlOvwbX1jCIFJhdNv8tEFntb6ocA9Bv9C
FBjEWQPzlHZF2geYQX2XwjEhmbaNEEGufqiRqqMNkL/il9k0iibtXQbIcLpqi7aLY3cJDp9amW+G
q0nYqAk3xzfxez2IsnJlDHUbAQgtctc/NDaTT0+pIeaxzXvww7w/TSS95OKD7TAtLFVtw5CpuTi0
6IJ+H46KBDMGEJqSedIgpssZ8VRXNosL1a9bwsMZgtKEYQ3QYNhK4/HKANe4j5KLgoA1YehPhFRl
Nx3Rw2I82bMfjpIZT+4sSAo0NzFE0UNEwIEbkeb6OWAxXziXZvXCNyODMt4X98yVZB2AeVXizaGp
C0Bvy2tDmxy8JOiqKhKOW/yIItPXeKxHl//rcsDcy49E8N+kfYIMON9ClK+kLhKrggGzZG3mlUzC
gEaPsJQN8bDbFyhF3io5DcMOcrWOuidAVSThJs5EcFteA5Y1NvIT8HHVkNgld61sgrexW7I5BwlE
S45vlaDJ+aYG1BOsJLhtpQkE1R7Pe5rMHAL4ktfu7nAA4R9SLORQ/hUGeQz9EgaTBMgfLUkmJINn
qbbnCXPNK3eTIAmYijVZtRZNaKsOFrCNgJUgI9QVnaf9mMAy4TUZt4A5FCjnnE3+dbixiKMdGIyI
Fks38u8C1j8bbNRpbcAWPhTAtZD6GgvzWdzjA3g7eEkbPtrqRxOKLYSC+/nNWHBVC+VfrD0KxPv3
qe1rpbLW3u0lyH/mSFlkVzLNoxYY6IFMO/6XPufs8cMmj01ITKK7fdU7SLNp/zCSlowcbo+Nv43e
nnEQN5ElVIjGT4GvnMUFtrel8+t46KXCPHoHcFiNr+2Vp8Bd+H3aKiY8Rqic3YmBndMiyXMYw8/4
/DWAXmKkLCFpy+ty7CFWOFJuV06ewXyxMs79IZOul7KKpJLTrmUOlqxi2ZbPgD0ulEaJcVybJdwl
wfwaL4gHEbXJm1Q8wcYueIEO7YKb80as5UkMbDoHrXUzf3O2HIzLubnQssnYXwBV6gkEfTw8kctw
3J/3tWUCHaNvGL2+MmjO/33e1vL4LWSDL8bfPc+NMnS3yuxdEVHe9JfeVRL0uNY3k4kYzd+iwD5h
jbamaWYME4Xo2ijh34Fb3DEIK8evLO6IS0iF+wUNmtvIWn7s8BnF/tOyaW5Hk6MXeEdCEKMYL+lV
SeOhZUps68ISQbrLltiLfbbkjSXjZ1mye7iCIHyuxFUbc4f4d13q5auOQwEVKvkKzsxrbwUcty/s
vS1hIOT/tZoPPPFnJ5Y5vAekrsnR7mUUD3fR/8rfWQwxVcoXX83BdiWrZPSys5etT3OAWTIu2Oij
sMQ4gMSO1c2+S8tl/jazwiUfVhmN3csAD9Z8aPJGkgMADlyOTcymuwzL2s/xufzjXdYCDfHe9hyb
S1MSu2W2sOOPxHPa7Al0hyPdqJ9ZwXHMKzpAfCcOUPf/tZzYImfmrQy0Y3JhUGhj0FN0C3FOULlX
vO+m3IA63VXY2VKuJXJqpHL/kT+AzwS1Xct+YcSu44Hwt1k11kDXhp55ar18PiPEe8NiAurIWKxB
gj0ypsFhb7OCd9kbUMby9KdV7MEZuQqUF7GloDKSUATjQj1flpehtfMFkeGGzfFeqVtEhAOXaSsP
9Vy2LCm7Ie2S1hSgMAMGHKrwfVedDxLQQI8yBh4iCkm1EYCgPohV20/+UiL1xVgh5aE7pEXGFzvh
nORm7ppSsLjfuNTdo6vysUmUAvXJLmWwKt053w1HuYUhF9SDyO/twhu29+2mZpNcKKViiiyqOY3y
7BBl2KTx/6d1mBRmDriedb/ksvJYSBO9LiOGgM1ekfnJ8IVWSbtsH/X3HnDKXNopXnJa5N5OE5rz
440N9/uAoor0Okg2+/b4M3RtbQGsQ7flnERKEJxKPwOosGE1Ytiw5f5oSoiVvYXQ6SKNGawFAK3A
BFhO0Q9I5nhZnXMcLdQAXvwNM4LH5hW6mD+Hk2rK8zWAf4pNmA+Vix70dR+hBogDiWUzy/QUh5BY
ePQUAOky1hybh033MgIwTm1sd+F64lEzoC5swkW4Kp8RBm3QoVG3slPs/Oiu5eTg7btwuHM51PiO
qfD5X58yWBMyxABIU97myTzv+frlHOj0s+3T6kUn7kgAPmCA0Gte6ShXY6doIAh+JPauJxiqQxg+
B6VkAUHZpRwXhV09c4OFLaE7qcfbHACvU35vpG+dLEm/Smm8lmbJhjyyWOb3xJO4xbtNAOMmDqCY
mfdVvH1/CLqjtL59Z19v/hujZaF9NKg8zqMuHTnz6qbx9Wy3WCY8XYp5VHkALzPybmv+BkygyY2k
sC+5xjPOa7DOjxx0FACLNt6f4rI+b6He7CtOt2kqWy5qJ6oskxrOjUewc3kh150BX4UFBbyRRIgN
EQ6K9AYyDp8f7icmnKuof+Ab4qM9Nhr/hmW5oCSzHbgwGP1A6vw0tmlkTjf9evMwZ5av/spmngCf
2lHFMZ2GvlBcOjfrx3iz/xSEw2hxeaPoU1Du9Ep29i1ptPIsNaipPrIyH3gpWkxyLk7uiHpc3GzI
8Hsjuniv74lnA7QLFCGP657YbbscVl9tylg9sQVqykuHV4vbQ4E41c7q0qYVvSnYKO+I83zc/uQp
NPUVrtOAC1A4lLqZrLJ6oSi1J99OW0EUcm3+SMoApuDj85RltrirF1UQFrT27TOVog3ohmxjfOeW
8ZkJqFvYWOZHoPTtaeOrpKTs9Uf+jKXQAYdGFypHDqlF5OzGHhoCFdYoL5Fzkahfm5Sm0iKbRNVv
ndnq9W++D+P5ZBvh2fpW0DpQLPpsx9S5dNc1yWCcEq02ukZtLB85B2cPgyYW3uAh/VN6H6qNqf2n
VBuovquw5gJcEjz09dgEzEuX/RjooGx059G8Fin6YtzMzHYlwY07R+eI6QQV1D448Aqwc24lXTWa
gqLvY+R10OmN253hoNdnjmrFvQ0V+HbbLvVykp8sxgB7X08u0a+Y9A5qEQqJJYylzvyrCA8lPpbY
66wHu/HfwGH8YkoLVUTb5/57mwbhptYPnUD0MgmAMH4U90GWwWnsH8/Tju6/P0cGOc4GBjfD3pB2
ErGO1xTALyIv/0t+/UnYOaDcBtLRDxMylmA5fORW4XGiRmXbIRqxAu2906ovuf6ira1+Gxe7fqQz
XDBwyC+S8vzqCQHX9ilIZ6gzuAqMDZVYc5QsztI6la7KmobNMpGDguRs9pB5ilfgEbPzO0SSRbBL
wD3Yzc9mQW2i387AlsPrdgg/kr9GKkrN/s6xVpYlT8NnPru+uKmONH09dHDyvdMwUdLYRtctB0yP
s7b11738O09u2DV7lIphiwxzMSsCGO+Efl1x1MERBLWIRXbmGZeTPB/Jz4QI56UvvRBLdx2nGM/M
V0vWqghcJuq16p0uIi3avfSqelI6sxaaNTkn3HzmSuJ9dtAR2jqLjqRPqEOpMOoRxMRdsyfcZHUh
ojmy9PmoupurkH38h4/ua0K0/cnGuOzNl60SxcDX+q87oMNS7E8qSW1CZBA4PCUHFntnFYy918zn
+FD3NzPkJ+f6f1BFvbOXfp9RylsLqF+B+OGqI/iLwgU/hKAdRlxK7JtpT/8G9yJxKw3sWzoTNgg6
rbMxhH0q5GN06wVzjO+CCHqvgu+sr1pl4d9Nzt6w2jM/1l0K8kQ7F9wr3FkDB3i/bg6uvz2u4jfQ
NpBha8I3iP4ZuroeYP6zIxfdkFaAHpdnjpyI+mkHXjWUaSqMVrp22VjJy6A5qfBWhlSWolRKzf+E
60hx3tBLvsbMzuMY9bf8ilrx+o/2zZYgV4m4VBodmS9l1Eod/eHr4wOq10EMaUXZFSZTg/Ka8GiM
CK+g66rv/SJRNKwRKs7YdTl1hZzLYMxIg9FINlvvmRpR5QHnPd1KEA7qOdhRBkGOm4iEi5i8bdnq
M3zkdCxM+FAOKaR3iux8RCUABbZ/4ObyYo3Y0q335NoI7jSV/WVANwoG9rORO8IVcOWjD711i6sK
jfb9tLaOGHTKVN4LLHU8rhevorfh/LSY4xS5xNMPfnI5RVnu9MGJuia2liFO8tnYJmGgsbTs9I9o
HsAMpQrWZJNlaRDkfu2I8j9+wV7CKWr4jcijVtbFyIIlYU5HlPQn7gvxd5tbY+JnIkB/PG8FWnIp
j+gKcae74IrsBLVva3RgvVwV4TXczxsVgbL+akb7F9tqPvLIagDo/rlXbekIvJwGV39A/EP4dwGF
Uv40wkxMRLBdJQSa83KSxMyf8ERE0ISX/7mZ0f359lf2IIlZLnxz7Jv6QLbK4mBl8dLLnkKm/EuS
rDG+aLANUynhpT0XOyswMNgUlDmVCxCVsaHwcKF5Ig5OcZmcpRIxheoKzD1NKRcNDSUbqhh7nWI2
lRLEo327ej3h60k4mQGinoUzHcTvVYKcaIzc+EXzJFV3B/I6SgpNDroQpZiqKWVrIcItxw9Fepvk
IAz01vQNXelrKRdZNPDwT2vrxMg2d32NLKzDDDX21UXuQTBRz4Xt4dK3uB4RLDXH89wM+eKFvCgj
pu/OSswynBS87z9wkI+uHDrZjiLoUkf6XtPjkByYLK4k2bjjfjP1oXfC0hQZ/9/DDygXRrhxp0lK
AYWihABgezwYYkQjiaAY0pMRwlRbC0hJmk2sxuRxGFVu/ckr8BsAk/bGu5A8WtZnkVPB+s4eEXhN
grgaWuCkbIF79+B7JBawXjGgZW6KFiZzdIKgBVhN8AS2OqPTAXeSX/QQzRurSH3bBoT3VoOsLdvq
TL7gc7a/H+D8qvxQyCAc7UVmuEXEJ7AV1k/Ork4rEZZXAIDqN/eop4tjAR7UCNCf9B0pnmBm55Cg
qEKDBCPI6gpO1Skhctq5SjT3fnprorT7IudGmidR+sR0jAnakLLJwXe7BzRl0UXAnGzRKkRoILDo
1JAHqj4zY871PhLi9Hww1i34lEoj0ZoUXJFDLSeYi4OI7GWmBkqw/pSW2UWd7HXtVJ/Ul9PYoCMX
m6J0Kpntwnvk6y3Q8eAidYQLfNUm035i6C1cm8GgpalvtJHaUvsCI7hsPd1oyvsYEw9/b0GlzAaW
uOQ5f7hHT7PykzCzxZ2dmgqwPgzqo8casZLNdH6fmllp9XBBzagq8Dj69a9ofMRKVKdOb8n6SgO9
KoathQ8MPD/aGUjeNePb/IFbq5TLWVNxCLut+wdjET5U2F7Kuw+GJa+tBxbidQvQRLWEKAhPvpEF
Ciy+pCyVZ9Xgs7MIo23R0dbCgL5nNeP4YsHkorluQ4im9CD+/NlsUiKkK/RFaHLDp+oaWdz7qLno
EIoS7Dm30GMlR3xk9gQPslCgJaXSJ8LKeciPy61DCb15egKiJ+aj5ZdqTBy53OmjuK7kk+vZlLeb
f9OLQx1rMD34AR/h7EhVGwgZbKrbD9DCMSB/VdWkMlUuYYJDUUOrn3MOprlxaJRiSp6+OzV7fEKh
QZDNoepoD0r4hpAmOIUSODLmnQ3oUmOnLD7lfrNvgKo3LrWdm7n7MKdhinnVvwhDx1QsuuHwvFUk
K2a5it03IWgnDxvzIb5AQjlOfHoIDtYuX16HPyPbAeQ4+sXLtSOZvN4EjcxP4cKoLY8R8Au7FEVF
xXuMKY8Kv9DquqzXuLIedilhFAeu3QgHh0saJ/vByWgw/hwIlRysQBdAyoCSGPp7yvIuTD+E0Zho
sPqD2JWojoQN+TERvj1TJO0LNIHlH097dVE3xCvjZk1u+ggvJ1MW/EllYQ2SVxCTUNdrnqOGT5iu
AyCfyP9+pcncx6i4mPrSKAAvuVI88rVK7sTo+rAFwxNRwwvnuNsKOdYZKJvRqQUn90OBf4kFeEIF
cKvwuLwkI98hYFsYJ5RF1lgHz0Kw+UiNsddEeEZ1+4x1yEJamiNGiqbSgRtNcQO3mIWW+IfxmsUA
Zo7sAxR19u5rc/xWPg4fIInaMLyG15ChyPFYwXFRLbN+5l9Eov2FM7wLObeSLV6Y8P8LaX7rqgij
AqmzyjGgZJw08ars3VCzYGGSiMTST7r+O1M+cjQg0og/DsJi1L3CpP/uUS/4rPghn417CxYrUoo+
41MwHKRZ/bGr8hIOGaJZ8b0flhmXTGYS1NaGO/bQL4zNG4QC25EhsWjUWz57S8sdkU+38kupe5YJ
Ff9x9KyCHEYb+2dx17aF1uYcuVOU3qa0NT3S7AG1NhEN+hgYd1esBO9LofHaYdReHrpLpl5DGmAj
l8CZ8IM07Kv7h2YtILso8vSnrjTdSX3aZgKGsZop78HoHQC3dpjvWhL7poW7guE+xU/MqUTMACBx
XdzHssWOYBKkbQJu9nqPS/qbJ9n3CU8fL+RzGaTLELyom0cpPbYIk9cb8c8oFJX7GRemhtvNTjDG
1vS8J9KEH1ruKxQSiFtRBKIcGs83FW3v3S67VcfFevqanbSlIy1C7DChd04IBd/+Pa/FOi2cKYke
ywE6PxjFVCqn2zO7lXB00HtLOYBIOvArjKS/YnrlwTHg6yxjBOjxIeREI4B6x9BiaV9zgIM9IluC
AMIrpqK6lWYeBeHEF6rm26IF7qvwErWPv8/UBQiabYCZNGOKmkFkVIMwZ4/bmba+n+LQtCv+xnt3
/uyJn9TyZbtBpMFsAIZ9CzHZFanRQpmkGHLDZTtY4a8iWLRoOEKRe/mmzjCUTzH8w+4TjpA/09XH
w/MEH9b4KJY5Zd5ZGUwP96pzAVRo0lz+VHJ4jZQGVG55pWbInHrnlE+U6WaoRj95BJzt/BMqicsj
tthtzvhucVH0law4kGZVdvDFbzu7SRK3zvTkzMYrnEFVzF6vfHY5LlDLE6dyctT08hqVvFDYhWuZ
FJv5yKbFoA5Z9z7mA4HdbYv1WoE8E56CCdC7DtxT9y3X/N3mFmrvFvrMi4yhbRKfDx2+Pi73KV3h
Je9acBUjojpZzs4my6ynawsgd5T6yH2EAqDrfN2aE5VBiFC8TDbub1NrAJXLbijTkfmcI1RlRwSe
7wVUIAo/dX+fvZUMafquIA1sxA/fo5nxOm8aPhMpFLmKs0bmWpDRqErgdioEKKisl4MSLhZ8wDvr
hrdZ8LKgvDMcK6fR0qfMqs1V5ogwHP8qzMWrKfz/JwZaerwmY5MREaS5wxv3sJJMGX2T6qS2b5XC
cN11Zd0QITo9gn59+WO270qqMw+Mzt6RZ+nq0li6lcUdt/bBkcgsglmlrvN8vub3Uthpq2JTDZan
AT7YsXDbnAfiWPhMtT/a3ej7z4X+J5aS6LMO3Gu33mOJHg97vjKmEw2Ea0j8iUGU27rzhdtWgUGC
2lsvo/ilPJ1Ta0fAW6k0JALdRjM8On0Ml9Ppcy1KbuVnQktErPgetv4sCXDceIDo4tHHfmWSI50Z
wOpcIHFUBv6+ESrH/hDHr+tQR6xeMUfeum/fCKQuGNX9Y5OOYPvd7RF1FWoS3sXsalqSqiRICBqV
2fNTaF3+2VbjAwndF4+NUWI+9u8N0iubrU9lBssS21ow8Qzh6qnswH/KvaesRjPlZ2p4TSpE/y94
nP4Twng5VsbPeBLIqDc4br8jKgJFU4QMsX2tyxivnyd0gKGbP4dvACKQ4mhJ/45WX4QyGRCNnTWt
D+WGZCLqHtbDaih08WJao0lMulIyHy/pqq8+mljDwp3/r2qBMfKWrQy2JDX8PTgDYYJcfSLhbb1x
ida3uUbS3f2skgBn7R5dYLiITJnOyMVXFPDZxSvwhsxzd/dX1UZiYJTT1si+iiQ4xF/E25kNdCBf
DovmlOGnEiB2VYRAQ50GsNdv90wiwcwlGVoskqks5mSKfeehZ4qAHeDzwfRDl6xTkf5d1u01YQRr
rFLsBxA1YZgB2tDlgG+mFaj/jfPsUUckiCbYcBVrI/mFYwwn8PDeAcOLh2DPZ+FwuODndngHFB6w
nxqwMjKzgeRcg+s9R9sgPTXiwB3iad8P1jzXJU1qZ683MJIhXMXWOkqs/0vZuwTUSnfBJYLdZK2A
NIjn2omeunriGiEscljrIv0bLsRgmq8vByeQSKfwqSfKjOfw4C0qhwMdVq+pDWjmzIiZn1i+G58w
HnaRjRHbGh4OBx+0VTrwFkr6UrmoIbmPs9T0WPCm1JxezahuklEEGtPBowDOg6l56eW8zoK8IaVQ
AebiHBFh7lbsP1hqezfl5o4lUGq6i2Ha4ZMlsEgYO+KBQbfOZNXw3ezmp7rfydccs5Y+6chRhCGE
oIhuh3zG0wCJuoNi+C08lGqjUb8c6NMVxdMUJ0pllJGouwEHdWP3uO6APGFfMquGwOrbNuJ1Rqmx
SFMAcKxdMuXrDSDgfV8tqP7KJ2q9wXbrfb2qtUVA0xjSUzjPcn1UPqiEyXqOM9hpnG9P35ZAPHs2
6Br6dKoqMZUo4xiuadoOvfbriuPpI+1eNrju8snBdvWKqr4fcKpz5aYlMu6px2HAgqrFiqW6k6oj
1bb5LsHwT539eBKYwz86DSBytyO6Oo3Z009bJTowS4V8lvu25PuLzHcm7mu7gds5GazDbIYdhfKk
WgloQAbfVg8D15g5XtiH5DNnvo/OCk8yng/4E3cd5b88Z59C5ljl+uAXeqhFZxQR88IyXU8DFWes
j8wLTFBO5ia/UuRbnkXHwjuTh+A44Dwk2DE+WPhl5suk5CmOLfaawGL0TVGTVXsPSyd/qrjmGRnb
wF/KFNa8djjGDDsUKxkWmKrxQmPic2/Yv96Gp88Ix4tV8vE9sRr3sFLgpbmqHA94iGNE4Yw8xlp4
vWKA5b176ltCKYKeh4AowElmz0zhfaxWn73zl4SKUxAqCwD7l8DEqJCXbbUeHT9MQc8py0WvFfWh
/s69ZTg/39hFHzpUU70imXub3f/TFRvoFHfy10EOKNj4j8n6+tIsanWarBDIsMU6OoKaZMDQez7F
6vXsnSctiiF5tAxOncd4Z/XRQX6aDeJK3aLI4vsul9iiJlLvEs1wvbEXCcT6K/JFgfTZfTz/p4Lw
my5eiW6z80i/UfHk37TP98UvFPtyiK1vgYnUeUlAySIADHCvkuEum3P1CPH1EuiHt+qaqAkUaGzU
Iiqx82rLMCVgRAn4A/YsG+VkB4T49gehmLRJkBhLHaYzEJwQVa9zGpM8EvioNIl4Q5bOdlj/ZZNg
dVczeaSP3R/NKoN/nTLk1YwP1S0WZlognqu0ABOTJ+AR6xZJC+LTwvmczUuvSUNqVMV/6QNuCY2O
S8TMKCn3g/7M6si9PG55D50WpIrCG4LJ/1idqCdlvr+fP37rGAD0TagxsM2hjeD7IQulvG9cxjbQ
j356a3M5GUo5UrIc2zeBoS8KzKxH/d+zq+KECrCd1cAIOE/lAAHDzdIawPfHy9jAJl2dVW4MXHW7
DtqWRJKt4m9HrLM+HDQULST2NWsGfGYGbGZZJAFqG0EW9XOc9nAZsu+bMMwR/TlMSmO4JBOyJU5z
EU2Kr57pU1ORfm/RuhRQkZb5qvdf9RAV8/p6FJ/VXC0vidz86cLAKb4xYUzB39glfuBCfjRSq5Hg
mUg1SfcdoNsCb7KdqmtKpPNpj7oDbz13Sc6cCyTBykA528XSa6M9izRJrW5jT0P/79eEov5a1BRE
/wH5//4rtheVsHWUcMLbwJdYqTbuBFJxJnDFmkeqKl25KGCpWesm4tWnmXC9muyBwThINnKNtJf8
l/U+x5A/tObHcq3mWAUPAA6Y2+K4s1RV0InvlNoRQQJSbzEGDAVyYzd+AOvjACLhQF5zjmM1WD3u
QnxJ9sLfnaUTIRWgOxZAGycVfCn94SCAUgTrCChRy/rXq2VB4/3ng2k1cyf7l7bnmcOKiKkxSdvP
EUoZKcIuB6DrMsO2Gp8tIie6pJ9su9/1ocg/zTkQYdQ95M1Z6peNwvxoagfU1NxUe0UG4Yfoa2yi
RULRBJlixlRRoGSR8AW1UMhlctcMUAzbM/Bgj/grY1+Vrc1XkEJmzJhEN54NfusWeE9kyyhgINw+
ErcwlOeZv0d7CDmUrGj9BpRKpRpdXzed5lJDEPFsk55CmxhRvXBq5omMOhtxerwrajVfhL3o+Lru
iCs//hfC7eNAnoecLy/VkU3VLigYioQz8yw9+kXEA1WpbBy52c18bRDDghawf2X9MQKU0aR/a7Kz
E9c5EUw+fpkyhYmiDwK8TMdRi3LvBdDjMBSNQFbaXs1BB7f/X9YqqGxAVMZhceZBqQ7ngEnCTY/F
VCrlDV70dkNdKJUUBhUJXXD+3Cj4fum84dGgnMPZ/t11Nzny7luzc3aznI4nPDLhXIiVP03XTmuw
1LqbhflCc4Lm4QVGQX3mlGRaKi+ARpyzRI82Snl7yUrOTuRp8wIT6FOH9yKST+JOcvPRiYRn06lL
oITUyGKp3I4S+SrjRf6q6NwaIBbUQNuDjSP5/XD18C99Zw6gPXxmLChzOGTBkgCnQ11I39QPfRRt
lfYJMOloTMxUnitwuXUwadj4LU10EsOBOGV/9CE8l5wl3pdCYx5fEKqfs287UFFo2s1A7KDif4ms
JFO395lZWO9gVDVLhNDt87saEtOwOBGoAHMDcpabCRxK9wheddiF4pIaSd91rILPOS5cgQu/JTgp
Ey3k/nB/4haTIOu49yNsubnIM8lRuYgHeEJFx3ltP5OZuSO898ujVCoBljS/kA6nHbFRtcZXjnw/
wKc9uX4t2XaE1pJjRjanDqD1rTEpYJRs6z3Kv+vfPjO0PRKzisejV+9Zyv5fFtOuOBQHYguTkBNf
cMEOWYukb3O4CLVXQ15bsf+UPQ0Ps4ZkClsICS5WQaYT1QAObVYjHv8RQ9f5kVuoRS5BRkOVNqMu
K3IZz9H2+AxdkNW9EX8p2nJOI6nUYi9DqraHydodOD1wG+ZVQ2rtNXj+VOkt7SuG5iDsnhbWxroS
6ehqaxSG6VyftRb87aOKEQCPQPlDli93aSE82JFpwMa7WcRIOE0e3qZUa6tzihAC2/jRwp73ti1c
fhNpHLp8aRcQ3bjTYhApdGrb4uwtF6y6ZYup5Z3AazRGrzrw8KyWqz2rJRUkuQl7dYKMkjaYkgI/
T84PM0COPgq1vBnEOIyZ30huRuWmSSusu+8fRq2qxNj9JreeAszrB8l3ttc4kn6075G7+m3/iV+1
peZRdB1jCWt/uKfOdr54qYB7i13ZmBziKWfXC1XnVY6xg50Ns29NJ9o8fdiT57gQv1GMuJHSdkBO
7Rr4dujtImEOlpT02LMkL34yHr52eOPMIyJtt4ln4H3/w/8+OltMXpKbyoH/NfVc/2bf0h3dTN21
clC7TpfadP6bbsIxfr37SGzoKpvgQW6C8BqItpK5NZor3lAaXb/87YVtEg3n3ZTcLgR//L/TlY6O
3Q+7duAONNTVo6Z7i/m9waO8N5mD5RmQFWQVZxwh6Qj1BAiqY21WpD85ZlZ+BpZju10CbLLsFHnI
Zz/bvX+CthOcB//rbuwVVxP5RotRIO9EJIc4UOdQj1zPoFSbXbEurUIGMMmvhu93LZVwaQ493SM8
Ls8TarNhItQYnQpIqwHM6CWycx7j79dUXpOYbCoqAMk14O+K6rlsoTp6OVhvADAS4BZfT/6m8PCg
oKrkpKFdAbqXmSX0y7PZH+Rk3+HZ1gDOPqszQT7XOyEhLZ2VOmW2J9tu1qo0ETPqu/Rf54Le0b4Z
gEOdd+02zPuJYGd478vx9sXRcK7IpZr8yW2A1hSxci7qyhLITDKITooPS1aGWh8FmBFFuYOfqiJM
JSVYeK69Gzumtt8n1qfPU7URvdfYjJVQJZhSkGmbPEeFVdQwnzxWmo7vkzC4Ktwhg6v7M1DUTegi
U9cBPhR1un5TsdEkb2G/a96rx+a1rxtQ1Jk+jZ2UE761is8wwgajDRiQrov5RA0wrJvCmYAvCB43
LcMplOX83abLs5CGmF03FS2K6m+NixB1hvBDsYtr/jmVfaBpfx0Pm7E1NmTXNdXG7wPvFud0qwdK
vBhGurhSfQcDNzXLVSqHTOEVy0vYH56l6XBjO+m7CXx+6EqT6ds2vBFVhrfBytmQKHKC/lFJQQy/
l3xPDRyvAX2uxoBMvTH2N1Ac2sQPthDhYrj/JiaxPjLp/ImwAM9XWCaNKhhnPAdYtnRPy4NKCLCK
Di2zQyRbYB0rRbjfO1c3JiGsEi7AFvIVwNc/u7YbmL8s8Ut+kJcoA3AUvfkq0qLD4bzwrtptrx4a
vBzC/Hx3UGoLqFR/Ri2NpTJtqr9rwDEeAR//MtDAqzHxV+YroiRvUvVcOyXBynwOnpVQ4MUSe8A2
/SGdM0ciSKJhUYZD50D/dZ+rC5apc/IQ8Xu1jVkdA9wKsD9NhzOCM1iDrRcJRpcrsDmWSbcx/sGw
WvZdKUlsYZmgr0HvKvnABgN+lawGrp8om5/K6rLxaGdvMe94+NWfB0hQaeHgFhm+BKQ/g0BjyRdz
pTfgK5VM1VuUmKffMdF3SI9f+j0SLJVgfrKrRO0DcbVX2SSYf7R/5/aXWkceuPKiwrCxofNNL933
NQpmUNYjNHbmQz8LLEbZxn6kXw3JBE+5jTuW31P4YEf0RdNyKvE2fsfWosKlt+IfECure0bCavCE
RkuHnrTzRsScCpl0HXSxVQrpZ/9uAZM6KK8RnsKz7fxpBXsOh6Jawc5e9V0+9J5nravxYQXZF4mg
JnOg9is7BjlI+Ju45HY3mUdY2nG04c5puHWcBwu0cfp47OZimReAb0OfkbeMlTsWHLUYpGltad3U
Uja0UwH607AhHR2cGqHpyyWgth+uQtdVf86VjJxdBB0FTWuJzmtmwTj5am/1EQgULrUucpnP6bO1
VVzBKUlWfVAKj3j+QQ9TKCXotEy0M21opbl6AJD2E8r83M0HL0dPqwl5MpG1eXoH9xoR0xkRcWwx
zW+HMPFsaYFJ2T3K95DpB1knpoBJKtK8RCxYYLa9rxFyLSX/7X/8jOJ5SIqVVo5cv+8GmUkARpyJ
nUqI43imk03PgBHFES/KD7E7gYY96tRIOJZTY3oris9onb/AzsFHbefoFBTsVKBd8Gz4k7mK967Z
xW9yGyXn4c/jPycFxYgMOAMGlB4Ym1tAad01SEzx9kczet4ezCEVXx7beHxECj3ixoL7ygi51KEp
sSvV1IAycAJuSQQytP5JhpSn0eI9OIFf0oQtR2AK1tKUNtaZhkk8GmQb94dOKLj7xYdo3PltaAPC
R4mRk2vM+OywIxns+SoZffRv9qzDUms5omUt4mxjTvbX2z3rQ+UQWwszTlyhxT1ir+BOjQ8RS3F5
7N+/MX81+9lL///BKDedjhSxsGTyRp9EmkcSw023t39v2oXKkcpSoHAv9SN7/Z1Vk3pkQMvfBrN6
d5j0plx9+f1WChqB/UI9ZK0XOpNW2QgkbPgK6bRAzePp6NGD7q9d1MlK20xkXeTc7cx+CZLIQFxY
q1/sDPOeqEYlt4bPi+u/TWbiTiW+iPucHX0gRxEatzsbfMq1NKQkFUsDLhH9mnVQbfhaOZIbtDgG
4snxuVJ2os14sV8Ub5/8oGwq7URsW1Fcc8046LY0KFsWHvT7PxlZVcphwoJan83FGRHcpQLUOqDJ
kw1LRZl3c9DSt8aDjRtEHn2FlSOEZtMhsBw5x76ROuDP6Ty8uC/0yGd9bZdO6Ur7ZXPigIYyG6TQ
mXkHaBCoJ4GHPDd+nEcX7Rlt9G74J6VNmRuvACnv5UJCSjhheXuR5Yb6hAQLlMRcF85iWFBUPnX6
cZidno78Q34ScNWHBdcXrgLcsZyIOLNp4T7wp2cFRUZQhJgil/wPGT2GIqJ2C66gY3d9rW3cIWe4
bEUBCtlfEQEwxRVf9crrt0ruiWYJvAY2KakMYdmGncbPgjGuYq+dmR8vylVBuNyzGI+hmeTEHeFZ
CwYZMCH/P5jwhMhZBV/z5Dz7/UTaYj7Yq2/aU873KcK4uMy0nKDvYW20A8xs4SEoKTm1NQnklAsg
zjozp9q0zmsLOnR7Qq7TvNqJo0vg6L2LmwetPk+D2WQI3uL+0YepPj+5ra6wZ2LnltMmv5s5fT/7
kAxc7CbLSob4jMX3K+bvNVdDDTii38F+oFZ4u+WAYxnDQjyqkUoGufoKLttjWMKAEG02eKbLvz3A
NKgBx7iW/fomjvvBZEkW+Q4iREf6nxyTBrUQfalZGi1mFwxONhpuyXt5qFndIoe2+bsvDBX2L07S
38L+K0A02gxOopaQNBJE+QsW9DfBQMBdElN4yD+i//NDkLJ4/ioDAnRRiAwWlSfH8l+/wUvysNP9
wrI6e4ic41hLyeu9VwWr859HAuzhPRWhYtZloiia9ScievqXr4E8YHanD+eWC8wN3zaXjpl1h1OX
Om0NWOzWqSzhG2gV5cCt3DAd9bKhaeo/PMm/2apaP34hDx46/7FV2VPpljQ/iHzC14X4bChOzXxN
Pkas4Bq3kBKyCzWXalyd5QQPM/5Td7MGPizy446dVVxO12EzUvAvvcaCgaUaV9nsIyaxm6uUBO0t
k/9I/e/jpDBQQthrVttV9Rd+70eAeC1UwfK2gBbnNsjgjq6/fG42wshHwj/Qr31AMnVUAWBcE2/B
D+VNc3Jhn4FIwEipw/mf0p0HEAE+GtQqvdFN2OPf0Vbr2YAlGESFz2szjY17mH2pMpYd6xJt70J7
TU2dxoGZPmBWmfwBmO550ipfAjdI3kSpe/U4mDIM8PM619eViBpyqiXJlOyGI4PkfgJUg56lONiI
LtJdBhT+sU2nV0Sze/IGURSGLeOOh02edHFQ0mCPpht0HMEMI3rSTLPNScPR1K8TfRFqkrhw5hDL
AF5lowHRB0oY1p/tNh3SINN0dGQREHqYP+LmGAy7Pi+1GdFEY9CKd1aNuyrKLphjSANBVZ658+cY
yxhAJ12cBxLANzzwRDWPyLCgr8jrtIk+/DUpT752wDNAw/QUxmlwMdMJ7vOWRc3QYvLCKH/PK33i
RSROk/E1P4X3oNyZTkD6F3NH5mg15YNTt6aDKjGAiKSH2ZWmWlXisw++Gcg7ZQiVQzwxPFbgUMan
mGcDVMVpenEUf0OfnWno+3gLipRmJBgqT3Tdis1jX6J7qwNtTZNmYznBOBtL94BOhz1uoWP9vE8a
+n1Si8yPLBhLHPgPV0wD9k50esBBEp4DF3Ip6sbAy0x9ZMYHvaqyDfXDtMrhlPTyNh34S/gCvHgR
ohWsNnsdF4xSHZ54voUbr0B/TRLj6qHlW4jRNkLjsTUnnl4fBqIN5RBYdRqtbjGxI3eb0ncH7ESP
NMizC9+Hj/LME/wo5XvBpR1VFe+KtLqJLBAgmZSdcx0VM/9WYutEXTPOJcCIh+XJjzLaKl90lzga
xR2h74icydhtAKOgJDho39YwTgOkh5+qIN2j1r0G6NydFHxz9tr0P3a7jViRII67o4R23il2Xy6O
ekcUoPlZsbgcAe+6POprTvXV8F+wyWrodmLGIikNICMUhatIFHL9eMuSGinkTiWI2XdXu5xgAO0/
KrzFhOWV307p7n9OUzzNqovzTK1zx3SNJtlvlOUwstT4b6yRVsYErbVQ/h+KWO081RQKLd0xWKJN
B7XfOZIYSs33wcjd1ug1FaTYi8HNtn9kDMq1MpPuReVWeQOXONvzjqUh/dOSiSwlQMsXiy/FDGeo
79rqkSEHwZp4z6iFCD50ELX6/lG8yqDjcOPNW8p591zpgqZUo/Yiofv3bXrSiS8JJ5ddsd594xPM
ywlVT8Sfggz1PaV1eIPmcljLcC36kNqGmtwdCam6Np0JTylxxlQsGX0gF24LHhcycpT0KB7aBij9
6fkXhSeJMtjgcNOx7sourxzcwpbe801PuzsFQomS5dVAd1AqzjbJa3qZEwyTP0kU2+voKLPkDv3C
SOHzI1BvU80tlzECjcDyW7oZ3YswCQVeLtz4k5elnACCfpwIH+olSXT6+qAqILluB2UPONNiTpvV
da+nbWCXYT7dsZPVb6C9Ek/sDmnMnGCWZ2wW9eea9CvOxrT0QoNACh7cO0qF6OOyThlYkOASEM+Y
kiOinvw8583MS4663TmD0t+qB9vw3yfYM22TZ/8IEicngFBIr5TKIjsq6Uplp5l94E8Uq1XcKXgE
YSOBIcF7w2V3O/CR/oMy7ad+legb36kv11jbfGNtvyh7oA7lwZaj+V1wWL/7v7FxSbYkWxqmsxPj
oAYk4tQIeCCm+HUs+rnOFahHN0TLxV+xph88Q5xkFBv4dQZ96Hsm4DxnOqtghOSPyMtUEFreZGv1
1y+0aHe134/8/dcunyGc7BHExZ8N/w3OCOHNbEgY9hwfkAbmYCxR/+JpV4hqohA+1Br5h2eU3+3C
TH1O66E5WmdDAmZWDA9mMC4fJeGW8/d/+cRPaMjL2ehQ4nhJ6w0hIrXM2ucoMGEQ6bubDHm3elIf
DmjQXcMBc1gjRCk90WFYkP2FAjvIsP1qV3AbJCPk/h+/d4WuxUsxJstR6xS9xgTpfqo7RFUuLns4
Kxig8A545a68MxpuUwqGBwnjLLY5QToTKDizkHklXlQoZt+hWz7M/Q0IZ96wjdnQiyPntGl9vGDV
9QNi0ATohIf0BpMrzMTw2j65Y6XsflkQWFEvgnOX5yfWct4YQj/E0i0acdG8kTOWTMU8vuKWdVAw
Xz0t7nGSx9X5sStGr2/gzfdb+NK2oTuk3rpZxGkVD7xPouMWAZ4UUP87UbikXJmYvU+O+1PmYdWT
de7gHMJqFsK/w9qfkoV6qAxlXZDR83oKZIoW6SlcikWRELxz38JDvt/bzoCafbtIWRYyPEKQF4+Q
MOPCAt1SwJonIwfRIXEZh6XzHPhRgHAvhR90CnBDe5BWy2jO2vCwgHTJMK/B6IihM37T+9kSAxsh
i1Qs1/iVdxMmkfFj3cRrMhwx+Wk1kcj3x2aUxnwPD98AjWGCQyZjDYFBilhQdfQdp09EIa9i9M6Z
Qv1W9dj+C4Mspokw6wmFtJB/9oRUjw6Hw94IXGTjPUDkvGqctYyE4S2YbFRSyZjmC2ZutPWUsH7z
dWV+1UASOjgCen2ZKbefq4WKaS8zA4i+lRXNjZd8QTjtgis456q8HmOhzbvcL5Ig32bBy9QqI3Eg
LoIHpqNut2j+hgjomSsfg3HSjuDOjwO1Qtid6+BW9V4G7SLebjWS8EQ3IW+VlQQpCRV4lZCKvvgH
CEh3+xnIDhLdyczjLwue1D2hvZkwhM283peD8aV+S8+EIOCvpnE+HXufv/rkHwYOjqd7t+3Cqeth
vBwwrGu0pRkCLdwtmwbmvY0UJ/e+2OdhK/6YyeVxbJ3T2Ecz7E9WzdUL1WV2gS/R6n6hyRTcJRx4
WWWTArv7sfbWbujlIqMrm47M6d3U2bD61ra59MBPSjmjWkIG7zkb/Jcn/3hL1E2N7RgJnLSs69zT
0jtYWmXcudOcqT/XBhkmR003CwLkN6ZdCXnuPpjTm3MDrwbhD4mzzCoDz+WOANSnQqNdcjLnbUgA
TB7VSjTwxhBGT2b5FmDsZ5gyW5wULE7l+3RlkZm0GhvjsdvnZqPVNmWP14PWqK4aJBj+34AWHCLV
bkcrvtu23sQrf2kRGyjO/z2HzaMFQ1RTRH6Kp5v6k8mSweeiCvmyWFcJc6ugJ9+rv6EHkuYtf5vG
z73BRqpIdG/6SpTBHmRm+tbDQpJ7LZwZ4w4dMKmsM0HivXyx5r7tTS7HgKnjDWp2SuTz/rAbCbxm
EEMafm6STqjM7LDymy0M+jt3EEgZcCXaTtZUDlnZiaV7223fKSXgkkJkZxggbKvVLXSUzkLzYsjY
jVAFE127kVvvP9+JMPDb0x7p1NPVxnBUOOSGE5xzSkHgMnST3DziZ/2tWpjedZFO4sS1jsJauKfU
+AJJ6Q3V3CuDWkcuFpVzVLaukIL/tIO7icuyVyguAsSlLtRn20FajUHGRdQDn6qcJoH7ewtzOvWm
/btvF/EM1oU2Hp9xNeWtypy+CjOgK1LEoR01J2VdVPjsLKlA2Q589RJ+cSL3IkB9kCZ6FPSHA/+I
A14rCGFsOf3GQ7sDehxwZvf28mUJUQ8HqyCvrKfmQwxTVOCt7vsISx7HT9/2du005hj6OAPa78Ag
zDLK6WRIJWkYgjCP+SHejItuX3ZI9ECK4obZVVNGyuxqVPwmg1JaXDcRxHCE/29Pbjduq/dyjIuZ
kkEWjIIuaIPhdLnsp/EaYfDyEoLofL2KBmowG7UtnKAN1yKLKSX66pTpbLfHlWVr5NW3rqCDHJm3
4tPW4ELICaVjXCOCv8GBaKAPcJrLTway5q0G0rPwfdNFF1lVjIx2JHIdOlIcAEXX4K8mOwhca2uI
cW/DEqWUo/uUTEJdM4M5PyJ45jNZN2d0NM3Bd/GDECMUJ8DgCmA67pRELQDQsWo5A06x96ZSNpJi
xdYz0eNYr5vFhJY4/CBfTH/L5P99UUackGmbyGUZB+hcskCh4Gmgf3xh8ntnoe6gSIRclexq5ZRC
4+K/Drn0wNp6Ntsl1Lj0n0vbxuARPlilJwwl5Gd1YjjGfXpkoaD6C2SnOHRSscltZIW4kVdiJyqJ
poQqjAi5GmBu+HvWnuTnxx/QNh112GH3fCUKulNjqwGvr/ONTGPysIQbviRzAknd+mE7pd8VRg1A
BmpSBshBqmupwAvRFJYR9595eWOYFe7b1M/8p3MA4QEhsTvXD8J2ZFXvzgL/Yj13wt7QbMF2WhoA
rdwj27rjvGtd+MATv4ayhSE6fOGOBwUeGQSCuzWttpp2n9oR/JVwnRpF+wQYxmcJcSYWRzWMZEMO
MMbBHTFoTj9gM4gGCo284aqH60T8apcjd+8nzqft7+Jpe538AF/pgihzM0h9DVb66q/LJljZHbY5
isnk1K5h/6+mwOYUGqCV7F5mqOiwayk8tj4zrgWMQIxI85n4qYObTc4OdAWBrHqtKZwKN3ADho4W
X/gYuo6FL+FCJhSig8ULGqNpAsPKxGM1Ilv7QWqRJukk5gQUQ+EsE8MrTAKbhRCireQUDR0ep4ZU
2FY2hyjCbYxecCAHIpErEt6pW+kW/KimcTxn+USrOETwcqF5wK/ldktfDusH4BVuDWwMNtJQU3Dg
MeC3GYFr3IMSunHddQvQo22HPXg+0aicXUP1FuVq5c7RlZ1666D0kVln4st6d7ds8MSiVCP9EhgG
zjIMtZElKfaGjsFOeVoZj7cifb4s2Y5Fb9agW5oYZBCB0B0deVj6/nXefLQEUNfQt6ZqN08PVW6K
KOsH96u2euzdP/G/nQrhpdC9PXnv50HHp/3Gsx7YStzdYUWTrBlLAqWkTjOWA2FBtXj/gamC7Uza
5S1cDuh1bo/u/jjt5N4OhOufLvJvWwMKg5tZYTGJvdi3MFfwwGTklgXZC+SvCvOEzQvvWT7NGl/A
YFEfHHq07rAuZ/gJYZXDh92VJF45nPPH53js6TvPApkbTDBwzUImMYFGJ1ud7/ynT4wU6gL7izpU
3nZMoXosxW3n5FHoUfam6C64yqQw7sQFzr6cXtXc+ExcJwwyNiUX02IznUyDcVO1BBoPPYMgm9CW
YjZZxavP13fi2rz1sOmsV1nNofxyBln9n+4gf1r/6zk99wIrFE3BgPTohPYB7LBhMDBF0oWqIgtO
+A1uSi63d5Iq3ODrT3OmbEl9svpQTfu6z2YK/nBLmiuJxVabxafLzDwsJM5wj0vK813ghiQ40B1T
1y2IeSqRfq9OKcg4ipULxLzs5ahckZbbWXA3iRjqaMB/Pk6hBIfHFI78+5T64+46mXqw+v6H+HIk
4lYYXOS+ylcajUNrZrEsq7Ml+I0rdEwdYsbW81daoe3/VvJAnqAmdXq1KQPnIQVWLJORs0QuJid1
DokbyKnUmV1LaZhEQWrErgWMxJTXKQh1oUfRXVGcZlXMDRcHPf55+hezEX63YHEuEe53PL1RYYPd
WmEiCnj4BQ1HU/9pgMyJVnaTXMOwl4rUex7CQ1dYpHwtwKzOu64WKOCDK06HO5KvLNNaSu6H93tL
VVLxXvUlUIDW01HqustoA4YtEdCcDvrXvh5z+ordBRFe8I1L9DCbB0Dq5bFmH0e5yxLJCfHs1imJ
xCkVHyKRjQ8BzGAA9K4E1IeavaxX/BJ8W2piELpCW7S6ozwJkHjqmfi6gHbPS1WP/yI0pDKuVYAh
/JHnoDUl89igF4YNj3NI5LM3J31Y7xBIve5i3xfeY0At2XEoYc1Su9nDe8Ytww2wLzaRU8m86ipz
RTPfpRQrMFvzFts2cWgIigljr/bHQwP9F0+gzOG/fvARu9ucdiezLeepWnJEz2erKyK0GonS60Vm
PSeQ/2m6+aNeTw60mU48Ql2TTRbEsjt9KcBLnu1d5kHfq/kylBUJfKAQVu3oJopQiMrKygj2stOx
IZ/1O8RuIQdmUJlvg87uWFn6PRH9zFCo/nW3PRb5Oioc5D3jt+aYLG4jSz3cAWlHG31Aq2cIDLQh
zhQUa4h13/CN2bkUyJqsOX+jcBpWGVe1pkrQ1FAm8uvjF4IxD0mNoYn8e8VRdgOOAphJ5m2X07pV
Jg1/FoqxBQlc3mQYlYM+q1B0udbei9u1zMIFV0Zu8vtxELkOmAli2XytIf7uSHJRr3X1wcp9EXPV
0rCxRzV9BZaez/WPMDVpGLhO+Ekn10Ffoyc9t2piHktodlpX7q++nnetfhYPbWtA5KgIHrji8MtO
FH+eC88X4mQRafl6lc53GtNFlhs+oYO5m6fijBmuZIpgEycAVhO9qwONUg7mbyAk3O6/UEdEgtms
h3xHn58PUct2auHNA/i4RHS1pSEXsZtbkFQh9jYsYhYVI9bA0fsOCDalYZHlpPDgA5DUPAQsOGwt
uOZIYbtNCra++I4RSBdbTUeMxdVx04WcxrsCs+YttxMzoJzTIykrSE1Cggj6tEb0RZs/rqFAIP7A
Dj2GNRRDF8JipiasbWjUz31yy2D7OxktyyX1JOwsfCyRSihlKsPxGF2zGJZjFkd97f7k706KsBgN
6WkpWa41VDQtWNBtcqRGnTv8h3ZC4HUdQlu1YxTEBTRTT18R556IsicArYQpMDOlriipG71NxYzW
mW/7N1DYjkIqf8JHKmaM8HzKfIoxPiksI6z4lzSTNwDNDqesRQi+wSuKF6wtZRUYSQTT4hCd/+pF
ufzrRBezldDsod1ojfucZRUeYZBBBslWFPQvuEQnRj3MnRyN/x6xgzkBWhbBGUd6WF+UmioL7UGB
4gdg6cfJSRWE0wDuQdMe2U1QAOmhhG3bUEcw3A9Iv2atliMclKF7H4lGCjNYXr/mfvwQqq/Bi7WE
Be+GREd7aZiJ9KQg+wpwLMRzWmZkqnj/J+Q6bzyVhxmPP+Gi9ds2ZlsiIPxXrNByIB9nQ1+0DJQD
CXanUIXBO6/MylJ6d+1YuScS2a6loALj9ZGnjeD7g756laPTGky/WW3NMLTjc+CaelAE5hzXz64h
U6b/KrEbsQoRiKtsjH9kLYKZ+qet6qVFdieNRsCTPQVC7517+c464deMtQ6//Xq/mkQOFdmgEVFH
Y5/G5Ei1+YLVwKoTPBU/pfQ+2KaogbJorTuUrxWUxCtxqETenYLLi58Tq3amggO8R7lHK46M5HhW
p6IbJw9wxXyW7r+IrgYa0doNVHt7DrPIRAQvDXYAPPlOa0g6SePEB2kP1ct7asj5nGOw79LoMHju
MczJVJNo6AtuTfT8/F9tHnSjaePyn4mGQGNsK2WC9HfH+7sNrCYlU9ahKgMbW6+iQvgS2H1JJS9G
eWpd6+EeGnWJNm5zvJm8nV+T5j6nUI/LY0xBh+UZomPy43P8RLZ5fvkOrd2rwT52zvZrRYwqftxV
etLoqLWv8YJ4ltJX4VwlHXnLlf9DepF89blrYFdx762UscOiLKvsklNaG6UZZHAnjwgwt3PXsQqF
4I+QpBcR1Lpyr1xTSbtZY6kO8myb8V7mJXFXUrsg34VmxlPCE3n3N3u/v+/hBj/rJxYuYuC5uj/0
1H/RU1R85fhGyxK7feaE4g0RqPUtnn+BC72+mBmFuhoCUmQTagp2XxsejbVOqvBGg9WhMIEOQBjb
nIH94+d/sFZ8BBO2w/fgbsIywbMeUugPMzCrn9LiXZqJEmb6A2FnSrtr4dkr/mU/Fwhg0dGGO965
blIAqyxh3ifjquYQQDQIURlhqaRJltIOptc76SBCOnvT1sp3BkXcEKOhyJvoDCjURB+7487HuDD8
RsqSGEzVzWuJZdEJonzfpf/ae8FyZ9Hsm/ZyHnmU85BIkdjV5+OJLMPPrNPyUrIN+4/tJcohSYZF
N2qLCxSZmC0Zrnz8ZNCcsyC0WsgTdxeXiTWAYKJ3TEGlz0r3/rrb4J5egusl4/eFtX3MsN543x7v
htDF+oJyooZNR00WJJGiOMpmhsYgRKlgFD3ob74Q5c4K+ATmX452aNXRL0sWmd8ICWGqRgNigZdz
Fcdf+c2X4H7sTXtfTkimXPkqtslOCAjvO1RPwgEJyaBvtEa6rZ9ySzX09Z/rPX/AMvzjzQoWDXo6
Op3dm57FvR+ZqrjjpCzDhdylBCDOpLfh0FnWL9AEW9oJKnCUC0n0IsjfdL8PqG+8d1dzHcXU7BrF
/McI7y05QF+4/VQkqns8IHoWLfHx5K6VDCatJlMdEqUoTLoeqH1ZPf3HUAeSLMY0Md+I1XcMLiSH
8mys4/7Bgj/mjAh4b+j3HeD7v2ajK29Gq+H6m23NPqEnC0t8kF++1UepV0D/Wg5d6Sz/uGKhA/tH
RVBCf346wvPMzz6A5EXZYomqKnfuHt56f9yUjmp+1stXd2chxnxJOcXUWdCOGCapqfIqOjaFtjzj
t3wDz6wopJLjzhdRmOMbwhMVaHQg9QvSzN1JYrCIRvc6kjUQfLS7eQNu51OCQszr3W/OjXOkiENW
XBnj+W3NwrpWUCIyVS3xRaGu/YlSQKKXbGzbAm6f1YXHmXab+0WNgoAjX2jB/OzAXrDy+4GL3DNS
tuNCsNkKWjVIpu1IZ1Y6mOhWMns2jGyIC/YtFEIvmQyyZtzL1dsyN6sOsFPzuO7vYTp/roSW/eeO
s8+6M1FhHKmML0AiSZFnNT7OlChG/SmSJ7W2Q/Zbv5PTNkUwwphLCPy2KqZksYbN/UL4dJeD0JQG
pms0m+v1/TtmxuzJzqr2MxGMhv3ZpVqycFd3iz1ULCt+3E7x/mXbjUMGASfQOAPiyQsgpNr+VQ0W
cot8yzt1k2wQtNrhzDKEefKKVzWKnddwV//NLS1URyRMQOXA7vqaMUhT88nzE/jUDm69LuC4XAqn
kf9QcygSGVSGtrhK5N2LR8puTCC/v7cU3cvxCNkSHkX1YEZ6Fn2s+ODS1EqERYGdqmtijwfMLd/u
ob9MjwUKza5X6Ms0lFv1IHvZJk/Ud/vq3BX3zxwxeLdMmGp+ARWF/m8vOqK/8GTmvgDM1T2OIT50
4hApzXvDf8+lnQVch7PitvfC7QNogcwOBcP5x0T6IpLr/1A1662jD3H0i4Gys7xhhtjXQY6+ioRI
+uR24o5g7E396RT0P6yLAEe8KD9PZFPyJwzsmszyG6hAA2ktWVIuWiBqSYuLn4hp1awrAda2MTXh
0IO4b+UK5Rkh5l0weGM5+IWqoGOKVqFDMHyRQpIIhSYsZB/Mzu74UcU0b38VCnW71jjv/KTK21ai
z3sogiHoiqRloW/6ld6COO5w8lXYBKQiqQmt92JKGEQ708QsQ2/1PeayvujHx66z6UAByl8e4gqI
Kqk2vodsHGPBpygiM8MFQ8WJbKMW2nlR1BPAlUKHzdPq95WDGHfECMgU1JYZXfkW7jjr2nqSUqm6
2TK9dBcGYAra2MYjZjwgSMuGqY3ELZAW54YcQya6u0vI+8D9YnFBmk0K4DuKH1yR4YkzgH/OJUha
41TkJg9lszq6VtdCIy3DZgKvd858R0JIbcBB9GlWn5SYSrAnmY3M0UglhoJ9IVg1ENyF3xy+p1X8
Y8SFO2BYaEjZ7SEocUIJQ/8HahqgyeNajGzd0lMsMbQwmXqywaqgF+vJiV49gnX9le4yiq+5lx6r
ExrjKh+qmwY+IA88nwXTOM2UJNz4/C2xn0TR6iNivxNSAo6S+DGkGtxPmvqxUIcRpnxehQCcfBij
4jyedCAjFG1+TKcAJ90XClDza+xhQJxldUun+uIANldzOIATpYr0B147/mrrwiVfUza5nAwGnCbj
XYMbG//nUBH/y/KMyMr6NiROHsvpAbPirXb2uqtm3NLRScvRVwE4Bx16GIh1N1DT9rYJaGfbiAp6
5l2U8v7EPNkkTZe/0xNy0BmFKi8GOa23ffT5KRkdTaaK0cvKKGfHwvNCE+Xvs251ybiLanhjgxrK
1Vww+KxehDFTgv5SnMh9q0Fc7d/RC2LeD3T2fzrwWmCibLjoy9lAces9lOZh6+iews88GrzT7nNO
Rnk6SF6jIpKOdhShcXGs16FZPtypGIwK/GFLRKqf3xAJNblTJyCcQo7Sni2k3tOPuNiWfIKlWvP7
mXrnPQzSl+kMCtzkBl31g7LC3P7FJDxQjsBEQWJnl9aM/Lttg/CGzvJnWDRVjSwRNJF/UYDe+YO5
hbr3rbp6fFluC00992OVhVD2joJGirIUfklHwy/Y7eZjVEyyDWkcWi2OteNJDpz7Rch1gOhaWJai
+U1OFxQu6AW4+xV0IigrQK5naK835dxjycV0nJWXUCgBbDM00/9MNPYR6InBasFP76JpCRDujl9L
9zdHJq6CyNHNXH+i4XC5B8TxnKyoMmgVKdO9JQ3e4c3ILw8ieNroFfy/xxxhknCl11udHyMSQQBB
OWau/yDJCm3f/BE9uaapf9GU1Oajn0QvofqnK54Us0BPGIRYueH/9R+yRB3S0S1vbbXhWM8Qe9d1
DEinEJNrPMXjPhSYZbDcVOJcZIzsuAOfwSNjmSmOnNOXpn7fZyzV6c5bWK5Bb35eS4ouYMFLg79C
7XYkbOiWV6cvH4ewgrNDWKuWoQC6tc34Jg8B0O0bZDFzEw6IPYSaQqfTuqPohViwX0HnF04oicEQ
X5AqomqaBcnGyzpCu1FhbqvtDF9gc9ESZPWrVm/lmiOtWxHlDETHnDHVzHRq/A8gdu5Iuvt4K8N8
cBu9//SlUFKVEYSfznE7qAtN/1xzVGK9opHdexAhyyfQle6h3EQ4tXGFUlreC2GkJwv45vtSX9gv
M5q1ltdHmb3kg9feVFw+P19aw8DEh5o/JDAUUYkbOEilJGNUHV4k6av/57wXmomfAYGn2OwMFNTJ
lxtXq6g5c4W/BftBJha/1fc4UmXznUvm/Ouir8I9fDGEe7hwzFvtr2LYwYg7JHvlJkxAoprIwUsO
PLiJSw5NlZxAtCLdknSzMhY1Wgz/PjI+LGYVillzmp0pR0oMlc+EFZGhC7+odzGiQ2wfDEUgPXdv
zPJLyEhKTch45V38KutMvmdEVTBTYG6G/RSs3sERfEyWZhC+HVaeSOCAGMQ7nqS6NHtotVCaM1zp
KqW8G0eCPk4fNm0JT2hb5fxSTk1hwuVGBeu0bOh8O2ahVP8f9y9YOI4p+JU2TVYD1AZxd8B9zhCv
QOWPZmcCrN3tY1vUJnFV9+Nwl2vXMfavohKKE+f+3PugkHed9czJUyCpsCOu3rP8P0mszJXxZuU3
E6MrP6+bcAcbn3FymGLhFayr/eMHSZgYw2jxWYhSQqtKyys9aPlDZrzK96nLBtz4yM1rRI/MNQPJ
4YBaRmnSAzBZP8iDJco/arw5as8aMl6clq1DizNA9Gbz8mzxi7reXqsMYmStzbalfX42OzZErNSP
yLhBiXnngCGAJr2NUX8HwgUtMI5jo82kTANBFWsZ07PpiB4nZfq6oueAi1Rz7mEDv6n1KfpYzk84
Q6jf5BjFAH+765dRumSFiD0bXoGBaysZu1DRh+LRZhNUUAXMdWpCTxip7ZX0aeIdymD6a0axZkpt
4Dq+YD56YMlTJCv/+3Bv1WEkTFfxkGgdsf6zUAvpyJTIcGVmss3P4Lwmt+M4PqZk6YN9muaitDPo
WOKzMXUrd8ZYbR8lc3RxLDBdpR9aoRLfhVr2kELjY9W8+cyYp80y30r7j8MXPan2qBXr8IeBXzGK
TjtGTneckqqNxZZprMRuketM6b/GxNKqrHZGHW4FEVxkkrN6TpUruzsNIl2oBBgunDZ+UgZxDH8g
7GrmRjR+EsNE7lKBPcFu9XbwxAxRQr0+badzXAhPKwljTrnNaUA7ULvNL2YYwhQhvKtih5B6flo+
eMnBPdhqEpiiQekLGDC88c6NODMhcUyMkhRNixZ2Jq19vA7YcFKLv2FBECqMWdYyjQtCWTx9dw7L
o5OEzGuIsz43bzifVJry4Cp8mnWVZKGY+IPf93btUuvuKRocElAYVFvqhT/wUX49VqAhE0oetYke
h0HqjbAaNlCPjQSroZgO46Sfg85o2KIG+u3vQ389X9Sjl53j5oWj7DzYFhGqc2ay7bT74Qh7HbEX
zg75rLerj7t8JUAxDos+n0gSzF2Uk0k+HeWY7XaCnF22a6+z7cbYpF6HYxkXvsz0XA9T7iT/gBf/
ZZcWForgBjnnQjDmib4iI/FgafWVtzOgz/jD/OiMw4jlQ6UZrzTpYx6jUbanfRiKWfLdt9O/U1QX
mKOiqGLRsu/noe3Ob6ND7jUXDsMOgx85b7Es7Mx1SEwhYncpr4lAAVzVhYOsMRJ3cEDanIIR4PzV
DuIx8IXp4BEbQUD94y+fkhOsL6wmnQS5AgWw3nTeSS74vOEyU2X0fQDx6wDidr4iuCMNjRFXvx4u
A3QaLpWvqQVqHaXXvfT5vHSMt7dkXL8okTWPK/ia/v6rhOjFNPpCi6rRw7Ht1OdPqvcjBEEyvg/i
UwpLKOQwbfa9RgRTU0v2UOF94Q7Spm1cZq8YZVibv7dkCEdUKi6NQosO7baYl4WQMEHHMtr1xFiP
DIKx72Q82NjwYy3AY9jVhSUIlZ5sPO+ugCyAN36p4b97/52tDftXDT8rVD9EosR+dPEkrUip5WIw
Hrs/y866G5hM1jgmpyEe0o6Jm66NjXNIL24wm9uL4z3KBpg780nBj4bbt3wLmtCaNwiUqcY0DGhE
m2filh6vxqRlspOaipMG8/tleQXhGzteRm9Ix3ja5+h9WhUkWk7fuHCNB+HupsRZiBaPYzyRsVxb
jCxFcYoAdKJkaClqPbgWvn1Tkly3s02uXlUl/88S9ShAPtrf86sXG7JqtSrAWwVxqWGW1DqNPdAP
+QKuTXMEee2d11L8Mp5837sZT8AIWZHuhATKaLKwT1qNQ7jTPNoIvkkvG9h/6FAdcMVpxC4ERW+p
gMhi/sEfJq5f/C3mFMxDuSsxYFtx5ZJxB/hgh4iporD3MlIDZ6gO7EaA+jqapj1IZsUxHPkEKkEn
GnSJVDyJs+lxPx7ouZxRs4ZERJFvdWZfYELViDTdiCnSO0I6wC37DG8wkY78uh3UmgeKUNTNjGaD
ZhioXsebDTkEwr+Nw/phvgV+w4MnVW3x6WRF+ILXm9IE7GQUWcS0S2deGVNSTeV6ge070xHcOAZH
O6XREIIak9KBGroafpKzpCh0ihY7H8n4vOEjhAQV0Nh42foQSfls0qrI2NfsVdHZ92WZTZBYjNwh
yRRJLlWN6C7eKI8YHjrzTXLIRbY8F/3nH8k9Y+PXhf4rmEBz78PpI+vXDXSlBfG8bGI3M4zE5BYX
ikHylIGJVhTLeeOzISJJ8X5TZ7+b8cXAAewyHt8OylVLxeiz++CY9n5/WCDRhiVYhr2BMFTNNAxG
KSeUCK7vkc9zwCY3oYGZLNCsk0fDngy5KYB5u4S+kpSO6OSLkMuSOe2d9NDKpbZuXHTqsh377+jR
7YfWSu6eL6zX9ObTbKupD94CpFXmYo9h4NdUp8XYcSLahrwPcKEtO9GrbKH5Q4EiT6QaYKSwS0BL
tl5F+W7OKKTMMlJeGydupL5y/ysKi8eBLAxNDTtaXuG8UNSudbvcrbjZnYzyAxKZbHAI2UL+kDdm
imEBYZTNsTOT76WJX3c80uY2V5Ho2hcaej3ZGhVmNouYeYaUCjX79dX0cvWRiCzVw1wxaNTmacAU
28J8Xy3YHXEH8/820bzWcxh1bbmRjsw2bOux+GNhsS5QuNAV6FX1Z8Obo58pMMJ/u9/5OAM5WNsu
TBqGwTwxYKXlhXJg4lpbOsCHloV9YEiOZ5TspJqVtaz8SmQo6Xz+1ZCCLFbIUL54WddbzSIRGSJ+
lsMoLJadiKGckkIS1+t/HTlUqrpYYFn6TYfP6TmRBpCB7ZDVfbJx1w3U277Gc3OCMWhxb1Rg6wkf
U/r5EVNj8QdsscvWXSYs65/ErTS+VzlfOnBLtCL9XKImkhrzItDMPx5r38XDieI6ozWPNHWdWMgt
p1MdPPKh39CFDfGxKFPlfT1kshOBdaQEqJoLgUWg7MOKQj0QABwSciTOTyEAGx5dL778LiowFdl1
y4Gq38rCrZyUSTLdAC65ExyF45Oq4jdGqQWAkHR4OWjnoLRZIbCCRM5G8a3TkBLz9ggndEV3603x
+EdKCMcIzEIZ1wuF1OGf8X+hbk3xD6i5XHYqLuw7jP8FGa80YOeojGGI/czKkzd94M3aJVysDQ0J
y3o6PrkiFKtOL66C7fVH0PPzCdQcDunuANCjnYNA1AdUNJUDzZSh97tTrWmzTx16PA0+YszVBwxN
vBM+cVA0mwaPXM0wydUdhgc/9j7r6//1zt8OwTwaAK/lmCzQx3+Wn6GF5ZUoNeTQQcFXJ49mwFUa
Cr58vvHX2a00w4fNxa3puhWie47/2WoTgeRjlnHnUPGmBS7mpfRPnngfUxWmB9KLYEplItVsqQ3a
8eMVaAN4clZ6INwaRJFsBd3AVTmeTjLGuUNosDkhLjwcYvqL5ZR0XlvpJ8sZm6DKypOoDqUNmFST
wwaR9WCr8anNSdwUUhgFrkF25cRvQJpw6WNGkJZLb5dhRvpJty6XfwKbq8GZrVaw1/4SqS86aNV1
ghUEAOpQ6Fz8/mrx6+ppnK4lEOhsv5Nm6vaZGLwCUZS8nU9TZgnA7VzW4JArhAmlISrQIwHfZ3cc
5nMeNJXmyr+kE70OD7YuhP/AxbZ1JIW8hW5uTICDKWG07HblC5Y/I/lh/9XmZIgNoxmcUJBk6rq7
LLGGS00ui/duFVPa62dEG9RJ2VPh2KlFciqNLV8jNjjPQSAp92HFHmOaSW/Mx3p8cEHzDHSyrywK
a0u4hH72fmX6hU3UXQs/kHKb4Wp/iTYyJe5iRhLMf+v92SznpquIzfQI7GaSuohNB8aco+nWAeM8
r0BzW5NUk0zVJ9Fgxoa1Q29rb2sK2688Fu6Zm+RS2zXdR0JoL4ckx3N7uEkxZffMD2FHfXdp+/0j
SOR5PZVWJ9gUi3hXyHiRmqB6E/kukqqaHyQL7qS3l88pKJ5xNC0rFgQhFlTNHUQw2UGE2lRaKVlf
tDfPZVE5+ZtTTI3lgZm96rI4nJQmIQ0gP5F4/6+jaLSzMqXhBIrrdfibuuG8+Ip303bzJ5e7hXo6
oFSBFlcGrCnZCEA61zr59a15E9H2PC8PHJF+HXFiE2GMmrTf3LIyqljIqSRRF8NF7YcBYF88hFSP
jrR8ZmbV8PIJ7GgvXAili9U6tWZmH0q/Nos1bSFI82WwQM2p7f8IshX9j0OXUJl/mT9qMdcsLSdU
DGfBqpih02VGZEgpMoyj4kmLg10/JjFQrHGJEap8QmbC6Kh9+bdm3d3c7g6zt/AghdL5KbF7vg+C
ofuEqUOE6AGhejAGfUplrdenilgEIgv/HlNgVEl3iCS++hLVEvbAOZsajV7cHRPLo8ty4i7lXPs5
21yXdTSi2cohbO7T21n86SWQVrbaw2oJIf9Cn89itYc1CEz7AdssaibVoQFkjfJf/Wdg9EbgAvSa
s/kv8hU5qCTu7A9OaQIOPvXjdp04L7yLvk+LVibdksZLd+pqoqD5e7Y8Omualrn386pMhUxyHr4t
83LYxdb9lsi7i5G6FOx5FKzdDxywTH4W+D0AJ0IFPDLYjPakS5nyMOOXB83v9lyq5aEWFNi4OZKp
R8xtxLHfFdGAV/L5LV1yQooCWxxL2D7M6ZC5E7CPhTBgPyjBTUtJh10DjNnrp5Rb9c0pf5FVo8EM
7BUVDULTdbivL/ex3ZdV+tV2daBigxxHY6nGo+zzgfDzmv8y5qa69/oZBKb+pW0M4f5LaOdeLhDz
5GjkHySiLoeTyh8BlkW54A05VlwThc1pbNIBFzNgerT1bwDsrkMCMg84CF5XI1jOpbYH6s4GZ8kA
vFzfaNqIrgqrKTl1d/MLyN8PCFhSBfBba8VtAMQdMsZ1DQ7eJZ2rnolcxktLWIzyTca0ozTkX2fL
ZpCAOfY+KZcPN6ktJrnd+0ZhqhyNhGdPxC1JVeWns0uJzTepHg9b9KRbxVAlKcqoT8FJZnO2bE+9
exvA/9mlU3mbBuWTKk61zS/RCn1aQV0BZnHDenxxpbJfIlEXlChzI1YtAJu9zgXpOeHjBwQPA/eb
Lpb1J+3WVUjYm9SHD1qawvA3pr/Q+AdYmSviuWOKOAt0L5n6xyc23b/GyTPjjZ3VgaRnrvXMG5e+
0PR4oBU2CxTyaQUZwwkP2QW41slP5TWIOG1jstM4hGZ4Dx/MzRTXTjywfmoEZbLn4YqZVJXGNP3U
8+KXnVv+XtgOMfPlN27CZALVHPR66z9st0isTSMEfnlR6YLIKMxqBqOp0ZEZlJGIVL5oK4Bu38kL
HO7VtoKHY2Cp2A/h/GmcW6W1I9MAsNz27NWb12pnw5LhAsUnmfVvtY+dlBmo+Y82+sfwUz9tTFcP
Ueoi0S6DG6vy1w8+OBvo04brSGF7P1lhiFrSz67DsS+Gh5lCekfBUeZ3v+tyIsv4aBgOT4ujJBfd
/D9TQiYJluXBwDmHwJoSwmmXGK1me2AfPGjxzDpxykFPFuRcDoATx+Z7UGvoXw1DTvH3VNT8wdZo
yT+Yk38KPlR2aFZLE9aAICyapSzkrVlYa0/TMZkDYw0mNLP+xt/OLr7yWjh2Ao7fnttjjYrfBHiV
4a/0hPLDodTw2gIm+s0ciegKKlBy1EPWdQWOkflMXy7Ff826YZRk0d0r4MbgEUxyblSOa/yiHmZG
BOsqRxmqBImL2MMeBYQxwG9p6HvaPwpj8JO8ZRT2Ub0sBJSPJK3ssch7S57KO3O0OY7nUQO1DF6A
1Br3WDKO/IulsRrxR6/zhWPlJ/PxhdCnXjtoHYTs8NKRn3ayyHFYCPM7rF7fEzzhUO3rtH1aw4Jw
YjQEBZH984BQaNLPbOAXcFuS7aRVTc/R+VxMv2BajycEGcERfF9Uz92/G0AmH7arXgK0Xlmb9xtk
/swPaVxOah6+8WalchJqrI+p4v5af6v3OHLQ/+3a9X5muWeEPkxi7HUTRX9+FluUl6SHKn/++SoH
MIOvARGdpuGiUcLWa/Hfzk0c381N5bWcyfkcRbFVrKkTVXJFSjlvboEBJLyY637TjhuV/5+jQ/Vc
in1JLbwLW7Y6gqVSzDqD7rejUwCAl7zct7ndvePJ2GRxP9KaTyEfscozZSF8qFkzAO2VBkDkkErj
S5eYjEWGDB3a3SLbqYAkzVFV65nC2ePrXnAL/EfmEP8PfR4I90I3TimhiunHNy4x3dpIL1yx2Lz+
X4u+6uCOMPkQdxjs4SNC9eLnkrDs6q8aAMbGSN+/clncnVUc1nhRw+P3G4h33AHgDGPxrdb95Tl2
5y0J/fNJhMGMOEdMtPuacxGNtOEcKTnwNKhCSuKK7PWNi8bqn5s49R02a74LQuIpd5n3DvbYfMRl
8RaJr7EJz7GrufXalUlTKpzSIkK/PrUjEgDxc0WfZRabNoyMbkVu5gy8eucHRfFIY6MjnLOAZikY
B74pA5I6G45eTvANZKkbVi7sVmRZPJuGO8iKHCxTTmh8f87kgcFBr6nA9UA6yiiEx3pySwuZKvar
t7BYhhH3yEsRJzz/THDZpoFYPdlnNZt3qgbKcncm85jUhREt3wxU9NFHaPU+/pwMQyM6XCiH7vIA
UlJLM6DHvYfzko8T93RS+mw1hv3384OJSirzxaUDhRDvCMbZpqRWstdAfXnM86MCMsxjufW01ncy
9ZaSid3zVHLsBG4o0TRKj3PGGTzacw405RNgvcWDzFW+Vwz1tmJVSuUr9CLEuYOd8dnoqa/TVWcU
W/s9E0FuVJmk1KIdbEEVPm+XVZPUNtwyai1VzBq9c3jZKlwJgCOAAM+/mKWzrwp7fvV/GL20r6bS
MMQFYLPIS9oMdU3CUXOYJZMNbhc6TVfkeAz5dUzXlE/oYmAF4qj2ZTtHCwFWn2JAATUdePjON/Pd
JVm9tdl3nkhUJipRh6Jt5I1HKWjvsTl7QR5Zi3ato/HncwVYIRzjgJfg+R0ySc42+bg7qYcuneSx
k47Rm9cagCTCE6+dqggC+yVk+BiRCuU+vXcIfOPLFtpjkvZCCHlPbNEgR1aifxaB0arFBrK9gaPR
sAh/sHvHhHnjJLnupoCW3/6TV0/OIS2k3/ux6gKYgtGBgQIhaiDSYbvKvd0h1XJ9zLgTypsc+M2A
1n5sFA69uRgRmHrCYsemcPjFkyeWgsSCsFxxtOuCHWjt8u7L9ExRTZgin0uaGEi3D+Er9MGzPLTF
kxWFFwEwmRilRudm+0ThoBnU/JUtbZUMWNN4FxMPvr3jEugEScmZrPOzl7XKn5cs92NwrnST8Ief
SQwx7Mz5O5/vsDFkDQAFxveBm9pdvO3bvL8e1PqoxZiEXQLQLZAf8edd1qayd4KOVZ0wXYwRTSSN
0U5SLg2Bi7QOyXJDbMv5mHJuEVqsvaR5qyZqPR9+hXWIt4ZZGVKU6Y0FtlBqf30t0y880mvJHFA3
opIa1fuPy80fok2TU2uVtfGR9epaZuZ7GG9r/jxfBVCEGnvWMzCjudWFV0MFsBNe6IZbjjchODPs
VB6MTqpLJaRMctmGnti0C8dwwiR+3+FfOUvFjLO2udEuiK09HCZUs2jO2uAU8ED3eM955bi0F127
PKP3g2WiVZaZTvWS/n0to4HynLDUdARqC2yWShF1ep7qX/cuR0H8MNc5BLQr4voEHnxL1PPvssmo
5xc+bv87jb5CaxqDXoE97sXgINu33asWrc9/ZoNmC1IaUkfTGvbhoPXX69ZCUz2QBA4Rr7iIfX2T
XpilujuFjkkGpXP0MjCU6+V3gNHnrWnno5R645E5s4w7OpPCu0H2Xar3/kQUUwEdLsxvSli57WsI
B3CDkEFZlX6JUNz+YKzDgdnQJwI+tc0A6Yi4KYuJd0zQ/GEhzV2FL8ez27NncOH0J30lHOFdc5l+
D3iOO2Z8TNC1rFkOU4W+mHjTt1UwL9CrsnMVmXSXVzQPjIHAuzxPS1tS6x61jlH9f8PYLtIQmnbl
+tyKumSfpv2ocQC2vpqfn/+AOlufiDk0Y6Cv/Itp/NCOnrdf8Bqo33jD3CFvtrSOduX8X0y5BEGq
qMm4GkW3DUYE4N5LWqsIYhzb2YyZZuFFIsp1xNaBheTe5PXGsxQy0EpXdEmxZHVUm04A+atp+9+X
ciclJX1qlnViE5QwdlW2T6tHrT1Owx7SkiSIkti/sDfaHFbeUeS70lZ7suhpZARd3KOFy4/8pbeV
L/qLhAg7WYCEltXgBCbuAfDF5bBsBvtBBW2IW0xzeDVNLY6a9iaJW9G7IGdJHJMAIu0dYwNQ6WAY
6crNMrF+CeTRmJvGRpX1B8ocsYSPeWzroHD0Evl51rqvYex9BKyEUaiqxUlbR2hIIieb2OmQEb2C
sA7qw/tYAUbMYmkD1PULUmKJp4ecu6PpWiW9T1pDcRQNUepo4focnh8EuyMByghg3rlvA3dAhefl
SFc821aA9kghgto3++NcoPQ1e9AQJmioxXfrG/torkW2ZyYsW9EUDZamdJdXGGus/qOszzOX9iAB
PGzl/sk/o8bVY1PYC2KRD8gQebjDsa06S4LPxnf1JsV3F6vioNVuZwztE/y0zDXZ7tAAp7t0sahF
Vo/KMbw6ULZCCtD3vNL4+/1wVfvh72555PPHbTIamKTDR+bypxEkAdqpjyydFlJUFGbozoQJETvk
oMCh25wJGH4fkt9l5dccQpqe969Vg/QdMmUvnsb/ip/BdCCsZU3x7kwaz1K6w01nzAq6HZ/vlC+b
8GrvOmlhsE/s/0zpB8RxAQOBNODOeCN/2qgWOwPZhWZds/s7tyT27XLREAJez8d39o6qXxf05bgB
SeNnTJq3TAvLEnTSY86OpV7uGU7Y2ZEhYpqdHwS/ZwsXFNi9iUigGGNDbEjT0MlIX+klmnyjgqlH
5WKHm2W03t5J5xc9oXUu1mkURQ7NZFjeWdz38s0i4YK9oQ7FxwNIt4wtRJGnxOH4Q6FmbiQSyFqs
NGSiDw2JEeQBDU8GjqvdwFULDxPGSgUY38GRES4zCg/SLdhY/8fj3Jbbbe2PTR7HznYkme/sXq1Y
lg89L5/EgLi3MV6OZ/bJ/0+BajsXZkbJvnm5+nZWFCRyaYY9b1LsDVcxCNq09fz6ngUdMCtJCjzY
CBj3KAxFRVFi4Yhtn/FkpY66jTE8Sa8qQiuASmMV7SOujwMsgCdEn7LGE1gB/P+shIXKnjzJM8oo
Yr46r5cQMVvaRFYfQGzTBSMIA565Yqr7apSFH65NCqGZlQNdD3/ZJ+H+0xl7PlmG3yXF5I5SZ2Qv
29a/XOIoKy6F6WpNgk5gNWTQO0OXUxz2NbnKHjm5Yn9fh1ypTq3mZ3//NiRH8wVGeP8S4kG60LRd
f+jOnwwiDm2hZxs87hlCqSrPS1NpI8OAXRjnaW+V684dgwoZl+NoFSiOss/vsRzt/OY2kg86ZIbS
BbukLc2PPXmiyGsTiirWqJhSsUK04WoacNCuDqQzETVGjr9NSnn/Lg057x13yTRVhilGgx5dQ3jt
kU5xRTZoukp6oyGENcZoyZecs2rBa+aEkyVfR+l73dLOlXBCy5vE+6OZ1y48Cvyh1kQUbaVjxC2W
JncdV46+M91qqEBRxytWQMIvCQk0ShceGhB4KHW/qwzh9h4bM6mfpOHD3/cGvKLPVT9PhmPG9I/e
/7nWn8xaqxZmUV0zEgHGO5ZzAW6yRKlBPUpDhVDdfyho7NXPiixpzhyUII7CaoEihC0TfsuDqc0Y
DrZotRYY1FJ+pxBovFLMq3MwyboiBARtkex7DyaB9e7bUooQY1P7cEu0X/hG9R5odOVmDP8M5U8T
+CUtpbZ/nsXMPngRmy3Vk5tGFAZIsU0iTgsGFjbfpAIsSVXZlHSYBQFMM1pBkuWTS0pqBvrJqhex
HCT66xWc7gD90KDMvHhUiRWIh8eRilx2vUjOkcnS/2Y+SOI9pQuQiDUJje7AHKgywtPJxZl6Lu2b
L8MC9I3fw0BA0znv3+zeH3XdpqQgswb0cOTIxcUYfl2t4UzI0gMrzryDdntZ+xZF7SEoKkz7fkzj
icQJDE6zE6/MS7JDWDxJFUgTENrbk33aGSJNK8SMr5aaB3Z9zJLGx4HqWnvIlY0nla/7yu6t+TNI
k+YCTQVFugdQlRKdU5wDcOoxWIXAXKlzGlKNbE7134jBkDQrNAL+dpvcrPDbuXY8hju7Xps4R7UJ
h0JcGcQVXTW1saeE/l7jcTXveb3OQkHXj0OBlzb91ko58iyv8iqaU6Sv9A75pSHOd5Lk7h770YKq
RADKyQ3prbns1joiP72xpUUMchhACfYROtFQopmXZ8dNUoayS5DwdFbgSBcSm+v1ugnBRPWPgWAx
9TrYgxJcmTXgKEX8zlJY7wtHGb7LdyTxlT0Ie16+Lvg0WQwdn9TwzGUchzZc22ZEdVFGsIr/bSOL
rlIED2fG82I8B6DE7mgqXLSPoLnkZIq6JDWCh8HRgA0CbPV+Z+lk85Y919EDGe3H/wi8c7tCXBVJ
a2aZPcNeXYn78SxEtJdjueQ5FkUvXVTAcFvtl3z6zZyj5emLyZ2rbYDFY79UBLNwghLDn/hGE4c6
Z0Ywy5FiEXf7M1gjYGlO+9BZ0eo2BiJ3LHXJIOfJpDY3Vc+4FxOa6tbK++kgwl14fdKIH3vin8Pt
ck8vtvgJInQr8Pek31UycVAkJTUy3kLGgkYomuBm6neE5WIHceHBe5ySOD3uecSgoVvw5zumAUVR
/VrdB8G3ycBlj/cGqOVV4vOfF2n/r9PV35EnSDd7F48itcJ4xsBauKmjVBsHCDeJr81qB6I9riB7
NjClQXOQuz8dRUKsUsNbeSHWGbgOXNxuoeq/3sLgBE0woALsUI6F5JDwAFJeEYRNwPg6Q2xl0j1F
/ivaK9MNbEGUA4rAp7J0dbxTz3iTxCTlD6qm3yYna3ssNDkuVIWTuDgMjy+zWYCHDwZtPaiN0lMN
tc8iH+YtbJTFF2MPmTNNZXrcC3p5+EvbXoM1wGuW3J/ORbo5erXnEPKba1KpTNePnR5u+CXlzZPY
AFR2XTrz4j2HBND90jTsnTL2zYHB10NQotIt+tbMPhnODnfZ2OvoGgZgfI/lT53Jy9R1n84ORxlb
kNqwwtRlplUig9NXgWcLFosHGkkwISugoaac7gvEevh73eOiR3YzbSnrWNAbuL53ybjUoFOV7St/
XNdu/4PNUGK6cM9k18kefil8xy104pjiUqUf8b52Oj5tcHoGApvsUZTtRYvXH8I4YK62QOOy/w2R
Roi4nB1hI/6bZ7LEokqQudto4FPv17MERZ/KO1pLAulEYpo+OsLOdyehylww6sPIS9kaYGUZzH1Q
iW0NG1GWND5mM3OcLpFdhqQi3bgFVZnfOy1IW2hmQMD8Hqu5SLgAwanYucg+9L/mtNVSgG4EM6MF
cMuoz7hsWBUjRjnJrekkIuT5tYWi3c1/3tPClshK6eIkw7Itmn5pRfFMo/Foc/cuoXCOJgQbgSLx
kPBYNUjQYEQr8Y97a0NQ5qZL3zpO7w3JBGsCME5gssyuVqqIYJjrQMmHv9DBhndsawAh3grgGzXz
pP1NR6g7xC/5uLaDcCQ+TvZUpnu/jtyE8I2gkom82rNG1LTvDzlXpYTi59Fe88BET8EUrJjNbudV
qxTuLp3pwGN0M+OBsdbMc3V3sjD0bjGohiqFLvkSWUTva09qdacQPRps/bMI2yrj2ZUeeWlvvYBw
a43jz6ZMRIvGi5wOdr2lUG+upeI8M2vmW84bFjEGbhlz69Tk7B9pP2s0XvKktvbDHlxIYN6aQ4/B
hErNufJwOCjxH82hyAJo6XjpqgELKheZfd02vDC/i0Z6ijN7q36dg/SisVrKbGrdqcrKJUlVH3v1
bX9rPeXg6u5CrLU9Vf8TKSlpLw/nLn8wMl6MtsxyTXcAcaDjtT61+bLb6J2LWFZlVyIeO+J8YqA4
+dfW0BNJp8a0G6LBWqoNxGz9c/Ag2zcjX6lJR8X1DK386g60LFaitHuLm1disetzreUrHjHkPukV
UhaaY+pvOg2zNTUDdoUuW4YUovuZMZul+rpRwScSJBQ0N1Y2ErX7iPnnir6bUNUgalGU1TcbGl2k
qy6Vc+MlblTPsuJgYO41BetYEQ6Xv3RAN3LWpCmrAdtkDGxDys+3Xv7MAb9FoE0Y8leLEw7xHZqZ
jhIK8GYwdpPCnecK23KR1JjvlmnOoFW3PAzQ9B+u6n1V+yNeS9uZtuYDEkF8Yabp6NRa4xC7SQ5E
YZXasoMGgN/VzoBwHA3640iDlNWSW18CCnRFXX+ETR96zU36cTGAevq+gcDuULvDGnp5MhjMbrUI
LloxmB2bl6SsrxfFMUuEiS2De4vx/iLwSliI6ZRgl9B6H68k1tEBxEaP/DG1cuGuPtLia7lQy1Ql
ux6CKPTYaNUYjLweiZ2TTFM5UXK5STSbkTeBn8GVPyZ+9fD6NrBn/zVcOnq7cD0+dz5VaQCK8Zig
3kDcm+64DFJR0Wkc/d2Xaiqg5t8fd8GGsryAHkC2nxiZN0aZh7cCYbJLUJ/H/2HLLWyH0s+KMTO/
ZqEeGuvPct42lCV9uwxQv//1IYdeQqmCuM5G3xt3ddZngP4CmU0KqcV9Q6ZGG+b6xBh0P7QbQqr6
tNKY0chGhu5vEN4OAwqXUwN14YcHHWjmZbQHd7T6iPBEsXNqJfmkN9izCjfdtxR/rokMXJjXugIu
Tn/d3a2IV6qYZYh2kagKjUig4AIdn85K58tAyMrASE4LIcCrkgdQs4ThnmKoQL55QdCcEVJHSFLG
hqstaGpcr2H7aCtalQSFstpWf5K3fm9e/tzd81upFV/mADZdvyBUGF73AsrYfbWFidgV3JaqjI42
/HzC+VHbj2VvcwUY12/Zt0gvuQGxeyixk2QEfHvrHK+YPsOx3XwK+NRhGB+xjWe3W6tQ4QTln9dq
QA40QZANauqSbT70EHpuiBCUwCYtaDmHN9sNYCohvJpKluxlKIBVqNDJgPeXFtU4I0WuiZGXzAbJ
xev/zv99Z7XqQZEz2o73EV18XvghllMa5OfBCwaG30PSVlyTwPwDS3bHOPnSuJKWy7p5TXgX/O3y
uCIMUfEF9OhF8Zx6r8GrA3gVV5voFIbtVJXn7ehVpI0CXM4CqI3bi1fV/KZxksdpWA0HRslYEd3H
pPUYR1alKgs2d8cQn/tELiHq/ma1AzJjmBDLYhvALBQ1cU1FBE+lJDJQb/FCxojzDkp6+ssRJsl5
B7dV9h3mXuD/FWBNoeV9Bf36Hy59UzRkbn0oz/7EUj2yrBTqUrMhlrstiWf3HHwCE2inO9czBDa8
OkOZoyupywKdTY1OuwfpU0IINV+N9Wnt/4qYRYWBM3LvPER+T/vSfWrF5eN+BRRNzHGvo6qVV4J2
fAKwrkJvA75oFiq9YStuV0YI/MaVtDDzCnbnuYtDdvsYflu//cp2XfXT90jwZO4wghsPctqe4ygP
BY/oPILRykfprvoyw7qQ4NsTkWoD8Kl1ZXqChRqq5vGvcqXQK5rfHIshcoJ9rWJyePDak4/sScqW
wm2e1TLWdWdI9RwIFSSYl0TqwCD1grrU7a7QLcNupn11Z94GWz98w/HyIpjXLTdiBn1ORif6Of1V
pEPz0lNn9sicpsCjOF/BdH+lrkCQhlP36w2mQPi5DNyvfMQ2L1qXlTlmL8HCZNhAVLRorb9Zj2tJ
9xS1uIp2M0QNhqIlbdC1IBimNfT4lV5ayYrvj5FCm6wi4G1azpKdD4jKLhe6OpsDQYlh5w8KCEqU
aNpCXoMloLCB+UvI/4aPDeo6aHwEfzYWeYWYAomYiNpY2X6vSE7pmxSGnSWasMAQkXhc5zw3BL1A
HOBXCWqZPgLRJuYZNPLBWAE+l51F/rmxCGrLuYmreO4zR7U0nte6oZinbSJzcS6uO4Drybzy3Lts
qvoNAZJHhgWUmYpgXrMUqFKiG3IbCWnX2c/nv2rgXLEus8ynZLU037rY3rZhmKbzDghESdaS/ZbK
jtz43Sqg2svmfNozwVzFYHlEBPFJPTDGUM9bSKb+HxDi0vp3amcL+D8kddntYVNBSk5fw7kUFgjO
gMuBVyG6cDM7vvVaQ/lD443UZiL9Ul0cPvRkIfHMN7+8RH+qc0AiM4UdI1EfesjIw09hWuNJT7Qc
hqBCzgu8S0R/yAfOHvO4UXEyTErsd7HeazrlLm9D42jmyU20gvsF+/alyaay3rpxTJJlTEtkwD8M
vuT+yeOoHghiSBh1aPYewE5YV9+QW1xTxJ0rpR5pkxRkqYXUiqsFrRFttqgWJI2/CLvJwDQT2Ns6
ruKrrhBJ5g3aRqESsY3Mg/2LB6lb+YawZIlQh6GmUjtRFWosaXzEJrnDNhhBU41h3DDw4+0jUgo2
iOeX5UC5tn1nj+/AC++rT0RVnMSRxK0b4xGPTsF5rLTWIYzjTFm3JMc2BxdX4+bxdxPgPg0bIo+3
xRjGAjOtLaMK79yQpsj2FMfUCf93tpU3g2KBjlllq/KYliF2v+BzlrDOH0nvTuNsEhYzl+Ry2awt
7CpaJWj4kASGMY14Rrcf/xu/bD4xHIMNeDOqIS81OhLK6aODH3gLz6ad4zqjEXrkTvpaHF+JSxtp
faidfDVKzVti3pJZILhl42avnapThM8TXdbdM0S2gXIAj5ccV7/zjSCRAwfGIStYQy7WSWMMvL5Z
OQJpwOvdACyJJBRZki9c8lJ70OSc46cdOwzmKy6q2wv3c/F2J9APLOM9/v7NfTlEjexgxw+8LMMk
Atg5N3rmRzDq6nqoDkC298XUIVi01FthL27rmSR4evvTW7L3wiqyro5OxibP7yOG3r5OQE7UmKHC
EhRD6/3m+EyKNbXdbcQ4N3NSJwX/hY9wo5giRFgRvrcr+coM/53Bw0XbGvlWeyeyj4WVq4KUPyJ0
blVaI+U1vON1IvG1GdaUB6gZLc8OWDE4cQ62+J+y6bGSoUnRyNUCoak6KVAwp3iQAyaed9U6z/Y4
6PWyzp2ta6jgiYl5tz5gu1r1/B/V5ysa+QEkNXY0WfO1wbfK1zwEA+yxPo7jq+owC/iBYcwiL3CF
GO1UVHyFCefPLJSeXNMfpqFm1OCVeS7+q+KCZU+0FFJhQY4aAdysaNRDxEZD15gIRo09l6/63Y+v
7CibnpeJ9CrqZG60gQGRJufAAegkdCI0nrV90VSDfReO3i4NvXiv5TNjbVNKZfVl/FC88HeaH6VR
86DNNMlPVAlIw97R68cGfzZbumUy7PSmMB+zortHJKT8XdhKN3u3jooJgU3bYJLP5HWdDkRBsntz
/3A0d2+mdSO+Yjv3IGvqnTY4XRNU54U8CiLaaTxPlqmSzHLt5+Ywi1dvRFqRHdSWJhhEDuNOmpWT
rtbsMRlH+j73fq/jQp7aaCnCQSeBkAh4PrhIE4N3XAVkmZfruoSmoVwBlsz6R2GZmKFkeIuSUOnn
FQuw1rlSnWmuew+Xbdqx6zMniWeOdpPBf+64fLLl6qC0bnx6xKX90VVPgHgxdhR+DAfLb4Vy61U+
XGiidv5Fl/w5zhAD3iGXoCelOsVk/G7qf/kR+O11bSjNo4G+kr9IeNE7KbG+FM5MzbdJ6wzllGX6
Z1Beq3h4WrdY5iQFWQ59nxjoV0jusUnFgTp0iSFftzMe4DZ9AvYq8uqc6/ZqD6goe0SnAzL2m1g0
N85BilML0PlAMOVZDHSDa16MIlFq1OoBZ6lWNgWKMZP+q648ghsTg5JJJrStlIMfoVN6WZeJOM9/
E1T9h/BoWSFTbdUv7PTuuzQRMoM4dcmuzd7PO8ej3D2X/XacQNxfyTrVRNgt9l6dTF104GmEHmgY
8/JdChHho+vb1JVLGIlPCjeF1T7NsVbuHRBcoMda48wQjWqG6476q8Lox8xF0jI18/wQfu6ZTVsR
S2SpyXZvMXLIIoRogovOwg3kvHjI8CvGeD0lfpTpg3L9f9CYVS4vIm+KoIJgXiGtZYSBEWvtNxNf
wztJmw4LaHPdq69roFQyXs6yHJGgIWpqyXKhXdBkzmDJ7dtUGUI5KVx07nrDx3C1KxjIxd4aYNz4
oLKnmLNWEuIHyBReHMSfg1vSqMPJWSp3BNFXnVuck7bns6DDWwWUqQuFm15WDOud8bmcv5fRfNmS
4bCEN5h+mseL7+w3/0iTOeErUn8HLOhgMSzdJ5WscLMBg+FUtvGMoq7dsoTzVTEaS/JXC2fOD5gb
nfDHpYJYiobefmIPXTZQckTckyfiONoY+3ypBfWpeEhs6bF+MmSVVbry+XThqa+MykbCuU2CvKCR
HbhhT6m1pgiwqJ5ulG/Z2UMa/cauMFJI5WvD8FNM4C3tWYiG1J+Yha/Nowh/hmcxSZiRL5+D/vnC
PRKEjS5lGza/eZPcnAEKgH/ErVgQTc+aZGQcQE49HqTU5mYRklJU2TqQRexoO56V30Bx+8cImsOO
2FYH+VxTJ7HHKmfKfMTrI4OWcatYFolZFhdW90S6vH9hZtGzwUw+Wx7QO22kMblbcl1Nxo5QiSHC
dldv1ia8y5gsvjvT57GFhiYjBm5sqp30jI/vTpTE3SX4Kb+IdD/8stVW6RMvJmLbZDNuiN3t9Dfs
T8Se2vUjV1FIiBkneRTwmSjQ1DU6qr8sdP6gVVjZv7gARjMpdcflvlNFbZbEKME2ILRVedQqW45W
7qpPavblXaKNyHHMaK2Wsag1kP+VIucGZIDOsYeIkxcxgwvervJKdrjV+0Dfa/7rH+Z1DGntJps+
eqY30I3npsAcyX8fub1NoMaHSGYFzu66Qp/Tu2U2kNPW9hwbNLLH3N5qXWuAEc0TejmVDwR+yc0B
jyoZpTa2JYuSTXWPhuQ6EluOhi39HhRVzh8tgsFzbCENioFUOagYnhXQ72Lxj5MkRgoTe+cj6JA4
Sc8ILOsvZsU82spiqI/Ow4YO0ZvfhJ3AIbycdsbjYmdR1pcvK9ySOQ6cOMgf7cRkEdZTp66hesIt
iK7JLHWHeAJo97dMQ7obR1ULreDLZ+wNQYVol+u5/BnWkx6pcgcm0SM60ZgG0tyhkBIDeVETeriw
JKsS8qFpBcw0vyh2mtwH1bDmn4YndG5/625TnFNuQzOf/90KkvQySTrNtxdAZeyF0I5DrosFVqAv
QC2W9DnSD84HrXuuHS7zUDYLLR2HWrCA1d64QCz6u8h/UUTcqiBTaelqG3EudUdH5j1DlnMYVDNS
sOKfjboT0vDFlGKYnmnuuiW4CG73djfHudLOS+bL31ed8464rD3POtre+DX2Qpw8y2YH/IRyV4Xq
PTE+bnEE2DULC+D3CWwI1kX/82lRpyciXaAXBax1S0yVaJBnyBSPyDmjJbAeMHb22pm48G7oZFdC
ksG4jqlSoBAZKY3/uPVdTkDhust6Kz/3b4jcZYCHnVvViTyskh5oxucSWMSuo+SjVZF3xwcx0h7t
QuUttAAxJQUR3RuMrFUvliTrmk0jyPGhgJKV+Rgu6Fm7Yx1Fe1MGzoFg7zn8pmaFnY1ehxyf7IdB
8VuogpskepiefJwYWeqbCEE1CCXAyOcXR+oGRYalkVdtEOgztbfXl6/x9HcjWbOifb9m2a5ivfoC
x9WVOdi7cECqVVDGB/PKgNhx9yJ42EBcSvBqCwocGZsF77DxY8OdqJ1Ut92ScaaM9TZh7AzK98Bx
09R+Mww0Yiv/KJe2Sg/C/9F47iohMuddw/UZBlpPQcH2J7TraDcZWij5xme9S4irxeb3HWoJnODj
jeoukFJLBPoLtLmpGxJRABQziXdXU9gB3IauoEBHa0P+L8VF+KaKt4yGrvwmqXWtMdV4jJjm/KWX
5y7GRm5vogDVXfhFWcw6Ytycer/6SazpOiGeXwDI5pyUg+vfSovvzscz497mChiWeuvFwhbEvFSj
lW5fhF+nWWVxsvPCAyANIOjzBDImOrhz0iKrf+M5Tcwcx4H89koH29otMXYN9dVuUSRT+CBeC81a
Fa02sSqLQacBBLIwW7aRBFV/puN1jsGPfjXPiTbfJlQRcFWO1OAibDYB5shB+/4j5Tzny5E4661u
Zz/geq5v8mPf3oqMWO+jO1kN9k64u0GgQqfho6gIYN+NziTxe0/R5F8LFDJ3GuoB8WqQEcat/BQE
jFnTjY/ADsZsKnYvilFaAT0JNI1DHlCsTZrnpU0FgKZxNGiRVbWdTj3rOKPYTw0PVZsUR6pb8UqY
sFWCXlXNu3e7tQ0gkoOqwi8DRSVt/3JqBulaLz2SvzJuvxmj9wmqC7CImgyy729gGogm4nUGvD7I
4ZmnObIjfOZ1iAv6ARAbT0iOfTHR8KrpcT0Q6xbtiLtvxtutsqd2Wuobrawc71NCYCjge3qKnVl4
QGs+CuicFuYf0axNFmWfFbw+jkTHCE19iai0+7tkGsSdJROJiYOx/yu3Ix+kSs/JLmUnLlv/s7qj
B/ut8Ak5f+jP1JoQK5+uIB34j50lE1d12W/4BsXPQlad7Zxvqq8EiTi6KWhURRIX5oIxKk6M9I7J
5Pa4VyDuMzazJHD8PN6OPreecsZWc/92ncOeE2AgWLecAC+APSmTUzzhPFgvdeYdaWM1n4/ctIHp
2CzkPBAi6jOK1XvmrwHgOeHut8vaxnzPmZZqU/+RP0MP0x9hG5pmOimzocFHRAOsEFsPtlzoSy1X
/8VyH9L1DI8oMIeoyS+uM14ecmwqo9HzJW5j8ji/HDpkqg/NZShuTjgXNUmqLaMt1r9qYR/7iNOR
TKrHHGQeY0XVkDqsD76Uq+YXqApv34+/waFtA8KasQvQh2foLNmrrrRgRFvqENY7fcNUkqLLEj3+
6qxMqGLmScGVG7VNWTn5ioceE7klgfX+fFYeDQe+JKcIaw6cnnCkK38jiCXNUOqAyw9DlR6WM2Y9
J7qlLyIuRTo/8EbhzagYfYrgKkYgKVAqx4aDiO8q5QJU1OhIAfyqa+4rfPP2DuqPM6AsS/qbQAhq
xXPm5O8Hy7GzcE2Q772dYO5PYlPBePCFswWNw3DTfhdNhVlH9w7+Gvil5KE5f20bEwd4jJQisAS2
FTTy0qSKJ2oUXwGommnCTWwwR0HMQVbmBh8eldPMixMzf9wpNdVUTXmye8CanSFi6Ee/3mUt7D4Z
ldBXe8t39bohrcDglu/BaPruH6rGlIMHeuWJc7/nNEbQr8NJXPxNnyrv0fOFmcD/9Y9jvm6NRuYX
2pE+pDBCSPejRXs71MiJmo4ChjIGT/v/wn6JD9JXkrUclu6YPHZKn8wW6qdwuduQFiPz2E1P7OAz
Pb2RP2QXT/lJGPvn8p+mwyWttX9hAJiA+XAeQRB+UjBf8oWUkz7pDrh1z//1RKPpNDGELSHC87p0
sL4bH+MyWTOHBV+Sme+vOZ2JvnaCfoCmLmap9igkEeHkE/04FJ4MFWxWBoeQuh4O44bBljh1doLN
87nH5SFQSgPncJCaW7iB/d5QvDE5eXtw0/NCPY5pb5GAOLO4XtgPKeOAAKettEdkDLWsZTLO9i/6
MXZ27QJRUtK7mbIEfDqPUD/CMNrkQcBCh4VjwPZFSwXM5M7i9XpNrfFrhow+3L9ao2t050M/ehVA
v0UXR2GCUajW50q+Rx24ZRq4mWPq9j405pLovkTt47Gb/XENwnmO1ls8ly9gtbdjHyU31EGGKDl4
//8D1OFiFN+u7Ygsn2EE4BuJpzKpF8LvymBts0vpF80+F2Ca4TvFkleVPGiqLd7KvH5tXK5WJTsT
K6W5+/CaKlrPrEsRg11fGaU+tZVYJnZhii/YRj4NeNugs7V1FrcTI36De0ohZpIcH0dmde8g7ZAu
P8oJx+2FBi2/gn1i22ftmQWydqvkTKwZvNMX+PYk4p6vKOfMNxmlfyGzcL1RbrLQeAvAikSZccAU
fxNVPofgF4VUQ0CnrjXO38giJxexEOovjE64k1e9GVL9n01b2J/S4BigDzHIligQv/h86DdO3vEb
qJ3tM4vdL6xIFMTjuTfKX7g/uECf77fYN+YAfwV4l7QyWnpuBFp6MPDUQIqM5LnEdacUNCVr8g15
iEeTAEAifOreUjNMwcTvk4AjD19dSCo+yGs932eoo338SI1dBWDF4Ta0yHbf22/teyww0JSNNKM6
gsATbLwzcfDE0XTBCxhhv7yyhYvofpQEo1UkGWorbGD6xAzTsA70+pFyOQGy22s8dhLO8+dMRdn+
sKxGSCvYQHwRFOfVDw6ahnSQOSX1OVajVSjdyrueAkBk/MDJbIBovNVKXx4M4362JvuKdyVTdGJa
fnG/IdkHHVUmRg5mKjkqaUxaQzaKKvtpkIY7Yt+Hd/z8k39M+BXOMl2V3fEPXF66t/hToDUzxPdj
9Q1EG3lMOiQKRmyZ8nYUTo7p7xY+yGC0LCspytLhwScvqPW8MtpQbtU5bw0JM5r1MF4A1NGBWM0b
ypu15I4xg84zZQQihKVLNA1n+q9oDY8Y53IeeWtL5aJNdBdjVfKkeaLxIgwAYLQaDLEFVRVxT6ax
JE060COZJ8dzXhj0wvQRrS3hShBiO3ByhmFlrkUfqSy7u8cP3Kyo8QKbR9khLJt0XqlvGZ4kYyyI
xSxcX9Wn+zsXVpinCPFJm3SssY+uHSbprUre1PpI5HbZIBoRqnVrAHmSiuk/8Vors3ng5yxmp/cT
YZZaGNMwb5NxrsOf4rkqGeDuLvDWQoVTkh6GPCGxjMZm2csUsSGUS1cGhglPrN6e0JMHJlIKDB9V
2vgIUn/NcTtyITC4yUc2McYIYMspvPiAFW99fSdrq6rMm0sRBDc81a12eg8AcvsfErCsDM0aYbYu
BNSFXroGss9ip0M0by99hYJ5Gw1cMf7QU+ZzfYOC7auEheE6GzdgZBcN99cQxX8O0ZLAj7WAAY0J
8yH0GNLL1M+K1sjGPfrQAxPq9WdyjYVWw3tDkAe9iOjmyU87imfP0kBv7K2OrDCNl8xF3UISN68T
0wULwTznYosnObvCT2Opmh/Ehdz7aWrDo4WNZya4YPxgvYX8z8MCJmDQhjEawrRvJWpPdphu9SBX
61kJ2GmdTYu0TkpEi38FSfROrizrFps10Qagp91EJ1CodzFXzjcO05lZYEn9DB+4CAiYOWkFhrrh
8TAt/VpXxb0aAu4YwhcjKQtHEFhFO0hfXFbvfU5dN1r+euzN4VGnBAZzQmb607Pwh0LAJxhMr1Tg
VOlu0Fxk27zo2ZqVihaJwpgtAzWF7dzhw8tzR0kT9O6UkZuEE0WIvpKzCUS5CxI66LijqNzvxtuw
kXTKG6G1huAb20v9X2buJVRZsp4wYJ/w0uD/tDEj4blOvegYe7phQW7G33RbPp39cfutP+QniHRl
KOE4kgST+mILjnPcq3Nj+mYfURe0vi8ApjDKfbucJ1UWWucO6smegsSCURDMRCdsv3rxz+gQaLtr
t8g/yv1YcXOW0CFyJev+9xHwsyzUfQru08NE6ALB8jNYZHk5wQJH+2hAronx/X1nFYDKOm4M4Gv+
NRvt8vykwYHUiBdMQnr8BLLLZ3+eOf22F+DzmJGmedaL2aZLpUbfvSL4ZSV/A7hhP+l3MDMM/pHr
3RNzzwalimxfhhhWV+MII7bryoxglyq1D8tJRssMdWgoln3eH2CMUTw/kPOlzCQ9+IX05sKPOV9I
W9BYXPA5KrmOD1zMrxFZxU4Z8Do7vIxvgSzJwdYO38nzaAwRSUbQNNzsHr9SFFYZYAaMrCbFw2u8
1nzP8AgPMiLM1eOuOcdbD2pZ2ZFEzeb5MnGL0Wgrg5kSgSpfbAzDTlC5PWMZAbE8V6nFfmYHg2oO
5ZkeCUNbqIjJp5uGbzpkCchdH10wwk6KLLn74o2VsfXBhN33an/Wh/XOCeIjhlb+MbChZKV7QO2M
lQsQ3TP1p4r0CWAPvbbrtelneSYWEglLQU/+Cb1pU06FTALCPxh7UvxUp172A9ru4aLklCEvir7T
BAJZfE5RkEWM7SPqW5/J3p/oi3nnnSKfC+MkgDjJrjCnvnjHQWRXWQmxNePkzPUgC/KS576Lgm1h
meHkXLqMA9oGYIztuM8TXbvrqPqEuoSr7htUgnnSf4YjVwFQBRYCESLDtw/lSFFr/tI0FE5jkc/E
IArNRwme07phWHVcMtfG7X1H5CNhvirlaI/RrinkebNKATQ5GQH15QxzMtdS9cRPowTz+D3vSWFB
zMAXGWbxp5IMwSEc1POBwMgElEHciAQ+oZNr1QixYCz4Jw5CEjNwXeTrTJyxphViVRj8B37DSPX8
Rt4FZbwJCDsh0DLi6V1mTTBkpasrARq5xEgcQ2dBqLrmHcv4ZhomX7gpuGmbeYKvUw0FzhwYU/5s
0pzi8fXYbioyRBBXmlXkdVQo2FzkjXwrQPC+lUMHrOuM9DIwRzYrD4U3XfSLydWsx8rWdntKNCjL
FCs/7XHtIsXoM8s15L1UpmWDeIO2ljeLAs0LdvHbQVZFvA52A4j61OzmYWQUu3eAkW01Yq8x9YLA
YsuR+MoXLanhVi+pLnYT7yHkbwmgGuiSEi8ka2HKDKcm6s8QsUMnzzgXdQivSsDHgoemoRo0gDSZ
lXBrxngK2IFU5MbQcaDfWO0j7wmfWAJFp7+kzdl9tbuuYZ2pl2WHpthWTi+XRqh/S0quzKVCOtK2
ZyF8R1iiUeeHT73+G7mCUmQzOlxpORfa5NdjZtfFkbtD0BgkkWcficjM4cTcfUNFpXX4TKJn8igA
c8ewm84F2wNkNi089B/jnQZpw6j4apGuwyaQ2kVIuE17sL3Qf0C3gHm4yfCooj4oxBQkDvCiN/Yg
YuDu+ylaM6R3ye5R69OBehqMedTzcbdMNKiqA0cpfYZrlhIfFew1oEj+WJ+P93/0NWcrcYnRBFTV
amci0VmheayZcDlUuOk5nrrRVUavxHVGDKHQiK6I8UIm4fZo2cB6w/mWkl6r/9IEo+e1YQgxd5Ob
tWdP1fVsbC52+ujShzqa81lZ6jpC+OjEpA2wTcsSFjh2LkMfLlc8eerDIqE9vmimuwu48JvlHtMY
70R/d74IvtD/mBAagpcs/n7xWSWCg0oOWfZIW4mLewrXUyOBsUIVRzk80RTrZs/Daqt2JRYyH96f
2t2BMgk/jEhpm6FhIlkEKmapmv6HXCYU3DrtTEOwbE0c3rVHnKHzpfkw2Ea71tzDmWlGJyn3rBT1
64r+jmO76O/pCZWaB7syZfMnZXKyobHWNOeNmERFz5NtaQ0LPXbI33JSFTGBeZ4RLmHFsG+XviIa
9pkagNy23Zf2eR5qo7PivR7Xi84WbGL9qGZEiiAc/hvkMoUNolhU7j4zAd0kn5wI4JLrGYV8WXLC
IDmhS21cn6siYjd8GV6vzjWWizeRx+oYFN0+2UzFfYZQ1ZScBH2fOtxsnfX+OP7xaMXJxqj6Xmc4
yDlKMoj94zHDSx1M4LAWVlNiljCon0xUvfqCDqrddd90sIP4NmTwixPdj/+Zh166+bOFZCiblSKD
hMrLldau2RJvCQgV/Z1WQGnDbyHJf4Pp75j2MPzGF+1hSOfM1rgyvz/3MtkT6AfEfik1vHl4+cEw
GIJ9ibZgCsPmx49r+7RCcWGHxuv58EUV998onbuQ0NGX2L/W91oOWMMYWgouWvvt2SVotbhhhru3
aHPAJP9PmzBJy5Dhtli9mGMpR255YxNPth+9Bly6fgTQQOeovLNZqmyBRAWBCpChlYra1R3Y2jVk
0GfS/W0/SnpW6fv+hwuHhYCIzsWXefkhGrUJVk6BXAVoqiXOkbl+TgJbC4j3gSqnwk9ZY0F04lhD
rxOuIhOzLdYTW2gQ3qi6Jf9MsABUO2VOM7e6W8izi6WwI+9Kb0B4VaqsJMHrP+Ibg59LU7N0gQ0B
5kkBe4GmgFomZPNWr9ZCJ0uoZjiUgfaa5usI48DjmGj1lJ50hKBgTv4UyLgbsc2JF1U1roUsaFGi
ecmJYUGCwXMDVzESrZ9xnojk7rjB/8PYcpezL2rq5/uV+k4f82htZqSOje892XY7+xrd5C5B9Rqt
nIKPhc/iUPoJHBXu0WEoQmwP1cvXocev620M8V56eREirz7n3ft09O1HlSnzNwhnXfuyA9hLx+pD
yb7hQ7LGmA03owCwmJ6L48wsOFVeReUs7YLzmXeq33ys0E1pHyTHze/9N9bLSNvfkUN/Q/XgmwnA
yb5cAhbeJas0BQ2zikffgFLoQOZNGVKS8QwiDHw3yK1XGdn4jjsstUXNw5n9YmyZrWuJF1Irppj1
HLcduxhErnGO9U2LhYwev/YN83Lmx8m2cXwKXGb+pOhQhl/qL3k9zLUjuVzvNS4622duLn4KKJgF
+NhhDULEm52FkFNMJFHyYZDVzDW5BrJqdGiYy9rZWsCUYF079RrwGnepVvNA9UclUpRxYHXPWx3L
obGJEIfVvp5l8O8NNbwbCS2Vc00jZaBDMEQRZaXDuAgh+SIZgFj/5EtTzNU3L+DMGbsGoS+XVH8c
Rrr4bMWprioZ1Qdg0fd1rpKWGZqEE34gOcLDxtOSAeMbZa2GAq9ibmI5xwx18YRAMotvt+SA4j3U
dGzkfXc+ZAK5184YvfP7k8jzhWacSV1f/+tQIHkEkt8Q7tJ1Vl9+4DCOP93ocp47qnHS7NwpBA/x
SSF6n1bdYDR989zosjEw2EDK62YmW4MWH0EcC75J7sYt3rp/0TWVEJSZiQ7VlqxxGw6SC3H5FvUS
pw0CMyzx6C6uzcNbnmpCZGYVuUiahKbqF1pVZQEkPjN8OtHKjQyWt8YUshU59YjCSwIDRy0Men29
yQo6WQiMJAc7S/1ZoboDGAcLCiMrmMj3ZYF4UXN1OWp/b29FPLSDXd0rJrbiANtxF/zr11t1zeEn
rhm7aK5mR/RI0FbLzrmFw9bCqSZK7Nv56K7us0YfOk4geghspQ8TD/tFRbeZD131Kvp+z8n2LLnP
TYjRSSNgCIWz+ZUNa30fjWgJIvVaIqqvkBrQNdg8vR/KGgo8BRvfXv/dL5/am7Pfc63u7i4xSQeD
hyepPsmAQq2PaTEPue5KVeiOY8uUzHN4GeIVXxbY2DF7+4HjkuX4iWSr4GeibtsvGt+9NtaqpD8g
ek8OhBaSxst/GPOs1Ww4X+6oWpZ3Ei+f3mIOQtA1ufMVHZEEgt8e5uwXd1JZoYo/hXiSZZnyBmB+
382ZhgUHZapTAPSh5ipVf4of7hpI2ei/g2imne4WUW5SG0GhH0KXdk2n6oKwjvMSQeDtcT+U9qsS
4S+6ogOArIoKJGiFEm5Hx2zxC+O1bTM8aGkFSQTLf8n8hXYRt+UxF2bC/uMDpaopH5nzjLeSw2S0
1Oi1/MEnGv/ClLJn5Z62NNDORrMQd6TDd9K000fjCi5IjfWkwC7esPPJeyXIZvqXNK6L8f74ZGuZ
A9Uc3uJddOPEGyw1A2s4iMWuRHPWTweEWMN10Z1bxrG+SlJogZj5F4HIfamkhjRDAU9Q8oIs7ZqD
FB6bsar6JT1wLz5SuU2ol3Ka/43CYmldNYV4SSTpxXPVsHnbfYEdTGjtqLMp8uwsb8rhDL8OOIYo
CDiI9LqjRGUNSJiAUoOkRJuP6MNuLBocUreVgJCDEx5lKnYCZPOP9EzZiOlKkyCUb9dk1U0Bh6gi
2715wwvOiuYJnHZJaOHRHe6Y+DAScLuybdxvKrEHCzzNLjPw/vCkDsRHKmLEw0DiwH9Uae3/tgrO
L5Qvii+cplw/o0ZO7tLxqGJHkeeuAIrjPJwjQqd8mYPgyvSnZirnO9cG+hXW/DjD/65gtZxxVFJd
bcja36beqQxTUlWmTbzCq08ylYOLJ53uj3T+m550pi01FQtMlijFrvxke31Ae2AiqR+QQB6ztqpZ
CLhRA9LwoW74HTM0ofOYMTLuonlVnh0Pwck2DdfU2aUC4WrpOGSaG6LTO/KFId5d9uMaZ00m/s38
g7kay8wdXDVXIy1a/iZhfYIvImn8QyMnaXx3VttsYOARhW+K9LBwcweuyJ9U7ikjT+i3dtBAC8up
dLfgtThd8crn19+mekAe/FU/39Z92RidVtdeTn4ZXj1eWSPVbCzwZEC7Ysz2YyNJJaeAhYbyy15K
uCIygqZ8P3an6Kp3Pwn8e2eOaADH8aL20vvdZu+86c8ZW9OrpSjlUKiF7UhHRYdk1Szfm1ES9iaR
a8eUMkjHE+kn3Xwc+BIigvk9qFCAvBBqY3wRRjVcmSS2vOv9PVfGKzNA12h462MalgTSwvy9H8gk
5AF9lRvuF0BvWE2Qp4L8C9JVX3Qu7ya+X2Gp+80WQpRZdUVKeCgEfn3SZnVdnpQ2LUgADJQzueBW
jsH4MEW2HRDBAd/SYWm0ha1f6jfh0QAG3Zgtis7Vgz+fzlc9wWhvOHcuQdmYqmSany/NapiGobWW
TQZnvwYPPAvzZtFszIepsu392rgHebXfU+fWqGyW+CmOb89VXjt2L69rLj0NmQFHW+Wcz+go9FBU
z6Ki9C2FsGGtJezOv9CLdT7tiyohiXBL+/Cp6P6qG2H/DCFs1jnBsMwY5Y2Sli5fI8auES7OFSXv
i1dvuNhKsUAIDAgupToX8USQD405pdYzUkETAJxLLDU+5NT/1mxSyDV8zsJAvaKHgVEXUEvpkDCG
aIT6JxmwEdKt2v+UE1fmqdSVI4yCI45nHDUnwTLUsDaClpRgmLpHnA/J3zAvgNyf1uWWnULFnGb1
f2Hi5j4+IRJMzap2coXuKzJbMwnrSTI84U7oyROhoFwdv8znmMPvSzo/9GJPDT/izNXYUTr7Zv8Q
+F/FEFgiCVYbyOGnEv8rn7UIlehkSEGjoAifgXW2Lgd4aJRbnKCJ7qe3E6QYqdekOjYsa1ndcAPO
hFIA84NMA6DgiA/gq3RADrGThk+LP1EQmDGoPMkX8zMYbParO0FUv0AHwGPEeiYxrljgfiI3wro6
xHoLfRWAp+ANuJg6YcYX+UXEw2DBg+Pcw5f1L3cktQmBK1HHNwEavGo7m4TMdWQGHEGhq75xYnhz
1ceyZL1OUsaidgm5rDmHKk48LjyNZUTNV3PgGJNYKGdVdfmgzZiaKjnUst+0WugbmUbBdHF7WXJC
HrB030dVy/w/34laTD6cGsSCljAvL0+HlqAvLYZGg9wrTbVXLtaWw0SdIyfxVjb88nGfT0LWj4B/
nzfpItV+tqC9L9EFedOIYgL8OsvDhN23HrgKZAWev3G52fEQeBU/VVRCveOLxJClFw+sQtQNuwoj
oVLkslADt2DQaKPcaVsJRG640xk5tKt9Gux2iLoIiJzqVfzZ+jwk6GLP81ILtGV5g5NzqN9nM8xX
X1KutSEXhTcwKo+fO9BL+GS+CODhrhlbtSwFGKQ0/Tk238agKAvnGXXQ5vqxi1cChWsTaNmbNjby
wDecq6X9TDGANgk4+4bdaINdCxIpIB2eM2CTa/xTftJtHkWMrSHOC7c1T4LViy7S3oZ6ql/8GGfM
GNaXK1BYtO0youk2naRCD4pnAzSYWRk49Y+rGagAO6W/exhWL8xT6Ieh1OyTdB81wrg3v7XNXTvy
yRCyWT1KP0DirJ1Ekh0mkRECOD1K+ZUCpsispaiw2Ru1mn8qWTRtJa3lu59/+EJ8fou8bbqFfLOP
Gr8xwxIkLLxnCPzroQ/HolvdHBisdjgD8exSiDC4Mq75fHb5dE9XYNJsLlO1lCMoACHa4J1GJnC8
Kq0qQSUvUYsjkKJgsbuiXqDqYYReF+693vyhLrWLdC+2qs7DkqckM1knmESB6akpn8trTYRLbfey
D1TrSBbhqbJTOfs8YDxpn4zRvg+6nN38hYfaCIwMavcWBCG3g0j85UMVVpUILBm3RekgtLh0yyq9
VvLYMUn8P/pIcqzWj/L88aoB9Ds38slE8s3D+YM/BLdClbCZS1KJSjV6RM2k6KwM5VuFmbsX5rk3
hlInH0pSsO5fNpWkbG8GnWuI82YmvMLSh1X26xEo8r5IqN0Dmo+ymyI7ziOXd5wT89WsER3gOzJ0
4Huz2CvpuKFuwEhhboenGFV2CbQ+d5ernR5uE6ZqXHMcSeQpnygH6gC+TSniXTpLS4R1bxIUz5TD
Yee8w9XSsR2gav50+3UQsfXUhXQYXI7SxGrqSG+Bbym8pR5h/OOWMNzLm1at9VGAi77KGYHXmSCH
LFo4kvQuDLzZplPdSzQNhDbefKupi1W/nIZeJlleEJLjjDGSTh9GG4+hdIlIuzGyBQriB0BrkUZv
fSn2wHP9z04rtxWPQZIRIp52OrLphQSWJFM3RL1mkVVT5I1oEC8l5jHISr0a/RNaLhNTzb1LxSvQ
mB4dUVQOOKXPM2QVLBE/sI7lwuuY3HChrPezhOjiHPLvbpr9/2L1RTJXU5w/UnFC7Ijt7ARYbV7J
CLS/txY2tEM7xcK4Bd/8REBYwmmNCnsIS1nbwVt5kXHK4wc5IXUkiwbUCbw5Mfiz8pomkxMf6hFn
KbtKMvhR4k7wcw1fLsV+pa0PNDxlhIKei+66a8tpPPbmv69wARU5qmku1WJ6mptR/Nr+DsjriWUi
Y3iOanSGLY7JUAAQ+asp5WooVa1HS72cqf6/0E6p0IY5YS0JIqtS9keOyF7rROugsuh48upQGPiB
pHGz20AgHEccj3p+eRGyRjhe80CthLeaIMxcUSBiNHvxK5DYSB+XAOSh2mA6s+lQRewXyDjzh3Qm
tUSyRMrFAGqY6kuncbB31KzNmq5m2vpTxK99R8pPA9wRpqDAiIvKZ7Plq3Yl41r43evO+HGJBzFN
McFonIRwAmOPHqMo2ogMZJuUUlaezdcyblXk4fNJRur4x11qvSkJ+5dxha+Hg1hUzc8J7ol0Ahn7
h/cNx1pJqtPdbnmmoD8XkIl8Dh4LJmI4jtswAsozkn0M6+vzBTlPNTB0KEAwgJXogpzwTCZMSNEo
G+aw6Kq3N3KhSQAM79Z6zJ9eQ0lSnrhf+Ze/qpUyWW0ldr5fgByLnCCgk6sQI1nOL1FcWdAX2IKM
NEOOcmXQv/wS0PnZq62j0QS+PLOqfSATnsR4coJDN568VQx3h2ADdYlRqZWBpKx0dZizOG5HoIiu
JzX0BsFkSew66acEJyZIAF6ILI9X0aH1Y8XjZJ101EDWHg+LrqluILUmbtonGY0ebtvebbS+k25j
GuJKxKyOUdSuP9cuf2JtslpRMnWcnAaLpJVUQiiMNFqQJhsU7oEpb7kV776p2F3NKshiUxC0P/zU
SwaXNj9R9CAkX7zDQBZRT2l6MBOGWHDoBLOvBoQq+W3HdADLoA+XdXJvt5jDz8TXvzBv0OjtzFYt
F876eG6pGsKsdzvXwQ7L3BgyJTB52wxZb4HzHKxQ0epJnJVhxjo6WYEeU/GT8QNS1LNBmJqZm8MH
VWxeypIYjhhvBGQWUS8YC5Dvf264nyaGGaAF/A9+54mU01cXBl+fR/M8vlGPxmd93465wX2iGxOJ
mDxFeLj9C4+Brw1D58uB/nPelZPKufnl26k7RVUdX/08Ovy5kNjWS6qYhFCl2SfU9hmt3pm9FyFT
NLHSCY8npErWCOEkTTP5KfF1Cd6SuZwUNN+kgPj5r/jRKk0BNHmjw6+OkDv3wBJaj7+3zH2aZ+1M
9ckSmsDXBjKjTqpmAaFJ6mcSoKPbOSgrC+75r0Tes4tdJg5BcBdS+AoQU1CnoVoy6jGJ0S0UzQa4
FSg6+8sL9Q5ZSFK7alFlrF//ng7skQp9HtNe6K2NxU+j1WHFjt/A9TvpGh4Ef8ruzn0SvrH6LmVK
fzPShxoSE9jynbP4BF8DLjMfX0vM79SZfxOPBgWrrdYVtTpxbO1SUiBi2wbgi2ZZ18N5P7iVkuDU
UjoLyXH0LKixBJey8AqjURCMprN5JiWCMTnLRLe3WZOVYOBesHQy5M4do7ny0UHUUkR8gUb8x5tX
k4HUERx5j+nYkvYKzuUph0ddoOv6x9yB6KFUgGIi0q3cciX4PiCtibf4tLy3ZifB3Pulezh0Pac3
Bbv6vICu2K7jRlki9mB4ecX/ih+iykWxaOGxQxJ8MTK1tYGSNdOp8Jsp0y1hJPY/lLJIG3YXhNxy
8Iavn8mn3MMVKgvzNXMQ0lmP+4hcqkodvkkWwt9LmM1Riaw4UY2PcsjJF3hCOPe8ZQ0UqnxPc6wI
Ox8i91dQIdktOmfiMpv//lyjbbMqJWnKYNJzKKpuPzPzTTL0TQFJNPcOF4RbHRWZLI/8KVFkUd4J
kzhM+WN+u6OYa++Oli3ZqMSAXMqubR3NDT7AfZeozkhylTCN5jTK2w4K8JwP0WifAaGHFkmH77ES
U7Awq+P1E68rvsfSUnZg49Enb6AU+f7dP6f9yhSk43E+twsJ9wJ7XwRJeekv4zM/c6HzuD+D/qVf
dmtAIVUTBfQ5QjSanHXKTGlv+cvUhAH8GZIkovKVwIKDi1wnvpd1Rd0o2iJDdbClDHrCjG22opyw
eqzFFbmj6Zb7xUxuEFjcXAq4pygcL91JCt2jYz7yzU/GTUR6iGHwVwInIlXiQpPCs3H9hKwwtii3
88DqVKjooTq4EuzGatwxWwOrAncjty6VZXh6RMLvrcaLiAGWH6e2LOBTBwt9VC3Vz3uPn1CKvSfy
3IL9TnpCp1euyu2MTs2YLY3/tR9h1B3EMzItAykeHBkInbqVrSK46fspohTmHzEwNpg0nBI2sjx9
w0HmIY+q6GKtVGwSO1vuUSZnLDlg7JkI3gyLRL0Kk0yoMKLZGJoX8aGOxjyI5VtJ1ch3mZicdzEU
j2E9anatXDaOk6UwZEvNjpOXlRvgZABKwAV6aocZsEltdb8DyC/g/MQxbO+NsvOJOEv/HiOy6LqO
Nbp48U6oBD+j8Vlh/UPOVnCUeb8UjTDDw43wftMU85OIS99TcelQ9rqrlDCCduYlnFOPoPKj+9HX
QGzmUCdWcR/r6QwqWjrpznuqwImz6Nx2QIzNpvvuZSNZ5d98yQofTUhj+p3sT1osuPba74uZL7ja
Qp6/hABoMa52KWjsCfYJqmogUKm0/Njz/nF2+88UgnpFNy97/NrCwnpfYYQyJsgx6aMvaSn6HO8i
BHhkiMzwIlFFIQsyxA/Y6plQ7XUQG4BsZb4vBNNLZFHKHZlPXP8zUQhwoNyJnqqgVrAu4wbrB5GI
GMPsiUtah52qUn7wFdjdKZkBnj2rT0skfss6mUn6fcL01H3jkYF+lhiZhp7RH0K2h7cU6d2dd7xK
e76c8iiDjnp2W709aoMD6V4DLwKY9t2Evz1vCI4RbxD3bNQQ7UVeU5YWRDsxedGoE3Ei4JCom/cP
4p/smuaYUtc37PtNvy0u+3a/L93tXndu6sI98yUVDff27FYqr5WpRdQhjFfSKUQiU50T7VYbTIgR
8Q5jQzcmcYUp+ffRahLvEU+T68LVh/JlS3jxcJpzNW8Art7ze66BgVRo8EJOsb0TrDEj8fz8REs9
T0jqVt90uAw08OvKKLKg1faKnBwt1FoH1cDWVNmWXleKAFjfHkVnOoTGoNz58eIQhhubHyFrOzzh
sPLO3hrKG39mnY3zuFbNHbRCO0oZFjmcUzRiNyQ07RyPxtwyuoqryGNyywxkGRgLyUghd94IwXXx
ZxjssbzjH+QwlHzhE4gqYjSODao+BY7Xt/pxKBwmk4100SqbKwrIRsSPOfdV0j7VZ6xG5JIZPiFG
vSfdSI8FQy6gsxnE5yaUPJ2nbX7zDmDytNYFL6NLPdK+yKOeWlxlLG2Dt4dqOPbG9sKIDdnzhJ7O
C3f6RQ73O8cp4goe9K2RUFfFHa5YGEHWFMaj9O6QIWjA/qN/v05wLS+uf4w4jc9Vb8SY9IxIy4Rq
QBohCGWq5vuU8EhJ4cl603LaLbGA4P8L9vGy0NB68yDpEMp0x1Crl4YyK05jIAKn+pf+50oeZ+bp
S2OziQ05QeS4d96MK85yVTRmw8u6JiT/fdyCunij0bVqo4eAz+wjzr6eBXTYiDgl+QSAXq/0M6HJ
KR/GcuPE4Yo8N4pKEo98I7WQcZkm7Xsde4ibdsU1gqajvf/PUaUkzlyFt4PsZB6fyt+qkE7EVEao
g/Vc/ROmU2ZUQ8329takB9ukzqLUb6irw5rjEYL58fXiXEmfU64/eawujK4LGOHMmSOflrSWs89p
MiRCtvUJ+K9R3Ca18MnF+qvHP1uU+kCT9PmePFPlAY8EPQIhbDj9ymf2RDbyNFQuWOaBkpU30ypE
ezCpI/jc36athHn8l2yZnqq+mHoGYEiYL8j1j5zZb2rnrcDXH0k+oXiVJtqokDA9XJ/SmF1YAjHR
yDh4XP12DCNSuzFHWHmhl059OXKOwWOp3l1a545ikt/e/TSw6Xv4aT9N8QPtBnpHJVCFFKXbJYPS
cGYnJDOiSakxxOyVF0rZkvIF4R54GqkK606GRVwA5owb6MiwQnesEL3Q2Yd8J20hVd8XtPAj/DcJ
PAL/gxN0+iBmR3lq5FNu7o9t5jkI6hF7HbTvEWCKOs7Pvzb2pGCQXx2K/4x3FtoU7TL5Kj3IZQPV
6OorRcrS9zPTRTvwWvzHWyWYYtjYtC70NsihTJCpPi2FZhgIpEd9sIO0+7rL8Jt2oD+xAI0ZZcWp
mjWzNhjTdF6Po8grELI1a0yo2kM6kpfjszKgCbDaVx7HWcoxB9U80IIrztqCPrlDRVvFl03JjeJG
lsBbMcrlf8wZpCjaV76Kdox7R4zomJheyIcm/1LZ0c4aur8bRsQHQlZxWtZtzc9CyPl4IENztlrX
H0Or3m+Yws3pG+3rmSwhMDg7lDvE7s7Yr7UCrq9QyUEVOHpgmRMS4G1zavh6txC9ZmOq51S5sFnA
qomPEmmOct6uqnQ+kQYQfM1Ifr6xn0KYL/cMbW/+M+ugoq3sG+V5iwHJB1Yjl7hmrJUqtwONRnBr
PCVsfTV5gN8cDcRWTqbWdfO1itecUTEtFyQV9egxfOnvJBL9I0kcF9gGfhwjC+nkANkzOArCNx5F
9S8bJxAvs+AI9sou8FIVmK2xsgJzcBaAeyMRkykZOJFPSGFAiRDV9jwYbBy/mA7N/E0fbWPRfzTq
y7/4yIECXjkKMjHygRSlwcaLWH3LSrFm7pw7mBWhDLZGYUQvBd5SnfKCi/sIuJaAuCrU8aFHZANL
99wfkz73CjEuzETvvKiTtl+d34+m3TAwqbli/hbUBvGtKdNFLxQ4ggazOmYOes3nnz0pX/a8K6rp
Rqt9b7V+t+fU4kr8K4W+Qowz6f1aYRyobrEqL1npeXuzFd2voMMOdBokzKfQRl+gTzXrKLgta0yk
LaYO/CXWaZvs/2ocgCS3Mg4nNd/RMQrpZ91QGx+OUZRz2F4Eu1yY374PP4fG8q9zqaBT0p0u5YHz
59w6G3NrMHqbUi4HuMUQl8ODgRGo7TnhOtkr/zMnAxFgZFThDKjZI7pGek2vAGO10jJ9VZ019cx2
uz1nxxOfEduuxYiHNva9hOstl2ZiMU5QrtFadnB7AJaLJt2T/PzcOOkwn3Hp99moyirXtT04amld
fNnz6uoCzTybnqBdXBtwDOHiC/ZAXPOaj3GqO8P+A2tqQgy6lAt+mbixBf3GNhfeFlpHK43v/gcE
pFdMjdBMYbUGv9qRkKZUnpJSGIDGedt/FdQkEhVVVyfU6YaD50INcolPUnQ+yRMWg6KoTW5kFIZt
/C8kAfmmS+99nXcATqZ2/6orkxjCU5CFn+VlQaBodN4+Ubq77tbCESpc0aNBVmb7o2gxQbKPA5Rr
rLQ6dC3/PHl34n6n4t6jIBKOh9xiBgxulZjpM11g+OQQBzQXLGWabkZDFjwAzAYjsWUsilXJBo48
8kW8lK6inWtVDB+c4rPR06KOW6sLgc44m8qeIh0yvqMh/p30XjUOGNHUb832YrKv5w3EikKeEEtd
TgcY/L4NFMAQqfKk7zGdYVaWams9ChACMHaXoQP5zYW+/kDl9RJOK7apIO7HrE7FCjEzKaOLfmBs
8IKFq9ZCGnjVDXA2WRx8oDicS9/y2ridYrZ9tLWYBSz31ODF9Q50UFNqEdOLmNj8vMgsGqVz8oZI
pF+fbDPcMQMeCYFZRRbS4v8/NQu9XMmLhCWdURH4tomzvDpVR7qyF4zJrUVeutbOhpG8ZmuXFE08
X250LWQ9oWcSCn+Kdz0Az1H/ZQhdYPF9l4MHw6x+jPMNNfqQJF/44R15lQg2xYUATdCrItClYHbe
/cBhq3Tp0Z4nnn0+CjbxICLqHz6yK738pxchZvlgcwZupyxi8/eBK05XndfY3A/PNCQRCoXvzdej
zI9ozVbnQkatDTs9AXzMVqbo/6arsECK34jOibwkfFwrNZpHRXHkUzQnYDX7CsZ1uUAt3k3L8en/
LvPFn/nXznj90AyjxrDLlxTGCBSwx1nE2bXmsplTjXtUAUtO03LEvrWig8dbV/0VRcyOjXQpwIaf
dS5b8nA4XE1y3lTND+C9dlYGKukap4j4Ms8RToJ63AUrllIdOaJOH/8A4nXeMDb9Y84z7q3/ZQEv
LXg242JKLpdqoQkZ6kPGhMpKKxefsmGhomYW9qUf/R7EvjsgGUZzf54TPKnYsdOD34KSi29A9EzQ
nkfCtl46z4mvAJnAcX83k3M2Qq072MIrKPEYQQcD8/BjjrkZzzvwYet2n2BvENBBNg06Nk21BTLr
A4MLrLYdAQQBRAR6AjuXvZOEj3GFeh6aRaiwZtsy2l9r+N1FE/sjyyi7b1YICvj/ig4eUCmQA4f6
nAUamsy4CB36ccUNQr2GOvc9KaTT+y0VrX+YQCldnulYt3oMVARd+qMdR1yNedg+w6JaA6dd8WCK
lA6SKtxfdGjccLsz9tXwNDjPpmJNZQ7HFuEorMSlhpsXV6CneZ5rbx7pp3tASGR+d/CuTVo0X2r7
6E7aQbyX2PR+vc3SvH4zFUsVidQyb6pzhpSbEwVFHynIjMnUbu7LUD17i1Na8VBn+AezrLSPElh2
3scATpT1krTtk0akVBppUWNFJEGO7nro6zqv3SlkfhNbASHH+m1A6sDoBtTskS/ppGUw8gITg7tw
RI6KJBdLGQ6KyIe3W2DuxhfX9kv2N6cGxqLFc7Crwg2j3C5uPYTc5LWGR+zFIuWSxUejTj1ifK38
tHPGk1at8QrSYJ1rFzQWtYPmpPR5brFRLq21De0q1CW+GbE9DrGpmHwTe62YlN/VxqP5i5smB+P9
1R4kdubs3r/kB/gbTso1eeLoELbTeugfUXU9JAU4bE5Kwaw1CEz90Qyn4IMhFiBSQWFg1wRSPIaJ
AFigrtofI8FYmnsnFgzYitW+9Dk8bcaTNuWzd3D/pao74FrkNjpcqA9b3JQvpIxK1Z6OkQU8V6Tf
gYwKzy/0TPjS3DU5/Vu0vlvEJoWMe2KaZQ5McuKgAdxnmT1XVHqMyyGiccJ1z6ANLEL5B8LmuYuR
Lu8PyHPCN31L8zHHGZBIl8RiQO65KfXm6LgA+kd6yzHaQFbpCYiIJ9pYQqet8VHwKIptYgpsnEdh
HtR78FkTkhyE9lra9umFzt5db40ZFAXoP07U5hrbXgIBrsXoVd033IU8hZyqMNasTcKRmrQZuLaF
TY7mUrRqrYMwEJq/Z0eCJ8EssLSP7mZcE6P8kjDMIvE/I1Rx0P04/Dv6N87V3Pw3T/FYDJFzwy2j
kju/L4do8pLQ1Y8OOR0JO4eUnisPF4fUyhVidQwIHHosYqcm0N7upYQrmDbTPlxwwrmNzKXJP1kv
kGx0LVBYt8HezrralP712xUkP6wz29FJ+QEciVxYkvkQM6LMcmHkZI6TkZ98g76cs7edLaG0Gy0q
hbtK5UdI5A+yOQz1VNscZcuSH3dTgXYTjUC803g78dtAxmVfO8rFH6ZTGXRzfG33UtThUoIO3kQs
K4Dv1IAbSJvqT8GIS42BVIvK0ARQDptly+JrYpum5H2DvLqehqOCDfaRrNBxIsckfQicPw92YZPW
KwWAVrBgBB3Wj8UWACBOoVKleacLtiFSsIDjz/nR5VgFUVHZPg4sRFwdMF3++JI8eFrKFMCOJhhY
YFeO0cavSn1Ama6NM03v14AoEWUojPWLMO0vNqQbnWGR1uFQAqI55+al0BrhHc/Z878w9CQVsHIq
iEPtZWLYfurXe6H0ICmAtznre5JzGwpaMybdY5X+pQUMWWpnu0VarkXOkTWZTllUMFZ9tDVJjYEn
PaV03d3KLKFdj6p07IdYrvT9nuZT0j6b6nYW0jn/fIcThJCIM8Bt8jbgRSN+iEliF1UG68T1M6dI
4ojQc1Quh0eEP7M9Ns4EkO7Gb+tr5/68i+CXQNauz7TIWNbhk7NxAaiYqPZR69vFTi7L37euaf2C
Jg4jP3p075KqZoAiVGed79NiNJ/OhJr5iQHU4DbpK8Ifu/hVn+fMFqwqNzs0iFG3/0owKV+4RD1L
QB7iCadHhcEWNLTEJOTpKAeZd0GjQWXGSM0FMMmbQhMYuWKfxKD+jJtlkZkRM2/seivBjaO5/jzT
b/qXZxgNImLXksXm+vrR2JdH2cjtKgB6lPtWTj9GCsAW3sWf+2VhuRIdTBx2pIfzx4g0Ak2sHNdO
DRvelP9TY5YQRPHhq75fZ0vlC8VICPLpg4CsRypHYZpt42uEeZz9x5DnnC6jb5BDmKVcaIy4A0eb
CAbPSorA8woMxC9LpDIRu+NIqW40qVg0GE6GydpdnSk4wT+Q1jhUhQCaNV3v4DGG23pLUvzfnRmH
Qdwzk7DZzPFaXY+qLxDKYhwwL+fw1WM9UmPvxf5x1FmGX1bn5JT8At8zhUd+Mvwtyoq886Fk1AOD
ZV1yLpi6k5PpOU/y9jVOfP1YYisKrAFWfSpOZk5DYDuThnqJ0eMyD2xxy97Nw1TsM+gvTI2srK/V
LIJMOGQ7otYUC+lkoeK+XdEs9o2nCMOX2T/aw9WBhVY5TBal8nwGh1gHAReBdwTtl+CanIbEDpbO
fXrt/YA1bpyvk0PE7+qQHXuAAhuk8F0ka0QHLTdGziE+imVzwM5RT6akheJiQCN+91NZnP1m4UCk
xUBvtKmfeyEs4Zdpst6hSg8PLHV7skCiF4Vz+L9o5qdsVJ3OkR2bp4SHBthNq0ktG0sUqoyE7q6B
W48p61G+YQW4AnmUEwQE/9xwarKFMq3gmyDoQ7Z3jkiPfk4VIcfL20tLeoPnx7cLuvxzls8Hcta8
vNoH27LtRdipvOzZ2Yqx/FVVLug215Ww5oMuv8sbit7P6/wyu+q7mpB3KTRO6JkTNetu6vjAMFXg
kI3yonB23HeQrGlKU9p7dOgNUXwc9o0NofxW1qJpbLHA3y2v6EfwFgLmWtlTvWhj0Osl87oUUFXo
fw/UQhZlHoB7kdr9PpffS+1eRSGFoI7wW3/eb39KVDCs2PuLYXPa9qTkG1D/WaEo2ilRszciRu7d
55cbVDdvp0fmbAYclJqvG5mGyRc9bqhRILiXh830llm8qoiUqeWhpIBbXiJyMXdM+YzoPM/JVzuN
9MeY1iWtiaoOhsECIybnSzgMf77G+AGCzAZ1zIDhMuQmm18br1MV+7u2rB/eg/p50IdTszbji6ib
EXjgc1Pqn+yZP/UJEaMqG5gfYF2n/Ml2wU3qcQnPKMhzqVwc3Ssk0XwfGGS9+ONKDtCPjqhYpI7e
1vAEJ9Vk/TlVtNwdRe1nL9XrEgC0bfAK/qcKsrdzbv2aTNhoCFER1uoFKIkADWExp11dztMOUMT8
lXxoCjUvSQ+gL3Out5jAA25HDiEUu1/S+0+RbZFd/8SL6ukgAK6sIGl9HoQFHrwMicyXc/1Rx/tn
91Hmbd3ExfcbiN2uJMGTgrOKacwqCEy46Ffp1Ys4R2GCLnwpASLSs/HaxjSGXw9sFNkjUXych/9y
bEv5yis9jaq19hm84rpYiUbPKYUZS9NScLSyWZK92JUz7NMDBLQGF/KrLXltLpYRWTp/UttX0PwK
3Oi7PRGNpUT+QDVrFJU3iO4BO2tLPThk2VWNd9Aovb/dL4VyilsKzSAeu4wBzlSE97s76CNKrHbs
jmO7KSHHuIoDM4BHIZjWxV9CGP7HNrnBKWttqn+lpxqChC8VOFTs4C26WU7xD8aMBswxZuEi0NN/
/ic9XtAlbpRe+kuBhPBXjx7A4Ds/2pbdO4Q8hCF5yDKHfWYp5ADO55JfEMqL564c3v1GL4k8SpfL
R6FQrYgq5mpjwQmG6g4byaQQYAO01Njh+3yYTqTRBFNfb6t6BNbFv1IJJLxQWVKoy43fIcLusKgG
dNHGkbQZlk3OTYxtBsXMwUmtEm8a2dEffmvT/kIshFUXXODDY2QxCDEhwZAc/MOxV6D8uV+FtPRx
xl3YBzlZakLLXj8+S48DqAQ1GUoq1hw0CtxsswZmYiMdsjPVzMJvUOayzomFxsXEDeGWGx7W+Qat
1PEAWP57iF3MxEFkKd4oBTLCzJq8ONT6482JeQoIZDhWRf9E5LBl5TgymtoOqEVkay7h7i6+Tnyn
A+jMhzqhwSuUd842BQAVjiImEYQ7knh5m5dY9M6E6QbdQb7/5Ik89SBtjcu9O9yAWWXX7qY0kxLr
fkToiYmtbE6a36Wqg1baM3vu5gVS2jgx0BvMq9SyJK3qeYEohqum2Zj3eaZ3DQ+2RBZEs+eU4rD+
vrnlLnJTsKoBey59qnSo9OmCB7g+EG+Bx/u/M9b+X9w/ICEJhn/3j5zc/qvuip850wdwCG/x7srt
t6jWbfdayEInspwLQMqkoIRnx7MlTeFFrrHgRr6AS/+JQZaCDmkAdTOZqsi3dmLBTtjrCwX2VXqN
hofDKT2ufyUoLAPOOsu4Z3AkIOEQjIYDvVypskezaQuwdhGapiWJb9fjj8OvpSpf/eRNVXp2YwCY
5Jxe59ZdUQplNrz5Upx+1RBYWuJOOxZpICqe/FP8y1t0kgYEvdX0ECuese7VsvoJsFArw7agYEh9
upxLKnTguUPyJYtrPrL6wQcA0a00R/JEvN1ttIXe+xLLQgA/HRo+X170b4plDaSJI6PcXxMEh3oB
rhi8hrlD8BctuvUGtRw1dHKoeKw0vAikCUbi909DF4aYq/MlqnNSGohwpiDsdwIxuZtbLtFlbSDZ
sP1OSh1QOXSDd9l7f8xPeMeDEVaizr6JARLVXKfL9MuI4Sasd/olbyM/0iv+kjFacjfuQW+qfvR8
6W2kirRlHPBayMT8wJr+ANcKrup5g8JE34NwV3eTdBf5UBIybPoQ4fD4XN+A5bNL6UlbtP1EDycz
pBrEpoY1LFmRjS7LIVslteXp7WBOkiYy3S35l+PlRjI6Kwhp/CsYD6jCBoOyUIEiTkCMMl0UXwd1
RxcnA2xVg5G+ES4YSsNJMzQPO/KEANMLoBXXCeztzPf8gxz1hJm6Zn6G0s3JWUi0Gyu+fqOoNauo
YS42zE4fxdX7RB/mZi8ipuwW+QUlXqLHND+7Kp5TO06wqfjtPOF4OEtRB41JhFsxixXPCSWHKYZK
wYqMGrE1n1/y9uP/eWL6hW0WgpWMDDKjfKwDnnrtXTefXlP7LweKdwkRoB5Sg/dIXoRRTUQk9v9U
cOaeov/XdOyXUJ/dya+NkY9YuWatMllcP/Wep3tDizmWVeM6iy08Z+py7+7JbFsVZNgbgWNIQGO/
MFLZ47wknTc69FN664symjz+rYV5vRr2RLYBA9LnekErScgld2DCFNCMzXxGGggedycHhZzrdnKO
tIzfgB4Lf3Dyf508t54bGUgPCQl9+jYcrGvEuRIiM5zSNzxlyv509zzE+7pthqmZ7p0wefT5JGcd
569I4osxSauh+pApnR5eqLVj9YkRJonJ81fJaCkN9YI3+am3eUDfCmLYzZkT0ZEhhzerJzB5QxKg
4zenyd0fVZi6AesNaapA+CUP++ITcg6wOxqsavOFZz87x7kb9Y18Q7T5l7+O4fXEplQ2Y+UKQYtr
xeiR5ONiCcwaQqPLX6RDagSgoS+mQnqMi4fACItO5HsTWAoFBN+bGCGSU3w+5fQApqjL/IsmKkTT
4j0zaMlVyYRYPArq0rqb3m78CH1ehdWDp6FQTE1JDHC3cli64b/YJHDwPlh3uQktxba55IKyDNHj
mfftXrOwOvQ7BRecjQGG1aFXjGQb07ngCKcazIJyBTChl2UaT98tAq9WfqiusT4uANF5/M9oq0F1
4tYq4FGNuqH9pJJ84QO6RVwdeocgXW2qHsmZEufm6jKiHnktxDU6bPsTN67ss4TXpGbzHmU4IlLg
w8cIsYOP6vTqaM7XXk/pCNlCcfxyM6/wi3BfPr/E0WrHEWUYV4VDtt9wjqIt6u27TvkFf/oer+v1
mCbDDe/g2d03+Ny+9Qesqmfl7YrkLq5XzFTge+buGrNDa9xkdajc+4ETshYpLnh5KPISJN0ebA9H
c6TpI165AyR49hFzk2bevE+Exc5tRwbyVznS2kqLWrVs5Z2bNr9Y8xqczY4TEMvETU7kEbke5vHs
ZQy8sRSwagePAhVMXIrbiVbUDnkD08e/3crImlVcoZPbXLSoQJrUhFOA+sGPXez2mPme+UsCCsO7
p4+C9VBs1hZnx5ZrNiM737TOBeFOTZtWSQOqral2HZABKt8fb459cgfLfPUbQSkW0RzPs9fXo6/j
xjLwjzWw5B8RGPNXeu3tq1yQx+pm/Aif6GAZrXs704GbAD9DuXOexpj8ex125nmutIT2O6df7o56
7HbAr3t1iIKZBpY1r2XdxxUTvEeOJCmCdLTQOP9SiftAgGJDcQRlTIEH3Myj2yxHqNMLgFuywDzE
e/isMlhaMKh4fy+w/C5rHmW1TpPEkoFcJoir/crBQvGIc436Me9BGK7BFvvqYEuCdeKT4p7yCgxE
Cq98tAwK1WmeyltPHBPk56UanxVj9Ax+vhISQZuQMwKg2GgJr4cdvuDo2iInnDkEk6838OZ7axq1
wcEWTKesAg5TwBTky81+HCkksbfKWjcaKh24mK/mD90y3+1AFBOnmbW+2AUGcYK8VPq9ImZauBY9
YgoeTt9ojmZ+mxWJC4JNmKBvhG9P1n/7ReB/xnfIRZi2wdepPSOLbdjqkeKK2ngjC2CCuQLMhcW4
yZirZNHg2l7fQwrxjbLjQ5sN9l8A6O8pWLRS2TXSsb7obPcHYv+WbpaWHKNLcFmVuJojRuwbt0+i
is+dng9ySp6UMv0cLGIJlqtbi7kcwjhcY40HyBIS24hkRt1uTf6B2SB94DG61tMjsyrHgvuR3wLY
wab1RzLgKLnb07G4ll58So7ZnIIM8hiX9XnNJJ6K9cY7CGwkDTn7RW0PmgJ+HI87XUwHmgXuC0Yq
oVbfzToksy12Aeo2pSQOG3Vac4PzErLa1k++bNEjWeluqlRzCX74++OIVmwo7Cqhit/4Q/EUU7eC
myLarsrJURcjd4k6NT+QH5pQhJqliQGBd+SANZHb7Xve/rxzT/xACTx33iMG+nHCXC8iSkhGdutw
pNSSTRqS7xD6u/ikUOTdheGpPa+GvXcqlxdK1nzr4zAqytTMrTgdAmoYXxSxjcM0iafKHB+kAbQk
Iqy50H//RkFtYCU+yF5bbl2seEajU+7ZCPQY8Ae8yA+JL7xaUcT/FNkJ+zEzVymfm90Gz0G4CW4i
tqj0r2GykDleebhJwXZEM5zzIplkTOZAtXV0nVs5ZvIYxiI1IXB/iZtBI8GWhWj1dkAQ9c4SuuwG
2njt1cq32/EFKwWWlluA9mQiBZYl3wCG+OTunhVgrYnwwEzvGGZ30180b+UFQOXLYEL5bM0c73Vv
NlE9CcKw+HPNdU4YNqjWwiUEvDdrTpN+1WNuBPbbyVsx2AuvGN4FvpMAT9KXHezEXoFIses+FhxP
IMBH6jtlpMjEk1k1R7kM/a5Hm6a8OobJ0RFe2Yj2B4uhsF01IJVmSDGGnIT4qttniEMDBsYYwFd7
HaL+//9HaLYGAaPhAi0uDmYIKwpDakuk0VLL2yUwmhnObe6FtdlGZXdRE1CY7zLyusLBmx/dfhCC
uU5AlqXHCQL9sG7gC3eqKxTOOoZsW8XrnwKjInJHuNjv4kWLJajhQYvw45GKRVp3EbUE9H/Zh8lZ
px6npXyGH3G9F4Xp1za8A6mSxPwt5OzJPtJAPY8YyabrlJ0uy4LZ4iQ2oEXDt/aY4s4pdkFajjgH
M+Efk/NnNwGCP2+1djQZDp9QLOH2hc4UA8KlLEp+ntLIneA5zztpjrpFAeh1gPlnkkzKo2bkDwbM
beiFHiY8xHS0gxtGIX8ygoMLQ2I3L1toh24zLx+IKJTVFPhb7N4JHxvYd9IzqBMIthTi4kS7/LMm
fWpWguz+h7P3d/Ekt33Twv2UAHXvypozpCqyyi2EKEhPGOEsDqsLi/UmusEcDmBGN94U+dX34op/
ol3zJs1ltI1IhBs135V+d5bCwVIUHeCamMO1wzpw9H2qCNZxgg5mqD1S9tD6UmxvQ7aHNR6iD2HZ
7Bp2vZGSMU1i1sLUHI8xuOXY2dCKhx4G9Msymq2j1Q+Buu2WCMJJClSENXmgmmVBg9N1MQjBj3xU
2m35xPScOszcFZRgyLcXy8oJKdvuXcSFjaavQ8h5Kvgq/ROXXUKhv8jXR7wwm/HTRGSCq6RyDuAR
forNGR0Sfntj93TM0HZPWCNfkEdOPYeqEsZiITjKwEbztoPqRqcnTK1+C5eZdce6z15DpU9tGmuj
fPFoNLZ0NVpOVC0HGYm/64VQxmkRhlGk+ftWcSkzeS6U/OkiaUPiJQK5KmZnxHW+ojzst/dt00+M
/Fb3UgqtcGhDej/5JkzSOGlcWxOUrRb8H7RnCoef/76/X5HAaz08QJSPYiyKV1V99CJXnGHcACeX
9WueWd3JoomcM7rCueU2MiAOUYOWGrBvNxSwgr120KHKULuc/rwZagEZfKCsz+htxFMlJMTf74MZ
sPBAwxsRUUC6LcIKZAU9Cbqv/UParg+uqJGwqQ1kmF19UGtqNnIrnBK03YWVu3F03LzM8OKe+Xkb
K6acfHZ6Ysf8IevjMvCgXb/lZ+Atxk8YAdV4gsUoZykFQV2QbJ2JOVn7fHFU2MENHo4FMcwt6yBG
admkRaUpGs44NbXq9U5Qv9vNaQkEYsJWXduYMpCnaeUmF9W/qdZRlvMh0hE4t5aDGpuWIL+HjTRj
AaS6AjEIurrqCG9GJlI6UJvJRHvJzGebMTKiOeIWKRZzxJfig4lCGCSIq1E6amygCHrwpuJ/M/6g
sKNGAR/Ji7sMdgLdzaHlGBQTqrGUBfIB4brx08nvKehCrWhHfuwWYs/lV8u/O01tMn2cPVknhYzq
0jCaDb7/m2iWB5ipIYWxy2IqrYeCSDmCOJI6ULZcGv7H+R0xhNvHSWk10gFKvQP7p24/15Gg9mQA
fsFRFAYcdR4HW5Iv4Eb35k4CbohCtDWZ37tHL/cprEd24fgnWgSgmJRSGOdbYJnGVQEOgu4ZKKfo
kc8BdQnSW0KIjUA4kJr0Ccf9q7pSKKEzyvN3gpkBJnPywqjcX+wvo9iQssJCcZUgxjGRUPErXXzC
bFn7YnF/9vRARwvB3CpVxj43pvild6fkkbWvaJldjsyBugaXS7b0aq8Ick8E5e0dyc5BybErN+GW
XEgKOsFHvs3kQD8oebbA2xJ92iamoddG68VhcHR4MFYOEvSgUxLTHQiso/WDJA3/NKzp4fCkiyIC
PNf2pi5Wo+jnY6zQzIXVDV1/D/vUv4LsaRljcn5iffMChu5Wsh8wf09SBGyLFWkgbiVnZxJiFwie
6ZV+CMNdDVpihYESEm2Z0aPjFwdAv33SGzV8L7QClOBkMwKQdTrcVoaFrBCacpRpYQPxtWgJ+7Uo
Md4R0nFfPwJirCyorGgdbmqHPEIaEYsDSfWkwERbYay7Zto5AFnmrltljo34KS59474OYWEEBG6m
ptEQvw8XdThYk1iXFlpKu8/Md0J9HRXfquAj9uk8HB1P6D6Xx+EdSTvvGqeVQAdlM8HtvJ8TwORv
LEKBMTwNHpoCabvj/LRsb4SZJ4pJU0FvyWHyZASawq5nyCmZf4ohMo/SBh4UILm2qTBbsZFkAr8v
eoZrChS8IDDFsTNUf9KAlPrUT2nRDk7h1NRDzeu5oLXvCwBPFhKjgjeeYr3Y03PrfZmq0F2B/3+w
RgaR49Xh7mE6n4/Y4f8OQrOeOSwFHx866EPWOJshpSokYKuOEapxD/eAvOH7bypAYMwhsY5b0U0w
UiAEe8vi2F1NinYDctRaimsKOOJoNGFWy3P6Fk5CZujeKTBfOhsFy7dy5ip0hc7KT7pJnWuotJai
L+2o4GovyC1tHB5TZqhTX6XKmeVvSbLbI2oI6hjbWKudeq0Q2ZDJGfUgQStzm0/6OIKqm1i3ZrXI
a/sGPj8pnS+Z02QsafIJaEDwUN5I5Zv1kwW7HhEOvq/ferGkz4GRKP+yLgVxTF/pFHQ0vGIqAbkL
QYp5AWkXJlnlA+VS7zo89dPN4nbvcKmNuwC5CTQWKCh2ZY+WpiL3osPYsK08peaNcaLUekEO4Yas
wg/zMqRKmwIbwa/EWX6ZJ1PSvOW8xGdiUu1P5GYGZcgEh/TITYjlPBW7DLOdc07FxOlx9a5OJ9wA
NoZzq0Qw1GmAElmW/w3PCGl6bLzbsx++inUZvcCXx1/nDn3J9NqKwYtWJm1kN7M537df5mwi6MpU
rUq85T4Kp0QiXFwL0vRrRd5allO1adbFLtHzWDuN0IId4rC2SQBBmt0YWqScnaY+mDIoKEGMmb9f
mcmwLLCXTosvdhQDEFMjFKBu7g4NU65l/z+wTgn8RiSXtf60JTM5Ecd+uqA6eXz1tWJbnX54L3JD
sBa1hFhtDkDGFX8VA1ZfjKHvXhQUemjcozgFkO8XJ+yXY9fcAx8jH1SEoAUlfwrwDloJpBB9utL4
wLuVrdXT5WuXS+h6yIeLy55/YjlXXsdFGxuFMHgDbyuN953bV/UWPuF3rC2462c/Xu5RGcuzEseL
lqEMsRI2/ZJPz1Ql7DMvPcQMO+Py+mdF6GQaETPQCau6aTzRtXeK6ZNrOSEZTh5tDtxGgfrbjWpS
mayFbKtkIco8EgP/0RoMHxgdMPf8CQO8sbsiVe14tir/eHYyxXH0E8/gul9FnazyIZ/aQfAuLM0c
TMJ/zQtoeHqEa1Bfr7IUojTwEduKRbvenV4HcAS3dTgkC8fDNraXb6AYzL8wK8z9CtXV5z/2xJDL
/3L+3sm+lhd6ENpVAT/BscKOKcA6eZzIiyRhDnM33Gc/72/DlLG/iUgsgY2B0o2UQ0gFzNDEfNsf
gdkyu/BYrmby7XlXZSoeX7jBQfk109KdazGLtYyOHIvR8VXxDeq88S4aDdizwg/TBXiWArS6K7du
alZHtE4oa4v9NMEayhlsi6T4KUafD2i9gI3Kxbe1fmqez7sw2P2j/S5y/V7VL1/VyFlBDqWHAErw
CMeAXnG9RMxdeBqtoeKpUM/nvjKYpcVO3kGXSKRj5SFJ6mJ46cRgg56fI4g6ytRupYv2Vk/lySj5
eMErG7ueljhmlgyBFCJzVbQgqqoIWUdzo7/oz+lBsL6p6kAnKsOVeJarx0IyI2IJqCtN/qAdFzTa
KF0k1YohE0d561g4sywl+JbdkwHjpD32lX4oiiH6sTL//XCL7YXnsxdpmziQ0Y9BfNk8ITmC98rI
8ItU3aZ9Pvnq985vl5SXX9jwzfTiLk8HcsfPqw9kTDvIiOQ/OkcVG7ovdxRpukTrQE3fdDvSjjLf
TeTDlNQ5eZUsChXdInxV6cNDydtO4vmnR3tgxjFOO70nDyU1kgO9RX+viZUSxO4jXrronN6ONED6
m/RM+ngyu3PSNL8Twe2Z+yTCzFy+4SkcwPxWWyPNY5dJ6Fn1XP6d/FCS7YvDV2BfqUVltf/k5Fhb
ebTgoslsJay/fBDX+HzbZimNiuEzTnj4aRTQLw3nzL7Egfm6d63uahPXhgjxJebKegA3m+0HIRm7
Sf7oZWfXdh6WX9QYLadJWKo496O3EuYbSygCCCyTHlZQUoFGZH+ll05+ca9skPG5bUst7TY1xitP
TU6oayLcODeRjDjunDHK1wXXIlGtgTo3/HWrsJ1U1mpMvtKrqmRD7IxLtCswL6PZ5HzbZUpUeVOR
GUA8dYJQlJun8gLOnC/Oea9XribUuf/5/bb2Ua7U+LG6OxINoe1rp8FF1U7xG2rgGRXu80dxWtnz
nW7EiEcaUyz/06TzQvIraUQvdyDmNxKYytDJzTR4e5x2sRR6d4MmNebnvR6QJtwa1C3R6x5XwZ7U
v3iKw12xLCr6j8DoVN3mHSpsT/EigsFAntDFlZL9mqKDHoxpBGmgV5GhZO9WlKaaODZhz4jV6gr3
/OoLtIQRlddaO7O04nFMOrVhpBHbEqQx2+7uwc5ewJ1l1x0tJ4B4v02TtSBCtY23FXrI0StnVlPt
YrxkGi1PYoYH6RYH2xNZ9UMO9v3i6r/m/gh6hhkDGvovyA+VNskaBG1gf+H6CDJCpjg2tHJapVzD
uBcqnWNX1VQyRBCEulJ4ssoQvEgQw537TXerDGeCE+u98opBu9PJq7/bMsnPzj5dIUAGtHsvzeVo
UW9agXd02r4lGtmPiF36FRhc0Q+LJViBsIy/MYi2PcyeDnaYdqaj2dWSvNjG7nV8KPDT6zIvtj9e
ZmbvdIsCLw3WTKONWbUIGJtQ/eVJuvXHdOFIGjGVJ+33IF0To0UFyZogkpmxN+zK/6Qt5Vakg1/5
xZDHUnlP6yfdBGAD6xMdpNNA84nCoCvtxS/VaoLlXNupH9iSWvy462QfdDbuRl9+Fta7/zcwDkY0
IC+1/ssUOeM2LYqqorzMCXhOuIzq+oiwC4zWex+Yz96NdMz+KS+x3sfFqXDyuGMp4+8MOSm2krmy
5DYG1NTMkfFnhztkrlxQEZP6XWRRNH8CEhLJ9ki+UNOV+YOdQb/xqcURgZFeX2RPNMZajzn+Cwwe
7164ls5vlAHyqvgOG4KIaPFx7EmwsAO3RHiKFVEWF7IDCBuFE23TjHq6WDQfuR0qxtt9EV2XVYOK
A9ZObQJlPkCrX3ydz322QxQd35zPdsV5B8w7TvcYrzts9WA3pAj5JlrLoazBpKWEKHdlZuGu1CJN
Iu9RTHE8qkDy2bpK7SjRFsJnOTKrVi6yGLBnGo/tn4r1YWjBs39eIjLVh1D0jGC+uBSAexs/0d/j
8clOfwJVIYM6UCn9b1fzKn8l0YHkExPoiIjn4CTim/Ztn3hhNcVVMBY37w8MX8h0Id0BkXvOKXWU
JkrtCx2aT/gsSmMdfDbqq+bJ1R6b+1D+XucFgybc4TpMJA/1Jwh+Xx2G1CsO1PYZzUQhtYCrnEn4
hEsJjyelpGl2uTi/28CIkVkphprJasHaK75yTUUo766lPGgVOLIZJVZQPombCxUToxprZG4jfFI+
1Sq0NyQ7MN6q471aXVbna4/yFMZhoC91pMu2CxNNPdYs1cPOC6v1POFCpO43+jcRaNNv3ruAi286
zQ8rW1NUjKUgx8giNFhceXkcDBSZD5vn2Rdwu8YBNsklHoCV3/0muWJezxdhWw5iq6mLsdAg4S15
aaE5uP44o62FKs/05VbWI8UsT98/LcCx1RxDO5tPKL0C9SdOtbsABEz1mumKtvUtN2uUickaCKk+
E1jXiyRoSSCxLeSDEGn7J0B+24isLwIxRQ0rPOtYCdWn07qTSvsPJdMihhRkAonU0dqUbO9VGc/K
kovTirAcjhpWb4wJGBuBDtWqfJVKajDApv0EngnSB2sZu+KSXHuULsxQ4LyrmRwD+iDRSKjNvEeA
Ah2aq826HKSjPoNQL6UxGuxNm/Ve1tJnPnw7u0755feqD2GnKN+6G0JQTFIYFR3vCTyE3OcPCaC7
xn2ceQ+h+Mg67tFH+pYIA+YWm4rvhp72qVmYbcGBvaOYCMf/OjPCxLS+t0xc0UF4UB5zFaGtKbDG
4ZLyBYyn/cMcMST7ujfhayK7HnsOAdZcTI9gvnQdc7wzrhbpuKLumijyKX3QSsIUj4Uf5zCbgdYv
24ZMbCDbQLUQSY6444sYTxlmkl+vGFihyVYgXnkmJXLtuGjgKFXKY2NI6sOdu3QVCWss7VtNNMQx
7tST8wkO4e8FRxU+ZY2jIRyUTY/NoaBCDXZc+JjEeouF0y6TxLA0ZfDa78kyp+rrzSL99j2zcp+B
UEsailSQJKaZNwN+zAbeOphA7SeVmcnUDfXDNXymB/2fLUNR9FluqOxTKkjUz05oON/K36/sVoKb
iFtV2YQzW3TradA+8vlC2s/Z8tfIL5fxPMnDY8mrwsqPHDSTpiICq3vi8sV1GBvtPaKDlV807wOc
nVJXzYBZfIaTH9EAoK4q5i/yRVoRGGOT8ZpIQrm5+ORXQHil6y9i18SdnEIqV0TfbEcIC+FjL/ZL
rlBQCF2PCnEV2cyC1+JDU9+rD6lLeTxz3jl363bVLLrOM930anrJuYpoWGfD7zke/YCnbnBUtvxR
CGGzIwTA+5t3ypR262gq4NRfwwM92Mu/yjLoqWTsn0glU2tTHXnhZYUxpxTE/IndbkmhTo5NxO6Q
8UG94flKseZGvbbJ3cQa5KqS10ejksKPGOMxE2tXFSC6Ro0pbH7v/ct78OOpsReCrnP55ocJi5ea
5A/pJpp70azcp588dNRFHcBcxtEP76xIivhTCN4bvF3+fCcLX5kiR4A+GsrlJNaHV1hBjjSMeodV
lctR7n5XmyeIduVRHx7GjvbQ/6Lk4is4TolWM6gOBioUptcEkRawYP+5ozpTMxuMgHf3HxEm4eiF
xwpzItscZZZlNBZUI9ov3mSEmmBK0eqjZhCO4euwuQtzAhslCk4M3f1RbXr478QszoN3uwUmx8Kf
RFKO+f1eF/LER4er+BgqPWhH0xD4FEe4AsEl+M5ONnaEUWd+4C5hhTjn+DJa1Rc+nxz5CnVEbJK+
Kj1gNZjtUVKhdGXCpyDWU6SJd9cRgfOXhiZoEMkGugIw/ai+UBu7+DPwX8SkVBkKhrajzQnY1B8u
ywQmfFsZNh96zSrPq0dc+Gyx5yHGPy1kWF1Ehx/lCu8VhUNC90MzV4cmAEbHM6jEguy8rLjLtQSD
lZgAI5CiXQhhHexYO1zYFHNQlPKWaUEAQNz1asQMZd87A1Tj+uetSFdwDad9Lnao06mNV1KZyA3B
M/wF25a9zJsTtQ+V4fom9Q+q+Y/BqzI/Cyzb5HDfB97eKobfqcaF9XoJcC2YXfyQn/jiINyk2N3m
jHjnW0pZ+1rI/Trf/9Oe73NNE7+g2HPJ9i8bRNKmEonJg/zzUNFuGqoLmzkD6vP6a8xVMGyGuSu4
GVUU6zouQhjmvIOpIdKBCY3T1YnJv5scGKMNrLxPxGZ0WETQ5kFNZXY96Jo0vAvF/a6xZWhQt6g4
1GC+u8eZt9Qo6d+9MC30vf5mGsw14Kve4cBSFTcvTqjzX18os6p68678QUN9dHXWuYqZka0LTLuJ
jFTOX0q4iVvNK61sioLEvhou+JkPzpP5Mp2/+w+Y6SDHJy2jvxzhDzJQrm6vW5w96SpL8STlly4L
9xOpn1NGqZ+otoF3InLkIZhzuN9Nporfd5lPFopFLwTUv3FlqeukRcTI9e6GkT1KGDQSDJDuvxG8
2D9HswicmF7wzjI+BiFsjbmP2bUZ13ErSmkLmwioZ13Fu4nQStCb+Q8hCIHvf/wrsZOvhAAN1Val
LRolWaNYxXPDEoF+mE9LEUjTM8Wy2cGbDQilCn0ByKtanB0OJuHtkiyyNmuqGdKa78+qbrePVY/L
J16kD1dEgHEjKdrP5bokoobU+ss17HceVHmLKKw7H/35V06w5QZn52w6CTI+CVSEpOerIHaJB+Wo
kFc8B3SMTn6VGW1Zi+8qyED+il/dlCl+zdxfOrDIW808JWOCR4JEFNI/GpOVb1J2tg7z7/hpb7MD
zGt/ZLOmL44UwDfwzKPfatDpXPfyR3/5LgQ1RHOBIZsqN/FDQwsDp+VFfXGgj7CzoBP+oKwyly+y
hcK4G7bOkTZzj7iv9Of02opb9JOlmHOOY2cokikNL52mh4rcwvX8v39noQqCdbwKDMtqsAF+3tVU
swehyFYdGvWWGFRrSePY/yspiAb0jMqleXBgpiPQiyB8SAlA9X0j7ai/vA38DMUHrssAEg0+snjB
pFcrNJ1WhfiTLzFGvg9pAQcebv95YXlBa9w2sQ063qEtLv0SRSkCfqeP0Frcmv2eqdHPfr0ml1CP
k9SdVIlrpR247esffWMcfTuzDu2X4dwh6CUyLEJHUPLhUAL4GcknKEFsMfG6WsWlJZWQwCGDpG5s
CSAPLaH4kglRkP9f66LBP36NT15buAuIUOdzpZ1ijvXKV0yCQIfRxaAN48sKw400WxH7pgWoCD3X
Bi483ngeb78BeSEwlxaTNbWHBT9UcX8Gu0N8NbcU4YtZ4V1wqllJMp43Hay29M90qDGihq12S9ac
xY+KxRLuQFtOEcKr7ZEIzUZegmJo9pdrM8a6Xz2IAvQRWOgj9RvRE9uMVSXgRBuz2hjPcIK5682M
rTPyzp+3r7Iil7Es0NoX4+47sMIkyyn3l+D/NaINKRqL1hd66nnnE1ZPuDsaOgrLF64ecQPAKOW+
+3eSrXowp8WYD/ed85Z3FNPJfvyKr10tVBa08Wz6oz0zsh2S50rV/G7EEU3g9serO2/eDUgsEwAG
/w5znUUs/i/JE5kUhcpkU9rFNmi8KBkuDzLdwZpP/BCL+8JWmo5ReTgappFaZiiz3KZwwaNlskY1
yZiYnoynQdXhzkCTG9jVbsgZAB8gj+Xym/JT6la/3SMW0FBj4ohJTEIkzjauhFDOwu3BJVXgirQF
vgifR3Ffr4g9QGh0PBLcxp4u1mZGOPLIsSpec+qa1mLIgUBZhXLpoN0nr6Lw0XcXm48B/wWN0Q/q
6TExSOvJtdJj3NP5GnMEmgLL/Bkj5RmzF91KLyJ8ysrtrltkjsH1MOfEf6+IaSN8uM2Wgq25pLo0
trTT9RyAOkdaPh1fpDr+tby7xBSoPElK8Fm3DW6cEU0PFZaT4FoJvxI+CapTDZBHp/mifJlGZMGA
jsAaMZQWEuZ2ftULkU6XzG/hYA1Un6LysOnhyQSb8sjJ0mIM9MrePZ1AIhdRHhb0xBBr7nH8D3wB
9wsNKfjPRB5ncL1J6ewXzWTj3CTUfcKuZthVqn4hM6fJ341nJutRHb9RGmXtEnLnEbhuq++9IpjN
xXze8IrDe8zrORz6Rw7ginRkEooNyoBvtVrEC3IMp1K52YXXMdK5TJq8Xts8JEd8JDD8DlzHVs04
H0PHEyWNPWxP/cvzKn63a3CdEzSjgxMt5WJLsDoscSty4fZM9KosjpaE46BM0QpzpaeFEL6/ER+H
zCf6yn01MugVbsokjnwzjbsiTP4jGRaGHgBJf9UzKahLZxM7n+KT18gfkdTXmX+Mr4C42gsKq3Rs
w+xJlEh5bMH3A/0GRNIjqaR+QCCb/4iF+ksmfUC36sC6CKEIBaF0zizQRE9+7iBiK4cIxul8Q/mF
SwNPti9oLz58daHQUbOAez8qSUwx1zhzSlIXUdxG2GYIh9llXaXg9uKeuEbeHetFPziY5NbE5ZmJ
uRse7s0hrJ40LpT80NXwwrNSIqw018xAulIhydl2msub8i1TxyzixYMH/chBO+jy4thCstwcrJgu
21EjJfvfQz8BNLC4BFV6BH4QWUX4OazHcOJ7VXyT01ndFpIEjWS1wb2d6KRi5TQJSJeYU2z4V2ak
sjVApxuGEx4RCiu7g+ERQ4m3mQ2yrMOmNZoAP33Cf7qD9SzFCNSAsTVNPRp3jOTBAfCRoK5PC/sB
zgOj0CJpEOqh3yGzaKe2FiO6DqBtjMOxAf45DWYivA8i5TuLXCPO/u167Qd3HPW9JGVLMdQB0VXB
msh19ZsoxUZoJ/8/Zgd9uxtA7pd7Xkp6JYz3H0JyzwW0kT4lMoxJnSApHRa/OQeuVdi6wUtye7aY
E4NWsnYKwNMz2ZoLUtjLYpvM6HpmEGlBMmLJNBatgWYRYKcnAwzYtbRxP8MNtFhjPJIR2B56w6nO
rpO7m1ia4FHSyg/GS+xcw5aCQbE9xaaZ27wnqL6GHZHEgia1JKJE3AW3aSVOSw3s62eJ/Sd2AzBT
tqOgepBqh3EuUZzlsOU2VT3CRtnecJLlOnLJMEKeF5wuGa7tI5/o0XBvuOpFdOMpEdlxxS+jEcI7
ZJdJEPnK9OBmdX0T+OLr97NAxpge/gjkwpZD+oF7umPCtkJDMioDmmfLdlWjywvi/9s+nM4qrGvF
c2tWr8JtOTxJZ2C05I5m21Rhjyox8Rzd4/py12u7mWRB8XwsBgbeMka+FqoEIHmDUZkPAJ2FP7Bb
2INtD52nIpLIAZUXScQwRvvgQQ5qm2LjVF+neJXt+8UYU458ZnWAtC/7McDzIrvv62mm39jKZoVZ
lva1jUasSNhCfK5Askmo/YcqvCH0VSb1gd4KCOBOOQr8VL8scYLCp1v0MND1RY/S5CaYSf7ArTvN
5Ga67uXbOQieVR3sFLahCC3rJH26gHelfFGN+M4L4IgOec85WoIWcSdzJdTalJOcSqIB8TtNChO1
HEvG4yy/0E53fP8+VtEJsKf8t1AKc5hyAZNWMTYsPdk/IryyuT7iE7y5K28JAT/wSjsc5M6t5sDV
o8WIFvxPCQovWtN6Y2Z3HvEn6+r/G5KR5QOhUgmjhFLkYW/mbJgrSTg8ZO6aY0/8V0p+bOWmzaHV
q7PP3Kl2y35o8mnvNihtmx4yDMYIDNsiprAqlylLdltxo6yJjjJcSCmFQ7JslMUqicy/N9Un0XLr
Y61UStLlA3drIU0/ZOJdlNUDkcyIBipWNKpgwutx3g1epCTvNamvWeXAeePH5wfYdHTLrI0mLPyY
6O/mx6vvVQ5M4wSErCrBp2gqipaFSH7aMQB8p0ikjBr0sGTMjopbkGdEjH/T9ZKXj1DV/lglBLky
EUzjcGuqS41qUuu0YOS3WwQMiL2hzrdTxvuwF0eoqIEzY5OoK/RXI+kvWk7falNeFM6e5L05fSso
3Xv5bflASnChIC6Yd7ie0Vm2L4j2CpRLoKX83ok0/MIVzhXbwz5An7wBzCTOKHmQLH3aTY6a4l8/
2xq57DJQp+pDAMEGxy1tF0W6MntKf9IxIZKDX2xb6iAXFLJjQVgS+SoeKC/qtDvdoHzleNmQxtvO
c31X643JJqGeZNoUImeCkLUhawWib6BU0BOUnjsq5rMzuEVwDSFFFf/BAk9HAf8jD8K5hNucecl6
OFtySBWtID1XyI9dDhJS0lVvEMK4/l0zdam3QCgyguUELOtg64sg9n2emDrPpluH9oMug/gOSasb
qo9VS0PQjL2/vmwD3YQGQ3xTrn22JrRSp6Y2dBPOjCSexY5PNBLXBCPLpv9umTBewKV9kTS3w1gN
Dj7en/WzLqo7L/Cdk6SuVFsbLmmeapDr58Y5CRVTGq5ju66sxE6a7eEWK4Prl/jQsQsfeSPH5p4y
11aolDMy+pM+WBHtepMpViM01Ofd3geLLLkCfOJf8n8SmrxE9mj1kbYk6+83rlQInZwHCk3PmNH3
dSjrMgaWszU6Mr5LdmefRNv6BuBYAN40Qx6Dh/2c6+Q1HhZDxzLNq2d2O8CJEfzcAFvKPGY98/H4
FWX7I6cfc13x8F20svO0gGhUKkCUIhzknVm67Gl2Sp6EyJk/rNgdhMjV6C1LPAZG5qM7Dvx3y5bt
Yt1GgJIU6Ya4IcxNETcnT5H8aCsG2LEJgUcPaF1ZAr2OPX5MglfmLsxma0aiqT/1N9jTiuI4hfVf
DiBhQu4LfO0bEpcFehu7ExlXu5BjRhpmBReGQgz5aVMCAirGIoggZPxzN2Q+JcA6AVkuUCTga0iF
HLQoGF1ECe5AQlsNzaufO5X03Y6j9pzPqCyDJ9FpuNxBO2C4HOsU4T9+Hl9xUXlSHEOCM5d8L5I2
CQUshqF+yKesdZ5+6MMFfgT9Pl7zcLbh3sfwth4dcnc3pQ0ptjw/t+H1MHIpDy+SemjvXou+96wY
KrheOghHP9cDjGFwMdYYkMtK8TBEf6kvLOoVIoV4hwM0MyVbKG2C0sQfrQ/kJ75FHYjtaCjjo1pe
hiB23VRUKghNCFLmQ52lMtS1WRMN4HcP1lfAaaNqwfhj5NWL5Ec7PxJaN1OVstWDmD6ZK/TZaWbD
QIwl2ymDUQyTevipuHFDIJFnFBQDvkysgG+kjmHqSG3IoyZKFCiXWMj8IWJoGKqzzmBbYPbRqYjH
3zfiARrRFPeniWDvuBR1MHxh4fzoU8UgmwOvhG9eL+sWTUlLZjKWSzqVDzJp857fpWmBKR4EcP1n
EL+BHg2wrtwgd7d8sYACREboPcVnmmCi6kZ0KgAPxCLCH28o8tgGtr7C4ZnV5sv3ZrfdVkve4rLU
sXIGemJJtwUTtLAgQ1nK+oNWVtygCGha5JRYm8l2819pngqrGEzGWYlHWJvI2YC3vrQ7hjT22sSl
PPDrNqMeX5DlB7m/GuGo9UJ3xVOqAs6sZdkdb5mXCJPlI93AT+m+WMA2/z5iIIb27vwYwahMbWiI
yNQTHNuETU5PwtwJRKgFNFXwFIHhA0SwZQKWZlml5p83pwfFG5q+lppx41jyiKK7gHoZYJxkgZef
9CkqlZcF1nVtA9K6vTrDtEO06PchBDxJUqYfUMLfmtpzJkrxPJ78NRhwvOg4O+GWY4SpysAHa4zZ
RUPJn+i4F20g1ZfWc5k+YL/XJj/s/aYwz+XakSxCRrORgMoJlI6hWs5Vln1glRbPiaG8YUq9xFok
GHzK5u2DWnal+Jm+aPy6r5mWDkcvRaiZdz7YU9BUVCZa/oxsTSY7How1eV0FNFiNsqWdF7Ltyp3+
mJmQ//jH1I1EGpGWI+X2LvOF4g8+J1LW6kovMbDlkNgaZKM28MOu14hNr89dVt9UhbAvPUvyh1PR
EAhIyus/ktODjPsitDE3TqrQWREgReZSQJmKixPSpx6Z9gjKRPATrushTz2jY3QKfUuwiWL2ZIgk
qDSUQ+YN4BaA6R29UOzcLMQ9RA8BjL+KCdoVWnabFG2vSLLczbhW/iqS7QI18OdJF6D9FaAy3ns3
ufQBFioGnmF+n1ZbvhJRXahauCCCd0Nr6/UUTPssSFkC/82xwbJ6vjwdePehNChlYYU4YfEXnkkG
JlWmMfDQK6IkGejD5e8MeUavv/sw4UZHq++EIH0FZE6l+2/+EXny2u+UHwbhsys3swgslLRyLX9B
cm4kP4ZSmreeqka/WImQoESQbtkeK6ooHe1sln4ZWvyYA3O5G2AVhCNIFKyHmvpEcrexKBVY2tIp
OBw4yA2G54FCttGcc18fze+MEM3AXcSSVfdmaMs6w5tEpCCWV4ssS7mjHtd8TPRvrwLI463i2Pe4
QMlGbcDgsp6M55e+ifgihU7h2NrV3GKyOr98d+LfgSAqLmo6PE9w+B1usdW6k6DEBOOBoPpqDZvA
21L3c1TJ47M+/hyD9pp/qREBlt2wgKlHp/Tj6djBY/bcDDgzjIvEXz/l6AMLSQc+R/Mp59zL66EE
H4gW+LDlfgi4DSIotCLqasVbuoxbojrn/6p/+Lxl51Swb0v2OjPvSIYlxL3RWlrEYMOHMj0jRVGl
kYXbv0amkm5T+RubWcci2/fGrRGBUX2DRuSu+t3awkoaaKpRfnJGb6NJlOR1wGFQg8YWRocRwcY7
VFvB9q5/EZB/vp2J1j0AxNXl7+q+HJIf45oNHWx4GJls77y2nbYSb0Y3MuFc8boxBhi6flkYunLZ
mcrTbE2ynhlucFd8qIUXvu+xTd47Q/AaiDgMo3a60ABlxiRy0jvVLXrWOs5ZRaIOCP5EtLhya+hL
y2M9BPGObZ959L6DVdjnZAH6azm0VoaGITxbxxSDRlRrHRN7W7Vy4qFOqDGAfPKMBMM7lSgVlqCX
Vs0PoUSj9120cSPRDZK4C+6sY1yyPAO6f3xBexrPVNnT8Wnpq6cGpC9Pm1Ryjz5UZODfYuO2Dl9b
Pw32QUe2MR4+86kCdgeuPjLNATrQmZtuvz7ACKtm02u04yQOGm1zl4XIxs13ItjCWVXpyhvpETxt
3ZMMa0aDtErEv0cPjlFNEg6CNbL+LXgbl9zG0MG0sPDaw/ami533+MZmmEIzC61kTU5eKxHXpUgr
OarafiUt3ygK4qdfYUUqny9o+b6+hal3dQCx0Smx3CdLQQzJ0s6pecNEv8uDd11SPouEziI+QZjP
NpptFC6FnAOv/3i0smVnS92hO1/Lm+OmqWdMHYbU9iEoIGY6yODr9wlQmjjqczOuVMzeSHPwaomh
S320XvzSifHV0OE/GxXICopxV1z9GzQ7bnwyHIl7V3iyFiHQf0dW7Iah9Lh8jPi1B8iftCn7r/TP
JFm/RTZcb2SR9Ff+MVSoAIqRKsLMyudBASIOcsYjaGz3O8YjgSvEPvH+/26bjLZXfma7A++/PaQS
x0TNhy/fnXkbfemeqGiqWkdOjs3vExdMXG1AelrBPh50v2X0Zjv+0g7qIKZT1h7S00LzJaADA2sc
L4J/1PN5RrDlU6D3DDusaun3SP68TWAqtQox4Ztt5dnjxvtLOQ3Vu1CCJFVPI4VgZ6wtV2+xJCbi
NMBRJwiBUlnucPkmzypBxP4kuF3XSQRlDoLeLZl5PF45ykpN7IeeaxpFMxZ9Wyh4lHdSeac1RXkq
Izo6Bd4XF100c13OC7tqSVqu+XFhqxC2hy7jno/qPV755/3ayY/p8Q58Cz2paIdzWJPpbyyPiQHM
hBKo7dj5I76+5ShMn18EI6ycF0RYxcRuJ7xuT06joFtQx/0ERiNVzY3X2FgbuZO4/eNZi/3oiVs7
sWsoJAOaEcHsJfI8Decp+Vw+NQhU0Ju2yduCe81ewCJQyXA5Cl9RPPnE/SFsWCWUnRi5BVmF1Rmc
nYi5tlmwaSokiLxr7Dld4AecpIB95hzdp6Weo8Sj2C0rBvR+b5TgzOrjRm3ENPSwmXVKCBo25HNm
jiT8EXM3BOEmIkHNZ3XbxaDN1CwuYERfxxgcz+VVOxb9pHzm1aPbYqxBlJXJZkRcDTRwmSFFSprc
XC+SLa2li69mmvAa1oEBjMux+8AH7FUE8l6nZigAF8Zp45pZeNoo9WR9hVSlhaG/NJUF1TF05vLb
B+SGkbg1d0fsFQcNnQEcVwd4d7Jtgm8EsWUa2Hpy0H63+3CN0vDz9MgiWpImOFO3rlFWLPxK94QR
bDNJAQlxWHTieIOfdcstIEHBgVxieMtuk4A0/fp6P4nX0zIEmtespcPnS2yd96lE+YjmSqmp6tsj
pnzUMDpLgvUxMXfrbnPFOPxwx0ojJ+QLF7tt2Zpas0MB9KPF2jI/lvTa3rKvaGRfT7NnAIoDWIpg
H+ReeZR+8WJtl/j+hQ7lbL0BxrWGGarJMkYJUOoZxsHlBNgQuqhSnXm/nKW/p8kA5tszy5X1GT/f
p2QaS6R7jcFRYQ4dlyb8n1w7hJ+pTHzKjDPxMjjBUEDVETh8vKvzm60mC8zEz+JzfEl4I+6ZGhHb
4XSZXzTibwdMhQ9GlmfCkZ5a/FiExlgjklfyzenlbQsGIh34O0ZnKomO0EDZtDiJNwnpEruuKM+y
+oE5SimAw5oXprdcm6uMos1tHspWb4tmILm++7nNl8CXAJdwjz2nnm75W/WsWo1PU2fsLUmV8rBl
4lh8VVpEBV07NVVk7B97TLou/dwvPHCqXvVj5M+6MxzVlIN/XtiSCvd8okEyv6Z5tm1NQjbYPa7F
i7Z+sOj9KGQcvLTR6odYJ2DwJwZ+/ZfeAyxMOZd6mT5VlTZoIZLxgcTf6dblEpYHUOEcUfpjPdNL
lp0rcssUaIqgT1+WCNJWVDJH+tCSlWpp8+FkQYERF5//8qI4SudRcRdorKAG2gCoXhobqD13868O
jCeRi+vrJjfgJOuhplPNdlpm+Jpy0ElIvkZpirTKofTZcyxmsd/rZ627JCJYnmLjjdjKUQVsiUDl
zUEykbyWYFD7csMxZez/BR7N2IbM498Yub02BRkjNpPiZSgSorKkCV7xyOkb4eTc9+pSjLTVzNui
X3LZqw2VjfVbFObuzcrTOHSK4UUa+w4JN2IIozTlNfYcR5jWLqxsGUgPAsI4LmEILSKl9Piojb3R
S8jV9pHEom83U2nneuEi+qdshdPBvk7jBCBcA8inqYuiXUBOn6zFQZGIWyx/V+E7Bg/u8NdmPngN
Or6HkbBSUfCGYtyiCh3iU79bT3NjJBubfP7OD8Fqu1L0KDkZ5YFIdTo+qZ18D941rvDofQZ7axmH
x5TLBhRJSkiO3LCv+lqTB/sJIfLR+z2SmCdGSTSKSKyuUMtHVLclVAKXHCUpGJEHFhPbb9kWY4D/
zm834kIfqQBTrskyn4qXSKGETqlqu/LnhELQOTevg6ZFodpz10RxGGntxb5Xah/kBWTSjPzFM2rF
klTezZTF2mI72A/PBu9wCDAo4rWYdQhcQyYhKG/6m9h8wJ5J9PZb59RYuXgIYDxwiSJE4m5KOV4W
nyjZ+WByOob9XxyfOfmEDYvuIPN+Pq5oPAQYjJrxat2sZh7RTzBDK2/FeR48WM+n8M8QxEepvKRw
VZ7d9XXUxybvOPJKWAhuVFk4yIrwBIwJTJXJka4P+4hoeQQ66kIeYomsEjUy/M0D/MHyxofs1ZIk
Nc5F2heyrdWTfK1zCqqMlMHRcjI4x5M1DQDsZAEPh/BzjiD0Xb3C/Zoa9FKOuFJ/XUwvHMJ0qpbN
pHgUVjdlYZaFl9B8LzV5l/vEz10ZLnc76UYUvVkp8JH+VPz7oiN5a71Muf2HAeI52ta8Lt4NQmCz
Egq94Xhm9zlqlGBt5XxkQk9RhUF4VtoJaqyZYurTLd6qfwsLoX5WfQ74TGP/hXVD9F8XqD0+nQDf
j6eM2NqI15nF80dmrxVobYVizYSKMn05AgDbvnkqh5W/wNB4rKJUVOk/edhRXbbjs5FnGVQ7JMK2
j28PxlC26h7v/6D0KGIQjWUt7d+pF4hChPFQG3juUON88d5RXwy9Yw/JSHSpcV0MIGFBsGoR3O8V
hEMyU+L5XI7RCNLZ3UJtI2HtTLvWUmiNHKShSzVT9aEaCKYGxqLZq/kmgEU1+4JSAf0i35SresHy
Sn3P0vYWPLnk5zhxQb4k8qAvvJN9tRwERUPMguI/DsEBfsPCGL3LnX+kXmLhX833n3ouuqtUqili
nO2JhRNHmBaOcIYCfMiU2oajPN8j5gmRLKgKstfc4oihz/zR0YxJUz1OkJDTbQ1gpo/DpDa8GhJb
3wEnSWuO0ZwBBR6qWh2+2ypQgdqLftoqLMjVgm5c7SDe5N9E5BaEbanMfSEqnaCrnjNxnBIAECEQ
Ghz4CXU887hDX+Sn2iMdVV9SQj1UY/THlKt7HOOV1K5BZWYk4A08/XFFbdZkANhbqUDc3f+Qq+YT
qtYFp3kbKJ3UZjstIiHWiHxBWj/JGXFanbj23nOvPc6kYiLEPOCJkQ6Sq7TQLxJVZL/zIeYRw2Kv
L1i3FUowweVbqn4/HmRHWRubtK0b9S98nH6bE++Bkptuw4rBUhX2x8UfLHI6Nc6r5CckHf28AX03
Z2zSKA1CEYQCEkNTts27kWryq5WuBM5y1vaea8YpEWyvmuzp2MzaUR1BDf4ES70V0iJKE4YZ9Lq0
56vdR2Y+7myDg4rlSSIONxMSeFjUzZLqhfIgeaPVLyEMIPMp0wX/HpcOsbDCgiegy/iUOx1xCG1k
5BL1XFQpqhfe58njKMZNVeskpZVKg4N9tPYZt+arSgThsVxCpzfvkUxSXwCqvyExXP/SU8EcXsuJ
0Sz4awlgX3XZdsTrO9EK74Pp9VKVyemuaKRTzV1bnXXZvisxawoVdQA9vhfqnqGgrbu6AWLs2pRE
XJL4PpmWQ7oxXQKUsRXV8dTedF5irfbFRM+ItxXn0WJ3FpWWZW4/e7MZy8A67pyiN1L5msBP5o9E
LjDoxoL5V7KpyVPe9jLErm3N4quHNCazen49Yz9K8nEMJd6Zkh8//Iz4EQcVeEHl70PjaEPUvXzc
siqLeCq2axAJ24PzuR7TyNBYJye0RfScaSfBFw8Boo080UUST4qzOUhh20DkpqYIBzWW6njlIu4d
B2eTPYZQyGCA46oRp8UduY+b1F59hSS8TonEBZgCBk1O9KzmI5cldKy3fQyouUvH6eQDpuVtIACr
4O6HCCt1jILXhT6952wkuJz3ICu5cUe9r8LKhVJinA46NwW35boDSrzi3sKvhL2Bj0Iqg/uuzJoX
KFdEiZyJAVpX5aV3NlB70M0cC3kkh26PK3vb0+Qb2167govH1McPvc9Fd2h/7HkxNfuAEGQL7B6N
6gDEYaEncN3MN0Ovwyq1TPCfKZfHV5PQ2beWIYjovCCAqmB5lBm2SVLRx20oD82X5C4ODZiZoiXA
LROYsdn0s26LV6XmEbOUpAAvVrnH85l3SCO5NVoE8rChHVdY0sUrGlb7+4g2H8eoSK8xUdUnc0R3
SYqvebljrADaOf2HhudTTby7vk95arpcfvu7Uh/dzTnVlQxWJRjUQlx5O3hSn1pU3zFdcAwTLxst
uJqccSkJLJRTZ+MEZxJqBnNI8S5OXWKvTzdW0CTpvgfHQfwdV6ToR9mx4bI1OAZ0Y6g26+zGI2ie
SMdT83G0OekErbI+JtHRcJU9z0CMr92OB6eUWe8xbN4ijLvzjSa5ear2KjpBRZmx7OZbaEDAbKH2
e5oqJqRPmM7QCk5ftqugefBUWXCGvMBKVEcvgZ6ziYylzkaOgxNdormf0u+RrJ+LMHMEpS3E9KRL
mcF2E15/VRf7KfIDdpnFWG315v4AkxBLA8Vr5DUSR4+KRFdxGbnEvyeozI6L3ZBc+2I4WWzeO8fL
8A/3pFS+iPzZTCpusgLOEkLLd5WTuUpMhClU804bKHg1Ne5sVIYMC/8F3wRnprC2IMjFAJrvSxoG
FZBo5wtFx99X3FXjylZEIbXkWowIekoO+ww2iNXWnqP5/hcOkyngIvKHlTL8TssPJp/MTJsN7qwm
7HPEsrKX4ap5OOu3WQlYbyJZb4nXG7YPUYPggneKVOzCHt9ozF5vvbWUjP9aNRJpuNf2kRtU3YJj
Q1zq3hZpUmqvuIjynkHetCRJIqDDhMBCyNCmHW8jGw1xN7NpdG8f4bPTjdGK+cj6T8O7Rws831UE
bFh9OnB4yTfHdbvlNNOscpxufZ0TJmeUZpqOak3sV2K9j01ueV7tgDqyPcUpZbVXn3tNxi70zR2h
47pLQM40LI61EctcmmQE5MaA7v/6aUbmYtbvkWsSfOJZ4GknDFKjNCCnqELxn2bHHUxTrjjkvKZ+
+z/XTtXSl0Aqlp67M7EdbzCs5fzH+mwwR7E87TEubi/azT1OMQLDNqQ0oLpV5CualXc8CVZKNLGk
+mYLJoVeLyspFZ+aQCc5w7Szg+oz/bYnJDE+SAulkRi9gwRv9nkHMXTDitJ8oGInMiRwIl+taPbf
gv5j2jhfdejowFubX5HKwVncxWAgZpN/UGwEo4XS5g1T9foaeC34KfXi89Z4WjREcFHg9qReNY7S
H8s1rSp2u261BLEWyKd6zbFRQ4rlNDwWT0YuVKjxAMeT/yWc5QRui1apQ8KmDM8jWEebd/mNdumh
UpUxRMOp8Ut9SjWiS6R5+LbBkN6iuG+PZZAfFEqI6eHmevjli6TZ6DdekMEZmVdFp1YGXENopf/4
fMbxgp5TZMi708URoZF9i1/s5sgX1osuLjiSDd8A5Jp7u9ac/W1PPnVhd87czM9sQr5E2v8Hiz94
c0mRMwEIauPbbpj8iRT6cOLQSi4e4pZw6dK1jpvYvM6bW/iyX+1uO5kIavwLO5z1ZsuOHUqMk6Tb
yJdXVIB6HDv+3lQJ6tY3MaXhlpIoUMG2oeuVsp30IvOqK6OeyvD4vy7LsY0er88v07y0KQxuPMWg
TBBx7QXU2swVze//6QxFK7TqAPhQFZrBTIPFX+P+iCCwtgfWcXM/Dqa9YC1Ue/t7A8BmGIs9RcF/
ddu+siDuDe6OLT0vUCM/ZVdw23A+oOD64em64okrTt01GDGlr4kQkWlBOI4qZueEeb87NtkYHO2r
ObcAE/xj9I0jqSfl5C7s1BiL3uXmQ7IoSM24e2FbynCTE3/rHP7+RnJvgiupaTz8spDWUSxlaNrA
5JemE03HAFnHxGbuXSsnUbpD/Pml/I6XGza1/a4YhNTQO+Zpgt69A6YTtyqv2ymbY1cq+zVG34jc
AGGk3ddckqO041vWsuYOsswOohzfmW0lpk5r6ef7jTAgpGRhsT7w00iLS5QuHN122nSv/nz2FMbk
P85b89aW5XoB1edn1RQGLWaa/1ZaQ+TBMecwCmy546WAVsr3s9duDCBwCtHGeMQY9y2Tbj1W/NYf
7cnXBDwCTUNHNyPumUQHMhqPyFwQ+tT5IvdXRzF16hd32QjJiNFmkRrXNfgAxLw8t9esY9YfxYbY
d4poyrsz3T0o30u+LhcpQME8YcuiYowRb1L/qyPovMzAIglPImypZZ69haMQ7tiX8hj4iNwP++7e
ECE4tyMkt1oBwWiwAvMAtMmUovF1ARX853zX2okExFI1gDsJexTWblP4hJfWe4s/9/+cv4Oop0MB
gjpMlf4FyjjO1zyXVZUmwLWeA78+keSXN2XprgwvnXX6TbXDWe59LAcqwoCeKkkKRnHKTD3y0FGJ
VSh3uQcI541VYSIK2LWRJQNvVuNz4mbDFZADp6N3i4da2gM8/twxlUUf7Xrn/fb2LqiKr6ZExn2V
BoPoHpm8LvXgSdID8V5/BtDCYI3xVByEogq5xGdOOHH8RzaPbpa0NIsr8uj3+IYl6uE5AOX3plG7
UAUT3LzDnvZhM27Ek3G+cNYwsEEhUYpjl4a/tZsbi4TxjnoxIB5jG4W67hVIa+VHP8JJOg0JHbFC
b0HlMWTKYonjL/d9aktGiJtJX2+xOkQP79STQ1/MgvDylty3xz7rP/OWX0db142cyniDRgtburuT
qw6EnBANZcdyvbcGzCCfxmiBJKIPg9xt80Zh8XKWEvLoVXjE1dbycpSo48y2ukQshTMfmw/J/m+e
zDqMpg1dshwtQc/QjrlouujvEneM+1z30oA6jUABajzTwcGEqR25luY2jepCVA6F4/M5lHcCh+7O
LiGws5QrbUpJvbgyUjYWBTGg3byGe3fQJZY91t33ZalbwFk+RzZLMrbMpMRfNgsMUFQjbbk2KuNx
ZahNCmda7fa5dPcX0xr7knTk2NbKBMpJU+DWBkfY3SGZitTl+TGDp/YWgfzVQF6We014y5YykXbz
D/yCP7hnYg3KDsoXBQGO+f364kAHrLhwJgLX0ltOBoK4qMpwIel618+sYxa6NzwSdCCR9v20PXHe
7NohHjQyV0/gmJBizulDuewgRJUQIs617jICy86g9iQDoRT5/8rzTmtBbHIq31DL22WJlmnaR64I
QO1h77pSYvKT/iFFOaLlyg/d0yErm6kbcbO5S3SpEXblEBsVlWNMxkndycEEysKy8tGFV2F4/3fg
k+jFFyjBvfOV4GFmjhzWbAltgObNUtZGgrf+K/UXXTHZgdT2HxOtTqIFZDyPxiBAnRbxyF/rXwhG
jZVZ2C5ro/bjh5xATzsj1k/5M9exjS0ibOCwJqcj55uTBZL2bTMW2ftWAUzcwmo5MWadudXAv+LQ
lJDbH6H8dJG61NXjxRVSvUmmRSK06rtw7T5M5xKNjH+kRjPIjMezpSoOTgHgV/dU75z+hKPSN/Pz
RkwFT8GEyBr4nuqNhlXuH5JBoZInE0wr7pl+dcI/Whq3AT3Y8KoW968LWWiTeNOfKF8VF5cd5pc5
47VankaXIKRHWpNnr/ht6EGeHfyCKAb8+X6ZniAGauoPDf7GprCO25UDs9GulQtaEohMM1bPqLyJ
WWtDoLNhi+R84y97NUuTmEajVkRKPHGcCiolRpWIGGWka6gmojHuf5ZzWE41XGkGg8QL1AXO4lYt
ldBHublDISe5gyXNQ1c/zeXO9YOhN+Y5JJkSsSVI12K/EhiCJhJ5EsfMW/DvbtLHTFVACdApAMwn
dpxWH/f9Qdfqge96KzrEiuUFMtZ9cg9a+d9IueZjL+kWl4x6AQYGrJXXb5jikKLqGNQF7hk1tpcX
vhaaNU0AQh2AEPSunm1tDhTVyFK+i+orpPdCaMiNZnSC5D2OaUh4TLfRzzZzAgonRrdCKHwUR/iK
lqfRGtq3FRosy5qWH8fBBcZ99K40He53ZKBFgOXmnge48NuyoG5+OG+v/ajyOU+aTWpvq0lYOk8i
WLp/b/Xh97zMjoC7VY/0hKwH1LzoJ+iX+jGS8UDx99//6OOkcj9zdzg7YTUenDZHcXov/C4di+cL
/3y2ZEjLWVmNE2+YJ8yMWYQNOVAx5QUbB0Y7/7lgqIKTfDzdPuVIXUMxYetxwiI7LvoYBZEItQmR
EiNGG33YpE4D2YVRiAjxmfaL6BMM6RBnoP9mbEby5u/gOTKB44RbpkQ920inxU+TUyNkvZ0fYx69
WgI4d8pWeoMt/DPFbnWX6xPWBdl6OouAWmuaJgIHhJL/2EcRBHyaI9lM73sJYV0ohLFF9d93PeD7
W/YoNDCHx8X5+sYaS8kv6JadxW1qAplSh2bvk2+P78tWnyKh+e5g1RqzFIBrDCV+HzqpYBNjORPL
ztv82C2vXn3sxIVXP8nHsKEXEYykq/FBzGaDBaBVKUKJIapfeazyvnl9NVI1WpOT9IDDrUwNUCNt
8/43AVVsL0vk8M/YXag6b9MJNN8+YNWAOBkO0qKKbPmIkV9E6X6WmQcswxo9jkr5MgEqHyavXXmU
ikQOwO7qJh1KDx/aYX+t2TCViEtnrTNs8zyo9mbtP+84VadEH4ihLltHKHM4mxoLs2xTlOoFygx1
ZNJfrAeTVNh2gyJbjlnHQK3pmLVcb6rJi2h9BrGrpsPjr1yecdaWFkUZ9QoPje3RdZU5y/hrYBQs
AK+WEbMTvvOCYKxD1PwB5OPprsrjjH7GiIs56zjIClbmrX47CIzclPVGjNb0GwNDvD+E6p4d4Ug5
ebNI8SO4waHoFhlbCCrkTNBODWsAxhI6MtXCgsX1PCEoZDra2q4htqaK7wGqkKecFkCOzs8pbYEM
PGa2nvzq4xjszMWFyECH5C3YXdd26mZfzIeki5ffqW29TB6ziEBdBFaIYyrkI4rBEnDW6+X97VJt
rJbegGSMprxtzgYbZc6DjHeYUWGU/Cz9glEfjJYupQVmNZxEGTu0pGsADTgwFCJD+iMonE8esIGb
8W+7PuuBsuHt+5BkC7ynT/G/dMziwLAQzXETowwR04aHyPysnJ/7a8qewOGzb/1NdEDjCXevD7Pb
NVUHV0gLihBm3d1bNcGtiFwvWhCRWm+/QhJLwGKv+LJEwTQlZR2D10zYR+3gkbYWijSBLv8h1TE3
qfKoJON5pJTZ/AS6pbUokGi7HWpyB0+wXT/zlB3WttDf0TThRfj7qW9ULxjI0zbLIZlytWUVNlFr
toMvTr+BD/fc67nCCTduBTE41/PTKFfTg2PPjG2nOS5xDsddPWfGzwfrmaGomaUn/jvZ6vCIQOYw
2TEbXCKNLkPyGK5rMgkTgDGA6SzibaBHxL8G8gl1lXoMrrp7x/SJhAUCPnOD7J7rtt2Pi/T7H8ne
mchlwcryWOFxP+oFadzdkdLIa+Yxzaw0YkQZwctb57mcftcAuStw4/F3WbbQLjCCHtabm3N3L+5S
V34Xz+sK/sTp6BYIHxLkM94U2ieWP4GfE/BiWyMX8DYzRlJdfK+KOG6Gob6cZLkxXwH6W4R0qDJI
2I1W1ddQlf4x6l55aOBLvSqv8D+rXCj8GLW+PDFE4M2zipOUWHhhTyrqS9wq4RwU4zmy8VXQo1Qg
ZyVcCzF8RzhQPo7z32DRLGO1F9KblV2l6//fZjTQfpY0jHBQzk/6QhaXxgb+6YdFaFfhd4zx2fSX
n/+LGBevZTDdUNIdh3wmcXVtmxje7C1X/dJbsGUlDMHJnrtmRk77wynVKeiFK8PDzkQ/HuC0zb0d
wKcUE5aomNOl64ygtlG//MGDCpAwBRQl0qTJMaUxuNskF8X3GFRRbIOLm2QyMEJ+HRt0bY7pu2rp
vnD7ejjoJbQxLIDwXywuf7HximpfLBPRIkLQuRRpNkaSri6XA74V8zI8jXs/9cgOASaWKWAhvtWH
RreIOL5Bcwcsa6m5UlFfdzNug+3qGvxPryc4knTcnuOoQFEJog0uR6FjLuuQbyVqluDgYIihdeuZ
7A20VCl3CoqgEnO4pfqIkv38h5vJ1BLCs4IyQzBc6AAE4D12J8KZMWmtHdZiwUPHvnptasWREgiq
MiesmLm3OPAmD3QqNr/UFN+UuN84LoE6DzFq+1DmHcmyBOj5+APEnybfawmOmdZy1oCPU4Na91AA
tuHkB6fMhuK99aZ7qjMBq0yBF3zQaOQkgjPY0v+1KuICXtx61NDgvv/MOoTHooqoeONxr596vW8W
SBDNrruH7PTdQIO9IkYgMmSCFsiu4NgTEOogaclQP8ljfSUGGwZOCpUBJuwmAN8fczxCJdTPzAuS
bnkMlGeqYLhgnV7XFexYO63Ey/XNdyVJ6kBps3alNHC+u9++MZbKiyPCD5q6ZTyXSySRvGGf710P
xI49S9pQyCOMygoAtSwkKzxqWgiAuQgVAFb2040rYnpDFdAnli5dyl4fbip6QxgrZh7r0Lr7fJoW
fiiOO1KZ647/kIf3hu+YUIJlzW1qDkHAk0/oB3kpjPTwiyZZqa4CATX5XxflUlA755d3+DmRD/WY
IyZ3DNGeV921uXtWK4GVR9LvhOSGup4LKowfPoDb5820pWwDcrFwt4YGCiuAYTJ4iABRwTgAnBP0
kxpKysBmqu+wv7fp/mLFbmE/zP2DRAj/23CACY+EuSsk0jVK8VIX8xr9uzodl636Fi8EQD6pgBFz
breXEZtGsHOtx2dZmIlzSUoI6bqDiMuw/jThEnpXkvx4LPznk+CYMDZmUfVdYwYyuN20+PXIYSYr
l/zi8hh50+GhjS8uf1IytbPs9hCZG1h2bZWidU5OFmCaxPwLCowKDbwuPXc4LiIiqKp/LXVSpw1Q
pvyKCd7hvjwrgDDGdfcb3fVxGJzSN+dZdTtEtyiwJ2BaOMByGBb+Ofuzg45nAD17WibMccXUCgYK
k0dClJeoXOuh6C6ZkfmcuwAClsAypnIZ6TKeBwqhmzpCfoZs6VgHeNzJEiCeZIxC3GDoivoCbZ/Z
toW0uuw5QxviiZxLwFRMo9D65y7QUDWHEBlQ2E9WNnLWPwnFMIbfNxHuGXw5aIv3un71s20xK0iy
Uj3TJhUN77VgnRh02cnfCp5dSCdMSeevIR70nMMkM1nVwK/yEfN8jJlfALYbKUIZ1nrP7ZDFQxzn
dfLQ975MXV4VPLHMyymAagzjg5Rp8IAf9cXY6cNONyHDn3yRj/2aMGRAKZnR5Mjo19AlKZHClYKT
uONXrvaOXeVDLllneoMlQAV6l1eifwLaczKErfHVWnxWSlCdjjowAP0iXz6LdasVzG/0s9AgGZyN
2CjU1GERHHVS3dUG3Rzw3LszuiXpmeGVW+wYIQ0V8BtNsEsm0/dpabUy2XuxQcHIWBjNcwBMhHc8
LdLB9VyU/93DND6VNDuJJCsU6tTWDYXmLYZFlzT683aEAqelJh/N7s2vZpsPE2mM0bSz9h0ZkUAr
PIZECKH/66f3a+QXCDaemJOtaNlux9egnQ57OZNnLpVcn6j/F4OTntslzVydsvMH3tYpb32I4a2X
xJdvoSa0XUKZkhwjClHifNjRs0EpRJa31UaC7bDjX2cqGhBCExZohLq/UEsOGW8ufiyK6mJg53Ur
aMTu15S1NBqLs71L6CxIVPek9BWxT+WvWOxeV5IC7TwVgbppzViXKPlObM7tRCSTdJKzAkWVtk4F
O4DVkw+w0D3K8POv4XnV/YDkhJAvtbt/zWRBMTfrDjLW1pUos2GDJqlCOMtyO/aY4uKHbyj5wq/u
h9HQ8W1pbhbFo6T2vPhkoNaN6sOszEVQxLKkqSP/qV91sq/a0VMWzC2XVlCYX0dpCIVhj5vjaHSD
QZPoJZtUZepcwCN0o8BjKi/N4YZS4wn9vBD19VdVVvxNadN08nETEOtxiUsDKqbMZPW+VDdiMnh5
suoStxHoyBIPiFUg4qR8D0jNDBYt56B3vQxXR30FPgS61gcRMDConLTvQ/itmd9qGvXLk6S7cZqx
8QEk99nGwhcHl7OfL/C9BL/Hd68hIjORptsDdWJuVDfKqjSp0nBHIQhcRAm/FhccunikW8nvYHWx
5gdgLQ7BGcVMGDw/7ILQ1PLbDPto23SXOG87NqxfqQKtBlVKzoUD5l+0iP96mTufPb7/hoRvFKwP
dctAkngwByYNLmLORYans2pqMLOEDphBqu0Kvc+J3j1w1uUy5hByhPwHTmdouJebdCkfLD7aPpo4
u0iokS9xXN3YeYr9pJjNe4/HEzFQS4HMQ7tRIY48TRQcCbPyZU4d6i/3ggVen994AiAF+ZqacaXE
cFdFTnKpGbaER+Fn0z7KkkjfXSjlFGjEaelaXpQVYegz1hqdGdW9Sal/rDNcgxgNlIen8/fdVVjc
h1XQ5CRc/bkDx/kWSv4hs/ZW68fTqo7j8SiT+QtZh8t8EOex2kYQaFG/F6K3Gau1/G+EWRvkWMU3
p/xomplDZDoFNqWiEuy+ErjGmbKksxz0S/Clr7xcO4GfTZcj99DMFcB0laI73RHPPXuierJBTwVP
FmG3OqLTFYqBTwVpX/+ZZM9VAk+hFnmlEoWGH8vnToP4V1c8arynkhObpEbNdVElRRbWNprfhBnN
tnh2q8MjQNz/BEhsjopKW1N9vQXRRd/noK5CqCS8yujBk2rh8jX5yvnjAZGmWSZ3FZ3TfdCZcEaP
XnhUgH/8hUHyeei3Am4muOw1EAttk0drp4fLGScBqdu/fWMuOSKTjidWSRilkLNGYsRjJhu/TbmW
rHYhFdS1XPH46ykKLqkGizvDRFSQ6mtVw4hOjrrp7C9bqVJ5WJxoSZUru0a4vOQx8fw2l91rreyH
gGQU7suRTG8I4jdKlQzQ25D5VwId/y1rwDR9Uwc0Rq5xO8d1nSVk5iL2El+8k9UTgA3TJSy7xYnY
3M2qc62Ah4SRvc17bo5V2pCDV+w9htblLO73/xUdVFKNjRsV7kHq+G8TXl1yTCsDwpx0muJmm7Vk
3DI63k9hhcx0hZHxzCZJC46prraRD4r2bU2nAgUNU6HCyI5+BWn7YfUcYe6hrM1l/IPD3TdYkF5V
/lJRD5mJN4dG8GYeBraWRTKUicIz3qMJfuVIikNrxpUjULFUtwjkI/EPjKrcvOeKJVs30UPjB2nh
IE7c+TQqZzXb6YSUI/Rz01opeEmqhgu1Jq1BXLXqDEfH2RJwBMWWk5UW2ZvfqrtN82CWpq7XCGgq
V5rbe/mCdiYp3R2Ry9HwbTwuOzTSTBMmWeExegJC0iPYIbQ9OXqFps5Ww31UdUrTVXK9QzxotgNX
MxYqiM94c9lFbtMEXraAuY1jaAgXkyjSjjcs0+YClB1HfLwhVoL5iVPNAeaRdB4voTPRTCeufc3h
jOytr06NbBVfXBPv1EgaHDsj41P5iNMT+FydY5lwFFYTFd4bbUhSXdlMkVOvPhJK4HaqLM69Cgmp
7wBfASq8GS0WFpOKQxmQ/HPUV0utZOAL0tjqo3m3f7e4I2gWSIpf6jrJJajhu71vwqjLiSv0bhF2
uBjp0O3LsO70NENQuHbLv62VRl806SdVkLCjcu0dTSswHBJnbOecYHrVXqMWkZ3GoR5OX+rE34NG
SkMcXkIJo8sdPKo3fXj3WaDjM+pgwjuruKWElCiymnm5ist3wpM6VulWrBmmvc18l8tpYL07ODte
8RMQ82LErWMWup0P279bUacNy5SmKpH/1GGTYUPLnNMb1vW8WmOtrGb/8ifQeWAKjSckrnCYGKfi
4Jgp9+pY3vy8w9ptkyoSog0l5aR9rsZhjfK1M0PqtVLjUq82SQtknIKJWGJCrlKxG8WtiyhNw4tu
vqZX7I2Bj/LWDkiyViAcc1qOAmOiuv9xDJlzG1FJPEzDz86Qp2MFQVXP+Ko8qKfIRytiVC8KMtid
bEfkz6sXLCHghoEv1xWLl48LWWzCH4+zlkIjT8v8eV9D6jIe1LD0sAHK7PEq0To/PxRY97mGwVAz
rvr/3YHPJObp1CwLWkCHXPFNwOHVVHwroLtk03o4Bxq8vMVwTVPx66XAyZMDETB0o/gGdn8eYcyT
w8imgq0CF1tD4mEfb1I+bUdPHaIslQCNJFgaCK8Y8z7L/tAjEawVZFhpVRFKV6B3M3TSf4weVPOx
wZ/tE5FzDCVVd0p/mGaKamj5oRGKq/x1oyK6N0+XoZhWoyjDjdtvND6AnhfR9qvu/qeQKA4a5eQL
ld/5iHqVrGis9DNTC9EgiFOx4HBhGOpKhW1A6Eg2PMAMzMnfLzNYzpqj3tQzB208Nm5yIX9k8Aea
T/1Hu2D8xQ+BMgkH8WfWaqayhPiJz+b6iPg/zrcw4dgS7F8BoaH77yFjR4grD+EJsPzXP50CL00t
Q264BwhzkEqO14rsClT0bzJuGDnsbwPYHYvsgulY3QcjORmrFKQkvPcwwtwR2Q4M3E+FH1SNhzU1
dM24szdxFxTmUXdaYDFIQHiXa4q2qqZ5V3k9ShsNIRVm2g0zrY0Vl/hS1aK11aiQ3kibb2aKuLhs
uh938oc/0hhFtmqLJvmMILjN3kSkOvCZcY+ccUJEY20V1WVEspqzIE3IajFXfXrXrQ58Oh0xA1V2
dKcpSmF2r+bl7WsdVnKq6GykyO6DfC33+uQuDN8mptbGwtidDJQlELXPcqVEYIJrKZpL9JVF69r/
3luAB8hgi1fy/p7FnCuq7X9WsuWidm2Dc1AoaXxkQ73t1DADP4bhDKV4yCB+I2Aup/F3YPp6vBhs
pFTzGuWskOjsoJ0t3JsBL8JUAGVMwi0yuTw+W65Fl/buq9HQj26lS0erXFaffNHFnJeJSdmKfPNa
r4TsUUyWhJvQVAD7n1F3j4ou8uY6vnoozcZiGYARJ/OAkRdGJcNRXbmCH8ug4k3RSZHh8Gk6sY0l
5eBdK0RyJ5A2b8h3lBXcjPLk2pvixuUOAQIyDAa0Z05TAeJV2x1UAPLimZ3yhXhvFzzK8ZY/frOq
H5SEd+9bsveX882rNTeHB/FumqXoOAi0Ji3WE1a+3nA7uvAB0HHsV2Mz5gCtbQb4N2lZ4KhFthDM
2xC8oOGKRjuPDxlitwEXKaQRS6w2H2/XcbLeE0jpKf7Wrn5LWMhET42OY8pxtg+CqiUhZG+T7V7+
Tdt+TjUYdbRkQXFsMiYuFK9oOfovMJmarf515SE4//ja78i469MB5NGRYp+loNXeWfNTKDzouTnd
E0YHWWwCjdQMu8yET4Y3ZmAgDW4YplOVgEQ65bY4psc2rlbm02BAL5zOVJAHcYugNwEpD2eRLHIM
kkK+rrUiKfJjWVQGhvNtqbbYNLUnwcJn4XeWOJKbQ/kToznWK7e90TPGt8jvgB3NOMF/OwmdL/KA
FZdvvqphlKkefszk4FfSsxG450Jtx9YmmZLaUfLjliTUQ9x0M28gUCai7HhZsBI1JnF6XHmqvJz6
sEX9USJv/KNE1bYZwcnSC5AIf3UVtdIz9q8DVjlGBDFOzX/5yN5GxJockc6V54ZimqjBI1LDieJ1
UmalXKbQ0NIOLoTxvT7kvt9DslNw2CDooWbZHFQk7NTg7mE8cipVjELJGizfhXJTxJE08nKcGYHM
J22qZuI54QiKib6tMfS1AO3B3fSMcPmTdPDM6Nhd1pKEC4ZpkvyYKruUhkLO2NQ1BCdyvtOFU3Ww
tArnF41z5zuuW0KQu/PzQhYDVW9KzvlgCgQ028ZyF+i6xHVt1cVzvFIDGo11y5GqIolb81+kch9t
z0FUTfMSr0JVWME+ppbpaT4I+tEONxjOcyK8xKtvgpgrBpbR2oB/Qmpc2cs5DKQeI8hatS72U/iC
+ey7nnQmseR8RB8nIayHFK6y8yQhTQbMd9AFz+ibw0FNXz8p/tg0lS0n5WUQrwdov7tfzQqjsVvt
6tXNowm5Qi3GcGo/ewzu8Wrj+tyoCfwG/ay/0sxfv8eX3ovdaDHjbcpx3zx8cKsa57FMF+osARnF
SoPEH8qkA6C2XJEmfuizsLnxK+2dCcV1/hJ60h2V30MxVKRV2Mx9HmDfCAx0AjbHm294KluosMLz
cNjp1sYX+e0fDSMEcVK9MzffnnSyCpsIZpxaZkq1w2wOFw1Mj93c4axwF3s9/gzScJZZ3KjeY6BR
xmwbhLTV7D7vQ33AHszqv4iWh43tn3gDVIDz7CAhZHJw6mseSxppVT3LyZYsYp/elPrNB2kMX18g
sEjGLp8sYXiRyQlL+PYo6Fum/+/+tgYcPZQaEdKV6MkkQ4ZE1Z5L2eYIm7L01gHKhmM4fRJRZriA
OJHFbvGcRPEbrYoVSfJmdWmZdxI5T33IPpyhxY0DZ+nfhtCbx3IBgqSB1nGJ0+wKZ9fqGhJmX0XL
K/wCGeqQO2thZFpYxUOTnA8EfDEok+bq6Uoj0nldG0PEBqQiw2/A1PY3qSb/lIqFuAozJx/XeTvx
tKLdiuR5Jg2Yy9ilFKap/GpvUED53bIAedJlmh/OlI5NflO0KMS90MQnI9Jki8v6yRswgZ1nLYK9
LvAux6BEnnQCejEU8iWmCMzKS1S2aQ+m7fvcHoc6GmFMQaw50nnAXXYps9FIGiTiIqL6NMosspf4
bkd6SOSf+sa65yrydUa0oBNHnP2uYO69zx9UC/Uqsms/yA6knY3s9vixrv7EbzhBvW0F29SoOqw8
6hEc0XhupkBpxhC7zRuJBkN+kw3cdNi1ehgSjLVChXODZJZ/etTMlzUNJGPPZCv+PxUUtOHOOZi4
8BEYV2xQYOHBKnLhjcTP/n8ur8Cwat2BpCS6pVW0zINg/dlAjHiMddqNX/WYs2ObDNiCIc+xg/Ny
ZpeuAUVAVSmQgVjZ864X2rbT7WtdgSXqd2Ij5uZgPqP39Z7pXI8MpIpY+4xaU4lcvIXelwT+8Pj6
MBggJuXH5tBF5LcUtBcHDTwh0LiiGTRCY7glVOxSBrtvhKMdraasABoBH8gjhTUX9f4XXz9vEKOb
jRIWcGbDmWx6GC7kbfeJxHgEYObDBjn1XbxQIiasz+TJLeB/1+fjlhGpX2Kj6QW00qJw3LJzS2qB
Tfb10w8tRpWH7WUf51FKFO5RZaoYk4xVFQcZDo/UF2ADkaqzsj2D9p2dsuyR6L43xUUKcgIeBOV7
i6DCLje/9AbXRGheZ4k+Csz4th9OW1ODvF1M/53pYuRU9wdjvYPKFpHglivbqqxBZI7GoJGRmau0
AUj+o988tZHZ5j+6A/5CLn3fXsA2C3jWf8T4nDhaQ70YSf7LlSKw3L7AhRo904kxGUT/yh0ryXKw
mC1CsRTIPayICmXfZAjiFDMl7Jpu0h/xGR5rmrgGbKs1uNfU2DZ5x1P8iLYl4Gsm1drIGEF2AddR
mF229fHMquU99roPsS1BMZe/BRdJxnfNGfhDPOdvXFzoEz0KDprJHFxA3hWw75acVDChNBXtXBAR
Y6VXp6w3bbgFayWEpKEtnUmWii8nyWqx5QbuMD9J2saSZGf8fFE5vW2qx087Q/xh/O8v6lahI/9f
hLFG9EAA7slxiqfTX+wzLs8jKmBVQTob1qEoUnTI7lmNibv9WK18rh14NVfCuYRT6Qaj7HFegYqT
f3UGjJSMXAbwR4V45Qs4KQCID6HjWgpZf0QlvjR9L7qj9mhevmlMXpKaeiwPe9psSx4ZUie/z1d5
2uaD7gdzlKChlQ5eT3N6V+n12xdjoyOxKx7fZpzTXfoN166aHaO+nYEbxLP2uSQCIk+zNULpW1uU
gJNCPywlYn22TxyzFltGhLk9xgDcxrsclB8Vx5iIebrqzrmURsf80q2xAsPy5L3X87Vt2DgJaFtd
gaQCgsmN26gTMO6vuF1GbaJMrDNzwgUEsmMP3cdfNG+LQ+LqeaZENbmlA7hQb9bUZ2v/rown3QC5
q96TfBaun53JZx2lr6G5tlfuQB5agJBsw5AmCC96UD0MbsH7KNS1cnMzF+o+Pugz+156q7dEyazb
LytV6vtdzZgpRw6bq7u/XmZiZfmWxPEWDYOXQ2JzsZd9yNn75rKQl1WIgzYgYv2xMD/jKWGo/DNm
2TcjNCuQeClIe079srOBLh8ff6gE4mP7PYUvLVclVYiVnu1zjo+N8svPZqFt3Ex+AmGuZkqnk7pv
1L/fcO3uPtwAXMrFCReZj6uw7D3GxuSb4W2UEL8bYx6gilDSJbT5u6UA1yARSQG6rjHzEfq848pg
DMsA20XJmbde/DrDExee9nokkgYbY2ktK/0X5/x0AVSiXvJwt4ZHGOFab+CTpDnlnjAR+IRkSC3x
+Zi6Fxr1v3axY0IvST8vdkGTpJUc7uT9xMPcsqlrdT2qQAIk/4qgkts5vTRUeakZLva4GdaXDFel
bFk7CeE3QavUKYrwxvjRkvV0Jmp6Ku5V/DjVi9X7H+CGozrtzlQU/BY1bm32D6i3f42VotaSdpmB
Ihe/DCEzNoRWEtghL/S9UOqjyfhV2+MO7iTLrSfIoZdCi8AWgHvWfuyLQBALs07aGeJyQminq7ki
rlEkO+n97ACal6vbdxjZ06QLTDR3k9i2/TYNLOVKbmVnZ7P4YFwqytFQ3MvtE1S2kXvn01wzaVQi
T/nP4Crdzmk3cwTzzEbFLYyQ5NYCLtTlpviGdkrq44ipIrlO2SNFClkzBXBvfxlcC8DaFKpO+Kqv
M9U7W5tJqyRDGG3ZpY6wCySCaNR2mj/+PBmeulTiODk+9dnVi7KOujbF7WR9yRAWU014R4K2io8S
ww5PcPXWJ//3ABDH4aLSouZB3nGqI7Z9fqp89YEepSgB9+2GDePaXwnaXujXGeUfM5bl8GKqpHlk
XAmXSSW+xmr9DGC/8bSkQLC28FL5yiT6PRX2Y2Jey0S7gFvWNZmhwyUOkrhZ2/7dhX32v9tDnY7a
t/rbDVq1Po0uDXDccWDdqPsOAFX2ZmvOqj3WQv0Z2mwp5RKOibHq6fxbpBxxdlz8JElsI1pFegTq
OwG4sq4e/sWwHlkYVoT0hrXndc0Qe6v1aVv75J1rlj8uJwRMAeqbSjr3Cleps4qv0wZcpbEgevNL
9rfF5a9oBZ6dKezZWcXlS4s36R7ydiaVwjm3UGmCNyUKWhgkej/ydFbhwFZemnjkuU7/en/LACbN
7SADxu3ZIT41JSkqXpOKNtfyDLHKk7wqTG0YJjtQcjQ5adJUdL3E6vgmDTYLY9AauXD0EVK/9zdr
W7TpFjZYIFoAZm0Ad+Q2OkAVS1Ycka8z5+bgD5hyt8dlY/Ii0lYNB9syX3qqiTmHyRxX5OviKDjC
P4pgw/DwiZ8WfmzNdYp+BPxqASF02fgluxp5eWUVfdX0lLsviAAfY9Vpz7B06B5IpMjOTq75J/MJ
siWhUWcQxwcrHHD7xpEItF+K23NUeMnp6fSMm6rC+ky7tbaPhB5JdhGCpwhtxJlga2cIoOH0ioIt
O1OA6w1x831sB4RPffa2UWqoqpQpUpcQ9qe1zaiUpdpyiZHfMK/oLmLZM3tmKTE1ad8iCTfKOItw
IzXcHJk2JIuiUjh7uE0q3GoHJvsAt64LjmMEa/Gc7Lz5Eq834+oY73Se/EB17Cc3lZeQhkuSVcp5
V5y4iHH9gAcq36Q+8gYoW/n8VxdwNKphNDgoiXqWMzJAmj3NVgZkyh7d1RGS3Q4KsC7SwWcU5tL1
ZdwK2LR6c9b35jcDpaCG1gOHrblFgNHqmyEl6uK+HteLjFDUKk0N9OCxapp4gKcO3FdnCJq6z1Hr
+T9m0ev7gxTbK0CsI9MYA7saRMjkKIFH0d5ZEtuOW4Au/x8uREAIWb39EMhpEBZhAgK58EK9odlk
HZhQf30+H/Al4Me+pwhEd1YImU0iWt+BrsfffOu4F/HUGrVmpqLhbJmk7bjtsKOCWac9OXLpxGwK
owzYRMV2S00Na69vu7r3x6P4QJRgmoj4EIK0NSGqtnNlTEno9rwcR67kQaUnhtx6Jq6aOIDNhrD9
IAfFLw+C+OMzTDTWXw0gshWkT9xbZoOUq2WbQZHtV/kJDP5eiZS0rJmRX9a0frTRLIUvrxOLHDlV
Z0ku0lMCoWhmCapIkWdIszFowdbp/vGe3+zam1lY59wwRO5uEnpaSdodArchhEGAq09doienePdG
UXOIOdwE2KxyywnCcqsnASg4aR+UOvjyQnPeurx+onRfphlO6iEDPzOW9MB5mi7V6DEVvIUQIML2
uNwJn3oQ4gHnUo92tJgHp0w939ye+lj+75dkMoHmb/YspZ8abIErB9gUmAYTTFsbrEYEKkccezsB
PagMWxWhkS3GZlv3WY+8CXsnowjeFQ2G1RW4EbvjSewIlMvGvFnb5CBrwUzCgWUWL4XnxaWcy9e/
ElKz4c2eIoD/eU2Xn+qARMqpp4r8+DYFqhTSwq7YkyqhulTfcRL8fZglc8yioFcych1f4V8DTdXi
Kn+GxJCNhll02ux56hKBGeYMhhGLvYI0f1lzvSXCTDBiELIUnynrZM75zhuXe7+QYq4FYx0t7YfB
+XkXqFP94b2rsRGV9YpP637hk/FYTpep69TRlVU+wTKDE0RkhzJ1NRDxi0+cgU+UNufymWZcnJvD
yQR7cL+8SG7hxPL6qpMBN2JUNUrVj8G0Dlfd1hWF9vlRyrdVG1uVN0mvF78juaV7RFVqJGxM3rBP
XrRYA8lbkm3LaeQ7hH86UFyYTLxwu5SXzMxONegbYElpRnvIcdBGuWJJ1bBoMbqn3B/MOOb0IlNM
gHIzDyGqX0noyPwdP36YtCZdfCQuuT4VtSEh9XTdeeiCQOP98wdGLjR24Rx2kBngvvw9FAnua4FX
s18pgKLDM+xPzjcdtYp18052ZrWYwl9wD5+DTS/bT8jBXi6hXT+IbBfmNXnR8eALUT3wPJ/sJ7FJ
r30zpM//XYkCwHgYhPdrXIRgAZ6PpAsonfGRTG9wn2VIGU2YffbHk7usxo+9jNw2CiVX04ekihTS
/8syiP6iceDeZPrDWP6VljFTAKKP+dkfBavOm8PqaLsY6HVrIpXE+0XG/EO05jVUWeZyAPa7eo3N
6OHsQQkyhOB8LXrxNnznvnljn50bW3IL45ZAXVKOZbpU1zi0I5yu1DirLJ9nv+Sjr/48aCKpak7s
LrlUvzacRlVEfdh1BMYIvPrKnoiLUjdHpQV2e4ydZ22XDyYhnuvXiFPpXccWCVf7MZKikiePqxB1
VNaQeIs5UmO83ye2j2Xff3zl66D7uAqnjRA5ar8tfIFrVnk+S6wVJGVwRlMlTOGDNVoURPoQBsi2
fRoIGX+FRq9w6vTn+A8RD8WjAA4DAqZJdTtoyDDOIj4eTTuCaowb/tlbw53XZvlAkSZ9/OODZTWk
lIBkPFc+tbb9vOLLN0sEOxTiW2Q4k8/jSwHshUtdCLS6YWwoLnIYXRDvLoWkvXqRHh+XaOfw8b6F
1LbPFu+D/8T8fasToY1ZTgOvRWtKfpT4vE9UM+5mkz3sqFoZMXUMYjn74bkkmZqoEsqHBzabGp5I
a0/WCjzO31TuCKVUVv6zJ1KUZM1BKEYOtt7xSpasVju5HPZtpwhmy4j0RfDsVyEzPTyBfMzj0tyW
5g70xVmjlD2FYCjyxlAH90jzBGzEfLQmIDtz9RTeygEiuxvPZDGd3xxcV2isA9xoKgrCYLZ/tA7l
fPP4AoGAHrcHclkjkCAGGaKQdDV6yUAERtzCHd1zu0xM3eRwhebF8D67NIWB2ysiyllzV6F9LG+E
5RGmChefxQr4VFfiUGhYE4Hljqf6S6EacBxMdYwIB436OGyDTNYCYekH6k2Yyi0VTulAvKaTVQQU
LY2scR4tovGqOwSuxy8V8LL7KwBNarwg8ZeXs6kvf6s4gRa3/5SSBUyRfILdeTkG2peXqoKybBWq
T51yUvKpAHjzdUaDc0F97T4iJPlQJUDGfKoUdJj5RJS9Tw4b0Rf1//Qh8NGVbCrNrCfoqFmxjZS1
Ut9Tym0lY/tdlkXwfn33AXPy2/NnfIBMO4lWoe22qw704L9yfzG8RE/9gflY2INWs+IeO4nOanKx
iO2M7GkEeiNUPP+C2026uZQq009Dk826+qa3FxfL1xjXctj9AtppmyYejNYteXBBGu9PKUfUYJF7
AbgGINBqoF/q37VkjqVVuuHKDEyH6oKrUIUidiHAhZ77lzM8E7v0BCY0VTs5Rhx8kWt3Qt7OikCK
WKi37DUybprG1BTtsbw8jm5wpSncCOzfzGkRBlOdHwfrFQ+c2Y8Cb+Hb6u1kRW6siErCKCAeB/Mg
EPzq52uIPNjye7dDroeAjyMXtpn6micOp8yUHmt8cDUZmv8tD4UokgfRRS62ChyKlwZf+rSgAzxN
IQGmhq8e2UhW05lbWZSPK88rS15KlXtPEY0jGP1T6ieT1EMRa7Ruyn3jl3qRPRnO7lD7jNB5NX09
mWmFH1SdUMwn5jmEErWTUorJHw3QODBXu4DfnLvXHVEvm/QIt0o6/iVc+k4DvEW2i2digffaESPp
aOAFpJuP+8LlmasYJG2EuAgxMi/RQmsRTsHNF+yyMElOHOtB9Ak1QEyQUCtIAoFqtU6gjBsGteUS
otj/CaR9xpM5JvTNtcw4WMpC3psl5kdgOnEbE72iNuS3YaVhAB+304NU+I/NW5q59ErSWDaxNvLM
FIcW5jp/HnlMXReY0zk04jARkbS2OLp8il2GPZEsk0VHizwd2q8tQntbLohqA0DXfdv0O1YqArrT
wnDbfnsj+T664qkLf6eVvkH64muNHgvb3OBAtLsjQACPKEYtzjZ/BuHCnRq873XZ/N7QDQWsrH+p
3/WGVgG9r9ratBk3nYmAaBLbURlEOCUIugW5d20ESldWTULQntOl6hPmEj3cem9gSdUpPHPsM5E5
yL30yl0eE4lCais7Nf78mfYyICh3KV1ilkhDV6y/RJHWxPGbxT/quYWh7wXp3698HPPGbmA7lqIX
kvDZNmLCM/qr+JXpqUzzV83Qlxt4XomEgLh96GcfqZnxzG3itBWu1TMYqpx2WELAhCdocWazK7uV
+NIQLUZLPjiiELcqWaYEdEaLXPxnilzwquIZlaRIks1t8sK/gxgpHF8IS8+XPVRRVMGUE8ehytXw
S0bD0IGT2hzF3J00kNkcdfEqZK08ELLBXhTlS3qgP4OZ6PO3qWHZLdgv6EqDG0dqkfCFNuUeeG+Y
bqTxhyjJDPTnycLKoH3lAab6mBfdEGEKp1kMLHnP2gg1PvNfZ/KbtMbBlCIq4xL/ldm/72CbpFfs
tlcQqPCKei3CUJ95wr9vrkeCkFtPsuPzupOxXptcRk9o8Bq1/4b+ZnhKVb5qNlrsqhLCyRWWbT+d
dLCD22CVycIKqpfP6sOmu/HELASe65C+IfNA0/KAFvnIi4NUtW9PAdvcwXDADwY5HFuKIgtdntTB
Dp8q8AlEic9D0f7XKX5LJ1ryCEfYX8tk52Hro7Hzn4/Ihexmqyj5haqUiIqG+jmwvFMcdybvE4g3
cP+DjDJlyprf+KXusEcaAQPsWZYNLwvN4xe6mguo844HZZvMiGc4Q4mkcEWxUENSPpsQoVQY3iRo
azv17/WeHUsP7FmEZSQObteIZ2gTdneTSgGIvVH6j214I4W1jHWn3gEyOY6xEXC1TOq+jaUtREx9
CFsbPPUY7IGvCY7hO5Ll6uNX2SZ30g2vxGi0OkI3VFYAyH11YC/CRl6kM7cxTPaVFOYGE98tWmDV
+E9EJ7YvZHfaor2FN3Y5pEfdFPHjZi1JnGUatsUyaEnttlUiylSuAH73eZr80DNGLfzAnmB7+bEL
bKhYkCtC8ubLfNqBExXj2WL/42PBHQ5SCpAhZGeHShnohyE1XqihLs+3Pu9R5F98MgO+pl3vQFrV
zX7RHhErTVPO0hXudJtMeEpXSXqaz5MM+cuNeBKhzO3AzFWp/Px9XGQvsUyp/NNEbzJbGksVMqGL
Fg4FOpA10+vIAust1HxdYXwzR48N7egyrFvOlupIrPItezJI5r3T9Slly33ItozVS1sENpPi4kG+
/V+4HFKtHHIc8Kn2ireBe3vPb+x44p+7AbRFEOMKISMn6TOyu2iiJfubRHXw5ah2iHQBI9Fn4bJl
ysf38lZGZqsrx29oD9hqugP7+ixHdyFKAuCdJp/aPZCxGVOOXF1oPe6QisaNinYKnYyenOcFdhRS
HeokKmUwu3T2HyCRSA8AAd+Def2kkbGQnl9GMaD8j5M2OZfUsOHX8opswGBH9AZ9fc2DEsSGgXXm
PYRh8VjAYpBNBbomz1ZZU2d1SiJASQFHccE+8YEL5i62fmjGSBnN/9L5uGXHqLj0vkPlkMbKfBfw
ireOF4VEvR98qZCj5fSqiADOZsR02x3ao42qiKqg1Z94dUofAFT0Hcp93vL8tMgcf7010MgVF/TQ
/pezDlOvB1rPPmyQbKEPn2dYrcKGz1cqwQzlKCBntDreZChn1UL/p4yAyLPtq0AoFjLnMfYVchDH
ONL2PvjABohWT1x669LY+g68O3qq13iOTFjKNXN0oD6UeMZg028B/fxj1uqWMJwiBnw48cN90T6J
H97dVwxtfzMVaz7aKXaRBPADrXnPy7eHqwRgKW1c+gm3/J43Ek//kbV3TEKr7WJLHBT7TlpfxvJR
/ongH8Mob8KCflhojA9AKTDI//dXzeOmouAvvmSk2ROe0XktNz607Mnj+DG/FIwUJEc+nnVZwrtv
fe3XWGYwIuXeYnT6VVWzhgva8xAQ03UQk6NlQpCyMm/dW4o6irUz/HeZ6NUmObd0ZxBfRpmuF8Rv
t4GHDVhQAwbpVodI+aU0rKMVGQhEUJ+owKlg+vDwI1hX4cvFMctmAvyVLo2/TuGAHuU89+ccxGNI
a4UV6RmwJSz8T4xmfNEy5XuDdwDvVgtg1jVLQFVwGoZEKlpvdOS8Bexfj7k3z2/vn7UjcHzGESml
5wSEdk9zXrnwxzkQS8e993+c1ux+Yla5S9YZw88wIG7VLgA0JBldEf5Nzsh9ywqeThOingRZ51l+
tPjTehW8BpY2DmI1d2ttjlA2Puh87ul2+Eq8YYC+ML86l49p4M/QhSks/+wunuHgtvgS9C5xPRys
VIK4uQgiDZVVAZ8ojVLdTzZyeQZ0kEp8PpGIQ5N0NcHl4oMaFqE6ltO66RV2Zr3oENQmEa8qV6e1
n8QUKzj5YY4Ddnicfo3GEJe7vSankS+YFtIe0z7R66IQV1jmvt1kAqZgeVXIvC8TxakbycGCMMye
sF/08LkHad8/TEu0I7O06t2pKL6MJO4qfcbFNNmXgjJ/o+H7hdA9h9X5sT0mJorLVQWtdIqCwCpk
EhVVCyFnna/RuZSAhQP/0bdgivY74QlgHODGySyqOKU3odu6eT3gVOfLYg3plgIS831VCEMrO8dq
3EOoIGuawMV7OSozZ6RnVVh5nDGhHoH035qqddyIXjPdhPCeT6SIGIKCfHLuIXIu1vk6hI8vR6F3
bOuqB8+VB5TjNlJSmDa9qzOGYA+8/5LWECkN6i6iEXYRWmIMpOF0YjdSsw9u6VrjrTKG6rG4bpC0
w+mgPmOMHvMaXX9QlEHWFqo4YIS5Lrs+zYswxRN5/D+jjwhQrqaX2gG6wm4fNxL7w/LK9KwXAblN
LWIcbLVebgzdA2XU1mOwtQybJXW1/Ijjv/F1cmI+h3CQ5gKaEO+lcRJl2fJaypNXxpRoU06tzHry
kjjTwsDO34DAFVlDMbwzmdQKsNzzHwL88xuwWEI5CT6siTaZIUba1NObg4MFqoRNQ368nU9DENDk
rH+vyb2l7FGgxxfpEWKb+noZKYgQQV0fmw7DqRXgWull9xVSiBlArq67heoZ7vnQxTu/AzHFu6+m
dxNHUZn7+i0SO57u0D3xaRTAi7bfWhlb6CA1QNYIBlJdAoNWlXI/9RyO4SoWrE/sRlZelZ8MNSpn
XgW8Ejw4FDu8AgkGuSXuMfOaC5JK5YglHOGmBEHp+5+q7ZKCUzAFAuZgE2BFZgWs6U2ryRigqtfo
OSWsyUX6HW6INoPS87gkr1FElmARds7/jw9BLsZdY8mLfbVFRXfeKapfUCoQ1jWRPVySMOcrqkSP
628q9hAZiWRQGTQ4YwIN8aAMGdiQ7IRrnw/vXjPNlXHSEIV1lJJE25GXw9g9YisOzsu2QFtjx8Mj
0qVgZLnsQbak2/kxEAgHaedht6xTbHlYq+BScctUU8Yr6Hqn/7jdtw246DA9gy21e6/L4pC19knU
2ksnHTZ8xPzlzM99Hbwth4t4RXaMoqnzXva8nLieE9q/gU81r3mGc4OZLjzdah+ZPQQ1WZXCKRBE
i1C71dODGshDT1BiHuzU9aKzTDaLOElpFbLUwM1AX+KI9lDb7dUgpQnz3+7GUauk1fTldpYZGXAR
THd+knTCC7XYAYNO5+YOHAhTymuYtt3rhAQGGCYpy/4Otr8/5S1nfYJdVixveDToUJ4h/v5G/xlE
QfsYpMP5+WMf19Tn9FUBhQN6t+IFS3hu/9SfA6oHDwtEh25pXsx53KGx/fJrmQfZ2dDPVQWLaXcd
L6VStlG2QEyPrISbVti96RpQ180auQ+0r44p5n8bya/MOrbVfL71V1lrfA6S4Xqkq9YNb8nLRNi6
acunE4UUaTIIn1ILgm32iHdADVZTIpkC0314BwlE3fcnIBc+AJXENFg/uc0XTwj/9jFqtG5kFWaN
2mhVDvb+k2bLe69TMqVbOHqD3nEixq5lfN1c0FIij/FuT++X64j8Q7/9d6EfX6pbMiyiYvraAFu2
e80JXDwByeQWkB2NMpTXjMGE1jJLLQh+MD1GqlBmCXXKnAgjU21UX2g52728kxIwZf2qJkCqebml
Y8doPZw/jbzVn2g8NO6BI/6NchxVMnG207UoA9JJbs/bpnTZOMBVbg67IW0VFz3aD7iWaLS6RACm
vgJETP9IPfxThWWUjlpm72ziCy15aBoVwFVk6D4/iWtVlJg/zWoRw1fgTOfdna8GINr7xtuyd0Bq
hWrNQdSxXuaZTfzLCna4lH0tVo7oHxygKfI73dd69Gk+7UDRVbPAueLo74Fmc35JvSdKSOJSarlw
H0+DLUTheA9eu4ri3Fisr5U5qWWv7ucUQs8PF7i4ye3WdUKXuAlARiAJcVVP98FZxbWEf1VjWNrn
JPKY2f+RAfA1g1/TrJROx8hY57rTIGCNETBc8o3cMR27FEDvv2WXh4MwbcaU5LAJ1ns0+CG0VcLA
tB+knbaRf61SvbSj+97ZCfaw/AplU5n5WMT8MGd9pfwRJjoEiXOAgKxDI4qymFMqCdC47IShS2Te
+8/JtpyFUfd/iQ77ZvtouRlrpYePa/JtQrpyR4p0Z1GXvwt1vZfSBgzBGeJqXAc2PSWbcMGKDBqS
hVbmj19aGtFK77ufN3vzjLKcDPOk0x8powth0ikOba5bQ9NGmD9IIwP7kPx17TtoH72eUzt/kV2f
X4G+oq3CT8KomjAcxm/ZLJEX0ul+0ObQ4pSSk7QtEKyyfedNvh+uQVnc2iDgSo7mDWEN6rh8H8Wd
OflEHZ6VV9DXxKQSfVh6+kfMG1groi1r0t3qAvwtNPlAD6IEZtL2d0H2+i0JPuH92oFhNP4irqXD
hX87w1a57redDRSCi6szZlFwjkTdBVt9/WKegzJCL5VHuJjXCMDAD8I5tsmvfd6jv9nj3g5YbjoZ
zpHkq4dug9uDh6aHQU36NmH5iLd8Drivh+/hYhnTFdzYiDtGCaUpYDg432CrGzZn0xGMLyibAnOC
kYAkd5pL1kMImSEn9HkHz94Ga8D/OEVXjWE5b/GKmsBc2aLQw8lidBJS6oiTURT6bHfO4aPH4hgL
ATD4gj+hj/du0MqygjslBkMHYLy8CiOhtcqT7D7IP0KtCqI6lJspHsxl3dvz/CzpF7zih7iDrfHr
/510wkobnWSw0x29QqlT3iTwHjC5uMWeJ2v8isUjKmlOPobB5TMR9peUoi+peHI7dSZteXo/FV1K
cNzQrG9PMawalg+85t8nMyvjB0bPJSlb83pkdy79BnLtTGtCOO0L1xDwS8jWMmyufy/NvPJh+TCY
Ry14cBP1jFyqso8Q5wgAYRnGKtXPxUb20CdlADsHToJAAzw6la/17fuTLOT4JnBxuZF5T7IJK87d
vSg343eAdRjZfQ5IbX9iAjRhAvOA1yn9mdToNZsmA7lGZ57MzKzFmenL1e5wBlX5M3QYxDqVnfZ2
U3w7IkbLscbq2tDRy+dfkG1r35+FFm3Ji+/xbM5O9Z4wpSPe4WOHfRKr2JkJXimVg7AQkIVerGfO
lbpMaujrAbzLCND9DDMzWEMc9xY+kmqjXwLWLshL59bVWVLVR+mb9ekxuqK8iivLatTd8H7991eM
puhwZYsvK7AIeJ5+Xz15F7aI9rqVaC35UTACH4Ufy+VjHEPGm6pzO5CR9eR9SMQ2A81m5c6CzDLp
10+U5atQYx+aPgOpqKKkORpXkezVmUus2nol+lAV7kCzOcY5bMdBTVHJ9XPZIUCe04Z2OVNBEqI/
sLgYdXkPIN4/aHAtkm4P11haxxkGHrFiJSoq7v8GLN5pd/u+WAKEOppfQgLjzyqJVLEXgbgDY1xr
CAwA2mEg3jmtcBIAH3zBssSg5l4g6nI54a7EIQ5SsjS/jZrVWhi9Gqu7af8vQ9mwNw7jo+mXkR2Y
BO1sKvDp27OzmnojGXl3uI8iaV7XTPgo44dKF4gVVgSsDjsdERAQhKQrSgNak8EH2EtMq1DgQuoR
Z+cS6mMahR2S8FqN2j3pf6XqG9VVr0QMO63wRPjGjqjxw6nMe6VZ7POo/Ap/cMbBDEt2C1o689yN
Wp57mtXgdKQw3t0QaeLB4/mZdnRvug2yBvEOhGKedqolP2/Gjlt/6+/9fYoY9n2+0qNM8CEg/FrF
FxCKScJvw92DGQeDqigjnU3tYvOB7FGHU6rPR0qDnf8MUE0kGApcecZR4KAXfYOMfoQ8cI9AsrKR
Zd08NRW0KnDwI7WFQrmTrOVEQ4IO6hoirFc0/ugtNG9btINUBrVkVgPVWLEh2TtpoWmLvPQpcJv9
k/v8jwFQhWDSd8QfmwpmPB8lmsW09AJ/ceNQqLQrHOrksQTHB+55lTJV278QzsLM+6YwGTCcUIda
tgHL09rL65HLQtAqA8Xe2DrLDFSPF3BHHUu6jq0/aY7sto3pld1QviVCzZnUXZvYuZCLqmrY2/0a
cVQtxvJWXXCA/DpyCm6ud4OJqU7mjeuK42XLT7sCXvxhZ+oifLpaEfYp2O0kOed+8D1vv4xEzxVp
SvItjMrHfycbliNUgQHKyV72c82F7lyaI/UszHACrt1pfnA2v/+v0yO075YRtqhkplpSsQmWfd+O
fD6fAym4aahqfplsGqNEJYTl2QwTXmL9XJsKt3FB4MICmVPMtAXndBH69ZKe3CrzSM7Zd6cLdg6i
9ti4DV75Yg0h8EY0tQuF4eXnBzCe9qey0caJs+ODp2M0Qs5rSdV2BxzNG5+e4OSook1DGMOEsOVi
5HbE+Z/ti1vzvKYNf6ej62cnl2jTuWL3p+t5CmsfYGZe3c2OD/vGoA55eV3YeyKs+BjTO/iuQOLr
OD/o/5AzHSzd7G2iBKQK4+7luBPEMnXu65FFA2fyYaRFA5/BP7NIh+6pSHGeIOLOBdFUhqPidRUo
8M+nuApIfAVjnyaOcOMPytCdIPg0eDseLQjLgTOqsXQGe6LBf6uab6Fh2HnLFk9l746sGpwfhUhW
pjWIF1yq0t4mRz/QHzo21UAvX04yMIZdDxWitiqlYx9YBmLe80ZYJokUPXCQltFdlmZUbIX4DSn+
e6QBWQgK/MYqCZBIfTSStne/fVoiQ9AwshEoxiA8mGrPRek5CzCbEK33TZMg0MfFExoFO9gYJiBC
RiNOTLF1gnU7MJmZUyQ5U2Sm+Pp5zeJcSVlMi0y/qtDtlIT8KdjO+UHuEi6Eft22rQudbT0SAc0q
mxmlVjCQ3hNDxCLhhB7GPn/MkDtMojWwEpp5aeLr8MkF7L/fD4Kb4ljglPcqhjKS2pD5WxWWMyEn
t3R93w4/DjrWWY9B2e/lRcFSbNI5HR8bKMhM9tG3qiSpr5SO3o2bRhebfJBcAi44j+/ozEng8b/3
7GrK+AwR9D8LgyuF142QnOLlgFXyH6bO7lyqEnWrnkOan5ickvv72VvGa0lIin9/DzLOQ9k0Crs8
i2MjL8lq/YSyb/KouaN0VhuMsOrPVjCcknsvc9HrxLHGdPFmYaDIvgYqEMLYY0DFbyCvvL46x3+D
QrALXr1h5rrX+MGWWB9NVcCCz7wJwdkBmkXUo4Sf3gnU+zRIRwXulULUDt63xTPgqQZ1ZxSoCKyT
MIiGfugCgtqB0l7mHWO0SdnY+OXQ30HDM2eLjgqscwDLNFxt56QRjv9IixWQXIn6/HtBXVYp9Kwb
cobH2LgF1r2z7vN5kuwgOMSgO76/arA1IMIOeVQ5fu4o0syRUx9R76Iahkw9X94qLpi2+TQbtJlD
ROBadxBd0ZDn0+m/P+M93j6QnZnXDHaqfq+ZTb7b6WkpYVcjJYfIKfVhMiMOdDZd1VpQtnjhhF4J
iXHCc6W0Ftp7NekO9AuMwJe/jSDDL3ETEopMw5gBflEBxZGs9v0E9IVz+vJR8wb8DO3+vkwyXddD
Rx4rHT5L+Rbx1HIsZ6TQzUHOPpHpJG6hBOmR19sYavN34tHJTdR4ng30Ltyf3s0ASBM8CEY0BMMY
6mkEZ5KJ/Ko6y5/UfP6fdJB5KRWIsuofNqCxBBAp1+C9T+VdDTwIAAbVIY2NOSSE1af2PTLqetjM
VD0PycHB5gtiFODc1TvcOa0EpUeHKwKftjiMlV2J2+dhQ5jcgUxiqYTXgB7OIPNYeLJU+ouHogEX
wpq+20Qv5Bfos9VI1DEYz2cYBSe1xrWMHj2tIkRQkmqARn24vSPhi1V7O0i4v56BHg8UvPt4Yo4x
FqrsKCvEr3S7+8NLUwL5vEIOG4qsNphy5SOoJQKUt9mQMIQAFfpwBwQguCEyxZYW0x9/tuXsJk+H
ycYQ7iXHdcEpPgnXMOlv3SgjDW1X0x/LJNwzF9H0n3Af1Xq+wCKLM5yeM+e4UGrZkHeumXh/s8Cp
OcWU3akz91RO2KvQdsPDqCkWwl33KnrO7OWwD7kz584cKrNJEOtdz0bm7CP23dUyZz8xfIhjNyl8
s3sAec3zJv17SqcHgBlx6ZVLtZhU3CjTQjNRwf8U/KAB5kYwVKF2K1zkRg8AOSnnchph7OnaBQV7
9vSUnDkybTY4TLMBWF9//6Clmg3gYDfAsGJo2euO1PvzCPQFfNh5d/SIYXClIHkDpZSk34Yt2kkU
l+Rb13cwtnK+KkJM5JP6r2C7UWDeaTiKb+HDZALkViQHZoiTY3i8dyvVpHtmsosgqlLWNEQvNPIv
awZzrYrkEOid8fzNlQ3XyEV6ZF95i+JJBAg4TuLlphvwcqj/bUxWVFAqvkDF30QclCUU8hUkwWYP
fXvMvUF1T+UZ3Yp42aR2lxs7XpjpWZOMd2x6YGipFaQLjEwwhdrkAPvBK+AF7fBDBaKJ+2mEr4b1
nckSofUbm+IQwDan0VzsM/+7DKI2buoSVIhKRZ+OawPDU1JMuVX0BEA7vx9fx4rp3775iwYY6oOm
RqDs1WcUdmI0rtxqrL2ZebmuRjVM65Lls1Zbl1foxeGcYk6HcCIRMGCAC0feZzEhxr5fU8jXSGIv
2P8pREggXoz1FhtWtAsb14j3UEZMRhh/IrnrDFQMQX0LFqBIuJehpk0kBYPssFht4jYEoroqTgS4
DWQL4+QfZVdVvHse25meqqauXHbf0x35x4GyPKyd8lXqPXj2AJgPuCneuOwFnmU4s33lDibh+Xo2
dDQv3h5V6NDPKoas38sNOqPgVuf7LgpkCSYpTsUl9e8xQbkprY645bRh0kMbqnxjd0Gnyc4I85Bi
gHDyExNL9S+0SOeX1an+swMCJjli8IWM0Rd9Xgip1whOxziWLaE6MdmdIPjiPRFyUO9+zePrwPvy
7NhZverbRbsjl3QbXUFvVcEtT3DK9phdm0dT5rPzRfvts+tR4VANHC8VpJ5CdNU8g9UUUtInD1Ev
xTtzi7ozrWbr4R2/WFBAAZLgY2qsHid5S78ne0/8//LFpwJNqIlLRhrKEavDZB7ag4LEaYsJPd01
8vYa1qIkUiaAZscI0/9+e6sheWg4B1yYY8GgvGVDr81jUEs/rFweGUeDIk8SKE5zCKgP5PfUWgHf
65hBLY/eXMgsfTq41CFM/S9ddibtWq2qJgEiHnKHyqcAxOXiaWDZhdab3R45g81gLAhYpv2AjtkK
eXY/f+hutlBqS0z5Md7kucYr3+i2V6GmCrH+mtImlC18EDmNz5ppBtrOONaRd8XOxa5p6g+yap9L
of5/1bZczmJPTV1d3+XsNEUJ0ObQh1kS0WM4yWtt/MqwqWmjQrwESkVquxT4zwGpr7H+Gtt3YBqh
KgjNtcLIIswA2h5iCaTjyM9AYmlg9yBYSAEB3JCjRKv9XfGZj0ZN1tQ7M3zXwHvLqeghHEh/I5Pa
pLXD3YsTMy+6sQIpKfhAffWFnX+S1PQRwiQ4v16r/vKtBLY6ICF5X01Zw0OvLltdrRcEAGY+X4SB
EkmpXje6VqkggwOsBdE0Zf++71eUpeT7cZWRqo5mUX+Hmy8+HdlbQgJ3xWeU22qHfwcRWL66imQH
ItcY3vNA6G1jDrLKHmDaXQB1XXXhoODmn919PF64Afhc0PKRdzkxI9caGzmGTct9mI9pj7ovYLmw
65HUAnfbb5xnt0ynR17zzMN3331yjpJewTUv2Um29NSrKOeC0jMHBr108YOF0h6b9Ah/l6/1sSIv
6sAwVQyGT2P8c6+BhOVGVaH/dU5qpnm3x+luOcXW2LEWG9fZxlQmt65RnyhnLle1uMDE/jkm3nuD
F4xCjN3cJyeT8FJLgtlr0gglWx1dlvTSUJyFk2G1tOhNQjjhXOumcbwYAeCndOatvUaqgSKaCKDI
hbvAA8EX3Tnw0OAtfrvXzmzbSvLfCIHGdQmbCZRQV24/MLWn0xDmMzXSd+Gf45GQQVvr1WUOehZp
XsPg6S7WFsrLz6ftJB9Cz79MDp8QleMmh5cEPSoG+nnYyN49l/9k3tcf+upHOfbskJqDklnaMv0v
RFmEr4qlfHYLMOyW6PExuuaOfcsa0vTenLIP+t6qDQR+Ap/Rvq/JeB4cSoRvqj/IePrbS2TXXUYb
l/tgj0as6wqISHRzBrKEPRyjrpqzXiv52CNSqJyEPB8oj4GApwdpcoGbUZdZnv+ZGrmBAbmnQaY9
5q9AsLlyaXIPOzn13gsPEfS5MwalmpDK/CT3TvTHHYiyd5KSP0e8JvTU6QP9fcins5MVicqScQfM
/6pEY++cYQWZvGzG7zpGcYsnx0IyGprk6982A5ki7PiZd4ivBewFPj3ivD0u6GTy/2vxIbHiPDZC
5m3BQeaEuijkdJDbm1Xx7kDHmqGhlOpKvPQPziQDiacNZpKd9KQFYwTcvCLhbuXH8xI8K/6siJCk
LqG/d9sayReur2RzLxpfxOnS+cXmRC3EmmCJyhqSDuYn52108ko+Dr+M/DMJgFhlq/YGTOtjR571
WnusF7jY+1KSei3WCFmRKAz91AYQBBlw6BVNBK0M/yhcHKlFWd+mHd6QfvXlN/5B5YxulHgebrQc
0j7V/vOYTkl36WpdVldj/mWaklWjwn2FcwWbTgpUEIMHsH0KI++JGujxeXp6tRmHm3GwExqUBzFU
HOI7x1boy/Qcf74rgmzWl0bE+48mchQGPqFo76bl5bfcNa8UWvnhu9in5Wn0BKxqLilEC96bNGpu
4UeJWLbngzd2GmQUY3UNiaeFrBenkVfvfhhJrloTdz7s8r3q0UDAhuxssinPA/OhqRZTF0ta5zxg
bxQSsRFIXPEaNze4kghmDaRw+FShSeZegpecjkhQ/NhN0pboBqaUdAK9t887JDCtchUxoTaG6hQf
8kU00SSPLBIRjCRcP3iJec85znzRAUdGX2T7/xDtEC19A9WkpKFkpSUzo71YIMqRDGGKrvF/bRrQ
5t9G4kJ69YUQNDwJznYI19ys1x4QfOTneqvOf9j9MgkmkZc3P5hlkMs1aYxibmPzYfrgC6IJ2mIR
dT+IIUdbRZjbUl4kJDNHVeXwdbA5nPSCzGdO3l1pnw7PoujplZKyMBKCZV05QXG7HLdy7L5GSq+w
OS3yRy5ukBrnjA4U1COVvWMuSpbufprFQZRlfSV1LTwUYtK6pLQDYME8fZjOnvtszY0hfqqPje5R
yrIpApmqLRvaJ82RCeCAIJnlfvgwUa7g/O6O9Cf1qNngw0ANOtLpRy1fXSXX5k8a7SwWuYoE+bcQ
WIeUgrQ6makFdwVD+s5Yxdcdul1K4lQslaEsz10PvMPn3jj+M+zTcF+3rSMyZnwj+USl1Q5qWmi1
vhMYymaRzr4BrbwerJCUjwVV58Jx5DXy2Pp9wJdVSfxbhSMhWDXv7na+nvKnWSodykDJOO617yZW
c9MhALTbJw2ql9Sdh2+gjVsuv0naqys4xYVQOCxx5YbbMeTeAd/XDXM8/kX2VxRRUe2oGz5ce07M
Ht83bDx5fUjXMtF1HeLxRRoKPZNzfP2NN2hwAZ/vbfeA5vNejHssaPy4kuG7fGy0v9qnVqGWY5+9
HvYl0FrUyj60AGlHVjxrTvuIfoDRf6chiTO/jY8bwTba1IMLXueHc8LJzg1wqIq3hDn0oUAFRBuh
TQoHC6t73x9h490y00Dhh80MhA6uScQN+GZAgsMpByNSjwe2oZZPN16NoE+hw8JN2UMfH8Q+PHBJ
hhqrI/Un6g4Oitn9lLgM1gOe08GoX6xGJqL11cgZYFnhf1CNZH6A4UB088nT4i+RrGvZ38iswqvC
Z8cksWksnAPBM6s1lfjOAV7KuH97aHC5MLAI8W5vO3Kaw0KD+LlZT5s/wV3GfwsyVms6myWglbRM
HGj2ixyoudw+1eZI7gV7QsGkkz5Z9Bhhkee7BjtF9LvKXMmL3VFhEnx7XKPCU7tIdTsNEofkVz2F
WNWNKEw83efXJq/xe2sDYvLW56jucLBdlfJ0VXz1UAig2Z5N+5XGGIhSZnAmkanyO94Sd5KMm9wO
S4ZYeBG+ngEe1v7gZ0aY6HP5AfbTXIgsnQjedUAnJ89Rq/ziPkQ/90/AXXWBmAjvCkRdpYAjzdZB
SupUFLxGbbua+vWcLIVml4fZ/PL/pinkqZOEPx+CmrphNwPUTHxb7Zz6mGNEb5OSGZcJrMOVj8EL
WdRkSv+MSNxNSGn20Z/oTL3Rbk2PinaDjtlrGWvYhzHcpQFI8bVe0BwOGrpzHLvOk2DfpxPiKbAc
7eiaCqMy5DXkgqyc/Ry6/QeOMXJtDzbV8Tka6hnDu/CmR/5+k7du9Kwaw7lNrxcLaQuXfrNXiNOH
eRtuXs9MqjtpyvF9gMjzreQVHDzzc6cPYmCYtxQDraOKDuYYwFdbimhrmeVZZiN7xEoWs1I8qg1i
DQBRzeWZt8S03a4sUwIXj6Am/vS3cDjQHyN9iy79o8fLPyqly/ORbNsd2sU9L/XmuUJLSDWbNhfg
6B+7fJ91N9v3/5OorgHF1R5gJqhjwIAO0Q2s3WUpK3fUAkKVqkP4WliGctlcTm1fYNLiw1+1POKg
7uEOUebwlqM170uIm7p5GY1NZ/TM6HO/whkxOds0iZiarXJ/5TzDFplc2enAcjI9cv4euuEvS3KS
0BPYBHwBVH/8j6sSfRMqPwZPKRyGYpWHI7Y3mQ7sdvoQcVsGXrBsuSSReo1g5fJzeN4UxVl3zHri
/6Ci2O+kyaPdB0sON2wFBWO3ihC+C3BUIM1h++2r2Qzckd69Mo/U+CrD2axPeJZ29GRw2YKSnmG0
fZNSkU0CfniKWPDJh8LnaFhQt76ShmURvUNiz5NvxASTjSwSwTSWbnlbeelFqQ6Z0YsII7WZl/7S
2S/1m6YNd4N4k47cHg8a3M0EgOKxshrGEfCYvbDwKz2VPyewRCb2U480iw3d5hyGikbE4cNSnPJy
BFncGsstFaW4kMf4cPcOL0SvNeCYG+AluZ8+NhfWvlocxkswKWzYNh1YgbmKjZnUYIl2ZKxkqw+y
JesKC5l4SEFyk9KgotLFKm1pKzLuQA+6DT3YzDVwiwifeyAm0dcl6k7rhEkhCHYHahcP6zKh1C4a
GcgfdUNoLrwgOVajCUrxksoiFTW+P0DK6USQW62qcm7qI9pwdi6BbrDWQuyaQpiTv6PSyCtDDB/L
psbqHMMEFoxnCzIXSgsoZbxmDL05A4NF8gMPcSESbPhdCUYjC01X86EX3rn/mMLZ4l2puB5ZNiOj
R9mcx8sgUpM5pbWzWeY4sTNUrUO5S/n1D9X4OqD0Mb5FQHtVt/o0NidWhfe9uewIW1DBwAwWLwcO
CBeGONdY4DkmdvUlYIzTtmMx+OoqZhuKZzY+nwvdolJdwSbfAAVwVMm12tw4CDQDgi2l6PJjz3PH
4nwDtPxNZqu1fcTDqj3YC326sOvQdx19cXHP1P96G6AqOMAvVQRTOzOcvr5DYPCsa+TyhXtWvV1B
m6qbOyMk5Jp3ejUU7GLXjpgZHB6hcmXea2TjJOBYMUm9pRIvRDt6AYu4Y7bYmm3k3EgZlxlTEZyl
85fv1aMy/LzuzbP/ZKlZ4O0HacmvTijx7fA/3GCHURsHCBWuNV/WCAHU8MgeSAa+9WOQKN+IcMuP
LcKqwAcRnTDVD5ggdlq+aTUvRojyMDO4TFZEGMlh/iX0Ncs7FG7+2oJm3bLB4xbYo4BnGclyKzBj
nNe+mvg1r2TKMSeLUcR0Gg/QGzu/041CyjiMQvzQbZ+b76XA8+BGVGHcEZ+K9Vj627TzrSBflLDm
w5OSEmPyHmjs00JFMoS1Ef7HlxQyOy5gyVmV95cedsnRaAssrNuxT9LgP/U2adPG9ZSTURws/hbU
pdL9nvG6/aeT8A4FZUAAG3/XC8q5yOP2KhI6nyEsySj5Z65oFzuLnHHg1NWvaUIiAEOBUQsY7SbQ
yB1Nz3HI+HzFcc6SD/2o76+JRO/v5kUdkg9Ixn8OEq/5XFLIakK5NIy98Mr+23ysBYzvkpLsw/0c
fAX0+3p021cZXook44KxCcgMzeJjqAetPfmHbi6VZkCWisFtkwr1t2opi2wqVJlGmH28rgLnYOy9
ks/Y7gxRQWs8hO1WM8BLD1jepJKZoOVN1YiZEMTdwkRrgIQHYjjLAG5f/688kPvRAIC3ANGQMPHZ
Bluw8a0aGwTGgiU6gH2uIt44Olvwi94PxCM8vcD0J7azpbG7wxMNdnYdeXD4PH//DXeXp/qo5P2o
BwUCxSOApab33DcTxOYQf9JcMVp2IDHzCdQb+pXTfwQJcGLfYKYvkCvhdtlTWKmMGGSqD0tsiQ0w
ePHx5evIwkwwe3q3xFd6Ly92jRzrnG7zNBga4XS4CIcNMv/MaHKjdijJvP5BpnKfym8Ne+05/3c/
I93/Qo1OAaZe52KV3MDkx4ALgsCWCgwG/TaBs39BVdoSKXRA+iZ5/d+fXVm8I4tIBmzDoRD0RjyT
eUR/c1AlopsXvW90ojJUT7euxDbdN3JUaRuShVsXpcRx6VuB9KsND+1oM0vr2TVce0F2QMmu7oeV
GNsKQkoH8Ir+yOk8yJb/5kzyo936Psn6sZzLo7h7v/YLrL0sdoN64MJeCpo+Qq1coBAUL3WsqULz
Iy4xTrYnRqZvDJFN5rbBIAyBYCzhhOxiT0iJLvRyCAk/uOHiXIhr7//F3H48AeSgHhIy27MUteGV
HMzEfUzvWcHWm2k5rY/UH5ZVlZLFP3q3fEa3zi9doHJNXcSLqPXV4xTa/eJujr/Q7ypK7VIhMZ8L
ZRSpSeaHG9AWwN9+W3Bq2p6UqkfP6djMdhvxZm1vlRO3rNV/8QsX+mAV/HQ6/lhuIWAHeprRmm6G
s1Qx8ggGEWSD+twvZdxl8x0RJLr008rTPBvifRgT1k7/LuNZA+Gp7XJxKDz4v70CMD4zIMP/8LFm
+KM5NPL47dPxCECIb/oz+EtiwnVkaQucWxWJeV/OI8KrMP1m9bsnuS8W9X1iCXwtEDTGT1S5/xDz
zoRvB5QMWCguuP+PrJ/Rq5tif14PtlkeeKkkYONY2N0ysPC5JOwMSfuKezSUGriIHXq9N8ezK67w
1IvaXr8gu2ACQs4ybv3OzQhGdvgGajDZwpnHZzHFuTmSESQTOJJJMgWZF4JSPL+gLNmS6m1SFZgm
pMGOLqVEUC7O1z3w50bVV+df5Z/ZhdZ6SqiV7f8QNIouIS2EhCjZ5ipzaR07p7ofNiSvK2Q7WUUs
mT64BtSKzT7NOPs+V4LspS36SCStT/KIbt5gz4QU6MPEBAQsoS+Nu9usNSM5q7m/0C1Af5IiCRTB
Zi8bI9K3k5/djpRiXtNNLNFapUHiD0D5cCaTmGQT2FT05HDohYOkwcFIi1DyHoPW3Ymnl7LMgLHD
5qV0Qli+SwehvhwCLremKdNcjuEJG7WeUkCfrmC4xuBbdGUncidiWv2+D4xZvB/2m3+Qjn/bpMpD
8tDtFa7EbiJh0gZUC1CdttrWLiKjo3rX+NMKgdPVyHgRn/1qCUlApRYqo3HglexBY6VRXIYWjcCW
jt8M3gNqpBuDFrPEUONq6BApdO3rB6ladLDZawNqI2wIlY/wFZ4G7uqMJ4Ds8nCHiY0FQUmn2947
l+zNKlOLOHe51SNmaou0a90mK4ABzXEtBwk+bEd/t+zKSnYyFK2Vot5ajDZV3P34O9ntjVyi90nO
2oebD0vj5UIPllwz/jehV+4rYRf6cdd7L1F8JAt+PescIqwh8T1WTd8buvd3rgzY79bS+mHhtqiC
MnNPQidmqVaiXraleJtbqKvg7yu4dFmaf3u6xxD7Ucv5ObGrcEvRf0NAd4tP64vbg+yiBkt1EVM5
1SHniIg6wcwDNNH7MfV009cuiU2pQQxDtW148W1Dni0zzG9Q7neNd5Hrfrz8LoVBAtOXAcHzN9cX
fX9A4ZcWMK6OtCFrTDbl3M1mQrdKn1wiLT+GByTg8/V96te3iLnr9/dGmGz5Y4X2A9N0lGie5Ruo
Kf00U7Nk7tYbvu13bVnmrlvXOrUP+4RX8T1Nvm+6uJz9yAd7YyQo1vWnFt4xyIHM5VEBLUxhWADC
hak007G/VFs5YHxZZiKlH/XNqmEl8bx2pNAMy97KwnsDNT5XMJbX9dk0CNBBgZhHZu3l84BAL75s
E++kz8HYZrnvf1Tmx05pBzUz2qANAaf96gPojGq/ogzlGkleVFAx5rBWeNmm2GqeeLHMtc5aMZay
8j86LSrx/t7RFs12J+Xw3KbgCt2qpLSNWReTEghi09AJ5bViSEd/ZW5/9+fcY+NaLkyhRA5YJZqR
Q/5Rk0TRZ1TmLM08Mw4yEQolgO5Ps+Cmq2hl19qm8Qh1OaJcth+nQ1aFdFX9F3uLBGQ3yb9gHHP+
TZqOIceGtNmTJy78HAKMRb2J9S+L4L2iTp9wF7Jn/Nv1FclRZGj1Oj2lrUw1O4scJYf/u92pwTSW
DpZHNdoUhV5095jhLZS8y+kb9VP/ytJ5hCpygOF44UzKRT1JEAqKGgEGDOdauLPcdkhIdTqBvaRh
XQxXQtR7dArP5CVqUDJroLyYHxumV6of3FRyLcHK3fwo1WJwBaWAIMFiNZfsBv6SzsKLcnxYvejE
Ik7Zay4yHuH1qLdGfTXU8IustrZnqzbmbNEXPM+XxAErbIw6ZhCPwZDwYSNopwQjLtzTIrZiS6pG
ua/q0zF/VB+R+ocPUf0/TGPgeGNcBjRmV4oZq/v3ygKtbGrgwOQ8JtJQfv1b0WDzUASrKQtoE8zK
YGJy9yf+Wevm6wnEtMDF1CkCPtU3Tngotd70k4Cm8JdTNKaw4+sQvJczpcUSvKPY/LIZRes4JWZW
9GBul8ng/ogDM/KY+yZiPZTcioG2Y8yfE9/7eodTEPUPe4yIpAWLiGQqpv8xSqK+YTAosaSrKyl8
UsCExuGGdqKGui5SxB9K4GtOFaNVvAiJ17saVCCxyhJMRELqrFhm/qxgWJfV1TRQTqWX6Kglwzmz
/JqTzGrr4RrxpiitWFiK30EX/JisXJCA7hQ3adU0Lno/7uZ26B0irvq59iZQR+H9DoN7h94pYFs/
i/RZVbg4+S7VeBg6DbO1MU/TiyM/Lv0bxsfVk3/unIr3DJjLiynovSXL+D3tXQN4iQ5PduuUhGg4
gmL4WUW20VXShHuxa94FK9CYGyFtPFWg7P7sV+ESZKw5BS6qFSNU/8I6W9r9DLly7EDimWwzBqc6
G2bkyujsdgQlaBtsowDr3nf6bGD7csMIJRpH2nhXUOieA8U+hkqLocumNfJd/+I0CxioZM4RbnSE
aReeR14q/h6rCXfZQegZGmBmOPm8WZERdrdORb9svYKpNKto0ehikCRtAvsTa8Y32LNIc290GXqP
DaCMBKsLAFaKfKEi8zFrrMw0N9JZxBTKxYh8nx4cBULOLMjSJXSG1fXiwYUOvlPmvWtXGsFx/pDD
xyK4S6ZeYYOmr1+/aPaTUttA0kd40SL6268Oka0O1Y1NcknTXlNCQVglT1Kz37MDMLyYggMGVVbz
/ljtqb3ONcPN+/lhvBdQ83jLqq8Of63vjvxpQvAJRtKktR8DwjV6LR4KlbHm01aMGGR3GUrtYS/m
mKvCrHY5zqnFa898FKEJ1zLTvYVPGSriJNbTfITcEGP2ElD/GVGC5FHgsPyfbaWw9wekP3WxntnC
k3vGtdud7DW002C8sS9OSNZUvSllkDwQjiQOn3a503Y3evJyqWNlOwfit7JIE5gxRRMvW1kVOnuK
2UpZS+pVv+bVtJckmteA2/9uOFJ1SCKwaQBZP6s8d2rbsK6DDwh/Y/pvYuEEGVgYtuW5AyYjZJ4M
B278PcTkIWVeZJiA+mTP0mEr6h1I7qXvMwW+5o3zl1rPd0QtO+2mw2j56u6QisCIOMaZttjbDr4r
Jt7YEGr30Hqt3BXxT18YC/A/547GriOe3EbN1J2mTnypZM7FCrtjBhaOAAjUcmQ7NGef6/LnFj44
huGEFpAwlcCFr9p6gowu1ACgsJfuVchz9jXGK9avWce7AfrGzOnomT+3IKcikgrTcypO8qp2xG+e
OBnGviIekUJZRCmV++A+r93jW/xs954g79caaAMvIS5XeZTx9sIXWpFflBevXCeTMJFpfauBIxxJ
7btArvMOHUw+670+Qb6gEIZQ7IamZg1eOssX7bVU45+zd2ESHGIkFOla4+AAiAukjnFJK0CVEzr0
Fi//JbZCVl7JDnO7FoBHnfN0kxMr0dUZhVn8CqU7Jl3I355XPC4qG1np71H8BIxPc99U/qb+oary
yPcFMkWLIE8jJ8Mao3ZyHQKRNXbPS0oloKUJ1CXiBRsXhdyTtnaeqNJe5B+rSlE+tWraR7IJMwoS
Wd4TyVaFE1H3k6MaD3hWRSUUMoM3a2I3i8G+RfG5ez0fHlordRI0yOV/TtfPZ87ntnxUzUq0KrYC
njG3daQ0D+qy5b+7kpDt/Y/PQNivBoqkFWXsTCemTcwr8kKbXsTn2qyuLzbCdYJlyKnJq2Aaj095
i4qAdI8G0cZ7A7MkhupZLXVmh2A1Nx7vJAHHuwUb6uQaAKn7k/jg7TmrTA60XJNa6X8mOcxsSjsM
9mphy+SOWPQnVDwP1Te8SOrHc9+dnUVTxHIDI8PPtPsQc9+AgnaiYYgTwkW0mIN1KuLoYS7QFLl/
FOg+JuRFff2YurmA/CnfsJkN1pdnqqhz+Bxif2hSYLXrEt1Pm+aHzMeYAP+CeIfzoahZw8gIN/hA
0aJLe3Tx3Sz4QnxD3jkcjSeG0E1wgyryVP+wZSyGRfwL7vZOKrtnHCmgt1S4wQKvNhxhG1xMA8KI
eF03Z8GqUJeEroK3vw6CpWJeIYKECngMWnlAcEg1XlYw/nY6yOlhdKSBjpLuFrp0SoG5psZTSbQJ
i9YUbCsQnETUo0Xutc0ZtQPyL0BGnmmj0HIzh6DEhVPcbj1okexdxJPVthNfsf7YQcAHRADBR0eF
YcBuDywtMeLBwWo9LHps9ehbzSSgw4gmdqqY0I5bA/9jNAo+c8PAq6h/RH4PqHONmUX6xq6IIafH
/c5jjIKgg39LrfGPq6Qkjm4Ly0dayGJ7IHInvHFa0wxCc+X454CY07qlkkbAeaOILUISTSO45TQ3
LorPFox/20QSvx02QIRtxYRv+tlTmKaklqpiDFerg2g1vXjsW9eX5leoATNFHQ4O9Ke5dckdq+DZ
vj8pLohno4ksv4Mc/IxShbV4yXK+znKpA3lHIqVBU+JodN5ncEL07MhiCWk0TeWG2nw0P7+URToZ
UZBVX2FIPznk+wcU7PyBgi2m06Es63nEGLX1BEmc+IlPT1OSnVnm5eDkWUP16vsyX5b+XecqcGOZ
6slkYbhXNaGD464OEs+z6Rep9GnTzk29S146vP271JBW+1JYXqfRglLhdB+IT9K2bP4e+VCGWvxs
Jv9h1TIZNrtxEve62ScoaEM9RfS/AWZ4511WkjPItgtOqfc5hpXAA7uc0KvM5CAYE8OnSgCsKpuf
4ors1AGY0k7Mee001R1n0/XpW1xfjQy15Det+BC2TSC0FNr/fUgJSfqAqeCvd3zj0ROFVM7OjZjj
1CzER7ohLPM01Jg/SfCgWH8VqAXcrhCwxchnkJeN4igDyaMWkvSCEdc4wiEcWUJ7b0xQFfuTMtZB
RlFkh794fUV8Msw9zHvCQ9+T5NgZKEB8+Y2LYUpo5uIgNCA2UODk+csv7gKhXrpx1w3QfncW/5DM
PBpSBoYDgyIF/ojMTjeO7s/gMJueR30PC/F7yznIGSiCnvP0uvmjRRkKnYjcH+Yap5JVaSYVz3mJ
xrTZ/gqCrTXsnhMpx8mcfLNNXZyn1t9uoNdI6Qw6Ui6e6K8DWaC/LVwM4B2F+0ZfS0vlh/4u6ZlZ
VY0Eapwd218hAHZGzO5Qxsuk37wo/a34YOCbp6y1IE7L4AawZEserj2OK3WD9a7fLIv2rUKueIgq
ZY/pTLI8YhmRg/yE6kOrz5xU05EdAXTaSQONkeIZhtybtk4gG3aEulx/F4v539+a80bqggFT3XLc
f5PRwZshJJeom6iT39p3oP3Y3JBWXs/auot2IewPSj2VOvjF/rPIfCR/FDs22sW1NsYTmEOhWaFW
C6zvEDyg+Ra7KMXE1Ge2ePupq3xIygC0GxKijO4cZ7/gr9cnoQzksuEU8OX46eQH32LPQnDW+HCM
tgEsEv4OMxUIENm04T8UrU2RdhYichHOlZl1tUhQgS2NxVDu9Q4iLWAi6Gj5ReAKVh1XSJ1Ms/Hw
UruWDVPV3P3wKQPiP0pRuG3Tu7jsfa1jhUVViQoJGkWHXXWFEDEeLDwjyV3hboMHyan0LCAWHb1R
Ze0aWBYATg0e85OtzNRGhMSkBW0NQx4fVyjPNmplBqJ6QJ6U+gB4PCgi5uIz9BcpwAiZulyIbfl1
FdoVDhPVliA8t1LTuCdcg1u2bGUkXHyeTS/dz8ChfmaHcA9ANs4g/VqQ6WsKcKIa8cFlwPR9OG0B
IbS+k3b15vmFqVgRxunMYuaRT8Mk/9uPefLTJ/zIva4Na2UVVnXLDkoq0iG/3ABVAt98RcypWs7v
AgUJQJxQBjfggcth4SjHcX26v7te311lM2JN+tzO8uqMfo/4RuAmboeADn4Knw4cRWFLsp53OY6F
krwa4X97K3yURe5ZtI+siAHI63AyR3avdXD0TiIx/qDMdl+uiHiq73HRb/jmgjoUX//xr/Nz78BT
VC5BGXD8jztJEw+8NiAGH4bv5nJk3E+FP3FoT8SIbWP6tkERwC7DkVACQXwmbaBKXV1dayU/ySmw
fB16CCn3HmC7fMbtnZshiOm0OGbMpilP07bAtvWP3uAqOkjcANYFp2fWwo3INBsnLdHPxqw1aFNa
ARKzXNZAQBVnmPXhEFbmnCNbaXJS6PPnF2OmySo8kCdkQOIG4Xn+90SE3dn/BIb2yZBfXVAXTZiF
E/Mcq5R/egRPTdVdRDnsHBwrhVsigDKQcyWJIhttqtn6y8ChOS816eba76cG4QYRUX77JEcRAzqi
rrMY8I6pIgzfQhx+hwJgat7/sDr1TmnuHK+BVlIfRH3fZptcI8oSBiAfsj+d3qX3p2sTsAsq+BzS
5j0ara2gdgqqZ0UvBUIkQjcaZuNQqTSaVeGZ+teI4Phs1rn8OdElT4F7Iesl95aWIzBqjgiaoQu7
TspyJhZ6F+KV5tLsAur2TQDjIVyfm+4ncg6kTy05JiQO0kwPzUTF9RKVh39Ni2jbxcRwNUAuTr0K
0F07g+6ASUlwElLl5eW5RlAgK09+IFz7jjo92INH4PvEhqkLViw6pvuubyHeHonyA0sci+Gt8njU
3g5eRxo6oWBFsiZ/cEMD03CyXAd/YChKEc/NAI+9RZaDnIQ5hfXzzPTc+hCy8JKOqkKlTuCuKFxI
U6Jc4KNF4MgQbBhksdni8rYyKA6MUWgFNz8tcbCb+tVYqD8yyujOs44JIHfA5D8sverzgzvzS4iR
cUOqBZrgVjjE2DyS2dIEpapacEVYGyDQ25E6Is5KS1rhOx2D1yo5JnNDt30MqqKjwa+9yN70VMie
/eT5dvEWKNiWxPVRIuxpw2Ki0EdArJEPR51V4n3iwMsTxsrOe7J+k1POhhh9gwuDgXxhX4B8Latx
g75wGNViu1brL3DSwdAu7UpmEmMxYyWjgcW8ttmlT2wANkSHLieZuM58GBmbuRFoPdu/VwtPbA3X
EzOTxhWigBApYk+SSsPSVYNB4xaS1SrDu0pa5j2FiiJEtu+4qk2BgLySCIJppF7zGwhzxp8oNStC
mj3ds6mCTK0X0RsG9m1ArnvU7fxY0ONzvv03l6IFds474esbqQ+8V7TOzHmxR4twUIpBtZkBXy5m
kRlyheAwDW040z3F1rblnjzoSXMrCdTaAlal3JBHkp5yvckPsCgBV+Y7JKCUPaTMltz/0T9hDEvQ
7jlvzDf1FgP+9Q9D/k+NXOR9yjz/lJmA1L+Q+0G4YDE+tVdOjzYXKK29/f1Y45iYXPzuRCpus15Q
lov0IyPcKDqqiQJWNiGIwAP4pCCfq3TwAfYxNDc8Zmrt6wuS5iM7+yUoH9xLqrqBNJHKaqz9UTeD
pRsfcH2eF7us9g91OGWQlpx4rwLtFIpc9KQfYf+S6sqF6FepyAsveuVBziT61W0Y7NC8SW8I6oFS
rchceq7pUcQK4AvBhFhhqM7BBOFfPozMKoAET45McnwdfJQWYeHPOzHPli6XTk/bj4mnUFIug+Ir
OYvkvizg/jfIJM1MoOvYpAwwaA+gM4uVdNUtcIsmOYEdj8UeTtySft6T219zGpLCBK8TMNx0/bTK
HnfsugSh3U+T/kCAsj/kAuASP4qFexo0U3JsYPJ44kYsDYOQfVBKve14DgUsE0aHulL+YWTx5N6j
6cJQlE/T68Dc/5PRX8Y5ojVGuIMBpEcs3hmW3Z/xeHAyNE/uFOkMxFAydRq/g71JlPmhbbGxMNMy
X4/H2ha88QEn2v5n5YDvVf/t8u5IYplFrzAmKlXN3uHialZKcMWMs5oJukrI9knQSYmjrfopKmdw
U00hF/Dzsl3DLIs2vYOz4a4dGy6vobw9QZHRhDfQSEb9onSNuffSgGhb0LOC8F43h59f2VKnpRQn
VdKnWZfGIfCEH6p70zBv59+4bn7jWbS0U4qDAwjJIvvzsrNRk8i4IRDdVNXnn3W0WaJNg/TCsOuy
5av5s7Mc3DT2IiZhltRJiJwmwqczjZ2TpDJnBc1ppbpz1OxpUUiJDyUdZqYXbDo/7cukismAlk40
UVtb1bT77ePZ9nv1IYAtGRCBf1CfzrciG+gFywz3JXBBVhB1UNaB2qN4DjWPolAfwVvpc3AI5RmW
p7LUOKO/bftdPDgjclF5NYLVr61+cdeWm0aJTOWru1mrdrIXhO8c+4+4LSY1K80a+zwV3n8IwdYl
JUZVDWwL5ulcR+Ju2rXYQBT/+WvLIrhET1Hkm/r17m3sFq0IMkovMVRDQneq47LLyc4cbcWS6n1M
0iHE33FHcCDhQGbAHzd88s5Bxq0cyF9j3cOUDG07TZ7xUG6FQCa3vEtogzvshXNIAjdYj1mxLG+t
MC5UcZ4zMOOnrdFMRiR+kxfXawDheqLhStbi6XvZDWmu6KemyyGsD+6kKC3fDFWm5IXyradTdkg6
4AktL7SM9FMBNg5IJo+Dh33s5t3qcxDsMFXcZHog4TBo8mwkYcOSetQm1QcZg3HXsdycy5F8/mJk
OE7c5osEbW9iiZylRfFUPlOMUxWuOGOh6Vr4l407yTtGWxDxkuG8xA2WAAzcISRJw0b5GW+6hYdS
XM5bUvMBAMqVMEIV/ijnZasGMIaMz8X6H8s7ynIFdUiBRF+VOCkxz7WLRHqF6k2+6QkWLVTL0zvq
HQMbUmhrBEja+ojcSBjRF4WvN5+S6oqeTySSAvfMr1TDokrxm8kcdu+pfTdx/JbwjZexU2v0dcLL
asLNJARk+UDG1a8EroE+34lVsCs/Bc/zi6LZf5LdD+Eg+1wAdeEq01uJFifGe07tjxFPf32bLnfF
zOZasfxbrH1/uVoKhS1ZDfeR/qGeTBQJ05FFg3h97uZRHzR7AmE1BRx5x9Ay9uUhAOqSBxURFTzl
Ee6dPxzLSEJ07gS/bfI0G3fdDQWgUEojKyRh96cjhrDWuNyZ7jpqOCjGgQFlY7K6A6/KR8XrMoYj
uhCobA5zb2dAFw4Orp2l/fE7kPfK8onw54kSUY2mUgcxOt3tdsDbZ4QiaG6R2TFJ1pc50oHL1u76
Uf1DCCwMx9RI1ySJeVmtFtWNjm1Meju04Ii+fCvwoR2byNZj7pcdhslzfo/Nkz0KvcDXpI9Y/g6y
r0YFvQ+wFJfcGXxRFkNRqk3RZm9xCgRJI4+0MmFpP7jJUWkjv5cbll0aH/jDGNdJ5NYgvoeHKbKh
wkBvnt08qW0h6M+0T6NNObz2GfsYRcXurPldV1cBRDVn//5k6YRzrKp9QujWGpW/UKEhIszagT/V
lgrXcA/hfOhtGtC8t7zEP171hj2wMoFyQgjesQQsLsKPTuCiK1qKroUqZ7l9ar9PGTjBNP09CQAf
3P7d7U9aznYlM/132HBFC2lCFUiup+2oCENwHPLLU6weuZJiwemLR0SOamzJ5nX3IDMbk+ByE5Mu
T/+4sxwhBrQrvmDdxymptyNXB7lNl+byfOeH+H1IbHUv/8Sz9+JFWQIqspwtOnfOXcBF2aNQbvHA
xsbr4xoLloFq2jdNHs3r3945eGqeYb17D3/z3Aj+I/EmBh49/ENQ/FE63Lpay8EmhitDs7QY5QVY
6p2PwCXcCDmfR+DrzPxBZmhAySBJpKfX6jfWf40p/hs6KE8zDDLTiUMtEJ/cD86Al/SOJFXasAwu
jE0I3qfhIAZPwceYhW0HFppFF47JrGlOPsnKOxk1moLNENErse91goBDI575wSLLKjSiS0H1/M43
y0FxiFxl0jexu36uoQDBvFM95OraFyvhEKyBfCTc65Xjrml6jMY6bH/QMRt8JGJZPLENWkYm9TFB
GV647Nihq5drIn6HZVvMvZGMJcnE7UgbiZbk1sXQ8GVPXib0Y7pg79wPbeXe1Emj1tJN+8rrDMeg
slaJ5Q8eGaXXemt0d9j2BjLl7FYnZRv5lYaa97wkvTpR/6TmkHQKZoyeX/r3/QNvaH/i4rME2p1W
J3aQjzBDcn5tITwDzLtDuaoCE3CcuRJWJmOyAzwsFx9i/0tPbP+JJfmFHhEXt0snIgEuQsKSUvzD
hK6ZtfDCCO9Z3RU+aeYLsJqZa+MrrKOMno7PWqVo1rJfSBvJFVVVQyNvXf5xqohfN0TLwNTYKsXt
w+GRWJh9ZphTaVUARWzXyuN2Ge6YdVrSioRiI3VrptrQoJJotFRdaWDqk6cZaUvsYxBHoj4Oqz/+
g5wiO+JFPkbYYmCQdPkNI8H85SGaFdFeh08P1+oyPX0XDUDtVfFjevhvCJkBK+EBKPhIh7uWc34U
nZEPq/DjJSyT+h7ZzE+GTgVDtkQxK8pimk8jid7VoqNnJOxfySmZbTyFK3HBJdWuWO4RRsDFT1p7
qqZ/6HLRqyYFRuSRE9zKl5XdV5FaPxAY7NktyKkzSd8qo3dKQGInqYKVcYqr2KxoVKiQBE+x4x4g
rD1mXrgKpGnOtkUILUkvdhCMYZ/qnMtZN+iqQYV3rhJFRaskAYzhmsXWdIqw9U+/N+2WZHSKMAbc
tw62mMcKZARSc92C6wd4e00dNzUNakJPpcvvkAWz2B8H8eQ8BWufnxPl34gziF1vrS3w2+DnnoUF
5gbzGteOw54sjU+7NLitjcPL35xSdbZbFtU4jgY9PAlcvGWvJrBsra4D3v68yWlxnUVb9KFR+PrO
kJ5UNMTm3C1+T/8KHobUNV30T6UGnOeCPnOvM5y5b2ApYfHgT6aa6mJmnCR1o1E8aV2gxNvhUd+e
ytGC6vxSEKBg6629x83Yr0FJPjUJH4Vg9HiDGdBdDDrlapSNmCf6huGZFnfTMgG85MBg4S9vmSnN
SmOvKX+6B2detQz6wBLqzkZzogYCnR01+u71NNQQfkV+nOiFRVUJJVt0VnmUEP8XNVHRGy5RlQjl
ITuMAIMz8aMWj+nxbG49PYnYoQw/DRV/h/z/s8Wf85XbOMm6v/K7eJjdeRTveWt1PTVD8yKjURfG
pyNHMT2fVYY/Ic3W3bWny6BX4ZmTz0WNxlwUBN6lNdWKNyVAoh9gur9vC0FzFWUCRKYPQXPqU7rt
FuwUyvPJa3ExA8dtkA3N4QOOiLueRnGc5CnVIq0d1Y8oJx4lOW3S5njoBBhl1d5ZE7O9Zrlw9xLJ
s9Wkz9FV9jMR861TZ7WY3RqEXwzWmLXfDpe0w9BJzwiEBU9ybVSyF7P9B0f/iOsNxJy9DQLNjvhu
SWLsPqJSCdpSgjimp0y5hHvQsBUDwm6k2PiQsaX5FP4+uCBDAu5x90C6tgELez+/YdVcb8YOaLKz
nM5+mp+FUmYaRc/4VQ6mc1qXt5bLFBM0ESrX1ZuDQCtr6mnIVEpc5v00Ys2+lcz38aWGHFoKk0CO
9haaB7p2S12E+A9LGxqOMn9vBvQErfLwSqQAeC5m7GZD4RoRDB8MMm0CAlKgQsPquWpsf4ScXi8T
fq8quhFNlAUJhrk5Sg0DkiGLr81ZZ40T5ohBYhIQSk2aHd1z8Wc5QuWIGsVrFPpHpK8v3kWPShnx
PoGk01z5PYOSgjMAATCTcxEAVQFLmlA/NqV7xMj+320j0qT7ig08Lc4Oj5oQBNVnhxGsgm+S153T
4iTzdwvYA/BGBoWX0x+bRsX1kU9FNyl5UipFBDvuPZoM17LZlx8LZvt89mstRtf0lBV6ssgz2tEi
CXqISqEgtys00gLyIcnE/tzO9nymaBfAL1Pi9EsNWrvHoUunLdChHykh6uSJG5taP8eq9WsCFaF5
3yfK8P7e+3AG/dEQcNXAreFTuJwz7vf6S3C0S+s4ao3/mE77b4730oY959W7V0C98iLJTpioEmxL
zJFOIhvIP+3XlJ7GJMz1+aoZKLwNhJ3r3kLkgISXE6aNduloJ5+UciokNyWPhoDZS+BEt2u8jWhr
KOywX2TVtyXZuJzrfYMShGCilTWmc4QKIwO/kbNru8voJkJSaGckMSZ+lcFIWFABZBW+lrb6ia1K
a7reKbedd0l0Q06Xe60aMnXS7+LDnX1F+jCplKlTX7BAMdZsxJw28kGZg3Yt0ojieqgVOUK7h6CV
Vf1q6lt6HJXAmVD1vPyeCIUD48B8HKGjQoh5ZWei9flFPM+eqtyzOBqADiuzvpr2v0dn+jjlGBrN
+UXaTqRuIqQ1EBUUej2jzjUXVYqZrTqA7I8JULdAi6/Bu39ODkJP5F0UdDzA7hcnxSidyTRI+jwR
vHBuxGayur4Qil3SxTypnkbm8iRo5te3j74khgSVq1mXK4A7Vm7hkkkNk5iLLMzarB7mxNnUWPz7
iqgfYeAVG8bYapDQzrD1uRyh7ZEPxdPCq9j8XRIuoiAxqXixdxzpbwtG+2kV+Sdn9Gr87M2PVr1K
fB//qmAreujlbcSwJImQH82hIcj3LF4OGXjGcLd6ZtmbU5Do0VWetb9/CCyCh8Q9cQOBx0lQY0g5
Wl/N9ml8v+yeD1WsRH15W12P9VVTLCuoAvV05zv1TOC4gneKlo5lyuiXt6oy2uoW0ipvFsKyIvLa
bM35E7QP2ynJrGc8AVp3hA9q3ENZas47rth1LwN8q5EIF0iuPSmIBp7OVRqGr4gky63evPSNHAoV
89pyhMOsNl02s1kX1IwB6iO+KY6cds0bXVlrNJP5CPO2QTqnDG3CHFGKANXlMcHnPDCeXCw3nprE
FdrJrOa2uT+qIiNMomlXeOEagdtSmQsE13DaSL5Rm2e8m79uyVsSOJIS2zBrL9ukHvj2oFvNXjOl
kdfwBIrjkm5wEDmpY7n/aK2+h51JTlwliHMj+i0kWysLQibtpane50IjlB23rYyfSptQesZoMgYg
iURyRa6u1nmgjlWdfHv0sPdL5yqrB99sVSSiWCSyoY3Jz7xHlRWIGks3rk1CQ6qmTv6bxkr/n17n
atRnZO7cZVFM3/Pv1CePLCLmQ7ugV+un69P6vVbruMqPaJ+NlOAx/TSl1170IcXvMhaxYaPQ9cW8
5e5NKKQ0tO3HwqZZui9dyU3s5G04Gd7TEJB0PEMwnrY58aqWBx8mGIB6AHvzH2RAaaJp9ap11TRv
FEAGh0+n/HwQmk/MaqgCSM8Dub9vObs8ahswTA1CtPOeGh0tUL4JsAKamlYZ/5QlhkS5G9LHO+dx
5ldjlWHkgH5+b9xOZbRgGLvWglViRMswamzqqbfQCG+VsV3vT8ZCCjPdcsKfZH3VkzD4YLUg1E24
YMadrw+4C4QK0CSClW8dbgD4g0mI51JlRkEhyGbjg9LrJpsGp//YGEOG1tCOv5Fdisipl+eSuAqt
6RD9bjuReJaVoRBtHwNu4TeQ4O0upNJzNxKtYh/ChRlcszBcUusKrJpUg2QA1+JEGh+CqN11KQG6
zc98sQjsgkiaKq9ZPwh7iRqlQk3MwrVxlRbGHB7jJrHy2mxtjmdCVNh+Er2Xs5MhkQHT6Uhh8f7X
TiD+mZbOXevufFa/CgIVtRL91AlacgebEOJCsRhHVI9sup8rjYGUqrUsGVvc3/Wy92K8/6EmIrmB
cDvXxtkpllwapmfuLsWHsxNAH60WXTTtgCCvk23VDTLfMVdh6uPS8CBIbQhcGd0g1BwaXem9fLUM
BN9I8UJBmPdck2qhrbopbSw+LDFGjorJ52tgwM7yy6g5Qiriu2xRDmcsQxAd9R7XpGfrSDJIRs8Y
ja+VhJtMvYr/9sL2UWo5iMlj6i6DXHUfke9Ibj0FUhYe4CtMR8Fu0ok6ot5lyBUEFJRSVcIMT4W5
Lj3fZ50SEOXNHDIMbNIhVZySdcEmlsJZ4Hbe7GL3+r116scSVmkeVzprOv51WCDaUg+Crt3znfyA
tQpdEuNns6QbsQHs/kxVCXNr/IIdr6MIZd4pijVzwQypNQFvVYsLxeMyki/2LU7YbKUEWYWJqWnp
lFShyHX1Ha038MMoJPGR7dnKYjvNvVlwpMRswkORQGkPe0KfnjAdMQ+QsYbP+On1xJYPcyhvawMd
QS1zOuLK4t2I3yA2v/g+kU2fDLzwKzFVBiVbCHKRSrSmd2r1CsTHnwjIpCP7oDRwjNLSlMGdxpqg
AggKacRvMkfgWAPdjUYMILvSY4xwsB8wSFRhaAEP9K3iFOkx9QrcY/uCgeXqfwMmdxu6tdpj1bAE
hLd3kretQCs2bKUiL6189C8ovkKH/Mq070BWCz7auO61dszexf7EqXNgHPq1aK5mjmZ9eOUOMJs3
shnAocstZiGnmKSUcRKfXSAbxGqjtoncflgxXnX/LwFGhztgmb4j/HH9zlGJUIhHBgT1l1RSIlo9
PEuSyCVVOVy2/2pdnJW2dBp3U210uVhunBATDUCu3zeKDyQxXuiHmwrrgXW/6Dvb2mixyd+CYQpz
CqIudWgP55ZkJi/JHlMmwhw+7UwNRICd+BNe25uwUPIe5Ny145MTdsb7Pdt4Q3s0x9BAl81YPQ0t
BmaKVVfXNaAg7ekLgkiSTWGv12qg0Vpc5TyKeV7o5KKIQkWvaVs8WQXCp0JgJgcv+AUiDFJrUSdN
wb6B/8NfGrsOvpP/+UHwqFMtk71rWY9Bd+SkUCprJ7zIRJQ3DB7wULbUiXMkEpSy5U3JQnsBHH0F
1YltfrOSwoVQsz6oMq1vcBOsuX4PtZ5W1F6OaEXyGnCAmXWILq7PqcMhkl5P0qVLo3Ew5erme4nd
nK18Yq/Tw13B4wF82iFDPUClFO8m5IM3tw7ZmM3Rlq31jApyVNS3SR01L8LRHNjaFCuWqIWcenDc
yPiDBJY+sn3BnKUHpTAypDtzw3jM1nCx+jJYSzlLUip9VFQuJDxdvpbed0/jsxEgR2rqGXaeqcf9
AFFaG3yxegtjdQ2ug61kwXSO4uzK+O3t6KMXuWDcsHsWRP35BmzHqNYsXKvH5PdluUCWLpyNWM9G
A40GZgeohSs7Mq7N4iFKc7yzqFbrf5ltQRu7f1CMk0vmLXI0r+1la+nPI4crXZhYfLnNMxKit01E
8V6LcFNwlMOuTJToX/GyvwhWtB99fykT+5VEW6bCd5yzdr850s6sVFrCVhfznWoplYmthO1hA+9y
ccNJ9/Muf4p/eTdoOyfIIvng/il4b7fDgGxf0LttfhJSEgjajAEwI6e74BNY+kETrPpWngP3IzIz
XdogYxVj2TEPs6D2t3NcHrt+Evo305SD41lnuVMdqVpK2/5HX9xALUZzaKRIQ3dpZPTak0kPl8un
ohlryggBbZavF5d6Ybv+V/wu3F9Pp7R7cCeqONQdPd+SSXYftID/3wMp+JUNtYLsWJM0x18xGwb/
YdljIGDQf2zrf5m0abNDGwrM70hmmYv9aIDKKFQ9uOk0HuyrBjBr7OrVRiUXeDprLVgG0OoE81GG
CBQ8jYSldoFtf9k+bWrg0zyTTPe0JoAz+KQpgFhJ37BlqnbICJng2nNKwJVhzBVeJY90VyoDK+w8
LT4hhC6CrUXQJIYKRmmA+ZYrjZ7d6H89J1bD57tKAcuWWHG4OfutiBJ4yy5sjza/14LARzwD6ioi
aZe0Qsb3tTy52Qac5Ez7vr0Xc9Ce8pPoJLR7JWy38e556wTZwFF9E0kZrgYnav8LRLsbL57VBMED
+wQ2NgdmAmYYgFLeYg0zm5g4OVcZj681L7yFKWXYS2vRhcCN9GlfVzavQDwAlLwr7htEaEtIgU9b
ixd3MaMhAufj/l88bLOFiiOj6d7GRKG5mBpGFTL2dSczfnfdZQ1nl3kNXCejUMchvnOC48PaCJOh
hCIIFKSJJVrArs85UqTwcxbdaI0eFxVL1zYGCtlJ7wuMoKZVrLZFza7+JyswC11m6f1BwWDmTXW/
+vZUlQ8ACbYPEoVWjG//SHR/uFaqb2mWOGWp4fzkQJKWvyvcgb2P97Xj0hxUdPKP9MazoBb/xQQT
2/2JiiQzrCqU0wtU1fwNUtnEQWlz+J31DAXYt+XeEFgetSdLDPXruF6+dku6XxD9W6zRiplcvWbq
LVUww48h984RyrSDa9U6DNfURZ5TaP1OjDbJ765MQPtdor7rt8fgt7VSuqKd50pK8z3l7rb+deQB
yx9lCXJL/JkUZEJrWyb5ypo5Ioy3zjZHKTn6+U3/Ywbn9uYBX6U/2MZSliDJ8PlKiJ01jy1tyIp+
b/Cbpr+k+1TfU+/AmPwUaolrV5T7T7WGXyn4QkWJeSwrx7vFoynAQs8d9YILTclXsg28EQtr8X9l
Pwt01r+jVR2IQQke3UMWBOuXMUW1rOtkGn4LunUwr5UqDafhBQDNhvhwAGxpmZCCtaKzONldNOqe
CvYXfDBZxIKz7UHEaTxNK7pDlrig4DSklPr+6ELRPRcFcXQvpEqj8JKjYIvKwMovfD9leUVjED9U
zGSw0ufWjKsUXeY1yn8k0Qt3ga/mnA8k5hi7Bh8Fu6K+jhGTsxbXkwgLoZo39ZPlJsnXcv2lodje
E+2qynvYJLF2Sa/mpQkUNj5JQj16iYU2mSmBQk0YnlE07ZnUSbTylxsG9vWJSS6v34CUTFVuzofL
KhFG1HWWtLQmGJbrkMoWzcKk96L044UcQYi8DwTzarw8xb03vtWRew1/A/1N9me3wZOS1kB2oj/G
FV27Vg/uORRjk5M85BN0TUGTROdh4iXQgCBGyjafrqn/m/vMktKlN4PUb0pS80gCHMF7HvnCnlFg
d0D8nK6DoJH+bAeRSOlYqv4twurxOJ0/QEBCEbnSf9jS+hzj2hsBVIFCrdkBXFnqgojcFsEKtxq1
qfjaRzc/3L8yyT/LTIvPAczxvvc88NNH0Gu9/Zk8q0Lfwc96+djVoOcPhN5ePuVYIR6d7Piv4p9w
g3LX6iMeWjWzdnWbtXpjyJH5JA4e+GRKRHrm74/cmpF+nx3BCgqESe0Fnc6JSbgUB15FRnjKSXxH
dcoeheKY1s4gmB1SWErNxINjbn6A3GEMw2KD87FOKA1dj+708+lVnwL84GLpphchZpy1fi6QPPwP
aJLAVaEG1l/QeZDdEDB3ZPmq1uxGJ+6dn1XaeiiSVanQ9Z+est0pohi/BRQQsT2vzzO9aL5axxlc
Vq2VMHzKGBOlwIbQmnqiVTMClPSUWYcFey5vNnXyg1Gn8bR154LEDmhHWhjmEv6ci4owDFfKgFaW
0ZaK3kOR/BBREb9Ayw7Ochftu1MwvIP/248eNy23n+NXL5vtoIHpronHzPXS6Age/RvlzB7KO/lB
YHhkJvt9jlzvgUJZx5Q6YQv+pH77f3HB5sG69ZK0n9EZx2s2Q7hAb3ZjJS1VW/fo3Fnyb3+PfRMW
SbXJffNH6kNZJCkV+b1aicQfUOPIofa0FQF7rZEGZLQIi8v6jtMhZj9vO/Aj8PYkH60B5EVHt8qg
D0zUSBck4WRTWM5ipXHRFGhUMISTuYKGz+l3s1mgCtFS9kZAuTT4nBRigSAyUMCHR6wrKg4UKB4P
DIZDTtKeyMnGohGMLPozsLmbCdUWW8LKZ93l/RgetLRaeUw+AuZLDIcRiB8Md6pemKMxTNxkft4l
UVmZqTsuDS4KZIik/2ypAxZvnccmMMPB2polp1oYljyPzohCocNqll/MZMtBB7TkOSaKSUecp9bz
c4+iN46DKTwCmegA3n9yw9jLKGq2Li8nAxvwHXlj/jwygpSSOM73JQUXxcMy7Dm+HJqTUilzecoS
ycUz6zmRELaEyatlGdOS0aX24Dx0nwC39fSYaghXGZOf+yZp0Gmg35IXapeR/miHPkgmC6OYuzBG
LriGIAVK5Q//2Lf2iskUGOhAsXWlJgfnEFpzBur9Lhk9a8YaRWrpPPCJ1Zkk1d4wyAC4gmC1wW4w
ixUKrMd4t8qMQLvlCu2feE411YfOKWFgcHSQVKkmmqeXgY+XTV0y58bjQChcUJ4dY7LGShoDGPie
laaOFQJaNI5rrWJb1s5yWnuKEADqGzqs5/Ghe+R0/kkgzY/DJHvUIWsYOhPqTL+Fa/S3SNzQsMkz
4h7doDI5pu03S6MoYL++HpSmh8wtlyO5nhOYrlRMoz9IggV1ff8BA3P6Cn3+KWadaDIsNJf+LuAO
qjHR18PwC+ejS+YymRjdk24eSJnWplr1MhoXQXdXX/5dFWBdCflT7vgYbS1rLRSNLYhat2dmJaKT
VrNgr6ITUIpqJWOK4TubGpCSJM/2CqcRDHO+0CFO5atc0AtrbLhuQIx9O3I7IfSw7DeEF+VKIpBi
TC/202s0EZltOsifuagDr2dKemKgqMdJS+3pm9fyscKitIpVxS8rib3qvf4elPMDcj9H3gr+fcCa
K4jfxAZMyy81ST5ZChN41wOwt41k5XzpuvQz/mcfe/KTguYXWs3G2hiF7X172v1rPwpHILTdwRU7
2TJpIpb/lWZbrCP7ejUDaNFPn1iH9HAzloBOemTyzkTripJQw1WD10EI2va6aqPSFg50CkxIc7Rl
R60G2eqhsGZaNDJMMFbXlGGx50eqNuM9DS5bhBkwWZe+Q+0s5Rmfh0WAGgC2EdTI13UJWHm6GHDl
pnecBBOHtRMiOfyJCMTuLEwYOorA2udlzyZzQhMJAhulJFdzju62/kv1wZzYh7Q5An2nOAkJecAS
QjrP/HIExAbwAmrO2Z9ZkmFsLMM4hgl4m328GdKXJGcd/rtcvrqM8NvmtbkLXTdAu22EHOOD+1LQ
9MJpTHXDocS7cR6IXKQoAGOnzNAamz1XHH50IGdZyPrfB+f+n3BLMDbGs1vjjByU1TZSPQpB7e8E
xfU9Wa36UgenxAHwotZuNUcrw0BatsV9HjKo+qftxSxmNigQLyqP6RcAga0WwNt7VTmlH7IARrdX
5PnI0gwUltV4n1iKGyEVzvmZqsJgnvjrk6S/KNexhObSakzIfIAz5ususvnYYv266qyO8i4JcPGs
49Z2fQMGFjaAnS4dJ/dwdgbZWaq9ASdSubewmbA2g3vhaAQR1AXv6udGyDwjw1ynto7qO83x99OE
jvdmWPI+yyjP0BP2we+mx2Z8bDaAjiicFivNuUZf1ZoLXhZNpPevo8pcACzHdUmGRtZIKZQUJkey
+UA/6w0Dh910Ib+oJ6xymQGUQc26Ejji+RomqBcC9Vhmld7TmaoM7nnRibf/o1CtoQn/xF0rucxa
INfW9SZkcY4KndgMm04ptLguhixYnDx+4yZlm5LubOXo1KVm9SMmtOpc+QnLHsBSXv57ZLlB15sA
cvjn5P9aouOVUfSFmPpXihY1qDgA1YWwvme/VCy3J0CJ1TJIHqN8fXISSDPoO47u+gfVArMDp6Gi
KheK0uTvuY0It3R6W7xWrUIoE4LxKBWfVcA8DmBdt1lB4RtNr6adkSh46nGFjSCQQALG2QsdhjRA
xi8Ew/yUIlaNQoKmY4AAIKo0GraujCBSwnOPnDVkXnxYjnvWy53Dnu2Nelg3X3UVuxi9BhNwLVCK
WzUXz1U/Cqd86/U3CREWixsDaCYG2PIHGpw4I3Izcab+tnIPKvHPnruGumY2pgA/irBG+w4bjIqV
YZ77p3Vb2cw8dAA0K+Db0K/SniQ+H8g/D94tQEPOvCH8UdghGKzDosaSzuDVQTWYgqpeJ1IcI6zn
J844seaRnWammtVJ5ljxL+lbHJXAwvfO8uuv/yuc/+fqQ9cv5tkJ4D1gY76nqwLEFDefRTuIrtoo
HGioJ0VvMyBaCLzdtb7pFQwStLhABtWTV6eOgT3Yq3Af3kzdcL52zyCmckmqz5F7cKYTrpaoH9iw
kw1PZEOQ+75zeoj0vkVA1CI49mHpkAP43VoOBFDDbFJIo3NrOPBM4SlI0EluNe4b5QZaIx2JDrnr
2pbtVsIHpJhO27TP1PPBm4NP2TWFAesjHha5NVioNjXBJeRISEDSBDbOiMVZ7kb/n6znn18FNtlR
wl6Fz8gpVxMzJMdCLRaXgfraRcVhgHtPdw6aOLHVe0TeaBs956EaekSTC7ErkoYIatbFEW1iLEUB
d8pbknjE6m9XImRG6ewJJP4buhaQgIqeQ/lM5yeFbjaVn0e3cPyOIPKoRlTvgeUGZt1t1eQ0ebA9
DY+Fzkw9u1wEH6xmACe23Kh1JbfVDBUfmd+r5WFF6hypkoo/NYB2LpBo69GC1ZKYT8sY+pLgSSMM
RfD+HCFp5TQEFwalaghJ7Efx81ZVnBEIKOZpLC0mB7zfbd/iNkqxmAk1v0AcNVyow9JuIotV6Mn3
eMCGOl0zEHKf3NS1l7viD87y3FowXYjdYIFueJKoztNtKbhD+yhkBVfsWr5oF7sSriiQuSh/7Eto
Hq3Dp2mkCqzsGLjuz1gi6HIQjWY0VRYKSpBH6zZ5o4HUSS5EAl8WaPbVp0IUpMvDDA2GHn+Jqor0
q+19OXAZuzv3SHpfGj4BnfmJzv52coouKH2Q4vy1fWieZhJEaqDXRPMe4ZqghPq1BtheyJR7owwB
suaAUYAATF6+5IRq+Zuicy0k/iTSdZnyeDkW3C/Gm6/TmusD22vQGMEaRy36xc0LMyn8rgX91zKq
YJtVLSRoNjb5RyIVNfgpxQSl8DODhzcIzdsHIMGEUn9n1frESyvS17H7cAnZB46tW2wocZ66F2KE
jiQv4sODe/UDffAu80GGDAxETaJ3ejp9OSBFtiCqZ6dX89jOHPIZHqwNTGg2bYGBUWZVlIKYujMR
XhrjSP8RFTZCUhQDkTq0gwvRYwfUlcWstgSxr7q8AWmQk+QZ8OOWZHU/5++u5ThDzUp9htu6ELKh
Nsm7yZpk3mDywYogs/w/Skgt/LEJmdE8go9zPh4BEi/CPiSlv6vLa3hMsg3dStI6iBpT4bdc9b0N
zQH4eaDFU0VpoZKZ/mkP4J4oocqQv0TgG+mteFE3eEYEHwPuuMfF2qBvStOphqtHkZBDkd9bxDbe
kSsSQ5PAX+DZ+BXQ356DAtopPZWXnpNeKIuqFG5xsoGCHt2TdMrKMeIHbV2gNemokbl5DtTCVRzX
j5gxL0YYGaf1KzFdDR68Xsd1kgFhArXMp44AJ/Q3+MEIW0wGu3/2nhKJp53hhS0Uj5M1LOVPpPVq
HTX3+zUnfsaUWfTaAbTZHIo8w2Jru5N4Axavg4RDhaJ1ttu1QcZZvqimXLahqi8jSa6fdKJZpDjz
Me7LNKsrkwNm1+YBZb6KDPDJY3/WyTz/HHe6LCq7M6IvCYAX97kcFGtNBxogRMqKyxwyyyPm4cpl
8lIf9B09KqjUaaIYNvPWv87Ef9Y5pd9HriVwSaxoiqpI+OhsMQ78tzCZzl9BPNaA7U9VwcFPjxmK
UNL1ttu/kDgFydsBifGvX1nKfhDkJ9uKwGP0mIis/Vx23AetF2hrkLm+lFE5CRTXZ3Er9haCflgf
rgJiyHGl3RnsORNzjz4fQ7mCTYwSSyRUTm1/u81Gcj7AepgNjWzmvNodE9TtiAzpk7xchNvEcKeq
KLT/Iscl8NRwasrVAryYayL2nnJ8+UYgBaPM3t9G8/SiruLXnP7wHCWmpZqcoWwJ3vl8eKq2qADa
DFYKk2Y+4OMc0O5S/sa2VEn3fcXgVscsawxG5ce4j7lx/TxZe8lARyQPuAG37qRHQGKk9A7SQH8F
caborgyIU8oUJ1fdynggO0NR3c/H2LAx9ImiHw+57AZf6YwdZ0OpN4YbNis0Vy/gLQsaZLXpA+xf
cnlnHQs0/MTwbIMaHI3x3B2bM8yK6ziKb4faPabZKcm3+jXTMUmz/w6S7of9vcrnwsoLVhLH0Nqq
h6vINtW1hyhob7eDWzhM/roj5M8A+rAAje8E4/KJx/Lm42ilchan6u1F0czx+A3vUU26NsYeaODL
hT9UTde7QH70ajIm+L3S7DfFYug7SbBVoeZk/3qh7KSB+1T4C7mFXjsZHsYQmc6ZYkPVTDAC7GLc
sBHlvlIibCkU1geEfsM7dsxz7LR5dYe723WqgsBuO+w2slxbd/7AOiF3O8rEbLbaiLyqZcIxWmpY
vwsJL/L6vq/AkREtCSIu28yktO8fonF7OEjtwt3XHoFegE4ZbxW3gEkdAQlN7VFQYMJkx5yxJvTd
9FHYAqAyJV/2z5pqWUSfB5aOQd3uAtuvwP0aUQMVUnDbJXyBgM/7je34PStq6H0QHAcMjiSgeplT
ozgtOhJ+eCTqRGniLsfLqp9thN5CWprGRPbrRKvKhxCtzORDywdIiyoyp89y435Km4fs0PoZGAII
OAkW/xEn/+a+ajVRFhhKlyHnOF8Dxpj/5s0grf3MslCc92Aby385nHG804+6n+Q0XFngL8333DvN
kvOCotAfN8TMB7YJ4vsoeDrqxYxBQzjGZzjSfuRa1a3qWg4efkgzhgaR3whTyHoAOxntoZOJl6Jm
PGiZDjNqRQ8/gZcbtBCJHP38jhw1w+mp9/mohWVOXp21TRLckLI6SBex6r0F0GDabAbG6gjzOA5F
vELy4qQc6TpjByCoc94aaOFei5p9HPM2jqtBwA+xsu03PWtZl8k52RAWiHWxFl4leKbaPdqw2OMu
qS1z1cy2hbVjjRGhm+2QVLGsp3xVPmUSvkcy9Oiz/LfLuCrBj50Xtz87H/kGUxcfbLAGpGS//4R3
HFXFNdRt3x+g/7sr2CXosTaPT+hFbk+b4r5Mn+ksCw1y4hWHbMFoPHRihKfFS3tppIDNB2mne56a
8bVOA1YwwBcja7KQW04Ne8gCOvCtdoMKa7wtaEYn1dMZ/E39DZpza2NT2QQtkQX3wh+60PxiOUzD
XHm1gVxx7nYyZumF/UcCFyav6cIx/1uzdHzk7hKpqkECVDFlaO7pbTQwaZ2pjL5U5sqxgfQ6ytB0
qv4vxKFPFy52fbY4VT6WRsk6iD1Vxf9F+tA6hQEbmxoHTGajEZPTuIsmdMXsHACDfMybIDAyv6RR
/tTEm7X2wPKoXHV7AFNCOiw+QHnnuPypKX3CplWmhL6PdnsY+ttWoNg7CetfeOA+s6v3WFa+xVe5
3e/C6JlqSpWNO8EXIXa3DtQ286QwVxxKS0wNtGimddXFva4S/JLhU0mlvzVribioIuhTMm38X9L4
t77tXjw/j8DKLkk5y5UjFb7+iQ3e2BC5L8g/BvVsuC0xKzbMLobEGZec6mxN//dWxO4fuqfqOGHo
95JKGBX+6iWvYkc5Xrbh9tsxQq2OQ05mdwzBEeNorIaz8MDdfrg4Zdlh4wCJJW5J86OTiYey8LS7
KOvitmudaz0/wyXbrkNTBWBjL7AgL8A1c6cLQo4CgiP1n28lB0tgbwDuJqRDi8aV6j6vli7vde8l
kst5E/4AX3sOOW6KXj9VKwKBoWmrDrJox9DHNcDwm8NxFoOfVsmCTzHEpMh+vgp0PuaMdOB52JVb
gImD+0E5VuUalvmyLL8AYXTEDz4NoA9eLdSOEjjYqo0T6VBWQFpWa0mZkcdUcYysGX3r8CC2JRKN
8t8/AgsjUQhhxD0Qlfa1eazRRIuibALGxYgc8lkBiYGFyNxDnUIswnIpFRig/E9RJIWl+d+W43wk
9KFFJs9ZoARS9unMR57xaRav8Pq3hnHob0hs22DJvT5JgXAYxzgqQc8/OPSlLWyTbAlwyX5HD7Wu
hr4YhLc/zq21+yePbrHymCwDxU7JEBIONICSmWcurjZWXKUtdME9Vzk3ufbUa8NWzDp+EIDEXHQU
B/XwTx2nzB2ihHCukH7Wg5fW5Cjc6wdSeKWxEN1jlJXAIkxkYZ9wXhScKYEU87M2EYWup/qMXznm
GaQ7aMdiP+I31UwrTGJPgjymqukxyWTxMwUn9TY+AyWBAcoi4UkNIWqfnKkIhcJWilFooTxjWa+9
y0jbyZce4U7R+r743lw04amkUNmkahnxPebh5A9RQ1OaY8cVEoZfPjPpus9Yt0wcS2fkClhi3lS4
V4CQEW05CM+As+KQVyuSK01bBdKAm3jawI9hseISJMTOhxNsdJwM+UWkqmP6nKhQv/g6syD6iZHy
71vC/aCg5e0JPw0ZLP310UY8J1o4SvhuE86TOc5WzexmotbMYTarXaAC+CG5GaxX3CN9dRUqag3p
ON0FHbKJRaTnPlBc+rVj4Gg1y3UnMqW+DKS+fEm5ulQvsAOEB0dph4swN+PFSmG4nsODA6kv0hoh
vLqgmEtaGL7F3QdMYX1OXmpvd2lXDs/tgGU526mxEYvbRhrHiWpMYaxxDqxBgdGiALYhhiv28iV0
oLHmk8hHmK7zGyBhj9WfzKEuEeq/5h7QXJGIbpLONM8GXHIrYQf5J2YJ17ZWW8zKSeP3zkZdIseS
ZVSfRR5kj/mKoec9OJWIeIJSQF/D52+ALtnMnhN7OqQjM+aPhdiVUOI+XgGWtoMa7mR0Rry+kc/7
voX6t349EHHeKb/+ctwncJjkUhSKH4IDEr/8BYDBz4VtrRFoGbSYcs48UrwDpI3ShozRVPmaGxXF
2jYyhz4A0pNaqwQ8H+CqQYQcROkCymFYc0qH5WFdndolpIHCxEouX4EKd1P1XxSB7vC8Tp5umjwh
Ss73FcW4zsHT+CzfALyAu1NsSH0ZrySV9tO3kyHHt4s2y5p23Vzqm7N4qGUCV6RsudgrCL2AW/Cp
2x+0dg9Mu2FnAZuon6DtjAANxS8FUrqHK2vgxLi2AWxxW4kaZVWQGn67nzTOV8xQkLpmEcUty+V8
OHbW+UFloWoRWv8o128M21qmzFlfCszsfOVxwyFrBwj0Ep22wuntLc2v35xuo4hI7JykOXh5eTla
nGE3Ixk/K8WmE/V9J1iN/vC0zwo28bvYLejOi9DQ37g3tanNmwaTg+bKwVC908ncKosax6mw5Duq
phfTLjZetpaE/YR6/37LhxEwKvkv1oEqRXxgY+7rJ2rlV1LI62LDm7maRjdb2++NOaO9lB8W4Rir
47OGUy/55JViZY1l3fF/w4eJOTIxV0Yd1Q0NbSKJ7My7/YJzeZoBqIoS/cl+5rqAHk00t+nDafV+
C2uAFqdHGH/EKi59c+EWhtQ6bx64m6UNrBS8d/Km1ocGmZG35I35sOVcAbpey8XSAFMs8Ajaz8e6
jT+sk9mJvPGN25nX/IhA6sNJJtGIE6vi2vQ9aC9b+gx+roEH92TLTKBF8LlJ+b56JwuP7gyI5n1W
SgpfvEduOyVqTWjxBBe0/6QEko7thF35MJG7U3CR5cNxXqHHNvpWFjM27y/w8DBekrCIwglfOCKx
lspt05zEzJTvUUbyDQNzRDMGftE/o2wvquqN4h4tWjiBMNWJX62jo/FKOPPa639gGupBX3imZP9A
N9peKGlou+eF1+bgaR9NYz7vORk0zASx4ya5HurQZ4XtJcdtvkE2GZMiGLh0OSAxhhsFPxzLumcg
On5ueaQF8N4e/dz4Tor7CopOXmgTYYYYATDCTjhqLsxcQCSsdVssa2J66oxY6nXnO7d8rU2trz/z
yEJQbdjc/kcKJC/X5UdZzRGRr3E/84LDCKUHeNHo37tiXT09xU1Y7QvOfcnWewUkfTew2LWPClUM
GRnagE/4/cIG+mYPEOaXzKrQoBizy4HEpXBaACxhY/tqXwtoeKngWpHD1y9+Fq7D7bC0PDt2jloR
uFzt8K8jVsK5uETEwndCdpK+vyhj6N+bfQiAbnA8JyiFD2bWC8H/d3umNlTUnUEDgfhDxGUt712r
K5yvegJr1g/mcwMk8+CaPHM3Z5Nog7gk1mqCSRIExSKdJVF88CR+3WNyhiIbGFD2UNyZL443YFmP
YkTAh3bbMCe5l0xc7IlG7peP109fmqs1rglYnMx7/fX5BvXuSfXxGLWPTrO6LBodlIfRDNBMYug1
Xnuut2hcqNqSQDnWEnuGycoxizdg8uXYX8plMRVWkxPotcXTFCqc6UgOxbRbl97wRTCR75OfAFHM
YuyDWcs6vj+5qWFf+c0fnPBYlR8gFS2zS/KOlmXuqp9Z2U6yzvgCEC0EDIwHFFubYR6cA0jeKJMv
DPEtK4xhE1NpZKAtIZ9J8bH6su65tAaGWPCehq6xwV45veq2Lbw18W9JltTfOGlZuKW7UKQPsR2f
UXatCbALgS8Zvf7fwk4yhlEWYFzstP5ZZilrcte+hkNU0pemgzt+OlR2KJvEeWxFNVGvmn+xYCdy
a7fqSFBF1IJc3YLM7WsddaiEP7g71YGZn4ChxM+az4LH32TW31NdOXidnhQ4gzfmgi4GiyqBV3kn
BbJHkAhxotO3DNFZXutdazpBxKHZ/nA4DY3t30J3f/u8LE0pFhxRpWuFiKOn3P3Ez434RFbcDUkn
5tY3ucITZN037/gfhFdjl2HK7uOEJ5eKT2kikHJlB4Stt66A02KmABoy9iSGgXDHivRJbAanrhgm
uxgQ6JE6oioga8lKRpQ9Y1fX+jzChVKZpcybEk90dT86pfriMHKvtIGtDc/IBxHEOUJ+wZlFz9GX
SL0nE6JsWZmsUm3MKu+7gAFvhawMGjyAkaEJSqsJdcqOak9p4uzuHlMgtJH8WwgB/meub+EwTNBz
Ke/ts+5dwjW3Bx8LKLd9nfdbAx64l/wLs6ff5MUUczZ7rDRg8LXFykgovq7bgbVqVcwVnjx0ep4R
Oebx9cguxsdGeWVPCZ/MHJfoGYEcwgAug4MSZyjUOjga3/aM1olT68scRceqF/s0oCW06HSOafem
L/nqAx5xdt73GL3uW4vqmMAYq8ZqGLxZoJ+PXEcaLxL1HkAdwmLl6h9OiW+L3QlWhL4ZQvUfJJTo
mRHhK6Sh27LetmtnuqoJgQcUFnNazwS7Wj15ZsJMigsuA9sM/nZumDYOsYZZ5udCiShganqF0JkF
r/IpJccq4EUR4JPvnhpy3GylOwVwIOX0Q+ZZgOQEuQW2D0SXZ+jV7SFduuN5eQg0eRSSee2GIM4A
gfv2HQHU/ypPvIHQPsAENSrO/QXWI+t5Km6Btc30t6ky98ywJX/ZyhE1Zbr2lejGdTA++JvxgmG9
TrMBX7hzY1sy99xPYkdibVlTInxoAfjRke/npEy/JX7TMsvcC8dUls3JIOxnP5/XJ2EK8C1HDOJ0
iRj1obwPtkjfJa4uA837zm6IQms8Yk8dQrkdApkvCnyDt8JwmZKAfTitx9Vp6UZiDt1KOI68zGH5
Qa8V+cDv3AzQMIDKLEV+ZqJm9kcEpKorFuk8HeRuYNlJh7FsYeWgJOgmztXaVTx1CV3r5E6HvfSJ
TKSnMNXKkTtZfeYbMwomWi0DrnnJC+GsFfsfjy3iTgGMpA2aDNDxQ8mdnkz6szdf2SwgHHNQ65Ye
rZn/Sp0L195JgB5n2DTgw/VFN4k7WJOBRNL6BTTjwAYFhKI8uvbuw04QpQVsIYS1M94Z94NhIQYu
h4oiU5pl4UH7tP/ZR2qb0CyyT33Q6D+oUhK52487ftVqJVYpdIP4uROX1EHtzC+mEJp0nVDhL89j
gdXo2D+iibIYiVAqCFfvNXu22ekEAaLpcVhXEFKvMUC17KveSGq4859O7/MWl5fnBR4f1eMu0Uon
zTUsxMHYuf0sbWHbcGO07vzKnTYqASYXKr6tGXTYBjaYFmWk/6CLiK3D+l/iW2Y0FjBpqUGgELPv
vj4A1X3f/MrhrrNMqYaCwgEXwRkvfAfp4Rq+NYWMAvuBaCAiSqONaLR5/hl2CF+8Z6lRYs2LgaTG
TdFoC2zue1isaqJC7O27RWCp8Woj/rT/zmiDNJyyeAC/p6/WzB4jvrZz3rXMGKFpXvBCNpO8goaI
IYoVdzGesNUI+ey+HftsfOiJ6PSLLyYmP9Igf7R1rjjMr6Qw2jjRk5QYStzow/yTQo8f/BY3FN/f
LsMFeWIGy7cpDNI0DiIsRf233VfqdV01fqxUsDs1uMddOl1ZrQsA93rhWZY6QaX6cPK8I+jwp4a7
IOvZhoxbS/Xz62r3ngXpRvX0sWTxgCbVAnP6Va4iVsbf+n+Qi99Ck6PbMznLse0QF3VKDClxQIfa
UuCXly9ob3Vf4rQPuF1qXW2mtiVSWNAz8YQ2WrW9o+QTWB3NJ3I08vDYvfrMxu02EkJahVrq5jWm
mSI2zoVyYIoMVENy0OjAxagm00YTHN/yIYPjBUZAleZp0FsS4IR7ChM7G5IlUVYkBMlymyg1GqmC
B3NELkfMg/bXCSJAeP6Ps2YT+qCbAKPJwOzH2E+sjsHDJLSH1pjqUS+Nq572FYxjKr5TeaGCqHH2
69LhWpdIreX8ibW/VeTiwc1AoyomwImR7DTtiZqyXxTPN4xIKzEB/4PWijtdUmQvLzXemHt0zzTc
gvKbkZS+pNpzMH+d4SztMBOS69Ld18YcCnk95mY+TAaiby6luexW3ITa0imoPdm8TW3aIC2wx6uH
UeQ7nsbO+Vd+5mhQ2RttrrhABLaFjUl3yP3W8l2jhQsZgRPcI2LKXCE/1zr2yOyyETmVuK4a3Gn/
a8o6saNrnJXCKcI0GtfgkIrPsukIkHjkjnVzR97eDdwgEKFYx+ZowvcsA9n2gLczjSvdGEEaI/e4
2YqrDn+NXPv7uquaOgririJz0zOC6W83c5eABwfOUNQeyLrrxgm0vWVWrD1a8qzbyrXLPnrc/KVq
NedYPcAcuc/aocRXm/6Euw5+W+MEVZbcA6H/ol0JuxDed2Jxc8VDqA4Aa1z2fcULAIGLK5dMNU8V
dpI9b5R0xDFJk1ubUNQATqc9kH1Clx685tljCkyN+vRGRg18/z7/fYOIYhEoW1mPmFmlLG7hTNsp
q06DVgHNeCcfWXRcXfb+UjcQu1Fqbwd3AaA1+u6FffnlrcEgV308m5DShSh15ngm2ErED0ZLD+XD
HNExkdj3HXqQHKTu5X4GHJaDTTInAYSUs3+qYogdas7bNEExKCW5FXIzY3xKU0sL54A8Tl3l8t1x
/0gb7F26ixHzKodeycf9tIZavmdm+5pLYXRL0w0tazWPnd2MTwKgva46U2cjvPRyfBgcMflYSaxL
fFiCKx54690PwfP0WOdT+qkqWWLaUmSOkSYrn37saGgn1w+wCNsqlKhY3sR/4Q8NrN70O7cihjyV
YmsMH6E5xVRyQWb52wx0nWAuAeXonXEUyLOGoGeT3QN8g8qsX9ikF1gfy5WUS9OyYBkVfDptTANt
zjtMs/0v1hX79RPc6a0T2ry8MQeFOG5VEyO9sGWxJCUEC+XBjHd3qGKjw2BrSP046ltM9xhIY0XY
uBBC/Mgc3d3V+Q5uNI82V4ioyCcl2VD3zy5tYyGloU4RiZEZhDp+3XvrknICQnu05hz8G/WUrlXk
na38SbyC4FLj/TqqGZFV4VUM2Cu4h2/UfFhWjjW3JoG236Pmc9hNbPSQwwYnv4MdupB2pAtmHBzH
TfMYkbpDrRSMmt0WiWPR596UfX1NCpG5BFDrXoLpyA5xq4NI8oCDWSBEzfodQ9TvIJq1aBHqcEyS
bjT2uZrX//z80w//Sdl8ol4+MQukyn4YlTtAdCAQbSi3/2SgN4HcaLiDWh49cYW96+BzOaSZMEHd
Famq0ZtN9eM7nq5S3Yuo/EOu/ipz6VbsvXaz1bJMNc7ddTWSKgC7a6rE7pig1dfko7uz9TlMeEKH
ENRLDZMSFFqHDZhTkfrByd8oHBU/MQPsEFmk5rJdJNKhrX8s3+ENqWh+abDgLfCXZk50aM7AEAZM
KDn7R+CI2aywV6VnP4fEarp3bKTSPyK826//k+YE3xVr7wHvaCAAdfaj1SSyyzdD9WsQ4t33Ju6Y
y8pkjrYDLEfdAkzmnifGwvFUzhGn7/rZAG+mABxnTeqV7qEzFYgYQUv4/RZbINZ+ewg7o3ELngj8
3uMWdQzMHD6/CmelmVmp8Q9k0UEbZBTWSxMOXaMHn9G7GL7m2+rYKvTx8JhyXHAodJbrNsNTv/z4
S4y5elFinpyEmkSnoH9Yu2n/q5vVgmdeLxnieoCDU03xvK1k3KGNt65ifxpdVxB5rYGZe1k+cmMd
JY0oYlnMPLbLo/DzcxpLeCSHrv9k5DpIcQTH3Ot73IpgHDd6zjwFXmGL5RhovOSsMOOoGasvyE4U
6HHalcsucMuBqHVc+HD9eihgiBbg3mq/l2Gz0KOUryH13j7nasw/qC41/wheeaOber+HXwAKEbnK
Ffl9WWCCyR8cn6smUM+mPmI6/Vi8oZ+C4Bi0EVRfzdWJup+avgWWo8wukx0wZgIHy1YREskzZ/Pt
CuvqtG9or/pWO7Q4dNzYm5jXZZx3nvLuWzplJJZiYGM2hdDWJX2sG+NndDsZYOMLqhNqp92c/cnF
ymaxkmiXPrIGv3DSAE+fimzfekwHHwloN0I5/AU4BeNGsd63uihjm9y7SXDcQEDY7IOlTvhYB2Io
gRftKqBGffA/GavNZvLMWKf0xNtVh0Zw9iMaLlQtCHhMg0zrKX2Kl/wzi2km6Jl/hjGwjvQQJ5/5
FYoCS/VWpuvahNaob+3i3AZwd5sizwFqHkc+raB8hLGUliNRR0fBMrWQ4urmdsP1W4sqeDHLSA9v
3IwqRyZnBmDfOAlaYTJG0ScpYGajtN/PowrRgbSgy6kxDllh8tqH3DncoHlV27Im+nNwZJyui6tp
MfKXo0QRvZBaydtH5rYJKSNsN47fgTAnKHIQZ+TvfzlxTn/UeVZqbpAak9/zRkkhRYCXg0Or9W+z
Y1PoEeFmhRz81nEiXuyWwD12AYTxejjqri5ahr/2FnFnnjxoMCPtlbJsJksoBmBp8dJcbdck4aph
TJUbTjbLv4HWExYpbUhU1Bl3Zqm6LpO1gBgs36JP2Xr17eIU2U/yLMS2TrkL5nzvPrtoedggnXkd
ZtsNiWzvJaHJPwHftZCsw81L1au1Jk3DbLGlr3vPbqSn0OqUkKn+VkCKcbGKqhsNIOdfm3oY1ENF
LOT3VEx0Op3bbAl9Yi512aZ8/Rg6M3TIFSLssS5P6QmNCiC7bHpJUA3xPlPBaAGGOGwFXeY5PjIG
jJ52XSLhwtrvOAyEf5QG3FgvDxSh8lv0QoLU2UU6kMcY/QU/5GEfYhjE1k7tO7npOq5HFahcdci1
NcG1uxiGOLnaTRnYmtcQKlMlFwYHNPRiC/PkVR+ohzDMjswFuF7180doBS7+fUNYwNzB4C/CkOCs
lp7nNcg0Iu73iw6VWCv7OG8fYkhLZn0gS2WmjwZr63yU6x3HUnlB63CljB2XWBrUW+pF6c7YdstD
b6F+g2zdzxNqtu0PXD8U3Y+6v5AXf22a8QawqHSSXjw3oDnaq7nN/707KSZymbETyRO1oSd+nnVA
j1L/u5hsvc/RdJQIW5gL978wIxFm5WwCg/2yWrS1Lpvw3fhbMcsxxkhV5NTUPbD//YrIpTcoY100
80/9FTn26Tt88Bv+l3Vm74wRbyRTHu9RtQNOjY9Gizf/eoWqmMe7FIJobiqSkMsmfSnJBS9UxXiP
rrIOqSq3EmLdy99r9CofO4Q/dt7nR3lpC0Qj4OLkNZL4N6aL/Q1dyVakupent4FMtEtxyij0vcBY
unoC8gM6NPedY5VvoYggkcoOofZjvToDWQkK1DIIrwIf0Op+8jaKayNnkrO/F/b8+wp0yd1y6/+5
AqWhbrXiPuKqNppkQ2L0bnABSeu5NuM9+92mfnbcXdRROvj4Yuxxlv9Nof6wZr6riVFHQah4ucbT
wCbjE/MA7qZOn2+dVn0Puxj1YqhG/xdoeDMmAPdjbPnb4WlsmQm8xwcrNQGR5mRKAFSm6YjYgJ71
weiPU/ewsSDCZ+kC+aiBThqJR2M/jsUhsgEGD0jMXEt5f3YZDjmzzaPqtHqxqFgeyfZ1rEpR67rM
LHklRgu665i/Gy962K5Kbsqh7sW9fF7++vCLBCpSMQUqXny1Ipch5weKGmMs4vvUCiN4sqJVbmlr
+uxEe+9+anLfOaeTAfaJt6JBhU6cjPOh9TaVBNdWr8/GufVNMgBaOIU1xc9LHK/hDbzxXptCbTIl
peedQpRRD++sdTP4BMk0QSjU/mkXE7EGfPatMf432kcHAXf4lUYYDn9yHfz7sJys2hefDYf+7Mv0
tchWh6mO0Zzih1Sz71jsRh2HMWyq2VBfMABNaC7H1KON7JRTP2zXB40pszwgTq4MPVEn/aKy8cFm
qE2Odif0Z3wDtpB6IMOIg2fabxhJj7zsFLE96sCFNL7c+QI4w5pjyhrvDb6nf6hNNISq9UxS1beb
xUlYfSnl5XMza2DB1cMQ32vcA/Gk9EKZZSIB3DPz4Zts7L7XtzKg9nxNuW9sUI1NgmEtwMaH7Huy
FqaQSZpglIDbbIFHf/5IePvGjPSI9i95Jlyz3/4gxtKi+kxv8a0Ze97tT50Kzim7pCd8scAMGVE7
L9CT+A491h40zt6BKUYkHbGGj6mhDOsfGOnygXruYYKhRlfZRXwLv67LR2ueZlAUYpaljtgNBWvF
2UmAm4jSk1H4f2JZj9wy8jL/zp67+by3vYclmzx837aHX1ASg2TvxXWyTOtYCkI/3mcpsr90wFKB
LY5ZBwgRrlX63QRpC3qhe6Zd4GjtMguidf7zHMcCT+jSZnMiVsA1eCdmsQGLZjaR1/YxMLY55QUs
cC2M7W/CC5fj+ZMD64xcgYeGybSxOcq3iJzmG9ieDs+Fl+NV6HPbPuKULXT2oj91JWjBOiJCNFiW
94OrlgZLOOKXEM8pSsFO97mDDXMVHQmWvi6dBGZ7dgobwlHFiZEWJozWjl/yk/2n2iLvW58a/UMF
VLKCl2wcVT2PynLKhIv+Aiu5SULC2rqzteIZtaLD2yFXuMfYbTD61Rs9Zw6BF7UfHFcBr32/MfMK
xExw2x7tobEIzKKSzMDJ8JgLyRnCIU4FuMcGENQoTj0D8I1yFtHu/3mSogHa236nTB8Qsb4ILW3x
woGs/qgzhlmvTuxdL7CSUeU4k3m2kPEMnXnGreTTVBZkXDwvNU2w2j2RRefMA77ekyqC4HXWJByp
/cAiejG+LZBMrw5y1jsYQxF65aalVuSDPutMS1KhASpFX9diwkVDw8fylNl+LSdrKABU3IklsLb7
Alg9ufk5POIcZAHyFfznloc1voYAqSqYCHeJ51brmjnuscTS/ILjIooVJmGqQsMkzV+3Jp1z0EfO
afGPgoKC/uZO1ybcfWTXQhd/dZwTbIYeZ+C5adXAOSPHvm9m/jvAS8nFM2+xjBfel6nJcbtS0Qf6
hhsHzih4cV+Fl/Hnl5MwnFgr7xtyLFtOWxJPWF9JjTWkZ4Gh8rEqtjvoJY8kQxtSIGBTyw0C3rta
teiAumFfw4QlEi/CT3TcWWDRI0yZPFBdaABdY9qWNI48OO1l/FsU41S7day0WXepcc399fhc7naC
fjXH5xsHOSeRa+ql0Z6t4onLitozIzVFKae/YnWVZxsbpzHYvdpHOsec8FHp/b8kdazqzGHCiJWm
X9rNlCAbD5Wc3RX7j4BWtATG9d/tthh/WG+9FCZWjeGTXKvOEaI+n+TVJFoeU+z0Kg3nbaYqsbIW
oW0lSerOX41NwgXsB23pUVNPoY3q3SC7O8eEFvJ6Mafhmelj9t5kaASXfs/AQZ1sscN00rq2PNrd
xMFb9ebStyG78JCasr6SH4Z9k3lBLeOLJuEF+PBfjSCbw1IMUFFcv4VqbMIN+XpqIbP/6ZxGta0I
+7iDN+qlnEwHarOYii8zve8c2bh9fYH5by9/vzFo9WiDxCblMyMAqQChRyU17jQxCWWCdhLliIxp
Lc5e5y8FtV+A8xWY8wguvb3XyKRnvwYggtRVbaQa4/8Aezk3sY7l/48E992FHV6KGhLQRCP6fBG+
yGv6IWXSOATYzJHacQZJgY0NFyTN9iM5WX7jFFRyCYGcirbYvSS3UDhZEAT1cmAEiT264Y7iBRFR
/5kjaBZbfGBIvQw8BWj7TUaQfwDaxQxy1xNF/moCqzXDrjyuVtDXuDEMxM8l1SoYSloiiirNgbid
lQJyuOcn+ZwlmB/NQSbhUYQsVGxGJ9c8PloDn5maociKsHz8NDI4uitRg2ML11ukVRnD6W/m+Tza
3ZrELeY2/8C6lSm+VI3oPdvinLFJT5P6eKpScRBEnJE8Omb4vl0fw0mN8ysPNWhfHA7zH6p/sI+/
HaH/2pyftn5aLUEiAoEHfVo5cJLxYxhcfzfKyh0+vA7UhKyhsbGUIIs7XD7zJWYZduY2HQkiGpie
M0DoRSl63YWMOFsOCyAaUESEA2+JJfz2B4z5lCbGzh//nyEcyNsX/WyEG9+p2oa/FD8r3kWUfgI2
mFjYzNEGHoSwVtoReN5iAfdAT8wA8pa46024ZPy8sMt5iSW0ySM3/vWCZhqrHnDHnpULR+XetCVd
L3UCv+C2U1VO1ZDDOtT2pC633qpJsNkKsQfO3txQEkwChF5mSWKiktFJGi2R0m8D1MEVP6Rd7D7G
/YChELObrfZKaYCyjm7DEsa5bGKOeOy4CFaN4i3VpDnNC9NDt/VqmZMIucba6Ed6/0/m8i44fhOd
R6N0oWA+TnT0LaqdOsDD/wRLQR5uX94TKgqSf0GjPz2JTPBXHcHF6UvGZD+zSsL95OfXX3pVNVtN
YIPtlRruI7SgBdMctL6tANaDBsq6bo0x77nceowA3m9FiK806KYCU4zcODa2aAm+j+uPqRx6CWPB
oaPyRb/RoOYaS+8oJAIiQXoF2scTscgo3INWnmFkERk6fEEVlxkdZJDksZ1AewHPwgZy3NuDkdsM
KG6EAzAt9iJa8AggDOXhciyB141Fko+9rFIEbN2r4aiDmhMOiOH+iRwULhlt5QaqIfa6WZiKPp/9
zrx55bVJiePNmRycL0YlqYGrWCAFxfN0U41CApQ3iFn7t/vIg4nVl/kyde+6CwwGqmvPqJKIteQ8
mOXoA7MNoT71VwojpVj7As1fdWRwzNzdLf1kfcXN4Gabj0Mbc5cJdNtyav+dClHHBGZKm9g838Lb
lYPLX+ROq47quE3zxqIN+/QP+lMX1QXMKlGOYkWvTIAWBnG9oB9XDrZ7JC6qPHTZCcOJJUR2VXrj
s6XMejt04coCuWgGzHQc1rDVhAAgoelcEdutRaPEb7NYKhAtbLD2gd7cLoX6jFLgzmHbnpdzdy7N
jxJlz3a/VCN8f9B8TDsmd0r1Bh0X+fUixujhKa8EQQNhdW60TnU85lnFSrrF41U1xh/XofjWa5nQ
BEoLSG7H2A1KBDlCOH7aR2zT7GGuWr1YytK1CxzDo5XGwDgKLDON5gBi4TQkNfMqca1tAo9YMwSa
YXqJ/mEBiaeuugRpXtY5VCXIbLg0lMb3MK40di8MLC/mQl99V3ZCgyECcAKyka4uqZOKRsngxzmT
R+tz+AKEQ0CtuAYZxRnJHbYELVeWbBvJdgvS00yvmXOM8Qr9+sngCQlsC+p7s33sX40i8d2SBcal
DZdiq+DkMDJFP+dlmF4K/1Oet/noUFttV2YcXeTgBPO7TVlArsmfsGvNO4p7S7cR0MtTlIRLFjOf
8ktyfxn/xpyBuFmipLfJRsHUdfWuofIwzIlHKAaCV30djr2fXDX6KX0z+8zHqYI1r8fI+ZH1VhrC
n0cZagrOZLws8nGhZ84dJ0fBiFmGd+NjpPFTleyxEyeZoSXCr26oQQ3cgnICZRxqQWv5hP7yTK26
jro/owvm3FJQPNt/YB1AsciQ/cEkM1yJbyR7/u3V2vkEEPrqqTTJTTXGVmxB2N/1otaZpu6z1DWR
uw+kwMm/y0KOi/nftwapEt1GAr9f7RaPBnFl+UGQmUQ8wzcPx4aX+6+sYLAcqtP/my+atjvb8B9R
alCpdnTt2gaQnAJh3LN3Ds3gSllQkU4Csfi2Q+3dSsj3fUELk+uAQp52nODws84sD8bfl8CpDJ30
hEZTIid8dZGvBqx91G39Ra5cYZwJLYqFABjh/FAxL60bF2uNps2zO0r6CF9IeT+Ty0vtHsIwcKJr
7fw5dzwIYQdBwspNMcWAMBS1JQvRgbV1oSd4e+QN/75sSRnzSWN1cKKet5M1E+B8fIQDK41aXyYE
en9DlsJ760j10cONDhtnDtKvXzC/fd2wWj9G7M/2jVwgpWBrwJxn484ntCNO7SyyHQAEgHug4VGG
y9r9fOy8B1VQWS1wezBai4vcDmKqnu9O/ROOlQ3mPnmhWfJCTGbC/fMOzEpNUI07KKvkcu8xQh5K
BQXNR7Q2CQ+ZbDzgvnaYvi5WCcYc4+aqgLeu7xJuCaE5NNSqBG9dm8fF2lE2L2hNvqu/CqObeU1C
uAkJpwrO4kHBP++CQ7pYcdJqvsDydcpe2VrYYyXXMBehTtvaGx5nPNu/6DRFyDaZ4DBTxEOuHdLD
YS/59SX43DpAAPJ0D+VRdexElt3G17XdBGzDEWgJI4cHLU+O6IrkAk1TMQbrGccFRbIAYGICcAAK
5GxVkwCuSenQHghGDwBZCn/ACF0ifkhW16Yhhu5+rCgt62mLU0vfA2TEcxiwLESp8p+GkYfny3Ay
Of468xOfNSxSRSQQf0XHCRAecgMG6uDGSd2oTCTDRujf4ZWlyXD+KragzOF8oFoYYOgK7w5un/+J
0xOr5iWeDE1KLQMilbBDV+VVAS2QFlYREh0+hsIVULACLMgI2sMT13KOamsB7gZc/jLXPhftKqUP
+x9pnGWnCB7o3NZmFoP4tD/x8uJKkGmQ56AqPiHG9bAGQwC6EFKRzUI9H533wV2BqptyF+XsNrDR
DE1Ve+XjSn9zFadX1PsKpK3UVRoAhwpEDGIaaSCOV/Jg1loC+/21DoICFHWF9enOxZbwgM7NVnL8
4d1PX6PmA+oQ6C5+mLa0QxJ91SCaByq96Xx+SzqaWEyHxAaHoOsvs0PZFca3yhgPLc+52COBJZyA
lwAIfAhvqk+zajUkOSKVA0QMIlcly1Wdej4S7IM8Zn9JDYEXRzQATFSmrmqeSjAvUv/nCEogtCST
2CutpXXgbRDSdgYNJkpZRhy1xaKykPvMo12sUanFn2JZyd54Lu3/2Nff7d3H1Hhyes0/UQ1FO2Ey
9iqCcd2X6rfBIGgdx/H1/y+XrsBiE6znBjpIwQQbJ7JV/J5VTs4qZzSRfXhDC9GPwqX6uBEKsF19
wcr/1JoxaZm+vE4jc/2Mk6/d6HC1gQJAg4knnehoyLQZF7mPoNvC5Li/RsfWsCbhlB9WMNm/5U9Q
YCwGpAVCZjM3Ce++Ekds+1G0Fh3plQTwi9KaXJqQlimtHe00eaY5jmXrjgqx74giWQ5R6408t66g
nqGIuxy3VjPwVHXi4b50dS6uxJcbn6fgT1eMuSaOHhOzAlTz0nE/au6jeOeHWCr3xRmWEjC7rcxT
yjH205SYq/KdsFkzk3djJYz4HscnfuvFiocztJL8cDaeVL2JXOT5lnUeLaTg0FjKksS3X2LHKU+K
r0vvQNyYBHPYPemhsUVBTPXx4GU9FO8J6F7sqyNY/G+DDllRK0XRF+hToMg90gQDI5lf7rDPi8Th
ZNP3Lj8raEJz2MMO9mdlLfwsc9T7wdLjwQ2FxAFpThM9FQJZNBMJdpL0BX9dKSkT/MohLMmam+Kg
i5XPcGway4KyHbtvCPkCKOCu4wCt9weGKx8eG4P+ls6UHTKRYnc6bUV22tm39PDC1v3z/LLnIMiu
3ZD6ofd3tHF+SkXYahDQsOCvQ9gHh8PkTwykwZ3OLitda/HCIvAnli7xN5HJVYOfT1JFX6bNrCTe
/gX4FPiocqLvncTEmIZppxfcox2TnRmKI0ZpMMilcJZ9Cy66H/kQ+ep74DtknL9xVXezVDIbyrrb
9q93ZxHU3gut7jVDpyvTPdZo1psGuCkE26QL9hhvAApKeuFXrsw+IfloFTYAtqttMJaqWfHKooSB
81kv1tC9CY2e789dZixHJMhM5zC7UpQW5NMZTnGhdNUx/CZZbl0qHO8djfwiZ0q9oBWbokxN/VBm
Esay8DMjXW6qB4SGg417e6Kbz1qkfeBp0RPDxfzxXEeuo9CXXdARoRBpZd4Svr9KsBE7MoVCqApW
JZfp4RWJTvxJh14ElT/dUX0DvFZLbddhIuHLxzQFnpNWxQaDJaWF9W1GVrrcVOeRj1yCHpZ1liKw
dJSesoqdR++Zw6pWItwafdS9ew6TGenLIiUZ2e7xUfB56atjRmwnmH96iWb1UF8H+5qjt8T77/Sq
Csw60sv8HIy9vzI2NcUUH0Ez3+WLQdDip09ApoZnfDL/cqUosd5l8hgZU+2pM/5yzRglrd5nlG3X
pH8iOpgFpz8yqTsRGOEJp6r8KKWqhGirk6GXiR81ZxhgbwZyZFm/FbkYQbgcOBMDTFuiOBkQpTOY
rkcrRubIysbq5k1ziUwFZe3PIMeUrgRkU006AoImPmXYQ5OTWyDcHAxloGXH4bk8yttx8RlVJKE5
wU+NILOk0xV4teOj69brvc8ankq/rktMXieEu9TY+ZibqEafPtpViDzOAxKu+kJqLXkNon72suTO
DA7k7Mll7rTYdFCVuyxwd1Hw/IGyA3FfIXRLYaUDIagfs8/zFUsZv9EzFC9LsnFQ0WFT5PRkpXRe
mMOE8dhVwfBxgQny3RIDLHHQWmak1S18SScBgXaI7q2p+tHJUm5aY0ij3RyUmZFkylfJSyTFyuwP
ji7lC+jw7+dE39FeKoWNrKWMZWMkY+fGrcD7M/A8cGpcc/Nqa085FXUXmHPeyOa22HuoTRXO6HsF
hv8UDB6kyzYWOQhyPQ02JBgdQ7Svc5xpEcE6xSWJUzA/DI0MudsfB1r+DQIfiSgPXFStT8TBcJZ/
r+V58iiHw9x7pBCBDcklMqb7Z55BsWHt93Rp5TZL/oMQ8BB0ZgszA2DVrfNVcdbfoKZVh4813Wwx
QB6rkyEOSAHZqojXYG5YvB9dvd0LRahX50CjUOsYAQtVFnQNroopZ/Z5ZLHcnoFaGo0oB2hIlc9e
R1BWDCKFjQTPjhyMr+m7VnpAuhFVwxK9Z861QOtTPoRTJSe2FKQU38xOLJJnk1BOAMlgp5q7rXjv
YjzeisMmL4ovsKjf6m+MPE09739R/eZdKb9XrRECuYl++TPhc7Df4f4aPd0rYOEqgfJ6z8X+d1NY
DdURxdlgFJDvu8gTGmQMy1s+9/QjgLsdHTb0Q51nJ5U8mSK2U4Xin8HLVUjDDBu4PsB63Ny4W2LM
GVQ98NkvCKoODitgLBFDMWu5XljDsfBPyRKW3McrpPAz8QmA7NCa4+IAu+KQs3R6zSaeIPWtcAIB
LtXwHiLt+VZLXbSbNocDHIl/H6U14kSI94rcM6ZiAh/eTRHBzJ/bpCub9EaK9DK106SXakB/d+to
sYKMVS90nfAJDZ1CuEPbmjaeCsZKGSSjmO2H9IsPDnlwt2gXUXttULFJ3xNA+8IRhLfZIvpzU0I+
4lCZtRvPnm3PAOdI57wgKGZuuazevFDIvJaf9TiwPZ6uSldY4uMowoJcHdMeU3TynuV3g/5W/j2D
4ruvK1JQ6UKUXeM6yXnrx5NaE/keKTj9yxS429ThVq0e/79LRE6L342MBaHKd7FdH887ytTgsXeH
N50RV2C/XOXHvjeQ5+rF1gdELp2GJV+Atb88DLE2yFyePxoIw9vvE4xw3EUVv1FdysnxBV5DHO9w
0oit8i2YEd6uvOrJzVmwJ8O3Dq9swznaFKrE8Okeheex3U5mA7Hxl0izo/sA/7gE/GJKnELyXUGZ
Dh0PcBj27oJ/OsUM5nK40OVkrTPNw4P2VFjv7GLPmA6I3JJP5atawzNSHaN4lS5zhxBzzynpeNnj
jZY6rYT5cc4QgOcEqpHOqB5eInGrrmDoEqhXnsNryzNyZWMCGbcjOLLrlvRyM5vfZxZJT0bnFIdH
II+lCB3wKg0+5hz+Jmu/B96xX5nPqk771ofeiopvgQgKRN9H9wc5atLn03K/9/8qytgQcDw2lExu
4vOdJ8muNF/XTJvyL+VW5Nps8L0Ngydp2CiSY8q8DitQb/9e5BpcCqm4KJDHn8KIvtTuaRoT4M2Q
egEx1HtDZFy0euEWUgF98c4vuT1W15c6bSSJszdPV/XsDxcqMS78rOkV5vfwHsVAc3QsU1nimwNi
U3FFPxisx8K4dBjPfeWKCq6kZEubOlc8g89juqjOk9g0N3ruuNMUtMSYdH98rwwKsbb28YMCO5yr
W3GrXVpQ3NsaWh2Jg67I/AdwMjQZtEWriXaL8RHJZL1vbTUtVcS+G9m3vvODTI61+4gv3yNDpQ9t
EATBKp2sIi7K07oAIQs91nPHoscgXE0jf18utz2NiAC1kfI7yjRsmBS3WKfmcTlTb5rzsN24c1dh
9pDi+U2bVdkxRaqE+5EyCuqxDM5+0pIwsFXGtFFMhKL98SZb0P5ng3GeceGU51/BUQ0c+Q/e/Dqk
uqjwnnCLeqalPFD+pUC8MJbYXYNdJ4yr1nDHttXyAuO6IJUu8rwbZAnGYhe9+UITyRLHqCzGIzVP
f0t03dFpFdoAqXs+t4F+Fe5Pk2QvBmiszeh5wOQZuN7O+E2uxvqFThkEPGdOGXR7BWwElz9ZYNvH
bLfhXShOWqGIWyjoJGpKHShbnCtsbB9Eo/Dw6X/z7gFd84WFv4sYSXQ8WxMvEUuyzbp2x9Tt1M3H
rEpIV5LfM+j/+/CXvIj5JQlI0nhbQPCtDOwNyhltKdJ+SOFVfyzZd9ktCV3n3H5+275yGGENrCFV
4hia34MWIX5vIJI+skS8E8tQaof2mcPtXzCgDPmfib2R3yEaMdHGBtSLwzNXk5xVhjdIcFwHUkf2
DCRxOTKr0/ZECJKwtkSYL7xOW5E1fa8rzbGfNZhr+accBfBVL+TLn2H+ClUI160mO8Dxsc0dXjuO
yVFVTipZw2paL2DB6VJ4csoYdQTN1Wox7GsWH4glgR6aomqRmb6e6O7jYNpNMwDO4zZ4imCtGSPW
jqWD/mGoyUuki4mLYOG2iYl1R+KPR7+vGH2It/vxU77LcmhxvjoieZEiSMu7EvCENHX0br8jGxN1
ZYzd4+g5jhqXpe21oKpJVANGIPuyvxtLCsOk7JICUZ4UcZv2kI6aQ30W/fNZvRBoBBjLbVDX8sNh
8opJPP1xPdRXAnL7ls6MvqDzt4mcShvqAPhTEZr4Pw6KlLZr4qvG+CbSnK0n/utAa2Yaj4OrMWFQ
5M0zwBuQsgqJ2Wn/hDSQPZtg2SxFUXnP6WsGeoOHBRYE19QPuFHDVJkFkO/iSA7++MNUxs/cBwMv
gDkCLa22SHj3AWJjbjuO1iuOWrZP83jqNuphy4xX9h2RJRvRu4WbOBYa6j/0ITjOq8toq7xSlZaE
eDa8YcDY4i7qVbCp21FQx6KOPS275MfDVLZq8w5j0wOuuGJJSQEtm4gLZC1VdztAafaXwAPTcatB
yQRoFvzq1BEcziErWgB2l4goCK2E3nqjmv2eLEkuB9ZPEHB+XK+wAhkPjrIVpW09+xITSZzmNdVd
MuFX2slBMDhkdAhxwtr/lXBBSBUOkKySb4F9UnZw4TAXKn44uq1GoyjbLTLh2MZTbqo5aqATrgB2
rp/KM73YpOjfXZg9+/9+y+92JMkqKJiZKFhby2Y0YHtpDuL2rJzHjuZmIGBKUDxmTw+gfldX1HmM
mvS/BuGKmtzmXFJNzbCQU90KxvV+C6yvmI9/iwYHkfX/2x4/kCb49aiiufLu961CSZ5fT3k6ny45
nR+2jMc7pVth8SQ+Pkg0OJ7xToEVRTAmMfosjf+0eTpneACf48ju9G2cAV0xwEavZXTYV901WZzs
yho0x2WNGnvrC6VxnWmnBJUlMoPIat3T+4tLGBolddVuWUqLaP3Bh8F2dlNGVDPsAhS/ODOq4M7V
h1aGDXpm0JWaUF+wwkkKpTWz7LsO6hO/7WZbPXo77jwnyEdW/g8nF6HByBvilMfkPg9mOz/pWYo6
dK4d3trjDSvY13MjOiuqAS8ZNv4ieUSfzpvZLN+ebYDSe0/18gzEtKkm/RK8U6QMaRTwf15R9Y9Z
vRxehVaY9jn/q7XEx6jU6tH+ABXUaCRA+HzRaHKdcH/dV+0UAMgPJ65lFxdPmdrwDmHJGFBKWh3j
3zeRILmFa4YrltQbyaToOoEI9R7Mxl9kH1s2HbXXi3n7HZArNuMdcJ1mk5MdrMy9dzK/Z5Nfq3Bd
7sqiVU0r+kmPSmz75slPFuVV9ZDlasEG9CF7MbYuwVab98mjWMsDdaOd7QGAn09MbD2Xgl4O5OvK
sJjhw0yopn0RrRvgzblz3qW640f64oZ7xzDojxZg+fj0PFkuZY8aQAY/2pDGh4q4Qluvnds7WHTn
mO8eY/O3/OQlkXYtAagvnhLJ4hmCdjzq2YSp/Woz9I/dYjgp/Ut6m/ZhLJt9yc4D1WUML/R8lIt2
9xQfuy5dZ/lFMPuiTtdQJ2hlemazfjbcsnPHUCVPj0UYIM25+JB/PXM0GW+uVcDzZfQ4akTSMZt/
hDgoJhzF5bxCS8pFTRfu2gKWbKUn9xL4F4UQvhhOmw2Cq0U6b5zOcUy+HITauPzaEBDV1Cb+OZqE
p99MNkoBJGRAV7aLHtPkNv6KWCAen4uXpS5XHKBfm5LCaCqFqXQs4qwHttXqG+RxSp406SvmElKT
hv7eVAYrAIx0Yg760lXu1b1GpwtoDU3Y42B3vj+1zcXAzZsv/Mhp2SR4nWI0bidIFcudt4jm8amu
aJ/R8n+JcyRo938OOEr6OZygeIpguPiUYe/xVNS5QXGO+4m30r3TduwBLbNU3JJ/zMQ5DZ5Rwa9/
dlvBmFrgBObFAfKz8A8zAnjN29Cqpp96GAk/diTubZc3u74Zqex0Ov+o8/k/afIzSW10pFoIIyHU
C5toSntKy9dGU73lxq9t6ZwUtlBjMjTIlTvVNKUXivOxjdvEyhnw/dEXO/HvUVX7UUPhj+gasDR+
rwPHCMtYIiOeDKgDUhN/vpwUA9uciMmya+xxu9NB2AZRB+TH8AGEvkatUvkSxr4FyVY5IZ7hbjN2
eDKlxjeTS3ugMBt7IwKBp30/pWzwpUp3tizV87swUYbYnhhkDpVMb9whPBGK/EtiOlHuHAdKNZU/
dfNmWEcb7ZUtllsT1hWERe9XKlfAcwQUoM3dtMgy6fitVyxufwZNsLawczsCtBEt7VRYk5UeJ8bS
lv0UFKc3+B0Hexc+HIMjxdUWYGpNkotn9HC7l/GJyG94Q88WGjFxJpL6Zkz7yWF+CQSy3Vg05/CD
qGC5Ud4Ztn0kFxZeNxTSQx8aQpGxxzzEHxqflPgulPRpb0OwjdQeB1sNxpfS4N2G8iarINyULIL4
ARBmgZFBdG61nMgmTegOUBbj8FATHssUBwmb4W9moyuplvaMlRp/qQakOvWzCzgkYSO4lWgMu9Dl
7SEcnNbjpp+g5ju9ecFIDQ3k4kNmE9JlirbaKivvfdWvi0kZBNspxwRxou+QvUW+2OmKMcyfM81N
mpabahr34NpruAJHSbTEgVaoQJbQ7hs5AmMzAfPtK+csBjAwoG8KOE/eYF8Silo/NG9OVM6LDppM
kd8eDdtpJR6NEUjecVQFBekYmjWpE08PydTts0syPk0sO7FVt7KGzi/VqaWRxaHpRR60+PSJlJ2G
JMFiEaZUVZwmiLg56VHpwdl3UoAZNLMV4/N6ONB1HI3mU253TmIZHEj25VQ5FP6Gg5n+nrs8ZlFe
iBzDs3At5OxmulT8Q9abW73KEXo4a1nr9tf0KgLuPpDZgtsWazRgtyp67tFzO5rb0sF3SZz5oZ4K
c0yBDiWabmQ9IHDaeR/L7esGWNFr+RvJMNbNIoz5rQVuOtrtfYY1VEqFBmlDqMaHbuzYitY/hnxi
HVkJOyZUc/3/VCYWdZRPRpHxoROoKoyV7IcDEMQfkwnYzRQjHItO5Q+ck+dUBXLwJUHhyzvyqINF
nivgYJuV2MFZh3zi0mEGhtfC2lNg4nDivyc7h4zzTPUo8ayf5AaHBnLHf2j0Z571/fsVvSn9EUgI
iOroCQDycK2dHWF3bKX6HQCy6X781mLjHuN7cVmr74CiLRxf+ahIz5y+cOKD8cTKUkHNqt0LbJ3V
Tk9uUs9nc0/M7ylsokLP7Ni+e2vXvABF+6wU+joOUn4bYhrI1cvl8ffPohe5qb6uv1ui8ncdEGl7
JHB/s/bhOcycE54GOlx71owvVgbWvQSsMhHFUsVVHkTSBGVhDLBgHN85wqRgYgU3XJ9dtimOaFXo
MS4haY6tAJPqtF+rSBF64EyteSnfj2ic7JySMvgKKb/N3dyen7gvsB7pKt6Fe+TcbesZBhwKoiiM
zRBEN9yAFxX90MzjxDj3bUzDoP7bNWsJI8LG/a6B9cYnTmTZqx7hW/kRel+aacPx/bKlatS+y+hT
FodtxcQS5gMbNhewyNqZbnooR/StUuZKCyzACpRooJI3nEXWQM7C577Q11zSJSAXMlP8tWMXOI6Z
2MJgKLfaMnkbvh14ieqf8mGFS71+FCZAFtH0t7Cc+/wMrSfxbe9qe92zs0B68stUoT+xR2ocqShA
rcQThPpbaW8q79q2/emZicSyo0w4GSG2vgfAABjC3664ls7n1Av8j69a0PO//2r0MDSGAJpHhKo+
Dh792BKmGBkzDymfPGrHAw3rski+IBRwBl+RlC+HIFmfEMsoN1stU+UYSL9X1CXM38ErvUy2MOS8
jWL6EW2sL4j7vdZMmXBGEaWchpDIYK+0tTJsJj/9iliaWLSHzGNu7ptKHHSby/e1mCt36t+jnzmm
WE/Vfde82fXeDLVVSp9iMyT5AoUq76VqaOng+/zTDgFVdluW9JmLeTFXy6BsGQKzLeRPiLoe4hRR
26DQbT6idATRTaAr/07verk+bYRnZSCyx9hgKQEfQLzYPXvWdzhTj8LqU+zqdIPRTGXtD4iElFki
RpemW59nw1syrtrPDeopVnd2T+Od3M5g6EyIYkYlHS79zN/OEifPSl0jatZWxb9jtMutEFuVU0tT
T9caGyTjSnGg6HI7somM0VuzXMZiwj1kBfIyjbchiAB0dsGuBB3aQB/2eCics3B4DZ/HkGEIWYj1
XY1kOVrM2GGyxgA07xq92+qBp23lAKuLXgwep23Kl8irtLkYwOaawM6x90PsT0YsdU/SX+HYH1tz
/IziytlyduBd2KPz9GSo+w7hIpszSbPqsmpNwaW8ySx6M5omPxgc/yomZzM9GilxvKBSnxS7uEEb
IFPmW2WNRGZ6GUo2j4OBC4cL0OlExS/aSj+dIyz2MNn0xV/tuLdB7X8RGWhkBzOKdxjMJcIhn9oe
DMzQig4b75ddfJrdXxIGNgm0w8VT89OaZZZR6iqJzCFtLVcNiEtngasQqjlRLTc0kmTK2fKTb5HL
QP2mikWK/OCRvvWvSdAHxIqxmGzjMQJX455NfOY4B8vnG49TcDRspeJbxiC/3ouA1yD/YAfjx6yB
np3M5py74rxrkgpmYaJhO4w2mLzKxyC6Mbs0ke//ANkbcY+DWenFgVPE6CZ4Zo0z/C6h+FIYs59y
96tK1r7QwidD1/Pdu2AvmwtKj4M5AdXQZF8HZOBJs/wyo7uRJSqWbwfiNBeLdpdRwi8KOgGWozDf
jaEbYcFZCY810QT34/jjnroxg+DvqdWIRXZO6Qskao4dnzayHc/mMhv602xX0EfBv3idFsyazd+5
6wRtB+gyG72QuvTrpBGe6nj2uSWsRhKO9q8ZjegQbtXMKvWlKinnY+Kp1XHXWLv42Ne//8LuDfrP
4k0vvZjjB5QBmgKuO3E2KsQJOcmHmdFUYVO6B+jm8JhNuGtxFxbE1nWRTjlkfTXvCrhVKL2iFrY5
lkU7VXHdm43REOlAuxvHc/IF23WaPje1DhLLh53m9VvkXeSFm0qhL8ECut7vtwIrb+vIZPRu4dTV
PuVJD1im7XzNCtHuHD7+pEk2HFbhRDABpEjoXEFmUPpFxwieTii371V3YerqR5+ZhK30NHehJxVf
h+Y5YZE6V89ETYFoBOFAScdcrrdKDRFZUs4MCEKabdGthSWMP+90tv5xKMiIWP2TqTnJ3Ab728qD
bgJOAgQCY1anY1x9RqmWRvXQI48lDldRr3D8vPDvqbhiwKJD7oI64AYedHzjSa1mf8U8Jc1PprqB
4+REtfl7HMLjlS/nOAjLg4T/oB6e4o3YSMnbOPcf60Ms3KtCRrmDrNYIO+9W2+IJRBt/Lva/SzkZ
/TdJ7nDoOCnkRsWhoKAirdIjWgG+mYMz/eY/KWDjZHcwnaK7FPlorzUoOfBZ7IVHNu63knsekNwt
2qYl9mDrFN6QI6pEA/vLmTKCNDAiBMzw8V33i7wR5HoroLDOljqPC4zATWWp6zD7hZm+jWm0Fx28
Fv2PMIcKo/6BEVYmcqBPz9BZqYbcp7uYcmkDk7RWfNIJxz8sw+9ti1N6LN1OOYPbu+R4mZpoCXkt
dNXkjVqgMg8q73Hdk3fNA91hv71L7Z60GCuyE0Qn86EBsTqd36j1exirR5g59VExMeKVLHxEdq5s
lP3vbL0wbUvCnywnnD2iSqXgx6op7Cu83pXdNF6T4wvGs2GAstrKDbo6Ly/L+lVt8IcqsYmGoT3u
m62YVfHy9eU4VVPz1nmgfzEAVMt1IM6foY2Ppg9UmaIPtN5BQBan6CtV6mS/uJ3U7UZRTi8K1RGt
0nP5Z1Z/hIXXOBcnQcmY7cPSZ8Mx9UC/rTFK69LaKL0qax/FAvnclx0NCqRaDhg7Pvc57wpvS/LT
JNYM9PMBYCpE/Kp4LUk7Tw1PqLrU3krZaqtfsgCwCVP5GBkPh1SLe9UaCjNjbRsI+gLecpLP22Nz
aCvcMofG879vEpT+1lBUY3auT+ubyYpxm00DMqjR0fzQuhOtGZePqbC50MSTeRM1FxtbCnn+u53J
kfIMds8iriQvbDNF5e6+8s+JC+lDeX3iT63102iJ3nQtWH7VoPl9qmy2ZSvmWPuUCLa6tWV6FJpt
MPkOpM1eAI+27NUadwP/qQrgTc0rX/lkNMyWq/Te0QK72CGxOIczGIoIxFf1u45jDcyyLv69dxUn
pHkVpxGPKbhARcD2VLUgniSz9u/qFhfxdTyuY0v+auWy2AnF4e1DgMwoEHEp3+mpOB2lSOtSqsQo
VuuCFEKaqpI6x5V/XqYAJXBDdoutjBzYDt+KhTz2Na8XrO+d9eFsSQpxjPSjpDfOSq4Q7kymAsKI
VbaP6Gi1FarNbC11zbweBpJeBEHtCfsuayyhjsic6wOvLZOPX5XUobMxaCN6pGGq0iwJMBeMw67S
6NJf4Dfh/I5HOVZuKdPPGz/bLYHjo85QTi7zCDlBsnEWqzvhK7Eijd+694VI7XNrHXuOgJiwtIvZ
58hBoN/qiCupIr2SiO0U95c/dV2SVicAVKkUgMvw6EhanYfNluhEtx427FKNOL48BBpAjovL4+FN
zFS0ZNatLmoH4KB76eCFYnHTSJQW5yxiQWKGNFZ/sOReZ7s0BHNvUnV7Vnd9Tl31nRu5VhJXCVW4
qqTzc2Z1fPjhJSauj1MHACgNxh9+SVUR8TcgDl6RKBFrUMM7p9pEPUoI15U+8DmNUIyb3K5xqnw+
3EQyEYR54g54+tDZXlTCk76y3Q8ULBD3S5RmDornYpN4JYYLktD9WUJi8e+2Je+CnoNG3cxft6/Y
T1ZCLed5F2Ma7lYiS9YQk4m0NisUl4t3VHy5Cqgt6CfnvCIQir8ZkeI4yBDQNJCsGdhy8rZFE/1C
WyTzxLY4Z1dHw3znPWSj9UKB7L+uBFk5j6Nn/gOmViiKoPnfLeV/JZT6fSy0xsL34X9+jAYBjb3M
3eSokqsFItwHS9FbuHMqlyE2NRL6Kxaz0pEQ2NPpnc+Bs74kyoPZ2ZMsX3mkGFj4XDTpCbA9qNK6
H6HjeinZOmI3hgKiw0f+WsUtr+N+nYcPtV031C8mBy8MzTQk28hH2f+c3mJ9tUukVdbhZSPkj+zO
1zQhKzLUqHGtOz17ahBVrMh/iZpOq6FzoZpsp2aSqlgElbASUrZ8L57knCSvwef6t29jrXbr+aOA
w7ueWTDfUtyJnUElnptXtc9iiDOSKqxMAwxvNSQws1FrFKBDtSCoX3pjhKj5J4NC2v95L+h8GE6W
9PRf3twNIJXe4ffpoFMFOn56834sWEUYV9DWITMzZYGU9WIxRPHaqwkX3bSlHe0dbxaO9e8M//yS
oEya1z8Vhdporu8jdzjOAFXnA1wu24h5ZDuWNiUhqawYNcRDRm9pY5XRKCh4N8+9/X8wlwlu3Crs
c6qL2hHRvE3IOWxA1aAWfXpKuvz9koh7BRB6BKB6Upzv56Lc/0xaPGH3mNTZZqngRUeG4rg8eSgx
RS7cvFFNPFVwwUf6IW7ivFYQ+H93aPksK3qtPpbiLPfiwHb+UvMazL2qXedpFVKn3KLQxCYgcswn
5r0pjRK0XwdoiKvClw+9ktBocovYvUV1MtWICZTll19dcdhr8woSDkCrCOn5BnACAqDAs/DBjqZL
Ukn06BLWtCG3Ga6OSltDHvL5UbEuwdkxdkysI71eucnYCQmi+SJu1GywtCzhwV5zpFmvLdyldig7
LhAuzDpyVQMsBJKosRQhyCsfEJuuZX4m0C3EOCiC08V9jcSjkpNCJhi+V/2t/xMFifhqMrUwwAW/
Lmm/ujTL8B2nqiRL2nBA84fwpSxQTJ5up3JDz+65zSoj0EldM6+58R2CVkOsib9A+aW89d/cz+CO
t2usZDRnweYTnCSMVxZq9XEoqEITMcUv4bpNnMf9mAxWCeO+blOB035Z50KJon4SgxK8uCGy2vk+
kpqXGPgJijHXNY/q0HefnvgoB/JUdKoI+Cgpchu/AWB+RJPfs9zh83zPEH9s8+etdqg3qw+iLzBw
6Jz0ZEBPm1TjC7cwosDeMNi3x+IHkdBFEMz4Tx7f3tXRPXNJR0KNknkk6lMWkL7AQv8rxa8rGH+C
9qAzJCQCkTwddjeP9xU/etGuQ0VEjdXP/l9sT1FtEis6hFUmvjkV4eie70aDmw6BXf8gxT4YX9Do
q9172sCEm2TU1ivH0MZ16idaF5QE+OMA3u/5nc4hfLZUZWZnJkhAy/FW7DCzcKZxRbsuMwwL87yp
pm0/yNE9nO75BZebkGUWmljEtCqorniupTQNb3WDdkZi5FOugdcAMTdvPXNshjoVrPbIk0yeYyrR
mlb8PZ8ZyhE6TZ1GnhqG/Etbp38ko4BasKsq1d951cbI01SmLME272M0H2MqxeTr51T/b5qi0M+h
bSd6a07ur4RA89Ii0nF4JX2x7QvRwZKNjl3Qk9Uxtqjw5I85BGcWDrRAaSZlEqXzpFF4rZZRqZZr
AlL4kQOleZ5ntC89EiM7rE0FgdvrRoQ4g4ypNLwl+hZx6kgf2Q/1x+OyI+vvAHZORZRZbcQLFLbH
Me7VKCbqEpNha0PT47+kcsjjU+bXa2afuvhWRVjG4vmQyGNAXRYyjhG4SjACz7izItclDdlprNf+
aD54rDCcME+c2R9VjlJhyHGjuUxB4igJFtmEz8psncnKp7GplQ5f0aJ5ILk8lFq+oUwmR0h2SozW
9KUlj1w2m8MIXthO4UNBPVOH4rhbRO9Prw78UGWq8haoVwBlqgWr7GbdjaVY/htiDoRpa5h8Bx8k
Z0net6OfO4YV25XCAaUOzTySnOmFzcSvmtabV3V6KEXU1VFUv807MU7tBCLtXgArYjS9w1yFDM0j
BmTBd4RfNu4djxLjPFLjpm227aDNZnvNqJVAfpjUH0DTEsY1Gzfh5+8b1o+C/fTKx4jsW0Yk+JIz
5+tG15dWszHL2aN5JZmRf9JkGvJcGLymcMOodq7N39RyX6Vop9xgpUSXw8B+aYlqBLad3mwXi1ji
2nYwTIDRTaiZnQXWaOFb/bBH4KDrJPuJ/4+FUgxqh69I+ZNvuMHSjPdlIqLt7+i+TfnIfDlj89/C
gbK2cEJMnzLftOoYjTUNqcoKblTS0MLDwEczDqLPTu6y47pu/iBTmE6TS06/YQTl8/ImLUAso0Tf
omtAR7ddNe/SBwURZEW7nZv5QTEBYaNsVigeJH9Qh5qonhm0WWB1ylkuQEfPD0nZ2VR0bfMaoAZ8
BEOOBpOTVF+Yn5LrelATUkImBtdQiL8QaJ/EoXI1GloWmVNPOGo9x3lQf2BgVEJcrRBfQZGv+0K8
dInfrFnqcLyl6rRPXPqgTJW23dNAi2EBpFwhs4txH2ogw06w2hhKqG9x2CAv/di8qp9IOaUHJzBV
hCvbM9eeEmaVLbsX+2z3K/h7IUhqf1xCEkfkLUvkbJIIw+C+UKvncffRIh7JsCeTdNQwshfS1cHR
Uw72d88LmD0E9ykva/GjbS+q7X1hLzm+Wa5dIQwcrCnTT6duGtRBKmn18OtTJE99xKEK78RNmR9F
zpOEdV2shFYRxu+X6sZC5A5YiavndFPSD8p5hX7PUBu1mm4HiE9uwucIXSCAhLg17NDa//Kb8Ndl
NEb5tCn8uMcy5DeNhja6p5hmR00qaRgwll185JfFtrsxqM5/bFR7pi2cwPu0BcIgRP+RuoUzaUnv
Z6gL9gRCkil85NmFyMnv4ntuUqIUeikUiwRWYqM4oRPyGrTrBnSb7JKi8g2s/HVRXueB6vnRjX0P
hEOtEY2/nRieTXFcHl2X4JCUtocATltFZvGbnY8DnD4AgL0j8xGd2lD6CxYYvha+kw4g/F/34dUU
1IIkrGLw9Y6nrBWpm/8hc4a3K6Z0otGcNWS/GNDfyosJm7aIPfuMhPcEsejPd0TpxPR6VdQnK+pI
oJz0/AUxcFkMdQOF/0ipl6dsWLw7ItZ9dEkiB9uDsE804uK+9Yaph+2oUrjvb47k1EMM5/7JNIPR
Bpp9h2gI/GAcvvWfylsRW2VliTNHICyerIUYSD/K7Al0Is9PhkEfgMFWk7u/mwDYA+fYKXAFuSr8
Dkut1VpYabAs2FKE6uO8O6e1QUBVPU1XQjz5V3HcZrFtXgYLnbJkwRoC4FT4enWs1VNBRMOok14q
41hjzti8YPgTnKoOnOKwlxBA0gbmdtErL9DtTLAtLwgPihpPJQNgHb3NM2w0DpWARSkafRbBSibi
LjekPDrrv42DfISZKwIz6b+zesmyRwgDFYzPSd+O8I9vFe9edThjqOoeeHjpQ/sIpANwNXssN02T
vFKlNHfVfZEJw+glLzOHnnrvK/cD9jqQC3sf9hva3I1ZVqoGoP3ALLZcNPxHw6BniAySbWcNDm+x
ZoOLGfGdSeJIrD7gXIkAZbRcYE7k/17Rul0C6noqmEnzLG938rZsVdXBiCE3m1pR1e1Ac8eFksvF
kB8lNmin3wx0UV9+XIr0vLho6FxINK58Ps2736yXwxknuEeEgqdOkpQg6BnVI4rbsQ2qWgNshnZv
R172ZM6n9lc3cMZXIW/vnk1lVMbD74vKdoeiVHE0bYlOm+kqJAFh0xw12ltel1syWfrsKcdl4uV4
ZqGoL7WCs+78c+F8plv0jA6kNm2VWconUXTDA5qVIyWaG2v54Lff+FO2solYD5JS+LJb6U10j+yD
DwpJadjRtXSVJJsWwkU4XkUgLBJ8KGmVWrhvDOP/TIWBR1Jn7UvkFXxpU1p9Qz/KD5U97Ab+v652
piYlmvMQarCFQHZn6bVKBN5OzvMgcYGR7a76tIZ3FsXO/PsANdaaIeslL2TEfOXIgAmxTfRfc6h+
4GJfmP0I3JCG4v6RG1SmkoxG9Pdiz0A5KuCOdtFcOLh0M5LeLb/N5tuPPrSonkLxJJ0D+ZuGhvDT
Yx3BrexlknGiydQFjcpACjI28wVY7l9oiLl7uUecdj/RlPREhPNFoyeWKA12dbIyyZy5QuI6g2L+
tr1vySv5MwNEK7rtT+iL3dUI23qiX/rj2GyC0pfB/J/v9bHw2OEXmLb8iHQRHB6imeJxJ2NlIUrH
FvGzEwpL0xwf311gtyCC+wQeIzQMQ8WoLuVuNxtVDZJiPw01p9AwZV2AqwXEL7Xe+/MYY03Nvj7h
gkkysB71Le7qUDtsi2IH36JHnTwGVqnSZKP+Khi6j7p0IoXyJ5SiEvOjrrSxVybku8N3WZ4CwNAJ
lZki8VSTAqtTJ1gESLtroEJwJH1/U/RTqeAOSl6m2wiqtXyvLmbOCzkxnIbFc4CWmC4+V/77a1Or
yKxxETOmlUpgk3eSGkJJUOF5Hqe7pOe63JdS5CRijw2e2TYKez0CQ1QKwwX+sy+vJGpuw2vXTnh0
ffDSKxlmYoxW7iWaSew0CjlynRfviYysMbRUNQ+HuFvPP078sJhmB8BsX15yqIAQ61evjWoCCsPW
tCW3Dh8HK1P/+0UwwyfP4JonE1C22OYQT2C9y22OLLTeSnVs+4IiMQYyCETDTS/lgJaM4e7oVGNE
q2dt2ZNyPyumR/xw7wDAK4hqYafjbc8ASfWdinNMBTFnxyZqr/G1+a/50UkDWirIbr6OyuTCH607
THun2qjBmb2bGwIkEh+PJH4+3JlHPOTmjZSESkY0yaP/BVY9WjCg76Uj5pxFv9WidswkFjwMcFFU
XPGf9bWfF3dXpRJl+v+xHmG9e1ywrTL2wPzII4Bud3J26IzHY9J2dWK9SrQvO9U861Xkr9+G4qIK
k+EZGSlo57lGd7w7J0EE+vIOAaTdKnwUGDfpNcaKrGrrtU4R9aT4YtwQ8WgANzfotRaYf7cg3IaZ
1wg0wcjHIRKOq6+LbYQiIbPdVpi4iEjW5/j6kXGh2OjMRBXG1KRQefMRjQY7aSAQ9m24Kj5Fka1Q
I2r3tlCZzH3UnMKGTUdWfSHYnidHJGiK6khbTBmjbHPpaCrfwVvX2M7gdLY+/qwL9C6NePgyt5yB
pbYB3rPYtPuUomeyCsHUgHBheOYCZYvjPrgR6WS2OUiMHa7FRhQmxiuWiNnoEbkKdoEg3oH2tnBy
v8sFuHeOt4KV9JyzNEjgMu1F/1mGAkVOy/XolVxGrhbCvjnM/ldEiosJyqFoBz+Wp2hRtEE0+uMY
JFatgmhuyYRbK9NdHUZ+aVJUbZO9wCRgfQ9Mh1QGcs36k83uJjY8+xVzFOYOQcD80Suf9ZmX7Ywv
c0TA/2BQdy/9yebA2db3kTyMplroj53LdhpzYzQQnrTznWaz3VL6dP5SzYwOnuMijzZn9PtoK7jM
NWeKy1uW//zD2hwyFNv3GPQu0Ade/a12D/qpMZAZRTgOWCKn+yzGk6nzTWvWpBZkQgNEoxJBkoe2
KMZ9ckOpTX+HcLmybpAHcVscom/sxZ8JmVV53fs4Yg45Kn3oEP/Ci5JYbB2tta019rNxCz88ha2x
8irg5GOLkoHSPAE7B9RibpWvrSFCOzP09n4naZJ8Yfu/fuk0O9gT2knV4pY4Yge/L0hM8FwEScNz
jD/RFrKk8hyR3wcq5Z4D/UJnj+SVtkX8thRo1u3OBtTaVCbgV+4y3XFB/qaSByCP0u2al0fE+UUd
S4UUxfvLaWzzmaj5ZyLQDNESELWUm56eTmTruyyV7BVwTx8KzxzsWg6Cp+fte8vvuEb22rdISKUJ
EGBasrrhJhxgC8Uxmb0oJ0FjAvoa6nu6+GBVVUgmGUG1hMf3SlJf1JIN8X/l1qVmAOccbl02H8Qz
EBguGtRySmEpEgSKV/5WICn5nN8otQZ67unFZpEMfS6JkampP1sYakq8QtojxaRC8NrRjmUmhqsR
r+Nw4ASDloziKsDgqJXkfh6RkS3eddWwWnD0vsSkLYqXPCCy6m/yL32Vruifp/7FkDC7ytFwpYoG
5EdWJxRRvIn/29yodM/LOl7updsY5yCHGu3mqisgxsvk6O4JWM0ga1nGMcL/5J6Krmt0WZHupzWf
3iGQuqZaT4gzm+Pnq/K9w/r7c9nAn3pZseDU0mr2WY7jFf+/x6QUfXRAn4XVFZiV+oG7Bn4M8FrO
j1/6tzAje/edP8qL2Hk3RcVlmp3Ij/5YDAMkJZg2qFBoOj5Nqv3ktgm8DxEqA0HLomt8eUzkaMo0
8ebeLG4IWsGvFXvQ+9W/hKZQ4oN3bbvZhLf8fXM+IgN+lh5NALS6L1Wr9DC6AfY96DAstAWjvH1h
96fe8LWYPoRDbv5UeJvykwPBFXWuox+VBHr9t88feqSHTKbKn7WPYPbZyhqx257m8fDg4u7/P3vH
yN1JC00m5jEXT85LBHGnWmqbw/xcj6Xb7sB+E6233kGkZ9EkTIjqQOo9fDM78gj9XXjr+2CDHDlb
gb9FfBhXxiHFo+VrkM3Io26hPGNjJ85Gru+UVIxcyaWjL9tCOj0W9SZ4N0HWZyKSYaPbsTXN3TNA
pyqmQBhACdF8rnh+ngzRR/cvbAaRb5vGHr8AjS0llrkLzpNNuSXS4B7/ZP1mtMf4hEBqg1/B0mN0
IGhUCjSEce8pPJeI3etH9MUWiLreZguuLtCvJxmf0N7tJtbN9BoNWanR0hPN0oquhN48Dws3gqxU
c3/yRO3yd/6pqq7xbDcggfNe3SXp6tlJAz4vclKI9GwN5XduhAOSwD9Ygejmixasf6VOMuFs9Rj5
qzIY5U+76btHA7ZRyOQDULQ7iW/0zbr0eQ10W22iGmgEAdqILU/mgVnd/LFEpAM5FboNOpMrYiG5
6QtTln7upEqXmcxb4kVhoyt6NgiXMNO6MuDQfhQZyxX3IARa334yUMZV2QL8OyKOBClizug69p4L
hFLZqoalN+iL8G/wtHhwTfmO7CREDwswkf7PFhaDPzFly9dVhy96tYX2hcEzfMBP2mW2SNGamyzs
wCsgywqD15bKPbVMO/YnveiSyUetqj6mCXQbGjWtgDZC2ypO/33ufiXbytgg/NjOQnI2P68up6be
Dz0jnqTqZtXhEYqIsxAjjcAVgPCHrYJPkXmUu9JE+8bsr7dK703aXsRl3UeHLwgitaLp8E6bPPfd
BdopF6WQ9p98ptujBjKg6SqwxXTtNoya6jB/1iRs0JY6o51ATucdhB94Mdzcs0BbWev/WD7OGgHX
0VbjwOFFenOD1r1meYHs12PYY5iRYF06u994sJgjyJ741Qig3slhJweBPgJsQTDUIs5syR7rvqDs
fJag1fDZG0vW4NGXU8KWj29T1yXeBw5u9XKIlxSt9RbAEuNi0xwYwpirfWJM4VG77uCE656c38nH
SBniFOLYMcHGV55jIhlgkC6MeIdZZX09A5mWRm4NrqIVO41RvHVIl4wX5/pGDjqFeoz4rAwlKGii
wixh1fUkM+4fApVPtFF5eN4zGLFqzNAlxksBf+TC0kLyiRN19Wm3gnVIhstWnIxLohyZ5iMBFOXi
wa/TR2+UFag33GUlMryZMf1aIjN066OI9MopDhR/BAwsb2WBbQ8ODaoc2N2V/zRX1PX9buwADgNK
yXCPyy2IU0EkHVPSZysCvOmr0mmEqYAMKj5qzZaS42nO8LqOkpW9BOZBBnsUyyTTH4Bkt8N9P+MY
i+9ytMjW3gF1OUzC/OKzYQOrvhppExrckOU1y+4czqTtSluFwcG3qilRd3pb9/oJL9yuPLAEt53i
j/j53SnxJlJUcy5WxNWwBgBkZbi4KzPts9fz8jOj0A/MpBBvuPmKQMydA5A6avVFkVbTcoEzXB6r
GPhWoCzyz0aBjMRAEQbtY94ASiXBaohUD8so7NynPypjT8PFHdlYooXHTVHx+oCW+TnIr2Tx2+rE
NsZHdWY1nHX5Ipz/knL7evk3/AWQDqdntbUpmYBcpMDGKEPXfzDKhZ5oHNtoAP6dE9Ot6mzbcKe0
Mhs/25WuNvMxAruNROmAY58ZUUnSA42id3p9bBm92pMAbSx4jcJhK9/63k1cpPnNmtEF7PGZnvaU
Q2gfpXGriCUqNlAIvWsVnGZlF1eZY8IGyCSPJhjy4gT7k511KGVJhv6zFu0dRrsbMaFk0fnmy/5h
U+DulE344TfmQeZhUyJ0FpyeIisC7DlCixfK14LzL1EXuz8ZHy+Z6WDJURwpph9dkdGXtK/d5rWH
jE+5m23UJKhvgULivMVFqdptm0mdBDWJoCBiRn+E2wjI8rqC0xKkBolRkBF/NDt2Bhk1NO+7lQqc
JA1xrsdSaimZwMPzu6lxNSbPqBXhEfsBsff2lBTHZK3x7z4j5Ti0ivCghnygdG3kLv3RI5t1wEM/
r/71TF6CaQA49hzfEgLuVEEI3SO0Uzu8VJcOW98udHC9Hq0vBMpdiqVNbgj120/watWvscHWGBUL
Xu4sPnaxZSNcMoqP7oHpSeuqUbW1aM9xU6bfsY74mrOnJzr0er8cs+RGO3ldeQAa+TaMpaAFoMkx
o4q/GDK/b9r1vzpJsVDa0MVuQXs9d3fH1+aRjLbug+lNUY3ECgjIGCPTIz7kZf4FEqJEx3GpI9pV
I3myFhZkmE0vmd5n3Q6leimJVx/ST1SAQxqzrGdcyvRYUH9/pdvpZJL1swpggB5hB1/P7ft8farV
/tdd80l74IT6w0oDe2t1BHkxKIl4dSs0XNHmrhVx9wVmdKZM3GUE4gXynDjaiGkDrb1HKzTNVfdl
5m/Z9zZ8SWDUgOa8VISMDW25giJWjcXJWJpMf5+KhL3YGDJb5tTR9/2/qwT6AZXLaE2pl4axnRNm
4iOWwwRbE4BaqzP8Aohuyw2E9U0jLZL6E4ZtHACaXit/buCJEBkDHbj4Wk+zASIpUeBeWTYX7vCQ
tMQoTCu3BIzPGzLZNOwkU5f2Jt3/E2LsEMCqaY8LpRF02FjbtdYSpY5NvkT08IBgNC+VoTSZkIRi
iLZHiTjU/8gi+XIuFdtWrKXjO3Y5QZ6Qy/ZnL0LM5qq9KGfEPdz0nTmgvUPXW2WaA+07e8aicfBA
PiZPG+EO1nWp8UGnClQ52i4YQ25atGKedaTkcvStSze7t2v7MLwNq/gD4oDLcsrCaS6VFjX+IDi7
R0hdFZeRNCFB0Ua/1CBFleorcaSJFQrsOaZpaDmnE+u1MEJjFHT2hU8V86GTCjy+179+o7/SQRb3
q2qjPUY63MjGu7RLrpIVcwxKZhKT/QoaxFn4Pw3We85c0XIhBxukdv8WwwvKuTyr/ghNq47wy7yb
+Rh4Di2aPRcnhq1fuKv4Ip4zRxGJYpz34C0EhA3rZ7ZS67x8/7+dp+zrlHrwy7BHmYd+m+cR2dXZ
eDdmYmCs9lvVEahUXfuIKlPYSL7hzxyUiovgQcA62gXgH23CXeroxT/W3EviJPriBL2ie8Qd0Uz5
BpWnitM3NREDpKoD0o1ith/cgBFLNQM++l4+qrWwmySQ52z/RHlFWbzC0DgS1z8O5HawZTvZGq2p
ilsIGbnvqkSa8Lz35uqOzuHqRNoRQL4bx/C84SxG4ZZcLI36V8RDThDJ2V0Pka7F7ESPbP8XN+9e
qp78XwpITTI+IlhTVx6ee08rIFTGp7v5dLgRBCLMF7DaXfWD60hbrfRuIOLFNhAZQ6SW/srTsK/9
2ECW+jBd0Uz5VCj4w8EkFL6/A7wGcNLIGEFQziG+JkB5863e2eCb080QG+re3vRerJubesYjgfJV
HMUxD5OALyBjuCR155YnjMGc9UtEEkh87mmnr9NB4DM+4U+XELLqC2V3fOIP3iwvy2Iel+XeVQvt
7aCQXQbkJ2Q/PhSNorTrJOsRGxW7cb3hCP/B7WTCM6pZrF38vUFhdCDobgIWiw45lG4C0M8/j7P8
kpZeufAIvp6S8C1SrDkX20tBrFztpv0PDZPR8xfwbF+qL0NnP9jYHofO0bpxkXt7Zz0z/bl1o+cV
vXPOI4lLFVUI9vhhoaGZ7Dy/xfMaXk4x1e4vVVtCNGPe8fkWmk0o1Q8et8laM+ED4w3Do/FzPFVm
UiHhihH1paFaJIBithX0YBz94u8MXu+nL816bGj8skiqPIyg2Pz1nMFrmZMX/QzQnXzh8RbJ6sTC
r2V3gFywbuTxUjZWxkcSMLE6XDLyOPfBrGy5qCGGpOr+ItRbeVJVrLK3N4oTjcqYyxI/2tn+SMUB
SQZ56FG81/FCPjNe29c6Tzd+MT2fZJ2D+PoVgSA4FrKzvJTBBRkNACDZsDI2cJfw2B54H9o4cPIx
27iGu6rhIBZDF5BmLwBYyiPQGGVo9duE7ZnX5ys2SVe0Q300pZJZD27Qa0AGGQzUNHKxs9KJyF4R
nkh9SupjHU/1RIsfTr/oJ3CE/eP9GutRX7qhPwPGYUQtnWD9FwhsnYanBOcfCukQTk+MRJ4Bfar4
YQ1pzOidL1ADlx3v1MCkm1w6UqfrpDoLZu+QeC0uE26pBEtkZL0ytkIPfMErQc6H19JNvh4fefad
NKcV2QV/cmiqt7wTYAWePZaXSsgcnTzN4zuuA2uk0uibFNFzXpb9objm83uP9JAlDVGQZlBXpYYV
wDy+RTPkLhnfPYr+dzkXWKmEKe5dF+2lgSEISLtacyy9s5WN/a0xgpRuLHPKZO8XJQ+K65bo6wmy
8KNOEY8PRC8nYoR0Okpit5aiIb1OmNGp5rlmUN/kVICK3kH/OYV0QpQE7nl+UgHZy6y5YIvXxKC1
A06QgUDYLjNTHUBKdxPmEavuxT/ljctZcD201jjRbhfkBKWOauPX6bqdGc71JAHfjtqbBKSZou8C
+JRfKx97y+ZU+1XAXiUKhibZy1MEGIbEcyNy53y/jKEf3rHtIDAzetiEojL0moW1+GSM/iYEKre9
+tzPES7KjIDkN0rBfPHN8MlfsDTpdJ1j/6akbYGrB+/qyaIlUOB7Kd13ZLATqMhPJlRZtBkT8KYJ
/ISRXOKi9wIvLLEkBl9gP+V0JFUYaZWATV6tt1I66US4xDoku75KtQWLC92rk9ftF9eF4t8gcZ6C
hiVu99RTzA5mhZ3oKyRPYUTWwGKBkSAqIFwTXX3Em0zwOw6jsZcEiZuaEu2FQCbEfmN6TxkJK/B6
Woq4EviyoL0yLhNhLtDgBt2gZQ3WidN60ftNXrpS/tMahJ5eROAOF/I0f2XmNgoilzWOPYSyCSLl
fofD7u4ddtnHYMSdCiOYdoiG5Z3O20mXhCoYsFQwUe0n5A3/3ZqaFN3ay1NpmCz1j6TACfYDGMWD
DypK5I3QhNAI2iFWiM5f7PhGqb52oa5/6/Kod96/pY8RX1/c+vk9YUFAbD9ajdwXTMfhWCOGmcZt
R/FxcbDbRrqwSGcHvozHqPe/277wE7l99xr4ufXU+PcTz8soopIxTTguuWUKQhpBW9TvZUrspR1h
0xjoOAMVK+c/v2J0KS9XBKVLbKGL5FO32oHG3dEcjftixKqKyFZGb4pJf9GwPgDTaDutjH79PEcg
haVtbls28n1VpP0jy0tpmgNM1ganMsH4H5mltVOo/4whWKYhHizMnWN3neAfuCxVCBfpqPXBSG+/
Vbr2/0l4/1M7XR69W3dA1hmqsXDwjmx+b1qgaf8bRolyGZA/2OBDBWk/eea42jQB8ZCAYZUGb093
NfrfBzJeKgJ8L70b/H5HDEIlEBqylYddzWVCW7VjyV7h+O26nt5VIYwacY/gDjnldrrhyJ1BJsTl
qq+bqU65s3wdoxToHcIr/Rjgp66DDDA2SE3Ms2qy3MzuVJwA27BpcvY34/WVJ+Hdv3SieAQiOJrN
E1gZjOYaVQRCyza0+uTamv7BcIKKKdGjppx5p+mWF2u968xBV1CEIcxUV7NatrPaprU7Es1p12f2
n57OJFWMe2NX6+CjcsnT11EwZyrjlvh7qY3p7R1Cb9vrQ788vpniqEcc/APys5uGhDZP4CsBhFHB
7tAdj8D9urNhppeASQ7goDWnqT/SZLPlrmNRkv/Sp6VJ2PgsqAumBu0olT3Nwq7s8c+oZ5G5Tbuw
9FSanAe2Cofu+8Ts2yqlrFwTNs7kxguOgB5VhWhcQLcDkZ+hA0P1wofaJmC20CwgKIcccxO7qOii
CZoK5rbbR/69GBrtXlY55dT72NV0vRbbJXKNOiNrxov1I4gERV/0L59vCFDvJixS+PMVXtgkb/mD
S4u5Cel9chAsq0cUwxOQRzPjr21lijURlu3rl5HQAtmCgQzZSV4sLBiJx7ihGWE5WbbVKYgBPc+l
kuXK6MaMvH0oxG9V5tdBwgXlT2WBvCZIuXpMVK/zCLyL82acrnyKPaQTLRnyTmBapie/wUBF/RUa
GcRE+ae3kQLB5yWgwirm3qt9Q00sIBePMy92V5ssPp+yKK7Pvhjt1i77m6qDETvPdwOm0kztUcGf
nKGpCoXEHRZpzpJf12xjixSa4Sdx4dWBTZ9XYAUMBmNnfRQEVlPQs5GMhZEuD1avq+Hz9Ja6Jrvz
iy9vqJAmoNSVZg6RmxUV6X31ROvypJxfmwrIlNbnWe6TaV1tKtIpZaWa4i6aENGYnepYjJ608y3I
tyOyCan4E/be0aLrJDc7qzYarEt5EUvivh3Po4SNHq+flhSmIk1zZnEwtVzahBfxYLTWZJ6yGgQF
VM/BaYd3Yk8PhwhOgIJ1c/3C0hySrhAgAPeh+ZjyxhRiiN8Jo8eo5ZsnUX6OwJyB+lXgeRIwSQ75
tjVdssdr8QSTCxO5k66ADapOXVPSOfS3MASDgjrv5OBmKEE+JcWcLNxh804Jdx8jAwTro5wnpdAH
saCyW/NP9XZgNMf39JNVMae+vyhzcRgVzFkuPQXl03gc3EgH3CDvsGjGX2HGHBuG9f0WGKTP/oU9
H+u5swa93hSmUwcstFpiH0gbxQkYO6DfstxLSxJ9xtTcv+KZ+sjp/QmE6L6AG72HH/4q5TS8YHXq
t51MZZtPmlqojHO4eklRaXJqLksk72hrqN1qPTDtQI0m6AJgFpztPJGFOtzLEKBIWPu8aOH2ggox
ChLOHSUk7KvBk7caU2pT+8AuvYuql7dOOPR2TAxPjEH6qEt9pq0ZVjhuTLjKqNMKyowWgy28zkeL
8w6GaKmTfEIXVY6VhDJsEJN9BrAxwVshA0muzobOfE8YrtFeR5D5xT4zU2k4NGKFQ9tPmNG5l5oX
hwST7N1TSniTUsHRspdhaTFankjC133xQeG5ZFIkxXe1EWTc/Zywvoe2NUEIo3RQKpWh2AHNb3Nc
d3wS1bsd5k4B2n92Hd9Phu2WVgDIGiRQDPL5K9x3NTQvO6MgtVt74W3IJVNcMTR0A119vn/rykQ4
usX+jxHPeIrvPBxHnQNGKDa0bN+Y3uq3mpoMLUgmwNqL5M3CjZVjeVkIu+zGzS8gZTdShmxfw4J8
JGi27ixIJCHF7/s50iUL3i02Yt0Q5dCaGqJMDmzeiLlljAl9rk2HZSOskOYuUTumwZT7B2pcosFI
xEU3fBrh9ZSSUuHm729EFfG7iISHpV+ianFRFUI6Ekz6yTTU51qoP0m0F/RAjNzfeZp2IBmqW4pq
AWvlc6DbCEsddVdFRr/0SsfKmYkaxJa9yTRWjJrlIJMF8Lw5loy3n9LJarfeMo7VbWQmSb5mp0w9
CXtJo+IXe11WKNQMGMLXoWP0WT2QwYvCLHoGR5NygTKpdFtcCR8uWSeK+BgX7JbDOy3JAod+leXJ
wWKvHTy+o7TWEypB3V+u897xZjMbC40mvSJ54tpGsrHy5eeH5sXx8VKp4DvfvXvT3GmlcI+ujLqh
KjSaqE7o74HS2SuY9dPeL0pqfGICXBf7sbXqoSgi0KrZMsA0JYbze6jCw1nIOgi5x9pX1dF+Z541
tYX2d0TUgFTw9rgMPLOqEqXDosVDX7d4eskM1zgoIpOJ4mXP8MdJvs1DaFRomly5qzvXcEgGmjlz
5THCAX4j91SC2T/qXFsbVGTK+KuhiT7OFgdFIvzHyyK3eEsGG6Dol/g+95wPv8ruyK5MFy8EFghK
ajUq//FEN/As4Xbi4p1HvTFEAVrQfHqWUPzWHqZR3TUmPOf/aZhIMGmLyXUzeNr8i8sVS4vUVyl7
L9Xz/kdtPZ2G12e6e3K+bq3B6biQH3Tsi6Kr6thYEfA0rr5LK7DBLHyGT0W1LNz1UK+VJIA9U+qo
CpM1eIli5XgHqUXPKRHqknewPYkCfjRvweAhEaeJyosTsc+KumpYlEzC/GP2RVnd1BsBKQcZJ5rv
dfR852sxLsdeOxGlA8gxlVyrjJuVMV50oJph/WWgwhh3WMhDmjbO70CBminB/AtG6q6FKtUfEpq2
oAHomCsmTPsyAg/XFoGVukcJIfJyz1lZ84hV5zjG8JO6WFS+k7cFWnAsydKDCKYi1l3zXAlikyhE
GQToakIlKs1R7145HpZQK3kl1p5NiQ3t5HanF4VGf0j3YAf5Uvz76AuQpnumc7VVh/T92M4KlTPs
8k5f45fNxPOi0XcEqIm2kjeG1NlfBsGp3W6d9eEzXCcLPepxbrTOQAQLgVuDxGJy+PGlcJnFu6Up
acZvkVDjQhR+k/37W5HfJzWhXAZgQ/vk+RSTwZmdcvFsjWnhovzXptpsXZgVMAknPJAL3FoJUesM
n2YO/w1EKSPkSvItMUKc00NRK17+FLhTqIrvAwOLjZQBKRMBxQfT6IaOKCDxIxkPPAhrlyv+Z1l1
0uRuuepSop/l1F2cd3RM5xPcA+J7zFyqprEEQvPt3S+QU/ArdFxjHpHd3Bbjpr2cJzu0mhRCn6aJ
VIRAIjj30KZ8/16TaeQQR/LwJgYoeLZBdZiRlLcCf994deC/M83upDYs3XatO2B8dHHjMeGaadSX
4k/PUeyzJlZQ1adw9nZcrOYKqxBMjk7Asxmcv/++xD15zjCBKwmXJXgeF0L9nwXUhOArpu3brlw8
vncb0gB78VSumukV2fGdUzIR5Pq8DEhV2SQc20lvAQaYX1d/FjaEcC4vKs5OuGQLoMvbVbeFwruN
i7qUBijGLh+lCaTrbbbYimP9xL2UXOXAU7Ec2TJAokF6GVxwUIKJPNiCA4dMjpUKhXtPtLEphgF/
mSIkgBdhxZmc9Bkbh3ODiJw+6aB/MvBrhPsghoK4S5cw+QBVIPn1wfXr4ZpyowO/Nij4BP42CbMw
LiaPwi99J/P7gva03rsan1oznZtvM5vMIz23tOAm61qp3Ox7X/tkOv4E2e/wenTi0lA2KXgyntt4
f3R+sGEb8kVs59njAcq0VYcUhkuyJXbeBHAFMKNajxNmN68MTLZE6W7zVGtJr76bnHOY4ugWPkKM
VDEL6+4kIL26utNVUi2rmJcKXhM4+ryTkmwGZRejy5/VRhIi5nMdZpqQ1//yk5Y3FYerC4Vg1G5w
r0CWxIB7xzmFbUZuXtrxs94k69qeS+ncMnMDEvNagz/FhnSjfRNyLbdNLzhvVdHOlkyHjw3fkGxl
+peOh3ho3sSTwTdBkm5JHcfEm14q2ekomWxOaGnogIcA4Sn2PX876+hpQIIkewVNK3BnZc5AyANB
+fwJMHQKappOZUY01SO0uZqUArtq8DXPpMM7x1V9bejgWHWML+RZWuBOk9eGN2XljJeE3Tu6Vp5d
HApdsxjbLvgLnpGltbmDmcmRVbg/CU9caRXq51mdspPLr50ZLQn6iIa2lFUPZ9+G5aArP2GC0x+x
8Iz/ZMVtwRHmulcY60DFCUOLHqWTI/wmAKqmG5AQEzK5IHiHuQ3jS5PQgsD6C1gLyQJ7cgpFz4ZX
tZMzPsOC0v1XrtvF1woJuzcFNyNWCxXXMx/hi5uIwCkpTC9tagvglb+O33KOIUlUnUK3ZPi8OEjl
Lue7WCskG1eNNwU52mgoT2a9AhzvJL5vYfuZS5/0eHvF2KQBOuRrSJaPWEKKRW6w/fZsZWzse6e9
iuyv3HguY3pbhEh/xKTl8sZRfJXtibzf8DFOMaY8ZoybQDi/KB+U2kUguKF8uk3SxUCQntrGNTU5
3nT/5UtSPcbdXdI0v0+gHnP6xCYGCoB4o5RFYMU823uKdOYh95j1hopcMD3S/n6LAPz5Ei7Py4KB
HAoxBrAWasoYeTIUViFqyaffxHTssx9WKWdtOkhDn3CfDJt7DbV7KQh9M1RPK4kn3U2FpfvPLH6k
LpFNIafhTemYol4AbKIIggBPQBbAFNpVaAJ7NBjHHWyxrAPDbumz6+VMP14at+kt9ydS3kqi22CM
GUhIRvw4tp08klirPqyOz/h11hfMBZ8S5BnvmSA3Pd0kTgD3dlLpeWntbUplT+o/0KAds5c9m0VM
63W9kzYSHAoNVHwglOawygWvH/M9kHyau27pdDtYd8SdFm0Ugzf29Kapqgsq8Slpjvg7bkJyMuU3
WF2X47hS/5PiBbvQHvjT3J2Xdxlsu1wuVYMwWaZIzHy0wu9731DBNLBhMNUD1DI0J2DKGRD4s6eE
Y8uVPQ+yQKuqgkdr1CK8eF/lDHqPUgdn/wpqcPbyKwML9QlvwcI0i522pT/ttOCN8n6BvnoRdues
aL7v6lnr0hh3tNvnqfZh7/Bj0HnDmHNv7N2ug8fkcgwM19jyLxP4ZaDt6AVo5OrOupfv56OvEfJJ
GAmtdR3+wBwOPNJI4cWYUGX8FxuePzHezjo03piKoB0phB54CqJgP/77pkaRemBZTbnYN1ER0TDe
maXTk0eCVBaTYvgzxNvPU+t+gRvYhGsyxILpgGcxSAQsqq/uYog2BSFNOvGLQ3gQW1YbnkFfeCAu
RK1EvRxTS+opPmm4rG1aNWnAVgOycVUWNzf1+aBxbPML14/7+GfP7YRxsK71PiBEXvYFwvA2ZneN
kGiNrCNb0hFRUjPQU9ZDxLyiZlI5yQ4Jc41P77OZtqenxF9Kxkp78Gelm1k61GY8ivkH14Gy5Ad8
oRiIlk0Ooeu1EWu8IWuv9IG/9GufLufp/FwB3l6kxxB4Tpfxs5Dagdh1ZImmd1ObCBhj23RDVP0f
eDGb7rB9evB5btXEfsLAM3ERKTIKovzB6iJPlTxBT1bHWUsRIKVsm1dQgQiVH7TseIYV8G9u7W96
B+F8HgN6Heb/1ch1Shvm3m+2ItDUGk/E1FGhjbcFdYsAqp29GFlqR5Y0FSJyqXVsK4hp0uU4OQwf
Yxikq6JDo59QOqUcMd9Q4fyHOcpvbclQjEGjz/+/ecy9sqNnM362/ERAWx/4BwOiEvcFNYhWGpkN
tf6SWIi1JkPyh4LS5KblOmdDwqGo2b7vqdhk6wrBhfEOKCMCPnxnqiqGmFB3Swjm5tT+0LaXeEmk
MeHFqpjwKL9xjeh9ceI/4cDBs0HubJzWKGG8piI7vo9UvgK+fk/eZ5EIdHX5dVSp3ahTdrMK/OYU
kzQlDR9UusrdnesWj9/E4Z5nFqcfsI+z5UAgWA7FR5Wyvzs5l5T//2zsvpqj5HwLwkpWkYAtGmlm
j3/MsZVzVgDoc016a01l9/qa7u8gioobxayRr7OcF4bwN1mwvrDelHyOO/MHcGDCaODekqsrktAc
ompHwkpgcktRT5BVTROk/+tLqhOt/9PCoNf1+e7n0wpVYq7DYLPZfaP+9q/jOOt04D58O91X/lae
SLXjAGEzj7KqM1a/QDoLN7C0dRL9it1BjOZWxD+NTUPd276UTAu+VIQiyEzhEamJ7w1P9tJcAZMv
fUogUQ0zLSFyGZMlI6cQxL3cGESXACjbsESnqTlTlHiWkbmacsYrunIgxeLPvOyRtndNlUqHpb8J
soZffUwdN/0KtNj5rNMS3iJivX2KefMqWgng1Rmeho958lQI27TUclxZpqJrk8EeaqYt5/tYNNFl
im0GuPxmqqooxNcK0GPsEs5NKCnprcdIBxo9gcIOqIjEFco2QxBMU2F9lfkofmv24PAi+gY86EG2
qfOaoU4Yr/dSWoSWLFeEEto8kenycNCnCQ/bNi/Ti5NruxJOCFo6Pbk1iCH0mppdeXlXUaR+o8th
XGSZccRfyIJqNqo6zuzLNiEZgvlb5YxnCAmDwF9oZGZi+jr6yX2Hk6fPdU5llwmaFXZh2Pak0bLg
oQXfPbtMGR7jjeuj3mPHFq2qsW5Eq+yJP7KxamvaAweN3R8zL6ZBU6IOHu5DwrIFTgdRP7qsJX0n
FyqfBs87uMZTPdOmgFMxCjo3aOMA9DFk+BNGaWTdxJ59SYsjrUp5mz1zPN53TprwPYRaz0JpYOd+
Idmr6DjI6FXpElHyu1tA7qt2epV3MhGxs0lnkuSG4guIg2BPLGaCR/v1UJOJsDfjbefMNZtTCHQ6
OChL5+HhZVSMZWVPIC6PE880M+EkkyTIGKdtjAjUWD5+C0bT5VhFRPUYp8oIx0x8OAJlCFFMQTwD
Jlwh4+8eBS0DVlF8lTLGUKxP6l7i9/w27LEitQeq5MDd85SO3rM1h8wMambl3YEA7J35C2m88vF5
0shI6yRH3e2VIVUtqv2jSsdxlfVvJC1N9ysQUYAYFljRtk8RaiRgstifHSkjbUtGLAEMmd6hC5wZ
j19zJ+3L5lTCtfULR4CL9t5nNVzqHTGNtmUYK51IkoAmaP7TjfT89inQa9u9MtjsyeQFXisYkFqw
sje+kf7VaDke3pPXqd8DxePGQe062K5iIGrC1AJz174tJ7M8a8WtKqIP0qduDpwQexQl0nPLDsRc
87mNG5lxgWrgRlmpcDobLigLZ/4luixWVwYba4VF35LoGADcsdcYwddg5paYbwZiH80NGdcAzvOV
j+j3Ui6JZFMEvTLZ6J3vBXY4ldM1my0ZIoTR6XxNGzu5DuC+V52Fgtsn/0+MngVHigbUc6K/Jnuh
V+CabIkI5yXiHsWo7XvYyOZBhesBb4eY1MsdHIT1MGEg8xpgipHYP1U1Yhz+Q4DmoRuzcVWCoCUe
WhsNjIH44RroKRdgg+4Vib4D4G8v6UT+qBRygAF/T98A9x6MmDwNnFNI0+WgwBBEGLQAOKWTTvPR
L4Kk3bE0Kaa+JEm/PM6Y/bv2cQH+eUfSjawpT/ZlUqPoIUGifO7ITBtxhUZAiWMPWgEQhHm/iYSE
UPnjXfOzN1549Skn84NZ4LvjeB/41BLCIOhDB7zKVkRbNuudu4FKjCjqcG6376tqaWgU9p6tORpl
V3q83HCMOZdp2THvxcoD75aWuccZkvp5P8yi0Q/TSGDIThYvLOKLvKbKkNzQP1uy3IpUdUOh5eP+
5TWkgvTf+xPIfvs1NW0HtjHicdRjKvOlLoShPavypnk8ZyvWfdhGJMh9+rySizVjJXq/+eMDbwoC
02NAdirHYCg5Ea/8epOBU2i0G4pMtjA0bqjQjshRPjQiYUPH3ES9EFv2pAlgpGp4jH+umWWX/+yv
RD0Z2iQ3hfofY3u7ck+Df5rv9hevyZmlIcn9+rQzo7W4VtRtCIKO49RQNchZjyroi8vx5yU+TK40
HKc318TqUlA1BofCxf8Ddt4K+eb9iwmA2OZL760bhD8o0HoOgkUk2mp898wtMSl9O5K6dtoDs46I
oVl4Cm9wobDLME0YDAdQvFTFK3cMHWyJkQke4ZBWsNunTvtjnpvcHRIoc+9w2XJjmcm51X5JBABX
XEJbBPwR8CF1LEckF+MaQJlP4UMWVNOKsjpfx2mH855etysF8heASgzqMLPUhYMnSUK2He7wPCNq
juozkwtMsFLKupoXUGQP+F0/NmqrAkdzood7sPvnZ4ztKlolRAr+hr3/4F0jYbYpUQjZuI/QK622
fEdjHqnWhvr0a4uTIki+Ppm/UvfGi7eAsbzTDTWQJplpySvqGvE707RE2aMox+xc6AbCWv2q0MUX
n9SBDLp+6i45tLexBetugHg9YzfuxW0Cmy3F2jEVSpD+naxCdf9jH1UrtKBN+OXy9DdLiilhfCiQ
ehqzsu6Cdl4ZommbiqcdZQ6LYsmMSPjF34iuce6t5BHWPq37DN2HGdrksfvFCVE0pBWEOl+QnxDk
UmH1iGnmbpwjScdxrR0ANdii008oEl7F/g0hquifP2VxeK9EjSi/c6AsrjHBSFmXnSch5kDYsvMR
/+4NSPJzhKKktIlSMILCYl/HnvDTUr9lcDuxtRWzRII1l7NUL3lcpGrrJck0Dic5ZQQvEisC+XzF
ue48ovxLmZYnHIqxz9GoeNU6wwQMHtooLV48Mu3hm0yhgETjYZMQhaCIf+KsvxPDOe9JahITnfJe
n0tur+05iSzNqKnTD+yeXEIEbGJLcQfjaZeKscQKfL4xM6Jjm37xIhW4H9Cuq6dkKLdFnoq4Z7Fa
9DJ8jgGSndyUXhRbIBe7AndGk+JQs/gDwKwqpEygIEjx5fNe+B4uWK+daMCIl9Xz8H0+itST12Nt
Eha2vgxkhsJDF1BBLHD72arcAAc2RkmR1K4vWirY0EFjo5YPEnucBtA3yF/aSgUEc9o84R24d/2U
7+9gGzEgLAI5bqUjoCH+QylA7RXLOSqGur71lAYAcdLAM16XU0bAmX4IBky4pTZL6VEJuqu8Qu3F
MrhBJ3m7723u1rdIa2BwS1u3St+CYgtt2aMB7Mtfi71LO858M5aeKIHIMo+Pcz+gfDYFG+w8DTjj
giQrAoeCqgYtcJQOdLFD/ojC+8KvqVcRV1GhLOkEzLRrD303GYI1HaQ8MR3OjgF26yHFaXyLfREH
gPH6lt+EPSlePLjX58xiDjwLvvbJZHm393QctVy5do6wEMN99etBoJCE5AiEDmi8wXA0APoyLq4G
kDt1IuY7RQ723KKkdcbAAoP17cNSRwfqY+Q/KuO8MfFj/2DqoDfNBXswgXk82BrzmliLFkYNJn6I
jFRjYh0oUJdL64s6rO0O3MhdovdOxM6TLW7L/Aqs7kGsQY6GqCr+R+HSo5IcoiHeqVM5bdOdr2oJ
AYx3SMTs83PNtL6utcei9vD+54YUXRbYftodVewI1jYJri5YFHIF2YDZ+2nf+LgnPWFJ/lBKImHF
u57GhDcBJF27ggRYG8jvy0mTzRPXWhYVrXy1aGAOcR2oKkSlBCSOR3UVmllVtl07G9YqjfQ9SA10
lWE92AB8tEc8pAGRfAk4wp51kWvEBRMeedBJIqWxc2t7f47mNhafmANpfhdkDYlaHqEP5bO0fgLP
gJe9nKv2arAk7Qr9xVZeEXF9C5RJo3ZOaJkvRuldE5oiGAwfxyi1yiz8cjNBr8DNN7w3W3raJNcU
EvFPYfHKSyQq7m285wQgeuz2KNO3A60eRw8b2A/GzCP/SP+1COH9L2qoYhCnNnt0RgiuMGBEM/Xj
Rjqgit6PMkuvT4AbHVY4SYzXbIdj12k7AjMxfrLsXJ6azvEzCEdYiNuPGQKfaoVmO1DSToZAsWZw
haDse6/edOCh93xiL94iaKhMWvG0IjJp+FGbr8hoRw6tawvDAdTRzF3ZFdV+du0jUZ7uWC7++Qgd
wmR8a1gcSx7Ettt0gQzipVA8pd7LfU56hZhKK3sbDSKltWHmTxxk14rYJZ4tWjOiRF79M1t750N4
IWmObX8oQ2irQqxTFHLi1Qjo0ieqWxaGY0TM9SxKzTDRwM2j1mZ6MgoU727hCCTEjx7rUrlF2GOZ
hsuiAe3GGbp+l/ppQxtYwHzcW9zTILufTPzDSds9eLhNyDyb26XthGTcrEFZY6lhb0wWp1lnxbGp
YVIUYpnz+uXJjoyE2tjjcErw8z8kNpFFtpIJnuTtWs0C8orBlcTV4LC/RqhtG/GNuvq56DAJYThm
WZUmSHxefq8FomW/mxp2cJhf1HTAkgBgt1+GTvVh3Xo5Y/qrXuCAxhu0CGziFURBNeoSzaf3Myib
GoVdI/n5lidaiAqivrwFZSF++lgDftP7a1DZ1mataZvnwPpnzoKVxAQyWbZBD5uqFIetMrx+Ofu+
i1eS+k9jTz6X47VSQNQUTYIQ1qA+KVav4SW91IFxLyIMC1Ung11GcZz22fGaQe0sLkaXZDIC5xlc
xflzAK9tXGRGeM7hOQfFZ2nOmGqYLEc4TOT7/92mOZE6cgnn3d1Lqwnd9uSqrkxzDC1lPsRtMG9s
iDczkcy054w+yHYnggff/1sQKu30jwX68qgiOiCTidG/04DlOZTf74AGzcjlqstUPP3uAjbyx/Gj
85xKtqooYTlYNXE8CGn0ExMIOVxjWC8NPOhli5mhogVH8pcqJyYVNf5MgqFM1knNvMNbMMf8nMgD
CiCiQCJkybdnPJMrsQJl1ygx5uV2acM8XsThr5DvAMRMjekMjOMDl6ro/gHWBfsQzhZgjJzsd8PT
IXJi5It04kGD3DKxmYHB4deuBco/RyIamhzbz4nD+/7tMmUUzfy5QIMzj0awFATDHReyrfAa8PQQ
bilsBGvHm1iItCmjZ6ERqjTu9AmfTJJIC7RFldtZ1GTGLCXP+8PA3i5h09wBRdCrQhDka74c6V33
KoYKLQZ42FdmC9/dt1fmziWPeA4L51GtxOM8jZtQ7wuwuuzHUAqvTxTXFFAqvmgbPfKK3mWd+GCo
ECDsFWor4+K19nlWCnbPPblzHCLROYBRCzrf16YxjvdlefJjtsQkt6rQinLKibBfkXUiBhEeQzEM
+RcpnZZrmoqOjZVmP+VAg6zFb+shSImnJAzZlG9SI34uIEuuNfI44NJ3Q89be1+WRNkGRo+jB9ER
LWtqGDkuSxmAko9Pu0/lLG+/vIaxncLZOGh4vl9iZ88BMzJLEOtC+rX16fJOwexq6z3WUSKaRGBN
VusYNl9gm0REvDCmMHITxYQtFXfHWDyTyOu/eKtEPM6AY6Mu7cmlZEpaxPozx9G+Shscu+FMRDIx
MBupn+uc8T+/rehvdb6mf9/y+JtiRthRNDAvP6URhRhUE2QRzIauE7fLbftutjhyvh4bEGnw7+aI
xwJ85j9oQRY4KwMz23nNOKDvYmNWY823l00coAzRDMZPK2IxN6hhhtLsHWK7PJDGwCj73Tju2CSV
tSNkp0sCOxv5plTI2k0HDrsdLupKwX/O8yckt303AxbNIKlpBkrGeEj45MFqo5KXTqDEHrCs8Jpf
fxSf5R3qN5ZwVq/poVVNHnD9qhaHEex5Cm9iHERCEqVV1ZAwm8uzgMOHCbYa3n9lynGXckQUbPIZ
HjgUBMpJe1tHscjJRkI54dP8o40hhxWqEBinK0XVPYNrwmbCnFqS2PFpBmUSkAu9xptGc13Dv4H1
5jex9z/MfVlwQpNsblaG8riENuRUAQJoDv6CiW1mkHEUI5UjakrwVhYJGGq9e0CH7rMsY8ajSmvi
dorz0/hdCF0kyafU4Jbyr54wRbbzDSFI5u83k7VE+9ndJLmgNI+4CnnIgQfjrmhbAE693nRNou+p
P7fqFr0YSaOSiSICI2GllD+dD3rVY/pPNN1+A1eBUBghNFRQhkZDPuFpHzoBeGeyixNz7jrtyFKH
R4GXPFpnCwFxsApH04jVvNquV6JbfRzGhopK9GmGWox1IHiUncsSS44WAQfRShG2eBTiGZrBqB4w
zN9p8iHv5GQvjADinsIuoywtl0DBvJ4OOuEawsuaHjukb55qTXmPWMxNg2QGeuno1BLiPAt0abWA
01aZvBfrHLg30VabxJnGOaE3OYLh/fRSEnkFl746U9p7jBazeMPpRVZBm8KQhc7uXvwGuKWhUkAq
XlEXU14qVqk78A2SnZNK4zgrW6I5TvX2R5oBhMBjXWooROCXVsZ/KKSJAwpvQeyNMhzd6uFMKZ3w
iOr+a5ACSX9Gso0xgNRFnbghlZ9dIpuW7GhYIvIUto47o6pm+nq3ehgIlSjpVUxBrJWSFajnQznb
J+BZwSk0gpZ8P/mXVAqXJI3FrBDnbf4Wihbp3UYJ6hZQYCvOZnHxKTZlrpEmwkPn2OzxfTpIJsNX
42MBS2GY3YPOYXCbaFda/3QvLQWvX5Pv9hDJG92xipm3GRoqNXLThCsuLbx5OKTvpyNviYlBW2Dg
MNDI69bOIKqtTLYlUltsZEnYBeOex58gkonVKMheeRdbTthx3F5qNGRYfbUFhr/KApVyxX2GbvXv
6N83fJBuYijSUu1SUloV0cm/XDsqiOR+ngJ7+aTyQK84l2wLZlPM+9/ZTyz7tRNuTNYCa3nPPaF4
V4ChsBdEaUQiLuNIveiEQI3JWNGUwQ/lSTDHTW9TyRFUrjlhd02fGXo7GrlLPCU+10bobD018aDJ
X26qh+wgKdtVYjc34YwLZAxrw12xkAZp0qr3jJPa+0iyvQzeUifLRZZhlolsCkz2Puo4jWzsq3y0
6qiG3t/+W4j8e2mE+Qo9Fyh+EaNYxqK1XRimD3Rx3OUXzYnoxA543bqxnMOg9PR8HMQAajz6mgJ1
bq+qQHDaPe2LqY0nGEgv63odkzn+6qWKq60l3XOysoRFPp/OK3QeIQ/Ow9/uVj2Vdj9GvOHFvXFW
4+qJNcoTjGH/+cuaUp/zlgvIDD/rdAL+HDLtjxyDbBvK4u2Sc/+5PViVBamt4psXVSNdf2EsTb54
P/8LNO6ZQoyD1N665dT0KjBCl9zPAJ/gUMt0taUzRFH4fVRB0qXifoXIPYWzcthHBklnz6mXIzT6
ZR/vDXrausDkztGD79tQtcOUd7YVhuL84iIQ8I3LuvvISUNeAAF3AYE/s41JBlF0CeeC1k9vS/Hs
m9eqOCmL+uzJ/GErT8i+0pb0XSXsULitwysaeleI1aBWP1LuKkqRsD7Sl2Wln5aLnA0fS6/XWl+6
EAl03vYdzIFtFBa3jxZUyQXcCpVC4aLGrBG2IKC/A9IALEk3LCtB/K+FUheGyxOoYnzS3eaRC1ub
kJYoCbgfznXruretyoAeEgISJV8KrrVDnmCyPqyKMvQZJq3akjWjQJRnYM3NwaZxfkPjk1IA/jXy
tQ2AocGrNTS7VizRSGITLReTcIC1t62M7XO5odC2ZHidp+4TEyTEiJ5xhcb492MZYgQfu9vMbu3w
x457d/iBkOMxYcXpBsNpPPeih1Y4C3WZQLN3PbYSGamXcxhqo9nChfJczBxGx+3wHtj1wUtwfyT+
X04Mvb9gfjww4lhgrjLZP2Veqi1woxXXv0RQH5u9PIkeJhy2ysmOpYieZRTvxJvdS3kImUD0AFFB
0ByWnUBfD2Enho3uHENjf0pQlrVmZYYDfloWIacWmYaXPylurmzhdjtGkGCQZM3gtO41DIPx0Nmw
pCft5fu+VQMmNV4theJeVSVHKKzp3Gf6/Zl6Z4gniIWGNfIUL6zq80YVdBa5aRzcji0K3odSMpRb
GiLnV0vI6DlMcljzQxHh1M8WcDMbLmAZyI28O0rdAkKpPWdFn0BzHj9VUsripwKPWHnvc1WkjL2V
A2ZCkoR4ff64A9J0ZbipHRW2YXo3LczjeO0G9kjJTBcBBlqOrCYrMFg3KROqNzkQgFYInb436hHs
SvkL59RR67Od9EqsN2xRL+z67Wogy/9dZYGz9i9o1gmThUedtHRElsbN6ac59kSx96rsQI4Uzmm4
e5wO7/aZTq5iPZjYKCCxmwEAI/fF7Exfb1ON+znOQjwsZruLmWFwkJQ+mzCkslm+qxLuKSDwJ/u9
YSBnA+yM304FOLy/IuJ4zkUVY0Tlea53ATPwR9wVVXOV1MAKaLzPHuddIjyE9hmcZWjU80PPbk4E
+DkDYJD+n0okqNR+HjdL2O7bt6JPalrr5DmZ5iyNvQ3/G2C9OrVIMhKeaEB5uXQnNgG8HrYbpyPV
TXZH42OYqtUmalgZ+4WKz9qsmkKxOsUbE+6PPKMrqTHC2DejAtiLKrB1crJWeVoTt52hHrGaqqO8
wckIyxVDTOBVfnLSjdaarECXfj4T7wpNa+LyC6VWm16n0f0Bz/fEmtRgIiM5DzS7k7C6MMhWDtDn
yQuWwaoAZHxusY30E5HW0iHxwpcKjbKmNhl68ynFDtq0NlxgNTjZo8HXRAi2g/nfI1SYHrDmcVcL
bxgQMr/HqRpVRBjS7gfQ8mAxy93qu2dC8yDkX7J274ifxEB/Nx+Sm6Hz5vXXqtg59X5tx4FpfWdM
y2A+sX/x080L3j9+ojltX0QCfQjVPa7Ly4/pk72MKbk/8zoANxLJOkOJvnzUDBdWOE2NMj2ANKsC
TQPg6jySBtbqeRw86tSg3/sdqb8hA3c7FgoXH/xu9clchZZgMC8gR8LLh0fr8UVBuy9OecR7KP4s
gs+yZDvoh7rHx6A8r88FJ2qavaHWC0Zq1+iEZcnb07bYlsD1fRWqpYRNbWIJCpg5UHenes8arotQ
ZIC2scZG9gtJ6u3hBcopAVa4ELQi/5WrLG2P+fjWw3MQ0n6pJ+LAcFkkH3Y/mq6DsmpyW1Vz6GmK
9xbmH7yTOMOTMrEpyTiVA5yNuVjAeOtY0DfRKRoR+IJcZRw4BVplIX/u5rsb6AoH2A2xLQxPHbWW
WsM1MuNkuy1WtpMPVgIeWZ/xSB65WCFOXgJCgZg278lD+WmAiYSPutWyplfSUGIcLbXJzrVxBAMR
+DKSTSHuXQaqfg0iqxco7eCbXHxKwiJJST5a7fpLIIi+HL2wKE3Itk4x0E8xdMsa+hrZ5bMJ3cCR
7ior0M7Ax9sJJDbdju9wBFFSRSDvc4Q6isF9X2aK1ssL/P3lN9mIho1eRUK3EiLZ90kzNkYHPZ3x
eB91ddQMjyZMkdeHOTPf4T2nhHaxM7S82NroOm6edUQK6aZhukbZ0C6fEvKgXqPFVg40ynJVuHVU
F4zA7cU8RpYFw1QICh0NlptC9rGiKJzFsXTLSLB07TC7v3BZQhW9hJ3b34dRBmV4QiszkveSIpFj
JloarQaRLAOWujIyp++HGelb75una/dZNxBaE9bR2Uc/imjf0Ge9xYbpf9qpOPGIfa7QIOqMg6K7
IVbuMOn2Fd/AjUrljfo6l+/jkvLaGh10MaPQ3osCd8fBH11suknHZAt790ynLWoPdgA9G4fnK97R
ujdT+6iNEXBfwmlEEm4JQMta4WJ8bJdnAqSgNwAAc0MEMqIzg7TTK3euWd1V3kro8rMLo4eAMboM
mUYu61FldKcmRbqVekwt87/Ywici/SjmKO3PBP5POgzj3buYOKKVrsPll7rCEe1Jg3ilDhpV182q
LjtmszDKwadFcr4nulA/CKTA1PhTKVOcNoYF6yANICBHbYaeVGUEiLRExY9b4aYyso+BYzA9DOAN
LIQR0OwQ5dOkvqCjTRhEZmosJ/85R5Ti9grCzU5/c738qECrYw5febRlNQmoi+Hph4JTcAFej8WF
aPsUYnDaP0y7oS1OmqbCsqlDUGLfDTnM8fC6RumwYpnX7oo+pBWb1/qBJGj3vkTqm99mWA0vwmzb
/YNyFrbyOFwYVtxa6wABUUYRyCglPWvA4vSJ+E5PgmXsN9bP2QkiDGgA63hc4O/QDu6arXSocGW5
jJR1dgyt0m9KbSamb83uzIBCBLMOHLV6can8nQDNShfFYb1WGysyJsKNrD0l6MquB/RAVYWUAdi0
kJqNtnqR22lcCaOsJeKP0IhnvMw2ejzLw4bScWf3HjhoTvZfq7fEF7Tx9YYEZG0QjehKF+tFjNHQ
OTcwNgH1c9/vuLVs30dKSTUtg14EWDYv4OjF/FKXqgG1S38TphYAsNZaagF006XAhbz7PdtlYhHz
Kn4Ks+Lijb74rz3p99H4df94ITSizrEdcNYht0aHK9c69jjnQwUbNgva7Thtofdb+gIvRoQu2Mri
P6yZmVuCgcvfDnIB/9dbETx2LWKWyM7Y/quGBxRMEaLt7J8hKRT0RrxvtpL1AkP5Z9KVyjfZ6Pj9
A7rLRMMG0sC+eMpq+6pj22UkPmWfC33UDMwHHtlu9K8O83G626I9tLXcRHUIkwXegx07ptUVA5Vw
bQz2VDlTsWjxM8MpMQ/V/w0lvcgKWDl11OB8PqB3SH0yy41HcRyTBkkgojBYJOnqKalnIMTwvV4X
4kRYm8Ai9WQglA6fEAs14JHbmrdDzEjPmT2QRzDeWT1BvYDnvWR2qCyUnd/GVGYkAfE5qnKRLdNA
r0WNIBo3iwd/iUOPU/5lH3tKFtbuCkUvVPzQ2zxUDN5rxLqvYvl/7eo7OuJJWMJ7yF3ELmJ4ux1/
sWucVwuvvmV7HFVCeloN3Tw4Q5EBVNsi4ZeZy11AaD9/iambscivSjM3l3sMRVAqJ5z2VY5KwH+u
7ic+E8wdeASYz3bO8vplK6cCtLGWkF9S8J/zr4VZUZUR8O5C3/WEZU04U4vHZPfNhQgCkfkgCPH7
FDxf1Y2tx+pJfg73Iybx6UKzy+JbVNYioD4PniKdZ49IPLS9EAHAW6JUtOp40gObgIuX5aPF9/JV
6vdiVAEAJR8drQkoFuhpgbkQsUj1fAtw8Qrne2Y452w+ocVv9LgbOtw6YUbpB8dMaWXrvyurtYhI
AcWZE6OvqrifxowmI/Q9ZeYh2NtDfUqlWYB3nAnqKYgS9DzWUHh8+ZdioqBpU3XEoLBtvraWmwzS
s5ScuHZNr4rbVRQYKAq6tQes/TD3tT3ICD/MeZqNjz04RkuEnSp7PlUdp4o9FqU+AWNVW28wpRhG
AlJ6rM3EPHUtXYfCfpQhzgp7FnrirFZ7ZCpp3YV0qkedcyy+HLlfiusHe7JX2ACRat+3/Ddane3J
gVcmfjrDbGfeN2lPGt2E7bX2IuV3G0aG+KvA+RwHCgdbwAoXI00IrVAFJYvMoe6lQENxKOWI7HoY
LRqy9foRMn651Q0uInzPrikIkBn6+At8I/QFbEyt3APDEvIRt/blWSzbvUinwssPgLw/r6pXFf06
tDgEP7ZIXx593GTEawX3XNlEzw0GuTUkTDziITNtuBU+AmfXQrtdNIpLWc48AdiHR38OgBJD/AxM
rdfId9LZnOhVPvvN3mdvwehrUWC5XdfncnUIE8dWY389516d9KbC6DxRM2CdjjW41OTRuc/ooJQr
j+e3oHp9AcZjo+NOBTnW4akdgiXTXweIavgONAgGWj9FTbEmt02Yf2eu7LEWFfvWG3ZHkwFon0ki
AYe87BKXmXz14CnHPOhIOjKakuH8/LtPipXktOkA4ByeRO7i34GUX1xgsCGum7XxBr8R4q+Pvy/+
98DgLzds/2PsQ+WKe38Zzj81UsgF+iY8XmQXm2/kkDFVWORRlsyBQKm/25xC8sr7ViWNdXEB9BeU
ZUJXi2tXcc4GrNc6Z0U6IbOMJJ3t5m2GtneChmUtw8IQFYk88hAKdK59jv52+cHuAuXOIZPeZtmt
SHA3vIXhX6nJZbShP0C6yZ6DwABr1F1liCDfI7ShDfFH9aogsybCJfOOpAwTslkj3XHymnrVBEOn
Eemwjj2a/xqpL+T27JyLmzidDOLijQ/RKvKA3YlXPq5T/G+YzqeD9GRsRsttf8RKWdJG6q5hvnzO
+12GuCyma7WPc1DJ+zd/9xfQXtFjjO3y7754sAfdzYxmWurBw6mLur01Xo0DSgCGwrfutjO5JYxt
zw+FkDkpIw9iGbCRFsMK96vtV7NYWaNEVxE64dIYguz+09a8Ic+TSlV8rtXetZixaL2tFXT7HpXL
tm3fxFtNl3FCCvzgnjni/D/XklSa55MxAhNh8J29yuN1o/w8dckWnCMCvCZGWyFInt9tKq2gOGKI
eaitbcifvBqZEUjOlVR5VCD+elRzNdN9aMB/KISRVuAj81bkybSqGLAIog0ADb8i+u//J26CkSAz
iVpPP298EagOY9zVpTJRoyZi/imnLhd6dGLfb5ZTVwgRi9aK1xsGnGXHB2vam49KoqPM4tT4fP3D
idzuRVCXf1bs0S/lGSg6E+1zt0jT+mcg4etx4Nj13d1rK6DjDa5z+zH62uqWZHF29T2xfPtZdpWq
hnC3lmj3QBtyaxpSp+va8t0ZZv8PgNVRnFn/WQnzcsirETYUbPiXh0yCJ3yF9WATxDEgzyotFu+h
3HlnMQwwN7bfyra99JKbz4suLnlYmskpONJ42JM3eHSCK9yPkBXYMr9BM8p0HvUq2sVOdhBKDKll
rQE4sGGKr5XI9pMYTh+qAV5LdyNA06oFbpmpl1JR4OKpRPctRdlcTxiDrr/VEXUdoR9zNu6b3u1r
nomYcFeA12ungIp2DG9zBh/BB3ec9WnuzrOKpY93gY/2xRVNKDzCbGws9CgtVlYdTMCX7i2OU0xr
JQoA6XupnsZQWzifFvmlRIjIe5CJjahlEEEqw9ejqZCaH2e1GcL4D3hL2DOvFv7iow7TwPTrl4Si
FGPPkg+AK1tauAKwK4pBKJ3CuYGFnRZkV0w8SLMEXRhFbP0Nxz5FU1ULiHi8lpIBK1pg8uAXzKzY
AGvavZSb3lYtGSm6amyXIxf5fSj/LG5ubYWPwvkE+wq+GCNR1B3F0kK0oxNK7/4ZGb+2znGRe0Mm
NJ34mNi+xzofSGg1C+E3eEJ5URr2mj4Jax7l5tPH4HVhpObXgSnn9tScqIENo+KustcH3EzsGnRf
NjwFa4DUmpPTZhdHKCiWg7Hcgyq9HkM1RySXA16dsjpOCZ28JES04TlXM2q5rTvgRkjDvIucQfYs
Ts2GUhXRXyx8Q5EngXD2X8ne2j8aKKxY6j168nfOrRl21wGYwVHZHjF6aBtJP+WQrPkMSnDgvwmk
F4vM2uqzHjDO80RdaAoqm0X5N9BlrmIgPjyZd2tJsSXpp1orWPifYdKBCW5aTsPxJBvQpjZxEjPg
uAN1n1LADcWfjYj/EKB2oGSjIpN5T9GGkeBRP7e7KUGBbGQint2BzYoPbuychU0ApMeI3gg95ppu
B0KtOqb6YMI/oc2HCHhRvMQ3dxbGcLmrH+3FXgqQp4Am+enXTG/dYAIqfkvaLpWvbAFM1IBuGH+u
czFFOt65ixK6CQqflY4kpDeFd6nyv4mWtIHIkCWv8UQdLGJPr2CIJWkDPi7PKcoa9JQIfsd2xysp
jO7JYzgKvKjCQyjz0PVUqtBENx64OSCDIxAw+5srQHrfKSfxNgvCIHFN0mmPkpDGeW24dZ8QSXIn
fMLF0M2kCOLSfZ0GgEykxM6Hb98NLz3JOa2zvUkEVjMc27SrAtv3afEbfHuLD6KF1gTkqzBZhe5p
WQH+VgRBeps0gcKt+LPKUbHnn9ze/bA6wWRr9YmKU39P59Ly0vJCVZJXfD5w1qo+D1idjb36xFG9
T92FefMGqnGTzCC8ZJT2TcWC5w9gtPRRnQQ4sWRU4lIG5EHoCIuiFF/yumFeUwJQPso1sfultg+w
dFV9sivOPF/CMBPVd6wL7nE+XU9vai5ioiNjqqrzscdjhfTOPEEkG1db5uf4MyI7LTFD114FPoWe
PNWPqpDxM7QgoIQ6erFT6UmqaIpWTcNKPnj7LaKY1F9+jRXUJCip8YB9YAKoTVwU9nRNLD93Gfjd
SbY+/qe1KqLXlnGg3nSbMdRf+KCgIEwHqlS8Tmh/Gqyo5W0m41PUVF+z6aNoVol2Wq6FC+4sZeF9
HRTt+kb0GFu9sANAEABmwqZcRvaEI/6ZnvGiejzuaKHldhRcdZtUHHGUR2w4TEj6BIHPftKINJK6
PKZZtbhzOAqIFL6cvKt0jC4s26d5y+KQU+TtEDqGbrIh8ZlHmZEZJndKaM9k5XzyNgkoDXK5SHng
DSakj+S2LdMV6ADZo7jh8/8zSRBXaRRuh42aYh3p3zFTL6TS+5hyZ4DibAx9Pb62JE707P6sHRS4
D34S4Cija8fkXpBFBcHE5nwwLfoCQr03AAxVyeXOqc3IVTpjGb2h4R3O3GGwcC4pqdC9NCca+3uR
QaLHoubAwX6oWbW4XJjy4CPqr4QAZDC6qvAKUo54n8d9yo4TDRAwgxYKFdbJM+X9yBR0t6Iz+hYH
3Ysu2tGoiWEmuEGo23Xao/YdQT9Z8Wpyb9IL3VWTENVnoP+uJvjyr3tWrvjB8ARuMY54fnU2CoEI
446CRtnKezCEBJEd9hAp/WotRSxoreQZoNpWv4C+2smMlb9mcl/CoqCjXo5Vfg9I8f4OT2UBziEj
zasRkYJtlLnQ8v5+xCj4oxZc8x+AjjBUgl5bv1m+yXzlDTMDlxJTypitt0RcP/udZ7Rx4Jvh+9qy
VD8Q7MOUQ1j0LaYDRKNsb/IfeUiV/8yLnzobrg1K3IznHIUGTTwIHtgLe83L0eHk9/ymoAtHDAbf
kq0/SLipI1z/D0dtutayMR0M6EU4H6vSYgxuT45E92mX/pvr7KqhnpE32ae6g0OdIn5GDRlyMOtM
/oVIumG9sxsLNWlkXyedwVyf3aJyxPVBdd59XC2ItZ7p4usYMP/v3VxEM0RLCj/bCBUjowcDAzhV
db0UCs4H51qkM5r9XsZ/ZQp2o6G83iuYhXI0Fdku3AhQ/15bkDk614BjS4OjlMM2N/AxCz6db7aQ
n5oQUd+35I0qCHoOqX4a0MYVTJfBmqVs/oP8rqZSr6Q7Phs9xWdHVkkUW95XBnXkNQGbEx+xT/HJ
0SaFgtYe++c3tw6+0YHaOSxQ8Wyb7/ySTmSBo/Jwbn2IgT7YZOMd71J6mhqz+37DgNihLatk+nTt
tUbwA7cD8we4uKtDzg9iDz9ukvrZP9JHRXrUoyuctMCMR5Br0vwAGEQeX7vKD+oSbPxDUkHZwSBe
zLYD14HfxFHpUH+v2mr6sUYtx8dtG+yI6CpVc4Gx8MreE0sxlaeVY9oM9+yShA3JAabuJ5moyH3I
B8Xfshh0pw+a2e1vSGV00TuLXhQTWrf4OJVLVIijyUJrkeOMnB/H4SY47HIBOmd7Vt7q7InDKnx7
sab3RgVL2Y3Ak0q0JxZuEzOByd0NAYtIsn0cMQA9+ElaKkud8jLB1W+J0h40eLJ5DItlHG/W9Cpf
2IztUhSO+txMxx+23sEUVe3+e5lKEfkB8hKlbsGMJLhnHeosncAz9f9igqnnVC4trJAynrwnNL5J
E86UN/je+owdcOgB32CQQL41PD1zzacV9+nAYMC9tD9Wctk6URnuKJukub8MKGPhPwHJkM1so2Y+
ieTLvBbVuFrC5kKOAQRDAdH3imjQJPwVyjGbnV3I+D/HROxFfCXtTsqmn6GKJIadeAR0cS1c4RUa
Y9Wyw92PU+aKJX2NlhYULonm4QtN0uL48JnseNA33yMVa59Av2VKCaUmCUei6IDqG/h3OUBegKxJ
NoJPXMB46JdKvBJOl4itcUBmhufSjx1QrP/28hhd4m70XP/7aK1Y2T4pjG8CHDlq8KsJTS7rNzH8
nrN77PsyyKalLDrK3A83R1YUlLkw6O7yzo02T4jnAcZ7lF850lOHjDmpGO9p4s1u9vpf5JbId2uB
tNsLTT0BRYqPojTK+qSrMJYJV5+0g8MP9ZRib9xeQBKac495DiEzoXp9IZyImo7bPiIHmCa2kDTI
5ymsqqGOK5nIttfFBXZpPwi2jpNJJLJOFybxcbztUA3ofUQSsbHAFVg20xRYW6fzKDcz0e4t3uAy
NtjemE/5hxahRwGlGZfElwhCI1WcLMMwGxDEGUIprca2pKwIn9V8X9/T6NJf97G8v/bSh6FRoDfw
mZTA1WRsKlUmElDuc5vd+ZW2AKq7D6gBwfFvHgEZtShZ9o86TSpKGJ8iJw000eD4rjpdLzlutCgc
RWQcogUFIsIQlwNcmiGD7g3NHlGqcxo5eTljr89SBk+CnxdAA693tyNuZ8bVpmV2nahEpgQbHBCs
5TfuODjCUmI2ljjw85yzVXLiONNlzcQYsZdIXy7ejNvYZ6uP7anCEjogfqCGSucBUzQ3+sTKiQB9
xxj6TPRRQqrZNGf0Y7/9TL1FznsaoRPlTakpzw5Y0LdkgkxpAp+mMmZ4V8/J+LNk0B6KR39uGD8K
X9pYzpDvsLkzGzuS4CHFp79p7nb5//shZ4o9q4Egrpaud0Ho+9rjmFxfMDISfQjfQhkwtgtVOq89
VlXqrsSv2Wg/89JzyIcXv1jiCPFjYaLuotPko6uvPrrrzr/40+PrOI69x+8OB52KSGwWVe8QoAEh
3lyY+zvM+f+ixp+UgphX+rtQoAUtF1ffZh4djexD7PzVqIFmdDRAQ7GvJrQR6twhm/D9aubuRRtO
DQ7o+Hf5qTF3ekDxVej01AVci6F6DTXumeAHCozdFcmePTUPdZSbsxTvQwBFbkHre4zP+WoPZzRm
Yd4bUQfpAgDcQoGS5c/wIgECc7RQHplFKeyB3uICYuAOHkJQPzCtjzILmAk5OLJDhiWpPWSnPmeA
ahkNaVX6b0WQGdGhIVvMOK1eeHgOFcyje4hK+vx6PxVGSye2EOKK+241vIPqlO5l/jOcaRL6W18h
EDhZ7nOjOInDNP+HmSK5GOS0M/voDr8CtL06zrbdJfjl/XLxJtus4S22FjNDa8jkojdVZDBpUlb3
fYL8ErRuFnU0ahqCQsmXD4kYvpE8He0IeD6xJLYP+nCv1xuqv1Kw9i/b1TNJRC44ial+CYudw6a5
Lxmx/C3tHxMksCIO+50I7GCWAzkNJFWgXFitv9IAfhT+2yNCaFtkepTi3Cj3hIwlw2Vtw2CxjTk6
NXU4dYi2kGSVkrr0ESo8MwptiPxMkd14w24vOUlMi2t79z4fns76RBb7XsmHCB+W/K8JW+sKiC2u
T77WZSeM/C17HZbLJGY5agazwGv63OoaKg/Mg6EfGlCm9T5mvJFhkOQCtpFSo8bXWVSiRd28AXKm
gZqMoc5I9Wz87fDZw2CDffM8f1zMB9qjrRLw8JnqL2eqR80dNc5/K4LaFKjbHEwEw3u55Xo9Hqxm
rho5nQ33cQv7lBdaAi6JpBkg46NEfyaia8QaHK7TG3fBBOVx/D7neJLPN+mMfyP3iGN6Gs529uRp
GjZ2f7ZDb2ctZuUS/pOxltQ66RdIBC5iphBpQ8PUMNWiHC26YtsuvWguAxUI2ND/nVBn+n9PSpz3
8g7TgiUzLkUm+qeXnQenbAuytXLtK4IyvkavKKt+rWD72F68iQ+TRRZF+xUIIeALJljbf7TeMZu0
bImnC0TJ0WVlbkkKr5G8F0PDzJGAfQfw2S6JsZiT2+Kte01VJ/KwDpcmSCMskocLwFifGoAaHYCx
6ARtHcxY9z9KK+YYTaYhGdIZadMcCPbpN31/TkFBssUuKdHQG35mMwY1kzoAmCsS+OeO1j2QUaUq
ABZSEkp3hwaqWaNvnXL/e8q4E/3zgtK88DFeN/K33VfUe4si7hrcZejxiAi4SXUMyBfOtDXFNfc7
5IQFq+El5PF+S/3b5ug3zLPGWLJtDRbq8eZ6DNjegr68q6GFSTjAHpSC1kagtjQlqgBG0LYaKH6G
KHj/rehmukzxGYMjy7fB9d1NBcfCOlZOmOGC+kX09W/3tfz8iJNBlKKkb/9xGJ7C/ZgGjlRS0KEL
JZYtX1JG0i4Rq3nGLFBNiuv688n3avh8VqHgljpFcffsF/jYz0mJ1vFr2NaN07F0Zk0T2idVzvRX
Gc96DFP3mM1xY3Sf2nDE/lDZ3w8zwD0LdWiy/8hla0muvT5PzfUqA8CTFkSKk/6OHTKUAgz5Mb9k
QWaVQ5htw076mCQBos50FOnDMXtFeXjgO7g9XVDcSiyJK+YmYXCsaHJpiC1Nw3vbjqlyuMVVRpQb
8gBYp4HsFXDy8CedXbSKRPk0KxqVKIBpLrz0CY/6KRc26lcVFL9MhfSEkzpFQ9tn0RbD1u+8Dtiy
ROiKEdxVAgsuLSjXL2mk98EeWpdEDgLXafHIa0FB/+7y2LqZG9Y/eQ7qosp7GHckzz0DgiIXyYEm
p7di7YEoFDcDitv4shYudA9UQBze/+Gb0U8tO/+oHomxsKc73hiF8ZoeYeS+4M9ihrvJM3+wDTWO
zk2NVD5CZqpxCFJZu0kVtui+7eqreJ5SnUjOq158ovIJHOk7LsTO+3vBETc0f0DIoq9W4oCAKIcn
Nf/F/RVRut3WTsJplnjkRDP/NFeAOARDPDbkAeWP1rgWFglNlDh8D0HGhAtQRaghGieZstaHp9AF
SuVH8L5Z5EyVTNRqmqqoEER6+k7gjj0BnPMFICnVcxwf6kSN0eJngVFanOSlAceliTdOez3Mnadp
oJJmiFhhz8R90MgVKg5Exgtpbjyz3gtho/pXyEs5oJlAxCTCfgWKAHuT5QIUWeW2v0JBwS5LK+xA
Cihw8IHO0BtNVDlTaE/vamzq8bE+kFpR+NTbObCqUqOIbyEwlTUjlbqLNRGBneflR7oazHRr58tS
CGQTGD5mws0dEAqILslDRSHb7A53lMl6up6IXdAKphWcEfC7eidKe0Ep8+hMs4cL2oZEdoG6sDHU
0cd5FUx3vHaYpPrjg7dh5Bw6S7HM99nYQUsRHrDEfr5shSiXfxT3ZASyuJCWa5hNvPuhgjxgqYAx
Fa4qe52wE8reqsi2Qtw6cve1NnJZ3w4y4NN6wVeMzbOcpFoT47LylRHB0P4s9oUSQLNC4ub7bcm1
MJOR9A0Y2t1UPVLZ4gmat+nADj2hVVGVHM7OtGUx+AdgXwPNmEFznfe7WR9LlJ5fCGe8//cvizfK
blArz9v1ZVFz/zoFa/pD3hW+UjTSTvXKVVy1Eo/D94eagYF4h8qd7TCdj4LtSRLljd1tVGCUCvyN
8ZOoJjU8H1Q2dhFWn7FF3CngYvgQkD1qW8BckZSKrR1CipyhVHArIxPLhfNkjN+qOd/M0p7Guaps
hGgA+9MurVDM6wxkC0T20HUcsYRKzxMYWSA0JB7QSC9jUwYC/tZ8d3HGWxELui38Wav7UWXDAAG+
M+JiLozNnoP/jycIjK29fEn+FwtENUiIDoQQdvy0S4Tpzw0aejZ74L6E9iRGSybGzm1SEg7z0U5J
L6r+QZtAzAa+l1kEhiTfKI9+iAwvyU9Ew47IdRs7dW7mnA5RkIhrJn2l9U9xOXxZXnrzIJ2bE9VC
ZLYdFoBrHJvieYPZyvkvAGZYF4YcjC9LQsl5pe3yo96VqtHERVASBhY9hqcmWL2qLdm+UBBP6M8p
2mFhfW4rJuwpqtLN/dFdVKPmzLoP5j6dyA7e2aWJiSnc8vsPkpLgyreF3Vak9gjzYqRryOOa7iqx
Et2o4p8uS4xOo/HPvxPxZlx5K6HeqBWD0eIZFQFOUl9JA1EJGGBkts6tbqge07g4OeX2p6GQ39Hq
kyfQGZmk7lwY8QpQqY5NNk5tTz2AAlyVESh8/R5ye5plWbGjrnTx5+dc1c5+ks0YuEcwlH8nw3+6
+yQW1bTqvJTMu2weEEoFhqbYQvLukkYGiPd7pGwBeZsKbLHDCMd0MOdIepv7mJNKf3oWEOwpw7nS
m6unMoNaXZdxjFsqiYXfmvRNXe0AcBPi0R4jQrzEwrlywqAE0PmwM3cRUU+8dF/acSFiVm995Mdo
7xLbysJlPOKOzuOmwWIgtriKXvLvhvfTJ0P8cJT4yf6VhfrQD+JoMuWsKF+Dq2PWa9z4a7vNvwHI
xEnZMIVQOaq2SCGyd97p5vC9AduLuLRbBao0WmLEj0y64MZmHelrC/bnEL0IS9u9nzocD0mobqNe
iEFupDlbOEgXl3+tZnAvUdWB2bIEONmwVT4e8BTlx73W0FnpyGEuE4S1ixkYRtW++tOYKqlwkdk4
Jba53QUwiRUN8BSAh9Nx40E/4PNYwcqnntg+1vQG51IH/gbbFji/JND0siSYdcF3YEsuTd0y8bzJ
4JX6bN3ZO74UD723Tsgmk+EPJpNg1eT61amy1BAhhJ7tMVnVx0/Rm999hZ1095/Aq/fWNA/Eqkbt
Li4/cM7yblB98/8QLYrs0C9Bp1ZRUHMGuBhUoJerFYNfWDCjxXh5gDQsNzzf7TIPR8Hdv2osU2CE
dazqpV5oxG7/3AfW8rAvCLgwXFvYm2Shu5S4VnLygwTBd/k5V1bDBlPU4nFAI1riRHSISMRyeoBO
cRAESn+V5N2maFLuHYAbkRVhmw+AAGan+dGuxBz6ObZnP/iJtSO3afAlhnAtZvIGUtMSRbM84+z4
XY0OUTI20Biw/iweJ9HR0S26scbbSNY5ffsnEZxee+cudFa1JmwdNaSDulkytt7K+T6lhv2k29Sc
iwZ0je/IaJp73Z6MKikU0BT9iFjokYrB4iOuYNH2HwVNZlF5Bf59RO6uMpnFwlJ62AcXJCpt//jQ
LE8DIg+UAKl6BelbH0Q8GJ2Q1vF0TdiP0t8S3PzBle7101D8SkeoGAjMCeGRRUwtBLC1ltNbOxaz
bVvY8yWEY8K69U7EbFepoQoW8W9uoP+AJ0qzISuJtgoJ7fVtmZbNkBxlqYltLA9qaRV7V24rQhjA
0Zgc6x1T/04RR8fvvMrURfBS/TAOUibAJYsaCbRLixBFqPEIDF+oc/nG8eskEGvsOWQirW9F+NY2
SDmniDE3xypn7DBv62gnVaN/d/adu3aKvN1AvNbCUfsUhNu8g3k4QhAuh9R9XxUVQqcld+F6xX3C
lkoVJuauXYqDD6azROW6d5ZUPBmROvS92qgJM28TJu227ZmmA/pC9CZoKmwE2STtWKLZQC3Y9mes
5NxfXyxa4AqYx34fvv62zcBmTB0mrAPFzy97Z0fd90cEmWauAzMVB+JVoXIcwHUX9kYFasKMCknV
QfDs61MYGl4SprjTkMLeHDyIc5rkOdKVsBATxucaQolZBly1z2teOb27dt6nMoh9ZI6X7bXMKG4A
/dtLhe3jjIJpSv1zQy2Fb12dl3GC892IZIIkVjTkfV08A7yBm8sXWsABAVTlv9KB6qL3Qpw2ACH/
ZfBPrNorZKjVzUteasR+m4t5Rq6i2Uy4mR+43GCnMf8tYz+YADeXiGw1EObKUH0NUcux8ulg7wdG
MHP1SmbpPr2uVuEtgLkeV/gjgXyi5VcJJKV1I3KNcCqUyILsXzGzMMN02mGbNKbc33a+EnBpcB8Y
SKeTUQNKgm3u+SurcCfyJWTNw+CUIZ7M3jmKwVAXW58LK/rzDmFI2Qf8qfBnpjup0NgGLeotw2hA
3BvVfeJ7H9QP70L3u6qmNqdEMA4e6TC/e8OdDKazeuq7z2/n0StpNekC+3H+loc3WLcrJK753oxE
g8ZVOfcp04R2wArC13/AoSRj6BH+Z5j4QXkdxPyk3z9WWgH1/cl7ogszYn0NkwWPN4+Uaf/B23kx
iVtMa++CGtEQ2o1hsLh+OhdWwhSXUcyaHFM4bWYZxH1D6eeJDDtJbh88YO+ifZBVcGXXJQXnqb26
8RNNUYIOvTY6zxxPXQWYYbhi1rOCeXDlMSFdzEsHNIQZZwyWYHssrHadNtB3zsM1YAYuBlwUMfDI
RG/0OwQSdQXedGcTumMDKu2mfxY81oP/UYAzrSz92OCrau/xBIHmkJpgHXRtlmeliGWv+OiqJn0E
Yl6mE13raeNGtbBoBysNx0IcxMIsOSYEF1wD8TUFoCD1Q1K8H4pKPCen5kEN3zLO7mQ+jKByBMvy
as76RMvG8Hio6uKt+rzt/21JX4hTmqXoKuEmcwUolbaKHk/Jk8+zV02QtWmxPPEzMx3ICAl4L8FD
yi+wCOKsmXpRSBRhRm+FyqaCQ2V+T8H38mTDudiY6gyWom4KSv0x35ga7K3R1lbYRHAmV0m8UmVG
HHuEUIU4Z8PmTlx63uHe7zzOm9Pl0YcYOikyYKICXfSuBC2Bt7InHwndBC+mgC53ktsuhW402sFE
sneCtZejLP5r2xG8cihtytlOEcTQyXgQVDTaQyvpKg2YOpaxIgybKKMedv1IiVTPcISA6lSlRGX5
xPfA1kXEcDc3hEZKWYYUlzIvNaTjqx+ApVNrn77dpKD3QGrUiZlpFucGu6lSVZlywpAeymikC5JB
8Q4yxwrUWeLr5k25CbvP7SUt6zO4nQQwmOe03O87UHJdyZhkNELxiY4v5j8hrVTOM13c6Lf6kDiV
j61AMpP2nBK2P4tcAhaC+quD2hxhrWAAlwbZY+OxR/tz6mKFHK7fCFkx+4j5os3ev9Ka36IhNxUk
WyZCrX9Nj8SKOQ2pkv6G9sX5AvDB+HN9TBJcio7cz4DrBidYt34kTIuh3rci1/GBGENvLRiDq0gw
/ml1mbI95FqtsohKm9XECw4W9RSWIny7qql6g2mkIqrNzLIAEt50d8YPLOmpWH3wAstJ/akcF+vU
ACvmVI8hUPIpeAfpBJUUU/CsTmjxlfw8N43Ys7kfg/Z2vGxzlSgEiSVynQGXXrHZ9k51JIIOTVSk
+gMQdZFneDjGwv5cDkFm2NKkZtWk+BTYFMoaIpM6fl2s3PF9Oe3OtJxi5Lthk97IfrZQOdsemsfx
cNaer5ed1cicpiPFWRPskoFGcIURYNrqte1FGDEeHIznbAgOfN2Qrk15dja7PtHs3HRJsZRj7zk1
/Ct5HBuXz1zyuQULp4LFiiw6JazELLZTltIHdA+CVbRywKqUk4QdS+WpnZKlX+1wsWH5+1ghprOy
DefF/IIqZtTWC+BSSv7p8p5iWOANeP5BJHyRnmvlZgimZRnyStSY8hPs9X9DYj1Cf+sKWL3+INJX
Ll9gqIOygBc3UZAl/bJqauh3NJa1I4HzEcaHOAR7jKkPZIcdGY0GCCXVFuMkRQlf7pJyU99Gn+ot
6sz/CHyhTNg1+vk4OYhAqVHMT8bsEnzHPMedceKT+/BFF/ChZCePX2f04RmYj8emGjfvQCpNBRd1
5lpGE0pVQ1NuCGKG3zbxq77MZ26LSg3IhWiZG3AcBV0p5RIO6seisjtFoEP3b+IjUchlKXmFauka
kyn1VM4Xu1cjtHxRXqpXzliEt62AwmeuJ+8/9mMELD+OcK842qCYPM5yloJ0N7JMFkKTKCiGVvKM
Qh+amcZGYkAgnUrvlnxV959jGZrjbstTPgiAt69e8RqM3LW8/pMr9unVCMQ9O4HQrNULfnqxIrvv
O/oG5Gz9axbrAjIRz8yfpLA3TfVKmP4t2hOz/SlfyTeglfD5csK+RH6kNgrzQ9xWdFN+6t7p6ZBS
8YAZw/SYBaYgFhOoFjOBVj/Gt7ysTdx6fdksVW6Bw8W4lI9LpDWolCmgxmW+U0LUx5cKXQ0b6NlK
ZXUTonCS/DkPvcQESotKyJ8XLXKoZKf7iGOlbttrEPdwe/yjs31YxXk5OyqrHPwL/WdtQO74eJeR
PaGHzQMSPljJsS6HvxWAvJcFLlQy58Al6PLjmhECbOJk+c/KqQN+ON5PhIPaRb8bJJsd0WTiFt2C
1WZIeMeI6vY//O12d4EtZ4bifjqrdmAAANiXpg1GFEy4nq5WLZjxpyZLhZrBYsmyI2s56RDD5rrc
+T6em5IW2QTC918/BbrvlJiQ9peaKY2cOuFLEvgXiJ2w+9Ip/EKjTrNFSyTQVBbj5RxYihEb8JIH
netXELW0jfaCzOBtMlY2vR8MJt5BjYDYmyJSu6zcYYgJKxSUGK7OCY7WKcalSd2Ok8m/fXCOwYgm
J2o7dU5qbmb5ISc0U+kK0ge/82Jb1cUiy5Y2D970MidpCIijXsix62rTqdobXXclARNL2Y3QraKl
GEJgztprGBa6+51pbgU6IXWi3EewYEYF0yincqUKG9iM00/cMOwaFSe9izVaGnb7Ezo6CDlqpyEf
cJEkEW6lZ+x1dQvoQ/vmqZwmSeNZqFYjzmYtf0O2dx/Ge7obYTHclF3Rg6oiRFCJfd6pVVT33zJG
SOZWHgbnVykGLWHYHwZVITuKEeBP+MGFHJMe2n9pBTRmKS8zCjE0WCUrRne9mfXDfs5/iZD3QAm9
l+Wp5n1xGm9pbidQ8uCJq+pe0Ad4E7k2AL9vFid+w1vwpzgUsuvs2VqJtVgbj9eLYE/eF2edHBDk
8EbfRS3gtJJJ9R2Tt3Qh/Kip3xy4EqMqYrWFFc/O/O3JGIxh9Vu8lkAUV/6Ba+E4TvBKAg8ny+tu
oBEv+xVUGIIkOR+FtvhILQm9gWFd2hJMstDkSozcUgAGl4MGjueHMTgGylJZir2Y8B0u9df+ANUY
mq9HHHXHS1qvL06WuHblh2BVcX56vZynMzx9W76d72IFJGFA8GAJJW4FY2y+l6AV+1TZrsvScUaM
uuGW7jv98yXF3EPaYa1aqvUaLjr74j+tkIg91fi3yXW53B12mIN8BnxP2Q6/zB7tymn/4RI+0p8N
UlBwnb5AioeGaZlhBf7agZFNYNYHVZvg5ScrjpEr77V7o+bmu/HkbBY/jbyCu8kVoRjYDgBqZM9T
u4zpZQOQRaMFZsdII3QiKqP2uGtPSR2ptT4skdh6qA2TYhAzvcMauGVY/hykaWSGX7CwBV8dQcvP
oAD+WmMHOi3xy7MhMYTQJ+ar2gTRmjHome6DmyIyJhZAso+9qxRood5SG8bq58vZZt3xkTtjPv7S
kQe6RIGOsPnB6QSQLYw2X6JplUPR4GlHo1SbX6uvBWGYTcnq0wb9sYPEQZTSIyBUPz2PT9kh0m0E
AoZLpIKVwGIJcAtzBkOXpVtL8awrX04+8l86UEt7zV91/ECl/zM6sYoEFIoHepEsQRJariI9qVXR
hN+xb6zc+xN+X9LpVmpIclLdNWRVUB20D4Ne0CV0pvZeLQ1By/KkysNb6HgEmwy4lsxFw/WbA8vT
KsJ1Z/SLMj623SBdVtAcFOouXFvY7ivZ4v2gcsiMih5iQU+mtaR/OH/YRDidek6x+fbWqS4xKE3S
HXCw5cf5p1MExJyypFWpxCe0s7yj2zETGItS4GYTEWoBFqakHWcj1Tr3PjSLvFrs0AJGOTM+95gO
03Kx6b+FI71cpo59+OirPZ+C/TGulLVpkmLKSNtfRnuLj0TXM9X1Jz7juOYe2+TcxrszsfcHaSFR
xX6AYFmUu9i9XNPcdNFelMo784QpnlhC/YlH62ObkhvfrctTHpYF9R11iRcSBSTi5ovy+AM5kzP3
d6XmkeugVinmWtkAcs/dt2BZT2ErQ4Xipd+BS1x4RmFCdwD3vlhY1OAgOANFKz3S7e59UL4CRFxB
Zbuz0DIHX/XMlqCuMj4VG0RF/m8qsYo1E5pGt4952aZkORm/irVWa1pRaknpPtS0jt27og8DVMKq
3D9i4VJkH+2GrWmT8ulnTZX+Bs+ml4PvVq/2TlZoX1NDVIZTj8K1DSvvLGFAmH7i2vlqaXfd9SHE
3FkxRwbFWJ7NpGwsHuVlateqcf8igSPLeFY0DxQGEFOnyY7Rtw6tI9fRTyD0rBILA2dW9SXziyHn
ldIrtsf0NGOHhs02ATRMK54lceF4mPvs8uNEQzCnf4y+vHQLouQO24ffB5nb15/8jkgeGLgmTgZJ
kk0i+28W1gSVPfh9KJO0NFJnRshmvaSVEzCopNY4vI1+MaWWZ7rVKfvjImXt1PR3e3qOrgIgb+TD
CMsT9Syde4DfInxSEwLZlTRJE8V9sTOr61fFuV1MCrPswaA8RlCaW7/wTnNAYe5kRRuBf3TgkY7B
OF10qGe5hh3r01qaD238ZZki8FvaFOS8Fj3Ru/3WWlRp7ruimZixOaBrdtarpCSAh6AZWyyu7kM3
RgtdZvg5UMjjhAKRosmywSl/8BUQg+bYAFt8e6pxFnzDfMRu7AFGTFkoM+I4fS8w0UrQOqF66/ei
r+KA8eCft45P1BjLAyaJMqkl+RxuUfeGLzzAgOKhyWY+7bl3E1jjWGJXJllaUyBkR5uNOrqZepvs
zfcypKx/kiqlzejAGdL02o9F4Mn9R+ov0g7Xywi6uXuqr82MmBb1CqSwuVpXcBJhCxzVEct5usxq
+U9kia+D1q2fTs7nFezd93rxVbo9V1skYmflvmRiUZOggRJa80ErrCi31OLA8k56q/2CXaKOBQkl
lHrMRwVFqJD7XY/VzLC1Hya2ZSOiJPxEnsGPIOQ5Sqp7s8+2Ie9/x14RpFdy+v9VAo32mycaP7bQ
j2RaF5bJ+DPFoTXAdXa7fvmFbAScl3NfjcoygiExGt0T2sTHBH71BnSjx4YPkxzOXRGs+Xr/o4pj
RD4FGQ/fqlij0EoX+Ah4RBLHAfljZ1MLE/DSxsb0n1cBjuAshgw78UDfJkXilaB6x99wVgGim5Jh
K1+CAubFaUxW3KSKme56QLj9qHS3tMfezKZRhJw3tFRzGZnS7K0gQovGwkxTCTwm39r22SFZEkAm
gcR4HnAF3+Zb9v/2tHD3OYSPdsPVvbbvfbpVmQ+okTS5lXoD89rujNieDYpMzRUj1QAKdQZNMtdJ
T9fgt9GuBRZ2tnp8qoznxUxNQnVC9xpwZVajhz80q/smY5kDNK7CNo0/o9Acy87VCdyKLhF4bCj6
JhwwZN9p6+qM502isJyxRGdNKqcOZ3BYLuIId8JVRIBixjeTY3Szz97lnF+j44NQ3PwfUzVPRzXL
f0daM3BIHvJKDfqWK9B1XiePzwImyZYhI01bToINQ2gv7g1G1LZTigYpQ2ZpaQqmLdJ8UFq892KN
0u405tvDrPoZ6EcNsxgS7zZBGUPs7+iCX+kXevieZ2KsFag4x5KsHptWbR8esXMnWFkxpcS9HAWP
gxJTNJoZYH3uBhA9qaa1qE7Wo/QP1LtA358S/Zv0gQz2Hf5H5kOqgrvUoxgrl92z7QyankLxLJQ0
m//oNPqdL6IlPzNLb1q0WRAwF62q3a1RjXuCH/jPOtt6c12035JLxslNyzGXm3WqUr1DkFSrrkv1
ARAEdRqE6eGO6cloZ67A6C+xPp4ejvHbB3XsiCNmILMw6yUO2iez1KMaZTR+xARsnzYKxBWF2mVI
W+UYfJ5AieBpNCtAm/JNUwn4NyBRrva8Bm0TbyvRbCK0/+aUPMr+3kIgG2/tbSerXPSo2EEtYHhH
6R00P65T8GiamkKs4YJ3uwidum8/pVepHklGsQJQxdAFmgSWGVQdi5PJKyErYhXYrmwUy8gF7Mv1
uO5XUukbC2ZzauEKa9I7AWNIg5cPBOw2HCvwK55ZWAvxjmXMbB60ElYG2eAsGTCJgBScNkGPMwwv
8VESvUdYBDen2IvP3QJ2GzTGJelNt64hQx+XUkfaFa4Kb/T9OnnPbgfKTVKTaYUGVvQezthGvZP3
Syya6vBTBL4TkTrcbjPseFAUdncAi2/wOEPjqjBRzlhsCOuQN2aEvAucW5l8pxmQ2Q0fIQhCKByc
2v8DDuiNS8VFEl/1ZV3Aml71IK0qmdh/cfTDOUDGiPU5pbALks2YcGB7WXm8pe7jUdlwz5NGRWg6
/foaLyMxz410FHR38ai3UZVNZvS3BZ+3BcDPdx4hZRS+0K++PUWRrcIMo0kjKvv9oIjzdy/YlBo3
N8TCS8EXSaQies5zBopi2CDJHYCMlsAK/LJK7tvflK1Njdj/XxxCAyuZ9VZV49/0bP5WclJFvert
/bVS+/rBV8J/X92vOWT3xxKgaiahOITn/r04ZjXZNIgbdWDDiH0mM4zBk11PO9nIHaqkqtYi/nO4
450X4EBMk/7R24EMGL10YDQzITRbD3/OWzmSrF0ywj2Vy0L6gfHLqUEnpkvWbr85nfpeWGSiVzF6
ZlvINloRPAPM4TGNYqh3WeXf6X3Vg8ZIXu+nxVtHmMbF2vfUeulYScxLRbZI6CNZGGdp+xLnjZ4W
l2bsCrT/1CuIxhy6YIer5yQM518MhEMnORKES11+NHY4xwE8F3rTVakLI2c6Wtp9EEN3qQ0rOU16
rsftfQid8pwcD94m13RpD8GbyUMPq8nKIRrFesvkZWBozhLeMZFVcxUvGTP29SsNYSAytJgtfK6L
UFToavKcyZJ3+4RpLXnemiKGJIfHV7aJgoVbdGZoLc1y/by1hoTbBc5vhqzInn26LJZ4hTmEZS67
sV9P7F8QYCFsJAzLjV+YJ3FM5oRIxT9WB4fn0DvR2qyvnzoFsWZvTdPvItALzmQhYBvqdpk+Ytn3
uQaSScPfHHXd/Itjwn1flD9B3YHPKN1IdjLKv0SKK3W/7HqhEE82oLxJzgNkLjGP/MUzbkUQ96j3
ua+hXfe/SjFnbamklVOh62ixNZdCLJ+DclCJLkyccM+vSQvRMsxNE2VdSDEe7L+DXm2QxUn1ZkgA
xpLAuSQbLW7UQGx5MpAcEBlnDW0gnCvkmMefsQKulcUKuU+ANk9lFgxRUgJBj7u11dfJZajiCNDm
1fSriO2Od0RVy+LZcqgfGfkm34OGwsgaJVANiJ5M+4IfPWPJdFI91JAlr3Yk+L1sl4rzKBcoBWxR
gQ+nDrDMIa5jEOD9vhbbvxKivrGZDePSxqkUc4UsQMV/fRtH+6Rqv6agKGRn0FwAwfuZ82dKAPxo
BCC0E9sA7BOkGBhZnxFPorWO7QAkRT+xG5sM83V7RVk6erCDAfq5CVnmfE5Ovssh0yfToeC29WEb
KbYg/7S3hi2Gd1LbC7Jtj4jIoc8kkYGk1en/GCLg//7egSl03NG1Pgb3G6s95H6YX1ESUM9fFBmS
//toCz7Cfh1tpyms7u8G/lHxls2dzdiMwszq+9ynf/cTbb3OS+F+zfjhKbEJEqdzHWrHnManBVov
e/pDDtC+jUk5HfABonXVPmlk8yHho5c67UhP+hP+PIQ589ysJNSMSpPnc3s8hYl5FOHM7w52kux4
EjZW1eO3wsY0Xfj+1YfpGTwqTdHn9f6/rVpDZcaDUdi6XcI/R7mLAZA6RvNsnQYJQGUF6+W5PGm4
2BlhFmhPO3nZl1otwT3TppU2QXtaHCVDGVyshNP/hR4EIAsDAkUrMdT6J1EkArvtcD879FvKVC9L
aXbzROrlWWLUs8Mhn9hwDcGcGqnRjsRX6upQzA315JsKAUnULKIWzruWcHaLJGI9wdKmfug1szG0
36sUGk17N15aHxwcqDmOYqMnAnuGCpq4yNW56lHvJEApgYNGKN3MxN2AfEHt85AHgtQxB12GYq6Z
piN4+353hsbwFG1qVDgSlDVgIZYR6WcJNIjbhunP6jpAqXjcyunroB4v0mnc7q8QV3RNxb/znAa6
/xVpiafTPBxZ9kwZk9GM9FKusp3K3FwBZJIFasu6qyAP6kzQ5phfZF6c9u15e9E2EbEMRXH1MPUj
zXC7YwTpzIW6LURoPKA0ciCkNfVts03gb4fOW/1X85y23zjlSnrseBTrf0MPLWm+RVyR3KdNrqbF
0qyy36Tq2NO52Rwbjxa+JrQm4ajcrNikSKOVspIrV+Y4bee8R/BJDrzFSCXFmQRnpNwLvPoaIMaR
Vs/4IfRkAprgnroKD+Y7W2L4kMf8dqlilOU+0EnfJ8kF/UEkCNukgbluQzti9Sjrce2cIHmZBb2x
ZMorvAvpz6MLrMUw4cuwL+6+PjqRhfXW0h0Mg1mTaX1pUWNhtc9IjiHBxv21zCYd1MtCbVtkGsak
esiznVvRWg885bLBpFFZDKYy8OPTZBXgC8ZgBbfqkff+dJmbBEJyGoJ0u8XKybiOKfW68EGtivoz
3r8Ab+d22/cCQ2wfSGkMJwJ+p+X8tBuC/NSSrg+pT6ZiPX7acCTxv3/a9IfX0/lT5odQdE9tGTw7
atU9CqJ3cMiRCytwAg1y2iIY7VwFf27TziICKwmeo/nvHJwft4JoXgYEQPcjw1OfJBGzSZjG51lN
wKPooi/Or3wKEmyolDyrPFsFJNdM7QRT+q/Ets4vaKxZ6CUwD8kgvbm305kR2JUPlLIBfDYYtqhT
wIzQGOihZSDI9J3SRN5CaHtU0+wF9hpeVavjYKXSUmcK+ug2Ntw9FH+xr9twEt2+xJA6mf6y/WRT
jQ7pTySOJ1yeIPOwwt5dX8XmuCIscT2ZM17HvGeLEIxQ08yTgQvHdHDc934l4D+VawFsfGbiMSDi
AqG3Js5Jkpu+Ka2l/9pPqPUcnkh/y8JHIlgHv7rsUDBsfyB8BUQjS2L2GoObQZ1qW9wLib3AzcnF
xPkYPtfdSiHvhmt/mhdrPF8BqumZ1z6fr39ZHhRLWKfKd/Qa3KR3bO/tEYnBpmC6dOrRkAwmViNi
ffWSgI7iPRmmIcoj4qCwNnvAyKsH4fETjNBSodDodB8qm4x2aJp/97o8tUAV6pjRs8TM+hwNpoh4
zEW1qNbaXTfc4rjlO9MUpojz8F5cY/+At8lLccXhl/FwK6g3o6t1wcCAqBPR63Aa4Bc33NMPv114
EaAczvlrnfz7KY0A0PJoNTlY7ghngRj0j4dw2J92lzhCvSxGHoIFf5zl1sgsfqtcKZizOWTg24gP
zJti4WTYHTdcfnGN4tGN0c+nuJinPzvLPro0he0mf27dxSZ9CEYPKI9bcKZ4xu2M0Pzj6/wxVd+x
VtnZADqAyGJipAoDSjhqYCsAUATP6O6Vvnq4yedcDMMljJhJZSSxogkpbAzMgTUwEPiGNY3l7f80
ISZfFxszL5/umJXppuaVHIM0U+XYCwTgiGSXkNn9+I4Kkpzmj2FU41If8+nDhIR4m86lXPoSa4Sq
v5rvzsPh8eQZ+0vR0niDl2SyfPavVc2LfDCWRwXVqwoLZcobYe+joB0y0O/v2eHd2y+RRc6H6cjh
wpfdXtOTP6lU1VNAVK96scomhJiFJI12iaJMs8bw4p3pLWOH7/gAHVbSAjSTqUW4uJ5Lz4hqWGpp
ArWL9+vC4jUkB09apnn9pLAbvS0xSlMVqOYNzpDL+olymHJo16NJLz0YAHjxnhFTgJtJuKW4ba4M
SB/OnSqrQ4mAsRXHMFRvPyvPhiFjp+Kmhvh2NYSByKcuhzspBE2U0bxVe3tSUUmNJrph50BVz5Ha
mIzSPlM6fITFA1qUiJpLAiRtf3acMEMfheTZAuRGfaA2Jle7fvHGbmI8QRvskFr0rZlaYKSf0JSX
PhbIRW7kDyixdZz6qTH/R10/TNbohGdfKLOUBnCV1Nw7wCUUeCw4ZXIlhhPHClDjb4Sjb7NVMv0j
ccnLnXs2dNdZd/isD9Rf4MLq2Nk49zVqXwwN0fw++sdL3/NddqDpQ/XjRdtFz03C1XL3osDTIOcI
jHJq3aDN4cfIjo8rhdGlmco3ci85RHGWr885wXcYHGC8jyRRazk0KdJqhMGkRC+yDbQKq/99PtIQ
Y0GP8M/uhZrTkepe4WitnOlXIQW29BK6Jm1NtELphOcSl8vjuaFE67GM/sqWhKcsP6mec0s5BbCe
Us4PJBX8UP7ng1U2fE02cGX3ZusdaLvyL8wkOftQrh8Sx4Ehy/lxFPYQf7LefYBevqtws+SWi+Ve
V0uxfznnnGO7YdWnDI2ciEUgQgCESYEXfqyX3gv3PF9eEmjpuGwC5SNP6v01fxTrl+tUHM8o8wZA
hOp3IgtS8uX1k8UgjJLXKmqs2xBRypyI0bVA6l4iiv/4aQIezSVeqbC1Ss89y/jR0YoPZYs3NzzN
ouE3ZbhW9KwNp8hn+xjtiJtGjV2/IUTO6heVfwtRBsTSUtsDMiNVBN2vZrIuLgoen+IybD3S9mU2
/4XqGC18b3LnfbaFhqw3lbo+VF+mX2eGdkoBmrXaVA19eccR9hLiNfLlC36gbaCsqYEoZf1vIzum
LJUPosDSfJECaAF5XdbM9uGzwB5LSe6kgrWaJTip20VFISuecdWHXPcFKNhQsYtlzhfzak7hbt6N
cGmX13M2p3VsbjUrqbbhYq/amN1tluQBTkRm6htay/8EafSUQBVses7WmC4xtzU2zxg0YR7KYKNC
V6TJbrZgy2irrcQ+BPISWgxuiJroA2zS3K/RAKOXhyjQXP3pznz4VuO+o8WiJ00+wl68RHHuMPMt
W0RIISNYfFgDBmZgpp4y9PGCihFm3sTCsM2MteHdqTZo1DQamfX52jPsz/WbaogCEsiAbkZGchhM
OZUuTgOm8tRJ3WHSGl00l4GM6/gYNb/RT18NvgHmUELdU1fYSxIusH1J7rLRpDNCPvCsx2I77yU3
bD0NJ5BcoJTvdWtQR79ntICyiaiork5TxOge4YTximbWBAMo7L781LHzwcbZBtb7BwkIH0TeT67J
Ev8MtBJFUT0bInHr/qr443KLeKD+E543AfAp9R//OcqkUdNwuB3+NBnck5zh9p7WPLHic8+M4NR3
3OqxmEZa/SkVAByjaTIBtDMXy3J9Deb+Jw3UBzu717kvS0nCnt5QXxiMsj+H5pK+vhXZoIG1fRnp
ZZN2nRj7rmannmgE5xVMXsjM1lECV7SR+FQGK/F7Ejrpo/YTmiSeAfcOCFK3XJTfzMb12VO/fmVC
Bh6hAXQCyQiYVZKN8x8hJlmosiVXyslZZ8nCyzpz9oMxrf/FpJJBtJRgeFn0uvNJNNlawJbZpWUL
HolfMXb8RBO3YCwppslpOH1hr1K49XSc7fn8HxrqQTQbJbGpnJVtKdPmxKiEJaK4oFr7H4lO8pjA
K76GWMhWl8+yGNKbxLu5SdDEGt9fQriMxaQ5ZaKtmZdvoy1OBmOxJzLKK0QyzwMerDRio6H/uHvp
aoyXrtrMDB0aH1PBBQKk7TlCgRX3nRGhV08aZp3yOyr6/AafXADlKcMo9SVugUpreb3isxF+8JtH
P/KZO7T4BZ9/aWS8e/JieUm/KMb9uHLTDPEEZjnKPeTnOZcZhcklW+Xs1QyqG8RJ3XAJZTOPkJmj
UWEXpAVe+bGbrQz2aBPevGa2WGunYI84hEf+g63rnOF9izzSBY8xfBxAhscdjK8VkWy5g6nFFb/u
q+Fe7exBDZ+8H5lsxYTo5Po5/j5fnyo8iK6CQXYCBwWs2ONDStWVCnCGuAXyIFhlTpztGbIB0qfs
KJmiNgQ2kjNTzfo9A9sEELVWDIY68x3MANfstRyfqX6IjQyCYR05xafiimL8IUB5oFsOCQtTb45l
w+ON9Yw3MaZGHE4jOBhPKesjqEg/5OMKyBTk0Mg1cyi4nWwkDhWF3C422NUcJbQRnbPO198N2/zC
KiZc6MSo7CBZgcJAqf3Jyb9VwFQuq+6z2yWUsvqCZOZ5sTOfTF2UUDs7HXj/vJ0Z+Tl+jD23ghz1
RPYCK7JLIndRILblTOK2CfbCzTguTuKDNrCJoqRWYSub74olNEbF+YxNaF3dpBI1ulzaOJaTvZAp
JsWFrcXtL9fgEYeeoQBrMEC/n0IVE+CSZZyMagVHakujqN4chcxnfFSywuE8SVxSzScUOoUSB2jK
VYtPLeL+2AV2Ox9Nfed3XDbpoOu7+n+catgxz+fhBCOhE2fxosuYOavfMJTQsRBMoGM2t3uiQhVm
TortR9n19IdsZGMEbTKGfjXS/M9WLhcfMlUYeS61BOcHavsy0S3CRXIpsp951vmIK70+tZP7RPZG
NBfJgG2QKNA7s6iuLQ92/qNOu9RVp8vbwLpaLoEkrqaGmGizkHWb6KlZDBPzLva0CmWS7pPsYTa5
eUPtf54z4r1aIK6JxL9UgrSmwyn+zKtaXBiIvdGtC3CIaVS2regC3w146NwbhLdxEKTUaQqrIu8f
/1PJ8A7eqKuOK6aUoTRzj9yPsYW5o6NjNtwU3CPP1zEH6O4+PixCokhbXLF6lNKE94GTQx2IvaqA
U/1uGhy5QO8NIJch+grneYPvrfbzantHFNIDawtviETErSFqJO4LE0t0nABld6Lt9hQr0EdiV+J2
HQHmqkMD/+ncoRTD3xfrCqc9Na7E9cnbNtCvV9735/gW5HB8NkuvwkSilC+3Y9oBu9OFBBMB7axx
62fPCVZDqtnsz3XhvSEbpraVSYoOs3QsNYSz32F4Zt2/FlrybljqOge2nTw+OKZZub+Si02AtYsQ
iGBxs9wdFuNMSO2FxzzdgGSyQqOB56LW5tbhwSzbY7rOoz3mDbZyMEk7/8D2Uf1uU0iNLdm3vSIC
5qnThN5LagXNNJ5jS6MUaK0GmMxt92cWGKEn0qQPDPzctmtANVWnNkVa00zAMkn6K39ua9hFXIJH
lXYamDDVb0hAN5YM6Mb6xwA1hwiEjGgsggCG00ajoGuPOnDE6HekZb18ZtOKh1gjXT/6NEld79RQ
PwZAhCoJ2c05nHL930mqNPcSllhwrl6Q5sYl9i5zcgZoi77oaWY3IRHqbNjZasJAWclQnFGqoXb+
+v/mxBw9L+Psz5LETc8RoZFZv2pD9+Vf4s9vEiXZAA06VTT7IR3aLEPOkEE+J4i/BEroWBlUI02D
pZOSApwJ1bu6mh19moPkCoJ8vCr40iAFKlTeMcSi3W5OF2y0ynO40nnBDPgVYbc/uvYBJrsgBi6j
gtSkkWRf4rEcB3boEYZXbHezhktdgjpN3mjgodVSx0C8Ogt9L5cSpHJV11MhP3ZKjV7CXZJCGjze
4LJSX0mY9EzbORDKU+V6P0+RQ/Q+/ZeIG+qnrqUPyI9WmjIWIZn0A/H1s4/EkEIU2SRhd49dnz1N
0NjEOabhjGabOzy3OsZ6kqpGAeJTg8GSj+jJ7NE2wRaNImZYaiCBfg3Awfmr66LlwQ/TfEzs7EM5
cNjFnzB2AnDwqwf6dkMJ4SJUjSaVo3GnKWkfe2foiCx6IQcMyGMp5Q2bi0+Jsm+jizDfHRG0wFDZ
cjuYUOQausBQi1Lucn53g4Qng5SDD52UdliXxcQlkMKrhOScX+DuXlkWFqxKpBCNBKXRNQWYBZ8b
gsYDVcJYP1jSwRyHZ0ltiwdRT5OzLUIm9RSQJHDWfWEW0chnu5QjP9X67guibU02pZNNsCSKPnPt
e9fpbd3BPSX7RnMXhA2JaY7Qs8BPdTvArDXfpEukFLZn3xi/Xi8X64Qi9l/ly/UQc3bp+XJyV03t
60YVTW5t82EFWPcGaTUaQMRVIECssuzm2cxXqtr83QvPt06ZnwbRb0BQbpnPZ2sO8qAcSi9PX9W2
0VdtwwjTC/oAX58pN+Uurz7M7cWmdu7G21C/o8InNrYHrJpw2otBI1Gh2MAH9cHbbSf+2DGW1nOY
405iV8ZTisgSSXzSR+cw7vC5E7HpMocGVP5ZXBpe0DgEHnWd/7YfQ867WWZ5azwm6Rtlc8kllma6
cBT9ucVMC6+krv0DjwNp2TFQ57MBsDj4OCu819nKmJZE8/g1ZdfLd4bTW98Y65WVy2lFV1b36zPi
pUyHppd+jn4b481GpVJQvfE1Vss2cmzpYc1y/RCITtkSLOhhjgrQQm/6xoTGcFaiZ/ogiIHZZ0r4
7E3REBRV72psb6E7omBiUAqDkj2LC6LHKcIjOpzm9wfRWGNeZbdZYcODl4ZQ3AXJb7MBhnN7d1xF
ioJ4D77bps19Rv6T32YLXpf59u9IAKLl97REGO4Tw/F9REe2ULMZrUuhffz0Q69NQXpYBfAVXPft
BeDrrLfjx2CxWms2MyNvNkXDWjegPi13zk6Y0OYVRR0Ncvt2HvE6eClcuUYo0teBJF1psVXQrgaG
Oe3fbo+BHaAfGLIimrM5/kVFt7trP2ySGfaYO1hJvCVnYn8xBTTWr9sPFzZ0FSk7fqte4zcvULeB
+js9NCNSztlR0HIarhFo6FpL7Bs9CTV8wPJhsr4CP+Lw+tINxcjilwgfrO4CD7yUNEq6QnB2o5xX
4z+LmkuVyWVcd4kvBgGFMg8kbN0bgQp1vE79yH6fbG+w+FkYM6rdGNWlBcFiDrjBPj1JUuervG4N
LNR98J3iYsAwRpqYtfxi3ooHmA78okGrA3tGx2ildFJ4KG1b+iXSaU3e00KlBuHIqFPy5wKO9B0O
ToRNfI9Zbdk0e6daNzAatBvbTiIwIBBi4/hJOGaN0J7byIMnnUN2/YLM+l6iMCThxcygvPlgI25y
6HlDxHbPOkZfektbsg60kq+vBQuBGH8JUbruHMyu0NXeyzWn4iWqVaWDBZtR9zjOMD3z587opBiq
dq3+QAUqlqu2e2bzd1GGM5aSpobQqw5WSRal1Kqj3PYpoOK/31T4wS8CxOZIvyVcyPP2y3XNgeDe
uYEGQDcEKgIY5nIINutIhFnM27oTR5ynOJ53F7As6SNnjDioHhdtRVNe2kMsBabFVzFNuFYBS/uL
aE8a84PLys78c5osYPk72VNGSyGoAg8r+rEZlmluOmHzAXmq2y+BCRCxkFzAHB/g3ZafyMYwu+w4
PMs48wn+gDdov5e5SVbMFVRGPncVkSrMiRn5crwXSjU3bgWIwIQvveK9CtIfwLJh7KlHgiDAtm1d
QRJRGKHiAgoAuV85jdmdCBBb9ikY2+JyoNxWuzEnJQeRVF4/Q5C4B3JZnuqgegbWvFTvLX1OUamd
TQbebBpV8a37tdr2cGBE5X/6oDNPmNDkoz645PDBiwmJrj2tkpTsouLldIil6WqoCzIBLWq0rQJC
R8sJAxmJAhaWBf08nzxNOV0TOn9gas+NqKQuySHuko8RqblczBlgpKNPS7gmv13cgF164BvbIhWz
asxjfTTvdFO1Q1fY6SnaeIWKYJcRMMJQH7GSzNtMLXvNGde6CctU+IGh0SXUzDKU4fW6RFDpLUWd
qxTHxFjAAUy57AUgjIPtF/g6Hqs/gC/tGNJNCRuWgsRtkIPJHcvR7JEUo0I3J95oDVDpBB+C83XT
wbeZKfHhb82W5MXrnXpGtStQSTeJynXd9XP2FQUxuhCUeV4AYUd5DiRt/OEmDe+rVEee7ruHWwKb
QCP/2MmZayzRv3F6eRwiqJvv7+cjKiQYc3SLcmTweIJftLxy7BdD0+repnCMtfaMPGvJW6xE5eQt
d1Sa+4IQvNQLXFCTMp4mVSmeKgZbiWqCp3bBl1gjXnW9j0DHW6/rJNgwqlI3Puk0M/BdbDeBRmPm
OOZ5GaOU2Rc/kTYekXG0nLrb0t+vAUzuTO4wUb/0V90c8mZNsssXrhWdEcsKe51b6ZmGNErzTcUy
Dpvmi7MbnTJkCIBZp7IN6nSz2YQMLOTaageOouE+PX2S8u8kTugyj805FGGGU/Vl9ek0h0h9iqtU
VYVaboE/1YFfL1C88TG45cmMq9MJQxim/xXYwdCfTqXyFb4FQmUJx5MKR1FmLAnjE8E/ME+VmtWd
qdpOHapVuSdhWAVyqFGcPnC1+qSONBtrJA7yL2yZ7gT342TYDgefbjzGlN4ZkO2KaopYcPHkfiF+
Vt/CJLnu45DXy/0qB8gCtaH8tllCYKUGzvRlYmjLHw9jABLneAF0KCoEekZhyCIGvAkNYHCBqO7e
pOCmqrA+hFj0O8d8UZ55l7k5sj8XtsmExMH49KTO/LrY5ljXjYWodcWZSC1bzUomNRuUB6gEewsO
EPfxsclpIlo/WhV6tLUw2HOIcZbdnIie7aBbagX7zN/0ri6eKufg5XmUsV3stVRg/Gy6yj2jehba
a2b5sUO8C9mPD9N+ctgzRYRNqRp1oNPbYbSI11TpOI7A8nCuBpxuTtKsd3CX/jEvUfg8eO18nDme
RHhc5Adp9WhLx0BlyI5mCz4za/sc+A0RMhs52UVSqPxECweC00bAmm8tkalW96ogRnAIkXYp6HwU
T7jvjDiv9wQ1Gxyn1emB1lKBpm2dZD569HnFdxfGE+2i7U5kRMe37Py1ifSmcJeNrh0QH9EL75JY
D2Qng39f1iW15vXkNaudIO6qv0Ch1NqGsAL5JlN49ZzIvOMInVEjtgPIUBn2vRpaK0Vpd9TaXx+t
ol5vyeGKTPoSMnN1nPatBbeFBEusjvXdoEv/Xcop9n3dkmbpyHX6w+ui/QKbUJK391d1+bAI0NP0
/Z5ZKuf+RjDTUyXwvm7NW645okzQXHQZme1x31GaVJ5ICE7aN4hUkkkcvUVO0R6ru8+T4NxUjAvN
T0cbE57/2I5/0dJtR+ukuQob6Ch7F+++XeTFsufQAXr16MB/7hShJ75zyDQNdCYsqBukeqYEe+Rm
eDbSdNOSmQo+pRxS009fK8WHBf8po4TF/VVFeMcjQKsdw/W7ewnundmqSsXTqfwRmg60rj9irW2a
mBFdP1RmqUHx1GAAgE0+dJ9BMZdsQ4R32rLnIRe6EBs7IHlnIW6AiJrnWtPyaB8Jshq+SFSBxpvj
C2TYlu37lV6/lXpt9y6D6q/3GCBUdxn5kc7eYJk+cDdIgDrdsW/iCcZs2mYo50Gvsmjrtx40M5Aj
KhIEz0ymtHmOLmiPTSFRtAy35DXxA2ZlIgENVjVwvsx2puvNFAQZGaH6DAQu0ie5FrVSuYlMBGm2
th9B6fiCd93K0GG/wiR3tjtilidhAvXfJbHgmcqoOn+iaeZP9VGN4tiH6rxM0JeFE+v7jzLxSjmi
scyUzu1HHgL9L/+EjGVV+NnyPuOu1X9t8isQeJcHP5oRP+fo5CIQGLWu4VTSJJyUmngVNGjU0lHR
v+ofDNKx8hLozTWLXLV5PMv+2dFISsTQspaOmKQRYtLM4iM9VZuQ4+l6Vy4Tt9TmEOgXuXSOlbfq
OYPDXvoTXrtiyEbo1tuhkqL7fAmAsBlTOZOpSjZpSeguMfvUVNxqRBLPCDlre47UC0dFbIye/6+k
GsMknmBZIfUlP5ZzxYHy5FvHUST/t4cAf/U+g2E7skQBfFWeL7GXSTtPfEgSZllOcyHnv7ruIDn1
UH1Cf4FCSldRy/7XnvQWUgwE3ni0Iq197ZEbwk1iZOKCuT6GLK/+A0Td74Yl/e5K+3r6/2XugkpR
Uuj3wnWdN5NNc5NelsUfef90ApPGdYRuuhP29yG2yQuithyWUZPqB1pCN0lE6xwDqi0q4Vlh3uC5
hMz67TnQUTMjgysy6FMg7GD6uOGcx8lj233aXwb7ZQanVio3b9rJaDGDFXe6m+2k64ju6Qeh8IJJ
rkYBDVHkpwhgInmhp/BBlOaxFYR1JLCJjIcT3t7LTL72tiqzzPyjW3a6APHoSc9f2z0HskMNHGN6
V4zdJ6BsHxrXmydIfhOrmNuWIJv0Qs2NNQL9PuP+Ol+JFydMVrU7t/TOAYpYFL12bQRr1T6XSH/2
Bpq4cpLZm6EbnX59BMoq4ihxQ7um4t26kQO1xFlB39lz1ylHSj8TMNEUPx5fnFKcfpNgDK5A6/oV
o4OAlMhmMWIiHgO0VouF/pEin/fqtf4J2sKdlVI57nFkmnMP/466mGUS+4grqxUkFMzRgx1CS/NW
i/bScVET4I0akkw/auF4PrqiuuNCPcJwi3jm4jlHswOTz3olk9wn/T9+yXBTyVco3vL4IWDebDyZ
uoP8bEoFQURuBO8SmhaiEEwy5stbvqlpRCw6/xq2VqHgL4f2yzLAby6DoiIXJCzxfOJn5MVeLvI1
cHHkbL4V/ud+HeV8Gbu9kC7OzePsdBsvvKCF2RnfnX3yzgunUjybt5bhBWO9A8pToVErOTiw0Xzf
mQg4r7X2JsnxeLnVQw/78CEdPecRzkIV7Iu+i1S3FVWmu1++8PrRG6P00gkkQS+9C4AMmtD+mL55
g6q825foMR6uwvHUZpC1ap/II6BfNzG588cOrsNiUSpL86XCf4CNWys7OyyRxCQICgGgME1wP+d3
te7avUJEYs1npDu2fWcdqVY6rFViFDzOaZJuUbWJ0dNwBtAT+lUjJkVIMvafGqAEmQ4oL7os5wVG
vtemyek61Vhjlx9Jj133DOhWt1qY0NAuEAMp5Wxy6Vzgh0erqny1jdzq7QlThp+BTFtBh1QuhExA
GJlELzjNs/Ymqti3PZek6Gsh/VaCD+aG/tyI8FSI0SxHlc3UynBozPNuxV1L5do0Vlt55JRZxW0y
Nu+QEjxhiREFPNwdZoqBmIgwGYGUR2PAQl9WO+D+b89BqvxYYhvb1gLJVCZPaGsUakGLemZioYzE
AeZitakL63FfDnQFhLXEjgcufE5QJ4BoCMQLlVzrDZoCJjfdeBZDrYq3FOx9bnVThwjYsJrlhw7E
YXPbjnPP8gzDTiawL7K2SSo2Aiz+3JPowUalNtusYX8EtGnt8c7g+FwwXFmn94micb7d2fIFN8JG
QzrJt9PMrMJVWhDVfco6BfGiCtaOqy/bW6xRKmjUzO4vxi+7Xo71+lXBv0x59t5KVkSmnIdZErC/
D4PPEBWucHaplKo91C++ux6F6q55b9pCfQBPU+Wv10qlNCJTn1f/F9+MMGiLVgV+z5ZoqyTJjtUw
9fjizRGeYa2HRevdWCO9jLSsRGL0OFsAvoxdQ1XYlS+2BPKbHD8OudtVhsebuafRR+KMJ4rzxZf1
uKRk+o5PR9SDITi1D11oy+5JCVGx5+yZMzMWGYragkz0bJEjnLNbgOheRU/tTjdtqdYrfiWQjjsZ
CJvmOZYkxs7o4jKSVjvHRfl7Oh7Y5X/7HG9RVMJugywm1axqXBmkv5DVsxYwxWf0Kmuy6i8uR7Io
QGGdnb8F+ZUl+esdRvOCXGQf+FM0rouEwXDK1AVN9+4XxDxOynkUkVRT6IJmm8D87WI6PDZCwtZS
h0mA0g+TAFF7dSDM7iJsnBBhz4nhK9CqfIWqkhJmZxyGf8gQGbAIISgxCn93KiBp2bCvBNewURaK
VshuWyhD3VAtjKJVV5ihLajxJuZJ7USspzkUgEDlXLbXQkkCJlOsc2kan7IWfHzldZUihFK70EvS
l6lcW5nMC7BSHA3aM4mLqxfxOOUQ4x3U1NRMkkznKVG+TOIdUxgAn9jgjVcS58t3MnlJMduZmfvU
KUp815asWnxKegkP6c4mN0WTHWTSU1dmVxte9lD7hfKfsmlbo0PEUWWt4geR3v9HwIcH2PVC0rEx
cPI0Je3oH5g16BP0G00aDQKQz7kLZO9NgG9U9J89QtXbBF4eVWhgAXDLIuiaCLROZb4zip61QaL8
5Gy/txVDgNMj5I9FbSuZWmFKotcxs3vnE69znJBjD0K34dCpCHAUlG/HzH8XH3+nLd6zbOQmFiUK
hxGEyasLf8+NyZULH+vA2F3u/xXkKtwXUJx+etXUA5O8yntxjQ2rvzJ6mGxFzUPnpikxhcQiDq0Q
mEC6Sg8n1cTkOhuaTAREe0q3Kp4zCyOCGEpXZQDWGnXLcQF9v+59WyuZcTvU3T1XIrPgF9mRFdNp
zz2ZnfkqM64bdH/b68q6a0ID+EPiEbQPFy6qyJU4lLyX6GlJtIJDA8Z6L9Ucc0rWIEG7fa1YDmEo
Rzp/jQF+ISJPRXbWodEgw5zjrjwarF5XmG9up5VKcINPcmzGcZzQSemi7kzyjAg42Y/D73QpHMw6
mLPQ1r+4FGBnjsevW7OJwD3mwxNUfBtLyOokR8EDQ4UCriLrlpW5M559j5mgMnAZqSNivlLVzlIK
o1OiOsy5VyT9+8QwuGJY98xorz9F2BQxvmLr8YWVd8dNYN99ZgdTH4TpoQ/B2o3c7Z+Zq9fDJtyu
uUz5M1FLd6OU4On8sDPZ7GK/1uWVQlpPEPztuSCPndFkOEr1oJpLoYkBqYUpkB0S+Q+gAeGjeUxF
GyepGMsLr6ldqGOKWVPXYq7rRAS9OT6QjPilBqvQhUdjJ/S28Mj6dRw/8mJAEaU54gDTQgfz2KMZ
R3C6T0+4i9siLn3R0TjRmozu3JfqYw8Ny87FKYETGlmuGSFlVeoVs8l2gUo+qFfMKqioRb9e2ped
TN2YNUtfrs4ZHCnvPdsd8O25xgEl9DI35fmLIah2InL5taXIi9AKGTri0tJho8R3RTV354O2CDsh
c5k96Jh1ywOylr8MPRv5QyttCkZHgYrlACqGGt/Zn8oA4v4cD570Iuh6Nwoxg2eLgnMPZqwvosTA
kyKFZ0GuJDA4nHMLv1BtnKgeZX1puQUAEo1P/sWxRKa8Fb1ysXd8qkofRXtV7drnA2hIfikFCCHb
EwL1VU3IhNP402xyN041wG3lUcF0GdoMoLuDYdZWViheK7tOMAW/MDNCcFV/pdN+MJXODtI+ddHA
+RSAwb3szq1NZLN/5IzlhulZdxLLo64qmBClcXvBQEnKJJPCG0v8nSVRlJkHqFFadL2u7BqLumCp
hOioFlqRurNQaona2jk+GbzmZXmHEekjXQkSJNEFJnh7W/lA6shEP22gJsz5+wPJ9TEoSzYvNXbk
3zKMIsRf3g5SfJvaJLkCUjtGwFwwLGEp9tXv5bNKhmSc5Rka2VBMIe6KOSJBZlNHHEf6x/qL2JY+
RG/tcgCBGi+NDop2j9pu6oR+8cQnDWPrQzwQ6EDNQrAz7y67a5hvoHEEW+f8bVmF19GoCQHrCN7g
SdLMNpi2OnB+TR6f/Rf/cJKP0WB96z+0ea68LrV+gdvwUAULqT0D6MxoRCbu3zo+7zJlBxpRotRm
zRnQnNf0mISTfB1KIRBxkBJj59AW6pJ021WNQP8RPkX0PgQSNjKh7J9Ro9seIMFXyC+gAYATiQk1
f3IujNHBdfs/gVJ9H8ejFbK/pl98iV+hzOuqsfon1/PJPqTE3kj9EA8lOtBdgJy+fH6mvy/9Br48
Pi/ZyUl3b34OCom/m7xmjFAGeH1UEfmZYlSPTbvAvQgzg+rYpb/3PPP4uMfRovj8sgOCwg8460bc
/34/6YonIZtyP9qeZ1nKrIGDEgioOjfUqyWA9xmmWX1fbd0cWE6jg39HrWFo8ctcXc3QyAMiXUsx
T7xfwlJ0HG1THcMtFkucBWy8GSgxTuLiaTK9nqPNfRBGZ7ybhscwR6OOtnBnr6bBW4IWD1Prbbdw
qroiXEzdedhCSzzZa1C5F7eYuYpbR0JN9el+HmgQW6JCgKkniVkWdQ/VCQPGQehLHIrAnSLF3BUP
eQ+PCmw1k+xHzSTU4udh6FjLpUO/aM7tzaMM+rjUGXXxrG0MoBI+TZtfvh9iWX8vdiWSF0qjAGUK
FL4/5kjz2i0b/MQAtDGJhM7fuTW1jNRVz/R55opjLg+SuL/ATqmPZK+P993fxQIne4gfj4102xCy
kBXPyyAlLqOAeKeuJqD92ol+W7pZgZkPRo4hA8xDwVo2IQmQb/gyS+4ELmnymHmeSq5V4DOjGOMU
WjVMWdJjNFJ00NEgQPRCwNqmHdLEagcGNH+8H4cQWVU7YDFro/7yyfv38sV85WMmu2oSh20Pa+u+
b7+yPpLeEBbOOZY1BX84NUxwmRq1Pv3GYy8CnhepPssPStTk8wksJfMt4o+86PXGChnIaaZ2+Nw0
XpLp03Ttj1ToNQkLl58kFpGrrma5UTS/36xnZ9Pw26uLdH7jhEuWJtcaV3GrDYBqgczrRqHSYFti
R7g5JaT4tFNwiuXSdVSzqtwUE1sZ6q3CjXnGqMHPTCXKZMrup/WDJEEGr1UXa9bFRsm4AIvg+4Zl
DxPcgdkLF62M6j3z/BTsZGKXxlm2W18ZClbzv3wSDLogVfqq8jAi16ZXVx4qHqf180/Tkidz8okw
2EO1b46zteQSXOwH5jf4s9FwyZNipvdjVKbNg7ep6XXD0sq5pfy4w9FMo75yfzn/Q5EXM5OBVqqU
F3kIgZ1Oc/1PvxmUCcanwuO4v6bzCOrfQRGo2DBZV1EtmV8oWuHHTNJeOvUZGwooNKcA3Gt+aBV9
1cbPPAkO0Jbyge0Km6FsonswXQmVkJzG4U1Qc2b+zxuFHQWx5/4XuQBWOS8jNsrpCazTdoAoQD0s
XLURrsLZefSDQ4fK3fA5NOFULGF86oAYCD4E0IJySnc/GnPnAldw5HYFNo74N5rqUodlWTJfDntQ
TJa0sgYWW9dKNAfZgLOIcE9KFPQR3kwFSPFNVCZ1CPUB4xbbFYp39kdq7oDYNnGIEpxChITqhNgR
BWLU5OA8/VLuJpiGh4FW3yVhXixYiQND8PJRRVZWgZtBrVTRDe50GT8NiEEw+4kAwvJczKXwYBiF
IlVw5EhoZLhh94lVjhzxpXhXX7Qnq7JhTZWm0D8nBC29YLvAhyElQ/v1yHi4ZSYFaPFUtnaBbJZI
faxP37wFmJZeUjlrGP5+fgmvrgHh8bTEKBiJZZ2Xr0+baBd/Il44cHhtYf1wzmh1zqBJIc6v3cX/
4QFqPmgpay2SellxmWl9Emk7tcIb6G3SR8936Bhhgu7YNxHx9ckko1W6w6u10pZ6KSqVXnrzr/nB
gko8I8KtJoj+TbFo2U9BKNRkmMzKpmX9Ewav+pTOhKoIXor2Bi+Kpx0bRPO9bM5Hdg1zU0o0cLoV
cCraXc2b3UM6Xd8IeNyFl2suPbr+Uy/eB4zXoaSwI0ZGbaWAkXYEupPIRF1p67QK2iHrOnKhWsBX
8vMc+DeGctN7VQubIQv0SZOwFDFLaube/7LW1BmY0r1kkngev3gnj/wdr51KQJHHY/09aSCgxZCe
v6eZPpVifa/9JFBoRAagnBVzaJUjNjyoJ4nNQ/GcxnibRRkdybPBt4ItTE0lvXQpRh+DXbPaQ5C/
JxsskaZvNpYBuST1z7u25sYzsDusjCf3zmys0OkGHLaN8A4pppbawmzZOufttpCHbiNlRCDGNtdJ
xel8gReud3EgcytF3WPGoJJsp3Xm8khc7u6lVb9HDBFdRBEpyBpshjINaErzBdHrXe7/VHvYsivP
8X9zbWqO2m9ECobR6Ad/kiXumLF2cu5F7pREbAtxZY1iVJ4pvbfgrrhgQm8/Mjk8FbJbXo5WVD/c
ZjqtLruD7DZhZsxI6jf/RBK3H7xfYCjUEziXLwvbhtj1xbXjqpeQh9tfD0u3QAc5ECtH41YwNBZP
3VZuF3fvDSDtC6f+wTKZNGn7xmbGeuGoTtvEvZL7ugpGxDX0jjGCZLGQCq2t4dLkRmum725i31Nm
zbbDy88hW8YHDBLIXiKo8YH1VfELTVywc/Opo5e10ePk6Hf+/UswnM1lF4e3VAnkwC7MKlPnd92S
4xFy9vMafTGHaeT34DtNirNZ6Wnxpn0VQJmTJ1M+fAeV3AD9aCGk4dqYN0JTFdnv1ya2hm9/hXCb
MT7L3tC0RBMmYVaa4iCSzHUiGUUxe145JEt51/sEcqO7jrgY2ZpFRiepiDWX1Y6AAxZqO2MfOU0p
mB+YJ8Z+gghFQ0vXQUxXKok7cqHCBFXbcC4PArz0AWx2pZJ2PAY2thHoteB2k/85IuhtXL/hwpVu
l1hRi/DWi8DaMEgXUbBYyLs0uwPQF4PYVdV3ueOdSc3elYN9+HzWPNLvxyH+XmuhwzwZTRAlPQ5f
Zg8ZotaEaH1B7MYCDzubZXZITnmXQkTYp9WQoeqTa64qPiyOLeLe1eHNTlgnYdt+3ynlPfDykuGs
tsfl0BN+ffPy5Af38VLnHk609Z4fjSG2USxfAev+Wuundbr6GgcguojF03xFIszni2h+cwZUOq5y
FOXLZSBs20JguCEt7PwYQDlElW+ZC35g49zSOUxp2kS68BGED2H7KkoTzEibRM7qSIgVMGIFuCXk
qrAvPkd54p3Ueav6e9b00XgMQmsb65a2C/lctaWwvLXzYWwG35u36nKsGcB51k1qDbbPhAZHomtp
mvNUdB5VBx7dwg6Vk3oLJmGIBiwtSr2UuTx8CnR726SkDOT/SrLnl2Bm71FXyuytrAo5JKW0G7oH
ZwQ4tazHlO53H2IVH3tOIcqk3xfjPyVUOrfwmuDWyGYbpbCakDW0bAQAo6N0OKVjTeXoq1ZjiVOY
2InMBaaLD4V7d6O3Nw07dhxlhGzraiLUeABzAnbaJbSUQP9TfOvtg6NfLq0A2dCpWZWTEGHEftBO
Xhh+yIPysaku4leNnIWbxjoJMAiAqIstQRUUUPR1CW/SGkiX846MZulN+k1OIu4yOYZ0momjNhRn
wAE1AoJD41naMEGKRdp7sLk0KKkVUPGluanCDUg6lpUu9L0zUb1j2NcOyxXB9En07xz9ipojlYds
qRoPdAz2Zl8OgozmNo0bVtrnTJn/xT17wSRIY234kFexFAuLLrhX9Z387M1qkk0eqkA3q0VXaduT
C2H6FRMAAyQDBUcWA+IxKu+sKmWWBBnmXB6faTsJdTmgG6XoFLM+iRDjKh7j2a+uS9L2JQqJuFrh
h7Ff7tKtCjzKIguP/u+aWCpxe/LKJlBUrsVy7QzaaDBDd7yZouj6FMZNM3wYLKyAfTD1vgIqrl04
PcMjvDoFRlzJPiSy0CRnTx4P3rqp0rkrRjhmar8dWkrTLZao3K5MhgAnriX7bDH2J+r/DeILHBzr
IiJPUHDwqor2HxThDY514SDY7pT1mVl1bOG/vMF9kUQbNRltk+4wqi/8QjhUQsImlPUoIMPQKSQK
G7EJlZhjbybW1eVmDqSlUUDyKqlBKf3vp6D7T3d2klDJ5zFiggLbBefUYwQgu3swN7QqD7wtFcTm
BwN49zse8js/VTJUTdQUSHC9+E0XSxuTazrLQJ1sx2i229uEJ7u0b2LAGbe/X9emx4UEjV49LL6L
q3LW1c8RR9C867JqXnBu0atQ+NxpIAf+wdkIrHGrGvY30UrkID6xfTAAGIw8x9p6HrwaOqM2/2bK
aOmLAwn7NIIxRG27s5TpBsQE/yWDGMhIBmL76z7JKzwUL5zHYQhHrrfqCkCGgl6uOxTzBlQEcHiH
dRTir0yjqDe8GSzH1y3h9+EQZ0pFINb1QQP7rCs7jOHuhBSZedT4985iQwekvO7qizvounk4ccgq
zbFNEyuAwVtEvlw1G5cQ+GWBXoNJvLtp9ENGQPCYU6GQ6qDrwoof6tgwZ7llLaga8yyoCjk0uf6B
go+7dVsof53QYVEF8GgDwN3sfY1rCLWDx5mywq3d8irI6UhNbxBx2qtKGha+G814/NMaytYaXiwL
amX0ASgxvRrwxEdiXphVBDTrps10avZPvK/wGjIvKdTQpMUJXJCZrpRLSDqOpEd4e5plo/mxByIl
1zyh6MRWVZTLqLU1sM4YVgeBLIrmqndMPscpPjrSwsSebVHWshZpL6TGsGSaXM6PPHEarJjr3Pf+
hhU8Tv9kCO9wC1cB9wGPqn76hPo3Xz/cJKpZjGbeDhyd3Lp6i+T9Ft9wekEByc7iE9CtAz9E+ggZ
E/CW4l8tKYpNEgBrWLhy9WSe7HUexM2JiY+ah5CNhTupshdDX8+uKNkRZGoguA4WZvi8PT/flCmA
V6iuEfouI+ga0/OKyrZTH962RG/eri20j4dX8b1ijOXoCKr6dwzcQwtCj6fHLLLzIRWqnn/lBOIt
p2pADLVUp0i1NZ+uCeC1ppV6ZOAs3I5pWIHP7c3dzisrmuWssB0euu62V4y+YAl1mttYmmYtvMnh
x+4tthB6YKVpUqNesHzWG+dNcpddOKZK2A+6JkYdZKH8mzDz/949Gac6UwraeVao9/wEEHGRLiPR
RSnT9WDo87hMVuv8LOFI6S/it2PMVQdsoT6sxLDizVlJtS4U4TNRUcGg9X0Kq36hGwCVHGpzHQPD
yl11qE5JK4GmNKpe4+lHYY5P1O0DlVAwNGCwkHrHlf/7t854CaUP+eZh18OKKF7B7eoxhTEAHrya
TqCKFJ30jIIqHtOQT62f32OHvEPxF0yOanKW3LsgNF/dXeSAYMZKtOnPwGUIz0WvP+NBCPno6y6v
j9IMU2DKP64B0ioImA+uHnpP5FWLudSmES+VTWAsoPsxDgXMimN9mz02mOqnqkJJsdoUlWlK7pum
5zM9jJZo6+CuvyzDPx4vZuQHKtBZl8Vd/+Wgxza60uIF91HuE233zFMFmuWPcaaKwKZibYxjzGbK
Ru3L3c7q7cIZJb5XpRUg9rpljA9GdqZCySgcE77EdhNHjBH+p06bP5JCuBS4DZxEBBNSnXoTANQt
YR4E2zPTK3HjrjclWXLv38k7rsf21tvSf8Dxv53r4NfSY1/rCDVb8bXR5Ru40KnJDkviReef3OHL
tq0Dbtb43SzoaAYTQ98ZuZedS2iEV++/2tlGkiyrKT/JsGb+XjSCAn+9VmQzd4+37rWeOhSnjdIq
9KaiS0JOoszaeqVyEmEOQYoidOth9BIghqBBkuxulFazAli1uS/fE7plWZujYitkEjB9uecklM06
hJE12olbr74g2jt7flvnLQYsHIioZlIw2T1pB8plRoPRnQ3lR8NescqSXi7jUwCGnk85DNRMnXO8
ZVSBpTnkVuo9NAy8tSq+ttPz1G1gGEsobJaYPv2eCImC271PO80zULJyS726ch8oe8ILHpSCJ/4b
eq/TX+NfeWktlUiEFCUjYChUNN3pH6NBrD4LfXJQqhh3ij8FFj8cAn0XIihhJdTx1TD1q3IdeiFt
E7ypWNWU/b82FwmOzTAEsUnFp3kzk53uVmYEFXntdyqFE8nESivqBt/ix1UTCUVNSu96cBK+rKVA
FjB8nMffvRcmUitfB4ksb9PaDltS9WGHWyGBVhWSOEnnqPQ0xMdxQXDOynN0MeoLGCaOaj8P+jgW
3kRDB3w+UO2B18k3SSNhX7oIMEonJZ7Tf6Ag9JjJ+v0YFxM+JM7W2bV3IGty4I/4AjbnYjrSkrCJ
fbT9VyEuOyhw0WPAcmFe6fymXGo2B/rVqTBl4HEFOoH8LI+MkHw994CipUrk/P6A0FPnazLgAb16
pjpFGWx8Vu2+TtZU2AZdNyaN+vfPsvDVxNDb4rLWIMmOf/aIrOzcPHMnnCDSpWJBx39mlWJ41HCM
CWiztmTmqKtlSS7WHtOjUKwItNoOB3qGX8wYgSMjBtPzfGriA49f2gfrtEXSCuZm7M/VND1z6iB5
12NqwOQV69qCF5B0KmxuAPOqAA++LwyiKF87ggoqBofKvtS2qN/3LHyqjLL7Vu9fMsgxDwHk/tcJ
ogOBUqZMb/fsnes1Qn6xNTjBaxOaRz8ABlTSsKiGKZu2k2SrxRLPLPnPpedCG1zjNZ+7dVVbKRxO
znUHRCgo1nMBYhJg8BsLAL+8JaiAixfPGEffeWUBWL84S9myXHNw2QZ2WSV4M8DQiKgrTIWuh1lp
O2NMZ52OxQZMFEpibcJQxbaym/DLg5qhDCGyl62ENAmhdJKLKcZTA2V77NK8WUz5YQrW0pbqtfZs
aFFZXJ7jUm/VzJu32emneNB0h8xTUhVFBUq+lLaGUdtutsu8GGkEhqFM05gOHydIQc04MlLIGSwM
JK/xc0ahtRqjzjR/ItuUau0lO5zGcG57IzIe+tVqHDc57HVUMkoNBAq6R+u3bFzbofhyFyV/+kjJ
3DiOB5GH9GilFx85BwNO9JBd9Gb/gRPIJe2Zw6SZJewHPlAUadd2mZmmUO0k3FKPuJI4PUrWEYFj
X2/yYYWNRRTW9oq5l6+JVEtIPTmryQdP86t2ITg7GhfSfD6ZVhHQ+dfPGDieC3tXzGUHuXfVoN/Q
kK6sSJUh05UvWnYc3b8JPMoTrnCt/qxhkhp0sse+PhdWuPSyOKYFry9EACmFCD5S31jMtLfL+r/T
7UdxPlA2IGoGUq+vzsRqTSM6Mp9XeuFoKDLejrJCQB2schEliq95rWun6jHX7wWdzuFbyYJZyJuV
BhNUKsh7bNZjA/vtXzsyMryN+zigIKxRmrZY0zywrvlsDbEJGyMduxJr6rHWmMM1ZadUvL630mBk
VxPEjZLdGgzXXtUu5518DFMQ5y/CaU00GUj4YIkeUp8yuB7uUCStvqNsxafLFLjzHzxvAzVjse/W
q287kOzR5NwJ2ULXv9fQ9nIJ+5wiGv2Y84m5wF2jN98eq1bEPZfHR2wlDDOCXcHBItxpOrBRkhtq
E+CcZBxiwcw2Kdd3p0SlzkG1fOepzXZZ64VTaQcJrDp921nAYeZAwcbAPzcE424r7T67wI2Y5Gt3
BUraMzqUBW4nDJLcqXYXlQwB0gz4RQTh7eFih4OaI0OXWsfVsp/c3YEU5CmekuLtXSfT9WlL7KmJ
VAWY8KPDAKfpvNxeH0l0kToQnroeACwQ5asR0dBeoUPk7kJXmgPhwzAv8GXCy5ABoAylGwW+84Kt
/R6woEL3hDnyuc/n+51DJF2+KYfzZFRr8A+i5S5gdbbedXaSEGky6cppknB88GPUa+3vPidEmFtj
AueQIMcO3QEnzP3e1QzziP6mb1gi/cZnFVDZKGiJPXrgDj7dPo1oY/GjNOOfH6Dw9zkCOVVL1jbi
5P54/nRC37UaKOk8h0j5VNhHAiB+1wPv1OWALzN3lZebwDHzNaV/s3jaHB75p/OoGgnK3nCEO7Oj
gi5bZZ1yL5hbwAXN2O/dR79tMW1m6ezYU2KWDM4QfT6Jw7TkXj4SBvf+wFEvI82ZFjszgXg7mcaI
jjDWNlm7ShxWreCn2KM1Mq7MS6N3/K5Oqge8O0Xi/XLHAGaFN03zBQPy7JL96dCsFdLI+SQu3tKn
2G0STumEpqKhR4SlJWVMyIEN08qABYxKriVpCGurMk7FptbgzvPrBYxONfwtrzbfBZXXKYpbD40H
yywdUYMnVcDHuBYC2kcJZ79dYHc6020e5rsVFEd2SbVEuMYNHnmgE3Uj1aOf8i6caOPoFo/psWRb
wItPbCQqt7ZCWkmymc3Chj5/Fbz88QxjlCPFhQThaSlULJxPDf+Sc4I9eGcH8WF8fbneeoYFX0LP
Y46h0afoLgrLdadXq39pR56tcSq2upca1LykEeiVs6kQVKL3zNQw6m6oFR4emVqeqrla/j3CuyC6
VrXsjL2o4vTB879EvnLGgydgcOchp9aVRY4BI/7K6M00+p0oPkBNSqRnfuGY/vNToXKaLXFvZxwT
gMT4MReOrWxvt8+y7+2mz+WsrL3wOY6ehTmLVqtGN72jIvij3+qZJ+3J73R5+vjGCh+VD+/qEJ4z
F5xn8jXdArXRoe7FyD5GQV+7lFRSjCoWZ5wsNREecDIFG7m5jy7esHrCGOY1dzEkBIg0IbOuwJSL
2ra5z8WPFPKVOCr95K9hWfg6dWyj3OfU+pJrhAqdTXk5QKRDWTlo874c5AeIf5/zvJCnB1KSoQbQ
Tf4zr2JjWAnVw3VuDn4edQI+A3MhB9FjjCeHYlib6yW5YCHa90rhu6fxRHGelQnW0aFKIgRG8SKk
56nNRyBYCQ3rl+2XdXYu2BrEEnaA8Z7ZlXZ5AYna+xq2OPRSFkZSOYX3IiAEBlF9y3j4eGmaJbi/
GAS6WLiMhBh7br9Bo2ivQb7nAdDPF4J4AH65q06Dkk/iw5PTOo3S9jS8+w3PbTNo62qg4Fzddw5l
THQKg/MinIG1/8Wivq8sChjEPyxAHWruR3c8WFsKMEbQBehw02OCCmPcPCuS9CG5KyxavynDHLsJ
gquTU0xegXI3DTtOYbSkbKp0d4RYfpRJr3WNN6xr4vwPk47y19gobQ1N6SS/cxbPfxnqgZLyb4Re
YwismDV9cRwPBm2DcVOZVYDrzuWbsirGE1NnXzAVF0/xZ+qTNZLzPmmXVPh6yGyLpJmODIegdEBh
FlMP/qTp1WgrErYi3SriyRuYDi8FsnXM0rhzqdt+OtAGtc8Dz2SMr+BwxqcwT/X0lhcFkOmmBx5F
h+rFwZmn1VTTWhYcDzogCqweL6oSH7aSbKBUgYRZHENCANonBcZGPlh7m9ZtzQuiT5IX/Pfj05bD
QkmS0lODcVfAe40i68r33rycw4hnrQNF8PJEoZFnlFqkH1EtLX6G26ZWOEx10GUQf6hFAvNP8Jyi
aw7TrBEm7uww/+pHw5X+Ble6edGtCtrlfZsqaCQ55FejtdBiqcrbM/uixC+jLyyqsHa0lPjJZoRu
HZ2mzSS+5vCw0w4/91D65oGziWz/ygg7R0rHql1UqyX+FyEzx155hfd3h1VBVRm3gjj5+1F21Jog
TIDQ16TYh6bTU9L6ilMnJa4OTEoQWMT7vWp3c2ePH2Xb7lOpytdVoF4ZZSrsWH8vM4lQ8Yb5IRAd
VvIh7OmxKS+zVgN8basvfgvYH6UihmdMaxekKYnxIjWrwGrdjkIEnzhb+87ECCztv5ZC3mXDBCEQ
HBXBlzQSCdj4g7KosEjQt5tbB9jp9YhxrlWlHUPJdNbr+nu0gH5I8yG2nmfYMjuhBQOP3Cl3CGBE
voHFYo36MBtt2aMlbNe3gn9al5ZyXSWIMbGXoa4tOe6lGo5Wq0yde+Sxr0AvCbgFkt7mYJ9J57tU
VLexyHBCHykQjjA3FghRiDANHyiPh82lBtFjWP+mBO69fO1fdYMu6xP4J5eegdMKoBrjNxshW9vz
cRxMc4m197o7O9hg6ZMOvEpUwBxNeDM0r9AQ5gENIREoCluFU+Y3E/rbpjCg/0uDdRBilIicyPtQ
er02CI9pPU/ibUbIHRwTulbCxCJWF6tpYtWUpD/MP46qVKZwY1qPCqMUQvWuSxIV0mJQvGO2Ahnt
CbJfJwGzn2Rv7FmiKr5+biACh2TvhSg3xH9Q3XyVZxLXK9AR6mDnM+EayorINCCgfh7dJyaZ5Vtm
0E8Avna4dXKgOLuT3B+jJ9ULginGceJ651GcrPbgMfHXWD7TQwo3IXBaS+TWDjUc1p3Yz51Dzhmq
/DbBE/H26AYBond+f6ZX9pKItNJWn0zrRCeGgLxEVLg4F842myodho/gQiild+YcN6gLOd9OLgzC
yJlZrDTNAESeKy/scUaZextx0zBgqNk+/kVV/S4gv3frYiEhnCUeTRqdAm/vf0MC60RX6tHcTopk
9Jm2smurqaVK5I9WnPN6Z35r7IxSkhBzz4HxfNzhQEHPNByi6de96oSU2TUED5qojew+ou7N9TJG
2OBO2CGbmbcdPN3QZBQKnchhmtQDXaUXcwZ4lqPh8kb2herpVACByYAToImeofHOcqbilSAjG0Sy
IWlh8IjAE/AM4hSxMJpCphoB/WeGEc0OIHhkvaG0WzyMCIb3LR4kseCh79y4XU0yjRLRZSf82doT
4PByAVoh97vXWXGtgxazWhFAW5MU8D3I8f8CVjGKhrtBN2IAGIp1Pm3lPTW2r/wx3OqPBKRSQrpo
NKr6JbFWmB6+GeLJ5sJ47AFqQMnOunYqAONjpvtC3PTjiexlboXcxQprmZZzIeJ2nwPwDMx/uZon
TvdqrDTOfPRm5OY9q+TGlIxrpKcZ378Y8f2PNSGFUgWvN7n2jdN9CAIEHngq8YQsNlXquBiFDih1
MoXAR2HYXgINqCNb0uEPUz+df2/lc1Wi6j0fdszyfU4nSMxYyRRkHSRWjtlk354kXvo5UCPDn5rE
cuG07nwZVfehJj0U49P2oN7Pcve8ydJ8mOFdwF9b+JXObEMrkwdsxxzm9jHvsTrByCLUetSjnt2X
JLGI9jTEcFPuxPEOppNYbdtCTK+m7GMI7Zgut3h9ud0Fa+qZZpFy5qMoYdPDRC9dtSQZlEqmv4Cp
aQlxax3MIBzxshyk9cvb3ITz6vBqjVWxbMnQNsGOxvHEIhkcQvPGcIS5WVpVRRvbNtl7YCOLArBE
7l/86Tm11C/o8d+SjpVRzjANj0LtGq4AHwm1SF+szPl+jQfk7sNUISTnRQuKJCWZ3lDXEmqqHJvQ
YKjl3qtvf6KJHMJJL0YRkulL65ND66bHbKHfGM7U9GlJThpEM0yeOoQTpfMV7ZiLYLXu6Bdr8x2C
kiV53uQ0BwNHS5b1IcFt9QW5xpeAUw5anZSixVoqT1470C4Soxs+w6jvQIpuxu+OoJ8rmnP86UT1
QGY9rqcmIssXXs9SRXLb2fUlZwM/cayLaFVts0r5BpmL4GeNO7ElG3753wmAQKgkHpkTT3XDXAM/
LBk91BVrtULuTDRmFUQB+3XooC9BAhn+VEYqTxNBzORrcbrP4accX45f51WXdvncooE8h7zmepu9
/YmgbmZdLqdYVbYZ13HGHiVGj67q3BJ5fRFkNgEk1ZCQqp4+SZchlPo7smnFcCs3+HV0XSv+a2G9
JWY0fljTJsG/RTQSoxHVw/Fuoxb73Vy2Jnss/r18jFEqK23PrBmBGlUwtGSZ9sWhZcLxylDh3keA
gLmFVcsLCQPtryrEpC53e4CJYB2AmY23P8UCUml0j7RkEibBM5hS9CS9CuE+ilW3H1oOjhm7ADRk
4NcXjuGm+U10BICT6SopAJvxtp4I6i2zcC4XcBcV6J/IWguHVAqtJuYR1TirXwZ9Zfm782HM7408
FPdlQ4q0SX2gYymPpZb7xw2XzvXoTNGw5McSAKvMj7wUNEILCB1WyefPSERp0GxnmCTMbj4oJbpC
izBzD6byOrStjpYIlwjaF1G4kdU1uU0efDQGPTcnxS4InwzTwKiAitszue4ToH5Utjp7kf0mb7R8
ugJ6b/lcVGsKYM/Iczkiny3LAytXZqNUcj/dIWplSoy5wOD3d5xfHg2byQZbFGageMF5uveeZW94
7R10x7LClXFgt9h5V78eWLTTuQ2ZAPsaKcbJHDEuzll41tRJURhBpxj6G1gZNV9mgiTLoVUXoFkS
sFrH8I/VxKuTQbrXcCZaZpzSK9Xy6pf984MZDu9dXBvH/EG0OepMUdrVxa7QmO7jem3ldLuKYuLf
0xvglm7oskwjXNVL4krbwQkmQEhjlEevsElFFoSvKnIFrpt2jVIZhHG78MwwQG8TkdIkh3TIYTUM
1tFbZlD88YiU1eJmw5/9mEDCTp+g/MbtsaP9XQyisx4lmAEoPQgnGYYxmzLriQf/MKa4E95aSj+l
Co4zSY3iu8i7U9e9j5rSYyNVWWiJcM5QGAEIzCeyRyhAKd6RibkCyisWuq9co74H4/ef4j8+f77z
skD1JB9muIGVE5a9xV+xr+QtjEtLvoDpfp/I2wPUCEW6YaCw5fukR9PJNT1FrDk7aVr+A28kB6AK
np496ATYVnx+RMcWhwmKQn7KrU2XVeKqRrAqFOXliDVXBdC0mnm/xGUJ+uQPwkO4dC1I8oXO1Z8P
Y6F22/hafSXmNAE40qoNJXNZj+Fxa6ooLfGiprTMqExqgFMhrIcqGazAHv9ObxyJ6rNnoA35vFD2
x0cf3HHYqtAYNl6fLlOqQl9zm9ozgQAyAtvx4xjJ7c/CIPWCkE/q/5beN4/Eur3cY5YtA29Y0ujP
ijs2RMplfFlNpHSMxqDFLQOFl2vW7uwHWYPa7DE4xpp2KCZGIFXKLX6ZCwvvHT82Q3zXzFRWP1k+
yxmt0dsj8ncECTXqPrDM+iGG9pb4/7+2r+5LZdsgLkjoaq+BRmBdKUurQPPUY/5fUAAqMeEUe2HR
wbMGJiz3zwIQdkOAGlUHOvdKp2Cod9sCeBl4CgjmXCQM/h9XLgIIiY4qhlzBAjRZG9LTAEwafyy4
7TjmuZJG7a/Vn3Ug4c53Qttm2adnaqiIJQ0IGrT3f9pNVm3adbQSfiTHqc4C11aSOnooDp6LSuQu
WQPOKKt9jDCmWTnfQ5g5k8W2u4COyFwegdbjQadSYJw3ALwQPjvEUgdWQP9uwAXbI95+/4cOGy+q
JxWr1xRGdSVd8ZUcNJTGDuaFiUUi3PDTms3SP6w0kPobYdU9pExxZidEk8F9BDzH3QAsVAEB2CRJ
PKKuE5FfaFojzE9T4OctGpcZozOlZH/HHGZsZSUXdtQOSzzQRS23lNF/tOxgW3x747MipoG3LECB
jI/JgLiVZEd69NXh9GpIVPYyWFX+RUfUrP04i0/oKfs+IToBMBvvYPBJR2L0xUqfjOboqVa0860S
6TpWou+4YHk4UqKhkJz/5kxraY/+OCs2NXf5cCkUy+Q+CK4WMebzSVNIrOpLMBww+HAo7FPK9fff
ptGLBBTDafaD9FJMHgss088IXBheWmGCNDCY26dqEIn6O8TXmBAhqcFel/hiCxxLC1OEvB9HwllM
bnZHFS1xH1hoMGlE6WgyDuMFBeD04hOQKnjCYLuVVwSb0BZru9aWWWMusO2ZDgBKFM93ZPVrR/DO
07hD/m2Flyu1/fhypudYK8DTCobXpPCvAhsGZmoghn3WntT2NVLtUcaT8AFGq4K5854zgVAePmKa
tyQ6YpQlgO7Wp9E2LKKo5g3n7BxLzjILBFOsGb9HEWG/EpltkKEnPtG5zV7Pr61s6ttJ3cgLVsN7
pmrifeDGvXf3PEvJoG7vpSxPAkwkIIEce9jtL5P6fWmOljG1BwKgd61rmD5axIcn5o/lPfTceC/3
SAtXeOobLwif4FOKm52eIdfKllun73oEWDTP5Bt+kRv5phQ4W6nd5usmUnehcP9NLhVGPUCA886L
pRFFcdaUEniBLOZmAQbg5cDl1Y6cXdXKd8BVqdcIBrq8vTAHhMoPSFy3zUqBG46y/7anzlNaqGJJ
kRCigEI5FpFzSSgj0dvgmykWOouV5yYTO18t6437J5VccQQg12W+o+Of6LO3HHE7iVdWziDY3TiN
yd4dC2mw6SuPlYOJUNomE+vDdw3GetHVOzDK9Yn+V/4S0cB48Ii9P7Bmg8CGHGo/wmGjTtudJRth
HRFtUgfVzPDYD6rWiMjaQT7TTSM/PC1H5JxTF7E/EG1vRkTqHCYGYXata/NnDLwqOzrmGVp6Phcx
rUEpCfd83abtlu7zRWVjZU67HZD3CWHeplnEkKh4jIpPdWMbEeqh+a1RR2YfrUyTpNyvAfJzQpOh
OcDQ4Dcb3ziWFizNALQvRV2cq3OoPu/24erZdJApN8WO1HKpn/MsSJwOXDwTNv+DSnFCiN0rkFj5
f2oqWB1k0EJ7ffQ1a6uvxj2KOkk2woxo2LpDUyWNEol8JFdKLBBxzejj+hloTNrgDPeQniv88abg
VnP6z3HRf400Ymemus5t4mtZ847ynFpJRK6dusCzMRMgcNpTZmR8l/kIXvwS6yyYFJMQr3bjV7Qe
3GRZxAVX0h5QtbVRKGbPvXWjmsBEGUt5bSzGX5pnvOdzMJ1vfeG87WNIPQbZqOeY07WcVDPRY36j
GvR0El/7aSYNpIXQ3B8GQ4azW+qFCYaJaWDz8uA2PIeWjJSp1YAVtbiakUzc7PRQUv9QQzrcMJvj
7o4h6ue1eYq0H8DbHz3VYG67b0iHos6itlFvuzg3W7Av3faQrufWEK6vVqmujETVXY2InjVg9OhL
PJZ1Ro47SMKe6llWxo2qg14UOFB0RMvlF+kO0gLX/DmDapb1U1LB1WENzhQIoz9AhjTTtBKKLYec
Zc2vY1Iw0F0NLlmcu24a0L5WhZYorNTsVtIJkwAi+LTjW13IAyxbYov1QuCVjan7HSfUm/8SCsds
fbQzZmH3P231Qm1gl0Z2PI2FhgYEOwfHQXSD/dgozeMMRqPRJrEnNkFfcnxkSJXCaF8zOrc2t6x2
1d0xk6rOMCyhXXDfwh30KO4Aib41uY1WxTM5Xpx8T6QwiKV9IBsrQo8vNntr7eAqf1Qugh3jukgo
SqSTjJD2y5+BAIGRhMIScKnO/Bszh/t3XJu7QY8hgSaLumICWPf1vCCC+8cOJitvaujwLY78JgbK
1AZmBHxs1PuF3ac7hRWePSYoMEb6ABSH5sHF7QAAZLcS5o8gMiG/ItcmQwEjGu8ZrX6eJ/fPSGSI
MrgYPnoCpvkihFu/6s/SyA8Dcq4MIPUgd4ILt93mEAdOpz2d3Y0UquVUHafQ3o3Ke4V0m/1ogoVw
W+k3OVbUrPjIDTGPlx/FoWldjq52gSwk3PFwRZxrIqg1VQCm4IX1J8e0bp/WeQbAFoJ9ggU86mH4
tWui7jluhRA0yVx88eg6hmuKydyGuTilCu4C7ViT5I4T/vPdEgt7BH9o0CusU8BPbmoDV4Vdc0TF
g3LLEf3VA6G1HtqvszQ+rtqtqjlRRjdhTzSf3Fz72OJZHQzq6VBFsaD78bLZ2MOGHPg2mUaHJNtA
WZod9cqYR+tu/yUuv7jgidBW7j2dMkVyhAFm6gIu2zQoyfW+wOEIHkfx209DcrTRDvDNlt+9nNhJ
Kb9P6E8tVfPEFHEABWId5+TNu9Ihf9MMFp1UlptffiVHYp8GXeoSc5Iu157Q3lXPQ42mtea5xjTo
3nf9p6gjHMnU6+/0I8yyxmbQAlk9Dh+SyY81Z10fon86GOVZstLL19IAMU1mVQxIqbY96bePrCaA
fBpDq3EUTrXX0uP0Sx8W1NRptcJXZGw/x5XLi1THW476SHJQtlk6oxFNt/Jm2ZRoXycpDd4RS1oI
lcHU5qXmGJo+G8YegSdoA2hk/wiX/18M2IbL2UJ/Eg5a7w+Yv/+V7JCIBv7ot0q+2ZbtUhO/ikTT
mw+DY53vJ+NXaIB7+C4f94zhhn/43OGeVj8zgQPPFy8HHEDz/w1qJNosh9XPW+wmpVzkE/gu2uWf
iUpem3GFLixlNWAw4XFXGXNBtzSwJqHWK88uNBJzSsWWdxvMjssjTCSb5qviKZ7Wszn3RryaY6No
a9IhV21EdLmOKT8q42e0vf+jedyL7u8FWV0s22z2HON/aLNz27n7J0Tzfe0NkRoGXksp4Oec9LNj
J5PpustAPolnf5ahs9lNIpJLBZUUx52ciVqMMOSQsGqlLdYTH/tt8zIuOHcv3F2Ft9CDXUR8GGYE
yyoEANskEQyOtGIeYsiQR5KC5IOEt8VuFNemZTwkkTLu/K+vv1U4ZmY1Ca+yOGFUOhaSaS1Kp0md
yLUn1xEFko57FlD/xTFovhNzh6Wa5h+axmP5tCJZDVaBOnL1DbRkqCJS5KOSWPLK7AChkpZmyeBa
ejXtaSz7DOA2S8A1//LTQDO56znOVN6kJZ1RUr0MqsR17u8yUGrz10/RhU+OelYK57GNbYbp2zZu
gjkPOHT0bVPgWla+fz0KRYDXCwd7PD0SPofUhpw+aWsnCpe6LyI/t+5s86ci078OFV/jvGxtD0XF
yQdp/qRsjeLR83ZdgqmKsJ/Dt29HBvhK8mql08qxOB/FG5IBsN2qdRv44aZBJDoOM9azkiEmZdUO
p1q5i3VvZn8ET7gIfU+HlhmBxFA17KlvIrfYErTGsZGhs6kqdSe2FRQyi1AH4L/ArcsiLxlgKTZ1
y9jGZFcnzV35dQocZrR7pIFWwwJR9gYkXolkzY91jrBcga4C/OP59jjHRiH42u8X4D4w86E5+r61
5Gx17XChwl7Oj9IdUbqxFqitROdHntyuhT7tEiKdvvmY8Z8vci2GOeO1vIc3TTsQYStVFJybbZlo
91COg3Ej/FlbkstC08ZLYJaC0/Wrst21XDf/5+0qaj4QEl35zOtBCdcZ0kTXkRtEDd1jN9sA2ueV
UkUVjbLCoKmQ8Pucihi69/cLodTgEl0ErEhnH8Qy8uVbnDlg/uiiPBL+bIxks6Z9T/QUT873/7Lh
SraH4Coad+1xcbiSbeC67ZfXZe2888BgYRPYnGgPmAwbjq68YTtN3CuqJosMEnA8NpuXdVO9F+zo
5yry3//es/MG+togDboUwPLWoY1QMr3bjXD/L2hKnpnXhO6uhAt28p0BDCFfL0HLc3eo3JXsjiWw
gUiFiDebDvCerLdC1TCAgse/0g6EZhFU/l79QtMni6GrKzDxVL6n7vIlk3LjEa2dvGSwzU8m4+0X
NqXMAyJB7aeDrNnrSoS+fRfE4yLrDdcOoZAltEx0UvLDdXezKsU/Jx5XD3J9sY+AbVC0uy2RRzwE
4XqNI9ycDf+JOHuLeMk+79ipFGoSfeL3EOc0Ed5+lxp4YvlvTnxyGQYQPu7Ak/7Kh+7z/JQqxalN
Et9fcj90Y5SC+bfdARSh6SCWgSysnV9jltjFreHgsUv5b/s2aUDXCLQzLzj858D6e9GOoLHjmNFm
swG17bC/CpwClqWPrG+lgIOeRjhG3ovYL8/KFkLvERKy+jbsDdw4LL1kbYlhzjeMDquQwBr29Oyv
B0iuEoca7U+WYqIXvXxOraj8O56scMseQTkIu7RVlV/O4045h0BUdDN3ALMYAo//XSmIUxC7VIic
sxHAUCEAxysMtcli34+dzDXDB6UcyD4Q7nEArldGW4z4kxvz+Lo5DpILAzeE1N+IpnW3eIqz5YU6
C6R1PmQW/J6i/CSq4YCKZEjifntSMJyZBSfbA0Dxu3oFsQwYPHA6glWsOb5/3A/SBnSrSrqH5jVs
NNqC/a9em7EHamo0X8Bqhzo99xn0Ug1QSMQW7DF+X3xOauXvd/TO7MelEfygU9gAlsyQ/N+3mcoT
fTZuu4oNRIkHmGRFwZbUUEQqY4kmrirfK6+Nb7aSWZescUErnrEU+rpWdPFT2F3+sucXNKSzWklb
EoQ0yoj7VuoI0oQnxRHhBgbZFsEAt7xOxO0tMc66EHiGUp9Yf43SEnFohe3cAjTPWaR1nhTMbUcX
51fJ6w6Zn8aeuOpEyC0MCOEk1Of4LxPJ4UBPrsBJVXxRgKGVVcE0daKJSMZwxhLx/mrFvu5Fifwv
7akex9qwe85xoGiLCtmukftNLZW4juT1Rppo3uTQoGNxaOq9IQqXNqnCNqjDXl9eHP4fLu8d1xtA
xXSGBhxCX2IN5zkVO+8Q1kC7pzWaljnw+GJOi89IdNafFLafvbWNo64eRNxudpnz+aAOQkZjMM2W
nrLs8UoXv4WuQQHGvMK90DX0OLlGLHeylpxqQL0gsmkqxOnkLvXrRgrgnXg5B1ZxjOm6bk/NPA1J
Mjj9dM4VYdAvi+sQkKJzBMwzGPhsmEli1M68ftXqh1RwPguv8Ul7KvBP/922EYahsPT3n938v0gT
5QHnIBR706JRPpPC0ThIotVOMXz76myHOh/RgHGwVQCPM9jCHDKJTSzKUs1MLg7hWhRvXLWbFHgS
JROA4DnD06Qa8L93f/euFaMhBh8sBS0l1KpUdLca3CaWszHdRuwlfRw/ooiSi3rOGz+bXoTzNbQc
bCS0TIUnwjIfRirfWtiiGElJOuxg11Wow1lYr4sSnpItuxjO5E2HrjTtFe9XULEXbU1qZEjem7We
kjp0umAxbQIZHGeLKHxVzjtS1gYxf+0BMv0Mb63RNyxzxVW+6fvwNvzOrr4o1Z2PcK+WYKDL7Pse
vmg7H1FeN7tiA1XWbrs3KOIddV+DSoptoqFn5zo2+BEOdAdu+ShP/TFCZHe7oog0rewt52EXizOa
tGV2yiQ3O3poKOIBBQjjFFZ7r7bURmMAzmm7Cv+Xvku1fhgLWEjBC4r2imoujLAFjBRF6ltAP5ls
a53pLBjnGnfs7vP295Wn9RTh929hobTW1zKJwBBQ1tEEnMRWegiAlcTU6C8Ira2zByDgkaMj5Ycw
yJPIvhy2/zAl395d8lrb6aG57eeigbhK03nPq2A5qp3i3MbkHTFArsiM0RCw00CKUyyys9Hn7uay
BZOUTUB5oRa5mS9s6tZSIUO/7BsKcpEStoQst6RMPrZRF4ZbJxlBRIodrholLSAhkGsnGEqeLYl5
NOxnc1INkTeN4mq4x7GbVKgmvjDVApvD5YjuEWvkV4aBuiBjpdumqhL043JpmMX918joIMaz0gIp
fL8bap33a91JKHYJmJOBV6nNOcDW5Lk/V2CcNggbxLu9swgQGWz/DMiMgu0p2fLT0ss3rJ1fcLdW
w2qwiiRjOoYVBZCUAKCJAdV9ntW/zBRYOw1JP6pFDcty9EfhSqAt/MLoTOcTOYsc7lfg/iwiHODi
03pgAvavLJb+1aQGDYy/Bt9BsaSPRTBg8OrDvQHuRH5SxLdB8WcHMxDgoe4guqw3ma3oQ81fC47l
fbAPPL1X/H8r71RD+iygl9cxZs5jByNJyj534bzlqOCMpX7cvALqIjdklwud3Nk1fRIZ+C+/WDwm
35Cavy4/1Cm8XSlYsTxm75xpbxcW7p4ycvkl5F4kA1r5RhxVy/gIHBTFhVxInonpb4zAIc77U8yT
krr1uE3Y8AgyVGHSgEvt/Cn5fmlb/PJ3SxJMQ5ZsrxGUoSp7+VIRET4Iom/aQh2SsyR4hmkOVEX2
z3A56LutnUbMh36qR2W7Br90wvy6xraVb66IBkNpt/z6VWS8lTUVHc8hn2aUYICxRh10SRoYaDAV
ZMXyZ3LPz7U6SvHE2tWiKNhwekMtCNHBsIewVkVNryNbjRXS5uvZ7HtZ+YdM6qljPrkRbot8di2b
O2oOzPqY+cj1et2stWX8iX+Y8z8dlASM7idS41yKNvtGsooq+mW5ZTocLE5vxiqWUpo4t+gFGb/r
hAEWPB+gy3nzE1yDDtOwltr3rM2mLxAXFy7Hl1B+zxXQ4hJWD1xptMaVJ4pMcUc0xtCztlsiyDbs
6fpQHJShtWahn0UtxCgffpiZ1iKcUuOc+oDZEn3yofnpup5u8zMMSNscrQrPnd+A/NCyVdaB/h9G
MIZzoiUaIwXofqz3/zkK7Mx1ra4wV7MrV0dKLczqkpGfb4CpIxBJ1npZ4/wRa9KK3Vva5icPLXip
iiB5bWzchbbM/wHSeftmhluCOOCfxX8YbMJknTauN2ufkCrxl87quJIkZ70iegQkglOjoUik4/gZ
t+6bvl6IRg/GZ6ETlenwgqHbg+Z+zKPcD46SGc4znbscrB5FSsg5XvJ0HqlkPgui40Lp4H5M+oU7
R3oJSYViztmPHpP0wKkG2v5e2ZMi9VG948pt9ckBZad3KwwdlaTyGPkWNArtYPzDgvCBZFf6d5x7
/JDRGsrm6zHnUOa+cEKLuyjLK6c5u2lp0rZnuN67SWYgwWuUgOPaGTjjepvAqRTGdTqD6WIxE8eI
GP8zIzDC8EWcU88SdpGrKjeF5HLjLD34ZGnsyyBVjnJGhMGFD+DbEAgsq0FmGCufHLah0amLcsmJ
OW2lZ2Trr2WxBDpbX3/JzNzhExxGdAinci3ClehGh6YD4h0zC2pDOQTwn34ykac6qhPoOlflJaxM
WF8twhE1Ee66BoLpU6mezNfhQDok/SsDB+3Rh4njwyEQmXuR9B1DWDXE7QxwTW7Uml4833yywsLq
eZmUKIivNhqDG3bGGuhp7fPIXENPHTF9ZgcOKEmDtmeqf39iw6AyS0fLG8vVBCP//MG2bm71Q4oE
sEGY+5H1sKyfAZ+9JJssFCy8uQqhZgXF3EV5gtk68K8XJiz07Xd888xsf2V2SnjTtLaCPpb42TTo
fbTbsu7Vt7wVgd8ZRk26kq2zkeJJj6nG+B91O5UTXDaADIAdi0asyRvmjnRZuFRkl06zTfDail5y
EhNBKkShXZg7ev8wpZQnBkFLaN0ejzFcSimytUHlFntJ21neirQ07AgVqSKXbTuf1r/h+YiTGOKP
Dxu6MLH2Ckt9yFwTPVrtps5rWFImWQ/phqPd/BcemyaxK+2MjbsREWzkTuVSt9Iwz2RDIyMKyVxd
HzCFi7qvDeZIEIZTjVJUWqWRlvqcnl6KF8/7bsJGL/0SR20MKLHe8ZP05AE4xLdcMSj19Xj2gnEA
eNJzC/Y47KmsBz5CXQwaR/YSvGW+3nl9SmQ4JzBEY0NzYVug/lBfkm0UNKou/2kIaZPj1dk+wuQq
b6RNeTT1hDVu4Wve5T+TQwdJGaDXOZVkOXCZRFHajhCbd9SzH4DKhvtm0DZhZYpyiM4igGNjgjeh
hJ0MvsbMbyvaxa/chseaal1qHdaz9GJRUw75q4CNSyHi23UdcrLMisiIM3J3dMuzWwT0d1h0DUY2
UDEPBYaJtP7Cos787wCMuV1Qk0+sijv5TalkE4R2Hu7/TceYOt0dLcmJSyHabg8taXvA3xxa+Ket
N3tJkkYugqNpt3B8lOcAYzqOUpTNqgcepZjz4m9sry1/dSiUzCq4Jjxw24ZxhkyUYMa5tWxPVaiR
668bWpxsL8XGM09q7mBy+HY7xhsx8uNzHzFl9WqiJlLxjf/VwlRev+AlVMwIOWpPLKlCj/1ZHtOF
OykcOiNKvfU+7oryovcFFJHlsxOgs+OcfGg/OUz10VyUPGsHhU5Z2XoB8IzuASui46DPee9zA6z+
9T49Vo03ETc6Nsr/M8GKB6L7CXNNX/erpURQRXOn1KGuCJkH3VrF1apicE37YIm8MPw8pgGnVUFk
Q5fTtdCkWGvEFrzScV/o7/kl7Y65ll2znvV1KUKIIW+YyUarP4bx3/CdXH7ighCfBIzVjRXollch
JfdjDHpF63VNC+rJpI6avpoWM4+ZMyd0jfHSt8GIVaxtaVNtxSESw8tEP721Hb7U/pxc4kqUnxKb
qcRGRZJXXnJtBfvZnScIE1aRSZ2LhpBBbHKrVF++Gm5bWITpJl6mEwo1Ky56U15voNVP0U6SY3eN
4cUvvAAPuatOP/nKgoqZQkWMtQ+UYjGOmRsD4Nja3Li8CKLERdbbSuKI14d/aAPWhYFyf61wX3NZ
QYi8UtwyvUtza5Uaq5d3yraw/ULkdlEzT6aHMjELJaugST+4E3ozWi4c/zv7od/vNpgho870koxr
QBttj9mGY23LLDPCg83f3RxN5k+7XuLye3q3+hnSGpdxsOidszclUvEEWYQiZ8DKKjtJrIkjh469
R3zElkXemnFPVL72b20a7cTrtWzTE6ZYiP7+tmrkhHQKH/T/8MXWGZwkyQHClxykCGWlfdl3xdPM
Tb/Z7NmkrYiPmvg3NaNhLB/hgaBEkv+suAeYrcV0ze6N/0jogFs9pMjHLnZQ0zlBBfzIlIX9G4mU
mlTXfYwUO+HJGE+lboiWsLdxTgeO94WfrRELr+FZeMRO/kyZtulipY4+mY/uD4l2gSMJqHpl9J64
o7q9QW+6sd0C7/R7U1v31VdV9cA5/dCVZj8LCdLlnyQM2Z7ld1lTlspi8MMJN0whbOc7jXtX2vly
pU82iLAQv7enepYYTKKn6uukbhvJ06s6T6ENVhCGPsm18R4cVNcH3jPFTbqB6PSX6aAc3di8SWvr
2kg+7+rwM5ewOsvtZ+joMdRQyCIB0iQGPyFPF4cPhuLeAzaMKj6PfNqWKxUK7rDoI04PfYLZCg/R
sM4hBY5k4diHIxoB0NFwDmNum1x7TTvD1dSliu3HKzRN3IYfcmbkFtGE+gsGXxLIiccEqhLheQVh
STgBpWxgvzwd1LL6nRedIV+KQvoqjaK3Avj64/ConBKKF7EpstLvcVK9RiGGJAG3fZxYuUTchu1z
pXVyAUnz1KJq0pjY1FzgNRzI9xPAdbnnIpl5QVzASpO1cRddxKNGOGoI4ZFoUQpFsBroWNn1faCa
Wq6s+EcizVuYKLMGRaalXbGqSzfMbo5Ii8+2PPYu70xb3siVO0WT6ydM1OIEzAnK/5Y9IvT7uBPX
eUVtpYGNATpOMX98DqCtgwdtR6vPjCs5XIvucKZow9jre8k5oXYbZA4qHtGR8Gx4pHLCcy0oQ8Dp
7fnLGL9tSCnNLz40DFuD2krdGBlDOQXXHUbovGRQs/svbYsf4kvJcwyBMtMbC225RW9ElmuSwxl6
etKer2TZ20/t2FfvLRWdYL5SdEENBV1PRlNxQ4DLdOCd+VS5PepcgrLC1OhZByGIeyRFmUFHsBFL
yWSYhZorq0L+OpvK8uvDvLIv7AeC+qmRQxfmaYNw24whOuZEFldhvYqugUjnVqLCV3O02AWe/9Pj
4PUJn4E8BWT/pP121F1zpNe6NG+CkmiJXr3kQBUyFdCnBwfI+l8YEml5surKoaO51EcdvPQjPITA
N72mVUMlYfl+XvbzPORuAAlfH12Sp4yvmWiZ8p1BEeKceCzA59aJ1UvYWXNRzgnor4U7XtS5hmGq
GO22ilqUmJC+vxNW6Q6T8iwceaojP+eivWHpFb+MsOO4CJVMVJ1mtwGqKOx9gbAf26JVxYWbmyme
6ILnAmc+1K8X95g2CrpaCI2F5paflBEUy+eBaOMrFiE/XB/uc4oTVQpLe4bQFlfM+P2XlqJQjvN9
JjV0RUpKU8PdSqMR/1L+VZlZAj3gckzE+gsr+3SNvyVNTxWzRX+LHaFuoxiZeMHfZciBWhFogC4L
sr9ef2QHh3riT5USj4V3HTGLBNArQqzHPMrKwyRzYh+Oz9Yvs7H2EgyedfkKJImakETsxx1O9yzc
iVdteMGnwVMsYfkJc8XzFUfU2biS2tVff5wF1uX7w7Mdk9SgvrB6zbL+1NSGfQhgnhcdAIGWh9vp
y2+lWJq6dFXsCVBvgEL3z78Y1jCt8AMjkiT9CYWBcvCjltk94mfM8+GRKanF6n1iFJu4D0a3yyhE
ejJXRk3nYxWuaX9rGC9auuX0nkQWT8UMBcMPsH4JHG0u9+dIVowdoVaJpX2x0QXV/fH/n2ef2bIv
LwyfjMemLdxFvhTeqc3lE1aFOdGQpemOjzO7FQ1ON36gyzoQf3CjQiHBs0bmXOplUEkoqPTEAL60
gHhUOjZ0iktBz9c3Xh/TyI5m5SGhDNGdGZM036BGQHzM+f3/db4D0xSOV+LohYd74Io46hxQKps6
EioY7q0WLO+qoI2fxqQ3xsXZSvJE2gk7o3/quPh/DceCG8LxUAYq7lJBjFEmC2ydJXEHm1OcMsnX
bAE3OusTE1mmhQK1KGrfK+iiL5/HF63b1gUGQ6W+AP87EIrN9T96NuSkNX5CSzN1m8FGTXQDFpu0
qoBIYiq5BRPK/TdgmS/D3r/V0WjYPs2l7b5KEgV9gtAN5KEO56B+sRymTGhllOF2XFdR76zIxlr9
n0xWTXkKOCmvZb1KTTs4ICdCKBdOO3iQCIXopCrqqtikgSxl+asQcEMGndCjgYECkgf374yUtDPU
/vU8nWqC6Oph2ASt1iBg9VjkI3A24h8ApQo2uU1S/ruRR5EYBymDyAJpo7DKCIZi3axBWxtenBoz
cqlUtfO1mSuvlEGHCGF71ToUnN85QX+n+j8UthQdaaWeX5Xaor1uRdZwH/DRmkDqo17kvR/mHp/4
tgzHDYq61h+CRgu4hmBtvy3srhSKqmtyiLjIr33mw2rbq+CbCdNFEzF1NsofHz8UfthLeIwDO7Bp
JWyZXWqco5uhSnVnUdCP6lgvnzwwxDaNHTkV605M0qQKDgrbVvAZjpSUvR6jW7lU+dITL5Kvj0Y9
dGSgL+m1Mz7ygGy65yLjnA/g5FlfZ9xKeiWbGbkrUwemanjJ3vmJylfng1pxdNgTAXqaWiLX8Hb7
GtQRQTh5Eo4faezm8ovWjdY0PeTalwC6kcWUEGfsquD5ZJoCcjEzdin2HN36vnMc0VLZV7Cv2AvE
elBGLLpys2D8yiE1FYAvIWuCRbLl/uYVd4LijkLD2i7/GDiYbClIjqMKjCX3b42a8eKqxUSIRAvQ
lG08OiPYWR6tyb+GEVeHb7YRpUuvxOwa9On86BRnhOBCFWULN2Zjm3cxcAYUpX7Ekuv7VbF1vvpo
mxg3aSvKRp/cLCow1MLBINMoy5X6y7DTaUuCz+SjX3Mygka2CwLfXaqcJzbL/NKDq/XDqw4hMPK3
0Ro7nYjDZBQbDl4MFnNTsIWlcZ+cRUtMdu5UDXYHcJivhZYE1jMh+Un5n0lw0H4XlOMSa2v+1/nw
J82WtNaxRlKBqhDrCMfj3aygwv5PJGTowQLycvNYTxPj6XehXPLpYSNuKa5wcdYFH0z6FxVYL+3j
kV1jpdcnB9mGINzbyEZlZZwi1z18/eSuEC9eBbjxDPLqb0kRn3LSIW9f7KvQ/61tihBvd6pwwdNW
KmJ/fJH5Kscd4x+IqZZY8FF0bWx5VTlz6zfWwZrCY1k7/nXeDsTbUENGR38WyMqrppzUP1oXYXKc
ziMOqRseMDqoHM1a3jk+6SApQcF4Y1eQKrNV8XnO348BYoTrcfchQS7A+n+kvHqZsBvNFpRyVuAC
Bi4E2O2biH5sJpDF+h8wQTX1cz6fsRq1VTPUJIi6RDSMzP3k7FBrTlc76c2VRMctPootyova1Aki
rEzJEHGzshC94Ee6n4CsdQztdhw9ZgW+bY0SUtTuH670HcQ4d38d+WK9rDRvmdwG3nbdB6BLCXDz
sbN66etz0Tht4TAEvtJmohx9Gn6a1pEF8x3YsCGkdIkshPZkxEiX+okpsSAxfD7QeQvhV9i5yLoe
m2T5RagrBmqszbyDT1uJEvP2Xx1XCKM7YMIdAGWecdvhPyI/uuIMEg1EaVAo6wHDdoip8H7Zox/R
K21LegMYH7wkxCtCi5gusRqXUt7iEuVg3gUPbZBr7ms2akX3MlgWD2B6XzyRm14SKALu4ur4a1i8
UhpKsPuAnohdxBER0MCItv0sApKXTI5ycaX4L94/q/FjbwC7W0X8Jg1GQoGrLIVjTGR+RKedCvAs
Yzu7ly88YVWQFomRyah82LsqltXfipLe/QtfEWkIgQSU3F+0c3GTooFRyBYD5rfFNUkLwX1EH6y0
obq4a94iG28KJe6C3UdOp6meGjEYC3xUDM8n7Ban5A/lhaEaSOBXMgYU3QVHZq66lrY/a0yz3TG0
R83MX13mZqSkNaBhpc/2ECRXFVu63cku+Vkc5p+x1UKzdUNdEeGz4Shfe9bjGZ8Yxtr/sRliTuLw
vsctKAtKoNvavJmL96Q0LkweygG9pEMtbZLBhMILiFQqYHK62+JGjgreJtxyHJjZoFE5h/yspVSd
YF5G3mwlPIJOzz307GigpxrOH4E0oA6K8wCnABlc5W0M8LS4KmAols42/A8WWI8E+ITHvddVXGD6
hiHsUYMlGpcOFhLJAWZlys5XLIw4cQcqXMecPZPxqzqS1dqoyQda2atK+7ss78qxjX1k7Q9P2nJJ
Y1uo78lNkzmJn9BenHnmLpmKrMvv5BRYtkl55jePkGUcOWZDjMwqmr/CLJtGrEbrQbVcFyQAzD7d
ZZCvxtVNyXwfHLM8o4Bw8GXTBsvFrnS+4t3dZKKskqTZGIPnQobwzYv78rhEBhLChFg9VBCD+Y/U
8tSLqon2WX3WtPo+G0ZgrV0xGDfekTy3m2Odq7Gknmfp76X2JxCqxR/UhONTw/2xmhB7YbIgE9/j
TtUilpAAZOEfGcq+Ca5+UZqP9IH/+EJrd16sw4S601J11Y7wqOoNCGhzo4pDNl6suRTDxHQB7naI
GuhTxWTS1UrdA5aGQS8WYi6aaP3VPl9JOuYdzfHEu/pzuXRmDWaWssaGJBQOVKOQJ8Ls/2foRPwG
KlgvvibTRMdQUj2Yhm3RwCez79TBY4JLbfUVbfiF2YB5s0bSUv+fxaCu4C8diP0WA36mdwCR99re
+80dOICx186akvCpn1Yu2Asu2LLscxfFzJv9hbNJ+X8lfDZMjEkdDegXPWxMIm3V8kqWPmOpBKVk
jaWHJIY5YQWIU8f3j8IqOp8rD6Pmrd8yZiI7VBCUZbpQ1WLI+mEWlRUQMlDRjkb+D6jfBBjxN3uh
1331pvszLSXya3RtyTzaXgST+UlkRjXinAXHV2UKpbRgp+oDRsIWfe+N+4qgm98I1X0Z8a9bMaeC
hvNgcUic/zBfZB+oqdhqdG+fom60LiWve2NTckkerMbGBregZ81IxKbBONJqWMD5NzvXO5r/5nm5
iEFAmDAq+eWdyd2oFNV3mmuHhKbocflbg2moMM2jRWxDwFlIthbyXIxf0KitlX7LO/mZrKI4BQWD
bFXlctSu6HlfaUI2bkRDnbSI6ibQXoOosV5jvj8TvUd0xe0sG/Ie1YQxGLBVHzPND2JWsGcwh2wd
yzsXk/MjkPu8TIp4lAZHvJmTAgjyC1+oxxhPGon+pZpB2yZAOIdYVW5upA4dyT2mFK3hMHquImBP
mLrV/Qf56TJzU9j20GZ3A08pwcR71Tf7k6jbGhdrsd98HIiy4GohXrSpDvoz6o8W9PS+usR9Rghb
CDFzB43jjbY7xpHg9wSn/H3JyA3JJkm8QerZ9WfF+lILyJW5YTp16jqzbFGjPoIqtGGe8oXWcLwS
eew+2yAJz8s8bwY/2TvEqAiKREl2rzj6Op35ltI4//IjSUCaGWhr5m6ScewTX6+1VzDIN5vFLx89
8tR+6Ox1aeMp1uXinz+qfvo2yH6kVReQ7pRl5TxHoz2yZNZj7UM+YiB6Le4OrwUAzOKt+ucST6aq
NyQXYfGJ0gtC6AYuG52h19kCF4e0G55xVVWJfagoL3tt7tS2Xsyt7HdwXlY2xRS1BbxQvnDYSL8q
239qRFBYekXMJpv+gurx1NwVzYsd2KJywF+FMtKwHQh1yTAgf2k7jDFEgkoGnzrE1R890KU4VDq+
iqB9trxGmou5yAM5GpFow+a64rL4pv93sQo+++3Ke8V3vNVyl4yydMqzMeWeOiyQFVnUrPKsr/6m
KpuPSdScW5NDNMgRW0J4WtNBwUzxEqlu2D0PcBhh28LFe7npIUlXXUna1Tqq6YFADSaorKDCkmAl
8G6sCeftYqNGdZu9LCDOos83ViNKcEoy0rmPG9ZWJPnb/O1JbzyUgFVek7fxDLI4taiZsxW0NxGv
+JPAlws5Z/7KTrTTfQgcv77xGMUh+ihmPAS54I1ClaUuz/2t8PXyzsf9Jeb9IAKqsTnQwe0ksTZq
zpPB6oIlwGlICVXHerYx/bbQrYc//0NdWCeXXTm7GgB8wQR0qCeYv8nqTydJUrjXc8vX7pW0btb6
TOZVW8PtLgFKbMh+gKWSaxTBalRm/z8t1wN1X5IfhxhymbKgFiGxd2yw4Xe7gkxf8pogYg38lcSK
Cq5rlyw4HXjwOC4u1yclTaRjJ1n90Mk1LeJffqLMYlN7ptrhLDpdJbu5Zl4rXibB74rhqvq+g/2G
fzmEueq+X5WPRPtt47l5dx47PVhqsfSIqU+WGezo9x825EAWtuqYSVK4n1AzBHQ/tHYOhm0SjQCW
IsvVwobOW993Ocu1/QzDU3hEWVm7YvUegy1XeZO3tF8KmgF0OxEtsWc43ERukPl4+MasOC4Bt3mf
QzO77rL1uQ7kSbTuc7lYABvMUQzBVTdpwi1gYo1nn39VpKdkzuIikoi795q1amgP1Qnyj70rMJhA
NSNOrdyodI5d7XAsRg8XytfE8mZkrTqpzougmgxtC0yPCkIJnH1XMsN19kvdEoqq+4lUo5x9GbgH
xSA1Ntk+sAAXhfvyy5odbwsXjxOmdsqJHAhmQgneVXaErYZiTXRKlTCAHOXLerOoKZjl4A69wQ5B
AMc8zxZY0PFPgVVSJSU1nthA7lg9BzTeb9ckuSCyBUbkYpUAENM53fcXredORHVkAo+gDOqq4Ij6
/nGZtQe4PAS1XIOTKFY8QcP3j0cIEMImitk7G2LTeJcRNiQA4jxTI9wbf7tsPLM85wPp0xEpBUyg
ZgjrgsQoXYRVYGoCrIli+zOp/IWIpQoL7oEBlfD4Fhww0SxIt2ZSbFHk0iMSNtIrOgsoB527abrP
fWYSAEcvkZTpcIM/nRiYyqeKXJBwBymMnZNEJ+1hcYObv5iqqaySnxr7bDEyVEfzH5xUkaf/CfNe
nMUuw+9/vZAinIW2caYBJX7zk+C4sKwQr1Cns0hdaYaX7jAMPBBcE2pqGQ2ZdVvpD0DS/PsqGWjZ
/I6mzjDAVoAFmrX0x47Zv7rhVAm+PR2rGh4TM5wRJ6XKcYFhytXDcGtLt+gzfHztVYiMqQnSOVLS
m3Cs9Sd7J9IdInuugOWBAduROtfNuYYvqzsrPLc3RjHSk9ICc2eGhq9da44VvTfse7PVzV5Fcpdd
S4il8/P6Jf7jItwt6wEWTVzWWFDlndjNn96p4tESDiccHsnfD7xASdkF0pRGIZjwx1t/86xW86+m
C9RBYf1+E+2qW3mTRvH5zRxX+gGRjmBap18sWs5/PJJAnEAQDL9Otmg9Z4GHN/VYnDWlBvNImHXA
1N4PnsPaGMo8CUdhCFbXb0unN50Bj5t8Y34YqER5FTLNtynk8IjJOZFp6yY43xor/InLN1wrAuTj
QrZAMqVpoKhmGAOUUV4plSYPYz95wW0kDF2IFpG0KHYqBM3546+gJ7p33TT9lIk4HFJnhKzJUiKV
uJZ8xcaHVktfUNIHzTUHLA3UAoYi681kkhD6mfT5cayfRFZKTTEFOEt5ahGz+gRq7uzPn8DqJ9WR
e2v84JJe/9ZnzmR25t+VTh1oc4Z8kq1LNicrBpIbtcY4xNi5kYyzRdtbK/uu2Udd/VZT40hfWUs+
dLqVAuLnbohnUjBa7ly1AfbeXDYy2HPr5ovTqAR1nQ9phuCUzMxyeyzwThUM88g5yIKh3H+YdBel
kb7QwhhPm6M49zo+vz7Q14ARzsBb0dqhxE54fkcDFBpsPjJXg8vlQ787hwinrfpALx4h3rlvRvPE
pzYJjSxA4s9LwLNEf4dj9VkNElPUl+I+OEvm93APGzJWJR6JYKcNZxPriMnHKcrEWCAfaC06pSek
Jv4mfBJzNLbmqKgBFpA2VYKULjppd3gE63cECRIrSZMAjefEnv6veKBNx89jauat9r3ump4rKFG5
C0hlrFeCZ7l9v8RatZqm0RXLb5aTWWwpveNGp58cfacu9VQzD8nBhn0xGK5JZrJUkE1ErnZhTnqc
0dU4VIIDW0AfvsOakwpKjvEJ7Vd6ca6QYXzoWbnWowfWOdcnKhOHq4I41rL/rDr3Yc883zPK2Xor
hmoFqT63hOmEotNx2eDJN3Vu9J47tFjePbA+TzX68gN8szF/Pu4vn+sj3RQfgdh9HHFGDML8WG1S
YI2tscsAanMlnok2TgSYiWxgvNCBElWOKP6aWZYMfpmy7dmuMJgOyp1Uf3a3bW0dTv2U2W6KxyUc
bNU9CWv666uFMuE4AF2wGEh3LANybAbEPQpHBXE9W3eZ6u9CpYIuLwgjDoem+arFLv/8KrXX/5PG
Jc1jksQX5jEjyDlNRXOMqeZBJIq3/qxfSX8hrCGsSLZGem2ThgSHHGmL+HP8N65ac5FXQdMbiyjI
4FGQRuGai7WSyWGSxomZwBIe4L+WnSb7krQwq6dFSeEtmQmurtgSt/OgQDJEUL/+TaX2rrJWtfie
34GgX/aazeBpqup9yR9Tx8HHqioS3urGzqX1LvbzTcLICjdEMkFocBs3KCV4b4oQNcxU7v1nGTY0
Oz1yyd7dsEWmD3+yG/3RgUsiJsxLziIl+/h+PMqML/LMynLZ5ttUobAUsU8TQjBHYDQTmg1i0dVl
PI9KE/A2jzTAY1lTMJFiVC9ssC+o1H1Uxgf9u9dclllXuIBlZepo4H4Wbtf5mS6K2Vgb2/M4eyg3
ijltQGybYBF5hQm8oN5/H2Ggy1M5o8xCJCTG8aNkVZgI0KJSPswfU4cmcdrhtA/4xlzYdCPsue/Q
V/M8VCmsiLkkhiWsMWw4Gr7O36XaOX94yKU1E6j4You4TfFn650+sv384GMddCkqpaEQ40c/m83c
mPeV84JTqX5ixPagaSoSEtX9x8zP3+kyrG7wb8ZpMGPt85O7ihX/rcxBg/+JY73GMabxGZyU5WF0
xZoYemNqQ8qkyKeuq5nbcuzoCYW9gdx5FM+GVgDQJbzd3l3iGHjY0eVywvmT3Y22oZnYctnMfR2F
0pv7YDk6XTZJR11aa9/1BalnB17XDbbWhi/wjOkAoeU09lTABoPVpwcwlND0E4m4T+ETDv67rgaf
RhTge3ksuB74eF3HegPHDqDn7ab31HbAKW2H9a6pk3G6Z37r9F6ax6PnYVCQxtUqT4HwmbRoOHMn
6Ha1CSaNOfH2F2KxQ/C8JX3sYL4KQqXlO7q8u0yKIAti21RrGQ90xBL1ZM4SyE7M+PLA93Uikn18
ti8rqP38gSsB8XptOE48C6OHrcWFBFpmTCun/DjOkWlsih8nU1gsnG3wWPKSYmeTw+GtLq0/sC6T
tcBY+Orsslx3qSxrQ/g/C4CXK/zHQH8F1ZQuP43rbtfFxGT3JymyLqJb91PGO+hmI52KJP/h7D9g
s0cmYXzfyTA+E7WVOhbvWGJD2R5OUGYQwt2MqRX7aTWI7d6Qtc2BL7K5LEvMXQOHtJPB1uKP2DG9
BmpjrGYH661qRwgCnRvTisrkCEdqaL6he0RZ0ezu9vlzXO+g12AVvSxrGa/0wVrtPOO2EFF2LIx2
DAeS843hgTj7rb1YEkv5+AGPTc0HkrLO3o7JIRf1HEgc/8vWqIH+Kw3rjTcUpHulDfwE2WMevTbF
QnMS58Kri/2GvcPrYYAZ8sUdUAuQbV4QRAJ3O4GxyH7Sxus4XMtIIIEVhJ7qsgTnO7Ge9e3y3Qdn
wIovmc8Ae9j9H+Gi9CNlCvuTwquXlznGD1aEewncuvjKOIm/ViVpcwytisxkvtkVYuVKgCSvGDJJ
U4nkumtRsq3tCEmWz0PPCE6BOLMOS3XgtSzY20Gw9znLjArbPmevuOy7UwvTmWy894sD1odwFXrj
VYSm++YrQRhBcPwmtZZDDTC8eVj0/esDk0p2lLDCm+YopHesPkziu/zMuo3AjLlOcBWhlV7YY+5D
Wx7m8Nf1h38jfgE4wge7nZLjj6wObKh9iGjdYkLGqG31c2B6GWRvR+jm450elBygV2uqnQ1Qp8vz
jSMl1T7TCGSBXcgc8hCBF5HC9dtWxS25s8Wa9KbOQm4eCngR+VkegDDEyCtc+p0ZMjYg1RTznHc9
/nGm7h0GbbZXRlw1DuSvnubTURm83fQ3qyEHLGsxTlB0cjB2HBxWrjmWWjjt9oidAALSEFXHWrPL
nbjz/QsOOCMawqW98iLEn+O9IMczwdnf/cNHqtetcamWJI8DOR1UYMaTqEIwS3utmReWaJXwLDbl
HYcV/6sMTUKDz+lMiFXsOV7uHmgJBomz6hkYcQDui5F/PQ3cDhiRaYMXt/JINLDhVEAmpaEXT1XO
DLfZf0rdCM8XENgBHefThM03ble4xMIEM9/VX21h1IdYADX+ZHGOPZ2WHR7+E7BNvteFBFLwBXBO
TURsdgY0Lkuc7fKh5pgXtpxQH5OsFo3Q74rHjyCghY/NsgkblSaEVQZUTFVBriOfqcxz5FarmLy/
CANq9Z+Be18nKGHLxCOyoCHmWArWUJdGFnL7NuSs5qdK0/sgmR4B0yUEsQZZCib1nGFHEp0c/C0Q
ple3CvOBsNqQrmai6RpbuRpXC8LCKn5iKBNCWc7SywS1OXrfKc3DVJqIAvI66/vMJV6TC3jKCUAU
4WxghU5EaTF8Qc2Ts9HFqXSMRWZV6hpR1PBA8odcD75+yjKUquH3sZnUJKzKKHhmK/HzM5277c7+
FZPIMUrj7TEF+mYxJizn2G3l/1EgVCxyrPc6FylCObMeDx5/KeWLLrG63fpIHpivK+1OguTlOFkW
/agw/Y6TILoLrsX72fl4MQEP6gUbrTcBEFvH/jYnRkuBBrzjQT8jTjDZk2/6T/cjjnES0k+KU5OE
hAqnj4BFeaMKyVBrORXP45O+A21V7H+4DBUdp0CjQuzqybDd7BZL5fr+P4h2i9QXeuQ6AuYfrCsz
fiOJwsdZiPQ5/TiX0DPJ864NA14Dl5+nIcdnLHTl/e8xFnAAUAmuRLOR6YFf63mteU89jtMtWs3T
OeKUtNX+N6cxbhmsNU88VIjY91LSfCDtEwidUDAQDWBzO0gCErZYzcoBS1m3RfJOGrgjliyfZI5F
6+pcgplYr2mN2qH3lopcV+dqhr3+TS7dyUW3uxROReUqitsObaeeYWSHg7wlAzbJadbrxZDQPoKM
af/hHE7PrKxHFHBtg78TWa3YrG9IXrnHKVYdXxrK93B+13pmav94a6rD0IgJdbyqSPPVhWuXl5aF
RjAxqUbnVndPZT6YiUX5Mcnybdavltn8G/3fWBG5zXJ0yKy9hESC2m8JeV9vN8m7HGJZ0hCThYKN
64tI7EdEiKjDRUauiHnqsv3xmJWIujQw8SGzb0kMheJ/gdck+HAX7XD8gEE9ukJHQl18Eqls4Xbs
Bryb6TABJlHuT2Oj0NVrqM+h0o+WUX0meUISU3BN2LREkLG6/1fg8ZXsQrrwG+wef/v8d8mwb/Jj
THvpdF3FCOQfB6uARqKZLVjrzmoMGnQAvgp9aQA3SFB1csBqSdK+mLehoo0l3UYBkZOWlg5tR0PX
9iljhMJyXCZW+Rdmut1jLsajcA5JwkDdWC4hYObIFeHVlwJvU2edSaZ1tmnPSc3fD+tSN923lak3
CYPYBFiSnhYxFfMIQL+xYe+X+Z6yCsmnhW8Bwvk8VO5vQKjatJnNq/5Vr4y7PwYmCmDjKbt2RwhL
idNMOh7zgdhzekTMMr+bAvtYPco5d/8YwzVoXwUu4emg6fh5CoN3rdqmsp1vIvj2STSiJMUNVkVX
s/U9BzRaquqeNbyUEsxvnXk3dUyoM4afBjdf8Q9+Ywtz6DqpgGXb5GbpD0y/OCE9gXb9IGCpn5MD
QL5oGpZ9EjTCtdwn0eI3j59aDX9TFryK+KEgcEpiCV8vH2p67Jqfw1RCOXvBUeSYgqcZ5LGIAWrC
pO5Z8is2Pa5RgiugWsXNbjApFyME+rtiSUPtaTmEMMtbwwjBBheoe2Aupip1+oYYSm/kEoXNKpxH
Ftnxr/hMh7rzSSdPRtJLEUYfzns482gBvAzkgW4dkShtBvZ09+XApqCZZdWzjH8a7g7MjURmSwU7
OrG4i4dY7h/hyPSXMwrbWVhMP56iK4Pbe+cv6JNebjiyQhpjNHEtaRIsfGGH0Mk267CzXfBd74sL
Fn1t8DIjfy5Qe+Acj6PZTy+rCc9j212zqIGVSKfK9fCeIrizlCJGw0umpQAMmzvF+TkgXZkRPCfp
HObaFdG7dlPM+VPdMyir0Rqqkkx2UytvRDlWpAjo34PVlxpBleQDuWN7wmSZPlwoL9zjxruVs1ZS
wEQKJCMwj+y1Dq/9PNG8KJd+jZ9pYUZ3xxkM7Wns6qsYJG8tHYN+ITuA56Gag/PJeLRlfekqxqSM
T5X8uwqUjhCyvtNRY9fgNdf21uG0TWFH/7p+k7/DF8e9+TIv+gery7wBL802U8/1PrlGTKZoKDxs
CUtmVepoCowvp5xrSIYaFhKv4zHAAkslWw/t74XGox3qvuIdSX7qw/fYP5P5gxTME7Hv9RqgXQmb
hIXXuogZ/jpWWXHB+8YA//HpnhlmrB9IhRCFCn0D28AvQAvELjTsbp3B2IIKfehU03WOrcyAF/2n
GsDZfnMAETT2Ic20wsZdjVpRjGkq05hL+FLkVcxxdU0PJRb08v4yRB3oP4LYgdEJQbk4bCtxazeY
xU4DMDLB2FjNpoKF4OyJoOrb+xHL6GqHf+2M39oxucrhGqTmHbmyATlQCrQkY5CqO7e8JF4/foPy
S+L9v9Bg0HuUVq2BEHl35yezbhvCiy9Q7Sr82DsvboWy5lJzxdMEYOAYLQ81iqEFWK/5rpZQCsgD
69msAGAbf6ph0n6obhB0ozxQ1xIwGvB2PQuBh7pZpHslVMye2Rnq18T+oxjFRv+FxL5J6NsMm50N
Sqxk4i4BlxXSnJcLmMDYNJ/kWg3jaYLSsSgqAD1J5SbDeIR3eDXP6CatXx3N5c/5T/HeST9xGw6s
iAjHnJahfU/8UBtMM7nWZ7WM5Yx0tyJ2CBFZEo/DXqVN1usxuDdavDexTjZNBV9ekOv5J2RY5TOL
j0Kqsj12rNh3Ig3VEO0IKRK6Ysi/xutfJBDJbab2P7UADFYlavsNaRTyaLol/5qZ2KArlHbc/+gD
kgvVL6Izv8gyN1hANiJZVKMwjAlqxrAIH2hv8P3RUBIHuijXG587dtHEnknkfveptwdUEBvqiOO3
vdFtHndlhVMjzx2n6fI9ud3LPBQfdrlhx5tzlNqXUKB+M2iKWwqGjqIlessQ22d06ijbnLBVZIja
zcspH2XqI78ThepYZqTrxk/WHaQ1I7zdI/Sfd6nHdghPn5RpxLKWFOzPUC0YfkISTZaYJyIn4M6n
JX48E+frbVItVsf/ZzYcU0wwR7Vbhy4kUejpcWC5VlZjb+zCzSvOBIPWnQggyyrw4AjLrxyRt1lX
x8GuMYDsTl5szsalX8vf6iH8GzwHhM16M1KOXb2BtJMr6m90EyhmOWsQ4zy+VDqqCrDKeFIyQ5y3
pEh8UJm5MBuB4vQMjQZXpHFqarGEJKM+em1ilhN3Psm79dnT97SSJQF+Q7mxKtYIA8p6bWMCP0Rr
6Iat96c6gOj6hlFKVyBLMpUxY+vlE5LkKBwAS6POF4/JpQo9ASIbq+g5qLELZGn0MOaNI0V2Lu9K
eBS24CtkPhO51rV8nYDDJJ4SsnAuNwrM+JZC4e7M9X8/zdQKs7uMSezn9Rbi+xpVevnPFKHJk4j0
gRhOiADl0IcNk1elhaHOc+gDg7TjvY3SCJD4EnUd6WGVXXOsMNOb+Y4x1CI0NZfxgmP+eOu+cXVA
a3+llfOO1MCrh5PALKEYNwVHwoKTgzgqpBDFIn92XtaH2bfyQVGiAsaNni37pnXBYWmKVAhmdvZw
i2HL9u3IRAgUuVLKE3vye4OTt7/K0OaGRQJ0KZ4fYDgG/mIrhQ7i+8IqIWFkC6Aji5+qC4635ZjX
RsfzhkrqK8hjzYEbYFRjfQZkf+FwSyGUc+Sx4KM2zMohc7wk4/V4IdGI+MDHNYGHhwkdTe7aXY9c
MqPLCWo9KmQNLCH3Omr4J701vEcbSBeeI1KOWXRKHY04TmyUlcAEc9vE4oWPpH+LQ3wD8uqM4uZP
EUx/rk+IJgAXK1sH/YKmktlhwcMxQ61h1+G15TiIGvUgV8DHzpocuZOkgR0PAvCbjAmsqMiYuoNG
iXTZUilX/xgsiTdAt8WUcYKSwaeWGTwa3SbYj7Nv1+h1f9J9Eeeb/g/Q7uUEtM6ShsJFFI09hpP0
J+BB+WKXGAI2sR4xzfpoAXCFk83f1I9nMlbb0Xjk0XS10zk3I76HEdou4Vl9S//Cx0CR+Iau84D8
P7dy1vxhOa8s6IeoUjqIM0XMrqmIoMCSh1xaL3yWXJ9WGQTv/xFuqXjJtsPjw4Xsyzv+OPH/P4gR
EKuj0hMoYn6F1NRWPC22qVmuYunfo940nQQufn+p4GgPK3aRsaEMUbgL/rmlKysnfnwt0Kdv3EQM
tnbYSfDIpPoG27oIyCE0Ccyc+EphzotS0Zir+ONPrG53Qx36J3tD0+T/px9aRJ8mbqIiPryW8cG6
qO+uxHLZHbEOdYCysIHfnx0R7FWfR23ISOCI50Wh0pVy94at0/h2s8xld5IgxlopQXIjQO2RHiXt
eN/FCR79y/shUCZcAG0hqP0avgtahApYB6enc1t1Ymfbas15dMMpTqfbvi/4MJi9Py4ah5hULo2X
ONkJ+Pbk7dh1YgyDxuANuD5CzPqDWaYM3COsyZW00bl/agVKS6AjFP147CKmUb9hEfcAWhwz2Waa
9ghq+k9JyBQxhW3vLuJdyDEgnBdC8DqiT0jMmeUWg9h9BHyfeTF7Y5PmVGOBtoX0wa3GLlgoQQb4
HRLeS3xHobzzHAvwB5H4C9iq1zlOx1TY3OGsqB+Re3q6gKsReVNod6+ZGeLO7kJS3S6/EqFT3J/A
Vi5KtuMNagCsgIqZXoWSjZMep4L4srTQubXEfVGqKiMpBjx0rPC4mDWms0C96nYh/h+mtdAFC0gT
nbPKBXZgA5f6/AGrq6p7x6SNeJCva/92N84cBJi6vFeJvwC2J11BmJJkgqcI+mLxWe56Y7NtPrlu
sXkGMjERxWmSflZAhchywIdcDHKFXy34YdG5bG/A+5+xMGDlGlGE1kIp7Ofx+IK50QNQ7Jhpwf1U
oET67ZHkLBwG9yA+LZdG0iMrujeL0930HuWZ/Cef8BaE0t0z/tJGhhpa8ykUCcyGjj0Yn0YFYtMF
1bhcTQ9WygT0TBuANlatnOuV1ODNdGwY6G0K+32o5vgcSalM3UuNkM2/mD225b/W/7kcP8HrNzTo
NUoB4vgrVtfv1zkM+X2EVVAyK0KTnjhsIRMDhJsscEgB4co74A24YQZIrYkhMNHWGk45npUwbZI0
2+C+D11Sd+ZIdIswXtYT4lYWAT9MO+S5whqg7739ba21lk9sw1w2CIEO/OBVCMnv56yd0SdS++tD
CCZNHUWSEaobFV1Jsk/yDuzP2g6nuGmJ/GjwJ9PzlgjCVrnblG4aa/BcxwRjAfHhlgBoktH594h2
14GvaNgioRti9I12tF4rY5/LxuPS8S3lCk8/r5y8BfvAtEennbtUHde52jWrfeIhV2KRbWQUd5b7
0FrALmw4ZQLKQNOJmEa96ycU26sS6u2kvwM7v8/P5C1+xI5jQkOerJ/Zx0hev8hn+pFs3aetkP/M
XtCW0SEbcUgJIM83ELmPRkeuySCxD4rnNwMWlk0DTSiPDTC2klVBfjQ9MQFo/m931WG+Tzd3cafE
iRxAHy9PwBJFnBIjnID9wZlP2wAKO2l3FTAoK3smO1CfvQoMWtiJfbsMkV32An0VD9jzIzw3AIzP
td6a4yVo5PF4YUM77e6U0hot6+MkNXA2IJg47tEsyYXoeDz/U3kfRcj7A2ZxZBi6R+B7TO8VFNos
hL+OymzTuLQHPl6iyKiM57ba3ylOJ76qojGu1tO2eCsR+MA90A5LDMr9hU5vN5XEKJgc95bS7Lek
mNd2vsxrbYAiv2HhML1Mo9qiDbguBw/bXtPCST/yxjBvXaogq5V6G/5dUArN0WuHqMSx8GgHehVO
fHFBe2vGLpSHxnulpFvS/YUu4/We9xM1tc39L0rMlOu96FmS3LfXRaUdkvf/ZXUyeHNUMyM04eyF
9HaudJbIYsjfCPpZuiREd1zQxWeYqcd8dfbvWyDZaWD7lHgINwp3iUSBP1CQdywJaLv4dDFScFlh
c3kWSUaduFl68tcXVmfZ49MeQz15l0VWRNhdQ14j+QyWMt18fUZmOVDKekoEyCw0MjaadMct5+5X
DI9U3ztn5hA71kATJ0Lg47faIGs4YZpY8r3KaZChAHgqTEZ708crmH93zkCkc0wvamngzcRoWnDj
/8nK6HpAoqRRKVhJavG5HQmvegsIjDMEV5HO+0gK/LhiUbEKK99TB1XPJQj7Ruwy05rRjl6lTMZU
Cmp62jdN5rFamCXHd0UeYejppRUPj+/WG+ZLtgxWbLum0ksnKAnRtVRP4U65djKpoT2LIyHHisQb
L9ZgHSWAFsBG8U4/cnHaEfjLUWJxcyPVn2f62rR9OZsulKgBFyutie/N/YXydRWlCCJJi2g4A4us
duC5/A3GZI1vImpJ2C2egaPY7ISevrhphPVoeUHfoHGCPOHBe3DzXQLshuM4o+jbytLMSSoerT+v
DqgFbFNcNqRVcCnDD5rGcEkhFqVKlHsqxCzdBHA7eNhB96NNV7iIWs4OPLqFZ/I2z9w2XXlSynP0
/nxIe0KJGb1GGqhlCxk6x2QJR7EV8x7b6cHWssI5t3EfMV4homYieJz+yJrjGgPq5T2zQLjcXwsU
eo2ULScBjVedTQD0tiTj9E70lotBT1NaJPqfNI/EZYYXLXVoHNti+o7q7MayhJQZUEJa+J6vyNRL
jyIvxU7P0160VEH3vTMDRYiNTXiDBvOXIFUSl3fB7US2ZYz3lxaQSf6TiJqbDskVMx8e0AoNE03h
CoSMSXFekczP8QBlY+DK7NyKq5YFinPa6B0rvxnvidafaBUQTKs7PTngtyB8Xq3VFYkifGrqkgkC
3UN9MFxq87gSdJKuKGPytDPz/jca4bl8kms0wuqYFiMBOilqvLGREomOxaL50FOhPscCw/ePslCd
nXVawBCAoVNpFE6GEzbpkYtRZkx7HI241xrXDUCpyCiFMQ5tWszweqG4HhOyMxaebqaFVDIsX5O6
W2m4sOLCVUBSoYY+tObnayA/XuVw0FWpT+wvEnYrOVDDSEm1pxbPmsLDFNXw7Vdu1bP9n7XFL4fJ
LI5TtwQ3oHjb/7I4wYG199YEfMq1vFrGbVr7v3FNAwty9UuyCDsVbq7j2CIUXpJQPukJIqizTzix
GSdVw8gTXm96K4Q3rTbvvULBN0oTBINtIut8oYEe4VKlnwQByYHouMbh0zCmBnJgyIcLx81EAzvC
/tEgyEiS4UCoY80PeqFAGvbWh93T6fZB00yZL8CL7uo9gmbPcBz4cXUzHprUv3oFhSR4Hkn0Y1C6
VeBLRIFngkT2VkTqJHfHqr1dQzQVb6HXOpUjof8jkGr1jACpz1SgiXcLInr+Fgl3FqlqRhp+YMVR
Ff4aRPiRZpFbVsHuffH9nIG6W9WJ/rsHN568tDYcDjYE5cxFLn8nrLMUO/M1XTMef058/g1cfH9o
TcDTUSo+RsiBm9kTEMrLMaVZwFU8rsrX2CsHFcQvQNTnZEOXNoRppBjEqqZCEQQQH8fm9bc81s2h
6q7tQ84//DXwdSAVlmfq3446v3jKTV22euybYeRAbpyhSWHEKd+n6i+v8DXnXbuhirRF2AZw5c/f
QtEmZEjLbTj2Cc78IH5BvB80s/AniWiVs/Infd+lzwimf/W9i+adLKGIVoilUmZSUBLUD6xDDB/q
XjHPLqh5T+Gy5/Acs6sOf1K3UkqY+gRfAWzxFIR+AAPA+VPkC7Wr+BHzhvwra1PlJ2hO+z6tutyo
LwcLwwkEJSCIC+FtvKiyMqli02zliT01sa4Un87X/Af60KiXUXOtK1wMAhYwXHdK6oUwOx7xFMOd
CvcxApzD6miNxtPCXJBXTVSJbJeG5eVfvOwy6AC1PPyZkaPsnqcbB6KE/ovlEjwN06tzNK4ssEyW
gdZz3SlgJACRpfwFnSXMp6k2MrcQZP9h+SkZvUVog/f1W+C75GiysoZ1Zbi+JZmsNpgOxk6oWkIM
Tt/LV0E63NYF3DVufLupb1hoF5EVPcQDyE4Q9nWrj8vmXnSoijKuItZfrkdjUprdFy+XpHvBX6Gx
YODB0ecvb9p7hzB5Iv4hPgFEj8qcsCyZ2On+DFpwCZBZ3psvJl763DawtiHhEY0QqkK7uPvvP2Iv
F8LoSOnR0Nqt41yPQQb4FHHINND47/rPDng9cHp/sWp89zqL77kxzdA+2u1hJZvRFc3u45dYPV2v
7nnQhVtJ3RM3kMzOVL7oUyOQZ0ZsTsg6orMVR3A+2+ds6ESl+UMwFxCNLzuZslksB6tIDMl/oAW6
b+7XC6HUM/c9U/boi/PmhctEF7hOdxnTPoIjxIMrFBAfgTkSykSC1LXmvOnf3V1M0Ec8mPwrdH/e
7U1OFg12Y87EAxqqwLiz0f7BZisTcrMDQrj0nXnUaA6j/G6vyaPLvdm1/02DuNl6NeJI9g9G7OQa
vfgfjFzptZChvmU+pEsfsHNiU1ahBhMhKRHULA8wz7x6flKeyktrxjvtcP74s0PGLNG9f9CsKZch
NzJs4ECIwnBCMHZ6oiJRGpEJ0MkO0HNIqJDRTUmPT9Dv3DyOgk9xzEY0nW0vkivXwn4G1xuvxBan
AZRPOy8W8Mmi5q6po34qP4ZA6zBBtKELOgNj5NBL/jJ19czPVWnZs1txIlg6xhqnCO9uvkOAmaIY
zTxuyjCZlvRg7pQDymkDIKoEbu0sfgcYuPXc9CQrhLYCave1futkBfB/qMQ9YtI27XHQfmE0d0zn
uFdtpNSSsFdMknLScWF1wTeuqq3CygJy2l6YYx9s8k3io1h49KyEewp+/bCjjfZ9C30Tjh1Rvray
uhJuZWWvjSrfYakcU4ZQqu3JfxBe20i/pEZKCqHdZKSpS3MN5w02my1976bS+puuLZb6t2IlutfV
uGPVsOLQezNbu1xvSHiUQxexsiIwfsM8JgMBQqdJrPUbggDEu77M8GmsnhPy1ko70W/Dx4BfaCUY
zoP16MwI9PfN1xfxTkvDrCbSvXUxpoCbTwgeY1oJWHE8CCVLn05q0oDoT/+AAFlrT5leY9mpjLh6
N5DO95tzLe2R0+jMANhaaQ8Dp+lPkmZxs0EItK3rT8jE54jT6YKqOtdDDgrvK1AUhiNlIEIH7oT9
FLiWAeIWhKqtNEGk5NfDOiJwP8i/dKmZC0mSVN8SAKKJU1rMA5uZz8SMcFheoDHMXv/yfNNsSfRB
au4uik1mIv2+cKtteTFNmfcKwEG1Qg3wN0vp6Ac72vgczP+kVyV0UQRvSUNTRSYIAtoHNxYl0VKA
a8n/2UGxRikk9riCoEK1DTUlep5XlGnfdeBvKQ+GGjke0neMU7Vfi3cHN4MeLm2F8dc9rD2y20gL
Cv4X9tNr/zvnJJA4o1AeYfqhO6yUajkhVZO5cPHI1BD7rJMXXFkzS7ppAAqxz3efun6z2w5UgEqa
8nDwiRdhGtPmbJPjUxh/ZXdMc3xgcSYKQU6qcyZboyOUvCLj3OfPDe0IQPWJVR1UQ8qhKH2QPOtZ
G8c2VydZeUHfVM0VfBQ47ReErK1CzmYX8YH63v8NnHAbGAa7PYy5WmTKsADRISq5CD/yrP1Bq5nD
SWUWrG8viCs7lohta1OT0oCDko62fSQuvopy4Zvbo7zdyRiXKrOeoKK/5Elo0Y+EJ7kWEkEpIwhY
EreYPPwqVQkQERu71f82dd3r6YPDN1FNTJwCiRjdj7s2Ghso1QW97g2nWlo+ktoUFyugb49nppgj
r5TySNcJS7X6VP6gQONzXtCTaoOtCaRSUaGOwTUXVYSQcDY3nJxyKialrb3SlQbvEm6QDDEKFtX5
oWx4WSYZDOQ0bY5LD+PHI6EOTg0VxVD5aA7YOH75RvxWy7SQsnkWBD4vKl9bRRjZ/Wfh8ExXupN7
SKTtcXURiadQ3t1fFhVUmxzkfs0vZ+EPeraEGqARoDRwBNyGCNr2KOxbYEdMiWoFEb8m8yrv0kpq
gbXjNBUluBlSHBLeXzQOV3i863+G4odtv/7g+s/oY11qWv72mjP+hWCf9V8XnwE5lxqcsxqhMYO5
S66IctC826H0MVPDVALi9yeokJ5XeTvZzgQZ4a3shvkGW+dRkPLfkktLQzUA7Q5puFM9rDV5MYAK
fa/tUaGABjGHiEGWXveY5l0Z+NzwKjyNTddOOtQmNYyWfCvWx493QMKOKjMt28ebX58QnW8zrXwS
vHRpK41PY0pgAhneBFf+a1K1UE/8lGJ33p+eN2SkmOzPGBa7GnwK38wLHh4MP1p+W7MW6UZjiSho
MPJPmCddnabG2OzaL0tohkdphWGG19cAVk09gcMr5xkR4VDUU6DyhnhT0zlkpG8qHS9BkN0BIclx
EI0ySg/76Mcaf85ETTpzmeVaO4r674wPcNOri/mi6ou5b3OlEcmI7h4YS0a/wMTO3am0LVCEHYRg
zsaxLhgS+N6CG1OWIIO5vBUELQM/R286ZuPnw6BiE9CoUIAAb2usgACcwbMCWPsFa9Wn+nAMOguc
il97xyGWPX0p6MkdnyGza61Mh63JnbAQ05hOfBnCHUGX8WR/OOdSfEgssDEX3BjE6vaNT8DAPswp
Qo04PulbzN6sASqaJx73SbaxWIeiP4xO8nEu28dHzyHOVHClCnTMgew0qdWAV8Syg/Lk3hQF0nOP
8ARPzn7LiresW0+rz3hr9EVGcjajV05JAbVIkaIg0gRtKkpyWSwy+dvU/nHtcpcthIkZ/zjA3KYz
d1TklI9x9IhltMLenLX1AxV8n5hNMVKW13T4JoikninmDiuXgVNKYozrFou39clQYeKx1H5LBfkv
RFODNt1GXaq7e6sjJYl+DnRfmrjkMIFXhAX+dLDzRRwaKyidLiImwce3yG2XG96kEEcrMR5ZvpPZ
L0jMlfj5SdRwW8pbAqCxNEK4YCcOU1Cz2UH59WCEWF5+6e7lHW0bjVcFxqL2SgNi2c4c0VI1LF15
D3wMWwnhNh0cn8/+GicfgrE1uKUNBDq0Ji/FmgszMFVBl+B6akMHOz1wltW/sGjFho8KVg7MZmu1
2OYLVkvwQETn2oIpWQF1rkICe7osdVJAbOEnNvDtEHbSKbUTeHqBY18sq1fJAVfGfEaR6jrc0xPL
DfoHDYYIlBhc305rK38duWomlw/81SzUZ2oc81se2pZJLPwtBunvUwJtmnviUVMoD1J54PnWMk51
xVC42z5KYgq6fVbp6niXB8eV/0CWVKEPPbuAnpKSgCHdWtGQM//KNHKN8twRd7YIms8N3lzoasFT
/o+U5tenC4K+QHMg9Oz13S0+1kxcjkYOnidAi8rBdu7fdoc1UAIUjKjUfpLhn1e8kz/aW6/gEo57
oKA/mM/8niAtEUDHioxoUpktdwTZ3HS7T6nv8qN+FykpdxL9myVEYGwSX6nbxgBQ8d7CdPc4rw0I
Yt/28WIsMkvxL2Xxu2ZLrooxpqI/0RRALLjV3yuj27nVmhOwDrBx7bqRN3O0xByr2O/WUxC3n1GK
kE+ZnVFUpHhoVIMtg8cVi07wExDvXVVyOEuQyIshUkbP9BNDTDE2a2IvU/be6AqYQx2X8NLAder0
nUQDNHoTi0tYnAV25HOKCoMwzzxM4r3uun3Tkj4Yl3Nun3TUP66td5v5/p5JmMDzbaEHda4EDMTS
eQ5HyJE0YddVyj8TjmLSh6W9mYyqT1oxSD8WFByO2+FSK4y/sqRck8dxu/kJw1udhVS4Mm8AW1Rd
rThye5JuCmeehTi2OAk+VJzAy46eHfQEs4nY3RIHMKUO9jFZXTDxD93BRWG0MxoXG+mrTkDeVjr0
x35humIIFdo/Y0ZlezPQoQeWMF4EoOBhxQ4/RoH64GMcT6doeXhockTGFolzECHEcy+nMH7z0X8U
7WOk1EHcT7JYeZ5i2Q2ayF89tN2C2KfCns/4U9EO5wzES8ZnyCIxPXbWpgJS1xdVmls6bYdCp53H
9WUdfZ690uQzy4I/1vuIara9lvXaladJ9zPaE170/gFWiUAeQOGoM23ytsKssIqH7fbGeV20k1ld
dwV87T+naZTfBjk21uUHkIQ7ipNkk+ClP8cOkDyN+TEHR6wocc+kD/0KR9OMuGyZHIoj5F7Jv9uA
c//qikXNquNL2tYMVPeN4Fk/XA9imS3dJjq9JFYpznpifprv3AfCtLc+MQ6BkcFu/2C0TQQP+dJT
ECo+9ThQ9AHMMbhD+WhgzQndT3RsU5PBFvc5zYsinmm+ZpcXRhsR4fCSjzoJxBFQ4uHdUnUBuLWx
Ze7u8wds9JrGVo3WU5XrHUtGhPzwTlCxwYL1bg4NN4QT7J8UwBVJI12vushG9LLdcUnzOMFFrCOq
JmhRjSqjzkvl8qf7+HZHOQXD5VocrLBY/jAet8XYqZPXO9YDYhnyUYvB+h4gqE1LL7hxlK40S5tW
u3guu/UeRB3X4PL9+PeyNYXPcSBjJxAv0RoV8aTEL3t9owOqY9SPFEFCNNti2ZFFnyuP9hbVms4/
CQTzHk6VLvOi4K413g4QfVdSjFamtgKem1/2fJAB+U8y7m9XT2eJ7VwWpggYrne4PuGhWebDc0pH
cLzu9ezRIl1qRHedHr19xj5X90ZeTjU0D+PX39imc8dZ86cKH0sJGkXQexC8YDWrtcfMQ/BD2ASJ
J5K9gHTeRJjZTss3fzp6UKlgAUZwGa7n/xCHWa4cScJVJURSDKTHXuTz7G/Wad9jS1FFbw5oF2R2
2XMQnEHmoMyhmtpStZFosIL7RCFiQyWWnIC5MDrhO25tHTQ66CqhKa5LvMQA+oXTwzcr29cmHT+K
zlT9J6Z06C6XTWjttELhqHbXdorUEiRjJvV7a4pKZO1tF+sHssZ8UOqEJa7W/JLHqb0NSdsLPx9m
aIXqueogvJ2aCG59Fbp/JGh8Sbe4qHYLyvsthaszz3R3OmODkJdI+eQcGKqLEw11+tEQFdMMsDQk
8Syok8Bqw/qf8BL9FOZs4J57OMYJmEHhzfzm+bNMKJDEfE2MwIIQOWTcpSH/APcOHzwuLyqVbtoJ
OqbNaon4p7pDXlEDVKMgGETUddyxGkU19wRNCUYztq808jgK5woF201BJ3QHcLr1q8P7RkE1e4wt
MpYUalI3PUG9BQoK6O5vjTcspWYTvkDSwhXnJLfEEZxjcLMw6+v5N1gjSxr6JNbKsNnZbZveM/cz
J/De1vlk9RO6OgFtHhFbLaA3ql3XobJv0kJluMB8TOTdmPTFgSB0YUmcqnCXD1HoQWJKk/rSIlkd
ShPsjdEaxc/Lutm1xm/auCllVAUi0f+IgGh1KyuO3bQZ0IQD4/Tfc5y5CiWoIOZWrb+yNAg4PV6a
MfaB85fdLpkaxKwLeWOKUov87cwMp+b7C/qhu+j+Qmr4LVtt8KSkjEhl6fywBNicGAaQ17wRK/nu
3LL9RQuv+yXIp6uHVUDFNyRHNQOGNMuewICo6rT2H6eZR+kPnm9qM5mebpCS3rUsptRwI3XPiToN
C1QaZNIXo2eSLp69CuhjDqY10EsfWDOYXvMF38ugdmwrJCGBHWWOIBC+bEZtFkAnCmJ2C/8HioWk
sHbgVWk+RHERVWQpBbgLjLa3/xUmTVaey8+4piSxz7/gX+BjMdu8IJM0tflV8fTSovfozGf6ENYh
PfPzo1Ss+LU0+mC4IHvHakpSYnwj6jHMWXJE/vEA5Efoq99R2LaLJpQ/hv25k5+p+O2MAKlBpW0i
nNi/Ri7LTNGHghLQa0KuOAH368vUU6YGJOttHdEYjDCvkQiiqN/aHERtLVWbVUBhxdbwQB2N7jnp
ws5iKuG4O2Im+/8HoR/3lM24C0jj+zDezmH+2S+LrMlAjVlwpsptF/BQp+Id+YRcrzv/PTrWLyqA
yOgMsQfFDvld7oYpBy+ciFpv4K3d7B1/JQhHEuI69Li/CNMlKgo+XTH4gTuXRIdnJPyct/UguLVD
uH4Ydybpwrvl60Vn4MhilKE0bJ9OpFNk1UMz5Ch+FaDqKb1NWuzr+v1fysK9A5kADyneIZnKkmvn
nGyokUUMNIEU0z/8OhrKlpbcgtg+pRDtU5scXe6tvxUjq/pvPSk3i4zKxkKQtubqbKeWDqENaMr6
OyKy6rzDcedMEaeCUZV9mEvHhNY8GbJxnUr7wUcDGjJFOe8kxeGPljINgVZPfVebu3pUcHJizE1U
NX+6gWyK0a4oL7tZDEtJnPvCgkC1mzVNAeapL6S8mhcyfOTXWB9WfvXNHxv2txPFXmTPct6KftqL
x0Aw9kuaUQmfD/iS8RlBBIDIzgej4vGv8TMITPFVfSBQfAQ1U1a7ycsAZZc/G0f/1zFYrwuDDnxp
9KAW/EQ1r7llHTvnRVgYZlbsJXCoQGEwTU7fNOO2OFzD2oKNWK1Il+SyDX0EYwgENxYw1WhQd8/p
ZjGdGlWQL7/yMdPUenmKAFXleCOafechfwaYKSabwbvqyioaGzYaJh6eC7I1Q4O46I+xIyKWGgB+
6U5p8sYzMVTOGZS8fidj0u9JwKuzRv6BQOGZPdBFbiuK3MasQqE0MjJfKhOfc/aLLUHyBqC6CJsz
9QLp7f9PquwObRUfQrR5a3OrTHedv8eD/xU2gsbBnRbhLURGltw/vy2i2y1rmhvQkkarCk5FAF/n
twNIIXxSqeOhmwROfOEwVNtKVO4ARdL7wVlqFeYWsjdSDeTFXa6G1P18hCEEwvTCnV7cJ1Mryw+c
osTngU/Y+mYg3EPz1+DTwW1sBLB29LNGPc0RndopMc6i6uKg946Fv9AZ+gE6Nnp3ygfnLgZx2Da/
iZVRwjsgNiw3IKbSf1Ofxy1kArczWbMeQg1J91QjHvD/Lk7MwQLi/8g9a97GGXcz+e57SlHFHGz+
a2+iW87SgJSt90TySE5NJA+Xf8dldSPVb/O0wGyVOsAWxATmYh8VMR6F/JE2dgOq4c5LON4y+9D3
/htsa7pQriigMF02D7/YaQkN7NIL+o6470iA4yqUSWHaJZw8/twDE43hhoLlT3fr/83NFPks12o5
0uIG1gAGRSlQa9Rbyrzn8MkcXLKIFNfzMqukp09V+wTqr36Q+W7BPnyDVDogB6IX2Erd3JETJ3TO
8rhH2GPYc+suAqxP47c2clll5dhaCCvFqWAGdHSqNc91HxBYF7ObRKEDT6zJAziOND32mwyD3/GF
M/medKlH68R9+fG4YRVR+jSz0QyyTJ7xhoAwJcFwdR4O05nDJiqMPhELLSkmdx4RLp8Tg00AuoAE
S+HP5uUlTqK9ffYzmVhPQXLybLwTB5jiQ8mgyXQTYns5N1lWc1AFI148w/cn0qjpfFadFcM9aR4m
GZ6p+QZhWUB4gL2Wa+4k7p7T4/I4WUo61VMVE1VnA4uP+1pt8Ktj3TOarBVZwDoBaMTkXQMWynFv
ew5o6yXgJpqnDT/1VaXYh7nSKZ4bB1A5vpWpgCAGp6T/hlQL3HSBlDhss9S+MZyO+rzKqee2Zxgx
Au3rUwtIjcN2Z8N0JGcjqF6ZrapL1vCiNsAjPhK2z0tTw2+d5tpDWpD39NzdWgp+aIFHgcn/Ehd+
Vbv474bDT8nL+T7At6yIAMcAGL2dUB6O9Q7rjnHQu/CVcVe6/lLxgtqYICTwUYliVbCPoEKRdJzZ
9eQv8nqUHE4+Gu9/IAz/mC8WXI1IA38uT7uGq/f0MagiYYiqI4f/wvzwGGWyfP3TfjiH/N8jWBZa
7ui7+/ezjfXIIwCaMoYL0UklLHKtM6e5MVlaiIruECcfQu7WD1DPoZWIOhYfCH83kmxkZfvSITS8
VHJ4m3aLPllbFXM3D90TEHWn4qH9IMMJCe3oIPr4u0spx29Q5M6mquBFygrH50XcUdZR7nRwakvc
Jps8O5oQLhOzMlclshnrYlApP0woQ3Yrm04tvYiHSj0GxGFuN6+cy3hzf1dWPiVcmq8SWFyPc+zg
iV2FKK5dTOwprUUcQ96ty1spVsuXb6TwaUJQIpzL7rxJylvySgf+rLyB3rqAZFknXr1DI687Cw91
GiTQB5vIa+5fwekUeVGhM/6b33WkZ1PFEZdd89d0EqagW1hpBS8iMh/wBkhEVDigmjfq4FzTySgf
+uRjQHhzzBGXB+WhrNmwFXE0INsuQl7wqEGsw0inhpwVawLukKOPSq4lrMrGOI8IJH5uxW175Ias
rstIw0nhybNwx2A4GkLDfIvADzrZzx6/TAHIJWIMzBvX+KfeNrZvT41E0dLC8WEzgQaEr4eIM0zN
acZhGNIuXEu0AESs3G7EF40fEr2bsdbRISBC51xeJs+knUk0Hf37j44g9rksJEmA3S39y8M5dZd0
VDcdezM+vpe+55GbrCMnr2QSU8GbMEolhipS/IvHLBD8sfhTc+b+bgfdHC4lRAC1SxV8aMUHH7li
s0E+FZEz3p9Vb4ZEyG0bX6VXdn6jvR8Sy1qeFjpL5OLOnh45G+cn2GHvGdlACNabGviXsl/2FLDR
L3EQSPpF52K/mON5rHWwEDwmTKEv1LHrGIyEVLI1YEvqlYDNOf2/K2bkF/oLXH4RcjEhWr89rJOh
hr/wlw/KnVVy//HXHVEGRzCQ1ZBb25nex18szoYYG4hQibzbUQavz2ovgO9aQ8fiI8r556+/811r
SKBFfuCFRmu6RkC3HFBG0m7sokcipPFtEFOR0HpZkDlsGn57BoKDoD46VdYfdtOTW+CHA8ABXKL5
H7qsUBk8hgC0CWWX6eY15x4oy9z4YbXuht8ETrU2f0lPbanEn8clQq/inNxcyW7EuW8yBDSQxNt3
HYHvmQVJNTCyTVk8dxFEXmyBpHz+3A6LnbHwT2uvzKcfY8UrUqOYRX/G1x578kaTSubYlBVJQNOZ
/64Pdr9pmmudGjsZXOY1fLHHnCGAF9akVh6sjokVWJJLGRfiC1k2Uk73MoiKye6pCua9t4aJ1TM8
PYg0k2TdsG+nRup5ydAK6fobW+CXtW3W2sNIs4bAX8ghWPSV6YexBv3LRXRItbf12pRWIsv4Oa9m
m6UdabK28jVoP2x+pnNCgqujhFFQL47hjLgd3eSdBy0dpDDJRfwscoZQn8M9HwC3W7y2AQZsoBCg
nZrf/eZlEO+XG2Y8suTz+pK5EDlWNWsqJDHYt4iOE/5NbVhIlrkUyxKVFGCXQXPABb/pscv5yZAK
cpcCA1O6RhJ6yGAHEkICo4HWU6Cnqo6jENs4ky+pEO/m+2MQb/NHwvukLFtrM3+zoC9Ebo8gjepa
pqK0C8r0QL6zu1BxNYIOhsOGSOkaxSzTbz0Of4kKEjFUxGFtqOtW1yGdkUv34IdoPrYoPu0/wljp
xncyxosfnKTFu9uZ4eqRLAYWNYb/SQ7o5H0Fi6mjVnX2nZGb7IoNdIOdamfk6iURklLFvtjmm1YJ
+aMcoaH5UjO9ICc0aL6+RLByzCKcGIL4pZI6OFHHbN3l/C/Ipr4J0s3npJ8uPuE6ZjIOaZdk13Be
Hsc+kwjDTGjCnujLOEElXmYh3X9y4BmyVfGhgE2BVg3B/AYR8HZ++1Gk3yVfw0SINAv4yl0gTuS0
Su7tm8W+HYUgWaDLCppAZTn9Y4zZL04/fv/0nxBNtn5nsbH8eQsE6GK7vpz+IJHSnxcrXqSR38o4
cKrs2RK1SWZTeXKH9XwAzT8RdwVQOvJwwe10mTYgqDo9JNjnlg0RwHxBe4BThCelrpkMRVmGjWes
5zTdZvefaZ9xuUvO9cYA0hIAOAfjEYTsiB/JGS0ooEeesaxfgqKjJAiFtSeFnKhby9j8HOnhWPTg
VXbZKEyYb4jnOZ9jEJHKV+64kBuwlg3RnZnucOKCKB79oEwwtOEfeINnlNByDII5/VptIWZdOJUL
/L3yHPo/WRFNcxPsL6Bp/6J47sEsqGZ6sgcC0wk+2zosQ72XffFQrqYWvX55Stx7/nhFwYCG0tzj
KKrd4bwpLTO5gkqK6cfvBnnzXzRIzj/Dx7n78ws1ymIirFdjvYYC20hOhCy0ze8VyffmABDyfc3n
/aj1iOgGZ+QJUtctDvHmXexJTDl5xu5zkbN+JN8B1CrfqcsIOIly9IYJMJireZSY+SX0QcxgZU7J
olwfqvdlUveIUcFUYUxH5SP0nYva7BwllY2wrPfS0SX0Q5r3JCaheHSsc/QpImY8RC2g4SMq3V+1
sLFmAYCfeoWHnEcBdDL+UFRNO7ZKYTsa4Ld9xYCdF9d60KrHqDjRHHMH8dBoF6DDNRFwSbFbQ4yJ
2hknUOlvQ0ugcspev4obkkXbRS27LmLvKt0e8XFWI0yJRw33wVMTkWs3xSnd2hKGD623Z5tdMfd4
78jGUzCUaTSDB8kbZSvOT/pxxfoPOx3pCFLHQx1hq5aePkGMW5J+5ikR6Yp1dHm4EruXbsRG0uwc
JYU9prBy4AV7xEfj0DO6h3cI5x4xvRt8MZC7aojxSfCU/dkhtwxHSpnkHaAfMz+HlLLnK4X7TrIR
6jci/r5q1evYf3+a2z6om/wXI8y951DhG1I+KAQUytaYduQkswdORZS28H/JPasqefN39WuH+HFG
9ddTk3IX16OTdFIpN12E2FSz3GJgX/D7I1UfyvcHMf46uXTk22l+n2Z5AXVJ+FzPLoe7EehdtXCQ
FGXM4pa8eLTlsh0nYpQ26M6A7H+TLeA1l18hcRaNsm1rhB5+1bv0sGDZIr6EtJ+xe5HdqtyPKNWd
fn1MJgQmnw/Yw6ZpkpSWLa21PG177W1Kn0mmXSCU5pFJtS2hrq1rvUrgbLtSDTemjJTaGgKD1pZv
IqcLrXOpOK7jD92NpzJT1Ai5+07pn0MuuXdS1bgNd6vu9vqspOtmznWlTVbkoz72W6oyRA/duOhB
J3bMxgEfZIQ9IhAVH4xSQtFCP74TAXO3MD6zrLVlN0Z5EkQNBOh4NlYn3ptgszLtYuTjLfsSBha9
ow2ZYWXmueQtGdYz6Gk6bZeYUrSqTlG8QQnYSf5omiCn8W7TuwDqHrkCMAsvSTlqfA+sp9oYgPX5
3hM/UzNDCYUvS9FMTeD3enTQcGeua6jmoTbDWX8osc+8faF7UWvfP0iDoQc9LM8XPinekGpe3mFI
9TZZ4PgrFEk5ACEYACJHH2w4yIOSFLNuuEsn003leCvXfGaJoh+7OIUVPrvwZfIeac5SiNUYWnDs
WiEG1G+vqzNqbk4KDSKaa89D2JCuxVbfoJpSnjKHoEAMUJfpUzfzLR5hyL0gCHYKnJAF0HK14i5H
Us5yho8Gq1kk1LSS4V4HanOagZcsOoTW6kpmpo5tCAVPXDovmhyk3AhZ4gM+dertesJC7xblgMEs
6SbcAzPj3XBC0pE3aFYSnYJM15Pfx9cwOScF/HlTNOeLIE9aJsuQnhPgniuqqQMZOLJCwrHhtBcg
k630bnV8VE9JZM8bdBJYMepgpwh7wosjGlu/CO3VNQR3WlJU9jpd/LfN/kld9dzRhiOfgizVAEkI
FMiCoyjXYIrG9xSi6zmKIWe2CxAP8kXYrrrmfNh7Ae6NLIJ3mtkBR/BANo9qbpBhDq6jjnNFxIHT
S4/L4c92v0AxY+qBWZXvJYEvtPo+qoSrTjKOLcQogoGR0jso3JN3e82nentyC6m1CLLlcTGcaUYp
7rCSH/u9cGdyTQ5ZsvVlvNNBD+z4xP7myHG8Mx19DqTK4BHKztY+qBiObrP0JTo6KNutlgD2Zck+
zOc8xAL/vtOPcNgDCZWFsIu7t2JwVgS9jVKxK8bTRoCUyxsDXCXuSbNSMjxV1K2sJbXGoAXBMwBA
lzYU3uQTKzDmfs+MK8Y3q0uWOAhNlW3j8xQ852xMK7sVytvMnq2bBOzJz7Oturuy5eQeAOmf2XPK
CoMhwydP4saxavUF7VTRUK41azA+pS23X+IdrKQG7urnqeSbp5NPQ+wcRmthYhCpv8teciKaCSfg
ym5+CVSr8Junalj+OVoDiiyf3H6OWaFOsbemiJS4w/T6YHu12cNJ03nphL8Jv29u9MdX2rSS172A
dxHz1AeJ5ZSFjwih/5qlJ5I1b/PkEMzBaAX8pTqLs8bGkjTLOPfXwkHrDyEh+X8ob0B8ikK8N1Nj
QgWUhqxbetULoLZy9LlG7kVd/wU21SaGwLUW9NsJV9d3pqXfzjV77MnNn3adjJIvCkcDZH2Cdkzr
pLBE7phe8m/dZnqG/lO1RGnJTK/pk8zZzzX/jWnM6/oCyxs1SZyfoevO4GHzeZ83AkMD+ZXN1aAY
0pBAIHQTy0+srTIn7rjwn6JMe2PioSOfrCNRDthYRQRYMfOccDK6ReTgKNBG8rfcSdx4yAKM4t0l
j87ZMiCI32yI2TFpXavXh0FOwSB58CAv3yUClGs+p1f2j+PrqYUihmfRtpsBxCzWIpgZj07z/gEf
lgT97LqUCOpIv/zRIXcMIUNkp49U1PaNRKBQk06jCLXGs+tLRmqSSwfzSWZn158JTDqCHKorJ/fM
WPbzDIirINtizrh+gjbw5JHljmG1IZXMlwHbg11cXSLFjLtorgCIWCr5k0OOZqs+TJ1PGUms+TBx
rt/KMCy82oFbf2wAUxQ8xeOhGd3tGclLm1Gnuq7Hy2LA48taGixM1XR0/vw8538lHVDpMvsYzPYI
UQK7JgnyYnPpUlP5NWqUbv5Q/nPOurxQhGGpx+mJdag1V/WXpdR6nCfuoRK06Q4WODuHxbPqbOkq
rTdZ8WW9XF7h3ChnWoa5KNMBMZevhh69Z75owuFkmLMFstDXrhTTHp5CoqDP46dspFgLkUy+kJrm
wjYKYRNBdgTxw6uPNRdMeFHfPrpnWBjPIZOegWLs9sswtYE/um827ibsWOzCQzLM6fO9ygTfyN+v
jUH7Tiz66xim8tYUi1NsyKKr8jbWtmm2SPVgYNJyZm9PUbP4I+Yr93XQO5tHO30YQwRqGcjkQFh6
/QIaacaf1zpQkwvfeUZd73Nf5pr5Cax96nCl+cb1VfK5sR+P0GmRqUADix2O7YKXZ5EJBaWWpWbh
LDlILzeCS8KQdrsOaByFdcw9nyOSgo8ocDPFDk+VD3AJMMYu73VrB+3UHHsyghCafYb5Lx94nOc2
eaNtTJYKegjsP3SNy77AnTCrgo5kjGh0/bU9oj5BB0EsfQPbnKGXXQ5tLQtVYpRzWpxfI+azfGIx
EWakxnVsJyjRLjC3COf/q7NG4qyFZ/O/z8C7Y3qejSN1c9xm90eCO+UJXW2tXM3WAOkom8XThyGn
VGyp+LulL/65XIfEnBxZilMxeXT7pptZT15+9uLJ4OrxlQmjRQ91Ni2XuAatMGH/Y1N/7tYp12f5
lLkC28rMl17G2NSZFMLRdLOF0SJX7nioA6k2hYF1aVworP0Fr3C5/3qZ8YUyRR+5wQ4tw/rH5jiJ
f62F6dgQVTGsx08ZVDZvjUXvCIuowiYjsiPs6RQYHX8ZsEtqTofuSLdd+43GGTCFUsnRjrSTiQ8B
wjvf/ahKYT2uNY6avrFn9rOvqV2OLQV83AxHlt+sKSTOLWsWJfYn3tqufyQti3nBx5FuzM54rjj4
3ZqAE5qDfr31lqiNBtPTbEuaP5Z4p1qg5m8ayV3iWTYnnN91Y6NC431j3MLOHVeL1HluzZtipmG+
n3BVIkyRAGWyaw4pOBzhChKnOv4I9ieA9fy19ED8DFVoD9mQUwxrbwfu2UVDUpz2R7SLu/LkujVp
PCYiuVkiHIKn1HXskJd2yELm7Nvb7Ynvhe1VunFrjPcgUkKn3BYWftEwKIMX1dEZpMwvWB2fHBg4
yKIW555hnDyZ4SoLi92Xgama/r2EQOJdiUnY6ueV61lU7bityOd7434ykNl4S8JK2tkzZiOB/N2Y
AnuNICNRwza840OtCAifdD2t/9yVCtV8L3i8UGkrAvj8GkwvxqczE9uTSH5uYLJ0vuNmD7dbQJip
cwhYn310wfXC+4lwQwuI4ySkYIwG5l5KXeAHd/tG3T21oQqkttE0YSRojiJ5VT048G7dmSkFpYZ/
vzOcegmbZMhY0UZVN6R9/WRvPvO7cQUs7J+TMFBYG1wU4JXzjrNYVyY8K2cd52KtSPKJd2IX54dy
ihxowjbqqCWPRgcPbgrvqaLlMJunwH3hHbjACnVMtt8rzlfZjTClqmRjMd8w/s6WWTtdoLz+Yq2a
aRXP+X43xMgOLjmqETekNMMOCD7qulBh4p4N0YJkfW6ENjLSvfFKwM/doxUiaztmY1DG6hinvEdw
3lNz8gfJeLAVUFqX9PYkPxS24ruCXAru38F3iuW/X/qw9IvT0mHwSYMW4IGWEarEHyuNzrthHqrS
V15aezfm5KXmZk7mrIfDECbEgdQlwgrJChShBKJSgdGpSMzfHhL6xiANP44kZkxPeMRfzSVUFzqt
ALSK7MZ+tSbmjY4kcijrx8Pau93Pezl19EKTdD9FMA57Qs4FOMyom/63MlOLm93w/H0huMBsRXIg
yhwPxFkwSP/4u3J8VjPqq/sOPPI/cyxTW8159P3cbk7fcn5GHRY7E/YdesUcItsV7ctAoB1rmDs5
mt167bDX4YcRJ60gEUui7FoEUZ7pwAvJ3P3HvJRV5Zez2h5126HZwggMbd86LWmLYnzH3k1oGJtG
EvcZzNY0Z3jR3OBTOrZcslsIC/IIy+S96cWwzjOcpHNSKDZKwOoiBbTnE1BbSnB3JrkRn+JmYa5M
56Du/p8BiGWyNs0zDUhogbgyyQTLzGaObYzZ7mSP/nuA3ija1GNMmgio1stLk96N3kfmY+G0PDjk
8Rjw63ykRh5FDQR5p+vrjxDbqimtqukPY0EXdByTKXgYa1CP3amaJMenOpvynPVxwyI3CXHoK0Ta
O5SRHAiTl6xYQgBBw9AYdw6mivkfrfEvzkrpkvZYP4lB/sskSUmYbJS4eve6P80khOONKrzwTmH5
lnB6JUwoF7UFHimrXmnsg1XFsHxWJ6f+lvrnjJnFLNNdZiSAtxUpf41cB9MmSoswVNsvlJtA32dJ
ecD5WtSx3Y5fBfhpsTKnRDKlNle0oJX1nUWAU8WZRyb2luSYF1yvuQbWz9xzgg2w0uxLNNhOpMFm
nODh4XJkfvgJI279CtJpNU44Pw2BXKWgDjBKDV3PfEAro61MmSiYrPCKJ/qnzgJNLUHbR0yO/QjL
51hych+Ds5MFMETKnzyhKgFQoEwN2JjFKG7q5SI4Mq1xlcii12LxD9jFxUdwBQMtU55yvl6ubTVt
VpfpT2TfbKvq+wpgTMEMXsDOMqrL6VimFmNz0HG5sSBJRBxpkld2Rdtjkhm9pKp6KVfUbFZ++96k
zNKkPrHGfHDLr1PEWHr6FQAchCU36RtFrS1RSk5w6EQIKuG5iAT+uu9zAsOV0K95n8Ss9lDRr2UV
HkNET5tPMvadntF7z7ORq8wIFcuJuLF1pkZLbFCxDcEbsqkwhS/f4qTLVFZV/ntXwliRZ4qYq9+N
k+zeoqSFyJ8wi+GxybwGfkvVdSaq94GJ7/buuFaXRQ9b4UtsBmBWsIyCz69YlGvPf5KhJC429T9y
ufGAGbjvWqnGSjga1AK6o2UsYJJ9Uu5mAcRz0/4bhamzI8y7imB1STpCE0fMQKUuzxwqfz9Zy0ff
7Sl5k0VtZIQJijOBbMHKpKUulWo8D3Ih95pTUdJqAFmMEG+hi8bX/d6cYUYMvg8ZrwO7aKDBr3OO
AldIPfYbhsspM4sCj8ArXEebshW+f8zB34E1LEPRbM+F3CIsey9MbFP2Bp2Tv61IboU8RDXC/Dys
ehUkDhNWA97SiXUcHSnKaiaB9UCufOk0ZeGJg+So8vWmZehLWoI3aAWM74vIt9zbWziXUGXc7lUj
xPilpl6Lg2AErCFfPFsb2I0LMyR8KH/RGhsprjDGopMg/PcOsAQgXTub8CQ1BItyKVVrdzxUVsm1
TsXlTWZ6E/34FVd15Qq5MpKawpt78qDlIs4YCmAoDtZd5b/JODFHXqnAO+z2hfM5uKO0h1kPZrmU
b/wSAdZdl48QRsGgo7hlHh0bJuaIsquYJtCQV/lbuPP5yFBBsSuVX/Ex3hYu0km5R1kN6aJMsjeD
JwT6QwEXPT2pTckVWJg7aOxZI4ZIj1BoY0sYpvkVi63mJOFwofDVXR8WQbCohg3oK7l/4P8yQo3q
rAaoSS2hWX7qotp3+XlQ6wPE/BTEvqoZT6/twcttHlKDqgIzt5fBtWaf+48TpgvfKcQS8X9X/+TH
qcGQGde0/HF20A+JGrtii2c+gLQZHsuKmaXVz3r4WKgbp5VWucU+Dned6Z2IXMiYthyTZm+AsSK8
eB5xhohSqiN4uBProC6fdhgBYTIzFCwKHkO8UcR3w2kor84SXkTIrE4rV4b7tjkbSf3OtGYZoiHO
tqVqY5iQDIT97/+PefARY/a9fvjEhKH33USsZCWxhSPaOOL6bIoaJtfyXIK3FtcNpr1vXQkClSZm
Fqxg1y6lkHA50UkVi5xBWGDQ5ZmR2EjQ42ny4edb+n6lmb9ocUQL69R9RtGkq0eV5w2gV5hZl3Kl
Mn/ENjbc12j9X2/WLP+QUZtLVt1ezbzDcHyP8RJjzEQI0pco5I76G2UdEKGQUASnSXTWe+BYWAoP
+N9kF1tSdmrWBcLJl1zZn4Rss+Z0IdvJYIHIf775PSRSLZOgTJelghR7s9NbMV+ifPbAvCCevYdD
NR9q17sH496kFqPr0F/Wz8Rk03qBxvLS1je/tU2vEEoacUwQpM8VPi5fkC+Ii44sGwAOcCixhoFW
peG7LI3cmkaTqtdgTZQ8E2tyHMkrpnXj/sLsDW+80wBUu2WDktCrkB20fd45u5QRX9wLXjNBbnWn
GHptPqCOmz9jv52y+BICjkjTXqcP65hGyhQLjY9dG4ltvGK2qHgCZh8E2OGu0g2Tog8aDutFhO9E
CocQqsL2FgEOdQKUQKP4aVevFtmrkR2VzP5h7whFQXlT4LLHz0MWfIIofhfFdTolFwYcvQNfrY+J
Q7YfpENTz/WvcMyuk6kVW6TLbcXAeqUxGLWX3bYGIObG2fgGy3z4tfSwU550QoXzi1UCkUtzLhXj
aGUK1XS5LfrcSzRsv5nUfIRmii7MI9WPkEE4w9uwRw0DvRo014hKSN8mXojlgYMuP9KEEFdNOiyV
jVGYtIc+GXwtmgAdNZv5iXS/cMfzNdne22j3/kleCybwk62OgIM7mEY15dVA8T/9sypuZz8ImL7A
+cgiP6t0ww3FXzo1OyJAAMY3BXzjZ5A0b1KQVT6muVG/Fc/1coFaNJTK8gtojv7jXl8lpNFzylbS
PFDh1d2pMNexpxXH7/EkjPi7E2RK4o99kEfWPlKvns1Hf2ROOVR7U7GzICa5GJpmM6rxvNqMOE15
RKaCiU9HfDQcoB2v7yEZ+TxbtUKTKVHy+SCczrPLd7ysSnOFRja1552ispKKDaDUT+d7N91QUpZW
uA41nnaTxpQeQRmACrVnlgCB0wDrzynLmJLFLSQogcvn8NlnBO+y+WmtIfzyVoTUxtRuW8VWezTL
euEr00b4qRVA1kC6rB2ok6218wUirub76XkKfMmcy2XrczqlhyY9dYPrE3HfeRnKk29Wn9Urk/U5
cNpAPzpWceC9UclH1z3KT6I++G39u4FX2xWoTjXUcZgBbk1srQJHmcPNXqAWQQdiN+Ifz9StUrEO
36f8aCylw3Gm+A7LNcZzJG9QdgptQb8VpLZrIaexex4Wefm/CUyNKAItFznusNqNxnxJ5Z+hgr3y
IljJdZWARInHFE2lOd7c1Q+d69CKv/zCaueDYhjPO4+dFGsMGuvoYQxPkjZeoA1f1RXL84dDIMml
AgiCRYvcoNHDEbWmIJPGCMUnfo+OFtXa5BpW5iZRF0e+yZaggXc+X1k+/duSUuE3GMpxMxeC34gO
4k1VxMdsza6clpztWKNbMbM+wGEABZPkLQG8WBV6GAYvqHqYswuvP3LZJ/KxMl3OzU90YDDa2V5T
APOFqidMfXss6uaPzlRD12/fKu7J8k2suS332H0yy77XFzF7uyoG0byOl/F7p03vwWyNkzaVPxbB
de4DT/Nltb/zvmSD3K2YeL7edJ474yRi7V4cVheBU55sUPf0x46NTMF1tolLfKjpjzbyphWt7uOF
AIpqs9MthhB45ykZGaAWVcdqXbVwaeTlQENlTvrjVZJRx96GBq4SeOb7Pc/pjZL8WWptooVqhhm9
xcTG2qLCv0W3MdeXUemcy7dHBZ4iCBOOtIsO3DCuM3+fdx6VdXgrPGBkodsBIM5C2CVV1c5uvcur
nM2mekHVDZoUruYFj4vQXML4GdSRUgfhGGAn3LxmQwBpHYamq2YXlV0/8lLZxOeC5zwx1HaPHqkC
fXiGwv6BZRziHZH/Yf7FL9CTud/JQZjF0WMyZXAWJA5XUDx0sSss1Kin9Y4zrGwCqmTt2VHjEcp0
y86GRMmLxC3crkDaXWfiDrxZb6GFRg92NOHsK4bB9PpWfwMg8f2BQ2LZy37+mIY0pEqF6mc47Nj2
f8iwklLJ6yDa74LvlPQ9KX6jEXzNclyfufEIhW8ApqTrKUD5N1FrpuNDO1B6ZY3DrjYlqrWCEx2T
ta1qLnAjJ/7sz6DDRU9ipqbgDOplkTv4HDHp8wOd8G0Q3Gdb2rvqjp7V2Pk1y2WchBUGbhd60KYa
ObzcHn1NmOiipUNUhB65NYJ0WWZYxYju+0sI72Xbc168aC6vOvc0vNIUFyMvVpZjaovhyaIf/EqK
3kuyhGHIm7RSsVnjus4c3BzW6UVhc6pbwUOGOOj6j1RY3oJmsQgzLf3sqpA+/AQ0UfrfQyiNHQ/1
qMgOBGyTGdIv1MRmpJL/OwtbUi5ycby5CJY2dUNOHL8awdsuNk8BH1LgOs/CIXKzxF6RuFXGET19
kt3uniZpAlijKEUkg3YKyMic9ktAqs4rO5c9k6RiJYn4/2IQSl0LQc+2FX0XyHpNgcQGrv6a6VG5
E34i4Xxgv0YGEKcdCKIhIpkAV7MZ1NGUf5qNzeO1VjnA8x/aHM4qRX+brBGZDZAN1UJG7qOaIUDC
yoV5YNKfaLmJTr5Cxy7zqu//eTI+FBTWmI4fw5wT03iLBumKwtYAL5YMtBL9x4PLY13YldKuI5D7
5p7xWkgZHJLVi2fmH36O3FU9tUgZx8ec7wJ+GAz/zroCNCLYqN61dEzydIVybqCwAeKYaMm8jfVx
5HZBfEtkEDgbxKyAh0EHVHe+hOpN7Fxw0cceGgR2GB29hrrK9XZ9vh8g1mJkeAq0pglf7hEs7EFu
NWhNmqjNmi20K9IVqnhJn2pjdnhtK1aqHj8cdfYgLcVGx601gc7/u2OMaToMaIMTttJzDNs0WEv0
CfcK2q6Y7oueq2zoa1r+RPPfu8nNF/A8IFWh+KH0UTTstRJ0BAF36+capZGVNV+SeYjie4WOIjrt
yCmrmeajiivLYeVwjX6mfb1qBjZlebkWgNYJ5tV/hURK1+yiv7oO2/p5rPtDqXOPH2hLwwK+zz/2
ZxTSW8Aet8KmfvKUR10dbZ60SoLxEM12t8xY9T+gmcgozgM+TTlcxog0nqSqyZzKpH+OVHopXESb
9D5zdR+iIA262h1AX2RNZdUr5RXBdEi/1oeQz1WU/7VLCDzxTf7678M3EghI9tsCjkV63QnvwuIm
LyDl5t/5rPymETG/O5h7iEwZQJ/ThrCHN1aetuJPJpXfvgR1+CVutr9I+sHgdqBCWjnHiuISN5hn
+RdJy5bSyguLblNkZdCPYrm0/C1W8cWMlt+UwmjeF5ejSNgPP+XBf38MHzX8A7kmLtS35OR3Vxi6
QtyrIow3+/bqgo0p1oY49GWcadfgub0hEfU6bqtIdngkmMHUEH/inBADqb/zQVACOOzfQLca0Dt8
ZdGA6FxHBYHy+0OzxoEm5pzqoyqrvz+elqEcuDSbypxRNEpUWsI+QmacEDFYatmR/IYzqR2pPFoa
nM01oUEXWj6Ulm7noV3Km/J+7itaRoYL+hMDnYUChkhGVKrgw1UcjarEuzEqTvRPsTQAQVxweiRK
AR53xpboSjzKx+vOIjOnfgffCU+ZIdvtQQ/9PDkcYDYxKrMn43lTsMMMjYO43ZbPlwKuANoRdD3I
ylJsCLavNwkcTdBpFSUzQKaSIUmF1tar+NBqhfqhoKrpmhVxpDM8POBS5O8Ye7FbG6l21DSgy2n/
FxxSgclro5LVwh6TQrsr+mKbFeZlgDqV7OffR24tufCS7sPBEsONNW5QZHCwRw3DNdpxnlESUf/Z
Ih8iW0FZZX7P3JNzQMq+yNhxxF2P1m5h40GHuJZT4olMx/FzTlBwvhNUuLs4EnHU40rDijgVGGS+
H0sef6JY4ctSAdE3q2M6s9xUHj13D57G1G7DNEYnL7dOvL7OMwjmJhfr+5oTFZjtafXTfMEEpPs3
SFV1y4B0jikkBljEUkCj+q3sf8GHCO7fNoNNdRbfqtihAp+a8DY1xdTaclzW5iXeXeDr/y6U9696
HaC7kAPPNYDAuAbZiVyVxuG4sgJR5I4XjZbEFUPTxrQoFmLtNu/Sqp30lhCSLtE6fIeHUFXbW1pQ
EBdhwKhXtvv/sHzhMn7d2UgiOF49P0hPjQAsjw/qO3SNsL2/7GCDTNXKyTRb3QaYe8r2kmy+LoFT
awIxY6C+nw1Buan78ijZl16rYA3wP04jFbiETPDvAnTs/bqSwmPqOZut0+F5xtvYVTzhEOJqbXCx
52nGI78L1KGziFv11Dh59EHQnqWLksS136LPYIktUG8AwRXV6sFm4VgKrjuFHacS+ZoM2jBlXSQv
PYh4C+pd8OEdPr3a1CyrzyXh9owklk+dD6YCrTkk3viK2NQX+MQysxyboBkhx/KSjiaYK7/x+yFN
WUkq801sMzc6HzbRRRp8RACnmn3E4jlHgxT+3OvnE/m0Vk3cE4lQCM/1hv+h1nt9MbrAl6y20QSW
BNPSZwgwfzGGoCe6dB3eD0sxehSACsokI9ciKocR/cIlGZStxIsWqLIO2Z9uQEb+4ahnZvLk+m4k
s2GqLwrcff0Hkpmjel0hm6zGyGEE+usaJApwQqZvkJrsR0wzV86ivdJP+17gfWHRy/aylOEFOtsy
JL/KAZn2lv9Y7FvjPg3t40uDGEYyDB7Y/DHmdvCRKQEQ17Q2/glG+0h60SlzprfWrOZHIuFDJbJ2
nTCYjgSie41A+/S/xSwGGD5I4VmqmzddRlDp1X2mKqgGwGL/aDfd1Yge57X0zjCu7HX9OUEdtGul
LPIIFHoxM7o8XBxJzqFGbi1OkiBu10C3NR9xBQsK/gmrZ/p0pU4tOS+PTXo1BmFYgDFUAW1d1MhQ
WvyTbinbO100kaH86qFWLm0ky+Av9h0ZvAN9NvUb50bi3NJPqdThCiwpL51HQj23TTn9IvErIAld
8J6H98HNtkv7ftluOPLy+3jYP+LVSZ4wBpuRjA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
