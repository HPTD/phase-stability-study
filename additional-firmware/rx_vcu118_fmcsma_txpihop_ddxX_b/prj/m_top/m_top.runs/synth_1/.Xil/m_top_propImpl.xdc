set_property SRC_FILE_INFO {cfile:C:/Users/eorzes/cernbox/git/rx_vcu118_fmcsma_txpihop_ddxX_b/src/xdc/m_top.xdc rfile:../../../../../src/xdc/m_top.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:2 order:LATE scoped_inst:DDMTD_inst/fanout_buf_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:3 order:LATE scoped_inst:gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:4 order:LATE scoped_inst:hop_gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:5 order:LATE scoped_inst:sys_clk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AY24 [get_ports clk_125_p]
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AY23 [get_ports clk_125_n]
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AG8 [get_ports gtfanout_in_n]
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AG9 [get_ports gtfanout_in_p]
set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R32 [get_ports ddmtdclk_in_p]
set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN P32 [get_ports ddmtdclk_in_n]
set_property src_info {type:XDC file:1 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AK38 [get_ports gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P
set_property src_info {type:XDC file:1 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AK39 [get_ports gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N
set_property src_info {type:XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AN45 [get_ports gt_rx_p] ;# FMCP_HSPC_DP1_M2C_P
set_property src_info {type:XDC file:1 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AN46 [get_ports gt_rx_n] ;# FMCP_HSPC_DP1_M2C_N
set_property src_info {type:XDC file:1 line:23 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AP42 [get_ports gt_tx_p] ;# FMCP_HSPC_DP1_C2M_P
set_property src_info {type:XDC file:1 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AP43 [get_ports gt_tx_n] ;# FMCP_HSPC_DP1_C2M_N
set_property src_info {type:XDC file:1 line:30 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BC21 [get_ports si5328_en]
set_property src_info {type:XDC file:1 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BE24 [get_ports firefly_en]
set_property src_info {type:XDC file:1 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AH39 [get_ports hop_gt_refclk_n] ;#FMCP_HSPC_GBT1_0_P  #### CPLL of Quad 121 is used by HOP_GT - MGTREFCLK1
set_property src_info {type:XDC file:1 line:50 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AH38 [get_ports hop_gt_refclk_p] ;#FMCP_HSPC_GBT1_0_N
set_property src_info {type:XDC file:1 line:51 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR45 [get_ports hop_gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
set_property src_info {type:XDC file:1 line:52 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR46 [get_ports hop_gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
set_property src_info {type:XDC file:1 line:53 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AT42 [get_ports hop_gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
set_property src_info {type:XDC file:1 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AT43 [get_ports hop_gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N
set_property src_info {type:XDC file:1 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN P36 [get_ports rxUserClk_n] ;#FMCP_HSPC_CLK1_M2C_N
set_property src_info {type:XDC file:1 line:60 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN P35 [get_ports rxUserClk_p] ;#FMCP_HSPC_CLK1_M2C_P
set_property src_info {type:XDC file:1 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AT22 [get_ports eth_gtrefclk_p]
set_property src_info {type:XDC file:1 line:74 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AU22 [get_ports eth_gtrefclk_n]
set_property src_info {type:XDC file:1 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR23 [get_ports phy_mdio]
set_property src_info {type:XDC file:1 line:76 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AV23 [get_ports phy_mdc]
set_property src_info {type:XDC file:1 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BA21 [get_ports phy_resetb]
set_property src_info {type:XDC file:1 line:78 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR24 [get_ports phy_pwdn]
set_property src_info {type:XDC file:1 line:90 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AU24 [get_ports rxp_eth_sfp]
set_property src_info {type:XDC file:1 line:91 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AV24 [get_ports rxn_eth_sfp]
set_property src_info {type:XDC file:1 line:92 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AU21 [get_ports txp_eth_sfp]
set_property src_info {type:XDC file:1 line:93 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AV21 [get_ports txn_eth_sfp]
set_property src_info {type:XDC file:1 line:100 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BF9 [get_ports sda]
set_property src_info {type:XDC file:1 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN BF10 [get_ports scl]
set_property src_info {type:XDC file:1 line:106 export:INPUT save:INPUT read:READ} [current_design]
set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
set_property src_info {type:XDC file:1 line:109 export:INPUT save:INPUT read:READ} [current_design]
set_property RX_PROGDIV_CFG 20 [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
set_property src_info {type:XDC file:1 line:134 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_property src_info {type:XDC file:1 line:135 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/txoutclk_out[0]}]]
set_property src_info {type:XDC file:1 line:136 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_property src_info {type:XDC file:1 line:137 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/txoutclk_out[0]}]]
set_property src_info {type:XDC file:1 line:153 export:INPUT save:INPUT read:READ} [current_design]
create_pblock pblock_ddmtd
add_cells_to_pblock [get_pblocks pblock_ddmtd] [get_cells -quiet [list DDMTD_inst/ddmtd]]
resize_pblock [get_pblocks pblock_ddmtd] -add {CLOCKREGION_X0Y7:CLOCKREGION_X0Y7}
set_property src_info {type:XDC file:1 line:166 export:INPUT save:INPUT read:READ} [current_design]
create_pblock pblock_logic
add_cells_to_pblock [get_pblocks pblock_logic] [get_cells -quiet [list AXI_inst ila_rx_inst temp_sense vio_ali_en_inst vio_hop_inst vio_hop_tx_inst vio_hoptx_aligner vio_rx_inst vio_sys_inst]]
resize_pblock [get_pblocks pblock_logic] -add {CLOCKREGION_X3Y0:CLOCKREGION_X5Y6}
current_instance DDMTD_inst/fanout_buf_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:2 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:3 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance hop_gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:4 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance sys_clk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:5 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
