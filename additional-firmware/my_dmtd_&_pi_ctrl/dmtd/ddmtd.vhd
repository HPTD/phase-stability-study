library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity ddmtd is
  generic (
    F_IN   : real := 240.000e6;
    F_DMTD : real := 239.990e6
    );
  port ( 
    rst         : in  std_logic;
    clk_sys     : in  std_logic;
    clk_a_in    : in  std_logic;
    clk_b_in    : in  std_logic;
    clk_dmtd_in : in  std_logic;
    meas_out    : out std_logic_vector(15 downto 0)
    );
end ddmtd;

architecture Behavioral of ddmtd is

    constant N : integer := integer(real(1.0)/real(real(F_IN/F_DMTD)-real(1.0)));

    signal pulse_a   : std_logic := '0';
    signal pulse_b   : std_logic := '0';

    signal meas_dmtd : std_logic_vector(15 downto 0) := x"0000";
    
    --dbg
    signal q_a     : std_logic := '0';
    signal q_b     : std_logic := '0';
    signal q_cln_a : std_logic := '0';
    signal q_cln_b : std_logic := '0';
    signal qtp     : std_logic_vector(15 downto 0);
            
begin

    period_ampli_a : entity work.period_amplifier
      generic map (
        F_IN   => F_IN,
        F_DMTD => F_DMTD
        )
      port map ( 
        rst       =>  rst,
        clk_in    =>  clk_a_in,
        clk_dmtd  =>  clk_dmtd_in,
        pulse_out =>  pulse_a,
        q_o     => q_a,
        q_cln_o => q_cln_a,
        QTP     => open
        );

    period_ampli_b : entity work.period_amplifier
      generic map (
        F_IN   => F_IN,
        F_DMTD => F_DMTD
        )
      port map ( 
        rst       =>  rst,
        clk_in    =>  clk_b_in,
        clk_dmtd  =>  clk_dmtd_in,
        pulse_out =>  pulse_b,
        q_o     => q_b,
        q_cln_o => q_cln_b,
        QTP     => qtp
        );

    phase_meas: entity work.phase_meas_FSM
      generic map ( N => N )
      port map ( 
        rst             => rst,
        clk_dmtd        => clk_dmtd_in,
        pulse_a_in      => pulse_a,
        pulse_b_in      => pulse_b,
        a_minus_b_phase => meas_dmtd
        );

    bus_synch_dmtd_to_axi: entity work.bus_synch
      generic map ( NBITS => 16 ) 
      port map (
        clk            => clk_sys,
        clk_start      => clk_dmtd_in,
        data_asynch_in => meas_dmtd, 
        data_synch_out => meas_out
      ); 
      
    ila_ddmtd : entity work.ila_dmtd
      port map (
	    clk => clk_dmtd_in,
	    probe0(0) => q_a, 
        probe1(0) => q_b, 
        probe2(0) => q_cln_a, 
        probe3(0) => q_cln_b, 
        probe4(0) => pulse_a, 
        probe5(0) => pulse_b, 
        probe6    => qtp, 
        probe7 => "00",
        probe8 => meas_dmtd
);

end Behavioral;
