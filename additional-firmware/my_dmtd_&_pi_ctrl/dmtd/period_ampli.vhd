library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity period_amplifier is
  generic (
    F_IN   : real := 240.000e6;
    F_DMTD : real := 239.990e6
    );
  port ( 
    rst       : in  std_logic;
    clk_in    : in  std_logic;
    clk_dmtd  : in  std_logic;
    pulse_out : out std_logic;
    --dbg
    q_o     : out std_logic;
    q_cln_o : out std_logic;
    QTP     : out std_logic_vector(15 downto 0)
    );
end period_amplifier;

architecture Behavioral of period_amplifier is

    constant N              : integer := integer(real(1.0)/real(real(F_IN/F_DMTD)-real(1.0)));
    constant QUARTER_PERIOD : integer := integer(real(N+1)*real(F_DMTD/F_IN)/real(4.0));

    type state_t is (IDLE, WAIT_RISE, WAIT_STABLE, WAIT_FALL);
    attribute ENUM_ENCODING: string;
    attribute ENUM_ENCODING of state_t: type is "00 01 10";
    signal state : state_t := IDLE;

    signal q     : std_logic := '0';
    signal q_cln : std_logic := '0';
    signal count : integer range 0 to QUARTER_PERIOD := 0;
        
begin

    q_o <= q;
    q_cln_o <= q_cln;
    QTP <= std_logic_vector(to_unsigned(QUARTER_PERIOD, 16));

    sampler: process(clk_dmtd) begin
        if rising_edge(clk_dmtd) then
            if rst = '1' then
                q <= '0';
            else
                q <= clk_in;
            end if;
        end if;
    end process;

    deglitcher_FSM: process(clk_dmtd) begin
        if rising_edge(clk_dmtd) then
            if rst = '1' then
                state <= IDLE;
                count <=  0 ;
                q_cln <= '0';
            else
                
                case(state) is

                    when IDLE =>
                        count <=  0 ;
                        q_cln <= '0';
                        state <= WAIT_RISE;

                    when WAIT_RISE =>
                        if q = '1' then
                            q_cln <= '1';
                            state <= WAIT_STABLE;
                        end if;

                    when WAIT_STABLE =>
                        if count = QUARTER_PERIOD-1 then 
                            count <= 0;
                            state <= WAIT_FALL when q_cln = '1' else WAIT_RISE;
                        else 
                            count <= count + 1;
                        end if;

                    when WAIT_FALL =>
                        if q = '0' then
                            q_cln <= '0';
                            state <= WAIT_STABLE;
                        end if;

                end case;

            end if;
        end if;
    end process;

    pulse: entity work.monostable
      port map(
        rst       => rst,
        clk       => clk_dmtd,
        data_in   => q_cln,
        pulse_out => pulse_out
      );

end Behavioral;
