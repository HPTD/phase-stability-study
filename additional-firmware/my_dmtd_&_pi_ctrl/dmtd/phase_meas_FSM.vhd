library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity phase_meas_FSM is
  generic (
    N : positive
    );
  port ( 
    rst             : in  std_logic;
    clk_dmtd        : in  std_logic;
    pulse_a_in      : in  std_logic;
    pulse_b_in      : in  std_logic;
    a_minus_b_phase : out std_logic_vector(15 downto 0)
    );
end phase_meas_FSM;

architecture Behavioral of phase_meas_FSM is

    type state_t is (IDLE, WAIT_P_B, COUNT_UNTIL_P_A);
    attribute ENUM_ENCODING: string;
    attribute ENUM_ENCODING of state_t: type is "00 01 10";
    signal state : state_t := IDLE;

    signal count : integer range 0 to 2*(N+1) := 0;
        
begin

    FSM : process(clk_dmtd) begin
        if rising_edge(clk_dmtd) then
            if rst = '1' then
                state <= IDLE;
                count <= 0;
                a_minus_b_phase <= x"0000";
            else
                case(state) is 

                    when IDLE =>
                        state <= WAIT_P_B;
                        count <= 0;

                    when WAIT_P_B =>
                        if pulse_b_in = '1' then
                            state <= COUNT_UNTIL_P_A;
                        end if;

                    when COUNT_UNTIL_P_A =>
                        if pulse_a_in = '1' then
                            state <= IDLE;
                            a_minus_b_phase <= std_logic_vector(to_unsigned(count, 16));
                        else
                            count <= count + 1;
                        end if;

                end case;
            end if;
        end if;
    end process;

end Behavioral;
