set_property SRC_FILE_INFO {cfile:C:/Users/eorzes/cernbox/git/cpll_ex/cpll_ex.srcs/constrs_1/new/const.xdc rfile:../../../cpll_ex.srcs/constrs_1/new/const.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:2 order:LATE scoped_inst:hop_gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AY24 [get_ports clk_125_p]
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AY23 [get_ports clk_125_n]
set_property src_info {type:XDC file:1 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AK38 [get_ports gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P  #### CPLL of Quad 121 is used by HOP_GT - MGTREFCLK0
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AK39 [get_ports gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR45 [get_ports gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AR46 [get_ports gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AT42 [get_ports gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
set_property src_info {type:XDC file:1 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AT43 [get_ports gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N
current_instance hop_gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:2 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
