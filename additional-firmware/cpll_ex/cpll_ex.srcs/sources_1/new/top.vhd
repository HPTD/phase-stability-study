library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use work.gt_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity top is
    port ( 
        clk_125_p   : in  std_logic;  -- 125 MHz system clock (freerun)
        clk_125_n   : in  std_logic;

        gt_refclk_p : in std_logic;
        gt_refclk_n : in std_logic;
        
        gt_rx_p : in  std_logic;
        gt_rx_n : in  std_logic;
        gt_tx_p : out std_logic;
        gt_tx_n : out std_logic
    );
end top;

architecture Behavioral of top is

  signal clk_pre : std_logic;
  signal clk_sys : std_logic;

  signal gen_rst_hop       : std_logic := '0';
  signal axi_gen_rst_hop   : std_logic;
  signal gen_data_hop      : std_logic_vector(31 downto 0); 
  signal gen_ctrl_hop      : std_logic_vector( 3 downto 0); 
  signal disp_from_enc_hop : std_logic;  
  signal disp_to_enc_hop   : std_logic; 
  signal gt_ctrl   : gt_ctrl_t:= GT_CTRL_NULL;
  signal to_gt     : to_gt_t;
  signal from_gt   : from_gt_t;
  signal gt_stat   : gt_stat_t;
  signal vio_tx_reset : std_logic;
  signal stop_reset_state : std_logic;

begin

  ibuf_inst: ibufds port map(i => clk_125_p, ib => clk_125_n, o => clk_pre);   
  
  bufg_inst : bufg port map(i => clk_pre, o => clk_sys);
  
  -- CPLL requires drpclk connected to the free running clk (defined in the IP Core wizard) for proper bring up.
  gt_ctrl.drpclk <= clk_sys;
  
  hop_gt_inst: entity work.hop_m_gt
    port map(
      clk_sys      => clk_sys,
      gt_refclk_p  => gt_refclk_p,
      gt_refclk_n  => gt_refclk_n,
      gt_rx_p(0)   => gt_rx_p,  
      gt_rx_n(0)   => gt_rx_n,  
      gt_tx_p(0)   => gt_tx_p,  
      gt_tx_n(0)   => gt_tx_n,  
      gt_ctrl_i(0) => gt_ctrl, --> record containing signals that could be driven  by an external controller (e.g. VIO)  
      gt_stat_o(0) => gt_stat, --> record containing signals that could be checked by an external controller (e.g. VIO)
      gt_i(0)      => to_gt,   --> record containing signals from the user logic to the GT 
      gt_o(0)      => from_gt, --> record containing signals from the GT to the user logic
      refclk       => open
    );
    
 vio_txpi_inst: entity work.vio_0
    port map(
      clk          => clk_sys,  -- 125MHz system clock (freerun)
      probe_in0(0) => gt_stat.txpmaresetdone, 
      probe_in1(0) => gt_stat.rxpmaresetdone, 
      probe_in2(0) => gt_stat.txplllock, 
      probe_in3(0) => gt_stat.gtpowergood, 
      probe_out0(0) => gt_ctrl.gtwiz_reset_tx_pll_and_datapath, 
      probe_out1(0) => gt_ctrl.gtwiz_reset_rx_pll_and_datapath,
      probe_out2(0) => gt_ctrl.gtwiz_reset_all 
    );


end Behavioral;
