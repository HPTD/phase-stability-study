set_property PACKAGE_PIN AY24 [get_ports clk_125_p]
set_property PACKAGE_PIN AY23 [get_ports clk_125_n]
set_property IOSTANDARD LVDS [get_ports clk_125_*]

create_clock -period 8.00000 -name clk_125   -add [get_ports clk_125_p]
create_clock -period 4.16666 -name gt_refclk -add [get_ports gt_refclk_p]

#### TxHOP transceiver - next hop of the chain (Quad 121)
set_property PACKAGE_PIN AK38 [get_ports gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P  #### CPLL of Quad 121 is used by HOP_GT - MGTREFCLK0
set_property PACKAGE_PIN AK39 [get_ports gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N
set_property PACKAGE_PIN AR45 [get_ports gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
set_property PACKAGE_PIN AR46 [get_ports gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
set_property PACKAGE_PIN AT42 [get_ports gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
set_property PACKAGE_PIN AT43 [get_ports gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]