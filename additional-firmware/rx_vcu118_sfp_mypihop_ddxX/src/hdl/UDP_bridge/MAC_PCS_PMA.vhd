--------------------------------------------------------------------------------
-- PCS and PMA module using the FPGA transceiver for 1G Ethernet communication
-- plus the dedicated MAC module with a basic configuration
--------------------------------------------------------------------------------
--
-- Francesco Martina @ 2023
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity mac_pcs_pma is
    Port (
    
        -- output reset and power signals (to the external device)
        --phy_on      : out   std_logic;  -- on/off signal
        phy_resetb  : out   std_logic;  -- reset signal (inverted)
        phy_mdio    : inout std_logic;  -- control line to program the PHY chip
        phy_mdc     : out   std_logic;  -- clock line (must be < 2.5 MHz)

        reset            : in std_logic; -- general reset
        freerun_clk_125  : in std_logic; -- free-running clock

        -- PHY reference clock @ 625 MHz
        sgmii_clk_p : in std_logic; 
        sgmii_clk_n : in std_logic; 

        -- Transceiver lane
        sgmii_txn : out std_logic;
        sgmii_txp : out std_logic;
        sgmii_rxn : in  std_logic;
        sgmii_rxp : in  std_logic;

        ---- MAC AXI4-S Interface

        -- RX
        rx_reset           : out std_logic;
        rx_mac_aclk        : out std_logic;
        rx_axis_mac_tdata  : out std_logic_vector(7 downto 0);
        rx_axis_mac_tvalid : out std_logic;
        rx_axis_mac_tlast  : out std_logic;
        rx_axis_mac_tuser  : out std_logic;

        -- TX
        tx_reset           : out std_logic;
        tx_mac_aclk        : out std_logic;
        tx_axis_mac_tdata  : in  std_logic_vector(7 downto 0);
        tx_axis_mac_tvalid : in  std_logic;
        tx_axis_mac_tlast  : in  std_logic;
        tx_axis_mac_tready : out std_logic

    );
end mac_pcs_pma;

architecture Behavioral of mac_pcs_pma is

    --- clocks
    signal clk125_sgmii, clk2mhz        : std_logic;
    --- slow clocks and edges
    signal onehz, onehz_d, onehz_re     : std_logic                    := '0';  -- slow generated clocks
    --- resets
    signal rst_delay_slr                : std_logic_vector(4 downto 0) := (others => '1');  -- reset delay shift-register
    signal rst_i, rst_i_n               : std_logic;  -- in to logic
    signal rst125_sgmii, rst125_sgmii_n : std_logic;  -- out from SGMII
    signal tx_reset_out, rx_reset_out   : std_logic;  -- out from MAC
    --- locked
    signal rx_locked, tx_locked         : std_logic;

    -- data
    signal gmii_txd, gmii_rxd                             : std_logic_vector(7 downto 0);
    signal gmii_tx_en, gmii_tx_er, gmii_rx_dv, gmii_rx_er : std_logic;

    -- sgmii controls and status
    signal an_restart, an_restart_d : std_logic := '0';
    signal sgmii_status_vector      : std_logic_vector(15 downto 0);

    -- mdio controls and status
    signal phy_cfg_done, phy_cfg_done_d, phy_cfg_not_done, phy_clkcfg_done, phy_poll_done      : std_logic := '0';
    signal phy_status_reg1, phy_status_reg2, phy_status_reg3, phy_status_reg4, phy_status_reg5 : std_logic_vector(15 downto 0);


--    -- clocks
--    signal PCS_PMA_clk125_tx : std_logic;
--    signal PCS_PMA_clk125_rx : std_logic;

--    -- Flags / Reset
--    signal PMA_gtpowergood     : std_logic;
--    signal PMA_resetdone       : std_logic;
--    signal PMA_mmcm_locked_out : std_logic;
--    signal PCS_logic_resetn    : std_logic;


    -- GMII interface
--    signal gmii_txd   : STD_LOGIC_VECTOR(7 DOWNTO 0);
--    signal gmii_tx_en : STD_LOGIC;
--    signal gmii_tx_er : STD_LOGIC;
--    signal gmii_rxd   : STD_LOGIC_VECTOR(7 DOWNTO 0);
--    signal gmii_rx_dv : STD_LOGIC;
--    signal gmii_rx_er : STD_LOGIC;
    
    --debug edo
    signal pcsRxusrClk2 : std_logic_vector(7 downto 0);
    signal pcsRxusrClk  : std_logic_vector(7 downto 0);
    signal pcs_rxusrclk : std_logic;

begin

    clkdiv : entity work.ipbus_clock_div
        port map(
            clk => freerun_clk_125,
            d7  => clk2mhz,
            d28 => onehz
            );

    phy_mdc <= clk2mhz;
    
        process(freerun_clk_125)
    begin
        if rising_edge(freerun_clk_125) then  -- ff's with CE
            onehz_d <= onehz;
        end if;
    end process;
    onehz_re <= '1' when (onehz = '1' and onehz_d = '0') else '0';

    -- Merge with previous loop?
    process(freerun_clk_125, reset)             -- async-presettables ff's with CE
    begin
        if reset = '1' then
            rst_delay_slr <= (others => '1');
        elsif rising_edge(freerun_clk_125) then
            if onehz_re = '1' then
                rst_delay_slr <= "0" & rst_delay_slr(4 downto 1);
            end if;
        end if;
    end process;

    -- Auto negotiator.
    -- 2 clock-cycle (?) high on the rising edge of phy_cfg_done.
    process(freerun_clk_125)
    begin
        if rising_edge(freerun_clk_125) then
            phy_cfg_done_d <= phy_cfg_done;
            if phy_cfg_done_d = '0' and phy_cfg_done = '1' then
                an_restart   <= '1';
                an_restart_d <= '1';
            else
                an_restart   <= an_restart_d;
                an_restart_d <= '0';
            end if;
        end if;
    end process;

    -- Reset to PHY, active low. The first to be release after 2s
    phy_resetb <= not rst_delay_slr(3);

    -- Reset to PHY config module and temac
    rst_i   <= rst_delay_slr(0);        -- high until reset slr is flushed
    rst_i_n <= not rst_i;  -- as the previous, but negated because the temac like it so

    rst125_sgmii_n <= not rst125_sgmii;
    -- Reset to temac clients (outgoing)
    --rst_o          <= tx_reset or rx_reset;

    -- PCS / PMA
    PCS_PMA_i : entity work.gig_eth_pcs_pma_gmii_to_sgmii_bridge
        port map (
--            gtrefclk_p             => gtrefclk_p,
--            gtrefclk_n             => gtrefclk_n,
--            gtrefclk_out           => open,
--            txn                    => txn,
--            txp                    => txp,
--            rxn                    => rxn,
--            rxp                    => rxp,
--            independent_clock_bufg => freerun_clk_62_5,
--            userclk_out            => open,
--            userclk2_out           => PCS_PMA_clk125_tx,
--            rxuserclk_out          => pcs_rxusrclk,
--            rxuserclk2_out         => PCS_PMA_clk125_rx,
--            gtpowergood            => PMA_gtpowergood,
--            resetdone              => PMA_resetdone,
--            pma_reset_out          => open,
--            mmcm_locked_out        => PMA_mmcm_locked_out,
--            gmii_txd               => gmii_txd,
--            gmii_tx_en             => gmii_tx_en,
--            gmii_tx_er             => gmii_tx_er,
--            gmii_rxd               => gmii_rxd,
--            gmii_rx_dv             => gmii_rx_dv,
--            gmii_rx_er             => gmii_rx_er,
--            gmii_isolate           => open,
--            --configuration_vector   => "00010", -- loopback mode
--            configuration_vector => "00000",
--            status_vector        => open,
--            reset                => reset,
--            signal_detect        => '1' -- Fibre LOS / SFP module disconnection (Not Used)
            refclk625_p            => sgmii_clk_p,
            refclk625_n            => sgmii_clk_n,
            txp_0                  => sgmii_txp,
            txn_0                  => sgmii_txn,
            rxp_0                  => sgmii_rxp,
            rxn_0                  => sgmii_rxn,
            signal_detect_0        => phy_clkcfg_done,
            an_adv_config_vector_0 => b"1101_1000_0000_0001",  -- probably useless
            an_restart_config_0    => an_restart,       --important
            an_interrupt_0         => open,             --useless
            gmii_txd_0             => gmii_txd,
            gmii_tx_en_0           => gmii_tx_en,
            gmii_tx_er_0           => gmii_tx_er,
            gmii_rxd_0             => gmii_rxd,
            gmii_rx_dv_0           => gmii_rx_dv,
            gmii_rx_er_0           => gmii_rx_er,
            gmii_isolate_0         => open,
            sgmii_clk_r_0          => open,             --??
            sgmii_clk_f_0          => open,             --??
            sgmii_clk_en_0         => open,             --??
            speed_is_10_100_0      => '0',
            speed_is_100_0         => '0',
            status_vector_0        => sgmii_status_vector,
            configuration_vector_0 => (4 => '1', 3 => phy_cfg_not_done, others => '0'),
            clk125_out             => clk125_sgmii,
            rst_125_out            => rst125_sgmii,
            rx_locked              => rx_locked,
            tx_locked              => tx_locked,
            -- al this below is dummy but needed
            riu_rddata_3           => X"0000",
            riu_valid_3            => '0',
            riu_prsnt_3            => '0',
            riu_rddata_2           => X"0000",
            riu_valid_2            => '0',
            riu_prsnt_2            => '0',
            riu_rddata_1           => X"0000",
            riu_valid_1            => '0',
            riu_prsnt_1            => '0',
            tx_dly_rdy_1           => '1',
            rx_dly_rdy_1           => '1',
            rx_vtc_rdy_1           => '1',
            tx_vtc_rdy_1           => '1',
            tx_dly_rdy_2           => '1',
            rx_dly_rdy_2           => '1',
            rx_vtc_rdy_2           => '1',
            tx_vtc_rdy_2           => '1',
            tx_dly_rdy_3           => '1',
            rx_dly_rdy_3           => '1',
            rx_vtc_rdy_3           => '1',
            tx_vtc_rdy_3           => '1',
            -- input reset
            reset                  => phy_cfg_not_done  -- hold the bridge in reset until PHY is up and happy
        );
        
--    freq_meas_pcsrxusr2 : entity work.clock_monitor_full
--        port map (
--            clk_ref         => freerun_clk_125, -- The reference clock
--            clk_monitored   => PCS_PMA_clk125_rx, -- The clock to measure
--            measured_period => pcsRxusrClk2
--        );
    freq_meas_pcsrxusr : entity work.clock_monitor_full
        port map (
            clk_ref         => freerun_clk_125, -- The reference clock
            clk_monitored   => pcs_rxusrclk, -- The clock to measure
            measured_period => pcsRxusrClk
        );
    vio_clk_free_PCS : entity work.vio_2
        port map (
            clk       => freerun_clk_125,
            probe_in0 => pcsRxusrClk2,
            probe_in1 => pcsRxusrClk
        );

    --PCS_logic_resetn <= PMA_gtpowergood and PMA_resetdone and PMA_mmcm_locked_out;

    -- MAC module
    MAC : entity work.temac_gbe_v9_0
        port map (
--            gtx_clk                 => PCS_PMA_clk125_tx,
--            --rx_usr_clk2             => PCS_PMA_clk125_rx,
--            glbl_rstn               => PCS_logic_resetn,
--            rx_axi_rstn             => PCS_logic_resetn,
--            tx_axi_rstn             => PCS_logic_resetn,
--            rx_statistics_vector    => open,
--            rx_statistics_valid     => open,
--            rx_mac_aclk             => rx_mac_aclk,
--            rx_reset                => rx_reset,
--            rx_axis_mac_tdata       => rx_axis_mac_tdata,
--            rx_axis_mac_tvalid      => rx_axis_mac_tvalid,
--            rx_axis_mac_tlast       => rx_axis_mac_tlast,
--            rx_axis_mac_tuser       => rx_axis_mac_tuser,
--            tx_ifg_delay            => X"00",
--            tx_statistics_vector    => open,
--            tx_statistics_valid     => open,
--            tx_mac_aclk             => tx_mac_aclk,
--            tx_reset                => tx_reset,
--            tx_axis_mac_tdata       => tx_axis_mac_tdata,
--            tx_axis_mac_tvalid      => tx_axis_mac_tvalid,
--            tx_axis_mac_tlast       => tx_axis_mac_tlast,
--            tx_axis_mac_tuser(0)    => '0',
--            tx_axis_mac_tready      => tx_axis_mac_tready,
--            pause_req               => '0',
--            pause_val               => X"0000",
--            speedis100              => open,
--            speedis10100            => open,
--            gmii_txd                => gmii_txd,
--            gmii_tx_en              => gmii_tx_en,
--            gmii_tx_er              => gmii_tx_er,
--            gmii_rxd                => gmii_rxd,
--            gmii_rx_dv              => gmii_rx_dv,
--            gmii_rx_er              => gmii_rx_er,
--            rx_configuration_vector => X"0000_0000_0000_0000_0802", -- RX Enabled in promiscuous mode (Jumbo pkt not enabled)
--            tx_configuration_vector => X"0000_0000_0000_0000_0002"  -- TX Enabled (Jumbo pkt not enabled)
            gtx_clk                 => clk125_sgmii,
            glbl_rstn               => rst_i_n,
            rx_axi_rstn             => rst125_sgmii_n,
            tx_axi_rstn             => rst125_sgmii_n,
            rx_statistics_vector    => open,
            rx_statistics_valid     => open,
            rx_mac_aclk             => rx_mac_aclk,
            rx_reset                => rx_reset,
            rx_axis_mac_tdata       => rx_axis_mac_tdata,
            rx_axis_mac_tvalid      => rx_axis_mac_tvalid,
            rx_axis_mac_tlast       => rx_axis_mac_tlast,
            rx_axis_mac_tuser       => rx_axis_mac_tuser,
            tx_ifg_delay            => X"00",
            tx_statistics_vector    => open,
            tx_statistics_valid     => open,
            tx_mac_aclk             => tx_mac_aclk,
            tx_reset                => tx_reset,
            tx_axis_mac_tdata       => tx_axis_mac_tdata,
            tx_axis_mac_tvalid      => tx_axis_mac_tvalid,
            tx_axis_mac_tlast       => tx_axis_mac_tlast,
            tx_axis_mac_tuser(0)    => '0',--tx_error,
            tx_axis_mac_tready      => tx_axis_mac_tready,
            pause_req               => '0',
            pause_val               => X"0000",
            gmii_txd                => gmii_txd,
            gmii_tx_en              => gmii_tx_en,
            gmii_tx_er              => gmii_tx_er,
            gmii_rxd                => gmii_rxd,
            gmii_rx_dv              => gmii_rx_dv,
            gmii_rx_er              => gmii_rx_er,
            rx_configuration_vector => X"0000_0000_0000_0000_0812",
            tx_configuration_vector => X"0000_0000_0000_0000_0012"
        );
        
    --clk125_eth <= clk125_sgmii;
    --locked     <= rx_locked and tx_locked;

    phy_cfgrt : entity work.phy_mdio_configurator_vcu118
        port map (
            clk125      => freerun_clk_125,
            mdc         => clk2mhz,
            rst         => rst_i,
            done        => phy_cfg_done,
            clkdone     => phy_clkcfg_done,
            poll_enable => '1',
            poll_clk    => onehz,
            poll_done   => phy_poll_done,
            status_reg1 => phy_status_reg1,
            status_reg2 => phy_status_reg2,
            status_reg3 => phy_status_reg3,
            status_reg4 => phy_status_reg4,
            status_reg5 => phy_status_reg5,
            phy_mdio    => phy_mdio);

    -- Needed by sgmii ports
    phy_cfg_not_done <= not(phy_clkcfg_done);

end Behavioral;
