vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xpm
vlib questa_lib/msim/xil_defaultlib

vmap xpm questa_lib/msim/xpm
vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vlog -work xpm  -incr -mfcu -sv "+incdir+../../../../../../src/ip/udp_bridge/Ethernet_Setup_VIO/hdl/verilog" "+incdir+../../../../../../src/ip/udp_bridge/Ethernet_Setup_VIO/hdl" \
"E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm  -93 \
"E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -incr -mfcu "+incdir+../../../../../../src/ip/udp_bridge/Ethernet_Setup_VIO/hdl/verilog" "+incdir+../../../../../../src/ip/udp_bridge/Ethernet_Setup_VIO/hdl" \
"../../../../../../src/ip/udp_bridge/Ethernet_Setup_VIO/Ethernet_Setup_VIO_sim_netlist.v" \

vlog -work xil_defaultlib \
"glbl.v"

