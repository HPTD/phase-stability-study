
O
Command: %s
53*	vivadotcl2

opt_design2default:defaultZ4-113h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2"
Implementation2default:default2
xcvu9p2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2"
Implementation2default:default2
xcvu9p2default:defaultZ17-349h px� 
�
�The version limit for your license is '%s' and has expired for new software. A version limit expiration means that, although you may be able to continue to use the current version of tools or IP with this license, you will not be eligible for new releases.
719*common2
2022.072default:defaultZ17-1540h px� 
n
,Running DRC as a precondition to command %s
22*	vivadotcl2

opt_design2default:defaultZ4-22h px� 
R

Starting %s Task
103*constraints2
DRC2default:defaultZ18-103h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
U
DRC finished with %s
272*project2
0 Errors2default:defaultZ1-461h px� 
d
BPlease refer to the DRC report (report_drc) for more information.
274*projectZ1-462h px� 
�

%s
*constraints2o
[Time (s): cpu = 00:00:03 ; elapsed = 00:00:02 . Memory (MB): peak = 2789.840 ; gain = 0.0002default:defaulth px� 
g

Starting %s Task
103*constraints2,
Cache Timing Information2default:defaultZ18-103h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
P
;Ending Cache Timing Information Task | Checksum: 1f4555cb6
*commonh px� 
�

%s
*constraints2o
[Time (s): cpu = 00:00:03 ; elapsed = 00:00:02 . Memory (MB): peak = 2789.840 ; gain = 0.0002default:defaulth px� 
a

Starting %s Task
103*constraints2&
Logic Optimization2default:defaultZ18-103h px� 
�

Phase %s%s
101*constraints2
1 2default:default27
#Generate And Synthesize Debug Cores2default:defaultZ18-101h px� 
k
)Generating Script for core instance : %s 214*	chipscope2
dbg_hub2default:defaultZ16-329h px� 
�
Generating IP %s for %s.
1712*coregen2+
xilinx.com:ip:xsdbm:3.02default:default2

dbg_hub_CV2default:defaultZ19-3806h px� 
�
;Using cached IP synthesis design for IP %s, cache-ID = %s.
286*	chipscope2+
xilinx.com:ip:xsdbm:3.02default:default2$
ceb57d243bf15a0e2default:defaultZ16-469h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0802default:default2
3082.2192default:default2
0.0002default:defaultZ17-268h px� 
W
BPhase 1 Generate And Synthesize Debug Cores | Checksum: 19a98231a
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:06 ; elapsed = 00:00:31 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
i

Phase %s%s
101*constraints2
2 2default:default2
Retarget2default:defaultZ18-101h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_read.si_transactor_ar/gen_single_thread.active_target_hot[6]_i_1	�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_read.si_transactor_ar/gen_single_thread.active_target_hot[6]_i_12default:default2�
�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_read.si_transactor_ar/gen_single_thread.active_target_hot[6]_i_2__0	�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_read.si_transactor_ar/gen_single_thread.active_target_hot[6]_i_2__02default:default2
122default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_write.si_transactor_aw/gen_single_thread.active_target_hot[6]_i_1__0	�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_write.si_transactor_aw/gen_single_thread.active_target_hot[6]_i_1__02default:default2�
�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_write.si_transactor_aw/gen_single_thread.active_target_hot[6]_i_2	�AXI_inst/system_ctrl_inst/system_interconnect_wrapper_i/system_interconnect_i/axi_interconnect/xbar/inst/gen_samd.crossbar_samd/gen_slave_slots[0].gen_si_write.si_transactor_aw/gen_single_thread.active_target_hot[6]_i_22default:default2
132default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/gaxis_rst_sync.xpm_cdc_sync_rst_inst_i_1	�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/gaxis_rst_sync.xpm_cdc_sync_rst_inst_i_12default:default2�
UAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/UDP_bridge_CDC_FIFO_RX_i_1	UAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/UDP_bridge_CDC_FIFO_RX_i_12default:default2
22default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst/FSM_onehot_gen_rst_ic.curr_wrst_state[4]_i_1	�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst/FSM_onehot_gen_rst_ic.curr_wrst_state[4]_i_12default:default2�
�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst//i_	�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_RX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst//i_2default:default2
22default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_TX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst/FSM_onehot_gen_rst_ic.curr_wrst_state[4]_i_1	�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_TX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst/FSM_onehot_gen_rst_ic.curr_wrst_state[4]_i_12default:default2�
�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_TX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst//i_	�AXI_inst/system_ctrl_inst/udp_bridge_control/UDP_bridge_CDC_FIFO_TX/inst/gen_fifo.xpm_fifo_axis_inst/xpm_fifo_base_inst/xpm_fifo_rst_inst//i_2default:default2
22default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
AXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC/U0/temac_gbe_v9_0_core/sync_axi_rx_rstn_rx_clk/async_rst0_i_1__0	AXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC/U0/temac_gbe_v9_0_core/sync_axi_rx_rstn_rx_clk/async_rst0_i_1__02default:default2�
BAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC_i_2	BAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC_i_22default:default2
22default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
zAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC/U0/temac_gbe_v9_0_core/sync_glbl_rstn_rx_clk/async_rst0_i_1	zAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC/U0/temac_gbe_v9_0_core/sync_glbl_rstn_rx_clk/async_rst0_i_12default:default2�
BAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC_i_1	BAXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/MAC_i_12default:default2
12default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2^
#fsm_aligner_inst/byteAligned_INST_0	#fsm_aligner_inst/byteAligned_INST_02default:default2f
'fsm_aligner_inst/byteAligned_INST_0_i_1	'fsm_aligner_inst/byteAligned_INST_0_i_12default:default2
142default:default8Z31-1287h px� 
�
VPulled Inverter %s into driver instance %s, which resulted in an inversion of %s pins
597*opt2�
Gila_rx_inst/U0/ila_core_inst/xsdb_memory_read_inst/current_state[0]_i_1	Gila_rx_inst/U0/ila_core_inst/xsdb_memory_read_inst/current_state[0]_i_12default:default2�
Cila_rx_inst/U0/ila_core_inst/xsdb_memory_read_inst/read_addr[9]_i_3	Cila_rx_inst/U0/ila_core_inst/xsdb_memory_read_inst/read_addr[9]_i_32default:default2
142default:default8Z31-1287h px� 
|
3Net with MARK_DEBUG TRUE property was optimized: %s169*opt2&
alignedaligned2default:default8Z31-233h px� 
y
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
592default:default2
13082default:defaultZ31-138h px� 
K
Retargeted %s cell(s).
49*opt2
02default:defaultZ31-49h px� 
<
'Phase 2 Retarget | Checksum: 1dffec0aa
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:09 ; elapsed = 00:00:34 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
.Phase %s created %s cells and removed %s cells267*opt2
Retarget2default:default2
1522default:default2
4392default:defaultZ31-389h px� 
�
�In phase %s, %s netlist objects are constrained preventing optimization. Please run opt_design with -debug_log to get more detail. 510*opt2
Retarget2default:default2
6302default:defaultZ31-1021h px� 
u

Phase %s%s
101*constraints2
3 2default:default2(
Constant propagation2default:defaultZ18-101h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
32default:default2
52default:defaultZ31-138h px� 
H
3Phase 3 Constant propagation | Checksum: 1b0632726
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:10 ; elapsed = 00:00:34 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
.Phase %s created %s cells and removed %s cells267*opt2(
Constant propagation2default:default2
2292default:default2
10392default:defaultZ31-389h px� 
�
�In phase %s, %s netlist objects are constrained preventing optimization. Please run opt_design with -debug_log to get more detail. 510*opt2(
Constant propagation2default:default2
7542default:defaultZ31-1021h px� 
f

Phase %s%s
101*constraints2
4 2default:default2
Sweep2default:defaultZ18-101h px� 
9
$Phase 4 Sweep | Checksum: 24066709a
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:14 ; elapsed = 00:00:38 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
.Phase %s created %s cells and removed %s cells267*opt2
Sweep2default:default2
02default:default2
37642default:defaultZ31-389h px� 
�
�In phase %s, %s netlist objects are constrained preventing optimization. Please run opt_design with -debug_log to get more detail. 510*opt2
Sweep2default:default2
57632default:defaultZ31-1021h px� 
r

Phase %s%s
101*constraints2
5 2default:default2%
BUFG optimization2default:defaultZ18-101h px� 
�
PPhase BUFG optimization inserted %s global clock buffer(s) for CLOCK_LOW_FANOUT.553*opt2
02default:defaultZ31-1077h px� 
E
0Phase 5 BUFG optimization | Checksum: 24066709a
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:14 ; elapsed = 00:00:39 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
EPhase %s created %s cells of which %s are BUFGs and removed %s cells.395*opt2%
BUFG optimization2default:default2
02default:default2
02default:default2
02default:defaultZ31-662h px� 
|

Phase %s%s
101*constraints2
6 2default:default2/
Shift Register Optimization2default:defaultZ18-101h px� 
�
dSRL Remap converted %s SRLs to %s registers and converted %s registers of register chains to %s SRLs546*opt2
02default:default2
02default:default2
02default:default2
02default:defaultZ31-1064h px� 
O
:Phase 6 Shift Register Optimization | Checksum: 24066709a
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:14 ; elapsed = 00:00:39 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
.Phase %s created %s cells and removed %s cells267*opt2/
Shift Register Optimization2default:default2
02default:default2
02default:defaultZ31-389h px� 
x

Phase %s%s
101*constraints2
7 2default:default2+
Post Processing Netlist2default:defaultZ18-101h px� 
K
6Phase 7 Post Processing Netlist | Checksum: 24066709a
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:15 ; elapsed = 00:00:39 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
�
.Phase %s created %s cells and removed %s cells267*opt2+
Post Processing Netlist2default:default2
02default:default2
02default:defaultZ31-389h px� 
�
�In phase %s, %s netlist objects are constrained preventing optimization. Please run opt_design with -debug_log to get more detail. 510*opt2+
Post Processing Netlist2default:default2
7002default:defaultZ31-1021h px� 
/
Opt_design Change Summary
*commonh px� 
/
=========================
*commonh px� 


*commonh px� 


*commonh px� 
�
z-------------------------------------------------------------------------------------------------------------------------
*commonh px� 
�
�|  Phase                        |  #Cells created  |  #Cells Removed  |  #Constrained objects preventing optimizations  |
-------------------------------------------------------------------------------------------------------------------------
*commonh px� 
�
�|  Retarget                     |             152  |             439  |                                            630  |
|  Constant propagation         |             229  |            1039  |                                            754  |
|  Sweep                        |               0  |            3764  |                                           5763  |
|  BUFG optimization            |               0  |               0  |                                              0  |
|  Shift Register Optimization  |               0  |               0  |                                              0  |
|  Post Processing Netlist      |               0  |               0  |                                            700  |
-------------------------------------------------------------------------------------------------------------------------
*commonh px� 


*commonh px� 


*commonh px� 
a

Starting %s Task
103*constraints2&
Connectivity Check2default:defaultZ18-103h px� 
�

%s
*constraints2s
_Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.051 . Memory (MB): peak = 3082.219 ; gain = 0.0002default:defaulth px� 
I
4Ending Logic Optimization Task | Checksum: d24f0901
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:16 ; elapsed = 00:00:40 . Memory (MB): peak = 3082.219 ; gain = 44.7502default:defaulth px� 
a

Starting %s Task
103*constraints2&
Power Optimization2default:defaultZ18-103h px� 
s
7Will skip clock gating for clocks with period < %s ns.
114*pwropt2
2.002default:defaultZ34-132h px� 
�
$Power model is not available for %s
23*power2�
Gen_0.BaseX_Byte_I_Riu_Or_TxLow	�AXI_inst/system_ctrl_inst/udp_bridge_control/mac_pcs_pma_i/PCS_PMA_i/inst/pcs_pma_block_i/gen_io_logic/Gen_0.BaseX_Byte_I_Riu_Or_TxLow2default:default8Z33-23h px� 
=
Applying IDT optimizations ...
9*pwroptZ34-9h px� 
?
Applying ODC optimizations ...
10*pwroptZ34-10h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
K
,Running Vector-less Activity Propagation...
51*powerZ33-51h px� 
P
3
Finished Running Vector-less Activity Propagation
1*powerZ33-1h px� 


*pwropth px� 
e

Starting %s Task
103*constraints2*
PowerOpt Patch Enables2default:defaultZ18-103h px� 
�
�WRITE_MODE attribute of %s BRAM(s) out of a total of %s has been updated to save power.
    Run report_power_opt to get a complete listing of the BRAMs updated.
129*pwropt2
42default:default2
42default:defaultZ34-162h px� 
d
+Structural ODC has moved %s WE to EN ports
155*pwropt2
02default:defaultZ34-201h px� 
�
CNumber of BRAM Ports augmented: %s newly gated: %s Total Ports: %s
65*pwropt2
42default:default2
02default:default2
82default:defaultZ34-65h px� 
N
9Ending PowerOpt Patch Enables Task | Checksum: 11698c76b
*commonh px� 
�

%s
*constraints2s
_Time (s): cpu = 00:00:01 ; elapsed = 00:00:00.201 . Memory (MB): peak = 4490.996 ; gain = 0.0002default:defaulth px� 
J
5Ending Power Optimization Task | Checksum: 11698c76b
*commonh px� 
�

%s
*constraints2r
^Time (s): cpu = 00:00:48 ; elapsed = 00:00:43 . Memory (MB): peak = 4490.996 ; gain = 1408.7772default:defaulth px� 
\

Starting %s Task
103*constraints2!
Final Cleanup2default:defaultZ18-103h px� 
E
0Ending Final Cleanup Task | Checksum: 11698c76b
*commonh px� 
�

%s
*constraints2s
_Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.005 . Memory (MB): peak = 4490.996 ; gain = 0.0002default:defaulth px� 
b

Starting %s Task
103*constraints2'
Netlist Obfuscation2default:defaultZ18-103h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0232default:default2
4490.9962default:default2
0.0002default:defaultZ17-268h px� 
K
6Ending Netlist Obfuscation Task | Checksum: 16fe42b0b
*commonh px� 
�

%s
*constraints2s
_Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.028 . Memory (MB): peak = 4490.996 ; gain = 0.0002default:defaulth px� 
Z
Releasing license: %s
83*common2"
Implementation2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
992default:default2
922default:default2
02default:default2
02default:defaultZ4-41h px� 
\
%s completed successfully
29*	vivadotcl2

opt_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2 
opt_design: 2default:default2
00:01:132default:default2
00:01:402default:default2
4490.9962default:default2
1701.1562default:defaultZ17-268h px� 
�
4The following parameters have non-default value.
%s
395*common24
 tcl.collectionResultDisplayLimit2default:defaultZ17-600h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
H
&Writing timing data to binary archive.266*timingZ38-480h px� 
=
Writing XDEF routing.
211*designutilsZ20-211h px� 
J
#Writing XDEF routing logical nets.
209*designutilsZ20-209h px� 
J
#Writing XDEF routing special nets.
210*designutilsZ20-210h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2)
Write XDEF Complete: 2default:default2
00:00:012default:default2 
00:00:00.1192default:default2
4490.9962default:default2
0.0002default:defaultZ17-268h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2z
fC:/Users/eorzes/cernbox/git/rx_vcu118_firefly_mypihop_ddxX_f/prj/m_top/m_top.runs/impl_1/m_top_opt.dcp2default:defaultZ17-1381h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2&
write_checkpoint: 2default:default2
00:00:102default:default2
00:00:072default:default2
4490.9962default:default2
0.0002default:defaultZ17-268h px� 
�
%s4*runtcl2u
aExecuting : report_drc -file m_top_drc_opted.rpt -pb m_top_drc_opted.pb -rpx m_top_drc_opted.rpx
2default:defaulth px� 
�
Command: %s
53*	vivadotcl2h
Treport_drc -file m_top_drc_opted.rpt -pb m_top_drc_opted.pb -rpx m_top_drc_opted.rpx2default:defaultZ4-113h px� 
>
IP Catalog is up to date.1232*coregenZ19-1839h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
�
#The results of DRC are in file %s.
586*	vivadotcl2�
lC:/Users/eorzes/cernbox/git/rx_vcu118_firefly_mypihop_ddxX_f/prj/m_top/m_top.runs/impl_1/m_top_drc_opted.rptlC:/Users/eorzes/cernbox/git/rx_vcu118_firefly_mypihop_ddxX_f/prj/m_top/m_top.runs/impl_1/m_top_drc_opted.rpt2default:default8Z2-168h px� 
\
%s completed successfully
29*	vivadotcl2

report_drc2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2 
report_drc: 2default:default2
00:00:142default:default2
00:00:082default:default2
4490.9962default:default2
0.0002default:defaultZ17-268h px� 


End Record