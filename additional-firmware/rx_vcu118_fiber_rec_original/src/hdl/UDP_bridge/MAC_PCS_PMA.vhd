--------------------------------------------------------------------------------
-- PCS and PMA module using the FPGA transceiver for 1G Ethernet communication
-- plus the dedicated MAC module with a basic configuration
--------------------------------------------------------------------------------
--
-- Francesco Martina @ 2023
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity mac_pcs_pma is
    Port (

        reset            : in std_logic; -- general reset
        freerun_clk_62_5 : in std_logic; -- free-running clock @ 62.5 MHz

        -- Transceiver reference clock @ 125 MHz
        gtrefclk_p : in std_logic;
        gtrefclk_n : in std_logic;
        
        --PHY
        mdio : inout std_logic;
        mdc  : out   std_logic;

        -- Transceiver lane
        txn : out std_logic;
        txp : out std_logic;
        rxn : in  std_logic;
        rxp : in  std_logic;

        ---- MAC AXI4-S Interface

        -- RX
        rx_reset           : out std_logic;
        rx_mac_aclk        : out std_logic;
        rx_axis_mac_tdata  : out std_logic_vector(7 downto 0);
        rx_axis_mac_tvalid : out std_logic;
        rx_axis_mac_tlast  : out std_logic;
        rx_axis_mac_tuser  : out std_logic;

        -- TX
        tx_reset           : out std_logic;
        tx_mac_aclk        : out std_logic;
        tx_axis_mac_tdata  : in  std_logic_vector(7 downto 0);
        tx_axis_mac_tvalid : in  std_logic;
        tx_axis_mac_tlast  : in  std_logic;
        tx_axis_mac_tready : out std_logic

    );
end mac_pcs_pma;

architecture Behavioral of mac_pcs_pma is

    -- clocks
    signal PCS_PMA_clk125_tx : std_logic;
    signal PCS_PMA_clk125_rx : std_logic;

    -- Flags / Reset
    signal PMA_gtpowergood     : std_logic;
    signal PMA_reset           : std_logic;
    signal PMA_mmcm_locked_out : std_logic;
    signal PCS_logic_resetn    : std_logic;


    -- GMII interface
    signal gmii_txd   : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal gmii_tx_en : STD_LOGIC;
    signal gmii_tx_er : STD_LOGIC;
    signal gmii_rxd   : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal gmii_rx_dv : STD_LOGIC;
    signal gmii_rx_er : STD_LOGIC;
    
    --PHY
    signal ext_mdio_i_0 : STD_LOGIC;
    signal ext_mdio_o_0 : STD_LOGIC;
    signal ext_mdio_t_0 : STD_LOGIC;
    
    --debug edo
    signal pcsRxusrClk2 : std_logic_vector(7 downto 0);
    signal pcsRxusrClk  : std_logic_vector(7 downto 0);
    signal pcs_rxusrclk : std_logic;

begin

   IOBUF_inst : entity work.IOBUF
   port map (
      O => ext_mdio_i_0,   -- 1-bit output: Buffer output
      I => ext_mdio_o_0,   -- 1-bit input: Buffer input
      IO => mdio, -- 1-bit inout: Buffer inout (connect directly to top-level port)
      T => ext_mdio_t_0    -- 1-bit input: 3-state enable input
   );

    PCS_PMA_i : entity work.PCS_PMA
        port map (
            refclk625_n            => gtrefclk_n, -- it's not a GT ref, it comes from PHY
            refclk625_p            => gtrefclk_p,
            txn_0                  => txn,
            txp_0                  => txp,
            rxn_0                  => rxn,
            rxp_0                  => rxp,
            ext_mdc_0              => mdc,
            ext_mdio_i_0           => ext_mdio_i_0,
            ext_mdio_o_0           => ext_mdio_o_0,
            ext_mdio_t_0           => ext_mdio_t_0,
            mdio_t_in_0            => '0',
            clk125_out             => PCS_PMA_clk125_tx,
            clk312_out             => open,
            rst_125_out            => PMA_reset,
            sgmii_clk_r_0          => open,
            sgmii_clk_f_0          => open,
            sgmii_clk_en_0         => open,
            gmii_txd_0             => gmii_txd,
            gmii_tx_en_0           => gmii_tx_en,
            gmii_tx_er_0           => gmii_tx_er,
            gmii_rxd_0             => gmii_rxd,
            gmii_rx_dv_0           => gmii_rx_dv,
            gmii_rx_er_0           => gmii_rx_er,
            gmii_isolate_0         => open,
            configuration_vector_0 => "00000",
            speed_is_10_100_0      => '0',
            speed_is_100_0         => '0',
            status_vector_0        => open,
            reset                  => reset,
            signal_detect_0        => '1', -- Fibre LOS / SFP module disconnection (Not Used)
            
            tx_dly_rdy_1 => '1',
            rx_dly_rdy_1 => '1',
            tx_vtc_rdy_1 => '1',
            rx_vtc_rdy_1 => '1',
            tx_dly_rdy_2 => '1',
            rx_dly_rdy_2 => '1',
            tx_vtc_rdy_2 => '1',
            rx_vtc_rdy_2 => '1',
            tx_dly_rdy_3 => '1',
            rx_dly_rdy_3 => '1',
            tx_vtc_rdy_3 => '1',
            rx_vtc_rdy_3 => '1',
            riu_valid_3 => '0',
            riu_valid_2 => '0',
            riu_valid_1 => '0',
            riu_prsnt_1 => '0',
            riu_prsnt_2 => '0',
            riu_prsnt_3 => '0',
            riu_rddata_3 => (others => '0'),
            riu_rddata_1 => (others => '0'),
            riu_rddata_2 => (others => '0'),

            tx_logic_reset => open,
            rx_logic_reset => open,
            rx_locked => open,
            tx_locked => open,
            tx_bsc_rst_out => open,
            rx_bsc_rst_out => open,
            tx_bs_rst_out => open,
            rx_bs_rst_out => open,
            tx_rst_dly_out => open,
            rx_rst_dly_out => open,
            tx_bsc_en_vtc_out => open,
            rx_bsc_en_vtc_out => open,
            tx_bs_en_vtc_out => open,
            rx_bs_en_vtc_out => open,
            riu_clk_out => open,
            riu_wr_en_out => open,
            tx_pll_clk_out => open,
            rx_pll_clk_out => open,
            tx_rdclk_out => open,
            riu_addr_out => open,
            riu_wr_data_out => open,
            riu_nibble_sel_out => open,
            rx_btval_1 => open,
            rx_btval_2 => open,
            rx_btval_3 => open
        );

    -- PCS / PMA
--    PCS_PMA_i : entity work.PCS_PMA
--        port map (
--            gtrefclk_p             => gtrefclk_p,
--            gtrefclk_n             => gtrefclk_n,
--            gtrefclk_out           => open,
--            txn                    => txn,
--            txp                    => txp,
--            rxn                    => rxn,
--            rxp                    => rxp,
--            independent_clock_bufg => freerun_clk_62_5,
--            userclk_out            => open,
--            userclk2_out           => PCS_PMA_clk125_tx,
--            rxuserclk_out          => pcs_rxusrclk,
--            rxuserclk2_out         => PCS_PMA_clk125_rx,
--            gtpowergood            => PMA_gtpowergood,
--            resetdone              => PMA_resetdone,
--            pma_reset_out          => open,
--            mmcm_locked_out        => PMA_mmcm_locked_out,
--            --sgmii_clk_r          => sgmii_clk_r,
--            --sgmii_clk_f          => sgmii_clk_f,
--            --sgmii_clk_en         => sgmii_clk_en,
--            gmii_txd               => gmii_txd,
--            gmii_tx_en             => gmii_tx_en,
--            gmii_tx_er             => gmii_tx_er,
--            gmii_rxd               => gmii_rxd,
--            gmii_rx_dv             => gmii_rx_dv,
--            gmii_rx_er             => gmii_rx_er,
--            gmii_isolate           => open,
--            --configuration_vector   => "00010", -- loopback mode
--            configuration_vector => "00000",
--            speed_is_10_100      => '0',
--            speed_is_100         => '1',
--            status_vector        => open,
--            reset                => reset,
--            signal_detect        => '1' -- Fibre LOS / SFP module disconnection (Not Used)
--        );
        
    freq_meas_pcsrxusr2 : entity work.clock_monitor_full
        port map (
            clk_ref         => freerun_clk_62_5, -- The reference clock
            clk_monitored   => PCS_PMA_clk125_rx, -- The clock to measure
            measured_period => pcsRxusrClk2
        );
    freq_meas_pcsrxusr : entity work.clock_monitor_full
        port map (
            clk_ref         => freerun_clk_62_5, -- The reference clock
            clk_monitored   => pcs_rxusrclk, -- The clock to measure
            measured_period => pcsRxusrClk
        );
    vio_clk_free_PCS : entity work.vio_2
        port map (
            clk       => freerun_clk_62_5,
            probe_in0 => pcsRxusrClk2,
            probe_in1 => pcsRxusrClk
        );

    PCS_logic_resetn <= PMA_gtpowergood and not(PMA_reset) and PMA_mmcm_locked_out;

    -- MAC module
    MAC : entity work.MAC
        port map (
            gtx_clk                 => PCS_PMA_clk125_tx,
            --rx_usr_clk2             => PCS_PMA_clk125_rx,
            glbl_rstn               => PCS_logic_resetn,
            rx_axi_rstn             => PCS_logic_resetn,
            tx_axi_rstn             => PCS_logic_resetn,
            rx_statistics_vector    => open,
            rx_statistics_valid     => open,
            rx_mac_aclk             => rx_mac_aclk,
            rx_reset                => rx_reset,
            rx_axis_mac_tdata       => rx_axis_mac_tdata,
            rx_axis_mac_tvalid      => rx_axis_mac_tvalid,
            rx_axis_mac_tlast       => rx_axis_mac_tlast,
            rx_axis_mac_tuser       => rx_axis_mac_tuser,
            tx_ifg_delay            => X"00",
            tx_statistics_vector    => open,
            tx_statistics_valid     => open,
            tx_mac_aclk             => tx_mac_aclk,
            tx_reset                => tx_reset,
            tx_axis_mac_tdata       => tx_axis_mac_tdata,
            tx_axis_mac_tvalid      => tx_axis_mac_tvalid,
            tx_axis_mac_tlast       => tx_axis_mac_tlast,
            tx_axis_mac_tuser(0)    => '0',
            tx_axis_mac_tready      => tx_axis_mac_tready,
            pause_req               => '0',
            pause_val               => X"0000",
            speedis100              => open,
            speedis10100            => open,
            gmii_txd                => gmii_txd,
            gmii_tx_en              => gmii_tx_en,
            gmii_tx_er              => gmii_tx_er,
            gmii_rxd                => gmii_rxd,
            gmii_rx_dv              => gmii_rx_dv,
            gmii_rx_er              => gmii_rx_er,
            rx_configuration_vector => X"0000_0000_0000_0000_0802", -- RX Enabled in promiscuous mode (Jumbo pkt not enabled)
            tx_configuration_vector => X"0000_0000_0000_0000_0002"  -- TX Enabled (Jumbo pkt not enabled)
        );

end Behavioral;
