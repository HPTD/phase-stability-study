set_property PACKAGE_PIN AY24 [get_ports clk_125_p]
set_property PACKAGE_PIN AY23 [get_ports clk_125_n]
set_property IOSTANDARD LVDS [get_ports clk_125_*]

### DDMTD inputs
set_property PACKAGE_PIN AG8 [get_ports gtfanout_in_n] ;# MGT266_CLK0_N
set_property PACKAGE_PIN AG9 [get_ports gtfanout_in_p] ;# MGT266_CLK0_P
set_property PACKAGE_PIN J8  [get_ports ddmtdclk_in_n] ;# MGT233_CLK1_N
set_property PACKAGE_PIN J9  [get_ports ddmtdclk_in_p] ;# MGT233_CLK1_P

## main transceiver 
set_property PACKAGE_PIN AR45 [get_ports gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
set_property PACKAGE_PIN AR46 [get_ports gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
set_property PACKAGE_PIN AT42 [get_ports gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
set_property PACKAGE_PIN AT43 [get_ports gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N

set_property PACKAGE_PIN AH39 [get_ports rxrecclk_n] ;# FMCP_HSPC_GBT1_0_N
set_property PACKAGE_PIN AH38 [get_ports rxrecclk_p] ;# FMCP_HSPC_GBT1_0_P

set_property PACKAGE_PIN AK38 [get_ports gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P
set_property PACKAGE_PIN AK39 [get_ports gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N

#set_property RX_PROGDIV_CFG 20.0 [get_cells {gt_inst/ch[0].inst/inst/gen_gtwizard_gthe4_top.m_gth_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[3].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST}]

## HPC1 - rxusrclk
set_property PACKAGE_PIN AL35 [get_ports rxUserClk_p] ;#FMCP_HSPC_LA00_CC_P
set_property PACKAGE_PIN AL36 [get_ports rxUserClk_n] ;#FMCP_HSPC_LA00_CC_N
set_property IOSTANDARD LVDS [get_ports rxUserClk_p]
set_property IOSTANDARD LVDS [get_ports rxUserClk_n]

## ----- Ethernet
# quad (bank) 230
set_property PACKAGE_PIN AU22 [get_ports eth_gtrefclk_n] ;# PHY1_SGMII_CLK_N (RX_D1_SGMII_CON)
set_property PACKAGE_PIN AT22 [get_ports eth_gtrefclk_p] ;# PHY1_SGMII_CLK_P (RX_D0_SGMII_COP)
set_property PACKAGE_PIN AR23 [get_ports mdio] ;# PHY1_MDIO (MDIO)
set_property PACKAGE_PIN AV23 [get_ports mdc ] ;# PHY1_MDC  (MDC)
set_property IOSTANDARD LVCMOS18 [get_ports mdio]
set_property IOSTANDARD LVCMOS18 [get_ports mdc]
set_property IOSTANDARD LVDS [get_ports eth_gtrefclk_*]

# quad (bank): 230, X1Y13 -> SFP1 (bottom right)
set_property PACKAGE_PIN AU24 [get_ports rxp_eth_sfp] ;# PHY1_SGMII_OUT_P (RX_D2_SGMII_SOP)
set_property PACKAGE_PIN AV24 [get_ports rxn_eth_sfp] ;# PHY1_SGMII_OUT_N (RX_D3_SGMII_SON)
set_property PACKAGE_PIN AU21 [get_ports txp_eth_sfp] ;# PHY1_SGMII_IN_P  (RX_D1_SGMII_SIP)
set_property PACKAGE_PIN AV21 [get_ports txn_eth_sfp] ;# PHY1_SGMII_IN_N  (RX_D0_SGMII_SIN)
set_property IOSTANDARD LVDS [get_ports rxp_eth_sfp]
set_property IOSTANDARD LVDS [get_ports rxn_eth_sfp]
set_property IOSTANDARD LVDS [get_ports txp_eth_sfp]
set_property IOSTANDARD LVDS [get_ports txn_eth_sfp]

# IIC
set_property PACKAGE_PIN BF9  [get_ports sda] ;#FMC_HPC1_LA01_CC_N
set_property PACKAGE_PIN BF10 [get_ports scl] ;#FMC_HPC1_LA01_CC_P
set_property IOSTANDARD LVCMOS18  [get_ports sda]
set_property IOSTANDARD LVCMOS18  [get_ports scl]
## ------

set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
#get_property RXSLIDE_MODE [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]

create_clock -period 8.00000 -name clk_125 -add [get_ports clk_125_p]

create_clock -period 4.16666 -name gt_refclk -add [get_ports gt_refclk_p]

create_clock -period 6.40000 -name eth_gtrefclk -add [get_ports eth_gtrefclk_p]

#create_clock -period 4.16666 -name clnrxusrclk -add [get_ports clnrxusrclk_in_p]
create_clock -period 4.16666 -name gtfanout -add [get_ports gtfanout_in_p]
create_clock -period 4.16684 -name ddmtdclk -add [get_ports ddmtdclk_in_p]

#set_false_path -from [get_ports clnrxusrclk_in_p] -to [get_pins DDMTD_inst/ddmtd/DMTD_B/gen_straight.clk_i_d0_reg/D]
#set_false_path -from [get_ports gtfanout_in_p]    -to [get_pins DDMTD_inst/ddmtd/DMTD_A/gen_straight.clk_i_d0_reg/D]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_125]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks gt_refclk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks eth_gtrefclk]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/txoutclk_out[0]}]]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *ddmtd/DMTD_A/tag_o_reg*/C}]]

set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_*]
set_output_delay -clock gt_refclk 3.000 [get_ports rxrecclk_*]
#set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_1]
#set_output_delay -clock gt_refclk 3.000 [get_ports txUserClk]

#set_property TX_PROGCLK_SEL PREPI [get_cells {gt_inst/ch[0].inst/inst/gen_gtwizard_gthe4_top.m_gth_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[3].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST}]
#set_property TX_PROGDIV_CFG 20 [get_cells {gt_inst/ch[0].inst/inst/gen_gtwizard_gthe4_top.m_gth_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[3].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST}]
