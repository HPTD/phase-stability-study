from .visa_comm import VisaComm
import time

class PDL(VisaComm):
    # Defining class for VISA (GPIB, USB, etc.) connections

    def __init__(self, rm_visa, str_addr, logger_name, verbose=0):
        VisaComm.__init__(self, rm_visa, str_addr, logger_name)
        self.verbose = verbose

    # Common VISA methods
    
    def clear(self):
        self.send('*CLS')
        return 
    
    def get_id(self):
        # get device ID
        stat = self.query('*IDN?')
        if self.verbose:
            print(stat)
        return stat
    
    def set_delay(self, phase):
        self.send('DEL ' + phase)
        return 
    
    def ask_delay(self):
        stat = self.query('DEL?')
        if self.verbose:
            print(stat)
        return stat
    
    def increment_of(self, phase):
        self.send('INC ' + phase)
        return 
    
    def decrement_of(self, phase):
        self.send('DEC ' + phase)
        return 
    
    def ask_time_elapsed(self):
        stat = self.query('DTIME?')
        if self.verbose:
            print(stat)
        return stat
    
    def setDelay_and_check(self, phase):
        self.set_delay(str(phase)+' ps')
        self.ask_time_elapsed()
        setted = self.ask_delay().split(",")
        count = 0
        while float(phase)*1e-12 != float(setted[0]):
            count += 1
            time.sleep(0.1)
            setted = self.ask_delay()
            if count == 50:
                break
        if count == 50:
            msg = 'error'
            print(msg)
            return msg
        else:
            msg = 'setted + '+str('%.2f' % (float(setted[0])/(1e-12)))+' ps'
            print(msg)
            return msg