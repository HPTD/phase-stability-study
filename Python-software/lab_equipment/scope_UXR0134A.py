#!/usr/bin/env python
# @Leo 2021

from .visa_comm import VisaComm
import time
import numpy as np

# -------------------------------------------------------------
#  ----------------------- Class Scope ------------------------
# -------------------------------------------------------------


class Scope(VisaComm):
    # Defining class for VISA (GPIB, USB, etc.) connections

    def __init__(self, rm_visa, str_addr, logger_name, verbose=0):
        VisaComm.__init__(self, rm_visa, str_addr, logger_name)
        self.verbose = verbose

    # Common VISA methods

    def get_id(self):
        # get device ID
        stat = self.query('*IDN?')
        if self.verbose:
            print(stat)
        return stat

    def reset(self):
        # reset device
        stat = self.query('*RST')
        if self.verbose:
            print(stat)
        return stat

    def clear(self):
        # clear device command queue and register
        stat = self.query('*CLS')
        if self.verbose:
            print(stat)
        return stat

    # Ausiliary methods

    def waitProcessingDone(self, timeout=60):
        # wait for the processing to be done
        time_start = time.time()
        watchdog = 0
        if timeout <= 0:
            timeout = 60
        isDone = self.get_processingDoneRegister()
        while not isDone and not watchdog:
            time.sleep(0.5)
            isDone = self.get_processingDoneRegister()
            total_time = time.time() - time_start
            if total_time > timeout:
                watchdog = 1
        if watchdog:
            if self.verbose:
                print('TIMEOUT during processing')
            return 0
        return 1

    def clearMeasure(self):
        # clear the results of the current measure
        msg = 'MEASure:CLEar'
        return self.send(msg)

    def showEye(self, channel, show):
        # show the eye
        if show:
            msg = ':MTES:FOLD ON, CHAN' + str(channel)
        else:
            msg = ':MTES:FOLD OFF, CHAN' + str(channel)
        return self.send(msg)

    def showEye_funct(self, funct, show):
        # show the eye if the source is a function and not one of the channels
        if show:
            msg = ':MTES:FOLD ON, FUNC' + str(funct)
        else:
            msg = ':MTES:FOLD OFF, FUNC' + str(funct)
        return self.send(msg)

    def displayWaveformArea(self, waveform, state):
        # show the wavefor area on the screen
        if state:
            msg = ':DISP:GRAT:AREA' + str(waveform) + ':STAT ON'
        else:
            msg = ':DISP:GRAT:AREA' + str(waveform) + ':STAT OFF'
        return self.send(msg)

    def autoscaleVertical(self, channel):
        # autoscale the vertical axis
        msg = ':AUT:VERT CHAN' + str(channel)
        return self.send(msg)

    def autoscaleVertical_funct(self, funct):
        # autoscalte the vertical axis when the source is a function
        msg = ':AUT:VERT FUNC' + str(funct)
        return self.send(msg)

    def readData(self, nPoints):
        # read the waveform data
        stat = []
        msg = ':WAVeform:DATa?'
        stat = self.query_binary_values(msg, datatype='f', container=list,
                                        is_big_endian=False,
                                        data_points=nPoints)

        return stat

    # Measurement methods
    
    # inizio edo ----
    
    def set_acq_memory(self, points):
        msg = ':ACQ:POIN:ANAL ' + str(points)
        self.send(msg)
        self.wait_for_opc()	
        return
    
    def set_acq_S_rate(self, rate):
        msg = ':ACQ:SRAT:ANAL ' + str(rate)
        self.send(msg)
        self.wait_for_opc()	
        return 
    
    def set_trig(self, Type = 'EDGE'):
        msg = ':TRIG:MODE' + Type
        self.send(msg)
        self.wait_for_opc()
        return
    
    def meas_edgeToEdge(self, ch1, ch2):
        #measure edgeToEdge ch1 - ch2
        msg = ':MEAS:ETO CHAN' + str(ch1) + ',RIS,MIDD,NEXT,1,CHAN' + str(ch2) + ',RIS,MIDD'
        self.send(msg)
        self.wait_for_opc()
        return 
    
    def set_channel_offset(self, ch, value):
        msg = ':CHAN' + str(ch) + ':OFFS ' + str(value)
        self.send(msg)
        self.wait_for_opc()
        return 
    
    def display_clear(self):
        msg = ':CDisplay'
        self.send(msg)
        self.wait_for_opc()		
        return
    
    def set_timebase(self, time):
        msg = ':TIM:SCAL '+ str(time)
        self.send(msg)
        self.wait_for_opc()		
        return
    
    def autoscale_vert(self, ch):
        msg = ':AUT:VERT CHAN' + str(ch)
        self.send(msg)
        self.wait_for_opc()
        return		
    
    def setup_switch_3measures_edge_to_edge(self, memory = 200e6, timebase = 200e-12):
        #reset scope to default settings
        self.send("*RST\n") 
        #display channels: channel, show (true, false)
        self.set_channelDisplay(4, 1)
        self.set_channelDisplay(2, 1)
        self.set_channelDisplay(1, 1)
        self.set_channelDisplay(3, 1)

        #set channel offsets to 0
        self.set_channel_offset(4, 0) #channel, offset
        self.set_channel_offset(2, 0)
        self.set_channel_offset(1, 0) 
        self.set_channel_offset(3, 0)
        #autoscale amplitudes to fill the display
        self.autoscale_vert(4)
        self.autoscale_vert(2)
        self.autoscale_vert(1)
        self.autoscale_vert(3)

        #scope set up for maximum measurements per time
        self.set_timebase(timebase) #does not matter for edge to edge measurement

        #trigger setup 1: EDGE, from channel 4, threshold in 0
        self.set_trig('EDGE')
        self.set_triggerEdgeSource(4) #channel to set trigger on
        self.set_triggerLevel(4, 0) #trigger threshold: channel, voltage level
        #trigger setup 2: analize all present edges, single mode trigger
        self.send(':ANALyze:AEDGes ON')
        
        self.set_acq_memory(memory) #memory: max value (200 Mb) -> determins number of measurements per trigger event
        self.set_acq_S_rate(128e9)  #sample rate: max value (128 Gbps)
        
        #clean and set single trigger
        self.display_clear()
        self.send(':SINGle')

        #set up measure edge to edge on channel 4 - 1 (fanout illegal data (txusrclk) with ref clk sky)
        self.meas_edgeToEdge(4, 1) 
        #set up measure edge to edge on channel 4 - 2 (rxusrclk with ref clk sky)
        self.meas_edgeToEdge(4, 2)
        #set up measure edge to edge on channel 4 - 3 (cleaned rxusrclk with ref clk sky)
        self.meas_edgeToEdge(4, 3) # displayed first
        # => element 0 is with "clean", element 1 is with "dirty"
        return
    
    # fine edo -----

    def meas_jitter(self):
        # measure jitter parameters
        msg = ':MEAS:RJDJ:ALL?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def meas_sourceVoltageAmplitude(self, channel):
        # measure source voltage amplitude
        msg = ':MEAS:VAMP CHAN' + str(channel)
        return self.send(msg)

    def meas_sourceVoltageAverage(self, channel):
        # measure source voltage average value
        msg = ':MEAS:VAV DISPLAY,CHAN' + str(channel)
        return self.send(msg)

    def meas_sourcePulseAmpliture(self, channel):
        # measure source pulse amplitude
        msg = ':MEAS:PAMP CHAN' + str(channel)
        return self.send(msg)

    def meas_sourceRisetime(self, channel):
        # measure source rise time
        msg = ':MEAS:RIS CHAN' + str(channel)
        return self.send(msg)

    def meas_sourceFalltime(self, channel):
        # measure source fall time
        msg = ':MEAS:FALL CHAN' + str(channel)
        return self.send(msg)

    def meas_sourceOvershot(self, channel, direction):
        # measure source overshot
        possibleDirections = ['RIS', 'RISing', 'FALL', 'FALLing']
        if not (direction in possibleDirections):
            direction = 'RIS'
        msg = ':MEAS:OVER CHAN' + str(channel) + ',' + str(direction)
        return self.send(msg)

    def meas_eyeHeight(self, channel):
        # measure eye height
        msg = ':MEAS:CGR:EHE MEAS,CHAN' + str(channel)
        return self.send(msg)

    def meas_eyeJitter(self, channel, kind):
        # measure eye jitter
        possibleKinds = ['PP', 'RMS']
        if not (kind in possibleKinds):
            kind = 'PP'
        msg = ':MEAS:CGR:JITT ' + str(kind) + ',CHAN' + str(channel)
        return self.send(msg)

    def meas_eyeOLevel(self, channel):
        # measure eye zero-level
        msg = ':MEAS:CGR:OLEV CHAN' + str(channel)
        return self.send(msg)

    def meas_eyeZLevel(self, channel):
        # measure eye high level
        msg = ':MEAS:CGR:ZLEV CHAN' + str(channel)
        return self.send(msg)

    def meas_eyeCrossing(self, channel):
        # measure eye crossing value
        msg = ':MEAS:CGR:CROS CHAN' + str(channel)
        return self.send(msg)

    def meas_eyeQFactor(self, channel):
        # measure eye Q-factor
        msg = ':MEAS:CGR:QFAC CHAN' + str(channel)
        return self.send(msg)

    def meas_sourceVoltageAmplitude_funct(self, funct):
        # measure the source voltage amplitude when the source is a function
        msg = ':MEAS:VAMP FUNC' + str(funct)
        return self.send(msg)

    def meas_sourceVoltageAverage_funct(self, funct):
        # measure the source average voltage amplitude when the source is a function
        msg = ':MEAS:VAV DISPLAY,FUNC' + str(funct)
        return self.send(msg)

    def meas_sourcePulseAmpliture_funct(self, funct):
        # measure the source pulse voltage amplitude when the source is a function
        msg = ':MEAS:PAMP FUNC' + str(funct)
        return self.send(msg)

    def meas_sourceRisetime_funct(self, funct):
        # measure the source rise time when the source is a function
        msg = ':MEAS:RIS FUNC' + str(funct)
        return self.send(msg)

    def meas_sourceFalltime_funct(self, funct):
        # measure the source fall time when the source is a function
        msg = ':MEAS:FALL FUNC' + str(funct)
        return self.send(msg)

    def meas_sourceOvershot_funct(self, funct, direction):
        # measure the source overshoot when the source is a function
        possibleDirections = ['RIS', 'RISing', 'FALL', 'FALLing']
        if not (direction in possibleDirections):
            direction = 'RIS'
        msg = ':MEAS:OVER FUNC' + str(funct) + ',' + str(direction)
        return self.send(msg)

    def meas_eyeHeight_funct(self, funct):
        # measure the height of the eye when the source is a function
        msg = ':MEAS:CGR:EHE MEAS,FUNC' + str(funct)
        return self.send(msg)

    def meas_eyeJitter_funct(self, funct, kind):
        # measure the jitter of the eye when the source is a function
        possibleKinds = ['PP', 'RMS']
        if not (kind in possibleKinds):
            kind = 'PP'
        msg = ':MEAS:CGR:JITT ' + str(kind) + ',FUNC' + str(funct)
        return self.send(msg)

    def meas_eyeOLevel_funct(self, funct):
        # measure the 0-level of the eye when the source is a function
        msg = ':MEAS:CGR:OLEV FUNC' + str(funct)
        return self.send(msg)

    def meas_eyeZLevel_funct(self, funct):
        # measure the 1-level of the eye when the source is a function
        msg = ':MEAS:CGR:ZLEV FUNC' + str(funct)
        return self.send(msg)

    def meas_eyeCrossing_funct(self, funct):
        # measure the eye crossing position when the source is a function
        msg = ':MEAS:CGR:CROS FUNC' + str(funct)
        return self.send(msg)

    def meas_eyeQFactor_funct(self, funct):
        # measure the Q-factor of the eye when the source is a function
        msg = ':MEAS:CGR:QFAC FUNC' + str(funct)
        return self.send(msg)

    # set and get methods

    def get_processingDoneRegister(self):
        # Return 1 when all math and measurements are complete, 0 otherwise
        stat = self.query(':PDER?')
        if self.verbose:
            print(stat)
        return stat

    def get_errorQueue(self):
        # Return the error queue as a string
        msg = ':SYSTem:ERRor? STRing'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_run(self, state):
        # Set the state of the DSA and return the response
        if state:
            msg = ':RUN'
        else:
            msg = ':STOP'
        return self.send(msg)

    def get_state(self):
        # Return the state of the DSA
        msg = ':RSTate?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_systemHeader(self, state):
        # Set the system header
        if state:
            msg = ':SYST:HEAD ON'
        else:
            msg = ':SYST:HEAD OFF'
        return self.send(msg)

    def get_systemHeader(self):
        # Return the state of the system header
        msg = ':SYST"HEAD?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_channelDisplay(self, channel, display):
        # Set the DSA to show the measuring channel
        if display:
            msg = ':CHANnel'+str(channel)+':DISPlay ON'
        else:
            msg = ':CHANnel'+str(channel)+':DISPlay OFF'
        return self.send(msg)

    def set_functDisplay(self, funct, display):
        # Set the DSA to show the measuring function
        if display:
            msg = ':FUNCtion'+str(funct)+':DISPlay ON'
        else:
            msg = ':FUNCtion'+str(funct)+':DISPlay OFF'
        return self.send(msg)

    def get_channelDisplay(self, channel):
        # Return the state of the channel display parameter
        msg = ':CHANnel'+str(channel)+':DISPlay?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_functDisplay(self, funct):
        # Return the state of the function display parameter
        msg = ':FUNCtion'+str(funct)+':DISPlay?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_measureThresholdMethod(self, channel, method):
        possibleMethods = ['ABS', 'PERC', 'HYST']
        if not (method in possibleMethods):
            method = 'HYST'
        msg = ':MEAS:THR:GEN:METH CHAN' + str(channel) + ', ' + method
        return self.send(msg)

    def get_measureThresholdMethod(self, channel):
        msg = ':MEAS:THR:GEN:METH? CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_measureThresholdMethod_funct(self, funct, method):
        possibleMethods = ['ABS', 'PERC', 'HYST']
        if not (method in possibleMethods):
            method = 'HYST'
        msg = ':MEAS:THR:GEN:METH FUNC' + str(funct) + ', ' + method
        return self.send(msg)

    def get_measureThresholdMethod_funct(self, funct):
        msg = ':MEAS:THR:GEN:METH? FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_autoSetThreshold(self, channel):
        msg = ':MEAS:THR:GEN CHAN' + str(channel)
        return self.send(msg)

    def set_autoSetThreshold_funct(self, funct):
        msg = ':MEAS:THR:GEN FUNC' + str(funct)
        return self.send(msg)

    def set_measureStatistic(self, statistic):
        possibleStatistics = ['1', 'ON', 'CURR', 'MAX', 'MEAN', 'MIN', 'STDD',
                              'COUNT']
        if not (statistic in possibleStatistics):
            statistic = 'MEAN'
        msg = ':MEAS:STAT ' + statistic
        return self.send(msg)

    def get_measureStatistic(self):
        msg = ':MEAS:STAT?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_datarate(self, channel, datarate):
        if datarate > 0:
            msg = ':MEAS:DAT CHAN' + str(channel) + ', SEMI, ' +\
                   str(int(datarate))
        else:
            msg = ':MEAS:DAT CHAN' + str(channel)
        return self.send(msg)

    def get_datarate(self, channel):
        self.send(':ANAL:AEDG ON')
        msg = ':MEAS:DAT? CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_unitInterval(self, channel):
        self.send(':ANAL:AEDG ON')
        msg = ':MEAS:UNIT? CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_datarate_funct(self, funct, datarate):
        if datarate > 0:
            msg = ':MEAS:DAT FUNC' + str(funct) + ', SEMI, ' +\
                   str(int(datarate))
        else:
            msg = ':MEAS:DAT FUNC' + str(funct)
        return self.send(msg)

    def get_datarate_funct(self, funct):
        self.send(':ANAL:AEDG ON')
        msg = ':MEAS:DAT? FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_unitInterval_funct(self, funct):
        self.send(':ANAL:AEDG ON')
        msg = ':MEAS:UNIT? FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_measureResults(self):
        msg = ':MEAS:RES?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_clockMethodSource(self, channel):
        msg = ':ANAL:CLOC:METH:SOUR CHAN' + str(channel)
        return self.send(msg)

    def set_clockMethodSource_funct(self, funct):
        msg = ':ANAL:CLOC:METH:SOUR FUNC' + str(funct)
        return self.send(msg)

    def get_clockMethodSource(self):
        msg = ':ANAL:CLOC:METH:SOUR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_acquireAverage(self, state):
        if state:
            msg = ':ACQ:AVER ON'
        else:
            msg = ':ACQ:AVER OFF'
        return self.send(msg)

    def get_acquireAverage(self):
        msg = ':ACQ:AVER?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJSourceChannel(self, channel):
        msg = ':MEAS:RJDJ:SOUR CHAN' + str(channel)
        return self.send(msg)

    def set_RJDJSourceChannel_funct(self, funct):
        msg = ':MEAS:RJDJ:SOUR FUNC' + str(funct)
        return self.send(msg)

    def get_RJDJSourceChannel(self):
        msg = ':MEAS:RJDJ:SOUR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJState(self, state):
        if state:
            msg = ':MEAS:RJDJ:STAT ON'
        else:
            msg = ':MEAS:RJDJ:STAT OFF'
        return self.send(msg)

    def get_RJDJState(self):
        msg = ':MEAS:RJDJ:STAT?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJMethod(self, method):
        possibleMethods = ['SPECtral', 'SPEC', 'BOTH']
        if not (method in possibleMethods):
            method = 'SPEC'
        msg = ':MEAS:RJDJ:METH ' + method
        return self.send(msg)

    def get_RJDJMethod(self):
        msg = ':MEAS:RJDJ:METH?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJMode(self, mode, NUI=1):
        possibleModes = ['TIE', 'PER', 'PERiod', 'NUI']
        if not (mode in possibleModes):
            mode = 'TIE'
        if mode == 'NUI':
            msg = ':MEAS:RJDJ:MODE NUI,' + NUI
        else:
            msg = ':MEAS:RJDJ:MODE ' + mode
        return self.send(msg)

    def get_RJDJMode(self):
        msg = ':MEAS:RJDJ:MODE?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJEdge(self, edge):
        possibleEdges = ['RIS', 'RISing', 'FALL', 'FALLing', 'BOTH']
        if not (edge in possibleEdges):
            edge = 'BOTH'
        msg = ':MEAS:RJDJ:EDGE ' + edge
        return self.send(msg)

    def get_RJDJEdge(self):
        msg = ':MEAS:RJDJ:EDGE?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJPatternLength(self, patternLength):
        if patternLength == 'AUTO':
            msg = ':MEAS:RJDJ:PLEN AUTO'
        else:
            msg = ':MEAS:RJDJ:PLEN ' + patternLength
        return self.send(msg)

    def get_RJDJPatternLength(self):
        msg = ':MEAS:RJDJ:PLEN?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_RJDJBER(self, BER):
        msg = ':MEAS:RJDJ:BER ' + BER
        possibleBERs = ['E6', 'E7', 'E8', 'E9', 'E10', 'E11', 'E12', 'E13',
                        'E14', 'E15', 'E16', 'E17', 'E18', 'J2', 'J4', 'J5',
                        'J9']
        if not (BER in possibleBERs):
            BER = 'E12'
        return self.send(msg)

    def get_RJDJBER(self):
        msg = ':MEAS:RJDJ:BER?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_sourceAverageVoltage(self, channel):
        msg = ':MEAS:VAV? DISP, CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_triggerLevel(self, channel, offset):
        msg = ':TRIG:LEV CHAN' + str(channel) + ',' + str(offset)
        return self.send(msg)

    def get_tiggerLevel(self, channel):
        msg = ':TRIG:LEV? CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_triggerEdgeSource(self, channel):
        msg = ':TRIG:EDGE:SOUR CHAN' + str(channel)
        return self.send(msg)

    def get_tiggerEdgeSource(self):
        msg = ':TRIG:EDGE:SOUR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_measurementThresholdMethod(self, channel, method, rFallThres=10):
        possibleMethods = ['ABS', 'ABSolute', 'PERC', 'PERCent', 'HYST',
                           'HYSTeresis', 'T1090', 'T2080']
        if not (method in possibleMethods):
            method = 'PERC'
        msg = ':MEAS:THR:RFAL:METH CHAN' + str(channel) + ',' + str(method)
        self.send(msg)
        time.sleep(0.1)
        if method == 'PERC':
            rFallThres = int(rFallThres)
            msg = ':MEAS:THR:RFAL:PERC CHAN' + str(channel) + ',' +\
                  str(100-rFallThres) + ',50,' + str(rFallThres)
        self.send(msg)
        return

    def get_measurementThresholdMethod(self, channel):
        msg = ':MEAS:THR:RFAL:METH? CHAN' + str(channel)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_sourceAverageVoltage_funct(self, funct):
        msg = ':MEAS:VAV? DISP, FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_triggerLevel_funct(self, funct, offset):
        msg = ':TRIG:LEV FUNC' + str(funct) + ',' + str(offset)
        return self.send(msg)

    def get_tiggerLevel_funct(self, funct):
        msg = ':TRIG:LEV? FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_triggerEdgeSource_funct(self, funct):
        msg = ':TRIG:EDGE:SOUR FUNC' + str(funct)
        return self.send(msg)

    def set_measurementThresholdMethod_funct(self, funct, method, rFallThres=10):
        possibleMethods = ['ABS', 'ABSolute', 'PERC', 'PERCent', 'HYST',
                           'HYSTeresis', 'T1090', 'T2080']
        if not (method in possibleMethods):
            method = 'PERC'
        msg = ':MEAS:THR:RFAL:METH FUNC' + str(funct) + ',' + str(method)
        self.send(msg)
        time.sleep(0.1)
        if method == 'PERC':
            rFallThres = int(rFallThres)
            msg = ':MEAS:THR:RFAL:PERC FUNC' + str(funct) + ',' +\
                  str(100-rFallThres) + ',50,' + str(rFallThres)
        self.send(msg)
        return

    def get_measurementThresholdMethod_funct(self, funct):
        msg = ':MEAS:THR:RFAL:METH? FUNC' + str(funct)
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    # Incomplete, to improve in next release
    def set_clockPLL(self, datarate, OJTF):
        msg = ''
        possibleOJTF = ['FOPLL', 'SOPLL']
        if not (OJTF in possibleOJTF):
            OJTF = 'SOPLL'
        if OJTF == 'FOPLL':
            msg = ':ANAL:CLOC:METH:OJTF FOPLL,' + str(datarate) + ',' +\
               str(datarate/1667)
        if OJTF == 'SOPLL':
            msg = ':ANAL:CLOC:METH:OJTF SOPLL,' + str(datarate) + ',' +\
                   str(datarate/1667) + ',' + '0.707'
        return self.send(msg)

    def get_clockPLL(self):
        msg = ':ANAL:CLOC:METH:OJTF?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_analogSampleRate(self, rate):
        msg = ':ACQ:SRAT ' + str(rate)
        return self.send(msg)

    def get_analogSampleRate(self):
        msg = ':ACQ:SRAT?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_analogMemoryDepth(self, memoryDepth):
        msg = ':ACQ:POIN ' + str(memoryDepth)
        return self.send(msg)

    def get_analogMemoryDepth(self):
        msg = ':ACQ:POIN?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_UINumber(self):
        msg = ':MTES:FOLD:COUN:UI?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_waveformGrids(self, grids):
        msg = ':DISP:GRAT:NUMB ' + str(grids)
        return self.send(msg)

    def get_waveformGrids(self):
        msg = ':DISP:GRAT:NUMB?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_colorGradePersistence(self, state):
        if state:
            msg = ':DISP:CGR ON'
        else:
            msg = ':DISP:CGR OFF'
        return self.send(msg)

    def get_colorGradePersistence(self):
        msg = ':DISP:CGR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_displayPersistence(self, time):
        if time == 'INF':
            msg = ':DISP:PERS INF'
        if time == 'MIN':
            msg = ':DISP:PERS MIN'
        if type(time) == 'int':
            msg = ':DISP:PERS ' + str(time)
        return self.send(msg)

    def get_displayPersistence(self):
        msg = ':DISP:PERS?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_waveformSource(self, channel):
        msg = ':WAV:SOUR CHAN' + str(channel)
        return self.send(msg)

    def set_waveformSource_funct(self, funct):
        msg = ':WAV:SOUR FUNC' + str(funct)
        return self.send(msg)

    def get_waveformSource(self):
        msg = ':WAV:SOUR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_waveformFormat(self, format):
        msg = ':WAV:FORM ' + str(format)
        return self.send(msg)

    def get_waveformFormat(self):
        msg = ':WAV:FORM?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_waveformView(self, view):
        msg = ':WAV:VIEW ' + str(view)
        return self.send(msg)

    def get_waveformView(self):
        msg = ':WAV:VIEW?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def set_waveformStreaming(self, state):
        if state:
            msg = ':WAV:STR ON'
        else:
            msg = ':WAV:STR OFF'
        return self.send(msg)

    def get_waveformStreaming(self):
        msg = ':WAV:STR?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_waveformCGRWidth(self):
        msg = ':WAV:CGR:WIDT?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_waveformCGRHeight(self):
        msg = ':WAV:CGR:HEIG?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    def get_waveformPreamble(self):
        msg = ':WAV:PRE?'
        stat = self.query(msg)
        if self.verbose:
            print(stat)
        return stat

    # Measurements methods

    def displayAutoscaleChannel(self, channel):
        if channel > 4:
            channel = channel % 4
        # self.reset()
        self.waitProcessingDone(5)
        self.set_systemHeader(0)
        for waveform in range(2, 9):
            self.displayWaveformArea(waveform, 0)
        self.set_waveformGrids(1)
        for ch in range(1, 5):
            if ch != channel:
                self.set_channelDisplay(ch, 0)
            else:
                self.set_channelDisplay(ch, 1)
                self.autoscaleVertical(ch)
        time.sleep(1)
        self.waitProcessingDone(5)

    def displayAutoscaleFunction(self, funct):
        if funct > 4:
            funct = funct % 4
        # self.reset()
        self.waitProcessingDone(5)
        self.set_systemHeader(0)
        for waveform in range(2, 9):
            self.displayWaveformArea(waveform, 0)
        self.set_waveformGrids(1)
        for f in range(1, 5):
            if f != funct:
                self.set_functDisplay(f, 0)
            else:
                self.set_functDisplay(f, 1)
                self.autoscaleVertical_funct(f)
        time.sleep(1)
        self.waitProcessingDone(5)

    def quickEye(self, channel, datarate=0, UIs=1, verbose=0):
        orig_verbose = self.verbose
        self.verbose = verbose
        self.displayAutoscaleChannel(channel)
        self.set_run(1)
        self.clearMeasure()
        self.set_measurementThresholdMethod(channel, 'HYST')
        self.set_autoSetThreshold(channel)
        self.set_datarate(channel, datarate)
        self.set_measureStatistic('MEAN')

        time.sleep(1)

        datarate = float(self.get_measureResults())
        self.clearMeasure()
        self.set_clockMethodSource(channel)
        self.set_clockPLL(datarate, 'SOPLL')
        self.set_analogSampleRate('MAX')
        sRate = float(self.get_analogSampleRate())

        memoryDepth = (sRate/datarate)*50e3
        self.set_analogMemoryDepth(memoryDepth)
        self.showEye(channel, 1)

        time.sleep(0.5)

        UI_no = float(self.get_UINumber())
        stuck = 0
        while (UI_no < UIs and not stuck):
            time.sleep(0.5)
            UI_no = float(self.get_UINumber())
            if UI_no == 0:
                stuck = 1
        self.set_run(0)
        if stuck:
            self.quickEye(channel, datarate, UIs, verbose=1)
        self.verbose = orig_verbose

    def quickEye_funct(self, funct, datarate=0, UIs=1, verbose=0):
        orig_verbose = self.verbose
        self.verbose = verbose
        self.displayAutoscaleFunction(funct)
        self.set_run(1)
        self.clearMeasure()
        self.set_measurementThresholdMethod_funct(funct, 'HYST')
        self.set_autoSetThreshold_funct(funct)
        self.set_datarate_funct(funct, datarate)
        self.set_measureStatistic('MEAN')

        time.sleep(1)

        datarate = float(self.get_measureResults())
        self.clearMeasure()
        self.set_clockMethodSource_funct(funct)
        self.set_clockPLL(datarate, 'SOPLL')
        self.set_analogSampleRate('MAX')
        sRate = float(self.get_analogSampleRate())

        memoryDepth = (sRate/datarate)*50e3
        self.set_analogMemoryDepth(memoryDepth)
        self.showEye_funct(funct, 1)

        time.sleep(0.5)

        UI_no = float(self.get_UINumber())
        stuck = 0
        while (UI_no < UIs and not stuck):
            time.sleep(0.5)
            UI_no = float(self.get_UINumber())
            if UI_no == 0:
                stuck = 1
        self.set_run(0)
        if stuck:
            self.quickEye_funct(funct, datarate, UIs, verbose=1)
        self.verbose = orig_verbose

    def readEye(self, channel, resDivider, verbose=0):
        orig_verbose = self.verbose
        self.verbose = verbose
        resDivider = int(resDivider)
        if resDivider < 1:
            resDivider = 1
        if resDivider > 10:
            resDivider = 10
        self.set_systemHeader(0)
        # self.set_colorGradePersistence(1)
        self.set_displayPersistence('INF')
        self.set_waveformSource_funct(channel)
        self.set_waveformFormat('FLO')
        self.set_waveformView('CGR')
        self.set_waveformStreaming(1)
        width = int(self.get_waveformCGRWidth())
        height = int(self.get_waveformCGRHeight())
        preamble = self.get_waveformPreamble()
        preamble = preamble.split(',')
        dx = float(preamble[4])
        x0 = float(preamble[5])
        dy = float(preamble[7])
        y0 = float(preamble[8])
        try:
            xUnit = int(preamble[20])
            yUnit = int(preamble[21])
        except ValueError:
            xUnit = 2
            yUnit = 1
        if xUnit == 0:
            xUnit = ""
        elif xUnit == 1:
            xUnit = "V"
        elif xUnit == 2:
            xUnit = "s"
        elif xUnit == 4:
            xUnit = "A"
        elif xUnit == 5:
            xUnit = "dB"
        elif xUnit == 8:
            xUnit = "W"

        if yUnit == 0:
            yUnit = ""
        elif yUnit == 1:
            yUnit = "V"
        elif yUnit == 2:
            yUnit = "s"
        elif yUnit == 4:
            yUnit = "A"
        elif yUnit == 5:
            yUnit = "dB"
        elif yUnit == 8:
            yUnit = "W"

        eye = np.array(self.readData(width*height))
        eye = np.resize(eye, (width, height))
        eye = eye.transpose()
        xAxis = (np.linspace(0, width, num=width))*dx+x0
        yAxis = (np.linspace(0, height, num=height))*dy+y0
        self.verbose = orig_verbose
        return eye, xAxis, yAxis, xUnit, yUnit

    def readJitter(self, channel, datarate=0, transitions=1e6, verbose=0):
        orig_verbose = self.verbose
        self.verbose = verbose
        self.set_run(1)
        self.set_channelDisplay(channel, 1)
        self.clearMeasure()
        self.set_acquireAverage(0)

        self.set_RJDJSourceChannel(channel)
        self.set_RJDJState(1)
        self.set_RJDJMethod('SPEC')
        self.set_RJDJMode('TIE')
        self.set_RJDJEdge('BOTH')
        self.set_RJDJPatternLength('AUTO')
        self.set_RJDJBER('E12')

        self.set_measurementThresholdMethod(channel, 'HYST')
        self.set_autoSetThreshold(channel)
        self.set_datarate(channel, datarate)
        self.set_measureStatistic('MEAN')

        time.sleep(1)

        datarate = float(self.get_measureResults())
        stuck = 0
        while datarate == 0 and not stuck:
            time.sleep(1)
            datarate = float(self.get_measureResults())
            stuck = 1
        if stuck:
            print('Failed measuring datarate during Jitter measurement.\
                  Run aborted')
            return

        self.clearMeasure()
        self.set_clockMethodSource(channel)
        self.set_clockPLL(datarate, 'SOPLL')
        self.set_analogSampleRate('MAX')
        sRate = float(self.get_analogSampleRate())

        memoryDepth = (sRate/datarate)*50e3
        self.set_analogMemoryDepth(memoryDepth)
        time.sleep(0.5)
        transitions_no = self.meas_jitter()
        time.sleep(2)

        transitions_no = self.meas_jitter()
        transitions_no = transitions_no.split(',')
        transitions_no = np.round(float(transitions_no[25]))
        if transitions_no > 1e20:
            print('New Attempt')
            self.readJitter(channel, datarate, transitions)
        stuck = 0
        while (transitions_no < transitions and not stuck):
            time.sleep(0.5)
            transitions_no = self.meas_jitter()
            transitions_no = transitions_no.split(',')
            transitions_no = np.round(float(transitions_no[25]))
            results = self.meas_jitter()
            if transitions_no == 0:
                stuck = 1

        self.set_run(0)
        if stuck:
            print('Failed measuring datarate during Jitter measurement.\
                  Run aborted')
            return

        results = self.meas_jitter()
        results = results.split(',')

        jitterParamName = []
        jitterParamValue = []
        for param in range(0, 11):
            jitterParamName.append(results[param*3])
            jitterParamValue.append(results[param*3+1])

        self.verbose = orig_verbose

        return jitterParamValue, jitterParamName

    def readJitter_funct(self, funct, datarate=0, transitions=1e6, verbose=0):
        orig_verbose = self.verbose
        self.verbose = verbose
        self.set_run(1)
        self.set_functDisplay(funct, 1)
        self.clearMeasure()
        self.set_acquireAverage(0)

        self.set_RJDJSourceChannel_funct(funct)
        self.set_RJDJState(1)
        self.set_RJDJMethod('SPEC')
        self.set_RJDJMode('TIE')
        self.set_RJDJEdge('BOTH')
        self.set_RJDJPatternLength('AUTO')
        self.set_RJDJBER('E12')

        self.set_measurementThresholdMethod_funct(funct, 'HYST')
        self.set_autoSetThreshold_funct(funct)
        self.set_datarate_funct(funct, datarate)
        self.set_measureStatistic('MEAN')

        time.sleep(1)

        datarate = float(self.get_measureResults())
        stuck = 0
        while datarate == 0 and not stuck:
            time.sleep(1)
            datarate = float(self.get_measureResults())
            stuck = 1
        if stuck:
            print('Failed measuring datarate during Jitter measurement.\
                  Run aborted')
            return

        self.clearMeasure()
        self.set_clockMethodSource_funct(funct)
        self.set_clockPLL(datarate, 'SOPLL')
        self.set_analogSampleRate('MAX')
        sRate = float(self.get_analogSampleRate())

        memoryDepth = (sRate/datarate)*50e3
        self.set_analogMemoryDepth(memoryDepth)
        time.sleep(0.5)
        transitions_no = self.meas_jitter()
        time.sleep(2)

        transitions_no = self.meas_jitter()
        transitions_no = transitions_no.split(',')
        transitions_no = np.round(float(transitions_no[25]))
        if transitions_no > 1e20:
            print('New Attempt')
            self.readJitter_funct(funct, datarate, transitions)
        stuck = 0
        while (transitions_no < transitions and not stuck):
            time.sleep(0.5)
            transitions_no = self.meas_jitter()
            transitions_no = transitions_no.split(',')
            transitions_no = np.round(float(transitions_no[25]))
            results = self.meas_jitter()
            if transitions_no == 0:
                stuck = 1

        self.set_run(0)
        if stuck:
            print('Failed measuring datarate during Jitter measurement.\
                  Run aborted')
            return

        results = self.meas_jitter()
        results = results.split(',')

        jitterParamName = []
        jitterParamValue = []
        for param in range(0, 11):
            jitterParamName.append(results[param*3])
            jitterParamValue.append(results[param*3+1])

        self.verbose = orig_verbose

        return jitterParamValue, jitterParamName

    def readEyeParam(self, channel, rFallThres=10, gateWidth=50, verbose=0):

        orig_verbose = self.verbose
        self.verbose = verbose

        eyeParamNames = ['Eye Amplitude', 'Eye Height', 'Eye Average',
                         'Rise Time', 'Fall Time', 'Overshoot Rising',
                         'Overshoot Falling', 'Jitter Peak-to-Peak',
                         'Jitter RMS', 'Pulse Amplitude', 'Zero Level',
                         'One Level', 'Crossing Level', 'Q Factor']
        eyeParamValues = [0]*14

        self.set_run(1)
        self.set_channelDisplay(channel, 1)
        self.set_acquireAverage(0)

        self.set_RJDJState(0)
        self.waitProcessingDone()

        offset = self.get_sourceAverageVoltage(channel)
        self.set_triggerLevel(channel, offset)
        self.set_triggerEdgeSource(channel)

        self.clearMeasure()
        self.set_measureStatistic('MEAN')
        self.set_measurementThresholdMethod(channel, 'PERC', rFallThres)

        self.meas_sourceVoltageAmplitude(channel)
        self.meas_sourceVoltageAverage(channel)
        self.meas_sourceRisetime(channel)
        self.meas_sourceFalltime(channel)
        self.meas_sourceOvershot(channel, 'RIS')
        self.meas_sourceOvershot(channel, 'FALL')
        self.meas_sourcePulseAmpliture(channel)
        self.waitProcessingDone()

        results = self.get_measureResults()
        results = results.split(',')
        eyeParamValues[0] = float(results[6])
        eyeParamValues[2] = float(results[5])
        eyeParamValues[3] = float(results[4])
        eyeParamValues[4] = float(results[3])
        eyeParamValues[5] = float(results[2])
        eyeParamValues[6] = float(results[1])
        eyeParamValues[9] = float(results[0])

        if self.get_colorGradePersistence():
            self.clearMeasure()
            self.meas_eyeHeight(channel)
            self.meas_eyeJitter(channel, 'PP')
            self.meas_eyeOLevel(channel)
            self.meas_eyeZLevel(channel)
            self.meas_eyeCrossing(channel)
            self.meas_eyeQFactor(channel)
            self.waitProcessingDone()

            results = self.get_measureResults()
            results = results.split(',')
            eyeParamValues[1] = float(results[5])
            eyeParamValues[7] = float(results[4])
            eyeParamValues[10] = float(results[3])
            eyeParamValues[11] = float(results[2])
            eyeParamValues[12] = float(results[1])
            eyeParamValues[13] = float(results[0])

            time.sleep(1)

            self.clearMeasure()
            self.meas_eyeJitter(channel, 'RMS')
            self.waitProcessingDone()
            results = self.get_measureResults()
            eyeParamValues[8] = float(results)

        self.verbose = orig_verbose

        return eyeParamValues, eyeParamNames

    def readEyeParam_funct(self, funct, rFallThres=10, gateWidth=50, verbose=0):

        orig_verbose = self.verbose
        self.verbose = verbose

        eyeParamNames = ['Eye Amplitude', 'Eye Height', 'Eye Average',
                         'Rise Time', 'Fall Time', 'Overshoot Rising',
                         'Overshoot Falling', 'Jitter Peak-to-Peak',
                         'Jitter RMS', 'Pulse Amplitude', 'Zero Level',
                         'One Level', 'Crossing Level', 'Q Factor']
        eyeParamValues = [0]*14

        self.set_run(1)
        self.set_functDisplay(funct, 1)
        self.set_acquireAverage(0)

        self.set_RJDJState(0)
        self.waitProcessingDone()

        offset = self.get_sourceAverageVoltage_funct(funct)
        self.set_triggerLevel_funct(funct, offset)
        self.set_triggerEdgeSource_funct(funct)

        self.clearMeasure()
        self.set_measureStatistic('MEAN')
        self.set_measurementThresholdMethod_funct(funct, 'PERC', rFallThres)

        self.meas_sourceVoltageAmplitude_funct(funct)
        self.meas_sourceVoltageAverage_funct(funct)
        self.meas_sourceRisetime_funct(funct)
        self.meas_sourceFalltime_funct(funct)
        self.meas_sourceOvershot_funct(funct, 'RIS')
        self.meas_sourceOvershot_funct(funct, 'FALL')
        self.meas_sourcePulseAmpliture_funct(funct)
        self.waitProcessingDone()

        results = self.get_measureResults()
        results = results.split(',')
        eyeParamValues[0] = float(results[6])
        eyeParamValues[2] = float(results[5])
        eyeParamValues[3] = float(results[4])
        eyeParamValues[4] = float(results[3])
        eyeParamValues[5] = float(results[2])
        eyeParamValues[6] = float(results[1])
        eyeParamValues[9] = float(results[0])

        if self.get_colorGradePersistence():
            self.clearMeasure()
            self.meas_eyeHeight_funct(funct)
            self.meas_eyeJitter_funct(funct, 'PP')
            self.meas_eyeOLevel_funct(funct)
            self.meas_eyeZLevel_funct(funct)
            self.meas_eyeCrossing_funct(funct)
            self.meas_eyeQFactor_funct(funct)
            self.waitProcessingDone()

            results = self.get_measureResults()
            results = results.split(',')
            eyeParamValues[1] = float(results[5])
            eyeParamValues[7] = float(results[4])
            eyeParamValues[10] = float(results[3])
            eyeParamValues[11] = float(results[2])
            eyeParamValues[12] = float(results[1])
            eyeParamValues[13] = float(results[0])

            time.sleep(1)

            self.clearMeasure()
            self.meas_eyeJitter_funct(funct, 'RMS')
            self.waitProcessingDone()
            results = self.get_measureResults()
            eyeParamValues[8] = float(results)

        self.verbose = orig_verbose

        return eyeParamValues, eyeParamNames
