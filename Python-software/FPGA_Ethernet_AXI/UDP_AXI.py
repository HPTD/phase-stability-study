#Python functions used to perform transparent AXI MM Transactions by UDP commands, exploiting the libraries
#(https://gitlab.cern.ch/fmartina/udp_to_axis.git) and (https://gitlab.cern.ch/fmartina/axis_to_axi.git)

import socket

class UDP_AXI:
        def __init__(self, pc_IP, board_IP, pc_UDP_port, board_UDP_port):
            self.pc_IP = pc_IP
            self.board_IP = board_IP
            self.pc_UDP_port = pc_UDP_port
            self.board_UDP_port = board_UDP_port

        # Bind the socket
        def UDP_bind(self):
            self.sok = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            self.sok.bind((self.pc_IP, self.pc_UDP_port))

        # Register accessing functions, according to the FPGA implementation
        def AXI_read(self, address):
            read_cmd_word = (0).to_bytes(4, 'big')
            address_bytes = address.to_bytes(4, 'big')
            self.sok.sendto(read_cmd_word + address_bytes, (self.board_IP, self.board_UDP_port))
            reg_val_bytes = self.sok.recvfrom(4)
            return int.from_bytes(reg_val_bytes[0], "big")

        def AXI_write(self, address, value):
            write_cmd_word = (0xFFFFFFFF).to_bytes(4, 'big')
            address_bytes = address.to_bytes(4, 'big')
            address_value = value.to_bytes(4, 'big')
            self.sok.sendto(write_cmd_word + address_bytes + address_value, (self.board_IP, self.board_UDP_port))

        #Unbind the socket
        def UDP_unbind(self):
            self.sok.close()
            
        #Destructor
        def __del__(self):
            self.UDP_unbind()