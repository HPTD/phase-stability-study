import time

class Si750:
    
    def loadSi570conf(board_iic, rx, print_regs):
        
        deep_print = False
        
        IIC_SWITCH_ADDR = 0b11101000

        SEL_EXPANDER    = 0b00000100
        SEL_SI570       = 0b00000010
        SEL_SI5344B     = 0b00001000

        EXPANDER_ADDR   = 0b01110000 #### last = 0 -> write
        SI570_ADDR      = 0b10101010 #### last = 0 -> write
        SI5344B_ADDR    = 0b11010000 #### last = 0 -> write

        EXPANDER_CONF_R = 0b00110000
        EXPANDER_CONF   = 0b00001111
        
        ### Reading values from Si570: slave has 7 bit address (+R/W), registers have 8 bit address
        HS_DIV_2_0__N1_6_2_ADDR = 0b00000111 #### 7  
        N1_1_0__FREQ_37_32_ADDR = 0b00001000 #### 8 
        FREQ_31_24_ADDR         = 0b00001001 #### 9 
        FREQ_23_16_ADDR         = 0b00001010 #### 10
        FREQ_15_8_ADDR          = 0b00001011 #### 11
        FREQ_7_0_ADDR           = 0b00001100 #### 12 

        REG135_ADDR             = 0b10000111 #### 135
        REG137_ADDR             = 0b10001001 #### 137

        board_iic.write(IIC_SWITCH_ADDR, [SEL_SI570])

        board_iic.write(SI570_ADDR, [HS_DIV_2_0__N1_6_2_ADDR])
        REG7  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [N1_1_0__FREQ_37_32_ADDR])
        REG8  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_31_24_ADDR])
        REG9  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_23_16_ADDR])
        REG10 = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_15_8_ADDR])
        REG11 = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_7_0_ADDR])
        REG12 = board_iic.read(SI570_ADDR, 1)[0]
        
        ### refer to datasheet to select HS_DIV, N1 & RFREQ
        ### help with interactive api on https://www.changpuak.ch/electronics/Si570.php
        HSDIV_R = (REG7 & 0b11100000)>>5
        HS_DIV  = 5

        N1_1_0  = REG8 & 0b11000000
        N1_6_2  = REG7 & 0b00011111
        N1_R    = N1_1_0>>6 | N1_6_2<<2
        N1      = N1_R + 1

        RFREQ_R = REG12 | REG11<<8 | REG10<<8*2 | REG9<<8*3 | (REG8 & 0b00111111)<<8*4
        RFREQ   = RFREQ_R/pow(2,28)

        if rx:
            FOUT  = 99.999340*(1e6)
        else:
            FOUT  = 99.999535*(1e6)
        FXTAL = (FOUT * HS_DIV * N1) / RFREQ

        if print_regs:
            if deep_print:
                print('REG7 ', hex(REG7 ))
                print('REG8 ', hex(REG8 ))
                print('REG9 ', hex(REG9 ))
                print('REG10', hex(REG10))
                print('REG11', hex(REG11))
                print('REG12', hex(REG12))
                print('')
                print('HSDIV_R', HSDIV_R)
                print('N1_01  ', N1_1_0 )
                print('N1_62  ', N1_6_2 )
                print('N1_R   ', N1_R   )
                print('RFREQ_R', RFREQ_R)
                print('')
                print('N1   ', N1   )
                print('RFREQ', RFREQ)
                print('')
            print('Real FXTAL ', FXTAL/(1e6), 'MHz')

        new_N1 = 4
        new_N1_R = new_N1 - 1
        s = (FXTAL * RFREQ)/(HS_DIV * new_N1)
        if print_regs and deep_print:
            print('newFout_withNewN1 ', s/(1e6), 'MHz')

        new_RFREQ = (new_N1 * HS_DIV * 240*1e6) / FXTAL
        if print_regs:
            if deep_print:
                print('new_RFREQ', new_RFREQ)
            print('new_Fout', ((FXTAL * new_RFREQ)/(HS_DIV * new_N1))/(1e6), 'MHz\n')

        hex_intRFREQ = hex(int(new_RFREQ * pow(2,28)))
        strLen = len(hex_intRFREQ)
        if print_regs and deep_print:
            print('hex_intRFREQ ', hex_intRFREQ)

        new_REG12_hex  = hex_intRFREQ[strLen-2 : strLen  ]
        new_REG11_hex  = hex_intRFREQ[strLen-4 : strLen-2]
        new_REG10_hex  = hex_intRFREQ[strLen-6 : strLen-4]
        new_REG9_hex   = hex_intRFREQ[strLen-8 : strLen-6]
        new_REG8_f_hex = hex_intRFREQ[strLen-9 : strLen-8]

        if print_regs:
            print(new_REG12_hex)
            print(new_REG11_hex)
            print(new_REG10_hex)
            print(new_REG9_hex)
            print(new_REG8_f_hex, '\n')

        new_REG12  = int(new_REG12_hex,  16)
        new_REG11  = int(new_REG11_hex,  16)
        new_REG10  = int(new_REG10_hex,  16)
        new_REG9   = int(new_REG9_hex,   16)
        new_REG8_f = int(new_REG8_f_hex, 16) & 0b00111111

        new_REG7_n = (new_N1_R & 0b01111100)>>2
        new_REG8_n =  new_N1_R & 0b00000011

        #new_REG7 = int(hex(HS_DIV    )[2:] + hex(new_REG7_n)[2:], 16)
        #new_REG8 = int(hex(new_REG8_n)[2:] + hex(new_REG8_f)[2:], 16)
        new_REG7 = HSDIV_R<<5 | new_REG7_n
        new_REG8 = new_REG8_n<<6 | new_REG8_f

        if print_regs:
            if deep_print:
                print('new_REG7_n ', hex(new_REG7_n))
                print('new_REG8_n ', hex(new_REG8_n))
                print('new_REG8_f ', hex(new_REG8_f), '\n')
            print('new_REG7   ', hex(new_REG7), new_REG7)
            print('new_REG8   ', hex(new_REG8), new_REG8)
            print('new_REG9   ', hex(new_REG9), new_REG9)
            print('new_REG10  ', hex(new_REG10), new_REG10)
            print('new_REG11  ', hex(new_REG11), new_REG11)
            print('new_REG12  ', hex(new_REG12), new_REG12)
            
        board_iic.write(IIC_SWITCH_ADDR, [SEL_SI570])

        #### Freeze DCO
        board_iic.write(SI570_ADDR, [REG137_ADDR])
        REG137 = board_iic.read(SI570_ADDR, 1)[0]
        new_REG137 = REG137 | 0b00010000
        if print_regs and deep_print:
            print(REG137, new_REG137)
        board_iic.write(SI570_ADDR, [REG137_ADDR, new_REG137])

        #### Write HS_DIV, N1 & RFREQ registers
        board_iic.write(SI570_ADDR, [HS_DIV_2_0__N1_6_2_ADDR, new_REG7])
        board_iic.write(SI570_ADDR, [N1_1_0__FREQ_37_32_ADDR, new_REG8])
        board_iic.write(SI570_ADDR, [FREQ_31_24_ADDR, new_REG9])
        board_iic.write(SI570_ADDR, [FREQ_23_16_ADDR, new_REG10])
        board_iic.write(SI570_ADDR, [FREQ_15_8_ADDR, new_REG11])
        board_iic.write(SI570_ADDR, [FREQ_7_0_ADDR, new_REG12])

        #### Unfreeze DCO
        board_iic.write(SI570_ADDR, [REG137_ADDR])
        REG137 = board_iic.read(SI570_ADDR, 1)[0]
        new_REG137 = REG137 & 0b11101111
        if print_regs and deep_print:
            print(REG137, new_REG137)
        board_iic.write(SI570_ADDR, [REG137_ADDR, new_REG137])

        #### Write reg135 for NewFreq bit
        board_iic.write(SI570_ADDR, [REG135_ADDR])
        REG135 = board_iic.read(SI570_ADDR, 1)[0]
        new_REG135 = REG135 | 0b01000000
        if print_regs and deep_print:
            print(REG135, new_REG135)
        board_iic.write(SI570_ADDR, [REG135_ADDR, new_REG135])

        #### Verify reading
        time.sleep(5)
        board_iic.write(SI570_ADDR, [HS_DIV_2_0__N1_6_2_ADDR])
        REG7  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [N1_1_0__FREQ_37_32_ADDR])
        REG8  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_31_24_ADDR])
        REG9  = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_23_16_ADDR])
        REG10 = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_15_8_ADDR])
        REG11 = board_iic.read(SI570_ADDR, 1)[0]
        board_iic.write(SI570_ADDR, [FREQ_7_0_ADDR])
        REG12 = board_iic.read(SI570_ADDR, 1)[0]

        board_iic.write(SI570_ADDR, [REG135_ADDR])
        REG135 = board_iic.read(SI570_ADDR, 1)[0]

        if print_regs:
            print('REG7 ', hex(REG7  ))
            print('REG8 ', hex(REG8  ))
            print('REG9 ', hex(REG9  ))
            print('REG10', hex(REG10 ))
            print('REG11', hex(REG11 ))
            print('REG12', hex(REG12 ), '\n')
            if deep_print:
                print('REG135',hex(REG135), '\n')
            
        valid_a = REG12 == new_REG12 and REG11 == new_REG11 and REG10 == new_REG10
        valid_b = REG9 == new_REG9 and REG8 == new_REG8 and REG7 == new_REG7
        valid   = valid_a and valid_b
        
        if not(valid):
            print('FAILED \n')
        
        return valid
                