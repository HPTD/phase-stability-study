from FPGA_Ethernet_AXI.UDP_AXI import *
from FPGA_Ethernet_AXI.AXI_IIC import *
from FPGA_Ethernet_AXI.Si570_setup import *

import time
import csv
from datetime import datetime
import statistics

class FPGA_ctrl:
    
    ### Tx FPGA udp-axi interface ports declaration
    Tx_pc_IP = "10.0.0.1"
    Tx_board_IP = "10.1.0.3"
    Tx_pc_UDP_port = 6103
    Tx_board_UDP_port = 5103

    ### Rx FPGA udp-axi interface ports declaration
    Rx_pc_IP = "10.0.0.1"
    Rx_board_IP = "10.1.0.2"
    Rx_pc_UDP_port = 6102
    Rx_board_UDP_port = 5102
    
    ### Rx HOP FPGA udp-axi interface ports declaration
    Rx_HOP_pc_IP = "10.0.0.1"
    Rx_HOP_board_IP = "10.1.0.4"
    Rx_HOP_pc_UDP_port = 6104
    Rx_HOP_board_UDP_port = 5104
    
    GPIO_REG_ADDR       = 0xFFFF0000 # 0x0000
    GPIO_DDMTD_REG_ADDR = 0x00000000 
    GPIO_TXPI_REG_ADDR  = 0x10000000 
    GPIO_HOP_REG_ADDR   = 0x20000000 
    GPIO_HOP2_REG_ADDR  = 0x30000000 
    IIC_master_base_address = 0xF0000000
    
    def __init__(self):
        self.Tx_udp_interface = UDP_AXI(self.Tx_pc_IP, self.Tx_board_IP, self.Tx_pc_UDP_port, self.Tx_board_UDP_port)
        self.Rx_udp_interface = UDP_AXI(self.Rx_pc_IP, self.Rx_board_IP, self.Rx_pc_UDP_port, self.Rx_board_UDP_port)
        #self.Rx_HOP_udp_interface = UDP_AXI(self.Rx_HOP_pc_IP, self.Rx_HOP_board_IP, self.Rx_HOP_pc_UDP_port, self.Rx_HOP_board_UDP_port)
        self.txiic = AXI_IIC(self.Tx_udp_interface, self.IIC_master_base_address)
        self.rxiic = AXI_IIC(self.Rx_udp_interface, self.IIC_master_base_address)
        #self.rxhopiic = AXI_IIC(self.Rx_HOP_udp_interface, self.IIC_master_base_address)
    
    def connect(self, fpga):
        options = ['Tx', 'tx', 'Rx', 'rx', 'HOP', 'hop']
        if not(fpga in options):
            return 'errore: inserire Tx o Rx'
        if fpga == 'Tx' or fpga == 'tx':
            self.Tx_udp_interface.UDP_bind()
        if fpga == 'Rx' or fpga == 'rx':
            self.Rx_udp_interface.UDP_bind()
        if fpga == 'HOP' or fpga == 'hop':
            self.Rx_HOP_udp_interface.UDP_bind()
            
    #### hop RxFPGA controls
    def hop_rx_reset(self):
        self.Rx_HOP_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x5)
        time.sleep(0.1)
        self.Rx_HOP_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x4)
        
    def hop_rx_default(self):
        self.Rx_HOP_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x4)
        
    def hop_rx_disable_alignment(self):
        self.Rx_HOP_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x0)
        
    def hop_rx_read(self, addr=0):
        if addr == 0:
            gpio_data = self.Rx_HOP_udp_interface.AXI_read(self.GPIO_REG_ADDR + 0x0008) #address GPIO2
            gpio_bits = bin(gpio_data)[2:].zfill(32)
            valid     = int(gpio_bits[31], 2)
            slides    = int(gpio_bits[0:7], 2)
            lols      = int(gpio_bits[7:15], 2)
            lol_ovf   = int(gpio_bits[15], 2)
            odds      = int(gpio_bits[16:21], 2)
            temp      = int(gpio_bits[21:31], 2)
            return [valid, slides, odds, temp, lols, lol_ovf]

    #### TxFPGA controls
    #### GPIO_REG[3 downto 0] values: [tx_enc_bypass, rx_aligner_en, tx_reset, rx_reset]
    # Tx -   default: 0x0    bypass: 0x8    reset: 0x2 (shared QPLL)
        
    def tx_reset(self):
        self.Tx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x2)
        time.sleep(0.1)
        self.Tx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x0)
    
    def tx_switch_to_illegal(self):
        self.Tx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x8)
        
    def tx_default(self):
        self.Tx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x0)
        
    def tx_read(self, ddmtd = False):
        if ddmtd:
            gpio_data = self.Tx_udp_interface.AXI_read(self.GPIO_DDMTD_REG_ADDR + 0x0008) #address GPIO2
            return gpio_data
        else:
            gpio_data = self.Tx_udp_interface.AXI_read(self.GPIO_REG_ADDR + 0x0008) #address GPIO2
            gpio_bits = bin(gpio_data)[2:].zfill(32)
            valid     = int(gpio_bits[31], 2)
            slides    = int(gpio_bits[0:7], 2)
            lols      = int(gpio_bits[7:15], 2)
            lol_ovf   = int(gpio_bits[15], 2)
            odds      = int(gpio_bits[16:21], 2)
            temp      = int(gpio_bits[21:31], 2)
            return [valid, slides, odds, temp, lols, lol_ovf]
    
    #### RxFPGA controls 
    #### GPIO_REG[3 downto 0] values: [tx_enc_bypass, rx_aligner_en, tx_reset, rx_reset]
    #### Rx -   default: 0x4    reset: 0x5    not_en_ali: 0x0
    
    rx_running_conf   = 0x4 #### Used to configure the equalizer parameters & ddmtd reset without touching the running configuration
    txpi_running_conf = 0x0 #### Used to configure txpi piloted shifts and tx_phase_aligner
    hop_running_conf  = 0x0
    
    def rx_reset(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x5)
        time.sleep(0.1)
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x4)
        self.rx_running_conf = 0x4
    
    def rx_disable_alignment(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x0)
        self.rx_running_conf = 0x0
        
    def rx_default(self): 
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, 0x4)
        self.rx_running_conf = 0x4
        
    #### RxFPGA - txpi gt controls
    def rx_txpi_default(self): #### pi calibration starts again
        self.Rx_udp_interface.AXI_write(self.GPIO_DDMTD_REG_ADDR, 0x0)
        self.txpi_running_conf = 0x0
        
    def rx_txpi_pi_keep_rst(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x4)
        
    def rx_txpi_pi_rdy(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x0)
        
    def rx_txpi_calib_mode(self): #### needed after first automatic calibration (done over rx_txpi_default)
        gpio_pi_new_value = 1<<6  #### this '1' set the system in UI alignment mode, use the value of firts calibration (if '0' does new calibration)
        self.Rx_udp_interface.AXI_write(self.GPIO_DDMTD_REG_ADDR, gpio_pi_new_value)
        self.txpi_running_conf = gpio_pi_new_value
        
    def rx_txpi_shift(self, n_steps, up): #### n_steps has to be < 16
        if n_steps == 0:
            return 0
        gpio_pi_cmnd = (1<<5 if up else 0) | n_steps<<1 | 1
        gpio_pi_new_value = gpio_pi_cmnd | self.txpi_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_DDMTD_REG_ADDR, gpio_pi_new_value)
        done = 0; esc = 0
        while done == 0:
            done = self.rx_txpi_read()[1] #### firmware set step_done to '0' when writing '0' (running config) in start_shift bit
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        self.Rx_udp_interface.AXI_write(self.GPIO_DDMTD_REG_ADDR, self.txpi_running_conf) #### if '0' it causes new calibration
        #self.txpi_running_conf = 0x0 #### friting '0' would do new calibration (saving new tx_pi_phase)
        return done

    def rx_txpi_read(self):
        txpi_status = self.Rx_udp_interface.AXI_read(self.GPIO_TXPI_REG_ADDR + 0x0008) #address GPIO2
        gpio_bits = bin(txpi_status)[2:].zfill(32)
        ali_done  = int(gpio_bits[31], 2)
        step_done = int(gpio_bits[30], 2)
        phase_acc = int(gpio_bits[23:30], 2)
        return [ali_done, step_done, phase_acc]
    
    def rx_txpi_ali_check_and_read(self):
        [ali_done, step_done, phase_acc] = self.rx_txpi_read()
        esc = 0
        while ali_done == 0:
            ali_done = 1
            for sure in range(100):
                ali_done = ali_done * self.rx_txpi_read()[0]
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        [x, step_done, phase_acc] = self.rx_txpi_read()
        return [ali_done, step_done, phase_acc]
    
    def rx_txpi_tx_reset(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x1)
        time.sleep(0.1)
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x0)
        
    def rx_txpi_fine_realign(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x2) #### rising edge causes fine realignment to the half response
        self.Rx_udp_interface.AXI_write(self.GPIO_TXPI_REG_ADDR, 0x0)
        [ali_done, step_done, phase_acc] = self.rx_txpi_ali_check_and_read()
        return [ali_done, step_done, phase_acc]
    
    #### RxFPGA - HOP GT controls
    def rx_hop_pi_default(self): #### starts first calibration
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, 0x0)
        self.hop_running_conf = 0x0
        
    def rx_hop_calib_mode(self): 
        gpio_pi_new_value = 1<<2  #### this '1' set the system in UI alignment mode, use the value of firts calibration (if '0' does new calibration)
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, gpio_pi_new_value)
        self.hop_running_conf = gpio_pi_new_value
        
    def rx_hop_fine_realign(self):
        gpio_pi_new_value = 1<<1 | self.hop_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, gpio_pi_new_value) #### rising edge causes fine realignment to the half response
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, self.hop_running_conf)
        
    def rx_hop_gt_reset(self):
        gpio_pi_new_value = 0x1 | self.hop_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, gpio_pi_new_value)
        time.sleep(0.1)
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, self.hop_running_conf)
        
    def rx_hop_pi_keep_rst(self):
        gpio_pi_new_value = 1<<3 | self.hop_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, gpio_pi_new_value)
        
    def rx_hop_pi_rdy(self):
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, self.hop_running_conf)
        
    def rx_hop_shift(self, n_steps, up): #### n_steps has to be < 16
        if n_steps == 0:
            return 0
        gpio_pi_cmnd = (1<<5 if up else 0) | n_steps<<1 | 1
        gpio_pi_new_value = gpio_pi_cmnd<<4 | self.hop_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, gpio_pi_new_value)
        done = 0; esc = 0
        while done == 0:
            done = self.rx_hop_read()[1] #### firmware set step_done to '0' when writing '0' (running config) in start_shift bit
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP_REG_ADDR, self.hop_running_conf) #### if '0' it causes new calibration
        #self.hop_running_conf = 0x0 #### friting '0' would do new calibration (saving new tx_pi_phase)
        return done
    
    def rx_hop_UI_align(self, n_UI):
        gpio_cmnd = n_UI if n_UI > 0 else 0x3F
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP2_REG_ADDR, gpio_cmnd)
        done = self.rx_hop_ali_check_and_read()[0]
        self.Rx_udp_interface.AXI_write(self.GPIO_HOP2_REG_ADDR, 0x0)
        return done

    def rx_hop_read(self):
        txpi_status = self.Rx_udp_interface.AXI_read(self.GPIO_TXPI_REG_ADDR + 0x0008) #address GPIO2
        gpio_bits = bin(txpi_status)[2:].zfill(32)
        ali_done  = int(gpio_bits[31-9], 2)
        step_done = int(gpio_bits[31-10], 2)
        return [ali_done, step_done]
    
    def rx_hop_ali_check_and_read(self):
        [ali_done, step_done] = self.rx_hop_read()
        esc = 0
        while ali_done == 0:
            ali_done = 1
            for sure in range(100):
                ali_done = ali_done * self.rx_hop_read()[0]
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        [x, step_done] = self.rx_hop_read()
        return [ali_done, step_done]
    
    def rx_txrdy_read(self):
        txpi_status = self.Rx_udp_interface.AXI_read(self.GPIO_TXPI_REG_ADDR + 0x0008) #address GPIO2
        gpio_bits = bin(txpi_status)[2:].zfill(32)
        txpi_tx_gt_rdy  = int(gpio_bits[31-11], 2)
        hop_tx_gt_rdy = int(gpio_bits[31-12], 2)
        return [txpi_tx_gt_rdy, hop_tx_gt_rdy]
    
    def polling_txpi_rdy(self):
        rdy = self.rx_txrdy_read()[0]
        esc = 0
        while rdy == 0:
            rdy = 1
            for sure in range(100):
                rdy = rdy * self.rx_txrdy_read()[0]
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        return rdy
    
    def polling_hop_rdy(self):
        rdy = self.rx_txrdy_read()[1]
        esc = 0
        while rdy == 0:
            rdy = 1
            for sure in range(100):
                rdy = rdy * self.rx_txrdy_read()[1]
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        return rdy
    
    ####
        
    #### addr : 0 -> GPIO_REG_ADDR ; 1 -> GPIO_DDMTD_REG_ADDR ; 2 -> GPIO_HOP_REG_ADDR ; 3 -> GPIO_HOP2_REG_ADDR
    def rx_read(self, addr=0):
        if addr == 1:
            gpio_data = self.Rx_udp_interface.AXI_read(self.GPIO_DDMTD_REG_ADDR + 0x0008) #address GPIO2
            return gpio_data
        if addr == 2:
            gpio_data = self.Rx_udp_interface.AXI_read(self.GPIO_HOP_REG_ADDR + 0x0008) #address GPIO2
            return gpio_data
        if addr == 3:
            gpio_data = self.Rx_udp_interface.AXI_read(self.GPIO_HOP2_REG_ADDR + 0x0008) #address GPIO2
            return gpio_data
        if addr == 0:
            gpio_data = self.Rx_udp_interface.AXI_read(self.GPIO_REG_ADDR + 0x0008) #address GPIO2
            gpio_bits = bin(gpio_data)[2:].zfill(32)
            valid     = int(gpio_bits[31], 2)
            slides    = int(gpio_bits[0:7], 2)
            lols      = int(gpio_bits[7:15], 2)
            lol_ovf   = int(gpio_bits[15], 2)
            odds      = int(gpio_bits[16:21], 2)
            temp      = int(gpio_bits[21:31], 2)
            return [valid, slides, odds, temp, lols, lol_ovf]
        
    def rx_valid_polling(self):
        rdy = self.rx_read()[0]
        esc = 0
        while rdy == 0:
            rdy = 1
            for sure in range(100):
                rdy = rdy * self.rx_read()[0]
            time.sleep(0.01)
            esc += 1
            if esc == 1000:
                break
        return True if rdy==1 else False
        
    def rx_ddmtd_reset(self):
        ### in RxFPGA the signal from_axi.tx_enc_bypass is used to reset the DDMTD (needed after a frequency change on the DDMTD inputs)
        gpio_new_value = 1<<3 | self.rx_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, gpio_new_value)
        time.sleep(0.1)
        #check = self.Rx_udp_interface.AXI_read(self.GPIO_REG_ADDR + 0x0)
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, self.rx_running_conf)
        #print(check)
    
    def DMTD_read(self, collect, n_avg, fin=240.000e6, fdmtd=239.99e6, dmtd=0, corr=0):
        #### DDMTD reading
        UI = (1.0e12/fin)/40
        rangePh = 0
        stdPh   = 0
        error   = False
        oldR    = 0
        if collect:
            ph  = []
            val = 0
            startT = datetime.now()
            for _ in range(int(n_avg)):
                val = self.rx_ddmtd_read(navg=1, fin=fin, fdmtd=fdmtd, dmtd=dmtd, corr=corr)
                ph.append(val)
            rangePh = abs(max(ph)-min(ph))
            stdPh   = statistics.stdev(ph)
            if rangePh > 39*UI:
                print('DMTD RANGE TOO BIG!!! '+str(int(rangePh))+'ps -> adjusting')
                oldR = rangePh
                for m in range(int(n_avg)):
                    if ph[m] < 0 and corr > 0:
                        ph[m] = ph[m] + 40*UI
                    if ph[m] > 40*UI and corr < 0:
                        ph[m] = ph[m] - 40*UI
                    if (ph[m] > 40*UI and corr > 0) or (ph[m] < 0 and corr < 0):
                        error = True; break
                rangePh = abs(max(ph)-min(ph))
                stdPh   = statistics.stdev(ph)
                print('adjusted -> '+str(int(rangePh))+'ps')
            endT = datetime.now()
            phase_ps = sum(ph)/n_avg
        else:
            phase_ps = self.rx_ddmtd_read(64, False)
        phase_m  = phase_ps * 1e-12
        timeTook = (endT-startT).total_seconds() if collect else 0
        return [phase_m, rangePh, stdPh, timeTook, error, oldR]
        
    def rx_ddmtd_read(self, navg, fin=240.000e6, fdmtd=239.990e6, dmtd=0, corr=0):
        cnt_size = 20
        period = 1.0e12/fin
        phase_int  = self.rx_read(dmtd+1)
        is_segative = phase_int & (pow(2,cnt_size-1))
        if is_segative: 
            phase_signed = -((pow(2,cnt_size)-1)-phase_int-1) # hex to signed int conversion
        else:
            phase_signed = phase_int
        dmtd_unit = 1.0e12 * (fin - fdmtd) / (fin * fdmtd);
        phase_ps  = phase_signed * dmtd_unit/navg
        if is_segative:
            phase_ps = phase_ps + period
        #phase_ps = (phase_ps + period ) % period
        #phase_ps = (phase_ps + period - corr) % period
        return phase_ps - corr  #### corr centers the measurement in a region in the middle of the UI, based on the first value acquired.
                                #### Supposing that the noise and sub-UI jumps are lower than a fraction of the UI.
                                #### This is necessary to avoid that when a UI-jump occurs, if the two clock edges are too close 
                                #### the drifts produces UI-distant measurements, and so the UI-modulo would jump of 40UI.
    
    def rx_eq_adapt_mode(self):
        adapt_conf = 0b00000000
        gpio_new_value = adapt_conf<<4 | self.rx_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, gpio_new_value)
        self.rx_running_conf = gpio_new_value
        
    def rx_eq_freeze(self):
        freeze_conf = 0b10101010
        gpio_new_value = freeze_conf<<4 | self.rx_running_conf
        self.Rx_udp_interface.AXI_write(self.GPIO_REG_ADDR, gpio_new_value)
        self.rx_running_conf = gpio_new_value
        
    ### IIC configuration for LTI FMC Fanout board / EDA-04485-V2
        
    def iic_conf_rx_fmc_fanout(self, also_cg, printRegs=False):
        LTI_SWITCH_ADDR = 0b01110100
        LTI_CG_BUS_SEL  = 0b01101001
        LTI_JC_BUS_SEL  = 0b01101000
        LTI_CG_ADDR     = 0b01101000
        LTI_JC_ADDR     = 0b01101000
        print('Configuring RxFPGAs Jitter Cleaner - Fanout FMC LTI Board')
        self.rxiic.write(self.LTI_SWITCH_ADDR, [self.LTI_CG_BUS_SEL])
        ### Configure Si5344-B as clock generator
        self.loadConfigInSilab(LTI_CG_ADDR, "ClockBuilder/Si5394-RevA-LTIFMCCG-Registers.txt", printRegs)
        if also_cg:
            print('Configuring RxFPGAs Clock Generator - Fanout FMC LTI Board')
            self.rxiic.write(self.LTI_SWITCH_ADDR, [self.LTI_JC_BUS_SEL])
            ### Configure Si5345-D as jitter cleaner
            self.loadConfigInSilab(LTI_JC_ADDR, "ClockBuilder/Si5345-RevD-LTIFMCJC-Registers.txt", printRegs)
    
    ### IIC configuration for TTC-PON FMC / EDA-03231-V3 
    
    IIC_SWITCH_ADDR = 0b11101000

    SEL_EXPANDER    = 0b00000100
    SEL_SI570       = 0b00000010
    SEL_SI5344B     = 0b00001000

    EXPANDER_ADDR   = 0b01110000 #### last = 0 -> write
    SI570_ADDR      = 0b10101010 #### last = 0 -> write
    SI5344B_ADDR    = 0b11010000 #### last = 0 -> write

    EXPANDER_CONF_R = 0b00110000
    EXPANDER_CONF   = 0b00001111

    SI_PG_SEL_ADDR  = 0x01
    
    def iic_conf_rx_fmc_TTCPON(self, also_ref_Si570, printRegs):
        ### Configure expander for outputting X_CLK0_P/N on SMA3/4 & X_GBTCLK0_P/N on COAX_OUT1_P/N + SMA1/2
        print('Configuring RxFPGAs Jitter Cleaner - TTC PON FMC - Si5344B')
        self.rxiic.write(self.IIC_SWITCH_ADDR, [self.SEL_EXPANDER])
        self.rxiic.write(self.EXPANDER_ADDR, [self.EXPANDER_CONF])
        self.rxiic.read(self.EXPANDER_ADDR, 1)
        ### Configure Si5344-B as jitter cleaner
        self.rxiic.write(self.IIC_SWITCH_ADDR, [self.SEL_SI5344B])
        self.loadConfigInSilab(self.SI5344B_ADD, "ClockBuilder/Si5344B-conf.txt", printRegs)
        if also_ref_Si570:
            print('Configuring Rx GT Ref - TTC PON FMC - Si570')
            valid1 = Si750.loadSi570conf(self.rxiic, True, printRegs)
            valid2 = True
            #### this config is custom for a specific XTAL in the Si570, and for the startup output default frequency.
            #### so the script returns valid only if it is configured after power up and not from other configurations.
            #### if this is not the case, valid is false, the Si570 automatically resets itself (if the config is illegal).
            #### so the config is called again, if this does not happens, the FMC has to be power cycled manually.
            if not(valid1):
                print('Will try again \n')
                time.sleep(5)
                valid2 = Si750.loadSi570conf(self.rxiic, True, printRegs)
            valid = [valid1, valid2]
            if valid[1]:
                print('Success! - remember to reprogram the FPGA now')
            else:
                print('ERROR: FMC needs manual power cycling before trying again.')
                
    def iic_conf_tx_fmc(self, printRegs):
        print('Configuring Tx GT Ref - TTC PON FMC - Si570')
        valid1 = Si750.loadSi570conf(self.txiic, False, printRegs)
        valid2 = True
        #### this config is custom for a specific XTAL in the Si570, and for the startup output default frequency.
        #### so the script returns valid only if it is configured after power up and not from other configurations.
        #### if this is not the case, valid is false, the Si570 automatically resets itself (if the config is illegal).
        #### so the config is called again, if this does not happens, the FMC has to be power cycled manually.
        if not(valid1):
            print('Will try again \n')
            time.sleep(5)
            valid2 = Si750.loadSi570conf(self.txiic, False, printRegs)
        valid = [valid1, valid2]
        if valid[1]:
            print('Success!')
        else:
            print('ERROR: FMC needs manual power cycling before trying again.')
    
    def loadConfigInSilab(self, DEVICE_ADDR, text, printRegs):
        with open(text,"r") as f:
            #with open("ClockBuilder/Si5344Btst.txt","r") as f:
            toSiLab = True
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                #print(line)
                splittedLine = line[0].split(" ")
                if set(['Delay', 'delay', 'Wait', 'wait']) & set(splittedLine):
                    #print(splittedLine)
                    for k in range(len(splittedLine)):
                        if splittedLine[k] == "Delay" and len(splittedLine) > k+1 and splittedLine[k+1].isnumeric():
                            time.sleep(float(splittedLine[k+1]) * 10e-3)
                            if printRegs:
                                print("Waiting " + splittedLine[k+1] + "ms", " (you should check if 'ms' is correct)")
                            break
                        else:
                            if k == len(splittedLine)-1 and printRegs:
                                print("keyword 'Delay' is present. Consider to add a delay here")
                #### line := [addr 0x0000, val 0x00]
                lineValue = line[0:2]
                if i > 0 and lineValue[0][0] != "#":
                    axiValue = []
                    lineAddr = lineValue[0]
                    lineData = lineValue[1]
                    for j in range(2, len(lineAddr), 2):
                        axiValue.append(int("0x" + lineAddr[j:j+2], 0))
                    for j in range(2, len(lineData), 2):
                        axiValue.append(int("0x" + lineData[j:j+2], 0))

                    if not(toSiLab):
                        if printRegs:
                            print("input:  " + str(lineValue))
                            print("output: " + str([hex(x) for x in axiValue]) + "\n")
                            print(hex(axiValue[0]),hex(axiValue[1]),hex(axiValue[2]))
                        with open(r'ClockBuilder/silab_conf.csv', 'a', newline='') as f:
                            writer = csv.writer(f)
                            writer.writerow(axiValue)
                    else:
                        #write the value to registers
                        #print("sending: " + str([hex(x) for x in axiValue]) + "\n")
                        self.rxiic.write(DEVICE_ADDR, [self.SI_PG_SEL_ADDR]+[axiValue[0]])
                        self.rxiic.write(DEVICE_ADDR, [axiValue[1]]+[axiValue[2]])

                        #read the value back
                        #board.rxiic.write(SI5344B_ADDR, [SI_PG_SEL_ADDR]+[axiValue[0]])
                        self.rxiic.write(DEVICE_ADDR, [axiValue[1]])
                        read_val = self.rxiic.read(DEVICE_ADDR, 1)

                        #check integrity
                        if read_val[0] != axiValue[2] and printRegs:
                            print("Write Verify Failed!")
                            print(hex(axiValue[0]), hex(axiValue[1]), hex(read_val[0]), "\n")
    
    
    ###
    
    def rx_ddmtd_correct(self, navg, fin=240.000e6, fdmtd=239.990e6): ## not used
        count = 0
        Tin = 1/fin
        phase = 2*Tin/1e-12
        while phase*1e-12 > Tin:
            self.rx_ddmtd_reset()
            time.sleep(0.5)
            phase = self.rx_ddmtd_read(navg, False, fin, fdmtd)
            count += 1
            if count > 50:
                break
        return phase
    
    def rx_ddmtd_read_2(self, ratio=True): ## not used
        reading = self.rx_read(2)
        phase_int =  reading[0]
        fin0  = 240.000e6
        fdmt0 = 239.990e6
        dmtd_unit0 = 1.0e12 * (fin0 - fdmt0) / (fin0 * fdmt0);
        phase_ps0  = phase_int * dmtd_unit0
        decimal_bits = 26
        refclk = 240.0e6 #125.0e6
        if ratio:
            rat   = reading[1]
            fin   = 1.0/((1.0/refclk)*reading[2]/pow(2,decimal_bits))
            fdmtd = (pow(2,decimal_bits)/rat)*fin
        else:
            rat_a =  reading[1]
            rat_d =  reading[2]
            T_a   = (1.0/refclk)*rat_a/pow(2,decimal_bits)
            T_d   = (1.0/refclk)*rat_d/pow(2,decimal_bits)
            fin   = 1.0/T_a
            fdmtd = 1.0/T_d
        return [phase_ps0, fin, fdmtd]
    
    