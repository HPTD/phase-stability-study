#Python wrapper for Xilinx AXI IIC Bus Core
#It exploits the UDP to AXI modules included in the repo:
#(https://gitlab.cern.ch/fmartina/udp_to_axis.git) and (https://gitlab.cern.ch/fmartina/axis_to_axi.git)
#However, it can be easily generalised by substituting the called AXI_read and AXI_write functions with you own ones
#Written by Francesco Martina (francesco.martina@cern.ch)

#####################################################################
## Xilinx AXI IIC Register Space
#####################################################################
GIE				= 0x01C		# Global Interrupt Enable Register
ISR				= 0x020		# Interrupt Status Register
IER				= 0x028		# Interrupt Enable Register
SOFTR 			= 0x040		# Soft Reset Register
CR 				= 0x100		# Control Register
SR 				= 0x104		# Status Register
TX_FIFO			= 0x108		# Transmit FIFO Register
RX_FIFO			= 0x10C		# Receive FIFO Register
ADR				= 0x110		# Slave Address Register
TX_FIFO_OCY		= 0x114		# Transmit FIFO Occupancy Register
RX_FIFO_OCY		= 0x118		# Receive FIFO Occupancy Register
TEN_ADR			= 0x11C		# Slave Ten Bit Address Register
RX_FIFO_PIRQ	= 0x120		# Receive FIFO Programmable Depth Interrupt Register
GPO				= 0x124		# General Purpose Output Register
TSUSTA 			= 0x128		# Timing Parameter Register
TSUSTO 			= 0x12C		# Timing Parameter Register
THDSTA 			= 0x130		# Timing Parameter Register
TSUDAT 			= 0x134		# Timing Parameter Register
TBUF 			= 0x138		# Timing Parameter Register
THIGH 			= 0x13C		# Timing Parameter Register
TLOW 			= 0x140		# Timing Parameter Register
THDDAT 			= 0x144		# Timing Parameter Register
#####################################################################

## CR (Control Register) bits
CR_EN           = 0x01
CR_TxFIFO_RST   = 0x02
CR_MSMS         = 0x04
CR_TX           = 0x08
CR_TXAK         = 0x10
CR_RSTA         = 0x20
CR_GC_EN        = 0x40

## SR (Status Register) bits
SR_ABGC           = 0x01
SR_AAS            = 0x02
SR_BB             = 0x04
SR_SRW            = 0x08
SR_TX_FIFO_Full   = 0x10
SR_RX_FIFO_Full   = 0x20
SR_RX_FIFO_Empty  = 0x40
SR_TX_FIFO_Empty  = 0x80

## ISR (Interrupt Status Register) bits
ARB_LOST          = 0x01
TX_COMP           = 0x02
TX_EMPTY          = 0x04
RX_FULL           = 0x08
BUS_BUSY_N        = 0x10
ADD_AS_SLAVE      = 0x20
NOT_ADD_AS_SLAVE  = 0x40
TX_HALF_EMPTY     = 0x80

#####################################################################

#RX and TX FIFO depth
FIFO_DEPTH = 16

#Polling period in seconds (can be set down to zero, but a non null value is suggested to reduce the CPU / Network load)
POLLING_PERIOD  = 0.001
POLLING_TIMEOUT = 1.000

#derived constants
POLLING_TIMEOUT_TICKS = POLLING_TIMEOUT / POLLING_PERIOD

import time
import numpy as np

class AXI_IIC:
        def __init__(self, UDP_AXI_class, IIC_master_base_address):
            self.IIC_master_base_address = IIC_master_base_address
            self.udp = UDP_AXI_class
            self.udp.UDP_bind()
            
            # reset the whole IP core
            self.reset()
            
            #set the RX FIFO depth to the maximum
            self.wreg(RX_FIFO_PIRQ, 0xF)
            
            #Hack to allow the IP to work @100kHz with 25MHz of Master Bus
            ####################################
            #self.wreg(THIGH,125)
            #self.wreg(TLOW,125)
            ####################################
            
            #Hack to allow the IP to work @100kHz with 125MHz of Master Bus
            ####################################
            self.wreg(THIGH,500)
            self.wreg(TLOW,500)
            ####################################
            
            #enable the core and reset the TX FIFO
            self.wreg(CR, CR_EN)
            self.TX_FIFO_reset()
        
        # Register Accessing
        #####################################################################
        
        # write the IP register
        def wreg(self, reg_offset_address, value):
            
            #compute the complete AXI address of the IP register (adding the base address)
            full_address = reg_offset_address + self.IIC_master_base_address
            
            #write the register by using the UDP class
            self.udp.AXI_write(full_address, value)
            
        # read the IP register
        def rreg(self, reg_offset_address):
            
            #compute the complete AXI address of the IP register (adding the base address)
            full_address = reg_offset_address + self.IIC_master_base_address
            
            #write the register by using the UDP class
            read_value = self.udp.AXI_read(full_address)
            return read_value
        
        # set the register bits identified by bit_mask, the others remain untouched
        def reg_bit_set(self, reg_offset_address, bit_mask):
            register_value = self.rreg(reg_offset_address)
            register_value = register_value | bit_mask
            self.wreg(reg_offset_address, register_value)
            
        # set the register bits identified by bit_mask, the others remain untouched
        def reg_bit_clear(self, reg_offset_address, bit_mask):
            register_value = self.rreg(reg_offset_address)
            register_value = register_value & (0xFFFFFFFF ^ bit_mask)
            self.wreg(reg_offset_address, register_value)
            
        # reset the whole IP core
        def reset(self):
            #launch a soft reset of the core with the magic word 0x0A
            self.wreg(SOFTR, 0x0A)
            
        # reset the transmission FIFO
        def TX_FIFO_reset(self):
            self.reg_bit_set(CR, CR_TxFIFO_RST)
            self.reg_bit_clear(CR, CR_TxFIFO_RST)
            
        # Status Query
        #####################################################################
        def is_bus_Busy(self):
            SR_value = self.rreg(SR)
            return SR_value & SR_BB != 0
            
        def is_TX_FIFO_Empty(self):
            SR_value = self.rreg(SR)
            return SR_value & SR_TX_FIFO_Empty != 0
        
        def is_RX_FIFO_Empty(self):
            SR_value = self.rreg(SR)
            return SR_value & SR_RX_FIFO_Empty != 0
        
        def wait_bus_not_Busy(self):
            t = POLLING_TIMEOUT_TICKS
            while t > 0:
                if not self.is_bus_Busy():
                    return 0
                time.sleep(POLLING_PERIOD)
                t = t - 1
            
            print("bus is busy timeout, exiting...")
            return 1
        
        # locks until the TX FIFO is empty with a polling period POLLING_PERIOD or POLLING_TIMEOUT Expired
        def wait_TX_FIFO_Empty(self):
            t = POLLING_TIMEOUT_TICKS
            while t > 0:
                if self.is_TX_FIFO_Empty():
                    return 0
                time.sleep(POLLING_PERIOD)
                t = t - 1
                
            print("wait TX FIFO empty timeout")
            #force a TX FIFO reset in case of a timeout
            self.TX_FIFO_reset()
            #print(self.is_TX_FIFO_Empty())
            return 1
                
                
        # wait until the RX FIFO is not Empty or a timeout occurred
        def wait_RX_FIFO_not_Empty(self):
            t = POLLING_TIMEOUT_TICKS
            while t > 0:
                if not self.is_RX_FIFO_Empty():
                    return 0
                time.sleep(POLLING_PERIOD)
                t = t - 1
            
            print("wait RX FIFO not empty timeout")
            return 1
            
        # High-Level Control
        #####################################################################
        
        # program the GPIO register
        def GPIO(self, value):
            self.wreg(GPO, value)
        
        # read the GPIO register
        def GPIO_status(self):
            status = self.rreg(GPO)
            return status
        
        # write the byte_vector to the addressed iic device
        # (Dynamic Controller Logic Flow mode)
        # TODO. Extend the maximum byte_vector size to > 16
        def write(self, iic_device_add, byte_vector):
            total_bytes = len(byte_vector)
            transmitted_bytes = 0
            
            if total_bytes < 1:
                return
            
            if self.wait_bus_not_Busy():
                return
            
            self.wait_TX_FIFO_Empty()
                  
            #set the iic device address and transmit it (force writing address)
            #0x100 indicates the start generation
            self.wreg(TX_FIFO, (0x100 | iic_device_add) & 0xFFE)
            
            #transmit the payload bytes
            while transmitted_bytes < total_bytes:
                if transmitted_bytes == total_bytes - 1:
                    self.wreg(TX_FIFO, 0x200 | byte_vector[transmitted_bytes])
                    transmitted_bytes = transmitted_bytes + 1
                    break
                self.wreg(TX_FIFO, 0x000 | byte_vector[transmitted_bytes])
                transmitted_bytes = transmitted_bytes + 1
                
                    
        # read read_byte_N bytes from the addressed iic device
        # (Dynamic Controller Logic Flow mode)
        # TODO. Extend the read_byte_N to > 16
        def read(self, iic_device_add, read_byte_N):
            if read_byte_N < 1:
                return
            
            rx_bytes = []
            read_byte = 0
            
            if self.wait_bus_not_Busy():
                return
            
            self.wait_TX_FIFO_Empty()
            
            #set the iic device address and transmit it (force reading address)
            #0x100 indicates the start generation
            self.wreg(TX_FIFO, (0x100 | iic_device_add) | 0x001)
            
            #configure the number of bytes to read
            self.wreg(TX_FIFO, (0x200 | read_byte_N))
                      
            while read_byte < read_byte_N:
                #wait for new data
                timeout = self.wait_RX_FIFO_not_Empty()
                
                if timeout == 0:           
                    #store the data
                    rx_bytes.append(self.rreg(RX_FIFO))
                else:
                    #a dummy value (-1) is written in case of timeout
                    rx_bytes.append(-1)
                      
                read_byte = read_byte + 1

            return rx_bytes
        
        
        #Destructor
        def __del__(self):
            self.reg_bit_clear(CR, CR_EN)
            self.udp.UDP_unbind()