set_property PACKAGE_PIN AY24 [get_ports clk_125_p]
set_property PACKAGE_PIN AY23 [get_ports clk_125_n]
set_property IOSTANDARD LVDS [get_ports clk_125_*]

### DDMTD inputs
#set_property PACKAGE_PIN AG8 [get_ports gtfanout_in_n] ;# MGT266_CLK0_N
#set_property PACKAGE_PIN AG9 [get_ports gtfanout_in_p] ;# MGT266_CLK0_P
#set_property PACKAGE_PIN J8  [get_ports ddmtdclk_in_n] ;# MGT233_CLK1_N
#set_property PACKAGE_PIN J9  [get_ports ddmtdclk_in_p] ;# MGT233_CLK1_P
set_property PACKAGE_PIN AG8 [get_ports gtfanout_in_n]
set_property PACKAGE_PIN AG9 [get_ports gtfanout_in_p]
set_property PACKAGE_PIN R32 [get_ports ddmtdclk_in_p]
set_property PACKAGE_PIN P32 [get_ports ddmtdclk_in_n]
set_property IOSTANDARD LVDS [get_ports ddmtdclk_in_*]

## Main transceiver - RxReset with phase jumps (Quad 233)
#set_property PACKAGE_PIN AK38 [get_ports gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P
#set_property PACKAGE_PIN AK39 [get_ports gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N
set_property PACKAGE_PIN N8 [get_ports gt_refclk_n]
set_property PACKAGE_PIN N9 [get_ports gt_refclk_p]
#set_property PACKAGE_PIN AR45 [get_ports gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
#set_property PACKAGE_PIN AR46 [get_ports gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
#set_property PACKAGE_PIN AT42 [get_ports gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
#set_property PACKAGE_PIN AT43 [get_ports gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N
set_property PACKAGE_PIN K2 [get_ports gt_rx_p]
set_property PACKAGE_PIN K1 [get_ports gt_rx_n]
set_property PACKAGE_PIN G5 [get_ports gt_tx_p]
set_property PACKAGE_PIN G4 [get_ports gt_tx_n]

set_property PACKAGE_PIN BC21 [get_ports si5328_en]
set_property IOSTANDARD LVCMOS18 [get_ports si5328_en]
set_property PACKAGE_PIN BE24 [get_ports firefly_en]
set_property IOSTANDARD LVCMOS18 [get_ports firefly_en]

#set_property PACKAGE_PIN AH39 [get_ports rxrecclk_n] ;# FMCP_HSPC_GBT1_0_N
#set_property PACKAGE_PIN AH38 [get_ports rxrecclk_p] ;# FMCP_HSPC_GBT1_0_P
set_property PACKAGE_PIN J8 [get_ports rxrecclk_n]
set_property PACKAGE_PIN J9 [get_ports rxrecclk_p]

## TxPI transceiver - used as phase shifter (Quad 121)
#set_property PACKAGE_PIN AH38 [get_ports txpi_refclk_p] ;#FMCP_HSPC_GBT1_0_P  #### QPLL1 of Quad 121 is used by HOP_GT - MGTREFCLK1
#set_property PACKAGE_PIN AH39 [get_ports txpi_refclk_n] ;#FMCP_HSPC_GBT1_0_N
#set_property PACKAGE_PIN AN45 [get_ports pi_gt_rx_p] ;# FMCP_HSPC_DP1_M2C_P
#set_property PACKAGE_PIN AN46 [get_ports pi_gt_rx_n] ;# FMCP_HSPC_DP1_M2C_N
#set_property PACKAGE_PIN AP42 [get_ports pi_gt_tx_p] ;# FMCP_HSPC_DP1_C2M_P
#set_property PACKAGE_PIN AP43 [get_ports pi_gt_tx_n] ;# FMCP_HSPC_DP1_C2M_N

#### TxHOP transceiver - next hop of the chain (Quad 121)
#set_property PACKAGE_PIN AK38 [get_ports hop_gt_refclk_p] ;# FMCP_HSPC_GBT0_0_P  #### CPLL of Quad 121 is used by HOP_GT - MGTREFCLK0
#set_property PACKAGE_PIN AK39 [get_ports hop_gt_refclk_n] ;# FMCP_HSPC_GBT0_0_N
set_property PACKAGE_PIN AH39 [get_ports hop_gt_refclk_n]
set_property PACKAGE_PIN AH38 [get_ports hop_gt_refclk_p]
set_property PACKAGE_PIN AR45 [get_ports hop_gt_rx_p]
set_property PACKAGE_PIN AR46 [get_ports hop_gt_rx_n]
set_property PACKAGE_PIN AT42 [get_ports hop_gt_tx_p]
set_property PACKAGE_PIN AT43 [get_ports hop_gt_tx_n]

## HPC1 - rxusrclk
set_property PACKAGE_PIN AL35 [get_ports rxUserClk_p]
set_property PACKAGE_PIN AL36 [get_ports rxUserClk_n]
set_property IOSTANDARD LVDS [get_ports rxUserClk_p]
set_property IOSTANDARD LVDS [get_ports rxUserClk_n]

set_property PACKAGE_PIN AP35 [get_ports rxUserClk_1]
set_property IOSTANDARD LVCMOS18 [get_ports rxUserClk_1]
set_property SLEW FAST [get_ports rxUserClk_1]
set_property DRIVE 12 [get_ports rxUserClk_1]

## ----- Ethernet
# quad (bank) 230
#set_property PACKAGE_PIN AK39 [get_ports eth_gtrefclk_n] ;# FMCP_HSPC_GBT0_0_N
#set_property PACKAGE_PIN AK38 [get_ports eth_gtrefclk_p] ;# FMCP_HSPC_GBT0_0_P
set_property PACKAGE_PIN AT22 [get_ports eth_gtrefclk_p]
set_property PACKAGE_PIN AU22 [get_ports eth_gtrefclk_n]
set_property PACKAGE_PIN AR23 [get_ports phy_mdio]
set_property PACKAGE_PIN AV23 [get_ports phy_mdc]
set_property PACKAGE_PIN BA21 [get_ports phy_resetb]
set_property PACKAGE_PIN AR24 [get_ports phy_pwdn]
set_property IOSTANDARD LVCMOS18 [get_ports phy_mdio]
set_property IOSTANDARD LVCMOS18 [get_ports phy_mdc]
set_property IOSTANDARD LVCMOS18 [get_ports phy_resetb]
set_property IOSTANDARD LVCMOS18 [get_ports phy_pwdn]
set_property IOSTANDARD LVDS [get_ports eth_gtrefclk_*]

# quad (bank): 230, X1Y13 -> SFP1 (bottom right)
#set_property PACKAGE_PIN AR45 [get_ports rxp_eth_sfp] ;# FMCP_HSPC_DP0_M2C_P
#set_property PACKAGE_PIN AR46 [get_ports rxn_eth_sfp] ;# FMCP_HSPC_DP0_M2C_N
#set_property PACKAGE_PIN AT42 [get_ports txp_eth_sfp] ;# FMCP_HSPC_DP0_C2M_P
#set_property PACKAGE_PIN AT43 [get_ports txn_eth_sfp] ;# FMCP_HSPC_DP0_C2M_N
set_property PACKAGE_PIN AU24 [get_ports rxp_eth_sfp]
set_property PACKAGE_PIN AV24 [get_ports rxn_eth_sfp]
set_property PACKAGE_PIN AU21 [get_ports txp_eth_sfp]
set_property PACKAGE_PIN AV21 [get_ports txn_eth_sfp]
set_property IOSTANDARD LVDS [get_ports rxp_eth_sfp]
set_property IOSTANDARD LVDS [get_ports rxn_eth_sfp]
set_property IOSTANDARD LVDS [get_ports txp_eth_sfp]
set_property IOSTANDARD LVDS [get_ports txn_eth_sfp]

# IIC
set_property PACKAGE_PIN BF9 [get_ports sda]
set_property PACKAGE_PIN BF10 [get_ports scl]
set_property IOSTANDARD LVCMOS18 [get_ports sda]
set_property IOSTANDARD LVCMOS18 [get_ports scl]
## ------

set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
#get_property RXSLIDE_MODE [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]

set_property RX_PROGDIV_CFG 20 [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
#get_property RX_PROGDIV_CFG [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]

create_clock -period 8.000 -name clk_125 -add [get_ports clk_125_p]
create_clock -period 4.167 -name gt_refclk -add [get_ports gt_refclk_p]
#create_clock -period 4.16666 -name txpi_refclk   -add [get_ports txpi_refclk_p]
create_clock -period 4.167 -name hop_gt_refclk -add [get_ports hop_gt_refclk_p]

#create_clock -period 6.40000 -name eth_gtrefclk -add [get_ports eth_gtrefclk_p]
create_clock -period 1.600 -name eth_gtrefclk -add [get_ports eth_gtrefclk_p]

#create_clock -period 4.16666 -name clnrxusrclk -add [get_ports clnrxusrclk_in_p]
create_clock -period 4.167 -name gtfanout -add [get_ports gtfanout_in_p]
create_clock -period 4.167 -name ddmtdclk -add [get_ports ddmtdclk_in_p]

#set_false_path -from [get_ports clnrxusrclk_in_p] -to [get_pins DDMTD_inst/ddmtd/DMTD_B/gen_straight.clk_i_d0_reg/D]
#set_false_path -from [get_ports gtfanout_in_p]    -to [get_pins DDMTD_inst/ddmtd/DMTD_A/gen_straight.clk_i_d0_reg/D]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_125]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks gt_refclk]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks txpi_refclk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks hop_gt_refclk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks eth_gtrefclk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks gtfanout]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks ddmtdclk]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/txoutclk_out[0]}]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/txoutclk_out[0]}]]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *ddmtd/DMTD_A/tag_o_reg*/C}]]

set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_*]
set_output_delay -clock gt_refclk 3.000 [get_ports rxrecclk_*]
#set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_1]
#set_output_delay -clock gt_refclk 3.000 [get_ports txUserClk]
set_output_delay -clock eth_gtrefclk 0.800 [get_ports phy_mdc]
set_output_delay -clock eth_gtrefclk 0.800 [get_ports phy_mdio]
set_output_delay -clock eth_gtrefclk 0.800 [get_ports phy_resetb]
set_output_delay -clock eth_gtrefclk 0.800 [get_ports scl]
set_output_delay -clock eth_gtrefclk 0.800 [get_ports sda]
set_input_delay -clock clk_125 0.800 [get_ports scl]
set_input_delay -clock clk_125 0.800 [get_ports sda]

#### Placement for minimizing interference over clock paths 

create_pblock pblock_ddmtd
add_cells_to_pblock [get_pblocks pblock_ddmtd] [get_cells -hierarchical -filter {NAME =~ DDMTD_inst/ddmtd/*}]
resize_pblock [get_pblocks pblock_ddmtd] -add {CLOCKREGION_X5Y2:CLOCKREGION_X5Y2}

#create_pblock pblock_ddmtd_h
#add_cells_to_pblock [get_pblocks pblock_ddmtd_h] [get_cells -hierarchical -filter {NAME =~ DDMTD_inst/ddmtd_hop/*}]
#resize_pblock [get_pblocks pblock_ddmtd_h] -add {CLOCKREGION_X5Y1:CLOCKREGION_X5Y1}

#create_pblock pblock_axi
#add_cells_to_pblock [get_pblocks pblock_axi] [get_cells -hierarchical -filter {NAME =~ AXI_inst/*}]
#resize_pblock [get_pblocks pblock_axi] -add {CLOCKREGION_X0Y7:CLOCKREGION_X1Y5}

#create_pblock pblock_dbg
#add_cells_to_pblock [get_pblocks pblock_dbg] [get_cells -hierarchical -filter {NAME =~ vio_*}]
#add_cells_to_pblock [get_pblocks pblock_dbg] [get_cells -hierarchical -filter {NAME =~ ila_*}]
#resize_pblock [get_pblocks pblock_dbg] -add {CLOCKREGION_X2Y14:CLOCKREGION_X4Y5}


set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

