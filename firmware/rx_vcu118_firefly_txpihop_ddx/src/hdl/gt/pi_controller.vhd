library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.gt_pkg.all;

entity pi_controller is
    port ( 
        rdy      : in  std_logic;
        txusrclk : in  std_logic;
        sysclk   : in  std_logic;
        cmnd_in  : in  std_logic_vector(4 downto 0); -- (4): increment if 1, dec if 0; (3 downto 1): phase shift; (0): 0->1 shift command
        ctrl_out : inout to_txpi_t
    );
end pi_controller;

architecture Behavioral of pi_controller is

    signal rdy_synch : std_logic;

    signal command  : std_logic_vector(4 downto 0) := "00000"; -- ps
    signal newshift : std_logic := '0';
    signal wait_c   : integer range 0 to 10 := 0;
    
    type state_t is (IDLE, START_SHIFT, COUNT6, END_SHIFT, PULSE);
    signal state : state_t := IDLE;
    
    --dbg
    signal tclk : std_logic := '0';

begin

    bit_synch_txusr: entity work.bit_synch
    port map (
        bit_in  => rdy, 
        clk     => txusrclk, 
        bit_out => rdy_synch   
    ); 

    bus_synch_sys_to_txusr: entity work.bus_synch
      generic map ( NBITS => 5 ) 
      port map (
        clk            => txusrclk,
        clk_start      => sysclk,
        data_asynch_in => cmnd_in, 
        data_synch_out => command
      ); 

    new_shift_start_pulse: entity work.monostable
      port map(
        rst       => '0',
        clk       => txusrclk,
        data_in   => command(0),
        pulse_out => newshift
      );
      
    process(txusrclk) begin
        if rising_edge(txusrclk) then
            if rdy_synch = '0' then
                wait_c   <=  0 ;
                state    <= IDLE;
            else
            
                case(state) is 
                
                    when IDLE =>
                        if newshift = '1' then
                            state <= START_SHIFT;
                        end if;
                        
                    when START_SHIFT =>
                        ctrl_out.stepsize(3) <= command(4);
                        ctrl_out.stepsize(2 downto 0) <= command(3 downto 1);
                        wait_c <= wait_c + 1;
                        if wait_c = 1 then
                            state  <= COUNT6;
                            wait_c <= 0;
                        end if;
                        
                    when COUNT6 => 
                        ctrl_out.ppmen <= '1';
                        if wait_c = 5 then
                            state  <= END_SHIFT;
                            wait_c <= 0;
                        else 
                            wait_c <= wait_c + 1;
                        end if;
                            
                    when END_SHIFT =>
                        ctrl_out.ppmen <= '0';
                        if wait_c = 9 then
                            state  <= PULSE;
                            wait_c <= 0;
                        else 
                            wait_c <= wait_c + 1;
                            if wait_c = 2 then
                                ctrl_out.stepsize(3) <= '0';
                                ctrl_out.stepsize(2 downto 0) <= "000";
                            end if;
                        end if;
                        
                     when PULSE =>
                        wait_c <= wait_c + 1;
                        ctrl_out.ppmen <= '1';
                        if wait_c = 1 then
                            ctrl_out.ppmen <= '0';
                            state  <= IDLE;
                            wait_c <= 0;
                        end if;
                        
                end case;

            end if;
        end if;
    end process;
    
    process(txusrclk) begin
        if rising_edge(txusrclk) then
            tclk <= not(tclk);
        end if;
    end process;
    
    ila_dbg : entity work.ila_ip_ctrl
     port map (
     	clk       => txusrclk,
     	probe0(0) => tclk, 
        probe1(0) => newshift, 
        probe2(0) => ctrl_out.ppmen, 
        probe3(0) => ctrl_out.stepsize(3),
        probe4    => ctrl_out.stepsize(2 downto 0)
     );
    
end Behavioral;
