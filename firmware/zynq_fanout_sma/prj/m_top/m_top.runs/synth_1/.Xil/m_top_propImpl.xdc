set_property SRC_FILE_INFO {cfile:C:/Users/eorzes/cernbox/git/zynq_fanout_sma/src/xdc/m_top.xdc rfile:../../../../../src/xdc/m_top.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:2 order:LATE scoped_inst:DDMTD_inst/clnrxusr_buf_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:3 order:LATE scoped_inst:DDMTD_inst/ddmtdclk_buf_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:4 order:LATE scoped_inst:DDMTD_inst/fanout_buf_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:5 order:LATE scoped_inst:gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property SRC_FILE_INFO {cfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl rfile:E:/Xilinx/Vivado/2022.1/data/ip/xpm/xpm_cdc/tcl/xpm_cdc_handshake.tcl id:6 order:LATE scoped_inst:sys_clk_inst/freq_inst/sync_freq/xpm_inst unmanaged:yes} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G21 [get_ports clk_125_p]
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F21 [get_ports clk_125_n]
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G7 [get_ports clnrxusrclk_in_n] ;# FMC_HPC0_GBTCLK0_M2C_C_N
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G8 [get_ports clnrxusrclk_in_p] ;# FMC_HPC0_GBTCLK0_M2C_C_P
set_property src_info {type:XDC file:1 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L7 [get_ports gtfanout_in_n] ;# FMC_HPC0_GBTCLK1_M2C_C_N
set_property src_info {type:XDC file:1 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L8 [get_ports gtfanout_in_p] ;# FMC_HPC0_GBTCLK1_M2C_C_P
set_property src_info {type:XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E28 [get_ports ddmtdclk_in_n] ;# FMC_HPC1_GBTCLK1_M2C_C_N
set_property src_info {type:XDC file:1 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E27 [get_ports ddmtdclk_in_p] ;# FMC_HPC1_GBTCLK1_M2C_C_P
set_property src_info {type:XDC file:1 line:25 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M33 [get_ports gt_rx_p] ;# SMA_MGT_RX_C_P
set_property src_info {type:XDC file:1 line:26 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M34 [get_ports gt_rx_n] ;# SMA_MGT_RX_C_N
set_property src_info {type:XDC file:1 line:27 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M29 [get_ports gt_tx_p] ;# SMA_MGT_TX_P
set_property src_info {type:XDC file:1 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M30 [get_ports gt_tx_n] ;# SMA_MGT_TX_N
set_property src_info {type:XDC file:1 line:43 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J27 [get_ports gt_refclk_p] ;# USER_SMA_MGT_CLOCK_C_P
set_property src_info {type:XDC file:1 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J28 [get_ports gt_refclk_n] ;# USER_SMA_MGT_CLOCK_C_N
set_property src_info {type:XDC file:1 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AE5 [get_ports rxUserClk_p] ;#FMC_HPC1_LA00_CC_P
set_property src_info {type:XDC file:1 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AF5 [get_ports rxUserClk_n] ;#FMC_HPC1_LA00_CC_N
set_property src_info {type:XDC file:1 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AH4 [get_ports rxUserClk_1] ;#FMC_HPC1_LA10_P
set_property src_info {type:XDC file:1 line:76 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C7 [get_ports eth_gtrefclk_n] ;# USER_MGT_SI570_CLOCK2_C_N
set_property src_info {type:XDC file:1 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C8 [get_ports eth_gtrefclk_p] ;# USER_MGT_SI570_CLOCK2_C_P
set_property src_info {type:XDC file:1 line:80 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C4 [get_ports rxp_eth_sfp] ;# SFP1_RX_P
set_property src_info {type:XDC file:1 line:81 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C3 [get_ports rxn_eth_sfp] ;# SFP1_RX_N
set_property src_info {type:XDC file:1 line:82 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D6 [get_ports txp_eth_sfp] ;# SFP1_TX_P
set_property src_info {type:XDC file:1 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D5 [get_ports txn_eth_sfp] ;# SFP1_TX_N
set_property src_info {type:XDC file:1 line:86 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AC4 [get_ports sda] ;#FMC_HPC0_LA01_CC_N
set_property src_info {type:XDC file:1 line:87 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB4 [get_ports scl] ;#FMC_HPC0_LA01_CC_P
set_property src_info {type:XDC file:1 line:92 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AK13 [get_ports tx_eth_addr_sel] ;# GPIO_DIP_SW7 - switch 1
set_property src_info {type:XDC file:1 line:94 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AL13 [get_ports rx_eth_addr_sel] ;# GPIO_DIP_SW6 - switch 2
set_property src_info {type:XDC file:1 line:96 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AP14 [get_ports switchPIN_ali_en_asynch] ;# GPIO_DIP_SW0 - switch 7 (8 is broken)
set_property src_info {type:XDC file:1 line:99 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A13 [get_ports SFP1_enable] ;# SFP1_TX_DISABLE
set_property src_info {type:XDC file:1 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B13 [get_ports SFP2_enable] ;# SFP2_TX_DISABLE
set_property src_info {type:XDC file:1 line:104 export:INPUT save:INPUT read:READ} [current_design]
set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E4_CHANNEL_PRIM_INST}]
set_property src_info {type:XDC file:1 line:126 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_property src_info {type:XDC file:1 line:127 export:INPUT save:INPUT read:READ} [current_design]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/txoutclk_out[0]}]]
current_instance DDMTD_inst/clnrxusr_buf_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:2 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance DDMTD_inst/ddmtdclk_buf_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:3 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance DDMTD_inst/fanout_buf_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:4 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance gt_inst/gtclk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:5 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
current_instance
current_instance sys_clk_inst/freq_inst/sync_freq/xpm_inst
set_property src_info {type:SCOPED_XDC file:6 line:30 export:INPUT save:NONE read:READ} [current_design]
create_waiver -internal -scoped -type CDC -id {CDC-15} -user "xpm_cdc" -tags "1009444" -desc "The CDC-15 warning is waived as it is safe in the context of XPM_CDC_HANDSHAKE." -from [get_pins -quiet {src_hsdata_ff_reg*/C}] -to [get_pins -quiet {dest_hsdata_ff_reg*/D}]
