set_property PACKAGE_PIN G10 [get_ports clk_125_p] ;#CLK_125MHZ_P
set_property PACKAGE_PIN F10 [get_ports clk_125_n] ;#CLK_125MHZ_N
set_property IOSTANDARD LVDS [get_ports clk_125_*]

### DDMTD inputs
set_property PACKAGE_PIN K5 [get_ports gtfanout_in_n] ;# FMC_HPC_GBTCLK0_M2C_C_N
set_property PACKAGE_PIN K6 [get_ports gtfanout_in_p] ;# FMC_HPC_GBTCLK0_M2C_C_P
set_property PACKAGE_PIN H6 [get_ports ddmtdclk_in_p] ;# FMC_HPC_GBTCLK1_M2C_C_P
set_property PACKAGE_PIN H5 [get_ports ddmtdclk_in_n] ;# FMC_HPC_GBTCLK1_M2C_C_N
#set_property PACKAGE_PIN H27 [get_ports dmtdclk] ;#USER_SMA_GPIO_P
#set_property PACKAGE_PIN G27 [get_ports ] ;#USER_SMA_GPIO_N
#set_property IOSTANDARD LVCMOS18 [get_ports dmtdclk]

## Main transceiver - RxReset with phase jumps (Quad 233)
set_property PACKAGE_PIN V6 [get_ports gt_refclk_p] ;# SMA_MGT_REFCLK_C_P  
set_property PACKAGE_PIN V5 [get_ports gt_refclk_n] ;# SMA_MGT_REFCLK_C_N
set_property PACKAGE_PIN T2 [get_ports gt_rx_p] ;# SFP0_RX_P
set_property PACKAGE_PIN T1 [get_ports gt_rx_n] ;# SFP0_RX_N
set_property PACKAGE_PIN U4 [get_ports gt_tx_p] ;# SFP0_TX_P
set_property PACKAGE_PIN U3 [get_ports gt_tx_n] ;# SFP0_TX_N

#set_property PACKAGE_PIN T5 [get_ports rxrecclk_n] ;# FMC_LPC_GBTCLK0_M2C_C_N     
#set_property PACKAGE_PIN T6 [get_ports rxrecclk_p] ;# FMC_LPC_GBTCLK0_M2C_C_P

## TxPI transceiver - used as phase shifter (Quad 121)
#set_property PACKAGE_PIN AH38 [get_ports txpi_refclk_p] ;#FMCP_HSPC_GBT1_0_P
#set_property PACKAGE_PIN AH39 [get_ports txpi_refclk_n] ;#FMCP_HSPC_GBT1_0_N
#set_property PACKAGE_PIN AN45 [get_ports pi_gt_rx_p] ;# FMCP_HSPC_DP1_M2C_P
#set_property PACKAGE_PIN AN46 [get_ports pi_gt_rx_n] ;# FMCP_HSPC_DP1_M2C_N
#set_property PACKAGE_PIN AP42 [get_ports pi_gt_tx_p] ;# FMCP_HSPC_DP1_C2M_P
#set_property PACKAGE_PIN AP43 [get_ports pi_gt_tx_n] ;# FMCP_HSPC_DP1_C2M_N

#### TxHOP transceiver - next hop of the chain (Quad 121)
#set_property PACKAGE_PIN AH38 [get_ports hop_gt_refclk_p] ;# FMCP_HSPC_GBT1_0_P  
#set_property PACKAGE_PIN AH39 [get_ports hop_gt_refclk_n] ;# FMCP_HSPC_GBT1_0_N
#set_property PACKAGE_PIN AR45 [get_ports hop_gt_rx_p] ;# FMCP_HSPC_DP0_M2C_P
#set_property PACKAGE_PIN AR46 [get_ports hop_gt_rx_n] ;# FMCP_HSPC_DP0_M2C_N
#set_property PACKAGE_PIN AT42 [get_ports hop_gt_tx_p] ;# FMCP_HSPC_DP0_C2M_P
#set_property PACKAGE_PIN AT43 [get_ports hop_gt_tx_n] ;# FMCP_HSPC_DP0_C2M_N

## rxusrclk
set_property PACKAGE_PIN D23 [get_ports rxUserClk_p] ;#USER_SMA_CLOCK_P
set_property PACKAGE_PIN C23 [get_ports rxUserClk_n] ;#USER_SMA_CLOCK_N
set_property IOSTANDARD LVDS [get_ports rxUserClk_p]
set_property IOSTANDARD LVDS [get_ports rxUserClk_n]

#set_property PACKAGE_PIN AP35 [get_ports rxUserClk_1] ;#FMCP_HSPC_LA10_P
set_property PACKAGE_PIN H27 [get_ports rxUserClk_1] ;#USER_SMA_GPIO_P
set_property IOSTANDARD LVCMOS18 [get_ports rxUserClk_1]
set_property SLEW FAST [get_ports rxUserClk_1]
set_property DRIVE 12 [get_ports rxUserClk_1]

## ----- Ethernet
# quad (bank) 230
set_property PACKAGE_PIN P6 [get_ports eth_gtrefclk_p] ;# MGT_SI570_CLOCK_C_P
set_property PACKAGE_PIN P5 [get_ports eth_gtrefclk_n] ;# MGT_SI570_CLOCK_C_N
#set_property PACKAGE_PIN AR23 [get_ports phy_mdio] ;# PHY1_MDIO (MDIO)
#set_property PACKAGE_PIN AV23 [get_ports phy_mdc] ;# PHY1_MDC  (MDC)
#set_property PACKAGE_PIN BA21 [get_ports phy_resetb] ;# PHY1_RESET_B  (RESET_B)
#set_property PACKAGE_PIN AR24 [get_ports phy_pwdn] ;# PHY1_PDWN_B_I_INT_B_O  (INT_PWDN)
#set_property IOSTANDARD LVCMOS18 [get_ports phy_mdio]
#set_property IOSTANDARD LVCMOS18 [get_ports phy_mdc]
#set_property IOSTANDARD LVCMOS18 [get_ports phy_resetb]
#set_property IOSTANDARD LVCMOS18 [get_ports phy_pwdn]
#set_property IOSTANDARD LVDS [get_ports eth_gtrefclk_*]

# SFP1 
set_property PACKAGE_PIN V2 [get_ports rxp_eth_sfp] ;# SFP1_RX_P
set_property PACKAGE_PIN V1 [get_ports rxn_eth_sfp] ;# SFP1_RX_N
set_property PACKAGE_PIN W4 [get_ports txp_eth_sfp] ;# SFP1_TX_P
set_property PACKAGE_PIN W3 [get_ports txn_eth_sfp] ;# SFP1_TX_N

# IIC
set_property PACKAGE_PIN Y25 [get_ports sda] ;#FMC_LPC_LA01_CC_N
set_property PACKAGE_PIN W25 [get_ports scl] ;#FMC_LPC_LA01_CC_P
set_property IOSTANDARD LVCMOS18 [get_ports sda]
set_property IOSTANDARD LVCMOS18 [get_ports scl]
## ------

set_property PACKAGE_PIN D28 [get_ports SFP1_enable] ;# SFP1_TX_DISABLE
set_property IOSTANDARD LVCMOS18 [get_ports SFP1_enable]
set_property PACKAGE_PIN AL8 [get_ports SFP0_enable] ;# SFP0_TX_DISABLE
set_property IOSTANDARD LVCMOS18 [get_ports SFP0_enable]


set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]
#get_property RXSLIDE_MODE [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]

set_property RX_PROGDIV_CFG 20 [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]
#get_property RX_PROGDIV_CFG [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]

create_clock -period 8.00000 -name clk_125       -add [get_ports clk_125_p]
create_clock -period 4.16666 -name gt_refclk     -add [get_ports gt_refclk_p]
#create_clock -period 4.16666 -name txpi_refclk   -add [get_ports txpi_refclk_p]
#create_clock -period 4.16666 -name hop_gt_refclk -add [get_ports hop_gt_refclk_p]

create_clock -period 6.40000 -name eth_gtrefclk -add [get_ports eth_gtrefclk_p]
#create_clock -period 1.60000 -name eth_gtrefclk -add [get_ports eth_gtrefclk_p]

#create_clock -period 4.16666 -name clnrxusrclk -add [get_ports clnrxusrclk_in_p]
create_clock -period 4.16666 -name gtfanout -add [get_ports gtfanout_in_p]
create_clock -period 4.16684 -name ddmtdclk -add [get_ports ddmtdclk_in_p]
#create_clock -period 4.16684 -name ddmtdclk -add [get_ports dmtdclk]

#set_false_path -from [get_ports clnrxusrclk_in_p] -to [get_pins DDMTD_inst/ddmtd/DMTD_B/gen_straight.clk_i_d0_reg/D]
#set_false_path -from [get_ports gtfanout_in_p]    -to [get_pins DDMTD_inst/ddmtd/DMTD_A/gen_straight.clk_i_d0_reg/D]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_125]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks gt_refclk]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks txpi_refclk]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks hop_gt_refclk]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks eth_gtrefclk]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/rxoutclk_out[0]}]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ gt_inst/ch[0].*/txoutclk_out[0]}]]
#set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/rxoutclk_out[0]}]]
#set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets -hier -filter {NAME =~ hop_gt_inst/ch[0].*/txoutclk_out[0]}]]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *ddmtd/DMTD_A/tag_o_reg*/C}]]

set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_*]
#set_output_delay -clock gt_refclk 3.000 [get_ports rxrecclk_*]
#set_output_delay -clock gt_refclk 3.000 [get_ports rxUserClk_1]
#set_output_delay -clock gt_refclk 3.000 [get_ports txUserClk]

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
