library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity monostable is
    Port (
        rst       : in  std_logic;
        clk       : in  std_logic;
        data_in   : in  std_logic;
        pulse_out : out std_logic
    );
end monostable;

architecture Behavioral of monostable is

    signal a : std_logic := '0';
    signal b : std_logic := '0';

begin

    process(clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                a <= '0';
            else
                a <= data_in;
            end if;
        end if;
    end process;

    process(clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                b <= '0';
            else
                b <= a;
            end if;
        end if;
    end process;

    pulse_out <= a and not(b);

end Behavioral;