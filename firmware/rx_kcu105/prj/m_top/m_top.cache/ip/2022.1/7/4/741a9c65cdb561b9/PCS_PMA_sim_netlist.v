// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Tue Aug  8 18:27:54 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ PCS_PMA_sim_netlist.v
// Design      : PCS_PMA
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku040-ffva1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire \<const1> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire NLW_U0_mmcm_locked_out_UNCONNECTED;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign mmcm_locked_out = \<const1> ;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(NLW_U0_mmcm_locked_out_UNCONNECTED),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
  VCC VCC
       (.P(\<const1> ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    pma_reset_out,
    signal_detect,
    userclk2,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  input pma_reset_out;
  input signal_detect;
  input userclk2;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [2:0]configuration_vector;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_out;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire pma_reset_out;
  wire powerdown;
  wire resetdone;
  wire resetdone_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire userclk2;
  wire NLW_PCS_PMA_core_an_enable_UNCONNECTED;
  wire NLW_PCS_PMA_core_an_interrupt_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_den_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_dwe_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_req_UNCONNECTED;
  wire NLW_PCS_PMA_core_en_cdet_UNCONNECTED;
  wire NLW_PCS_PMA_core_ewrap_UNCONNECTED;
  wire NLW_PCS_PMA_core_loc_ref_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_out_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_tri_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_PCS_PMA_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101010000" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "PCS_PMA" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_8 PCS_PMA_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_PCS_PMA_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_PCS_PMA_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(NLW_PCS_PMA_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_PCS_PMA_core_drp_den_UNCONNECTED),
        .drp_di(NLW_PCS_PMA_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_PCS_PMA_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_PCS_PMA_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_PCS_PMA_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_PCS_PMA_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_PCS_PMA_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_PCS_PMA_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_PCS_PMA_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(pma_reset_out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_PCS_PMA_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_PCS_PMA_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_PCS_PMA_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(userclk2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_sync_block sync_block_reset_done
       (.data_in(resetdone_i),
        .resetdone(resetdone),
        .userclk2(userclk2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispval),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(resetdone_i),
        .enablealign(enablealign),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .mgt_tx_reset(mgt_tx_reset),
        .pma_reset_out(pma_reset_out),
        .powerdown(powerdown),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk_out(rxoutclk_out),
        .rxp(rxp),
        .txbuferr(txbuferr),
        .txchardispmode_reg_reg_0(txchardispmode),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk_out(txoutclk_out),
        .txp(txp),
        .userclk2(userclk2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_clocking
   (gtrefclk_out,
    userclk2,
    userclk,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    rxoutclk,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output gtrefclk_out;
  output userclk2;
  output userclk;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input rxoutclk;
  output lopt;
  output lopt_1;
  input lopt_2;
  input lopt_3;
  input lopt_4;
  input lopt_5;

  wire \<const0> ;
  wire \<const1> ;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire \^lopt ;
  wire \^lopt_1 ;
  wire \^lopt_2 ;
  wire \^lopt_3 ;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;

  assign \^lopt  = lopt_2;
  assign \^lopt_1  = lopt_3;
  assign \^lopt_2  = lopt_4;
  assign \^lopt_3  = lopt_5;
  assign lopt = \<const1> ;
  assign lopt_1 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE3 #(
    .REFCLK_EN_TX_PATH(1'b0),
    .REFCLK_HROW_CK_SEL(2'b00),
    .REFCLK_ICNTL_RX(2'b00)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    rxrecclk_bufg_inst
       (.CE(\^lopt ),
        .CEMASK(1'b1),
        .CLR(\^lopt_1 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(rxoutclk),
        .O(rxuserclk2_out));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk2_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(txoutclk),
        .O(userclk2));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b1}),
        .I(txoutclk),
        .O(userclk));
endmodule

(* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt
   (gtwiz_userclk_tx_active_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    cpllrefclksel_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drpwe_in,
    eyescanreset_in,
    eyescantrigger_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    gtrefclk1_in,
    loopback_in,
    pcsrsvdin_in,
    rx8b10ben_in,
    rxbufreset_in,
    rxcdrhold_in,
    rxcommadeten_in,
    rxdfelpmreset_in,
    rxlpmen_in,
    rxmcommaalignen_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxrate_in,
    rxusrclk_in,
    rxusrclk2_in,
    tx8b10ben_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdiffctrl_in,
    txelecidle_in,
    txinhibit_in,
    txpcsreset_in,
    txpd_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txusrclk_in,
    txusrclk2_in,
    cplllock_out,
    dmonitorout_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxclkcorcnt_out,
    rxcommadet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxresetdone_out,
    txbufstatus_out,
    txoutclk_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txresetdone_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [2:0]cpllrefclksel_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drpwe_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [2:0]loopback_in;
  input [15:0]pcsrsvdin_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcommadeten_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [2:0]rxrate_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]tx8b10ben_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txelecidle_in;
  input [0:0]txinhibit_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]cplllock_out;
  output [16:0]dmonitorout_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcommadet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxresetdone_out;
  output [1:0]txbufstatus_out;
  output [0:0]txoutclk_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txresetdone_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [2:0]NLW_inst_bufgtce_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtcemask_out_UNCONNECTED;
  wire [8:0]NLW_inst_bufgtdiv_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtreset_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtrstmask_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllfbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_cplllock_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllrefclklost_out_UNCONNECTED;
  wire [16:0]NLW_inst_dmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_dmonitoroutclk_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_common_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_common_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtrefclkmonitor_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxn_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxp_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierategen3_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierateidle_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllpd_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllreset_out_UNCONNECTED;
  wire [0:0]NLW_inst_pciesynctxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieusergen3rdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserphystatusrst_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserratestart_out_UNCONNECTED;
  wire [11:0]NLW_inst_pcsrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_phystatus_out_UNCONNECTED;
  wire [7:0]NLW_inst_pinrsrvdas_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout0_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout1_out_UNCONNECTED;
  wire [0:0]NLW_inst_powerpresent_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0refclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1refclklost_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor0_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor0_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_resetexception_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrlock_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrphdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanbondseq_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanrealign_out_UNCONNECTED;
  wire [4:0]NLW_inst_rxchbondo_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxckcaldone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcominitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcommadet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomsasdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomwakedet_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl3_out_UNCONNECTED;
  wire [127:0]NLW_inst_rxdata_out_UNCONNECTED;
  wire [7:0]NLW_inst_rxdataextendrsvd_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxdatavalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxelecidle_out_UNCONNECTED;
  wire [5:0]NLW_inst_rxheader_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxheadervalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpstresetdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED;
  wire [6:0]NLW_inst_rxmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobestarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphalignerr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbslocked_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxratedone_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk0_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk0sel_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk1_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk1sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclkout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsliderdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipoutclkrdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslippmardy_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxstartofseq_out_UNCONNECTED;
  wire [2:0]NLW_inst_rxstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxvalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_tcongpo_out_UNCONNECTED;
  wire [0:0]NLW_inst_tconrsvdout0_out_UNCONNECTED;
  wire [0:0]NLW_inst_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_txcomfinish_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdccdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphinitdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_txratedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdaddr_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubden_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdi_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdwe_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubmdmtdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubtxuart_out_UNCONNECTED;

  assign cplllock_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001" *) 
  (* C_COMMON_SCALING_FACTOR = "1" *) 
  (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
  (* C_ENABLE_COMMON_USRCLK = "0" *) 
  (* C_FORCE_COMMONS = "0" *) 
  (* C_FREERUN_FREQUENCY = "62.500000" *) 
  (* C_GT_REV = "17" *) 
  (* C_GT_TYPE = "0" *) 
  (* C_INCLUDE_CPLL_CAL = "2" *) 
  (* C_LOCATE_COMMON = "0" *) 
  (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) 
  (* C_LOCATE_RESET_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_TX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) 
  (* C_PCIE_CORECLK_FREQ = "250" *) 
  (* C_PCIE_ENABLE = "0" *) 
  (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) 
  (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
  (* C_RX_BUFFBYPASS_MODE = "0" *) 
  (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_RX_BUFFER_MODE = "1" *) 
  (* C_RX_CB_DISP = "8'b00000000" *) 
  (* C_RX_CB_K = "8'b00000000" *) 
  (* C_RX_CB_LEN_SEQ = "1" *) 
  (* C_RX_CB_MAX_LEVEL = "1" *) 
  (* C_RX_CB_NUM_SEQ = "0" *) 
  (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_CC_DISP = "8'b00000000" *) 
  (* C_RX_CC_ENABLE = "1" *) 
  (* C_RX_CC_K = "8'b00010001" *) 
  (* C_RX_CC_LEN_SEQ = "2" *) 
  (* C_RX_CC_NUM_SEQ = "2" *) 
  (* C_RX_CC_PERIODICITY = "5000" *) 
  (* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) 
  (* C_RX_COMMA_M_ENABLE = "1" *) 
  (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
  (* C_RX_COMMA_P_ENABLE = "1" *) 
  (* C_RX_COMMA_P_VAL = "10'b0101111100" *) 
  (* C_RX_DATA_DECODING = "1" *) 
  (* C_RX_ENABLE = "1" *) 
  (* C_RX_INT_DATA_WIDTH = "20" *) 
  (* C_RX_LINE_RATE = "1.250000" *) 
  (* C_RX_MASTER_CHANNEL_IDX = "0" *) 
  (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) 
  (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_RX_OUTCLK_SOURCE = "1" *) 
  (* C_RX_PLL_TYPE = "2" *) 
  (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_RX_SLIDE_MODE = "0" *) 
  (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_RX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_RX_USER_DATA_WIDTH = "16" *) 
  (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_RX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_SECONDARY_QPLL_ENABLE = "0" *) 
  (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
  (* C_SIM_CPLL_CAL_BYPASS = "1" *) 
  (* C_TOTAL_NUM_CHANNELS = "1" *) 
  (* C_TOTAL_NUM_COMMONS = "0" *) 
  (* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) 
  (* C_TXPROGDIV_FREQ_ENABLE = "1" *) 
  (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
  (* C_TXPROGDIV_FREQ_VAL = "125.000000" *) 
  (* C_TX_BUFFBYPASS_MODE = "0" *) 
  (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_TX_BUFFER_MODE = "1" *) 
  (* C_TX_DATA_ENCODING = "1" *) 
  (* C_TX_ENABLE = "1" *) 
  (* C_TX_INT_DATA_WIDTH = "20" *) 
  (* C_TX_LINE_RATE = "1.250000" *) 
  (* C_TX_MASTER_CHANNEL_IDX = "0" *) 
  (* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) 
  (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_TX_OUTCLK_SOURCE = "4" *) 
  (* C_TX_PLL_TYPE = "2" *) 
  (* C_TX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_TX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_TX_USER_DATA_WIDTH = "16" *) 
  (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_TX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_top inst
       (.bgbypassb_in(1'b1),
        .bgmonitorenb_in(1'b1),
        .bgpdb_in(1'b1),
        .bgrcalovrd_in({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .bgrcalovrdenb_in(1'b1),
        .bufgtce_out(NLW_inst_bufgtce_out_UNCONNECTED[2:0]),
        .bufgtcemask_out(NLW_inst_bufgtcemask_out_UNCONNECTED[2:0]),
        .bufgtdiv_out(NLW_inst_bufgtdiv_out_UNCONNECTED[8:0]),
        .bufgtreset_out(NLW_inst_bufgtreset_out_UNCONNECTED[2:0]),
        .bufgtrstmask_out(NLW_inst_bufgtrstmask_out_UNCONNECTED[2:0]),
        .cdrstepdir_in(1'b0),
        .cdrstepsq_in(1'b0),
        .cdrstepsx_in(1'b0),
        .cfgreset_in(1'b0),
        .clkrsvd0_in(1'b0),
        .clkrsvd1_in(1'b0),
        .cpllfbclklost_out(NLW_inst_cpllfbclklost_out_UNCONNECTED[0]),
        .cpllfreqlock_in(1'b0),
        .cplllock_out(NLW_inst_cplllock_out_UNCONNECTED[0]),
        .cplllockdetclk_in(1'b0),
        .cplllocken_in(1'b1),
        .cpllpd_in(1'b0),
        .cpllrefclklost_out(NLW_inst_cpllrefclklost_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .cpllreset_in(1'b0),
        .dmonfiforeset_in(1'b0),
        .dmonitorclk_in(1'b0),
        .dmonitorout_out(NLW_inst_dmonitorout_out_UNCONNECTED[16:0]),
        .dmonitoroutclk_out(NLW_inst_dmonitoroutclk_out_UNCONNECTED[0]),
        .drpaddr_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_common_in(1'b0),
        .drpclk_in(drpclk_in),
        .drpdi_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_common_out(NLW_inst_drpdo_common_out_UNCONNECTED[15:0]),
        .drpdo_out(NLW_inst_drpdo_out_UNCONNECTED[15:0]),
        .drpen_common_in(1'b0),
        .drpen_in(1'b0),
        .drprdy_common_out(NLW_inst_drprdy_common_out_UNCONNECTED[0]),
        .drprdy_out(NLW_inst_drprdy_out_UNCONNECTED[0]),
        .drprst_in(1'b0),
        .drpwe_common_in(1'b0),
        .drpwe_in(1'b0),
        .elpcaldvorwren_in(1'b0),
        .elpcalpaorwren_in(1'b0),
        .evoddphicaldone_in(1'b0),
        .evoddphicalstart_in(1'b0),
        .evoddphidrden_in(1'b0),
        .evoddphidwren_in(1'b0),
        .evoddphixrden_in(1'b0),
        .evoddphixwren_in(1'b0),
        .eyescandataerror_out(NLW_inst_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanmode_in(1'b0),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .freqos_in(1'b0),
        .gtgrefclk0_in(1'b0),
        .gtgrefclk1_in(1'b0),
        .gtgrefclk_in(1'b0),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtnorthrefclk00_in(1'b0),
        .gtnorthrefclk01_in(1'b0),
        .gtnorthrefclk0_in(1'b0),
        .gtnorthrefclk10_in(1'b0),
        .gtnorthrefclk11_in(1'b0),
        .gtnorthrefclk1_in(1'b0),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk00_in(1'b0),
        .gtrefclk01_in(1'b0),
        .gtrefclk0_in(gtrefclk0_in),
        .gtrefclk10_in(1'b0),
        .gtrefclk11_in(1'b0),
        .gtrefclk1_in(1'b0),
        .gtrefclkmonitor_out(NLW_inst_gtrefclkmonitor_out_UNCONNECTED[0]),
        .gtresetsel_in(1'b0),
        .gtrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtrxreset_in(1'b0),
        .gtrxresetsel_in(1'b0),
        .gtsouthrefclk00_in(1'b0),
        .gtsouthrefclk01_in(1'b0),
        .gtsouthrefclk0_in(1'b0),
        .gtsouthrefclk10_in(1'b0),
        .gtsouthrefclk11_in(1'b0),
        .gtsouthrefclk1_in(1'b0),
        .gttxreset_in(1'b0),
        .gttxresetsel_in(1'b0),
        .gtwiz_buffbypass_rx_done_out(NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_error_out(NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_reset_in(1'b0),
        .gtwiz_buffbypass_rx_start_user_in(1'b0),
        .gtwiz_buffbypass_tx_done_out(NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_error_out(NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_reset_in(1'b0),
        .gtwiz_buffbypass_tx_start_user_in(1'b0),
        .gtwiz_gthe3_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe3_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe3_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gtye4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_qpll0lock_in(1'b0),
        .gtwiz_reset_qpll0reset_out(NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED[0]),
        .gtwiz_reset_qpll1lock_in(1'b0),
        .gtwiz_reset_qpll1reset_out(NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_cdr_stable_out(NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_in(1'b0),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_in(1'b0),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_rx_active_out(NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_reset_in(1'b0),
        .gtwiz_userclk_rx_srcclk_out(NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk2_out(NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk_out(NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userclk_tx_active_out(NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_reset_in(1'b0),
        .gtwiz_userclk_tx_srcclk_out(NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk2_out(NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk_out(NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .gtyrxn_in(1'b0),
        .gtyrxp_in(1'b0),
        .gtytxn_out(NLW_inst_gtytxn_out_UNCONNECTED[0]),
        .gtytxp_out(NLW_inst_gtytxp_out_UNCONNECTED[0]),
        .incpctrl_in(1'b0),
        .loopback_in({1'b0,1'b0,1'b0}),
        .looprsvd_in(1'b0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .lpbkrxtxseren_in(1'b0),
        .lpbktxrxseren_in(1'b0),
        .pcieeqrxeqadaptdone_in(1'b0),
        .pcierategen3_out(NLW_inst_pcierategen3_out_UNCONNECTED[0]),
        .pcierateidle_out(NLW_inst_pcierateidle_out_UNCONNECTED[0]),
        .pcierateqpll0_in(1'b0),
        .pcierateqpll1_in(1'b0),
        .pcierateqpllpd_out(NLW_inst_pcierateqpllpd_out_UNCONNECTED[1:0]),
        .pcierateqpllreset_out(NLW_inst_pcierateqpllreset_out_UNCONNECTED[1:0]),
        .pcierstidle_in(1'b0),
        .pciersttxsyncstart_in(1'b0),
        .pciesynctxsyncdone_out(NLW_inst_pciesynctxsyncdone_out_UNCONNECTED[0]),
        .pcieusergen3rdy_out(NLW_inst_pcieusergen3rdy_out_UNCONNECTED[0]),
        .pcieuserphystatusrst_out(NLW_inst_pcieuserphystatusrst_out_UNCONNECTED[0]),
        .pcieuserratedone_in(1'b0),
        .pcieuserratestart_out(NLW_inst_pcieuserratestart_out_UNCONNECTED[0]),
        .pcsrsvdin2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdout_out(NLW_inst_pcsrsvdout_out_UNCONNECTED[11:0]),
        .phystatus_out(NLW_inst_phystatus_out_UNCONNECTED[0]),
        .pinrsrvdas_out(NLW_inst_pinrsrvdas_out_UNCONNECTED[7:0]),
        .pmarsvd0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdout0_out(NLW_inst_pmarsvdout0_out_UNCONNECTED[7:0]),
        .pmarsvdout1_out(NLW_inst_pmarsvdout1_out_UNCONNECTED[7:0]),
        .powerpresent_out(NLW_inst_powerpresent_out_UNCONNECTED[0]),
        .qpll0clk_in(1'b0),
        .qpll0clkrsvd0_in(1'b0),
        .qpll0clkrsvd1_in(1'b0),
        .qpll0fbclklost_out(NLW_inst_qpll0fbclklost_out_UNCONNECTED[0]),
        .qpll0fbdiv_in(1'b0),
        .qpll0freqlock_in(1'b0),
        .qpll0lock_out(NLW_inst_qpll0lock_out_UNCONNECTED[0]),
        .qpll0lockdetclk_in(1'b0),
        .qpll0locken_in(1'b0),
        .qpll0outclk_out(NLW_inst_qpll0outclk_out_UNCONNECTED[0]),
        .qpll0outrefclk_out(NLW_inst_qpll0outrefclk_out_UNCONNECTED[0]),
        .qpll0pd_in(1'b1),
        .qpll0refclk_in(1'b0),
        .qpll0refclklost_out(NLW_inst_qpll0refclklost_out_UNCONNECTED[0]),
        .qpll0refclksel_in({1'b0,1'b0,1'b1}),
        .qpll0reset_in(1'b1),
        .qpll1clk_in(1'b0),
        .qpll1clkrsvd0_in(1'b0),
        .qpll1clkrsvd1_in(1'b0),
        .qpll1fbclklost_out(NLW_inst_qpll1fbclklost_out_UNCONNECTED[0]),
        .qpll1fbdiv_in(1'b0),
        .qpll1freqlock_in(1'b0),
        .qpll1lock_out(NLW_inst_qpll1lock_out_UNCONNECTED[0]),
        .qpll1lockdetclk_in(1'b0),
        .qpll1locken_in(1'b0),
        .qpll1outclk_out(NLW_inst_qpll1outclk_out_UNCONNECTED[0]),
        .qpll1outrefclk_out(NLW_inst_qpll1outrefclk_out_UNCONNECTED[0]),
        .qpll1pd_in(1'b1),
        .qpll1refclk_in(1'b0),
        .qpll1refclklost_out(NLW_inst_qpll1refclklost_out_UNCONNECTED[0]),
        .qpll1refclksel_in({1'b0,1'b0,1'b1}),
        .qpll1reset_in(1'b1),
        .qplldmonitor0_out(NLW_inst_qplldmonitor0_out_UNCONNECTED[7:0]),
        .qplldmonitor1_out(NLW_inst_qplldmonitor1_out_UNCONNECTED[7:0]),
        .qpllrsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd3_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd4_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rcalenb_in(1'b1),
        .refclkoutmonitor0_out(NLW_inst_refclkoutmonitor0_out_UNCONNECTED[0]),
        .refclkoutmonitor1_out(NLW_inst_refclkoutmonitor1_out_UNCONNECTED[0]),
        .resetexception_out(NLW_inst_resetexception_out_UNCONNECTED[0]),
        .resetovrd_in(1'b0),
        .rstclkentx_in(1'b0),
        .rx8b10ben_in(1'b1),
        .rxafecfoken_in(1'b0),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({\^rxbufstatus_out ,NLW_inst_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_inst_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_inst_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrfreqreset_in(1'b0),
        .rxcdrhold_in(1'b0),
        .rxcdrlock_out(NLW_inst_rxcdrlock_out_UNCONNECTED[0]),
        .rxcdrovrden_in(1'b0),
        .rxcdrphdone_out(NLW_inst_rxcdrphdone_out_UNCONNECTED[0]),
        .rxcdrreset_in(1'b0),
        .rxcdrresetrsv_in(1'b0),
        .rxchanbondseq_out(NLW_inst_rxchanbondseq_out_UNCONNECTED[0]),
        .rxchanisaligned_out(NLW_inst_rxchanisaligned_out_UNCONNECTED[0]),
        .rxchanrealign_out(NLW_inst_rxchanrealign_out_UNCONNECTED[0]),
        .rxchbonden_in(1'b0),
        .rxchbondi_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rxchbondlevel_in({1'b0,1'b0,1'b0}),
        .rxchbondmaster_in(1'b0),
        .rxchbondo_out(NLW_inst_rxchbondo_out_UNCONNECTED[4:0]),
        .rxchbondslave_in(1'b0),
        .rxckcaldone_out(NLW_inst_rxckcaldone_out_UNCONNECTED[0]),
        .rxckcalreset_in(1'b0),
        .rxckcalstart_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxcominitdet_out(NLW_inst_rxcominitdet_out_UNCONNECTED[0]),
        .rxcommadet_out(NLW_inst_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxcomsasdet_out(NLW_inst_rxcomsasdet_out_UNCONNECTED[0]),
        .rxcomwakedet_out(NLW_inst_rxcomwakedet_out_UNCONNECTED[0]),
        .rxctrl0_out({NLW_inst_rxctrl0_out_UNCONNECTED[15:2],\^rxctrl0_out }),
        .rxctrl1_out({NLW_inst_rxctrl1_out_UNCONNECTED[15:2],\^rxctrl1_out }),
        .rxctrl2_out({NLW_inst_rxctrl2_out_UNCONNECTED[7:2],\^rxctrl2_out }),
        .rxctrl3_out({NLW_inst_rxctrl3_out_UNCONNECTED[7:2],\^rxctrl3_out }),
        .rxdata_out(NLW_inst_rxdata_out_UNCONNECTED[127:0]),
        .rxdataextendrsvd_out(NLW_inst_rxdataextendrsvd_out_UNCONNECTED[7:0]),
        .rxdatavalid_out(NLW_inst_rxdatavalid_out_UNCONNECTED[1:0]),
        .rxdccforcestart_in(1'b0),
        .rxdfeagcctrl_in({1'b0,1'b1}),
        .rxdfeagchold_in(1'b0),
        .rxdfeagcovrden_in(1'b0),
        .rxdfecfokfcnum_in(1'b0),
        .rxdfecfokfen_in(1'b0),
        .rxdfecfokfpulse_in(1'b0),
        .rxdfecfokhold_in(1'b0),
        .rxdfecfokovren_in(1'b0),
        .rxdfekhhold_in(1'b0),
        .rxdfekhovrden_in(1'b0),
        .rxdfelfhold_in(1'b0),
        .rxdfelfovrden_in(1'b0),
        .rxdfelpmreset_in(1'b0),
        .rxdfetap10hold_in(1'b0),
        .rxdfetap10ovrden_in(1'b0),
        .rxdfetap11hold_in(1'b0),
        .rxdfetap11ovrden_in(1'b0),
        .rxdfetap12hold_in(1'b0),
        .rxdfetap12ovrden_in(1'b0),
        .rxdfetap13hold_in(1'b0),
        .rxdfetap13ovrden_in(1'b0),
        .rxdfetap14hold_in(1'b0),
        .rxdfetap14ovrden_in(1'b0),
        .rxdfetap15hold_in(1'b0),
        .rxdfetap15ovrden_in(1'b0),
        .rxdfetap2hold_in(1'b0),
        .rxdfetap2ovrden_in(1'b0),
        .rxdfetap3hold_in(1'b0),
        .rxdfetap3ovrden_in(1'b0),
        .rxdfetap4hold_in(1'b0),
        .rxdfetap4ovrden_in(1'b0),
        .rxdfetap5hold_in(1'b0),
        .rxdfetap5ovrden_in(1'b0),
        .rxdfetap6hold_in(1'b0),
        .rxdfetap6ovrden_in(1'b0),
        .rxdfetap7hold_in(1'b0),
        .rxdfetap7ovrden_in(1'b0),
        .rxdfetap8hold_in(1'b0),
        .rxdfetap8ovrden_in(1'b0),
        .rxdfetap9hold_in(1'b0),
        .rxdfetap9ovrden_in(1'b0),
        .rxdfeuthold_in(1'b0),
        .rxdfeutovrden_in(1'b0),
        .rxdfevphold_in(1'b0),
        .rxdfevpovrden_in(1'b0),
        .rxdfevsen_in(1'b0),
        .rxdfexyden_in(1'b1),
        .rxdlybypass_in(1'b1),
        .rxdlyen_in(1'b0),
        .rxdlyovrden_in(1'b0),
        .rxdlysreset_in(1'b0),
        .rxdlysresetdone_out(NLW_inst_rxdlysresetdone_out_UNCONNECTED[0]),
        .rxelecidle_out(NLW_inst_rxelecidle_out_UNCONNECTED[0]),
        .rxelecidlemode_in({1'b1,1'b1}),
        .rxeqtraining_in(1'b0),
        .rxgearboxslip_in(1'b0),
        .rxheader_out(NLW_inst_rxheader_out_UNCONNECTED[5:0]),
        .rxheadervalid_out(NLW_inst_rxheadervalid_out_UNCONNECTED[1:0]),
        .rxlatclk_in(1'b0),
        .rxlfpstresetdet_out(NLW_inst_rxlfpstresetdet_out_UNCONNECTED[0]),
        .rxlfpsu2lpexitdet_out(NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED[0]),
        .rxlfpsu3wakedet_out(NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED[0]),
        .rxlpmen_in(1'b1),
        .rxlpmgchold_in(1'b0),
        .rxlpmgcovrden_in(1'b0),
        .rxlpmhfhold_in(1'b0),
        .rxlpmhfovrden_in(1'b0),
        .rxlpmlfhold_in(1'b0),
        .rxlpmlfklovrden_in(1'b0),
        .rxlpmoshold_in(1'b0),
        .rxlpmosovrden_in(1'b0),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxmonitorout_out(NLW_inst_rxmonitorout_out_UNCONNECTED[6:0]),
        .rxmonitorsel_in({1'b0,1'b0}),
        .rxoobreset_in(1'b0),
        .rxoscalreset_in(1'b0),
        .rxoshold_in(1'b0),
        .rxosintcfg_in({1'b1,1'b1,1'b0,1'b1}),
        .rxosintdone_out(NLW_inst_rxosintdone_out_UNCONNECTED[0]),
        .rxosinten_in(1'b1),
        .rxosinthold_in(1'b0),
        .rxosintovrden_in(1'b0),
        .rxosintstarted_out(NLW_inst_rxosintstarted_out_UNCONNECTED[0]),
        .rxosintstrobe_in(1'b0),
        .rxosintstrobedone_out(NLW_inst_rxosintstrobedone_out_UNCONNECTED[0]),
        .rxosintstrobestarted_out(NLW_inst_rxosintstrobestarted_out_UNCONNECTED[0]),
        .rxosinttestovrden_in(1'b0),
        .rxosovrden_in(1'b0),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkfabric_out(NLW_inst_rxoutclkfabric_out_UNCONNECTED[0]),
        .rxoutclkpcs_out(NLW_inst_rxoutclkpcs_out_UNCONNECTED[0]),
        .rxoutclksel_in({1'b0,1'b1,1'b0}),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpd_in[1],1'b0}),
        .rxphalign_in(1'b0),
        .rxphaligndone_out(NLW_inst_rxphaligndone_out_UNCONNECTED[0]),
        .rxphalignen_in(1'b0),
        .rxphalignerr_out(NLW_inst_rxphalignerr_out_UNCONNECTED[0]),
        .rxphdlypd_in(1'b1),
        .rxphdlyreset_in(1'b0),
        .rxphovrden_in(1'b0),
        .rxpllclksel_in({1'b0,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_inst_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_inst_rxprbserr_out_UNCONNECTED[0]),
        .rxprbslocked_out(NLW_inst_rxprbslocked_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxprgdivresetdone_out(NLW_inst_rxprgdivresetdone_out_UNCONNECTED[0]),
        .rxprogdivreset_in(1'b0),
        .rxqpien_in(1'b0),
        .rxqpisenn_out(NLW_inst_rxqpisenn_out_UNCONNECTED[0]),
        .rxqpisenp_out(NLW_inst_rxqpisenp_out_UNCONNECTED[0]),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxratedone_out(NLW_inst_rxratedone_out_UNCONNECTED[0]),
        .rxratemode_in(1'b0),
        .rxrecclk0_sel_out(NLW_inst_rxrecclk0_sel_out_UNCONNECTED[1:0]),
        .rxrecclk0sel_out(NLW_inst_rxrecclk0sel_out_UNCONNECTED[0]),
        .rxrecclk1_sel_out(NLW_inst_rxrecclk1_sel_out_UNCONNECTED[1:0]),
        .rxrecclk1sel_out(NLW_inst_rxrecclk1sel_out_UNCONNECTED[0]),
        .rxrecclkout_out(NLW_inst_rxrecclkout_out_UNCONNECTED[0]),
        .rxresetdone_out(NLW_inst_rxresetdone_out_UNCONNECTED[0]),
        .rxslide_in(1'b0),
        .rxsliderdy_out(NLW_inst_rxsliderdy_out_UNCONNECTED[0]),
        .rxslipdone_out(NLW_inst_rxslipdone_out_UNCONNECTED[0]),
        .rxslipoutclk_in(1'b0),
        .rxslipoutclkrdy_out(NLW_inst_rxslipoutclkrdy_out_UNCONNECTED[0]),
        .rxslippma_in(1'b0),
        .rxslippmardy_out(NLW_inst_rxslippmardy_out_UNCONNECTED[0]),
        .rxstartofseq_out(NLW_inst_rxstartofseq_out_UNCONNECTED[1:0]),
        .rxstatus_out(NLW_inst_rxstatus_out_UNCONNECTED[2:0]),
        .rxsyncallin_in(1'b0),
        .rxsyncdone_out(NLW_inst_rxsyncdone_out_UNCONNECTED[0]),
        .rxsyncin_in(1'b0),
        .rxsyncmode_in(1'b0),
        .rxsyncout_out(NLW_inst_rxsyncout_out_UNCONNECTED[0]),
        .rxsysclksel_in({1'b0,1'b0}),
        .rxtermination_in(1'b0),
        .rxuserrdy_in(1'b1),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(rxusrclk_in),
        .rxvalid_out(NLW_inst_rxvalid_out_UNCONNECTED[0]),
        .sdm0data_in(1'b0),
        .sdm0finalout_out(NLW_inst_sdm0finalout_out_UNCONNECTED[0]),
        .sdm0reset_in(1'b0),
        .sdm0testdata_out(NLW_inst_sdm0testdata_out_UNCONNECTED[0]),
        .sdm0toggle_in(1'b0),
        .sdm0width_in(1'b0),
        .sdm1data_in(1'b0),
        .sdm1finalout_out(NLW_inst_sdm1finalout_out_UNCONNECTED[0]),
        .sdm1reset_in(1'b0),
        .sdm1testdata_out(NLW_inst_sdm1testdata_out_UNCONNECTED[0]),
        .sdm1toggle_in(1'b0),
        .sdm1width_in(1'b0),
        .sigvalidclk_in(1'b0),
        .tcongpi_in(1'b0),
        .tcongpo_out(NLW_inst_tcongpo_out_UNCONNECTED[0]),
        .tconpowerup_in(1'b0),
        .tconreset_in(1'b0),
        .tconrsvdin1_in(1'b0),
        .tconrsvdout0_out(NLW_inst_tconrsvdout0_out_UNCONNECTED[0]),
        .tstin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10bbypass_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10ben_in(1'b1),
        .txbufdiffctrl_in({1'b0,1'b0,1'b0}),
        .txbufstatus_out({\^txbufstatus_out ,NLW_inst_txbufstatus_out_UNCONNECTED[0]}),
        .txcomfinish_out(NLW_inst_txcomfinish_out_UNCONNECTED[0]),
        .txcominit_in(1'b0),
        .txcomsas_in(1'b0),
        .txcomwake_in(1'b0),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in[1:0]}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in[1:0]}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in[1:0]}),
        .txdata_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdataextendrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdccdone_out(NLW_inst_txdccdone_out_UNCONNECTED[0]),
        .txdccforcestart_in(1'b0),
        .txdccreset_in(1'b0),
        .txdeemph_in(1'b0),
        .txdetectrx_in(1'b0),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txdiffpd_in(1'b0),
        .txdlybypass_in(1'b1),
        .txdlyen_in(1'b0),
        .txdlyhold_in(1'b0),
        .txdlyovrden_in(1'b0),
        .txdlysreset_in(1'b0),
        .txdlysresetdone_out(NLW_inst_txdlysresetdone_out_UNCONNECTED[0]),
        .txdlyupdown_in(1'b0),
        .txelecidle_in(txelecidle_in),
        .txelforcestart_in(1'b0),
        .txheader_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txinhibit_in(1'b0),
        .txlatclk_in(1'b0),
        .txlfpstreset_in(1'b0),
        .txlfpsu2lpexit_in(1'b0),
        .txlfpsu3wake_in(1'b0),
        .txmaincursor_in({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txmargin_in({1'b0,1'b0,1'b0}),
        .txmuxdcdexhold_in(1'b0),
        .txmuxdcdorwren_in(1'b0),
        .txoneszeros_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txoutclkfabric_out(NLW_inst_txoutclkfabric_out_UNCONNECTED[0]),
        .txoutclkpcs_out(NLW_inst_txoutclkpcs_out_UNCONNECTED[0]),
        .txoutclksel_in({1'b1,1'b0,1'b1}),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpdelecidlemode_in(1'b0),
        .txphalign_in(1'b0),
        .txphaligndone_out(NLW_inst_txphaligndone_out_UNCONNECTED[0]),
        .txphalignen_in(1'b0),
        .txphdlypd_in(1'b1),
        .txphdlyreset_in(1'b0),
        .txphdlytstclk_in(1'b0),
        .txphinit_in(1'b0),
        .txphinitdone_out(NLW_inst_txphinitdone_out_UNCONNECTED[0]),
        .txphovrden_in(1'b0),
        .txpippmen_in(1'b0),
        .txpippmovrden_in(1'b0),
        .txpippmpd_in(1'b0),
        .txpippmsel_in(1'b0),
        .txpippmstepsize_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpisopd_in(1'b0),
        .txpllclksel_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_inst_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpostcursorinv_in(1'b0),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprecursorinv_in(1'b0),
        .txprgdivresetdone_out(NLW_inst_txprgdivresetdone_out_UNCONNECTED[0]),
        .txprogdivreset_in(1'b0),
        .txqpibiasen_in(1'b0),
        .txqpisenn_out(NLW_inst_txqpisenn_out_UNCONNECTED[0]),
        .txqpisenp_out(NLW_inst_txqpisenp_out_UNCONNECTED[0]),
        .txqpistrongpdown_in(1'b0),
        .txqpiweakpup_in(1'b0),
        .txrate_in({1'b0,1'b0,1'b0}),
        .txratedone_out(NLW_inst_txratedone_out_UNCONNECTED[0]),
        .txratemode_in(1'b0),
        .txresetdone_out(NLW_inst_txresetdone_out_UNCONNECTED[0]),
        .txsequence_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txswing_in(1'b0),
        .txsyncallin_in(1'b0),
        .txsyncdone_out(NLW_inst_txsyncdone_out_UNCONNECTED[0]),
        .txsyncin_in(1'b0),
        .txsyncmode_in(1'b0),
        .txsyncout_out(NLW_inst_txsyncout_out_UNCONNECTED[0]),
        .txsysclksel_in({1'b0,1'b0}),
        .txuserrdy_in(1'b1),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0),
        .ubcfgstreamen_in(1'b0),
        .ubdaddr_out(NLW_inst_ubdaddr_out_UNCONNECTED[0]),
        .ubden_out(NLW_inst_ubden_out_UNCONNECTED[0]),
        .ubdi_out(NLW_inst_ubdi_out_UNCONNECTED[0]),
        .ubdo_in(1'b0),
        .ubdrdy_in(1'b0),
        .ubdwe_out(NLW_inst_ubdwe_out_UNCONNECTED[0]),
        .ubenable_in(1'b0),
        .ubgpi_in(1'b0),
        .ubintr_in(1'b0),
        .ubiolmbrst_in(1'b0),
        .ubmbrst_in(1'b0),
        .ubmdmcapture_in(1'b0),
        .ubmdmdbgrst_in(1'b0),
        .ubmdmdbgupdate_in(1'b0),
        .ubmdmregen_in(1'b0),
        .ubmdmshift_in(1'b0),
        .ubmdmsysrst_in(1'b0),
        .ubmdmtck_in(1'b0),
        .ubmdmtdi_in(1'b0),
        .ubmdmtdo_out(NLW_inst_ubmdmtdo_out_UNCONNECTED[0]),
        .ubrsvdout_out(NLW_inst_ubrsvdout_out_UNCONNECTED[0]),
        .ubtxuart_out(NLW_inst_ubtxuart_out_UNCONNECTED[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gthe3_channel_wrapper
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gthe3_channel channel_inst
       (.cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(rxcdrlock_out),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .rxresetdone_out(rxresetdone_out),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(txresetdone_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_gthe3
   (gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxoutclk_out,
    txoutclk_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    rxmcommaalignen_in,
    rxusrclk_in,
    txelecidle_in,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_datapath_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gthe3_channel_wrapper \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_rxresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0 \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_txresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gtwiz_reset \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gtpowergood_out(gtpowergood_out),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in0(rst_in0),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxusrclk_in(rxusrclk_in));
endmodule

(* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001" *) (* C_COMMON_SCALING_FACTOR = "1" *) (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
(* C_ENABLE_COMMON_USRCLK = "0" *) (* C_FORCE_COMMONS = "0" *) (* C_FREERUN_FREQUENCY = "62.500000" *) 
(* C_GT_REV = "17" *) (* C_GT_TYPE = "0" *) (* C_INCLUDE_CPLL_CAL = "2" *) 
(* C_LOCATE_COMMON = "0" *) (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) (* C_LOCATE_RESET_CONTROLLER = "0" *) 
(* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) (* C_LOCATE_RX_USER_CLOCKING = "1" *) (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
(* C_LOCATE_TX_USER_CLOCKING = "1" *) (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) (* C_PCIE_CORECLK_FREQ = "250" *) 
(* C_PCIE_ENABLE = "0" *) (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
(* C_RX_BUFFBYPASS_MODE = "0" *) (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) (* C_RX_BUFFER_MODE = "1" *) 
(* C_RX_CB_DISP = "8'b00000000" *) (* C_RX_CB_K = "8'b00000000" *) (* C_RX_CB_LEN_SEQ = "1" *) 
(* C_RX_CB_MAX_LEVEL = "1" *) (* C_RX_CB_NUM_SEQ = "0" *) (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_CC_DISP = "8'b00000000" *) (* C_RX_CC_ENABLE = "1" *) (* C_RX_CC_K = "8'b00010001" *) 
(* C_RX_CC_LEN_SEQ = "2" *) (* C_RX_CC_NUM_SEQ = "2" *) (* C_RX_CC_PERIODICITY = "5000" *) 
(* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) (* C_RX_COMMA_M_ENABLE = "1" *) (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
(* C_RX_COMMA_P_ENABLE = "1" *) (* C_RX_COMMA_P_VAL = "10'b0101111100" *) (* C_RX_DATA_DECODING = "1" *) 
(* C_RX_ENABLE = "1" *) (* C_RX_INT_DATA_WIDTH = "20" *) (* C_RX_LINE_RATE = "1.250000" *) 
(* C_RX_MASTER_CHANNEL_IDX = "0" *) (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
(* C_RX_OUTCLK_SOURCE = "1" *) (* C_RX_PLL_TYPE = "2" *) (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_REFCLK_FREQUENCY = "156.250000" *) (* C_RX_SLIDE_MODE = "0" *) (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_RX_USER_CLOCKING_SOURCE = "0" *) (* C_RX_USER_DATA_WIDTH = "16" *) (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_RX_USRCLK_FREQUENCY = "62.500000" *) (* C_SECONDARY_QPLL_ENABLE = "0" *) (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
(* C_SIM_CPLL_CAL_BYPASS = "1" *) (* C_TOTAL_NUM_CHANNELS = "1" *) (* C_TOTAL_NUM_COMMONS = "0" *) 
(* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) (* C_TXPROGDIV_FREQ_ENABLE = "1" *) (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
(* C_TXPROGDIV_FREQ_VAL = "125.000000" *) (* C_TX_BUFFBYPASS_MODE = "0" *) (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
(* C_TX_BUFFER_MODE = "1" *) (* C_TX_DATA_ENCODING = "1" *) (* C_TX_ENABLE = "1" *) 
(* C_TX_INT_DATA_WIDTH = "20" *) (* C_TX_LINE_RATE = "1.250000" *) (* C_TX_MASTER_CHANNEL_IDX = "0" *) 
(* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) (* C_TX_OUTCLK_SOURCE = "4" *) 
(* C_TX_PLL_TYPE = "2" *) (* C_TX_REFCLK_FREQUENCY = "156.250000" *) (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_TX_USER_CLOCKING_SOURCE = "0" *) (* C_TX_USER_DATA_WIDTH = "16" *) (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_TX_USRCLK_FREQUENCY = "62.500000" *) (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_top
   (gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in,
    gtwiz_userclk_tx_srcclk_out,
    gtwiz_userclk_tx_usrclk_out,
    gtwiz_userclk_tx_usrclk2_out,
    gtwiz_userclk_tx_active_out,
    gtwiz_userclk_rx_reset_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_userclk_rx_srcclk_out,
    gtwiz_userclk_rx_usrclk_out,
    gtwiz_userclk_rx_usrclk2_out,
    gtwiz_userclk_rx_active_out,
    gtwiz_buffbypass_tx_reset_in,
    gtwiz_buffbypass_tx_start_user_in,
    gtwiz_buffbypass_tx_done_out,
    gtwiz_buffbypass_tx_error_out,
    gtwiz_buffbypass_rx_reset_in,
    gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out,
    gtwiz_buffbypass_rx_error_out,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_tx_done_in,
    gtwiz_reset_rx_done_in,
    gtwiz_reset_qpll0lock_in,
    gtwiz_reset_qpll1lock_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_reset_qpll0reset_out,
    gtwiz_reset_qpll1reset_out,
    gtwiz_gthe3_cpll_cal_txoutclk_period_in,
    gtwiz_gthe3_cpll_cal_cnt_tol_in,
    gtwiz_gthe3_cpll_cal_bufg_ce_in,
    gtwiz_gthe4_cpll_cal_txoutclk_period_in,
    gtwiz_gthe4_cpll_cal_cnt_tol_in,
    gtwiz_gthe4_cpll_cal_bufg_ce_in,
    gtwiz_gtye4_cpll_cal_txoutclk_period_in,
    gtwiz_gtye4_cpll_cal_cnt_tol_in,
    gtwiz_gtye4_cpll_cal_bufg_ce_in,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    bgbypassb_in,
    bgmonitorenb_in,
    bgpdb_in,
    bgrcalovrd_in,
    bgrcalovrdenb_in,
    drpaddr_common_in,
    drpclk_common_in,
    drpdi_common_in,
    drpen_common_in,
    drpwe_common_in,
    gtgrefclk0_in,
    gtgrefclk1_in,
    gtnorthrefclk00_in,
    gtnorthrefclk01_in,
    gtnorthrefclk10_in,
    gtnorthrefclk11_in,
    gtrefclk00_in,
    gtrefclk01_in,
    gtrefclk10_in,
    gtrefclk11_in,
    gtsouthrefclk00_in,
    gtsouthrefclk01_in,
    gtsouthrefclk10_in,
    gtsouthrefclk11_in,
    pcierateqpll0_in,
    pcierateqpll1_in,
    pmarsvd0_in,
    pmarsvd1_in,
    qpll0clkrsvd0_in,
    qpll0clkrsvd1_in,
    qpll0fbdiv_in,
    qpll0lockdetclk_in,
    qpll0locken_in,
    qpll0pd_in,
    qpll0refclksel_in,
    qpll0reset_in,
    qpll1clkrsvd0_in,
    qpll1clkrsvd1_in,
    qpll1fbdiv_in,
    qpll1lockdetclk_in,
    qpll1locken_in,
    qpll1pd_in,
    qpll1refclksel_in,
    qpll1reset_in,
    qpllrsvd1_in,
    qpllrsvd2_in,
    qpllrsvd3_in,
    qpllrsvd4_in,
    rcalenb_in,
    sdm0data_in,
    sdm0reset_in,
    sdm0toggle_in,
    sdm0width_in,
    sdm1data_in,
    sdm1reset_in,
    sdm1toggle_in,
    sdm1width_in,
    tcongpi_in,
    tconpowerup_in,
    tconreset_in,
    tconrsvdin1_in,
    ubcfgstreamen_in,
    ubdo_in,
    ubdrdy_in,
    ubenable_in,
    ubgpi_in,
    ubintr_in,
    ubiolmbrst_in,
    ubmbrst_in,
    ubmdmcapture_in,
    ubmdmdbgrst_in,
    ubmdmdbgupdate_in,
    ubmdmregen_in,
    ubmdmshift_in,
    ubmdmsysrst_in,
    ubmdmtck_in,
    ubmdmtdi_in,
    drpdo_common_out,
    drprdy_common_out,
    pmarsvdout0_out,
    pmarsvdout1_out,
    qpll0fbclklost_out,
    qpll0lock_out,
    qpll0outclk_out,
    qpll0outrefclk_out,
    qpll0refclklost_out,
    qpll1fbclklost_out,
    qpll1lock_out,
    qpll1outclk_out,
    qpll1outrefclk_out,
    qpll1refclklost_out,
    qplldmonitor0_out,
    qplldmonitor1_out,
    refclkoutmonitor0_out,
    refclkoutmonitor1_out,
    rxrecclk0_sel_out,
    rxrecclk1_sel_out,
    rxrecclk0sel_out,
    rxrecclk1sel_out,
    sdm0finalout_out,
    sdm0testdata_out,
    sdm1finalout_out,
    sdm1testdata_out,
    tcongpo_out,
    tconrsvdout0_out,
    ubdaddr_out,
    ubden_out,
    ubdi_out,
    ubdwe_out,
    ubmdmtdo_out,
    ubrsvdout_out,
    ubtxuart_out,
    cdrstepdir_in,
    cdrstepsq_in,
    cdrstepsx_in,
    cfgreset_in,
    clkrsvd0_in,
    clkrsvd1_in,
    cpllfreqlock_in,
    cplllockdetclk_in,
    cplllocken_in,
    cpllpd_in,
    cpllrefclksel_in,
    cpllreset_in,
    dmonfiforeset_in,
    dmonitorclk_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drprst_in,
    drpwe_in,
    elpcaldvorwren_in,
    elpcalpaorwren_in,
    evoddphicaldone_in,
    evoddphicalstart_in,
    evoddphidrden_in,
    evoddphidwren_in,
    evoddphixrden_in,
    evoddphixwren_in,
    eyescanmode_in,
    eyescanreset_in,
    eyescantrigger_in,
    freqos_in,
    gtgrefclk_in,
    gthrxn_in,
    gthrxp_in,
    gtnorthrefclk0_in,
    gtnorthrefclk1_in,
    gtrefclk0_in,
    gtrefclk1_in,
    gtresetsel_in,
    gtrsvd_in,
    gtrxreset_in,
    gtrxresetsel_in,
    gtsouthrefclk0_in,
    gtsouthrefclk1_in,
    gttxreset_in,
    gttxresetsel_in,
    incpctrl_in,
    gtyrxn_in,
    gtyrxp_in,
    loopback_in,
    looprsvd_in,
    lpbkrxtxseren_in,
    lpbktxrxseren_in,
    pcieeqrxeqadaptdone_in,
    pcierstidle_in,
    pciersttxsyncstart_in,
    pcieuserratedone_in,
    pcsrsvdin_in,
    pcsrsvdin2_in,
    pmarsvdin_in,
    qpll0clk_in,
    qpll0freqlock_in,
    qpll0refclk_in,
    qpll1clk_in,
    qpll1freqlock_in,
    qpll1refclk_in,
    resetovrd_in,
    rstclkentx_in,
    rx8b10ben_in,
    rxafecfoken_in,
    rxbufreset_in,
    rxcdrfreqreset_in,
    rxcdrhold_in,
    rxcdrovrden_in,
    rxcdrreset_in,
    rxcdrresetrsv_in,
    rxchbonden_in,
    rxchbondi_in,
    rxchbondlevel_in,
    rxchbondmaster_in,
    rxchbondslave_in,
    rxckcalreset_in,
    rxckcalstart_in,
    rxcommadeten_in,
    rxdfeagcctrl_in,
    rxdccforcestart_in,
    rxdfeagchold_in,
    rxdfeagcovrden_in,
    rxdfecfokfcnum_in,
    rxdfecfokfen_in,
    rxdfecfokfpulse_in,
    rxdfecfokhold_in,
    rxdfecfokovren_in,
    rxdfekhhold_in,
    rxdfekhovrden_in,
    rxdfelfhold_in,
    rxdfelfovrden_in,
    rxdfelpmreset_in,
    rxdfetap10hold_in,
    rxdfetap10ovrden_in,
    rxdfetap11hold_in,
    rxdfetap11ovrden_in,
    rxdfetap12hold_in,
    rxdfetap12ovrden_in,
    rxdfetap13hold_in,
    rxdfetap13ovrden_in,
    rxdfetap14hold_in,
    rxdfetap14ovrden_in,
    rxdfetap15hold_in,
    rxdfetap15ovrden_in,
    rxdfetap2hold_in,
    rxdfetap2ovrden_in,
    rxdfetap3hold_in,
    rxdfetap3ovrden_in,
    rxdfetap4hold_in,
    rxdfetap4ovrden_in,
    rxdfetap5hold_in,
    rxdfetap5ovrden_in,
    rxdfetap6hold_in,
    rxdfetap6ovrden_in,
    rxdfetap7hold_in,
    rxdfetap7ovrden_in,
    rxdfetap8hold_in,
    rxdfetap8ovrden_in,
    rxdfetap9hold_in,
    rxdfetap9ovrden_in,
    rxdfeuthold_in,
    rxdfeutovrden_in,
    rxdfevphold_in,
    rxdfevpovrden_in,
    rxdfevsen_in,
    rxdfexyden_in,
    rxdlybypass_in,
    rxdlyen_in,
    rxdlyovrden_in,
    rxdlysreset_in,
    rxelecidlemode_in,
    rxeqtraining_in,
    rxgearboxslip_in,
    rxlatclk_in,
    rxlpmen_in,
    rxlpmgchold_in,
    rxlpmgcovrden_in,
    rxlpmhfhold_in,
    rxlpmhfovrden_in,
    rxlpmlfhold_in,
    rxlpmlfklovrden_in,
    rxlpmoshold_in,
    rxlpmosovrden_in,
    rxmcommaalignen_in,
    rxmonitorsel_in,
    rxoobreset_in,
    rxoscalreset_in,
    rxoshold_in,
    rxosintcfg_in,
    rxosinten_in,
    rxosinthold_in,
    rxosintovrden_in,
    rxosintstrobe_in,
    rxosinttestovrden_in,
    rxosovrden_in,
    rxoutclksel_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxphalign_in,
    rxphalignen_in,
    rxphdlypd_in,
    rxphdlyreset_in,
    rxphovrden_in,
    rxpllclksel_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxprogdivreset_in,
    rxqpien_in,
    rxrate_in,
    rxratemode_in,
    rxslide_in,
    rxslipoutclk_in,
    rxslippma_in,
    rxsyncallin_in,
    rxsyncin_in,
    rxsyncmode_in,
    rxsysclksel_in,
    rxtermination_in,
    rxuserrdy_in,
    rxusrclk_in,
    rxusrclk2_in,
    sigvalidclk_in,
    tstin_in,
    tx8b10bbypass_in,
    tx8b10ben_in,
    txbufdiffctrl_in,
    txcominit_in,
    txcomsas_in,
    txcomwake_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdata_in,
    txdataextendrsvd_in,
    txdccforcestart_in,
    txdccreset_in,
    txdeemph_in,
    txdetectrx_in,
    txdiffctrl_in,
    txdiffpd_in,
    txdlybypass_in,
    txdlyen_in,
    txdlyhold_in,
    txdlyovrden_in,
    txdlysreset_in,
    txdlyupdown_in,
    txelecidle_in,
    txelforcestart_in,
    txheader_in,
    txinhibit_in,
    txlatclk_in,
    txlfpstreset_in,
    txlfpsu2lpexit_in,
    txlfpsu3wake_in,
    txmaincursor_in,
    txmargin_in,
    txmuxdcdexhold_in,
    txmuxdcdorwren_in,
    txoneszeros_in,
    txoutclksel_in,
    txpcsreset_in,
    txpd_in,
    txpdelecidlemode_in,
    txphalign_in,
    txphalignen_in,
    txphdlypd_in,
    txphdlyreset_in,
    txphdlytstclk_in,
    txphinit_in,
    txphovrden_in,
    txpippmen_in,
    txpippmovrden_in,
    txpippmpd_in,
    txpippmsel_in,
    txpippmstepsize_in,
    txpisopd_in,
    txpllclksel_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txpostcursorinv_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txprecursorinv_in,
    txprogdivreset_in,
    txqpibiasen_in,
    txqpistrongpdown_in,
    txqpiweakpup_in,
    txrate_in,
    txratemode_in,
    txsequence_in,
    txswing_in,
    txsyncallin_in,
    txsyncin_in,
    txsyncmode_in,
    txsysclksel_in,
    txuserrdy_in,
    txusrclk_in,
    txusrclk2_in,
    bufgtce_out,
    bufgtcemask_out,
    bufgtdiv_out,
    bufgtreset_out,
    bufgtrstmask_out,
    cpllfbclklost_out,
    cplllock_out,
    cpllrefclklost_out,
    dmonitorout_out,
    dmonitoroutclk_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    gtrefclkmonitor_out,
    gtytxn_out,
    gtytxp_out,
    pcierategen3_out,
    pcierateidle_out,
    pcierateqpllpd_out,
    pcierateqpllreset_out,
    pciesynctxsyncdone_out,
    pcieusergen3rdy_out,
    pcieuserphystatusrst_out,
    pcieuserratestart_out,
    pcsrsvdout_out,
    phystatus_out,
    pinrsrvdas_out,
    powerpresent_out,
    resetexception_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxcdrlock_out,
    rxcdrphdone_out,
    rxchanbondseq_out,
    rxchanisaligned_out,
    rxchanrealign_out,
    rxchbondo_out,
    rxckcaldone_out,
    rxclkcorcnt_out,
    rxcominitdet_out,
    rxcommadet_out,
    rxcomsasdet_out,
    rxcomwakedet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxdata_out,
    rxdataextendrsvd_out,
    rxdatavalid_out,
    rxdlysresetdone_out,
    rxelecidle_out,
    rxheader_out,
    rxheadervalid_out,
    rxlfpstresetdet_out,
    rxlfpsu2lpexitdet_out,
    rxlfpsu3wakedet_out,
    rxmonitorout_out,
    rxosintdone_out,
    rxosintstarted_out,
    rxosintstrobedone_out,
    rxosintstrobestarted_out,
    rxoutclk_out,
    rxoutclkfabric_out,
    rxoutclkpcs_out,
    rxphaligndone_out,
    rxphalignerr_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxprbslocked_out,
    rxprgdivresetdone_out,
    rxqpisenn_out,
    rxqpisenp_out,
    rxratedone_out,
    rxrecclkout_out,
    rxresetdone_out,
    rxsliderdy_out,
    rxslipdone_out,
    rxslipoutclkrdy_out,
    rxslippmardy_out,
    rxstartofseq_out,
    rxstatus_out,
    rxsyncdone_out,
    rxsyncout_out,
    rxvalid_out,
    txbufstatus_out,
    txcomfinish_out,
    txdccdone_out,
    txdlysresetdone_out,
    txoutclk_out,
    txoutclkfabric_out,
    txoutclkpcs_out,
    txphaligndone_out,
    txphinitdone_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txqpisenn_out,
    txqpisenp_out,
    txratedone_out,
    txresetdone_out,
    txsyncdone_out,
    txsyncout_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  output [0:0]gtwiz_userclk_tx_srcclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk2_out;
  output [0:0]gtwiz_userclk_tx_active_out;
  input [0:0]gtwiz_userclk_rx_reset_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  output [0:0]gtwiz_userclk_rx_srcclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk2_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input [0:0]gtwiz_reset_tx_done_in;
  input [0:0]gtwiz_reset_rx_done_in;
  input [0:0]gtwiz_reset_qpll0lock_in;
  input [0:0]gtwiz_reset_qpll1lock_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output [0:0]gtwiz_reset_qpll0reset_out;
  output [0:0]gtwiz_reset_qpll1reset_out;
  input [17:0]gtwiz_gthe3_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe3_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe3_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gthe4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe4_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gtye4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gtye4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gtye4_cpll_cal_bufg_ce_in;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [0:0]bgbypassb_in;
  input [0:0]bgmonitorenb_in;
  input [0:0]bgpdb_in;
  input [4:0]bgrcalovrd_in;
  input [0:0]bgrcalovrdenb_in;
  input [8:0]drpaddr_common_in;
  input [0:0]drpclk_common_in;
  input [15:0]drpdi_common_in;
  input [0:0]drpen_common_in;
  input [0:0]drpwe_common_in;
  input [0:0]gtgrefclk0_in;
  input [0:0]gtgrefclk1_in;
  input [0:0]gtnorthrefclk00_in;
  input [0:0]gtnorthrefclk01_in;
  input [0:0]gtnorthrefclk10_in;
  input [0:0]gtnorthrefclk11_in;
  input [0:0]gtrefclk00_in;
  input [0:0]gtrefclk01_in;
  input [0:0]gtrefclk10_in;
  input [0:0]gtrefclk11_in;
  input [0:0]gtsouthrefclk00_in;
  input [0:0]gtsouthrefclk01_in;
  input [0:0]gtsouthrefclk10_in;
  input [0:0]gtsouthrefclk11_in;
  input [0:0]pcierateqpll0_in;
  input [0:0]pcierateqpll1_in;
  input [7:0]pmarsvd0_in;
  input [7:0]pmarsvd1_in;
  input [0:0]qpll0clkrsvd0_in;
  input [0:0]qpll0clkrsvd1_in;
  input [0:0]qpll0fbdiv_in;
  input [0:0]qpll0lockdetclk_in;
  input [0:0]qpll0locken_in;
  input [0:0]qpll0pd_in;
  input [2:0]qpll0refclksel_in;
  input [0:0]qpll0reset_in;
  input [0:0]qpll1clkrsvd0_in;
  input [0:0]qpll1clkrsvd1_in;
  input [0:0]qpll1fbdiv_in;
  input [0:0]qpll1lockdetclk_in;
  input [0:0]qpll1locken_in;
  input [0:0]qpll1pd_in;
  input [2:0]qpll1refclksel_in;
  input [0:0]qpll1reset_in;
  input [7:0]qpllrsvd1_in;
  input [4:0]qpllrsvd2_in;
  input [4:0]qpllrsvd3_in;
  input [7:0]qpllrsvd4_in;
  input [0:0]rcalenb_in;
  input [0:0]sdm0data_in;
  input [0:0]sdm0reset_in;
  input [0:0]sdm0toggle_in;
  input [0:0]sdm0width_in;
  input [0:0]sdm1data_in;
  input [0:0]sdm1reset_in;
  input [0:0]sdm1toggle_in;
  input [0:0]sdm1width_in;
  input [0:0]tcongpi_in;
  input [0:0]tconpowerup_in;
  input [0:0]tconreset_in;
  input [0:0]tconrsvdin1_in;
  input [0:0]ubcfgstreamen_in;
  input [0:0]ubdo_in;
  input [0:0]ubdrdy_in;
  input [0:0]ubenable_in;
  input [0:0]ubgpi_in;
  input [0:0]ubintr_in;
  input [0:0]ubiolmbrst_in;
  input [0:0]ubmbrst_in;
  input [0:0]ubmdmcapture_in;
  input [0:0]ubmdmdbgrst_in;
  input [0:0]ubmdmdbgupdate_in;
  input [0:0]ubmdmregen_in;
  input [0:0]ubmdmshift_in;
  input [0:0]ubmdmsysrst_in;
  input [0:0]ubmdmtck_in;
  input [0:0]ubmdmtdi_in;
  output [15:0]drpdo_common_out;
  output [0:0]drprdy_common_out;
  output [7:0]pmarsvdout0_out;
  output [7:0]pmarsvdout1_out;
  output [0:0]qpll0fbclklost_out;
  output [0:0]qpll0lock_out;
  output [0:0]qpll0outclk_out;
  output [0:0]qpll0outrefclk_out;
  output [0:0]qpll0refclklost_out;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1lock_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  output [7:0]qplldmonitor0_out;
  output [7:0]qplldmonitor1_out;
  output [0:0]refclkoutmonitor0_out;
  output [0:0]refclkoutmonitor1_out;
  output [1:0]rxrecclk0_sel_out;
  output [1:0]rxrecclk1_sel_out;
  output [0:0]rxrecclk0sel_out;
  output [0:0]rxrecclk1sel_out;
  output [0:0]sdm0finalout_out;
  output [0:0]sdm0testdata_out;
  output [0:0]sdm1finalout_out;
  output [0:0]sdm1testdata_out;
  output [0:0]tcongpo_out;
  output [0:0]tconrsvdout0_out;
  output [0:0]ubdaddr_out;
  output [0:0]ubden_out;
  output [0:0]ubdi_out;
  output [0:0]ubdwe_out;
  output [0:0]ubmdmtdo_out;
  output [0:0]ubrsvdout_out;
  output [0:0]ubtxuart_out;
  input [0:0]cdrstepdir_in;
  input [0:0]cdrstepsq_in;
  input [0:0]cdrstepsx_in;
  input [0:0]cfgreset_in;
  input [0:0]clkrsvd0_in;
  input [0:0]clkrsvd1_in;
  input [0:0]cpllfreqlock_in;
  input [0:0]cplllockdetclk_in;
  input [0:0]cplllocken_in;
  input [0:0]cpllpd_in;
  input [2:0]cpllrefclksel_in;
  input [0:0]cpllreset_in;
  input [0:0]dmonfiforeset_in;
  input [0:0]dmonitorclk_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drprst_in;
  input [0:0]drpwe_in;
  input [0:0]elpcaldvorwren_in;
  input [0:0]elpcalpaorwren_in;
  input [0:0]evoddphicaldone_in;
  input [0:0]evoddphicalstart_in;
  input [0:0]evoddphidrden_in;
  input [0:0]evoddphidwren_in;
  input [0:0]evoddphixrden_in;
  input [0:0]evoddphixwren_in;
  input [0:0]eyescanmode_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]freqos_in;
  input [0:0]gtgrefclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtnorthrefclk0_in;
  input [0:0]gtnorthrefclk1_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [0:0]gtresetsel_in;
  input [15:0]gtrsvd_in;
  input [0:0]gtrxreset_in;
  input [0:0]gtrxresetsel_in;
  input [0:0]gtsouthrefclk0_in;
  input [0:0]gtsouthrefclk1_in;
  input [0:0]gttxreset_in;
  input [0:0]gttxresetsel_in;
  input [0:0]incpctrl_in;
  input [0:0]gtyrxn_in;
  input [0:0]gtyrxp_in;
  input [2:0]loopback_in;
  input [0:0]looprsvd_in;
  input [0:0]lpbkrxtxseren_in;
  input [0:0]lpbktxrxseren_in;
  input [0:0]pcieeqrxeqadaptdone_in;
  input [0:0]pcierstidle_in;
  input [0:0]pciersttxsyncstart_in;
  input [0:0]pcieuserratedone_in;
  input [15:0]pcsrsvdin_in;
  input [4:0]pcsrsvdin2_in;
  input [4:0]pmarsvdin_in;
  input [0:0]qpll0clk_in;
  input [0:0]qpll0freqlock_in;
  input [0:0]qpll0refclk_in;
  input [0:0]qpll1clk_in;
  input [0:0]qpll1freqlock_in;
  input [0:0]qpll1refclk_in;
  input [0:0]resetovrd_in;
  input [0:0]rstclkentx_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxafecfoken_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrfreqreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcdrovrden_in;
  input [0:0]rxcdrreset_in;
  input [0:0]rxcdrresetrsv_in;
  input [0:0]rxchbonden_in;
  input [4:0]rxchbondi_in;
  input [2:0]rxchbondlevel_in;
  input [0:0]rxchbondmaster_in;
  input [0:0]rxchbondslave_in;
  input [0:0]rxckcalreset_in;
  input [0:0]rxckcalstart_in;
  input [0:0]rxcommadeten_in;
  input [1:0]rxdfeagcctrl_in;
  input [0:0]rxdccforcestart_in;
  input [0:0]rxdfeagchold_in;
  input [0:0]rxdfeagcovrden_in;
  input [0:0]rxdfecfokfcnum_in;
  input [0:0]rxdfecfokfen_in;
  input [0:0]rxdfecfokfpulse_in;
  input [0:0]rxdfecfokhold_in;
  input [0:0]rxdfecfokovren_in;
  input [0:0]rxdfekhhold_in;
  input [0:0]rxdfekhovrden_in;
  input [0:0]rxdfelfhold_in;
  input [0:0]rxdfelfovrden_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxdfetap10hold_in;
  input [0:0]rxdfetap10ovrden_in;
  input [0:0]rxdfetap11hold_in;
  input [0:0]rxdfetap11ovrden_in;
  input [0:0]rxdfetap12hold_in;
  input [0:0]rxdfetap12ovrden_in;
  input [0:0]rxdfetap13hold_in;
  input [0:0]rxdfetap13ovrden_in;
  input [0:0]rxdfetap14hold_in;
  input [0:0]rxdfetap14ovrden_in;
  input [0:0]rxdfetap15hold_in;
  input [0:0]rxdfetap15ovrden_in;
  input [0:0]rxdfetap2hold_in;
  input [0:0]rxdfetap2ovrden_in;
  input [0:0]rxdfetap3hold_in;
  input [0:0]rxdfetap3ovrden_in;
  input [0:0]rxdfetap4hold_in;
  input [0:0]rxdfetap4ovrden_in;
  input [0:0]rxdfetap5hold_in;
  input [0:0]rxdfetap5ovrden_in;
  input [0:0]rxdfetap6hold_in;
  input [0:0]rxdfetap6ovrden_in;
  input [0:0]rxdfetap7hold_in;
  input [0:0]rxdfetap7ovrden_in;
  input [0:0]rxdfetap8hold_in;
  input [0:0]rxdfetap8ovrden_in;
  input [0:0]rxdfetap9hold_in;
  input [0:0]rxdfetap9ovrden_in;
  input [0:0]rxdfeuthold_in;
  input [0:0]rxdfeutovrden_in;
  input [0:0]rxdfevphold_in;
  input [0:0]rxdfevpovrden_in;
  input [0:0]rxdfevsen_in;
  input [0:0]rxdfexyden_in;
  input [0:0]rxdlybypass_in;
  input [0:0]rxdlyen_in;
  input [0:0]rxdlyovrden_in;
  input [0:0]rxdlysreset_in;
  input [1:0]rxelecidlemode_in;
  input [0:0]rxeqtraining_in;
  input [0:0]rxgearboxslip_in;
  input [0:0]rxlatclk_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxlpmgchold_in;
  input [0:0]rxlpmgcovrden_in;
  input [0:0]rxlpmhfhold_in;
  input [0:0]rxlpmhfovrden_in;
  input [0:0]rxlpmlfhold_in;
  input [0:0]rxlpmlfklovrden_in;
  input [0:0]rxlpmoshold_in;
  input [0:0]rxlpmosovrden_in;
  input [0:0]rxmcommaalignen_in;
  input [1:0]rxmonitorsel_in;
  input [0:0]rxoobreset_in;
  input [0:0]rxoscalreset_in;
  input [0:0]rxoshold_in;
  input [3:0]rxosintcfg_in;
  input [0:0]rxosinten_in;
  input [0:0]rxosinthold_in;
  input [0:0]rxosintovrden_in;
  input [0:0]rxosintstrobe_in;
  input [0:0]rxosinttestovrden_in;
  input [0:0]rxosovrden_in;
  input [2:0]rxoutclksel_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxphalign_in;
  input [0:0]rxphalignen_in;
  input [0:0]rxphdlypd_in;
  input [0:0]rxphdlyreset_in;
  input [0:0]rxphovrden_in;
  input [1:0]rxpllclksel_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [0:0]rxprogdivreset_in;
  input [0:0]rxqpien_in;
  input [2:0]rxrate_in;
  input [0:0]rxratemode_in;
  input [0:0]rxslide_in;
  input [0:0]rxslipoutclk_in;
  input [0:0]rxslippma_in;
  input [0:0]rxsyncallin_in;
  input [0:0]rxsyncin_in;
  input [0:0]rxsyncmode_in;
  input [1:0]rxsysclksel_in;
  input [0:0]rxtermination_in;
  input [0:0]rxuserrdy_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]sigvalidclk_in;
  input [19:0]tstin_in;
  input [7:0]tx8b10bbypass_in;
  input [0:0]tx8b10ben_in;
  input [2:0]txbufdiffctrl_in;
  input [0:0]txcominit_in;
  input [0:0]txcomsas_in;
  input [0:0]txcomwake_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [127:0]txdata_in;
  input [7:0]txdataextendrsvd_in;
  input [0:0]txdccforcestart_in;
  input [0:0]txdccreset_in;
  input [0:0]txdeemph_in;
  input [0:0]txdetectrx_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txdiffpd_in;
  input [0:0]txdlybypass_in;
  input [0:0]txdlyen_in;
  input [0:0]txdlyhold_in;
  input [0:0]txdlyovrden_in;
  input [0:0]txdlysreset_in;
  input [0:0]txdlyupdown_in;
  input [0:0]txelecidle_in;
  input [0:0]txelforcestart_in;
  input [5:0]txheader_in;
  input [0:0]txinhibit_in;
  input [0:0]txlatclk_in;
  input [0:0]txlfpstreset_in;
  input [0:0]txlfpsu2lpexit_in;
  input [0:0]txlfpsu3wake_in;
  input [6:0]txmaincursor_in;
  input [2:0]txmargin_in;
  input [0:0]txmuxdcdexhold_in;
  input [0:0]txmuxdcdorwren_in;
  input [0:0]txoneszeros_in;
  input [2:0]txoutclksel_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpdelecidlemode_in;
  input [0:0]txphalign_in;
  input [0:0]txphalignen_in;
  input [0:0]txphdlypd_in;
  input [0:0]txphdlyreset_in;
  input [0:0]txphdlytstclk_in;
  input [0:0]txphinit_in;
  input [0:0]txphovrden_in;
  input [0:0]txpippmen_in;
  input [0:0]txpippmovrden_in;
  input [0:0]txpippmpd_in;
  input [0:0]txpippmsel_in;
  input [4:0]txpippmstepsize_in;
  input [0:0]txpisopd_in;
  input [1:0]txpllclksel_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txpostcursorinv_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txprecursorinv_in;
  input [0:0]txprogdivreset_in;
  input [0:0]txqpibiasen_in;
  input [0:0]txqpistrongpdown_in;
  input [0:0]txqpiweakpup_in;
  input [2:0]txrate_in;
  input [0:0]txratemode_in;
  input [6:0]txsequence_in;
  input [0:0]txswing_in;
  input [0:0]txsyncallin_in;
  input [0:0]txsyncin_in;
  input [0:0]txsyncmode_in;
  input [1:0]txsysclksel_in;
  input [0:0]txuserrdy_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [2:0]bufgtce_out;
  output [2:0]bufgtcemask_out;
  output [8:0]bufgtdiv_out;
  output [2:0]bufgtreset_out;
  output [2:0]bufgtrstmask_out;
  output [0:0]cpllfbclklost_out;
  output [0:0]cplllock_out;
  output [0:0]cpllrefclklost_out;
  output [16:0]dmonitorout_out;
  output [0:0]dmonitoroutclk_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]gtrefclkmonitor_out;
  output [0:0]gtytxn_out;
  output [0:0]gtytxp_out;
  output [0:0]pcierategen3_out;
  output [0:0]pcierateidle_out;
  output [1:0]pcierateqpllpd_out;
  output [1:0]pcierateqpllreset_out;
  output [0:0]pciesynctxsyncdone_out;
  output [0:0]pcieusergen3rdy_out;
  output [0:0]pcieuserphystatusrst_out;
  output [0:0]pcieuserratestart_out;
  output [11:0]pcsrsvdout_out;
  output [0:0]phystatus_out;
  output [7:0]pinrsrvdas_out;
  output [0:0]powerpresent_out;
  output [0:0]resetexception_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxcdrphdone_out;
  output [0:0]rxchanbondseq_out;
  output [0:0]rxchanisaligned_out;
  output [0:0]rxchanrealign_out;
  output [4:0]rxchbondo_out;
  output [0:0]rxckcaldone_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcominitdet_out;
  output [0:0]rxcommadet_out;
  output [0:0]rxcomsasdet_out;
  output [0:0]rxcomwakedet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [127:0]rxdata_out;
  output [7:0]rxdataextendrsvd_out;
  output [1:0]rxdatavalid_out;
  output [0:0]rxdlysresetdone_out;
  output [0:0]rxelecidle_out;
  output [5:0]rxheader_out;
  output [1:0]rxheadervalid_out;
  output [0:0]rxlfpstresetdet_out;
  output [0:0]rxlfpsu2lpexitdet_out;
  output [0:0]rxlfpsu3wakedet_out;
  output [6:0]rxmonitorout_out;
  output [0:0]rxosintdone_out;
  output [0:0]rxosintstarted_out;
  output [0:0]rxosintstrobedone_out;
  output [0:0]rxosintstrobestarted_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkfabric_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]rxphaligndone_out;
  output [0:0]rxphalignerr_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxprbslocked_out;
  output [0:0]rxprgdivresetdone_out;
  output [0:0]rxqpisenn_out;
  output [0:0]rxqpisenp_out;
  output [0:0]rxratedone_out;
  output [0:0]rxrecclkout_out;
  output [0:0]rxresetdone_out;
  output [0:0]rxsliderdy_out;
  output [0:0]rxslipdone_out;
  output [0:0]rxslipoutclkrdy_out;
  output [0:0]rxslippmardy_out;
  output [1:0]rxstartofseq_out;
  output [2:0]rxstatus_out;
  output [0:0]rxsyncdone_out;
  output [0:0]rxsyncout_out;
  output [0:0]rxvalid_out;
  output [1:0]txbufstatus_out;
  output [0:0]txcomfinish_out;
  output [0:0]txdccdone_out;
  output [0:0]txdlysresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txoutclkfabric_out;
  output [0:0]txoutclkpcs_out;
  output [0:0]txphaligndone_out;
  output [0:0]txphinitdone_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txqpisenn_out;
  output [0:0]txqpisenp_out;
  output [0:0]txratedone_out;
  output [0:0]txresetdone_out;
  output [0:0]txsyncdone_out;
  output [0:0]txsyncout_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  assign bufgtce_out[2] = \<const0> ;
  assign bufgtce_out[1] = \<const0> ;
  assign bufgtce_out[0] = \<const0> ;
  assign bufgtcemask_out[2] = \<const0> ;
  assign bufgtcemask_out[1] = \<const0> ;
  assign bufgtcemask_out[0] = \<const0> ;
  assign bufgtdiv_out[8] = \<const0> ;
  assign bufgtdiv_out[7] = \<const0> ;
  assign bufgtdiv_out[6] = \<const0> ;
  assign bufgtdiv_out[5] = \<const0> ;
  assign bufgtdiv_out[4] = \<const0> ;
  assign bufgtdiv_out[3] = \<const0> ;
  assign bufgtdiv_out[2] = \<const0> ;
  assign bufgtdiv_out[1] = \<const0> ;
  assign bufgtdiv_out[0] = \<const0> ;
  assign bufgtreset_out[2] = \<const0> ;
  assign bufgtreset_out[1] = \<const0> ;
  assign bufgtreset_out[0] = \<const0> ;
  assign bufgtrstmask_out[2] = \<const0> ;
  assign bufgtrstmask_out[1] = \<const0> ;
  assign bufgtrstmask_out[0] = \<const0> ;
  assign cpllfbclklost_out[0] = \<const0> ;
  assign cplllock_out[0] = \<const0> ;
  assign cpllrefclklost_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign dmonitoroutclk_out[0] = \<const0> ;
  assign drpdo_common_out[15] = \<const0> ;
  assign drpdo_common_out[14] = \<const0> ;
  assign drpdo_common_out[13] = \<const0> ;
  assign drpdo_common_out[12] = \<const0> ;
  assign drpdo_common_out[11] = \<const0> ;
  assign drpdo_common_out[10] = \<const0> ;
  assign drpdo_common_out[9] = \<const0> ;
  assign drpdo_common_out[8] = \<const0> ;
  assign drpdo_common_out[7] = \<const0> ;
  assign drpdo_common_out[6] = \<const0> ;
  assign drpdo_common_out[5] = \<const0> ;
  assign drpdo_common_out[4] = \<const0> ;
  assign drpdo_common_out[3] = \<const0> ;
  assign drpdo_common_out[2] = \<const0> ;
  assign drpdo_common_out[1] = \<const0> ;
  assign drpdo_common_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_common_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtrefclkmonitor_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_error_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_error_out[0] = \<const0> ;
  assign gtwiz_reset_qpll0reset_out[0] = \<const0> ;
  assign gtwiz_reset_qpll1reset_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk_out[0] = \<const0> ;
  assign gtytxn_out[0] = \<const0> ;
  assign gtytxp_out[0] = \<const0> ;
  assign pcierategen3_out[0] = \<const0> ;
  assign pcierateidle_out[0] = \<const0> ;
  assign pcierateqpllpd_out[1] = \<const0> ;
  assign pcierateqpllpd_out[0] = \<const0> ;
  assign pcierateqpllreset_out[1] = \<const0> ;
  assign pcierateqpllreset_out[0] = \<const0> ;
  assign pciesynctxsyncdone_out[0] = \<const0> ;
  assign pcieusergen3rdy_out[0] = \<const0> ;
  assign pcieuserphystatusrst_out[0] = \<const0> ;
  assign pcieuserratestart_out[0] = \<const0> ;
  assign pcsrsvdout_out[11] = \<const0> ;
  assign pcsrsvdout_out[10] = \<const0> ;
  assign pcsrsvdout_out[9] = \<const0> ;
  assign pcsrsvdout_out[8] = \<const0> ;
  assign pcsrsvdout_out[7] = \<const0> ;
  assign pcsrsvdout_out[6] = \<const0> ;
  assign pcsrsvdout_out[5] = \<const0> ;
  assign pcsrsvdout_out[4] = \<const0> ;
  assign pcsrsvdout_out[3] = \<const0> ;
  assign pcsrsvdout_out[2] = \<const0> ;
  assign pcsrsvdout_out[1] = \<const0> ;
  assign pcsrsvdout_out[0] = \<const0> ;
  assign phystatus_out[0] = \<const0> ;
  assign pinrsrvdas_out[7] = \<const0> ;
  assign pinrsrvdas_out[6] = \<const0> ;
  assign pinrsrvdas_out[5] = \<const0> ;
  assign pinrsrvdas_out[4] = \<const0> ;
  assign pinrsrvdas_out[3] = \<const0> ;
  assign pinrsrvdas_out[2] = \<const0> ;
  assign pinrsrvdas_out[1] = \<const0> ;
  assign pinrsrvdas_out[0] = \<const0> ;
  assign pmarsvdout0_out[7] = \<const0> ;
  assign pmarsvdout0_out[6] = \<const0> ;
  assign pmarsvdout0_out[5] = \<const0> ;
  assign pmarsvdout0_out[4] = \<const0> ;
  assign pmarsvdout0_out[3] = \<const0> ;
  assign pmarsvdout0_out[2] = \<const0> ;
  assign pmarsvdout0_out[1] = \<const0> ;
  assign pmarsvdout0_out[0] = \<const0> ;
  assign pmarsvdout1_out[7] = \<const0> ;
  assign pmarsvdout1_out[6] = \<const0> ;
  assign pmarsvdout1_out[5] = \<const0> ;
  assign pmarsvdout1_out[4] = \<const0> ;
  assign pmarsvdout1_out[3] = \<const0> ;
  assign pmarsvdout1_out[2] = \<const0> ;
  assign pmarsvdout1_out[1] = \<const0> ;
  assign pmarsvdout1_out[0] = \<const0> ;
  assign powerpresent_out[0] = \<const0> ;
  assign qpll0fbclklost_out[0] = \<const0> ;
  assign qpll0lock_out[0] = \<const0> ;
  assign qpll0outclk_out[0] = \<const0> ;
  assign qpll0outrefclk_out[0] = \<const0> ;
  assign qpll0refclklost_out[0] = \<const0> ;
  assign qpll1fbclklost_out[0] = \<const0> ;
  assign qpll1lock_out[0] = \<const0> ;
  assign qpll1outclk_out[0] = \<const0> ;
  assign qpll1outrefclk_out[0] = \<const0> ;
  assign qpll1refclklost_out[0] = \<const0> ;
  assign qplldmonitor0_out[7] = \<const0> ;
  assign qplldmonitor0_out[6] = \<const0> ;
  assign qplldmonitor0_out[5] = \<const0> ;
  assign qplldmonitor0_out[4] = \<const0> ;
  assign qplldmonitor0_out[3] = \<const0> ;
  assign qplldmonitor0_out[2] = \<const0> ;
  assign qplldmonitor0_out[1] = \<const0> ;
  assign qplldmonitor0_out[0] = \<const0> ;
  assign qplldmonitor1_out[7] = \<const0> ;
  assign qplldmonitor1_out[6] = \<const0> ;
  assign qplldmonitor1_out[5] = \<const0> ;
  assign qplldmonitor1_out[4] = \<const0> ;
  assign qplldmonitor1_out[3] = \<const0> ;
  assign qplldmonitor1_out[2] = \<const0> ;
  assign qplldmonitor1_out[1] = \<const0> ;
  assign qplldmonitor1_out[0] = \<const0> ;
  assign refclkoutmonitor0_out[0] = \<const0> ;
  assign refclkoutmonitor1_out[0] = \<const0> ;
  assign resetexception_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcdrlock_out[0] = \<const0> ;
  assign rxcdrphdone_out[0] = \<const0> ;
  assign rxchanbondseq_out[0] = \<const0> ;
  assign rxchanisaligned_out[0] = \<const0> ;
  assign rxchanrealign_out[0] = \<const0> ;
  assign rxchbondo_out[4] = \<const0> ;
  assign rxchbondo_out[3] = \<const0> ;
  assign rxchbondo_out[2] = \<const0> ;
  assign rxchbondo_out[1] = \<const0> ;
  assign rxchbondo_out[0] = \<const0> ;
  assign rxckcaldone_out[0] = \<const0> ;
  assign rxcominitdet_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxcomsasdet_out[0] = \<const0> ;
  assign rxcomwakedet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxdata_out[127] = \<const0> ;
  assign rxdata_out[126] = \<const0> ;
  assign rxdata_out[125] = \<const0> ;
  assign rxdata_out[124] = \<const0> ;
  assign rxdata_out[123] = \<const0> ;
  assign rxdata_out[122] = \<const0> ;
  assign rxdata_out[121] = \<const0> ;
  assign rxdata_out[120] = \<const0> ;
  assign rxdata_out[119] = \<const0> ;
  assign rxdata_out[118] = \<const0> ;
  assign rxdata_out[117] = \<const0> ;
  assign rxdata_out[116] = \<const0> ;
  assign rxdata_out[115] = \<const0> ;
  assign rxdata_out[114] = \<const0> ;
  assign rxdata_out[113] = \<const0> ;
  assign rxdata_out[112] = \<const0> ;
  assign rxdata_out[111] = \<const0> ;
  assign rxdata_out[110] = \<const0> ;
  assign rxdata_out[109] = \<const0> ;
  assign rxdata_out[108] = \<const0> ;
  assign rxdata_out[107] = \<const0> ;
  assign rxdata_out[106] = \<const0> ;
  assign rxdata_out[105] = \<const0> ;
  assign rxdata_out[104] = \<const0> ;
  assign rxdata_out[103] = \<const0> ;
  assign rxdata_out[102] = \<const0> ;
  assign rxdata_out[101] = \<const0> ;
  assign rxdata_out[100] = \<const0> ;
  assign rxdata_out[99] = \<const0> ;
  assign rxdata_out[98] = \<const0> ;
  assign rxdata_out[97] = \<const0> ;
  assign rxdata_out[96] = \<const0> ;
  assign rxdata_out[95] = \<const0> ;
  assign rxdata_out[94] = \<const0> ;
  assign rxdata_out[93] = \<const0> ;
  assign rxdata_out[92] = \<const0> ;
  assign rxdata_out[91] = \<const0> ;
  assign rxdata_out[90] = \<const0> ;
  assign rxdata_out[89] = \<const0> ;
  assign rxdata_out[88] = \<const0> ;
  assign rxdata_out[87] = \<const0> ;
  assign rxdata_out[86] = \<const0> ;
  assign rxdata_out[85] = \<const0> ;
  assign rxdata_out[84] = \<const0> ;
  assign rxdata_out[83] = \<const0> ;
  assign rxdata_out[82] = \<const0> ;
  assign rxdata_out[81] = \<const0> ;
  assign rxdata_out[80] = \<const0> ;
  assign rxdata_out[79] = \<const0> ;
  assign rxdata_out[78] = \<const0> ;
  assign rxdata_out[77] = \<const0> ;
  assign rxdata_out[76] = \<const0> ;
  assign rxdata_out[75] = \<const0> ;
  assign rxdata_out[74] = \<const0> ;
  assign rxdata_out[73] = \<const0> ;
  assign rxdata_out[72] = \<const0> ;
  assign rxdata_out[71] = \<const0> ;
  assign rxdata_out[70] = \<const0> ;
  assign rxdata_out[69] = \<const0> ;
  assign rxdata_out[68] = \<const0> ;
  assign rxdata_out[67] = \<const0> ;
  assign rxdata_out[66] = \<const0> ;
  assign rxdata_out[65] = \<const0> ;
  assign rxdata_out[64] = \<const0> ;
  assign rxdata_out[63] = \<const0> ;
  assign rxdata_out[62] = \<const0> ;
  assign rxdata_out[61] = \<const0> ;
  assign rxdata_out[60] = \<const0> ;
  assign rxdata_out[59] = \<const0> ;
  assign rxdata_out[58] = \<const0> ;
  assign rxdata_out[57] = \<const0> ;
  assign rxdata_out[56] = \<const0> ;
  assign rxdata_out[55] = \<const0> ;
  assign rxdata_out[54] = \<const0> ;
  assign rxdata_out[53] = \<const0> ;
  assign rxdata_out[52] = \<const0> ;
  assign rxdata_out[51] = \<const0> ;
  assign rxdata_out[50] = \<const0> ;
  assign rxdata_out[49] = \<const0> ;
  assign rxdata_out[48] = \<const0> ;
  assign rxdata_out[47] = \<const0> ;
  assign rxdata_out[46] = \<const0> ;
  assign rxdata_out[45] = \<const0> ;
  assign rxdata_out[44] = \<const0> ;
  assign rxdata_out[43] = \<const0> ;
  assign rxdata_out[42] = \<const0> ;
  assign rxdata_out[41] = \<const0> ;
  assign rxdata_out[40] = \<const0> ;
  assign rxdata_out[39] = \<const0> ;
  assign rxdata_out[38] = \<const0> ;
  assign rxdata_out[37] = \<const0> ;
  assign rxdata_out[36] = \<const0> ;
  assign rxdata_out[35] = \<const0> ;
  assign rxdata_out[34] = \<const0> ;
  assign rxdata_out[33] = \<const0> ;
  assign rxdata_out[32] = \<const0> ;
  assign rxdata_out[31] = \<const0> ;
  assign rxdata_out[30] = \<const0> ;
  assign rxdata_out[29] = \<const0> ;
  assign rxdata_out[28] = \<const0> ;
  assign rxdata_out[27] = \<const0> ;
  assign rxdata_out[26] = \<const0> ;
  assign rxdata_out[25] = \<const0> ;
  assign rxdata_out[24] = \<const0> ;
  assign rxdata_out[23] = \<const0> ;
  assign rxdata_out[22] = \<const0> ;
  assign rxdata_out[21] = \<const0> ;
  assign rxdata_out[20] = \<const0> ;
  assign rxdata_out[19] = \<const0> ;
  assign rxdata_out[18] = \<const0> ;
  assign rxdata_out[17] = \<const0> ;
  assign rxdata_out[16] = \<const0> ;
  assign rxdata_out[15] = \<const0> ;
  assign rxdata_out[14] = \<const0> ;
  assign rxdata_out[13] = \<const0> ;
  assign rxdata_out[12] = \<const0> ;
  assign rxdata_out[11] = \<const0> ;
  assign rxdata_out[10] = \<const0> ;
  assign rxdata_out[9] = \<const0> ;
  assign rxdata_out[8] = \<const0> ;
  assign rxdata_out[7] = \<const0> ;
  assign rxdata_out[6] = \<const0> ;
  assign rxdata_out[5] = \<const0> ;
  assign rxdata_out[4] = \<const0> ;
  assign rxdata_out[3] = \<const0> ;
  assign rxdata_out[2] = \<const0> ;
  assign rxdata_out[1] = \<const0> ;
  assign rxdata_out[0] = \<const0> ;
  assign rxdataextendrsvd_out[7] = \<const0> ;
  assign rxdataextendrsvd_out[6] = \<const0> ;
  assign rxdataextendrsvd_out[5] = \<const0> ;
  assign rxdataextendrsvd_out[4] = \<const0> ;
  assign rxdataextendrsvd_out[3] = \<const0> ;
  assign rxdataextendrsvd_out[2] = \<const0> ;
  assign rxdataextendrsvd_out[1] = \<const0> ;
  assign rxdataextendrsvd_out[0] = \<const0> ;
  assign rxdatavalid_out[1] = \<const0> ;
  assign rxdatavalid_out[0] = \<const0> ;
  assign rxdlysresetdone_out[0] = \<const0> ;
  assign rxelecidle_out[0] = \<const0> ;
  assign rxheader_out[5] = \<const0> ;
  assign rxheader_out[4] = \<const0> ;
  assign rxheader_out[3] = \<const0> ;
  assign rxheader_out[2] = \<const0> ;
  assign rxheader_out[1] = \<const0> ;
  assign rxheader_out[0] = \<const0> ;
  assign rxheadervalid_out[1] = \<const0> ;
  assign rxheadervalid_out[0] = \<const0> ;
  assign rxlfpstresetdet_out[0] = \<const0> ;
  assign rxlfpsu2lpexitdet_out[0] = \<const0> ;
  assign rxlfpsu3wakedet_out[0] = \<const0> ;
  assign rxmonitorout_out[6] = \<const0> ;
  assign rxmonitorout_out[5] = \<const0> ;
  assign rxmonitorout_out[4] = \<const0> ;
  assign rxmonitorout_out[3] = \<const0> ;
  assign rxmonitorout_out[2] = \<const0> ;
  assign rxmonitorout_out[1] = \<const0> ;
  assign rxmonitorout_out[0] = \<const0> ;
  assign rxosintdone_out[0] = \<const0> ;
  assign rxosintstarted_out[0] = \<const0> ;
  assign rxosintstrobedone_out[0] = \<const0> ;
  assign rxosintstrobestarted_out[0] = \<const0> ;
  assign rxoutclkfabric_out[0] = \<const0> ;
  assign rxoutclkpcs_out[0] = \<const0> ;
  assign rxphaligndone_out[0] = \<const0> ;
  assign rxphalignerr_out[0] = \<const0> ;
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxprbslocked_out[0] = \<const0> ;
  assign rxprgdivresetdone_out[0] = \<const0> ;
  assign rxqpisenn_out[0] = \<const0> ;
  assign rxqpisenp_out[0] = \<const0> ;
  assign rxratedone_out[0] = \<const0> ;
  assign rxrecclk0_sel_out[1] = \<const0> ;
  assign rxrecclk0_sel_out[0] = \<const0> ;
  assign rxrecclk0sel_out[0] = \<const0> ;
  assign rxrecclk1_sel_out[1] = \<const0> ;
  assign rxrecclk1_sel_out[0] = \<const0> ;
  assign rxrecclk1sel_out[0] = \<const0> ;
  assign rxrecclkout_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign rxsliderdy_out[0] = \<const0> ;
  assign rxslipdone_out[0] = \<const0> ;
  assign rxslipoutclkrdy_out[0] = \<const0> ;
  assign rxslippmardy_out[0] = \<const0> ;
  assign rxstartofseq_out[1] = \<const0> ;
  assign rxstartofseq_out[0] = \<const0> ;
  assign rxstatus_out[2] = \<const0> ;
  assign rxstatus_out[1] = \<const0> ;
  assign rxstatus_out[0] = \<const0> ;
  assign rxsyncdone_out[0] = \<const0> ;
  assign rxsyncout_out[0] = \<const0> ;
  assign rxvalid_out[0] = \<const0> ;
  assign sdm0finalout_out[0] = \<const0> ;
  assign sdm0testdata_out[0] = \<const0> ;
  assign sdm1finalout_out[0] = \<const0> ;
  assign sdm1testdata_out[0] = \<const0> ;
  assign tcongpo_out[0] = \<const0> ;
  assign tconrsvdout0_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txcomfinish_out[0] = \<const0> ;
  assign txdccdone_out[0] = \<const0> ;
  assign txdlysresetdone_out[0] = \<const0> ;
  assign txoutclkfabric_out[0] = \<const0> ;
  assign txoutclkpcs_out[0] = \<const0> ;
  assign txphaligndone_out[0] = \<const0> ;
  assign txphinitdone_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txqpisenn_out[0] = \<const0> ;
  assign txqpisenp_out[0] = \<const0> ;
  assign txratedone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  assign txsyncdone_out[0] = \<const0> ;
  assign txsyncout_out[0] = \<const0> ;
  assign ubdaddr_out[0] = \<const0> ;
  assign ubden_out[0] = \<const0> ;
  assign ubdi_out[0] = \<const0> ;
  assign ubdwe_out[0] = \<const0> ;
  assign ubmdmtdo_out[0] = \<const0> ;
  assign ubrsvdout_out[0] = \<const0> ;
  assign ubtxuart_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_gthe3 \gen_gtwizard_gthe3_top.PCS_PMA_gt_gtwizard_gthe3_inst 
       (.drpclk_in(drpclk_in),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(\^rxbufstatus_out ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(\^rxctrl0_out ),
        .rxctrl1_out(\^rxctrl1_out ),
        .rxctrl2_out(\^rxctrl2_out ),
        .rxctrl3_out(\^rxctrl3_out ),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in[1]),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(\^txbufstatus_out ),
        .txctrl0_in(txctrl0_in[1:0]),
        .txctrl1_in(txctrl1_in[1:0]),
        .txctrl2_in(txctrl2_in[1:0]),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_reset_sync
   (reset_out,
    userclk2,
    enablealign);
  output reset_out;
  input userclk2;
  input enablealign;

  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(userclk2),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_resets
   (pma_reset_out,
    independent_clock_bufg,
    reset);
  output pma_reset_out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign pma_reset_out = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign mmcm_locked_out = \<const0> ;
  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_clocking core_clocking_i
       (.gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .pma_reset_out(pma_reset_out),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_block pcs_pma_block_i
       (.CLK(userclk_out),
        .configuration_vector(configuration_vector[3:1]),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pma_reset_out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk_out(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk_out(txoutclk),
        .txp(txp),
        .userclk2(userclk2_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_sync_block
   (resetdone,
    data_in,
    userclk2);
  output resetdone;
  input data_in;
  input userclk2;

  wire data_in;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(userclk2),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync5),
        .Q(resetdone),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_transceiver
   (txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    Q,
    \rxdata_reg[7]_0 ,
    data_in,
    pma_reset_out,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    userclk2,
    SR,
    powerdown,
    mgt_tx_reset,
    D,
    txchardispmode_reg_reg_0,
    txcharisk_reg_reg_0,
    enablealign,
    \txdata_reg_reg[7]_0 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  output data_in;
  input pma_reset_out;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input userclk2;
  input [0:0]SR;
  input powerdown;
  input mgt_tx_reset;
  input [0:0]D;
  input [0:0]txchardispmode_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input enablealign;
  input [7:0]\txdata_reg_reg[7]_0 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [0:0]D;
  wire PCS_PMA_gt_i_n_118;
  wire PCS_PMA_gt_i_n_58;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire enablealign;
  wire encommaalign_int;
  wire gtpowergood;
  wire gtrefclk_out;
  wire gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_done_out_int;
  wire gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_done_out_int;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_tx_reset;
  wire p_0_in;
  wire pma_reset_out;
  wire powerdown;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_reg__0;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire toggle;
  wire toggle_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [0:0]txchardispmode_reg_reg_0;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire userclk2;
  wire [0:0]NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED;
  wire [16:0]NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) 
  (* X_CORE_INFO = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt PCS_PMA_gt_i
       (.cplllock_out(NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .dmonitorout_out(NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED[16:0]),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_in(independent_clock_bufg),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_out(NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED[15:0]),
        .drpen_in(1'b0),
        .drprdy_out(NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED[0]),
        .drpwe_in(1'b0),
        .eyescandataerror_out(NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .gthrxn_in(rxn),
        .gthrxp_in(rxp),
        .gthtxn_out(txn),
        .gthtxp_out(txp),
        .gtpowergood_out(gtpowergood),
        .gtrefclk0_in(gtrefclk_out),
        .gtrefclk1_in(1'b0),
        .gtwiz_reset_all_in(pma_reset_out),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_rx_cdr_stable_out(NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out_int),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out_int),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userdata_rx_out(rxdata_int),
        .gtwiz_userdata_tx_in(txdata_int),
        .loopback_in({1'b0,1'b0,1'b0}),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx8b10ben_in(1'b1),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({PCS_PMA_gt_i_n_58,NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrhold_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_int),
        .rxcommadet_out(NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxctrl0_out({NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED[15:2],rxctrl0_out}),
        .rxctrl1_out({NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED[15:2],rxctrl1_out}),
        .rxctrl2_out({NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED[7:2],rxctrl2_out}),
        .rxctrl3_out({NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED[7:2],rxctrl3_out}),
        .rxdfelpmreset_in(1'b0),
        .rxlpmen_in(1'b1),
        .rxmcommaalignen_in(encommaalign_int),
        .rxoutclk_out(rxoutclk_out),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpowerdown,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxresetdone_out(NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED[0]),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(CLK),
        .tx8b10ben_in(1'b1),
        .txbufstatus_out({PCS_PMA_gt_i_n_118,NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED[0]}),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispval_int}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispmode_int}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txcharisk_int}),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txelecidle_in(txpowerdown),
        .txinhibit_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprgdivresetdone_out(NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED[0]),
        .txresetdone_out(NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED[0]),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_1
       (.I0(mgt_tx_reset),
        .I1(gtwiz_reset_tx_done_out_int),
        .O(gtwiz_reset_tx_datapath_in));
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_2
       (.I0(SR),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(gtwiz_reset_rx_datapath_in));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_sync1_i_1
       (.I0(gtwiz_reset_tx_done_out_int),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(data_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_reset_sync reclock_encommaalign
       (.enablealign(enablealign),
        .reset_out(encommaalign_int),
        .userclk2(userclk2));
  FDRE rxbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_58),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(userclk2),
        .CE(toggle),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    toggle_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(1'b0));
  FDRE txbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_118),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_reg),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispval_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(D),
        .Q(txchardispval_reg),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(mgt_tx_reset));
  FDRE \txdata_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(mgt_tx_reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    rxresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]rxresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    txresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input [0:0]txresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]txresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(txresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1
   (E,
    gtpowergood_out,
    drpclk_in,
    \FSM_sequential_sm_reset_all_reg[0] ,
    Q,
    \FSM_sequential_sm_reset_all_reg[0]_0 );
  output [0:0]E;
  input [0:0]gtpowergood_out;
  input [0:0]drpclk_in;
  input \FSM_sequential_sm_reset_all_reg[0] ;
  input [2:0]Q;
  input \FSM_sequential_sm_reset_all_reg[0]_0 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_all_reg[0] ;
  wire \FSM_sequential_sm_reset_all_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire [0:0]gtpowergood_out;
  wire gtpowergood_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;

  LUT6 #(
    .INIT(64'hAF0FAF00CFFFCFFF)) 
    \FSM_sequential_sm_reset_all[2]_i_1 
       (.I0(gtpowergood_sync),
        .I1(\FSM_sequential_sm_reset_all_reg[0] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\FSM_sequential_sm_reset_all_reg[0]_0 ),
        .I5(Q[1]),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtpowergood_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtpowergood_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10
   (\FSM_sequential_sm_reset_rx_reg[2] ,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    sm_reset_rx_cdr_to_sat_reg,
    rxcdrlock_out,
    drpclk_in,
    sm_reset_rx_cdr_to_clr_reg,
    Q,
    plllock_rx_sync,
    sm_reset_rx_cdr_to_clr,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    sm_reset_rx_cdr_to_sat);
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output sm_reset_rx_cdr_to_sat_reg;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_cdr_to_clr_reg;
  input [2:0]Q;
  input plllock_rx_sync;
  input sm_reset_rx_cdr_to_clr;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input sm_reset_rx_cdr_to_sat;

  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_n_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;
  wire [0:0]rxcdrlock_out;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_2_n_0;
  wire sm_reset_rx_cdr_to_clr_reg;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_reg;

  LUT6 #(
    .INIT(64'h000A000AC0C000C0)) 
    \FSM_sequential_sm_reset_rx[2]_i_4 
       (.I0(sm_reset_rx_cdr_to_sat_reg),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(plllock_rx_sync),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxcdrlock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(i_in_out_reg_n_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'hE)) 
    rxprogdivreset_out_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .O(sm_reset_rx_cdr_to_sat_reg));
  LUT6 #(
    .INIT(64'hFBFFFFFF0800AAAA)) 
    sm_reset_rx_cdr_to_clr_i_1
       (.I0(sm_reset_rx_cdr_to_clr_i_2_n_0),
        .I1(sm_reset_rx_cdr_to_clr_reg),
        .I2(Q[2]),
        .I3(plllock_rx_sync),
        .I4(Q[0]),
        .I5(sm_reset_rx_cdr_to_clr),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h00EF)) 
    sm_reset_rx_cdr_to_clr_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(sm_reset_rx_cdr_to_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2
   (gtwiz_reset_rx_datapath_dly,
    in0,
    drpclk_in);
  output gtwiz_reset_rx_datapath_dly;
  input in0;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire gtwiz_reset_rx_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3
   (D,
    i_in_out_reg_0,
    in0,
    drpclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    Q,
    gtwiz_reset_rx_datapath_dly,
    \FSM_sequential_sm_reset_rx_reg[0]_0 );
  output [1:0]D;
  output i_in_out_reg_0;
  input in0;
  input [0:0]drpclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input [2:0]Q;
  input gtwiz_reset_rx_datapath_dly;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;

  wire [1:0]D;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_datapath_dly;
  wire gtwiz_reset_rx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFF0088FF00FFFFF0)) 
    \FSM_sequential_sm_reset_rx[0]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(gtwiz_reset_rx_pll_and_datapath_dly),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000FFFF8F8F000F)) 
    \FSM_sequential_sm_reset_rx[1]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[2]),
        .I3(gtwiz_reset_rx_pll_and_datapath_dly),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000000E)) 
    \FSM_sequential_sm_reset_rx[2]_i_5 
       (.I0(gtwiz_reset_rx_pll_and_datapath_dly),
        .I1(gtwiz_reset_rx_datapath_dly),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .O(i_in_out_reg_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4
   (E,
    in0,
    drpclk_in,
    Q,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    gtwiz_reset_tx_pll_and_datapath_dly,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 );
  output [0:0]E;
  input in0;
  input [0:0]drpclk_in;
  input [0:0]Q;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input gtwiz_reset_tx_pll_and_datapath_dly;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire [0:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1110)) 
    \FSM_sequential_sm_reset_tx[2]_i_1 
       (.I0(Q),
        .I1(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I2(gtwiz_reset_tx_datapath_dly),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .I4(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I5(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5
   (gtwiz_reset_tx_pll_and_datapath_dly,
    D,
    in0,
    drpclk_in,
    Q);
  output gtwiz_reset_tx_pll_and_datapath_dly;
  output [1:0]D;
  input in0;
  input [0:0]drpclk_in;
  input [2:0]Q;

  wire [1:0]D;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h1F1E)) 
    \FSM_sequential_sm_reset_tx[0]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h0FF1)) 
    \FSM_sequential_sm_reset_tx[1]_i_1 
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_pll_and_datapath_dly),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(D[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6
   (\FSM_sequential_sm_reset_rx_reg[0] ,
    \FSM_sequential_sm_reset_rx_reg[2] ,
    E,
    rxpmaresetdone_out,
    drpclk_in,
    sm_reset_rx_timer_clr_reg,
    Q,
    sm_reset_rx_timer_clr_reg_0,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \FSM_sequential_sm_reset_rx_reg[0]_0 ,
    \FSM_sequential_sm_reset_rx_reg[0]_1 ,
    \FSM_sequential_sm_reset_rx_reg[0]_2 ,
    sm_reset_rx_pll_timer_sat,
    sm_reset_rx_timer_sat);
  output \FSM_sequential_sm_reset_rx_reg[0] ;
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output [0:0]E;
  input [0:0]rxpmaresetdone_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_timer_clr_reg;
  input [2:0]Q;
  input sm_reset_rx_timer_clr_reg_0;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  input sm_reset_rx_pll_timer_sat;
  input sm_reset_rx_timer_sat;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_rx[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_userclk_rx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxpmaresetdone_out;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_timer_clr_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg;
  wire sm_reset_rx_timer_clr_reg_0;
  wire sm_reset_rx_timer_sat;

  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_sm_reset_rx[2]_i_1 
       (.I0(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .I2(\FSM_sequential_sm_reset_rx_reg[0]_1 ),
        .O(E));
  LUT6 #(
    .INIT(64'h2023202000000000)) 
    \FSM_sequential_sm_reset_rx[2]_i_3 
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_2 ),
        .I4(sm_reset_rx_pll_timer_sat),
        .I5(Q[0]),
        .O(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxpmaresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_rx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFAAF00000800)) 
    rxuserrdy_out_i_1
       (.I0(Q[2]),
        .I1(sm_reset_rx_timer_clr_i_2_n_0),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  LUT6 #(
    .INIT(64'hFCCCEFFE0CCCE00E)) 
    sm_reset_rx_timer_clr_i_1
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(sm_reset_rx_timer_clr_reg),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(sm_reset_rx_timer_clr_reg_0),
        .O(\FSM_sequential_sm_reset_rx_reg[0] ));
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_timer_clr_i_2
       (.I0(sm_reset_rx_timer_clr_reg_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(gtwiz_reset_userclk_rx_active_sync),
        .O(sm_reset_rx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7
   (gtwiz_reset_userclk_tx_active_sync,
    \FSM_sequential_sm_reset_tx_reg[2] ,
    i_in_out_reg_0,
    drpclk_in,
    Q,
    sm_reset_tx_timer_clr_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    sm_reset_tx_timer_clr_reg_0,
    plllock_tx_sync,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 ,
    sm_reset_tx_pll_timer_sat);
  output gtwiz_reset_userclk_tx_active_sync;
  output \FSM_sequential_sm_reset_tx_reg[2] ;
  output i_in_out_reg_0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input sm_reset_tx_timer_clr_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input sm_reset_tx_timer_clr_reg_0;
  input plllock_tx_sync;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  input sm_reset_tx_pll_timer_sat;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_tx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_userclk_tx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire plllock_tx_sync;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_timer_clr_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg;
  wire sm_reset_tx_timer_clr_reg_0;

  LUT6 #(
    .INIT(64'h000F000088888888)) 
    \FSM_sequential_sm_reset_tx[2]_i_5 
       (.I0(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I3(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .I4(sm_reset_tx_pll_timer_sat),
        .I5(Q[0]),
        .O(i_in_out_reg_0));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_tx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEBEB282B)) 
    sm_reset_tx_timer_clr_i_1
       (.I0(sm_reset_tx_timer_clr_i_2_n_0),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(sm_reset_tx_timer_clr_reg),
        .O(\FSM_sequential_sm_reset_tx_reg[2] ));
  LUT6 #(
    .INIT(64'hA0C0A0C0F0F000F0)) 
    sm_reset_tx_timer_clr_i_2
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(sm_reset_tx_timer_clr_reg_0),
        .I3(Q[0]),
        .I4(plllock_tx_sync),
        .I5(Q[2]),
        .O(sm_reset_tx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8
   (plllock_rx_sync,
    i_in_out_reg_0,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_rx_done_int_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    Q,
    gtwiz_reset_rx_done_int_reg_0);
  output plllock_rx_sync;
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_rx_done_int_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [2:0]Q;
  input gtwiz_reset_rx_done_int_reg_0;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_done_int;
  wire gtwiz_reset_rx_done_int_reg;
  wire gtwiz_reset_rx_done_int_reg_0;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;

  LUT6 #(
    .INIT(64'hAAC0FFFFAAC00000)) 
    gtwiz_reset_rx_done_int_i_1
       (.I0(plllock_rx_sync),
        .I1(gtwiz_reset_rx_done_int_reg),
        .I2(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int),
        .I5(gtwiz_reset_rx_done_int_reg_0),
        .O(i_in_out_reg_0));
  LUT6 #(
    .INIT(64'h4C40000040400000)) 
    gtwiz_reset_rx_done_int_i_2
       (.I0(plllock_rx_sync),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I4(Q[1]),
        .I5(gtwiz_reset_rx_done_int_reg),
        .O(gtwiz_reset_rx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_rx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h88880000F5FF5555)) 
    sm_reset_rx_timer_clr_i_3
       (.I0(Q[1]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int_reg),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9
   (plllock_tx_sync,
    gtwiz_reset_tx_done_int_reg,
    i_in_out_reg_0,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_tx_done_int_reg_0,
    Q,
    sm_reset_tx_timer_sat,
    gtwiz_reset_tx_done_int_reg_1,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    \FSM_sequential_sm_reset_tx_reg[0] );
  output plllock_tx_sync;
  output gtwiz_reset_tx_done_int_reg;
  output i_in_out_reg_0;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_tx_done_int_reg_0;
  input [2:0]Q;
  input sm_reset_tx_timer_sat;
  input gtwiz_reset_tx_done_int_reg_1;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input \FSM_sequential_sm_reset_tx_reg[0] ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_tx_done_int;
  wire gtwiz_reset_tx_done_int_i_2_n_0;
  wire gtwiz_reset_tx_done_int_reg;
  wire gtwiz_reset_tx_done_int_reg_0;
  wire gtwiz_reset_tx_done_int_reg_1;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_tx_sync;
  wire sm_reset_tx_timer_sat;

  LUT6 #(
    .INIT(64'h00CFA00000000000)) 
    \FSM_sequential_sm_reset_tx[2]_i_4 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(plllock_tx_sync),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_sm_reset_tx_reg[0] ),
        .O(i_in_out_reg_0));
  LUT3 #(
    .INIT(8'hB8)) 
    gtwiz_reset_tx_done_int_i_1
       (.I0(gtwiz_reset_tx_done_int_i_2_n_0),
        .I1(gtwiz_reset_tx_done_int),
        .I2(gtwiz_reset_tx_done_int_reg_0),
        .O(gtwiz_reset_tx_done_int_reg));
  LUT6 #(
    .INIT(64'h4444444444F44444)) 
    gtwiz_reset_tx_done_int_i_2
       (.I0(Q[0]),
        .I1(plllock_tx_sync),
        .I2(sm_reset_tx_timer_sat),
        .I3(gtwiz_reset_tx_done_int_reg_1),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I5(Q[1]),
        .O(gtwiz_reset_tx_done_int_i_2_n_0));
  LUT6 #(
    .INIT(64'h3000404000004040)) 
    gtwiz_reset_tx_done_int_i_3
       (.I0(plllock_tx_sync),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I4(Q[0]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .O(gtwiz_reset_tx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_tx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gthe3_channel
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;
  wire xlnx_opt_;
  wire xlnx_opt__1;
  wire xlnx_opt__2;
  wire xlnx_opt__3;

  assign lopt_2 = xlnx_opt_;
  assign lopt_3 = xlnx_opt__1;
  assign lopt_4 = xlnx_opt__2;
  assign lopt_5 = xlnx_opt__3;
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC
       (.CE(lopt),
        .CESYNC(xlnx_opt_),
        .CLK(rxoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__1));
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC_1
       (.CE(lopt),
        .CESYNC(xlnx_opt__2),
        .CLK(txoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__3));
  (* box_type = "PRIMITIVE" *) 
  GTHE3_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(16'hF800),
    .ADAPT_CFG1(16'h0000),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .A_RXOSCALRESET(1'b0),
    .A_RXPROGDIVRESET(1'b0),
    .A_TXPROGDIVRESET(1'b0),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CDR_SWAP_MODE_EN(1'b0),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(15),
    .CLK_COR_MIN_LAT(12),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG0(16'h67F8),
    .CPLL_CFG1(16'hA4AC),
    .CPLL_CFG2(16'h0007),
    .CPLL_CFG3(6'h00),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(4),
    .CPLL_INIT_CFG0(16'h02B2),
    .CPLL_INIT_CFG1(8'h00),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DDI_CTRL(2'b00),
    .DDI_REALIGN_WAIT(15),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DFE_D_X_REL_POS(1'b0),
    .DFE_VCM_COMP_EN(1'b0),
    .DMONITOR_CFG0(10'h000),
    .DMONITOR_CFG1(8'h00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("FALSE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER0(16'h0000),
    .ES_QUALIFIER1(16'h0000),
    .ES_QUALIFIER2(16'h0000),
    .ES_QUALIFIER3(16'h0000),
    .ES_QUALIFIER4(16'h0000),
    .ES_QUAL_MASK0(16'h0000),
    .ES_QUAL_MASK1(16'h0000),
    .ES_QUAL_MASK2(16'h0000),
    .ES_QUAL_MASK3(16'h0000),
    .ES_QUAL_MASK4(16'h0000),
    .ES_SDATA_MASK0(16'h0000),
    .ES_SDATA_MASK1(16'h0000),
    .ES_SDATA_MASK2(16'h0000),
    .ES_SDATA_MASK3(16'h0000),
    .ES_SDATA_MASK4(16'h0000),
    .EVODD_PHI_CFG(11'b00000000000),
    .EYE_SCAN_SWAP_EN(1'b0),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(5'b00000),
    .GM_BIAS_SELECT(1'b0),
    .LOCAL_MASTER(1'b1),
    .OOBDIVCTL(2'b00),
    .OOB_PWRUP(1'b0),
    .PCI3_AUTO_REALIGN("OVR_1K_BLK"),
    .PCI3_PIPE_RX_ELECIDLE(1'b0),
    .PCI3_RX_ASYNC_EBUF_BYPASS(2'b00),
    .PCI3_RX_ELECIDLE_EI2_ENABLE(1'b0),
    .PCI3_RX_ELECIDLE_H2L_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_H2L_DISABLE(3'b000),
    .PCI3_RX_ELECIDLE_HI_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_LP4_DISABLE(1'b0),
    .PCI3_RX_FIFO_DISABLE(1'b0),
    .PCIE_BUFG_DIV_CTRL(16'h1000),
    .PCIE_RXPCS_CFG_GEN3(16'h02A4),
    .PCIE_RXPMA_CFG(16'h000A),
    .PCIE_TXPCS_CFG_GEN3(16'h2CA4),
    .PCIE_TXPMA_CFG(16'h000A),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD0(16'b0000000000000000),
    .PCS_RSVD1(3'b000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PLL_SEL_MODE_GEN12(2'h0),
    .PLL_SEL_MODE_GEN3(2'h3),
    .PMA_RSV1(16'hF000),
    .PROCESS_PAR(3'b010),
    .RATE_SW_USE_DRP(1'b1),
    .RESET_POWERSAVE_DISABLE(1'b0),
    .RXBUFRESET_TIME(5'b00011),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(0),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(0),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG0(16'h0000),
    .RXCDR_CFG0_GEN3(16'h0000),
    .RXCDR_CFG1(16'h0000),
    .RXCDR_CFG1_GEN3(16'h0000),
    .RXCDR_CFG2(16'h0746),
    .RXCDR_CFG2_GEN3(16'h07E6),
    .RXCDR_CFG3(16'h0000),
    .RXCDR_CFG3_GEN3(16'h0000),
    .RXCDR_CFG4(16'h0000),
    .RXCDR_CFG4_GEN3(16'h0000),
    .RXCDR_CFG5(16'h0000),
    .RXCDR_CFG5_GEN3(16'h0000),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG0(16'h4480),
    .RXCDR_LOCK_CFG1(16'h5FFF),
    .RXCDR_LOCK_CFG2(16'h77C3),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXCFOK_CFG0(16'h4000),
    .RXCFOK_CFG1(16'h0065),
    .RXCFOK_CFG2(16'h002E),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDFELPM_KL_CFG0(16'h0000),
    .RXDFELPM_KL_CFG1(16'h0032),
    .RXDFELPM_KL_CFG2(16'h0000),
    .RXDFE_CFG0(16'h0A00),
    .RXDFE_CFG1(16'h0000),
    .RXDFE_GC_CFG0(16'h0000),
    .RXDFE_GC_CFG1(16'h7870),
    .RXDFE_GC_CFG2(16'h0000),
    .RXDFE_H2_CFG0(16'h0000),
    .RXDFE_H2_CFG1(16'h0000),
    .RXDFE_H3_CFG0(16'h4000),
    .RXDFE_H3_CFG1(16'h0000),
    .RXDFE_H4_CFG0(16'h2000),
    .RXDFE_H4_CFG1(16'h0003),
    .RXDFE_H5_CFG0(16'h2000),
    .RXDFE_H5_CFG1(16'h0003),
    .RXDFE_H6_CFG0(16'h2000),
    .RXDFE_H6_CFG1(16'h0000),
    .RXDFE_H7_CFG0(16'h2000),
    .RXDFE_H7_CFG1(16'h0000),
    .RXDFE_H8_CFG0(16'h2000),
    .RXDFE_H8_CFG1(16'h0000),
    .RXDFE_H9_CFG0(16'h2000),
    .RXDFE_H9_CFG1(16'h0000),
    .RXDFE_HA_CFG0(16'h2000),
    .RXDFE_HA_CFG1(16'h0000),
    .RXDFE_HB_CFG0(16'h2000),
    .RXDFE_HB_CFG1(16'h0000),
    .RXDFE_HC_CFG0(16'h0000),
    .RXDFE_HC_CFG1(16'h0000),
    .RXDFE_HD_CFG0(16'h0000),
    .RXDFE_HD_CFG1(16'h0000),
    .RXDFE_HE_CFG0(16'h0000),
    .RXDFE_HE_CFG1(16'h0000),
    .RXDFE_HF_CFG0(16'h0000),
    .RXDFE_HF_CFG1(16'h0000),
    .RXDFE_OS_CFG0(16'h8000),
    .RXDFE_OS_CFG1(16'h0000),
    .RXDFE_UT_CFG0(16'h8000),
    .RXDFE_UT_CFG1(16'h0003),
    .RXDFE_VP_CFG0(16'hAA00),
    .RXDFE_VP_CFG1(16'h0033),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(16'h0030),
    .RXELECIDLE_CFG("Sigcfg_4"),
    .RXGBOX_FIFO_INIT_RD_ADDR(4),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_CFG(16'h0000),
    .RXLPM_GC_CFG(16'h1000),
    .RXLPM_KH_CFG0(16'h0000),
    .RXLPM_KH_CFG1(16'h0002),
    .RXLPM_OS_CFG0(16'h8000),
    .RXLPM_OS_CFG1(16'h0002),
    .RXOOB_CFG(9'b000000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00011),
    .RXPHBEACON_CFG(16'h0000),
    .RXPHDLY_CFG(16'h2020),
    .RXPHSAMP_CFG(16'h2100),
    .RXPHSLIP_CFG(16'h6622),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_CFG0(2'b01),
    .RXPI_CFG1(2'b01),
    .RXPI_CFG2(2'b01),
    .RXPI_CFG3(2'b01),
    .RXPI_CFG4(1'b1),
    .RXPI_CFG5(1'b1),
    .RXPI_CFG6(3'b011),
    .RXPI_LPM(1'b0),
    .RXPI_VREFSEL(1'b0),
    .RXPMACLK_SEL("DATA"),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXPRBS_LINKACQ_CNT(15),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b0),
    .RX_AFE_CM_EN(1'b0),
    .RX_BIAS_CFG0(16'h0AB4),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CAPFF_SARC_ENB(1'b0),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_EN(1'b1),
    .RX_CLK_SLIP_OVRD(5'b00000),
    .RX_CM_BUF_CFG(4'b1010),
    .RX_CM_BUF_PD(1'b0),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(4'b1010),
    .RX_CTLE3_LPF(8'b00000001),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFELPM_CFG0(4'b0110),
    .RX_DFELPM_CFG1(1'b1),
    .RX_DFELPM_KLKH_AGC_STUP_EN(1'b1),
    .RX_DFE_AGC_CFG0(2'b10),
    .RX_DFE_AGC_CFG1(3'b000),
    .RX_DFE_KL_LPM_KH_CFG0(2'b01),
    .RX_DFE_KL_LPM_KH_CFG1(3'b000),
    .RX_DFE_KL_LPM_KL_CFG0(2'b01),
    .RX_DFE_KL_LPM_KL_CFG1(3'b000),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_DIVRESET_TIME(5'b00001),
    .RX_EN_HI_LR(1'b0),
    .RX_EYESCAN_VS_CODE(7'b0000000),
    .RX_EYESCAN_VS_NEG_DIR(1'b0),
    .RX_EYESCAN_VS_RANGE(2'b00),
    .RX_EYESCAN_VS_UT_SIGN(1'b0),
    .RX_FABINT_USRCLK_FLOP(1'b0),
    .RX_INT_DATAWIDTH(0),
    .RX_PMA_POWER_SAVE(1'b0),
    .RX_PROGDIV_CFG(0.000000),
    .RX_SAMPLE_PERIOD(3'b111),
    .RX_SIG_VALID_DLY(11),
    .RX_SUM_DFETAPREP_EN(1'b0),
    .RX_SUM_IREF_TUNE(4'b1100),
    .RX_SUM_RES_CTRL(2'b11),
    .RX_SUM_VCMTUNE(4'b0000),
    .RX_SUM_VCM_OVWR(1'b0),
    .RX_SUM_VREF_TUNE(3'b000),
    .RX_TUNE_AFE_OS(2'b10),
    .RX_WIDEMODE_CDR(1'b0),
    .RX_XCLK_SEL("RXDES"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b1110),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_MODE("FAST"),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL(1'b0),
    .SIM_VERSION(2),
    .TAPDLY_SET_TX(2'h0),
    .TEMPERATUR_PAR(4'b0010),
    .TERM_RCAL_CFG(15'b100001000010000),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV0(8'h00),
    .TST_RSV1(8'h00),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h0009),
    .TXDLY_LCFG(16'h0050),
    .TXDRVBIAS_N(4'b1010),
    .TXDRVBIAS_P(4'b1010),
    .TXFIFO_ADDR_CFG("LOW"),
    .TXGBOX_FIFO_INIT_RD_ADDR(4),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00011),
    .TXPHDLY_CFG0(16'h2020),
    .TXPHDLY_CFG1(16'h0075),
    .TXPH_CFG(16'h0980),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG0(2'b01),
    .TXPI_CFG1(2'b01),
    .TXPI_CFG2(2'b01),
    .TXPI_CFG3(1'b1),
    .TXPI_CFG4(1'b1),
    .TXPI_CFG5(3'b011),
    .TXPI_GRAY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b1),
    .TXPI_LPM(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b001),
    .TXPI_VREFSEL(1'b0),
    .TXPMARESET_TIME(5'b00011),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_EN(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DCD_CFG(6'b000010),
    .TX_DCD_EN(1'b0),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DIVRESET_TIME(5'b00001),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b100),
    .TX_EIDLE_DEASSERT_DELAY(3'b011),
    .TX_EML_PHI_TUNE(1'b0),
    .TX_FABINT_USRCLK_FLOP(1'b0),
    .TX_IDLE_DATA_ZERO(1'b0),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001111),
    .TX_MARGIN_FULL_1(7'b1001110),
    .TX_MARGIN_FULL_2(7'b1001100),
    .TX_MARGIN_FULL_3(7'b1001010),
    .TX_MARGIN_FULL_4(7'b1001000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000101),
    .TX_MARGIN_LOW_2(7'b1000011),
    .TX_MARGIN_LOW_3(7'b1000010),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_MODE_SEL(3'b000),
    .TX_PMADATA_OPT(1'b0),
    .TX_PMA_POWER_SAVE(1'b0),
    .TX_PROGCLK_SEL("CPLL"),
    .TX_PROGDIV_CFG(20.000000),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h0032),
    .TX_RXDETECT_REF(3'b100),
    .TX_SAMPLE_PERIOD(3'b111),
    .TX_SARC_LPBK_ENB(1'b0),
    .TX_XCLK_SEL("TXOUT"),
    .USE_PCS_CLK_PHASE_SEL(1'b0),
    .WB_MODE(2'b00)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST 
       (.BUFGTCE({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 }),
        .BUFGTCEMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 }),
        .BUFGTDIV({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 }),
        .BUFGTRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 }),
        .BUFGTRSTMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 }),
        .CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .CPLLFBCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ),
        .CPLLLOCK(cplllock_out),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .CPLLREFCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(1'b0),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 }),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(drpclk_in),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 }),
        .DRPEN(1'b0),
        .DRPRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ),
        .DRPWE(1'b0),
        .EVODDPHICALDONE(1'b0),
        .EVODDPHICALSTART(1'b0),
        .EVODDPHIDRDEN(1'b0),
        .EVODDPHIDWREN(1'b0),
        .EVODDPHIXRDEN(1'b0),
        .EVODDPHIXWREN(1'b0),
        .EYESCANDATAERROR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTHRXN(gthrxn_in),
        .GTHRXP(gthrxp_in),
        .GTHTXN(gthtxn_out),
        .GTHTXP(gthtxp_out),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTPOWERGOOD(gtpowergood_out),
        .GTREFCLK0(gtrefclk0_in),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(\gen_gtwizard_gthe3.gtrxreset_int ),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(\gen_gtwizard_gthe3.gttxreset_int ),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .LPBKRXTXSEREN(1'b0),
        .LPBKTXRXSEREN(1'b0),
        .PCIEEQRXEQADAPTDONE(1'b0),
        .PCIERATEGEN3(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ),
        .PCIERATEIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ),
        .PCIERATEQPLLPD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 }),
        .PCIERATEQPLLRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 }),
        .PCIERSTIDLE(1'b0),
        .PCIERSTTXSYNCSTART(1'b0),
        .PCIESYNCTXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ),
        .PCIEUSERGEN3RDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ),
        .PCIEUSERPHYSTATUSRST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ),
        .PCIEUSERRATEDONE(1'b0),
        .PCIEUSERRATESTART(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 }),
        .PHYSTATUS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ),
        .PINRSRVDAS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 }),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLL0CLK(1'b0),
        .QPLL0REFCLK(1'b0),
        .QPLL1CLK(1'b0),
        .QPLL1REFCLK(1'b0),
        .RESETEXCEPTION(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ),
        .RESETOVRD(1'b0),
        .RSTCLKENTX(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({rxbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 }),
        .RXBYTEISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ),
        .RXBYTEREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(rxcdrlock_out),
        .RXCDROVRDEN(1'b0),
        .RXCDRPHDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ),
        .RXCHANISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ),
        .RXCHANREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 }),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(rxclkcorcnt_out),
        .RXCOMINITDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ),
        .RXCOMMADET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ),
        .RXCOMWAKEDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ),
        .RXCTRL0({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ,rxctrl0_out}),
        .RXCTRL1({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ,rxctrl1_out}),
        .RXCTRL2({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ,rxctrl2_out}),
        .RXCTRL3({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ,rxctrl3_out}),
        .RXDATA({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ,gtwiz_userdata_rx_out}),
        .RXDATAEXTENDRSVD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 }),
        .RXDATAVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 }),
        .RXDFEAGCCTRL({1'b0,1'b1}),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP10HOLD(1'b0),
        .RXDFETAP10OVRDEN(1'b0),
        .RXDFETAP11HOLD(1'b0),
        .RXDFETAP11OVRDEN(1'b0),
        .RXDFETAP12HOLD(1'b0),
        .RXDFETAP12OVRDEN(1'b0),
        .RXDFETAP13HOLD(1'b0),
        .RXDFETAP13OVRDEN(1'b0),
        .RXDFETAP14HOLD(1'b0),
        .RXDFETAP14OVRDEN(1'b0),
        .RXDFETAP15HOLD(1'b0),
        .RXDFETAP15OVRDEN(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFETAP6HOLD(1'b0),
        .RXDFETAP6OVRDEN(1'b0),
        .RXDFETAP7HOLD(1'b0),
        .RXDFETAP7OVRDEN(1'b0),
        .RXDFETAP8HOLD(1'b0),
        .RXDFETAP8OVRDEN(1'b0),
        .RXDFETAP9HOLD(1'b0),
        .RXDFETAP9OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ),
        .RXELECIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 }),
        .RXHEADERVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 }),
        .RXLATCLK(1'b0),
        .RXLPMEN(1'b1),
        .RXLPMGCHOLD(1'b0),
        .RXLPMGCOVRDEN(1'b0),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXLPMOSHOLD(1'b0),
        .RXLPMOSOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(rxmcommaalignen_in),
        .RXMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 }),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTCFG({1'b1,1'b1,1'b0,1'b1}),
        .RXOSINTDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ),
        .RXOSINTEN(1'b1),
        .RXOSINTHOLD(1'b0),
        .RXOSINTOVRDEN(1'b0),
        .RXOSINTSTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ),
        .RXOSINTSTROBE(1'b0),
        .RXOSINTSTROBEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ),
        .RXOSINTSTROBESTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ),
        .RXOSINTTESTOVRDEN(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk_out),
        .RXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ),
        .RXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(rxmcommaalignen_in),
        .RXPCSRESET(1'b0),
        .RXPD({rxpd_in,rxpd_in}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ),
        .RXPHALIGNEN(1'b0),
        .RXPHALIGNERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ),
        .RXPHDLYPD(1'b1),
        .RXPHDLYRESET(1'b0),
        .RXPHOVRDEN(1'b0),
        .RXPLLCLKSEL({1'b0,1'b0}),
        .RXPMARESET(1'b0),
        .RXPMARESETDONE(rxpmaresetdone_out),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ),
        .RXPRBSLOCKED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ),
        .RXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .RXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ),
        .RXPROGDIVRESET(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .RXQPIEN(1'b0),
        .RXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ),
        .RXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ),
        .RXRATEMODE(1'b0),
        .RXRECCLKOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ),
        .RXRESETDONE(rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSLIDERDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ),
        .RXSLIPDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ),
        .RXSLIPOUTCLK(1'b0),
        .RXSLIPOUTCLKRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ),
        .RXSLIPPMA(1'b0),
        .RXSLIPPMARDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ),
        .RXSTARTOFSEQ({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 }),
        .RXSTATUS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 }),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .RXUSRCLK(rxusrclk_in),
        .RXUSRCLK2(rxusrclk_in),
        .RXVALID(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b0,1'b0,1'b0}),
        .TXBUFSTATUS({txbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 }),
        .TXCOMFINISH(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXCTRL0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in}),
        .TXCTRL1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in}),
        .TXCTRL2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in}),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtwiz_userdata_tx_in}),
        .TXDATAEXTENDRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(txelecidle_in),
        .TXHEADER({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXLATCLK(1'b0),
        .TXMAINCURSOR({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk_out),
        .TXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ),
        .TXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ),
        .TXOUTCLKSEL({1'b1,1'b0,1'b1}),
        .TXPCSRESET(1'b0),
        .TXPD({txelecidle_in,txelecidle_in}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b1),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b0),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPLLCLKSEL({1'b0,1'b0}),
        .TXPMARESET(1'b0),
        .TXPMARESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ),
        .TXPROGDIVRESET(\gen_gtwizard_gthe3.txprogdivreset_int ),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ),
        .TXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(\gen_gtwizard_gthe3.txuserrdy_int ),
        .TXUSRCLK(rxusrclk_in),
        .TXUSRCLK2(rxusrclk_in));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_meta_i_1__2
       (.I0(cplllock_out),
        .O(rst_in0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gtwiz_reset
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    \gen_gtwizard_gthe3.gttxreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    gtpowergood_out,
    cplllock_out,
    rxpmaresetdone_out,
    rxcdrlock_out,
    drpclk_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    rst_in0,
    rxusrclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    gtwiz_reset_rx_datapath_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync );
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output \gen_gtwizard_gthe3.gttxreset_int ;
  output \gen_gtwizard_gthe3.txuserrdy_int ;
  output \gen_gtwizard_gthe3.rxprogdivreset_int ;
  output \gen_gtwizard_gthe3.gtrxreset_int ;
  output \gen_gtwizard_gthe3.rxuserrdy_int ;
  output \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]gtpowergood_out;
  input [0:0]cplllock_out;
  input [0:0]rxpmaresetdone_out;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in0;
  input [0:0]rxusrclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;

  wire \FSM_sequential_sm_reset_all[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_all[2]_i_4_n_0 ;
  wire \FSM_sequential_sm_reset_rx[1]_i_2_n_0 ;
  wire \FSM_sequential_sm_reset_rx[2]_i_6_n_0 ;
  wire \FSM_sequential_sm_reset_tx[2]_i_3_n_0 ;
  wire bit_synchronizer_gtpowergood_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2;
  wire bit_synchronizer_plllock_rx_inst_n_1;
  wire bit_synchronizer_plllock_rx_inst_n_2;
  wire bit_synchronizer_plllock_tx_inst_n_1;
  wire bit_synchronizer_plllock_tx_inst_n_2;
  wire bit_synchronizer_rxcdrlock_inst_n_0;
  wire bit_synchronizer_rxcdrlock_inst_n_1;
  wire bit_synchronizer_rxcdrlock_inst_n_2;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gtpowergood_out;
  wire gttxreset_out_i_3_n_0;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_rx_datapath_dly;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_datapath_sync;
  wire gtwiz_reset_rx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_sync;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_datapath_sync;
  wire gtwiz_reset_tx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_sync;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire p_0_in;
  wire [9:0]p_0_in__0;
  wire [9:0]p_0_in__1;
  wire [2:0]p_1_in;
  wire plllock_rx_sync;
  wire plllock_tx_sync;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_3;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_3;
  wire rst_in0;
  wire [0:0]rxcdrlock_out;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [2:0]sm_reset_all;
  wire [2:0]sm_reset_all__0;
  wire sm_reset_all_timer_clr_i_1_n_0;
  wire sm_reset_all_timer_clr_i_2_n_0;
  wire sm_reset_all_timer_clr_reg_n_0;
  wire [2:0]sm_reset_all_timer_ctr;
  wire sm_reset_all_timer_ctr0_n_0;
  wire \sm_reset_all_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_all_timer_sat;
  wire sm_reset_all_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_rx;
  wire [2:0]sm_reset_rx__0;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_3_n_0;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ;
  wire [25:0]sm_reset_rx_cdr_to_ctr_reg;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_i_1_n_0;
  wire sm_reset_rx_cdr_to_sat_i_2_n_0;
  wire sm_reset_rx_cdr_to_sat_i_3_n_0;
  wire sm_reset_rx_cdr_to_sat_i_4_n_0;
  wire sm_reset_rx_cdr_to_sat_i_5_n_0;
  wire sm_reset_rx_cdr_to_sat_i_6_n_0;
  wire sm_reset_rx_pll_timer_clr_i_1_n_0;
  wire sm_reset_rx_pll_timer_clr_reg_n_0;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_rx_pll_timer_ctr_reg;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_pll_timer_sat_i_1_n_0;
  wire sm_reset_rx_pll_timer_sat_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_rx_timer_ctr;
  wire sm_reset_rx_timer_ctr0_n_0;
  wire \sm_reset_rx_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_rx_timer_sat;
  wire sm_reset_rx_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_tx;
  wire [2:0]sm_reset_tx__0;
  wire sm_reset_tx_pll_timer_clr_i_1_n_0;
  wire sm_reset_tx_pll_timer_clr_reg_n_0;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_tx_pll_timer_ctr_reg;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_pll_timer_sat_i_1_n_0;
  wire sm_reset_tx_pll_timer_sat_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_tx_timer_ctr;
  wire sm_reset_tx_timer_sat;
  wire sm_reset_tx_timer_sat_i_1_n_0;
  wire txuserrdy_out_i_3_n_0;
  wire [7:1]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED ;
  wire [7:2]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00FFF70000FFFFFF)) 
    \FSM_sequential_sm_reset_all[0]_i_1 
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .I3(sm_reset_all[2]),
        .I4(sm_reset_all[1]),
        .I5(sm_reset_all[0]),
        .O(sm_reset_all__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h34)) 
    \FSM_sequential_sm_reset_all[1]_i_1 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[0]),
        .O(sm_reset_all__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h4A)) 
    \FSM_sequential_sm_reset_all[2]_i_2 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .O(sm_reset_all__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_sm_reset_all[2]_i_3 
       (.I0(sm_reset_all_timer_sat),
        .I1(gtwiz_reset_rx_done_int_reg_n_0),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_all[2]_i_4 
       (.I0(sm_reset_all_timer_clr_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[0]),
        .Q(sm_reset_all[0]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[1]),
        .Q(sm_reset_all[1]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[2]),
        .Q(sm_reset_all[2]),
        .R(gtwiz_reset_all_sync));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_sm_reset_rx[1]_i_2 
       (.I0(sm_reset_rx_timer_sat),
        .I1(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDFD8888DDDD8888)) 
    \FSM_sequential_sm_reset_rx[2]_i_2 
       (.I0(sm_reset_rx[1]),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx_timer_sat),
        .I3(sm_reset_rx_timer_clr_reg_n_0),
        .I4(sm_reset_rx[2]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .O(sm_reset_rx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h00004000)) 
    \FSM_sequential_sm_reset_rx[2]_i_6 
       (.I0(sm_reset_rx[0]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[0]),
        .Q(sm_reset_rx[0]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[1]),
        .Q(sm_reset_rx[1]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[2]),
        .Q(sm_reset_rx[2]),
        .R(gtwiz_reset_rx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \FSM_sequential_sm_reset_tx[2]_i_2 
       (.I0(sm_reset_tx[0]),
        .I1(sm_reset_tx[1]),
        .I2(sm_reset_tx[2]),
        .O(sm_reset_tx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_sm_reset_tx[2]_i_3 
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .O(\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[0]),
        .Q(sm_reset_tx[0]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[1]),
        .Q(sm_reset_tx[1]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[2]),
        .Q(sm_reset_tx[2]),
        .R(gtwiz_reset_tx_any_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1 bit_synchronizer_gtpowergood_inst
       (.E(bit_synchronizer_gtpowergood_inst_n_0),
        .\FSM_sequential_sm_reset_all_reg[0] (\FSM_sequential_sm_reset_all[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_all_reg[0]_0 (\FSM_sequential_sm_reset_all[2]_i_4_n_0 ),
        .Q(sm_reset_all),
        .drpclk_in(drpclk_in),
        .gtpowergood_out(gtpowergood_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2 bit_synchronizer_gtwiz_reset_rx_datapath_dly_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .in0(gtwiz_reset_rx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3 bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst
       (.D(sm_reset_rx__0[1:0]),
        .\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4 bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst
       (.E(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0] (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (bit_synchronizer_plllock_tx_inst_n_2),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .Q(sm_reset_tx[0]),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5 bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst
       (.D(sm_reset_tx__0[1:0]),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6 bit_synchronizer_gtwiz_reset_userclk_rx_active_inst
       (.E(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[0]_1 (bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0]_2 (sm_reset_rx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .sm_reset_rx_pll_timer_sat(sm_reset_rx_pll_timer_sat),
        .sm_reset_rx_timer_clr_reg(bit_synchronizer_plllock_rx_inst_n_2),
        .sm_reset_rx_timer_clr_reg_0(sm_reset_rx_timer_clr_reg_n_0),
        .sm_reset_rx_timer_sat(sm_reset_rx_timer_sat));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7 bit_synchronizer_gtwiz_reset_userclk_tx_active_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (txuserrdy_out_i_3_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (sm_reset_tx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_tx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_pll_timer_sat(sm_reset_tx_pll_timer_sat),
        .sm_reset_tx_timer_clr_reg(sm_reset_tx_timer_clr_reg_n_0),
        .sm_reset_tx_timer_clr_reg_0(gttxreset_out_i_3_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8 bit_synchronizer_plllock_rx_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_plllock_rx_inst_n_2),
        .Q(sm_reset_rx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_done_int_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_done_int_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_rx_inst_n_1),
        .plllock_rx_sync(plllock_rx_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9 bit_synchronizer_plllock_tx_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (gttxreset_out_i_3_n_0),
        .Q(sm_reset_tx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_tx_done_int_reg(bit_synchronizer_plllock_tx_inst_n_1),
        .gtwiz_reset_tx_done_int_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .gtwiz_reset_tx_done_int_reg_1(sm_reset_tx_timer_clr_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_tx_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_timer_sat(sm_reset_tx_timer_sat));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10 bit_synchronizer_rxcdrlock_inst
       (.\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rxcdrlock_out(rxcdrlock_out),
        .sm_reset_rx_cdr_to_clr(sm_reset_rx_cdr_to_clr),
        .sm_reset_rx_cdr_to_clr_reg(sm_reset_rx_cdr_to_clr_i_3_n_0),
        .sm_reset_rx_cdr_to_sat(sm_reset_rx_cdr_to_sat),
        .sm_reset_rx_cdr_to_sat_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  LUT2 #(
    .INIT(4'hE)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\gen_gtwizard_gthe3.cpllpd_ch_int ));
  FDRE #(
    .INIT(1'b1)) 
    gtrxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.gtrxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h2)) 
    gttxreset_out_i_3
       (.I0(sm_reset_tx_timer_sat),
        .I1(sm_reset_tx_timer_clr_reg_n_0),
        .O(gttxreset_out_i_3_n_0));
  FDRE #(
    .INIT(1'b1)) 
    gttxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.gttxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hF740)) 
    gtwiz_reset_rx_datapath_int_i_1
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_rx_inst_n_1),
        .Q(gtwiz_reset_rx_done_int_reg_n_0),
        .R(gtwiz_reset_rx_any_sync));
  LUT4 #(
    .INIT(16'hF704)) 
    gtwiz_reset_rx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[2]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_tx_inst_n_1),
        .Q(gtwiz_reset_tx_done_int_reg_n_0),
        .R(gtwiz_reset_tx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hFB02)) 
    gtwiz_reset_tx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    pllreset_rx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    pllreset_tx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer reset_synchronizer_gtwiz_reset_all_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_all_sync(gtwiz_reset_all_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11 reset_synchronizer_gtwiz_reset_rx_any_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[1]_0 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[1]_1 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .gtrxreset_out_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0),
        .rst_in_out_reg_1(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .rxprogdivreset_out_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12 reset_synchronizer_gtwiz_reset_rx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .in0(gtwiz_reset_rx_datapath_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13 reset_synchronizer_gtwiz_reset_rx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14 reset_synchronizer_gtwiz_reset_tx_any_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .\FSM_sequential_sm_reset_tx_reg[1] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .\FSM_sequential_sm_reset_tx_reg[1]_0 (reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gttxreset_out_reg(gttxreset_out_i_3_n_0),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .plllock_tx_sync(plllock_tx_sync),
        .rst_in_out_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .txuserrdy_out_reg(txuserrdy_out_i_3_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15 reset_synchronizer_gtwiz_reset_tx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .in0(gtwiz_reset_tx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16 reset_synchronizer_gtwiz_reset_tx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer reset_synchronizer_rx_done_inst
       (.gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17 reset_synchronizer_tx_done_inst
       (.gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18 reset_synchronizer_txprogdivreset_inst
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .rst_in0(rst_in0));
  FDRE #(
    .INIT(1'b1)) 
    rxprogdivreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEFFA200A)) 
    sm_reset_all_timer_clr_i_1
       (.I0(sm_reset_all_timer_clr_i_2_n_0),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(sm_reset_all[0]),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_clr_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000B0003333BB33)) 
    sm_reset_all_timer_clr_i_2
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all[2]),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .I5(sm_reset_all[1]),
        .O(sm_reset_all_timer_clr_i_2_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_all_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_clr_i_1_n_0),
        .Q(sm_reset_all_timer_clr_reg_n_0),
        .S(gtwiz_reset_all_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_all_timer_ctr0
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .O(sm_reset_all_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_all_timer_ctr[0]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .O(\sm_reset_all_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_all_timer_ctr[1]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .O(\sm_reset_all_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_all_timer_ctr[2]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .I2(sm_reset_all_timer_ctr[2]),
        .O(\sm_reset_all_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[0]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[1]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[2]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_all_timer_sat_i_1
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_all_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_sat_i_1_n_0),
        .Q(sm_reset_all_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_cdr_to_clr_i_3
       (.I0(sm_reset_rx_timer_clr_reg_n_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(sm_reset_rx[1]),
        .O(sm_reset_rx_cdr_to_clr_i_3_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_cdr_to_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx_cdr_to_clr),
        .S(gtwiz_reset_rx_any_sync));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_1 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[1]),
        .I2(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ),
        .I3(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ),
        .I4(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ),
        .I5(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_3 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[18]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[14]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_4 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[24]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[20]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_5 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[12]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[9]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_6 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[7]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[2]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_7 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[0]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 }),
        .S({sm_reset_rx_cdr_to_ctr_reg[7:1],\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[10] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[10]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[11] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[11]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[12] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[12]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[13] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[13]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[14] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[14]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[15] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[15]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[16] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[16]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[16]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[17] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[17]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[18] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[18]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[19] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[19]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[1]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[20] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[20]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[21] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[21]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[22] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[22]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[23] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[23]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[24] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[24]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[24]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED [7:1],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED [7:2],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,sm_reset_rx_cdr_to_ctr_reg[25:24]}));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[25] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[25]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[2]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[3]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[4]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[5]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[6]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[7]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[8]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[8]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[9]),
        .R(sm_reset_rx_cdr_to_clr));
  LUT3 #(
    .INIT(8'h0E)) 
    sm_reset_rx_cdr_to_sat_i_1
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(sm_reset_rx_cdr_to_sat_i_2_n_0),
        .I2(sm_reset_rx_cdr_to_clr),
        .O(sm_reset_rx_cdr_to_sat_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sm_reset_rx_cdr_to_sat_i_2
       (.I0(sm_reset_rx_cdr_to_sat_i_3_n_0),
        .I1(sm_reset_rx_cdr_to_sat_i_4_n_0),
        .I2(sm_reset_rx_cdr_to_sat_i_5_n_0),
        .I3(sm_reset_rx_cdr_to_sat_i_6_n_0),
        .I4(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[1]),
        .O(sm_reset_rx_cdr_to_sat_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    sm_reset_rx_cdr_to_sat_i_3
       (.I0(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[2]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[7]),
        .O(sm_reset_rx_cdr_to_sat_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    sm_reset_rx_cdr_to_sat_i_4
       (.I0(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[20]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[24]),
        .O(sm_reset_rx_cdr_to_sat_i_4_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    sm_reset_rx_cdr_to_sat_i_5
       (.I0(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[14]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[18]),
        .O(sm_reset_rx_cdr_to_sat_i_5_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    sm_reset_rx_cdr_to_sat_i_6
       (.I0(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[9]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[12]),
        .O(sm_reset_rx_cdr_to_sat_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_cdr_to_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_cdr_to_sat_i_1_n_0),
        .Q(sm_reset_rx_cdr_to_sat),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF3000B)) 
    sm_reset_rx_pll_timer_clr_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx[2]),
        .I4(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_rx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_rx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(p_0_in__1[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_rx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_rx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_rx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .O(p_0_in__1[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(p_0_in__1[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[0]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[0]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[1]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[1]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[2]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[2]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[3]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[3]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[4]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[4]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[5]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[5]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[6]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[6]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[7]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[7]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[8]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[8]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[9]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[9]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_rx_pll_timer_sat_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_rx_pll_timer_sat_i_2
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(sm_reset_rx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .Q(sm_reset_rx_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_rx_timer_ctr0
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .O(sm_reset_rx_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .O(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .O(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .I2(sm_reset_rx_timer_ctr[2]),
        .O(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[0]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[1]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[2]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_rx_timer_sat_i_1
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(sm_reset_rx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hEFEF1101)) 
    sm_reset_tx_pll_timer_clr_i_1
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx[0]),
        .I3(sm_reset_tx_pll_timer_sat),
        .I4(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_tx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_tx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(p_0_in__0[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_tx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_tx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_tx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .O(p_0_in__0[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(p_0_in__0[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[0]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[0]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[1]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[1]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[2]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[2]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[3]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[3]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[4]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[4]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[5]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[5]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[6]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[6]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[7]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[7]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[8]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[8]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[9]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[9]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_tx_pll_timer_sat_i_1
       (.I0(sm_reset_tx_pll_timer_sat),
        .I1(sm_reset_tx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_tx_pll_timer_sat_i_2
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(sm_reset_tx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_tx_timer_ctr0
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .O(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .I2(sm_reset_tx_timer_ctr[2]),
        .O(p_1_in[2]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[0]),
        .Q(sm_reset_tx_timer_ctr[0]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[1]),
        .Q(sm_reset_tx_timer_ctr[1]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[2]),
        .Q(sm_reset_tx_timer_ctr[2]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_tx_timer_sat_i_1
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .I3(sm_reset_tx_timer_sat),
        .I4(sm_reset_tx_timer_clr_reg_n_0),
        .O(sm_reset_tx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    txuserrdy_out_i_3
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx_timer_clr_reg_n_0),
        .I3(sm_reset_tx_timer_sat),
        .O(txuserrdy_out_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    txuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.txuserrdy_int ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer
   (gtwiz_reset_rx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_rx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_rx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17
   (gtwiz_reset_tx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_tx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_tx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1__0_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1__0
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_tx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer
   (gtwiz_reset_all_sync,
    drpclk_in,
    gtwiz_reset_all_in);
  output gtwiz_reset_all_sync;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_all_in),
        .Q(gtwiz_reset_all_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11
   (gtwiz_reset_rx_any_sync,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[1]_0 ,
    \FSM_sequential_sm_reset_rx_reg[1]_1 ,
    drpclk_in,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ,
    rxprogdivreset_out_reg,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    plllock_rx_sync,
    gtrxreset_out_reg,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    rst_in_out_reg_0,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_1);
  output gtwiz_reset_rx_any_sync;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  input rxprogdivreset_out_reg;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input plllock_rx_sync;
  input gtrxreset_out_reg;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input rst_in_out_reg_0;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_1;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire gtrxreset_out_i_2_n_0;
  wire gtrxreset_out_reg;
  wire gtwiz_reset_rx_any;
  wire gtwiz_reset_rx_any_sync;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire plllock_rx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  wire rst_in_out_reg_1;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire rxprogdivreset_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gtrxreset_out_i_1
       (.I0(Q[1]),
        .I1(gtrxreset_out_i_2_n_0),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtrxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gtrxreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h1)) 
    gtrxreset_out_i_2
       (.I0(gtwiz_reset_rx_any_sync),
        .I1(Q[2]),
        .O(gtrxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_rx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_rx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  LUT3 #(
    .INIT(8'hFE)) 
    rst_in_meta_i_1
       (.I0(rst_in_out_reg_0),
        .I1(gtwiz_reset_rx_datapath_in),
        .I2(rst_in_out_reg_1),
        .O(gtwiz_reset_rx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_rx_any),
        .Q(gtwiz_reset_rx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hFFFBFFFF00120012)) 
    rxprogdivreset_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_rx_any_sync),
        .I4(rxprogdivreset_out_reg),
        .I5(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_0 ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12
   (in0,
    drpclk_in,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_0;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire in0;
  wire rst_in0_0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__0
       (.I0(gtwiz_reset_rx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(rst_in0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14
   (gtwiz_reset_tx_any_sync,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    \FSM_sequential_sm_reset_tx_reg[1]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    drpclk_in,
    gtwiz_reset_tx_datapath_in,
    rst_in_out_reg_0,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ,
    plllock_tx_sync,
    gttxreset_out_reg,
    \gen_gtwizard_gthe3.gttxreset_int ,
    txuserrdy_out_reg,
    gtwiz_reset_userclk_tx_active_sync,
    \gen_gtwizard_gthe3.txuserrdy_int );
  output gtwiz_reset_tx_any_sync;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  output \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_tx_reg[0] ;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in_out_reg_0;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  input plllock_tx_sync;
  input gttxreset_out_reg;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input txuserrdy_out_reg;
  input gtwiz_reset_userclk_tx_active_sync;
  input \gen_gtwizard_gthe3.txuserrdy_int ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire gttxreset_out_i_2_n_0;
  wire gttxreset_out_reg;
  wire gtwiz_reset_tx_any;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire plllock_tx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire txuserrdy_out_i_2_n_0;
  wire txuserrdy_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gttxreset_out_i_1
       (.I0(Q[1]),
        .I1(gttxreset_out_i_2_n_0),
        .I2(plllock_tx_sync),
        .I3(Q[0]),
        .I4(gttxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gttxreset_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    gttxreset_out_i_2
       (.I0(gtwiz_reset_tx_any_sync),
        .I1(Q[2]),
        .O(gttxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_tx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_tx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__1
       (.I0(gtwiz_reset_tx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(gtwiz_reset_tx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_any),
        .Q(gtwiz_reset_tx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hDD55DD5588008C00)) 
    txuserrdy_out_i_1
       (.I0(txuserrdy_out_i_2_n_0),
        .I1(txuserrdy_out_reg),
        .I2(Q[0]),
        .I3(gtwiz_reset_userclk_tx_active_sync),
        .I4(gtwiz_reset_tx_any_sync),
        .I5(\gen_gtwizard_gthe3.txuserrdy_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0110)) 
    txuserrdy_out_i_2
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_any_sync),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(txuserrdy_out_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15
   (in0,
    drpclk_in,
    gtwiz_reset_tx_datapath_in);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    drpclk_in,
    rst_in0);
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  input [0:0]drpclk_in;
  input rst_in0;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire rst_in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0),
        .Q(\gen_gtwizard_gthe3.txprogdivreset_int ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0),
        .Q(rst_in_sync3));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 149488)
`pragma protect data_block
S4JhB5ELU6lfvi/+qlsPjJzG+JjztoaiKcaGIz50TrD6A1nRJasbeccGdXGutpHMLe7Om3J0ABmx
AavGbzPY37V6PtjnPM++STJHTc1wRGYdZY6JVlXAws6PQqswgNjfvrLY1EkJf/LygiW57vdqnnVb
/c/s3vc7P8YnhfOqslEAVbapGWdJ4naH8heVjM3CPEe4GV9ZZfknDXIwUYCoeupiEzigN0lMbUyN
x1fqNisk3JoVMH2bqr62xE6axXqIG0gRMqpOhaJ6m1tZ4s0Dq3EUoOjonIRfksN+YB+S1hd89LuX
iCMzq42tRvR9Rud26x2ls9TaTYTuU5nq59AOCL9M5tEoPwjJ+bKiVqKX6e4K3wxPVNmg53+dzB1s
5Trgw9K2cXu0DGuVmvMdfTu2Av2Yp4i1pyYT/dzMdHJbRjqA1pfhE3Khp24yn30eP15+3E5LGiWu
V3HEfitsn0HN6Oqma1q7LU8pJ0gExzdfwcbgnZkz5cPuM3gDuLQQSf7fWRdrkABXaG3VSu543fwj
9KnE4LAEEQ4Q5zzRaZxY4Rx2VLV3+wzJphN60YqXDPdnXHt9pnQMEYK1fYMqSVMWLciT2iIXDC2e
m8NiYHCK3FcCht9wkZwR4BehP+KD55pmtH44clVKidsfqJ64pxOa7IHmaiR3XiLusNv7O62gIXwO
ppg4WNJR6/u9CNdmI50eXcoPDPyO5+A1dftH1VmE5g6YU5noZHB38O1cAFXG0I1IJxKUD9NRjq7T
euzD5ErsRqjzU6vQWSqRFXxBlg6sFoCXl/4YYDo905iY+ftI3FYDVlRWiG8ZG438Pi2SXlQOOFfV
i6slFsnEfdjkt2u7do3zTefGTyZkEgTb3o5NLBO8y7KlS7iFnb3F+gjYGJtn3KMPbUsYD5C3m+0j
NURphDrEL4+HPvLiYk3W0fKl1o1YRLWVELfIwtcmIKTDs/pgWz9dfEzCSpV0aa6Xadg9VPKCzyEW
0Omhg52wPYF94yC8frhtflKcNzhBZnx2OAwosYWr+rE1AC0HmTYekK8MsApu7CfoN/Ii8geF1dqd
Cryxe1E/2cPDlvvwkCa3bvG8AEQg/kdfZhqCtsCJ/xpvJ1U5j/8/DH0jWqpkud34fVVlewiQZWAF
p9chI7rs7OcHsXgYFGatCRupokMQtnneXfMPG9osw45Eo18tKbLJV+sB4ZI1uSqiqXyYXIzS7Ktd
RbAwxX3cc6/exoq6zeOibJn7ZtVWVHMLX54ZELGquxsYlDcBAjt8SYxKtV0wgzntHGJEAQT1FJk5
AbCxm0CW8KHioqqt1zrrkERGa4ERKxqUjx3qTh6M9VAEI4dlEX10hCNhLOogDAQuKClFbca33NUF
OicDvHcGyN4gupUnqIoxE+XZBHEXxZExZEeLx6ulEmUXJsHhq54kaqB7Gj2+ThrJGlxhwCDS2qcR
ZmohqFV0L5hbQowqw1WuA83hIKOn8sm9tdWwtPRQIkL2+uiyCATaIHQftXPfBulR3n8P9OLBq6Un
DIpHWeMiG2g9gPovhJ64uG6GdqZqZCHnBVxZ5MezYcg6PpbNS+H+jwBCVQsiPd5ve7aXhaG7QB9a
HWwIsHrx6sawzPPr97UtXY0Kj8fup64GJyljjd6adXf9QFBtRPnR4YoQUPceSC9V+AmvsvDzAHhd
Szw/XHpMpV6eGBx4xeaALKK4ubGZtQNNSE8p9lM1NDgxPFY5HGGQw5k2FR1v9CHTk3agjtlov7Jt
8RS2XBxm44F146lYh4gGfIZPmwTnDDCDqfZeAlc86CJ2WZc4Ye7sztHXVcApUqLtdFd7KTgiDRtP
yTEJW2KwxKDjiHRpfboZ71vANEnSXtWs/CKrwJEwS29wFMmlfxTU5w/BehKEXfoUpfKXdJEfGv/v
KKYgWxb/V5T4sYw/nRX6tI3YOQYcMOFixC5X8o4Olewp1vtGyImcCpyod94GMshH2zuXkjnB6EQC
JeWjGFOvRY795hhXqpDAwGWoeNBvUgQbyKB/PdOZkLX6o0Mw2YUcOB2xTKTfP9fJZdBqcQvugRak
0WGrDzTM8y8f86PHLh+Koabw6sMp5r3kIstublNOKWZ9jiKfQ2NwYzEWqie8AJiwdz4ojMDbJMgJ
JzMpd+gnKja1OEOBLJ5NPRZDIFzJUj3PGAFzxi0zJS2uRO3/KaXixtnkWS9pACmJ2eT0B6C1gmqW
OH2aTVki/feZpEDbP75cyvmbsXznI2zvVB6Godj3LYKjpVIcr39rYeA76EkAuWMUbu3dLZUIjrly
BG+06UvDtgH2+uCfhiUIvWTDKQm0cTJIYhpORK68VwgCJUeY0wr/oOu17j6Kdcfh/w7OogNPuaGz
qmfHUtX0KeImyueIN/4q4ecWVY0j38qf7afzK5tN3gXyYxlsIB2KVBAUu5Xvnn4LPdM71t6CfSth
MrB9aTSATyErGPZmR9HwC51VKKrFcNdp0Ry7RxTp8aGATjkNZbKjOe/VO6joCLtU/h9Mslz9vXdb
olo3wB8WxY5sgeK64mppB7jrFdwGrpVub+tMwRcnfz85PicHNsfytm+AhDbi4ChoXUKZ9q55TgaH
KhiUeGs6LyarmDPL526zVlU1kHKF8MuwLRvOg/65TP3jh8RA0fir2tjOGrN2GoguFGvGYC8uOFEU
lpTJWq8deNURKC1MnRoOqr6Xi+KRg9KhwM0V8i6F/gpbnJ1cDW/0xyNZz72/2WtWO8D02avVY8xQ
59NfijxIwzkNBqq+dp4FgxYrKkK+JFp269G0QooUUNeWOS9s5IHshIH1MLcJgoeFXN02xCXzySfT
nPHSlDAfIzxd/db+YAq+yQLbkcwYsZ1uofyFwzk6EI3VagqGjLksIQ4HWaETebmW5GuxXB9H7bmG
AKNkYP/V5+3Gs7fBWQoDPO9NHzueJk9Q3r/7O/9C8mZ3M+Ue/uz3j7DoPCKJXx02WEQDKToV58em
cW1yC1ujg1CTtjwMKmFkPVelmJc3MtnfnAdnFRr7xCNgfwlxrwlv35wWyasb9hN55i80lMWL0/Ce
5jMqPi8DnHmQEe6jPgNIwKXguaVFafaoDR2RisCzqMrcxtlaNCGa4xXSm/IkSDGbCdPKPrqPMonw
J/1at8BBDNfHktw2Pcaa5KW/qr5wN2zi+fG6fknaKVTlbTuNKc/ZePyJR2h0C7p/eyZ7PEscGxWD
ZC6QIGoDJUg3sNCderoO4ZYvEbMPOvIyQixF60qa+MFWzAJaaP1N0KB2mKQ80ARb3rWRLEIklwx8
4Y18uWciproiEJO75lUsBF5t3gaYXsaxgA4OYSPFua/j5CVWbHc3cDiyids8AIr0tWlE8yv30tL5
hKACHxM9ytbpbaMf4rFPQlS7MylQI06+0UfEvwnXTHCXcBhuX9B8itJ/ct8fzHeiUKitSv++vXQS
5qciyNRHukUXNsQLgN4wlExS+LYgcAZ1bvXzzM7OISjOz4zSnc9/xiRFQjYpFRKJr8MmugAz4H+i
qzhI0JW/151S1Za4t4axPpnE7dLEmnenPFEyBAAYpJrLgphVJopeUBx+oBFh9d+SwXFptbUyDiC7
tRsT3YFK/q6UzvywzLhCNHWXljQO2tLjEEo2YZZJRZKViGKchkvhIjhzjWdmw6eBWQ4CTdu0oeC5
1AvHcnQD3f/sL5k+cqK9bauqA3O433+mWPJ28fsg6bjeCv+CX+J/ZARWTL/vOdcxCgJYESbyHdUf
PPzqLA1c/VudceT/4vSM6EXe7lr3Vr0KjyVk2o5JgttRVSpfKUol8MCuQVokC/UGe+ZDXepb7u6o
EdDO/apyhrgrliVgDnYzFsYQPtmHFi4OCWkKBRDHaEa0WsXwPXPrl4iKKtVCsxzxW8jutCZ7xwCD
bFsKDXcfKKN/qfraLJ6GaYqU7eCGwuLn+2Q6OPN1XwiFmt0d1rWoczJrrJjTuZqgdBUR01ezpQEs
HIiBHXk1QpoWPW0IzaOXuYZNjHo32aDhMJ5A/7/EsgmUOU2UOIFzKJT8F4jkZMHvZ7SPhIigdd67
/bJzChrNIAw5okfoFrvGebTs9DQ/0y3auhVFmY9GeRJIhbqyZz+UhOqVmI6z7WCX1szYAQwYIWVk
XoYCvAgzT+14lHxsoFM3PUivB4Eg7XtEMKj3yaFe5tIj+HMvzZCcltevA/k8umAuF76M17XqWkeY
yIG8y27CYkrViWbtKmI6pGCAOR6xZTazd2CSdVx7y3WiPw9J+HaNu2eWqOZSCc5rcK+a/IOkqrD5
Vd6J8dnnyuxDH6/B2EDpm2DZQWBrgiTCR5mxDaR6vhikBNN2d+Ke61qv2Y1Hpendtw9z7y82x3g5
0IDfj9Q1W24uELxdMAk3hUargksHrzHdKZSCsTPX+2pgUp37qysq/MaWBKr2IDYV/OeW3AujYyKD
bAKwhu2QGWO6YcQj6uVN8jLvZzGJifhGrdU4dqocoeZXr5V5AyURTezAz04Xb/GjdjGzPpxdw9zO
T8nzhuTMMM7myRL/NBKELIAEMosgBwNHHivZU3s5POi3GYE9oVLBM91ghZ2fU5RWL7knL9/B3ZFT
p1FDpKuBZyqlaslTss7EDWfarUoaxLtYJ9M+fS/gT85452tXHmkSM5PzSOMm/u7H/MqTF3IB77T8
mkPffHKRCoEJekqumqgUMNu94gPVpw4vLkGe98H5rk0wsh5hNq2irbKj3QVK4XQD0sPQf2LHGWEp
pS/UHZ+H/ZNP4KJJMFwqzAxCOYlfmms7u4uqfKuTRvGJ9jqeCbVuwYlMBsZKLUztP9XK7SgWSrkD
nkuWaeYTxTrpGJ7TmDL+PEIOOeYoMReteRNCa3Db0lZ1AR3VDVcUh2InkjLJPfNFCDqhDTQ7HTCM
ngwhxiHbXqxXtc++5XKG9xKqsDcHvCIE3ks761cqxeL+clsI6CzlMnuabs7aYLPYo3nT6TFQNkTR
MKC91tgzyl0PUguJB/hzN5RanyiFwAXoifVh0ZCwpHpwodFYmHCYHn9cIrLl/Ke4k6VFwrytlcq/
XciIkKLYMWM+KGNE//rvA1zVUqNOTiKINP3yNxfJikfR7RSeTd2YjaWEbWo8bpj5slgV39HHReOm
dETvl8h3sMcp6+XYB1mmU1GlUVDnRKuksII2pSxxdL5irBfuhg0Jiqr18pt/+Xs92fGU2XIAF8qe
BJrgJusISLqch2wQou/xI6fTbdwIo0ovivpZvYH3i/1pUO6VjTvAUJbDvVNARm0GDIbFm3swTn6n
h8sDh8CeIsUkxGC5Y4g2vZ1KezV9GKRhfj739rHEbGHfmZ9yqsSg8K91Bs9NCCs4gGkrZBgwPwM9
CA5aSEv61XupN1Qt+BCck2OyRAMbQwdPa5K/BJYespzgNp7i9D10gJrDW7FlcSvJXKT9sqYABWlW
9ZK4dWcDuds1g2KRkVWHI1KhjawyKF6YBjA1wP+0GTvWzP+GYFn0itts8t+XXgr/Iw7r7Eq4czMq
w5vdjJ34pxyKJ5vhwt1vDocmjpU3DhYpM1xqPIPkopUbchL8NtOfDmNpRqioNfOEW2hiawJtxf4T
8FCcJPceqSmyBh7JTZ52OV90TZmAxOSnjxjfmDVXPod/oXVIn/+6+IsT3WMskniNJyIgLPn0XqEN
sINC0rCQEseTrFUTYLJNR+gXsfVDblNnXOL/oVHWXOq0oveONnVMCRdUlmbcx5rkh+ErAmfln149
RlCGrWN3D6ZgGpWA0HDh5xcfL3MF8amwFtVK+6sM1bRk9KfMKBBw617nUZTmBL4vM6QdWhoapd5C
2jlGxcIQiiSflNbs4aT7JyzJs3q9e5se4OpARRZmbGL+8dmSbxYn+sKI09RfQd4MKli5h5wX8AmH
jf8hpz4fApRh4c7Sg822LaVv1z4n3ZHKNGBTF0Z7yyXELsERrTSgZD+aPQOnEcG6yZfoZfNVAruG
pa38CsIw4AlGyqfLSKYIjlgTTKCzzZ2QGWEYzmPBF5ESQWN0i76Rj+AhZdc7PKnD4r89j+oXB5KK
4cQNOGlBlPdJIJnuRhaEkNoxTMh0uyX5vm9mlIjdZiuNZiHmiwvvF/p2Mnop8bVcNAfNyZ7T8cJy
4b9ZSwslrjlGy976VV5hzdCLkozS2C1QHWSI9csWe7E9FRd0VQlnGNYjGBdsolXO+gV7s9Y15XAe
twmYuthYaAWqG6236MVxsOiWfHVM5sgnZAhVyct2z1tpCyOVscsY5Z/8LRhJXrZp9GfPyPUGO0Jn
LuSd05uTPBP7Z+pBEOvTyzlE3YXvBKk7V+aS252hjWIChxFnZAKhDLtGhHQWV1Hbu2jNKONRTzGK
peMuMgvR656ZL6cbadeBgy3kbj1QebfRK6NUuDcKwUPvzJwsRX0tVWa2Jy4Kg9zRZb5nI4/b5xbI
7CvpVcW4/XEfajxblRK+qc8e2/wB2YMMLsyoUu4vUrRflYjlb2gvZ2Z7tg9HBX+DZKq2xtjzxac4
6yAe3ICWwzsxqk8ylSmv/4o+ueHP7iLpZ9rIGs1FvqkA4YA2NQdk3WuxEwV3xQ0wFN8BUcDPds6o
V5VwDqM3q8mQQvVVKnBXGQSIg7VhwdkQGMUm3lSWtjoc4d3hwJb36TaRBjlnJFFp+YdkWuyQEF/N
1LuiCasxKXJa69a/YTh7yKmBdaudxhvClwG+za+95EEofk2+s/47e93Ki4TE62jb46pYNZdFYmNy
ZRLExt3uJC0fJO0TIC89JaEEMwXSlWOyeo33W/9cPmZ/0o87qZNviWh2dvG1GCiGfIfW/n0ZlB+l
Aj5wPRsayoUOn0zReSq4eBC0k7wo3lic7OhAl92vRp2psIuUJobA8S+11mesOe82eOhT508vLxiq
kv9EcNNPaGUCDJhF1PzwHrCmELf9oRBaYiYr9V92nudd5h/REUvC4T/SANnxeez81X0jcCPdGdHn
wSlN5YvfVSeUw+9ziPRCR6GVSUschBV/bhrukxZIWBLJBZ2BDCipjDNVzC+SNXOP/a3Tax1Wo+d6
4WPhEqwjsN33KvdZziCIlfq2qwie3e2247KfEpSLaLKvJ0l0ps4KWpNMUz7AlfqORTghfogD0TtL
FgNY5rOPybsiVF6G66tkv+DRgPvLR0C3uqqi1M8JrRlTeAPdwYwTJLuTBNIfrkWe0vj1vxE7BQSW
TifE6NAkzxcnq117jmiM6ls4f9ZGOdaQ/m3l1kQopSNwOg2duXB81wWf3P/mYihmezrJ7GnY9YAA
kN9Pqg7YRfyi8VtfmjpapYBu33nwsr+F7XkAWkYtdgEsIOeFCKHF1bEuXrv7hxMhIjTQaXdsw6yw
HSQvHK14odyxqMoMCn6L0pStT+Nm49PV9m3yR/3Kg7Z6rOrzvIijoXH50BME28HSwjs2nhFbwW9W
2BgdNdbQ9IVLbKDltzseHEkOHSzTTW3eHtLNtyH/wY8fivi4XKuYNULYjjRtK5QvCCH6l4MUDo5a
483Suqw4WKfiijcbQ2Dvaorksq07iFj6vjgQddzc6omiSm2mclnT1poAlH/9Y1H8Zzkfzmn0DrAI
g7R9arBY5aayavfgwIzMqn4vyqyFauP4dtgMIlL8OiAQhi5aW12Z/8xBG/La+91G10bLqaivrALU
5b2qIJCkwXE/G64fi5kx6ib1e/MUWVRC/P/yxdN1VdkzBGeShzYUuskmsHNmPQhjJZsJOM2Kcb/v
qnw2RTDfrR5On6sOQTgKt6J3qyiwzeaZgRkJX9lKKMno1Rr3VBX2FKasLi61csrbF27/PdQzluIx
w+Svyg0W/TI6T9dL1QhizRs7kbVlJaZ/8P5zP8DNHxmNjzSNpBPlFp+T5W/xm2iV4q/rzLuAS3zO
oMal1I5Y9zB0EpGguzygDCuOIf17DSWRMZC4o9pcgpKaO+/8VwBTYuyz1x8UkJNxK5i7eZHPSkkR
FgJFdmG3uK9oWdC8YyO114M2RIJHiwcD7RLXNhZAvW3pFU1HPg2IXB0UEsXxtzIrEw8/PtGjrTNz
XTE/KaFOVGw3mkwS9sESps2DU/tgNVr4VuunKQ2MmzBYQcy+Lg8JSInlr2CwdQczv+ZlG39m4YGa
tAm5K/Dd2mTmV8Wdb/q2HD9KJPAcImFoc/WWG5Z9mRKOquw08gYEZbZRFvVgMfRI3HIT1SRTm7ut
kUO4HVwWQJ5HOOn2k68FT9inCBOF4RCOARS41XWQPLarV7mx/UZWLw0Hm+0eRkrFE7V2EJjJvWk8
urVN0mlx5r5AwdYOtvfHSNjT6DCXQa0Jds2EFkAFYrbRMQ7TQXJBUupbQq80h187koU0KoI+0V6n
eRDIkbGkw1Jr2oNSLBJq1EZxh5QHz5hSuhnKYzWfgqKX8jvuOPxjLlh+Mf3S14PIv11xP768tyoY
nU6JNbKfR114cEDnywkDExsTsvQq/KLuPvC4rRoTCxKnAsql5MECiMRugjiwOmdi51ne2a13uTf0
gyJBOKFmY4mV3w47AJnAlkAEultlRUGj9uCbpwQr0/ZowQmW8SH3fuTlNADqPwbW0KYLODDaM16R
9U2LEAGWrk9hD2GdrZVwG/mayG4XqO/JXOGhxIYzsGh06UHqeYckRCiqI9E/8iZJOFYuWl5wYd/E
Ugm7n9i5Inn7cDNP0rWdEWd2w2tWzQwaEi94xbQovZn62A+CfyKlL/gQlJV+zc6bu1m4a/x/9mMS
/K1pSQwNpY1PJ8e8cJqNaib/HuoYuAMx0Y9M89oGe7ZXUscMdIoIqPMGnVN3VThPJ/iLklyeTh2r
80XagWNIHn6Np7qn6edPXQVUplnSMbppwx7jBs5mJaVqikwFyQNpO1VOF/IdFlU8P+wjWQznouyU
cSpJ865s3UEX6rw23Nmqipmh9FqDYpKmpbUjWIGpbppaK3bH6nPpYh01dXXgLid6K/abhAAORPjV
jZl9/xViggu42TKQYpD9AXXVh8nPhSyPRh2CTWIHwFr7pqg02By01BJFD2w6HqsXC3pfIJqih3PJ
d6JXJF5LDO9nUl34XIYBCJeKcgE8umD132TDUrDIDc5jyMLvzv+QXun8yuMjEBVfU0Wq9wJ+0HFy
5+4d3/yp72V+XpMbks56SrLmQ6IsnLR8VS7Zc/VbHTjhxAo/VJHHbwAIaGmhtxtInJ1t75D57inC
ehdRJcTo1djRP6vPW1LLzcubmZgvqQqq0JHtAK3enyQORC4b2ZzKlL+nzuWRSXioOigXOsa2IjqN
7wXPIaiD4E4mY0G1MN+oD7XNHTX4VKPA4ipnmzLoi1GmNHFfNygorZMo8NuwDUT+of9hXBM8HfqP
BjoH5gIIZ71dsh4rhzlGOi4mp1/MNJo+TjDJkskacvxC91uLNQPnUPkVFCbI/KnrLtrv5OlA/baC
7Go6Vg5aiVTSgeq4HzhZ8KuK1Zdb/bc6ZJHXdw+Qkt/dXykmZRwX0X6zYtOJpmihcELpmbtdhsVU
P0TAG+FJHeD0vACp6Bdy42wcr0n9AgrWtqpHL3GyTTfcxlukthmECl9G90aWdfl1Bhqcemz6+g9N
ON9Yq88/puW2XqLpafWOY4WwG2Wpk69GmgtW5T+X+eJ16XhhK6KpVXZY/GfGF4FGQnYwlDk/FpI3
+CQ1t1mui9I85fBeMdn2k4yzkahnfBPXhwYLqlSJPUOpVwf5qvqPQ3wlGyxDEV/nJens5G/IQxZw
qKB0Z4r9W5l9hnFzx2OBsMei+u6Kubeijhzpt+AZTIdL+O+HJeTzFMcjJi5uY4U+a5sERz9dgH7r
rTu7eIo2OuGwIuqJzc8S4Lt99UGe4/LATm5swIFV2QWO78CltCc+tY9X4shzaujdGB7gGKWAzS15
ix2tYipJ55oBjfBE3Q2QHxEEzyDgg9H8qXb+rT1P41c4SI82hqa2+nIRwbuLPFwYnAdLCcS+5MNX
Pv5w+pFR3vUdISrze07wueU3PGvr4Zn6VCsgR9WZ0tBDTjUouC+B8jCZJlFVEypqzxMBLvb1xAKr
DqQTS6zj9AhUHVTXIMPKWhQvcCADxeqeW48aqLlTN5bSIvulxxfNP2JiBtDTL5kYOPHZZT4itPDC
4Ignj1cDp4rPvxgCY+7rtR/Yjw7/KSTk/h/C4bXPPR8Ad/5Kq9DZ+j7zDceEIi3i+OWXAa8Axgov
3fpmFDwVFb2nnwtGcr7E2NNYacxob5sDY6V/iWIKo0khpkYKb8EsLUWMkxZI/NcYYvDKAT2Pj862
uVq4gwtoodfz2kg2rEQ+k1B/tZEOh2CdhDErNMPY3fEMsKr2SxqxuG8K1GJTiQFFfvBdmfbYtWvy
V+5kZO0H24qyoELTsyFhHu2oB4tzhMpTm0UXL4DYS7RevobFAQuLFMfFQSi1qEwlmWRzOWIyqnww
yxl1QSLtwIMDANUkM3+UTiWfxHvj73vDNNtXgrt6uv1XL4zVn/24Z0JiKFlcSVEIXsh+y3KEeVNB
SyS2qqTKaspVYCILtcm+v9t52K5epyYbb0CUqQ3jUXDEjI/zYte6LZzqBvtZiNcqa4yE82Np/OEs
/n/GkDzgOZVapvW5UKaThj8Hm9lGDu5hcDmTMWenIq0y4FpAtr4+2cvodBXF78tjLcrWwdqbdjtb
pwG8h9nDiUTUIHK+QtSI5hY4W/1t6a2YnwjSjHXk/oQtp9WiXxqznF7szlG5feGLR4+ZNzOx0d9i
y0Gvt86Nzax8DqV7cjokEz55BfEAEGTDZ6bTyF+gBhYddDSJULd+/wmtCp8ANceLE+m3OXvuMejF
Sz5P3oCX4TNDmiIbMrqb8cIiQygJ+WB4Pe/KjXnvW8oOXT3HCEnYahSUYm9ulCILlu9d7UAvHlTT
7LuQ2TukfJzB7Rbr0T3L4thYS6uH0MVf6Bt554nLtha9bqSkOobk8Q34ZKAXzVxallvjGi0Q9tIT
IyCTaLq4JewEmCpm7SzHXdBxBnsPMC+9aMzNLUWWUY5GTBqMS7ZZzrlAc60gOVI3XojOV2o0ECoB
S4sq4amb5FWvibCWn1iHH3hOLT56ixny7fFKdKeoF0x2e5X4L/GXxVrdoYntBwGeQJ0AXUEnkxFV
eQjtvchbFx3FMFgKadzoKNbm6LwR4lgyM2e5YpN6h+uueM8T5OMLHHtNF90YWh/Z/L1xjb7+t3Zr
jiAQ4t6aePlGMLgicNvKydU32V2PpONGwMrKJw7Azweu2MVjVNdMg8ORGGXLrnNAq5ZBOrukfMzW
CQp+79hQOykiWp0mZdwk+V9L8/ELe3PlhFJ1uxbaqTHckvfL7r/EZBTIfaH5JGCxY10gwncXf02F
tahGQh6q8DPOk75epKFwVvtPe0cS+Lwf3BxXkfKzW5hCGGazv0YAcPfDzYRXUW27EHrWncDrzLsw
5SdWJRb7lUyQEnSmYtFB/M+cWSznaPENinYuvrZmztyYd31842szTenZRt0sOYeQgnN84C4Y+f6W
9e7VZIad0UknQjp/Md6AOOUtGZdm2UJYkum8XG0ugWgIlGKksUjkTrLr0kMexCnQlsS6Glt2rOhu
9l57BIXr5qTPz48nB59WUddEAE4TUL/gcEb01FF+RktWVZB9oaiwjMgzhTTYU91NftIv43rH27Qh
3ATDFOIG7PdSxfT3N4f8TBA8P8QqHReyWJrCisIE7zyltdekoMkfaybodTb4vVKIvhVvFvQ908J0
Xg7WlT8XtU+VUXuaY/szwWia+sG6IeHb7q/UNkT/N35NzCaCfDfX365tFfHndR1S96HXTJsjd9M4
mW6wiRCVMCh9bTe9qCRdlwJsvZzcGw/8gVmHOkQTfLvQbEMAeSa/yVdkIhIgCKqxY/CEk7Y+zp9+
P3tiDhBw0RhOj0x9uQvT7OFFckbRJv2nOxeqqRbjBbMfO6JMyh5Xb2JShpWBIz4sO6zgDZY52ZYD
TMOlz8tqxtKr+UthVKPRQx7cHS1g0cB4LnH5FaM//ZFSigBpQhctcIWbLz9AmNBETlHg6OwbcJO2
KyUfblUW1mGAeypi7w/+x14bTyeVZ37hY1Y14QjE2AjjhzAw7hCI2UIqCcXWEKLpFlsY8sLCgYrO
Iqj0nUtax5s4p/854reGZdrufgTRAzHlmd+j7einxfRRgxqb2eK1cXgEq2h4ngWhUsXyKEY0WGca
1kyDOOjkysSFVwPbMFKZdzBLbZsZpJMShHivQD8mLOWGIaV7eGzdCYZHPb21HWSeoBfxRCt3lhq9
YSi7Wu1y3ax5aNSroScdzR+/Tiwylfyjwf/FqBWoYSr37JsA/42UUK7bFCSBcu6tOauoAsY0Sgs7
L283wbZlfV1/zZLSOgAPpZBtXlulm/ETfZUkDMRCluI8FLOneKipdr7FKW8JrX8Fjd+HXOPGTzxu
zdS7VlIw/Ew1EWbrpsqwRxpoM8X5K2yjG01ZqJFrJBoG97znCzhuiFqd3SLexrv8ACSwQTFUTsKR
MUIsO2L4RzyXjroB7OpNYZPE8UrwfQ6S3TehmX+ibp7B8uUCm67iWHXx1d/8bSQfw6CdM9QBxnCn
JIJQK/Tml5jfMHYkUp538mIW96aaZD9R0OY3blIbdRPoko9ma4yzJtP3tcK6lHg5oscm+BeD4l7x
umJNmet2PeFH7484djfoA5BgJ6tF2c2AQqYbF31Diy8czPiFZgItMxOQYq+v5yJ4rcRIrgKTq6Wn
Qp/FC7KAPa5no7TtTLlx2Ltn16bULNp8cbJSvIrEy+qhK6lMiD5hQz5JkXLwN9UwIF/qYNNCRhBo
ynlZO3bimXM/EvenUVwQKn84KWhKvbQNXcIYM6GNIGeSXIY4wFKcZqwz8LTiRHBywIKBnmzEYrnj
xLhrqu4SG38JHi3HFGTyMV/hPU4t1grNKOI5BPltoi294PSfLv8v9RBkQPwUO1BwqO3G8/3GjBmz
JZl8BF5Km0Zd3yl/Y3Uto7m+o7WMJfZv1ShOKxdvBKiAQ3P77Eg00457Wt9IYgtimv/6BpT+XMvE
RTx0nnl29qymSGjfNLg5YMnmW/hhxWHASq+jI1MPrfrVOxmtRtt5JqDu1aH2T3FoSm5dvZz3Na58
h3il+FW6DGeJdYOpNN+Uz8qLqP8BKeImrw/5yhlUlCdZc7yr1tUw2t7+V501hy47J0dNfpv4K5Ya
YFfpURqFGXCF9hmbaEnwaEKcHA/3uvUrAj1cS+1e+kSrdAbrH2Ruq3gv7+uuE6VNc8SyNcCejQV5
XmggQ87wN52jbexOfRc2qoOJXSkIuiTUU32SHTWGH046Y0QXq/2dqjzP5WMyHn/KpMcxtc0i2W/K
C4jIJWhSqvlZnNrXNMhr1WLhmFr5akqgg4aRmZEi7f6/7X62OPXtyaa3niLFhuNza7vYt+emZ65c
Iv8sZI2CsF7OiaurAZF1jYU9xrZwiAv+i7/Ye0TlEJG+zYO2S+EmFIE5TTqHAxpl6XFjWbF8vE8g
xLsT1tKHcJDCsnn90uyDME0h2eJAsh2tDDiMMWL2zcZmg34WCCukh1XduRaKutjg0vlMANBHdjMl
cYV3wRqNbwdrTIPkjGbicRFCMfRS8t1LMPxfbLLDDp+K6a6dGQvUV+cVJslHGQjxEzed42mRaVku
KwVApKV+Bqaj+62KrfMOANXUwYBayAdQXPjwPUCUtgAaDfEfsie34d7azStpeDHdQsOQhhYgV4Iy
S0wsVbJpwQaCdhPtjNjMp4bCX7HfjlbDJyth3OO3WtVeDzBoEFhLJs1AzWDGWiBf0H4JvYjkhvjX
ZX80PjsmJkYrQFdku0e5E5aRwBk74sf6rGsJsHxnLFkACR85ljTx8kfrIN7a6GvcHgV3aLmiFn5g
a4rAdZGkaU/PVif0ttBCzSLTwT4l22x7p9MO+jNU49xXTAXEngLoBuHW4Hs52GdtCm98ox9qBrPi
H66VxkixMMBXzq05ANUfSDuuNE8J1g1XRp5BSQlgr/YS2Skh1IlHnXrCFeIdpEGBKfU62Hwn+bG8
UObYCk7WsTIGAFHIEBVjY8wJ32SkbKZhxvZO48a5QKpgN7vlRBMa4DplvJWIrzt4XfHbcCucVFmn
nmj8x1phGGK0Fw4ixwVbXGg/9GbKUuSAb0/As7g7bzBxdkfzreriTIg4onyrE2+C6AZKJ9f73lWI
Vn4ZPTe3kYQvcsKVZfY2ju6d5rMYy1Ku+QLoeYewbcLVG9GhDrZzjTUMrJ7/XuqSACnW8xFLJRO6
pzC2BsyfAPqPQAT08uvER5ecUr/n8TD0ZfDPGUyWPN86llfHciGzh2YFN2mD5qBpQ2Sth8V28EEN
pU9uP6FM1J4FQPyRFqbLWuyfOk5UGS4Ojv5Q1dRlU5NgQ6hfN2DGEja9Us4khqpxKMdSOLKvkzRl
43rDVKAZSx+wBlJ6MqKQoDd3CWGaFKqTfL42UwfOkX2dhDswBJjE72HvCKIS6IIeL/CFSS9roz8o
znkbaIq2Sfh5aOjdb+2a+MvsmTEcRiLiCR3McAiSmgUzf5inkedyx4G7j0nUMHgSsGuvXBWBvHRi
JE3zk+lX6SMkeYa43rxa8UL/O5pNi3tL5vJn9btpEQJQ1sT8JMrX/8lBU5y7FarMFnsDBrZH6cDx
9OzUhLn/BmOg28jRbEgfgF8hHWDKe7cEhrRL6OBvkh3pHkqRcQbmdMjiys/bvWwNJlILaHEGdT1c
RPFgCeLUgiodlZWMomHV98Pw8MsC0QhAc8qtWs8DjAXtyIpm/tHUaO5SrdE+iwueAptJCoiEfcmA
ai0qjLNUGQkMFHyKcWFSy9rdebhoHfh+lXmWySFrPiFx4GjziJLFL0BwC85Z8dYXLWSApvk3NLUP
B2asYWiwUF+XddaCd8RTvJwL1l0raB4HihP+N/CPfr2BG39PX6hitbVd88nBoezYA5/t8TxNMVjj
4kWxfGX3+/mjXu/5lkaSE19TXL4VNSCrfRS5uooRJCVjf24telsL4E2seCr7+xJ71yRaYBUnbJzg
TyxajYEw+jfS2xr4gFOE3QeXITDxCjrDYhsch2a0fs7v/zXP7ndG1sUOPYKeWe2WOcreXP2QDfBt
qeQiMMt+dUcBRqnsiIl8wEHDhw32xR5HEhIUyqrt1FeBGIHWUTxpMvHIT4JOy8rTh6N+BWFZfMX1
yvI3cvw0nsxE2CcRRZNzdmymSBikaIgLAbQxSlc+1GdrOGjITIupkZAHM8O1GLywYArsS8EIsbEJ
1SM89MvvvCUWkcq7ndSCb/GrJYTnNTbqBiT1eAG21bPJdwsudZ0+k2V6o8Ydv+4FROQwCYEPL8PJ
uKXWfASexEPYyvnduGd8Lfu1cCoBxsyP8oVxsibw0IRuMKGvDSO+rqGZlqnTKQbQ8E0GsmKfZD0O
vbsOyZxfaCssOHUZ63x1wPUNJZK796IRxmadsxskyg6jcH6CIOtkqNsROMwEwhDgUrXYOJKHbiek
qAANq8SHM5QzdquHiLQQxFC1bWZSkUI7t9evT+1K6iqZXtie+e2XAAEMIS51GrqfTOnIxGNfRDWi
i4AFYlAjPIVrePt0Sk6u2+3i07qtDoWisg1+XssGZ0U1/Ut1EBTYuOt8/iabjfv4ODDj7/V0110F
bFIA2UD0KY5cgkaaakBJjMkhrGmp1adC1RqAMPz2GGIHSCLUPxfwmMpAv05a8Vvk33f1kSuM9OU2
nTRrQDFeLDUMS2AI1Xc2rwD72xGfPqjfse+q39URepFSWd1gzJ3PGMIZ13fEuzLLykxVbEeMywQM
d3E29fJrifsgo8vBab6m7Nz1KDNPjCgNm5juXvpfAz3u8FIYdXPOsWmHJeTLzcKutklRoLxUEp00
sb3prhqxRG7pqAw3Ej/61moMFGAwaJrb52Sm5CfMm6/v1ikpGYFPqlEg2WsQpEcKpKtasFRsNiz0
l9F1ADY4TW1uWNLNsEHBemMBuDTXcC1Z/aS3QWZSRIav+9Fioke5nsoIWv6jjvWmz17prSoIZymn
3SPrbBPrhKm/i5c5SQrXQDB0/XObhfv8lN8orm0LxvGD5mTpe3li8XhfIUGAN+I+Y9KREFO2j9wq
lseTTPrLF2/SM3kJnhMzGABggMe2ZkzVfB+OUZCFsdY+DcN4kKG7C8DXBhKLVZf43M30QgXTWRfk
gtXaHYgAkyCl9LoSf/E0rDNG+JHFBR2FQq3Gn9TuyncV3SnE+kn8NXcHvNbCyjg5C6rP9BtFmZz2
wUN22PoWBPs6cw8CeNF0vntzXjVgpzwZaayWVrpquw16rQ7Wpfc95M9Hi9OJhFzODgQR8RGuLOie
IonQvr1WO2+SebFPbOMmRdpP8jzbL0rYByKR1q5Jz3y1bs0Ey/I1Dzoc1Cgsu9gI+bFKUdgMJgQ8
KYHzFhLvaw3NwgtxUdi6AyaKStTXW586kVmII0OnhYDyOcOTArL/I0xtlI2+9uCeYaG0gVQk0xoY
UEf0sTbvKg8CMPzbbeQBYvQKLd87+mIon1KFCqAwsmw0DLkHPqt/zp0FJQ/i5cVxXNcL4t4Chqh9
/s+lOwthl3DFujvf1owVIOz/M6sN+i5cCD3aPYG6q1QFK+QzzgWtTU2FgEo1eSITpTRs2Jp+vPEH
YY7GLCl/t2eVyZtrEwF1naFOdS6piRDMpXYd61H/5W7rmouwFQQC3KbiviMaavSHDuV7W7qmlqyf
Uc0mYWWYg4tUONWLK8LEx/2Py0OG/HUH50oXU15M0vTq2PxoAWXunlmK3ri9IkOrrMp0D3cLFBuJ
Ynbs7NDf2j4Y/DWW7F37iCJwzlSIo5WdGAD1aNDBySdVdn7EGB9DDDDoSIm2i7vX7reGvXAG6kck
AZdxOzG3cta929Sh/f7vUUqcvgI24jV3gq48YmGW9eRl+y0g7T56e6kSRD+PZw0YhHLAnazjcZX2
h/AI4/ZIhQs+s5aMKorxC7+08pgCUyi2CJvirH536r23smOZt/lIWMuVz+jkPYYqqLiZEfMWDR1y
Yl6o2fO/KwWvZfaSEICjcg+nnVn707unXs1sEVLVrdf4EGb7khbEfihMMm1IW1ogGabgnF31GF1P
mx6YyjuO6RLDi8jGawGbPdfsfTIEexw2u4vtERr2h0C4H+Tk14W64I1BFo/oCUV8zSvvQE5vXWbm
MwtPmzmQRVvPjovt89/rSjifKWiDOH4yv3TZOKUJ45Dg9T/qPQ5PYictdk39sFJvWdk6dt7Qfbbc
fIp6HzHuSjc9n6j71RrG3z7mPsBNw27sYEV4wcAYeVvO0+NP+dRlfqGeUf69Pwyzk4btLTE/t60y
WZ1MYFIh/ErOpk03Vnq/S2Uue5VX/Ox/N3LoQWYmf7l0r3KWea0SAQxug81WfhQDdvIspZp1uj9s
5r5BDiC16Vy30We03UwN3qKLnQJlBw+Wxa/s6uIN2S6Q2KknVExlKYDgpwnoQ6DvXCdqw3Lb+leI
Zxry/uHVGx2GMJoMbsEfAsGOvJQJ80MItH71SyFsaat8sucg0s1rJJXgyKmHXztNkf9FNDqFH9Nh
/iuHlvutK5ap9hWzCc87rqDz1Jyu/RKiVOn9nbtZuj6p1Vx6/poYiEvwlGcnasWZLySg8ZkL5yp/
zBZ7ZNzGpv5jfKv/yPvTTwuAWr9UqZb7OCGQURMDrDKJwoA2woYcJijhr9QQrVCx0r/zjVProP5I
itqd3TtcSJ79KpRSY3mQzrFfWKys9lxBro2wv2JpDhqrkL38ZlWBqC1VNo0C7rxZWL3b9nNuvghx
HNaUHn9ZjjoczdHogX7pIJEVqNugRoDD/30KAZI0Tls97YPCTTdH80II57J+PnhCG5l1q96NgPY5
0aXNn66DoqEApIujMQtIg+YRnlXL4YmhIIMTuhA7FKLNXhhOnWKxooZJb2pvA/6tu50QE/7VREfG
/EO9BzCFw+HUusLhQpBpgCe/cp+8rndEYg/3UMNPfSlnjvkj/3L6pimjldiP6OxZyhT42VOOA0pb
Z1QUdBYo6G8uLaiQY1ghqmQQMAOzcR0jNLxPuTBs3SMRYK8+cxW+bRmDJCoI/m+kGaN649VkErnL
hu/2gMsX0e8NPzpizhePD423wnl0CFTc0maWZb3UvGVhZaAKPTQ/+KsP7CnVrbpMYXgg2/T3dr6L
VF8GLPKzdcq5U1CBwVXptPT//CMyalEOPwGBqaIT8s/iDo8OGXYGGjwCkC4+ILLdUkpbqMnTv1Ti
Q9nf8WeH8ufb0dF3QEqzeylsJc7KgaSd7RfYnTF99cMRO3KFUPVqUWAR74LlK2pNoqzZpMVAOdCj
RLdI5juQrYV5yGAhl/niZn31EHdlM9sczSvPcQhoG8yPMxfstQ39QYUffg3oC4ll/hTKWQeB8wUL
G5ScXobvqYuxmL0ffTtRlHUYTNVzIUSB8GP1TORPLhqVNfWdP06cIt45DTTK2ibD0gIt6f1E0oRn
NaTHMmWPm+IXRJO7LecfYMtiKI9NOijKc17WC1FtlrpuGbyC7LMffPBouzq7WwiQFQyOlyWH0cCr
YPzLPS2mVKBh+6vVDkAQ70hTNFAoFP9pRN39wpjxxZEyrosvtPdANAKtO817BW8mGQitkLmLLMIc
YexCaI9JWqFiurGBF30HBiyAKmk+i/J7Ogwvz+q2I3SQ49Y6r76o6P7eZZknpLoq/KNyL1xPGeeA
eE9dcglch6XKer8SqRMzJJlOOjRLzzGvmOkD8BNon1102d1oBwf9bwchfim1OKJcRcUyXW405sgz
cadjkFLerDdiwxMqVw0XXZGrFfFPMwror1msqtpChpUmMLvSbvsMdCpfSkEgMNyon2yLdo7sAKIn
RlmoGMOPyo8sREd40y6qKj7Cug3L8DA4xcRfX4I41ovGnf8KabIu8Jmof49+D1oY08P2B5c5A97S
8CNt0qX3yVCHHl6BuBXwo4Gu7v3PgdpQItRqI9YwIpXoObzvA/3xzh0ecpwt++tRhiLXAOuJDAoM
sDD7NgmTuH6Dtfu23YCb6Pcq5+6kpsNITPSyWvxZ1s9JJ3RVc2pAd01M2tPoXHbnl26Ao32NRIV8
6LS84hApoE5e4EiJ0RILVF5avskBTblHvdILUVtuOdFZTztL9Ej7RiYlvpJNyPYC2sXvhkeLCL+m
Z1j1n5izv17ncs9D5huJG092kq4oEDhbmq8v1NpGmQtnjckOkmGzcaN9xInVegJn2LSwhSM1sKzM
mhOFiAFWDEIPRgQ6jKnbezUkUx9XuEc1r2Gm0SlhEGbVCvK0ZYiadBWs6S5fc9OkpXrjj0z3QuVm
p+Xz4uHA4W5U1LRHMNmozvDta5Wcnxzxxneydh7BhY/kCJj52EvnkUHi3fXWC+d1pxEGocHICC6r
Az6moIJSHwKp0fPs3gd7umKsViH9U9f6AqtP5k5BYIhfJYMxJnQt3F9VmCbqdKvAKBYJw64yCTUb
JfBVUZXG/xkSB/57S6xGnZxkXbgg4IlgB6xnUgx70sARLVOBQduNwaweDhSh/Fm8pzrBAtWjp5T0
/y9Hv/goHflNtYCJyLPuRhkA+HbW2AIONsUap/WDly7i6g/Ex74ju1Gk0Imv3wUDZtZPaMnRyzsy
FJiLB3rW76WIygexP0ZSHTyg//oEWnqP385D9NFXvLeg90SKlY3DnTeGluFKW6djgADIFYvyAcI/
jJ7mVN4Qn6CcPnBSR59X5UN6gzWeiRSFzk6ymh7oFsgjad1E5jlw81YL6ALgXRDsQG7WGenoq4Gt
h/MfMlJibndSUvZmdy+niKPcP7liNz1l6MrNoHmvglTimHNJ3Rl54QOJayJ+vN12ZWHl7UgEIHGH
Obum+AMqgmqsdDWGbv2R8fGLBnBWOt2/vTXjJPOJcBTjlMMoubDjt6yQWpucBHmQrTdFYmEwmhoR
YUTVgcl1AGP2uzwb+U4Lu2sQyOQejkYK7UMdBX3DWjbbkT+zqu5/qLMNSp5AxLSpaLf3ZpfHIqZA
XlpMQq7PFbpF5jgCne0fYmS1mfhtp6FqEPbwkS+gnXUZiZydlcdLSzapUEopiPIzu91Fd34/SpGH
MrrLAr3/qDik1dhD+lgFPFmWRO1mCE/to+VWfpLLVwRgR3P/v+6bZsDQUqNbFu7cM2EZCDCfrf5z
YKcj4wV3mByWEmmMd0OR1ft793XySpDd+EqwcEht6C5RS6EEAHejZMblYU4XqE1TeoMUp9maud7E
YdnferXzCC/EFmf4ldl/00uFWdfqhUbA+6MBF1wvzc87l54/9HD9ItMHbsiOdaMkdSBssCLSXh8P
RAiK01aQABw1CSYFu6tXwtau/LhTeOn3cyRDwNIsqRo0DYoOo5i+4rEc8/Towft//x9gko3dOa7B
mX7nY6CCkBIkX/EXks76Ejm+bnlLfZuFfTZztiUQUUmcBjXguSA4vycj4p/X2CNTmVxSshYCTvi5
uN7HNXdy8kyjeR+4U/UszZv05RGW1g3pJNNKYcx1StIarbpfPld6RPrRWSL25hlXVDMyJG9xpPam
kVs5gJMnoQHMUNGDHqLf8mGm0RR7XYNt0GQIB3qxk2Z898pASZUYMNPPcjf8mv7637selVt99iAZ
e0yiRTMZGGU9P5hP4wxG0fMHM0wO10OsfqU6QuG1507BCkGV+MPIg8C0wpnsG8804VgJmHG1aoSe
0xlP/8s8wo/06hfIX+avtbjkItchq25c9FwLKf5Ayr99cjO84nixVWoqj2/ZxtJ+P3PQs7t42wx4
/KoJW+v4Ii/6Q/1gvEPVzxQqoSdB6dhNkqwOi2J1vvY3B1HgYpku6pcPWYnrRRtBMnPNdDEptNZ4
XcZ+cNv/+0GUNAouEd2fLLtXBdZTuw9GK74xAd0vj8xpaastF5PU8RBtZmWAEOHyZ/p7o0RzARTv
s6usvpKlGgqjLo3DXd9jZGzrfhDJ+srVm2pJMKGaGi8U34KQD49HxkvnrzkCQ0f5/SaaLXs1brwQ
knwy2LuJjBXU+/9ILVrcyjR9PwoagcrpzgYzil7VHbbsy/CCu4XQ6VCMxyGr7SOHNokmxHhZ5UlJ
G0HmEhu44grXW/A/5+7nljFJBYznLmzkRxGMc9cLiIn3jDQYdj19BfH5bwsfUlLTWbu7pTGoFoYr
vtQNbfaoHRkt0TSy46YABArZSbqMeNRRshq6QxlTcoqB7BLpagqR4jjE2LLdsJcwKhkE1JJnChZq
iRp1Ai6Uegk/mqrKGvDHyJYe4zPk2kzVHlHn1fBgoqtvU2CVHiua5wZ/MUNGK3qrPXOU7AEL1fnL
cMYU0ZHBPJCUrjkgzB6R2TYAKMbHnHdUBrcyI7tFyKKJp9Rw/uXx/Sers4T10j+RF/4pGEh8F82v
fBEloRjEUEsmK98pOyt24ttuW7ySvyJCaGXF2/fQ5Kcmkiejl03MwziCTQ/e90FnFXni0wo4+b+b
E7UJQFGoqW8F35KA2KW/BAmrO9wIzl+J/mgmNWIuJsN7evdDSsKtQncoVyDgayJRgCKZ4mH7WAjn
X1U01MAPH/dHZ9g0SJfZWztv/ct1GS8h2Nzn8+GToZvH4FqEmUAYjAXOAlMik+8pljIoONvJJt8K
L6FTXwiNui9GyKSGw4Eb9cfsnCSRzOSsruU0AGEjGYosbsk616EvTixmIZLL2thHTqIhMTncX0H0
5/EtBZXVVAJh7dq8kwQmK7y4DIv130k8OwRSwsVxMvLrsq4AsXOn8y9RbfnJ0yWM9j9wBD63PO5l
Se0EEfzx+z0XX8myctXdMIP027tBVELEvjhljrWqrKmJ2LakwX1yvBnOmrkqxRIiiHJBVlH0yDLb
W4QSI5D9n8QREiiZlWfFvG49NV9ULpHdkrR5d67xlX3LROCdC6Yz3WmtqPFv4omqIvXNT/1qjfhd
oZYNxs0iBDTnNHqqev87OjXL7J1Cxtykx7tGUaVSmuNyAC/CYMCPNHS0lU2p13+JZGpk3TPMhIxf
JUQY7vYtym67XOPtjKxX0HKQK5+CDdy4L6XMUYgOARCfa8cJxaTUaoUS/4GGUWLynnKZ7fozsH/6
ErOYyLr2ae2chqlhVsIoNkB2vNoG1uauNj2zQOzg4CocvvuUcHkwNrRJIndYCjJJgkZhIjK4YjXA
P3lvKFvqj5AboQGJGvYbg46k97mR5SpzgnX2cF5S9MeYpfBnzlEAFuPtt44rhejWGXdgQ9XnkxWj
2QnWprZb4YQdD6mw4TsGLDElGpyQ4r8p7C4Kw4y7jSo+eNFu0x1nzTfA9O+BSkGyN2Rp9aaJphRu
7OAAX9SQ86EwjgaQd6aiU1RhJ+aHNttnca4A2w1QGtz0TWEL590qwY2EYngn+ek34xgWOWHT/C/c
Gm9KggYV6omRY53D7tXEbnEo6Z4KZF74PhW3wR+iHqQ/XX9qREfNt9lYB6AImGUP9IH5/reUc6dS
WGOSlLPrnVlhARklXz9+ZjIrrVH1zR3T4w5Ug9YJU9VZv0KCbNtox6AIvp70pi+X7C0994q8Kb+a
f4Unxwy681CSvwWRxLIJlP/w6JKkbAyIYQsUNgpFPQuryh3+2AlUqmX9IF1w88Bvi1WKVzu/reEE
I2oGSDHdDV6/pTJ+7C5GHSemHCXahQL5RRSs0McAeqUCZdqn/Nnv67BdoiQ4yGelg8/OcS+MhlMb
s7KwtLCYNFDrixA4HIskI2onaUpR9Xuetk6splxkNoWSRtt15lJI2bvFZ255gMkdOeR1Rh4ibiw+
T0JDIs1PJttaAqPTFqY5zyX1UfkOluUdQhfadGq7OYnrom7Y/RVATAM9f+1fFvQ9YTUry4a4J7hc
0r+nfnM/yawAjmu5/fCRJOqZ8Z1yLSUbK5F1gHOLgqmBsawXlcFR+xVmHzDezi269Ivd538lE7sc
PTzx5ocg3fOwR6HIk7FVuSdpgBBBBfNjdrX/7jQBAKeieOdyFgBZSAFfM8DOa+MY2zCCcc7ctE1f
NvjsrrZC//sjQYnsQshP1of4Ys4Behr3mvu4J9Iy7dQqwYEXlgeXbP84fYaaaywM43m0yr42g5t4
Ho9BQpl+blrgApEPUm0iNJeKirOt3bn0sUAFkY+JtaD0XsNafuWXP/F4pb/yoW+6BcOLRqjCzS+Q
50GghxYjNDIV0vT0mUYgNwju/iRBBBl4Ya3X74Cot4tS7Ppr2ZyNFKRAFE9u8OOAgz8WPWPFdYAR
66Y2dLaBp8211APljwS8icCYORFbs8dUUd3daUnshZfP2O0r0Z8z9GLUV11o8wLXp9pFVBkqV8ok
b/NuW9yfrLeGxMx8945OEpaqs5CmJhdz5Slfqlytv8+D4dLclCIZ2FSxcRRhPWVo8UPzxXnPQRxT
GuepVYqLJrm2oHcwElAqzjSbzo1SfKzPCvGe0QMOYHj1sRLNHNZ6MOU15HI+CacGrAW865nYPUvn
/8Id1oGIaw7xz489/9xR/8OO2cPhTXekjp6mDhv7ohEM+FW6494C6USdoLkxQZFBDu/c3kH/wz7v
5ARczBLafzhnzPkN21T2n14NXfiAMJ8TLgorlhJJH9L8r5Ufs5rjziNv2ebBI5HNa3jaLsoHVKQc
WndcZU678c7bsKM6XP7W+pi1H2hbMXvKF4gcpR6ROUoT6hJxa9cNk+z4Wf/PbA1zOWoneat6UpA5
NtFhKFPWhkT55+k8K93Scr8hapS5mNnd47zNSc9GkXoOeuMeTE+275NzOJkTAiM7ifJNgpw8TkSI
WlSbfAMWSXqS8Y5YS3oloYMF1zS2ZvUYcwCilg/dh1DqVZoJmP195boEu4pdAh39rkpyzbVXVufY
qNsGkATEplHjCwRU7cHSKoBb2U1+SX1XqPLwDBnhMQ5FvMu4uhgPwungs2Sm/r4fTSXnQ/QE2rxr
7RetmDkkfIwn8hrV1p/KdhF90qTDfyVDb8y8eBDdbkak+qRbf2BDJZBr8viihdKDVwu52t8mTJ1B
nLZVtgPshFj5DM6z+t/PpLMc/1cdy9nu9s0xJ4ApRkGdA5Yk+ylgSTSqk8z1GPshjzydO7CPk5Ot
bAA9M7J4M2P2T5hzHEU7dZEufR3OU7cCBUp0CYTi76U4cI+Pd73jjE7FHM9AaN5UM30aobOZsF0o
X1rZ2YXlFF1i/TdKJkuEIMJUD0COWdqE85szSnehu2K5WpK8SIZf5FJ/EVYD04Rrgix4VPlQgCKL
vM4ERCcNPK6ceXalLeDiUpFE97RoP6dwQhqwPW+15gKMb6yPCsRtpDdM3kbhgz/KCila/4PGypqe
CvbeBnSXGs/1w9lLDIBgIajEK5lIht/WQNM4+XX2+NGghNJsLvY5FYsnftQsO4ZcCYcs2phOE0CI
Yy/Kd9j3n6c/1x5qzSijDXdPpLqUSuRb+uaWtnvjFZHwEaA4Z/5YIAph+0PO0V9aNPXqdvzMHreq
K2IDNgQbjHSR92x2oNh1yKpsC6OYzEaK6PwBlt16Zx1yVIUV83RYDtO3U2x8L5kNeMgeWusBxurh
bvrDEiPtZpm92Vrm5TzMYi1juuXqlSx4vUQ8P+F/b5TmtS15d3UKB6iJwDOnQFpr9qhFI1FCp3DM
1ipK7yVmwS+GwN4ImFJvPuazzZiKTdWunUNR11g97b9wPy1zxEmD77SZdWVs0ptS6E6aOXyP+HwN
Z/INdNk+5G4v8lEADanYNQwq/JYtjK+qoEHX3BFNuRNZdrmd+E0EqQmhaDSJ2zh/qT/htsh2siUD
77asNORHF1FClma42e0A7woVh26hvU+Uav2/xFRWL3GZsSOg+/zzK9Yse91KvefTuoXRe2jx1cI2
dlzdlI2zGc2lzbtGh+/BIyUCZ8sowNkziUi65nDVkryODdc7reWSCHhABEsmdPvonyyEP5AFxhtL
QgRwPHlYra0f7lZPYcquq/TRSOlcZXzSB3l9pZwoe1KXFcHN6Vcr0NvVTBEYjbayxJ3z1yaK7ioh
Ghz14mxPJyHekfHnWzZ59u4wHklfmQTtbLhdF2sEbBXwjbdb8boCwU8YCNXmh5cbGFxwoinc1oZm
PA7GAy/HKHmWxKXQ+78R0IwzeK16Lfx8p4WfCuPZCtOfmZYIu2o8zpXsx/+g6qPqEmB6UxnYxbHo
TlHRNmz3ahRH8YfykNCIckmKq6FynMpleUn7NwIVtOKquW42bLFnsXPyWwpkB25EJGuv1zLZ1HOT
4EB5s4EoM+zzha0yMEBYNuyhJBSohHcKukrQ8aUm3Z6VEiTVuCOBeRx8etcgtpgzstqPauIo4XS3
lbd7/rSBgiHZzhZGKIjXq3Ev87fOwGP9TIu8qs7YBYs8Pp1Pk42d5Im1qJeO2wcuRVKbbxltUJEh
tUGDK2fX0n7km65y9pCrlOm4J8ldZbFHK7i7voHAoklg4rTNlctfVecKLIotM0eIG5YwqqNEnJq5
7/4yfJ2WJqyOhRlXt95vrX3tnAgtzLIsmAJ3KG+F+l1KFD6Nms2Ddy8ITDU0clTnhegcXC9RoW5Z
/xntH4K150S0Kz4PmHlphVkYct51RxKRYb04eNeLTflftvYA+9rijRWOdOhex6wI/CPHS8bQxcuM
HtJebet+oLRl8U4sS+vZdez2CEA2zwZavkFdMNO6m/ttdNgQqD4dsEtmc3sXOzG3lPZt4quKeheY
w95o4uHmQ6cemfmSwcTHmyhDFS9n7ZlGhygJBAwOwde2vH+qKW/bbsz7fbDl4ais01G7KrBzP2Eo
0c94P4arWdHczPWZdkNHKjXpzZgDJ9Gi/Yh29XVx/Pjf2nCXjQ5Ar1oJ0GP06gq1R12aU8etj2Wc
dATr9PzIKx2HMOjlSVVNf6O2QBj+rzEb+LJzR+6bz8mk6dX2H/3ALDLVVBxh9nqb2H9SCW1hXQnU
KOU8Zv5THYSkONW9rKGlsLFvHTGmk3mbk04tVokCJG1TlaBrGau+6+Fo/FOJpZj8mGhMHR6cL1wJ
03wrGrpa79IHHJbSqFiqxc6wrdMOBHsCnWRdvvJdIw37pcLrlIipRm3A7SwLDEKHXoiRF0phNQs+
9K4RQFK7MPV6tiPb3hXtYlj5SwRlJO5xN2E2SuXP6jzX458MssLXN0wvlOBYA9/Kwn2AiWiZCV0q
9Sv4bijmF0XcW7pDrVGqX+gj651xKcbcT01uisQAVveWeX8mIl/a5f0szNxlcb6JxcepkyL6oNMs
Q1LAhg5xBs5kbMDP2p1mgwTb3p8GuNmxR4miiMJh/F7Kb2JfNx2GO1KgXKWdsMutp+rx94/qHZeq
KuGTdtn0vDLe2R5IvSoLgMln5WwYVDiW0uf3uywTi0ND9Ubiww7QA+/Rqi4RcxQdd0PVCiILMI49
hCGCbwita4GBr5mIjZxAr0/OLH37wAAhbRS1ex34NcmtGQOtoyn4W3EKuIpKdyJpQitEPqmJ/znw
TMCQbOHGUsCGTHrFcg99vPX775jiUqS9etRwJuMVAEgFtr+lI24Y+2uxFLdbxFZpsnPkZC1eRLEs
+oftpel3/kHF86LYkbgmzJnsazVYGJ3BUWepVbBKU+OoZbxn13K+QgDJovHjsIVXpFKERWVAeM/k
oZRQn2s18VUJ4W6YrwM31EGzkmruqE04p8yF9TTNoJXAR1sn07r7wODO3cLgmSnrwW2ulB774DR/
1UFrrDFtHqkDpTuIm8S87UbicJ+KgbecQCfXq4XJqWiqAhu1UkAAE5rxtP8ifbZOu/fg4vV85MNI
0gIfw8IKHw9euWjet2de8/awqYbMn9c2DCTDNv0i2AI4DQs4YEX9leVRndpOU2TZuyy4Dw938FSh
3AQAddCyYDLYzvcm4CFjnrjoNVFu0PuOWwub8n6h4BvLRS+UKiVovqGYesJcQBeHjqGwP+GQmgSm
yFFCVcuLyCW/siPZhIVbTvZQXjXK08f2vu7XZ0L4G2s/Q1Y9JdwEvDs3vghfRSXOtwsHzuPC7kr1
9tSsfFAKEjAsKBED7HDW3Kl1CzLc/b69UU+RUlDbtgxescG8KANIZ4V5A/gwHSILCtBHWJO15jyz
4O4gCiqjYTpz+vCoJ9LJmKOmhO/VBRYuHE/QLqGTC2rkMlst/FOFXmtfb1gw0jp6h/jOOzUpgiOL
OuCchPTB+rvodUK+Y47uw0nGDyeTjIyoYj9ADaYC8h1brtDKmGfCISVDqr3lM1r3qkdVXKzWeDYK
U2OOZm32H7obU31pW1pIVcZI5Y0MGBMQanITn0AIw+9GWFH0OsS1lJGF6bPxMF3hVj4JJP2tSoRb
IlXFnUDpnuTBhSv8EaiXu28pVUXrMcJU3mYeAtmTJLaZGCRMnk1qkCEaN3DWgqYMpoWDJn2lINjd
yNKETtAQCj0Giw15KoEcBSYfC/CZd4U72lUURQpSpPzgfQ1OriDzv3yjYUW4Ctl+AtKvre+mHG75
mQY0Mn+cve1/WuGA8TedyAaGnmKbwwbbzrOklV8vDPOVyEwrDXSIVyNWJbmHhmX1K9fgx9dAxuui
tSXIFyeV5ocxBLPJiUYXMFaYeUr42hLVo+74cLNzaV0hM+I2uBl/gULYNTv2C1KVjso9PFbtBRud
gFGxLg7T+35023wnwN5SMEk9Jjb3FgPLFCRTZIEDl2X1EOBCiATR7kT868Nvl87892o5Mek2Pv/+
W4mKa6qMWPa9fToJS8bMfdb+vs/iY8eCglT5jloak2LHxzcwsDvfDFONA6gQQf/m6VFMA5AMyPRE
wB8Az9Sirr9MAI3MX/yanVz1KuunZLnNuMREv20AfSbG971sUjm0j9YAKzXXpLIrWLyqj9Vv7/Mt
MYPcNCTE4FtacX/dgMLxQj6VA2voBo9goWnpQ6WODl1696raAZrCcMKhJhplbhchuGwP1dht1T6t
VGnRO5YBWNb7kJDVrhmST7xQJhD3NlMfo4FJGmjDeTZpOjN13Fh5qLS0RitOngOhkU6jkGg5kXzr
Oypg9iBkyCygI2dGYZElbvNwgJ+uk26WtX2HK93noTaB2tWEQcLR6us/A7nF8Xa50kZ/PHGiX0Nb
MN8724idPP+SG+/mGajOxHjC+Vhka4hTU8Ac29OkciVabP6Sh57EUH0Lc53oNNUEssJX7e43Ultm
MKk4Q4/RMvfMd5f+qsLTa4MNv4phTLEAb3epWhbms88VYlsT1YqIHrSS6N6az0MoainjqZ7CWJ78
DTxic91BUMFIHO/BpQa/97/15zO/rnu1AHNnqyaOvDCvhUWRM7XPw72/BTe5yg4r8M3gONV8TlYV
Qi27cruvrLcI+kcMMNuia/LMwvXf1jad3zQR6ZlY7QkBXmiKk8/HGOCbg48Q7KeOBXVccISoG2/N
tOD9i46xmSAlpoXWjCjsACIeZQzjlkLSLRE60RV6J2PY36QmfXW96tqKF55i6f+8fLfBCsVcF5mT
nzdvSrzu3LEelz5wMQmXmJNK/ZZi9ymoM9fIqcwqEhTohEn4n1hcomvk2Z4oWJ3UD915irOJMPrS
lzuLwAPsSawaa6U61MNe1PJWEtLkSV9G6z7IjhWiVypEzO3cRpf7/V3aXvNHKeF/kmKS7fm3GAZ0
veSkbAEAB7Wbk/op0rorYrpStndxaibr4JWqyTA1w+3wOea1fvFHMT6uItyAuHNnR/hDMM7wPk/e
o34FpKDLdYmoDAy8qsdGs260NNgLiQy92lwh/bRhM37UhHuj8bZCt3KpcuN8wKsEQXvDczBauG+g
Z+UihDjwISUxFmPEr2djouv55WqkbsMArecTnXGxf9KZKYOVAE/VIeNmYfiCRHMb5kF+i1lzEfE1
hQhgeFN5TC381Wm7wl29d17FynuSCUDIbcDnwRs6roUzIwaPqvgbKDlM20GxDsSP3ghKDo9bC+5u
aZ7b2BWLG/lP2/4SFyCO7/geNp6xYoX3J/Kj5trBFBZWezpG3Xoq38XoN8WnmnoqBgrde2evN42U
7hoDvnSE52SzzWnCEUiuIkTdP/7AufdKNqb2OCtycG8OLSGUj6PPHMGbblJxskjX81MJO2OQ9Pit
pmD5bjaO/fZlOIcW6iq/GzKWAwa8PQTyC/1OSzlAMlP3Ou+O1Qk/+kMDFLgIAHUJLmX7vNkkGsVn
4rfjpEf5HB49e48CzTmsHfxMgRyUmTndkeS3tbLxodbF6K7W54gZHq1L16CqaFQRldGEDi9Adhsc
ru4QOD22JGH4cQ1VGqcHDPinu0kRG/b+ta3qiXDYtcUApxhg7OVTpkwvyiq9L1Qkcq8bYk8Wi66E
OsWobPltVbyA19TzSQcOI9fY9cEyHLDe0SVpZUqFhtsSIRgTIOhwM758EXeIuK3dAeZTPYfA+Qf4
MNbMGQK4OAiKKjBQTzM0NmrYqr4CRuA8XlKmhYebKPvah3m/W+Pt3Qp09cxX/Lk6BgH/U+ptbClc
/WUBiDRP9rmGTk8LQTd+AZzAUUW9PxcAPpcn+hI/1RJLzG+Ug48SGTZIrb+PzIIZKImcOicFHCq2
d+2mrfwEdvpjxY62Yx+ODRG0InX+qFI9w2Rk+KVYj5W7yAlXlKRbDzWXd6XhHyFdg/etAWUyI5nD
Q8YNdJqWtChNceCkSRozGnudm26YZWQb4aUqHqt7HoIc72E6Ayg3xIyDnFbDz5hfNPysjm1jQ35S
bym4UP7PPFhpI790DS6YMPKNS7q+hJN14bIxdfc9mX78BczX+uiwQ9fXIsyO57/4Dx4HX5o/FbtP
L7/4NZxtS4ksHY6lXI1WIWer3Y+ebTupC41NeUbLl7CJ92vhR4JhpGSx6yX2gERg1bVlQQYs9099
Se+9YPT5UsU+3vCjlB1gpx5xBFimEl4+xtCAl7UWS87jIdSk9kHzgqukuk6u5Z0nM4Ev0PncoBYd
vjxOTnnvDQuvo3Q/0IJEW2sBN+uKgB5M1G586QciJ2L5GiOMRiTPf9Ba1FdeWd+AYV7XPG7xa6J2
ZHOcSnhbCxt9sDYjJkV+zwl2Spqi+rkMcDSdRluSKzFLF2yYrN4Rend1z82o5nk2xbELitQXl5/c
urtei3298MCh7/YdyxLjPC9c8oBscelv74up4xWL7I1ESQlxxY64vyBrQ19sw+q1Ll6TNOna9yGL
CHrq9V/lPGMYwA6o2K3lTgT4RzVCIifC1PIJu9IIK2MzpKn4Ny2opyLYTablOPzt++7b7BMoiWks
yNXgJI7bB3p6hdpHFabKc64aq5rf/tH2wKwQbvrBRe9uV2gJVz7SH88p1xNFWXDjOUuUZxn0wWW3
0iexMlwQOjE7ebRm3/RsRXJxgTY56Du9VNG5W+4xiIlCDcLj9CMa95E9mENjL+mF5wLdfDy5S04j
2Pq1os4UplVEHGsDvSOalgTr+PwuZTjm/4LIAH4dqQKK2Pszrzwa0r4wxTYjvuYO0HzywjzOvYjb
IjkzA7RLpeKt0zlWtlTsyJXnHDjN4qq8nvm0HW/iJ4v05PY/vwwmpEqb5Nq3phdtVQHpYWWSCSF+
V1UUVmdBSfiISskT+CSbteOjheXQQYJEPP8EWgA6KDc10eWqHt6aJcidkcoNOScH/F+zIV0W3wXT
dCjMDXsgvKXsrI0p9nM6Nnf4dpjR46RO37oEYoC3yzqxZ2ArEtR/QNAKcNmj8DA3QDVM/Nu6RUqE
cl1omHLetapUBExiHZfT8eQD4G3J+1kQgtf786V7nyTP6J59O8JSWUOWeGZvRQKCMzyB20TftLGx
Mbthu0dRLQmglg3CaFrPrm9BWWaY1sRVrxuW4NXhUd9GEc2SFf43eHg4BodLO8OFHchMQCRA7nws
BA5r3px3e+52lifp9R8o51ycAUcFT2x35lBH4xFNUCTKKimUnan9Rs/+KJIE15B2IKwI+j0pOXEP
Ku0Dgb1EsmxKiupzdu/KgXdjTqiTnZSiXcLWngIx/BgDRVIvuthortf4QMaMvHMr855sxQcnhUj6
f9r/HgxDlgSq6V2B2neueCW/OGjgT+ouJmyjBFjQFYc5Y2rP7jrUpVhK2S0fswdtI5KH5DUiVdRw
bM8ri04P7ImaOyLi+7RNWez0yYcpKD0ipV6nT8sIIQF/BOfKzLTOJRg0MUmtFFgtc6/ThdcrJz2N
5ZvijTyRsJC7T64ioRgTZq3fmDw3S4R2xpqMGlG3/ZOo+HJ2XTH3PgZBR4Z9Dc5zjBttXIdOTX63
JW1/nAcJANoXgIRXs5ECyZS8Dhx0iV+BXA5gnnru7t00NeINW4MLDwdN1xnomRhaHCEgXpBEvO7f
HFBePs8m6KSywdgojqkw5Xopu9DRx/KUKNFGaB8TXmcvpd634q4imRY2SLVrCLsA8A/b0z/ksT/r
LBJREfHl7FrHRuRjUb1O78RL5+I9FaKGZCuPfn7jYivRNThfjVpJlN8YeE+mGr+UuJ7drF5Fkh9i
2y66nv0rAMoittLDX5cOIGm3S6ZhY1+8OSD0u5vEv+KcH+VmPI4ums8lapLqlcggYEUSXoNJdGZK
y4ljP509g26M6hUxIQS6qTg2xAGun2LH7VHm0TqF+qTIOK+iZiNrtXogAqIQI0aQWTalIlPzIqWw
6+zwXcGoMyKWIJVWVX56BY/SZbDzJh5oXCu7y+y3V+VKW+bHWjcF4oZjDr0jBSmHlaUtNwDZSuNw
TuxxFqqF4mrX8f5FGoOznaKC5nDNS0IQs4U3CpJVmYmRosfD+M5gV7dAmEXE2cjkuVLbQeUg7Gcm
h/e5vB6amv/s2MTBDQX/3FU5/VEAXgYduAiKlka6858UkuOsgs3Q3EcrgxH/ccPevqAT6LvWBfGD
vNDN9wx1eEXUWQRBVXzxLaBrfpyu/fVbqavvXcJOgh8k5nuw9wZF7cylKggJT+hnbxSOdnr0s3py
0J8l99UWsYx0BW9l7970asvpxelikuolGOO3mZP8RNen/VQWfhnagg5BPNRTY3OTFRjYEwC41lmP
Nj/uRHX2dRJ4+3zDUiXyuUYXewfMSX0L8VdeTWvSmlE3FRnkDDzrPlM+TabCjg/DtDz/+ju2Mi2u
K7aKwrUsDEKNIdcDSjBI78ng3bdnLfDWG+7oQ8jxDrN1kKsSkmtHpyoLJWl5KmQuymkT9pebDnaH
IBoKwhk4FLkrWYrvVWSs+GUfJZNsfef6Sa2wGTh99/1CvRoS98yMKPXtR6A7LxHA21cruNANmQxm
RnTDgzLCM7vSK0rTjOwLQqbE78/mHJAoMFfjVh1arXRGucD6rVcz+0yz5XS379yDjfFjx2HDLLua
lfqXS+KcdRQm2+xXf1mUrXgj/Mro6wjLPylKTM6du8G7SL6FKW/MFwrFLz7zyeo+o15FzQvoMLHV
eHkWaML1ooTRojHpr2UgTu2BIcHO2rfM/I7Gxmrnr3BBhT81jw4LvXOFrB/90QkET1njkWbRRJx2
67o0FYPrzkBOFDcql2FBApqym/9aeavTRTlpJryFOjz7u3zKYwGx/VVS/0hCWhikQqe4gUPPVqCC
vQ5/FyTYSexXs+yRSU7bZaSw8xjKuuogIAJR3k/bQxCMkBNxF/K11or7BbpNynsIpiCp6/4oIaeT
a8a9QrxZfeGt9huo67sVw0OaNo5h11L8kSLPiCAIZlvbovoADy8SN0qgS4OwOu4wZ7vIKdOA0nSJ
woPQAKMDC+NCxOLpfVvr2GYFxMfJLhs0yGZwQFAGM9Wina6E4Ods73stVOZPrZ6rC0c0Tg+ITqMw
fRNuJ0+Fgt4FFTOpBUovqSIVmDUb6ZsG+7fdO1BdnbHuLCPqveDUSsEUQjb3B0MgZSM3qW0PJHgQ
iUIXww/0WWwu5f3qiZG/a5SorEpVl1dzTsJR1BbaU851giJyzRLszAlWYuV5YGHFJyp6DEd1dLNV
Yg3XQ9lTS0wF98jEDD/mBc7LlJ5VNPiM8JIMpGX1Mpm9M10obc1YqXUiKOpJx81buWP3hPIswtv6
3iAb2QAtvkEoRgQE7/vhxZDiM/4hOjOcG5pVQQtRI5fknz8CSka1QEhmG0YNd00ZgWR+80oSOiBK
1I4b8F4zOWgGS8YNKMPeCRxLn12ichMYtl8uoVxiTitsC2jjuxPGLN/g9RfzfyuZcplJowSHpWvd
fVyleDp2atbo1cdgQw7HDhKDDHr+y6lQPO/UavMHFKSyjLNHD09KgDfNb2tpIs1XvoxX0u9AKWyZ
NTAIST7qAfAwbsdL3ExtQFEjRsT1ayh97DzKuetL9daAX2pmVMsxiCpM1Fj36fZ4exout4e3ens5
cwndE9B+QCd8h6KnJP8dPPx5oChyFblkdFfXmy0oBkkOlJY+Lj05XFKoyJGa/GIMedJsC9L6CHNA
66dpLFAt3e2s8oqzqEdvLqHL8SajsoLe/tYUpn4y5l3AOyKz2Ws/MHnBA0Rq0fI5VCNKaD63KeWY
E42v9MPDE+0TtbykJ7/T+pe2nIeyxVoWyuWTuQB2xMYfK/lAxUptn/39iRxQ7xyqX3I6r1dhoI6Y
7ygS7M2S8YiyrcloYY4urPjjROtaSKl0fPjWHS8Z86hfp4Tybrwcz0CnbgdaXM4l+/+qV4SIx0qy
HpQ/NSlY8dorDm59SJYAzwSQSYY8F2Gxft4kpa1z8vXA1uCG8W6oRobJHTTK7gpbisSVF7HaBIp9
nxr6c1Dns02AGSUYTZYsKNqfFOPQy0mq+Ujqz963Dk/fZsSYycycsEaaN4UJhSsF6y1+9whS+wCH
91vfTOznTGeCXKUR2BOP6YAZ5TePRNY+iwk+FzxXp2ZD0wPR+XdIgEKte6FHL13HZIkBtBKKW+ek
bBVp3PrXbXf60ErFHqfxsQlE/RPc0YdzFcIEVvuJ2rj2vA7yyo5bENJOzzt+5+BIzJa3TzBpBnh7
PrewOd8H89eYQZXsaRKlOzRAEWNuEZ6mqombhbFnTAFJtp0Utt8O6ooBIDAFp+MVLjqBeL6D0y7J
EMs37y59Km0oQqVf6vwW1gKtbuSquZ6ao/dFfjBxWjMnxEX4D69RIEELPuUaT011q9Xp6VovdokU
0gITxrzbgE+C5mDGpyswl4zQ8Q0I3wonZ8zQqVCn45RC3bypklLprLv6G3/JwADHt2YkF1+FFKpU
jUbd3CNTtmuasuJqixke+mW9XU71zLPAbxcGJ6JLE6rCGkJV2CtSpY8wrWf1lqPxWXFifSkvIACe
8HD/Yua1IcLDpvt4ItuTBNxdFx1xIAz/pXllffTNVz0DDoli7234GSE7YxRm993vJw+SFEEIaVYU
NIeWo5XT+fCqVbSadWxHz+2DhaGeslEIWu6gYWyIExzTblOE1b7qEmxqJchs5vB3g4F68nj+PWqD
L+QNiSQrkcUBpR8gUJLfkEROIIhfs0SFsO17MmIKLu/QBW5z8tlUsWN8Sem3Ysjku/SrkI9FabY5
gal7p6TGRdmHH/LSw1gtbjAd01YfGXwvotHOqsDDsYcVgPX/W/wpuPbH9KpTObJAaeVwsqwMj79B
HSVzRKjUAnUL7d5juz+xHlsgU8u05B6XmT8riYJWCUR0/o36dqV6BkA18yYDQ5UkvDqfHP91a6jZ
vW5P5tK+mv+yP+t7oSiRqFi63vYwwQ+vYKU6CCeqcLE7y1SfPHM1Icqni5GjwqyJr4kD9aM9XFoq
7BK3f/B7UXZKJiUvJf9uj2ev3q4I0jX6kCdZdupKToQvgJJepPPwrUrVwRCXL7Ey/ruTXhB52cWV
VtvcorrB3phKxW/y2wnVSafatbbBx/ARI5efKDP7VgOH1GyT+xHwqQMpEt0Y9GQnsdwTEGcS+LHq
HSvaJjKkFCCajNah6FE0cMGXPL1E3hUz5muz30E0kmEZM8ghQLMJjkzo4+RWaHIiOwGa0mfA/muK
8mMGMk5qCiz8YJpguqLbqPktgKc94487z55+OBCK9xCnYtXYMaKqjgkDqJGcPjLkPBmahfIuV59y
fYmL6Mgjjmi0BxDuaS/ONHbQFbuw4N3iLolKWSjGbX+b+c0YQIDosuwyfsaEc4zbV6mvZKJeL8IT
8KjV5KxfUIogBou8qXOW6YfcISvrKRjQG5ailJBJ6NKEBpqka/LZrq742OWttH9QtllEozjMEgG9
IzKzAGay7Nk0gdgRCzWJCfHJy8kX/k9rEDTG5IV1ITp3F7RPrzVlBjx/9BYuFhl4vSkuXAJVq4JL
tBYTK87a9DYINWUjH291jHM46eze8kUQ7ubwmcG0zsUFrsBCGccG47ReQq8mG3dyHqlYH+lruDl4
XBXWIflbreJzXU8LWUoDfPUxokO1fdo8onf584X2hjVJPv2OLitAqDGEclOBTm0+8wbC0EBNah2h
cE3TEBZQ/+LDLN5hnaYGEPOszR4MxK+QIqk90Lx22GHuRrx+tM5zOwgAXhuXXG6kzuyHsN2mYwYW
Uy4RecV5G2RSPTUfxTPWquk+sDjxi803qBjbNFLZpmVbxD40qqJmYHGXddbUeOy0rHkVgz4Q23Fw
WQVc4fJE43b2bQ+w1FvisqYAZp+wGnRlaEIrpBimLB+kRdTRolXIQItApFIrYUSMZs1ijBC8q52B
MIuaOOO1B+Nk0TllxnkzH9hTaFNgb0v2HjdX1/aQ7OIRoRTiimH8aK/1/0/cH0ywAYeYoj+5b8fo
dYGhce3u73SFaY5yhAWScMP+F/S/Ou6Yrxa+lz013MkOIF/zbJIbKbg8sjsbzKLnC7tRMe145N9n
MVnEuYeoWPY6uN+Dh2x4iYKHfAH2/G/P5qqZOG8hyIVEs6AqEd0dSWGOuNSAtgObBObKxBCDPczA
Vz0OpvGxQw1KiEVkmuvdvANHHuVgBFqDVbfX9RsPCcpTlJu07ZzJquhr8pU/BkMPEJhloyHo+AjB
isF9tJX+uPFtztBh86yflBWJjxp3z8gz0hF+4p96AtUr17YdMKM7lBrk0uf3r1kxUcZlLmhYznJG
oIBkhvSoiojX3hNABnWJ2ml7uO6wAf8YaMel+qJ4FbYO2yI68OUjXpRR7Ae6xHzxdjjgmvYVDhox
sv82iAPpwiVluSGoHKNqm4s/wTqQhAuH93O3r/6upTc2M8/ZjdEKJX3MTyqd/u9dEM+GEHzGMQgZ
QFzNQlbs1il5pTOL7po0AuLBHc+tSyuUvVLDgcFZ6YUdyAPiMVYEFPPuAjrB1lDgMbE16SfneIpp
V5z5iDcIqUenUqCR20CDCjod40HkIO/z2S1iIzr6UI30unJtqWZ8ca3/rZyHUOV3QsScY3iozBXN
AhsihQM7L3+c+/xruKfnhv2JMlzIz0FBPz4HrqGInMSYXizrtCELUGy6c1GRLWuyGY08X71gjDTr
8ZPxRon1WA21VqrHB39Y1a9nGlU7I0HWMXDWy+hE7l3YIIwo7ihLYuYYF5kV6YuomabKgdojVV8k
H92BYRbiLrrG/z9A1RL3Y2U4fa89ETvLOxCY9RfS1dgOTbNiTJ19jDDIWlwdxheUJpcRytXoBr8G
Pb098E1MdoQczZAT+ZjcB02uN8+eSeXhZ2mQulM+HC6hIK/ITH3xuzbAyvX2GtR0bfOSzzxuFrSB
Da5Nn+6Oi3aplFEd2TYKowq2N/FDvRG/3QyoUNxtk6aEO3FEZAPxvZ2Svz6f87TNVBoUwnTZQpVk
Y2X1HNVVxzNWvpWqrBKhuj/RTVtJA+Cvt6isnluwo8MvKSUHbPVfTsyDH3VyOt1NtanjxnFDTkmt
0yAYJGdLT+E09J2Kr209E0QIWZndd9bAoUs9Br2dQ7cU+jGQseHMiRGsH747RoutbL9a13aWzQQD
8CfYPZ6OLa/wK6S/SQNYso+6C330t26NG7Dt8ZtmYi2yMM/B2XkSwX+venifcwEuNecRiZHAp6Dq
blSWu15G4HC051OeeE4c3QpUn7FHlgCoq6jAPv2x3WtmbOd1FCNmuqpvRfpjTfz13+NYvby3EZbH
HtMUAFyjqJAj/ug7VqA+KAOy/5ySD5ijK0d6ksJTluhPJ/xFqfbgXAYxP7kvVb6R3CKHCVueNDHM
dX0UZd7gMB66+l+8jmikYNunUhOYbyT9KN9gZg8WjXvXvUtM4rueup+YdN+gAMs8MbHGxnlBL8lk
5dDyLYLtBcxEegXE6eO44IbuAquIWkQy504qs342nuYCH18dI0ZvppRjXeyplTKrIrqSgnhzRJ3Z
tWme9VFHSiCjTizjEH840BhiWhibm+1m6m7QAtsGMoluLytQWYADTZMpJwWELI6Zo70ac+sh4dpo
seR7JMEtwGFAiZYq8BQjacSWb3A3y0RmLTsqQT5f8193QSsPuKGMYCJW+2HMf3oEa9nkARyoCNw6
vWAR9eFRY3Q7Sj5ZEgTiwjZ9vHsD449K9QD2UybcTCLdkRDExVIc0RMy1uHCyDzrJ+9MXlBc1lfS
SJ/3vSz5LLZ7FHTpyTdoTST11t7cJY/j4fpR5ykJ+ge0YS/FJrjeWq1idYCUgyvzIWgHa0M1fesG
fukZJviUOCWkZR/TdVLtikLZf78VrJL+xc+C2XAMGFh9IIDOhaCDcQrAK9DPlgxRvIYB5gMg/qtQ
ElNLQ2HYdSVOHlxlWJKlzG5JcFDsThvAlxIH7MnNVP9ZINiRAosBj2q7oN7h+wz0ymMoiZ60pxLc
3ONOAYiRAZTawMxS/XDzdEdXq7NKy9QbNGxkOS3r1CT6bjsfHb3um9oYU+SgpEbJS8BBifAm2GQk
NXhnPS1FVyBRzjo9Fp+wZabgFU/6FY6o3C2pYpPGkDsQ9xzbhWbQNQN0IbekBF1zzvF6w7aaVoLX
RvOpuljTkYXQYRy6ti0Cc4N2LFZIH/FV4u2cpWtf4c7LeoK+jLVUG4BXErEEh/fLVUyD205JRW2a
x52xXSxzfFL5KYDD0eeuzpD+As68dgHT4dEQkSIUdsGEH4G8b0p8tZnGI+96coSo3f394wb9ZIp5
/mBACSU2fd7nMOpeOC7g/LbqTXUDu9SmK1tSK80JbBKUxX+K/4zONgtYMUWJRCdp0WM2lOUgIi5D
U67WXzpjAVJVSzHxzl+82rk9JkOfvVtseuEkTgdv/K3ZMxunRTHyU6qNM+4XsbL6AxbiCXZmknvj
69Q1Ibp2KsRC5z/MACea4FUhFEHJmyM3JC+nNUJ3vK6LlpFWiRYGohu2XjMjTXGbeS8iOq0Nhtx3
r1FxoUNFwvc43py5WdHO/+JlseSuCr2Yd4qC+aDVqqR88uzqOGb4B+00toByp+7LW4w4rwIVxNcA
/fZ7wM1lgw2l2fvTBZngEbbNd/DRv9Ntah8YC/kzakYWdWQg49T3UmjsJ7kL86k44Xn5ysRGerme
I7SBv3kx6OtROQgcFrbo2Ft+dtXiPtCeKaJLtIkxlvXQJC3DVgjHcnozviST93wzg2sQahDGyUuZ
2eW4P0auzptdEGaWels6vbG0skoJ3798zomWvISpRwmFplgToEMs/ZM3il8A8H+7rmK2LPBH4Ro5
1mrpUy4S78o/P10Mv+L52IwQYJhN05EIQCdqRRLYVdZV50yVIB0f4WGql+KhYyqpqfIH3Tqo5HkZ
qfi7aOGVDaV1yRhu0qZsa0IY+EzdbNMrSSZ8/JRSzseqMH1lQVojFKd7+YFgUfhaVJFP2e+rCcO8
jknRfqHpxR//es5OwJ0SvMH+FR7KpLw+/NhBn9vHlY7atDyyjHBc45haiABU0o6yc0EgcwWPmsjP
+wlduE0FYx6UI6iLqVrs1NnFe3fKJW/ieOB3hNzdFjW7wrxXyiwRnEsTfxt+ZtkzgmZz53K82S68
9+odv761UYA4lTDDGNp9O5qyxY43haRVmJl8hVqDWmVvBFMtWLyN8tCR4mpi9J5zMdJ35LoYkpeq
eudXGDk2guAKarkxmFsWng36/kN200VQpuCuF5FKs0/xfXfZ6rzLqJGTrYaVAHWwCO7mGj3DJyG3
eAVksJa0CL3SKFOpj+lx53b2N2xA0GaC+wHlM6OPHGqD8X1IZQ6RfqZQvRur0Db56eJ/QMVtHunk
OWiUOt720elG2Od6hAkEEd5xn7WoRsJzUyHmlJbBkbo0SbrdxudvUJW95mU4CYBZXSMGPLmu0MlC
AabI+HgvLjdzsYprhmUL0EQOX/ur8DFPdLRWn8tpOTiWMr1clvbqEVLLBkDbG+1NPfS44P/Os29P
aCaYIbRHNmDsY9BGhbRL54W9ha2qbc7NQufcQrgwsTb6oGTF843+gLLewCzT8B2gC5YZU4wNBlSC
IDWywRPe2W0bOHTymzCn6cKJapUfLNXugcHbwqKaCDdKGmLbNUFuHU8RUcfXfhxGIuLSEalBPCAN
4WFt507vQf4JbAiUv4Z8dkgBieVo0tCsAJiGJg3sQ1oEYqEf86TNjVP0kT9ANSPyswAMrS7o33vU
vJqSwscrFjWv14NaPnOJsYxWY7DlzrsQfx5oKQckr5JdF+anOIANM3/ZxVvKAfNDC/NyS3TDKBB2
cDg3pXVjIg4pxeOnEZgfjio5Iw4LFdErSpZlEw2227jiMLIQTQ9UY3QCbgf7yODqk+rTROS8YMT+
GOmG4lvx5azPvjKaWO13OWZWutIKplJ2cH5xQtfQjBFHojtrA/imOCj+E97rtbgQvNvWfKnSlaC4
BkX7Kg/E1Gp7CBdMeZdfMQya3NGGiTBw+OJsKYGhzHqzffwwEHLziQ/rA7yMZGVJRtVhgnnWdAS7
4FhsfSqh+unJ1vMY9DAxZcDEieWBK/VOR/EABatE35GtsW2RnNKvz0idQ/wuBUBfmy5zgp1JhHBn
33ODpEEufVkUfETXOW0HKPdZiIfPO7NYPxZvJHkQy+vp/p38DLTyNrNQG5S8sJUHZtmuCiqW+KIQ
CVee7yGcmaAfV3obXokFdPcV3AYItYL861weNoFMlOhOzCWLt/+ZCdiuiEyTwQ+0nOJR6AruIXFd
a5typEJnyEmtcMH7ugMqeppxIxNoKvW6rJxcCxKOrObr4F++k6yvpGkc0DOX3bkCLNZMqF/z9OGW
NgHP+9VoVmEPE51AIN2xau208Vnty6qaY2uSBu3H/lIyZOpMHFZUIGUgYes9Wd37XlR/Of/hL6Bl
KUJxnq73nnCy7M5zvOBTL7mM/yE+i5Q/ski78WyWgcm78qIlJTZFWjmnr966pdPENvGR+Kl0XP1a
HfxASYM1COZxuBPfFeyu3jSMELM+5oznbCF7MhLBqJ5WVIzDcjMYWi1gEA2Si/MJhtbSrREd/JEk
5/2RwvriADgJQzbhgImyvtRsM+9TXfKxNmAGMwsZl8GeNWPSrfKWsXkz0TeewjlBlb0/JBSfKTlO
EkJ0qtFwpkWKCdzxYEhpgG+nO0hS43Cizy6IK/3JPU8B8KfrqmGfH4gHRCzornc+Z2Jr1gO8WyuZ
/eAa1MVNU6RjR0ng7CbTisaATrGNtmN1szl2hepfaEz7dr8fpoZpRK9dm6kVMlwg1YYgQ5uSbbLy
d1IzhZYMO/hHtVWINmXGaRVD/zCd3+94DTTRl3vDvD8rElhKqzjkCdURDZjiWo6oRhvwhAxxbjeW
QcJpsHwG35ctDUe4XmRGukJSmUJK5cppT920mjBvWqvRlj/mEkFg5YAZziqtc6nlEbh+dt2c3ddc
SaWLuv/G1wPpgnJ3GEbdGoBNCnlVwR1jEe2RYzetvN1m+8TURPugfhiT6J8q/az+SIwv2Ye5rkh+
mM5j26u8VkU6Xv5JV0GFMEFXuShrjdQGWbQkT2g3/NfUFFRqdN1nzpmsbFE/dAXNfVvx4oAoulYp
abqhV3d2J+zBVAAH3DcQor0lwzSMmKasngXLJ5Y0Sw/GUatTsKTxfYu3aycDbetPPscVB55nTPoM
aKjBxhguHL4v0Os2iyLXsSQriGcT+aEWBg98fpQymh0Xfgtg5i9By1+VHLAwAPuERmVk/mc3ihNI
sH4mdhtKtTY/36Ojlv3kqKMxV/W3xqiB4D5Vh7ZxrwY837SgT3lmy02cmf612NBGBszqHTdWvMUy
x9C+/emLMwmpYXgsE5KDwcNWu2FAEkv00gPy0cryZcH3jWGfNBu33UIJXOEMfbimmnuHStFdQK98
8BvximvCwaFR0kv89/voceDgqRQSM67m65nJW7HiazPFARC/10eZKRlVOUr+clivpzg3jr6b0rt8
LAB3yUJIl7F+dsuEHEAmt8ZsLgafE9wfMUQ9Z1UazMxE+OO8pP/Ph+LkaDeqpbGIH6011MCrterA
03tcf3BY3KAM8o0oUzfHwpcaz3RiS0pPoK4zRzRAvFDnerwsdY9X/4j2qIBvyqjOazda4ExPHrcI
zHfkK1dcBLJjP1ybVR4wA1YZfa2esvh1Rnf4KKwd0YGQqimTAOADIl/F30Cl97iY/RNDj0ho8dHz
+a2eeqBH/cXdJwLM7RNGsUfK53OQbYRoJxwLZyeUwx4MpdxY3MmPPJswjLRUDoTnO5qWWZHSntoZ
QRyZhV+mNVBaYOEQSyCcPdOPNKFJ8z109k+Sto0qeDNBwOhFye8tORX6Gqmbv9aMTtyrlM2BSdQe
l4oZkkB0gh7sp7TeyrsGlm0pvrg60qrJgOeixFYuqS9GvRBk6aZb2FDyX7QAoXaEymaQS1IMlXOO
MK1CQ5X6ei3xDvO9GF5Nen8A4SCBdJQwJBwZnkpmlp3lSIZp3FerG3pWloNjlznMAi4NQNgV9mID
HKYtOvTPMBuqpvCbmuuKyaunatNKWJxFWWXaSDHTD9s0wPU77qmv693pG4+kg+AEpb+dUzy9AHhS
mYl6gY8MiZWBCV/P6/nS1UDOEJhNfywAs7JjXa48+3qKE9/OVGnlil7Qmbjjfcax/1ohh3Sc9V09
AcQ+udDhyAxuVl3s/BJ8pAbxuRGMscOMf/HNnW0uh3gRScM6zaBwuvTxhVrO6uAnqGwrJZ17AZ/Q
kQvaykYgWBvB8997boq5d8kPhtxc3a/KWWyOIECLEJ/ME27cm3/8Db0gvR+m1k9rwsmK+hayjyMA
r5OaOjY+GKy9LXV9OMgQCQmqhooR8NoN+ljUxaMyFrx34BNkMHZE3X72nJIrcViYwqTxBO99UK4+
uLTv0swljaoNL61rpEXMoL0Ly18Yp/LOhPsyuJljm89ag1KBNJQc/gpmOY+vHgXW+cato1v1TrYn
X0grqU1uYJ+VGcTJ7U6nHlHAQPD9EzEx5ZM8Pus/qsBWh5guqnI7CChIJhItCRsY20F+ohOVZ569
CdFwjCGPFcZgGmFsiBDdc+spi2G79bcNLz4CHmA/u5rhGAlpwid95tleZ1gvgWwhyv/cAxxf8E8j
Ajc8m0jUzT5Nk8AhLIEQxc7U4yQMk/CZ7C7juIYHqg3HTdkC8vlyJjFQwHzizWq7wi+TcJN8GL8v
tloy+dLPCJ2fwovX352LdhnXVVHc+Iq5KFTRtcBo6tbPJD753qeVdl0DjrO7jBCorY5n6G4nVFc6
Xzz0TMInzB8kxQV8NG3ug++S7Ezmomuiol44Z++Xzg1GYZD93XQtBmOXg5rSYWJSdYoZTAnBnnND
znq6tpjYqu8hAyxLf2T9n/Mjjlfj9+5QOH4m/v/jx9AbZi9FhjwDFpdzXoKD6KG0ct7A1/pXTlEP
q7VgoehtQBQksXLNDM6nnORp8PD+ISBfNCvrRzROQvNTImd4z2Jj3fIYppf8qZi5rrFyULfmcRc6
qLlfxdYQkQSQ6PuaHoCI7QOdRS+lHEuz3TDYefryUhJ2ixpk6twTfnSnVuO6gAtcONA+PvziE+Sb
4qetG2ZVkxRhlsiFGRR9geSW4T31ic+GMfWhbiD7MtF/D0SPSwiY9nrI8EbJfHEg63u/kUq0xWF2
lrbAIO1niWPBDX2IqdBJdQJJ+JLpaqyA+pQOgjn8EZP4WqdavEUx2R0q3Kk9iGXyLPf+UeFXjMEX
NSSDD3eiOv/15PdXEyGuR5Zpiwjr2mCR+f0khPQZaAa1rI+RVhXK9tA79IV2yQucWh5Zb2740AGZ
gdU19RtgnTh/RdrpeoPFrz0uaoakXePoynxALm9QDVbAivE3l3gvvwa53CSuch7lKlGenEiK+Ueb
wltvdWPeFlDXMxR0vUVzm9E+EkxFhBj3w7eUu5rIuN0xZGBzhGkkTArPZ7PCJ8XZTwGRG3YmJyPp
I0W9HB9HyoV1YFWEqBdgGV1gSMFuxGyWYGpXtNPxToXK8OK4RyQu690QkICI3FQenuGvJ9ll3MNX
4XB43c97uQM2hvIyRphKH2u9kY+68RuFiB9sqz0P4U1caccswXFLg2etRPWnhDUv+R94xLj/6wq0
nU6P1+Kh8QOonWRHeFiWomKhffNUIEBlcLx/teFq/zjckTPY8nIsRPWNmJhOjHUqhtD87tOAUjkw
GtvJUW772CH8WysfxoXY5jnRJwmcHnvH/HGFpKEgNYJLEBpHNsJZUE2D7zcOCtgl/EYuRIyGvdYi
E+g9MOlwDkE2tGcCHOYTSNgs2D1aswUcnTLl8cfOdM0DHc1B+g89Vl4/bauGjlpKR1q5QKM5yHtI
m2AhymwdaIletzjaSZP6hheJm0yOFMtixMvgqDpWAO0t5JnWvurD8iloYiddR4DOsAd7qVhhfl7h
C/oqF61tV/axwV3XkZzlGV9HwcBZpEdCvUnT7upuo9XU1MUqlyl3Xbktahg+ix8SxGwfnPCeKJw7
CBbSJb7VbhimQNTTyu+KzVKiAOp4au9SGjxozkanYGJVSBzCIzuTo4hPhY/QsT76HPI4hPLfIOjE
aiS1bG98bAt/0pvupDbuVmcoAnDudePGJU8Nu2Xeaai6rZ7ezRY7rMkKT5gUKp4sflZcPVUhud/X
zitLAUE2nmdgzG4Kr4eyqKRgsziuLldsdSW9za4VzFNTDIf3gEsxZ0GPiHedFToYgUxLGcAG/Dm1
0Ek0AQiKLK2gNzsmDQNua6Tha1LZ71/c8MOooa05Vbv/6FPI22dMTwzOG9k8rh/S5vr4yStnEwTK
BwiXz6Yb7qWU432REFoCYi3vaovCapW4qDhAUc7/QXZ6qk+P2LZNEHtCm96sxBz52ypXmruGk7RN
9zijsBR4MJSg68ryfuO+29BdoAcWfYQ8mMv54Qn3Ald6AYNsAKOsm+EoujzhwGqioBdKcFMUgV/i
jIXyACrjxYhDPe0KcS8kaM7GfOjcWWWDZ+zqCMA/Wk4RQ3SAzL7xNQH0eV5txGXFvbLGv1OjrHDy
esuNF6NbogF0xqCyZt04Kq3UbvCa6NubuWgP3TbT4Pj1v3c9ZddgCrGrJc/xOw964lZ8gVhq39yM
oBKvIc7dec4wcsy7bl/hXPrqGmSAGi4DhrqytvpnfAmi22wCCIjU5pVSCxWVSUoEBw8TAjflk3IN
4lcG1yi+5YwxqFBJ6qsM+yL2p27hRgPLM3lmC0y/+SZzFA+6aGEDMfx/XATeAAVJKSBqcmuZ4dEn
b6ttqHAxyu1mMkPZLzOqkF5lRvmQQDJRjLT+0NA6JoM5C/DWOSsIXsIZXfYnMOoO3c9yu3HEYpNV
WQ3tCo/icCvGRs+1pG4rQOA+lLJp7MT5wtivef3EI/+1A27KxGMC8wqTRjwLlZsGGB6A8+1osiBi
5alCHLTzQ9EBx+fIfF6/qnqAft3Wfm7VWzCMFV+8pDe38I4QTtY1rpMvwyeF8sihFomRkWMtqfWW
rkI+PGvanQsk2ZQl90PvTKVnkrQVIqMS5/Sw/Lxy/TmfFHENW3enBy/4Oyp3sTwfoTSKrC108FBZ
JlF6IUqrwBLS30sxHEZRnvV0n+Nsk2zUE5WnS29jZ60GtpGcFeJaIUO+JDx9BWQOXYdYe5Fsn/mk
Dgxc3B+SMH+BVwCqjn1YoYgM7wSbGzqokQ59SAl3bQ01EBbfa5FlNopoShlSugwQzxdJpUriGtzU
odq5/It3Cck+wJBLLzJlWHLWy+28gPXpVQThhJ38IbH2JoBD+fKn7SJKsHGva3nigkO0GVfv+N+s
I6E3UHmDibi/sftSvitHYN4Rcy7ns6TzEYke48S8UCc8Oc+bouKADkvFSO91CTORZkKxcPklUFgx
I1SY+uxQy0WKpvk0aaJedAjEPuwWD3irgx/NVJe3Ba0zmEvQaAkaccqJlbg/oU7725X3z2sfPgLq
1rURM7xyMPJ7SEMZIQq2ysIxchhovGb0i9wIz2lkwKih2FeVor3/J7Zibc62bYh6rNesEyhuX0lp
/U4le7B7gq9OgoUy1Lv6V8zha03OnqplxTJCZY/zsNDyzQgE8D/TkLsFSLrc7dPqga7uakOnEkq6
Al8L8oULYQFJzRSJ6rOP+Bkt22zThlWTCyCXVcRc6Iwt+7bijAC369U0Y5YTCpa3XyBEo+EP34yW
LaGtDsl6i0bqbO3HXfYG3jT7kAlMyqk2jhkaiXMXAjxaEkUZtflpMRnOiDkQj5OG+3kARICGDz8/
EovGbhc6reGby+/TCCrb++OY6e1NUTltDCf67UMt79MgUKhPkG1POs2Z4hE2mMGrz1+RlWE2PJfg
ozRIjHLpdn32OdoNqO2VAui6Ra3041pTqKShU0SuXHGEat23WVPxNtEa+kw/IQ8sES0ElQ46iX3b
oN6DVREqvvnhumhfRgWACGXh05OMrUnQnloxDdp4T5W7fujy6Nin8JDLTnztS0+BO6AvSLVd93ub
QEZVJG27spVrgZeURYdrJAlzn4HmJHecACNURIVfYs+tqtxNqGZfraplZgfAMA6gJFe4jt8pX5fi
irTa0YM54CoyN3L6uD9hd6+KAG9pGDjxtTATMHdMCYCJ4UTXnDHyvX5GxOXiYk2A0A8L1awvE30q
54jOHFgxB2hW2BeYQSMgkXSwIzSQ2HirbbY8M1U+F7lIClhN0oaZVC7VVr8JwGTxCi13VcN9/IGH
4LD1cwGP7pt0f6N+VfdI/DH/M+LRCWN1Dj6LWDsbrkgTKXjcvzvxFrRKHkUCTJUIXhJGKs9IDqR4
7V6SbTMfWemiDrTpcCfXMHlSqVtCB76l1DrCxYHCcfFkFFAhxHW8wQCmy3NGCAFsrepKIPQzX5Cv
nAqfoWZ1B3ty4t6vxsYsIKqpXfLoDV/naXwxr1IAlwHG4OQHZYaANpClU4CHqQSZwyP4M5S5fJhD
VXL/s9G3oqlbO95AvLWi2FkxQQs8cuxCS1S/0mQKxUhMl0w0OnvKBaI4ZZd1mx9wot4v7SKv1zaW
BXW2GQH98uGF+tLprNinToruJtNX3Kl5U8atZxIXi+EVmMcDqtzWGcevNmFkTzf18KziVBRzajav
FIOJSbbKexnLAzOYtdxDZ3ur6y8+XBpIBLKWLtKRisgGbOTMj1PRqm5bTZj9iTd0JZ9B9c/4kVss
M98oeFGXExYV3L/2x7XGLlQrtJ+WsGcjodL2nJoF5HgA9DKoGq2zl6eer4XJoJVZBz11RolpiV1T
v71fNm+YWNp5MJrMIH1XUdqxRfUc82LOHfC4onfE0oDzeVAhWlPsGju6Z7OLuQbu4tLSwHBas5Fl
CZ1VX0SqNpRZNMZY0cZMn4bP3rvvfflq5QgeL/wMoR3hDtbLtRosXg2u3snijmnDWe65D1SPLtqE
hugWPEzGQxgBMuOlDj5DWg83fJnFqHUy64cB4VqsbqiMEPb8UFajD46BobZKzi5PDYf/CQjHMPwY
/eW+hmDbX7ZM35YTB3TbexT0QH50DMUb4MbR0pD+daOaOo8QLBUKIx9WdQfp2W87NE8O1IPkJa4N
BdagI7TFxhsTEjz0zB9hxEgwc664eoEncsm+HnYqN9xCCJ1lvlupxo/q4e/n4vGDy1HeI7TmBm8F
LshYA/6PuXaeaOT2LHFP4NKAzKvRvXzHo9pJ3Q8ZvcxQUONed8Kcy+r+PFQ2uFN/+adgWJjdSJDQ
CbUgRuHZhJE2ARZeH4oMR7BvBjLnUIqKaqFIZAPgFBKNRtH42BUxZLMUJ91tLxI2zUmeeEf5esd0
2ZFjDMB2ZLFy7BjR7/asteBLrHdQB7lUb82X7+JRpWmm4SP+HTRJHraQ35IWIuMuAtdoVj+QPHj+
/t7fioPhUtyHZQdiUwfxCSHDhcSN6O/kqKis1cMiRd93hchBfnuCOxmqXQuSIKuWKTQLAFw899nl
dYchtuk0wVNM7q/yF3XbCW0zXSXSeJBkWFBFUGiK2G2TWUsLJLsWwFXMLBBsYCY4YGzWi+BoYV+y
uKgSwY6e24qX21aONwtnTc1RwkW5WERBSpC2P6T3h9U9bHKkeZEVkLVj1uUmQxkCLg3Hk1TcBEYe
nupj3pWjKjpVMDVz0APMxs0JhBVgLkU2fvo+Xa5dhuZcCmZkOSRxgSPlgbJYQF/8y0prEn8lgKDr
sjVO/WpbWJc/GUKkQu4rtsLiLhaNmcH0eOUY+bd0tJFxKU84mA1BR4nvjj+vC/FNwUaFytCmCE2s
yiqavucYx6euDIfx1YVciEzEh6ruI/roFMwCdzK/fCz8SwwYMRnAdxrNakFF5u3QtU/7eRrPow0p
JoYDkx8g5Brn3XB1qTtA1BErFt364ZygXevK+Fl5qCHCSX+4FeJ8+9UppXhFWWxxIpmcVchGa6j/
O5L3usBzxQgtKvZK/Mq2nfTPNtSu8zy0p/kL9WjOyp5BeKkkEK7WrBXbcttHrinIADHs2wS820TG
mYdznUORUtPA7+OmYpSnOCf0J3eEaPvYKLQ2vC/qY928Yw2mAR4O88xWLI/vlOhrc1DNNcCXXXWM
430cxkOBZHB5jw9BSGXPs7YvlHDbnnOI5nQpPeKQE/o0Uj2BDBnmrPKz2uV6T5U1HVOHD/rx+oA1
rAg1CrddeoYpsAtkBnExe6xoZYvLS1r9WrwhN8D96t/ysjsvhZ2icdueD2nUrtFiHjWxn6HtDC1x
O2cNSYtHETc762quIhy8utQgAyaBH6yOnnR903pgbJCzAUlYcK+qFT2cNQU9FUZ57J1Dov+myJct
MFDZVph57gdadNbg73jLJNuHoZJCVzW0ICP+Bn/NV3I7QR0qjaOPyLQY0w7+4ISMtD3nkFa2Me8v
0vd3kKFdvIewvd4eJJW6YK21gfFqHPGnZNrjZ7kHJ5KpYI3xaRJNoukfuOZBG2k5CoDlTy0zDKjo
IkwNDdP4LJADer+KzG9SBkovE6zHyY0GVJOWbcRvsAkYk4V6g0hK78o/ne19Fg5pvFeGi+OdaHrn
NZVWKsK9o/vvCs9Dl/PD+40eU3ypf5y16fRNR9HZpUzOhIQsbkGKQwhemBUfhbZGk+yOKMwHMTh0
a5su/taDQE8fJNqPpnZt2gKNuBjhHQL9usKfgjJvTTALDJjLaNde3PwwscrI07f8JDTTJHPnbOtr
/TGE0vhsRPmK1tjyoCLJ128ZKRHprY3ks8Zo+ni4iiSOHuXjuRuM6EalBWVOgNckrogyPQM5K3NG
qgPBeYz4SBczgWPvCJqAXqirkfWEU8qvv2BC0b8d+D/d1s9v/8G4QLG2+d0e5MnP6TEK2d7fGxpi
d5ngVlUye9n7SrP7eLhE6oXIWa5mzj81jzAAJzWFDwu1s+LbB5aML+BHXCIiKkXRPKACO9C1L+6g
5xhwEG7Olr3jLIqk07Tp3ZrvSqZl9dY2nwI2UaSynOylF1H+0Ctx3ZsSsNFiRBoPYf8fwG3ibZS5
Z+no15OBhSXGkjJ0BTz0BDs6Q23tPf9pTkS2z+Bg1mymAOqHrG1qBG/n8shEen65NXeLAuurqIXu
trXiWmlRhY1RVcaN5xecaH2KAP1fx4i29n1pm2D1N1PurhJlS/kEdb2z7nwCwKeHOQZZQp3RvlsZ
yVrqeBTemvEn7E4oT6/V7CZ0xABF06Sboeh9bTzyvi/CFU4iHm01BAaijxBQgMjfvjCSkjnKMzj6
hX+cRapGrDjiUN+obLhmt2FiiZmtAs/0kvbBXp0zq/YYsaAFoVbRYDkSdJDB0ARVPcdV3w07bqzd
OTp+GacOJoUPqhBPYryOiMlGt/nrnftKBJl+yGkmrc3qMJKXliP9OALRZOEAEjC7f+NTMoX6mmfc
A6UjxcmcsMDfp97JfaL2PI9/fO6tklwmc7vwLf67czlBbfY0V4xUE/KfKr+4usGy18NV2m0shZ4L
+TFapXXwz58EJrcGrvlmk7jhZ4kPF9yH4H6zOATU6oOsMWogWrJJwNx6sZ1yABqoECxLzfvBq5Vh
mgDjo9z+urHXXcHIT//A2SNISAXKUjJF3a7qUstNIdZtd7ACRgo51Y3IVmoiwy5WVsUp05xxwBaH
JQNik3kxz/Up8TCbmJGFJ8aLoVt4AsOLTNOVasI+t7boqqTWAeTkbSz+bZ7AvZKwp1xQOBsx8Eh0
YnL52SbGjYfSWe9VuEHrcyu34MCsyNeMy0hdHYQkesatu/F6/DIf5dwFZsdY/AtLjenMI34raI+4
FlvVG1/JHHZM70SesaEw2dyHT+wgduXAGpUXWlL33pUW0KL61TiMQBHTOhTheidIPx2kmAWGqgIN
qrnqKBQ0INrXCnKTwOrCvb41XeucBncAVSpQA6FszIS/xlYRnlQBhpLkIR7jJgBt62uNKf1dcM02
MzSVMNkUJt05kI0Eo/1Nl3MtAMxn2VmO2FzedYnxteGlHGPAZNGIwua9J80iqh02ND/5E9HE2Hv3
ln8eWnsw286w9EWIZKQJAU0Qopwt0KjKlqqyk3dKKCEiR1jTyOrIjlCPib1KlqCQX9HBHcKC6HB2
rW1+3kMSORW52AhQld4FWKjZ8bPegNH7/7C7b8otlRT9vNQFpGPCwaCu8ci/AnXyLgW5T1A1DK3K
AzB0Ow5HC+1F5y+wim/WMZCH0n7rFHWWZOmmOagic4r/ZwFqcYPGvGavgnr31QNeyjLu5dYxl/8U
Z/vwwmDhMKv3UrCb27U1iSeeSdb3uCXAV1lNvDqRRc6LR+Wdjx20Axr+d3Zu5FhKNmvg1bgKhr6B
tR1GoXRUbC1lAKPAkXMpzJdMf2fZRd4Rz2oC+IjZV6bFFF+g9IOhGDoarWYfIUEXGDJ/HXFsfVSk
NqtimvLQtAdoZi2uLbxkulaVTpmNdagapjucQtic3b2sNCUOJWlozGK3egnfnOzYi+qY7xG21iFQ
ijlmcn1pdTG1eEZ5f/j5dEi3Ymi3TER8NNr/uMVuK7Yu1pK51zbUR/PqBwdOGBu0M4zFSCMQdjIX
J5F7IWBz533bb54rSELekb1cGJ3Khw45sJeWnqhsrc0aZ1foDqjtI4JQ+LuVLoOMiDOEPtZfiBQX
r8x6+gftL0WcfqzGcBC7YuxUkRzsOD5uC89f2pcV3qL88cnk7gxFqApvTkdlXFU+OfOl47Un4flf
G/E+2yT8jCy+3pgJyIuZ2O33NB0DNL5v+G4W/8OouAUJ+CunokaTWN/Y6RaVI29J1XVCrwGaIfjZ
hoJnwd+YBu+MLkp0rAL27ZhZjclhZqhHNB7tZ9vq1YTm2VAqX255PtnHn4IeTWlVt5ymoRuPsVgW
mt3i9s0HgWhs28WI+eiONV4k92gQ7Ztx70FXK59frEg0GX1Z46w1cbfJRXYFMWdoUMjLO9i2yVHs
dyRQ15lxduOnpVpuwyNYjVz294PQy9gSGRhL6grWg49OOMUTDNIfnTMj/NQ67GHqQHevHSC6tI+w
k3cgJZ5TZLNRV+xv61ejM0jt2kYqS/dFC8LKaJ2tcSF64JLn6LpJZwztloVUT9encNlEDieLAiwY
3CAc86Yzgf4S9MsBTkGC1p/Jt/JPhuQiUkO9N/kY421yloTnPvzzdKYE1X7Hy9tqQ4xNT0nptcQc
+Psbh9yZJE8vYUgd9dUHsS+Id4QLA1a43ECPMTzNV2lmWDapE0WCCXZsVGj274JHKPIn02j5JzFV
6job/0Ox5vbAE/sXIT1CP3RuBxR/aF61vFb5T50ZsCBULqEt2DIX1lUjdHN+TwMvHb08Es68rF1S
Zut6QQsAK/wSv4N6EUQYSGV/+CB8J1cKlAbXkDMImMffcTnyXnMMWnvL88kB75+GXc4Ij3t/EDlu
6Lbj7BFWuNYYQscRp3tChiXsoswLiBKNYZsVC/CrRrstwEyDz/U0ELHqB11mp8FhCJjnUAiyqXyy
jIkK2QpdahlnilehEaLpmikPLkOvIjRDXVLrcnKIh68OZ0VKJFojUKDqhwav/eno7Gn2t6MH+mnh
yofB+p6AwVMh1jLiDS+9OleEbSsZZU7rKiT0j+V1eIU2ES1/a6qNGCd2SRtWxpt4i313WSvHX9kg
S5rr+qN2UxQ1/NCgnDLJyojOUzmTzBc1x3w2kl8ErM90ms4DQzPzHRW4Pc2kHTANbTN5JLQ/o4gX
3rmWUgFr2ohbJZIr5rLgQC+M7QhG/vWXI/WSsZjraELVXUX4sLC/uHFwPMAA043T8XjqIvc7Xieo
vYK5AUXrTGUEHTfTG023lQap4EJy6wlWzx2RLiadg8sQOhTcamOPCTmmwfFZAKHEh9HJKTMrNR2k
HqgXvAEpUpdARaYzDFAwwnFu3pNdxDu6dmr23fpOD4EHVfZC95qEtBucwumHAtOiouNJk5UYvHrH
3MowuTBQDx/BCpyOV0BQOQJYDSv4UgwqyUcXh7p5Hzul0R3L5aWVc10w0ToG6bzijr0wyU6jk3Mk
7ID3sIM2/IBKf4JciUdk4U66vHqFs99T6P430BO73EtyJwhLq6Zu9+vWW3dBLsH+3c+s+p/gwexc
CRVa8bBbTDn7jEwzvt2zoTP1kzXSURXrWWBzo8KY2OYBRCGvdmjjMevq+qZQ/g4YHolbA1AvK30M
W7IZu9W092w1+8N1syo7xr5VRIM9eO9CbWZH2Mdf0AjALFTPz0fkHanZ/fdb4GBpCFkjyRiKSYw6
MV0kmmKjI1dQmFSLD+h/+aEAOkewgYU/TWZkS62bKKNd5IKg+CPa5j7RBHrHnepYJ1zL1GWgxFd0
uztbrwabvGbA1g9+iDG4U0tuVQIHfjyaFlkwJt6uQArrNXT5bO4E9o1e5lqL4p8hM0Bq9BncceXD
WtHr92Kukdl8VbIirvBsHQDkRvkMC/I+WaMpImYu0OELBXmtibLsN787R3upLOEfw5Z0nHIrGRTm
pZOyq4V9MXILczMh3B77E4+seaw55v3misOmk9UfmiD5Im5J3n9LeoSeBmgrL0h8FM6nAvkQFwZo
5IRXvRLHf8CLpNIPDD5+SLWEaq2US/n0c18Ep8UageuyFi3+gTdLujPWPqa2UwI13DAyQIrr8Afi
lLn9NjMRxigXTZ3mNAUEsuXic+NwLkcXgdfrZzFHCf8H+oeUjHxxNKIQjlnYgkCjd8T8Xp3iCL3b
qpaLUgQEE1UdkT+6BkQ3Rb1VjIHIhmnbyjIgqdDC3QBDrLQIUYjPNjkvNuyP1uPZ4zEMk/jVbwNn
0qYOVuUTeXnwz2ch151fZqLrOxOtzUtIa8KKGvBU7iYMG2EWzoPGaDGA+fzoGVHBqvCeIC6pfRgr
ol8ylfdbOWuPFtHG91lmUTUqaGfj3VUAgasR+6mOqjjrkQT4pBMtIPDXYsHTunH4KvfRghLUBvyJ
izPkCR+czdQDmDW+pR4M8H+TUbook5bXMyk1OuNYG0+iNRuG0c7y9w8D6Z0O+AKLrYlU1C2X8OBC
/qrb9CT+A882y/RA+pz2MYguH3Tj15hMasgu/qD09IwFc2K5t+qsbivGm4g7FvvTCdFZOQs+qsFx
DSzswx+PcG8ua4T8Gny4wtkH9IBu3rphCvk5g8UlS2SplP1BU2gbgtDQL1+WjlPeJ73ajtxyPFhY
G5oGuaTNbbkipibMDkVZ744lqw/lVjUzj3a0wgcK0YEtn+Jkv6KdIVoLCEbnsmW+vZRTd1NQDiYu
tnhAy9Gmvj9bdpbHs/XyssnvUMe4deQEsN4/y4u4xKyEXdNPgXnQkA3GEC7Hk0+9MmeGoJkL/4Z2
tGvzKnF/p+SdDA061Ae8ay0leO3onQQoVB19IlmIdMx7mLjIfHJesNTe6I9r9PT/Ds0irU6RhIDh
v9ZaJAX7yZATRVtatb9LRBeutRTnZp+PybbP3AChsYCygsPSVENlQJk0I9SuFigQE8IxFPdaYFgt
fouAEysvZqzcbJJBS0/2GqdT+/3UQ2Z7WdWfvKQEtyG4MbVKLU2/1dOf83sMnLQW0yaPYRcPtqff
At3tXsC/+yiJ7WtGUoBrPBlA3rhy1xoRdftFDX/GAUOE4XloBgdUjzAV+K7Ub0GZrynTku76/RCV
SDHYXyFWsXLK8djPjwGgPL9K/wWrLUQfdCVrVxRNs4cy5sbKyiQ/xefR2ZM7GYKPw9WmHnW7ZnkM
0z4auijcIL6vQ4XZmXpJGLpkDgtkkg7Y94ngqsALILaX4Avlmoyaosqc+LuZXppI8HSvdDMkP4Zl
wp7ZZ5duPLonguKoQ/N+L0hRJX1kPIAe42fh131nYobRuplL5cxfjIyFlZ4Xr7RBRX++PWS8TRxY
r+CFP5KwsHeWysFo/58Df57P9xVLnPMyW/WmMijVk3pWQUowbLFWd/3kPlHJtZnw7cFCOWT8yzY3
QwYm9K74qzaDSGDS6JCS6NJb7AWiDJpbXplrw3Rr5q7dBaBgKwK/tbdz22OIr1xIytDbtp7260Tq
sFo4zVhBKuqs/FB1C+YbT7/QC7xFDtcRD/2NC1VBQTh/I3PUQYFNBY4POBesKfsg/qy0NRZbpTwh
TbAHIua4h4MWfNuptLU7PhmQ4p4XcA1VDkW5ToQM3KyP0gJboP8QT/J/exrAAk46CCqaoxb3j470
yk2pPyurYpxAGdR2RPsnwThhdcKPXrmahPggkP1EX7pjT3Y4O0HHwy6UN3C4Jj8cIRa1nGQiB8rX
gutUOgocttP7v0222wv7MEEWpuPBxt87Yd1rXrb39BMEh46cMGDGuaVOZ/W9PPvZ9rFsO2OIuqvO
jwJyPmcjhiGqwGEqc/d9UEefeS1tTYqANfowdQKu+oI72eU4us6/y36zzEjEf7DW8z2KfycDXYrV
7L+zmonzwXp8dwjA2qyApdxznP2W2YSpS0gbxkeXCHzhNkz3S27+5oXWscId1d8xCE92mGnTmrp3
lxvkU2etmMV/YX+r4tPeeAezVs/M696o/E235LguyRXMTRiQRySkUrXT+KAyHyZ741K3KwGjlgi2
JsyHP+RPNxhTFGRfiwMFaaHhcbELfDvfuC2r9KjMLWhleadTmA3bAwwM41GhGn/fl5f4USO5DmQA
rHuMbRRaoojHd8tC2xR82f4shVcclxE/vV1ETxfkYTvEnw/+CBI46HHr6g8EMBTJB5kGxqWzsEDY
UaDuCxoUxagbzzSE/hTjj6L1NOhygugeZEVZjTKsob0PvTnWj1Fjg1xqi3u0g2phmTX58LGus+Ol
XpPzd0wyvpdwAULZnuDunCuyh5yIAC6UTOVxBX2DDBVkDrRGWaTujX0slcNnk77hCJ0fnIThCmvN
1biE6AP0HkPzgcA493k79IZquhyvSFkhUHONi56pTUDArjPFYX9vKcQEbDZKcIpy3iPIC5VK0eiE
YFICYWOA4P2SzxVNSI96m8GbRko5iU2t+TKpUVJD/EtS8azpshItshNpniJx7wnV2QKzo1+t+SF4
4Xpvp3Vc2CkP12tr4FxassIMQiIL7YgXP8UhW0bBAljfL5UA/3M2tfHnpQoDtfZ/yG3X2GCh1vBc
9PzpcYZS45T5htBr0F9X9WKuBjyKZN4tjjQqDziY+sNn0ySCkLGVfcUbFL9x6kDzFPqJYgfoTtJQ
ZDjcgbcx/BPri438uiHZN4IM3GUOqLN15AjAHTI7wfSlJ3QcHmbW5Sl9AOTYMNctQNCXVqh7bGqQ
kyd89iHY4fORI+EI90SP2RebeChcjEnbynzBGT0tEg4gRKjfpY4DQhbJHeBeSqlTCOlbNWseBJuN
nv/lo1LSUZIo0Z82JF4vD4/ZkfcXHAIWJvOhDviL4Dm54bDW1Qb1JD2kXGuU0VHdykt2ucUERUwc
2XRryyD2ieix2rD6ajyM68JWdJUPZevEsn/VRchX9NajeDGbr6WK4u7jFCGjGEVhtxGkP/xQx82y
bt9xLz3emBXOYmqLWSMFOW7j+bQzgydzaEQzNjDDYY5hl/r3gOb9wpg1u74ZmXOoVfZdheoi06Nx
+e8sT9/5iaRMLIosZPzsFQ+RAIlKJ07FDRodJkR3FyBpDnwG39eXQdelAPl3S2I+RikcWNDV5R3y
uVSV+3yAcMHKa7ZM2nTapLVKQFWBg5eRgrmemQ5W0LiVoEJEjm/pRPul8WwmX0anFsXpCBhR3PLd
c2bvaaneYIOj5vuWmn/3De/eNJ+onLKB+3hLf59NNDlLr9yPkh9g+qkur85Z5UH+h/xd/KiVC4Fd
UkMmg6h4/bM9Rur0UCYBxZ7U/Jm6OaEPUqkgjFvW7lMBq1Ggu/FepY9tsT2T5nTlRJy264HSfiax
7a6RxMj5eNW5fS1kYEUoOA6NjKFeVBwZJbjd6+yQ06plKu8cWcfNwu+HQnxVjBurNF2SCsO1NENe
COH8jjv5L0gQqrV1faSWjD7Kh6Qf2jbDRaaARRixZumMnj5BJdk1lCPv0QEz3czaLV1DBuZMo1WV
RzCMPypTGbtFFi/i7H8XZY9/iKmrBff8OJQUZ9jC7ds0U7sDXy/3lEAdX0Bh74A2Yy+AgTEnLCNo
xFRyat+BFs5n7UliF4gSuByUl9UQJhNYoWI/rxUoMUd8WYJRxMqf4V12pOtjBoYPiSDwC9exQp0S
XDyDuEx67+LYVfe381c7IdZ1009aOl28mSJXeGu2gFn7P6vBQTUKGFiFATN+s2X29OoG8k7NWAi6
zRHlItb64Tib0lD/27ppAjDziKw3co8Im+2OAg6de7XBEyAxhcwBuBCwi2MVhObV52ZGKnrtoVii
wJXyuv7QMIcnL+5U4ptozf/8qZLUWitO0yenJBY8A6ug7XtTTpHaNTqn4P9tMHFN6kJF13RqcHmb
Hafz4Gv4ysqvVOG1fqlRpmfiXe6SOdBc4S7ct9ew35U/9BVA6u1dU09Ix1OUcbfQYSE5Id831eqX
iXfZFUn3Y8M2D8xRg2WLkmetD3lz2I51mDO4s+HlW69WAEqegoyLj3P6E36/Kl7J2ehwOrGFUeFA
Ne9dycr/A0LbcwnMsCf1TJiPrROPLbpItiFGo0BL8yWzz0eCDlusYZBcwTOXwAmVuHf8U+XkHIOQ
rryxK1ziJkl1Ef+RTctKKpt1R7oeb8d+DUY+R7BY31C9qZUHaYpDJ+VmDZRrSp3j33sBc2+PiygH
l4TlHTOiOMVpI4y/tW5xZSw4KSg1D1eNGTBzf5LZJFaVqvovTpU8m8Ov3pPqhVgSa00EUiaH6ocG
9Kk3ZZ2kd5wf5REn6426IdKMTOcUZRnHxDn+t5P7/zxrpgLie1P2ZMWeG7/iTzxkozkEjSA0yPO7
CkFsIIs4wo3/Y0FPTa2Y2AaWv/Umb94+NStop6bluUYjiEEk4t5s6UzbExYhkxJZYNtpQAObk/rK
47aV0AXol5jBRsSQsdqf2qr9Uj9gOHACkRT7QOBynBSayVUkZMj9bMSxoR0BmXa0JqFzIQD1tiUw
0AyATrIH8YI7RDFI4EmJmFpbUhjfezlJRyNOAhV7LZt6cbAyEDO6bj8/h1eUQYmy+J1cH9kdbksF
Pq0wL5hBq7+tC1YjskuDrSvpFCII46b2ZzvAFCQC1FqSQnIKI0aRPK5ZuYn7Uh5EztRljUcqpKHK
PzJDZrqF5GoBe5aGdLqA2Nsjmd1KgfIi4YkmM6GihE281KHdb178ZbKh2Xfqq1CexfchmvAZgP7q
004nJjIEYE9jS+eqaAKUUZ+x83a7qd3chrLij/2MvQmdzY/OiL6iW4/XCLaw4lxhylVoWC/ZU2Da
zbBhe8eLtrXSd7FOMhD7iCXQD5sak0rcyeAv8EbRgkboTiTkJiJKKZPscZ9HISeNuypsw50QmB/r
LAujfqY26Ia/ncG2sR26X34OVfYstTCc9F4f5hyrGDK/5NHyCXXU82U6T3HRbtl6RaZNeqKnJF5n
WiO1VpS7c1sfU41uivE/V2gWbFe0y5DZ7QQbVR8cnmJDGr8KdiMsFzxultKykntgP3fvXLGyTCpb
A424lyEKQT8nEyzIUzZwRaKx3bQ8ur0JPwglOZJM0vmUlAb56cC/kotto/dsutiTcz6/quG6z9Yv
1riKj1IyZUkaG68gW8gXqM99Fw1Nk+vxVCQI0itLazyBcqHXF+t+KVa0Shee40lhGme7iZ6aKf6n
+WMnvHZiNFJmAjZqzyC4bACWQM5x//vKqe1uHdPwrWZgOYbzkGHfZ32gTedJv7H1DgVA5fx/QxJZ
80AYdYXJ+2eIZfp0e9R3aCyARAxC5J7Ql1rFmskn9LpBaLrtqw9aTxGETjhAuqwgALJveF10Xhr/
lFmPI9E6RLPIRtP40FW6NylpyRlMgxSn4ewdB7NjaArwVQKiCBFPooZjEHwreCkuiU3k0mBZtyu7
GCUNPRjDdJfWLkzBGRW1lM4USqgNcaDKJmaviLoi/RvleY99QtShwC13eM/vFVMZZzl/bZebIwfG
8m2MV95Z3RAH2ckBkEjzyHDNeZTE7Wqxo0vFpN5nZzI1txDCHFRJejIaa+TGVKZQv8mIMdjInsPY
g/mLGI90NcIlz/7t0TYWN0SIIoMIJwEI3FNP+ZL3UaqHi2DrfAGrfr94IjVDH5UIb6Lsr5unjvW+
MwuS4/UotgmM0zyerpaQee8cJczbPx559a3B0xF75uICyAWH3UWqdnbhTlo7BG7Ahpa/Xc0GEmoe
LqRxm+kQDaEHsrm+m0F5AGo+OFkNHKhZkuE1OEYQmlicQ613sM6lQ/QDnzZsyLI9aVRBSXn8WDJa
7+iG82G3d6MUijvS/tmxBhaWkuvWgeTF/uLBMdEDjFyG3DOvESoZqcf2Il1ce3dOsuh9FNyw6NMz
6p6PyQ38fl+8v2yzA7dVK89M9jfXo8rRwQqacmxhMqv8q1J03t5mTnY6zMaBv8LuHg6dxwvxzSLD
xDNlohbwvdZpxXGRXvBmbWC62SFgKXALjKVeOjAQKXxwamh4A3Q//EaMMEeeYgVrUkOTfGnd1CDi
jyOE6uRY9sltZtIFTt1+XCC8Aat9lrOcz/iWXn/9U/qvv9Tg5cH1MdiQb/qEiqO2uA3NO9Zlq/aL
bCBGmhJAvJsfH2+u53p8O9c7kGvy8tcoMXDw95WvG7b1rKZ6abmzU6Lmf5uLtYm4jEBVFGlHT52X
dDaVsznlZiQ1TZAxig9vShPQUF7+tS0oqr9fiOWueKMOx2NvEZdfQAuhyYd4wUIXltvTe2BwSyQ+
S4QfU2o9TrEvAtWH1hx41yDrNg5ft0WDOLTKqmH65tCQvDa5BQdm4foXk1o8beiM3I4SdBfOONRR
lKVeHONmGVYDKAsiHZp1LJsj0nfAVO9RUEpa4pA6fJb6rpRDsTyQewpnfJ3OfobCsPU/tfAnHUtR
6hFWm5ziuN2I6Ucr7Vt/A0krbMp5yGyze/qD0Sz2PEGsTKAyPSSxMmGPMy/hZa7NyD1R5XPmgyCJ
yJaFHBUAXk3iPXVkELi+GL96NeeaLdKFFc/mCxUUckvtulPIlqhP8LXIixtx5mz0LdFMameHwCVJ
IKaiHnapX+2ttBMHrTaX82nkliwTnyzzUQW2ImRIb4sFVIVAs55dLGeTra//kW3Wgoj73B0tbBVS
XnEBq4aDhdEl64cVIi1bieSiE7mjXiMPGjTsTS88lgGRwp4JpvleRjHIVEFAqTOYjDqa+/f6AjuS
Reb14TZmf0/ihkwN9vszamR9RgP3CExGIG2aHxfpGJXBkRRKvBxXqc4sTK+dENJWpBp6ySdW71JD
yP6xtK9krnmBDOmh6LXLGmR1xqzCNOrdfKPU3pF8z9e2Sgd4kYXxqMWRR6V0PL1Wb08D3HqKWjAs
JX8a8bCcoP0EqgN6/iA9/TRbI3FthCH8zyZAQSubXnzc9Ud+XsAf1l+6VURBnlM2JIfDiQB35f9h
Kw6zGvvSWgiKoSf2TlA4NChbSfAwHEZ4DfXlj/AtMtIwIcHwkADswpWH7/NJMHDzWFOsgrdu4iWm
3jjlKRsk/TL/jLycPxqW1ZAmW1Bw/tpMycUhwrmH6fB8hJx2gus91K/0QWrmshewrVhA0vV9Y9jv
x2xuw6umuF54zsXnh2xPvoyM10JYv6G/gePdT2LuOtcGnQveVzCe7bVdOllh3H49nepjYlLU6Bon
dw0jU6c1VkYvNvY5gzZHODTH9aNAnTVOD7n8HgbzS/Ul7sXLu/TqF7sgAVKNiOwKfvUmJUIqqUr2
zKq9QKLxOFkGG8G8swhQjR7ATVD+J1xjyfUfy86T74thvoXKl0Q4Iok3tC1sqU0VYsGQ5O75OMrX
+5EZeOS0zWxIQ9OlRfaQqGaBCReGAzJ/r8e/dvI+peZlmFFCRqleKZ4nDcRb5SWY0lhcviXOJHnT
A+it9znzuRzoDg4LVZ/RjXOoHinRocExl4htVSIIww9uJme2+3/J7tn7ZTpwhPMhLnUnjuTC52m+
OgdfUyoSMaZIW0MgcqyHp+E8xdBRxfhckUbe0HNA1pUfvlgqlAJqhRCcXbtruht41rV+8mYBQBz+
7QLoDlcAeH2BXp+pOpCS4iifmIidmK41A1n/nKu71iS/EYyI/qUNNWWuIUYgHZlh91XSVF4fK2iZ
4+geQHiXSe4Qcdq++p9cxb1uMvzrs0OoQhlZTQ/dCPN3DrX1Rvq6h49+DNsExzvk7B6wvJHvZho8
LNb4gGpDCJMUIXxHY17Zx/bgcvXyDLONcqeEt3fAZAPtLKJWL8hkPv5YCE8sSJWg3JDpD5X7dUSI
6b/RSgzBKNlGr/LKa26v10m4tFGbqO40gqhSKEg5LME8rMUQ08ojk/z0sgLgw2Ml/Fjas2hn+8j6
rXkN9EUDke18HkY3OM8Fg6OtUy2Ilgkk9OagJNTocodvKHOzcCx6VarjNZCpLWcfEuzfcd6Xk3jo
fV9UK47zLPzALnWzCAUuBvJLt8RQfTcNWpa9vPLHwSPLXmn+6+gWV1OmppJy/8KcIPJFGUyjiJ2U
a/9W5GhaUKxiEwA+/M+yitPj3WKk4M9mSA5wWZUQFztioM8L5w5uYq+whXKFIkWd3sa4W9dV8PQZ
kO9Y/mD185j2AhxGvXgff6YNpqAsM4PsmGMQCazY9h+m9zVuuLK4ic3PJjoqOFfRvoht2rj3eCTb
7/5jdmFFnMcdSJQm3LSCMwaMhlGxOnu7t9FypoS8Ui3t1tT6LU1sI6QthJL5ItV7ZH/sq0YAgaF6
8MhwRBK8rzQGBsO0JUr3AKYMoldtLXb5D938rSG3oXQuGxVnfzig9/IYFPT2KuF5zTgMPCGpdJYo
MECGcRmpvPvLhv6OFm1H9qHtDOEt4j5yaS3gtvB+BSd2sk4PDnr8SPmDrZxYeRpVQA6vJZPyYQYJ
DBHLiMR9k4szlg5Lc6aqtLIRK0LanaE0ZIonrtX1uQWf6zw8Lil7ymk6Ly65LgPbqxvxi/QeUHKo
AUqvazlNWKOiICq8HUPG3k72bEg6x5ukooBQD5qb1gikWvPaULnnqZvLPC/+KBPCYlnT5KlTLemQ
c+IyUFFYwdPT1n38PcXWrHkWqGDLBpkfNHVzozwafT4ZKuqIHA1o37dJpEfBn5+jYZxG1zbH376d
Z/NZISMw6WctMYREBrM/pjW/14eSXxi4gzbYn2ruGeHalxx+T73C302bjV4LSujtdw+w5QbhjpPG
KL8a0gXPl5V2Rlt2F1W/WYkP+mWttFO/r4HI/6LB8AeXRxc7LR2ZBwqTangel1xLYAsKT3vzeboc
dOm3Rq9r2PpZEZV5pQ7jvrKe+yYSN4diNAjpyqHU8+N4o51KL/ofz6RFGCXU4PAE8fOHArjBCNlX
1OrKa7N0lImLGemCy2H1YKUUInwchxcSoBgRE99E9RcUUVEHxl9vBm60SIRfMdpugdKBg28/sxV4
EWCimvc7igRhRmm+WY3N9zE+3YUcZNaf379En4D7pkt5KXDLiJ2atugKKtMZjhZsm3htslnzigMz
FBd6znfxxCjd98JQzhTgsrFSI94BYDnTGYQHxZjOw9+4IUxgKMO32iusY4zDCtbceCdcyU8M9Nva
xbeUA2qiIvIkmDI2TDS+BSJkW37mlHdb4rw/16OPnP59v/aPo4v+982lyOLeFptYYaReh0yTmSKq
3e/5vhaLtLBUS6RzcjvuURDyyi8nZKigdrBl60ZehXAU6ewq1HrMkH7Ur7WxeIU+omH/wNx69xv6
765QofYL2TqXqMf1uXPjWVHcGLV234sXrzbwNE14cbS6BXiA6rLsdmJZQAg3At3oJTBaCO+/S5sv
91Y9sof+ys+mLSZT579RxCIWevLRJabAYPcj46qb0pCs5Cp4iVrxS4Qg9MO23mZ4WuQ41w1AiQFJ
zpfhClu12eLm1tusBQrLELWvWzVYsRr1nV+lkj1HzeN0PYyRwuYC2AwJujJCPA0GCCnliTpYv4x9
+sWfgvCGhoRCQbjCCeqXOBtecBCDZ8TTWoWAnUl/be1QugMrdKRwgERGkUnbJuEddjOy1vXLkMxD
gjqkgQ/STGUel1qbZIBXeyQetrdwTa6pV1KWcOMt6rkINWYCUh5jFw6SxN5kuleB7lCz3mFxJ25U
KzYI1/7LqRjVroMjBpoh4q6RDP5hK+B7Nht6rXjp7veISBESgNuEu0lSRnmi6SbAsD3xocRNKDME
YnZBvGppN8Jf8nz8bvZhF20cqHCVuLzJG7YFn4klYVi1nXXsyialxh5pJ/ZOuyNgYIEGaRG/N+Wj
w7NSjw4CLtgTx2XDBq2SA4Tiyy3WoWPLrqZNJN6lfnzz93uNpN8BYXrLsxAtb0lhOiQ/0yED1RVj
q0iEmX8ZwXiXbBBno1kov39lu7j/EHb9OuijratPBJb65rLM0dYS8QZjlJmvuKhuJ3CJKcCnFHHq
SpKWWcFnGiBGlSncwKlsBKUDhaQJsKLUSy3AOWcN0a3Ig9Xbx4YAXwhCjULk5CzH+CyyxRJGH/Jd
D5lhH9VnC/4YmnUUV25Z5/SaU9sdFnFyYrbcXNmwdiKsSKpcJnYRqOO0o9def44RuXEoFZ5aB998
z4wguaVqibJxL9hJb40nUActrNEylXSrARKZgvQ+9BJcF3rgfP15E2W3c3PGXP2C6Ae6XpA2BqjQ
eP7nU8P+KmbHGkts4SWUawYG4CTvVnQMptMZAg3WqW4R87gOqkiujb4nFniBvlxztVzexO7P5hwp
hQlQBhbl9TL/xQkbhnd8cVLc2ezVXTtrqeuJn1/J+2MggHqF1X6BZVYtUf8ViMMyyu0soE1YtabX
1Kaf0GJW2AGPoXqWG1/BX8xkyy6+I7aJkeG5hRt2St2NcEvS8i/atNdR9pe+8+UZIBlTUBGAaLlC
SK47iB348e8Qbe2mOJgkl+sGL560NjGveT2j+k9imdvXkBHauxtWdAv8nJjjpFXW7MjdG5WySBxL
bN+Q/YslrOweNuQFGOlxenQN6mG0Lg3jfPo9Bf09z19wsfcLLWfJS6ZmGVioTzNXOEUTX1kLdbOI
DeJBDcsUt87VuYKcMOnaBnGB4i2wWjiNTMUQNYk6KDco1a//hx/4BX2EewmhWDpn2bqA+wcQJu3Z
3QeR92cafV1L6pSjJs4tkLldkbuyyoc3tAS/kuizJ4TmvAFOZC6Vftw/oeUi2tYTFf7yxaG2rmND
GNpfgTR4p06uuMXRSlah/A2AzivvJsl2AYppHUln3zoIo4JBzl864oxwBPYysuQEQXSMSgTLAp2N
ARTnnHLAilapNCJ9Cx//GTosfFDDarBmqlqhE5orZGTF9qnL6AauWZLvSKJZFqJjtAFwlof5hhW5
IyOY/0Ga09pPeoTX4Y0eAb+33frPKTJbAFE0m/3kwg7SbFsq2MKsIFKX78i+cX/olFdSwM9RDH75
UGvbW7cppJ70IrPTou/6XP2tTI0lXt9QUrjSRa1x9Q+qiG6dYOu+FVGZ8Hl/dONlhTwry46Pjhok
s8du+o3oOX9ykgjXQVXBrNeU93iUQi2tnD33BehM1WtVin4NxgCq1PNVa0+//W6vdpRqy3MHGwUM
B9bxluMvLuO8xJDOp1WUeYZpZbRm+dfXRaTCzib207POBoSAZdp6lm6EUf187GfkWA0EZNdRMlz2
zo4T9orEXvFVHddFJlWhm7vT4PeaXTuajrd2qvpOsvwHu76EHDKDFVE3DXJfe6mZ2ISriUxfgk9+
22zJI7UlzYa7bBSTksFqeh0cLcbetbf7iyxiKsFFR4Hx9eHRD3VnAZZixMjtNJcea1catv5+a7Fd
zsrnuWI/JwmVRR2LfLCPld9aKbQYOXuKc9rLQJ5Hn4Oc8JZ02KY5weDjkIlS9NrsnfjGTiK11anG
SWo3hCWCyEsQqLNZcBlWeSGd7nJIMAd7EMTpr1QXWE9wHmR+vmA5NHXlQoJJ0rg7sd5EKac7En98
sIcseVDkB4H6aPtuVzmAvHEeVEI01eaIMZ/mr9/XMR0Ctz58iUgS8L+Ll4kSPVdFIVpzOn0aulCa
SvXCrDkB8XcJshA7lD3Ns30ZUye57QSg8SpHTF6+pPlDM1IU89Ce2S/F1/F54hAyQqXtSrnMUi/g
QZvaXO4om3l9pYpk65qrBYxxWe9IWz/ryPFRvuJLkfihjf5nwNUM8rJPUVKIq0/nBy883+LanZ8u
kHY6Y+qjLH3FPJUdKTEPqa86v6NSddwTugY+A13Tiq/hiGcWQedcWscWduTBPpCpH+5V7min8EAl
wZxtXH7YzmNRiuZ8qbwm3Lc9m6V7VsWsfyuUM1QcTUC/H+seojpDDd0OX2dy9pC9o9BIGNMsfKKI
KR7NTHbzsQmqu+sHpiPFqxZxmIW28EmgZ5jyu9NVoIF8CAJO6fQ9evKmgXg7uAw02HhBfvimU5Oi
GdQdEOtU3Q4isbw/AyNSbLUV+rGd+fuRVC4bOcB65x4nzLayQAfeKa8Pbj7hB6HuMJZWuISnm9ko
pRcA3Q1TANy37TVGnt7XM43fBal2jI3pjvy9G9+7iBQ5f+FtKIZQ+gRoiAfMypoy9dhlDwgItfbR
jGLnujPeQ7CeIHFsjkpVYioLpI2lbt0CAoE5Nz2gN8x445SU2sEULhVDMtqPt6MJS0Gaw79zWnot
g5SfLtc96kExMJL1vrbkqAc/aa0AsbGWHKCy1iDMlQBOvAMQch2QpkgoSDMqGseos1Kv8hCnD90f
KUgZOymSlloAvfEv8xBAl5CX6BH6shnVVHtT3bHb+ytBt8tgIoHqE1W8yp4W3dgQRUGYHKJyZhP0
bnq8HcpPOWOWTWCACYBnEvFgoCNL4f09zyMEpIIMOeW6+LLhOu5xboqGLxINL/+JPlwz6zhlfivy
Axt9SVJV/NOTSFghj8hIyvXNEtGjc43SObIAOMO6ovN7g/eO7xCPyCg2pmjCHzvLjhFbTAkcrvZ4
4i32znee6DDeR+awVNY3V6HLhLbq/g37TsRpopVI+cqoiZ/6nui/wCUn+SSMMBRM0UcBYaL7Pcgk
tESXA2y+8bHm6KSW6g5IWEpIVmEvsjq5O3tE7xqJxemmBJ+DT+eOTi+aV2dNkGaQ1eOBP08f4+Li
8SI+Yfgi5zPGWqzy3dQEPc9yFigoUA/KTx+0jPwvOpCNsdv/s2o6XAHDSBwOXHROZSirvCp1o2Ci
Fhdy038YffbjevcL8MkryuC9HF7+9fuwUQtmAlRi+k461ft33+v3jXWQvyrOisehHFqT0y/wPHRN
TGP85coKx4ZqqMKNKhenCjXxtsCYF3apisEsWnFo0pgn9gck4OPBPDyP0Z+6wQZI8lODdccEKAtU
PiBgckak9I6I8FvFxsNjd1VTpJBVz3KG8wE0XSYPC62aJJfTKLb+jAiBzmrmNEFLX3w2hf+URMFF
Xf4h3zmXPT0Io+MFpnPoz3RipWcvG6F5K5rvP0Nl4hDpfwZi8Y+8k9lB7moOvzjBZwjU5Oqomva0
BGaMcikQaGyVld6LR9T8UtlIXPU5Xt+X4xnWObtvm/5THXcTtBHFuPWcYS0yDbjFIPYJOG/ODL54
wgbU+FEVJwFOoXbdmRdZ45Pb0PXRXA1zX2EcGAjM4+5nBZjQ/HvvXzORYI24Xersh4YWA2hYr198
z2jQPwesI15aVR7YZE8grCZ6NVHeIajaEr8YaUB4Um3UkmPU+0Dw/OAhBEL5mO9hF/OyO7Yo1iZ/
RfAftHtrh/SlZifEzxKaFdkzHNuoqAeJe5Luq1qkf/KRIolQuFHEBbWypyQ4tcDUuv/u+JMcvVcP
UyuNVJSklYS9cqWU0w39W5+TH3KaKpCr5WBbM3OMRWA/wNSSuOLP9R42G9Qn3/aXdak9XCRlQHwD
OmI/6OeipgsbVWMa86TNKznwr/Ezbx2UTVN7/hlrW0cz01QFOgQRqSm68OaL8jnFSdeO71iWyEYa
N72S0KCJwRGJoXf07Ttr2Uuvoc1fDUheY2f9C+yk5Ejl9ucadr1vyk1quSfIjIW2KBO8HsPSHUFu
8Md2z0xS/m3kpIuZ/xodj5Mw9EtlE80H+oJqBv0gOR2woFU5yQe74gygSQKD6+D1QjbZ62YBZ6IY
WLumPT5tBV87g/u1EkRAQ/jZqwVFK9LP6KXerlN2YFwDhByMIeZzx17x9bOV3sKI0Rxt//b1dAyk
EBnZPGPYAdRmsCsMsVxpNTp8rzFDpuYuok42M1ZSrkct+VGEU/wHoGUKw4KGAeK+Jb75ovlVawTv
oCS1BREWjthnt21Q3EW70zUyXv51h6xGtgaSDMysguS/baq17xmyUSbQ4VP+cANhrjXbRhEi5ws4
RmM1CJDbuQE8F3vxZEn+VF5HwjnF6KS+gcCs1zjJ0Pt9fHDqhm9zl77/GUcG6uCyEN+UYtMZmg6/
Ezbs+OjBwEb3h4Wrx+iVqsk+zTEFWF6QRISR5gOf256382lfA1E2r07+/iHYxIeFv1DpO6a10ZCO
tbz+dDzTkc/LW45EmZ2lCEq3btD6AYbFDk5Cr4B9ozbSUmkKviGibLV3U5+NVlrVaW216mHvIOvh
QBIaSpIKMzUzihY87OAZfwf+LSx6axchLm3CHa1RHPKZdDa/X+N7DeWQgEUM42i0fb5/3R6QqHf0
7mU2l4tpltgo3tcTwO6Cutzu0PtR+taySheC7aTjC70Mewx0gkCWi/2Rho7tw/jfrjGhJKZfZapJ
x0s3LbQBxffQVUcoIAdPhVEYpoqcBPYUlT70CvYUV8ic78R3UCWozbgkyqmqmd/UXVYkvu7r83tB
xTXqtqUb0hYU9Ajj3738/qXBhXuLMFhhe/J8q54Hw5ukqjD1GxezSnxad3L9UjC0IB/7kJBziiFJ
KR4bhrC2m+s0X18AS857Ls7jjOPj3XX1fkUTn2pxiv4Ql5UB1wFwQ5GpH7R22SsbzZk1H/GPJ0hQ
goXRAR0+P4wJdnvcoFkZLrCWxYveuEc6VEdJOSn+ZSNfWMFqeAIXg1ms5RRMsvyi4QSw2BLTvVnw
rQAy+GOINH3mXVjtUAuxkO7nreEX531ioUwUnsZM31Rkufww7GCvEiZWsXZyZhul0UN6fCXQb7i8
NU0RHTO67csMHFhamYgKr6sehZV64N4pyIxV3FuePlonUFg7XOgxo6zciK6wzjtvGib8Ak7cX2y1
MB72BG8Sdb7l7NrvFeP2UOhvMP500a7UTQnI/U/2oZP1Yoh5w6ZobBWYFpFqa0uWbjIQAq8TeoGM
3rN7/YjWyQdxWzD1Bu7fmbicJzzz57AKaDn9p3vMyGoHwJUmkEbBNcEIMK8nxjUXn7EzdM7AWVDG
yF1TwYfIbkyMxJO1NCTK2ojy8tofZyZ8IfUK8Ojk5vIS24APJqLsaY5sPJjA+MGQasCQDfzr1y6I
XS9ABnod5joZzbVaPzGVT+JKgN5Q0yClchNXDFIRy9Dyaa8C0XMNFnD/6ZgPY6KpFEsu0EEd9z5S
IYQNv4fIlHN03Mb6fHRzI/Txwp6kdPoLcD/E3P5RpLOdeRTwdhFxppOg3XTZ+ucCzGDF0lg7Shkm
uVXXO36aa8GKA/FT86GsUjSZmjCldiSbAg/CxJuqilwiNgs10/M+eJH+P0KTKE90uzC2Tag3qG57
LXlwhHuIF/RwI8O+9hG+L8PXtmz8OqpEgGhjsJ6qZwb8ajjX8x9NNXYTQY9Dc6VDxBjt7zKTkPZW
2qlU+8H0pUE2AdmDlLfQCfHXrgDoedX0KqMKzNU8V++4HGGtl54xGnHrzuXROhPH4ollUPMT1EWP
c2hQIM3dPjODTitEjDrmfH7+Fl1yf31hHfyQDBgSoS1TomAqo4773AWLM0wJ7hK4BkJrjMTVr3Hl
B9sMbzMlI8pDdhdEHzbLODkeSC4ZKkTS/wZ4PS6fqQvLNtUkdD9hVrubLvZYkrHShv3d5Jx5QBey
8nHS6zr95tFUDTLWM6KdX/QC1nRRGRHoHRk4jYq+Y+ci9YXsTgd7X0vDGYfWgO/ykFw9hMG7Fs/G
rPKO1FjNZGDptvXGlISPrlSYJCKN0A/icGKecMj2HfKsnRFt0gn37V+S/F/UQet1jV+vCbvG+ZQh
NYE8KpkTQTBtBN8yZEiRsoGjr0mxzh5uUIOKSwQ1pgzz4PXQqI6uQobKcna52WVbHdSHVs+066FO
CP1HvrYWlhQURz0sQpO6sjmkIxVb4CNeLwb1BZpi0Gb+VUyj4B3ieSArjseMPZlvyotjlnracmDj
lA45T3Zwn5Tgxcgn+EzHXvJuGoQDUSHOrDpm0FDi00a1ocR56m8RC0baJSeO7MMuT7eqMMnHB0Rn
VNj/itbi8oi7xbC66L8qxqzSX7AEDV3j4hnOxh8+51AF0YurrmBSfOiThMxtEt8xnEFo+OLwu5KE
XT2GU26MphmwaqsAporUBf9vx1s0tz0whXa4o1racM2fRUHeVTDU+drx/+pVX+nUvTRTnpSs47l+
OiwupMWpX95H2VHXixXtRg6cMfXZbw3Jk2WV8r1EHeTzstCOu8s0vmBrbU1tXTdC7FvES6RtGp/J
SegWml4C+4i4WKZN+IiC16OLAUbN/UjvIgTKZYUwDF+0eUnQx10MV03tXN1GaLEGmDJslCsYvtdq
MLx0/sUWkADn5lV4MhLPFzA3L6gRfTE4ei5rlx5yIc4Zw3gmGFhljz3F1FBpwu/E5q7S1hAjEprN
u3NCQ/3ZU8Zx5NCdAFHiAO80MpaTjrAqhxchzdDoZ4b+G/60F5bPORO2Ri/rn8I6LEa4VmGOXkRQ
TwvC4ufUybYsPn4wd3H77Bae4Y/17lwZfdfxLlohJSC2vkAZRJky5vVBHp95QRggD11LSulYfqcv
vSzM6DZ/ltoq6Ul427CUVs1sbldQ1SQEbB0HwipcQlBeWamzPUrS3TjsWTzyDualwKGu6VMgi/Wh
TxhGDMK0a3wwcvBN8GA8+dtJKNEIrL1ztGYdIn+LYpWJ2Q2gLoJPbMDNKuP14ro1HbMkClan5q3D
8VlG0p2u3hBPZuUIqdypfimp0x2tLFyb/DlXNkiRLEyEZq/VsWlBYDe7ibF3UGXhV+OHEn0uA+Yl
LCw1OQhKeRPp25AVz8igi+VvTs84q7kvGQACbkHNyzEMzfcFWC4hE/4XbdORi0lm7vKR673eGAW8
ku+BAH/JcASaeOTQJv2BmI6SDrsRWvXmeDP0DydJoIwiB9aCjMp6HVZ0Vzt2q5hyS54ee/EWVBta
D3YEDQYnsPa15UJ58Jfrcc8+7dY3I2ry+FpJSvcBoEaEqSyy0aTuAlwvMnsWL4ixHBCr1EgP3ein
J3XGQ14dYxkfICImIm/JOBaHT4UUizdePBTA+78jhpd3jfuDP6zwepbLEVBAwXiURo0pJPzmx1lE
P3rdWRhcWblOa17gNhfTumXgniehw3JN3DaBOw5T2wDRWMivtX7+OnVtUg3y9QujPnu/8SKZK2o3
bDPlXavTSX0rOBoBfayph9JZMM6Cdw9dY247W75qMwGu98mElTkhhmUo3wAWvkWsPoR6exneA8M8
doQZxBlp99Ews7sfq/0rOzC1fMDWgcKsc73J4Bx4LOagVnKmCvwSFpIbzy3imDV2HlUC+RkgWIfn
HwIQdcdhZIOIvtjC9pwtRm7+GUNs5f7Gx+TEEicZqDjRAYMmI4qH8yfVyFnj6Jx2J/rd4XsmDNct
aLMrhptNnNV+6HZq2+ua/3jRuR7h1NUGiIsstGqXiH0FQNMWpwF6PdDtmFO6mrpKRLRVbBtNMXTZ
zGLtrrs8Bmh67pQuXl/l7/ZVWgl0lLrMzIimHYG11bnkk8SrnfYyGwti4c+SmQaCSv2jfmO+gQHz
ukRCd4Bway5hDAJeSo4PtWG0000AJrSCeBWQSqE7VHSEvDIJssG6K4nay2P98C1RZpKxCPVZfi/T
1Sv6/zTPKB3ZOeis6P4TnQX5OEjxSrnX/ixkx6ZmomS4WsvMntlEOpYML48YFIHFSo3rjM9pMmGg
HQsBD/zwgS5rZeCBbbBUxtjYpHjh+nelAHzP981S4895cmqkmPlwj8KSe/nMEluXvR7knVNkWzh3
tw/CuT14xoVBYZXndqcFu3EZh/kDiuy6XXtUafyYJ+Dp4i9EVkGEqtOnNY2TrWyzrMqBcjWbeNAY
H1mdrTapgJQQN0WtovcYyZyLsRt3EozrwgID4MMGwZjXX6MrK2H3NTmkHvamTfOgmJhokghtIueo
SAIBKMBR9jce5hz8VzEFJa8GS9hCf6OYMHL2kBqDgAGDc5WsqPB9XE9T9MAnvrXy/6ub5IJ4X/6Z
0o5F5r6vjh2he/gKj8oIs6pkwoZG7JBaoxdWMhyXCQm1rpt93XHPE9BZRejClOSLISBP3Nuof//d
b0q/xQojejDfiafEb6hPsZ61vo3akFSMHscWToZcuwozpENse1MoUGq772epUCuJEJIaYlI65+16
PMpw1Ev86KHiSmuX5D++vEawT+kN6rRXV6/JVo10V/zO3hhSEb2Q5+AHczFV6l609iv26IJQ/N8M
wVBMIZaoloT/prRfKog9Dhv5PflWd5MILbmu2z9o4pa/aCfOazvq77FErzMZQgOr74FBNKM8LrHL
5y8gsZmbblR0StTohRgcVk7kBDYX2A0ntZLSzxRn2Ql8/nNa9qbtTatgfByuDmTfaRg1uSEnGHiw
7cSBrYmUvAAAFSqnF7aX47gL8mCKd6sXGxqvzadz2dSgBuEwutLYD2z4hnkOwqqBasKW/Ql/oBsP
Ys/AphUdWpXGcCCB+hxQ3X1mduKOnE0w/2HxjW629iVXmdf0BcJnXgVLR5ZAibcznli3X1i9kd0k
EQytVxr5NbPct4vAXahTN9G89/dMWhbs0YnRsMt8K4xWH/YZ3W91Yuv2d4U7xKWDMaTU4gU40OUy
Dwagh655KPG1OyKiYRdlvto8+qhEC7BZZgU38dwub+CHNEyTWnwtfzc4cO1VdIPwnfoLkroJlnuO
H6uxurN8BF4r+dykPbhyq371zidTJmOfJ/xNZeSmYTh69ZTHEM34VxECYqK42lhc4qiIi6WQ9/Jn
gzpl/w0eTMUA1jTTYaUy/kwjMVEU9knXCXk2K4bmx95oc/f3HKIZY7PtZmAxQH3/dRqX9JP0CDfr
axlWYvW2p/Q3q414iqQOX4Mxe6WMtPHz4lQkFwIQMe9IQOJHtRIaH/gnPvzsIj5Qireu+/CYoZcF
Ac7n01bzW8x8215Ap5AbAZwUU2duLRRFmdavuWvV2OJ3sfSxnob44ygIkIdX1Opb08HulADQNibg
hHPr1MX6HovkCVIA2NfuVb0OxcBFDETjzX+CoWek+WsbDqcX14b+dDhkDxbN53yT4VW9I9QQXd5U
WPhjWV3P1hKu8QBwOjSaKiKhi7amNLQL0V6+0od+VVaDKa5gEeeaS5/Xu4M5MS/aNKgoxx2yHmnm
/0j5SZsAe32GjVZFSRAXnLObVKijEVmRuh687TfdzgEQb4uGk9xCHKxLoioatVJUYuqssUEm+0Np
S531WCQyxbh0jlu2njKo0GM7NdRUTY/nQxPlEHGrz1s0i2RQcvAio51roNaDnzkSyvpqw7TS6wud
0rnTdSukJ+NLnQsSZJhvbe/BeE+LmVGkqg8UbCb+xk0adpH58sBm7taXt37cAKUM3gTVJ3bLRkNG
NfPIo24Cf2yu1SGkYv2KCsowo8cDhTBjpQ9A/ifeerdNLz1pDAoeIbOrl/9QMgcOc8/HpGywhpoW
HAunz7YP6wXC6T734kKR1LD75KD3ujm9csMVpkV66UH6F9NDvZzblQnSqAFLDXCI0THI7lGCXhbz
EDLbxAwD3qYx04mG2cLKWFqDezDOJVEInfr7jK78bsi+OZg/a5tLJmm9mE5yIoaFVlPxqZtNuXEh
XpQxC/u3HQcEG/WFpPS30WpNb2HXRo1brzXyUTOGSrUfJHedB0Da+g3V/v8GlonK/OqqWawb7uGX
hDGn0M996p4rrlcQ1q6OKmQo1teqJvWo0spbXNi5Yq8lFGxeWp6xoz7sO5H49WTOeDGIWNQrCYcK
sx5qXaAJ8KtB57+hNX1RSD0DCDS5HieX3BXHqVRWykmhBpu9fsZl0qj+aAlO/Mgc5iNxeLNidhz6
CN7grmxU+rkpjn5lB0qgsnxMBNvT+ky5uReEnY+4HCTKzUqRs7fIAlAdia5EFjDMzD99gf8aqnpl
I0/wMK+S29yGInctxI/mbiT19qB5MoLTyGm6c3Uq+9deT2wGN6JKknbbMZaw29CH4jqVllTek3d/
5SuIPFT+4Wt7oxj7jU/JK8vTZQEFquiIHTpItk64vyrGpDlx6qFrF9x8CSifBMQPC7d4DwVDOYBZ
RvubwZvp4+L3HPuV89SgTMJr/zOI3G4go0WPU1s9RNW2iVy+USz3J/buAeLOkOoX1x/xxMFUL9pP
yCe9xTGHVLTBTiNXFRIXmZm9bhmPLCG3GYkxixhirOuZdJjDcqsxvungj0JWpEbO6jrtKPKDxn/i
wYwU6FmrUljFaqQIoINLTigA6kg7fNg0dzmWbZu5KcJIQdLphJWgWgkGzMYg2I2YljQe/mos9ymi
oqe8UfrrogWh2YPi3s6I5cLbrTcV6QxOydpmsa846/On7b19VxlZ/+BLdQ4vSvJ6+VvNVUg+Bu+E
RRu51Uo4SAV7BAzxRDQan9+moz2fOYZzZwqzUP5HFd5L6v68s7gXiFAiO4+r2VVF7CzcMtG942NY
CyLZY7Wc5ZF9L7XpbkWt75PA8jaU7pxTgPTLlbPrOPHcvfw2ttaMa86PnSet9JIEYIyOC/no/sYr
zbCxpTGMlI+Q9dsngttYvnXbB5WbzmvEQoUdHkBPA0vGi4uHW72XA10WCZJKq3uLbILaRHwYD2y7
yyoqHp/nHU58bhA6hsg5kLYGnafWyQo1xessW0HlunQOYZXPEQzYZnyMa6ItEpeC1FGUXi0kiMGY
Wh5w4H57wrVllOOzPR+EVUO12Ke5/q6eknjucEyju9jFrJlnnT6Pzj6oVH4nnyRR96O2kTF9Qg1v
1M1fV36SrLgmuOCLLEJ3xlFr5Nl0NeyU0lx295DBi0XxF/waGjgzsltw9mu1oO2AUSC+hqvoa9+8
v7jxDN2pj7yRXUZeI66xppl/wsC0k1IL+LgZ18Cdila1nl4WSBIBU4K/TBULfquetyvlhd9YDTUh
cOnShnTWzzDb6+eyBLIu43eXIVAfWIO7osnQV6v0E1+YuU/IfkDoxzb9d3qsgApoYseMEKnaDB6d
eJJcAIVF//63xG4nlGNHJuCtZgeJCxqQO9BizGybdU8gyAFFEF263jGWF94RMToOpUDvMQNZvmcu
JwtdSfrDuOJ20hSQl7PkFaAv6sgc8ww16YQVdDxyGDZ04hNabacdXHOCuumdGmQe6qXQx7INqcKQ
UDY5PiEeHtR7m1UEKyApk2xZXIwPn/2WDFILVYL/5zuciEb52RLLdr9VbdV8y1kKeKDYuz7Sl+/C
YpzZKq89PWbG+zFtnRd8DqyN/tWN8/Z500y3/jo4huTz1cKFzsaIGiC9bg43XviEWavyELVwaLC0
4G8W+7/ksKjI4WbUOt3X8mhZKN0n9eSUjlUrmIuxZl4iN62kKZLIp0TziGem88SFnyXdIgPbPIz4
YAFpACHCmg3DjbTe4taG5zcShz30L4/1jyR3+sFA1Y8YMNq3quSBg8vQ9U3RFi+WPitECFknpUBM
3iBP6lqzAO072IJ6DYCr+GaMyWSA9vlGzSmuDDT9OIZxEJ2zl2IYvemyvNKaAubRkfKr52tI7ejG
ds6SHvMIZYQ+7D0qMuora1lctyDPbQmXUOU3izvzCPENfGLnl62w7J4pV35S9Puzr2DCn2OUHbL3
paXybiBRQMMyok++8dTQvT02LRyRo7h9qYweqXddItAYf6bLAV/oF5dtNYA4NbFu7jlD7sfa2Hwu
OOpYqQUlMiM1TWZCMWRcU0/QZT06vWbUycdpdH1iXoVm9qUDUO6BkoQawPyJn0RWwZ+qI7ExoVT+
s6yzDG/4L3M2gVpGZMw81WF/J3dA7WtMVPQm1T4JiuKS1LeORKFy3oA0UPo8ggZQiwLnLQdSjOTE
dY+RSkh+O8MvwjGorU+EqZUz/XJcC2NIoznEzuroJ+FL71FjEQ0Ibh+Ch1O3T/gq5FzzzOeAJqSI
25tltJkzl9ZuMIU56U5UHKfcRvd02+jNc6boe3N8aSJvQ06nMIhdbh6gPj9NV8uDxrl3hy0G/rWP
f7ZuHqBqBoSIFdy86fqzgTnsW3x+GpLA97cGZvtz9s1ZOGXEAoi/KT0HgClv9uDEqWefQQD2YeU3
2WsUZ2UHU8ewxjzH+20tmYsVbJ4wvslYPbdG8s57Bbed77EYLdS0UMN8h8+3pi3u/o7w7Kz12ibR
nue4/2SvnIkXiQPqk5rn8I81PGMfcm67VkEEm0RBacrb7wjQUO3VNG9GxX2SbrQKRP1FPpJh96Eg
WzjSMSBLFw4ypJe+ku2x/+LiOFCHimlc1NSXY0hHR0jCECpLNY4YOhanYb73V/f8dxUEXKZxo/Oh
FSlC3aZ0dJNK4Wdsjm7auRyzSCHfSVt7+zgsHcvKPD6Rrah0J4PdCcNoE4qMFgL5pnPimmzWRz6S
H7/yVbZlSJ0deWK+WsmjvdOvYzNW0XuZcwobPsA6RbikrdO2qkp7zxo0LMSyB1b3mu6H7gcyoGWJ
9yk4pyy0DNRng8yI6o6VlWjsrOwABpQEpDpRGaGelrmbmuAjTbBahBBXx9jnVj4qMPnJ81ejbrnU
S5yDdiGh0DP3WGpKPOv/6iJekOKkGwt/+yV6Re0bivxm7z3rnnrcCocLHVgswSxl6c7Zn1hiSw3p
UGo6XOb8SgQi1GrxNIFkNAXccS/vRD6AlpEQ4kxi7pN3xxkTD+QLSfPCd8dKGU+nE78aMFSuUCf2
DvkH8D/QG8A4rJjxq9sT/i6AmVA9a4s8p2oVonwifxkXDWNV1M70bqc9y7gMtJSEA8A3BRaXVUvW
5GrpoKCcpN7RfFXaXt1qzLFY6STEzBMVrpa7rYzZmxZfUf86tqZ/1NgtvPWosjqZuPAZGYI/8Htb
v51AlbcPbKvAqi1KZeNLyGdKV3UlNr/7GyUCtIWoce8c/mcdjqy9t6/snqt1M4RVIo0XCWHoIM5b
hzMzYHeRnWj2hf1tnPf/2D2J+81ZRWiK/O53cR7+O4BThEGbX6qIpSnW5R4KxA3QntIk+EaHce/W
CjMJa2zNQpvUuuH4jF/5wE/EJmNDEf/6+n8p+NfJU2BCW+YwUoVJ5iyGRbJmXNPYisKe0qNY5I/X
5FjKywwADh+lDjdJQWFbG7ar2PvXqEbMelv9DyDn5JN0rA/pyJx0t1iqssUgesK/ck8EOhLYiHOL
6smTszSeZqPsgw2vRFTyuiYrEuRYs/+FJpH+Xaphbipy/pVuVLu4jJge5tjVF/O45V4kDn2/zleI
eRcx7SJ+fjl8+9XsLPwiBtvwvKuOGkASKSWk1/YX/T1WepO/dT4q+kQzMgplvoNHp212H+ellXcu
yugKG0iN5vYbxnM8+cZ8CE5sdkRHMJRugfs+2WV/fpkGymKjfF/CyA13SJqE9vV4qheCQXR/ac+F
9W/cK2JTujfHTpC85eaRpK2hWgwLWv7cS9Pfb2TgODih06FNQrIJRYgUDnAz04B9Jc+IlCvdOPA1
ZdH3H3SUyzs97KC3YNiiUbU4vOCYSnuGDjHy1YyNi1NHXm43mrL+WhJsLgG3SY1UqP1ZTxaMrkB2
B9ANZPI9GAU5JD5xMQvLVnjuM0/czL2OgtWowkt3Tbfr+yiTfd8keTBiadSfqqWKdusz0UISj+0w
uU/W++M2KqVWujigiq4KSD18/KLVHUoLF8lDv2BM6nS6whhJUEQh2KhyPlBf7bOJ5kH02DZEQhVR
y+V1KWKmKGednNldn7KGR71pggCqIT7GsRYbWlJO1F3/hSCmlpJecJPSv/BXaktBW2YPB3Q2mv55
2WMDSme/nHORRHGotaRf82lC8t6hyy10Y8C6jh/gUaCaf17hCHNQkcse70rnRvdCVqn/5aa5UT+q
qd4imOBvrOWk9gCze9ThSPgGd9f6QzfhUfukNapf+s5s6PdihuKuX24GLxGbAaUElLeNtMUxHdQI
ovPZkeWO3SYUhB5T4agRjctMvy4RHC5K5GXhJlh7qBmzlLTmStf0BwNv8QUUrOOgk2firtKIdWBO
FaL/wNeRpNk8Ag2DWWEZ4OwajIdtctuiRgGgsdUHoQCjv6Jt0G2iX/Yk5Xo+iRDo103YFH89mKXf
CYn+770pBDqPiT5UC0Dc8OieWKer9BwVxau0ikA/XIjrNgH9Fa5QbxwxZEd75ZpeClLk06MwFs0T
qR4kT17zbUsthu0FE5L+D1Dk/3PqMEt8xv4QtgCNpquSp3YDGxbxEUbUl4e56deM/d7RSzvvx4nY
nnQDPV795pUfEe+l6K+J27k97gvEfTLY4wXa4ANkhuvDENtjrLO21RMIJiqmultQn29Py7EI7jrR
sQl6k9jieZJTr4+gNc9jVBY9HRt53mggDp/pC/G6JO+cRuIPvW/al6TW9RrbXYnUSQjHbJRv9vZI
2JE3o7hBfCwaLUPVdAyp2DFFMNJNo2gxSp7Bb77/IUgOVFXrqRRfge0QEjSX40KGtLCunC1PTVyI
tgwwdXcatOnwn0y8SSC0iL6axMbItPzA+vm9aCp/cOLsFm88R3bp8bl30h/cepBTXxi0mZL8zD6x
NL7EiIw9jpUcOe4vleeP74vt5FZMtq6cpJzTiVK4schmfWmurso5qS02ApQXNQRcTEzsfx1+Z66Z
EQ3YXnTSpxvm7zsd1oGNcTgTScJX4WtsB2acHMNX/X9tazF08dci6jSM9ZDsduTNKm3eIbdWo277
LHVbc/pidjHrLA9sJPkRTJQYpZeXtszFeTBDq/BTjT37HSreVeRvkJdSQa354tO9HRj7S0fWCGC3
u1zI5wfVMSKRNBRY4jWDHVhx/lkO23Ixs7PC+FXRwm7WCKmKnN5g8Eu6Jg87zIhbwVy4HaH3gEG7
PZavzptoEML9axjqfX0gKN671+A27adgG8NqsV9RSegQj6GbLDWk0RgnpKNY1gjKIfW2N4Mq2AOw
58NWcZu32ixNOn2mIadSZeBVRW9WYOfkOEyST4sAy6flRKU5V1xMgqSYS/eTyF28mOFvabBf4WwL
0JffNRwXfzG1vYVDqz325yBfjVlwCz4Un4lno5JEdtNYDX17/+B2tmbV3jao3SoKxa/z8W2hnuKf
qiv2kT2ckmm30AKVK4oEvebNt3PCZj8maMFjoPZr+Cfr25/oehrODf+l1/6VNWIyECAgAPKSKuSr
u9Pkl9yRH0rff12Bob4ACvaMDk4uyAOmS7NpSZopHphm1EZDvxpmQ74nx9Ri8SCuLmPZoQ/MiJm4
kc0ypmyNadfyeCJF9stlxNxdtoPv/uDHQ7vhQ2XMlR6ZKaJGyCUgcsegpIuEm6OikD+NIoIt0Z23
GAD1bKgA0VqljGB4bEq4hOvg4PK9o6np6ab4bSmEfHKHX9SyvQZ0CWuu/7jYUhF+CU+7CLPjAWfg
TwgwVlqP2gxyPECTnXHFBdd+NASwyPJ/DLYGCwH/oDLfS+n0EOmeHPv8gFJk38lGh67EhKRdDpLF
i/OAVgotzNXKpJ77UGMiY2f2Cx+0LAPSSq0sIIQRdqwj3XDDA7aFiByAf/SErBc5+vH7Tityp+Be
vmJbxMXPnhGX2pQM7o9D4h3eOFKgiBAnkZG0xVd6MaZCKePibINEU2ZQAJxToScYjwjLg5a/QcXz
pG4+fFDe4SPAys8ZMgBO84QJWtWhSMrEryU3GG+OPTxUB6UwWXLDXZfrhZMSMb8V5S1mxTcadBNy
VcyMEwkaRxzPSI8eLjBKOgAIW51r9pV5BhHRmA8Ocg7IZzsWhPb8EkU6mdQzplnCBask3jycTYEl
1lXfnRJUxtwPdBBSwx8GCNwwXNqb6sYxtvtmkUd1wnCHv6QCAsQwAmgGTgykAxM6i6MTJvRrGrtq
POg8ukl7jsRDNxnqV7G8Fmiaha7P1FakvfC3auynqqp53/7o2dAlSrxIbOilP0beAlJZXF6Px1RK
vYK1Q4Fcxc3CbXVWwdVbWbnMy/sLoyq2O0Y0wbN2/BMK7kICugKLOmppw1ItSCxJRBu0ogO297de
5cLZWptbe+4WpPScfyW7InoA117le4LS4ngmLn+TvN++8fHNBn7/Wk/OrdmVhP7fof+L0VbXhFsV
C4SuGw10ZrAb+ephgiRJ25EjaRfdylwXpbqTO07b5tRqnxes9FNxyVwWHbV0RHFQ3Q+KagfBPAuQ
yZ4th5HsaOviCRiWpFC4MrSwXl42LvMBHVhZUCAKwKiU9CpTJ0RhmN74zcypXneMHOuHFofBJ5Kx
iDCHxpgt8qN7ViIsYnny8r/tW5jNr0fV8O/6u1P6MT3OYNL2geQLAwuWAH9UvdMtFsCeQg6/91oA
ydHFkJCE026uuKUe/I3wsakK/80MjqgA8WECxuWMD2gnCWPr78s+ssHSRq0TbM8ncoNUGtzxhsiR
OmCbHsN3mR+MHDKZR+IxiABrpYIaUZvM5iy6HXIo9y5iYIV9wPjm7iAlJfmzBKmAnsv5kucR6V2Q
bi+amuS8NO08vIdA57//IxUz2aUzD5qk3+ANckweydACrtdYV68oYDupXxpalBZiSrhWhPZUikey
VCNAnmoi/Brk0FMSDq/fSCwHSDQfMtzYx52AACpYqijzJjxPxSsel3dy7wnk/CxHcu9GNDVDW/or
2TU9xGJA9jTxUTXy9avngy52/USPMvpC6lMiCdf2YS4oWrYZCPmHpCWC0wdQWzLA8VtD93Peswc9
Bddcckkvedd4HpXAdDTVpVFeBjrEEH7rKPauCyDoYCUYgGlNtJA3qPxyR2JMWR9AxeHsMfgR9lHz
QRx+4WfuUChTLXZh7u8g7/Jhar/hUH4Qc5HPgpLub/J7tl7AwSars9g5ABGHCnkLY5T7pJ9HA4zJ
efL4gen7L9P1cSL849Pf06QCaA7BbR24XadnQrHCExKFXxICXF74mjWSFZl5BClI6HEEAkX+Phvl
Tjs2DuSVZlKE9FiDgcKlgOPWKbLEO3qE9GFI7NGZpGxirbkyel/tJAUBr3nHU8Bs2fc1zSft5ro8
gBUnEC0+ZJjX7gKUSyAQ6Tq26ApssLk1/9BOBQ0XFzZotiYGDAnY/3iJJv8EFu9z2kHjgRFAsA8X
x9E/chd3/CF7g82e29IoVFKNhtZfiU3MNdHpV0HlbEIdxCoE5QPFOHA1I8nBTc3ul1Po7itSYUKO
DmkC6T3Gb41vAXL9x9WGNw8GqImS5l4Tv8cXebQ+VzmGFAerKPDlzEvlled503HAw9A/PxQl6el8
4s5ohyEmjZAIzhehLSFJ+NpADpkDmXIL7WWC1P1tRFCLST+MT6ZRFnx1KJgIHuKmm6SHfA9aUamJ
BoFj32N3gVIrX/J4Mkb/plwQj9wg6l3mIzE8uu6nAoUlfRGxm9IWOnuYJ51wUQPjyEpejjHafELN
efZFlD6RQieukE5woK2aFDjTmsuzriLgS8iFylaLyXbHZI5oq3BXZf1yZG9V2gw1ozq6Qj3r6E8k
wpO6jX+Ybxjqm67hB2tmHFygCD5/h5BCh44/pErao/GPE3tEGjbpDKP31tq0AvYlPgJVhWPUaQei
+0FJsfF27X3P6sRSspzaAjcn9kUIVXPZPzcf0DvzOfV3QmLY9UtidxRs8BSrDM/tZP3lXb2PpDgx
/ZFq/17DNFGGTkm0cslcuIsi6pcFPqcbWWCoeNfyzN77Qn7ny29nfm+++utLGgcQQCOJE9F862d5
mLQcWWfazYDput6m+WldIfUTR1t6OLDlSQ9H+eVDkKCjSKyNzIJ0vSowVhIVMKuhFQcPr3D3VB7M
roM+VaxpO4QD/vJcqfg+feDcA3qu9dGEpLrhjneccHd+hxHrvp9a/1sk3xvKTuhD5gwOe0aoAqSQ
MWRmEsIjX5RzKdzY8Y8syYmPJy1E0AaGmY0un8s27BL+g+CwyqcFxZMEbQux9Pc3nwSwWY/x2PzS
gBnXtqUJyASBl65h4+S1g3BTi7e/Xu9w9fkV1VVJYEf3Vxu2f+aHE/pjNDz6sr7MfPWYKx29uKOX
jKeGPXmm7xOfrCEJTDqvd+mLTgtNIsrk96Lf1kgw8OOy0LzC0mZnyrsp5/oWjZi7n9dlvLsBrqeM
k3fc2BaVZcErE2zGFwot/bb5KhQhg4FrqBc1iyUUEybMPfbdTmbFltXj4NBpjM2XqNNGv/WhPwjY
+jtJ+67eF5dnon3SSTbbwx0BI3cN+LY7PEr5PLqrmmuRnkPzp7oAcfC2P4z7gRrUwgo4tUrujsHJ
VA6sxZM3RQTyaS/M9bU5whCWaOvPqsk1MRbkOiF4IWGrH6Q1ax3qJvtAWt7k1IhCnPy82xoOzhFg
jgNdXc7CTYLyRHAKflaAtrDDzsXGPuUHS/B//xedWRE88RPnxbWw3+7byLcBlOFIAsK+A2z9bCam
ExJuJOYUujNmxrsif0Fhd5sj1sspPPtCfmygbIEXg+1gXlYiof7q9qA+pGQwGy7l8Ell/gPv5OLx
PUuAfPdaLh55yiLh+PdxGP6SXxboRQ7T7opwu9/zXOwJnXgda+mjcc+PBg8ahzD1NJtaOO2x7jD9
Tvp1rvnTYQcwNB/mvEfQrikJR9yTfSwsdFkj3WejqjNOFPYIIsqlTX+ysE3wlF1oUG8K0aWIjr9I
8h8kUgUZ2pUnOBBgMHmNF19BjrBpSNNuZAAX1aktjo3jm+0VaU5irA6/xK8YZYZD//uWdEs1D6/h
QrqMfVDSNHpzU437iVexbCeiyWWvf/4b/MIP6f6T3tp1/HjfoE7r+/lg++3vCjYh2cb2L+4I8c82
Y6igZIxNm6G8vSfd0kFdGd5IY/xtGIikO9Sc5/80p2Z1pRUUEshdaclnJCVPvxUlAiMd9WBp2SAQ
3gh29NtKFoqGYUjC/uOGzYGsAwR82tW+Ojc+saWRrA1D18F/he7GlfPUbDTNsa88gtxf1RW7ANtk
sTXwquazWhwCZ+ERuvK1dqwiD3YQfqISOGHJrkFzlA5soRjUaUS6cdA5972ASk+G9GZOn29q+495
Uy1kn5gIdzOTarh/fbBrBXlGloBhpnvLIfi8AraLGc5k5B8E5em+WDySqeiXHrVAQOtrlba2xS2U
UTdYw2CH0G2Q8QZE507H6ue4ErutY6Efd9wZQw8fdu8V/uWarW0y56kw92ja4ELySpA5cKMRuDiN
Ld2W2C9+By/NLB+TgRDae93l5OZ3kjbJJgdCa9q7kWjzwmAKZXg7GfgzB65jbyuNc/UpzeMBPk+N
OnIkFKavqH3zrLHzYNTZkkNB2fCTBfDS7qY3HK6TDhvmOuHc0/P6bLbttBjerLjoSmndvgJTBEpp
3B2Mt7jKZwpWyKraqTq6rxICjgcNcZ4urNTC41pxeXEWeKfzMPaYUtoLgJd9P1eGNSyLwW8PRAvk
HX7cIl+V5HqYAiwmFryzhoHSytztzQvXRS98x/idJWqug0A3arTdMlq2V+dsOyfx1FrTWHdFeMpg
Dh/h5unX6VsmyLuIRRH1Q+OQsLJeDf9lDQUhGFiveAmFVMCOMaQXOsqAOWggHzJLfRjNJFCvPxSw
PDbadOK9dFVwvG1S4qcr4xDEnO2ATwBNEqSBCAG7MwU6ouB24RxX1pxoGlTnj//B/d5jq8HCGZFI
7DCvQrcFAVbW6LtDUTt66jFqMDo4GCKpJxYwIkr1ZVUit3YjKfrJw8Q8pyJFOTECW1LUDkpYb1aO
A3OaF7sW6z/CM8aVOopI0dMfO92xMJeE6tebieoUGI9wqlCARab2fO/wD0vsWVSOwoJGWLewrALd
fzLtwQuACu4cIAMX6nWulr6OnJ9/3bught6ERN6vpfmp82GJ55zcMJg8YAFyGbbLVGmRaacM8GLU
fDOxcav1S1ipiYKAAWVxP44Iyedgi6vwOWYLS+uQDjxs0MIPWThdoubqY7314LKu4hJDHNFED1dC
ZP8te68+qkJjE7tvCnTRRRAWq+7TaG3YMAv1o4B6Vxoj16PrE1c961Ycf1PrFRh17XZtmSqUc6mw
EYkqv8B+i1c02fy+4WFB4cnEqNZkIrw5vVEC1V7E1HvjDxkARdg0TbAZ00BlM3VEUN4XnD+O6bQE
DZgJH/oqNx665x70Mt08HpwPP4xuKQsb/ri/vSYrKKDgq23afdf0mpmB037cuqje6nRwFci2xtSk
fP4LeTt7/hSM1tqyvLOFH8wTG2pEA7FDjMn9WEXv7jXiXXaEtVBGYTN9vVliJXXbctCQRC2Cmp2i
NfIGaY6he74TBjzqduYklKuLTJc7qqtHn5toZjtle5mZY+AJVqh8HvFd1ltyKrn2/ZcRQrLVqW7h
5SV+rTZxzF8VLqAWDbol3EsLLTrAeti4apxvYhitYKD7og1KfjNhpDcxCHnvuNKNeK5HcpXscFf1
5m8t0UzHuthK4ENj/75XiHPVTZsrNF89bNhzNX+4DGH1kK0UXZ8QS+k1bSn00+7vo8Oy3m7HL0jn
uWsDqKnP7HWjM+sX+3tTg6LAvMqQO1328lRpHnZKvF2FCxRNHbKWyOtZfM/UxmXGd35FqNmeuW7F
No9Jma1nQiZD0WD65Zd3zlM21rSkJ4BF/QDmQM9kc5J9dnMqLXaXVRVG8ekj0XU/OhJPKemDYQN+
4ticX19cubF1YEIsuCbFBjFYwlWzeMhPhHNjbSME3cU5L/JkwCnd2XnCFonCAP8jj+yUwA1M4HLD
8dcYyuBw4nLoYwrEWMG0kXGIt4EPVopNaV1t9HxcYl9XLqza7GNFgidjRukeA+o9YL4V1ACnyt6f
FqxITavL+e/9kEt5JakyRp+vooTyBgzBtskJQeXkXYXP085FGM6BzaJKSN5MaJnh5a6EiLXCKVeT
buZnARf+hihmXJNEoWlc5aFDkluWQAq6kwE69aLfg5JPOMyfXO8aD8dORtMsuZ9/GbUbEms2UCfg
GuaHZ3eNDj2EcTIxzktw41fS8ZXq9lHyUNN68IIbYII0GNeE/87i2Jftli2/vZrkNSuXl96F+Fmk
fKnbvpmpr5v5+T6z0D8pd6qR4H/zJbOvxXSgd2LtZYP8EAYk0bLUasXR07i0EYXm829dGqkLqHyM
dtntUteluSAhtwJ9utJRAG+EBd/FaTVxiLgacqzqU3+Mdb5kt5QFvY4SyDZQjpnWlRJk2c1EvJ3b
pbtoZ1yOlGMR5K9NJUKonJHA67YIiJfAEKCZ/ymYUuGkLDAFSpZao1LDWvKCCGDtkrmzYr0Y1dmB
4pgJIv52iU6vfrkhzqzTFefER6aLCYXIBPSI9ZHAI0tnkDVx/Nu3w/F9dzdsyOk9j505qbYCOmIa
CQQAuzH3KLiYLBr4CzSGxr1bQUGEfSTk5CXfPaNxmPOPiBATd5gw1rUgcsdbGsS4LzZD0zmfOARe
uxjDG2x0I+BY2FYvvizkixMwuE24SYqvgmTHMyjn0CXh2zAZD0JPa0AmvH2rFG6FZ4y9HAg+zbsx
ZH1X9b6WjgqL4V6O467x9Ua7+CLW8gE9Becb9DOR72lVp64KAMktZcONrXwqTqh9+e483NIbrjou
EUhd4Htj3Pxq6RP9gFSSzeAHrEkyiQibpHysV7lWIIOHDubEsqNqbuN+ah/Qa/TkL4WFWhnE0/up
j3MmnsijgOf0BRCT6uGv8BdwAGe4Jt/nk3W/Y3PeHVchEj4IyLFpygomtW8RFCmD9rDg7o/l38Mz
ROi2/f4+iEnoSFJRNQdTX3XihybYkNFgdqF6fEMpWpljR6dxdw0zEkshj5GPykzPtR2EhheVkR/G
QQVTrHd4FdKTFFyKoXdSsSrVaQ0PbdzqjF+SEX7bqBgVFFdRZLhzprTt7Bnsbwc0KMoNJKJWMXEM
QBVUAwhNovzD4TF4AMsEhz8A/JA+FzdVl1RhtQS1TeAtSfArffVPlBCkqfiQRCvgLtaEJUIfq+/8
MMM+tVoXBmIwgBUFo7ulcaONxhkzcpa5AhZX0OlP0Sa/A3YuRuYdUGD2WnvpwPe/OHJPrkbxtIKZ
xoldw7BXKlAip0RMkiUdz2Mm4tuX8LX92ndKGcjwuYPAfbBs1efURQ6ZgbMKFpOwKwISd/9PS6or
rehN5pI3Fx43NW8sfk10PUz3SsHbfSu/xDQj5fwehL+9HI37xCQUNl6ZZ2+dHUhoW/qG4a4V15eM
dQHGrzpbi+47fQK4fA1owbo5exeh4BqAwHZSi61mVEl6PQTlyFo/nUyt0Lhtidq6pv5FO9yCRpu2
cw53vJOFhakgxM3/W44gBUhFKfN45vpGRCSCcaICpZsZbl+Tux6WQBe6aCYOre/WRyb0Ae5CbO4M
z2IXWeKxeoQDMyhOX2Vm9ig9mo/43OB9gFCP+o8Ru3k1dfpKn3rbMYiGTfbqmOMrckzznGddk9mp
Ugy6ZjFtBvG8dK64rf9B1yz12K8w80CJk6Cba1rrwvk/o1IOEPKkhQqkQ6Xp/ykMb4onnJfTP2Qm
FiV5p93wfE9+1ebcaLHz9Bq+6zphTKOxIc2L1VysiP0/nszwtMTFXS4Fld/X4FTSJuO9+G14U1/g
zErZ/7jg7/qZZGYzZBMrRLWNt6Qa1dnj+CBdQqRR6t6DPE0SfBIGHNUd8DcpSuxwUyqJ/u1vAaXI
u+y9LpAUBP/i4lEFDhMi4ZsWt1IQHvXigUnN6HPmiIAQp7WnXN7npQMftXl65Ke4X0bo4Akki1NQ
YQmgyvdIvTpjERmG6dkwXWNv19/VhWhgh7Pa1lq8B74U4rbS2tdUbPD4Jc5SmafZdzkPgDHzy6S8
Zbn3EEnlDw3utdf4HD2yuOSAglkvXxy6pDLXZAU+ZAait9skRfUt6Frat1r6SsHolaXGEeLbouqz
OGUznF7R/Bp/48Qr3x8zL0PuuRJp4KNfgeWYsAz9UT24iirwumm7DoruaixbAK/nSMECZsf135rd
/Jj1WP6YYPlgASZDX6hTHNbpZW3d6JFd+9zDaXpNJAYRm9rRlt7UkrioEmybqIAvxubAG5o4yidp
m92kuX/J51hBaAGAXUR7JHlIJptgYPchiZYCeG4HJCbBmWM29vmK6do14exHH/Hu1XimX1+vPW20
Pww6piR5U0/GeD0hyml+cKDYLc9BXDXsnLolLWd4ppuAvXQjvm/TFiP5RVaBG4SN2SDlVF9K6S4L
tU3T1Tyj8m9rWkPQurHFm0Fldt/bLFNXv+EAcDvLrKeDPlIaA5a1CTgliAYtxV16674Vd6LQPnr8
D7l+1YpknOelGpuB6r8e9aHJ+44TBSaOFoD/heuhksZxgIuPeGqw78eRvnFMKYYF5FyChPZAFGWS
p4AqSwvCjPddWeCBwa8RFaYXbb6bBxCynz6E8TkhXMdws3uWX1zKrxScsepxfqFJfqGWPS34nVwO
zwx2qqKdSpplE2v7zqmkKg7oaSI1ZqYQy7kD6wI0HRch0J2EiJKDDJ8Vq2rDJDgB4gmvaoPb2bfP
h505f161U6MHiLIqdYbYAIFbpJnNPKk/T0nbu1t2q8vT5fyICVMZseBNZECxFcuZY9B6TiygKMAI
Vrwx+JzUobj7WWuCqyC9cpMGNk9vb9JsfXz07m70p+1M3z1UskB8DSPVddr2pFXc8aSxFy4FH7Vk
FGaFQ8cy9iI0Yux+9u+9fi1TB/lDYJeAn2mJTYsvTcXF7eMtEU5BVzqUtCyRh0kLvmVw3FnZu+su
YrSMcfgGrWhDG9qSNnLIoRW47cwZo9QC7Rdv1n9vun5ysxB09GwD0eadjMraGyLAV4dYnx5N3X60
gaty/DCHWMRQpvtfJ81L3Qsesly3LlG+UFlSX1JlwTnxs1eZ5PwOtv/JAgwkPgWKzk6j5UoDwef1
fDkPNrOXy+dV90sbuaFu4hx4Ih9kRPavjXKg5OhjNwJC4wW34vf/Surw6YRw/GY++1Al1w0zsQz6
h5E0r3wHoiJjjx1H5yf+QrZJVjTnlHGaLEBkf8Fk9IbuRMEFomfPXz3R2J5XnGPhlSuFXgA643MX
Zu7olDf+ay1egOsLUqt8s3VK4XXj6bz7OvMYvbdTHFCRlPXQdf5xNlPUmlpKluqowTlTBMfwddkN
coPt4O2Hkq9pvkrmiaAQsqHwsn6A4wiox1rl0ZCiaepdzy+ZwNcy3yHERIGssTDS8ehG+S2akuJJ
cAKuZtwfAmBMX9OLdzyfDTjU6lyoJ9NhJDDHZ42wOAVT4WjWzqVkK6JBwKUylA2MxRyT6Dwl+Vyh
XjLcbg7icI/eU8IxAG5FcFKEtIlLnnyQ3a1k3gOFKTKlJpKTLY3+AnrcI4Com9vlDp/z+avekAT/
zk7qH67HVFugBRSs19jQE4Zi9qC6/eh+9khonfQjm4BKy4nDnrGuQKFNVADTXKcoO2FQYke+8aOz
SiShdIhz2HZY+2rORKEQUTdNWx1u5zi0p1iIil8MCriZu6Dz60hruOHEeEElzUlCjvtdWEtGEJqf
ittcMblgaJOXvKHpzb0FVMcOHMd3FsgPa0M93dZllSh1tBjkKP3wphvi657hgE6Yb9Lo7mT9DD+e
IzswSlRqPdvF5e/9Mph+mzj1wtHTxA4+XoSasNnY/mt64/EysP4ah53nV4/8fMGSCnCn1hEO+w3M
iYqH8g16EQhATgaJtq5/lKDss+frKE+hZH+DBKwwXkaKgyGGoIeLJrJBaomCcbjxxwV8/daR3LIM
4ZEQozGL1Fj0TTLrJsMJTfg+GEAL1wIDDR9Yk7ZMgTOM3f1IYHuXG3Rrz6xcwjIGVgeG9LL0qERA
NOQcKcC2rSE+i82QQ99lzK0pQv36AjeToJgaJK1NS99/BkTvYuI5ppJeDlxJVTr46ea9HiY9AKva
2aBJsfvWhvDZD3xl7MVpkswC0V2Hs8OntmQFV9CnPohLCVQCA7y9Q+GCe6blSsDiWV75JZxXPyMd
pucCb3WiZwA1rVM1sc35NEjgWvoZ89qUIggYeUHjBgaNMja7/fd1Ex3wjlRrYB4egzFKvKRvvAwh
e5Gq1CA4PZXGRKMJKFxHOYW+mWLUca/YGtqELnlO0obpBnv452N9MK15ayQFAbaQV8OC5XsKqXlI
IVtRKEx4xmpwbTB2HowYsa6pk4L+hf70aiSPBA70l3j37z2ZokGoV1U/tbf119pvtYaxUCzhsnA1
kbJ8z+YTZbGnizs78fc7uYnSx3JMdNbxwRsFwx8SIwiD5Tetmiy0RAXHnILqMGkZCIevngqqq6Sk
NET33IB1yvHZRDfBk9y8LEnHFeiBhx8BfiWrqLYEGZYVhenSLvXQeCkoC4W9dgcW1+hCKET99+kM
Q2RLCqO0/XbILqekvsI2rpBtBjPqELFuOGypCqz7oyg4HK6SgDLVdYYJX3Im4FhzzWq8JXSIcenY
01O3pufq770Gv0HJLUnoXyNMKWYz0a4H2JZ+XgFLc/uKhuQh5bS6D+4MiGIL35I7Z+Sa4mfsfHsh
U4OX1DlJaEGI10sTkuEZ8Ve352lTXUXUbcIy3te8y8jWSNIuB4fMkkRj+owkccMmeKkbiexzOzkB
zee/xwlX+YL5p1EHComl/jCpn52K01vrL1+lurE8PGQFju9ZSufn+NRu87zp+gjmKK7COTwO5YAp
eBThLlw7ZXWYI9ULuADoZhqaD/Mm/RymB15AF900rlRd2/vse0RBqI1FHzX+EUbtUSG4zcRVio0E
N4CW2UnSjZpyCFPiARuoKctYPdPdofbT/wL6KLNJf9pLj72oqzhiajUBnEE4iwijcCEZYxoPocQh
mJZKWJHjzCwUk+xE90O2k8S4YIL/MB1XP9EEYj/yOcNZ8sFG41syp2XE/1BR2fhkrWh/lF+CrOsS
tR9EGeeyTmdPx6rllHHloSIX5pbub9T6fAWyGo3mWI4x6yL2n75jjogMQGa1l/zF+V94/+SfCF65
xLBhkAnkHTrH+X7bYEt42H9NAXdEihRrhBpfiKCt5W95bHDhWNTblcZVB94tK6+RHxwPzqK6asZT
yJnBVv9nV/Z+y834gUdnOBJtXFptRJE8QkoWnv4kYT9mco4N1Go1BI67yu628XxCS7sDade/TRIs
2DnNhx55nK1CG5jK66XpFe9BMTiJrykOIo+DDBSalgsCozLjoABFGn69KsQDAo4J6JVDAsLs+9Ik
CmwX71V5LKi1A0CDFTPS6ly9pbZ2K4QWw7cpbZA+V2gm9sBKxGpQ17NptzeweaD8XWgWDzSMPw9M
PkRdatpIv3K5nzYCo//tOepS0a+7eyHUz/yDtJRj9UPbG0mC8QNlzpW26a5pfhC5M+3+CnKFXMcG
icLkbfSiyU7Ki1FE38fTGrEFsUGdKkIOD/9WQdXsVrIvEZaW1FPHb0kOwyeV1CxbTsnFoV7aC46a
evhlwHSMWfWQsyenOSNOl0gVWLzffdlMXw34YPV61jysYYHwwmI20FVuo9q0VfCpXBhPfbWhnXmM
S8UXNY0lE74DAfqtxO0ODes0zrOfwZN60lCWqTkcKPA5PDqXHbsULwGGvbH1/WR6srJ1yRNfWgkq
LP3W3iRDg/r4lv0lHMXmQvzNhjlq7sQiYFwtUbP/Kt2mASWzk4JFqlCJ72Ch+H62HVhjVepjOV+T
w6biUGJayn4+F1QJu04/zaSBrUrHXP8BKcmBnWtWAWE7P/fu9dfoc9q+IN+O3ILmnOH0Rko8bQr1
hI/YhInopiDQKFOOv+WHw2zB+Cfq06QfLnUFoCR3f9PH92+kMlAFjpPNXB8DvwdC+kbQTBo3E9Zp
g9PAyqkr0CKMDwqQCZ/LRavNqsGixceSwH0WaspD3w7QtXHIxW+E+JU5H0PynAPhryZaZkUD6Dm9
gtLE1/A0QQTnmapZ4PoE1S5cdo5F0/2F5/+MvU/ESCVeuECEo/X9yQd274t2Zeb+X8rUbz0XV3uE
TUjBPkom/JcPYWQTYDUfu6E7F+CdGqMPwghBDdvh8Z3iPG3SoviTrxoBkpI8VwW310S7uv435jiU
AuHZ5+uoYZmS3I42L7bco3A6nxfLsAKKF1kBYVBkNCeJ4xIyGmsbw7H6WHM/JKmWmBkIes2F5e6d
ePlrXQDOxUxT34V++bhdLJycAckujpX1z9w747GM39Z9QmQJRzi6Z8EKbJbTVCPcee5nD65khvYe
tKNAuIIq9F1NbqiGdtNSF+E+rT7ZFlr2si7YIx7ksDnsBb0jPX7UK+J0sgbB2R/DZCBWVvS8OdNA
Jpmb5vPsYJBT8dB23H4dlp3eWIXs/aLzgFBbDHysQ++qn5w/OTQqjlMbbzrso01excX4GnLbFiMj
LQXP5zsOYu1kDAi16KrkihdluXEs/6G+UmG3/IuX832taR7VZE9vF+EDh3Jp6F9hUUg0PPSeJuyF
1Q7EE1cUM+3i5NVluzv82KXXGrqE23CdU/OQndULBtH/rw2vc8Bjl7YffhR4dazLSDUmEZxH08nB
6XNHPfBXIFRbWLhLN3+4pKgvBgd1BglEb5rjuck4st5SC16EIQLYXBkMLJ/6e2qjKrvNdUEIsB9L
FlGTPPe8nrRi0KhxnnfcDg1uu56WEWLZzB/8wTqQVJMNTN3B2N8/8P+30H5FwUC/XuLaIjFuhvtD
f2tW14KExLByRYg6NkYCbuieZVw0ZIXqihS8yFK86OSvKYaPoO/ZgmL8BiF1aQMmZDqLilxli0O3
dKLn7rdHzxK2Rfz5bn9AVTODJX3mJXxsVaYKWpM8ic/4yT4FBphaYIXoxOnqmHen9j2hiRHof7Xs
K2LfM0Cvj+1PT7lzQ2+j8KasYrvDlOorpPK0uCObYr6ecI47/3oJRFYIHDYy9Z0pT1aa7odhAnTY
HyMtRcWAHkXAg7vqWPGYZKYDLzlMfZn1M9MsiP+HewGiX33e77NfaaOYJNzXFwFMBmewr4sdCUHZ
+SNKzMkj0MHRFmsor7hoIJRqOVqVxh9YXS28EXQ56gw/SM7sJzZ6aCQZMtmNuRuCfsMBQp1q6h6W
9Zd94DKd1HNszDotLmvlr3u5O9TZfbMQhH2YCmgyTFRt1beTFmaQKgCQ4Hgnqc+UaLTdpXU4xjaK
vV3bHqropb1oXxKVmdfz5Lx3ea6giODm5XIYeA5ovV9IsNxTLEEEcVByPSKP1Af98kzkAXOmPd8h
9d624lpVW3Xdwd0y+44V27KaOZ/foLi25NIY0sy4LOIhe27GAH+vsVvMidUJ45WyDBZPgMjsMSLP
O86X9Fm00++lNY6z4JjQG922wFm6GhqiNQNEel6Fi7EleSGmnqvJoV+2rM+Wlodjq+par2FOQgNZ
G9HEoACehqA8hKkWxTe2uKzXzrBU2Wu9ho1rLxHIXdTrpYJnDL3iE8ERQkUmL9eyP6SVvNyoaH4t
ke8Feg1TzRSlVAK/mIx1VLKb6Ump3oFWH3tHTFvhMWHH2RRMKxUQ/50L1JGF5ZRB0AFZJkK0ikq3
ATJfnqoF1nTrmc20aDs5DwkKs5b/voMwgDiPgy5MZ4WmkdrN+ZbRiFeZUBpcH+WBDeceOkmiweed
C4O7XQwFNbKXmjqKXIMCR5dsIA7lYYswvz4HmqDv/DiKES6RFr7db0X03EshqvVTbwLLU9GDgRlm
zNq1WRDrhe13+PiD7QvOKRj1g8p5pKjF8BoBg5Sm35f9yOaQLNQCvSQC2XESQyktEemNpX3y6f4O
0kI1GiM2hYNcGzrcWgi5nYQPqSV8zaIMyd3McImjzQr8wEfy47Ay4SOl4FPUPz4a+5LTLnbULgat
wyoqZ4nPaPWF14NuxvlDO9uwe8LUpz8EwVl9NVV2BtS2T3rnb9ovXK6WD9nzmdXmhZeQX8+156DE
gmg6VVgXcDr7ZyUOqdknNt9EUvLaQWv4mDg7BYUEqz71umr6MEAd3UWSWR+DocdywcBHZOy0AeL4
2xJJZUEYJ/HvnIDZgPAYPxFKjYWRLNvuh1XKTmDW8+DYxNdfR6/K1TaD1ns2lKE75xMbhWYsVV82
TlR+QaaQcUcZLP9VXmA8JOHMEydGYaqfDhEyfYE41SK5u4m4ayINajhGaL8Us2HHLBhw6vsvj+Nh
lMjGIAmNWh1eklhkvcNPvYOkhmf41VNsQ9GqSvXMhRMn/8+DXLLynlHqvJdm8QsRMNMw3mtplLzK
1BbPE1N4MHL+YMcFG7BfuPC4Qt5zh/dAfUiZKzp7K6UQ4GGdUz86LBjX3+z7ZTRrHDc3RgaDmgWh
CrKokoAj8taHVJoBb8plvShffA+M4U4YZmG4dURS20jpBATUCv7wmR3nEd3EgL0FkQwcLGYACG3e
heVBokuFX0cQS7bwjyqQfAP0BD3lZVEvxahtdJqgkmtImW6TGgXWWbBSdz75YH0WD2pPamgVFl2W
9TdUp7fDPSJCRVd/L33FZq2x1lFUFTmC0FOiiuxDdn/YBzWyrFaQIdTQ6YtBiUOOijhAmTQBdWim
+O3UsJY8YTN/z3Wocyn1KIKPJXlTiDHJGxhWRlX2nwhebY061a/nQxYrEUm5QS7QWesRYz0Fz9Bl
iKH2eALE4N9a73hpqYgcgCKq1ENmBX8EleUMilxvjHWGgBwO63VnliX8prTFL7WqRSZEVWoVMi29
N607ZbyxRERqsMhSBkUKhnHYkLddgRqlHk6pP3n0kiSTPZpCTfgccfTdLBANUOhWeyM2usonaFTp
+qVTrcP/SyBNvCBqVrGCA8c6eGsirUaIKA2UV3OYW0DX1P2tWWLjWAAjkkJU5kD7wM58gXjQGZN4
7GTr3SV6nqQNVCbgg5oL+Laua40fR3+X2qMMIcHOaPcovT3pJ0hoi8wlIbUljT21QuzJszjOSFBj
Ocg1LOGNnNompg4HcB5PVpMTf+xw1Oa8hYDsxRYOodrjbT8oq8yvI4bUkEfHdlEYSVhdysHWAd+n
OWZMmO/f5jjWS41LjKtWciwL+LkjEiIbG6CtOTv9IuV590AkqUxtEM8IN8I/Q0XnhS1dixgrUavx
tAtWBvmlfDGM+IQ0eQdcFbkxTLf6bpzNI6BmBoEup3m0yriEHsjFA1Lq/mqlnGrgFV8yHgWOnNU3
iuP/18kEtWn902Ukc+xbybwT0BLrG7g9MFTNtXG+XSYr+bJzhRaO2yHz3e4425e5/zdFx2Hi5qJS
gkL8tWrxeybpeKvprho7J6ma33hJQb4GqrllBd7TdseMPIz0SIA4gCIuMcy3Kp9OsQLoIPEWXa0L
7cLEv3XceYO9rCwlP1NmYtD41qpb2dL3EIMsih0vIajVIFAjawA0M7HMHLU4yKiHN/qUaS0DQ3Co
Gp16wEdpfUH2e5+fGRWfRyIx25u438zqUFDXlMd2JCDnU3Nx7ucrFzdO0u/SAJej8I0YnWaa3gMC
6tboc0QQSdHW5e6QSq7BEfXA4EqChjMrGgL0pxrJlQ239/Z6Fdp7L3fDm22a63yyHxudNk2ezTqy
tYUou+JXI9+knLiMIYcpDzwTyF9hxNkJwr39U+CvfrnLon0Z2iT41yFsKb4IanP88W6bwm+PgEjw
2ptTUn2q+TRVBztiAg792Ke99CX6kGqhfKlMWa2zFrSiaTxP9OpW+Hc13qwm3cS4wU+A4HSOYDTB
axpMTomsrJSyLae1E95dJsIAV8mIEpJ5UBdzwb8xXqL2bIYuEL0xBEzXuR9wURMD4PINqivJqHYq
tHajBNv+sbs6WA1qHXNQ2p1N2ZCCDu8MTtrv5l2ABd579WhvKgJIM22QrA+ohVzLfIaljV1QZWf3
Hl6kcLRq2h28PQTm+DoUeIxDC4C5qo8bEen15XVdnBMlyIX8lv8ypbS1YlAWCsvagl/YGy2VmH/x
93Eqx49TJPiNBOR6uOi9YYdNhqTKu5PNtv0ddo5WD5Fmfxsorn/Ud8GukzowEib1OOI6Y1t39dcu
g8GOJXzw7sn5HKgKvXYIEh+CyBjJgMWyHO+a6cBLDwinYqKt9l9FPeLAz3ZhPho/tLtThp9IyCZN
fid4k7Cgan0S2M1kukJAYkOzmerQNC30WjpPnHXyvWMJkcIxCA2gt5acC0ndk7rPu789R4q/9r8s
Up2hKKBFnRwMCDd8+tC9J8cxlSE9l1ubkOZxzxHJScPZVqeEP1kcmS2KlioIZX1chL2JjzqWuBVx
AcUqQywWUMu6Zj5CSJzqpiLwAkof7XRyMHSnKluMKMwI25nFeLf57F/jNmJaSs8tfogAL82k7QGx
nnju+Nk8E3/+s6eNFRYmJk3l3W00VF2MaUDX/rp7hk/HVN8+ssg3Jeo5W/JkeGFl6GmZ3S0dUtcb
2E0MOXz41y3J7lIp6HfPb5LXeOniMb3pOFyK+DWNotOpntSVP6Y7QMBM6kpD7bAx/KkvcfLIMj3Y
MLsj+PORubYlilFZaDCkHpfyyRa/kxKqc9Yr29pB2LhhnU8BkRxZc4Chgf7IWQg4udb7aNheAMQ4
dmjV52sS9i2Fu3mJPZuPzka3zVCX+V1Oz631Dyn4Oz20hKFurs9+7t/XYG8MbiMoFy8OREFd22y+
fCdtUJCdi/lyKtIkLtJ7SgDW7GE+KFrKk2q1M7Y1uqp0ACgLNEidY7CIDZo1kzZhpUNM1LPWFm5I
pOyjCbuskyE5VEXSmTHn/rZ3NdZhrfb7AlGSWtmPRvZvcuj4KcSasKbOye/IRflSACVvzfGBTtKj
E2wli0j3evzEGWNtL6yPKIeexf+NRBaMz6vB9fTgoUatOmj6S/DyR4aILfShldRbaSIN8XxqyfEh
piiQOi+JhQ9+vh5gxue+nwRrL/nS4eBZKnn+j9E93yHUEsQ6D9iGCUebvmDQ2cOqFDDLIUgDCYA8
iLCDVQ37FRFXOZFI97bzCkHfpwofHqQ8Kgrz0KzKyxGN3xMr1abZsn5g2ARRxVLSE6Ls/D045wnm
u/x3DfQk1o9CmU64ifNs04OnH9MMGFNqljHTkKSlS5W2rbl/vno0g/m+xDX2k+oiojmaX/01OXIC
iZ2ne/6wQUjq0S1s354orsRyPKivJ125hOVZ25590OY0XqwJrG9l7s0ZVRSKF8uOnN/sRtJGXx/V
Q54u9IoNbyM+BHufVes9AJPFkgvnEy1JY/OD8MydxJNMoOYhG941XQcJN5vgdhoJQT1juTpRJQax
KZ+JahjZonPiifR3bpUSnzppnSeUL+Xl7KClSfyflHgJKGz6r2e+9MTHj4UiiuXxuaVO2zrRjx89
t+3cxPIdKAu8/jJIaAbkPXz7vfbEa4a/WKProP5n7HqWN41WUvodPg/xvZlt++fthCXIaQSRKJdw
jgrP+9aK9cCTKAJykQc8yvjafXX22VZVe1SPmCs5krlO4fVsydvKCgOKLSNutIRii7bnS7rVphNd
XTQXoc1vS1T+hd5WdZ4e+Bruk4CaGaT2IWQQ4zstNM9tRKX0DkItM60dhZtGiOyLjU3ab2aMCNxv
lv94rKPcfuRJFm3ACBoTcHwqCs8VsS+8VExNTH0H7H5OWUVRAnsqFNKwy8CPVRGbriwgavnyMLFC
/1Jzm23gKTlLIh/vEJOdzm4eV5gF9IyId3BGRfXUsKw7J9bX2UWlXrikk+Vdd40+3Lg8FyF1IEMG
Dr/jzjqaQ6mJxyzyZxUZQe3fP5aS9DAuEmSczYWXUqM/plBBzRpn8jduvWN/L7JHrXDyzrYC786D
Z/uHmFU1qIGxJ2IfA+8W/0hd3fx+XkJ7ZJiSy1hk2IcbXcF0SCTbTYy0RBob9JgLnX5CUN16vw7k
RSAoCDg77D7RQPqfWvjwVF/A3ql9+70vuCmm7Ci0IQYGrrqjzAp635Jjm4OPrCr5SksIl0b44tmY
ecknnetaibHO/0yxWYd1eUky838vP7oVVElIxAph8rejs/5+AfHvCQar1twPTLwL88wVY4TQrX0n
waaYi9zKhVEeVqpu/IWMcBDDShap3w66dNzLru4Mn+tfOxirTQJCU4M++r8W20FgzcOrojDDUXYO
94ioh3jJsmBq6Eh92/PQsEmQF1MsY1Y4V8dbG/hpRKGijJTRtXp6etnN2fdZT7mEgH+PBL4SzUIF
kY66LN7UYaQFUH0rB29Mv2Tre2ckVUgGx4+ii2EzU40/psRoVA9xg4fSNTxjVoZaefAfVA/LZbZr
q4Qe/HmheiJ+EER3pCAxIa3tKRK86uC6WbTECK0Ahq7nstnP/8CITP/wS6I0auVkhTV3t+ioMaf/
7u+VzgKNuDt9V/QHw7lrd4dr8Kj7qhst2UXFORWVZUIf2/YsRzw6tHjcaTwjST1PZTTWZfTugyEq
glhDOi8eoWkemzLaIldUZnSiJAGB05ML0ZaDuu4P5Ym6wZLZx7kAhZnP/dQt9ad1dMB+F/f08x6Z
+tbch780fFFkSB552n2qlLJuKGbjlDHegdq4aNg9apr/JQ1E21SPbLE3lWqDrUD3dIeEhzHdPDB5
ZszWsW7mlacV3/TEHVRfgWzAu9ZuzCze6cAhRwDThBIlHt4sQrGZ9Cp+tMAKENsa1mBocJsoVvGF
mWK23JR+yYfAP8uy77Vd32x/e6BV0UPPcfLJHNhU5Q/3UGrQFeA7Z71e6wvYoSMoFpFfNM2fC6hg
0wQSIb2lLnFDlBNVOQSNjQGG7Dkdf9bjDsW8uF+6VvqFw9H2EKaJ2zNhWI2lAg4FasS0lmPHv9E0
WGM7PCa0jLhfeE0gOxBK9POCQu+mfBgdZkLeudEQtVLTV2GIF3N6C83S9bPL//kMAljbkRwccPQM
JZlrAIK57ec+0dLuRDRA0nFEdnxkj/ktcwtRIWW62gazmY4T1Kt2UlZB8tlXb3ue91h/aoUbsq/k
nVTWj8r1svw8Baz5sDD50nW1WUwLPVO/m8LqZd3BVlVJvHMqgHRuwkHJ35Ltmt2s3+ZdjVaZj1yq
aevsLJnx+6I3Xilxmvpb9aUX+rzn7FcsEbDu1QRzK0/0G1dVnwPnLVpiMIRzbk4sflIgAmAPngOa
jBld19YJl+g1ba5WzEgBKbe+HmJOdJFSSaH9Zur2lVv52ZR8AUIInnN3Dl4npGw93Tcd1nU8djlJ
xX24Qe67BmGIJBvtvo622Ue16YTHora25/XCXISVXO8stiQOMHWslnO3dwKlVFTkiwOLDxtZ6wWB
WXqaAx2gKZTY6wjQ+VaYFNJRJrnP81V918Gkw+mz6YPf4q6dT0IPJ02hgrFNifSKH13d9l3+cnZ9
D2g/xj7U6SDW5o1//0mRFBhvfB9nCrdUIgbpotM8Nr6SzDeHbfPKUnyIYwyireA4vjpAtJZwZe64
KlS0DQobZiM8qgoDBkYbCR21yjaC7IOYBiSwa0rUccEEhQoQ+tcIhyprkTfbM/qF6luGwcf/Lvv0
qoDl/h6Idoiu7j6sN3H/jL/8NArVWggpqFBSrXk4Z9TYDvyqTAjAPIKcZ3UUyjVTO6MXoPZqKi1i
eAjb47VPoa3dviaN2n4O9PFaJRKyJofeJ0XRg6ko0gQHK4JnMptXMpxKpp0N3ZxhctMLBKY4L3Jv
p+gcWF+AalhFBWSNjtcXAC9Lfr0MsFRRk03bHZMm8FVtEabcMRvHTicsZnHtM6nuYV/nzEoFYv72
ZLRUemx9hGlTpIzVBCq1ifLmfTaMZurkJDxcDx3mVAfQtwYWgZT52k+wvSH2c/eiNgJSciHilJiq
sShQUvIdVFxmH/89/cm6ttDT0EMCmHkEILgCRM6Cdl9IJ9NTcO868oM7vw4cFr5YueYnGk8kZUU1
1LmZpd752G9ZjebwDaw2ww1AKCv017xsVDRv4QFbiB7ys/mKgCTyP6Cp4Ax5LNsDcg7LEV7/v1Qb
OUSO1+jZ5PUDgKS2JoELoNMvJKSW5viNMIBnNHxJuUvZErGX9AGjpm70idCZvtBAoWpJjnlLrhwA
9YkGR2MtGZzFiVPZN6nOGeBmcSFIeLTMixrxjvuVznnZF26g+hIeeq1EwkibwopVqh7vL0Audn0p
4XnWlc0TDNjtX+y2W4QqOxJFHBS1m3XgEfpSyh4zvnTMyw8TcCINcrVPFJot8mq/X5Ax/2lHcjar
dmrMuJq4bpZtPDxH2lFwzyNJZMjazUtJjQfisF9BbH4hwxh8yWsOuopBqmgjYvyl4WY+wHF4ldTY
VjXrx/bhSItfWxPvFHHykYTJyfbk21NhnboiBOmYm7+yKdCTZ616K5J/Ue0NpVf1zXRhlg3YN4bI
EoRlZPqBeDTvCUSW0wzqhSs+TMWzzsa6t9G/JLS3Mmj91x3TVYPf4pAhEVcuxt6yqZVcY0/NqO5I
pKPpgxj2Pf+T5wiaaa2KD7qqEZfK5H1vUjJ//+pBPdOmjAAC2HMEJkxISKmg4MgB5dP5BmZFy423
Z9pDElNqBpwMEJVPdpukny+I4cEBdgpnR1/PhW2KgtS8/AU2Bo2zuKr4q4bQ/GCLj1Ai3z4KVQFh
3/CUZ3nWxQgwV8+M1pBzIImBZwTr0/21wyadWBUBEzDJcXUFtvQn/6VCg1qhOesJ5+qsJuffIvnT
bmJBm9usP7/78MOWLt5BSElZPCrNECFN53DcWMJXjEN1DMwCsdGLXSK1b7T5u8FPdf/mdh4oOr0y
7Ax7KozDblzFvO82s07P97PnGoJBAcXk/WEWt5E6SlH/5wtQ8cgO0je6NRjSMog40DRxAGTjU3Zn
zFVHJl8FjG9dGZEJbHRkJ1YCPiG5bV/I1dyVLgnaifjpMGIj7HW4YbJMyR3tj1S8huwFabRtX6zp
8OmI1TIEcvXp1T/X4p+CmVDIkPJxmmnY7hrcPMuNGO/hR21Z1iI8bYQhxSogFDxyro+k7TYt6Op5
gwQc4zUkDID/SpETB4ai+oSFFNFuy0FU+qaEXDcwwrosQxxDMNL6nBjSv6yNRd5+WeHUK7NPsSWe
LQBNzv/4FsbxFYQIPmxBIkqoFuyvEKqEU/0tWOqtjHv1entndBnhFOGwOc2MEfPBTeG+yvEgtAZ9
T+mgAr1UIeGc2lUtOnNk8HLgMuCKaTjc9/aCLXgWJmwdrTBcjjp7pHfy54zC6sMhnJbD4upfJeIL
/VeAP2z7pB1d8LC1kuRuL3b3Zx853YwfJykepwE/OFp9e4UHqVZsvNecO3Ci3dZc+VEZtLjx8kCG
YH/Wmben7WXvijH5dnq3Siroq8WdyQ1PWh+0mY1daN67bIM2YwjW5Qwhj6H0P0x3ofAG47vA5rOF
O+WlMfMVMIu/rn1GDif0PZiErs/XyygxmOsxtprQ4w57xLKNEovmMvJGViUOxpjgD6W8PAI44aPF
8gG339zi1jXG/hpBwYFWPV3UylITdAZnaMUBS+eioFpWHX29uHt0Nu5cENzEeHFFPToahDJNl6Mr
wzpRvL6z93S6C0tlD/WsCO/6WyhYYr+2gzd21sb1dVjqKu5p/pVaPTNdis28Ze4oqa/gOZpnKO/3
LzHylDmxsT5feijyc/cB7MrxKHyghQng2Z0N7XDyqBeofQQygBPBWNcpLFflfRFrw00IyAtDY8Yw
n3Dbr/KVxeofrT53PQBHtZIeIkQV1nn99CIQXosyvsiwtuYAHafsWyAEJrCmJ2quChW57Vo3aeB3
5NUBXHAyhKgvBe0zpxjiNQYcDcw6SV5TcP4zNrOT1AnoKAnSfabkntOwsCBCG2/Mhf3TNA409Ksi
FatLwPh68qUnxw6FpW/33b4sBkrLubV8TOK5hV8hL3Qs+N2z1Emw32X+TZ4J8KSwiwggilwPLl28
zwGkOtCyWrhv7egyWhSQjBNE8zH3eimpf2O0sSd6NmqM5DdaCDkVRmek67qR6K3bopwPGTpz09hA
PAfe7D56HyQdyVaure10KQSqF0bj5GK7Q4xr38v15gtZRQwIohkxuOZzpQETB1hlHDal8mnyLB2S
IicmijOBde84MOxtoXSXjzjH6FFOenbNihH+yIadzibhXl2adVj2sVAdZH9EijBOajVsX8BhZ57c
BfUiufCL1uTG323uBCE8SgV5QeenYLZvyTN/ob3Jgumt65gnXUoJ3RmXYuawxel8cNPD2Gvguk+7
abVJCRhCMvMjzvquMZitBnx1+NhbUwMtVxyRbctnN0xOtG6tu5KRe9CCSobT1zqD/acxFZvVOe0M
5JmnXxf9OSe6QgqyWRUFO2kfnKcvOOqhUxGzkTG3YSb6qpCs1JXKT4OpQArXWDzsN3/tuaXHE9ka
3k41+LcC8WltLIwDy3iJ2KeGWBSqJjqiDcsDR+z9ObbM4DQDFbtfQP/qgZJUu989l7DzKYm/GC9f
hicwbDxEAJT0sg/57TZPffYAEeOelJCXsWgSv30HwYGlxusLaEYCkJCYS00YoD8ossRLTjLGAel2
RLOrc1UxYbSsQPspzaLOswEXBiJXNddMu6ky5QUGZkFq1UxnNgfYwZCDw3N+vCVUgvlr9g4rEok3
o14D6jzA5XMkgIelhizXIzuRd1z6Z5LYm0gYEpp6TJ8LDjbv9P91Esnj8qM6LWbeP2HMkTeesEZG
XvgLYvDSxBGJUXqzwwBoWQ7xBCyGdxJbR9z/THI/1pxBZ2M8xvu1V6ULWVs+MpvS2Y6PEvFoCH/j
J4on+Q2Lu5gB43xGHWrruNCKuV7rJAr+xoQOFna4keK0347d2aLZCyP7w2VCqXYJuqk7RzoObfcQ
I2J9gnm+3nnXCBdYlTDClYPniGhdPm20dlXKgGQSVQNvqJMPg+IyPDrLcyojFsSV/vnQoco6vL0t
kjzx+o1mzlKGRDq/+zl6xdHH1KYtESsW3V2p8IIYsnUqqtE/Ngt3uDrP/B7T0J8BkBQ8bJvqOR9/
vfnLVWFvoOe8Kh3j12aH5qUlorO5qSoOegn4AwJPQcR1Ajq0vLGb1HVGR9X3b9LBTM042Sav9MMB
uv/2RKh48pD6OfaiKHTgBBDeseo2ZS/BjeaS0sV7cATkWl8+Q1zli96w/mwgl89bmVo4d1TwLhhE
Tr0IxaVDHCkR40MF9eYHE8q5rSxZw9nyqo1fZTcO4dT9uB6Xj4JqVtEZ6pPTaXDMtw+puAP1uTc7
DDncCnOg+Nw+Hw1clIwKxuhsEk6iJ3DfF/DEzkP10fxT3d/5K/2JmBagqrNuzPUOfpngK336lB57
6SHRTQf9DgcAYg3M0315tyLlczUvPS6cCPE5uabtJaUQxv96DseaFPRfbgweRoo1nqY5F0wZJFxg
4xDhfwyVmKN9ARMKDj6zQE3SU+pxLMrCHU7J6O8TSM6mgB9fyIANxOm6nRn3U/Rsp/6i/HYYnRCU
NuzYndUykFl3MzdlzNcNEY4//VYfnfZ2I2cHW1JAj1FoYvWYeKFdsH/RSYfDvmhLz7kHba9IuoB0
SjMDe2kPldcDE1quSiCo4SWrtOMW2ZV5O4S8FHk9F5CGJ5Plx3xTc75xBWawzy+nYEs2ZfsNPFIQ
vAJJ4fAZrIXCKVCQ+gtb9qeKNycEI8wNbErkjfq7o2Lx4Hq3jUFiwv7VX6WoiW5MYcSCikKmm2SO
1U6nPWqTRegIak04D/08OZGzBLGuXCkkEikUzhoR0edszLfA7y130fGzu3sne1Kg/2fVw63JzbEk
PRqBVBIDK2RItw7KPapobtt6DDsHcGWaileZ6x/eI/Jx0hfqN7C4bTiU/Erxnqv/DjBXjl5WP2hb
dUcql86Vt+/z8kyzg5CDaPPnvcSITFvdi0aiSwt69FCHOjYIQq2QRj0jPSl9SDrnAM9naSrCs/bj
5Cw9BSqoS6KQ/jgLpV+AKStThYb4f8+L1MzdraB+tForq1v8eJAd0ogtRx/PZ7FsqIiE226h8xKx
WcqNED+nQmKFIks9gb1j/VFQdnejLwhOJ7ouphNoq1Ap8vLgAqCIAqRuEhCQI5hyHRcDFWbZ95+u
riuFK+otFpYdMxrEyAIDRYk3nleo0+aLqHNMCDyxDgme/L5ERactxau0nJcm9xEwURA2/aDQlB1p
BmbIx9PclgDYdScoTOhXWcQXXIWPB5rR/hjDYWJnpKOjqkkqJM35ZfIGnA14xwpNc2vKqWYo9/c5
pI36bw4LDubJ9t8tzGiO9bXy4GLT59bgJFKkudEYRKzejn9OGquqn3/1sawQ9+B9cEnyURHCST+i
PlkZszum2Ygh6y0Z1laDk8ftKHI5y0CrkZ9m0Ief/o3GAJ4COe0gurwDzdDnIxCpsYUlCrSs6uZC
rYZx9excRHldJSOT3jlo80dxTyAs445YCeBmcQukEO6852ez13nHia2EAg8JtgLSq2f9gwenHGXw
uW4VQDjD4oQgQ1ktQp+1RdvDQ3gR0pSXkigRutVPBwaDAJPx3zFu5IdxzRHoJ6MnOUtxEfxPF/9x
btorJlNITuzH7MAzwhjDxglnPqSkPRYoWv0bAIKMGrXRPKzLsDdo3uaHkBHI7ija8JuqSmOkFmCt
zR7X4ZzXHJk5eKmVXhtQkK1mc4C/eY6gTP59i8iiYh7I+K9cVKtobDRJttoLrKHnMU50BYQ6yLgJ
d4dbwRmjfgRUyv0ntoTMU0HodOQWZf42aycESmmeowDpKjkIrj30NTYU/vEESPI0CvjaF2jKuzcW
y1a8YXvQIQrkQHPmvOdM/JfYVAwJ0F1YcWxnVmJEADBHku74NPut08PzRepr57BIfz5PG1AHt/fs
j8Dq3SdYvsQ9ZnZWDK9e10/t8FksEfC6+Um6yreF/B3pxNnBaDmBOz3pe7w+pAQrhQkCyWKEUBYZ
2S/RAw5h2hGSRVJ78pWRGwiOoFJkUdlKb3+eTB387oAcwwRcTyWIEnPZvOZIfl0e08oDAGVlmp1Y
JpnxhSa6n3V24G00RTmmlrbF9ZotBDl5fdtoX9Yl5x4Fs8jDzgcXuIpmlBYw7DsVy99voE2aftEa
giV26yYcxD71+5hYjJBUqUItFyXtMcDmqczowRXeUwqjHQsG1WvMSGlqykbozs9pEw+knCZmnKQc
bCw3IxexMo3hQ+rhTe6f2Z7l1zUohtK31579ibtLqEdoFjBN0E6yOwy/ghG4UVtfLl7DXu84DF1i
Spk+8zjOJ5S/wCPDMCMQtH7Rw/WXKj63bHemxFDe/OVABRIapWYVj6NxIxHXyNA7Yv/iIfjS24dv
/lTo7QP2EaIOgwyeWWsx+kpJPSClvYiey2a2+M8tdGqp8hXv0V4FQdH0pj2udGH00L7Sq40LudEP
EAeNqRlNF4lBmPQNe0xuXVxUIjdDLT2ljoRkzPfoqupQYHCRwY2dxU17kc5ES7s2MGlymTI7z6FC
fHdmyWLtAJrpPtltwUQBRvzy+IFPt0HvRqAq4pykYm+PK/Qp/fbKAC3Dl+vj3uDPwePkaMYnudJd
ziucBAdugpVNYBfn/r/R0G1Wjd5UcO82w4nXteTOMYWQhabWqqy8JkVx7Keznicf5bZVkvrdOmFy
nQrK7dHQbSb0/W9vRDDsLw7FilYjndWiWUHWdzwXcKJcujMXHsymVjRB7oYbU8bgNLGfw9JaOjYP
R9VyI6/VqYqD1FcQrhcm9W7g2gMiGlYElhfTAgQKbcxGmWJ3dR6MERMItcuQUW1hGfcpHsNKY43W
EohVW7XJh9W9HfhPE8RYEFqzI2um898+bF59TTRoVo/fD/pgCMD/U3Yx/AafoMnSmGSJ2GqXG9IV
bMbbd/6Tg7tB2UcDMIR4kUrD1vl3K51gFs11SML7LI9khv4ZYjaL6lg5WXkIUJSPM5YAu1avD+fY
3ZZJfzDCV2GrVFJnWona4IsqaSHlmWMr/YpidqAuQ/rZ4oTWdZAxzxIVS/eQ7MugCF7Xt/dloNLo
C8+4D1ASktd7DOkdaZ4e3Gi52/f+LIBsI6NLM9P6DpB/8ka5RLgeSHGTjQ9mAvB7USRIyUR/DdNc
vvLXewOPUAMoO4g8DNFL5U/jR5JX8WRs/IdUVgbR6y4GI0xpd5q7a92jDUCMAGosee6GMiRuN7II
4kDq5J/NmM8MDR+OzIyD9ouIeDnvHH4BOkFELDDVIU6M/w2kPBipY5/abB2O2aF4cr+TdsCR4YwW
G4XkADVynNbZ60Gy4z7QJHcltBioTLCCBdrWnPoSP2aQlTJU8ulHeA9o1DgwI62TsV8fp7zf6I6i
hyRO9NE7TrxAA8CjHCoKLHFfZ/w+3QnYNWj7b4/phMmI9RpXKjDW0IrPgKWyUhc6q0DAWji5jlCC
HdlPtOEPvpJ+er0bSd0t72wqngeeWkDAX8PWwBQAJtvBwe8/eAeL5ud5+HCPjFN90S3byeQvEIwH
uZGYA2yfRlnKIxhNMYPdrL/EHH3HcGLBaAmlYlw2Yt8fhx8IEVSBsyMR0qpJVGkcAmVDYdcWQSVG
s6NoG2y+qIlf9LdW44E5ReOJb4NcIA+SdTbmi/a0QiySicGjzEQ4OHNZX+dI7H5nSlTaQwby0BMk
JVzSG+FudlxW78ej+18fYdPocaMKThvoxTqb6K89HXO9BWGhb0SnXp9vMMEAZkCCnlHQm92DyIFw
nePfF8l4NsoB6uYrUXOFm47pdZVgMdlumWtFgfeqSB9uWztMbk7xXKSPcdyQeHilQ5LhcoQ818oP
/wTgjZzTTT81b2+q1IX64M83fl//Qendhi1n4EsGD3KAmJtlaFGPR6pc651qhw67XnPhc5ywT8qG
JNd8mpX0Sp30No3t9axZSQs+Hcu77gLJWOILUn2YBKEx66xCbtk7Qkvx8/FjZo0Hk7bK2ngSrMVU
2VNBiYNcUenAMSC9PoWR+Qnuqsnzt7GD6bqxyApssude9QtYWY0LMTzg55y8rscst3icwUF7SE1R
GGTluNi1/UudCAcYDZPg5ewTlN+w9lPugoqtkY/Wfa9JxedmJ+FkdD2Ye38MtNFEJy4rVDl7s2IA
7w7X6wwnQiOkSX793YU7r3EJGsSZJQCzHO2mSruGs0rUxEwzMv2lrlQ1jOJCkIHZTU3iSuDTXhSG
dOKi/9Xsvwm7c7+REWHw2YyQ99VmNP1v1ZG54rxD2ZTyi+J8tH9Qh/fDc/cwHL5GQciT6CD/cf92
uzoihiGBqwsR6WqDOdANcM8iaUys862ov3i0kLUYxBP5gXU7j95ErTA4AHEFUPclBWhb7NPU21qL
G+C5fzrTH1PvSFvPbl3NG41RqDUQ197mLZrxp+QYRYwjvEVPcRpN8lXVY1IsWnOx/+euird5RiOO
lptjqFodk10cXEEeu9eLJ/uqDnuybKFEluOJqLjWoZFuj2EyC4VxI6w9r8pPGlh0K6zCl0sWDR6h
ggvWTgIcxCieksgfPzyuXO9ZNvqQZ1hN3P1meGuv8+80Zp8qPp9xTDTXL7ohLcAYDygbnaaMlZA1
eucTxSjRdtvrNrPa2ZP53ePIoVdUwsq3qrfym7H2FiVE1HxsZDSIntiB4+8JzxhxIdvSKKxSTdgR
nM5CdeqmT0m+JGZENByye3hRfUAPaxXCpPfUSfWTvzKF7RJiH3p4yMjgHN5S18buQO+bJKNfkhj6
cse7tXwXBPsHEjHPjoGjs+MR6CpsYN5hqHLaUZz/yTzOhqSo9FdZypEdXAALTPdvc5y6ipYwiq96
MHmikGoZ57Ok2C+wLsmhTaXXcPb9JRdB6+DyRbhgtmE+2xXhW8MGopitJcVqV7+HpiifILwV/uZW
9ehxSNHKwX3phyJXXIhQ8tFRWpsUM94t/7g8UVVBd+04r64AMMlTZTBBIEW2W8m8bgPuJhZON0dn
RQK2W1bBI4AZZLYt6CCgbW/x3L8EnB7A50eVk3AUzihNR4K3a8STkmkZLKSwZZhfGW5NrNwjy7S9
VC+h5qs3U8OvsuuAJyRRsqswcsVeCtS3GT8GYFaThDS0LDYJ8/uQMvY5eYHy+pd7kO3B0p/EVzwD
OOjq/XeOs9nCmbxZXbSaGOeqEaM8T3kAlZYIxJ4DIBTt3FUE5YEzC056cGa8bFy0nyDnOcHLmish
3fA0RX4NWQkamhkfWlpYe8meNG5cFPnpqTD4MAoyDkgXns3O/uzqqZDk+1bAfTA+X8d/yjxkP6ap
3vjNHZMUFMcpHhAb4TAy0dvljUi9vq0l9YRSFHwISmrmTowaB8l0tZBLL+QWBoQd1pJlIxh01K2n
uQIVs2F5ThrRIm8hNJ9GxEm6cgX2wBHa23/T1FKnJkpGOp161pBxs8um1AhTp9siVInMOVEUMZuU
Z4HYhzEozmr5cyqOH6KTD7lNseljS3bEiyztPZeHK+cMFh9hN4fCkhG635Xsum3NRUj7xoyJrzb4
KslLxOoS00rL+eVUVxjGj6yHKU+BCc9wvobu3WzZrOkFcbhk2JLW1JZIiQchiJzXuInKfezzELJd
ulDPgHS5NZ2zZPms1ueJgTD1DK44BnxspxbgGWHP+U7rYDvSIoiIb7BovoS+aV2gi6r0b830jkFp
br76IG5RwUtKD9bC1aWYZUApJG41tSCrR7bKd8izzGA9islduJcwSe4LFhjpRS+FPdNMEMqtT4+N
G1uyopdgz1t0fFhwsXomqVLSzZAEsTVp6jiyLfp3Zh2r+e9CMr5klnU6SDhjzxdpk611Vs2cHD9k
q7/TCMxBdyLZE5KTG8pjgHtPcuOJh0MBZDwehx9KlChjREW/pytio8p037SVOc4v1xZU1+5KIhPy
pPV8zsSHY0RoTzCCDZVgs/8JirOEykTxRgMWx420dElsODVBCqIDyyjRD2SZ7VAERXfZ4fycuQX5
xx2Uhodblq9h2gTFmzkQzeI+cN9yUAyMaW82ihJ0QNpq6hEUvkBJF2l/8aLq/5rlDDwOt+8MSLcQ
09AXVkxT+HD5HERUCGod0xAhBiUkRoMQTh9dwLGFJoxfTSsUmMgqLjUDtQwopwvGeyWQqG9Llc+8
fGyY9KCVL76UMIXdP+yKN82VugEnMiFxyL6EVMK0+sEE/sxE/9lUirqdGxDAmOdMyU5D1DAALbjV
UN+tTs8GCBoqJqs2zJsZKbk7sAcNK3VZTPPV8kVfL9QqRlqC8o1yu8XS+QiSNWwnt3H9k/d0dU9j
GAN6+Qepx+Ba2MhS55A/JEDJ3mAAbZ5BTuqa1XUrRQc6HyIEWnR+K4l4A2Gt4jUI1daU9qQ8BRfR
tk1N5REnZN1PnXo/1Fp8Rp5rlGyoQZAkkKVwZbTYGUWo7DmFelO9m9MNZ0YoBD91lw6gunV4bMsu
SwgC7U67Xc/HyRO8KY6Or3LpbeolbTJgnlZ4erZWzu/WJZB/w23DvUmDyCdbZjNis/zQHmSjJoyV
iXQ+i4CJ4IFs1Zxw+1ytwkSZkgh+nr/6txPeTOgpDqJ/5s7I2kDYWiyqt4gPndhaU1bq18UPZhmg
NJ65edms16lYslDxVO1yheuA6VtqAftnAcyJzi2HKZcgUQTpHwtITYSzf8V4Dcx6WIcjbs7xcHBm
CDNuoxomUvXIjWS4/2GBBSgbfX/ZjTIiGPEUlzdlLyNheuKHftaju3InIBIIX7BP+vBOPd+d44V8
mNBhS9/4AA86AzjbVupWV8BqqsBVYzpBcm8Vqc/g41NNwUFBGAoZgzFYMydUP401LePAELU+pGZe
I/klyvbARSZqbHab24MYujBOf35tYYSFU6fi2AC8X2iagxGSPcvQkjgMccFEuwyWj5jTC+mrCcsE
DrA2Pn+NCprJHVGvZ4R/LykVquGXCANZSJg/synPoilDGEEwb2T6DTToLeUMGjmV9RnGxo1SvxtH
4+0BSZp3yvvUxSevo7t5DyGH8AkOaCpQqTeutuKQMlQ69ryKKM/0yhYjiYxzsBC8cIQ/HhI/+A90
UCXfd4yGMuKszX6n+WKDK/PR+Ca3CVlyf0TLFO3+lskOcpiUk1zhkyJ6kPGQiaLSao8G5Q9e1sts
i9+9yKgz7AJX98q3DY1ZR+mEYL51cxIIdsGh+1XS9mX2nLhHGFPzVWAi8N97EWNwpfZ4b69TZqWZ
VRs1uzEnOFrVvz+HDjjWnVAgYCVhxDhh7Icod6v5nIBGqD5XJzMR+6EMXgLJXHvvySw7w3NNUo3a
Uqwl1ZI39OTT670AU2MO7mECSp1dTeZ8uWLdLdIBxP4UNbZU+n3y5vLCiyBsNUa/stS08/ZkGCuw
pFcnuvQC+J17juMKgtYPqohZdqJUwh01u8NZHjHlKuq96o9vtizrexNoJnZszbHrs8sZntQgM6YB
HWG/NLec1SXTV+rAQgEc1YhlvC3QdYDvfQ+UJFXlDASTqakfFr2ZGdjCcPV4xvTvSfIT9ruLqKnW
XsTUOV1TQ8jjonVMESbofflvejmc83aICe63ov875FAIUVFsDAUrhmbi2XY859SjINBsk2HznR1k
NqHpgS3XVss+PsKEIldccpaxufunKMXzCTE/HmyzfVwpfSvzWp47U4+fPH3Vpp4VXUGWC6xIiyYm
g9sBkrxBDV+Jw1WBpVjWS0nNpXfcsMig5G+UFwuTsO94TE2gxo3slg5PJKrGAswr82uErQ6lkMb2
0o9R4vpFNos4Li6N0zPsLjqg35ftjcFanhBzyknOs/y5SkM7p7ojeHkoutvBxYaVGvFpdI6+Cw0p
pxRxYCjtbbc1ukmG9yJa4RMfg9ghZ3x3T5vQ+HfKi5rvD6VnjXc9KJDn3UFkbaHR9400dnknNytV
cbYkmsLnTDtV/YHuRmqUetqe35SllZnfsu5+h9u+pESOiAcQgmC/AUP8sWOgfR+JX5MPelYuyiQh
gg6Lq02gqakqVOMdD9Jisz4pvMr0xEd71r7Fpj9th0XXWkfaQZVgfntigNjk3B9rhwUqAKW5hL7S
Ic02yw0U+KPoAVNqxzuZy/Y/CIQeVNXEXDKIiM8fI9ZVoND6pj7bjY0DBrkoBS0VRMSc7C+v4VwA
TjBj7yDqI9Q/hz+Fi//zs8OhcrmNoQURU4t8p4ulgRmLiNhLnHlztpV10jf5lor1ixESK+VYgDjD
zdqQewXHhRUQXYvNOBQ2D4DXdGLwH/mFM/TGuD/mG5xsh6HwdsTLtsrsoG2f4LgPUuQ91+NBQs8e
EHKNWvKsoHO1q3GRUV+72ioeF1XndSzBO3X+UFqJnxWM6+wYj0vSddagYtGTu9Fr7eOU+QRCwmhH
SGMzJ9uAExax6g5YWnkDxomN7j980C7xDX7YtDXjYk0aRXTd5CJTawKld+CWifRJ1O24jxicXVrx
Ke37JLnB9+VNzJ2RUPBC+AqFzPM2uAUbbHjJHt4eR6l1HawT8CAm8ZzUKj/Dmt/dvs8dPcy6uCfm
7OMW5PgUweSgeCLZIDk1VAQim/gpoiircv0fVa8icWoLXMx9kDSSj7ssQLyO5NPqRqxl+CYqsn4H
8Z7VjKXFsn6QYzQ+tFA8eZCu63mO8buwHoUiMdWZi6LxNYiFFOiI7Dmficfw9e57AbteNDN99VRO
TmXkIBp5A6XjvTe+qpBzxwaPTOQ96o/hqWMtgV6SEUUlaEhrWHGITfxJdaKwiWK69DoBRKRCJt3y
RzWq7y50GAJueZYuqRDIUkpqP8+m8ak7adlIUekQZjcOSDP0ted9jxH8GDJH8IFPC6Mcu62N7ASL
C1YPxYIsE6gqCJm8Oq4RKkjbEuo6Ye5EUBYTU0FU0WLKcmRYFk97mG8Z5hvQ6ZaI+y/s20a8zKr/
J1WBQiRhq34JA5QJJBpGlRi5wGsRXyUPS5r4MfGAxDGBFpZAYS9iH6ecNhL50rXM9EO4UkoaJFyF
sUzTKyoCt7DBm70Z7IIoAgYbM7I05IYs/CFUBy+YGHFxrcY2Aeng/et+Y3AnPbjQsK3VQTwaeSxu
O35VB0V2wOe7RB07ZUQ7/t55ddYMi2bRPh4z0rwjdF+etMVeVq1WQo48GZB/Nf3JDe3RnAb6cccs
Ux8TkONfXK44ZuQW1V9oGG8fwCy2BCCE9ZPr71kXOzViTBeqqrZaJbRQG66eMFhatULB5Uhv0m7D
MtW+BZvmNR0WeNpEHRhG635YTswYtEGA1JFHo8ACyeyiwrew7DZyNf46cjDD+VrYPofo69XeCZHW
nnJgS0cU0ywLn2eTieLWJ2lCixah5LD8pKKzLoxqWIx6Q/B8PM7UVJ+AfgAzqAwxi91czz1O4EYc
U4jWrfEfiaCP1/6WwWbRdvMc1CGR/Whvwg83PovLRI20HH16ox5Dae7l1LmBFvqCnZTD3cnEzDhn
sljfpoRvmRL1p8VBc299XOMIXgfIiRtcPzzvsf9q2nTzbwBLnv2sJiM5qc0yyrVgceEqCd7iwmk7
bSQrSCJDyUfKklyeLaHyQsDLAojpJa4AGTS+YTFiO4WQqlDhOYC6xzM8y/BVUhHWfGfyfxprWyy+
Cbp6ce8/UYMMcDEBdC6/eWKO1kX5qZzcWZea/lugIB4x0Z+/FepcesWIundAO1P4/C5oadtAgvf7
SXWJwpyqK0lMF91t5ampF6yyKY1U/wUrzylrEeydoTxSd+WwoIX7y9u5TDhir6Iy7JvOlW6C9wQI
BEpn1oEUchLDuUzmeN3x+toZ/0R6i//JXPGfkm6og0PFljNxcQ2EsYXHBjE+8GBQhPY2moYkeMiQ
JCb/Wvc7U1pOM1fVRXniXVkTbyWAmT8C2v6eCzbO3hzfwxtGi3ZX0ZKw6xRQJR64R2vL/zd0i7Jc
kQRKbrfk6pSUN2gj7ssSaKL6zMe7YpWze71Hc+JEgseH5x3uAdbOx4+oj/WW0HVwvIT96cK6cgBb
/2jZYEs2PfLxKLBXXY/E/oTMD/Qutg3mTbY686XWPrbs70pQVJjPg2kq6yH85dMX7k4Ogmwuntg3
GAWvmtXM3jhjiOjta/Hn+woq9IpXHkIe/oyA1E9hbWuJGaURLL+On9sn25gdmhOpjgaJMsTF/XMC
FDftO5uOjCcrmumrY3oGaLqX5xHkdzJuWIkX2/Kbd1KepTlJRsmm6hy+Gq8+MoudwL6bSTsaSjyy
N+nYVP51Z/DJy3IvkPSwkwz1+K+f7ElV6098NIp3ykbuel5It5KWUQYLWH8m9i2i0t8ZfYozAdw6
Jxvq5bVWP7vX0LHRO7jtip44b8hSixRzB4T86OHLPy4b9iOYik8ECs+gE9heorMQCoBC8pB12Eey
TNDy3ih+VRWRZOE8QZ9GjsittSy5VBCChDVMJQ649i3jfywTGgL2H+sPyUkHfXg3GRt3d49QRXkU
XO85v6n/Xrs3nHa4bejKH9G7MT2c5bCSKuNvIdFCdCUocxsv6fqQ4uLRwOw9RQEyxy4n17SHwwn4
QgJ/710SlKHUk98W+YS9hqYvXEr2l4nYg5jbE4Nx0bBw8Wmnm+ge6UPdGeTD2zOm4zOURoKtrFjV
9XSwxuGcJ+za2+ULpd72i0b2nZf+UnxprjPO0aUUOt0IQfAjBfqrpeY2RXn1ZK+qJfmRjOfcobaN
AKBHwoUxeQ8PHdWwdrTHH15NzKX8vjfnBkyPoksRSipMuO3ljQsR6ynNOfg5JrtJMBGD6+rWS7Bo
LLXTx6TnkY2rwJil6smwcbTRfH0tbU/N/kAGcUfnDYvPPW2h7wIqnBJKzPjPLRD1zael2wL+vLvE
TP9Vh4dYLjkp8vU3Xgi8uSWrSYFbMmqIInVUzrmbJWQYtxrKmJLKv7Fn+gEf+se0Y/j/HfkZq1Hp
IoPaHGl3IoWrIXcC44NFDXHKyGD6c1KVad5h5jpQnEw+VztQLnVb3qHdi35E+S4l2gy4w483ebnN
cwzsvDA5lqmprlyALTx6ToNBR3trUJ6zMnsVSoj45SFBA63j17Q8oljpO4ZUWwDktQYMfAY7rqIt
0aW5sHCKE6WhWomSNDdExQgjUuXwOhnUBC4nxIU/XcFBjKZpwLsFcWEN8ehDOkirWFucNL8nsqTO
Q6/JflqA29tN0SguPdt9Ip4RqrIS9Pl1RzqsX9In6vhGxQa3umYLWdjLonNZzOsiCDQwiMVAajOq
m5W1q/LDOsEhJoakpeW12u/EUXXfXq1DlTJG9lgIrlDjc4bWhpNiy5VyXRHFNnpygNq8AfvHQJW0
Sa/jey1mIg9Cu9+wCx/1VWJWbPCegYHL7IexXycOAurZyL7eRPYWQ3RtnitJar7VDpLK7f/E9aMb
jEBXa2116W2m7JKWyXGxXQY1xDW/gZ6AAvu5oieEqVEUPZc1WXCrFUVMvzRNiK0qEf7OOKwHfTbr
BlezZKHkPdgJV8YGBxa3YgnApGqqI93FLAo+1HrtYwmN++yPyYvZU9+yAvZCtEbf9VgKPUKFR72S
4Zbb4uq8KaVrZXUB9juc/UwzoA4ff45QuKrKffiaj1NYZglC/hvq8/mO91vDzc+04vfCLkDrixfI
uaNcMX3mEltllp7aUcpn0wFqDHxSfYNdypQrivGjecvc08Bv5itwogu22h64F0jAYaj2bQa2Rx1N
c7J3or5TFzUmvn+UrjUAGCIN7nebffRzWEtZauuiqcCHTA9smm7GDWirrfHdnPPNcZnUSbAkCIUQ
uNG3CWOcWISx4RE2C/ZqWx8rXs43n/whelL3xUTDhWvm0MjmiPFhskibUpXUnLMVRVGI0R5pN/FN
VJEpYdl4apnMhWuS0ToPZ2GXDjNa3p7rfK5XKWyxsvXPauUB6YwWrUtLIdY1GTRc6fs84DPmPvpi
f6wsH/kiRSD1mCzBI/SxtlV+XGyXYnd4YZPrS+cyphNDG2iZYqHymc+X8uelYoC4yuAAygG94sVH
1y9WCtMNg+aUkC6R4Py4SEXIcTpnh0vlfuv2YsgEWrgBykVr116KrCfzJv2l1aLDxGcRobHoDNLW
rLOnNQ/sz6aFpI5BpMK721M9DnXMxCfGhi3obtEKV5BNCW+mkLW9i9hi0MYhlRSh/DAeDd+/6+Ga
0nfsYOFxJgpGtSENv+WcC+i0oaCbH8C8a27ti705D9LZN1badDiwvcVLi4b8sRGkRv2FkRWiL5F8
u5Eo6qlbFTUE6TsxlfFRfUgfE2LyzeP0CGQN0wxIRuJd8f8yaNS0ob21XBmBwSszbjiDlbT44Elv
P8ko+1XQ2vhXuZuO+XRNLmv1ndj4KID+gBmmg9jrVxdjf5+Qa8xFmtbhmi52FeKpZkmSgjgPk48c
UkDnpjPyZ4XreYp9/dLaKDoAh+Y4J4//3hrsByYL2fZiOw13ddx9hz7LJZ2xmSzFnAgH0DxW2lLD
5meNzS2mQcxVo/Io45FwlUE7QpbJ0GRdq26NPZ7IJUd+2Vb49L23pIut/QXwgjkVhO5wlrBxJ3T4
aSHf600NWIyNFnGRiKMBA0qTPd7+lc5TgOi8c9olcJl26edD5BkgAWNr8FL+jT+6zD40hTqL1L7z
XbTBpUpna7TA+xbeNyyTW5zkX/bv278SGNrGbIeaNxt188ET3DflXE5r/V7D6XH7OCzb04FUs12q
cFQ7+PMW2qoKo/LanOHoa824qJ2Yy5x+hkoBIh5/0lhsJ/MnK5xGUs4PSSBhepNtumYWV6Tge7Cg
pXccswBxkk0tLFpQ63J8uMsOZLjvCfTXmDzaSlMu4ZdREDEuweVaQ+J38XQC+ugP+jKRgG0CIixm
9Toe03AbUYitsSIJ3O7HR4p0k1dOqhVx3lyimewqYej7A3CNobVX2d3fuDn4kdbOk5uAyvpv/az5
XWM7VezFmH8dQN31rqhnchp/w07wGIxCBDFBUK76G4vFG7sckRAH3fEuKOGKSJbXe5sZ/FK0dz0p
btdBzYW3Ddn6xpOJc02VkPvLDVms5LD9WjrgOmq+Yjs/N9vHT3TI7+f8b0S5dx4lTluX+DHK9iYS
9wHaDdq3Fyk3QXqShfmg7LvS8Rk/rnmL79YOY/8l9NCpmVmSUVGc8vH5pkeo+P1sfUXJKouFGv66
UCCEwKIPb0xlWLBKhnD6GQt6We39fSbUk2RNThz65BwJQW70qIqPpjv43T6GkLuGatRD5TEXZxLg
VS4IePgYV0TJydMnJSI96hiWDqU6TBSreFJPia4AbWf0Ux27kdulZE0uquUfvqDCW0WPnguQyJvs
rm86VC5eSY3N1YmXEv46fMw6tUMjhHBUDKr93c/5iaGdzT2sUL9AL7Wn9b7izntH7YTRSRs1D7Sr
6XnYjl80iS+asngHDQkt2cZji6hmFtYkcSwDfZYNYJUu+FRJjIek9YS4rqYfBxt0HFDL7668Zqvt
WgIuXMHZT1wwy4QP5YQOI7up6Gj9nXYSeaU8HqrB3hRemI2bV4zCmQ4zJvNAlEaUxSZGoN24c+gC
knmObF+mu2LW/fF/GauAHUt44PUvgQ/P6oDn/8auZGpzc41xSsCZZQWOMOuDtnp0aj/uMECS8/8t
kS/5Jbh5CsU062rD8FjBE9cQZbnK2nXSTQ5E4anElcOVR1lmswrd/mLNvMQjFV01OMtXamXQQuRl
0o+agWOl8NZHtll/dVTDwlX5cJKPlDn4GNwDsp3YdaNq0cCaSIdeIUt2VXsOFSgEH2BGWIM5QyFk
bKWmh4ODroREmkg6WRIU9G3ZOxU2cvq454pdNgEX/HTdpHzkA4br1vKmyXuc2N4IMqiKteOT2amL
Ra9/P+B6ReFvSUOTQ5J0U+9RbcSoUFbusgBdykBDSddxL7jCUa9Mf4q+X0J2UG3HGYHKx8l1T5Fg
hiC8G7EvM59+D0DS+N2FUWGNywG+V8eg6VjN+yWE4OVigYLnV4libUPUM04dRHJvB5gFUBuy3Htf
p3J/bNigFzk4bWm4AXk7SBKnGqgHiR+hwL3T5IsjcDstPVYkr0kpi4+qpogAg/qChIfrl+D2cMB1
58TqBH4RlJPnbC6YXz+djA8Qg5sQxv97sGqOE7boJUXoyoI6b+e7WY/5i/R9TJUqYExsvkOkTXvf
Xi0sM4elt+QTAMHeBy0UZS1LCfEO1PczPT9LIvlE3yaDIPXOCriAC0wrSE3/XMIvoIwFjJ0QnFiH
xs87W0RUjJGfj/j7HvPE4IgXiVQZait1dG+aee9iR2VzfSI3I3y2xp8LLnRjIBIBBnFIpghIakx2
15lg4PtEFO8Fw8Pf9Un+uyYCR4SZLf083Wvnd5uFc0nqKLCJvBQUrPNI1W7HTP11Fj8O27iAA5+a
RwCJlwUTriLUz9wZdwxq2ULq5JLD6QB+fg3vofLLneAuTgV1EmJPPKIL8xW/8G7/mMZORQuTlDkX
zGR4rgroMAp2w0aUA7HzxAzkrFlz38OZpOBXL0gBpGDilbDs6U2TZ+8MM5kCS5Kri3Rejis9wDME
mHHiRqlYNjcjKpbb2NrlloZx+zJA8VSUHySi/s09x9Uw5Bqv7J8wkJgAR01p8pz3ezaAaRis+7qD
81gC3pYAgEk+nGHD51MXcyu1ZBuownb3AfNXU41I/1+Dcdk54phqYtyFRVKTGR5PNSwKewqs2GMr
gdLAvS+Gm/00gNYWul+CSjfQQa131FAehZ5Vb70CrVfzRmW0xBzVTm9rhrb+KdOPEWq1UYIVKDqa
x7vU+c/pMEfuyvmJTHcEwfiJlgF1ZjsjU7VrKRS2Sb9AA39Xgz3ZQqwu8LK+TOr3f9gB1zO4FWz8
avP6ae0ruVec0mstEmkAfzCSV2SyPuE49jFWDLrKQrzFq3DZboSHWHc8A9whMF8mIvBXcvbMB/Gu
TUC4rsp4u0oUP23mMGaLhbva7E1zbizmpfgeN+PTjOyHlPJ309ogvQUTzoVrDvEROrbkJCMtyIq9
BuHzcxGabMa4FIDqlZCVSTQsvkCFBO9j+8aSRAVuaP4U7AKjPPbPWma+7mynyM/NkvQN85otlEFR
XiLJe83SLOJE8exHAlZQwvMnSxzWZS4kmUmodS0p9LuXGQBsl52l5urA1C2blAwzQranLDLDC1h5
cFjTXoRYt3upCddhASZONoSoS4GCpoKYVRW+/ufJyXtV/cefOW/M2toVQccEpY/cjXsvPR9JDrOU
zKhaBdwhXJINpksM6j97Uid5MgMRaQiBbPMP1jJStW89pO5wApEf64ATt8iWnii54R3iTKzfEy3S
d74TWWA51HDv+JM7jLvI8vICWR7d3bHJzqGqUN5D8sFXsu3I4nIrtjaKtGCVNoIkDpMGugRhWHY/
+Cp82nXTOXKOg/uKQeMNIHAZdGJ2VRYpHPF0YSHlu0kEgJ+8ekiDAHf88G1VdM1e92uI6mTctx3i
DiV8gcNVQwpmD2KamZFVCzMY94GC4ix/TSKkTIpDBlx+jaM9SgdctmJVR/bmVHkmp/Q12De2tp/O
N77VJx1kAcVLkCWcdLTDYkiBoUd3RXjlvD3sNCm+k8o6M/oAxvEaQDMw74sBEjhU9pIbli2ltQWa
3LyOHvrmII0bDobmmrk/t0j+jVs8vg4gyqFc4wgxR+xQwBbSGdQdAilrp8Gw4MgBiv0BjC+QXN0z
3bpnbJjol64Atg/XjFF+MKkdzAXTU0n1fkNZFPppeBu03DjA+78U8Hs6yXpvXeeNs45SWFrXM1jY
AqfbnjPH7R8VvgmOfz2oANwgQjKXbFwwziyNU+zCi5F0DOY8OaRFdu4smJZo4n/P9JBTBIDEPSf1
OJz5F9TFmd0MetZ0lNVPzFRbRVJ4TTSOG41OPCGjDYMbEg0NrAbFhxH9IeMYiYQ6qIJN3/0DpuvM
riZQw5SlWcjLbX+N2HthsJUL62JNUAluouxjmA6TPcadeEb+3zWzkz3pi8HGtLIMhcWs09gu9Y90
Nh9NF1ZYnpNO+CCWNvgQqOYE6Iyhy11rfxeNA/i2nZ9zXKkh/yLjx4mXOPlEUXbI1rTnXGShD5sN
qIRXjGTiHqYEP0mEWl1XkP3nSy9XoUizjZoqFJMrAYfjVdyRJ7GA6NX9+y58073b1i4/HnlfTGet
XfJzOBb60npxEhDC8EgvuHfz1tKQiV9bqKYzB1EnK09mKzL5IZ//Yf6lrXGOX5dw4GaQHikswdiN
nCMUwZk38NOpMq0EurY2vGajTvt1pLOCZXoojPvPmhIyhkWRS/XopcGjMR28ceEJRDEVXdRYD+SB
aBWTVTOyqbGlxsTVlPAzqgKkaFNAodJDBTOZxzA+QO04nEs2Lj9l779W95yDTfsIEy+hXGZ7DIdY
2DBYyPDqohO0RkoH5KceAXbqBIBBKQkGYQqGEisYM14T6yOdwbYYIIKrPA9KNvZBr44bx6HgP+s8
MVvTc6hFIIh9cgN9CfqxWlNdGmwpK4bRynXoxxaR4z5Z5goAWnHkDK3X7z7qkQOdkZixPtItmr+w
UmL4+oCEcf6FrK79hj8Pz1d4t0yvWXnzIfWClTULATdjEfi3C5i5nF4+An2hukV5WtIj/TxgTi3N
YaEP6zOEehsLt+2YmruUQMoI1ImVbj+H9/KhP15bH6XRlBXIfWhyIj2ujiz9Mc+OOr/irFe44VOQ
tmQoVMvpA2XU5LxPk6TVDLFUZImBySIbUpf6uUQw8RALrD1K3SIG/zrqNBDcqEsjrDpHresv5RxO
fQcAEXbbE8vX4gzrSfsxoZCZxj2/uNzIr234OIqysN8Ufa/w4kiGA6GJPa20eqYOCvOhxCjxuRTO
89EyPz1ZH9AfgnoXgsKHEO8K+K4TxpPIWOlobPYehyXSlQAkRQkL6PUIu+2ElN6nhy5mZTNxQcDs
32C61xI3dBCTXi97qNJFkegMd7jIfSE74uHhgt9wAxSgKHFcX2P/WS69CumdCOMvpEvgNk964GxR
jsDaGVtV0b2ncV6QcyVHdir0xaUI26+GUGdvDNNFGW0QQhnpcGDtfSZoaQnFkMZ+KmKId5Ukk+iy
82xR5hH4z8QxIun8zhhx8qFl8TNJ6uZPRA4C/XXVYXx2EDxKCk6AMQJaPWnW+OKZC1QgTM3VuG4Y
2tdy6tXcxGGm1FEb49aPquEIjCwQzUEOxOfzzWCEGQ+SAG+v1tXTXgGkKObnRsGQY43dxLrQKOVv
u94PcXQ6XSV7znmAT1q71JdlAPGESAZpb/koO7oRZaD3GqFmc7l5fpjCKuaoZYcBB1X2pM6WU65A
1U8GpLavqP4fgVxqWXU8vdMQpLZcD3AvJfBLKJVjLe0cKfjM0JT04hW3uEIJ715uRpEsz53SCvXe
OJVq9Y2cFK86DTvJIk1zohHDI1pq59AgCEcBsKGlULiW4Fvyzt04ZE1u+Jl7pG33IG8fqgroEUx8
wWo7L19i4xc2PAPpnSGe6OppQExQKu9Is6uVp9Jep10TU05SQD0uog9QuIN7frv3j/xz9emdsNIB
RsNiNxL0m+KyfO/6F1GHslCpt993pxAYilCYRaYxS4pkZW98T7JcEFf7ladU0PtLVe+szMz6iZrp
s24riQWhbaS7O2iak/9Wwvu2pLD36ju6gHv6Po4CCb+kZ/ZKV/y3y3g+yhH1SXa8as8Z+TazRuAw
g7HUEZWdBWVS4R654uNJmVUQOF5T15TBKKOdL5oR1LXyX7TYhYRRQvJYwO4ptlYd/KSD704sl0Ev
OCiywR84MGkc1AFI8SIhp97OID2wp9jYyRs0u9bNeHGJZYhwYJIWYgnMm1/Ir/BXRoxeyyiYh6Wy
buMuxUq5xRiLGvgSB5ygFdoNc/OB4q2iaO/uSjgRoHLwOb8fe5tn9tPYbxPtU13chaUBjBTiLZ+H
NMZ4WdmhihTpDjOLGt0YPwSitz1ExHf0xjmgMs3hce+c1OuWA8Y0mTZdMZXaeZ7tOWeg4nkNMjZQ
Puo5hKEIkIx/ZhQjVqOiznLpEcYOKLt05g8jPz5LPfToJI6BV596mXBp5Kj2E3AoWcoWjlh1j9Cd
8KMDpMojTi1TDHuoh+e7tkU7DJ2ZvGVsdlUof/S0eZ6p7vy2YW5iWk73yZevlDqwZDDOEPr4S0jP
gmJZfZ6oyEEg5t1XuVucirnyKuzVMmuUE9QzL2zLvtz9rcM2lJSqPjIhstScRRKVJFy+JeOENOAH
4Qqqk8J35dEeSEMLKh0GNr4TwCCouN1VtiALWe7NBqbbS11c+H0P1Z2AusoNSY3DLTd1jIubaGNu
wMwjg0oAiNSFgEVPv4/F3/IH7aaDMtZWZK/zs/XEDEvNrJJmDBCRlcMr/UCx025Qd7WKgQrS4T4v
c0jqm6NU5AURm7sXwZ27oNv4OLK9ejEp2+pgLmH9/ifafPIXBvrcC1/HC5GF0RDT1neoUam/Naja
MbW8pon6NdjDj5FrT4tAASqksc0NZzlqJM8Q0luqKgHfSvXgZNoym95FNplXUOnsebS339fk4qNU
tQ3znbZI7CEzc1dRuR/NH+zsFxpiC7eElRIkrKnSkA/nYnum9TAeUtvqHLHJdzyKE/i4pcSGaQhK
jvQ4twGBPRs4EF0q2gydY5s7fk7xvE9LUsRCa70MkAT5CN3diNFl8poqyx/Ub7H5q9mG1HvUep0w
A9Ffx66Rg+6jcqZ0K162Ky/Fj2wa+pb0tIiL/71U+5NkZL1ywtoJuN4Oivxmsb53sV87yXl0R77I
14cYSd10GSGtQp8wIl6a6HlxerGtwxvumBV6S8PfiN7IXXwI9bMuwN8il8ZpwJnZngpL4vhkkirL
3aRp2PqA2+3Cj7RqRAt0ya7slrJU8zouNUDNHP+7YQSPRasYGuE5vYzDov86ttoeEZZ7yIdonIZZ
pvPJ+eve7z5jzpcd39MsTJ7nrh40asGzJgak+29f71ATrpRx3O0Jj5eNkSz4Gkx/NDDn61/U7gtY
jzIzVX/kbnZMd8U4HfsA4Ba+SqBquYu78zxLPEwrvlh0A7Y0YpzpC5LlnAZUQX1ZtxtIXluZBHof
ZrykIQxHkc+0Q7TgM1NXRHw74K0z2pFo8HRev20V/N8q8gPxEiIxo0v6ExmWjALhv4rVv0z68o/p
OSGZkuwZlOuahOEBxYN1KctzZQt83QzKZJWJCU2edJgewKEHNdAMPaspHxKQ+XAzbtUC1UBCv1d5
nLOrHx/QFSjHgWs1P5AIgks4vqogXe8Dk01BPNf75Ud24OFtdKE2exTrN5YZ6Wx05DcskbWdmaAw
KOlRKmtY9o/7C07mlQxxEWWiN0diXUMP5iYQemTayCIioj/4e+zUQe3Oo3rkG9fTwmRGbjC64P2a
6eQxlS11LAiemi5opYumz9E4/4ZPps7tVc9WkWWljAOgZQtgTtNEehVebr2uaJLM3VIApf2IEZlB
ZfQ2Z+18KARKe7hLkuXodJng8HB1jqYaQr31kUQXiDXiv1GmL8j+NQnkx1dbaPBb19/EjoGZyjv1
eeatqnaewaoNNCXhNZwvxFOTR9qKDJ1ZCFdD0gVkPwi1JTh5uZrWsRNOQ7P9qIVn8a9BT6wYcuPS
0Ic6THNHOZKoED3Kpk62KdXJjBL6m/8xo0PbWZ804lpl7LmdC8QOYZ0tyFwHzfW9aX5EB6AldlMk
NgAPxq8+bY/VwlmNEwlTbSmnkAUCGteyFBzHBgf1paZ/w2/mZMHR9oqxjWqrVPTfn3H3zO7lkZ6t
CpRSss2g9tSp8k57+6erFb4Dk62kfJ63ppek2wS4U8UiOGLVIbCvqj7VQRvv4WCN2omcG3EuzZdB
pPhikJWrR73JuCmVQsuFI9VegUdiA6vcFAfii8yY58PokU8gscaLtJzew2AafN3rPg0NDHpoB9x3
95suwMcPOMNcxalPnygRbQenKVbPlhIP19H8fTdYSLmF9VkOlEIL8ML3axhQUWCbomUmXM4oXKyS
kzuN1SD4JmaYLjNJETE7BSgV/1DjTskq67zr8dr58I78eWkn/3nn6YsPTMBsad+DGBIigz2z8oLX
SEc9hlYILg9meZieVrPVhrzJuRgsh2dLESbxNSW3NRcZzFQmvIhGkH/HkevjgpwIPyCsVpxcjYsp
vvsok1IXm/kD+CsR7eFhZZv6UeDnAG62JFH/oghPyBNbo4LxRngvV1lQn/Y4kOXvuVpe+sldRgAv
vS4ivz64c/HV+KaGzUfUyvDf/4JDzFyVsYxarIeaUNBZC5Ii4fHVwFnUsY9GdNm1WoQJefVekvVJ
5HzrOjtCgQ3Bo6YvMe43QA3fyNKnccYFY1nOIUrvtL4AUSPWUphVt8uniCHO3MwORW7nXXnNO2fv
YUkGwYSFfKW97o7lxEMG5jPFi1TXdODq1ybV36Oka7wuNZu3R8yw125dCuqBZcBO/4AsywrsW64u
zw/XTMryPrC3dQ7h/NHH4DXJoUVOEwz5ZwV58EFwhNWH2U5vgFcrSSTFTGVWSLo0YdamrWmi0/VU
Uhh7eUP7Hn/0wP2rg9OB+WDa4+TVi5LY/k29GuG1kZA/EIlK/a7SLjzD6QW9WfyXSJxD+QmiFa+u
a3hbcZLTTfBXcXuSo3gy1dNn4MEuLXx67lcQuWIgChIwvCtnOWKyHGcp9HUGd3N83qVaxo+s4KDe
U2lXS7hoxIkbYfjxRJgudgttJtmDbMin3KV3Hu0k04AGVRx3uCHIncULSUlctaxXP/yU4xRgcY9G
tZX4agzsJOIO3YOmVsRqs3blaWftbQaKIuNvDmywkUybvouPToP8UnONZMZ4gmSs6ndOK3LWutop
jPkhmUI4W/HkHuP6CEZe4CdUKJD6H5r0Nt6+eM1sRAM2EzDcDGK5RmH73g2s0pmoGBTA8qOYX83B
tfGSqWpXsachosPX0lUWOnDrzjhVB8HcdtE+/zt1aeSVvpIZCcKSIRc8yLOAiVsfUgpE3/j/mkU4
aL5c3HB8mLvOck+zNiaW3cXpwQ90gZf8NnVRAV8Cx76KQW1+3UR2QYZMZOrocoF02BTwPoc5sKHb
+TvbSZ3J/PVzeW5BFHaLC2kYjL3FUC4AatRQP8kFKhRHA1sLJ2cO6xv0RI5/GguEpd7Hty4bHVxJ
KdUEdZ/sWz1HswWJn12sYgrOQokF9vDc0arpO46Ze25yNJ4t51qVUCNj65ObD+5SvuanwiEO6Ayb
9x1O7bz2ajesfG63UwhENJXQUwYy9E2OlamjFSYF3bYVAwwjNCpRIPFZmCog9oAaM1DUz8+Bdt3S
km7HVbyYD/spV3fM3Vm13a4zH79kQrPyTI9zO1CKYBpyVpQMHY9WJKL6MFSFSMnYmt8sAKc4EIaU
dUJGgL3T4cXJF7WMr0NQZ1dPwcmRFVuoU1yeK3xlzvOhIf716qxc1F0RCzo/v5Qfn9m5NTxpvvk/
DL3Z/q/k+znwpp1OJIgMrdj3OgPMPgfOn4VLqhI8FCG/YM2JtfUDB+YiT8+Grt8+6GPtkSBSKU6o
CvuEh9uljFdZeeIue/DJTKtf1yJh0AlwpiqSkMFvmUd4vj0fs5f5Xc4x8y7OPf7WNGV+m2dxiQG+
VccxHIhzdsA6pxNAh5RJwSGqEeQ0md3dzF6VxT6oMvYwt6qKQnvsTnbbBUWrLc1AQt2O9DgJJqCS
B0pvZPIT9Gi30VBwYEa+MYx5ZmNZAvJctkXX03kP8xc1Jc1wdtT590aBFwslC4uz99c4V+UrgBca
v+LDfFZ1i4kS1Eg4P8jzzR7dDozMd/LYenPBCnQWSOCwf7//b7P1G7iVMYTwF2nO7EnALnvk/xJK
XoKPA/WUS3NXZnExCAnt6kvBfzVOUP7FH0vegPBqJzEGbslYnkaLEevk0u3liUDJD2tu6vshLWPu
n9ZMTWKKGB0iYbwg92rjanCaE64vVBdJc56EIyh+2+NZ4hASTT7i6lrBXRIeySLBImoTYn9ZdC60
TXasBrnXOhLXljzOlii9bfVhWrJl+BTzFFDLhpRHzwtpmHm07psAjcAHM+Ktgz8NeL+QFHJL57au
RWSh+IC7JiZIuNSZ22QklVKwz88Sp7SI4OzOChJcke7m0p92NiFBYJY4vqTMtlzphYXSFhyLDEv7
k19ppXXANeBuZqqleIsIxl3Kq3+SHPJQZEI5s5G0w8InON9TDQMGjX+jC/5nMdp4xWnzuOIZ7aLN
BQpUH9syaRP3MsHI03VYe1N8eeDc29oVmrjmN2G+0loN0hCke8stWoNqzEC9sS9bRfYKsnx9a2Mc
Srv3e+NtYogP1SzTG8dajWewBzSkGVMnyuHji/sQmJ8w2FENApqr2SjrnUqXwZ/ykoH0SzrwtIoJ
VHlmUdCwag7PCXzKxP6Kjead35HSz49BKcwBILcWfqy+rLboTG5Xi/eZotcfkmGeE9eEQLkztEt6
U0zhArcAr+5AkRiQp03SYOkTDVTGYtjOEtQ/MZ7YlmB2bhAe8/0sh9vkbKcebGMCvejrcxDzyrme
9wz1lSHvbwUueoh1CRlcy3M8fIhO42oia4SonXNkc85WwwGjhuS3I4aIsPoryXgQ1C3FD5s8ZCRN
xyIck75p5PEzPyK95f1K03j5QSKCnAXOdlrkBJEs8vnPauItqDtGrsBJ/W9FZuxlakv2CJnmwBbC
A0iwzTSYyq8U3a+JrpVr91k1SX7i6dop0aPydQ0Mt2yb9/6ttYpHf7ZZb8jFnAnQl72YyVVDyxDG
svdCZQHQFMRRRP3N5IN7AAicC7zQEBqx9mNQ80hPA+A+WaPinwrpTfwhbT22mIY8LTDu00Bstczq
bAvKw8eLvx8wuCO0w0zjbEq/uG4jwlIoANMkK/x0RVBMYn+AhINZOzpS9AhMUgZzHjDddELSIMHY
+Q7hJ7mh194gAMKdBY7UsrFni1TDxu1whfomuKkvi9ReUz0f+GjN6WtGEbIRutXjwXO9DcaezLZm
PGaeFVQGT97LHmaHdN/rI7ykO+xhVPiwdZTTe9gYFESzxGU02eEyGknUBCwr6Qrz/h4UfzzXRQDh
NqkjkRWWg1vH/3ZhfnSfBOqWWCwNbTGm/2Gn+nfdND2dFlYyzn13bfNKRJ2mBWlIZyEe9y+a9qYV
0clQe3PgplyqZRzHYMkXmuKELvUQvVyNichphAAw97JXUvZ+mG2mTiomd9z6T6d/rtdn+DLaouO6
0aUCpqCsc5FuwTB4PlLr4vqkwFztSdBf5p7DRfBVsZAldBF/gsjJeWbmiWyrNPRjiSkCt/Wh4MJB
iw2EIwaecP6ME3bGJUWsKlQnceLviOxB0/LJ3KS4VcxAof7AuZFC3ME6EO33WdZ3stcGf5D+vaKB
KWpiHUcpc1sUMf+1ugrHJQVzi4FtNMfdnlZ9OVCc6JCmU95GNhepAUtSncfnftJpDng6Af+hMWwy
FlsgKhePgxx7CJhSK9z2Y8DqL1UzeUfszFb46CklHF5Dv5RzQKUdTrjuKrhkWLc5D4WfATkgwz0E
wClBmi8TgFOVThr4zhvzK/gyzR726u3yWKj3TUW60s4ypUxviGU4GAJpf6gFKd5uC+bygv858AEh
EUNXj8cC9irBEgn9LCE0M9KLV29+AwnzNNt+/N0ootJsEK42Zk7HLTEzADoqIUDTGP5vVfaVj/Y8
SM1uuCCO/KnRMsYLiYlMyXH9h+RVgNN9XmuFU2VB5o1Hjz3NVLHQm/u0eKrWssRPTXeoeultOQMV
Jp+DjX60fbz8Msl3sEgIqgdI1PDw1NV9ZazzrOUrzA4vtIdZuAhtaweeUZ1hnNa2v2A03+u1JoJ+
kGVC3jN6E39EfeaUsmrIFRpyYXSqVSzPA9+/eEQ4MCV+GkEcYA6qwX41ZYztd/KWHzvLt+aQXlCb
ifvAsTaHLwx38h0mG51mws5ngMLqWXb96k6OmqUJ2d073oi5CkjF21xTZf7If6P1aBflGjIqDIRq
cZ6M40BzjJshgCQPIN3MUfCR2NsxzX3hNGYjlkj3iL62NoZz8teSK3t3Dxx0/oDHsdnXZFNB+ADl
hwNlDcUDZaFM67ZcrDRwceh58x1F9PK5J3XNxN4w+ybe2qWfqYeb6ephJAOwl6cE9Xud0iIrNpag
s94OUkW3jEPsfu9YtUIqHJiNU4ryj0AkqWF7NwAa80Y6Jw1tB7m+S0apIVAGrMC2pId4rYKiLPIA
vVqa0XD3ttr0TjvLfXoApy0h22scdQ0UIpdAnCIddtAUpiOWaV7yu8LAAJrb8L4boXSh7/aHPEBm
+hZyuqmFWKSymNQ/EPaJ2fhSuB+LMwwmi912s+lINfvJ551KOQYt6z9nAsCmzhQyVXs35AQkCtCm
s/s7X/qPJZwMdjWY+c/R9GYSJU0xTeEjy3SiD8JiKDe+7e+XBLVOakxq2XTGQTDy0UAJNqsiJ3f2
J+1zowSpl8Y4Mso+HxwjIktIsL2q8MF3do+mwgwzGbOjkixXSLuGsXPdAF2T1avfGGYK5874R6xa
0s9xlEpD/4zfbdSfdSYDiSJX5Muxc/FV7vmwtd5wtdlDpKwvIzUurEyR49iDxHs0x6V4bRkfc+1F
DiiISA1auKhLpKruosdbagKdo6U1sab9N0yaU47S4eQwX3LXzvyiyANlN+678PgO9o6RjhuEKz4z
pn/4ceoHArSzfjM66aqG5WtZAoRZYMkGSdZY1p/Lq/s1gyNuTIjzT6cZ35LCXNbnafcKoE2DS/ok
gFctFoAmASpjDZQQ7wmPhH9hk7u1Su5ClXu+QwAeL0Mmyb3+7zxVCBuHrlDZvA5030IbOgZHJm5s
smAS11lJSIBbFrWVWTprzp9bxXbx4TIgDN8Mx0YvaN8jVO0uskNeHEFH2uHm6BMiVK/AsbqLSgiY
VPvYa2/ssCUDvQNnc3hZHW0EpVGjz6dl7mRnYIT9xxQT6Pnm1VhrdZihoyaG0ZpK8e/5we0FTQ07
H+H5oE41BhPmwQGPeTaYi4moxu72W0rGGKqE4AZPDPbvU9O+8Hr45+xoB/sbN1WmWklDAe1FdBxT
utWbkNCo5zzdTKt6P9sYsrAemOvjfr/+Yg7br2nYN4mGZkVzfeO9PVS5xhA9pnx5/QSXbrgsujGQ
GFdmst7Hm3JiHubiW3enmm6RdGwEl6ZQT1L4X+npZcr6faw7drGPLwjrzmw2tSe5zdTKOU3BM/Zx
e/C4huKpu6MgKHKNJJRwCTl9ti+FaN1BvC+F7g9uP68odO4hoH94zBWx6eOkOan2ixQ1zXxJJnp1
iJTJwTxsL+E0Ntr1MiZBwSm7wWC+Az41PawrPmneKN2jPpIpW/52/rDv0U5K0cxgpZimNj/pRWE5
XJv6C1LQwyuZGFvTduV5Pexw6fmyCQDn3onFskt4888DTj/TZOjo4K8JpBhCJwtGCIsS1oN/8wIJ
l7Z0sbfBlIqv1iBqcSb9Kv2qqVmSAyECpGIcqwWKvQAMz2RB0JoP7+v7otLZ3WuQwt+LUW0YqOjU
3isCzorQ6jAePEmq5Sl/V1q/paCs1Jxif7OaLVpJUOO9kMJCNcxFYZ0/v4migN6tell5jRycOfjK
Ur8tHPeO8r1q1vciJjdaibFNMe6LDGQIRSvfOwfHDpHMJvz7gddFkhb8dv1LqMKwOVBzH3OMvAso
i+lLDRFFZA1M5sIHdoy/duBXVdd4OsUx5bn15palWv1ZOQCNjh0T7pcx44W7cemQKdvUUwApTClA
ZWTp/TusGfobAwAVytniGOGgTzBx0brLiOocamQjFyS+Owi/dfF0HVMw+ODi6yTtthQDaSN2DcyN
2nwy5f9qCJrRExiJqgiaYoYr5x08CdAOWdqarJW9s9mi1ReG1vvSIBObnwoODttjS/6x5uhVbtqn
8sJb9Ep6x1jn2dgPH0qypdZSzHhpAdrY4NvAUczHDS8Nj9eZSL5gO+l2srZStYfnkQsSu7ZNFPbS
FyFopsTgRiGBrEqJPMl7WjqvDKc5JbUOOwQcnPuhCUcAL6OF2wQUhLK5+IinPNtShlP8UazAeZtH
0sAJYDRGkLAcpWFeIF9FNAAQmarF4eKT510TF0gxTua+40TK4l/wzI6EGbfEkX89wWyyd7kk/l1S
wlHqBZz6pyBJT+jV+qDJVyhGZLF9dPF8HpBrX2ovb9CBN2ckhW2sJ7Rofp3kDBXiq9oGS8dEQ1ss
t8/w41/zkMlCyxxtulo/nzGiPmOyX3GgGQdl2BOOwR+Y67QG6HlkG0PPYYPHyISge9GJg8GeyJIF
tPKEhhwjmxgxmGdk0ApMeRObedFKrcqkUfx2CWfuXADk4kPnDD+/lCaQ6ZQ6kTRljcaHOQ5f8+Wv
A9g9Cwca0U4MC8Dcbtfjgv/jqrxLBmd+iwUEt40mYWFUNfJLSHhFKesRCtvvSd1JDlHwOOSFDMz/
WCqrArF8L0r9zdqQ+DXjxX1MAb0RqRwLq5K0Fb3q77/asB6v7hGrVQSLGKLr4ffkl1MPvy3Zwgrp
RK0g+uloc72PjVzJeQCFDllPYBeYwQp4ovxE7Q/AybbMeD0cx8Gt1gWeB7wbywy2flN2o+0G6/mv
VPBSIT9wOUrgBc6r8/r9MvW0QOII2u7dRTNfpqZDK44+OsaRF0qzhZc82j/CUSfJvqd8NTtXyNec
w4GziE3f46ecAvuVtVwsg/tig8jlZQtPVYtjmKF8fyYZAvZqJY3yFkvWd8iWp7nc7pv6GIe7BJfs
NZRFhvNHjKR/O5pW8TRw1lbmmLvtwtXHwZIQeMP6/XBrtv575sK6RnWBTt2cIe+PYx0LHz4a00hT
tFkqOtGeew2+pJb9L+5uJQh5FZFsBTR6xBniCRg5pLIpAMJiTckobtQrwf4oIqJ/watLZHRHgiAs
Kl8lLxCaWrzow/BV71YQdBb+5LvPKI9gen87+Uyjc4q2EOH/OTO4Qriqi4y9p9tBXMwYqF3OgzxG
gHRFAFolgPrtbCRbSlayT7QPztrApQexVUMnTZCnio0mtFrCm6vszriFMmveruIMJCbOLkl0HemG
0BcbfpEyYeytWOw7erpfqWAqu8n8s0rnGGTIrk15p0HIZVnabVvVJom8k0ABtJ1eddpNfLlk3bRw
XqOKIR2ji9EkTU15AnHraVF3ZR4WXS0H1BfYblxywOeEGnmTZIFOSMkKF8G9we9PhSOA55FQnYkN
rblVXp1uACyL3vfYf2EW6F6KQ+FCtRmZJyEoP0gcq8Lp47foyTk9Ydv+7CLvesArrYwCZM9tvn/G
YK4wGD8A7siS3IUiqIpiXPV4Lgte5C+GatNIbVUQ4r0YD1cEtjbb1Kg0nm4zJUjm9p8KpfGw61F0
dDXW65bQJo+mxfwb0krUKvpH1Yb9mNCiadg0tAv4EH6dhvfDjBN2yl9GRUbsgW5Ew9XBN0WesVmt
Nu1ltiHEGefSyV3tpm9lmNeFvw0Wx8O/GfTcY5ivKpL8N7ywC0wFxKsTGNKRz3X/2grOVUQxMwL6
yKBomZLEPTBTChkU+4FaUdhV+Xc/I7fxl/ZhHFJ22C0c5H2EkqHSf0NN20e5Lvz/+CX3VtpBYQdM
YUK4mFDiG9LEUnMgKg4gonf2DENZP809O5FGKRcYbo3d1D1vmQzhSCje/NCzquL3vP5XHp5QGY7X
3prFhsrSTdB6Mx/0y7lnEiTPf03wT+lUtu7gA67eIYsL1nPppIA2cqC7iyvD04DB795hpUxWZbqK
U6zymFMeZTqam/BpxGclA0xkx4fgQBy4ckG+9NJdERkB/O/lvi6YgMSnjLhQwPcQfEbriekCH5QZ
P+w/tBWue4CDw3eWGOUh04AlaPZg08fJdTygC2MYGI+7XlO41VTBoFt2DN0eUd+ehTJYQ6UI09M0
Hzah71WbDL/jl9tPebGT3xnae8816nDjyU0gOSQPK8CccU6ytreSqkF6a/NZIcW1aLMKXM9q7POm
Qo1gQdU/nRXGZJIuCrUTFqZQ9ERkQiJosM2tlr9QX5ct5IRhiSC8zGXUY2MTviBW9tA8eXwgKLTr
KJVLhpuaBd/Jazmp1KtrboKkUdXPvyMy3Nt3MzAghrdP4xqhZQrLVNe7EVVa3In8Pm7iet7VwZn0
K7tOlMpNpdhkOKx2zARMcbZuJhZTHyYrS/IPXbCYUWTO7ec/V2nj6MdTGz9W2LtCbXK3tkwLw25o
r4haW4LGq8yWU3dPmuW+5tWdTpjoQKo5ngbkmL+PpIWJjezMmQ8M6gaCqJ/A9h0erhmJsMW2Atzt
lDvlzCC12eTAG5S1bvbYHJGdhy9AMIQNtC7WHfYf9O1jx9Mfk6HumB91AcDFgq+rl5wZwvDyumgF
R03bNP3cBf5xjrqUu1NIbAdfexfskuhNVHea+axw3B6FbUqzBCCJjNM1/qW3vPbUWOxuNvw5zh2X
3x991hlTbBB5wu1IqJVKg4IyfcRTamKKbhnm2epRCY7ycyxiunE/HbKVqebdk0/pEOgUjDKbuPGH
fcEqYa1NJNy1KvVyeq+qQvnL2V0IbF6QG0KooRKGkWSEFnWdWYHpJvmkLQI7nC5Z9VjC514DlWus
9MzW5uNI5mQst3VdCe6fMMU+obLhwAD/tC0Q/UxVYJF7BnAT7JwjyLq4snE/m28rCN1pFikt9M1Z
dPiVryfh68Wq2/sx2YxOl3qtzuDQ7dVB+FIFfRZa1jR8cFoK79RW9qyvCuKbpYP9d0fazFn6mztb
JNHuuZSlvhJGXwQFzKDgJ7bXaL7AXoaWZaCpBKm9V1xNZljGjk8QoUmaEL/FifFDdTMRNJIJweFp
LjdPDfcKWX13DAQcjy04/21GHpcPZZYyWoRFBQLcKqSp6sgZiSpQ0AdaMT4JqhYKEYiI43eMGtVI
xGcKAqt+kRyhdC/X9iTc0cyriAiu3Dg2C1ys+q8+Z3eWfPdnqEQdiuPhW64uv2UbT908yd4tZvuC
9HkQYYUPj5jmBLFQbL/S8QLFyhZDE9Mag4cYrtl/cwG5XYv4FKgvHUFRs4YnIAZmb/KmV1Vcm3rl
XrVUmiq6kAvkrqwyx+pj3WwyXEuSQUZruCRJpkvYPfdcbCUWI+DXlL+qPpYxkn0mZMvsxMQt086N
22BFonioV313W/VhMzbPYbcRJPGDh+FufoC3ZoP4hH0zWd5/kxsce8OZv6lIQ34Ls1GWaTwUBCy7
GADt0sfALwW52IhIzhYSbCeHDA9herWJ2EWWgnQJx5YlNtp8NA6vJTjzvB1TBpWY2L7k8SoGMJV2
J6SoNUpMx/1gHj+aXi9F8uGIaqTRgo5JnIXlfr1MMeTvZtd44J2tXeIGce1tD56oGWGAnVbBL5X5
iTSdQw6J4+qHbMiXq18959htgQzDkDKktLm+ctp/kVf+E9vw7w0cmYn6KG1dsY15PyOdCBHSwzfv
3Y5iax7TR2XR4Bx/AT518wqpWVS8T5+bJG3b/pKIkWQhZDkS3PxGDoujR6dvnTFFHBptipGVp9Zj
teN8rPBWqJdW7jfgk456zXHEseIeiiRWOzrCyJSm9Xp/mAokE12cH9NQim0zNDgrYev9FAJe6VQi
hBhr2MlSrCnp68P+18HQeEULPsBblk4jesxGWDj9kfM/Zam1GrY7YwNgXy073zQvhKhVu76Nf1h1
/R/4KSextC1tivd3BKB1af1K66sIYuraIWk7rPSpaR8u6BXiNPfcBUTHFySVZOxahOYJX2P/cyK/
vjjUUhZqOJoToFHBQdK++jjl9MLp34pclpNsrFhVR0Ozae8oWc9e/k2jC+QGUPgRC7+cwBVWj0S/
1LVpESBvkEsk2q85ayJlAwPokP3ZqBTJVsljm85Mqytvn6GAUYbGAB2vbRHEQa9xNm4cXg+0Kb+j
KW4GC0Mt56G/bjGu2CLPHGoQUFoeNn/pNTxYb7nYSrpOgXcQHHCNCwH8oXW1n7uOhzGNwA2Yj1H8
Ry4vRfaCnAiF2Fbm/4UnqCL+bRqAYSX8m1ioc6do62HQP5htZSk10/YUcdCz+MwVy732zIwBrx3k
w1dA+fLt/zsvI8klb1ywamVSksflJ57OTE4yYMFS+4E8nrcCks4+sZZq1JWZKGAF4CdoBZNCFCPF
zgvekxdBuZlWzMRgestRMHZtgteRL371m84YAIryg1l+xYq/c9BJmCAPVSWnKoCeLKC/fa3qwC+r
Q0EOyG2/UP/K0nJR/Jf+rECD7JnBdFCOoS8lAepdxaLXSC3kfL7j/nyPRkXaIVxxvwFk0W0l+rKq
kwk1YrnHRt6FhvCBdVyeqLQ0SxCJFERlwa9A0b6RkXwY1WvLIZQckNovS40+VGtZh1l7CzRUSMsv
WZdfKEhiPuDQKCX2I8TyEbb3Jp4triCgJDexN+H5sYYxvE6OGj0HmfS3rt8h6rO1BA5GfpdMOyyx
ZhwqnkgUiWjKAOhN0pnVRlX4no/EXdBbwcsnCEA+bxlNw6IoSOfpo1RWxa+EF4x3VXYLScEEdu1w
Ygf40xU9eEXIQeR6cgBBQLXYigoQkJZERmY0A8+Zlg54MzxlgQcqVuRZA3lH5Si3F28JQH8Oi43A
nFl6kGc2zgQ8wvWtefyJj4ofWWX/UQW3yDr6KIrxfuzSQM54cGDNPP5SiSlTF4l70w8AI9Q2qNnt
QRQed6liHGGj/9//z8l1alJ1xvxJ4InS5JDqglOVyfphuBOolFU6b92jUnbkdKbCabUlDIvqo/BW
AtihSU7pNCK+SE6QsUkyqXz90zgwck7tc1ysrtYNxxuu9n5xRVUBvw7NSWdvhoxwvYSeyRzNX3uF
q51z8T4n1KXfvZiSFJ92Ghsx9Ai2hcXAfD0VZakNs+s/F6Wk87ilrDAUEzBSgsi1xTXXBzPUhl2I
lPsiUx4Yzb76O+fpzs6+h2LtJRU17GYYsPhq3xFS8yf/Ri8o8ReRWeUaqkj3uvKBPK4ANvEJ5U2y
lTVtIB/yXoC7/rqCjtIrygRLfYFilrCFx2GFOLXjC95gqn0dDEtZXcmUBTjKOcBr8W0WS24muxSx
LeZ+aRsgU/e3Ccje8VpU/dUEOjJ+rxJ1EJQONDeYL4oJVxQfJOdwvI49llH28/uPR8cPsxI8u12/
XcO32YKsj/5Bn203KbSh4rjaNfFJ1gJZOMPwk+dZDbqgi9JRpo/YHn//NRTqZK7UvgMBxWFmD4YL
9LpQ0QHuwqo6YB/bTuVyRzMIp/JfM/QS8lBA9iTwWi4ucg6NwsdMmGYTEWZ80SuysirsKrQy7sgU
28XHDzrVI5GKHyI5l8CF6RPHBDbH/z59zVJNiv1rLRS1PRrmFIqbGb+VnZUZNc4/73nY9qCfQcDX
Bz4IQo8PQSx1bEnGI8c08n1UbUmn7U/Sr8XO94WxEv3S+Pvy6tMXhNPHCfB+p/fTciyHbfF5bIuA
v/OK4hbUemhg9AD4mytzsGbYpeHoy6rFkrBu+dJ1SWab7R2uTALyOFr0T0dsSHSYlPgpY5nieClo
5rc2Qdb7T9TDidzcWkbagoOtFv/SKyZw2xdiaz1YUKEx5ZO1rs6xqOx51sB7rzE7Kq9iggQMzCyN
yOw4epXNpRDWfNIqvo4bz9JWO6BPZ8SpQOjg/7nIfT2pkq6tI7nJKLaFaVQe3s1WfH3CQu+xS5he
7JK04kfTN2oxCOivzNbmwDTUPxfcESSliVG7RIc7z3I/EDrhu0ZRCM1WBZ9cBDVozumXQlWzujzd
4g1ggY+AngJr0rzJK6jdQesh/XtwFZra970EYNjGnyesZxEmnMK4x2LM86cnNvi9120HrEPgPy5C
+L9mLczA7LM1sYdTOeL2GfgMkXwgOwORLlQQuCDcmtFG27sdpwQ7jUXJY/E5QYqSbnVY4gVQJbKS
hH3dScvf2xNyuATvtYNoyyqZJ4behu2xWJPCqcveHk8clLpOSg+CGIKCXimaDLLkfA54IdQ64/Zo
DNqB2nb+9qlwOXoDchsqh5Q1OZeFHkpnsn+BR6L5llLcAddDwE9gFmUouCqY350j1xN4uO+sgMYg
xS9EDhzyHGN/IT2ltpK5BAEKFUZi5oYwenm5mu3fitH1a423gcWFjAIgW6Rm6hm+XsnWBrrgglXx
x4x7nOdEeWQ1LTh/Zx/FJu9PoxvoVUxaJMgpadUfXNQo+sYABFYNh/arnqHNVExW0NE7+NmK5qMp
NO52GDySuCCH6GNSYWibu7Poej8P9mzKp4emqC00BhyUc4OSjMaq/PgBZxTNKjM6iWcDCcSXjXQf
e13IuvFTQSosm/712bANh/NdSTxNWQq22DM/dXYuFubeVUYKieFtrWVQyD0vNUyo7a7zPM4b8cyQ
LV198mnlgbUe2VnJlDKQq0TVUt8z18x5wTg4Wmmk6XVLyia4vUR8Pd8hNymp6X8lFVBPDjcBcUtm
iKqA5VRpjpbmRVWGdjjgmcNqErWxA+PNovhhFd3MBHMA5zlVN8Es4zcvw+L6gbm2GHi7Gc5fulaI
jy7GV9+ZCV9DRig924MNYtOzHNGGNqEkBdlijyBm4YYcktNZPVLFj1QI+mrfZMsjE5Ii2c7qA46p
HJTrTUSwiNUmPO+fxHzcdFx4rUqebBYlHCQh7171G2dUjP0aemtzmpw7p+ixaJS/PnTAOltrfblT
H1knfuH61O0pitwT7qe3NgbUBviwDpNKL7GN20Rvu55LA8XsIAT7zEvzmrljLdwt3hY9atal4e0Q
NF4JRutSyAx1OsBKP2SFZLnMw80dfFzNFMxrDxwvnU3fjqE2MyV+3lEyn7SJw3DTJ8KHlfMxnii+
mmmVR0FLUsiQu4czId5n1VxhFTdWyCkntrSsYGi6ueXrIp2WBCaGLNmLIe9WzzJaFUVM9cTwmcK7
ShPti59ra0R8ec7qXO61kgSeT2K+xiwSpa/FMtl+UFg3n4cKv7j7dDiNesJeowY1xnr5eGJeT/N4
mY/z+EfnaTncXwL73++qWuqjmscExt6OHu17FR8REZFFKX8ZlAPZtETqADGZpRz9ym9aeMuaYRiJ
17uEkDWJSnTVpLJeYOSdgMlYR3ePjHac6gs5TrOzW/bjyNAP3p9ryKx8PkVpoNM/yiRfm8YOIEDo
uUJN1gIRy17xc2P7MaCYZJoPHT8g08NHr96F1stBl+pv9h9T+ncvTXtPjvFdX/fBl3ItZcbYVH1z
sepD2hFzVZO5kla6CPtJnoSMC1jCRtoLzDgdNliqMi/pF6jOt6UDq3ebntqlf+kbpNiVnNvbibs7
1saBNvI1USxAjTJYC2evbrl4gBMd83LLn/kEjVMYPTIY6sB8oRFuk3eVaOUQB5RbyIRxOsZeW5Rg
O4414YiRNMgb7t6pTbI4Dwz2MC4yQDYsTSWuqH2klhZ7kd3T/lFLZoVEQWeLYmOpRAOywo3vBZiu
DcoZkxeDcgl+MfPRo4BzQYpN8lEdoo7UbAQgcVqm0vds7qJrC/O7FoekHBzJ3C6c0REfVb1sUXi9
gQQb0Sf+LQk3pabGeFYk2SiEsHAowcjO+glxtb8hOJvYRGmyFQbxipTisgdNHnIGlvqkS4scP6Sj
pktWaLdMvBWJnWX4qhv3FN183GWYsu87QprPhaHhW1Sjk6ES910yKZhvg7WjHSn3LugUkkRF6Tr4
4uwKuuXgTup98SC5fI9KWrE7RWfgTGJYD2e9ookWV4MHe4smxrzY9VHb87VcxaeakSfO+Ipkoh+3
0hl8L8whc+BNb/J99v1F2r1zOOWCCITL7KrAaUaS4lmQrbfa3plpRMJn20x/rUL5rV3sR/NR/fAh
hejH/IiEzZPvdqMIAIn9TSJY+dVTo62RyEyKuHm7KA5EFHxVselfGz4BR0iuzCfCiS9h5qMs5IYK
i1HHz6wyDxvrXr3jhIm5WQuYky+OkDy1/yTRZjErUeuZdHGyV8Ud0DWCzVEI3BhygVBsIfhAMKXg
p4r0mIU88eHNyRGiDilzcUJuVAIBlDtbsC1L3mOPDTaAdYkZMpRBkJN+59XJSz3sDNwArpYp6nkG
LrO/xj9yr9XmxCOpwA+fkh8WU22okLAXcMYpFP9N4zeYKkjA53dnvUoqcMhizJMnJpAEWKCkpCSX
TLCgITBDnpQdszTpfNhn8BXnEhZZ0bYMGSqt1dyUekP2ClJ5thNKsDHsdF1eaw5pCmATpT6GL49P
2a+ybJjl8mnaJX/arCPCdD8H43uZw4KHKndO/JrErogWv52CcesL9RINCNhvVT/57+/vgMH/BJmB
MrRF6DH2EYc0mZ9fkvKWRSkZCT/XFEB/GXHp3A+LvcVVf+Ci2FtQZr3NbuBZWCX5jUI9Gab2HXjU
F9k/52cy7TyF93BVBOdDDb5x3gGFtX7NY3uQnPiLvS3HMuuH15g5ZW34TK6NnEGqY6FvMkQpDxAj
nwZX/uviGyeaGCzz2PWtoskz/JkyNceRlkab0bS6J//CsR+fKi/s77k0UU87b2+3h194maHAZXeT
ItNZffKKbqI+lqOm/aRkPZABi/uhsVMKAvpr+Bgm2m9+9wkPV+Vb0bRkJgj3oZMGPuYVh2fi4v2W
eO5dzJnSYIAcXBWO21cxGzE4uj/828Xyu9C3LXTfrcWdCGdE7pf56Ke96+0DyFp6urS0oT39K68n
351YIkgAGLB6CB+633AKF4tp6SicFdr1SvJZMpo4Rcbba80I7U/btsS6VDKSZAM+6NgU458SBjQG
/7wR9zaZXoZyfSrlbqZ5iK2vQxB+IdEtKCV8TfND1ToRb4dSiFpchjbPwWVhvoTi2iSv4qRxmLLX
CCcLv4ounMjI8z5KvS0gEPYoW8TWWCJN2v26CyP/9fn+H/ZxDchz4HiPkQw061vVdwweWl+e12ht
WWTu01S2/CAfoAcNmea9HJYGP+XY9iZA6kgk1x/LnOJ28ZgF1Dmn8J1BU4AnHDJdGae6JTf6Zsp8
/4stZbbsbg1PS7Du0OItrpeHBuq0V231+oXRh+R9CaniFeXwj4/pArUZ4BRqzoOA6/EGlY6W8yoX
8TP6pBfUclaV7HfNdkHM+k57lGKW8NzPhaguM2nznLMIUv47f0OPM+ma7uh3MFrkThnt38YRxVfB
jYWw4o9XCJv+17mDXWmKkKhRVpkKqvvxU/9OW+lSA1jdpMFcW0ueD+ZmxUI8IGbtkjUVJld/nRhz
0TbwS9HqtqKirLlUV6QNwaxdaziiLZbuayUzZW8LuVsmtzY19qN5YJAfdfE4nVu+ywvj1ul+mRf+
tKacpZKgN9Yf2D4zjRROmIyER70jRNTAtlj1WBshW+/gI1szgolW4QbmyEDUw2oPZFuZ6fFcVU1F
lXjRqHCJlizyVxznbZseJjxSuzU/v7pE5N9hneThRfae40nXJq5rvLcQG/up3YGXNi/CNHHOIDbF
QkDtMToGsEs+DDTlydKNXjBBcjcN6zC9Vt60ekhpSwOiH4/nKEEfOTbCc89De9ZTNNGAsv1Dv+iK
//mHvTS0go3insnkY79qs14+QMZunAfr1T2KMlbSnbJFSQGG5FvdTkHFbNFrQ0/EP+AoGGGutPd7
9eIf7kPUrmVjGU0X2XEIe6DycHyd2XQe9PMb4PygwxlVIMgpJjE0mpLmk6DCAxeT+aShybc8M32n
wEskK9Wf+/LyzznuvmQXPvBbONgPNmTm6T3+S07NxkzqjPieDzsCPrGnPHFM0UNTZc4Usp/YoMry
yaOtY3pjn+tNkO6nKNByd8DJlz5XBJM49nDRlapRFVGkGsXrzjNUh52jjv1iejeABwHrlcaKFo+w
USgyPS2LQAnTIyOxZ+KC714FjU2/9zmeg20kuJ62Vea8EFe5jaP6tdpM3QeApETIS6pAv7gkBRDo
1PyAdtBFBgd2C3Fb2KZMrTcQzjQAjMnK69aUtycbQkCSBT4jlOSigCkrkU0btlr5XPQgEeub6u2u
Vcf124uJ41AsPHNjg7tbC8xhA3VTmBAY4TXSn96byFi6QIEDkHLiSmTrdvUWSMTh+Fd9qX638Eon
Xm0VT92lfn1azvt2Ecd1Npcqx1xbiFD+zWYiZaggU5Mf4MMqG7msdA6bUS5my1yvgTC6V+75S1Oc
ACTzJ1PxpI1K/cj/RLef5ZUzvY5X//4cvisgPr/ZG5IRwPV7Ybf8Laya3ax7ugRrCtedH3Nj3Fr2
R8Wm5X5OUeDN3Ud5r1auVBi66hs+T8VvefePwFIo5cf4e0ebhFT4oalCuVh6uA14bo+/dIerMLdN
9EalMhWkDCgX4bWQTQPOMlljaUI6d7dUYNydq554c99fa/Z5XYrSInrVlgk9FDy3A//QpFbXN9SH
MyhtkNtW9ngOtsoLfR4Pqp1bU3+gmLCeBs3A2s6Sf0Xuj0soezWANvKB6tD/Y5OOK7VVD+TgLkFc
8U7Flr/+Lhf0qIY9QEXJ/nRpHk+8IjwpEp1AyMuqoxnPxKKknGTAxqKwsM+JgZ+tApnUFmS5S7t9
1kFrQzbSLMQep7VqKYYFFUo311AX5ngEh6L2KENJWEzNQui0ziSYyIfewf+qEMCRCcDKOMdAabhl
LAKE6Fn3AgC/vtIgdDCHynFGFG10tC22zgcwK+22CjRE/hDw67g5IEH4TUfH7kWP+xr5+nJhunIV
rg3KX2vrGmkPCuby8QtqQ6zqbNsGClBR/STyzbYm8EOkE9e1gM8/87seol/GRo+1lrdvfW0pwIe7
2LKuGuy6cUN6dVWIQ+IOP4p205j/adkCAjCuXalbGy1pp2P1Lh18M6UIPjvvCdHjDA4ZY0+SruT2
BipzrlBNGw3EH5g3hXUkIF7NnRB7USu5WBq+5iZgVOIvhxvLnGaN+1FTjQdKkU+IHolhlud8LQVg
YlTiKLE90pBzS89X5HVsVNAQFr4S8RUm2tnjQXazASvA/TqhTnCJT/SDMZvcG12idXeQ2xWGi6b+
P6kewsWQYWWYT8pzxd/TwFYYIpP1C0/MSe0LRXo8hNOUNnz6echbzliT4Z2uCG/rK6bKOS9UI3oh
Xad1P3HOUniuqhdvT4p+jrWsQhnXXIpZILh/ZLRurfgL3ONDUApxr6NnSUU4IOXscRbTZWH9NZrN
7QZ8L3JOEobEQMEXCkAlSPRMU9txIl02p1y2yzaHgxFwjWUisB3I4RE8blE7bQYOOseamD8ormp7
WImDr53+lPxfdlcps6O9Q0341hAEablcq2jBMEL1US63W8qKS35QWvLj4xzWEwoLdRYnBwUElAH8
bUK4SZI/vImAVDP4LFR7jUiil7Y3tcKIceAkALezW5KyTLk5Ydid27MbpYDDmNdVL5EeRhfTzKsw
meS/s9AveJ0bhfku0Lh60mhgeX8F1rQLAu3dQf8N1iizsVIbKrnYFcUt8cZMrmuD24pRG5zztRFj
eEUnxp7vj+oHuZM7UqGXKoIsaOtwhjM1AhoWKpQeWGjgufbEL9z3kdWBmBpxuwnx3R4lGsUIWKpo
GqRKiglFh49c7/wRI1xBt5aDXYovQciErnSb5LKxGaFd4lzX6h6aVPVsZNLgz4gH0JYKsD7w3e7I
0ImoCEa5RcG02MLYm3HBJLku6zTbZze+csdixZGUYFjI5Xxv2QUwEQgn4FIDtL/ghAva4fdeoEmW
Jb/BOPUFISpJ4Mv/DgKaT7MUwvQ4naZODnqsp8L5rJACU9OzkEzXFujhWIoQp6t1snsHf7btgkp7
O916PmN7Mx8BtaV+7D9gAiLoR9hhH1ZAFSZ6KpoeQ9uy50OB2ty00PbQWkjgNGUnLmpFJ0GxEeNE
55kRaB0eKEMR0c7LLUamnMx0IjblaE1zp2SDURd/N+kTFM5r+J/Ah07TrYUi5Q8HswqOGrjoiBMt
EWDVwSAJitSJliwlSRGbOERoGs5MpJOBPEOvOwiemOHUsgc11k19ph04nBKKMOSZm88VrfpEmMPw
0S4OwoPy5Vw7WlsIe04CXengnhp7DBqYUE9qcsDcAeHpi9y17wAssvrZ7b+Fb6Nffo9v2ACf6SDU
Zo0PHpG4SX0Li1To4umQmRF/ynpt4BdPZ/5kVTEkvPuEuOAmjufBRtgrdhP/VVmRLTWHZXFxhvzY
bpTCReWyPVlE+k9cISGnYH6hEbMbO/pBuPqTAytOlfy0E4OtvSoi4UPO6s4p9F1gKwanPzejzxYX
1BuvwKHTGxNZPiTVb0zcKdtRRyo+x0Kex32vrHUO2mdPIlqHYYAzkRGAr81rj+VnA5g0N2qwpsYR
nV5XR+M4yXEPnVMf9X+yCOUoYtrQdc6oxQXlvJM/zt0N5SLlBdLnZFPelZAnJ6Sc2hluPO05CkzR
drPnGkl5BQ+ChzlZBWtvXgZ8kCfwHh+eC5rlHezzMzL2vAWTkyhiUR9GtxMkZlmF+ies7LONhviS
NsynyEITNvxndOL4Fxvva2AN4yKo3u9CF8cuGJCCna4cbl7NEpSqIGPy6JWmM7Dpqmz0jGCK4kHk
BdHtk6TKRuC7UujaVAmqo0SYDbsvutnrQMegGYT6t6aI4R3ALafBOoMatUr+kJoA8pMiiFSQmlqK
DmkAl6+ZkJ2D2GQCC7CMhazEPREjleTNBLvR3H8hOyuTOwedCdZG14fT4JM8uZRTnZaKSW8oo3t5
SKgc3CxSQ6LokrjJVyXTjXfRr1pdnbM5g+HHtxy08jP9ZRqmCHt2XmhY52z3tPed6cGQeGof0zzD
wxLS9saGyXEZqifY4AAu/7RAqSI7AJ71aRt0nO98I8mtXu7fp2DL4C+rqMOXMv2MYzduVe4hD8y9
Oj0J82sONGjkndT1fOuXlrgyYhrzZuZOjsVYgFkNPE+yeeBhFdevbvYk7eQIFtg7U3exnkyu+GRa
34KaTPG/avbOIbafyKj4FMHTIypJUdzbf+uQq0R/003U65z+xmiBiu0khN7dJQOPZGvlSNLWZSWQ
v21uI7XqRatF/0DsufN5OR/8qFHNT9PvOFRT8/bXPl8xYZ05fCSOabiPMK3JGHbwyiRAYYIFkjLe
EN6OJnnHzFWY+gzk8yatC9InxMqn1jTQXzwKzPtefTvGyg40dHVRtT8kLzUTHuhZm16tWfM88s0L
5rYw/Q+P9JAvJm7sI/sHg/3yQ4mZaCFEXeFmWGdOO0jhVFjXNyskP1tt36gHyXETmFN19i4aoBWs
JWhJNsQgSrCTDyPNvliijmBO9SJGHIGhHa66El7sPJ6aH6HZ06lDKS5y99yx8fT3Evsbxvj05Jcj
glbTQVzSUojC4wAKtkxgTpFpt0vJF+Kco4dLLCW9+IsvbDo+jSuJ1zTQp5VcBSdksZISPS/q5yWb
gjwpO8eVf1RS9TerOI5y5+Th/pbft+A3c3vy9Nc1twxyYzCEJj2tPYlotRrzuo8EhRVcgJX9tuPH
yPgDMPs1cIdMb3ghjtelR2T7Jrjmovqfgnn8sCJhkP8Dx/G5FD+LtCbmMhqRzkp8hWu/Z8aHaHjN
1voh5M+CcoySY6awE/1HGgKCW+Y6owu0zRkUPjc69SycvN/4bHPVKG5RFzemUiySMSTYWIeB0bT1
CriCWv85g5rEqzs6KiBaXfHeMB+bcgmArD1rwbQDf71JSAvONKOJVBi8htDQGjIEKyjrY0BTCvHt
qjqSnlNt+szeoWQr+Y2wvA1WCkGJnjduHS4Gw5DOkhnMX3NDQgkVtnPfR9vE3eYlYSnRG0wTiRMg
D3N0fPz7Ewc2mFKhBpEOYO3sMbY2FvT5pbSLUaXGnjHszbZ4sUMRntkC2K2B/8BY6eT5MvLmXlYd
BrNmh8GpQKKBJQTA+J+SLOkw5lk+9SQ6dNqRjuAmuI1mhGogdJg8Pe8XejRnbEQLdjJ/fE1Kp7n3
2iaSHrmQrcdeShx5g5fj/cGYlrNb1lhE0BJOSrhwGnaAkXr9/Qd44PSjRnEZsy8onEH1lOxogbww
pnMosbysvZhRPz7yKFLaC3ZZhYaYPNMZ6v14a1G3a3t5WeWe95YmBtudQDG7CqTHxMLWjEgYPfyI
U974QLb6doWCp5YY9tFNNnGwbsNmhwwj450StyYs1jsJNfdDzIg1iipt+HPIXhF0gi4QiVUwYhDi
aD0SUUOEoGkkBiIBRqjJqm3PZ4h48DAXFZ3VmKty9pJUPGWuoTZBeS9FBI/R/synm7SBXoAmIQXY
VxvKHF7UdMCLWHDXYxYrFSTE+sUnSDVVbfnoEi/YxDZCO4gW4uRyPgO9rJF5cFjQfhf/oWlMglsN
Kgo8ouqjvawi80TNEd8oRnUVRSnE4AglbH9knncVyn5MR8n1XttzAyyQvawO0Azu21Jl7azpHTcI
ttJ27EN3FFWdeB1y+f0oVFU/RLehfK5q77FL8R01OzRlv3NaqDSOlJWftNnCuHJ2/LpWp2dbJS1v
96BMdH5iifNDliWevfv/sY/aw93wxWvRsVIE++IfU5KbC/67HqELk+3XKtdTDnEQnlJCRBxLHX5G
p7SrPulkteUzi8M/ZcaGPiGQwUi55YcZJeCy/DB+FHCPHUr3LXdLWOCW1XqdrI4RuRG5e4ZXQzYM
2SSJl4NWOEnugq72g8M8amfeHnd2cbzdXQommYlau3vGmXG+a1rwTToOtH72/53H8vO9UAPPrfDW
7ufapNTUdVDwKNFir6k9fCB9WEWl8byoB5qQkVNCwWJLpD0H7XkQmxdJJnNFtPmJHpcMgW4B14XP
GFrLY4j78vZIz/BAeXGbkAq9JIfoN7YZ/TPAqKn+zoVwQXFnxa8wZLTxIyv3OczM0kr1k0to5Xnd
8EsiXc+N1cNbLqF8jj9vX/eL30atg0kzE/6xmFbA0LpWjItZiC7NrBA+Z5RPdu8YxZhggu2BBDrm
90G2xBTyhch8t+Lr9qxmA3u2UKnPPYS8GyshxaGHUxjR4bCmh+5DiT4hTJrcYuPJ3tU0UdScES/t
DTKVpxN/5TmdDAt4EWk8nDW7spGWZLNHiZudRlyL+AXHSmbhrC3J5s8BzC8xOF1HXL8XBTLIyQKj
Aa/JTyT7X1KG51E7rPne0HNJQogJR8tkTqz+yHcpfgkGz4Z2984TR8NPlt08hUE7w/N/BwkIStH9
SbiDFt+GKi5MdQZC7a0oSQxqMaDX7CqH7hLHuAK0Hg5ID4KVMOmcBIw+pq4ipOQaoozQqtBCuSWz
CUV2wErcsP4MpncjkqF4N0ydjztxlyf+PdVvEkHb6zdlo3wbJvifJaZKqSOb7pKeI344tjjq9ScY
jwEs+2mvcKcYPujVJ65H0VJMBpR8QYhQd1Czxwy9WNNCjNDiSMT/Vyaxbn1LTN6TCNehFJeaZwgf
PrIViLMzJvA58yWShnDmxv2AVR2UWn+7IJGSUkBjKiAhkxKiVy8tGjdMwYGUUWZ8+6AUlI5mMBzv
XuUBFcFPNojUBD9xUF6nRdk0TwNARb13ZEkoVo3JqjxaAa6RoPwyrUhLsUAn7Is90H/Tb+fufwW+
eqTqgEVdbF1HKA4zBwMCgAgKShO8IwV1O8s2qyQWQOlv68XsATfJBtMesiYvnpvI4/N9MdIj8+zA
8mtf4LoBNhJNl9K2PMxlT2J3smV90OBqLzoQkAv+NLOZHTOPwVbbpwBy5V9sT8VqTDlnGvoF+X2W
RvrefCxEmp0hiDkjliRyfsJ7aJC10WYQAP0Q+m6krAAoDEbZmOSezLJdSFomrzHvglny8RRst2Ba
oXCgsEVexLJlEUpacdqu7K/YKEyA8sotnpwRaMjP9DYPtHbrR4jRTWxVjgV6N/JUwqGNXVCjmFnH
824lHatUcFUOaQpFaKK4jaJ5JY59zA+omTcQnrB0wvBSbQhR2OQ9KHvx7nz6WitAtQYX66sDi8sd
sljZKV/WhIjBylBecvqC+J8UVQoJ2INNLo4Vq1a4j9uGEtj52GkFPk71xp0cmePQlnI1rpm0ykAV
I+XpEdpe7iEJYjArxZQTO3XExnlHZpBVzx0GRVwkantELst5h3NI2l+oTlPmDL4d8YK22/Zh2KSd
aekam/ww0a4H5jZ2MsqRExm8CPQ0KJNpgW8vb7P4avBig484zMLArzD6HBF4yNQsaC2ITtFRENny
bID6djkhmFOOPefM6qiM8iws7GJy6yxKalIOmgBs2bI83pJk6306QtJ0tisv49YTApx9GTuvHqO5
OLG5gmDTpxGSpt/guGFYfZgLdTRLodsFMQ143+5HS7fpf7RpKAABKo7vprG9a1MwTi2diyWqIqDJ
iAELbG/9kF96Dhx2YyFB6Lb0IJKx1H79rY06vWXykyyjgPgfRzkWJ5H43VY+DnwuvvGK60MX/UAp
ltfhRfFSske9rAPs9cpyIPcZzdm/VgWkepc+r7EnFxRnSle3iz/TsRDBdWiHuVJHlvwi3iiterEZ
UBGE0V/Mg4iJoxuXJwnjGDqDhnCd8G43ohTyh1Md7VUE323TVsvNeFGW9z+SpbsRYpUpnwwdRmFW
YNmvaVBi+cw43a3KD7UBerxAyx6Wsytg5jjIWTQO+Tctf0xvOZi+B+KP+b7B5KVksO9B+3GkLUkc
7qYuF7TF/DPaI2GSpsYMfBMzd1HOmwK7gsnGMsdR2lAbfZoU0xptuyqJ2pXK8T0lTIyEGndxV+5n
xlFmMuyqq3aakC+1FVeP3a4s9YoXQqRINviWxHJV0Y9ztPps9c9ele3LhAT6rfupry5thNJn8iWT
NoD+jksGWGsevkGLt6PpuWybiQFi5GMbNLSyrn3xuRER+3E4bKFPcF+grCZq2PiPrmIdntceJkLJ
eXd6ty8L6MxVX4BY6zY/opYa2aBrvtRJsGK+Gp1cPRXmpaDmNM/afKz1sNGS3hOssEVEWwzjNQdU
m2xtI2P01REursjbtztBYjkc2eT9OlN5nG7HZHybCokgZTDJE9Ngqdwr+yef6rFoHnsJ4IBxHDQP
EF3W0hKdmfZqbOjG/diH6psxg6QF+gqz4BdgymdjvCvPBjLc9TKVl/8U4uYonfrHd5rWD30/A/WE
vt42AA6NWIq1wVIAq9G2iHN/nz32Rxoq6zjshzanKqiPGH/8qhkpVKx2sp3dYO/qK0zQh3m9Vr9k
3dfdgOMBP4f8W/R9O6LKdGI7b7pPoHOxAyGQckX+D0ZESTnp0jhNhfvZjB6GtpE7j20BiO0s6caH
GnqvwyVxuXrK9gfP/XpU2nAQm8pMqomJBUXxK52ye1lhr31Do3egnghck3WvYHqsjRBKjU5pVMEe
MIgINI3BzvNrBQjKqdVa9nJHP91BlQBQqp4qZdVovsXkqSE9OzyfpyHnArahzDeWet824ARhadFu
OsXA4ezCe723UPtlZD4/MYQZdk96OTCN9K57Z+QRlzi0SwCbFB3WBU+cBo5b5vnDvgrMoyLohOiH
Ei1EIGWND9vkLzuKgBlaC8jBHwrY8qMtjUztdlOakMa0wUQf3pOOt5KrDIukj6vCIAxU4EAwKOun
K9oFaaVu230cNy+QjLsQeQutRHu3EHXnkyq1k3tSZtdtLsUxnJLKemZS3/bt1FTB7NZFdwPRtGlN
ss9UJKE0ljt3gxpCjPIy3eWeVHqWxsdmtKMde6aa2aaP8m8H/kkUzhR5dKJ23SQu7uuInMd0UZN8
0NgcCg0QFQoVvywqEnkIlrMaMF/nVGuSpOySSFljx5BVG4V/SMzPwN95MY3jS8T5GfH8fJRQzzJD
SyzDl+oIswmlTUBqsbL5sUsIbeg1h5ArorpyR2Oorf0lLlKEjT62tzbgSPGGFgrxOthMimxpuimA
OCdAN0lqg41T7uF3364MfIdOMUPD2t2u58e8MaViHRAxWmcYJdOz3BlfOE7hlDdjwQSfo3EJsd1u
LUHgg8uGsbxJyJy2ZnTg+nSnhPjbT7koS/4CS+4B4XJZkeVcYKCJyFpao8OFHjgQucL5mA4/Qz1b
RhDmxYS8NGlkgaPp0gFRClTwzuiWVZLhoyU3JcPFxvjm6bEuFEmwz//lu5J/ysGATbOUQX2sOqda
LisrccDE9aOxZzN0CwPFBYgvoQYjjnn2r6uRWzNh+lTNNMqiH+lxBWbud7Hh1ex9plLA7ymOlaqF
R9zF5b7iQLC3Zkku99jxpVbOmN3A/pXMxlrrZqoVkk7bjESRCe3cHh/hdu7JY0zPmOk4yGA15TXm
P+B+gQrNZd3MUfSMkEP/roEV0SMslZcuJNJcntkmKj2SY9irwAbqrfpfP6ZVmRPlFlq4ysVQ11Ke
rkAHHRgwXs2iHIXJVJUKrRQQdjduPh7xr0on9E2H5jahNiJeFbZ8u583qh7uTsxLKeDGTNPcU2eX
Qqrdn+G0D72vfmTldzj4wcdb5dWd1EbmgbAX6f4WR8Jmh6NdrGt8B4Ng/erQxpPkvYFpcRW006EJ
VDxcH4UqBttqc1+3a+OUzULebY1RnigI5D1whIKlUbLW4BMZWh5V/Er6mBE41xIamv4dI5Pj7anz
R+lBuvhfBDb6RqiFkblKl7tQ4ZfDd0iq5izcjmtLSnc2TXnh96i/rwiAnbGUuywSX5LzC+E3yGtK
ZgLuHF+zOjV9ttFdzmxgkuy94oA7kyGcQGjCaIRQXTe8CdFEK5xZ5NXvbRA2QLU2KBlwK29Ck8Nd
tba782t2s6dsXhVyvrXWVrops+VA+u6e/igWbTKSqSb9EBVvlDlVyYhpbJ42yZpnesFmzkeBCecC
/xVmEGpF26kLURPvVb7ow8rX/QGkLNkJi8YOB8XgQLHMN5xoWK5/B0tmTgMGki8iNYy5Ca3SNWS1
stdp/tw+08QW1sIuqDd7Dew4Gjdib+XlSOpViYX1KJJ2xuenc0ODZDOO795LrKJ2k1mVaQ+EBisF
b8oBi/770bKVER7WOibTHsx5MqJdkkfh09Zf9H3r4/gkvUfrrSDpsaVS8pLdcFWgR+TDsTf6LlNW
Q2qsCWjn1eBio1T+mwtQpzbeKm5cnL+ggidehqX50W/dteD/MnUUPkgZnaSMv3NLgIMCkdtMT9XQ
2R/G9OdZ73OYJ36rN+gD4JdYSXoSuxe8rpu0PmMuUIzxeaWhVtbDrQtS7P5SEXQbTHm2pdmT2N25
JTtlwkksqTsAaStGEibJF6IEX84SL68hQY0bz/na+lZJEDAB6aR/bTrOp9XFalnm5JcBhiDZZy0Q
UY+Cq5SAQrHj9gDyvpksPtF4mw0+s9J1ceY/9coIRO8+FW9NLaa7ySOqYYN1Y6KDZ57fPwYgHW3k
g3FSZy9XJB6IlM+r7EsVKq0nCI3W+n+bS8DqoJbjHF3aUzqkCa+ptJr3MmwOK5REMEgyRdWO2145
jdWjVtyvG8lNT2dLP59suQtQO6XvcbRFADMm/U0+/9DluwXM7NsDYGVs4/cF9kG5hol5i94TjgQO
B9pJfHiVd3U9l1nXjsGkH53yar/YQooK8bPHipquBcJ9W8DCQL6s1OwaaA9nlvpDIMs6Qa4K1BCg
5Z5CRVkGTq1gTt/FOpXeXxc+RkTlrJYt6LTMicoE7x0zrjfS6Fyf4KEIYblUZ2xKhuRgdA205Vjb
hMFw2D3jsg36UIethczFcM2SSYlGOoesN4/kZ/9se9OHpkH7uzDdHKoo3HkKuNu7h04eFQ5CESz1
tCqirVO23+5wVJS7CcglH87U2dXqWOTJ78ecdS77V31UUA1dHXwFV0vT+ACLVode7+i1KBGSMhNQ
jQMoOXIee9kqa4A2zdOR3rF3cCh7qy6zNisuUWRZ50K2ihA1e/44ICUyX187PBRYjNJ1/1605tbv
jofvZlTuj8Bx85r95ZRX2XQMsLhA9XCE2ALDxPAy4mn4hce0+j/oHqB2N+v7lwA5UDoYEdzZGBTH
wfUWLabTrYp/TxIi41/8D5/RoqyxWLiCrkKcZLTqQnC9sisxZ/WICobWUXNo4NdwIc1CWgSjOZ7o
q0ojL1jhojKstGDnnCxLK31jyLb+KgOwaVhwKxDdHv19FK6vph1uXVWcSoXdse6xqjXShT7dD/tt
5adE3AaWG3lNOb7pHsvoTl0wI4D7GtxBqcndIwe0eIR3Ly0EPlntpizqIM8O1g3u1umzFoE+zV72
pUvs/EOU9EfQAPtg8UzK/EB347JkYCB5K9MJ1K9yzd5IBVfqTgmCjMTpwnzXxjgGtechFDG6EtG5
vjJ3il/pPGLck4afqxPxNpgwiUfm7eQdaXEaxBrnP7NgufGVdNZ69o46t5LxckPvptPB1/li7o/5
JccSKycw/GTlMwWU1bOlZ59u+2stzuRvVmOyi9GapkPgULbPQsouWl2Yng9iKsKa6a4ABwK69K/d
fE2f/P4scwRW59O83jc9LUKIvFtgxZHib0Wqzg3nnRZVI4351LHWgVnvvVpzFphYJM53gLQP+84M
u1tWcZYmsPRBrCDiv1QUrStAi1HSn3/V9iSHdg8/ZOt4G3Teqsyqkav9eCRn1jB7eQzx8WBmwsCC
6BsHofvZ1sDLIrA+dBVR1jEX2ANnoCmmL5qOyO0iNwJAQY4AruiPF8eRpskrgaAJx4v9H4PuCUlz
xdO7byg7NZAbCn8ldCfi4GhXmGbqteF6H6lKxQOU52i2TrN2HNHyQ6lAS5wqI6wPMci/qoCG9TDZ
O0dkLFKoD+tBbuGnpPv0i/57Wrtfk+H/X8g7Uok1eS7ya/8+1k8Yh5AdL/GVz6vlo5vkpDDkVXuI
6ZnjbmYTj+6WnRRdwIROcxdjstBlwD2bzrj9pUkGEu5wQ7eBtb681jG9mM3KF8XUVWf3HikOEwnJ
A9OPD/BkvVMzklFKlbLb9BUiN5UHcybY5z7NpmErjt0WgZsPBcraww8jJ8S305YQs1x/GPL04l0E
039kFR5YnRKoRvDP94gkJ5X7n6dyOUOBrgkopth5epmG6tjnlohmcQJ1k+aDPfG1b3TWixW+2q6F
19OCbRwKOpV85jeQNP45yBfIZUZvUKnI2vRb0Ln0EmEgJdoWtDoOYsx7+qJXhN7osfyDuSrHOxc+
m9uFs1TqZ6S8CM7TUXDqJJpBX2Fgnh4zdyHJYtHYi8mmSH3U1n4+wMnBALOGLo0S7BtaVGs9b91U
S8PfiifNvIv7fFvmNY9vnVl41xi82Z/x6v951czvfHZrltMlSTVTje77qNwnJrFnWpcxhhXg+HAk
9gDxiEJc6vLEZQ9x+L1bmDv1JSL6HXKjGogzaLaBc5GCt6GCD2BD1tP3ZRDCJ1JN4S1YK4ru3T5r
tRYDYeibTAn4XSqyqKuxCFU6R9Na0PFR8neDK4MuRlv5Nk+jctOb8tDg9QTzRJkRNdvM3m47YCEx
/4KAoEgF/GFYUe9SbAkH/54BOVvSrTfDaipkBQz2PVmfEjYXZVWSpFRwnnuRWz7vT0ELxX3B3PvQ
O4+6v7HYMdKxOWviD8FmMNOAzNnS6XPnEkgQw/AS4/1mvYK8LqCWBMJc0VdhDFw/NGDhv9ccw28Q
QJFq7hCju/Dtqa8VKgdIz6pZrasDe56aggf9W/mWF+6tuwKD9uvmtlGiEEZ8cc6AwWqBKamPP19y
j6xNAnGjzZEyUh7tnKFRdAYdXu5UoxcsK1eNxEwf6/DK/wyCVXv711RWaXjv36Q/KvE3NkRH0PwF
Xlz4k3ZDhW3yMEYPMKSF1Wz1mj6o0VNEvzxzryI/NGhJm8c40xqmQrEHpHp40Zpsiwl9sx2E+P8w
TEOo5uFy6JenJCYNI25oU6b+hG/FXfrG834IL20Fwp0VOchCXtMh/5N8vkx4pHXVQKtxZ0o15Tc1
lnPDO6gyBWyIOx9jj1E2CwehfOuXN/5UnGb7+thuyEF4pn/zzpkXNvljHy7dQj/uVt9Tl0QAt/Y/
11os6wAYVrNh/ZK+uBLqyXcFjamlYfEtQJp0eUAGVfz0KVFxwc0gsaSnWHz2pqx6jpj7bK5NMymz
uNyF81vQ5z5gsccMIEPD7BeKjrUrW8yid2WxOZdZDtnQ+fX0/ex8PPsGC1buTiDx471FgSWMJcKZ
e4yplGQPk2jyiHCGbeLphGPIViWnfTxr4DUtD4C33nL5A6x2Lf6zm/jv9ddectwXoUU2zUqZ63II
lJGoskV9EjjWvil6gRoLs9k0ftSvxs4mS70If+w2piomarGNyIUb/69DWTO0x1kabqN/89KoCjPQ
B+64YbaBvozkXSIAdV76/P7Gvdq8Bbd18VL6rOE63cuRlleB2gB/dVibSlQb3IO4ZI4IGG7/kxBQ
+F0c0TrYqmc6y/q0a1BpFTrxXJak20tQtqg09Ir+wlLr6rp5/c7AAqxGfRC92qasLwBdFAYiSpls
99RQn1LeSZFgkq5w90i49SuS+h/bogbrHC+FhR0lkjFU1kgkz6oWR2JTrnpvq+qPfOL6p1GLZgFH
JxfU2ozStbjs0JViYJzWxIAGoTQGePWNZV0wC1Nq/i/N7343M/GW2Oehn6Uop/PkvZhoTDP0xpLa
ygnq0bbOAVaXoOQZzV1Zp4j+w/BHtePeDALoYCtccxp7/7RgJNZA9VqpuKWRtTKrq//2YZ9m2Jfi
9uy4Bqk6KVYMDjEdO66KXKFcKTxMEd9pRYdbWQ8/ICzg2OihQQ4/9N4jUpnQZQNoBBSbbEZSJb30
8RZiVzXKEEFO/URsYOEYhHQb65h/FHqYyQqHfzVPPs2bOpp1vF41nbnrj7RsNL00iP9thY6u9gMe
rqPVG2TyimJreCyZhMakKaT2a7E+FGA5t4nPR2y3quekSL6jCA3U7fxdjho6gmYj1qj+84uvYOrM
TTB3Vds7UVfd0G0ZEHW50B715CMw+EbjuzWMjCojsUzHHYmC7pGgfyDymiS+D3SeNWJE9OZTKDZk
pmEydKMYWV0FvGaGxYo9xZuLzaOCUqGE9FYGByulBYMDxxgqRl4kXcIByYGA7MQDQbV/Z/hn3jvM
s8Iie0XM8XizbWoJHEdk1yXvqifbPC8FPmNHOy4CjUcClQqUyuvLEHFLME7TL6wEq6495VRitzR2
z+ulf5oqY4IprrivlzP8QhuPWGsjk4n8jBubk98bFaYB5ll28/2Q97K1+zlu0uXMhf/7Y96ulLD3
KVTX8aUiMCksplkvsXPA/JEzgOmPvfa0fhNZC1KKW6/ogsSjapnGJor/I1pgCl6WdW9ouRjjawNl
XhcH/Oko1e/k5qQgyV2/5NQWmgZiPspnQ9IppbQPcmP9wzNEtEiolmgRixnSNmUDB6Ezkp+OHW6u
Nsdhmcss6o5N33IdNYSiTL7HiNZlUAsC69OmByIfQqyL3TrTYgTUkzf9AXVWT/dK3qPHeJFd/Cb0
77xJHTpDEux+N+a8Swixml8i9CTGsrjvewe+vvO7bZ5KseovE20loEh8lWpjb2kduEV1sB1fc8Ql
stewHcfvV91wUT+osZlGC3+bgjR4nAAfwVQzIBYT4+g4CA4q9SEseLk0fAZ1PTchkqYte1XOPQ7n
cVCofc6q/krSu973gUdxjeU7Zm9LB/lM1Qm763eBW7+RmI+t1aiPG+U1u0iq1BkmGnHw8nemu4kJ
JTrqTfDTxXA6UgfyPtJIqNdCguoIkvKqEHc4tN5JNxEOx7GtDek4zaab9dPlgIdM9sIRyqCE6Cgx
wh5SMGmNYK6GGswyzj6Qo83kCRanReKqezsRSqRQ0CxE+cRC2l2ZoEQJE/3w/t6dhOL+B1Gohxhl
oQ5Jon1AcXNuYXvgANB9QYhtenUx1Li48kw8NYHR3uOcziKG1V1TrQhwhEKm1lqwkAnoJcjM3sBy
8rYIJzdaYIpyAjRZPvkNvbSykDlTINRh4ziSADNQvk1mry9R1Z/cxS9qXypbEaOFJvDCGB5npqSD
J2Z3z5A2sji9f0J9DHuS1+WD9SPZxkFK4dbtsNt6t+cXemKFfxCCd87X6+9oEpCgEwdcBJUBZfie
NNdlQ9OKuqrab3AtlYkYoJPEYGEqHXgcic1J07ERDA6MrQuipUq8EZ7vqmXlurNX7PBbwKc6SZTp
+Hysdh/V5HFf7A6kp9K8Z2xC6PHj9x+DLc6GGTfWc3kT5vdQL6N4qnlCw2hbJm+yKkMPKKfLnBIB
iko8RnGcWjaQNaxw2xP8807Ey3c2bro2G7rlFdzsKla/cFQMv+ZyfT/CJvSC/h0afXniNPFQQhDa
9Dh6CpNbBQsFl9TaTH3kjRGmTA50swPXXOLtlg1bp38jSrWIOlnWo0AIHs9u93pWExY93d4dZHTy
eLB3qrUgNxI0+dLdJ5s8gIGZJNHZYgTXgM/80JpxUFteI9gm/fdqObvR55nCAUkAsbRkba77SZrg
5cv/8QEKbThHnfGJqyHdH2ayD5HzxCjTC8ajqnNcoslmKa5/0PIDQ6hzAFJuhqqgJ0ALpsJ55X5a
cHfZOSbvG+Sg09GCSDjluR+dyRg5OkdBhcTljd8mMTuhVdiQDK0lmufwqJouPhf7bPAA54cTsgEO
yChuzhbNPSOhjNBLglYCbsHKFatlHq9MXrbQLs4k32ywS2aJwdbVdIpRtWM1MR9iKNJIkKjUhJau
EwjB6GlvLnUxTsS8ixorSCyZiYNb9DITwPshxi1zoHpIWTZa9mcsd46RRAF/AdODiLIjE2U48Y3z
/YNmHRX68CbmchnHpmI2YzHQF7hrLLNmvrj1ClGikKQ2kqEo4BNhcZwdUWDoOIdLw57A2eAqUfV9
R5DlKMMFdmIY+0lFR59Iy9Wl64lYnYdAkMrtT66st8xYkcxDPFyzx+yuxjxYCENE/YRL39E2/dVO
3e8R3AeGA8pVcokTyZ68qwBq3EfQtzMRjF2y0D2bagrr6kLIeQH5eJmAcAE46guXwmt/np47ZuJe
M8IzcE97WlnqedCceyG9vwT32at/H4nZDoUp7j5kBKFyjpeZBPl8/d453kejPHYDdaAagYffAUTi
3rvx1GbEdkZ88+p0wZorsbMMqYwcfQddyepCO2BD6losCi2RmOj3JwqqOuFF9c+Pw09nl2KkUsRm
+WIEapQc1lpUppkyyVc2Cn7e7lpI/144uBm6chCuJW4f2M0yUjxoI2bDZy+lCyRfV8S2xAXWLcwM
Czxmi1dBd3AjYFs7ehnSK4mZLsBzvF2j8Q0JOaEE7Ol3GT/TbTM6epAhv8prxxA4kigTVPm+/oSw
UaMaiOC52/eBmBzhuk4lvJ+yBVVvginwDNpKlPUhzfxMvNRMznVFtCDuV34TGLuW/FqpErG856Qt
/Ef0qYDIsyg7CM0eJZnSXU0d6Atp/7KkYl3hxROad78WnUSkXgqXu+Ivd7Gg6+5/XUb3W72jbQWV
Z0aftxEvXg+BcTUojC+MF2TGd2/F6cR6iNALpIchSAAfnbSSxNk/OZaMoaxK7BqsX51tDGGD5NjV
CZlKSjwMkmYJhe5/l2m6d83a61ikQE1IxjUsnorMlQEvKvzjlD1Zs7RLFzqmumcJWi4+fXFLYBXf
fG5SoWHhXoQ23ZC7r2DzQm6zFU+MLgW8JtrBMdHQFDbvfChGZ24t+Un9+lweoBRaMDlMnIMqCl9x
PSfMLWnid5jLUpNK30jvQ8QwFOy8pgpOKl4VMg+le8beGQ0aG8CFnwbX7+enAqTRABoDJj4b3902
vGbvWoA6VhjGMA+qqmBmXZfqRN9RH1XNrbVTjeQSDPv7RBbSjmUwK0q78eRSP6RXoMd2GYANUQ7y
jL1oEFA6R3GKpVFhF65RavCTtLJU+amjOZOi3WXn+g2jfKFxwIoSfqmj/MxR0YovhBPOePYWB1SV
BHtk9wXYPNzfOd8Iq8B18u2h9XsS5oWjmlNMgn4olspz4ql4U/1fWioExINzGEtx9FocmowWHOyf
uS4xOpbc6JYWp0xpjGZqor1AactZ4wf+7IdiMwk/ULIKOO+M+dIrBhZwmCmot0HN36TUrMUyUvwg
lRAlkiB3oM4rBmumpAwKuE7DmVEp6GEGNAEJKGT41V4qbgSSDJdQP4YSzfqNOwjHiG42sXZZAefi
f7BUV/flaap5nMFZhuP5ZNQV7PTrh8LiEnHilJfivy1g2HKwwq/kaMPVkkEkuil+EnggT8/x7P/Y
WmuFIybzjL1Eg0+MKyqQyA1pSB3QMQDmdw9jt8dF6olrnUicoPOQVS1tvCsKVvs2PJ2Vk7QxkXpG
59K32DJLE/u0JVzCyyAbW20tWvwAh1DTiFMwUeBewMm7VqFyq5q/gYo3+HipKi/j5um5Vnb9ijUn
2KCnIHOYLUpFOtKRsGTt21ymZbr6Ifh3NwTl63OVORJWEJMDfTTE2cg7jsPMwPvPOPEd+6zfqnmN
QQy8kyWqz62KTfiq+KUIYfQbCur13nmjuLx8enMJRpbklkgsDccGHKcnGagL2XjBS9KVTHeZUApu
7Ayc4LOogcbLbeuBZLKoHvCZ92pKornrWFkydFSjwCVCiH+Tfd59fqL97q0furE3PVA7+NQt4bIa
d4+lmPFLXcI1OSEPhTRcFUeT3kQ7+bVyvYMqUhHyWAc/NjO3Dtv3+QMkcVi22bjrbjHIwuOrrSdY
Ul+yjM0Z1N14yHCiqgB/ryHBifC4kAWDpgywiYpBuTr+u5j+KNmNe3r9kdG6jTYcHUlRCs7cb3iL
stClALuqStHk9jPavMOPnMUCICxvpzZOjKuN3l9GE0lczMeWT9/9z2Fw3ZTbi0iCBEFfMxQtbZO5
HxkVpAc0WspgCJlCCDpyImWhajF4Up/l5nFNJQGbOUID+XC0R3U/VXcMBV8FYeX0fRzvq03hh1hi
BE/e34yFQdfG6DfMoHUPMsWvDfszVZnxGNPN+jyzsrL0tvudoc7n/TQ9osKWXVHU7I2vpgKG+bbm
sFoeRbJ3XYzWADbrzE9Tr3smzfqnZ5d6uU02t29oAMM+oQaiVONGMEcn+pvPOHxSaPlCZ7cVwo7j
xFwtOGWXt8q6qhkgA9y1aWSSgsh4yRWmEGVeBXbeATrAOrEorQSN4ONIkgxmof6D8yfjr19I8TR7
7Tcdd1I3RMITmVFr4wE9p/m3cZNyaIr42kLDynh7zrWjG38qTUK/cLq2QW83POfnncw4p7t5IM+7
S4nh4emnhpiS7n7MEsOknBlMZu5KZuY/qO7zbehmr8hIGdl681EYOM+zkjMTvb0zxUXtlSi+k54v
lbvPKXdTYKfQMXQCVHQysEkQmLxEG/A1jCi53J5tDLO+ULvyFeSlbqa6CxxasaaCFRL0AzchZjZb
Rc9Gss5aLUrGeGXAXIqAocb5p4n7gDBI3c4rZCPxdb4TMyGZf922zjfk3N+XRdJ5U7iyR8MC9Ewh
MFny7gLdxIV6/7J4GffANGgJIHsrhOImAAF/4xsX+Aszbp2LqDSVZb4zYBApz+se/bjYBwU4HpO0
uTYspwwxK9SAhp0apSSuBbfyTc11BLLK8xfZkxkkgJJDUhMc0TdWBKf/8BMS8dabIAgojvrIuph8
wiPS0/7GRwONaicV/LJPXmz5eSxsrgPdOcjApvsjll0r0mR3f+zT0jSDqRedQN52f3B/bPo57/dB
nnD0VLO+99cfJbBdCCeNrvem2JbpOAdMNon1tayTeMwtVBjx+5zdkIn6FsUZJ8lWdcZrav5ZoPSH
oJL1t+0vnPzccVw1aLdcJxEyguz3NmSGt5CSp7OumK2A+s3JbLqEXFIC4WIw25qndu76/ry0iIYH
yADl4Cx7HWQHBZUK2lNhe3GmLJFkKo5uF+tbQg45vqoTu9AYIBTYOEe7NNmOiEexU/kf0RnJk0FF
lX/tUfz6Nizar/1bfpACBM5q3zl34Fymh0732cO4ymVoZKbTYalSeX+KMBeDU8mmFvuy8JiQx3jh
UjCJJz83ZbiumQSFHgS/ZCGJbS48BIOw5njaAobE/Q3+ycu057C4rQ+j7pSiqqd7vzuLgEKvpZCI
IGbrMcUFAaae6RlzfIb8swxNVaAlUIcMsqVPQzG2uDUuzXKNnGKj6bO11+b8hFEuPtwfQFeTXLi0
bug0Fk2QhFHoWmczw6vGljZhlE1GaWdfJVcZzZQYRuCUugFbwNzcDLuL3oiqdLrm0AME4N+iMyLN
1rCzK+RkAiB9vTZBOg3KziH5D6q0xmpPLTAx1QcDoK07aB8v9PQgtDgOmUJSdI9XKOMenMPWSiFk
smmaet4dv7GI+tH52zkxjL4nhqziHcjshxHsqexNDdJnhHEZgldX8f+imwZay62z8oh6Ugr48ORE
BtdjCOehk3M0r+1EkWsM+Px5Hd+tD8VwYR9d9uoJgoJBoZB3HU3b5RSNTv2h1A48B2f+R0iftGaF
NeMWNS1Xm2HieQDH0hYTxC+GeuGB8cGv2B5OyNVR3nJPEgmmLdYHknNDv143vIDDJbhhbN7zeMDw
w+BHVflWBMFsITRFXudvmAScZKDRkiow9Ux0Xj0c/lJRn7Mdlwt7XcDyaSkliNVglPczbINa2XJL
pXqOIZcmIzwTL7/HZPnfps7+Pzu7tq++yUxbQytOZKcoxiSAii9FcUPpE9dc/76H4Hd4JpUTReKW
X33F2kmtWK6p1SJLWKis2liUu8dZkPGAI6Umj3JN67KYLwaeWBEn8NiqOeYGAX5h+j7uxdFJj4Nq
5WeOeHhwKlqr6OO5ICLGhL6w7vArNwcWO/qpSmmMMqLlTrtsuNo+jCieSdgX/TEFvy7VPRWwu01Z
ayv/T1FC12YbGSClrWNrxyzo0txPe1/pssm5jrQRWBIVIpSO229UNIWFDeenJjyk5Y0CR59Wx71h
MLnmMKkaMXXHZVNwZrWmUP+wPZ+t5bPJT/1I3lfxbGQ7f7kUB/KrWMv/b2ySXqGLBFg+r+fcsbGE
AQUNLOEOPjQA5bGbl9WkOIkp8+IwyXMQB80+GlmSFfopuiuwWMeUHYCFNzw/Z969F8Muod2oKbVN
IEk0l9kkMcYgK1CT+KbUSxyl8e5Fzuzk++M641sdDWJGFgPvhIjcV/rdXxRdYs0PgMn5WIZiGGM2
o0vlS/bbnYzjDkfZUV9hAyQ5g/dR8lWH7n5I6iocXJAPoaG/xhWzoKrDkXrqNc/AsS08GqDxEKnS
2hD2fz/+NACMPqlSUrKmjF5FwCzeEEyecoKitaJZmYv9DzPsUZwbO/WFPYGOPjTLn1pcLhBMbTQL
3TRTL6OC36TK8pw7zjDUSbBtI1eGx4J7+Pi84aHpibwEbm+jpHy7LuSyvXvMDu3roSakNa5DENha
310LCFzCe9kFkkX/aYz/PwpQUHdniav0zPCga4HY5Bt+gLHyPAIxmZkQ0u4iagPN1AB/Nof4RCFn
RCJggo9Ekm19fYwhHpQzVym+yLAv5MDH9+dM+d0fFBZZM1I8h0kgVvNBiNksh4RmaAPQ+dJ2QZjZ
cZ9eSZlIZY51PYjriJTINH729r/mIv8+1VeQ3kQI4Rh1p3IdEavxYGIcko0YdLVDmXmN4nsaFK33
d6BRce5CjSp4kgILBB1LGsB8RHN1cAHb8U6IpKcMRXJLNaj7v8kzCW60YRwqVop9OlQlOyQNDHez
bKJ3Jn6YYx/O+p2Ey96O8NOsmievSgxsvdwtiTSwYxA5zgFvTPq0LkutW2Rup2lpSk/Io4WC5ZIS
vjWmFwm0cbrRFT8LPv73tEm1JcAnanApN3CanDOyXs1opwGpmm38IcpaYMxDc85eNKQtlMqyjzBE
UzzJdWVOGPHvQAiyNA/BQ03tasfAfSPKACrOV3Eztzea3TQsMyx10OFD90mR0betQi7pKKsBo+/Z
f4pBlenyb4rfDYPI9QaUYUbLuVyDQHSYr5Z6thGnTipfGyeUlmIC2WDWWFYyidCRLWyA2ad/QuJu
/RI3lxvAGuAFIhNKInUU//TuEFZTqbnD4WeCwX//aiwBzQxwt8m9+Q/PlWrtjnK87IZih3PiYv4e
GyY85xOteAMTtsZNsJSpXu/9P6U3Ofve+rHjJm9gLwZuw65zstTKEWzhTLRnEhGt+2ibRo72V3xz
UtIFImEU4ApfYv+ZXA5tRQW2cSUbuV2rbHyDwOcuETwVwQvrv0vlxsoAjl9FjUXhtDvqyF/DCcRS
BaZYEJS8Nxxzz14ctZ9gERyke43vexlCUUo5fCyc3iFHB8gB77nuN/8y29YnTuvHcLzGLjNQ7Mhx
OAX8m0fRW711hb7IpF9CKAe2nE0fi0WU9E/sz/GHDhdyFTWGLpqQhhRR3U/cwTNY02IjPxOKvWNQ
rCPSTlNWNGTEmXX/g/3ecneaW3+oFMRqkgGkR+ELkErmSopgVX+vZBvEQ8jN2pNDRNj8gDFPRWBF
x0JFb+IxRMohAdVxgtS1iCM8eLIO77rnto8jjzSguQwMDylNWyk6+YLAHp0vQ2Gqx6eEig9hmFqZ
z4IJdowD8pImBak9scpM8XzjGlA31LhHMI0ShsvwKPrmVEikT73xYwTb+sKHX0V/7cLxPQ/RMQFO
Q1s/4lWFvVOYpgZEajyH0JIcjvIMZAYcpAo+ABI2RuomPKVOUkUyBpD/Sevhs759iZe+AS6V34RH
xGWUoRivUeL+dOtSrtaVnv6hCesvQX3s+7X+MZ3cGEgoDOfP9EktoP4Sek1ZkRPz8sKZGQ43JMej
j+00CwhPirAbqZpscx2eJvSO9Hbrcn6k/budSRzvPaO9stDsOknx0F8nkxgXYQsZbiDZ2wmaUS4P
kML4AEX6CR5kcFoG9PFHkA1WQyYgyJ3H2/XrHucVW+InqDOp818ag/yLZwBSn10CaVF9uyCAZ2SJ
3bmMQAiLF5hjezA/xCaeG9AOP06cfizlLKWya5L9qtK1zxcS5cEj6BCdj9oRZ1PiZsZcAaw6iUhO
hKuN8tPTT6lQ8LtkjNAvzMolHK/hsuCbqS7Dfdsv0mflgsQws0Cs02vTj3C8WAooYf6u2jH9OXlo
nA/qF0yfwG+JIhjs9O/49JDPRewH6cceYLy/lJpg2670XlWQMsNT0/LOYy7FFUL26vIbP0xs5pcQ
IvojQ6JPALspJKyKCQpVN5JxyNWBED4PrNaPRfsvFqY/C8q+kBoLKTCVzgrCbLpmupoYEYYN3vha
+Dba/6So0DILfBM1ZnSf+Ov5C/RoFaKStySCcJ+onkPrXI0xgkNF0cAU9+dtjwelf8K/8A5W1zu5
R4K3TE+aUb+Yck54atAmuz7ut6U1v7pJHPhp+mPijMMqORPt1VC4L9J5kmDrQLkGHjlIRXMn5vCv
vaZ+a6BXLtAWQnYUmkoyet4Aw39l2jfnfAHO5hbfZOfksbyLSMz7h2w3dOuh77p82KqmLS9pnLS0
He9KgvLTynrTcMJYCbJ1JUWQBQTid8FRKyeF/3uxuwhvO+lQhhv8xtKGiY0HCmE7DUysna4GtASo
oFuI33orQ5LfDj67op3C8x6uDed1iJRDZhJFK9/7/xsDAtfJMQxTYv9IYM2/dS1Y+bZIsYDzpagR
J5H/2E4LV/9bdInipRPsSMogyvrsDG3CxKa4cE3eM19jWqEcABbcHWDiEo/qMLLcgr1TGSgzQ35E
79o6VoMDBI+DrYHergBWOqhD5jSq1DqDcnY4E36Pzi/RivNSQIPNQbeOfalBpMVXroO5XCL0O6ts
A0v9zjrH5iGdyg5Z6dL8nFz6ZFHtHuG7ArcngbrcnasJO5jUSNPZ0usGRjw+4YP81bWbWDXtIy9F
LXzpr01T+c0518QKBbOKIJF5ZGBflktjkarJr8CEVsApStwXIwHZL5QiCkGw9v9aZ5N7xgOAdZtV
QjKSG0yj/02IwYR2qT4dtaYODnafwWrCVH/M6sTRAb9ifSZrNvkc1zg2sEDFQAzwyHmoAXaDITNI
okr0uosUjjFy/ZXp1zrQyd3tzFMD6mxpeQavjHlKjBKtIOJiqpHbKnzVrE1rrXIiqknt/ZlgKGIz
BtRx8MQvURemLCZprTioNj1S32jvb8wMdfYORNXtXkDyEbmXtJSw2SQpZMedfCA4IMm9js+L6vSS
CCTOA5gtjKPwYMNzQm5Asafkfw71ZKGK42Mbc75Y5KHfMAyWNPFnLnullMvD1x25NbKZg2N9RkA8
LuuyO/ioRyLeM7ZS0TEwoXVAJOIcNX5TyYCi7yG/fPJpGLNbsiLZTBQuqA9QP3KxO/HzAW37NPq5
A2BpSLPJLmtEu+G4QOEby0CpfzlEsVmlAn2F0YWU/38+BzALIfdZ9ay1KLi69P7GitCe4US51Ych
obv9enPXFaHmTD+tsh0We01OLesQcnSBEPSoLnTA50+9xBjUJKDpUcIcgGIyB9krM0Ylee0pH9U8
HkEddcSvt05vqzGNSgFqZ+QmhY9Ck+qnFcyTP1f25vI4JEX6svYKAARAls1VQPFsXa2b5PY/lxvM
S8srSNdD05AffipGwNrISso4g4B6EeHwV2E9XU7PRuiukkymaYioGoXFI92g8lhjibnyEkui1rg5
coGOXRokHfnTKIah6RF1aFulUQzqN9svATlknuhaBnHka1ySAV1i9JbWhvi6ix9VElFKhOf7szOB
Rr5+HUw+7QHYDq9TFB3uLRD0bPr7fgAe8wtRsZQV5nHssUJLKMM3ausJAalFyc513mfN8QytL32h
O5dG/Wp/mR83ZvfxX/z/6LwGE0AzacFmPR42GQTNqdAdUsNGQibzVesp+DfNPD0rHZf8KnC8wo3R
tFOsZ+EWhkhFCQzSDzmXt0e/AqXDW9uTePh958oJ+Buhz2hx30Whi5EW49I/ofVgYZbTP6wSzSB4
II9ugW6NOEeC13TJuFSBO4mYwq1Yln1PuX853w/loceoEqE2ZknJwScBw+C6wBnu846xRlXQoGUn
X294+FJism+KTlkq4AiiwMXPs4p6cuomftHaoR9L0swT2vLYanXGvFkfoB6b+6ili4BrpYROGoqG
iJ7iKWuv22inmHLbd0PRofKumI9bq9zAUtwno9gQu+iqhfrqCTCSscgmuiQzKgGJZtiOywuiXcvM
tiA7jSAuod4Qx9copPf5uAdiDauyhxZl00Bh9ZYmE6e7FN/GnykFrjHvFvRTdUv1vkbyt1dwcOSJ
+FERD6Szy0WIeiroanh8c//2vycphKpcKzz1DXTGvgrLnb8IIJOdSLSQSPr2Lzx+aAB3tPxfZwCI
o3MmFftYCEwS7IaUKLyA2Ah/h2oJYLDMygDF+yvmoCrth5a5SpucEsGiO+qw1492AWBQayaZqsGK
i/igx0bJSk1WPj97TCxo5nbZNe2yFG8CZfiZrZ+d491MlntjE2Widv78h2v6ngihanWqOWQ/Grqc
+zUggG4NEwawDnMadOTGb2PKXDb/e6/zOam0KLwjRxqkJlscXOfTG1osSLO6uWg87fiLhU7Pgovt
XuSptBaj+AiheGmkTGvL1B9B9aiGMFmhSnaWprUL/rIlH2TRRgDllLMxakTaHY7NlWKKCZguN8FU
u0grSQW8hhuKx8mNWhphFB1th0gJVnyds9wHmr6nkGJoX6i07PezyMvymrDIRkIfNSA0Ga/gerO/
SaqoVO4dyjCFux9UZ6hdfm9jYxn4NYXrHHlF246ouszHelNC20q3Nugr/gdnoH1Kx5LjWGe6ElpI
qtgE/pTkvKo7A9PdInmTBI/fbwXZ7If7AinaHHLnVuD0pFZ1j8V5O3ZOJS3f/Xkus2nPbIM8iCYn
lvKewK7K/uQaiEcaqDThmDOBT6eWy24WdeME5szLE5qaoCAIz3lOEP+secuGqkDarbCp7O5szKFX
181nRwwEAX3Xqer4OVDVOsyNOiMAUjMIDiXf4tGBso9/p5lclvwPRaUdx0D7zon3KT7Jva826KoI
pG2hlzXjci9wog24w71LXlpmtdQAOIzSLkylFzmeeOVXB3Kib3Jj8u7XuxvLr9/cXNQd49cHrAQg
ybA4j1Tkg5aQDSCIY0nGkpEKi44efgFqzol3SsRpYsnspjrFB/vLhFqK2EpvLnl3rTFmAITI4V5M
UfcyhjF2Fa8AcToT/JKMvWkbfMNJhpTfgMexWzaQRUH5pkbjxUnqOXEDsOAyA7GGJtZFO9q8xB4z
0kt9AFhVYJQoAb5eMUr16aIdwVvo86rUZbVJjEpv/z2UEBB1Pz8KiXA6jr8WlCXep8cWr+oQcRKj
dnU37TCSLmwYoqp+MluJ7IcvEnVP6SJ8ykje7EejnmvcGZLIBwvC8oQQb4E6NkbOcPFD+OkbPLqE
5wKbDRplM4vNHIY7b8Q6lIvWge6BWKM6V9Q4LC3EgfPYQayeyOxqI9CSUEQD/0EGnwx5ktukIwOH
TnOpbRhBNYSh3ExqNNzZpzwvAbU4+ebOw3Psn6y/HgZBrljIoHG/BR2mxmk2sfe1fNffqcPeIZAr
UNnpNz4ZKPD3qrAysziTJztmoGef0rcikjFEnBl2KwWBaXZoWMS4rXC28mK2PJYmObmcmbZlPfLr
V3fIEcesl2K88JRFGlug84BpnX3ZnlUcXA/zZQhj/TiRvndl73sam4i1DrAjbQ6D62j5stlh+7M9
0I6KVoOLlydrh6mth4qsX7ofzdZMNQuC3PdmyqvP9efveLJXk+GZEHE3TxQrYR04UbJChP3f87jf
7oZaiJkH71CtF//BlSuTdaVSBug2pkuIRr3Qg3wqwGP4x559woLX9GmlEoTjPnfeXBM3y/n3HgwT
YBa3y5ZyKelrnN900gI7CzRsZXqDBLWhkpkEwKN3GEF7ibqs5bpHgTlGCj/FW8OS8gH8BDML16MN
1U2snczQKp8AHbaW2R+pMLrwYuZINvG8Lae+YWiNWhvlTu8tmFm5GbHyZwLZKNGPpa58Zg2BRFaG
DMko0QtXY9sZS6b6vwxcnHKBPCsaZrBiNlubambWbFKatkJDQIw96ArkdRSoXOcLHXe4e22lfPl4
Kyw6KGz5vrwb/wk5SyLBrHlkI5wRO4NuqNFC6OqvzTc+4mHU3isn8NqFrTXLHprd4cguCWXB1qIb
lf1Q0KMCEnWvcl9/6XffFuF/4l/VaFlp503F4SF+UK6LsK9z7AMWjONkTlMzLQL/uaOvPQQm3D0l
Z+Ozejz4oFKicgWbvaZ7S656TUuQRMSxBCxtsBM9PdmPpUNFOHwzDTW22fCZADTY1siUWDqrByd4
+7BX0d6sOzzreiu7ojldkRL4t2zHfne+ySzn1njNMEFQ+pNFiMcG24OL2N9o107StC9rRJWlINIj
R9obWMQIpr5bdOm4RjHhO3X0eY+GwZBm8ZqsuDddT5CAwO/NX3sb7NHBLLjzry1V6Wm/484hwaeo
aXmpliXK7oLaaJOD18gCBkNqgVPPB7hquhWyVjcDxpZBB29gr9bg0xVbWZG/aKGwo4nUpijZ1n+G
VWIDQ71O0VOVbMy0RiHd72pUi4mQh/UwczIGEOX6JDQYrUcMtEKib5KszZCGXa+s1N9jik1yhFUG
Kpq8g8JIyPIS5TJmzS/gMlv6YrpPZiFCcJ/VxA7dEPzeX68nMEeOO2LxJ5e2Td7O1tCDKXFySRER
HCCKNF3emevgZPRVkcniPErr8TNVJngqXcH6OO+LDKGM7A9VV6QgwfhV5MbpTXgZl0eQkP5CvXk+
A0QUHyc64+dezcDsS6TegUscD2BKImBsvMOYB43/iitXSc9ZBaK7ClzoGjVZFwU5mOTJ64iuQRK2
DVE5drtl9SGh5/CcJAys2tpRJCWnAqcKKIvnUNp4smY+JBSzAnrp+hm4eC41P37cabktkCqfo1V5
u1qTyrFG3Sp6YJew1OAr/IzUOHrznRQMyz/+D0eh77gJMbj4m4FHELSVzskaVw3AnPi72OHNcjOF
i17MfMZaFTphHvO2BxP+OLTAg9FBDvpzlCyp/UcPPd81o/4vOupXNhLzy2Uc6We1fbBM8YwH1W5a
rWwp6jj/HaFZy9zWVYZt1gjcoqSiQftYTaVEWnVoJXIjfUbf1RvldtVQJQXxXSzzKnNF7SN2ZsQY
3LOwRAqcPwaNICYxmSjdCEQ1cUyKJIGZsg3VVJoMeHO4DliVCNy8EPAawzMRmlKMjcHdQhXpP1QP
a4wvI5i6hUKHLzMgrplSVNt5e116dFkSW4hbdSZttRu9ikYgB8Az5U5upE8LiMUdQPTZHzvHmxbs
i8cqGMd17Cj8keK2sM3fPo//3oHROhUI23kMgXLqiVazfEeLJeMx8xQ6bBQspF07GbABpAulliuc
U8sg76gMUGFMnYXpte5x/GZGtx3U8tyNzZ4NcdKMofi0mzkQjgoUeIbYkabBeYhCVBNqCnx/3E2W
8h7leZp4zLjcLjY4VuhHtaT1Bk1z/mcgND49aoyO2NkJYM2RalP8LRe33jfF80l91unMYMZARB2B
5g4KXHy9ovr7orSzOihhjuyFVgiNvLpKgeRfQ9f5vrK6IryPfOWZUcRqYqYj+fNE5NPEoPpQz0Su
y/HOBTDRpFdfdMjzDpeWoAHrzrRuIV+JKlliH3HT+VsrK60h6lpLKhJ7+vIr+G6DDNynbNB59Zy9
2IjneCPLgJgDzmvDtLgZYg9nRnTGSbUtgLZXR25H2vSOSuGTNmY478P6iHn7LpotHkLhTSkLImvc
iYAltnvo3MbvyLM/rs77/Wsmm/Fk7cBONKMXYP42jkPgwYpsO/o4Vy4qsjSnWWGkOIpNWK4eVKb1
gE8lwDpqnRxXQ8QdJgRz8sVUKtOmFe5DLh2fz+sOeFiEqvdvwh3PTMvP/q/sesVnXE8ny4vkIP8e
qr41phP8BAkScWwMQQR81ZtwEPGAPLdpqMCSeq9FHHmXJnN9FqBcd+mR0Ux56VREsimidt85anor
NUk6xybMbjI21UZcaBXpm6RZwBvMEkqvgN+4Bysm3B2wAD2jSMYr0vMNaP4j8W/nP8W7psoMrbmj
3lQLFyyqqutivbuQ0+/hjnULRNWtovPM0b8L/Jqlg87s09v6NdbqRPn8AMmbgG7tX/1WlLhtW60R
ub2HuoCe7vnDIILYYp8EeCGw9RQ3OAjeBRQsqUeU0U8RFS5cukBC6VdsOxqQwICYwCRpVkbE7W9y
NY1x4QfGM7oechqaOdVt5s7wv0i4yOZDZsTSrexFn8VzSd734yNJnfaKHVCk0m7KC39eEM1h2eYz
cMGmj8oM6wJDLlE+bxZ9wvnRDJ67L1iO75Kj8RPJOMWngEysaBEfzaqvC8RwlyVJIStdO62MvWwY
QRMkUG5Sa2QJDeRB94pb/pGObfw6vu9DlVAyB8R69WP2bsIWZr7zq4zNm73b0sKTI3T/GhCOFZRv
KdjPDXnVs31wUNPP2nvHdntzDm+cgKIRy1r/iwyBLlKF/f4PSrTcv5+/NQzprN1PZG0K+Vraq6bl
ll7lfMNrGRSN07EsAOQw4F4I3DptFfNwWmcAiBU/dVZ9WpWXh+r13Hpee3gbfPb14Cws8Nxc9AD5
OGSG+0SfwsVAk43EfkU0E9RtvZRXhEP/+oLXZdtzsPU6N53ftPauptuEng2v1jFatiW97xF489dl
SZphrFTAz2svds+4UZKVaYy4xKwFk/Pz0Rb4Gft0RDposuo4Glpx+o8NnEiMVU5bKAvh/eXNq4hl
2U7590CJ9MeqpceicvPPSEaBzv5VcdsoBduy1/avrQJYQHkda4J52n7DxQ0jdqQDsYJZQr2NwF1B
sJcanXEU/0zL+n64WFVcJCY+dPDpKqF8GXMaRPBFMMeeajr4tWiO2oIv05QUfk1aO3EEceMt4LKs
QKNSfs3DVajfchwN+G47PfJgYgy3b4O+XGyG3fgf+olEMSCFIWsRSgKL+hZX/d9U2HL1q5XJRBRV
P8FsL07MGhzGGV6fdbOiIO7BiezXCvarMiAxuyxUQne+LPPdXlhOZPEjJ7Ew2WsrfU6/B7zll5rr
M+dm08AgSLSRjSdn4G8wRc/JJW/9AkKfRZEK2O3SBSY6/fvrd7sg7T3M2qEkmjII2AY2ICc96g9H
ar8b9w4NftChEc5URgM1pV17rfRe8m5/88PkQ7mfzOgRoXe0BlcL05w8ec+x7GLodqM/cexw0vfe
7xYNUbmV1Y/pG2fBPVTyZLCAHGltnxYYYXyzlv7WygmbzgaL0X+ZcwxQkkrLSbEgdOtFRXqvUshK
+cA/DIFVjQkVdCGE0qxq0cz33iwriBe2naF1UFok0eYGANLUkcee9m/2brCwO4U+cB/pA6pKyP6z
GT/OjAYu6jtEr/MnGyQoO1FkH7mnUm4AHW1C3/q9fRFcfFD1OCZoxas+unBx0eUHaMH10koAzIwY
z11RGzVd7PaOZtPfaDehPKV2scoFGJnkHPGGPB7cs/XhOgaKmZUWMeQiskK6Zu7n//ickZY+cY+J
iM+pkKtSGqNq3LIFhC2iXmfV9UErEgV2KsX8GWY9LSJ/pYezyo1YWC3xon/enDRxfbBDz4TpGa1z
Zk5qSWSiz1BLPHQWnEVdtFQ8t+C9nT9PNK5MokD1i9yJSNWatluyY1p/SYfwsHyCZoJBLzD1AUEm
8MbmvWIOIOIZHkHiKmuHHoA1FvYLvS3nxlmXlCxBbc39KdxK0O9BlXplawryyJA2YRyhtTgNsh1K
8g1dB8eZNwLX7vQtJWZBKXFVgEs0S16jooPnFhD4gKt5fb8wOImwTdVwI+3wEtrekoEnVva+TEge
WnNPNQe+8rjuqfp8SV+Q3jRfAk+SdhdaUFWbFLCBZWGNByxsZSm3T+DyysyIUAa449n3ASksy5J1
AElUytr46pU2NpgWuCKcuKIji2jeLe690XlUf6AZmGJVJGH3yj1a2hDbiHmSV7eDOs1QyhXCDV/L
MgXMe9y0WKIi37hFbbf0SRI67XwYzhY6dSAI3rQRa9U1+5HMAIKrDXbdXnaMIVZTn6pid74NuJu3
eP+jNNGjS54OHIV8k22k1jqzIsUzG4MNK0qCQy2CByWYS8zJn1HYX5pmjgO/t/n3tE90ocT8B7N8
VFKy28ts23OIL0MZSfNQ6BDBgVvwflb+x68Na/MGOVEf0CIekCE8tVLQYO3CGBpMQ5WyPODW1Am7
M2z/flWU6sqBoCOLz3w8bGsY3YaZA5iF33fAOdS2NCu9mkKAug0G8a9L5LJWiKKSEnOYKoZwoqzV
G0BbGlmt0g4Run4PB7jGGKUMAnvYrTod9+tH+zhFFkbwCqAPdRuiZAjbUF7hyW9mgVpA4Csy6lvO
jde7/SwvcR+uPp6JKI3SAHP+APbbroKg2CLY1OyPmjWoVGUsc3OlP7H/MeM9s26tmTY4gvfbuhjo
EF+QS7LJNbLN54e6yx3euVHpi6/Q2oZh2Cd1uzoQffZLMKsUrqAHsDYhoI8zKF2Xsk4sucrJrx7+
iinrOv3a8a/o7qKHfWA1WAgu+XIwgoypJ8d/K2roDSWYoBIi9qsUhj+W4/V8cKW8bjUow7Kc1JWM
5SJPC8SWjNmIxcsliCMw1eiCnTriBOURG5eA3vphi9+ST7Jc8fVeylSm6wWrxz6WWYO9D5QpDdsP
bNyxQbD0eI/6pmlSx3Y2j6wdw1PJEFadSNwccJoAfHeT73LyfxuOrbDAmDTfnMWd7Wc6tkDfTYup
/y2WAwvWtH7vlAQItupjMnYA8KKWjkE3HHCgikgho+wXvTGZWXoPS+xmh51CjIkE11WZLUvXpJLn
064EyGIkgPpPIH2f+6L4cdkG7ZSkG32Ng/Kvrh+p/5faEM2CnHK5cZ3kz5zzK5/c+RU8+fQOcLET
Tf8qqawcf+zf6f9GlgddZ5pOs6/6cOopQ+NlicAoj6oYbFL5uSA+jmeqyrzZFdqhzAmkWGW7K5Vc
s741EVY9rZHdNz6LuPCovGN0KSHnRy7B55K/XH4ruTao4a8ieQfysS1XLSkj439CJP7GA5uNbMBL
teTzHJbLQ1KQ3Sf0ILgg1FpVZA+yCbScVl1KV657uYal9ULOb2Uhz8cTAxlKxC4he3LENnrfT5Cj
VMOmBWkEq0ug383SxBvv7F1oNtP66dUMjeCizKqQGTHCTOxsYMEMT/9Fd3GsLsjumA2w75K4qwwQ
6dk0OGZ7hKgq6OnxzBn9mdZWE8+olQCcpmAughI0Nd1mSGujOeEdwmjRfOH3M5WNzcI/qTE/O4TA
BcEHnE4FQffvXH8Do2Q/vrsf8TFPbvefg0q+0rc3b04bGkdYH/LrBP4CN/BGAWZgdbQW9zjG4MNs
GrSMGH8+v8myHmAB23alDxqacQRWx0AnjQULoWTgJeSVFzIlz/57JHwkEj0yAzyUaQxqLuLxTseE
Co6+xs40A/KxZ/0xSlFlVpidZOtM63L/+yeLqSvwVd3xhWcDnhWE50UbMihl4HWoLuMCjlVGD2kf
XaePrL2qUfosXrJVYR628Cie9lQ/8ZZI52dVhonhWBcV4S9v+yAm0kjuMJqPRMOqUbzNtlFCFitU
hOPfb5slE14mrDsRZUL0wVNJ3dir5FfWKjPLpRucY5a1mQVaMUgFEes96acr2Pwwn382FdLscK94
poBFvPSDNiCeKDRoASjEbkH2oMC3MiV97erT5JVYHiZ52rODK7IlONxE3GJ2TMvXxLqlR5Ehu6zF
FBpbuT14KDCA3Phucb30X9AsiMh1Ck2CsJFM0ZuK9JPW9quepGFGRPH53UocU84MWk+y/VKXAL2G
3Nz+I9DQEU7ZhPVQSbLcFXxZmraX6tAO4yl5YVBRayk2Q7kyOeorgqKZoQPHXd+GhUke1Hu73BhA
PTw82U/zyU/RAG2dsBy7tHoYaEl6zfNzZUv1ma5qEoyn9pXVvYsl0Av+o41EQY6ukT767yBd/s88
8/scltXsWw5wAqiyjsSJt61YZowok8qGzs3c11WJtx2R9B+Tkz9acisKEBgWJLiF2DxkvSnAeP/A
LG/1FcdLb975G88uiKM8SRqV1bRj131YgY9g4JHgLjKdtfwoeSKhGgFcrfmr+EhQFBPafkdkpEak
4xOUPMh/O4eojaYMLZCEg02r5DDNx71SHfbFPQDTITW+z20x2IRiUFR53U0FoMN+LDM3pc2r1Qay
/I4JitP7fDr8pR+0mk+TqCjpkn0DCyOOVAcc0OMF7ytzvLbZdopW2zPwfl/yRQh2jWA/swoGhah5
pyYP9NuBLSUbiDrm9sOVWz6jbFP3o8//cZVFn2LwaDjqX3DJejNHGWGOGhgi2zKVlT9rZf5/Wf1H
jHR6mMyYCq0kAKw3c8GDDJtjHLupEV88hbIPjqP+blrWQLQxs7jdO8QPbxMsCJEGtHnN0KtDFKqA
DXWixzzcMkAFr4GvtVK/QZe2mdMRQ9D+SNm1ISIhYWpu9X4imMrpm7ZTiv5FuFJ22rj6A10y58hM
TVX/yfWNbhkV4lnGbN/OlEQLrNJ4vQeImIjGmSupEe+w2MX8kUOlnqKcC8mpBkG+irTd8L1oQ9Zo
lZI6/9G/9+MpEnvf96cVwKOhxRDcjWY6DaSUR8gdVCWlZUiwbVL/pVvAjTxQguLJ1QBBmp/ILAST
lqIeVZmhs9hMqOmYnhXsigGqMiKw5KRXEWhQsBFYzsc+nVk/CUlKN/W85i9o7c2Kw3mUtUeUwQe9
yDwheB8FYtaf1/w5gdO4GVgq1dGWGSeRJdd00Om6ieSle8C4Wy4kcvZzrHGNmS6wFVQtowozg4Il
PQuwcAFZRNWuTYUA4ni97P8Awz7ApP5bQ2vLHQg2Az+irBMU4cueiWycQY1mxByDZebpEhvke8u1
qfC+sUs76gyVsTblk9hpItqjk/KN+hQE827xJl8tOxZUpmL0fYkbUcz0SJCP8OHlWDK3l2fE6Q8C
SKzO2u4blNcauUYVmiyvpkICKgPDKWaKW+5q1Sw5MqUYHqYtDmep2Vl67ttxNo25elAR9KlOx0Bc
S8L9EP7Vc6a1GurBchQhmB+GqB6iO7wXNHFF7ClHU5prTuO8Q1vAyRpINTHS0gyY+gCcpnmU3+dr
ZR/SBnMkRpdo6tvNdA8nWe4VQCBLc5FtzJPg585pPkcb0Vcq3BQjuCwIzjBL247xueJioPHJsJXT
TPbbZ6ghS3W8mtqXOEhVoB/HuD16VcW39O3yAqGx3ZBF2YhZHZ0ZRGPfuv2UoKiHsTnCvDFTkfIS
2uTqCYhsGrGO6Gpv0CI3Ykk5hQ0BI13J2CNgg5zBsnFSrsJagGlAVsy5GdR1GIc+MZAQ9Q04DGZs
sUlC1oO/MKszAy2saUPAn9EHX9D4u1IOrhP4rATDwPBRyY9H8x8cfLVE8355OsLZ7WnmPob/vWIc
+iUTqyU5YjdAAG5mXl2yRIfDsYw+awo7Gq7dd+GFvFg1htFvHoVq0WPXCAyojPdpHCyYMYfDZ7Ta
mvV8ESQL9EuQFp1nUjwb6xyqOm7VbCHk+kiOK/p9uBOcRJPP1OnkW8Px2UpyuSQritTNrOdeQdgz
u4BUNnkXraoqZ7H/7akUH8DwhayYzR0/Dx/e8FrzIGS6O6L4LHMUtBnbg3Ey7Ncqssp3kBkSNwJ5
yuBB9kFBXA0bBLEIklhQhPwG9QKO99DE8FBiTicbMDQ6DKQNc4gonHzxpuo/dySyvSL4oM8m0J7H
IPzgIt0A3tpyX3rp1RSQXSnt8UIlaxx40kYS4DD8se5wFR7eGPcZdAzlFFNvga6FZB9UBuRDKpuE
eFYZ9Ux/n46alQ9ctPg7alxbSlZMZIZIhd6hY/ANIgDNRJdzehnZXRAr6N5LpAdjt3x1a1fNiTxr
HyE//eaGZtIBQwlmXBUvnAy9W4qDlYvVgqjHsK5mC5OCnbhZ7waZkpVK+4CZzPUEMPd84PtlEkod
USkHDSb5H3CIOwK7XGeCrXtWYVW4nrLof/6O/kPlmWbeljogdtQyM6l2QIMRFKleSZXYhB5Tsz8a
OUaqpnjwX4HvYzd/Ksx0JAXA60m9k/k98U+2CTiA4zoLBJfF2gmlON0GQ284dbOtBN+zyqgb7sLQ
wUUdgKk4sGBkihkA2DyfGYLyMQluhKj706MExzxVFcJkJTeRjnLcYx+HtrBUvMaJ+y+FDli7Arit
5S3oUuwXYMD3k6eF73iGHLK9UeyUeVsXehvoilLmAp4SU9bWF/JHpa4P91IOkj7SphpKrnkTGQ9O
L0ZGuCZpGi897bJHyiIp1yp/BcCvblnAw1gFqe0oDFP0BaRSV/vMxwJo9OK4JHvWrdXS7+o/MSSo
pRGTESXlvE6TgA9tUEJCGDCWVJRH1bGaW0DKv9JP84N1BqRpeXDJ3064R0ZW921mYouY1C7hUpYV
T/0VsckmoZDFroTKaDAD+pgSTwlc0yjyDGuPyQSp1xPXUDxJQxrq0jPJHHUenoOeldJNBcd11PZc
tH/e5rfRZJbW+0RB6KcfZAqkA9BKlxPg21ccfWgUC3YS00zdxxlbAdgIDWUBOrToGvMZuN2rOuB5
2/tzGUGYz4Td3gW7xmfLZoDUiZSGr7l2EnDm3GAw9itl9eeBrMDVrIuI/C6ju9MV2jSqwXMSwfVs
+N+qFge3JSh254OiFwX1pYi9StSWSwI6YpF3knB7J9yfsxICJ6xlfK61Gn/Ji7rHXFbD1rI3PYIl
KsodrduRdDrijAyCad7MvOc+D8OLvTH5AewDr4G0GpPPu+r7UUPbJwf6cdxkA7U8g7WFrWtYLuOD
KbxidqBqwCNbucMkZADCYzrunWuns43d8+elFF1nnOYJRiBXjv6giRFbVHdYj9tQoLX+04Bvbs2i
DIh2txs0+LvuHiLJI//7anuoEa3RNsLQNxIWAm0xoWFcWb9DpPiUxkoRIn10KZ4bbH2oHwL75CP7
XuddCRJksA6lgvtPC9eTy5TL1D7a6dAf3/Wo8VI6J0zoQNDbWmu38/tt5/22cOR0x5TBBtvz5Zoq
yavNNsTZEdTr3pCbFignjxixya6PL/qDxJAKpgytHRF8XpIgQTTJPhy7dv8rS78JI2kazuajSOsd
yMnwYzDAWoXX29sktNDUVFZh8skFty4in0SPOvWAnxvwCsXr6f2RuMK1sQFaymAH6LVqQtodzYnt
UGowUzoQ4MN2KYz5OSG9+n6ZWLDadhalzQHvlUC6P58iA+NZOCb/gQxn566gmPe1z3wEF7uqFuy4
IClrjn9Jox1AFXGsr8/kPpn0+pQ0cL7vXt/CQyxH9x6jD1VHxLfctCrlh5NZu2ZFwm9i46Ti6OWH
eHRusg3ELzZsblQoompuFRBc7zpkvR1csorOfzDtLN0a64mJf0zkmgyoKloXE38Ca0wAfZKldUlS
lssRKoSKS7Lph6qo5MMuufdLY+UQ4GG42MawNC6STEvy1iaKCiaMVwglObqpoahQ7bgULaunC76H
inNN8CWOVxJTAJ12IEEVjwzXURtmbNSgHqnow3Bb6gAJTtbBE1Ng3+lTM9mOM3f2s3G0V/cIo5kf
jrSBJ92SNHMCsnVUvx1kL9L1s+TXh7synHBmmZg1SJ6XHp+eRdwXgz44zpfeKmgQB8GxgVE07Q0y
kR6Degsev1ESMa75ye+c2LjUcNSCKh5Q9tP5Rrwny41EVUfzBWs4eSndPXkQQ/Cp6zlS9ncg4/30
c1TYU6Z6aibiWdQpAaT7FC0kSpkPX6+r2/d6l1lL1YeXea0eVOABSBShwmtW2ikz3W82bwl4WZnV
Hk2ggaZXAXJZdtYr+Hwk31Q+WMrOv2Mnqh5bckeK8mtqsve+sPsTxP1y0QYSnx2pBcdYJCl+vkCW
5Q3Koap1SMtO8nVeXEToAOVON6kLYLY/E10zKZY+GAo9+8z8yPousEduss/5Ye+8M7ZB2TiTpN52
1iBR436Gu3oQuDuLjC+uNlZFf6Gjw94mHWK2to7qnZAlhfQS8lRmN9ARxlwW7HEFv2CObrdi0KRs
WVg+y5XbuGlFPT9/rYHoW1wNuNVywzk9OZMrIROWRLGZhCnsJ35sORF63wNWJsXePJr+6bUs7NlE
Ker8XPrWErEWrSAr7wqNj8Sl2V0VeJjteuKZbiZd7Ri82W9TFE4vgtRtvNn2vbzQHsJO/NdZHy3N
HJfrYYwgAg2zaD/1yfDGP+yzq4moV7FkzFlnjaQyxQeoCq3geeGoa5u4ohehs+omtyycneux1u9j
+KdHDZCgEzhuVFlnRYol4c9PSu8tuj+pBXjARF9A+ZF5o8k/LXWwys5Bvkd3kUiwZN0GeQcD1cSz
AIeh0aIj6vfm+ejFt8JfoP49YGLJF5k0IkYkX0ip1WTB9P2mHtyh5N0WzvQcIN9/x+Hq89GAcUJn
GixKdjOrqMEU8KD+9iKdR/HIAgTiPjddOlz9JdVX6x1Rcd7Trq19Vs2AsxN5DtWg2qY403K4UR1Q
dGEhUhY9W4zS85NKhXEf58GKmL3Cho23GBGgMaNRyQAmFxFVKwmPX+lxRKiw4Ev7icwC8VRSIdMH
eo93eeM+zMz4Pj6tbUOngTwGHYfcE4PsNxI/8Uaf7AU5OqgGATqxSYC1UaHoIdFmNtQPY5Oc4s+r
SAMq4D/U1poEAX/DxPx7KQ2tX3Qb28hAhV1XYKumO376VMmVNEQELA4UwXuIIzBXVNZnezaaiTSP
+YZOmEN1FTsqtbE+avt6omLr5PFF6ajVulNspKuk5YXMdkm5OmP3Rw/Y6dB0yNkIaol/VKNC7+Jo
rApIMh1qiccWzkpDWuGAk3Ero0x58p8FY3AQf5m66WQNjw6Mif2DOr4pQnH6wzIch/bzZW2hIBa9
tcDk3FQwIq+xFp4scvzI925xAIHbtZH1TK0M3figXKYxoXBGZoNPRLr8CEL+9T6PhbSxMAh2CZTX
Z+fJEn83cYyUWDR8JEEo/omP7DzVnHbYhbkJBsC2jeNgZ68nBdgbwHViSxKXCpNnsfSy2Kl0laOL
+yu3tFvZxElbnrDKCENxy1ekgFbOUWuVcu+d9yEOAep6ewaVi9y/gWlLqnLjRvjTy9DCuUJRupuL
oN5/mB5fsaWByMfC3UNU7ICUWVFFJg5Psiw2rqt8rDjeQdrsbD7Tyq8xq1OvzF4luZvV9SaOvhB1
HiHUKyg5oaB8yBREvZMYIj1K60C5bmNnL5G7tt/7CaIHSK5n/z58HlQtH4m0Mz/YLMTdAISjhdGo
xBPgWed42xuoHRDicwqCXfmF/HECJvJxE+ntiazBCcg64A7MeYV9rG1VxepVWqU9odyXGGN73MRq
cb7b7sKm5rRwNAs3BMC6jxsuz0/RgT7booRm8TbAixLGORpU+YsrsNCcVWXMuv3IP+IkOAq8gmwT
ucfVH3CDRhcMrgM9Q1GqQ2hEajmsVpzbALx76hI6BoeNxCmI0DShJB4uRg7bBIyrHFtIS/gHn1st
psSdimNjcruwNcWsrRxNObWnVDdT3wiP5EOg9eDeYOBUh21jzbc2INMfoUS3mRs1hrBVF5lmN1xA
0Wwx2Rpikm8Cv8Man3MGqCefVwKUPEyrJEXasN0DxfLdirBvyagCz2jFNWUQL0j7ySrMlAQWCUFD
ZgIQGX1Lv79JeyGBGRVwYUpviqtYFCLRO7Bkln6MHkx41I3GZIDJ9AFlyxFGRlcMJkd4asl/S3l3
DvqfhRDWyOQobWkMf5HejCXbeYSEsmXTD4VRJUleyAMpLz+iaOBqtLIHyi387kb77LxTiE+YTcpk
HdgIMSLqEFpbPQj7IQu2psZcfv/GY2iZsYOHRTP74I5d7uZ4rnAvHi3Pfj+CabGqsbVaHssl8TOd
R54PBILybQ0D04fyer/6bB/8MNSOnNEKynOCf4mcTqas7pQ35mDzhSHFLqWXbWkpUKY7py5Wrnrd
VGJRZMLsOPvgQfcI97Y1LSD20RUz/peGuSanEB/IFTWfMMuKgE3cIsNbvGAakq5uINUbC8aVEhgr
84GI3hpmwTlFYJcXb985R7PXlYyPFtU7u+Jl0zn8Ubt932DaSCB998nVDKK+hy8Q9nNJ193ItOQJ
6d4hcfkUUEH23+Lud8I6Jz3Xq54KInAN3pwKmGmo2iCmSwPyAadvm8WRPRnQAr77KiPtkCiikgZu
tRQx2KGEMdMU/RZ1wvyNzQbZsPRUBXW8gH+hdYj3IxtI0Y5iP5TQjterJ8ns/NNNmXmJT2dyto+s
koZqgTh0d1+pykdR5xe+a5nkJaDcNQ0fHdlvL2tprFJ0mN7GtSIxzyRjKlAAIjGxsKWm9RVE11nH
zX5IuCYSIphRMMFkzsUQZoaUMEP57yqdcjZzUfUg1xITRhbdoFlsblVFCi+ha5rj4qhDGz34cAJj
1pd5As415C8qdymgsAdF9Clt/oT0jCvxbvsFaqNbwAG6feevbO7EJCZndywvw1D55ckCADXPPYAZ
Cb077Gq8rd/ZVAvRdf81PhIfI7rOVG7OTv6pQ5Rjtw6y0Hf1b/Kid74KiSd5hnhd8trCcr3CBNUm
0TxhlYSZhnXpeiXJMyInCbKgLlB7piIVpwEDcvHmAMeDjwdHK+AuGnfC+fdjhKFpzBBXCcIRFXCN
ooEw9vdX6hfhrGtzhQJ9To4562d+PlEfhzhQeRf1yQkNOgXTp06ULF4ynZJ6PDVvv6EHRTp8P526
V1WI6Wn+vB6hVg0YlFPNVX4t9db/euPBjMMmN3/jj2w+gg/v/V5JyP6JuapzYJhVGO27PUvBjXgj
b7U79un2l+oXhZF9w6L97dh1TQT2+LHOAYo1rYZdoYMccJdgT2hxQqfn17+xYPB/XLmFyQq9MW7a
LV7UoLJDvAACxXJ4rQmOM7XXxeCk6uIW+sXWjHGI77pNRxBmaDpO+jFp+CLNE1RU789BBiFedbTu
1mVggfPkvWTnYepFdnbwUDx0Z2UuD0M3EAHB4Y7/SlVDiNSdO6ezNE0yuuRnXi/2bPZfvjHeVWlI
rY5eLY9lMUcPSUldwGFONC6EIjE4c+C7iVYWiP6QgxVH0i04/bvqChZylam2xHWXtqweugSLarlQ
cZmpS8xh6YNIWWyU+ixmiR8hi9KMktZxGqz+Kkbfl5M6foGEOmIBBZGjM3TpgtWKVhUPTGbDLPPQ
/mDGRIDvzpo2I5lA4rkk/rqeDiBnF1vrJuxCn8sRt16df/kGfMu6hLEmjTxJABZb4jijJmpl1dYa
PKLhiXF5T/bTwbsx7d1xee0lVRYPvRrwSYYgyuQGpLEq/UjCtlE8/Ifd4emwI9DFxcxRmAlMJSgE
QtjgaB7rH01bk92l/kn27DnEOICWB0RVrw6GiCVGJ8d4Vl4aOrxZHKg/bdcIIvj91ftMi49C9PMK
L7ucd3AazExQqfMACi3bk2btLqT/Id8FcbWUWUHdHeM+sW4VCzFRGqrP+cAjUZ/hRVENLZHbDy42
Lz5P9KMdR9gsJzMd/5AjcsHHMZhjS/Mbrpg5dXlHNNC1Zp1L/PrTOASHp99EF7YtXobp2wwdnEki
zdNqJ7wSvbZrfyNwLRiKhrM7e4gywWPNGCyvW49ZAnF2QAyxoB29mejjZqFfeycGkJVnmFcDs3qH
eHu/AIa2SABYQZU37q2+VyV5g40acb4ztmIu6eDBt+aqJWhpLZrvf4VC5p86N/kN1DYaQB+0uAKH
rJwSB0pH3KhO8B8p/3lr420iIVqoG08umTazodWoSA6rVY5zMNAwT6Dg/xmr+KqkZSvBdhxC+Mr6
J7wK0GrbBWltjzwWBq3eZjPL6ZSYL27O57xr0sPZHotplxTaMEN3n0desU9hJQGL7/OX9rq50Iy/
IF+ICai7UXaNC0SRVNt6e2By9rvK5y4VHbukRV5pDH+XonuesZa/n6KSJtdAmIJ+zL3dzBxvp62B
RIxQ/BYCNleXgxfLEa8wRgYRhK/Y1P16FZO70ABdBhyFGXJUpOB4nk8n7AAnG3+r8oraftrfeBcG
DPYO2ZE07YcLkpVFWzeHRI+eQE/K0j1um4ucU+tncXD/QfitsR6f5nhkQjG+9jnjF2z2uKcPQW9J
L8WEfwosoKVVQBQcs/e3iis98r9Z6H8sesXG48qs3q9J2dyAZ7QP6C5gvOWLbD1CERxMfL2aJhmH
6JMlD8olLgTqCHLTV8PKU7upjk61o1VNzGDK9sQkodXY6E8wwcHBnAWGvT9dWf+cSfluKbuCRzLJ
z38wC2CNqmMfzRv4uKxLHcKoSqXxrUbYpMoNy7kEx5XhcjGDIyJvDGvtZ0n+tanWieJzC3ZnJcZH
jXHvaD+GStJiRiCcf43+B1BifUZKLa612wUb+n8hSiIZxoCFpN4tBzb7lHiN3An6s8YttRvJq1Xu
r4RN4c0+LKPlGVhLqjdLaoXGWYQhl6+aYAwOox4Y2+QZllOn5qaK9LzY1dOmu++CYHmvVklM4rrn
Sl3XrcQYY/hW2C76M/bN+XXw9pYJIWp2b0lvXDwkKx0wk8XKJYIdSGnCuVC4fzRpJI0+YjxAdLoB
PvjspKAJUFtiGcKM1OMcNBgiRcW4FUsfWQGogxLkaeHuLL6dFnXwEpAgMYTJ6h5CXe8mhDRmDri1
LBfiCkxTOcGYrOyPn86uOBuYFY5PW466w722QeP4rbd3W87Jnh6rpY5pfECTBMbph6P+/xX06jb9
J2p0l3Xf7OlZCqi7sq3X7xE9lw1109OlzqR431rmJK0R9NalrXOdL49JSd2/wT6S3QIMcUPOHovf
MO8jwYpgRuoZ83eIDB5uSBrlfqHieynxK3Tu8uNoVLNAxtjL5R3dhMKmZCKee+gRIa3+Z+bssUco
ZarucX+9+IYxw8Dr83tOVtr0Qd/J637hhwalkwC4DGke4mNbqDoD2eiZdDnKlRvfR9cyPm/LxIby
oeoPSAinKKaN5FBlwxXhfy67v6TJAVGUXedipxPNKqXTGJFeBKU0IWhlD7fjJ7tlsRrIFGo7ZGDE
FwLQM4Kqwd1wu4izz8u5x4GFMZmqpIGKgqOgH+0aKu1zu4EjN0qf1jQV6vs5vNS7eyvaGSnJhdTS
g9IHHP+TMdaysNjqucXP3UHbTQ05plGF7nCRLRC3h1g+dcrXBS3SkPszuCt2Dv793L1um+mysQXL
VuLrHEWbtLwG2x3yv/5d+IvOFHyD3H2ZGBJQvAQ83Z770slzD41XzL5+K5lgk2wf5DEV4WErLvgB
7hbmhX+8TxlbbsSwsf5Ic6LceOXTG+sEN0uZ9v+V2T3HdbQUtmpAZH3tFKq/GwGz4qpRTapvYjx1
a7XUqeD0N6B0LghniBRrtzQuBNdQpIPAusdA7ExdLhzprUpQ6v9sAY3ahrQPlXmjnWhC5ddRnE/Y
wBOtZLbHqeaZ/MH2uZ5P1SFqKhrE6khX3bQiA38Xse4akqB9FAyeloE0j0TxvxIBjzBFAuNBX968
JRWDbKduy7bwmWJE4B7xSszzLCqvmm6IogxBC0cOiOMjerSfYTeEOSKE9JuVvkPVLcK3Zf8qvwW2
i8kF7jFhPPy35WQ8HJhR/5o1bbxu/XWLEMryukzpiQIIoieancnVqQK8pLhAvrIwZgTZVihRnZvZ
RJxrFpHJj8iIhqhJU/iqdULaCvEJynLxjkQQSJnXFq2pz+cb/eohpK8J+y7RuqX20GVoZXwtnakv
efh9lyNefvGfP3E22YXvlHa/60G4/rYkX4nGFGlzgrNglb8WZVdYf9YcQ9zOr3jFQti8n8dhuqfL
1+W3X6stJwDsDme1UPF1vKzWzaQLSD+vD1hztwalLcy0u6hZEtNv99SoYAU13Q6vSmuM+T3nV13t
uguIx9OQLoRZuLr6q/ZbrpRGjOzinUOAhGnE8hO4MGrNAa11UHkkX6H6u7wHOoD0ZcX1Q0RKv2dV
wC8HWnPBoxrYc1SX0pgG6lb2V+j2ZrW+B29BfH4g8fDanAm66Ha5x7XEQPF2dcZwJQcI55cRuWY4
g/1MM7Z7nUM5vokM5Ca0bpXWB/3oFKMbOI6gkW0MELhNiHDLjH05vp4lXmhULzs8KpqP4/sC/zn+
2nssQ3j9T7koW3fDbBERGd8S6iVKaKvz2XqXEFpyBHJAbwGoL5lFm68EoN8O7I2TiL3j1rFg8eGz
dXge2u8lJk4sMeAF3PEXNLzNHzEB6QLuw+oOSFvqaWP7KtDlsyPKLh5SnvnQ1yZcSZGyZYkbn+oB
tKuNrTYOIlxltD1zDxGvVOBoziLdxG9gm81Ubsb8eVrfuAcNC30cbCclZGdapIKlp2uBhp3EgrqV
0szoJzuW1Rg1fsXOZLKLjjFIhDtVjScIlUvyDuxgXPhgPRQh5I41QlXBUeK7aRhZkJXe5YRdkfgs
eE5r96mosQlOVan07QVCYgKCPmGrop2r0xZFzN4hXh5Ec/ODr8S+QAmFjea3iXqm7sq89Ig8zfK8
I6roDRKWU0d6pFSLGl3ReZZPsnl83ilWFhG0W6ztd77nXitZeotxfc6lLqKX+Ff6jyV0lGJEwJvk
2jDU0yD0yyEyXoKhM9Nykuo/1Rw9nWur7/so4La3BeiHrnZfFZV38V0+cCLYSV39dOW1XniwAwlf
NtyhGCjXsaqwyLaPKPzX2ivwHwvU3FeoKu3HD1KJonvDLSgUxR6dB+7RL8aF7Ut5XPi3kKV+Y8fB
g9BYFOovvOCZ5+ne5+bk40gD87J8cqlJ6C1Asq/VU2hKwEgTA9HzBoGCzfIHMoCQsbEB8N+2ZS9t
JQRRYFQmw+bu3QH8l5ypivkTfymt0lzwK0Zk2ixXFeTRvFXUUZEvoAFIR1SWVYQDphwNt10oFPBj
d38X0QikRYVIWacoudnrarCbd49MlaU49V/q5DxhW8Zr3kUh/hPK3adpzFQ3DhuX5FuHSoynsxjR
ffrEMcgudczYLFDgxzg9pUZGt1Ufg3zaahfc+UJrkDkg6dFbpIUm9o8i6GMKf3jvPZ62+lfk4oPz
XLLmCzuKXAGkXNq10W1fZX9/Z0Ny2giSCPh4vrEFtMbqoG1gXLjR0Ot+0B/oqGcwkfBdiu0ODxjV
pfmrecYAVV3t6+GsMMYrG+7qxd+7FSoPS04MnFYkrNOy6hdxZQ/SpV0TUWqTX2Pg16HBBVz5hHCx
lLVBjJXnbWXDORk8OCGR8B8OTEhG4ZZlpR5OVpG4NVriF4IdMkTSe0u4H4DbZHwzfUDmziHZNo2I
BEUVWGOV5iVKMBD5hiaDgF3byZwGKHDjyz79e3a0G2MN8GRNjBFibVlzTreR9pvqtj7YlfzR4Sxk
deKjcbI74AcQ31wSiUS1Y043cRbl54jzD/LajaT3s3ncHk0AEXPjiB6EWvndadUXwlvdyoOKhT0D
lj4M0bFLlvnLhioj2fLih5/VRBrP4xF/DI9xb13jh2yNW8l/siHh0eOYLZprQuxANfeTQD7Igt+0
Chqhol5hPpJIQ4O2JZe9vC1A637+d0seCOROwriTAowRPCjWhdtYiJSaP/whGB2rjh3EMsfsQTCc
NBQuZZRw4m4Yfac035Z9prxlJj9kTyZCi0D7Xz0ESWY3DGJL4PTXLme2bP/yTjQxKM58pAjzVPvt
8XtOGXRvIX8XkNdg6rCx+Wel2jUfLEBwYzZX3itzbwBNqnYdYTPxZRsRMrmnRp+fou08Z92AEkcM
hM0s2Gh53G+I4pQMfnOezQJ4UsfqeNGF7fZJCc3pvDeciorwKNkljLbjyLjJ6Q/7/+ibo4KRbiLK
RwE14mCK7Qb00nOmN5qJDEkBF2YIci94D/3CBRnHFHPEbCbobu/rOUOe8IhdeQfY7SqPI2UU1D3B
U2RHa5slaItltgCWv/ebBdrzKGvGGK7Yoh1/bzXLdxOEd0io6lWfKoGCVupmDSFhV3vBwdolXsyF
TeVjqQMkVoLVQ0qJVWqC17VN89sKTp0dENQxXUdFgfjvPkLnXnF1YkBvBxMrejw9/EUiX9OjBw5e
l372/cmQvTB6o+4R0G0v0Srr3JaQ05gH/10Y15+bV64VP2zLL/CzKTx3c3pMAYMQYYYlUefJ2VmS
SWBLstHOkLIuxgG39DurSBFfQvgBM2XWzBppplQwdzKrbBwaU91owYTBaNspo8wnZQCPVQNHQrxT
y5yJg24bLDeKyz3BumeM3rv6FYEyFxFJlGFfZLTds8npXP6wd7KgX3h7vpvjDuPMeRd4oAtWR/+f
O98BAtG+67U78z3t4cvgJmRLG5aY7loaNwzbnwyiJR/i4jVNaO+cqGXmIgF+jFPG6M1kc41yufzM
NdIi0x7TKZN8jwf67VzPrzbUCAuruYmS+bsJNjh65tyka28SZ6S2qvAiNcwdW7Z1jCSpKVmMaTdw
DzspvJqKCaez7L0fmZR/OOBLygGOzKrubXYO9i+fV90vRO4VD+4u4xYqohkXkStzLKwafobq0oVr
rYwmTnVkGc7ol+nwtJ2DZmai5K9Fobt4U9CwTeKnKzsjL9q26prYfsY9uT8peFT087m/fe05qY7L
g0WMN2azzouD1i1pc72LIvl5ce9V2KpnEPb0etY41mkEopKEZzwFva3XtEgHsqS3d/BqxTDu0BlX
xtqgYp9Xax1VPtUhSK8A36lb8AoCzf7p4hdyrSBdglFb+jgCwjw0mx5GYk4ZK4OUTUrWMUdz9LWW
dqiGRGU6dCrSVfTiyHLguiJU3yDLAhx/eU4S0xpznAfNtze2iadyD6EfN5g591jpgPTQ6HQ2l4gr
wtynzfWsyheCTXS4kQnZWq1FSJcQFuBDzioG95I839nY0zFle3uPe+wp/xL3eiq417y+gSKhgZUX
ygbv4Setq4658zggBpflOMw31wFtXXxAX9Z3TWM1T1btRCuvX7zKmGTnAG78ZUtbrJz9UgOhADsN
HTxk8iCiS+iqhQYraUn0P1lY/izTUBd7Dz4gClVPijgqu9kl14dDCDyGoltGirUGwXki+zKKKL+r
8Egi3Rc6GRs8qIVw2TAPhqakd5acoRfGnB4bCWsQFc0FVMY27MwkfuPF3qx7de79NC8soaCPrwUF
aFx1vn4WcQv7Z1fNIONpfMQyERMbRwG8xCEsuwS0yrPvixzPF97O+KrSJJ99p1fufvx7n9+JBFVY
egr1yDBUVpXfll+S1WIyoOLQe5AX63BxG7bMh5kb3lVCAI+q7Zu2riRZiliLS955bv0FrjFMp9XF
TpY+SKDoKibY/PveeZ0cWv4E1aYqOPxk5FXahT+ONSaSzHUNbQSXWhBvCEZxQN++pNbH7uxzI1dI
IRHSaU15ZH8U8vuc8c4LcIIgYxVYRp4MGD5fW+CgAljr6izCY5qfwYg0i7TZiWnAnHEWOBrVBSge
k5u3no16uTRcDvvsdImQmbwIzd82k1nkieCdJQndHvCApvZCtPt8FxB/rMyTNbCZWMpAPrUmVz36
15EhXs1qkzl+/m4TaJ38vrXZ5tdv/AiP49Lzh6pfp4ItER27zbtUAbtEyJEpYIa0FGIHCMllxxyb
ec3roaqhsmlLFprlBGLgv1mvFuoaJilEuwFAsOfE1TKwsCnj7e3RNdioafTHRienX9LNL8nUoG5E
qeg4ImEH4oaidRKlF6XaOSOIag+AD36EGc3J0XchV943r5nMGtJ5f67gScgV334AVMJTc1Yza1eO
fbSkTEAQ1E731u338l4gZTPpKA5aiA1y3Re4iFQr9KKjOeZYOD8d/z/tTpNEKAJNvRUdMZIxG51f
hNJ3C5Ohy/FIjKoNZNfOQTBxlUG5O8ZHWliKGPcH+J6CD9HZuZAvJxzByXHgLasYZqb0q4mm8eET
kx4EdsEAzgy4V8QnedyjiShmebamM3+/BpkjxNsVyUGmqwrg9x1dwWmGpBAaI5u/3aJvnNG3v4t5
jBwkR11iQbkYDshot8RwFnXgBIVqvbt12Kc6zPCD3fNKvd//CFxRNEqeJJXavBgp9CbBaNsiqKER
WMjFbKyCJcrg8NvvIGutgW2nem4jsa8n1qSRfAtjix3cZWt31G9N9z37y1DoGHdbM97P+c3aHV4d
hlCWtFD07uL9JKqe6Kdrhm709E6Y3sC3CukI+n4ctj/mt3zzCaynuEC8NFh0Ni8R9CMLTM1w4Klp
VvNwUNpBIK/V19tByYTk+WIns/isQzul32DvVCZWSG2/FJEXhAYmqrd3vwJTPuS6K/9Z3ysRX8vV
OZ6XJQNsIhbnjv8J9lz99bscQJ516Hr2I03b8GolJi6/ZrYmV04cpB8gy3xavMAEzqCtR23pHj3t
PrdOjkmCTM9f5LZ43IHZF7PHItkjkeIB1PQgW34/0VhwyKEnBooujecEqzk9+GKqkLfX4/q46HEh
YpP3oUw4KOM60wAKkymGN/+IUYezQ7FPjoEaxnHSUHajy2P8+qjv+B7m4qC5P7FXInTHvQh2ft/I
61qhK/RGpSwGKyL+EQpR1Qk/cPhdWQlX8vcl9rJDCiimpHVIsHG/4JFqEKBQhbzpFVY2YN6ien7O
6Y2HnkN4kZ6HrEccwRCZ/HYlaQYAAJP4l0oHgbkUTb/JLaEB8W5PnqHlb2dbC4eG0Sg+xiXMRTEB
mLL6BklUjfTGnkeaACdCtidomP4PVFLCL0oep77/8hVn7diyObvTllOV0Axp5im4JGE+DfZ1+9eH
utXNLnlMwdKyeyNax5RvXDy4F8BAs5r5TCW/SIChhaa5Hh0y0GTT5P7bVaZYS3At0/EYET2yrgI/
rjRfyqgonE9NV3+VlcJu+kQTBICDV44/lMGPURu26cdj5uYrVnCrf13Fs5ZvxFjdQNgRC2GuYb3s
wRumEMEY8QCnRHCeL3eg8rsK9HG3ObymfY9/NOXncDIrEPA+ctvWBrPrMQxQjnMVMk0LfRb2juaY
VKq3KpYpn95U5EGIO7Ht91OQ/kKFgJa+o3Ie4yQ5Dyll1SkB0BzxQNIHCFAnsaSx9Q2arzxh8eZk
xCPPr43uFIdmLWJz/LYNP7u3qpaYarFN0ZQ/o6pKL0fvPhaEtcBsz3ooL4JFcwRIm0dT2V+2umcM
4W9kfH0H/K7svfn0h2+d9oxB7UPbxR5pVAXyau2YpFItzpKJa615WYld+29mWSbHu6IoHyVYGEnF
eKNodqcQzhZMbkrN+plwlvuizdGTUPt3c+M0+MGMlwkiHFeBA2Fw6Ggok2mbDC3blsR0sEcKA/cB
jYWRFy0F6asFpIbPEJEGukhwWp57GalOep3MeK0xiO7sarHMwv+Pv4RBM/pR/pPh6dwMY7WKiYVm
aFqZLl9ASPxZzeoNeLgRfGZ0S4YL84dEydnptZomy53TspOaKC+rm3CJcp8tL425omQocRqiEd8b
lks53BoBXQXTd69cL0X68AznVwVb027Em2HzxKccF3VXSRgj4V/cu04tWa7uP5lU1SeGrQnqHcKh
KIAmffOd4IuPL+D/q2h5tkh9L/JtLaCST43m7E4PJR9vo8WV4RChvus+netnei6a/6l5gmX4ekWu
rFSKCyBkpf7yKqqAOJKoCOc4g8FZXeOTcUQSxNG/Y8kQB3rz6H7xpOpQhHSNj7ezRNWvBpemzMNJ
q9Nd1XBi1CyX1SDjOpcgJSp9jZLeWm7oy+wM+X+RIJYAUt30cuBG4dzh44Bb1fZ0fngk6kjcV3iR
9sT4np67HxvRzKSfLJ1/0k/+X0V+77aGpV21MWrGfsAYCGQ6/zme+KAB6I1StDkoUOV4BudCEF4Q
Zw8IQCDikUnchCYCwrKEYYKSjX3YTtBh2h0SCLIdArEOlVLWjlv4b09szC3Kh+h6qrrJBAN+Ts4/
18oMMBAZD30OIFNsUv/bSRgh07qfqfFpInYCVYtMV7RtNsThQRsG21pijkjNy3bEBzoOwQLjCNj1
X7to9ry35fmxzYEJgOkG95FJMNQzA+z/oK1fGlo18LtJErCgIkP6MpFy8Z+mRr6mh+paPXjPvfSg
oq3LabfQlMJa2Wg0eilpn1uiJqgRb9VVEMI7Y3UBcWa19eGrrRC5lHBTCsif1uIicP6SYFg8ZlpM
7jnLkORz7v+mwNG323tywI/aFkgl4198fvcbPnN3RRtpgbTcGSPMrfvZgWyhKJ+qc70mMpcGwRho
b5wOCUOZetMe2mu0qD5EGK8YSZftWCShTlQVe48tiOr8qUoevgH0X7KqevAdDXUQRyTuu8XLk8N0
hr9XGQ39YrIpEzbgfOSYYIrKzms9EtTjgAwUAHwT9+2MF2FaJYnMVA2eMhiL4uNaYOzTLPoo5ZVF
CTDN0AiOTbRdtBGk8xrGjgGUtIGdJbzlwaazcbUOYU952JCKxo1Udv48RBwOR6ldzKkIlwLeIe2Y
mYAp5ifPDIbGr4Fi5OjWUjcRxnu/aXMra4DPNZlXv/vz8vZ552i1JizjFLhxxgmpq16xm6Xsv0SP
pgocjcvcEfdPkhp4F9e1uQgm7s1bxNzse/Dp/kSPFkhYIJl9g3sSZJ97iNht0jy7kopk5QkeBjUa
uBlmQFtSAPXVJ4aSnCSMgKwo4Al8gk3DrwwwL/1uUuwExrQ6xyUQfjbLdDriW9fUcHsvYq/bPQnl
BmmvKMVHaVx1phhQbcJufofP62KvBk6CVMo/rDc/P+Fd6ljg5DlozBWTQ0XvOPGP2MSO8gYQh1u7
CcLIfukLJ78vjEzj6FBC0Kbou7cPWqTiCfIA/6P9taUYkLJHLQzi0HETtw8rHnzjPNM4/qhmgRxw
uMlGH5D94/B2cqZFV4o0EKELfkSJkBQdmGMA8JMOCNyjE6bqb6+YYfJJh6O20Yl/Zp82VpKOKCYj
J7TKAUg9Q/MX4qY0U5zd0Tgwc65Op+8BMLRPz1Ao18ALOngQOQF64Ru4wBCZvzzRYTGVi8Yf4xCy
2hbR00ivl8jXQwJ8Xb1cHHlQIjIy15bji5BXjVdDCnW9uKMbqmzmgWPK0PZ4HYKrcpaBRnTz+QM8
kkC/gxavBz2chzmNoBW5/O3xl4SYkpz6l5/EQ2ieKpagvUW8YPwsOeK0mUxhY1UBB4p2c6xWuaJR
Gww/M6fcv5cQK2CjbrkAd9pqaoH32o6veoOijL8lEJMvDhHFBb2sF+UeXZmzNtq3lxR1TDrZTOFi
BV4Ym9clvqT9KI5Nskj8vdJyEtTCPyRVUNya3jd+c+squQGM86tDF2p1aitngjXxGw1ZtJHIKf+w
o8VWxiepffEBm2b9N2PSgRUaGTahai6kqGD7tJWHG/Dpxr1WfcHPL2rQl/ogl8TRQ+MX1aRCwptY
zL96US2/OyGxZKoxIlerlIh/1qpfauFpld6bHNJYzEM8ilhkBn+Yr2RUIiQJBuQ+N03UEGT24mKa
X21U0m5B0b8V3bx27i9FAIYQDU8ppjqeD2DVC/f83Wdrb2FO9xKtBu9piPfHz+KMHNO5xYl9cJH1
lfFENohU0xFgIU+HtR24eszYgd8yZJlRzv1VahYeA6mca5/xSfB2p1CzbR0i9trezxpYUwxEAGyV
/OuxFGSFICG5vVQRpLaUa4HpOD6nki3z1hfEUgAzzgrRGnsK1QA29Mpx/wuU3RqkICmCwZcEK77Q
bjtzlDq2PqHOZ6DnpAaZ+gFlZPS5yGt6BmLcO8nhj3TqZoKxaJ3MOK5vV5vFbRW4NyjrTIOJxzyC
sRw5EQrHOMjMD7bCKYfAEey4VRxw9m6mHFW0kwCYP2GTNxsfuxUqctO5up8znggS56Fao9VsK9zj
+bDa41R68DopfFnTNSnmOfStrYlfC0lXLvKORJtxYeS22ZffzvJnyF6f3JLLFmk9w+3VHXLjwinZ
E9JfVGRn+01WcvpCarifX1PHUU2BEz74Mw9rEHdTDie2c0IrsYLnIhaRTMyMy/BMol5yOhphFg6f
E/6h62YVMhvADMcv73rypjEf0TlyTXSk/FtvZdrd0mMyFhZ5swtdnaUEsPz0o/tKrVOtInJnistp
AqtprH/q32xHeO2HIetzLrIOJSforB+ra5YTg29T6b4+b0LtHIYHnJ3+PTKNJ1xuHjyqM8PfjTbw
ri5qbLUQwJQSnQm1E50AhdEawHu4Bz/eTnuMJ9sHDCy/p02j4LXl5V7oADvd2Cilv4Hu6OHWNZUd
S7+Tg3HQC7jw0ku0u+6YJ/LhflhFJjo385MZwHJZHb+uMajeh8SVJJRozZEUk9TW5sWthzfVgSvF
e5+TAH3kIbFeM5eWA7KODvr6gJNowLlEJEsohyQyvcf/5xfBVY8JKV1M5fxvaweBT4Jc6xVU1Oai
utOfXxNMbBJmIwpKnqEkLA5GCi09r/jdRLRkuGnw4Q4zqTV1ay6ZJZ6UdzO5vH8fspV+KzB+U2y+
Av647XWQSiLg6GeKUfd8wg1QvPzaO2kCKEciYTSLzdfM5hyB/f9W3HoWtq1+Ovm9V/L4FtfQMQ/b
cxKR009+Vu5lKT+pVq/86kJIbC2CLy7H5mkB2YbDcAQWuus4XHz0FurHcyveva2VM9Zh+M4GnAWY
nTcfogtEbgeQM0aHZ11/TJG/GsTcSsBfcM+RVsx1KhhDkIXviW4BhVkgog6EcxhGZQdoiwcCADMX
L5+8l/sG7GOWQdT8vvk7sS4IUbmnX5ShesQhbAXycS4kCKZG0LjxyP9iNMRbZFIlSzZ9xO1HfmZl
KEOE/IPP/5+NBOxiLvRJpQNotT5ySdnGwemw7Zb5fYJY7XKydDfHuKvsr1rCuJXdHkIp0LNtIRVi
/hQigI8t9iSOPddcSurI+c/qdeNTQSMlIemHNjfrzUbmox4BqoQGARkjQwz/z12ohA1dO43Pc533
jIUzbDusxu7LXv42IB36bmsuUfvHI/xv9ZPwYSUSie5QMg8N4F0M0G7QlhGvOJCDHGVjsgnmOA2u
8vy5lN3/ewNRKYEd2dSoswTmI6vITk4EtGFdGLghrQn5zVEReXEh40tdqp6KWb8UHl5mD947lI3i
ipFN2FXdewPQa9F+ZTKvKHpLOYKsmKwKYEjwvYooULpMdakAoQ4pO5OHXQQ7gs9Kvycos/bm/WZ3
QKixiM0dNAyUPuBNr+H5uL+4+rG71OP1okyx4HYnuofyahg4CBfM/Srpb9PPjrBOAjAWMPUlpYi5
celH4fS6tK/onQ1l6EKU2WIV654e0sGgBXZDYgjeIdGqfz9Qf/LfKijyTFxgZCM2GJRW1epOz8kv
NnO8bqtTvAwazID4OFL+TE5rJgPgIvNrqGK18qocwtSdR5tPaavkfT7PQcKfNfWbDkTvQ4ssaZ8T
D22NnK93ZNblBS/2cesIyZC3moSblZczbC+d3i7B4VxVuRVhYli1EGK7jenpRcqJ4g4+eU1jMnTl
95kTV3bij6y4G+QL5LGXGsIyxC59BAHdBaYtVU7DGVdOccoWjRl5V+9xdz86/mhmKQPaiWDm90RZ
n2ovudh0aUGhwoXPeHSCw743fRRKLTvBOdheoizWwQDstJUBO7iRSazPF/5JoAo8Qo41UCNyOLMa
CbmrdhS4no6Iii7vpmTgNyWYLUncgo8NvX3H2Np6gySapMLOOdHSF3fGvJm1Rgh/VQF/mqeE0QUa
z/pn+cZjE9eD/YtZiVXrwNAZN6oo2yDLjFoN5US4BOVuemRftHYO0P3XiuYR5yHcdQfj0YxkOYxF
/DjSY3zHHFQRDBXPM7mw9DYUpVC47fQhtwZAAYGiXj2EfyG+vtQQAzQmo2qHkPKsZfaAT2FfIoKP
BPNgKgfIg8GFqm4st50Fljdiww9jKCot8yaI0pxMb8YlQIs2svBwHtCjE/xGkjr1cYXUYtXlg4hP
d0RtU1c7zFuVXHZoAqfELho67B8XMLivXetUEm85wMYxSZREbDjD1I24U0nUFwrpg50xJzpGc1uJ
NcVLELX8AaM6qYp0Ty1nSBnd8d0ygImzcTfhWblYlKSQ2yBL1kVYg9TK71DARYezbEcgBWChwFnV
xAMYlu5Y4GhHi8G54YVRLx/nR+bkkVdFWXs1h2tDik0jtVmuDdAfk6aXTO28u5DgLHQ+nJN9nNB9
JXK+v+KKuKboVpjXBI2kHpj0NbKG4MH06B4PxIlp+1T0YLY4pPZFnNewSpbFlp++/CcGXfos+GW0
yTmx6IrquqT1pfwsQbT6A2dn7fcc8viOiwhDtntCxyKjhnl/UHpsaSq+CR6uV/wUENaq8TRsfcA3
l8Sdsw6q02fnPZBZN0SQA/h52/IC7mt3PiBy9YujVwWUSqoD10JdjyKAYxo7jfQoRcMLSZYeiJpE
dFHyC6SrATZFHEKFzlQEWKF3QI4Sa6hvP2FGoE5A8t7vcHKU8vQ4hLJgVb3T+wu2czoTGq2Y0wUT
y1wTiO87MTfvkeY/UXqSNLWv4w8t+qS3w8fmJ6htiE9jPvk9HO7SvLr0wu7iyZUZDsbvV0V2acGy
yXiysjx1fEMV9vYhTs/h/uGibErInmOHOwEJCBWEP0byuY6VCbO2bpnofGhAwvtM8PKCsAxuh34j
ijsALICYmojAh9l4AbLtYfd9OHw7AzPoxy8DWb+ARtJmyDLaLUp4/czRGRU5XGF3b519nUoOrz+d
72s5SOXVdKcHHYn067GSw4cvZ0W1jMsLpviphkS7k/nX90/W1UaxBoTpqf2tbF+OTcI4X8LkE7/l
C+YXQ86Lx9Lh1LjeBhAZ81qfk5CWPPs+G3msIzqrbUBCR+kGDkvJku/vde9hSDKRfC3Y9uUcRsTo
VmYbJoZTPUX7tndEAUMaS9YYrNzG1XKfkDweuZNTfwHjUZxcbw3AWPRslh+DAwQ7QMq4E5OIS4dz
NHyyELQQhqBVtmpzQbHNZUTddFe52OBoNtQSz5P4dZtXsf48nhsm7lkRTbx5sPJJ/vp14916RuCd
0bTTFgkgAkMZ5mlAWHeV9LKSZ3TdKtZLxl/YeKLeGUcKxYqtbLag22zsMBFwNNXRLBCLxa013TK8
WK1zgftI7YF5s6+2iUevluihwL9wDsmCsoYb7ncIX3owDTjFz4OhUr3Csn4CuLroLhXd2iOBp0Rt
uYlQctB/RgYL+ZcoMbpR+6p8mGx7quNKnBz/q8W/PakKx5R5ooCnDEsbC/Xp6zJNcFXazrQRkj1G
w6k7XRqOtlbC8mNgYpyY0PDRT34I91KMLnvLkm2kzzbLSydXLWv3XQ41t2roX7Dy7p3D8TYrefDw
omo2KOiqEgw8qpAcQCnl34njdheD4FfzPdsqcmB/DXzcgDa/dgFQplLGIT1PCoLHDyaIhS2x6M/Q
34JoHZsqrB1qseSfIbm1aEP03+2CE8GarjuQEjMeOqAiFSYr0rWQRdxairp9SqFyrIdc4vTnoiFk
8ogUpBxSiJUs7ey3UOV4MH3QzAfmdbIIaEWl2fqpEcULIqn1CG8Z7PybAR3Ymp2/0o6KxxTx/VpB
ujkpt2hQiixc0pVoWKV1lxgG4C3Ekt2Fgj7lnXA8dBzyd1OAYnE6zPo32u8vS6Cl48WlaDoB1SJn
xIoSZjVxv+gH4JfUEtrs9OO9wRgEX/TgbWEMbhq6casCJodwExzAQ1+qO9lZf7KUz3Zp2dgr+hL4
aRfz42e6znVu4ZgE1g6olZJBZU2wtcLX1gLaRzt2UOYMAw/v7hYKKC2ZqitIWfCTxk2DVz762hed
WrBPh7gEjBEpKI4FCvn3MG1ZOdmf5dVK8VwnSblAHqEdNfjNfL4ELquP3O4DJ2xErr/HCqppQWRi
8ymCKZWTOdKqL4Pz70ZWBABIWQ1yN3kiygGEfm+NNw64f2mGK+ezsInDv+6dwFfWAcC0gL1axjeQ
5Wpk1nP8jKIjnmIiD+3f1bEs2nA+gPn4np14gXMljnbGf9njbF0BrDbdat1pfpRjsbe1qSiZmxcU
1FPaFAMfg66SeOiNRMlIJwj20XMzTSlDeeeIR3yhnMfiHIE8dO908IANLucP3kRRmsw9rZ9aeJzQ
nf03QqN7d22gpU/K3Ug5pgoOQFAAngrZGlpurCCQvowlvlwDRLlus/RX8fkhHIrCYzaTpdyHb+pq
hv6zFb2WsRRCq2gSMEPj4k3p+KjFBLvZgw+0urYRtTmHLX6NYYimxzjt+YApVCwvt5+XSMyxxnUx
XHg7qIVbBt3qTaDhpKhwxF+ffFdpPThK6D1F5a9f+oI/KKpk8fPWHOzvMEShbl8fklBuza06/MFe
ml7KrntBO3SThw/JRp6I+vf5y/ws1w04sDDOwQnhSSDCW0rXIk0A7/Dm08AdRCfDsoriC/dYauh4
65QnvdaZTthHPTONaLja6NYqcSIdpaqIkya+EIfrVSzlyw3xn+WeNsZf118kGi9pMMn7zklRCMGg
42s+wjbHUSDBSQXS54NLPloj3okzlTkKf0rMHeKArp+kosWRghJLvq0GuLjUnF3eLpuGjra/FdhY
28XAG/STBBVfeGffPxlJyj6yw/b3UOTCHA73GyJlGrJTNiPlKZEl+b79xLPk5Y1fLlvEzIeTaB3j
FHTAf/A4iDOXAirLhMXuStjexTiuoDlgvkE2Y2vWIx95nPlKZSFxidIzQLNNvxjoeSuiOSq3qk3b
g9zYS2I8E2PdQewoYO4PXmghkCscXH2DaGJOQFP+TQdNvS77P06S3PzZZI55y3srCeXKKTiVXBRY
tm/wbLNKPVPejcqnVIzUb1zj+lgJKJtMOWEIunILkozpxnCiXwY5Bxm0Eu34MTl6gVGrCxK5RzyW
aDBi/Ldfq8mJodSNFMqzlj0AohmNSKwaRCU+owiNHtTGplicxD7l61xhMnjCVBSXQqe0jhwBYY6n
ObRlLWL9a8oG2eDIAqP4OzcuBZ5GkNDCc1JhjWUPT7RtF4ydFJaE3N6rWLsJz5BMJ5QHqOl2CveT
Ap9CIqeDx2Q69euAf8r2R+nY21yV08Il7hZSjO8Glwicoy7ghjlFz/ycExHREdp2j1fxx9jfkGjP
arDaIHTqCcuNANF08hu2/0zVmJSCoDBHEu/TQpwiAaj8TrSoLqqoZVKDlxQnwfvMR5PtckBUMkTH
U1JtNCs7AVwqX+RLkcUgZgLwQNwgbuouwxP5jFkCsSB6jUNDHQWwZ1y5lwpIMP5T81zmsZROzrWX
rdvAup3YQNp1Tb1mvWjQKXMYrAA8L974mBTJQ6FWNkBo8dkoK0lIQo1KTbIGn63tHkKAnHL0upNk
WjrS4J7VQLMHJjS0Jatjp8UUX5Mb1UwRBbo1GowaTDrOTQXyQEnzp9jAFrEQxTwKp7TPFVxZVNOF
DPAAcHW1dBLOMgZJNeh9ksMUKNixDf3lGxTew0h9QESNHEHNFjBtwQ+rtGJurxBa8HgKv9uOsu23
KwiebWmY71tltavOCa18iIn9ze3nJ1miAgznEGY7VNkTg60gqsQtEpmC2wxB9cH/GBb+gjY2jZf5
zYTwyN6ECcMZG1Tr+uwYfDhpwZzzarIuB/YWgZaJ8JYE8eaTWrxzywxrb/0tCBrC4o8lncSH5UGC
9O7WJ9rt5PvVfAfMRhmwSEDGzcVDQd45INLPX73WVxa5vOhLpwyPHAuGgV3lJCY6/woTcoHd2scw
XZMv4oGHC6j7GbJJTSkKc+5nUji0riFSTKkZSV03mzudsBxIyI5s6mxidfy6NGEuEzzLpi03snIW
UfJrecaLcUCrliYTWWMWENrlZ+SbLqnkIXy+7mv/Iqf3pacy984oSLDWTkFfsUSy2pwLGJb8ad5m
j1YDvG7sPL3JDZkbt/tJw8XZNdUdLe1NTrvORG1uDTk+lNbD9EPXXg2nxm0B+OOpboEJT7WTo8JV
BOM4PumgF5vfN4dJHYG8PYwVUcWc6oht8r7q8IfPAX7q3hN/4l0kIleYtzoVz9Rurwm36TJSkVxI
A4uBjLz/aofANEMmQAmdAg+O1t1exen8ebA6ovoh7GlLNC+LsPjHvdIqpwFiRqK5XWtcf0XgdPkP
g71l9zFMPRLmBL3aBSnvCmIM85bzN8IyJo7gJ25RG09qbCPRa6tTFO1hl0j/MWFRu/P+hOG3ifVq
atfsmKCocDTGnt4VUkdptP0um2h/VpV8Rw/AW9H6xR14jBJ3wDqRVs2oGON7Lnqw/wUmzJqM1w4r
bawo1+DLsB2u8Nx/x8GW6c5QAWXrJExhkgUEG0eZsr/C2baFCxDLB5xCjVHbIoVt1t6n/NjA72a2
EHXmrOWKoluH5OuPUFIllS/9lVdtp0bNdWY/cyy/eu86VaP9npAqUui7wGnIgRAIeXd6m87oOHqn
I/30KgfPqxVOu23/ZoX+rECkooc7DmeyUaFRRhJUvjnsPwuLsfFCXGsChRt37iYCAOfCExVR//kR
XA1RzRXP9PF1yR78sKCudTjzgHF577jiIRzBUUU8SL1MfzC4pDNgDYwoYWwHez2qEn+0/toLy84E
jTvE1nA7XEST5bc7yavd9CdqvWBtocW5mouAzBMWsI5BqLLEk55VsmOM3qwvdf/dXj7rv9wz57+H
U3wSsTHPefZC71sONCBtVQckaMzk+hT+7Swa/0DZzVv7yNp+MANtPq3UzJadWae2LezcsON2Njkg
/AF7UB2YLvy7e4BWUNjMc1NUZIpoWS0BuDGBJkNPdC2QzhkaadwFMJWaQ3RES6ecPwYEhA2J6WoZ
3efrbdltsrjs7Ts0DpHQezsMwSgL74FIe4pZ3CsnQ4iIrkA6pyC39dr+KLhFnXIcu6kamyId7GE1
MWxJ3Sd1y6YHHZVIgogPKlcUBile0JxC4EESUduFh+x264Vg8IrViFl+1bCB8xXUFBZaY9XPFh9l
3yBGhD0LP/E322/Qib4gPm3VdKKJOEPHus5rjVRogL4ivh2xUPg/OLAr5vvUAG4Ug3Qu0OaJiOoZ
nHQzhUCK/p0MIBg/hH6nISLJXCVslID7qSwHNxzyrqzqCg/+s7xb0wl0ANI1R70fwhQi9Lj1lkGf
V3YK29VfstX+t2eB1CkwUDNSNTjr57EycxHjsUAMayUEgMIA7i4Mj50u9gIaFtGo5Byt8oXtOqNz
IU3+k8s1ARgLwLtP1OqVExDG0KyROXC/KukyPQWlF54IpgBjKYZSBbRuyNMjBsFMxee0Y3oQ7a6L
CkZdxUqjjJi46FmVXicXPuTRrwowFolpWjPNV5OZmPSrRCdRMLYBo17X1tv9yjZRbcjv4VzdygGj
AQgYq0S29BMlX1IeJWUVADh3YJpnSNSzsWqoAPojjrMyfXzpfaHu9VImPXW9F65hcSgMcoXa8BMy
X5altYst2+3OD47o9BpVd5oRyX953qz29hSVG2vER8pbUDs3EYQ98zYWcPKCFPrx6Q/I2aYhR4qP
a/SMneqfeOja20UAGz9SeDMPkRNM/qQ6f3SgDhVBa+FeDhjHJm9CllCLT0X0czT/B4GXk8mEb1Bm
9iI20GThwR1vpjPCPmxH8CtZFyboAuiOgs6lFSsdhyQSWblbs4Ewx9SfvNxY0YUs604eZ7vbnWwy
Iz0C/6mLaa7S8O1Tk7ZIsoELhkmvwWSXRwwLMUH/hvcD4UdAfWR3v38ZuioKyD+tiqnRQMka3o6S
fzTUWizABmXFG0zTZBsvMU0g4040m9QQkVdScx9/kLIicjPAhydPf0Tezi8sQ3wZk9MASmtj2eUN
sJ4F1a6zorAE4VtMm6gbEiHRyEX3yoe5GnbvSzuL5ixoscOPT5mR9fLJezpnUQc4ZjhbBznSeJPr
P60F+2iDmdNI3IFVpvqq1E3q3IKRFhBvKZn0N+VE1Bm+Gfceo1KdHCKNIrlPzsVZkZ8RE2u7WDoX
MJ3ZirE5dbbGC0rtELMMpyfZ/NzjZH2IPeK7Ur31W0OOqL1zYJeuZGQSLjhnbOtwRYiyJ0AMF39t
j2zHKNqkYp3+vB0SsFedxhuV3vpjhJwaYbxNz0PJDW+bpoRhr5norzrPLMBoikk939fsDBMacqBS
mYPK1vmP1MqkMgUXWORJ36xELnM5ZG8RokwG883XrtlAd7/zJuCLBEsHEOMV4CsE8J8r+6Y94cPS
CmMu3PboII3dDavWg4QsW/dJB5c0VxIpyZAUMS1uL1RMMxN/lJuIjiJcjdIURBU1d+iYz1dPnqfn
DOmhkJvfLCiI1MCftmmnV5kSY+OQ51KSWSJ5rZGNGKPROiH1gvt4SGN/ZFdz2gmivsyFyq6QDs6X
QgrD0e6JIJqL8t8ucrOuC9ookAyI9FMCSlmwoYyXvytfUmcUttaNDIyQzZ1KPBaJzXLl0OgGOm6h
sDNXfEZBL7R3O6jOzjkAhcEaxy0uADbRatJXah+adJQoO5LJGKAoqqeAqB3ZA8CcqdJwCFFFvzfX
fqWum/YjRbZx1HSnsVOig6WyLC4w284FVDgkh6kLM1QWeLkouyBE+Vy8/SMDVb03GDGTcVCr42T2
KzWZUGSTRnk6g335mfu2LArR4kjZ9gPPyJYf2rVYRxgooelSfm1z62tyaTUnZT0Doxk8iYw+xJ8x
H4kEi7wQOk9TBE8ALcEcpHocmh9mEasVEQstAsULGc+B3PPkP1EpC0ZU0elcG0VodwC3MnuLY3e9
S0zVFAleJp4dSZc7Uvxr5V3a6k+wKmQ9HnWc3UHi4PTiG88qhOPq5S4PeDeWoYc8BLnlRY2U5t9e
NYUWcBwaX7NrLIrPg+FeoN9+eho9uWigjAaI7C+Fxksjv+kIDF0yoVUCfMicXmN7GAlNVJmLb4z1
SE8dIQnpcJfx5JkIvWGFq3UjIGM0o5AMAJQxA9KdojV72gZnGoVYbjyCp0pw1oTyOzSj4vH3UlD7
DI2bcIUg1qSoihCFwoTKJfmazaV0S6OfkN6p8alKMWzSJWIWtb+pY0hZTuDqTTCGjxFGA2Kxum9V
jcEz4s6Cn7FBaRFgFuTuu0UIhj6nqtFmv+Hj+pIP6dxmuFGvViSwM39r30y176Uyy9Q/J/Ck/UW7
5p4P/H420UaCwsyiKQYArdF9UyiddeTAa0SGgTyrheEqCOueMFUHrNePjO+5gbWUdsosoGkI9c9g
m55P23ssJ2ndO+iR8Ssq25cdD1tM2rfwqFjWsaW1vAQv1iuQpHnxaFHOTpy3L/FOlySZR7ggD/3p
hrMOrme+j+R3MndSQDIgtT4Egr/JtKWQSYuRWsyuNAoAGIOdzERJp93jUkUGs91TX1MWoDuxCa8G
UzPrm8MphjU0W8NL+/kejCb4ikFQX1pwgaIQwA/rzG8IVzYu195UoQXfvIngYwGlKB9oKwwKqXNB
hI50Nyp+GLWFz+GkMQpklZencfAbZalbu3x+WiWM4eozWFuFs8FnrPZy4dZAI9VpZZWwbxSB+PdS
wNdkh+h2By4WyAM1hL5Vn8VB8XFgoSBT1jenYlGYONiX/h5dSao+t+q9NA0Dxl17H5K4So5/PLJr
QyQJRKoXdMW6MYL5L4wOF5B29j8udJNyJeM2v6wafP5XmnLOstivdaMvM0obMxj0ns1izSlgvgER
x3t2UKrG1FM2eZPj+24uAL2YocU/Oh6PQvgRufSe7FQ9FTKBtpTT8m3xO19Rz8uVs+FcggMeGzIG
PFvz0AsUc26bsEb2PU85qaAv+3/pwU1A9Jsa2Kp6ZB6M3YCvKsMKJX80WnexoyBBrb6U8DZ1pJUJ
1ffvDvtq6AtPLW1BfJbUbHK76N/jjlJPJ9NHKIquUyHFPGki1phAXYSO6TSoutpQpIL4YrTfArLc
J2eVVzAD3GqOLKYoi2lNzGXfxB11ZBXVb1OefshHWjRIcL9qkHogzLZrzEvviFpL8PhHjVRR+quh
3Wh9XRzwY6GXvFAR2hLJLQgFhm1WnXLpxV96IBUZ5kSKeu70gNCpigqJf/owvcCs6teZPL5szN+E
bGYwCMZ0mIj8XU3y7Yc1WHx7pfadrypxso5toc3L6z8OT6CTdIOQshOzpEzNUvTcehQoeVKDVGXo
FD6UJrdLFWWivk1lBCahlA3TluLts49sn1p1188imx6Dp2UBjaZo9hdRI3HlBI/Jb/v6R6Qu7+Hc
iNS4JBi9x0dZ/7fMnMaoip3Qk6dirS4VtXESPaFySWkh+7elqxjdZggkD5DGdDfo8+c1rhD9h9H6
topIMRAajPki9LllKg+F+JmtcdJvAYah2p7NSJ6GlMGxlGY2zaqBvilHisLXAzTHt8SAguNXAUVC
QXemp0rWwpsPyyZwjGNYB1W1kIonKRuhuQCw1bFQPTMMtJsxqGhwgxDjDyGt88bqBtd/XXKqaOsJ
vC6gIc1+9RIWOMLqyP9zOw3FYfucsI5FSjTlHtwG1k7Y/lyemTXThAA4ScHulH9Xa/o7aLmAEtkm
xF44Jz8SWPjUmjmjOB1vNMvFfnYSPd6Zd3/8LlLJ1GYneyIaD2tRwoUWzRxFlLoUT4JCthUYEDrO
osBcV8J2THF2T2lw6MaMy6+rFDhX0rSMqI82PcNBJytvpaaf+oiw0Km+WfCb8755r3t4YZcTL7bW
CaWtUFwIjErOvKCkXhcCNQNrSqn+vVOJZyUBGqLeQj/MVnsmG+ni+6oNX71I5eLVhci66NP56nMg
PYAJPHnijrg/JWGhgobFcyg3Nn3DQlYv6Cek53LVg8rMuI4eJ/hF4g1ajtowJ7VRGNxiUG1J4kdK
p3l5uVrgBd4l6g5PU66Bev1tXnQbB2Gw+AVy1RUJb/UCfXu+qhG9j0RiA8zQmCCMIzqKyGcDCvHn
VdlXLtRVJ2wAamRdJwq5xAwIAGISHfdJHF4IA2G4JDcA4eTwR/trcLdjNS/6wbmZW9U/vEvhH48I
tUGBWhRYAvHuJtp2xf3kYNhOz4rOGo1JaGK4u84G/GupdQ4lQyT5d5GoyfcgHAk/AtOFgPTfR7z5
DngwBqmBmDCCY4UJuVQCUMYqBadpPrl1dpT8UqzWxOyjjYlwY77ryQWeQmmekuKhlfr3LWazGM+j
QB4LsXB6QWQ0NoukqQUMADmg63SBMLXFwlox+y4RKAmDiATrakOEMCzvcdZkHCbIN8caa0G160E0
iuQTMn/tTAFo3aFB9WDJL3xpPxoFg6g1bBjiJ3ZLr2rBM6d/x0IADf6BgXcGwdGmIakSL37gKIPc
xdW3grGas5wWAE7PWB0rtfNvPkwcZBdHBhfngVwLw4kILrxLHl5sOMGGfuztAHCOQ98oeunMS+Dj
Pdw2CaqhhmDCaEJs51gb5iugRKM6WtqJujcL2N5O/Ue/Rn+4UDHrzW8CJgjrClPbIr0uZf93yskl
EhcQ0tgdWv8XfAdD5WU74jHby0prxBWJs74xvFkGEF968sqGZ7qu9DJcC0jFuJYxwLVO/l7ZAR++
/wqxbphe3mnLmzOhlZbONbW2H3hgzY5hQyTmGU8B1MGHUuyeSDMiHKx5wThTVFspAdpbFvaZj8f4
yOYtfX00v9dKLFfPmRo10+7ysvfKlSHRMJVfQTnvR0YvRdCYSVUXc+B5NKE4yHLp8JYY14hmkPAe
vfWhCOtFoHpfTOhxXdTUHBvfOjYTGXgDiUb1i5K0Bw0xb9I3AxGxSGJcN+QKfULMDlNx3QeLvJA1
yZ5Yl/1MNwd5RD6VZ5rAhyGeEpvrTGrmRf1wxvVu5+r+/yO0nuWew0tpUVqx9Tp4wYUXClBIkW5i
mygIrtRj2ZFTjHHdk0AI+ZmXULs13YzIV57yzJw0pKbJkiXLmD238wBciisrABvAqeblkJeoT3co
GZVhX9sAZlfpml+fo2c1fjfDB7ImlhbLOX6InX+zo9BrO4IXHEwWOIZOVU+clB6NqnnuNkv3HesF
bcg56ppBrd2ldmVrYr2PXNG6/Ky0+JGqZcAV1o3I9pJcIEREdswfOWKlpGUwy8JUDN+Yu50dyfCB
OXanyObE2e+KUyjr1/i4k01VjAH8AUdMantGBKRD0flpwuTpzilZbkhmWdxUBGt1oVFykxLDAJlw
75cUM/cFj7Zo4RMyvoARbU8mo/mz6SdB+516R45vLaljjeB1n3ZYU13gECwbvAiX9iw7hazy4h+t
U0J4X6v7cRFo0BhkLQKz5wIJsXfj87O25bf/SmQPLDcuTAQr1w3Ie1TFwNew/KjlrxJQ2rXXqStc
WtZIpLz2VREfp9FXY5DfYX8j6+8uQQFsAIKl4Hx/uGKL0vsgLMofGPh4gP5QTKk69HlpsegN06wZ
N79j1f/athw8s5JJcj1Joeh9nQciDfiCVXPo+TDqxOkU4GoytkYwOStibn1c4uCUkFHcOMocvA9Z
AWMYofO+fnM087sQwyEnSub6YkUdh8u0y56hDeG0W21EgUYhk7tpEEjBlRTTgjQd3tgDQ7AJ1DzD
ER3Jm+rqxtrF5TeKu68+SsL5JpVnBXc2Kbs0Ih5sKHpMCU0J726fLSCeRwSZrSejra4nhsZ8uSzE
suMn1y/0nN77cZ2YYDzODELeY1j+TOnqGSCG+64lu6ARO4h+uYLyB/jR0bt3+PRJuEmQiyUvhiLT
W6+qzSRactkBrgL3eRH+78i7S4yQ1F96czRd3Ou7mklsXFmxM2FkRwO59tGRyBWIsZzhT1DHU5jd
PfK0HEUyR9VG0vq3ApV2JJRgkE/Bq4ux/pocjcO1nPP30Ct7QP1JgoQlrmPajxmXe9DOdO8RjV5F
DjMO+ZoxtknAf9QB9Q2CxKER7zW6dXpk2eBu/k7bwf75z8HfmnyYq2rNG3KZ7Us1azyFIDVfjGuY
cnY2hfZQJ7J+jvYs0ie6GZootxXzKmvPmXhQnXdP3pk/8mFiLxBCjIf8bFz/hOlH5Ad4E28iwgNb
kcvKBKOmp+2jgWKc5EX5ISHnEy18jK1XIQjTEwlTu1/We7nrwSHTaili4B018RlpS+aDHeCfPOdI
6f8TXeZwoBRphUMUW5d+NGzMkSHCjDK3WE3RmhXKnnX+qzuXQyFI6vubKGV4TWd6lXimsLkWOT+G
vuDgnsdgLoxeOISCuT581e2nQvLi6Oq3AJ5iCI1CfAiX+dr9xvtDAAHjQIAlHrBIrTVftO3h7VKM
7AfRs4zm1L3unlt3vzGyJe/d2sQqDap9OfaGiEZbJMWi5A3VhcpjtZCDlbi+szYh1d15uIC4Dd13
5DTYY+8evNBG5tCe4fIgR7yhnZo/aiDEH3Lo2sCmMh+uFZndVXBiisYbnU5VTllSZxVCdpzBU8bO
adiinC/L445OOdleQ04DoeFpFfAEahpubsqnp5rAO5oPdaOFqcweZyZtCSCB6zO3eMvip+no/Sc4
iEeA6l//CyImM2JgWPwFMNuwJwiCoisswi9Hxj5tg/j8D9AKw8I9SZs//jSsO1SgPnRt61uw8PYs
7VesmbSVllpmtC4In4+DlwoKsHlBe9jyd9jH0/uctkMyRBH3X2bK2euBvM4naXaDlXObcPjmdC/a
hxhia5E1gz3Nur+ggV2XO5o0iQJz4k1GsKpzNfENuTQ3kG4Idoy5wyhbVtCbXrPBPk6VAopg+aFl
V3WUS9SSuLVQSAlMAE7wCvkbEjnI2zuuuZm9x0IotDjryy8+pJ6j7CPQRpSXkjLKFxsDT51Z3FuR
i8NnloMQGOzhph4pPbvKOemFAhjnQP77T0WrHizFjm5NlSV+Sb7DlvRayK9iFUE3eABpvaD3QqC6
QLPbRZAjpZzFFci5bt0nJLN71t7aiXB2lAqPhh08kq6fHIaATXMIdshUlcHAxnwi7Zwh/M0cDwlb
+TMovbKmBsy1GfJfa1YGdlCIL24j+YwYC6SKWNpaVUZh9W4Hekoo0xA0vvmaBhjVZXwo6nwT2qiv
h8rfmanqPNupydaX26IWdWs0TrHir6v9JdbSrX1fzrVE78nmvEBdXLKj5D+LjUOqAajz4Fyf/cOq
cyDa9TvXFMbuErAMflx1dE+46iw5m5arX2r6msT7DeSdFk6OhY1SrkQNPGBUkVsJIa/zOptz/5NE
K2zmb21BtTR4jv0QHDccpUWS0vMtdxBS9rNS+KgnOCOXehuyIR/+qd8piQ7GrmaZFgPuHWHM3u+Y
mdP0srds3xnQqnDy6Rc4o0njskcy/Le3XHD6NsCGRvg79hgaUp/8Z1902AdfG5R8k0sN2OhoFo5a
xZYBHQIJa3Y9XmmpEn9/fqr0VGgfu86zO/hDEq/JyETnvMhUMr9CN+4UbGBVzlxRARVqqfZWKDe5
Jr7BmHRVhNd3lo2D0gZpXzZZQDrlu8n0hqO57zpg9/hTnUIGvIQ4yyXs140jQXj02AyeVcLh54fj
Y4OTX9VI7MlbbvNYlhpECLQfFrzPEDA2OV1e7SSnIm7yYqEDdZND4ZVfgXo02GC82VFe6PULAgOP
br3HQ9mCEJL5IZVulux3RFMlV0O3RH2F4L7208IYrH6IE1X079i8WwNcYAPt2nR8FgUldLnuFeKK
nlbI67rTCl9aFwBKxcleci5J2V+CUV2xKNNSdKwjg+pl1LG/6Q0eWmwcMADKB+vFui8Y+I2fXHM2
M3HFHs4ZQvYT6+UoJKyUy7XS5w0Ec3m7WrCRayaQwP++sa+RAnDvUTpAODsv3qIYcwYM5w8ZxrW9
+/zousPYskUF3XZYyXtQMl85caf0ACn7lB+uttW8/cGc3Xq7IN/g3cjdxjLx7H+15IaHUuuFCJSJ
A+p4/aHojahnimFH/Z2dX90VFj4W6P4qOvYVQ4qv8lZGnch648xwq+pJo5GVJ0hkeTMCuj/LGDFF
GL6ScIe4p8dgfYdYq4YCHt1W8OTLyWDT6JW60YPilTSlcu5mvkHbXx1dEe3brZD0rg6QVSefAPNf
yH4vnOYyDjCeRDe71m+9vf2adj31rgQD3XR54XRXtyC9n7wIbZPeA37jePEuRYMgw8Xz6zxilLpQ
de7uCBIpq87m1iZy9uHt6dPtVjWDx2A6HLm9EPtM91C6KnIN20lrr4ptM2u7RucRKpgVck2QaAH3
VtZyBA6hkcK6LC3DRVMUeac0VHDdENjq0bscmemrsD+AmJWzmo1eto8i5Qc+B9KLBWoT7+jWZcUV
RsINbH6UQLlHfqRvNdT+zztuOt2ny0R5cxBged14cE+0O6iu5SU5WXI5rGOfoma6nvUaRspaWqON
oHHViVOxXfyMQZyEEMgvAirB47rWd9GZiJcGzriwynUlnMsli73zwiReQHcTid5YedSS3+QnIG35
/eYyIR6XLXtXV4pkwb0Dr/IZSX2qE0iyD+0ZejManaBHhQprvxuNhjMbEdj31HcGhpPVEB6XHtdL
qvCLNudKs8DU9NHu0xBdz2tbt5lFIzxMiy/AsW0H4KxJCKC5ihExLyK/oF/nM39PgR7DZhP6AVGT
OCh1dXLFMttOHgexA7VL5/Bj8nFyOgnr1hesC6TbxtUged62UvYPtysFpiwyoueRsyXnzekUTxPZ
ZRXZqdpK3HQBjDlnNrmS0FyUJLyzxWb2CcgY1LIYusPDAdilfnAGEVvqFLWaasw15LSFL3NVzqyC
tKhjlHIllQdSOqSA2bjjq2o0u2kGv2pkaaBvyGmMwC7t2QlWi2qzK0s3vsikwzRC/CGD69f5cWps
+4EpHeNLf5ozKLEn3LyG+c/90FPxG8zLPOJZEla7TPztVSogFyhkVzawtmcEbGA0Y0TXxXh5KcPo
9HQadG8y7yy9dtd3vvXPpl5ZZAui65JrVDL7xAM2It0Q8gv7rvbvePjSN9SGSvmLecrNZVtU4zvz
gd7PoUA/LjE0CCAcj0zXNDRrhk1TalmX3/pVXHAsrlzKh9hKD7TvHB46SCxXCSBqOEHgJEmo9iis
rbF3cJ29XAIuLJyrih6H8Q8S7cQElN3+xwXrGOYiTuHVyy4iJPLQaITgMf70y7hXZOq948bIkTRm
LOW+zeQooZtCcrKZQVHTITPejjJL9gTLWgYDO7LO4rUx8auwoRFZl+8p/u9cZNAcAxLpb+eimWhB
aMyqvQf28YPSG16KqGFLM5iwbMeDlc3m7wISroN7lW+mAlEGAB+EbX6I3Jf4YREfsQNnnToLpU34
e67e9dH6wvW8V3JtN5PshMIp1iXWpOBjmp0/9UniXje62njt362b5aBz/+X/q5b0ismE1wziQf3F
frU4oOT6j81n6dcDWaqDEaW4NBKhZKfislhCwkwPEAgHFcFVwbmiwtpj8aRENal248nI8qk/xzQb
6k4FiReNqcB7R3dEAAX+K9RAi+xZjtJy5nfzxx5C46aqQg9OnkGUuGTXmrd/W5i9oJR48Re/EK0/
PUboR4C37eC0cQlvZdT93Vp0jomzDKqj1adB49KiOx1lCBZq00lbHIVs3KcAvHBg5EUkeHUN46Id
x3FMqCyhDxBxw7gpdEQCUKNRcqkrG+C1yVWjDGJxR1ELaWHBPRHQ2bL5XQz3VQlyOZmZgymHZo/x
97x29kG54txn/EzrmzNEpfWDJ+lLH+05Tvpm1k3Smld/+Di80sWJNvXKo3/XYEkKNutcJ0/X6iGi
SjWj88q/aIWq/6+hl7JN+CFJF7N2VG1UMfcmh8gB5apkU9D5OCPImTUaV+e42NNOzXUISptHrQe/
ob8KL3CaNF/Pp8pZaHOO8Xu0weN/fyjD4isD25tN8cJE7E5UoA+00iRdZae1zJXBeIOMcCBoDnlv
EpfPI5RR+50ll1QSt5MIWzwZIcbtVVNPfA8PkhP0AGZkfg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
