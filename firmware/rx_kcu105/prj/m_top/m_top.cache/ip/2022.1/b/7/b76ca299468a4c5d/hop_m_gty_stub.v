// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Sat Jul 22 01:58:01 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ hop_m_gty_stub.v
// Design      : hop_m_gty
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-flga2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "hop_m_gty_gtwizard_top,Vivado 2022.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(gtwiz_userclk_tx_reset_in, 
  gtwiz_userclk_tx_active_in, gtwiz_userclk_rx_active_in, gtwiz_buffbypass_rx_reset_in, 
  gtwiz_buffbypass_rx_start_user_in, gtwiz_buffbypass_rx_done_out, 
  gtwiz_buffbypass_rx_error_out, gtwiz_reset_tx_done_in, gtwiz_reset_rx_done_in, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, cplllockdetclk_in, cplllocken_in, cpllpd_in, 
  dmonitorclk_in, drpaddr_in, drpclk_in, drpdi_in, drpen_in, drpwe_in, eyescanreset_in, 
  gtrefclk0_in, gtrxreset_in, gttxreset_in, gtyrxn_in, gtyrxp_in, loopback_in, rx8b10ben_in, 
  rxcommadeten_in, rxlpmgchold_in, rxlpmgcovrden_in, rxlpmhfhold_in, rxlpmhfovrden_in, 
  rxlpmlfhold_in, rxlpmlfklovrden_in, rxlpmoshold_in, rxlpmosovrden_in, rxmcommaalignen_in, 
  rxpcommaalignen_in, rxpolarity_in, rxprbscntreset_in, rxprbssel_in, rxprogdivreset_in, 
  rxslide_in, rxuserrdy_in, rxusrclk_in, rxusrclk2_in, txpippmen_in, txpippmovrden_in, 
  txpippmpd_in, txpippmsel_in, txpippmstepsize_in, txpolarity_in, txprbsforceerr_in, 
  txprbssel_in, txprogdivreset_in, txuserrdy_in, txusrclk_in, txusrclk2_in, cplllock_out, 
  cpllrefclklost_out, dmonitorout_out, drpdo_out, drprdy_out, gtpowergood_out, gtytxn_out, 
  gtytxp_out, rxbyteisaligned_out, rxbyterealign_out, rxcdrlock_out, rxcommadet_out, 
  rxctrl0_out, rxctrl1_out, rxctrl2_out, rxctrl3_out, rxoutclk_out, rxpmaresetdone_out, 
  rxprbserr_out, rxprbslocked_out, rxresetdone_out, txbufstatus_out, txoutclk_out, 
  txpmaresetdone_out, txresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_reset_in[0:0],gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_rx_reset_in[0:0],gtwiz_buffbypass_rx_start_user_in[0:0],gtwiz_buffbypass_rx_done_out[0:0],gtwiz_buffbypass_rx_error_out[0:0],gtwiz_reset_tx_done_in[0:0],gtwiz_reset_rx_done_in[0:0],gtwiz_userdata_tx_in[39:0],gtwiz_userdata_rx_out[31:0],cplllockdetclk_in[0:0],cplllocken_in[0:0],cpllpd_in[0:0],dmonitorclk_in[0:0],drpaddr_in[9:0],drpclk_in[0:0],drpdi_in[15:0],drpen_in[0:0],drpwe_in[0:0],eyescanreset_in[0:0],gtrefclk0_in[0:0],gtrxreset_in[0:0],gttxreset_in[0:0],gtyrxn_in[0:0],gtyrxp_in[0:0],loopback_in[2:0],rx8b10ben_in[0:0],rxcommadeten_in[0:0],rxlpmgchold_in[0:0],rxlpmgcovrden_in[0:0],rxlpmhfhold_in[0:0],rxlpmhfovrden_in[0:0],rxlpmlfhold_in[0:0],rxlpmlfklovrden_in[0:0],rxlpmoshold_in[0:0],rxlpmosovrden_in[0:0],rxmcommaalignen_in[0:0],rxpcommaalignen_in[0:0],rxpolarity_in[0:0],rxprbscntreset_in[0:0],rxprbssel_in[3:0],rxprogdivreset_in[0:0],rxslide_in[0:0],rxuserrdy_in[0:0],rxusrclk_in[0:0],rxusrclk2_in[0:0],txpippmen_in[0:0],txpippmovrden_in[0:0],txpippmpd_in[0:0],txpippmsel_in[0:0],txpippmstepsize_in[4:0],txpolarity_in[0:0],txprbsforceerr_in[0:0],txprbssel_in[3:0],txprogdivreset_in[0:0],txuserrdy_in[0:0],txusrclk_in[0:0],txusrclk2_in[0:0],cplllock_out[0:0],cpllrefclklost_out[0:0],dmonitorout_out[15:0],drpdo_out[15:0],drprdy_out[0:0],gtpowergood_out[0:0],gtytxn_out[0:0],gtytxp_out[0:0],rxbyteisaligned_out[0:0],rxbyterealign_out[0:0],rxcdrlock_out[0:0],rxcommadet_out[0:0],rxctrl0_out[15:0],rxctrl1_out[15:0],rxctrl2_out[7:0],rxctrl3_out[7:0],rxoutclk_out[0:0],rxpmaresetdone_out[0:0],rxprbserr_out[0:0],rxprbslocked_out[0:0],rxresetdone_out[0:0],txbufstatus_out[1:0],txoutclk_out[0:0],txpmaresetdone_out[0:0],txresetdone_out[0:0]" */;
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_tx_done_in;
  input [0:0]gtwiz_reset_rx_done_in;
  input [39:0]gtwiz_userdata_tx_in;
  output [31:0]gtwiz_userdata_rx_out;
  input [0:0]cplllockdetclk_in;
  input [0:0]cplllocken_in;
  input [0:0]cpllpd_in;
  input [0:0]dmonitorclk_in;
  input [9:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drpwe_in;
  input [0:0]eyescanreset_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrxreset_in;
  input [0:0]gttxreset_in;
  input [0:0]gtyrxn_in;
  input [0:0]gtyrxp_in;
  input [2:0]loopback_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxcommadeten_in;
  input [0:0]rxlpmgchold_in;
  input [0:0]rxlpmgcovrden_in;
  input [0:0]rxlpmhfhold_in;
  input [0:0]rxlpmhfovrden_in;
  input [0:0]rxlpmlfhold_in;
  input [0:0]rxlpmlfklovrden_in;
  input [0:0]rxlpmoshold_in;
  input [0:0]rxlpmosovrden_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [0:0]rxprogdivreset_in;
  input [0:0]rxslide_in;
  input [0:0]rxuserrdy_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]txpippmen_in;
  input [0:0]txpippmovrden_in;
  input [0:0]txpippmpd_in;
  input [0:0]txpippmsel_in;
  input [4:0]txpippmstepsize_in;
  input [0:0]txpolarity_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [0:0]txprogdivreset_in;
  input [0:0]txuserrdy_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]cplllock_out;
  output [0:0]cpllrefclklost_out;
  output [15:0]dmonitorout_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]gtpowergood_out;
  output [0:0]gtytxn_out;
  output [0:0]gtytxp_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxcommadet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxprbslocked_out;
  output [0:0]rxresetdone_out;
  output [1:0]txbufstatus_out;
  output [0:0]txoutclk_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txresetdone_out;
endmodule
