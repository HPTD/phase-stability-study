// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Wed Aug  9 12:07:41 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ PCS_PMA_sim_netlist.v
// Design      : PCS_PMA
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku040-ffva1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire \<const1> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire NLW_U0_mmcm_locked_out_UNCONNECTED;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign mmcm_locked_out = \<const1> ;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(NLW_U0_mmcm_locked_out_UNCONNECTED),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
  VCC VCC
       (.P(\<const1> ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    pma_reset_out,
    signal_detect,
    userclk2,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  input pma_reset_out;
  input signal_detect;
  input userclk2;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [2:0]configuration_vector;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_out;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire pma_reset_out;
  wire powerdown;
  wire resetdone;
  wire resetdone_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire userclk2;
  wire NLW_PCS_PMA_core_an_enable_UNCONNECTED;
  wire NLW_PCS_PMA_core_an_interrupt_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_den_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_dwe_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_req_UNCONNECTED;
  wire NLW_PCS_PMA_core_en_cdet_UNCONNECTED;
  wire NLW_PCS_PMA_core_ewrap_UNCONNECTED;
  wire NLW_PCS_PMA_core_loc_ref_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_out_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_tri_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_PCS_PMA_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101010000" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "PCS_PMA" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_8 PCS_PMA_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_PCS_PMA_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_PCS_PMA_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(NLW_PCS_PMA_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_PCS_PMA_core_drp_den_UNCONNECTED),
        .drp_di(NLW_PCS_PMA_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_PCS_PMA_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_PCS_PMA_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_PCS_PMA_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_PCS_PMA_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_PCS_PMA_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_PCS_PMA_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_PCS_PMA_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(pma_reset_out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_PCS_PMA_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_PCS_PMA_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_PCS_PMA_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(userclk2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_sync_block sync_block_reset_done
       (.data_in(resetdone_i),
        .resetdone(resetdone),
        .userclk2(userclk2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispval),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(resetdone_i),
        .enablealign(enablealign),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .mgt_tx_reset(mgt_tx_reset),
        .pma_reset_out(pma_reset_out),
        .powerdown(powerdown),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk_out(rxoutclk_out),
        .rxp(rxp),
        .txbuferr(txbuferr),
        .txchardispmode_reg_reg_0(txchardispmode),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk_out(txoutclk_out),
        .txp(txp),
        .userclk2(userclk2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_clocking
   (gtrefclk_out,
    userclk2,
    userclk,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    rxoutclk,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output gtrefclk_out;
  output userclk2;
  output userclk;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input rxoutclk;
  output lopt;
  output lopt_1;
  input lopt_2;
  input lopt_3;
  input lopt_4;
  input lopt_5;

  wire \<const0> ;
  wire \<const1> ;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire \^lopt ;
  wire \^lopt_1 ;
  wire \^lopt_2 ;
  wire \^lopt_3 ;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;

  assign \^lopt  = lopt_2;
  assign \^lopt_1  = lopt_3;
  assign \^lopt_2  = lopt_4;
  assign \^lopt_3  = lopt_5;
  assign lopt = \<const1> ;
  assign lopt_1 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE3 #(
    .REFCLK_EN_TX_PATH(1'b0),
    .REFCLK_HROW_CK_SEL(2'b00),
    .REFCLK_ICNTL_RX(2'b00)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    rxrecclk_bufg_inst
       (.CE(\^lopt ),
        .CEMASK(1'b1),
        .CLR(\^lopt_1 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(rxoutclk),
        .O(rxuserclk2_out));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk2_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(txoutclk),
        .O(userclk2));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b1}),
        .I(txoutclk),
        .O(userclk));
endmodule

(* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt
   (gtwiz_userclk_tx_active_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    cpllrefclksel_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drpwe_in,
    eyescanreset_in,
    eyescantrigger_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    gtrefclk1_in,
    loopback_in,
    pcsrsvdin_in,
    rx8b10ben_in,
    rxbufreset_in,
    rxcdrhold_in,
    rxcommadeten_in,
    rxdfelpmreset_in,
    rxlpmen_in,
    rxmcommaalignen_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxrate_in,
    rxusrclk_in,
    rxusrclk2_in,
    tx8b10ben_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdiffctrl_in,
    txelecidle_in,
    txinhibit_in,
    txpcsreset_in,
    txpd_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txusrclk_in,
    txusrclk2_in,
    cplllock_out,
    dmonitorout_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxclkcorcnt_out,
    rxcommadet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxresetdone_out,
    txbufstatus_out,
    txoutclk_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txresetdone_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [2:0]cpllrefclksel_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drpwe_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [2:0]loopback_in;
  input [15:0]pcsrsvdin_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcommadeten_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [2:0]rxrate_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]tx8b10ben_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txelecidle_in;
  input [0:0]txinhibit_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]cplllock_out;
  output [16:0]dmonitorout_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcommadet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxresetdone_out;
  output [1:0]txbufstatus_out;
  output [0:0]txoutclk_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txresetdone_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [2:0]NLW_inst_bufgtce_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtcemask_out_UNCONNECTED;
  wire [8:0]NLW_inst_bufgtdiv_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtreset_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtrstmask_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllfbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_cplllock_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllrefclklost_out_UNCONNECTED;
  wire [16:0]NLW_inst_dmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_dmonitoroutclk_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_common_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_common_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtrefclkmonitor_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxn_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxp_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierategen3_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierateidle_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllpd_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllreset_out_UNCONNECTED;
  wire [0:0]NLW_inst_pciesynctxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieusergen3rdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserphystatusrst_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserratestart_out_UNCONNECTED;
  wire [11:0]NLW_inst_pcsrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_phystatus_out_UNCONNECTED;
  wire [7:0]NLW_inst_pinrsrvdas_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout0_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout1_out_UNCONNECTED;
  wire [0:0]NLW_inst_powerpresent_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0refclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1refclklost_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor0_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor0_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_resetexception_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrlock_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrphdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanbondseq_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanrealign_out_UNCONNECTED;
  wire [4:0]NLW_inst_rxchbondo_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxckcaldone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcominitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcommadet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomsasdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomwakedet_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl3_out_UNCONNECTED;
  wire [127:0]NLW_inst_rxdata_out_UNCONNECTED;
  wire [7:0]NLW_inst_rxdataextendrsvd_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxdatavalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxelecidle_out_UNCONNECTED;
  wire [5:0]NLW_inst_rxheader_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxheadervalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpstresetdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED;
  wire [6:0]NLW_inst_rxmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobestarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphalignerr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbslocked_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxratedone_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk0_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk0sel_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk1_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk1sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclkout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsliderdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipoutclkrdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslippmardy_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxstartofseq_out_UNCONNECTED;
  wire [2:0]NLW_inst_rxstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxvalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_tcongpo_out_UNCONNECTED;
  wire [0:0]NLW_inst_tconrsvdout0_out_UNCONNECTED;
  wire [0:0]NLW_inst_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_txcomfinish_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdccdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphinitdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_txratedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdaddr_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubden_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdi_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdwe_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubmdmtdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubtxuart_out_UNCONNECTED;

  assign cplllock_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000" *) 
  (* C_COMMON_SCALING_FACTOR = "1" *) 
  (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
  (* C_ENABLE_COMMON_USRCLK = "0" *) 
  (* C_FORCE_COMMONS = "0" *) 
  (* C_FREERUN_FREQUENCY = "62.500000" *) 
  (* C_GT_REV = "17" *) 
  (* C_GT_TYPE = "0" *) 
  (* C_INCLUDE_CPLL_CAL = "2" *) 
  (* C_LOCATE_COMMON = "0" *) 
  (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) 
  (* C_LOCATE_RESET_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_TX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) 
  (* C_PCIE_CORECLK_FREQ = "250" *) 
  (* C_PCIE_ENABLE = "0" *) 
  (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) 
  (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
  (* C_RX_BUFFBYPASS_MODE = "0" *) 
  (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_RX_BUFFER_MODE = "1" *) 
  (* C_RX_CB_DISP = "8'b00000000" *) 
  (* C_RX_CB_K = "8'b00000000" *) 
  (* C_RX_CB_LEN_SEQ = "1" *) 
  (* C_RX_CB_MAX_LEVEL = "1" *) 
  (* C_RX_CB_NUM_SEQ = "0" *) 
  (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_CC_DISP = "8'b00000000" *) 
  (* C_RX_CC_ENABLE = "1" *) 
  (* C_RX_CC_K = "8'b00010001" *) 
  (* C_RX_CC_LEN_SEQ = "2" *) 
  (* C_RX_CC_NUM_SEQ = "2" *) 
  (* C_RX_CC_PERIODICITY = "5000" *) 
  (* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) 
  (* C_RX_COMMA_M_ENABLE = "1" *) 
  (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
  (* C_RX_COMMA_P_ENABLE = "1" *) 
  (* C_RX_COMMA_P_VAL = "10'b0101111100" *) 
  (* C_RX_DATA_DECODING = "1" *) 
  (* C_RX_ENABLE = "1" *) 
  (* C_RX_INT_DATA_WIDTH = "20" *) 
  (* C_RX_LINE_RATE = "1.250000" *) 
  (* C_RX_MASTER_CHANNEL_IDX = "9" *) 
  (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) 
  (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_RX_OUTCLK_SOURCE = "1" *) 
  (* C_RX_PLL_TYPE = "2" *) 
  (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_RX_SLIDE_MODE = "0" *) 
  (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_RX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_RX_USER_DATA_WIDTH = "16" *) 
  (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_RX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_SECONDARY_QPLL_ENABLE = "0" *) 
  (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
  (* C_SIM_CPLL_CAL_BYPASS = "1" *) 
  (* C_TOTAL_NUM_CHANNELS = "1" *) 
  (* C_TOTAL_NUM_COMMONS = "0" *) 
  (* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) 
  (* C_TXPROGDIV_FREQ_ENABLE = "1" *) 
  (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
  (* C_TXPROGDIV_FREQ_VAL = "125.000000" *) 
  (* C_TX_BUFFBYPASS_MODE = "0" *) 
  (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_TX_BUFFER_MODE = "1" *) 
  (* C_TX_DATA_ENCODING = "1" *) 
  (* C_TX_ENABLE = "1" *) 
  (* C_TX_INT_DATA_WIDTH = "20" *) 
  (* C_TX_LINE_RATE = "1.250000" *) 
  (* C_TX_MASTER_CHANNEL_IDX = "9" *) 
  (* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) 
  (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_TX_OUTCLK_SOURCE = "4" *) 
  (* C_TX_PLL_TYPE = "2" *) 
  (* C_TX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_TX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_TX_USER_DATA_WIDTH = "16" *) 
  (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_TX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_top inst
       (.bgbypassb_in(1'b1),
        .bgmonitorenb_in(1'b1),
        .bgpdb_in(1'b1),
        .bgrcalovrd_in({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .bgrcalovrdenb_in(1'b1),
        .bufgtce_out(NLW_inst_bufgtce_out_UNCONNECTED[2:0]),
        .bufgtcemask_out(NLW_inst_bufgtcemask_out_UNCONNECTED[2:0]),
        .bufgtdiv_out(NLW_inst_bufgtdiv_out_UNCONNECTED[8:0]),
        .bufgtreset_out(NLW_inst_bufgtreset_out_UNCONNECTED[2:0]),
        .bufgtrstmask_out(NLW_inst_bufgtrstmask_out_UNCONNECTED[2:0]),
        .cdrstepdir_in(1'b0),
        .cdrstepsq_in(1'b0),
        .cdrstepsx_in(1'b0),
        .cfgreset_in(1'b0),
        .clkrsvd0_in(1'b0),
        .clkrsvd1_in(1'b0),
        .cpllfbclklost_out(NLW_inst_cpllfbclklost_out_UNCONNECTED[0]),
        .cpllfreqlock_in(1'b0),
        .cplllock_out(NLW_inst_cplllock_out_UNCONNECTED[0]),
        .cplllockdetclk_in(1'b0),
        .cplllocken_in(1'b1),
        .cpllpd_in(1'b0),
        .cpllrefclklost_out(NLW_inst_cpllrefclklost_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .cpllreset_in(1'b0),
        .dmonfiforeset_in(1'b0),
        .dmonitorclk_in(1'b0),
        .dmonitorout_out(NLW_inst_dmonitorout_out_UNCONNECTED[16:0]),
        .dmonitoroutclk_out(NLW_inst_dmonitoroutclk_out_UNCONNECTED[0]),
        .drpaddr_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_common_in(1'b0),
        .drpclk_in(drpclk_in),
        .drpdi_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_common_out(NLW_inst_drpdo_common_out_UNCONNECTED[15:0]),
        .drpdo_out(NLW_inst_drpdo_out_UNCONNECTED[15:0]),
        .drpen_common_in(1'b0),
        .drpen_in(1'b0),
        .drprdy_common_out(NLW_inst_drprdy_common_out_UNCONNECTED[0]),
        .drprdy_out(NLW_inst_drprdy_out_UNCONNECTED[0]),
        .drprst_in(1'b0),
        .drpwe_common_in(1'b0),
        .drpwe_in(1'b0),
        .elpcaldvorwren_in(1'b0),
        .elpcalpaorwren_in(1'b0),
        .evoddphicaldone_in(1'b0),
        .evoddphicalstart_in(1'b0),
        .evoddphidrden_in(1'b0),
        .evoddphidwren_in(1'b0),
        .evoddphixrden_in(1'b0),
        .evoddphixwren_in(1'b0),
        .eyescandataerror_out(NLW_inst_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanmode_in(1'b0),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .freqos_in(1'b0),
        .gtgrefclk0_in(1'b0),
        .gtgrefclk1_in(1'b0),
        .gtgrefclk_in(1'b0),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtnorthrefclk00_in(1'b0),
        .gtnorthrefclk01_in(1'b0),
        .gtnorthrefclk0_in(1'b0),
        .gtnorthrefclk10_in(1'b0),
        .gtnorthrefclk11_in(1'b0),
        .gtnorthrefclk1_in(1'b0),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk00_in(1'b0),
        .gtrefclk01_in(1'b0),
        .gtrefclk0_in(gtrefclk0_in),
        .gtrefclk10_in(1'b0),
        .gtrefclk11_in(1'b0),
        .gtrefclk1_in(1'b0),
        .gtrefclkmonitor_out(NLW_inst_gtrefclkmonitor_out_UNCONNECTED[0]),
        .gtresetsel_in(1'b0),
        .gtrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtrxreset_in(1'b0),
        .gtrxresetsel_in(1'b0),
        .gtsouthrefclk00_in(1'b0),
        .gtsouthrefclk01_in(1'b0),
        .gtsouthrefclk0_in(1'b0),
        .gtsouthrefclk10_in(1'b0),
        .gtsouthrefclk11_in(1'b0),
        .gtsouthrefclk1_in(1'b0),
        .gttxreset_in(1'b0),
        .gttxresetsel_in(1'b0),
        .gtwiz_buffbypass_rx_done_out(NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_error_out(NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_reset_in(1'b0),
        .gtwiz_buffbypass_rx_start_user_in(1'b0),
        .gtwiz_buffbypass_tx_done_out(NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_error_out(NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_reset_in(1'b0),
        .gtwiz_buffbypass_tx_start_user_in(1'b0),
        .gtwiz_gthe3_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe3_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe3_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gtye4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_qpll0lock_in(1'b0),
        .gtwiz_reset_qpll0reset_out(NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED[0]),
        .gtwiz_reset_qpll1lock_in(1'b0),
        .gtwiz_reset_qpll1reset_out(NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_cdr_stable_out(NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_in(1'b0),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_in(1'b0),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_rx_active_out(NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_reset_in(1'b0),
        .gtwiz_userclk_rx_srcclk_out(NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk2_out(NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk_out(NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userclk_tx_active_out(NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_reset_in(1'b0),
        .gtwiz_userclk_tx_srcclk_out(NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk2_out(NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk_out(NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .gtyrxn_in(1'b0),
        .gtyrxp_in(1'b0),
        .gtytxn_out(NLW_inst_gtytxn_out_UNCONNECTED[0]),
        .gtytxp_out(NLW_inst_gtytxp_out_UNCONNECTED[0]),
        .incpctrl_in(1'b0),
        .loopback_in({1'b0,1'b0,1'b0}),
        .looprsvd_in(1'b0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .lpbkrxtxseren_in(1'b0),
        .lpbktxrxseren_in(1'b0),
        .pcieeqrxeqadaptdone_in(1'b0),
        .pcierategen3_out(NLW_inst_pcierategen3_out_UNCONNECTED[0]),
        .pcierateidle_out(NLW_inst_pcierateidle_out_UNCONNECTED[0]),
        .pcierateqpll0_in(1'b0),
        .pcierateqpll1_in(1'b0),
        .pcierateqpllpd_out(NLW_inst_pcierateqpllpd_out_UNCONNECTED[1:0]),
        .pcierateqpllreset_out(NLW_inst_pcierateqpllreset_out_UNCONNECTED[1:0]),
        .pcierstidle_in(1'b0),
        .pciersttxsyncstart_in(1'b0),
        .pciesynctxsyncdone_out(NLW_inst_pciesynctxsyncdone_out_UNCONNECTED[0]),
        .pcieusergen3rdy_out(NLW_inst_pcieusergen3rdy_out_UNCONNECTED[0]),
        .pcieuserphystatusrst_out(NLW_inst_pcieuserphystatusrst_out_UNCONNECTED[0]),
        .pcieuserratedone_in(1'b0),
        .pcieuserratestart_out(NLW_inst_pcieuserratestart_out_UNCONNECTED[0]),
        .pcsrsvdin2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdout_out(NLW_inst_pcsrsvdout_out_UNCONNECTED[11:0]),
        .phystatus_out(NLW_inst_phystatus_out_UNCONNECTED[0]),
        .pinrsrvdas_out(NLW_inst_pinrsrvdas_out_UNCONNECTED[7:0]),
        .pmarsvd0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdout0_out(NLW_inst_pmarsvdout0_out_UNCONNECTED[7:0]),
        .pmarsvdout1_out(NLW_inst_pmarsvdout1_out_UNCONNECTED[7:0]),
        .powerpresent_out(NLW_inst_powerpresent_out_UNCONNECTED[0]),
        .qpll0clk_in(1'b0),
        .qpll0clkrsvd0_in(1'b0),
        .qpll0clkrsvd1_in(1'b0),
        .qpll0fbclklost_out(NLW_inst_qpll0fbclklost_out_UNCONNECTED[0]),
        .qpll0fbdiv_in(1'b0),
        .qpll0freqlock_in(1'b0),
        .qpll0lock_out(NLW_inst_qpll0lock_out_UNCONNECTED[0]),
        .qpll0lockdetclk_in(1'b0),
        .qpll0locken_in(1'b0),
        .qpll0outclk_out(NLW_inst_qpll0outclk_out_UNCONNECTED[0]),
        .qpll0outrefclk_out(NLW_inst_qpll0outrefclk_out_UNCONNECTED[0]),
        .qpll0pd_in(1'b1),
        .qpll0refclk_in(1'b0),
        .qpll0refclklost_out(NLW_inst_qpll0refclklost_out_UNCONNECTED[0]),
        .qpll0refclksel_in({1'b0,1'b0,1'b1}),
        .qpll0reset_in(1'b1),
        .qpll1clk_in(1'b0),
        .qpll1clkrsvd0_in(1'b0),
        .qpll1clkrsvd1_in(1'b0),
        .qpll1fbclklost_out(NLW_inst_qpll1fbclklost_out_UNCONNECTED[0]),
        .qpll1fbdiv_in(1'b0),
        .qpll1freqlock_in(1'b0),
        .qpll1lock_out(NLW_inst_qpll1lock_out_UNCONNECTED[0]),
        .qpll1lockdetclk_in(1'b0),
        .qpll1locken_in(1'b0),
        .qpll1outclk_out(NLW_inst_qpll1outclk_out_UNCONNECTED[0]),
        .qpll1outrefclk_out(NLW_inst_qpll1outrefclk_out_UNCONNECTED[0]),
        .qpll1pd_in(1'b1),
        .qpll1refclk_in(1'b0),
        .qpll1refclklost_out(NLW_inst_qpll1refclklost_out_UNCONNECTED[0]),
        .qpll1refclksel_in({1'b0,1'b0,1'b1}),
        .qpll1reset_in(1'b1),
        .qplldmonitor0_out(NLW_inst_qplldmonitor0_out_UNCONNECTED[7:0]),
        .qplldmonitor1_out(NLW_inst_qplldmonitor1_out_UNCONNECTED[7:0]),
        .qpllrsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd3_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd4_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rcalenb_in(1'b1),
        .refclkoutmonitor0_out(NLW_inst_refclkoutmonitor0_out_UNCONNECTED[0]),
        .refclkoutmonitor1_out(NLW_inst_refclkoutmonitor1_out_UNCONNECTED[0]),
        .resetexception_out(NLW_inst_resetexception_out_UNCONNECTED[0]),
        .resetovrd_in(1'b0),
        .rstclkentx_in(1'b0),
        .rx8b10ben_in(1'b1),
        .rxafecfoken_in(1'b0),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({\^rxbufstatus_out ,NLW_inst_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_inst_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_inst_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrfreqreset_in(1'b0),
        .rxcdrhold_in(1'b0),
        .rxcdrlock_out(NLW_inst_rxcdrlock_out_UNCONNECTED[0]),
        .rxcdrovrden_in(1'b0),
        .rxcdrphdone_out(NLW_inst_rxcdrphdone_out_UNCONNECTED[0]),
        .rxcdrreset_in(1'b0),
        .rxcdrresetrsv_in(1'b0),
        .rxchanbondseq_out(NLW_inst_rxchanbondseq_out_UNCONNECTED[0]),
        .rxchanisaligned_out(NLW_inst_rxchanisaligned_out_UNCONNECTED[0]),
        .rxchanrealign_out(NLW_inst_rxchanrealign_out_UNCONNECTED[0]),
        .rxchbonden_in(1'b0),
        .rxchbondi_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rxchbondlevel_in({1'b0,1'b0,1'b0}),
        .rxchbondmaster_in(1'b0),
        .rxchbondo_out(NLW_inst_rxchbondo_out_UNCONNECTED[4:0]),
        .rxchbondslave_in(1'b0),
        .rxckcaldone_out(NLW_inst_rxckcaldone_out_UNCONNECTED[0]),
        .rxckcalreset_in(1'b0),
        .rxckcalstart_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxcominitdet_out(NLW_inst_rxcominitdet_out_UNCONNECTED[0]),
        .rxcommadet_out(NLW_inst_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxcomsasdet_out(NLW_inst_rxcomsasdet_out_UNCONNECTED[0]),
        .rxcomwakedet_out(NLW_inst_rxcomwakedet_out_UNCONNECTED[0]),
        .rxctrl0_out({NLW_inst_rxctrl0_out_UNCONNECTED[15:2],\^rxctrl0_out }),
        .rxctrl1_out({NLW_inst_rxctrl1_out_UNCONNECTED[15:2],\^rxctrl1_out }),
        .rxctrl2_out({NLW_inst_rxctrl2_out_UNCONNECTED[7:2],\^rxctrl2_out }),
        .rxctrl3_out({NLW_inst_rxctrl3_out_UNCONNECTED[7:2],\^rxctrl3_out }),
        .rxdata_out(NLW_inst_rxdata_out_UNCONNECTED[127:0]),
        .rxdataextendrsvd_out(NLW_inst_rxdataextendrsvd_out_UNCONNECTED[7:0]),
        .rxdatavalid_out(NLW_inst_rxdatavalid_out_UNCONNECTED[1:0]),
        .rxdccforcestart_in(1'b0),
        .rxdfeagcctrl_in({1'b0,1'b1}),
        .rxdfeagchold_in(1'b0),
        .rxdfeagcovrden_in(1'b0),
        .rxdfecfokfcnum_in(1'b0),
        .rxdfecfokfen_in(1'b0),
        .rxdfecfokfpulse_in(1'b0),
        .rxdfecfokhold_in(1'b0),
        .rxdfecfokovren_in(1'b0),
        .rxdfekhhold_in(1'b0),
        .rxdfekhovrden_in(1'b0),
        .rxdfelfhold_in(1'b0),
        .rxdfelfovrden_in(1'b0),
        .rxdfelpmreset_in(1'b0),
        .rxdfetap10hold_in(1'b0),
        .rxdfetap10ovrden_in(1'b0),
        .rxdfetap11hold_in(1'b0),
        .rxdfetap11ovrden_in(1'b0),
        .rxdfetap12hold_in(1'b0),
        .rxdfetap12ovrden_in(1'b0),
        .rxdfetap13hold_in(1'b0),
        .rxdfetap13ovrden_in(1'b0),
        .rxdfetap14hold_in(1'b0),
        .rxdfetap14ovrden_in(1'b0),
        .rxdfetap15hold_in(1'b0),
        .rxdfetap15ovrden_in(1'b0),
        .rxdfetap2hold_in(1'b0),
        .rxdfetap2ovrden_in(1'b0),
        .rxdfetap3hold_in(1'b0),
        .rxdfetap3ovrden_in(1'b0),
        .rxdfetap4hold_in(1'b0),
        .rxdfetap4ovrden_in(1'b0),
        .rxdfetap5hold_in(1'b0),
        .rxdfetap5ovrden_in(1'b0),
        .rxdfetap6hold_in(1'b0),
        .rxdfetap6ovrden_in(1'b0),
        .rxdfetap7hold_in(1'b0),
        .rxdfetap7ovrden_in(1'b0),
        .rxdfetap8hold_in(1'b0),
        .rxdfetap8ovrden_in(1'b0),
        .rxdfetap9hold_in(1'b0),
        .rxdfetap9ovrden_in(1'b0),
        .rxdfeuthold_in(1'b0),
        .rxdfeutovrden_in(1'b0),
        .rxdfevphold_in(1'b0),
        .rxdfevpovrden_in(1'b0),
        .rxdfevsen_in(1'b0),
        .rxdfexyden_in(1'b1),
        .rxdlybypass_in(1'b1),
        .rxdlyen_in(1'b0),
        .rxdlyovrden_in(1'b0),
        .rxdlysreset_in(1'b0),
        .rxdlysresetdone_out(NLW_inst_rxdlysresetdone_out_UNCONNECTED[0]),
        .rxelecidle_out(NLW_inst_rxelecidle_out_UNCONNECTED[0]),
        .rxelecidlemode_in({1'b1,1'b1}),
        .rxeqtraining_in(1'b0),
        .rxgearboxslip_in(1'b0),
        .rxheader_out(NLW_inst_rxheader_out_UNCONNECTED[5:0]),
        .rxheadervalid_out(NLW_inst_rxheadervalid_out_UNCONNECTED[1:0]),
        .rxlatclk_in(1'b0),
        .rxlfpstresetdet_out(NLW_inst_rxlfpstresetdet_out_UNCONNECTED[0]),
        .rxlfpsu2lpexitdet_out(NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED[0]),
        .rxlfpsu3wakedet_out(NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED[0]),
        .rxlpmen_in(1'b1),
        .rxlpmgchold_in(1'b0),
        .rxlpmgcovrden_in(1'b0),
        .rxlpmhfhold_in(1'b0),
        .rxlpmhfovrden_in(1'b0),
        .rxlpmlfhold_in(1'b0),
        .rxlpmlfklovrden_in(1'b0),
        .rxlpmoshold_in(1'b0),
        .rxlpmosovrden_in(1'b0),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxmonitorout_out(NLW_inst_rxmonitorout_out_UNCONNECTED[6:0]),
        .rxmonitorsel_in({1'b0,1'b0}),
        .rxoobreset_in(1'b0),
        .rxoscalreset_in(1'b0),
        .rxoshold_in(1'b0),
        .rxosintcfg_in({1'b1,1'b1,1'b0,1'b1}),
        .rxosintdone_out(NLW_inst_rxosintdone_out_UNCONNECTED[0]),
        .rxosinten_in(1'b1),
        .rxosinthold_in(1'b0),
        .rxosintovrden_in(1'b0),
        .rxosintstarted_out(NLW_inst_rxosintstarted_out_UNCONNECTED[0]),
        .rxosintstrobe_in(1'b0),
        .rxosintstrobedone_out(NLW_inst_rxosintstrobedone_out_UNCONNECTED[0]),
        .rxosintstrobestarted_out(NLW_inst_rxosintstrobestarted_out_UNCONNECTED[0]),
        .rxosinttestovrden_in(1'b0),
        .rxosovrden_in(1'b0),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkfabric_out(NLW_inst_rxoutclkfabric_out_UNCONNECTED[0]),
        .rxoutclkpcs_out(NLW_inst_rxoutclkpcs_out_UNCONNECTED[0]),
        .rxoutclksel_in({1'b0,1'b1,1'b0}),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpd_in[1],1'b0}),
        .rxphalign_in(1'b0),
        .rxphaligndone_out(NLW_inst_rxphaligndone_out_UNCONNECTED[0]),
        .rxphalignen_in(1'b0),
        .rxphalignerr_out(NLW_inst_rxphalignerr_out_UNCONNECTED[0]),
        .rxphdlypd_in(1'b1),
        .rxphdlyreset_in(1'b0),
        .rxphovrden_in(1'b0),
        .rxpllclksel_in({1'b0,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_inst_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_inst_rxprbserr_out_UNCONNECTED[0]),
        .rxprbslocked_out(NLW_inst_rxprbslocked_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxprgdivresetdone_out(NLW_inst_rxprgdivresetdone_out_UNCONNECTED[0]),
        .rxprogdivreset_in(1'b0),
        .rxqpien_in(1'b0),
        .rxqpisenn_out(NLW_inst_rxqpisenn_out_UNCONNECTED[0]),
        .rxqpisenp_out(NLW_inst_rxqpisenp_out_UNCONNECTED[0]),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxratedone_out(NLW_inst_rxratedone_out_UNCONNECTED[0]),
        .rxratemode_in(1'b0),
        .rxrecclk0_sel_out(NLW_inst_rxrecclk0_sel_out_UNCONNECTED[1:0]),
        .rxrecclk0sel_out(NLW_inst_rxrecclk0sel_out_UNCONNECTED[0]),
        .rxrecclk1_sel_out(NLW_inst_rxrecclk1_sel_out_UNCONNECTED[1:0]),
        .rxrecclk1sel_out(NLW_inst_rxrecclk1sel_out_UNCONNECTED[0]),
        .rxrecclkout_out(NLW_inst_rxrecclkout_out_UNCONNECTED[0]),
        .rxresetdone_out(NLW_inst_rxresetdone_out_UNCONNECTED[0]),
        .rxslide_in(1'b0),
        .rxsliderdy_out(NLW_inst_rxsliderdy_out_UNCONNECTED[0]),
        .rxslipdone_out(NLW_inst_rxslipdone_out_UNCONNECTED[0]),
        .rxslipoutclk_in(1'b0),
        .rxslipoutclkrdy_out(NLW_inst_rxslipoutclkrdy_out_UNCONNECTED[0]),
        .rxslippma_in(1'b0),
        .rxslippmardy_out(NLW_inst_rxslippmardy_out_UNCONNECTED[0]),
        .rxstartofseq_out(NLW_inst_rxstartofseq_out_UNCONNECTED[1:0]),
        .rxstatus_out(NLW_inst_rxstatus_out_UNCONNECTED[2:0]),
        .rxsyncallin_in(1'b0),
        .rxsyncdone_out(NLW_inst_rxsyncdone_out_UNCONNECTED[0]),
        .rxsyncin_in(1'b0),
        .rxsyncmode_in(1'b0),
        .rxsyncout_out(NLW_inst_rxsyncout_out_UNCONNECTED[0]),
        .rxsysclksel_in({1'b0,1'b0}),
        .rxtermination_in(1'b0),
        .rxuserrdy_in(1'b1),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(rxusrclk_in),
        .rxvalid_out(NLW_inst_rxvalid_out_UNCONNECTED[0]),
        .sdm0data_in(1'b0),
        .sdm0finalout_out(NLW_inst_sdm0finalout_out_UNCONNECTED[0]),
        .sdm0reset_in(1'b0),
        .sdm0testdata_out(NLW_inst_sdm0testdata_out_UNCONNECTED[0]),
        .sdm0toggle_in(1'b0),
        .sdm0width_in(1'b0),
        .sdm1data_in(1'b0),
        .sdm1finalout_out(NLW_inst_sdm1finalout_out_UNCONNECTED[0]),
        .sdm1reset_in(1'b0),
        .sdm1testdata_out(NLW_inst_sdm1testdata_out_UNCONNECTED[0]),
        .sdm1toggle_in(1'b0),
        .sdm1width_in(1'b0),
        .sigvalidclk_in(1'b0),
        .tcongpi_in(1'b0),
        .tcongpo_out(NLW_inst_tcongpo_out_UNCONNECTED[0]),
        .tconpowerup_in(1'b0),
        .tconreset_in(1'b0),
        .tconrsvdin1_in(1'b0),
        .tconrsvdout0_out(NLW_inst_tconrsvdout0_out_UNCONNECTED[0]),
        .tstin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10bbypass_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10ben_in(1'b1),
        .txbufdiffctrl_in({1'b0,1'b0,1'b0}),
        .txbufstatus_out({\^txbufstatus_out ,NLW_inst_txbufstatus_out_UNCONNECTED[0]}),
        .txcomfinish_out(NLW_inst_txcomfinish_out_UNCONNECTED[0]),
        .txcominit_in(1'b0),
        .txcomsas_in(1'b0),
        .txcomwake_in(1'b0),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in[1:0]}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in[1:0]}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in[1:0]}),
        .txdata_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdataextendrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdccdone_out(NLW_inst_txdccdone_out_UNCONNECTED[0]),
        .txdccforcestart_in(1'b0),
        .txdccreset_in(1'b0),
        .txdeemph_in(1'b0),
        .txdetectrx_in(1'b0),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txdiffpd_in(1'b0),
        .txdlybypass_in(1'b1),
        .txdlyen_in(1'b0),
        .txdlyhold_in(1'b0),
        .txdlyovrden_in(1'b0),
        .txdlysreset_in(1'b0),
        .txdlysresetdone_out(NLW_inst_txdlysresetdone_out_UNCONNECTED[0]),
        .txdlyupdown_in(1'b0),
        .txelecidle_in(txelecidle_in),
        .txelforcestart_in(1'b0),
        .txheader_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txinhibit_in(1'b0),
        .txlatclk_in(1'b0),
        .txlfpstreset_in(1'b0),
        .txlfpsu2lpexit_in(1'b0),
        .txlfpsu3wake_in(1'b0),
        .txmaincursor_in({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txmargin_in({1'b0,1'b0,1'b0}),
        .txmuxdcdexhold_in(1'b0),
        .txmuxdcdorwren_in(1'b0),
        .txoneszeros_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txoutclkfabric_out(NLW_inst_txoutclkfabric_out_UNCONNECTED[0]),
        .txoutclkpcs_out(NLW_inst_txoutclkpcs_out_UNCONNECTED[0]),
        .txoutclksel_in({1'b1,1'b0,1'b1}),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpdelecidlemode_in(1'b0),
        .txphalign_in(1'b0),
        .txphaligndone_out(NLW_inst_txphaligndone_out_UNCONNECTED[0]),
        .txphalignen_in(1'b0),
        .txphdlypd_in(1'b1),
        .txphdlyreset_in(1'b0),
        .txphdlytstclk_in(1'b0),
        .txphinit_in(1'b0),
        .txphinitdone_out(NLW_inst_txphinitdone_out_UNCONNECTED[0]),
        .txphovrden_in(1'b0),
        .txpippmen_in(1'b0),
        .txpippmovrden_in(1'b0),
        .txpippmpd_in(1'b0),
        .txpippmsel_in(1'b0),
        .txpippmstepsize_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpisopd_in(1'b0),
        .txpllclksel_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_inst_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpostcursorinv_in(1'b0),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprecursorinv_in(1'b0),
        .txprgdivresetdone_out(NLW_inst_txprgdivresetdone_out_UNCONNECTED[0]),
        .txprogdivreset_in(1'b0),
        .txqpibiasen_in(1'b0),
        .txqpisenn_out(NLW_inst_txqpisenn_out_UNCONNECTED[0]),
        .txqpisenp_out(NLW_inst_txqpisenp_out_UNCONNECTED[0]),
        .txqpistrongpdown_in(1'b0),
        .txqpiweakpup_in(1'b0),
        .txrate_in({1'b0,1'b0,1'b0}),
        .txratedone_out(NLW_inst_txratedone_out_UNCONNECTED[0]),
        .txratemode_in(1'b0),
        .txresetdone_out(NLW_inst_txresetdone_out_UNCONNECTED[0]),
        .txsequence_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txswing_in(1'b0),
        .txsyncallin_in(1'b0),
        .txsyncdone_out(NLW_inst_txsyncdone_out_UNCONNECTED[0]),
        .txsyncin_in(1'b0),
        .txsyncmode_in(1'b0),
        .txsyncout_out(NLW_inst_txsyncout_out_UNCONNECTED[0]),
        .txsysclksel_in({1'b0,1'b0}),
        .txuserrdy_in(1'b1),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0),
        .ubcfgstreamen_in(1'b0),
        .ubdaddr_out(NLW_inst_ubdaddr_out_UNCONNECTED[0]),
        .ubden_out(NLW_inst_ubden_out_UNCONNECTED[0]),
        .ubdi_out(NLW_inst_ubdi_out_UNCONNECTED[0]),
        .ubdo_in(1'b0),
        .ubdrdy_in(1'b0),
        .ubdwe_out(NLW_inst_ubdwe_out_UNCONNECTED[0]),
        .ubenable_in(1'b0),
        .ubgpi_in(1'b0),
        .ubintr_in(1'b0),
        .ubiolmbrst_in(1'b0),
        .ubmbrst_in(1'b0),
        .ubmdmcapture_in(1'b0),
        .ubmdmdbgrst_in(1'b0),
        .ubmdmdbgupdate_in(1'b0),
        .ubmdmregen_in(1'b0),
        .ubmdmshift_in(1'b0),
        .ubmdmsysrst_in(1'b0),
        .ubmdmtck_in(1'b0),
        .ubmdmtdi_in(1'b0),
        .ubmdmtdo_out(NLW_inst_ubmdmtdo_out_UNCONNECTED[0]),
        .ubrsvdout_out(NLW_inst_ubrsvdout_out_UNCONNECTED[0]),
        .ubtxuart_out(NLW_inst_ubtxuart_out_UNCONNECTED[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gthe3_channel_wrapper
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gthe3_channel channel_inst
       (.cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(rxcdrlock_out),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .rxresetdone_out(rxresetdone_out),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(txresetdone_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_gthe3
   (gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxoutclk_out,
    txoutclk_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    rxmcommaalignen_in,
    rxusrclk_in,
    txelecidle_in,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_datapath_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gthe3_channel_wrapper \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_rxresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0 \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_txresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gtwiz_reset \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gtpowergood_out(gtpowergood_out),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in0(rst_in0),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxusrclk_in(rxusrclk_in));
endmodule

(* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000" *) (* C_COMMON_SCALING_FACTOR = "1" *) (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
(* C_ENABLE_COMMON_USRCLK = "0" *) (* C_FORCE_COMMONS = "0" *) (* C_FREERUN_FREQUENCY = "62.500000" *) 
(* C_GT_REV = "17" *) (* C_GT_TYPE = "0" *) (* C_INCLUDE_CPLL_CAL = "2" *) 
(* C_LOCATE_COMMON = "0" *) (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) (* C_LOCATE_RESET_CONTROLLER = "0" *) 
(* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) (* C_LOCATE_RX_USER_CLOCKING = "1" *) (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
(* C_LOCATE_TX_USER_CLOCKING = "1" *) (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) (* C_PCIE_CORECLK_FREQ = "250" *) 
(* C_PCIE_ENABLE = "0" *) (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
(* C_RX_BUFFBYPASS_MODE = "0" *) (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) (* C_RX_BUFFER_MODE = "1" *) 
(* C_RX_CB_DISP = "8'b00000000" *) (* C_RX_CB_K = "8'b00000000" *) (* C_RX_CB_LEN_SEQ = "1" *) 
(* C_RX_CB_MAX_LEVEL = "1" *) (* C_RX_CB_NUM_SEQ = "0" *) (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_CC_DISP = "8'b00000000" *) (* C_RX_CC_ENABLE = "1" *) (* C_RX_CC_K = "8'b00010001" *) 
(* C_RX_CC_LEN_SEQ = "2" *) (* C_RX_CC_NUM_SEQ = "2" *) (* C_RX_CC_PERIODICITY = "5000" *) 
(* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) (* C_RX_COMMA_M_ENABLE = "1" *) (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
(* C_RX_COMMA_P_ENABLE = "1" *) (* C_RX_COMMA_P_VAL = "10'b0101111100" *) (* C_RX_DATA_DECODING = "1" *) 
(* C_RX_ENABLE = "1" *) (* C_RX_INT_DATA_WIDTH = "20" *) (* C_RX_LINE_RATE = "1.250000" *) 
(* C_RX_MASTER_CHANNEL_IDX = "9" *) (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
(* C_RX_OUTCLK_SOURCE = "1" *) (* C_RX_PLL_TYPE = "2" *) (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_REFCLK_FREQUENCY = "156.250000" *) (* C_RX_SLIDE_MODE = "0" *) (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_RX_USER_CLOCKING_SOURCE = "0" *) (* C_RX_USER_DATA_WIDTH = "16" *) (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_RX_USRCLK_FREQUENCY = "62.500000" *) (* C_SECONDARY_QPLL_ENABLE = "0" *) (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
(* C_SIM_CPLL_CAL_BYPASS = "1" *) (* C_TOTAL_NUM_CHANNELS = "1" *) (* C_TOTAL_NUM_COMMONS = "0" *) 
(* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) (* C_TXPROGDIV_FREQ_ENABLE = "1" *) (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
(* C_TXPROGDIV_FREQ_VAL = "125.000000" *) (* C_TX_BUFFBYPASS_MODE = "0" *) (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
(* C_TX_BUFFER_MODE = "1" *) (* C_TX_DATA_ENCODING = "1" *) (* C_TX_ENABLE = "1" *) 
(* C_TX_INT_DATA_WIDTH = "20" *) (* C_TX_LINE_RATE = "1.250000" *) (* C_TX_MASTER_CHANNEL_IDX = "9" *) 
(* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) (* C_TX_OUTCLK_SOURCE = "4" *) 
(* C_TX_PLL_TYPE = "2" *) (* C_TX_REFCLK_FREQUENCY = "156.250000" *) (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_TX_USER_CLOCKING_SOURCE = "0" *) (* C_TX_USER_DATA_WIDTH = "16" *) (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_TX_USRCLK_FREQUENCY = "62.500000" *) (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_top
   (gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in,
    gtwiz_userclk_tx_srcclk_out,
    gtwiz_userclk_tx_usrclk_out,
    gtwiz_userclk_tx_usrclk2_out,
    gtwiz_userclk_tx_active_out,
    gtwiz_userclk_rx_reset_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_userclk_rx_srcclk_out,
    gtwiz_userclk_rx_usrclk_out,
    gtwiz_userclk_rx_usrclk2_out,
    gtwiz_userclk_rx_active_out,
    gtwiz_buffbypass_tx_reset_in,
    gtwiz_buffbypass_tx_start_user_in,
    gtwiz_buffbypass_tx_done_out,
    gtwiz_buffbypass_tx_error_out,
    gtwiz_buffbypass_rx_reset_in,
    gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out,
    gtwiz_buffbypass_rx_error_out,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_tx_done_in,
    gtwiz_reset_rx_done_in,
    gtwiz_reset_qpll0lock_in,
    gtwiz_reset_qpll1lock_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_reset_qpll0reset_out,
    gtwiz_reset_qpll1reset_out,
    gtwiz_gthe3_cpll_cal_txoutclk_period_in,
    gtwiz_gthe3_cpll_cal_cnt_tol_in,
    gtwiz_gthe3_cpll_cal_bufg_ce_in,
    gtwiz_gthe4_cpll_cal_txoutclk_period_in,
    gtwiz_gthe4_cpll_cal_cnt_tol_in,
    gtwiz_gthe4_cpll_cal_bufg_ce_in,
    gtwiz_gtye4_cpll_cal_txoutclk_period_in,
    gtwiz_gtye4_cpll_cal_cnt_tol_in,
    gtwiz_gtye4_cpll_cal_bufg_ce_in,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    bgbypassb_in,
    bgmonitorenb_in,
    bgpdb_in,
    bgrcalovrd_in,
    bgrcalovrdenb_in,
    drpaddr_common_in,
    drpclk_common_in,
    drpdi_common_in,
    drpen_common_in,
    drpwe_common_in,
    gtgrefclk0_in,
    gtgrefclk1_in,
    gtnorthrefclk00_in,
    gtnorthrefclk01_in,
    gtnorthrefclk10_in,
    gtnorthrefclk11_in,
    gtrefclk00_in,
    gtrefclk01_in,
    gtrefclk10_in,
    gtrefclk11_in,
    gtsouthrefclk00_in,
    gtsouthrefclk01_in,
    gtsouthrefclk10_in,
    gtsouthrefclk11_in,
    pcierateqpll0_in,
    pcierateqpll1_in,
    pmarsvd0_in,
    pmarsvd1_in,
    qpll0clkrsvd0_in,
    qpll0clkrsvd1_in,
    qpll0fbdiv_in,
    qpll0lockdetclk_in,
    qpll0locken_in,
    qpll0pd_in,
    qpll0refclksel_in,
    qpll0reset_in,
    qpll1clkrsvd0_in,
    qpll1clkrsvd1_in,
    qpll1fbdiv_in,
    qpll1lockdetclk_in,
    qpll1locken_in,
    qpll1pd_in,
    qpll1refclksel_in,
    qpll1reset_in,
    qpllrsvd1_in,
    qpllrsvd2_in,
    qpllrsvd3_in,
    qpllrsvd4_in,
    rcalenb_in,
    sdm0data_in,
    sdm0reset_in,
    sdm0toggle_in,
    sdm0width_in,
    sdm1data_in,
    sdm1reset_in,
    sdm1toggle_in,
    sdm1width_in,
    tcongpi_in,
    tconpowerup_in,
    tconreset_in,
    tconrsvdin1_in,
    ubcfgstreamen_in,
    ubdo_in,
    ubdrdy_in,
    ubenable_in,
    ubgpi_in,
    ubintr_in,
    ubiolmbrst_in,
    ubmbrst_in,
    ubmdmcapture_in,
    ubmdmdbgrst_in,
    ubmdmdbgupdate_in,
    ubmdmregen_in,
    ubmdmshift_in,
    ubmdmsysrst_in,
    ubmdmtck_in,
    ubmdmtdi_in,
    drpdo_common_out,
    drprdy_common_out,
    pmarsvdout0_out,
    pmarsvdout1_out,
    qpll0fbclklost_out,
    qpll0lock_out,
    qpll0outclk_out,
    qpll0outrefclk_out,
    qpll0refclklost_out,
    qpll1fbclklost_out,
    qpll1lock_out,
    qpll1outclk_out,
    qpll1outrefclk_out,
    qpll1refclklost_out,
    qplldmonitor0_out,
    qplldmonitor1_out,
    refclkoutmonitor0_out,
    refclkoutmonitor1_out,
    rxrecclk0_sel_out,
    rxrecclk1_sel_out,
    rxrecclk0sel_out,
    rxrecclk1sel_out,
    sdm0finalout_out,
    sdm0testdata_out,
    sdm1finalout_out,
    sdm1testdata_out,
    tcongpo_out,
    tconrsvdout0_out,
    ubdaddr_out,
    ubden_out,
    ubdi_out,
    ubdwe_out,
    ubmdmtdo_out,
    ubrsvdout_out,
    ubtxuart_out,
    cdrstepdir_in,
    cdrstepsq_in,
    cdrstepsx_in,
    cfgreset_in,
    clkrsvd0_in,
    clkrsvd1_in,
    cpllfreqlock_in,
    cplllockdetclk_in,
    cplllocken_in,
    cpllpd_in,
    cpllrefclksel_in,
    cpllreset_in,
    dmonfiforeset_in,
    dmonitorclk_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drprst_in,
    drpwe_in,
    elpcaldvorwren_in,
    elpcalpaorwren_in,
    evoddphicaldone_in,
    evoddphicalstart_in,
    evoddphidrden_in,
    evoddphidwren_in,
    evoddphixrden_in,
    evoddphixwren_in,
    eyescanmode_in,
    eyescanreset_in,
    eyescantrigger_in,
    freqos_in,
    gtgrefclk_in,
    gthrxn_in,
    gthrxp_in,
    gtnorthrefclk0_in,
    gtnorthrefclk1_in,
    gtrefclk0_in,
    gtrefclk1_in,
    gtresetsel_in,
    gtrsvd_in,
    gtrxreset_in,
    gtrxresetsel_in,
    gtsouthrefclk0_in,
    gtsouthrefclk1_in,
    gttxreset_in,
    gttxresetsel_in,
    incpctrl_in,
    gtyrxn_in,
    gtyrxp_in,
    loopback_in,
    looprsvd_in,
    lpbkrxtxseren_in,
    lpbktxrxseren_in,
    pcieeqrxeqadaptdone_in,
    pcierstidle_in,
    pciersttxsyncstart_in,
    pcieuserratedone_in,
    pcsrsvdin_in,
    pcsrsvdin2_in,
    pmarsvdin_in,
    qpll0clk_in,
    qpll0freqlock_in,
    qpll0refclk_in,
    qpll1clk_in,
    qpll1freqlock_in,
    qpll1refclk_in,
    resetovrd_in,
    rstclkentx_in,
    rx8b10ben_in,
    rxafecfoken_in,
    rxbufreset_in,
    rxcdrfreqreset_in,
    rxcdrhold_in,
    rxcdrovrden_in,
    rxcdrreset_in,
    rxcdrresetrsv_in,
    rxchbonden_in,
    rxchbondi_in,
    rxchbondlevel_in,
    rxchbondmaster_in,
    rxchbondslave_in,
    rxckcalreset_in,
    rxckcalstart_in,
    rxcommadeten_in,
    rxdfeagcctrl_in,
    rxdccforcestart_in,
    rxdfeagchold_in,
    rxdfeagcovrden_in,
    rxdfecfokfcnum_in,
    rxdfecfokfen_in,
    rxdfecfokfpulse_in,
    rxdfecfokhold_in,
    rxdfecfokovren_in,
    rxdfekhhold_in,
    rxdfekhovrden_in,
    rxdfelfhold_in,
    rxdfelfovrden_in,
    rxdfelpmreset_in,
    rxdfetap10hold_in,
    rxdfetap10ovrden_in,
    rxdfetap11hold_in,
    rxdfetap11ovrden_in,
    rxdfetap12hold_in,
    rxdfetap12ovrden_in,
    rxdfetap13hold_in,
    rxdfetap13ovrden_in,
    rxdfetap14hold_in,
    rxdfetap14ovrden_in,
    rxdfetap15hold_in,
    rxdfetap15ovrden_in,
    rxdfetap2hold_in,
    rxdfetap2ovrden_in,
    rxdfetap3hold_in,
    rxdfetap3ovrden_in,
    rxdfetap4hold_in,
    rxdfetap4ovrden_in,
    rxdfetap5hold_in,
    rxdfetap5ovrden_in,
    rxdfetap6hold_in,
    rxdfetap6ovrden_in,
    rxdfetap7hold_in,
    rxdfetap7ovrden_in,
    rxdfetap8hold_in,
    rxdfetap8ovrden_in,
    rxdfetap9hold_in,
    rxdfetap9ovrden_in,
    rxdfeuthold_in,
    rxdfeutovrden_in,
    rxdfevphold_in,
    rxdfevpovrden_in,
    rxdfevsen_in,
    rxdfexyden_in,
    rxdlybypass_in,
    rxdlyen_in,
    rxdlyovrden_in,
    rxdlysreset_in,
    rxelecidlemode_in,
    rxeqtraining_in,
    rxgearboxslip_in,
    rxlatclk_in,
    rxlpmen_in,
    rxlpmgchold_in,
    rxlpmgcovrden_in,
    rxlpmhfhold_in,
    rxlpmhfovrden_in,
    rxlpmlfhold_in,
    rxlpmlfklovrden_in,
    rxlpmoshold_in,
    rxlpmosovrden_in,
    rxmcommaalignen_in,
    rxmonitorsel_in,
    rxoobreset_in,
    rxoscalreset_in,
    rxoshold_in,
    rxosintcfg_in,
    rxosinten_in,
    rxosinthold_in,
    rxosintovrden_in,
    rxosintstrobe_in,
    rxosinttestovrden_in,
    rxosovrden_in,
    rxoutclksel_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxphalign_in,
    rxphalignen_in,
    rxphdlypd_in,
    rxphdlyreset_in,
    rxphovrden_in,
    rxpllclksel_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxprogdivreset_in,
    rxqpien_in,
    rxrate_in,
    rxratemode_in,
    rxslide_in,
    rxslipoutclk_in,
    rxslippma_in,
    rxsyncallin_in,
    rxsyncin_in,
    rxsyncmode_in,
    rxsysclksel_in,
    rxtermination_in,
    rxuserrdy_in,
    rxusrclk_in,
    rxusrclk2_in,
    sigvalidclk_in,
    tstin_in,
    tx8b10bbypass_in,
    tx8b10ben_in,
    txbufdiffctrl_in,
    txcominit_in,
    txcomsas_in,
    txcomwake_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdata_in,
    txdataextendrsvd_in,
    txdccforcestart_in,
    txdccreset_in,
    txdeemph_in,
    txdetectrx_in,
    txdiffctrl_in,
    txdiffpd_in,
    txdlybypass_in,
    txdlyen_in,
    txdlyhold_in,
    txdlyovrden_in,
    txdlysreset_in,
    txdlyupdown_in,
    txelecidle_in,
    txelforcestart_in,
    txheader_in,
    txinhibit_in,
    txlatclk_in,
    txlfpstreset_in,
    txlfpsu2lpexit_in,
    txlfpsu3wake_in,
    txmaincursor_in,
    txmargin_in,
    txmuxdcdexhold_in,
    txmuxdcdorwren_in,
    txoneszeros_in,
    txoutclksel_in,
    txpcsreset_in,
    txpd_in,
    txpdelecidlemode_in,
    txphalign_in,
    txphalignen_in,
    txphdlypd_in,
    txphdlyreset_in,
    txphdlytstclk_in,
    txphinit_in,
    txphovrden_in,
    txpippmen_in,
    txpippmovrden_in,
    txpippmpd_in,
    txpippmsel_in,
    txpippmstepsize_in,
    txpisopd_in,
    txpllclksel_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txpostcursorinv_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txprecursorinv_in,
    txprogdivreset_in,
    txqpibiasen_in,
    txqpistrongpdown_in,
    txqpiweakpup_in,
    txrate_in,
    txratemode_in,
    txsequence_in,
    txswing_in,
    txsyncallin_in,
    txsyncin_in,
    txsyncmode_in,
    txsysclksel_in,
    txuserrdy_in,
    txusrclk_in,
    txusrclk2_in,
    bufgtce_out,
    bufgtcemask_out,
    bufgtdiv_out,
    bufgtreset_out,
    bufgtrstmask_out,
    cpllfbclklost_out,
    cplllock_out,
    cpllrefclklost_out,
    dmonitorout_out,
    dmonitoroutclk_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    gtrefclkmonitor_out,
    gtytxn_out,
    gtytxp_out,
    pcierategen3_out,
    pcierateidle_out,
    pcierateqpllpd_out,
    pcierateqpllreset_out,
    pciesynctxsyncdone_out,
    pcieusergen3rdy_out,
    pcieuserphystatusrst_out,
    pcieuserratestart_out,
    pcsrsvdout_out,
    phystatus_out,
    pinrsrvdas_out,
    powerpresent_out,
    resetexception_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxcdrlock_out,
    rxcdrphdone_out,
    rxchanbondseq_out,
    rxchanisaligned_out,
    rxchanrealign_out,
    rxchbondo_out,
    rxckcaldone_out,
    rxclkcorcnt_out,
    rxcominitdet_out,
    rxcommadet_out,
    rxcomsasdet_out,
    rxcomwakedet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxdata_out,
    rxdataextendrsvd_out,
    rxdatavalid_out,
    rxdlysresetdone_out,
    rxelecidle_out,
    rxheader_out,
    rxheadervalid_out,
    rxlfpstresetdet_out,
    rxlfpsu2lpexitdet_out,
    rxlfpsu3wakedet_out,
    rxmonitorout_out,
    rxosintdone_out,
    rxosintstarted_out,
    rxosintstrobedone_out,
    rxosintstrobestarted_out,
    rxoutclk_out,
    rxoutclkfabric_out,
    rxoutclkpcs_out,
    rxphaligndone_out,
    rxphalignerr_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxprbslocked_out,
    rxprgdivresetdone_out,
    rxqpisenn_out,
    rxqpisenp_out,
    rxratedone_out,
    rxrecclkout_out,
    rxresetdone_out,
    rxsliderdy_out,
    rxslipdone_out,
    rxslipoutclkrdy_out,
    rxslippmardy_out,
    rxstartofseq_out,
    rxstatus_out,
    rxsyncdone_out,
    rxsyncout_out,
    rxvalid_out,
    txbufstatus_out,
    txcomfinish_out,
    txdccdone_out,
    txdlysresetdone_out,
    txoutclk_out,
    txoutclkfabric_out,
    txoutclkpcs_out,
    txphaligndone_out,
    txphinitdone_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txqpisenn_out,
    txqpisenp_out,
    txratedone_out,
    txresetdone_out,
    txsyncdone_out,
    txsyncout_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  output [0:0]gtwiz_userclk_tx_srcclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk2_out;
  output [0:0]gtwiz_userclk_tx_active_out;
  input [0:0]gtwiz_userclk_rx_reset_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  output [0:0]gtwiz_userclk_rx_srcclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk2_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input [0:0]gtwiz_reset_tx_done_in;
  input [0:0]gtwiz_reset_rx_done_in;
  input [0:0]gtwiz_reset_qpll0lock_in;
  input [0:0]gtwiz_reset_qpll1lock_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output [0:0]gtwiz_reset_qpll0reset_out;
  output [0:0]gtwiz_reset_qpll1reset_out;
  input [17:0]gtwiz_gthe3_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe3_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe3_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gthe4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe4_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gtye4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gtye4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gtye4_cpll_cal_bufg_ce_in;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [0:0]bgbypassb_in;
  input [0:0]bgmonitorenb_in;
  input [0:0]bgpdb_in;
  input [4:0]bgrcalovrd_in;
  input [0:0]bgrcalovrdenb_in;
  input [8:0]drpaddr_common_in;
  input [0:0]drpclk_common_in;
  input [15:0]drpdi_common_in;
  input [0:0]drpen_common_in;
  input [0:0]drpwe_common_in;
  input [0:0]gtgrefclk0_in;
  input [0:0]gtgrefclk1_in;
  input [0:0]gtnorthrefclk00_in;
  input [0:0]gtnorthrefclk01_in;
  input [0:0]gtnorthrefclk10_in;
  input [0:0]gtnorthrefclk11_in;
  input [0:0]gtrefclk00_in;
  input [0:0]gtrefclk01_in;
  input [0:0]gtrefclk10_in;
  input [0:0]gtrefclk11_in;
  input [0:0]gtsouthrefclk00_in;
  input [0:0]gtsouthrefclk01_in;
  input [0:0]gtsouthrefclk10_in;
  input [0:0]gtsouthrefclk11_in;
  input [0:0]pcierateqpll0_in;
  input [0:0]pcierateqpll1_in;
  input [7:0]pmarsvd0_in;
  input [7:0]pmarsvd1_in;
  input [0:0]qpll0clkrsvd0_in;
  input [0:0]qpll0clkrsvd1_in;
  input [0:0]qpll0fbdiv_in;
  input [0:0]qpll0lockdetclk_in;
  input [0:0]qpll0locken_in;
  input [0:0]qpll0pd_in;
  input [2:0]qpll0refclksel_in;
  input [0:0]qpll0reset_in;
  input [0:0]qpll1clkrsvd0_in;
  input [0:0]qpll1clkrsvd1_in;
  input [0:0]qpll1fbdiv_in;
  input [0:0]qpll1lockdetclk_in;
  input [0:0]qpll1locken_in;
  input [0:0]qpll1pd_in;
  input [2:0]qpll1refclksel_in;
  input [0:0]qpll1reset_in;
  input [7:0]qpllrsvd1_in;
  input [4:0]qpllrsvd2_in;
  input [4:0]qpllrsvd3_in;
  input [7:0]qpllrsvd4_in;
  input [0:0]rcalenb_in;
  input [0:0]sdm0data_in;
  input [0:0]sdm0reset_in;
  input [0:0]sdm0toggle_in;
  input [0:0]sdm0width_in;
  input [0:0]sdm1data_in;
  input [0:0]sdm1reset_in;
  input [0:0]sdm1toggle_in;
  input [0:0]sdm1width_in;
  input [0:0]tcongpi_in;
  input [0:0]tconpowerup_in;
  input [0:0]tconreset_in;
  input [0:0]tconrsvdin1_in;
  input [0:0]ubcfgstreamen_in;
  input [0:0]ubdo_in;
  input [0:0]ubdrdy_in;
  input [0:0]ubenable_in;
  input [0:0]ubgpi_in;
  input [0:0]ubintr_in;
  input [0:0]ubiolmbrst_in;
  input [0:0]ubmbrst_in;
  input [0:0]ubmdmcapture_in;
  input [0:0]ubmdmdbgrst_in;
  input [0:0]ubmdmdbgupdate_in;
  input [0:0]ubmdmregen_in;
  input [0:0]ubmdmshift_in;
  input [0:0]ubmdmsysrst_in;
  input [0:0]ubmdmtck_in;
  input [0:0]ubmdmtdi_in;
  output [15:0]drpdo_common_out;
  output [0:0]drprdy_common_out;
  output [7:0]pmarsvdout0_out;
  output [7:0]pmarsvdout1_out;
  output [0:0]qpll0fbclklost_out;
  output [0:0]qpll0lock_out;
  output [0:0]qpll0outclk_out;
  output [0:0]qpll0outrefclk_out;
  output [0:0]qpll0refclklost_out;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1lock_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  output [7:0]qplldmonitor0_out;
  output [7:0]qplldmonitor1_out;
  output [0:0]refclkoutmonitor0_out;
  output [0:0]refclkoutmonitor1_out;
  output [1:0]rxrecclk0_sel_out;
  output [1:0]rxrecclk1_sel_out;
  output [0:0]rxrecclk0sel_out;
  output [0:0]rxrecclk1sel_out;
  output [0:0]sdm0finalout_out;
  output [0:0]sdm0testdata_out;
  output [0:0]sdm1finalout_out;
  output [0:0]sdm1testdata_out;
  output [0:0]tcongpo_out;
  output [0:0]tconrsvdout0_out;
  output [0:0]ubdaddr_out;
  output [0:0]ubden_out;
  output [0:0]ubdi_out;
  output [0:0]ubdwe_out;
  output [0:0]ubmdmtdo_out;
  output [0:0]ubrsvdout_out;
  output [0:0]ubtxuart_out;
  input [0:0]cdrstepdir_in;
  input [0:0]cdrstepsq_in;
  input [0:0]cdrstepsx_in;
  input [0:0]cfgreset_in;
  input [0:0]clkrsvd0_in;
  input [0:0]clkrsvd1_in;
  input [0:0]cpllfreqlock_in;
  input [0:0]cplllockdetclk_in;
  input [0:0]cplllocken_in;
  input [0:0]cpllpd_in;
  input [2:0]cpllrefclksel_in;
  input [0:0]cpllreset_in;
  input [0:0]dmonfiforeset_in;
  input [0:0]dmonitorclk_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drprst_in;
  input [0:0]drpwe_in;
  input [0:0]elpcaldvorwren_in;
  input [0:0]elpcalpaorwren_in;
  input [0:0]evoddphicaldone_in;
  input [0:0]evoddphicalstart_in;
  input [0:0]evoddphidrden_in;
  input [0:0]evoddphidwren_in;
  input [0:0]evoddphixrden_in;
  input [0:0]evoddphixwren_in;
  input [0:0]eyescanmode_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]freqos_in;
  input [0:0]gtgrefclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtnorthrefclk0_in;
  input [0:0]gtnorthrefclk1_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [0:0]gtresetsel_in;
  input [15:0]gtrsvd_in;
  input [0:0]gtrxreset_in;
  input [0:0]gtrxresetsel_in;
  input [0:0]gtsouthrefclk0_in;
  input [0:0]gtsouthrefclk1_in;
  input [0:0]gttxreset_in;
  input [0:0]gttxresetsel_in;
  input [0:0]incpctrl_in;
  input [0:0]gtyrxn_in;
  input [0:0]gtyrxp_in;
  input [2:0]loopback_in;
  input [0:0]looprsvd_in;
  input [0:0]lpbkrxtxseren_in;
  input [0:0]lpbktxrxseren_in;
  input [0:0]pcieeqrxeqadaptdone_in;
  input [0:0]pcierstidle_in;
  input [0:0]pciersttxsyncstart_in;
  input [0:0]pcieuserratedone_in;
  input [15:0]pcsrsvdin_in;
  input [4:0]pcsrsvdin2_in;
  input [4:0]pmarsvdin_in;
  input [0:0]qpll0clk_in;
  input [0:0]qpll0freqlock_in;
  input [0:0]qpll0refclk_in;
  input [0:0]qpll1clk_in;
  input [0:0]qpll1freqlock_in;
  input [0:0]qpll1refclk_in;
  input [0:0]resetovrd_in;
  input [0:0]rstclkentx_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxafecfoken_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrfreqreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcdrovrden_in;
  input [0:0]rxcdrreset_in;
  input [0:0]rxcdrresetrsv_in;
  input [0:0]rxchbonden_in;
  input [4:0]rxchbondi_in;
  input [2:0]rxchbondlevel_in;
  input [0:0]rxchbondmaster_in;
  input [0:0]rxchbondslave_in;
  input [0:0]rxckcalreset_in;
  input [0:0]rxckcalstart_in;
  input [0:0]rxcommadeten_in;
  input [1:0]rxdfeagcctrl_in;
  input [0:0]rxdccforcestart_in;
  input [0:0]rxdfeagchold_in;
  input [0:0]rxdfeagcovrden_in;
  input [0:0]rxdfecfokfcnum_in;
  input [0:0]rxdfecfokfen_in;
  input [0:0]rxdfecfokfpulse_in;
  input [0:0]rxdfecfokhold_in;
  input [0:0]rxdfecfokovren_in;
  input [0:0]rxdfekhhold_in;
  input [0:0]rxdfekhovrden_in;
  input [0:0]rxdfelfhold_in;
  input [0:0]rxdfelfovrden_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxdfetap10hold_in;
  input [0:0]rxdfetap10ovrden_in;
  input [0:0]rxdfetap11hold_in;
  input [0:0]rxdfetap11ovrden_in;
  input [0:0]rxdfetap12hold_in;
  input [0:0]rxdfetap12ovrden_in;
  input [0:0]rxdfetap13hold_in;
  input [0:0]rxdfetap13ovrden_in;
  input [0:0]rxdfetap14hold_in;
  input [0:0]rxdfetap14ovrden_in;
  input [0:0]rxdfetap15hold_in;
  input [0:0]rxdfetap15ovrden_in;
  input [0:0]rxdfetap2hold_in;
  input [0:0]rxdfetap2ovrden_in;
  input [0:0]rxdfetap3hold_in;
  input [0:0]rxdfetap3ovrden_in;
  input [0:0]rxdfetap4hold_in;
  input [0:0]rxdfetap4ovrden_in;
  input [0:0]rxdfetap5hold_in;
  input [0:0]rxdfetap5ovrden_in;
  input [0:0]rxdfetap6hold_in;
  input [0:0]rxdfetap6ovrden_in;
  input [0:0]rxdfetap7hold_in;
  input [0:0]rxdfetap7ovrden_in;
  input [0:0]rxdfetap8hold_in;
  input [0:0]rxdfetap8ovrden_in;
  input [0:0]rxdfetap9hold_in;
  input [0:0]rxdfetap9ovrden_in;
  input [0:0]rxdfeuthold_in;
  input [0:0]rxdfeutovrden_in;
  input [0:0]rxdfevphold_in;
  input [0:0]rxdfevpovrden_in;
  input [0:0]rxdfevsen_in;
  input [0:0]rxdfexyden_in;
  input [0:0]rxdlybypass_in;
  input [0:0]rxdlyen_in;
  input [0:0]rxdlyovrden_in;
  input [0:0]rxdlysreset_in;
  input [1:0]rxelecidlemode_in;
  input [0:0]rxeqtraining_in;
  input [0:0]rxgearboxslip_in;
  input [0:0]rxlatclk_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxlpmgchold_in;
  input [0:0]rxlpmgcovrden_in;
  input [0:0]rxlpmhfhold_in;
  input [0:0]rxlpmhfovrden_in;
  input [0:0]rxlpmlfhold_in;
  input [0:0]rxlpmlfklovrden_in;
  input [0:0]rxlpmoshold_in;
  input [0:0]rxlpmosovrden_in;
  input [0:0]rxmcommaalignen_in;
  input [1:0]rxmonitorsel_in;
  input [0:0]rxoobreset_in;
  input [0:0]rxoscalreset_in;
  input [0:0]rxoshold_in;
  input [3:0]rxosintcfg_in;
  input [0:0]rxosinten_in;
  input [0:0]rxosinthold_in;
  input [0:0]rxosintovrden_in;
  input [0:0]rxosintstrobe_in;
  input [0:0]rxosinttestovrden_in;
  input [0:0]rxosovrden_in;
  input [2:0]rxoutclksel_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxphalign_in;
  input [0:0]rxphalignen_in;
  input [0:0]rxphdlypd_in;
  input [0:0]rxphdlyreset_in;
  input [0:0]rxphovrden_in;
  input [1:0]rxpllclksel_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [0:0]rxprogdivreset_in;
  input [0:0]rxqpien_in;
  input [2:0]rxrate_in;
  input [0:0]rxratemode_in;
  input [0:0]rxslide_in;
  input [0:0]rxslipoutclk_in;
  input [0:0]rxslippma_in;
  input [0:0]rxsyncallin_in;
  input [0:0]rxsyncin_in;
  input [0:0]rxsyncmode_in;
  input [1:0]rxsysclksel_in;
  input [0:0]rxtermination_in;
  input [0:0]rxuserrdy_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]sigvalidclk_in;
  input [19:0]tstin_in;
  input [7:0]tx8b10bbypass_in;
  input [0:0]tx8b10ben_in;
  input [2:0]txbufdiffctrl_in;
  input [0:0]txcominit_in;
  input [0:0]txcomsas_in;
  input [0:0]txcomwake_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [127:0]txdata_in;
  input [7:0]txdataextendrsvd_in;
  input [0:0]txdccforcestart_in;
  input [0:0]txdccreset_in;
  input [0:0]txdeemph_in;
  input [0:0]txdetectrx_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txdiffpd_in;
  input [0:0]txdlybypass_in;
  input [0:0]txdlyen_in;
  input [0:0]txdlyhold_in;
  input [0:0]txdlyovrden_in;
  input [0:0]txdlysreset_in;
  input [0:0]txdlyupdown_in;
  input [0:0]txelecidle_in;
  input [0:0]txelforcestart_in;
  input [5:0]txheader_in;
  input [0:0]txinhibit_in;
  input [0:0]txlatclk_in;
  input [0:0]txlfpstreset_in;
  input [0:0]txlfpsu2lpexit_in;
  input [0:0]txlfpsu3wake_in;
  input [6:0]txmaincursor_in;
  input [2:0]txmargin_in;
  input [0:0]txmuxdcdexhold_in;
  input [0:0]txmuxdcdorwren_in;
  input [0:0]txoneszeros_in;
  input [2:0]txoutclksel_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpdelecidlemode_in;
  input [0:0]txphalign_in;
  input [0:0]txphalignen_in;
  input [0:0]txphdlypd_in;
  input [0:0]txphdlyreset_in;
  input [0:0]txphdlytstclk_in;
  input [0:0]txphinit_in;
  input [0:0]txphovrden_in;
  input [0:0]txpippmen_in;
  input [0:0]txpippmovrden_in;
  input [0:0]txpippmpd_in;
  input [0:0]txpippmsel_in;
  input [4:0]txpippmstepsize_in;
  input [0:0]txpisopd_in;
  input [1:0]txpllclksel_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txpostcursorinv_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txprecursorinv_in;
  input [0:0]txprogdivreset_in;
  input [0:0]txqpibiasen_in;
  input [0:0]txqpistrongpdown_in;
  input [0:0]txqpiweakpup_in;
  input [2:0]txrate_in;
  input [0:0]txratemode_in;
  input [6:0]txsequence_in;
  input [0:0]txswing_in;
  input [0:0]txsyncallin_in;
  input [0:0]txsyncin_in;
  input [0:0]txsyncmode_in;
  input [1:0]txsysclksel_in;
  input [0:0]txuserrdy_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [2:0]bufgtce_out;
  output [2:0]bufgtcemask_out;
  output [8:0]bufgtdiv_out;
  output [2:0]bufgtreset_out;
  output [2:0]bufgtrstmask_out;
  output [0:0]cpllfbclklost_out;
  output [0:0]cplllock_out;
  output [0:0]cpllrefclklost_out;
  output [16:0]dmonitorout_out;
  output [0:0]dmonitoroutclk_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]gtrefclkmonitor_out;
  output [0:0]gtytxn_out;
  output [0:0]gtytxp_out;
  output [0:0]pcierategen3_out;
  output [0:0]pcierateidle_out;
  output [1:0]pcierateqpllpd_out;
  output [1:0]pcierateqpllreset_out;
  output [0:0]pciesynctxsyncdone_out;
  output [0:0]pcieusergen3rdy_out;
  output [0:0]pcieuserphystatusrst_out;
  output [0:0]pcieuserratestart_out;
  output [11:0]pcsrsvdout_out;
  output [0:0]phystatus_out;
  output [7:0]pinrsrvdas_out;
  output [0:0]powerpresent_out;
  output [0:0]resetexception_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxcdrphdone_out;
  output [0:0]rxchanbondseq_out;
  output [0:0]rxchanisaligned_out;
  output [0:0]rxchanrealign_out;
  output [4:0]rxchbondo_out;
  output [0:0]rxckcaldone_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcominitdet_out;
  output [0:0]rxcommadet_out;
  output [0:0]rxcomsasdet_out;
  output [0:0]rxcomwakedet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [127:0]rxdata_out;
  output [7:0]rxdataextendrsvd_out;
  output [1:0]rxdatavalid_out;
  output [0:0]rxdlysresetdone_out;
  output [0:0]rxelecidle_out;
  output [5:0]rxheader_out;
  output [1:0]rxheadervalid_out;
  output [0:0]rxlfpstresetdet_out;
  output [0:0]rxlfpsu2lpexitdet_out;
  output [0:0]rxlfpsu3wakedet_out;
  output [6:0]rxmonitorout_out;
  output [0:0]rxosintdone_out;
  output [0:0]rxosintstarted_out;
  output [0:0]rxosintstrobedone_out;
  output [0:0]rxosintstrobestarted_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkfabric_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]rxphaligndone_out;
  output [0:0]rxphalignerr_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxprbslocked_out;
  output [0:0]rxprgdivresetdone_out;
  output [0:0]rxqpisenn_out;
  output [0:0]rxqpisenp_out;
  output [0:0]rxratedone_out;
  output [0:0]rxrecclkout_out;
  output [0:0]rxresetdone_out;
  output [0:0]rxsliderdy_out;
  output [0:0]rxslipdone_out;
  output [0:0]rxslipoutclkrdy_out;
  output [0:0]rxslippmardy_out;
  output [1:0]rxstartofseq_out;
  output [2:0]rxstatus_out;
  output [0:0]rxsyncdone_out;
  output [0:0]rxsyncout_out;
  output [0:0]rxvalid_out;
  output [1:0]txbufstatus_out;
  output [0:0]txcomfinish_out;
  output [0:0]txdccdone_out;
  output [0:0]txdlysresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txoutclkfabric_out;
  output [0:0]txoutclkpcs_out;
  output [0:0]txphaligndone_out;
  output [0:0]txphinitdone_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txqpisenn_out;
  output [0:0]txqpisenp_out;
  output [0:0]txratedone_out;
  output [0:0]txresetdone_out;
  output [0:0]txsyncdone_out;
  output [0:0]txsyncout_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  assign bufgtce_out[2] = \<const0> ;
  assign bufgtce_out[1] = \<const0> ;
  assign bufgtce_out[0] = \<const0> ;
  assign bufgtcemask_out[2] = \<const0> ;
  assign bufgtcemask_out[1] = \<const0> ;
  assign bufgtcemask_out[0] = \<const0> ;
  assign bufgtdiv_out[8] = \<const0> ;
  assign bufgtdiv_out[7] = \<const0> ;
  assign bufgtdiv_out[6] = \<const0> ;
  assign bufgtdiv_out[5] = \<const0> ;
  assign bufgtdiv_out[4] = \<const0> ;
  assign bufgtdiv_out[3] = \<const0> ;
  assign bufgtdiv_out[2] = \<const0> ;
  assign bufgtdiv_out[1] = \<const0> ;
  assign bufgtdiv_out[0] = \<const0> ;
  assign bufgtreset_out[2] = \<const0> ;
  assign bufgtreset_out[1] = \<const0> ;
  assign bufgtreset_out[0] = \<const0> ;
  assign bufgtrstmask_out[2] = \<const0> ;
  assign bufgtrstmask_out[1] = \<const0> ;
  assign bufgtrstmask_out[0] = \<const0> ;
  assign cpllfbclklost_out[0] = \<const0> ;
  assign cplllock_out[0] = \<const0> ;
  assign cpllrefclklost_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign dmonitoroutclk_out[0] = \<const0> ;
  assign drpdo_common_out[15] = \<const0> ;
  assign drpdo_common_out[14] = \<const0> ;
  assign drpdo_common_out[13] = \<const0> ;
  assign drpdo_common_out[12] = \<const0> ;
  assign drpdo_common_out[11] = \<const0> ;
  assign drpdo_common_out[10] = \<const0> ;
  assign drpdo_common_out[9] = \<const0> ;
  assign drpdo_common_out[8] = \<const0> ;
  assign drpdo_common_out[7] = \<const0> ;
  assign drpdo_common_out[6] = \<const0> ;
  assign drpdo_common_out[5] = \<const0> ;
  assign drpdo_common_out[4] = \<const0> ;
  assign drpdo_common_out[3] = \<const0> ;
  assign drpdo_common_out[2] = \<const0> ;
  assign drpdo_common_out[1] = \<const0> ;
  assign drpdo_common_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_common_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtrefclkmonitor_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_error_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_error_out[0] = \<const0> ;
  assign gtwiz_reset_qpll0reset_out[0] = \<const0> ;
  assign gtwiz_reset_qpll1reset_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk_out[0] = \<const0> ;
  assign gtytxn_out[0] = \<const0> ;
  assign gtytxp_out[0] = \<const0> ;
  assign pcierategen3_out[0] = \<const0> ;
  assign pcierateidle_out[0] = \<const0> ;
  assign pcierateqpllpd_out[1] = \<const0> ;
  assign pcierateqpllpd_out[0] = \<const0> ;
  assign pcierateqpllreset_out[1] = \<const0> ;
  assign pcierateqpllreset_out[0] = \<const0> ;
  assign pciesynctxsyncdone_out[0] = \<const0> ;
  assign pcieusergen3rdy_out[0] = \<const0> ;
  assign pcieuserphystatusrst_out[0] = \<const0> ;
  assign pcieuserratestart_out[0] = \<const0> ;
  assign pcsrsvdout_out[11] = \<const0> ;
  assign pcsrsvdout_out[10] = \<const0> ;
  assign pcsrsvdout_out[9] = \<const0> ;
  assign pcsrsvdout_out[8] = \<const0> ;
  assign pcsrsvdout_out[7] = \<const0> ;
  assign pcsrsvdout_out[6] = \<const0> ;
  assign pcsrsvdout_out[5] = \<const0> ;
  assign pcsrsvdout_out[4] = \<const0> ;
  assign pcsrsvdout_out[3] = \<const0> ;
  assign pcsrsvdout_out[2] = \<const0> ;
  assign pcsrsvdout_out[1] = \<const0> ;
  assign pcsrsvdout_out[0] = \<const0> ;
  assign phystatus_out[0] = \<const0> ;
  assign pinrsrvdas_out[7] = \<const0> ;
  assign pinrsrvdas_out[6] = \<const0> ;
  assign pinrsrvdas_out[5] = \<const0> ;
  assign pinrsrvdas_out[4] = \<const0> ;
  assign pinrsrvdas_out[3] = \<const0> ;
  assign pinrsrvdas_out[2] = \<const0> ;
  assign pinrsrvdas_out[1] = \<const0> ;
  assign pinrsrvdas_out[0] = \<const0> ;
  assign pmarsvdout0_out[7] = \<const0> ;
  assign pmarsvdout0_out[6] = \<const0> ;
  assign pmarsvdout0_out[5] = \<const0> ;
  assign pmarsvdout0_out[4] = \<const0> ;
  assign pmarsvdout0_out[3] = \<const0> ;
  assign pmarsvdout0_out[2] = \<const0> ;
  assign pmarsvdout0_out[1] = \<const0> ;
  assign pmarsvdout0_out[0] = \<const0> ;
  assign pmarsvdout1_out[7] = \<const0> ;
  assign pmarsvdout1_out[6] = \<const0> ;
  assign pmarsvdout1_out[5] = \<const0> ;
  assign pmarsvdout1_out[4] = \<const0> ;
  assign pmarsvdout1_out[3] = \<const0> ;
  assign pmarsvdout1_out[2] = \<const0> ;
  assign pmarsvdout1_out[1] = \<const0> ;
  assign pmarsvdout1_out[0] = \<const0> ;
  assign powerpresent_out[0] = \<const0> ;
  assign qpll0fbclklost_out[0] = \<const0> ;
  assign qpll0lock_out[0] = \<const0> ;
  assign qpll0outclk_out[0] = \<const0> ;
  assign qpll0outrefclk_out[0] = \<const0> ;
  assign qpll0refclklost_out[0] = \<const0> ;
  assign qpll1fbclklost_out[0] = \<const0> ;
  assign qpll1lock_out[0] = \<const0> ;
  assign qpll1outclk_out[0] = \<const0> ;
  assign qpll1outrefclk_out[0] = \<const0> ;
  assign qpll1refclklost_out[0] = \<const0> ;
  assign qplldmonitor0_out[7] = \<const0> ;
  assign qplldmonitor0_out[6] = \<const0> ;
  assign qplldmonitor0_out[5] = \<const0> ;
  assign qplldmonitor0_out[4] = \<const0> ;
  assign qplldmonitor0_out[3] = \<const0> ;
  assign qplldmonitor0_out[2] = \<const0> ;
  assign qplldmonitor0_out[1] = \<const0> ;
  assign qplldmonitor0_out[0] = \<const0> ;
  assign qplldmonitor1_out[7] = \<const0> ;
  assign qplldmonitor1_out[6] = \<const0> ;
  assign qplldmonitor1_out[5] = \<const0> ;
  assign qplldmonitor1_out[4] = \<const0> ;
  assign qplldmonitor1_out[3] = \<const0> ;
  assign qplldmonitor1_out[2] = \<const0> ;
  assign qplldmonitor1_out[1] = \<const0> ;
  assign qplldmonitor1_out[0] = \<const0> ;
  assign refclkoutmonitor0_out[0] = \<const0> ;
  assign refclkoutmonitor1_out[0] = \<const0> ;
  assign resetexception_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcdrlock_out[0] = \<const0> ;
  assign rxcdrphdone_out[0] = \<const0> ;
  assign rxchanbondseq_out[0] = \<const0> ;
  assign rxchanisaligned_out[0] = \<const0> ;
  assign rxchanrealign_out[0] = \<const0> ;
  assign rxchbondo_out[4] = \<const0> ;
  assign rxchbondo_out[3] = \<const0> ;
  assign rxchbondo_out[2] = \<const0> ;
  assign rxchbondo_out[1] = \<const0> ;
  assign rxchbondo_out[0] = \<const0> ;
  assign rxckcaldone_out[0] = \<const0> ;
  assign rxcominitdet_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxcomsasdet_out[0] = \<const0> ;
  assign rxcomwakedet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxdata_out[127] = \<const0> ;
  assign rxdata_out[126] = \<const0> ;
  assign rxdata_out[125] = \<const0> ;
  assign rxdata_out[124] = \<const0> ;
  assign rxdata_out[123] = \<const0> ;
  assign rxdata_out[122] = \<const0> ;
  assign rxdata_out[121] = \<const0> ;
  assign rxdata_out[120] = \<const0> ;
  assign rxdata_out[119] = \<const0> ;
  assign rxdata_out[118] = \<const0> ;
  assign rxdata_out[117] = \<const0> ;
  assign rxdata_out[116] = \<const0> ;
  assign rxdata_out[115] = \<const0> ;
  assign rxdata_out[114] = \<const0> ;
  assign rxdata_out[113] = \<const0> ;
  assign rxdata_out[112] = \<const0> ;
  assign rxdata_out[111] = \<const0> ;
  assign rxdata_out[110] = \<const0> ;
  assign rxdata_out[109] = \<const0> ;
  assign rxdata_out[108] = \<const0> ;
  assign rxdata_out[107] = \<const0> ;
  assign rxdata_out[106] = \<const0> ;
  assign rxdata_out[105] = \<const0> ;
  assign rxdata_out[104] = \<const0> ;
  assign rxdata_out[103] = \<const0> ;
  assign rxdata_out[102] = \<const0> ;
  assign rxdata_out[101] = \<const0> ;
  assign rxdata_out[100] = \<const0> ;
  assign rxdata_out[99] = \<const0> ;
  assign rxdata_out[98] = \<const0> ;
  assign rxdata_out[97] = \<const0> ;
  assign rxdata_out[96] = \<const0> ;
  assign rxdata_out[95] = \<const0> ;
  assign rxdata_out[94] = \<const0> ;
  assign rxdata_out[93] = \<const0> ;
  assign rxdata_out[92] = \<const0> ;
  assign rxdata_out[91] = \<const0> ;
  assign rxdata_out[90] = \<const0> ;
  assign rxdata_out[89] = \<const0> ;
  assign rxdata_out[88] = \<const0> ;
  assign rxdata_out[87] = \<const0> ;
  assign rxdata_out[86] = \<const0> ;
  assign rxdata_out[85] = \<const0> ;
  assign rxdata_out[84] = \<const0> ;
  assign rxdata_out[83] = \<const0> ;
  assign rxdata_out[82] = \<const0> ;
  assign rxdata_out[81] = \<const0> ;
  assign rxdata_out[80] = \<const0> ;
  assign rxdata_out[79] = \<const0> ;
  assign rxdata_out[78] = \<const0> ;
  assign rxdata_out[77] = \<const0> ;
  assign rxdata_out[76] = \<const0> ;
  assign rxdata_out[75] = \<const0> ;
  assign rxdata_out[74] = \<const0> ;
  assign rxdata_out[73] = \<const0> ;
  assign rxdata_out[72] = \<const0> ;
  assign rxdata_out[71] = \<const0> ;
  assign rxdata_out[70] = \<const0> ;
  assign rxdata_out[69] = \<const0> ;
  assign rxdata_out[68] = \<const0> ;
  assign rxdata_out[67] = \<const0> ;
  assign rxdata_out[66] = \<const0> ;
  assign rxdata_out[65] = \<const0> ;
  assign rxdata_out[64] = \<const0> ;
  assign rxdata_out[63] = \<const0> ;
  assign rxdata_out[62] = \<const0> ;
  assign rxdata_out[61] = \<const0> ;
  assign rxdata_out[60] = \<const0> ;
  assign rxdata_out[59] = \<const0> ;
  assign rxdata_out[58] = \<const0> ;
  assign rxdata_out[57] = \<const0> ;
  assign rxdata_out[56] = \<const0> ;
  assign rxdata_out[55] = \<const0> ;
  assign rxdata_out[54] = \<const0> ;
  assign rxdata_out[53] = \<const0> ;
  assign rxdata_out[52] = \<const0> ;
  assign rxdata_out[51] = \<const0> ;
  assign rxdata_out[50] = \<const0> ;
  assign rxdata_out[49] = \<const0> ;
  assign rxdata_out[48] = \<const0> ;
  assign rxdata_out[47] = \<const0> ;
  assign rxdata_out[46] = \<const0> ;
  assign rxdata_out[45] = \<const0> ;
  assign rxdata_out[44] = \<const0> ;
  assign rxdata_out[43] = \<const0> ;
  assign rxdata_out[42] = \<const0> ;
  assign rxdata_out[41] = \<const0> ;
  assign rxdata_out[40] = \<const0> ;
  assign rxdata_out[39] = \<const0> ;
  assign rxdata_out[38] = \<const0> ;
  assign rxdata_out[37] = \<const0> ;
  assign rxdata_out[36] = \<const0> ;
  assign rxdata_out[35] = \<const0> ;
  assign rxdata_out[34] = \<const0> ;
  assign rxdata_out[33] = \<const0> ;
  assign rxdata_out[32] = \<const0> ;
  assign rxdata_out[31] = \<const0> ;
  assign rxdata_out[30] = \<const0> ;
  assign rxdata_out[29] = \<const0> ;
  assign rxdata_out[28] = \<const0> ;
  assign rxdata_out[27] = \<const0> ;
  assign rxdata_out[26] = \<const0> ;
  assign rxdata_out[25] = \<const0> ;
  assign rxdata_out[24] = \<const0> ;
  assign rxdata_out[23] = \<const0> ;
  assign rxdata_out[22] = \<const0> ;
  assign rxdata_out[21] = \<const0> ;
  assign rxdata_out[20] = \<const0> ;
  assign rxdata_out[19] = \<const0> ;
  assign rxdata_out[18] = \<const0> ;
  assign rxdata_out[17] = \<const0> ;
  assign rxdata_out[16] = \<const0> ;
  assign rxdata_out[15] = \<const0> ;
  assign rxdata_out[14] = \<const0> ;
  assign rxdata_out[13] = \<const0> ;
  assign rxdata_out[12] = \<const0> ;
  assign rxdata_out[11] = \<const0> ;
  assign rxdata_out[10] = \<const0> ;
  assign rxdata_out[9] = \<const0> ;
  assign rxdata_out[8] = \<const0> ;
  assign rxdata_out[7] = \<const0> ;
  assign rxdata_out[6] = \<const0> ;
  assign rxdata_out[5] = \<const0> ;
  assign rxdata_out[4] = \<const0> ;
  assign rxdata_out[3] = \<const0> ;
  assign rxdata_out[2] = \<const0> ;
  assign rxdata_out[1] = \<const0> ;
  assign rxdata_out[0] = \<const0> ;
  assign rxdataextendrsvd_out[7] = \<const0> ;
  assign rxdataextendrsvd_out[6] = \<const0> ;
  assign rxdataextendrsvd_out[5] = \<const0> ;
  assign rxdataextendrsvd_out[4] = \<const0> ;
  assign rxdataextendrsvd_out[3] = \<const0> ;
  assign rxdataextendrsvd_out[2] = \<const0> ;
  assign rxdataextendrsvd_out[1] = \<const0> ;
  assign rxdataextendrsvd_out[0] = \<const0> ;
  assign rxdatavalid_out[1] = \<const0> ;
  assign rxdatavalid_out[0] = \<const0> ;
  assign rxdlysresetdone_out[0] = \<const0> ;
  assign rxelecidle_out[0] = \<const0> ;
  assign rxheader_out[5] = \<const0> ;
  assign rxheader_out[4] = \<const0> ;
  assign rxheader_out[3] = \<const0> ;
  assign rxheader_out[2] = \<const0> ;
  assign rxheader_out[1] = \<const0> ;
  assign rxheader_out[0] = \<const0> ;
  assign rxheadervalid_out[1] = \<const0> ;
  assign rxheadervalid_out[0] = \<const0> ;
  assign rxlfpstresetdet_out[0] = \<const0> ;
  assign rxlfpsu2lpexitdet_out[0] = \<const0> ;
  assign rxlfpsu3wakedet_out[0] = \<const0> ;
  assign rxmonitorout_out[6] = \<const0> ;
  assign rxmonitorout_out[5] = \<const0> ;
  assign rxmonitorout_out[4] = \<const0> ;
  assign rxmonitorout_out[3] = \<const0> ;
  assign rxmonitorout_out[2] = \<const0> ;
  assign rxmonitorout_out[1] = \<const0> ;
  assign rxmonitorout_out[0] = \<const0> ;
  assign rxosintdone_out[0] = \<const0> ;
  assign rxosintstarted_out[0] = \<const0> ;
  assign rxosintstrobedone_out[0] = \<const0> ;
  assign rxosintstrobestarted_out[0] = \<const0> ;
  assign rxoutclkfabric_out[0] = \<const0> ;
  assign rxoutclkpcs_out[0] = \<const0> ;
  assign rxphaligndone_out[0] = \<const0> ;
  assign rxphalignerr_out[0] = \<const0> ;
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxprbslocked_out[0] = \<const0> ;
  assign rxprgdivresetdone_out[0] = \<const0> ;
  assign rxqpisenn_out[0] = \<const0> ;
  assign rxqpisenp_out[0] = \<const0> ;
  assign rxratedone_out[0] = \<const0> ;
  assign rxrecclk0_sel_out[1] = \<const0> ;
  assign rxrecclk0_sel_out[0] = \<const0> ;
  assign rxrecclk0sel_out[0] = \<const0> ;
  assign rxrecclk1_sel_out[1] = \<const0> ;
  assign rxrecclk1_sel_out[0] = \<const0> ;
  assign rxrecclk1sel_out[0] = \<const0> ;
  assign rxrecclkout_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign rxsliderdy_out[0] = \<const0> ;
  assign rxslipdone_out[0] = \<const0> ;
  assign rxslipoutclkrdy_out[0] = \<const0> ;
  assign rxslippmardy_out[0] = \<const0> ;
  assign rxstartofseq_out[1] = \<const0> ;
  assign rxstartofseq_out[0] = \<const0> ;
  assign rxstatus_out[2] = \<const0> ;
  assign rxstatus_out[1] = \<const0> ;
  assign rxstatus_out[0] = \<const0> ;
  assign rxsyncdone_out[0] = \<const0> ;
  assign rxsyncout_out[0] = \<const0> ;
  assign rxvalid_out[0] = \<const0> ;
  assign sdm0finalout_out[0] = \<const0> ;
  assign sdm0testdata_out[0] = \<const0> ;
  assign sdm1finalout_out[0] = \<const0> ;
  assign sdm1testdata_out[0] = \<const0> ;
  assign tcongpo_out[0] = \<const0> ;
  assign tconrsvdout0_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txcomfinish_out[0] = \<const0> ;
  assign txdccdone_out[0] = \<const0> ;
  assign txdlysresetdone_out[0] = \<const0> ;
  assign txoutclkfabric_out[0] = \<const0> ;
  assign txoutclkpcs_out[0] = \<const0> ;
  assign txphaligndone_out[0] = \<const0> ;
  assign txphinitdone_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txqpisenn_out[0] = \<const0> ;
  assign txqpisenp_out[0] = \<const0> ;
  assign txratedone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  assign txsyncdone_out[0] = \<const0> ;
  assign txsyncout_out[0] = \<const0> ;
  assign ubdaddr_out[0] = \<const0> ;
  assign ubden_out[0] = \<const0> ;
  assign ubdi_out[0] = \<const0> ;
  assign ubdwe_out[0] = \<const0> ;
  assign ubmdmtdo_out[0] = \<const0> ;
  assign ubrsvdout_out[0] = \<const0> ;
  assign ubtxuart_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt_gtwizard_gthe3 \gen_gtwizard_gthe3_top.PCS_PMA_gt_gtwizard_gthe3_inst 
       (.drpclk_in(drpclk_in),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(\^rxbufstatus_out ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(\^rxctrl0_out ),
        .rxctrl1_out(\^rxctrl1_out ),
        .rxctrl2_out(\^rxctrl2_out ),
        .rxctrl3_out(\^rxctrl3_out ),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in[1]),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(\^txbufstatus_out ),
        .txctrl0_in(txctrl0_in[1:0]),
        .txctrl1_in(txctrl1_in[1:0]),
        .txctrl2_in(txctrl2_in[1:0]),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_reset_sync
   (reset_out,
    userclk2,
    enablealign);
  output reset_out;
  input userclk2;
  input enablealign;

  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(userclk2),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_resets
   (pma_reset_out,
    independent_clock_bufg,
    reset);
  output pma_reset_out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign pma_reset_out = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign mmcm_locked_out = \<const0> ;
  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_clocking core_clocking_i
       (.gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .pma_reset_out(pma_reset_out),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_block pcs_pma_block_i
       (.CLK(userclk_out),
        .configuration_vector(configuration_vector[3:1]),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pma_reset_out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk_out(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk_out(txoutclk),
        .txp(txp),
        .userclk2(userclk2_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_sync_block
   (resetdone,
    data_in,
    userclk2);
  output resetdone;
  input data_in;
  input userclk2;

  wire data_in;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(userclk2),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync5),
        .Q(resetdone),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_transceiver
   (txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    Q,
    \rxdata_reg[7]_0 ,
    data_in,
    pma_reset_out,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    userclk2,
    SR,
    powerdown,
    mgt_tx_reset,
    D,
    txchardispmode_reg_reg_0,
    txcharisk_reg_reg_0,
    enablealign,
    \txdata_reg_reg[7]_0 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  output data_in;
  input pma_reset_out;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input userclk2;
  input [0:0]SR;
  input powerdown;
  input mgt_tx_reset;
  input [0:0]D;
  input [0:0]txchardispmode_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input enablealign;
  input [7:0]\txdata_reg_reg[7]_0 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [0:0]D;
  wire PCS_PMA_gt_i_n_118;
  wire PCS_PMA_gt_i_n_58;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire enablealign;
  wire encommaalign_int;
  wire gtpowergood;
  wire gtrefclk_out;
  wire gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_done_out_int;
  wire gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_done_out_int;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_tx_reset;
  wire p_0_in;
  wire pma_reset_out;
  wire powerdown;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_reg__0;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire toggle;
  wire toggle_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [0:0]txchardispmode_reg_reg_0;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire userclk2;
  wire [0:0]NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED;
  wire [16:0]NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) 
  (* X_CORE_INFO = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_gt PCS_PMA_gt_i
       (.cplllock_out(NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .dmonitorout_out(NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED[16:0]),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_in(independent_clock_bufg),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_out(NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED[15:0]),
        .drpen_in(1'b0),
        .drprdy_out(NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED[0]),
        .drpwe_in(1'b0),
        .eyescandataerror_out(NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .gthrxn_in(rxn),
        .gthrxp_in(rxp),
        .gthtxn_out(txn),
        .gthtxp_out(txp),
        .gtpowergood_out(gtpowergood),
        .gtrefclk0_in(gtrefclk_out),
        .gtrefclk1_in(1'b0),
        .gtwiz_reset_all_in(pma_reset_out),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_rx_cdr_stable_out(NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out_int),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out_int),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userdata_rx_out(rxdata_int),
        .gtwiz_userdata_tx_in(txdata_int),
        .loopback_in({1'b0,1'b0,1'b0}),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx8b10ben_in(1'b1),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({PCS_PMA_gt_i_n_58,NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrhold_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_int),
        .rxcommadet_out(NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxctrl0_out({NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED[15:2],rxctrl0_out}),
        .rxctrl1_out({NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED[15:2],rxctrl1_out}),
        .rxctrl2_out({NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED[7:2],rxctrl2_out}),
        .rxctrl3_out({NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED[7:2],rxctrl3_out}),
        .rxdfelpmreset_in(1'b0),
        .rxlpmen_in(1'b1),
        .rxmcommaalignen_in(encommaalign_int),
        .rxoutclk_out(rxoutclk_out),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpowerdown,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxresetdone_out(NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED[0]),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(CLK),
        .tx8b10ben_in(1'b1),
        .txbufstatus_out({PCS_PMA_gt_i_n_118,NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED[0]}),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispval_int}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispmode_int}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txcharisk_int}),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txelecidle_in(txpowerdown),
        .txinhibit_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprgdivresetdone_out(NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED[0]),
        .txresetdone_out(NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED[0]),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_1
       (.I0(mgt_tx_reset),
        .I1(gtwiz_reset_tx_done_out_int),
        .O(gtwiz_reset_tx_datapath_in));
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_2
       (.I0(SR),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(gtwiz_reset_rx_datapath_in));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_sync1_i_1
       (.I0(gtwiz_reset_tx_done_out_int),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(data_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PCS_PMA_reset_sync reclock_encommaalign
       (.enablealign(enablealign),
        .reset_out(encommaalign_int),
        .userclk2(userclk2));
  FDRE rxbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_58),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(userclk2),
        .CE(toggle),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    toggle_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(1'b0));
  FDRE txbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_118),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_reg),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispval_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(D),
        .Q(txchardispval_reg),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(mgt_tx_reset));
  FDRE \txdata_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(mgt_tx_reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    rxresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]rxresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    txresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input [0:0]txresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]txresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(txresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1
   (E,
    gtpowergood_out,
    drpclk_in,
    \FSM_sequential_sm_reset_all_reg[0] ,
    Q,
    \FSM_sequential_sm_reset_all_reg[0]_0 );
  output [0:0]E;
  input [0:0]gtpowergood_out;
  input [0:0]drpclk_in;
  input \FSM_sequential_sm_reset_all_reg[0] ;
  input [2:0]Q;
  input \FSM_sequential_sm_reset_all_reg[0]_0 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_all_reg[0] ;
  wire \FSM_sequential_sm_reset_all_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire [0:0]gtpowergood_out;
  wire gtpowergood_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;

  LUT6 #(
    .INIT(64'hAF0FAF00CFFFCFFF)) 
    \FSM_sequential_sm_reset_all[2]_i_1 
       (.I0(gtpowergood_sync),
        .I1(\FSM_sequential_sm_reset_all_reg[0] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\FSM_sequential_sm_reset_all_reg[0]_0 ),
        .I5(Q[1]),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtpowergood_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtpowergood_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10
   (\FSM_sequential_sm_reset_rx_reg[2] ,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    sm_reset_rx_cdr_to_sat_reg,
    rxcdrlock_out,
    drpclk_in,
    sm_reset_rx_cdr_to_clr_reg,
    Q,
    plllock_rx_sync,
    sm_reset_rx_cdr_to_clr,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    sm_reset_rx_cdr_to_sat);
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output sm_reset_rx_cdr_to_sat_reg;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_cdr_to_clr_reg;
  input [2:0]Q;
  input plllock_rx_sync;
  input sm_reset_rx_cdr_to_clr;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input sm_reset_rx_cdr_to_sat;

  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_n_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;
  wire [0:0]rxcdrlock_out;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_2_n_0;
  wire sm_reset_rx_cdr_to_clr_reg;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_reg;

  LUT6 #(
    .INIT(64'h000A000AC0C000C0)) 
    \FSM_sequential_sm_reset_rx[2]_i_4 
       (.I0(sm_reset_rx_cdr_to_sat_reg),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(plllock_rx_sync),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxcdrlock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(i_in_out_reg_n_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'hE)) 
    rxprogdivreset_out_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .O(sm_reset_rx_cdr_to_sat_reg));
  LUT6 #(
    .INIT(64'hFBFFFFFF0800AAAA)) 
    sm_reset_rx_cdr_to_clr_i_1
       (.I0(sm_reset_rx_cdr_to_clr_i_2_n_0),
        .I1(sm_reset_rx_cdr_to_clr_reg),
        .I2(Q[2]),
        .I3(plllock_rx_sync),
        .I4(Q[0]),
        .I5(sm_reset_rx_cdr_to_clr),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h00EF)) 
    sm_reset_rx_cdr_to_clr_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(sm_reset_rx_cdr_to_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2
   (gtwiz_reset_rx_datapath_dly,
    in0,
    drpclk_in);
  output gtwiz_reset_rx_datapath_dly;
  input in0;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire gtwiz_reset_rx_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3
   (D,
    i_in_out_reg_0,
    in0,
    drpclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    Q,
    gtwiz_reset_rx_datapath_dly,
    \FSM_sequential_sm_reset_rx_reg[0]_0 );
  output [1:0]D;
  output i_in_out_reg_0;
  input in0;
  input [0:0]drpclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input [2:0]Q;
  input gtwiz_reset_rx_datapath_dly;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;

  wire [1:0]D;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_datapath_dly;
  wire gtwiz_reset_rx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFF0088FF00FFFFF0)) 
    \FSM_sequential_sm_reset_rx[0]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(gtwiz_reset_rx_pll_and_datapath_dly),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000FFFF8F8F000F)) 
    \FSM_sequential_sm_reset_rx[1]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[2]),
        .I3(gtwiz_reset_rx_pll_and_datapath_dly),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000000E)) 
    \FSM_sequential_sm_reset_rx[2]_i_5 
       (.I0(gtwiz_reset_rx_pll_and_datapath_dly),
        .I1(gtwiz_reset_rx_datapath_dly),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .O(i_in_out_reg_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4
   (E,
    in0,
    drpclk_in,
    Q,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    gtwiz_reset_tx_pll_and_datapath_dly,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 );
  output [0:0]E;
  input in0;
  input [0:0]drpclk_in;
  input [0:0]Q;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input gtwiz_reset_tx_pll_and_datapath_dly;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire [0:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1110)) 
    \FSM_sequential_sm_reset_tx[2]_i_1 
       (.I0(Q),
        .I1(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I2(gtwiz_reset_tx_datapath_dly),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .I4(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I5(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5
   (gtwiz_reset_tx_pll_and_datapath_dly,
    D,
    in0,
    drpclk_in,
    Q);
  output gtwiz_reset_tx_pll_and_datapath_dly;
  output [1:0]D;
  input in0;
  input [0:0]drpclk_in;
  input [2:0]Q;

  wire [1:0]D;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h1F1E)) 
    \FSM_sequential_sm_reset_tx[0]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h0FF1)) 
    \FSM_sequential_sm_reset_tx[1]_i_1 
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_pll_and_datapath_dly),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(D[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6
   (\FSM_sequential_sm_reset_rx_reg[0] ,
    \FSM_sequential_sm_reset_rx_reg[2] ,
    E,
    rxpmaresetdone_out,
    drpclk_in,
    sm_reset_rx_timer_clr_reg,
    Q,
    sm_reset_rx_timer_clr_reg_0,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \FSM_sequential_sm_reset_rx_reg[0]_0 ,
    \FSM_sequential_sm_reset_rx_reg[0]_1 ,
    \FSM_sequential_sm_reset_rx_reg[0]_2 ,
    sm_reset_rx_pll_timer_sat,
    sm_reset_rx_timer_sat);
  output \FSM_sequential_sm_reset_rx_reg[0] ;
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output [0:0]E;
  input [0:0]rxpmaresetdone_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_timer_clr_reg;
  input [2:0]Q;
  input sm_reset_rx_timer_clr_reg_0;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  input sm_reset_rx_pll_timer_sat;
  input sm_reset_rx_timer_sat;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_rx[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_userclk_rx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxpmaresetdone_out;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_timer_clr_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg;
  wire sm_reset_rx_timer_clr_reg_0;
  wire sm_reset_rx_timer_sat;

  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_sm_reset_rx[2]_i_1 
       (.I0(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .I2(\FSM_sequential_sm_reset_rx_reg[0]_1 ),
        .O(E));
  LUT6 #(
    .INIT(64'h2023202000000000)) 
    \FSM_sequential_sm_reset_rx[2]_i_3 
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_2 ),
        .I4(sm_reset_rx_pll_timer_sat),
        .I5(Q[0]),
        .O(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxpmaresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_rx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFAAF00000800)) 
    rxuserrdy_out_i_1
       (.I0(Q[2]),
        .I1(sm_reset_rx_timer_clr_i_2_n_0),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  LUT6 #(
    .INIT(64'hFCCCEFFE0CCCE00E)) 
    sm_reset_rx_timer_clr_i_1
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(sm_reset_rx_timer_clr_reg),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(sm_reset_rx_timer_clr_reg_0),
        .O(\FSM_sequential_sm_reset_rx_reg[0] ));
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_timer_clr_i_2
       (.I0(sm_reset_rx_timer_clr_reg_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(gtwiz_reset_userclk_rx_active_sync),
        .O(sm_reset_rx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7
   (gtwiz_reset_userclk_tx_active_sync,
    \FSM_sequential_sm_reset_tx_reg[2] ,
    i_in_out_reg_0,
    drpclk_in,
    Q,
    sm_reset_tx_timer_clr_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    sm_reset_tx_timer_clr_reg_0,
    plllock_tx_sync,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 ,
    sm_reset_tx_pll_timer_sat);
  output gtwiz_reset_userclk_tx_active_sync;
  output \FSM_sequential_sm_reset_tx_reg[2] ;
  output i_in_out_reg_0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input sm_reset_tx_timer_clr_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input sm_reset_tx_timer_clr_reg_0;
  input plllock_tx_sync;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  input sm_reset_tx_pll_timer_sat;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_tx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_userclk_tx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire plllock_tx_sync;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_timer_clr_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg;
  wire sm_reset_tx_timer_clr_reg_0;

  LUT6 #(
    .INIT(64'h000F000088888888)) 
    \FSM_sequential_sm_reset_tx[2]_i_5 
       (.I0(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I3(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .I4(sm_reset_tx_pll_timer_sat),
        .I5(Q[0]),
        .O(i_in_out_reg_0));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_tx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEBEB282B)) 
    sm_reset_tx_timer_clr_i_1
       (.I0(sm_reset_tx_timer_clr_i_2_n_0),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(sm_reset_tx_timer_clr_reg),
        .O(\FSM_sequential_sm_reset_tx_reg[2] ));
  LUT6 #(
    .INIT(64'hA0C0A0C0F0F000F0)) 
    sm_reset_tx_timer_clr_i_2
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(sm_reset_tx_timer_clr_reg_0),
        .I3(Q[0]),
        .I4(plllock_tx_sync),
        .I5(Q[2]),
        .O(sm_reset_tx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8
   (plllock_rx_sync,
    i_in_out_reg_0,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_rx_done_int_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    Q,
    gtwiz_reset_rx_done_int_reg_0);
  output plllock_rx_sync;
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_rx_done_int_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [2:0]Q;
  input gtwiz_reset_rx_done_int_reg_0;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_done_int;
  wire gtwiz_reset_rx_done_int_reg;
  wire gtwiz_reset_rx_done_int_reg_0;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;

  LUT6 #(
    .INIT(64'hAAC0FFFFAAC00000)) 
    gtwiz_reset_rx_done_int_i_1
       (.I0(plllock_rx_sync),
        .I1(gtwiz_reset_rx_done_int_reg),
        .I2(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int),
        .I5(gtwiz_reset_rx_done_int_reg_0),
        .O(i_in_out_reg_0));
  LUT6 #(
    .INIT(64'h4C40000040400000)) 
    gtwiz_reset_rx_done_int_i_2
       (.I0(plllock_rx_sync),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I4(Q[1]),
        .I5(gtwiz_reset_rx_done_int_reg),
        .O(gtwiz_reset_rx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_rx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h88880000F5FF5555)) 
    sm_reset_rx_timer_clr_i_3
       (.I0(Q[1]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int_reg),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9
   (plllock_tx_sync,
    gtwiz_reset_tx_done_int_reg,
    i_in_out_reg_0,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_tx_done_int_reg_0,
    Q,
    sm_reset_tx_timer_sat,
    gtwiz_reset_tx_done_int_reg_1,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    \FSM_sequential_sm_reset_tx_reg[0] );
  output plllock_tx_sync;
  output gtwiz_reset_tx_done_int_reg;
  output i_in_out_reg_0;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_tx_done_int_reg_0;
  input [2:0]Q;
  input sm_reset_tx_timer_sat;
  input gtwiz_reset_tx_done_int_reg_1;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input \FSM_sequential_sm_reset_tx_reg[0] ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_tx_done_int;
  wire gtwiz_reset_tx_done_int_i_2_n_0;
  wire gtwiz_reset_tx_done_int_reg;
  wire gtwiz_reset_tx_done_int_reg_0;
  wire gtwiz_reset_tx_done_int_reg_1;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_tx_sync;
  wire sm_reset_tx_timer_sat;

  LUT6 #(
    .INIT(64'h00CFA00000000000)) 
    \FSM_sequential_sm_reset_tx[2]_i_4 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(plllock_tx_sync),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_sm_reset_tx_reg[0] ),
        .O(i_in_out_reg_0));
  LUT3 #(
    .INIT(8'hB8)) 
    gtwiz_reset_tx_done_int_i_1
       (.I0(gtwiz_reset_tx_done_int_i_2_n_0),
        .I1(gtwiz_reset_tx_done_int),
        .I2(gtwiz_reset_tx_done_int_reg_0),
        .O(gtwiz_reset_tx_done_int_reg));
  LUT6 #(
    .INIT(64'h4444444444F44444)) 
    gtwiz_reset_tx_done_int_i_2
       (.I0(Q[0]),
        .I1(plllock_tx_sync),
        .I2(sm_reset_tx_timer_sat),
        .I3(gtwiz_reset_tx_done_int_reg_1),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I5(Q[1]),
        .O(gtwiz_reset_tx_done_int_i_2_n_0));
  LUT6 #(
    .INIT(64'h3000404000004040)) 
    gtwiz_reset_tx_done_int_i_3
       (.I0(plllock_tx_sync),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I4(Q[0]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .O(gtwiz_reset_tx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_tx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gthe3_channel
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;
  wire xlnx_opt_;
  wire xlnx_opt__1;
  wire xlnx_opt__2;
  wire xlnx_opt__3;

  assign lopt_2 = xlnx_opt_;
  assign lopt_3 = xlnx_opt__1;
  assign lopt_4 = xlnx_opt__2;
  assign lopt_5 = xlnx_opt__3;
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC
       (.CE(lopt),
        .CESYNC(xlnx_opt_),
        .CLK(rxoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__1));
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC_1
       (.CE(lopt),
        .CESYNC(xlnx_opt__2),
        .CLK(txoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__3));
  (* box_type = "PRIMITIVE" *) 
  GTHE3_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(16'hF800),
    .ADAPT_CFG1(16'h0000),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .A_RXOSCALRESET(1'b0),
    .A_RXPROGDIVRESET(1'b0),
    .A_TXPROGDIVRESET(1'b0),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CDR_SWAP_MODE_EN(1'b0),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(15),
    .CLK_COR_MIN_LAT(12),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG0(16'h67F8),
    .CPLL_CFG1(16'hA4AC),
    .CPLL_CFG2(16'h0007),
    .CPLL_CFG3(6'h00),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(4),
    .CPLL_INIT_CFG0(16'h02B2),
    .CPLL_INIT_CFG1(8'h00),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DDI_CTRL(2'b00),
    .DDI_REALIGN_WAIT(15),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DFE_D_X_REL_POS(1'b0),
    .DFE_VCM_COMP_EN(1'b0),
    .DMONITOR_CFG0(10'h000),
    .DMONITOR_CFG1(8'h00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("FALSE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER0(16'h0000),
    .ES_QUALIFIER1(16'h0000),
    .ES_QUALIFIER2(16'h0000),
    .ES_QUALIFIER3(16'h0000),
    .ES_QUALIFIER4(16'h0000),
    .ES_QUAL_MASK0(16'h0000),
    .ES_QUAL_MASK1(16'h0000),
    .ES_QUAL_MASK2(16'h0000),
    .ES_QUAL_MASK3(16'h0000),
    .ES_QUAL_MASK4(16'h0000),
    .ES_SDATA_MASK0(16'h0000),
    .ES_SDATA_MASK1(16'h0000),
    .ES_SDATA_MASK2(16'h0000),
    .ES_SDATA_MASK3(16'h0000),
    .ES_SDATA_MASK4(16'h0000),
    .EVODD_PHI_CFG(11'b00000000000),
    .EYE_SCAN_SWAP_EN(1'b0),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(5'b00000),
    .GM_BIAS_SELECT(1'b0),
    .LOCAL_MASTER(1'b1),
    .OOBDIVCTL(2'b00),
    .OOB_PWRUP(1'b0),
    .PCI3_AUTO_REALIGN("OVR_1K_BLK"),
    .PCI3_PIPE_RX_ELECIDLE(1'b0),
    .PCI3_RX_ASYNC_EBUF_BYPASS(2'b00),
    .PCI3_RX_ELECIDLE_EI2_ENABLE(1'b0),
    .PCI3_RX_ELECIDLE_H2L_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_H2L_DISABLE(3'b000),
    .PCI3_RX_ELECIDLE_HI_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_LP4_DISABLE(1'b0),
    .PCI3_RX_FIFO_DISABLE(1'b0),
    .PCIE_BUFG_DIV_CTRL(16'h1000),
    .PCIE_RXPCS_CFG_GEN3(16'h02A4),
    .PCIE_RXPMA_CFG(16'h000A),
    .PCIE_TXPCS_CFG_GEN3(16'h2CA4),
    .PCIE_TXPMA_CFG(16'h000A),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD0(16'b0000000000000000),
    .PCS_RSVD1(3'b000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PLL_SEL_MODE_GEN12(2'h0),
    .PLL_SEL_MODE_GEN3(2'h3),
    .PMA_RSV1(16'hF000),
    .PROCESS_PAR(3'b010),
    .RATE_SW_USE_DRP(1'b1),
    .RESET_POWERSAVE_DISABLE(1'b0),
    .RXBUFRESET_TIME(5'b00011),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(0),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(0),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG0(16'h0000),
    .RXCDR_CFG0_GEN3(16'h0000),
    .RXCDR_CFG1(16'h0000),
    .RXCDR_CFG1_GEN3(16'h0000),
    .RXCDR_CFG2(16'h0746),
    .RXCDR_CFG2_GEN3(16'h07E6),
    .RXCDR_CFG3(16'h0000),
    .RXCDR_CFG3_GEN3(16'h0000),
    .RXCDR_CFG4(16'h0000),
    .RXCDR_CFG4_GEN3(16'h0000),
    .RXCDR_CFG5(16'h0000),
    .RXCDR_CFG5_GEN3(16'h0000),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG0(16'h4480),
    .RXCDR_LOCK_CFG1(16'h5FFF),
    .RXCDR_LOCK_CFG2(16'h77C3),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXCFOK_CFG0(16'h4000),
    .RXCFOK_CFG1(16'h0065),
    .RXCFOK_CFG2(16'h002E),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDFELPM_KL_CFG0(16'h0000),
    .RXDFELPM_KL_CFG1(16'h0032),
    .RXDFELPM_KL_CFG2(16'h0000),
    .RXDFE_CFG0(16'h0A00),
    .RXDFE_CFG1(16'h0000),
    .RXDFE_GC_CFG0(16'h0000),
    .RXDFE_GC_CFG1(16'h7870),
    .RXDFE_GC_CFG2(16'h0000),
    .RXDFE_H2_CFG0(16'h0000),
    .RXDFE_H2_CFG1(16'h0000),
    .RXDFE_H3_CFG0(16'h4000),
    .RXDFE_H3_CFG1(16'h0000),
    .RXDFE_H4_CFG0(16'h2000),
    .RXDFE_H4_CFG1(16'h0003),
    .RXDFE_H5_CFG0(16'h2000),
    .RXDFE_H5_CFG1(16'h0003),
    .RXDFE_H6_CFG0(16'h2000),
    .RXDFE_H6_CFG1(16'h0000),
    .RXDFE_H7_CFG0(16'h2000),
    .RXDFE_H7_CFG1(16'h0000),
    .RXDFE_H8_CFG0(16'h2000),
    .RXDFE_H8_CFG1(16'h0000),
    .RXDFE_H9_CFG0(16'h2000),
    .RXDFE_H9_CFG1(16'h0000),
    .RXDFE_HA_CFG0(16'h2000),
    .RXDFE_HA_CFG1(16'h0000),
    .RXDFE_HB_CFG0(16'h2000),
    .RXDFE_HB_CFG1(16'h0000),
    .RXDFE_HC_CFG0(16'h0000),
    .RXDFE_HC_CFG1(16'h0000),
    .RXDFE_HD_CFG0(16'h0000),
    .RXDFE_HD_CFG1(16'h0000),
    .RXDFE_HE_CFG0(16'h0000),
    .RXDFE_HE_CFG1(16'h0000),
    .RXDFE_HF_CFG0(16'h0000),
    .RXDFE_HF_CFG1(16'h0000),
    .RXDFE_OS_CFG0(16'h8000),
    .RXDFE_OS_CFG1(16'h0000),
    .RXDFE_UT_CFG0(16'h8000),
    .RXDFE_UT_CFG1(16'h0003),
    .RXDFE_VP_CFG0(16'hAA00),
    .RXDFE_VP_CFG1(16'h0033),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(16'h0030),
    .RXELECIDLE_CFG("Sigcfg_4"),
    .RXGBOX_FIFO_INIT_RD_ADDR(4),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_CFG(16'h0000),
    .RXLPM_GC_CFG(16'h1000),
    .RXLPM_KH_CFG0(16'h0000),
    .RXLPM_KH_CFG1(16'h0002),
    .RXLPM_OS_CFG0(16'h8000),
    .RXLPM_OS_CFG1(16'h0002),
    .RXOOB_CFG(9'b000000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00011),
    .RXPHBEACON_CFG(16'h0000),
    .RXPHDLY_CFG(16'h2020),
    .RXPHSAMP_CFG(16'h2100),
    .RXPHSLIP_CFG(16'h6622),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_CFG0(2'b01),
    .RXPI_CFG1(2'b01),
    .RXPI_CFG2(2'b01),
    .RXPI_CFG3(2'b01),
    .RXPI_CFG4(1'b1),
    .RXPI_CFG5(1'b1),
    .RXPI_CFG6(3'b011),
    .RXPI_LPM(1'b0),
    .RXPI_VREFSEL(1'b0),
    .RXPMACLK_SEL("DATA"),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXPRBS_LINKACQ_CNT(15),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b0),
    .RX_AFE_CM_EN(1'b0),
    .RX_BIAS_CFG0(16'h0AB4),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CAPFF_SARC_ENB(1'b0),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_EN(1'b1),
    .RX_CLK_SLIP_OVRD(5'b00000),
    .RX_CM_BUF_CFG(4'b1010),
    .RX_CM_BUF_PD(1'b0),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(4'b1010),
    .RX_CTLE3_LPF(8'b00000001),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFELPM_CFG0(4'b0110),
    .RX_DFELPM_CFG1(1'b1),
    .RX_DFELPM_KLKH_AGC_STUP_EN(1'b1),
    .RX_DFE_AGC_CFG0(2'b10),
    .RX_DFE_AGC_CFG1(3'b000),
    .RX_DFE_KL_LPM_KH_CFG0(2'b01),
    .RX_DFE_KL_LPM_KH_CFG1(3'b000),
    .RX_DFE_KL_LPM_KL_CFG0(2'b01),
    .RX_DFE_KL_LPM_KL_CFG1(3'b000),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_DIVRESET_TIME(5'b00001),
    .RX_EN_HI_LR(1'b0),
    .RX_EYESCAN_VS_CODE(7'b0000000),
    .RX_EYESCAN_VS_NEG_DIR(1'b0),
    .RX_EYESCAN_VS_RANGE(2'b00),
    .RX_EYESCAN_VS_UT_SIGN(1'b0),
    .RX_FABINT_USRCLK_FLOP(1'b0),
    .RX_INT_DATAWIDTH(0),
    .RX_PMA_POWER_SAVE(1'b0),
    .RX_PROGDIV_CFG(0.000000),
    .RX_SAMPLE_PERIOD(3'b111),
    .RX_SIG_VALID_DLY(11),
    .RX_SUM_DFETAPREP_EN(1'b0),
    .RX_SUM_IREF_TUNE(4'b1100),
    .RX_SUM_RES_CTRL(2'b11),
    .RX_SUM_VCMTUNE(4'b0000),
    .RX_SUM_VCM_OVWR(1'b0),
    .RX_SUM_VREF_TUNE(3'b000),
    .RX_TUNE_AFE_OS(2'b10),
    .RX_WIDEMODE_CDR(1'b0),
    .RX_XCLK_SEL("RXDES"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b1110),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_MODE("FAST"),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL(1'b0),
    .SIM_VERSION(2),
    .TAPDLY_SET_TX(2'h0),
    .TEMPERATUR_PAR(4'b0010),
    .TERM_RCAL_CFG(15'b100001000010000),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV0(8'h00),
    .TST_RSV1(8'h00),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h0009),
    .TXDLY_LCFG(16'h0050),
    .TXDRVBIAS_N(4'b1010),
    .TXDRVBIAS_P(4'b1010),
    .TXFIFO_ADDR_CFG("LOW"),
    .TXGBOX_FIFO_INIT_RD_ADDR(4),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00011),
    .TXPHDLY_CFG0(16'h2020),
    .TXPHDLY_CFG1(16'h0075),
    .TXPH_CFG(16'h0980),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG0(2'b01),
    .TXPI_CFG1(2'b01),
    .TXPI_CFG2(2'b01),
    .TXPI_CFG3(1'b1),
    .TXPI_CFG4(1'b1),
    .TXPI_CFG5(3'b011),
    .TXPI_GRAY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b1),
    .TXPI_LPM(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b001),
    .TXPI_VREFSEL(1'b0),
    .TXPMARESET_TIME(5'b00011),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_EN(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DCD_CFG(6'b000010),
    .TX_DCD_EN(1'b0),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DIVRESET_TIME(5'b00001),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b100),
    .TX_EIDLE_DEASSERT_DELAY(3'b011),
    .TX_EML_PHI_TUNE(1'b0),
    .TX_FABINT_USRCLK_FLOP(1'b0),
    .TX_IDLE_DATA_ZERO(1'b0),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001111),
    .TX_MARGIN_FULL_1(7'b1001110),
    .TX_MARGIN_FULL_2(7'b1001100),
    .TX_MARGIN_FULL_3(7'b1001010),
    .TX_MARGIN_FULL_4(7'b1001000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000101),
    .TX_MARGIN_LOW_2(7'b1000011),
    .TX_MARGIN_LOW_3(7'b1000010),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_MODE_SEL(3'b000),
    .TX_PMADATA_OPT(1'b0),
    .TX_PMA_POWER_SAVE(1'b0),
    .TX_PROGCLK_SEL("CPLL"),
    .TX_PROGDIV_CFG(20.000000),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h0032),
    .TX_RXDETECT_REF(3'b100),
    .TX_SAMPLE_PERIOD(3'b111),
    .TX_SARC_LPBK_ENB(1'b0),
    .TX_XCLK_SEL("TXOUT"),
    .USE_PCS_CLK_PHASE_SEL(1'b0),
    .WB_MODE(2'b00)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST 
       (.BUFGTCE({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 }),
        .BUFGTCEMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 }),
        .BUFGTDIV({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 }),
        .BUFGTRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 }),
        .BUFGTRSTMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 }),
        .CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .CPLLFBCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ),
        .CPLLLOCK(cplllock_out),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .CPLLREFCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(1'b0),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 }),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(drpclk_in),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 }),
        .DRPEN(1'b0),
        .DRPRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ),
        .DRPWE(1'b0),
        .EVODDPHICALDONE(1'b0),
        .EVODDPHICALSTART(1'b0),
        .EVODDPHIDRDEN(1'b0),
        .EVODDPHIDWREN(1'b0),
        .EVODDPHIXRDEN(1'b0),
        .EVODDPHIXWREN(1'b0),
        .EYESCANDATAERROR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTHRXN(gthrxn_in),
        .GTHRXP(gthrxp_in),
        .GTHTXN(gthtxn_out),
        .GTHTXP(gthtxp_out),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTPOWERGOOD(gtpowergood_out),
        .GTREFCLK0(gtrefclk0_in),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(\gen_gtwizard_gthe3.gtrxreset_int ),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(\gen_gtwizard_gthe3.gttxreset_int ),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .LPBKRXTXSEREN(1'b0),
        .LPBKTXRXSEREN(1'b0),
        .PCIEEQRXEQADAPTDONE(1'b0),
        .PCIERATEGEN3(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ),
        .PCIERATEIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ),
        .PCIERATEQPLLPD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 }),
        .PCIERATEQPLLRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 }),
        .PCIERSTIDLE(1'b0),
        .PCIERSTTXSYNCSTART(1'b0),
        .PCIESYNCTXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ),
        .PCIEUSERGEN3RDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ),
        .PCIEUSERPHYSTATUSRST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ),
        .PCIEUSERRATEDONE(1'b0),
        .PCIEUSERRATESTART(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 }),
        .PHYSTATUS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ),
        .PINRSRVDAS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 }),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLL0CLK(1'b0),
        .QPLL0REFCLK(1'b0),
        .QPLL1CLK(1'b0),
        .QPLL1REFCLK(1'b0),
        .RESETEXCEPTION(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ),
        .RESETOVRD(1'b0),
        .RSTCLKENTX(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({rxbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 }),
        .RXBYTEISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ),
        .RXBYTEREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(rxcdrlock_out),
        .RXCDROVRDEN(1'b0),
        .RXCDRPHDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ),
        .RXCHANISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ),
        .RXCHANREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 }),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(rxclkcorcnt_out),
        .RXCOMINITDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ),
        .RXCOMMADET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ),
        .RXCOMWAKEDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ),
        .RXCTRL0({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ,rxctrl0_out}),
        .RXCTRL1({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ,rxctrl1_out}),
        .RXCTRL2({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ,rxctrl2_out}),
        .RXCTRL3({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ,rxctrl3_out}),
        .RXDATA({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ,gtwiz_userdata_rx_out}),
        .RXDATAEXTENDRSVD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 }),
        .RXDATAVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 }),
        .RXDFEAGCCTRL({1'b0,1'b1}),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP10HOLD(1'b0),
        .RXDFETAP10OVRDEN(1'b0),
        .RXDFETAP11HOLD(1'b0),
        .RXDFETAP11OVRDEN(1'b0),
        .RXDFETAP12HOLD(1'b0),
        .RXDFETAP12OVRDEN(1'b0),
        .RXDFETAP13HOLD(1'b0),
        .RXDFETAP13OVRDEN(1'b0),
        .RXDFETAP14HOLD(1'b0),
        .RXDFETAP14OVRDEN(1'b0),
        .RXDFETAP15HOLD(1'b0),
        .RXDFETAP15OVRDEN(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFETAP6HOLD(1'b0),
        .RXDFETAP6OVRDEN(1'b0),
        .RXDFETAP7HOLD(1'b0),
        .RXDFETAP7OVRDEN(1'b0),
        .RXDFETAP8HOLD(1'b0),
        .RXDFETAP8OVRDEN(1'b0),
        .RXDFETAP9HOLD(1'b0),
        .RXDFETAP9OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ),
        .RXELECIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 }),
        .RXHEADERVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 }),
        .RXLATCLK(1'b0),
        .RXLPMEN(1'b1),
        .RXLPMGCHOLD(1'b0),
        .RXLPMGCOVRDEN(1'b0),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXLPMOSHOLD(1'b0),
        .RXLPMOSOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(rxmcommaalignen_in),
        .RXMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 }),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTCFG({1'b1,1'b1,1'b0,1'b1}),
        .RXOSINTDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ),
        .RXOSINTEN(1'b1),
        .RXOSINTHOLD(1'b0),
        .RXOSINTOVRDEN(1'b0),
        .RXOSINTSTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ),
        .RXOSINTSTROBE(1'b0),
        .RXOSINTSTROBEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ),
        .RXOSINTSTROBESTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ),
        .RXOSINTTESTOVRDEN(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk_out),
        .RXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ),
        .RXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(rxmcommaalignen_in),
        .RXPCSRESET(1'b0),
        .RXPD({rxpd_in,rxpd_in}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ),
        .RXPHALIGNEN(1'b0),
        .RXPHALIGNERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ),
        .RXPHDLYPD(1'b1),
        .RXPHDLYRESET(1'b0),
        .RXPHOVRDEN(1'b0),
        .RXPLLCLKSEL({1'b0,1'b0}),
        .RXPMARESET(1'b0),
        .RXPMARESETDONE(rxpmaresetdone_out),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ),
        .RXPRBSLOCKED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ),
        .RXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .RXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ),
        .RXPROGDIVRESET(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .RXQPIEN(1'b0),
        .RXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ),
        .RXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ),
        .RXRATEMODE(1'b0),
        .RXRECCLKOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ),
        .RXRESETDONE(rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSLIDERDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ),
        .RXSLIPDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ),
        .RXSLIPOUTCLK(1'b0),
        .RXSLIPOUTCLKRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ),
        .RXSLIPPMA(1'b0),
        .RXSLIPPMARDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ),
        .RXSTARTOFSEQ({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 }),
        .RXSTATUS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 }),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .RXUSRCLK(rxusrclk_in),
        .RXUSRCLK2(rxusrclk_in),
        .RXVALID(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b0,1'b0,1'b0}),
        .TXBUFSTATUS({txbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 }),
        .TXCOMFINISH(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXCTRL0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in}),
        .TXCTRL1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in}),
        .TXCTRL2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in}),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtwiz_userdata_tx_in}),
        .TXDATAEXTENDRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(txelecidle_in),
        .TXHEADER({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXLATCLK(1'b0),
        .TXMAINCURSOR({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk_out),
        .TXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ),
        .TXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ),
        .TXOUTCLKSEL({1'b1,1'b0,1'b1}),
        .TXPCSRESET(1'b0),
        .TXPD({txelecidle_in,txelecidle_in}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b1),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b0),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPLLCLKSEL({1'b0,1'b0}),
        .TXPMARESET(1'b0),
        .TXPMARESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ),
        .TXPROGDIVRESET(\gen_gtwizard_gthe3.txprogdivreset_int ),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ),
        .TXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(\gen_gtwizard_gthe3.txuserrdy_int ),
        .TXUSRCLK(rxusrclk_in),
        .TXUSRCLK2(rxusrclk_in));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_meta_i_1__2
       (.I0(cplllock_out),
        .O(rst_in0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_gtwiz_reset
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    \gen_gtwizard_gthe3.gttxreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    gtpowergood_out,
    cplllock_out,
    rxpmaresetdone_out,
    rxcdrlock_out,
    drpclk_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    rst_in0,
    rxusrclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    gtwiz_reset_rx_datapath_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync );
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output \gen_gtwizard_gthe3.gttxreset_int ;
  output \gen_gtwizard_gthe3.txuserrdy_int ;
  output \gen_gtwizard_gthe3.rxprogdivreset_int ;
  output \gen_gtwizard_gthe3.gtrxreset_int ;
  output \gen_gtwizard_gthe3.rxuserrdy_int ;
  output \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]gtpowergood_out;
  input [0:0]cplllock_out;
  input [0:0]rxpmaresetdone_out;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in0;
  input [0:0]rxusrclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;

  wire \FSM_sequential_sm_reset_all[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_all[2]_i_4_n_0 ;
  wire \FSM_sequential_sm_reset_rx[1]_i_2_n_0 ;
  wire \FSM_sequential_sm_reset_rx[2]_i_6_n_0 ;
  wire \FSM_sequential_sm_reset_tx[2]_i_3_n_0 ;
  wire bit_synchronizer_gtpowergood_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2;
  wire bit_synchronizer_plllock_rx_inst_n_1;
  wire bit_synchronizer_plllock_rx_inst_n_2;
  wire bit_synchronizer_plllock_tx_inst_n_1;
  wire bit_synchronizer_plllock_tx_inst_n_2;
  wire bit_synchronizer_rxcdrlock_inst_n_0;
  wire bit_synchronizer_rxcdrlock_inst_n_1;
  wire bit_synchronizer_rxcdrlock_inst_n_2;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gtpowergood_out;
  wire gttxreset_out_i_3_n_0;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_rx_datapath_dly;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_datapath_sync;
  wire gtwiz_reset_rx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_sync;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_datapath_sync;
  wire gtwiz_reset_tx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_sync;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire p_0_in;
  wire [9:0]p_0_in__0;
  wire [9:0]p_0_in__1;
  wire [2:0]p_1_in;
  wire plllock_rx_sync;
  wire plllock_tx_sync;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_3;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_3;
  wire rst_in0;
  wire [0:0]rxcdrlock_out;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [2:0]sm_reset_all;
  wire [2:0]sm_reset_all__0;
  wire sm_reset_all_timer_clr_i_1_n_0;
  wire sm_reset_all_timer_clr_i_2_n_0;
  wire sm_reset_all_timer_clr_reg_n_0;
  wire [2:0]sm_reset_all_timer_ctr;
  wire sm_reset_all_timer_ctr0_n_0;
  wire \sm_reset_all_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_all_timer_sat;
  wire sm_reset_all_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_rx;
  wire [2:0]sm_reset_rx__0;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_3_n_0;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ;
  wire [25:0]sm_reset_rx_cdr_to_ctr_reg;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_i_1_n_0;
  wire sm_reset_rx_cdr_to_sat_i_2_n_0;
  wire sm_reset_rx_cdr_to_sat_i_3_n_0;
  wire sm_reset_rx_cdr_to_sat_i_4_n_0;
  wire sm_reset_rx_cdr_to_sat_i_5_n_0;
  wire sm_reset_rx_cdr_to_sat_i_6_n_0;
  wire sm_reset_rx_pll_timer_clr_i_1_n_0;
  wire sm_reset_rx_pll_timer_clr_reg_n_0;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_rx_pll_timer_ctr_reg;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_pll_timer_sat_i_1_n_0;
  wire sm_reset_rx_pll_timer_sat_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_rx_timer_ctr;
  wire sm_reset_rx_timer_ctr0_n_0;
  wire \sm_reset_rx_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_rx_timer_sat;
  wire sm_reset_rx_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_tx;
  wire [2:0]sm_reset_tx__0;
  wire sm_reset_tx_pll_timer_clr_i_1_n_0;
  wire sm_reset_tx_pll_timer_clr_reg_n_0;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_tx_pll_timer_ctr_reg;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_pll_timer_sat_i_1_n_0;
  wire sm_reset_tx_pll_timer_sat_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_tx_timer_ctr;
  wire sm_reset_tx_timer_sat;
  wire sm_reset_tx_timer_sat_i_1_n_0;
  wire txuserrdy_out_i_3_n_0;
  wire [7:1]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED ;
  wire [7:2]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00FFF70000FFFFFF)) 
    \FSM_sequential_sm_reset_all[0]_i_1 
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .I3(sm_reset_all[2]),
        .I4(sm_reset_all[1]),
        .I5(sm_reset_all[0]),
        .O(sm_reset_all__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h34)) 
    \FSM_sequential_sm_reset_all[1]_i_1 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[0]),
        .O(sm_reset_all__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h4A)) 
    \FSM_sequential_sm_reset_all[2]_i_2 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .O(sm_reset_all__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_sm_reset_all[2]_i_3 
       (.I0(sm_reset_all_timer_sat),
        .I1(gtwiz_reset_rx_done_int_reg_n_0),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_all[2]_i_4 
       (.I0(sm_reset_all_timer_clr_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[0]),
        .Q(sm_reset_all[0]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[1]),
        .Q(sm_reset_all[1]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[2]),
        .Q(sm_reset_all[2]),
        .R(gtwiz_reset_all_sync));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_sm_reset_rx[1]_i_2 
       (.I0(sm_reset_rx_timer_sat),
        .I1(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDFD8888DDDD8888)) 
    \FSM_sequential_sm_reset_rx[2]_i_2 
       (.I0(sm_reset_rx[1]),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx_timer_sat),
        .I3(sm_reset_rx_timer_clr_reg_n_0),
        .I4(sm_reset_rx[2]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .O(sm_reset_rx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h00004000)) 
    \FSM_sequential_sm_reset_rx[2]_i_6 
       (.I0(sm_reset_rx[0]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[0]),
        .Q(sm_reset_rx[0]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[1]),
        .Q(sm_reset_rx[1]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[2]),
        .Q(sm_reset_rx[2]),
        .R(gtwiz_reset_rx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \FSM_sequential_sm_reset_tx[2]_i_2 
       (.I0(sm_reset_tx[0]),
        .I1(sm_reset_tx[1]),
        .I2(sm_reset_tx[2]),
        .O(sm_reset_tx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_sm_reset_tx[2]_i_3 
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .O(\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[0]),
        .Q(sm_reset_tx[0]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[1]),
        .Q(sm_reset_tx[1]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[2]),
        .Q(sm_reset_tx[2]),
        .R(gtwiz_reset_tx_any_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1 bit_synchronizer_gtpowergood_inst
       (.E(bit_synchronizer_gtpowergood_inst_n_0),
        .\FSM_sequential_sm_reset_all_reg[0] (\FSM_sequential_sm_reset_all[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_all_reg[0]_0 (\FSM_sequential_sm_reset_all[2]_i_4_n_0 ),
        .Q(sm_reset_all),
        .drpclk_in(drpclk_in),
        .gtpowergood_out(gtpowergood_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2 bit_synchronizer_gtwiz_reset_rx_datapath_dly_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .in0(gtwiz_reset_rx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3 bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst
       (.D(sm_reset_rx__0[1:0]),
        .\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4 bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst
       (.E(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0] (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (bit_synchronizer_plllock_tx_inst_n_2),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .Q(sm_reset_tx[0]),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5 bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst
       (.D(sm_reset_tx__0[1:0]),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6 bit_synchronizer_gtwiz_reset_userclk_rx_active_inst
       (.E(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[0]_1 (bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0]_2 (sm_reset_rx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .sm_reset_rx_pll_timer_sat(sm_reset_rx_pll_timer_sat),
        .sm_reset_rx_timer_clr_reg(bit_synchronizer_plllock_rx_inst_n_2),
        .sm_reset_rx_timer_clr_reg_0(sm_reset_rx_timer_clr_reg_n_0),
        .sm_reset_rx_timer_sat(sm_reset_rx_timer_sat));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7 bit_synchronizer_gtwiz_reset_userclk_tx_active_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (txuserrdy_out_i_3_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (sm_reset_tx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_tx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_pll_timer_sat(sm_reset_tx_pll_timer_sat),
        .sm_reset_tx_timer_clr_reg(sm_reset_tx_timer_clr_reg_n_0),
        .sm_reset_tx_timer_clr_reg_0(gttxreset_out_i_3_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8 bit_synchronizer_plllock_rx_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_plllock_rx_inst_n_2),
        .Q(sm_reset_rx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_done_int_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_done_int_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_rx_inst_n_1),
        .plllock_rx_sync(plllock_rx_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9 bit_synchronizer_plllock_tx_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (gttxreset_out_i_3_n_0),
        .Q(sm_reset_tx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_tx_done_int_reg(bit_synchronizer_plllock_tx_inst_n_1),
        .gtwiz_reset_tx_done_int_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .gtwiz_reset_tx_done_int_reg_1(sm_reset_tx_timer_clr_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_tx_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_timer_sat(sm_reset_tx_timer_sat));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10 bit_synchronizer_rxcdrlock_inst
       (.\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rxcdrlock_out(rxcdrlock_out),
        .sm_reset_rx_cdr_to_clr(sm_reset_rx_cdr_to_clr),
        .sm_reset_rx_cdr_to_clr_reg(sm_reset_rx_cdr_to_clr_i_3_n_0),
        .sm_reset_rx_cdr_to_sat(sm_reset_rx_cdr_to_sat),
        .sm_reset_rx_cdr_to_sat_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  LUT2 #(
    .INIT(4'hE)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\gen_gtwizard_gthe3.cpllpd_ch_int ));
  FDRE #(
    .INIT(1'b1)) 
    gtrxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.gtrxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h2)) 
    gttxreset_out_i_3
       (.I0(sm_reset_tx_timer_sat),
        .I1(sm_reset_tx_timer_clr_reg_n_0),
        .O(gttxreset_out_i_3_n_0));
  FDRE #(
    .INIT(1'b1)) 
    gttxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.gttxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hF740)) 
    gtwiz_reset_rx_datapath_int_i_1
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_rx_inst_n_1),
        .Q(gtwiz_reset_rx_done_int_reg_n_0),
        .R(gtwiz_reset_rx_any_sync));
  LUT4 #(
    .INIT(16'hF704)) 
    gtwiz_reset_rx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[2]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_tx_inst_n_1),
        .Q(gtwiz_reset_tx_done_int_reg_n_0),
        .R(gtwiz_reset_tx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hFB02)) 
    gtwiz_reset_tx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    pllreset_rx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    pllreset_tx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer reset_synchronizer_gtwiz_reset_all_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_all_sync(gtwiz_reset_all_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11 reset_synchronizer_gtwiz_reset_rx_any_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[1]_0 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[1]_1 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .gtrxreset_out_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0),
        .rst_in_out_reg_1(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .rxprogdivreset_out_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12 reset_synchronizer_gtwiz_reset_rx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .in0(gtwiz_reset_rx_datapath_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13 reset_synchronizer_gtwiz_reset_rx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14 reset_synchronizer_gtwiz_reset_tx_any_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .\FSM_sequential_sm_reset_tx_reg[1] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .\FSM_sequential_sm_reset_tx_reg[1]_0 (reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gttxreset_out_reg(gttxreset_out_i_3_n_0),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .plllock_tx_sync(plllock_tx_sync),
        .rst_in_out_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .txuserrdy_out_reg(txuserrdy_out_i_3_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15 reset_synchronizer_gtwiz_reset_tx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .in0(gtwiz_reset_tx_datapath_sync));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16 reset_synchronizer_gtwiz_reset_tx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer reset_synchronizer_rx_done_inst
       (.gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17 reset_synchronizer_tx_done_inst
       (.gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18 reset_synchronizer_txprogdivreset_inst
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .rst_in0(rst_in0));
  FDRE #(
    .INIT(1'b1)) 
    rxprogdivreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEFFA200A)) 
    sm_reset_all_timer_clr_i_1
       (.I0(sm_reset_all_timer_clr_i_2_n_0),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(sm_reset_all[0]),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_clr_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000B0003333BB33)) 
    sm_reset_all_timer_clr_i_2
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all[2]),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .I5(sm_reset_all[1]),
        .O(sm_reset_all_timer_clr_i_2_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_all_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_clr_i_1_n_0),
        .Q(sm_reset_all_timer_clr_reg_n_0),
        .S(gtwiz_reset_all_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_all_timer_ctr0
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .O(sm_reset_all_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_all_timer_ctr[0]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .O(\sm_reset_all_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_all_timer_ctr[1]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .O(\sm_reset_all_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_all_timer_ctr[2]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .I2(sm_reset_all_timer_ctr[2]),
        .O(\sm_reset_all_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[0]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[1]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[2]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_all_timer_sat_i_1
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_all_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_sat_i_1_n_0),
        .Q(sm_reset_all_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_cdr_to_clr_i_3
       (.I0(sm_reset_rx_timer_clr_reg_n_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(sm_reset_rx[1]),
        .O(sm_reset_rx_cdr_to_clr_i_3_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_cdr_to_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx_cdr_to_clr),
        .S(gtwiz_reset_rx_any_sync));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_1 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[1]),
        .I2(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ),
        .I3(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ),
        .I4(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ),
        .I5(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_3 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[18]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[14]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_4 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[24]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[20]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_5 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[12]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[9]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_6 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[7]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[2]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_7 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[0]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 }),
        .S({sm_reset_rx_cdr_to_ctr_reg[7:1],\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[10] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[10]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[11] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[11]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[12] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[12]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[13] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[13]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[14] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[14]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[15] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[15]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[16] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[16]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[16]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[17] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[17]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[18] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[18]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[19] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[19]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[1]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[20] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[20]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[21] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[21]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[22] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[22]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[23] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[23]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[24] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[24]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[24]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED [7:1],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED [7:2],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,sm_reset_rx_cdr_to_ctr_reg[25:24]}));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[25] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[25]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[2]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[3]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[4]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[5]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[6]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[7]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[8]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[8]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[9]),
        .R(sm_reset_rx_cdr_to_clr));
  LUT3 #(
    .INIT(8'h0E)) 
    sm_reset_rx_cdr_to_sat_i_1
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(sm_reset_rx_cdr_to_sat_i_2_n_0),
        .I2(sm_reset_rx_cdr_to_clr),
        .O(sm_reset_rx_cdr_to_sat_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sm_reset_rx_cdr_to_sat_i_2
       (.I0(sm_reset_rx_cdr_to_sat_i_3_n_0),
        .I1(sm_reset_rx_cdr_to_sat_i_4_n_0),
        .I2(sm_reset_rx_cdr_to_sat_i_5_n_0),
        .I3(sm_reset_rx_cdr_to_sat_i_6_n_0),
        .I4(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[1]),
        .O(sm_reset_rx_cdr_to_sat_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    sm_reset_rx_cdr_to_sat_i_3
       (.I0(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[2]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[7]),
        .O(sm_reset_rx_cdr_to_sat_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    sm_reset_rx_cdr_to_sat_i_4
       (.I0(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[20]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[24]),
        .O(sm_reset_rx_cdr_to_sat_i_4_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    sm_reset_rx_cdr_to_sat_i_5
       (.I0(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[14]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[18]),
        .O(sm_reset_rx_cdr_to_sat_i_5_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    sm_reset_rx_cdr_to_sat_i_6
       (.I0(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[9]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[12]),
        .O(sm_reset_rx_cdr_to_sat_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_cdr_to_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_cdr_to_sat_i_1_n_0),
        .Q(sm_reset_rx_cdr_to_sat),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF3000B)) 
    sm_reset_rx_pll_timer_clr_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx[2]),
        .I4(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_rx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_rx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(p_0_in__1[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_rx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_rx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_rx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .O(p_0_in__1[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(p_0_in__1[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[0]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[0]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[1]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[1]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[2]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[2]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[3]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[3]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[4]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[4]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[5]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[5]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[6]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[6]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[7]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[7]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[8]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[8]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[9]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[9]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_rx_pll_timer_sat_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_rx_pll_timer_sat_i_2
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(sm_reset_rx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .Q(sm_reset_rx_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_rx_timer_ctr0
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .O(sm_reset_rx_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .O(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .O(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .I2(sm_reset_rx_timer_ctr[2]),
        .O(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[0]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[1]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[2]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_rx_timer_sat_i_1
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(sm_reset_rx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hEFEF1101)) 
    sm_reset_tx_pll_timer_clr_i_1
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx[0]),
        .I3(sm_reset_tx_pll_timer_sat),
        .I4(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_tx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_tx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(p_0_in__0[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_tx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_tx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_tx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .O(p_0_in__0[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(p_0_in__0[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[0]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[0]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[1]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[1]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[2]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[2]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[3]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[3]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[4]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[4]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[5]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[5]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[6]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[6]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[7]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[7]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[8]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[8]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[9]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[9]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_tx_pll_timer_sat_i_1
       (.I0(sm_reset_tx_pll_timer_sat),
        .I1(sm_reset_tx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_tx_pll_timer_sat_i_2
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(sm_reset_tx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_tx_timer_ctr0
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .O(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .I2(sm_reset_tx_timer_ctr[2]),
        .O(p_1_in[2]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[0]),
        .Q(sm_reset_tx_timer_ctr[0]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[1]),
        .Q(sm_reset_tx_timer_ctr[1]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[2]),
        .Q(sm_reset_tx_timer_ctr[2]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_tx_timer_sat_i_1
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .I3(sm_reset_tx_timer_sat),
        .I4(sm_reset_tx_timer_clr_reg_n_0),
        .O(sm_reset_tx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    txuserrdy_out_i_3
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx_timer_clr_reg_n_0),
        .I3(sm_reset_tx_timer_sat),
        .O(txuserrdy_out_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    txuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.txuserrdy_int ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer
   (gtwiz_reset_rx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_rx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_rx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17
   (gtwiz_reset_tx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_tx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_tx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1__0_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1__0
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_tx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer
   (gtwiz_reset_all_sync,
    drpclk_in,
    gtwiz_reset_all_in);
  output gtwiz_reset_all_sync;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_all_in),
        .Q(gtwiz_reset_all_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11
   (gtwiz_reset_rx_any_sync,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[1]_0 ,
    \FSM_sequential_sm_reset_rx_reg[1]_1 ,
    drpclk_in,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ,
    rxprogdivreset_out_reg,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    plllock_rx_sync,
    gtrxreset_out_reg,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    rst_in_out_reg_0,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_1);
  output gtwiz_reset_rx_any_sync;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  input rxprogdivreset_out_reg;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input plllock_rx_sync;
  input gtrxreset_out_reg;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input rst_in_out_reg_0;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_1;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire gtrxreset_out_i_2_n_0;
  wire gtrxreset_out_reg;
  wire gtwiz_reset_rx_any;
  wire gtwiz_reset_rx_any_sync;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire plllock_rx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  wire rst_in_out_reg_1;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire rxprogdivreset_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gtrxreset_out_i_1
       (.I0(Q[1]),
        .I1(gtrxreset_out_i_2_n_0),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtrxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gtrxreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h1)) 
    gtrxreset_out_i_2
       (.I0(gtwiz_reset_rx_any_sync),
        .I1(Q[2]),
        .O(gtrxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_rx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_rx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  LUT3 #(
    .INIT(8'hFE)) 
    rst_in_meta_i_1
       (.I0(rst_in_out_reg_0),
        .I1(gtwiz_reset_rx_datapath_in),
        .I2(rst_in_out_reg_1),
        .O(gtwiz_reset_rx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_rx_any),
        .Q(gtwiz_reset_rx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hFFFBFFFF00120012)) 
    rxprogdivreset_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_rx_any_sync),
        .I4(rxprogdivreset_out_reg),
        .I5(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_0 ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12
   (in0,
    drpclk_in,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_0;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire in0;
  wire rst_in0_0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__0
       (.I0(gtwiz_reset_rx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(rst_in0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14
   (gtwiz_reset_tx_any_sync,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    \FSM_sequential_sm_reset_tx_reg[1]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    drpclk_in,
    gtwiz_reset_tx_datapath_in,
    rst_in_out_reg_0,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ,
    plllock_tx_sync,
    gttxreset_out_reg,
    \gen_gtwizard_gthe3.gttxreset_int ,
    txuserrdy_out_reg,
    gtwiz_reset_userclk_tx_active_sync,
    \gen_gtwizard_gthe3.txuserrdy_int );
  output gtwiz_reset_tx_any_sync;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  output \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_tx_reg[0] ;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in_out_reg_0;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  input plllock_tx_sync;
  input gttxreset_out_reg;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input txuserrdy_out_reg;
  input gtwiz_reset_userclk_tx_active_sync;
  input \gen_gtwizard_gthe3.txuserrdy_int ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire gttxreset_out_i_2_n_0;
  wire gttxreset_out_reg;
  wire gtwiz_reset_tx_any;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire plllock_tx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire txuserrdy_out_i_2_n_0;
  wire txuserrdy_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gttxreset_out_i_1
       (.I0(Q[1]),
        .I1(gttxreset_out_i_2_n_0),
        .I2(plllock_tx_sync),
        .I3(Q[0]),
        .I4(gttxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gttxreset_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    gttxreset_out_i_2
       (.I0(gtwiz_reset_tx_any_sync),
        .I1(Q[2]),
        .O(gttxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_tx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_tx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__1
       (.I0(gtwiz_reset_tx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(gtwiz_reset_tx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_any),
        .Q(gtwiz_reset_tx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hDD55DD5588008C00)) 
    txuserrdy_out_i_1
       (.I0(txuserrdy_out_i_2_n_0),
        .I1(txuserrdy_out_reg),
        .I2(Q[0]),
        .I3(gtwiz_reset_userclk_tx_active_sync),
        .I4(gtwiz_reset_tx_any_sync),
        .I5(\gen_gtwizard_gthe3.txuserrdy_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0110)) 
    txuserrdy_out_i_2
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_any_sync),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(txuserrdy_out_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15
   (in0,
    drpclk_in,
    gtwiz_reset_tx_datapath_in);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    drpclk_in,
    rst_in0);
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  input [0:0]drpclk_in;
  input rst_in0;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire rst_in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0),
        .Q(\gen_gtwizard_gthe3.txprogdivreset_int ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0),
        .Q(rst_in_sync3));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 149488)
`pragma protect data_block
25Dlemp7DJgFcCkiZuHhYdUNrKRvAp92knrBgF7/UGFwzjLNd8kHZZ/4az9Yjtyra2nK60Dd+zej
6KjhhthRy8rQGM54N5K1jvKbIPQ9U3b+XEIHEpsNOoYLnH+0/bokoS00kkV+FkPrSyqwgfRXlaaU
0TJLH+60oPjqlDY1JUQkcOgZ0JwkRu8QGHOi6/MXzqgMDL9yCC5hIVTGTBOHIwO6/+1ZZwMQhjxl
zE3v3S86QsCapIkNqfGDbv7zQvEihbRb1DtWGE3Sq+2rsS/Gdiu8vZdCgrbMG5L8miBkUsWDsAA1
HOXuqpwAkpuhpc6gW5wtA380vmme/9u8lWY8JWz93Wwc2VzFOW/0XFo2e+qdWnO02HGX7ZGkJAYe
QZt9fkG36IJFQGcsPqOzAIeKMSq+8EEWXLiEsIeBKGK3cLfuiGHSrM6IWhk32vFVNonn3EfUlJJN
HRCBQzg2/B2ZIgkl0PhJQdscDJDWCZMu6L7q5IrNbPXhulDjEXdYMBAP2lfX2FwgX4eIZTkdup1m
7IfBpaLHpjnyvTAmCA+LOxKju3bm5dxG7WCaQc0up/MqYVNUtQuXHcaif1uQEEDgHVzAuc9ZsmgE
FJUC7xasEN8atWU3AxT7icPWga8NDIIetFI1C7fgSGJffOUmb+T/EoSGVh7frlGIfFDUybr3FXK2
YQZmXRJYuaTwUduc6WhIj2G3cg4fQvacb0PYeoeW9VTai+3HA3kiO72OaUgr3QHG7YgoPs7vL5be
KYkamgyuoeImMcwmYTNEL859sDfxnQcYKUwIqR6u5J0XfizqvrxaA/IZ8zvzps4BLWbpQsDwFujA
hSvnqC5L6bZUot5M88Ws6Nn6UARX2/OgxcVPRDQ5xthOIvEFdg5jy5h4c5liXoTYpuFk6Pa4kPDP
8wC6tWQiCmthZXzDRR+4Pk5h+MpYnzB2HeWlaNwtf+6oDYX9xqLy97AoNYZQTZKgOTzqEONn3YCj
F4PD006N7qdMV3IozLvRl/L3ZwI0i+CY12IxBAvtu7f+x4/sV0TDZhwEREjNZo0+GGJ7YNZ12si/
xXU4q1H3IRBWaMdMn9UnuvmiVXErSTTwifIaVFZ/qojpg59JOPO0OxSSb1sCTzefrI4hSyusXZ8U
Z+x0h3itpouflFmFDiYY5yHiGBWmcGrYMHbfcKQ5H1yRAX1+zFU/BpGt7ZRTqjI8efD7doi4+7sW
RrHu/9/wFC/KGZsJH/m14OySgUhK7Stct1MdBeBFktbozd+y6OAq4yHXRUmhVTr2e7dsGoYDRzhX
VpApEGQ92FoiYTlNHHjJLoXsIMXC2GM8D1ZX7ZQVWEuUxk41oSkDfMRPH52qmCaYClnw3YGxUeE+
O+e3iLPYbT7k5uUp6wUDtwUTuYXvSmNlVGCaoA3eXvbYuOuqt3iVttahbP15QISLkEq4mNkUr1tB
/v1YOHQCtQu7UJCreg//oEvPIFQ63d8bMGaJxRnKkjboFfhD5BHp6S0jkPofAeByW31xAuJldRsA
r2x8rU9aNxecwW1u4cMqomWTsbObYo5UgZEvgGvyRObV+XdTFe9teZ/WYO/8tof+bLZ24gj5a5R9
8ooR0KUSxhUqwFNoXfTs/seBDw8jKizFJ5jJ+i/2RKI2FKkuMr2pkn83vVGs8zrrzj2F8c6hjol2
/FMDv9eb/YVClsvddMkXxV/5v7/GFoZykTP4YrtfSai4xy4I/osTEpCtKpQw4Z2OtGXlv083oJAA
xvs6jZPYPJOcyywdV5iL1trfG/CNv5bW22Ni4oDWaoL/FOeoTu+4vm/Ud8pBFfEwRCD5LiQwp86J
JsAPxU7rXQhPOwO2wbLBOM8cV4pTbu1y3og5ZZd1PIiSDpapGjmK74LjxZG6VAjjaffYacQdKPs+
Nk92hveZ5uiMhBSThfUA6ceB9JlFzkGNjNp10w13a+2vqFoTYkLZSxdxAp4ampKFHsGOlZK0PLu2
MsZTYbFg4mI2sNNj9Dpn9pYS1L+0rwJr1i+w9+aasEkjq4xJUsrv9a7KGDqnD4YYd0MGfknE1BWz
1Rx/vYLxMaKBWPW2IlsEsYUWcYTb7GcFfS64j/nPmjtu+vsJCJLAAiVBV1lJlsWLrAT6C7kWUwEt
d/mSRYaD2bOlA2f2HyV+M3zNicdfwoFQZ0NwEewFO8p1mUS1Hrz85bKPRuYdyFGiWVdVzLWWWVQL
EjvUihxyIEZ+P4GSh+VRyAl2mZ0ptuH8VgywIXu6447wYAuGh7HsT6BOWDFnMGdQJKJ1R3/5LNGM
trJYtkJd9dT5Z/lxu+tXyQjUECg4n/3AsZYrknXAakbQTYg+v8MWlv9SCqkz17e5gTK7dc/RKB1T
fO8O3Gllk34uAGGo1k34461ePgVQhn0IgQZfAh6yzM9oBPsDjZInNsW0Xb6FmB4SXMszISdZ1tx7
7KOlmpS4Zkm08MEe32Xn/VyX+353pAR/cje0ApSBv0sZALrgrRYWyL1Z41NF8f69gWlvY6LM1c1w
g1LMc6syjg5A+f9ZjBuEuGAYg/z6+i3c6p6bLmPCSZOuRQHcWxpFRRGAtWS3+7zI13vrB5Zmm3FI
wYU1ubljHTplwi8K0HKe//9QvhyBLoIFnb5jW1sdz4A3jT3BJzY/mUUmIAe1vd1/OY0xJA3PZ4hz
QHsqjJDucA8w9G95ZXZNCbei51alm+M1YltGhenIwEv0TT/Q3MCVd1Nata7EtFJzQYw1Jmj+Hgxe
lEHTMVHYLFUypkXll5gKw/BFGvbE++2zZeJwZEIOdInLq/cX3py/bXe87/RB0VzAEj7RUgkZGV4s
J862UMfb1q5kqzaBCcnWFdL1oL+00ISUbMrbNivhvShTVoOmcNqRCIuAsjlU0Ow2USYoeOUNAdVC
KQ4ZhKm8Qob0R9QFF/aBPCpmdp9VwcMTsUe6fHPYvmzKqO9EnaO7/DOOMzLMhxCaiQ38KO2iwx6O
vw4AIbbM7boDrLJjXd3SS5xYctLs6nFwLtygZb8ZmEvEscY3hL89npKQX8fEXkRkEUjVemqhvLQ0
4BvudQaU9tBtYZYUAMH7lk06/U7cO2Dh9x9sRnEoIAvuIMwvk7MhynDSwQ9/D+w4ggOHdwUfuS8G
UAjKnXGgWzwW2hEwC1ONAmjNdsnETH4zTC9O82XHLU8E9ODkT2hTssQ7U7OdcautH5SBPHkeSgUl
2m71Zqcc++IWUyP1QCEPW6cGQxHSiE7eIaJlusbr+AOQf2RsHjgMCmkzFOJMPWvf+vodlNy/CIjM
PENkQ6VmARZjipV72F0Nm6K2fUzWd+QTkjJU4/+w1ZSpxvt48YGZeM96CKnNb3z3rofoDk/2WBrb
iHjzp+xxUpxj+Ct7BGos0KjnINwZ1uULsbPcrqY3S/1zmr9lJCGfPZMYlYfiXRtK/DlmU70fNCBF
cXeKR3IHhpp3KdZCD0eZ/2Gm4DscqIdJFu9+XAz5bDWmKWKzPnCNAfKSPz9f9zLEfeiRo0rBP1UU
Bq97w8e/zZnfAUKE47m1L+WeHQzD2V3bGLNK70KJ0/bRCpBjvKZhjahD3IQwmjRrK8BzNMsKEmnA
QzRl+kAFZia4IeC8ACaqbf0nehyvQM/89s0NN5pTlcEKzkqtGC24hD2ytPk+K+orfEwTaN/MmtFT
W0f7JZZyRYaWGKZ9bwcbpIDDwzgDy6pBhbGRSe4vSpr2damrh9IGvwaz+Nch85gcDKdL5a6XaN1L
Tgq9fHZRxoJCv9yq9uiAFQCAUf+ITT0u0qFux2ZAs66Fn2ldmFE2wo1M8f3BQqWrIIEzq+jSao+I
+0GuFB0zIvFlCwWbAyZwZQLgmKm6gbXS4ODj+o/CDeoUqMg4/eKGXH8MiSb9fCD6aHhd4WUc4VXI
F5P6vizGdNONa0L/PNeP/4ZDB85odvTKTdmcqZ3Wo4oWcJebja8ZTycYivtLLuTcVvVXkG07klkn
12UcHh3FddrVnToy/cvqj4+fThnAQC6X+dPV19YJysDQvkfWVjBn1o5AwEve5z9x4ndBL7WygnX/
6pC+vb6dkHdNEa80FO+lVcmX9mhDBIRx6nBbzfne7vYJV8Hl+nGjiDBTrCuK9cgxDxcGC5y/E/EH
6/3aAFebvXLUcPIwavZfmbeMOcyeZLrjEjJC7UcLfYZmiQo9+azAclR4nU3a5kv3qPwQkwkxtpnX
+egReOrKPDe7nIz4yKyjKRnODFrr5af5gohKRb7H6Aq7GUyhuW/JMm+Rs3bOoZdpQzcuYcq6UtD2
+rs53q3GOoEuOeAjhsikIpLzLKDiodae285rSbe0j4lwYLtURIgorDe5p+9kZowzFRi/BJqXzz9k
5yaF4oSregW1KC1MGfGxOGHWtaiAyJTzulUk4Z2sXHYJw6WbTSglqMktQw314QUUGpotpl08AhCW
/9ZOe9ytUjitcfadS0K66+T2isQpfdl2I77d/KX+Fhq5JvON/jqWEifzRsw/nZ7UDo4ZUPFuOkwd
9gwR/R/0LyItx2R9Btvo79x4J/sB35h7qqvyAvJLVYEljBxkSMWxlUPQmHg7uOrEw2vrYi2VkiHT
gv+jd1PW7hSEyGlv2JtON7r6MCvNw6DsiZvZj4VY/BE2y9tSZREyQpTCSKH7N5cLdsvF54/p42X0
ZdNQgtWx5mlAs7STWRYanyaY5uSdSTi/oYYx+fL1VezmJaNOrqGQd0Horzb5+HPFPfIL0aLG3eF8
UvBzS/hG4vpaz19Gg3YY/G5mOeLbTTDnb7+wqi2PdmiW0nUCSdQfi60zN7st1Km6VIg7L4SFtUhr
3YRhJOOwBI/1fHd9sqohGKFfLRQs/vTM0Fu7V/eIZGvPT5NWui8oKc2Vu+ybeHg7rfRNwQ6sXhD6
JIFXHFjdSvuhc4HoptEw319CJWRKpUk6HDNtnCw1fzwa1QhnWA7PYsUvJxl8/WS0qBXo4YbQcjrZ
Vbw1ETRaipmNeXdzjX9ZkCMYhICdMheyLGr8EQqQDmtIDe94IyGUwYhvNtZ/rvIDcey0Ad5zWBpp
UFFuLBH0p20IO5nf4rnPkCpU+Of84URqdIjeJAIG5104SV9OCobIUFIJjs4GSe0I/28uYaHJkLmO
/gKdL6l2Xvphen+a4FpwNNGdrE3BVLYGd/sL43rF3EtMPodnWa06G+px/Zzo/QXulZxNrLLgX3Q4
YRFtbpP3RJGjCCI9H4LTnhDwUPcXMKGjj4LNKNu1A+LJttEZNwMNXItGVkIFJETE2pFKBZuKPjR+
/ALqKzwpv6UwbNX8u8uu8DTrZil49HT0kIXMdh9boQCH7L8KBerkr3PClqNOPCv72IPb/xT0hqai
6qSPbTgKmgWSc1m/3EdgiYh+wosehy5X6WUidhZZWQ46RFXmQEapQ4c0XEcDllkhItMHHUYz7NUp
yoa3Z6SRyzIZkMIdvya6V3HGdCCmtyIgvyosUWeLTcK5ZJd5bqjKVvvCYmFaSit0zDCqEZnivBIU
pySYi49cBq8hQqNQi4U4FZtlkemsHzkvxPrcq1br1Qwxmfqbn73x5pgH1QyiAX3W1zFHVbOWVJVQ
jGurpggJNeVGDeU65f6+Cvh3WwMg2GHfj5RgY0a/r3Yr3TlUT+B21d8zxwjSGc9iow/T47imJJDM
BpfBjnOrn2uyi5G3e4KWOOHUrSVcHFuAg43bG3ADWoqIUGK5PNYFn5LbLwHIfcf/7dNvLD7yjwEW
V5F+DdhV55CCPi4Av+vbtQZb86v6R5LvvQd/pXean3bCJ2ELWHpeFzQeKiSrWiegdELomEdUAxxA
LSeGARtLDBmYPm9HRlsyFkZzHbY3g4zH6QdJXRJlMRDCTMozeLspEcWjd8TlV5TWhPrXjKKkNBKh
JTm5PgkKDcjKG6jGEIvEpUXn3QzgaxWEgkb7YBo7lDolmh7jRU4n3+bm7eTJv3/IkC5bBt8dW5Lm
bnDaURfJKjF5beee/up+cus8JFYZM6Fd7qH4ULr97udCYiwGwD7M8mMNHZsM0rgenKFl8KiyUP7D
be3mqLjLYm6LAQ5lkUnNLT/s2/DuVqnZKPTdifCgvclFYXfArzz6CM9pX5R2Sm+7focK7jNSR+hI
vf7L5xWqge3zf5uQ1XwFbctZGDZXpNcOPkzNJHuUV6cqFA/+PuCG28wwnlYnaNDzz8Q3Ligd6KPm
e4fz6W4xzD4qwT8zPWv5d0W/Bjsg+ThW5tTUIa3s1ZOnan2udZBXoPPO1Bv3iTfad3OG0VfuKyOj
4yKFe5zamHqFLhHT29EMX9zHsZ5AtZ2SBtan93Jy3N+uGQ4d/9939rDxSM01S3VEKcN0vQLVStEx
0IO54CZ05evHg64+Uj/jWYrgbSqs/OA22sF8MqxDVAmgDCmXtBdhAl7rf7FpxuiE0+/KJYCi8+sI
A2TNYTJxxvuJpNxQ2HGcnLs2uNoyH6AzI/VqtyMr+bzGgYf58eT7oDe2z5Jw3bcAiVhhBlRxAEtm
FqJwX+rdOUBVP8tfikPX6Q37cb6rZN1yOvVoYRw79v3QMv/jDdzojP9pn1lTE/hnASq/PDdi081x
GqKQizghOQ7KBqphN4GrtxqSXcQ2c9M0qOA+JignI52uZ0tW3VPSRv8YFkKw/4WUnnidZCd5ecPt
QgisZev9TD3WZSJnoXmkY6AkfAwAS4y55yjo7jBaVnkPo771LmG+vjlPSUsYLb6WXCbKw3Ltf6AE
lrPs3kfhIRoe2paCFk1FZ48HTMCREglN56cetmzckUnNnn9dEIaOg6eYlLoz4Dq7jV0cCka8PjcQ
IfIOA5fDSqLTejrrVPmzi2hhjPvdk71rTbL5PQNoIpm6nWcAeeNbjBVekMk5reFV33Hy51wAKUU7
I6Xu+cQYHQ9DXYK195Mrj5LdfApdUIhI6VY4Mr4clVWhqRECaLrFRO9ESviXhkEd7aETBeqXyiM2
bYDToe9ybsudw4TuryHGwhZs6WnhF34jHxdkiBHb6WYJbQ+Jt0QAdaZL7AenHVhcE/x8RHVIB5kj
Lx1w0OC4vBXyocmAYfTBKb9hCfaHQeJRefCEuhbmsrGgGWSTazEuarDv6ANCLXvonrqva9E7nhxs
xd68a8DWz+pZLpcd4G/ZMLX25vong+CoKVtKKrJQ735Dr6lHBuIBkAHT0c3l0E6zxiYZVLQfp71/
o2e31jLp8gbJOuWDYMqQ3DiKRIAA/Uy8FIQSbfQ4E2qLGW99QE4sJ7sFUDfwkLYPTeekVHyTm9M1
w3ae8DnOLULlBJ5H71S11E/TvCzV3ZZXf+IK1I8z5vZ9syA676ukF6FQq3HW6Dm8yvPbiU8xbs/T
y+aMHrqHsSUnM07pf5vKqJ77PBNDQ4bm54gQBteMOxYu6JUwpjC72udMfo3ivD8TFwKrVzCmdRof
N1VX1B4sO9rDTzI3InsT6cokdWxSPqRE/BpkunWt0ClACb9Q/5XqqSB1E3Izrjhe7qVqQZPCfYh6
YJ7YyGRP6FPug5fXVXYeYA+QImUBR5jgNmz2RVLspaPPGULni0Vr9BM4hj8/i/8MuR1pllOxzGRI
nPeLe4r0rHi/2dqNAkyszUcHtu+euDX0bhq9I9F4sqToxyiKmEL+7Iea51xFXGdkr8rBWLD+5uyW
e34NEACB4la4CA9NkCe5ODNlveGiMmh8y0FESxSWk2iW7h7vFvI5qkONfQjPiPe7Jd2H6iXgJmhu
Ve2q4fIFB3h8Pn7J0TqRNooLuZ/PAMJhC8wdKE48m3iPacdtYyD2bjaQEX1uEbv60TH8qVbm3OvL
m8u2/jz9Y8XyKYNEsi/JYNveY+7pDdj4sCVUYNV7ec2C0F2e/BlArio0mkVh2IzDIWa4cOStQ5O/
jhNSEuX0JmhI0MKNp7XxD6GMm4ZXmDd0s7MkMMjVS6by8QMTZZUJ3dtiAFoDYZoWsng63Vwku2kt
1ESJ6SkV8CUGe53oAYFJeJIL2nKbrwsovhun7Y5YVtI6c1hCrlIHLOl8LQh/lWFMpdvDNFB9kcKF
8kX+X3YkGlfdDosJ0ogpRvTI71OVpixVcMHBre2nyGVQcpZICGieOTImuM4dOK4FzmGTcyQTImGR
JP0V9++hmeNXDXwxsgSMLwkhXvNakRjsW8h1RbRvRU03IGlSLCM65qjQkHp5NgBdWTW9vA/mpfcM
ai9duMrXJZbsUgdGcJ0kXG2+8mPWJ2IISayReAFlmVscKnFRig8kBBILrGpRYwdhx+dXOn2qY5JJ
7EPDFoQ5DjlaKR1waYgB1P3UHPbHJhd2N/LPeIhxcQUbxV0RGHGh9MgXrUeoHl7DIVQGgN0U7hrT
iET4bLk9hO9gxsA531/BnB/tPp2+ugIQMp6oiy42SOSMMly0EeO54ws8wOv670KuGMzx+lElfhgw
q9jYDMiMlV2FWE3tFmyfJD35n/1n4zfPdb8mpv4sJ+vf8HFhl/lnN6DLdtrWcLA3TVbSxvL8Xs2/
c4qVBFHU6fcF6ukQHSbW+nibMIU1YZ+5L3gZrASN3klhES3e4+dmMETxBnzW3JuQnHnk2d1da2MB
UUVw1/HMOBgcIeng3JUKw0Wz35EwXbNlz0xnZtw0HT8liwEc1UHq1gtxsETnzQE7vRbHfipphmgo
42yuZmBJOXA0tafc4Tacw7ynbyqy6YVaE2yKRc2MGpWARXetiQUgmlt02oHupd5SQxY6qKqXEfD+
W21xEgmh0J39hVk5NsN57pkDIApU/SKLu7lMtlGsoDJsnJScxtiVs9M6s45Tq4UKfif6QRUG/eP4
KTWdfk2ETtbcvRPo4Gr06TzFZ0Wa5x7htGIUJ16onjJ2wja5+TpM1HrCxPDtCStNTomtNV87BmGv
NWw9JsURuT2wrYcP7wDd8ODoZxKlyXQngADdV+VBLSOlnKd1rnhgY5reU6Z0TrNvHfVWJ7IaTeWg
dZlar+kpqHlCoe5Fr+uLbI4gX4Ht0qz6VdGLYngZ5vnruXu9ZkP0KmuX9ReLKiJKeuVlZbTkQn8S
4nVMXCD2C07kUZDd7NkLrxinuM1gQGVD2X7tCjOA/CtnvrD1Xt2i0DQHXnHpapSRxBYA/uqbsHg6
m8D0MiLBqFYO0CmayuPfLB8unMmwhVQrWzS2y1v/dqeg7ynYw/6cxgHGHthy8IoSaWvg1BdmyRE5
Yh6vdY1efH1D471c6OdAHQBDKv0U0VZH0FO/Jd904kFs+9kvM5pnucvPtWf+y54VcL9Dn+VoZuby
q8bm9loZObHcJBTEr1uyLuWnSsRz+PSGIyGq8NifjbHKb6bnY8CMW5FT8xIz5BtMssPVNIWAoV5/
GkyYY2Mca5HSWhEgVGEzFnaAIIoPI+DQsfxX5pmq/tudra/WOKIYjx39kvszzvuJZ8XYRTVfJuan
3f1qWJsv2u9JJWKV6ymzVRf4Pr1UO5Sz6v4ipJ/erOi7FaJDTiCmMEotEKnFoPUDq7jejIjVHu97
ADrgkcaaAKP0tR7Ws3P1v8aT939O8TeR5eh8W6J65t0DHqGRfLya00PiGMcWLYK9H9V887F4R2Js
tq26fcfyV3rvjR//sGF3wXxjk78unBOYt8UIWll2+atheL+BbRAVuM0i1og5jv606ujo88/Dtank
PZcrMjmO3fYjAhufYhwk+zD+emfBARo6F93bqbRNFhJd1eqwkZWH4K74EfjgJbEYVX1WlYtUqnaW
GqUWBneVUdqXXgTMrKUY3vZqHGdjx5KiFNY04VEYhLsP1cWGtiZXcVNrHJtUenpNFbUX0WSxnFUh
/Hi8sFD+aJjkbuhI6Sh0fYZuoK8EdGO09eq2ckuPjRQbRhXJpFnqkEjloh3WsvO2JedmHQJy4ANK
pVeJmF6c5qjzST9anES/nb0H+XoG3YfFC9dFnBX4+sJwa8vey/IEY3E5hQSheO7+JD0iRSRDEv5/
CkTguIClcAvrOxVnouEREGAdQrbTSFXcGnD+6OhO9W81rKTA/ef5qMjIvPtf6CMvWYCtz0EIVchr
kWB3SuKWG0iZxO2P6SvVFZ2XDdCOq9rugQxFxW1Xb2w6pz0jpHwNOo8qlgD4NRtOgCh+OY8N0t2+
YRsuloQuUsEVzTtzqusSAN/HuCFwehc7zn1MxRIsJRnKJATd45NiL2ty7y4rOLgLPq/CDIkQZTMF
xxE2DHyypqW5IYBBqHEGlqdO9urfFSK7Gmu27fCORzFrBcsLM539FafTGAuEpJogJHk2juBALcGp
8+dP82cHclfZ5iBV3wDr2TGo0HO2lLSRomlwcscgk6oVOWelzS+Jjoksz0N4aKJ4+9EnkL45sI8J
ld1n/UI90qCj9KqeSS4zmg0V4FhufqFUiF9u4O5bZfJn9Ev3LPdYG2wyOQ7lbmpP679TGDBPvaPr
tafihcGtG9S9YFBlPvLHdzeo4Gyvekm8ZeppgZMKs4icyktZGUquDlB3Kt9IYSOh0+vvnA+m3eKG
STHd1OUE/ecOhWBGqU4ptutf9g8thgFff0OephSSaeGHJa/7pe5zswR+TVJMLsTDTAhVpRS7Suc2
RRSedfc3KLU2o9XkGjocLOTIQwVV16n6tazYFlGpMdtKePiTx8Jt3c67W2N+M+6OvsyYq2LJGShm
ku/VExBbw8qRjkGda8K1BTWSInPrPIJsvwiMPssAK0KtY/pIxJNuSGcN5bUYJaJ+T7RWeWUqu4vE
05Euuf54/jiI+LBETOBuN10IPJfPZO6Hlha7aob/AWAlF/m1+ITiK3A5vnEWUFLyrvUPfFnGQeBi
BMQ2qOqvFKPtXi1z+zWBRJSLSMl3VDxKepLMHJ1LP+p9U4o77KPYhzUDYb/SRwjLC8DwtkFJsbzj
80Fuzg9Wo8aSNBJOXiYrdKHKwyzwtrmCh6IdIu4OKeTkzWfAl7LB7lRMlKdpaI45nT9+3RU9OqmA
eyWugPxbopQx0mYfQ5H/U9YMm67z3bYIoVYh0awS5PdwgryxJZb2YTYhgyQyu0lsd8J6lu9yILMi
Ry9Mr+YW92Ls5IpdO5h48L2T0xIxKvCuEJkEXBxhfoLu8RM3kBwrATyFYPwtP8H2T241Lz6QiI46
MG0p+vU/LBYDXuPOS092cHUvO+SpNhPYxN4vPJJQ1ISQx8A8lQjlZZxD49l9692PH4TKfsY0aiNl
mCzR3ZmSSEtGXrb+zGp5AdIt/7Cm+G/74qAhF3nTM3OENdid3KMr9LsO4fSn/rmR6F0KwcG6JA3u
bTaAKV9sCWnWeBRoUoOUUVGGBGdvSSsJrauHtaeV4/I/5ukPwoTpfd2V9jga05GaYuNLl5sBXcrL
1O8SichK1OnDMUxYc0OHPJyihN0n9zPyZ39pybQGLma/LXc6NVOlFN/WJ494nKZtajAbdQHZVFbh
FGvY6NE2/O0mMcTRN40FT8pcg7qyxrloSpEiMfy1c3rn6A5cvTBwQ+iHbVtEijhfcH7ybnxJoHKf
AvDgG3qimfQOTuzp6sCzxMIC+I2Q+V9rn2+kQcXdn3vi3ThIFLJuyE7h7taliGHtGS7sY0smJ+XC
XKGgb+gZqQLGnM3M5y18bQwBJ2Ed5ZfJ+y8T05eRDYuKtnZUFo3kxFN0ddlKPRuIs7DpYarJoQkG
sDi/Kxm6N6h7wlwNdMZnqNkWWpgyf0qd5LYh0oDxaGyvEocjfUhRlFTH8aEEdBc4iYpoiM4sd1G3
bWw96M+Am0J3/e0NHVnusY3Ky8nstO31ebh5vtc3p55uA6vZVrUVF/nVpc0/ksilGoiNAtliz0cf
9/eiQ4k7nVfjAn7ttjG845abjnTVT3bmC7ushn3Z5uX97AE8IKxkyTUr8KdPR63BJHoo4rbLH4W0
oCHQa4Ty1aicpG6zIBLmBMN7uSlY5E51XsXSe053Ta0FJj3B0yEoE0DaXVP2niqr5Ct6Bg6tTEcz
e+NNOTFQjwnlXspGz7Giwnr+xw9WmaXwUcUzAUyxDROxkMk9gWzkrAlkjffe7w6jHzOFloET7MDb
1W75vEn8gUhLiy139hpgkbTsOMZjFnI58KyraiJjmXCH3UNjuGy3sGMXyQb+mu396T6pu4RGkSq2
+KRaUia5qUkT+KQCm+4HNEEq78fS0/QlrplUsKB9TvkjLVbT3lhLm8rSU1iZwp1I+k+BKbUN1G9t
/RIavtf8Exf9K0b52Hv+gqav7MBCOlms6HQzGGhE5qR9uuZLMy2lumy/WKMjnO3YEvptu0XD8m6e
kD9+Isu1azkdwqbKUAjJIIhODbuxtm285C7SMbGVJsUqP/6YX+RnJe4Yq9wz5LZL4V0zOZkvvGeI
MzUyVuDAa9yboZi58VgPb1nZQiqmfXD+HdI8h/wRUHeziTKbvTzKDThAjo2kKoSuS+9tfR48SSRW
Fl5+Vr4u1wmllPAO0tF0dQ5D1Ntm6BdiRfV8TdRQ4DVnVlxaTTKaRQX//2YMns8abwCt9AHPZWR5
Us3w6hTr/ofUEVG+XTl/eMCK7VCCeVTM8LNwFW2SaYbrYl/tf6jzs11X83GHPbeRaqkFR/tpeW7J
tAZZBBzb0LtcQg3uiBfDQsJb9TSWYsMU8i1/sbbuCnSmfx+5dJHrgP4u/XivqhvyOuMQMoA2NHea
3iYvKxbIVM3Nq7N3p1p/4/mArnGpjHYWcALvdSHf0w+JDNoLErgncxv8y/r1tqlslwHzIMRpzwyV
z13rmKiH803ja1JOvfnnDRtM0ALdmEj/3ZlaZmH4SFjkND+N7M2F8UOdJtOOMrJs5eDN/HSi+mjc
75pDZeuUEc68RCvGqX1coBAnJTijy9EpPe80rMveXOzFFVs7F2aoj4x3NusU39h8zVcQKCuNAcds
TXTaZvMjmDr7MQo1HamJZOW22/v9fBVVsg61RwyWOy0AC5/jguuWmATEDp8cTaM8bDzPhmP4HO+q
4mFEh9NJvH6mrXlkKzlW+kmAQRFsldx53vGa0LYg82wT1Mv3MlkLYtl3JtMVb/04pkoNdO2HuzvS
uZZsUrp7cibADwITKmg1RYChAY+mfnUVatIgUt0TbA7bHk7F/ciMk8kHbkIYemEslP6SXGWu0sym
JD0rw0J5AYckdenrMsMCptU6xmHi4YrxajTRROOPEMOWBk8LXA8Gnzg0+Msj/hdo/wjXHFui3VhD
I7UfMCh5a093S9FadRHfTf4Kmp6b7T7nZSOiB1UhWlQxtz+16XuNi9qO61SDq9vxFtmJPb8CGufe
dRKLdPmXu4g+Hz/atgROKZ/hyg4+Dm+fsNR24bSyg3FdH6JEMUuSeB4a4I2B0OzRCMy1IftZhJWq
kaFG+MRfZ/yqEP0fmQtY9uU5IZB/WWEb/ix1BhAWoagxu0NxaqMDeC0HNYzojQJLYMdOjSTYwrez
xlojsEBdoQ8QAIEjLv2ovjObPdxvGdibq9KN+hAtUwmt06HEHgVf8rdLL6nPHYyaMD6ssHxhZzvM
3/cKYREFX/NEcWOef5j7BZ9tDgKUIEIleqegRmgK1rxac5h2LCGimaJCg6EIFv260A+MaRiz/bul
OQgpAkJ/NwXeV8fyodn63cYxXGJd2DGfpiNzqHkGmQ7auNlceqIbEGgRkbsgkxneBouG3VmrrHp+
vXBXR0b1W036Dq9J7T7ZUevKTVeQ8dF7yvFvu/krhLzoiZIVtbj8zkyUKL84mAHLBiFozxfFiOae
BTtJZ7piTRAkIhCv1JpewLEKGyc9Hr0VgS7cKv19lGo6gUA1mmmoZRd33ZNLSlY2cFqmXcFss5W2
l2rRXNpokes3pmiD4qSGq7uZ8xGxbHrIXMU/uAxZh//UGXHpl0kxjdaSkgJPsOm+A9gEmuLEKEBt
alseYUWvpcmPU0IyNp79TPf8/pPosO5WeHIZF/JtuOPI2+9/l3oh0kzNN3Z6xXT02bgnEh1IKc5V
rfVwKwO84ddEneTeNXlfnlOhUxpglc9+lDZkzizkCqYpL8dt1lxZoolT6nx29k10ea/HbH6kIG8k
kiijuLnV6tkPnP7dQfhsLpS7TVvDxRk9kvSd7/sRRxphL2n4ord/gIDMDEvaDJ0QwqK/YhwOuFGo
zMp9TQLT0xDEPgCoaQ71ScJS0+sBBtCy37orULNmQOqePYoEhyHCSNaHdnkRUniiqPVv3+NJaZPd
dBfbmlBw18PmPxqD4bJVTyn9XASEupHdPujTzLWUEGrF7H2ydU2GjJb61VMPDv+az96rwQdIuce+
Mw+1UqATHrmr6NOdRGsivugXs20lVD17LrtwRv58YnKqywRVpmnx4zMdJiv2XSHxNfO6oTv34Vol
WnkzDkq6qoTJrT+nh+2qxlk9KbQRu+Ea1KB2p1SkWXx13lZWVZIF7seVwUaPFHVeflyfUVLu/Vdn
uymJMAg14pj/5UviluV4RSwMKwGCipw7+US9+imBKZg7A8ulQL2PLAV+JOgzcmSvNSeoffrd4tUZ
QvzJg2cpJtoZQKow4L20Mz4ra8CFHcFC9ZU52M/gdCPAeltlhE31IboJygq34Q5FMqIKICrWSgHj
aVU+Rxo65wwcojlHvh0mMMW9Hwfosud6ui+DiOixgid6SCm+QeYN6/jjEXzOuum7qNCoLw2MLJyK
jMOHnQ7Vx7+GMjWF+dkNyKSbGi9311EGDLR2Gs8/MFWJizRnlSGCidVaDKzrwuWSnio8t0CxsDye
DPco4XEQklx2x54aqRImvmByBNSyo0nj/0znFDDS3VsfAOVqRK8hdqucFjpdE6H+m+VHxg+KhV6k
5jWxqVV34x91r7vuIPXW5WdZgO8KyC2j8cineWbyqtDSrTvRCFpOdAvCkBaaF15iVRzT3qgCHjrx
R4CgIe/i6KQRettr8AS5RYWhcnDQZ2T3de1/u2hZ7zTZXF7LQ8wWCOdfvKGj+/uOTiT9Oll10amS
sqHzFW4w14okkJco9SUgRnQJwZva72RWt+v2NtPlMSqqNz2syR3vZOrv5/lRVGSvHHH9o8pyOS/P
aNS8NUazDElGzUCUjsJoxWBjsfBkhlm8jbTzIfAkAarjiV7YcDMFO5QonGEWer+/k09dletYAtzE
vPkZ6mRyMTeEOB4aR5NO8ewklwp7QpoxtktB4IL1USEtfG4AIZT+oQQyPblbN1gMSOWWKAUoqmWI
WRjmxdzEhBc9JFUX9/IdKGeN97XO8VaXlJzcOMp6BogSTd+YnaVhOofZbzY4ClP2KXLlV338m/J6
YtWobMYVweOS2xWCM2Tvn2ccV/64j91k11ETtq+LKvELGHoxrkZ9MHOk/MyzXp7ZWJL/Ej6XkRKp
o/4prTSIoq5PLnSNxBD6/4a6Qp+oIQTHBvp4PQbf4txcZrGzmp8SSGk2MHz7JRsb2nHdg/xL/0NV
zdkmzJlf6+cn2hAR76k22XqSTxYSrOYvbFn0E7EDonMvEMTNaAlAlLBrLdWpyfEdUxr+4pShYRXP
PDSuxPD4zILKpsuc5Uk1d3zpoHORx2muYHbvQcJGax/qwf3/mj5eaCWuH7Hx+DwnXK9iEP/TsLjF
AdF6cJWpyp25rcRLrUX1/LCv2Z4W15wZ5JRP8pZ74SMTU/EdvK9UP0/fNEMSm7hCt1I+TSaKOgxL
hJ6TkWU4wltM6npPlzLEZhClSC+Ffy5kqvCiwicmTQmyre+UYXbeib4bwhs+X0sxz4oXDIEZux86
zjE8si8tBkwK5UoPPfraqAQS395DElf+S5BKo6AVkYpuV3TdDcFpEDfCqTJcvkOe+OMI/Es7CCNB
MdC39ba21dnu454FOFjsv/zQ5HGALlKMcNCZ+hHrY2KeHXWQ2wmI8La0/7QpIgLVIGraNNXfiNHY
E5zRInOn9dGNp5vz1VDaA9Z9ONt0qORa66V1WceffAUXuBhr2pxqjn1cM+HSw25k6lFksqWlu1Be
+m6INXzicjZIlkOqV/tWYHkKhACiR+wPOD6nemEGVz81f7UQ6B9aoZkurZr80HKRnpe2Fvtbhakf
PluQsseMdcYvN87uAio+o/MPDst+vaTs90/1KjO0m8BF9g3ztm8AKxOq1/zoEcsDFVH+xgY+UUUK
w5QtvP8PsBASQ+Wktu7YlVq83kD1z+gYG3Axdn0ndNszuAyxuNt0RKWA4GIXzWymaeLq35hZ3g5z
19osNxMEnYTfLrtt9wjLCTM90EDImR04FaOgGogQYpu53P5orFWxZXCZ8ZEaXifvNFdHmtcmd/if
mFBLDqcVO8HfJQy2o+HKHrPkZMbCyUgPT3Ir9BDxp7dzVydRCoYSD3LkvTbSqIiyEdKVhDokGKja
ERAkbOzNfLDliNrNcNpDBKcC/eKWEbE+V2kqMTqfbHImwPKcECW8QFE1eR+WntoF8kBjxapWFpsj
k+/WwIC1ERGkhdzdByhRJuzaW+lvk5EnfUIzET9afFeM5/XvB8SUuYQjaN47JOqSulf22oRG28AD
WDdXpCXo7nsR2ec8ZZvkNMChYLBy0UzV5CjoJgggYTP/4kw3Eqez9vc1Zzbr6vHc9cixzOsZANcH
yhe12bYCOqN8awyRpSm4Yr2mgW+XWdaER3Qk95LSK02IIUNznvk9/TPJm85ECJ2qhy/upMw5SOJd
wyGTshpjEgQPEFibrfkbhLab04i5jQXPl28AhjUOlHnvWyvnUES7ocYdD5t2GRaOG3u6OHuLav9t
rwYhVrh2fosfugxLXbU5e9J+Mq5+YeQOWgagm7xRz2kPX9XJnFFEy06GveKAINyHaVFayHoBnYsd
YrG43I6/QWbQZDqoS5ipWoZN9n2+hlVaeS8ZsZOtoI4rTbLSVIMAFpKBIrPfwem6wbHbTVSZXooJ
eUnv/diyKEQntjRq6JO0sliYjlvK1lxoVMmqECgX/cdhWx1ucnEiIDJh5elZ4RpicJqlfY83FAOB
rfO/6PtmFCa0xktEXFkzyxmT0r5t/TsoA9OCbglSkur8FjcgT0wy/V6FIUOBBCegdjRPoRq4f8Pc
tXXVDKvOdeIgbS69GoLb4dBGCaNMx7yo3TCDvYYrM37ONHpcvRSj0h5DgE9FnbLLTir48UppDG6T
RJM2/XnEZXusZalEi/VGG2PzU8IoJzwhNJ2TxkzlmDq6a1BkUrxcIL3mmFr6RRY0LDoRU/SzrDRS
g0WFdADGGgmc+0VNAQbyIq8v/24/i9ATNIGMZrxWVNAHM27EZDO2xwGhPP37ujDgWi0zhkzFf+dV
jsgBeme6Olz+hwctU0v1LAgI59yDKICc63t/FbG31aMlCr1gskMs6z8JcwlWp1miKHJYwtozhyXJ
UDrNaqFhfLxu0tznafdYMG9l4i/vruy4RA86o8WrPp6V27jFKwmZmU26LiphljJKj8c6wf9Ndmjj
UaZsVLF3oia1nNtgKW3WSGI6ec1whJxjhRvbHo5VxLE10NgVkAPVK/aMvQehUAQxZfeGWVC47eFj
1x/wHoOG7RoRfzGJ/EQzNwxo6b5/DaXaIMaWUvENSs3uCyqZywh6owcpKXghr9r3gAJrf7wxFGgq
F3XZ908ujxjacn3mZuJPHzytkROkAIymuTB7uRWfZChXxkSJrRerCRpp/GLlWvi5r4LWoz32EpCT
NMwlspgF4s+pYGrMK/pWpY3VoeL20leJ8nIMjP6vaGmM5Kv0J1nzKq7GMIQN/kqKW9jqr5ECfFs6
r7Lw12wjM0X6kMRjWu3wBBAunONf7DA97dgT7qRgokzt0jhdD3WsW9IPis3zV9DHBv5tDQs2AwEQ
eWd3rJvs1qGKAjs9c6jnk4NWq7DVyn4zNjd8RZIDDTGTA2Bg5tpZdmzwR/ZucEntO5yvaRUTjoRJ
AGr6q+QUzF5Nc0a2JypSpG1NKoeC+JEQn5eW/d78HHCgGoBz6X8B9EAhFNHL57Ij9/i7DttMd11R
5jeOKxXCOHG0PJtaAvQwGGPqZ3W7DSRdkwnhFj1VPDpSBdLBaPQhrZ/zljem2KpN4qYZvCwtH0HO
rXD5V4oGdCWLkPBF2V4r0bQF3ViPpcn0Sxl76OV72vvuXg7lVreaVnNOpCz+TafnGQ5Du2jkPQiJ
1QwyNG28z9JODn6u0kwPDwk6JXAJJrEOtxADuonoO8HloGj+4nTSwCHJ9iK/823xQfLBJ/0eAcvT
4SV1baE2ZCcmsCPU50pr70V1fwEZXun+8sbS+vDYqPn0Kgjxu1S2zMDHwhVsVaM/XdDfdFxiInFG
K7R+EPS3UoT7vmgNCiOcN1An0ajwcLXElYvlZFgxnjwxJgGKoH10xvuuNtB7l9Ap+wX5M7hVPkdz
lJFw684V4BDHP0GRaQK21Er6h5yzt1aMWwnkGN51d+fKh5Jti450rnkiWeTTzGfFrkeUjbRhRzwV
7CA+nEFUhIHrYFcOXI5hcsqGvT2b1w/5tdS+iXYsNwcgM+5ZWFCADntOYJ082v6xviEC+JG2hPOg
PhXeUO1E09dp3Vmh/8WWXnNke+GyzkEk3/T2uITn9m/R4wdekr4VgW+mzhrzh707gkDjNZXEzW6h
J3IU8nlsi0oIpwz3I9pOsYYt2cWLaeZXJV846+KqNek4qNh2aRZKElCot7kCjL/VmKx8gp8i5eFh
iR5Ltoby3N/D/oUOV8bsIUQHG5y7btk5bf20it8CURn5MWIhrSke2KCz0tkBltCZsyYq4LOg2MEt
UQDF8hA3vSEhxQIXm+rplCUnDLjfeD31YRiGU3jJl/Sv4fBh2pacstlWPPoPZrWVEFPdLc1K7fDf
WWh1sMU1wctUgdjcvucHrG3aceA4m/0G6H9BW5tu5cpqKVEbanJqKG9Vna9WJqEPuOm2GxnNNOT4
574mT6C9Yrl8eCmOsT80aLJr0xWaA6adsu96z1Kw2gxDNmaoTTN5bb24YEe1X9G6dhT/XYVKvISp
e5zMA7At3Eml8xRYy1hNRcqvbQPV1iEkR2Rz9S9tLsdjpdkBhkr0uOHcaswjBM7orBqjJtsHvILO
iEVvVdTfrkrWCaMY7pDkCmsOcrnMb7w6frRoJLDSa3VdsomWzV8ukyBlyV+WLtTOCMQTAXnlRD0o
IkoH58cs9sRcsCXSTYwvIJxAFNJNGDHolYT+t4agbmMLKHELhLiZ0Xu3PGwZMqdgHDwtige9OwKS
NWy8i0bGyUXu7Z/kMnFbavYIEeIcbTsDPwH8FUnVfc//kxcXx/AHx+6XtN2WBWZ1bGoi/5NTS3Ck
TlwRS5pVwitQP7IytjjVVQGro3oD29YyFYLMgKXD+XFEkjRgAxUcFMtYF1tKrELDYz+kSzzvf4eT
VTGFAfUjW+7AfsywKtLTHFtC2Y8Hq39xxoXL6ybh42E2qstYPVU7c5lKWYyHR5Q9q64psNPveEE4
2tHO9hf7srgUilET4e4X9ql2t3p6wEujuSRQ7UkWES25+47+haQS4WBN4zarrHnVLQ0q5hmsKuFn
kA8I3w+VpJmfsn/5iPRCfWhl6FNNni39acblZnZ+5nkOSdFZl5R4ew3yhr+ALgtJUva4ozhJ1iIT
D+W2Di+DThACz2+9AwgAKpB/dz3Lio0XitP38uubpeRdD1QMvkumT1AKlvgRPv7sXLOSSoIHHZsa
3RRNQ1ONLTNm3Z/LtneHRSVgXvDAvwctI631yMERNbqcoHSKCh5bIjxwng0HvA3IfdRd9Fg3gAW2
a+wSBpHI/e6QC+427uTOcYMfQUXjGWpT3C+4cAxgyJ9G90xJsdvHbrBiqqRBC3InOlwZSGQfstnO
u4DacLdgb8l7FicBSZmy4wPdOGU3al2zjY520C36wHE910aoewgTbIFm51K780KmfAGhzO7v9tLa
Y6YbYksIP2jYd5o7kXZRYJF+nz3Tof3ZauwsI/UXZSeZJZCJw8PYwxZ0XWnFAkwQbYFK8kPnqpU5
xkm7L0McNZD7J9+nAKSL92HxSnlpxUcwvlZhTA9IluWJoUEw9pRBf8BK8whySj9yLLVYVX0UdeYm
KvrlXmAk6WwKNqGw+TAjGGXssqlZmzpEKNWmqx6jJOLel1c7QcS17rQPxx1szN8D8SOWLhFl3rqO
HmuPma+LQIb8pJ2zls47Bp1szWSgwo8UnYG8+O52gvromF6ai6ta6B9LPIC5zQkEzxp+GnLatBsK
46DG9kb2lu2V+4I68DqLLxp1EzmZonhNyThNopxRIgTyzG25Q/ThQ9JjkTeuEEwqaVeEJyBLku8p
0RYduKThbsNpnXGgCp6N+iMsGUDL5g2AXooMOtxjmZu6EDKZtLptRXdZVn6JRvWCWi7MjdmWaIiR
1/45r5ZTxQi7vb2liH+vjoe8zc/ywstcioHPMRN1GlCGYuogJRYn1+VS1rZR5hugblN4MvC6Oyhb
pNcUnL1kemomzT0A04f80I3RQEvl4Gti1rATSRJlLAKREWIx6Ure3Ne5m9mlijD2NcNC97lToCEf
pF7I+QaqrUPMzPLz49aXKwb22MmafdwFL+PO1MA27I2np+VWWwTAXaJ4eoe59B3Z0SlhLTW3L7v2
wURvZT0ECyEWSzxWxwMWwFfcA91BUAQHYNMRXzU8Z66FcTCmAdKctxms3IHlV9Pk2QWlHS+uOUf0
ps7kouQgmJNXJZENN0QpO/8qiKwBN+8bP38U2dstt2DP1jv1hbuQwccITiKIoJTV/j6VlC4+tV0R
0WXkbvhLuOr6u3sxRCJnl+Ykng8ZCdBOBBuIKQExjVWLRDVf+TLw9Co/88Hej1B8P+6WFCps36CG
QQqbGQmwtk+Q/eVCWn/kC77q3aKZBM08a3yc8rlEB0OgxXwzuijizDQsiG5j6PZD0naVND0Re+6M
Y2o8Xph10oP4UVvUp4F/GefwvgKbVesdMweVzbR7rpd5XWeU4Z2cRO5J9pniQUnyNl0wOsvc6C5q
/4aKxkiMlSBiR9aLPAGeMbMTxR9TrOGQrukNqMbk+sU/q/Trm8Vq/0Rdxcdmak687bqPxVDQHFHF
Tmif/x3a4naU4uf+/vKzCgV54E04TPVOZ70JvbJeiS71brHQShcRpp6NoUtN7Xp+7LACp/x0nB9U
wTI+TbnjAV0PQWYm4lDjhVsgiva5/ADI8pzDqpBrTQGh6oOQxidnCpmN4GlISzMLtGxCqTYXgc1a
CpbFkmdYkTzpg4+ZEXuSK80HguEHPQstDaqFkjS6EZiY6F4dlkIUM1ngI5BB0kQYBtk4pf2T9KrP
oVep6FU9t333lQ+W3LLERAPTAdsJycRWAbTEdSV0FgV3760Ebk+uMvOulavPb+glTBjg/QxGSxDR
vy1+CpQqyZoOSn3/f7eLnaqE0AO9qpRCFeuQj+s/ICgSjOP1lrkt9IQGbGG6WqNVi3uaey2PRKCQ
WXqp6AUQEKOUDF7Kx8fVQnRtULJo9AAXsHnM9EoKCTQOAZP7dUhnRD/cbgnUlxAvbv3xiSkylxAm
m3V0vSBkYmTbqK90luviAALKgJTUtcz4Tnsn1dQWR0U90TtZ+b9RjmIbzLTa4ePJ87Sg9DNg90zm
8LXMmo54f2a0UadURoZDtk6z40kly8UH5zXlL2PAqHcl2tMRUgMwLy2pKs0FJgFOw317SBDLBU7v
Jvn+GR7F4Bb1p/EK0Z56pdbNLWBuQ48gjWHgJSLzij7mEikK7gcFaCgE4mDz5TeU/QFrVZEIm085
goxTdmU18q4/Iimd3oWEowm4pMTWMm5H26nhCbDgcKiR1MrQU6jQ4Q81XBJtIO6ORHNT9Krg5Q0g
7ureOzNhJwjfCvtPCrsRrzhU90ecAe2UglO6FF+XIY49Meqcc7T/y/IIO+8AkBdPynEVDIl0iu4+
H4cBfFmau1ndpG+ebsITyncu7JUkJ2rR0ZZmPCbKersa9YXyT3MPVU2XH/EKGMz8C5BmDbPQVsVy
ap+1pu2YfDWjHyifkDJDNMjsOClMqajAXBr/sOgndiuVMaXkrfgmj24QqkXelDF+w82ZwbRhj4h+
vzd0N7ahIYOx+al3ac/7WdGC281jdfRx6zrkQhptV56VWGA3QYqRPYlJJyR1j7uAwDj2p61Bauap
J6yOZmIkJ9KntLhKwFj9Kws574oQD+CFJmcbHRiritaM3ZHMJ9tgL4yQfIlNSKit18ibel9i0dLC
nXzW5eE9tbZXrnUWhOWn7WOYhn/TmTioR8fCHnpz2lRDiVe1WjqZfBBd2wizWwVJk3tymKpZhDJc
bAmPjQLO31hRP/14pC6X0ql69vaFErCYMMAixXHhZHZK5mxzrPDU2TwP/NMzb0IKVyM6ZRJa51Uf
DIx1GYs2Y9xROGNx8muj7redTR9ekTasMTFCtyMa1kq+xJudUOJxcOYVlJkXHtVUP5ara+dS8Klh
C1ckqnqfEM6pn9IPM5VChmPIdS3B/aNahS8XjZhGYel/1BwGfug+r/WDdLvxlo+AYASY1siLae+S
h7NB37ZTPnBpeoWJoYtXj05zCWcZgHph7gMOUiO+FpGUf4bvfV0Ky9iKTrpZV9xKHGbJF/qKY1t8
8RwnUHR/tFWKXobFakSmf9VFCxp9AARKmqofkcZgyDaujOYwzuQI+QtswUoXUJrPFXahEfswzsrP
7+4AKiyzYzaYBmREmTy25XvpYiqvZkTm7uplL9B2HnkTGmyOH0diUnkzmqA6NkylI8k7XZd12jw6
rCb/617/KEBL+7aCfY7UmCoIf5ccawQ3aLpEDop0nbpvmMZy687cSZ8mZz7vCPKqwzxOZcuDKwoK
yRZ2iBKA7ykYmeIpxkKkhMvr3pD5IMUDnASa0eOX1pauCi1jGtSqI2zB6i0g6H/hWuSy9zsFAM2i
O9bsmPeOhiirRTD+t3OJLMgUpWN/ZNPsxwtkHwN0rI4HLXC9lQ56q9T8zWx58IljREH5RbFCjW4f
HD861TY0huq9w+zMPf70nLOvc61suSlEe0vPWvluVmTij+ry8kWaMXROZG2q3sUPXaQ0bdppqBCU
q3nFLJE9bYyRBdijBWebCS2eogIohU9svLeR6XDlrEU+l+xP9/0p3vMTZDa6+C3S9f73txPo0Yxr
c6ZoOfB8GhJKTOgK6TJey2MLislAAivxD7HBik79cIbukejwZmMTtA4M1W0l/3tKFIe/1GIMLyd8
6eWIQjaMfjopvtbXQkBDnXsLR0AT0UjUcUGfZRNyCgGyliVgDCEo81h2AjmrbypLB/APTrsL7LhG
5ni/wMJgfOFdVGHKQAb4u9ok9aABO5O4Hfy8HY8YKABydS4HT39aa7gQspBwpFZ0EBQzVpKpFnvl
MD0sK1rcCUjZALZ5sYKuIJupp1RLG4nttYoPZ6zRj37PnTumm1QNZRk7d9P6AgaJwI/hTX/+nGKP
jverIflAIcROSe5QvRbvjhKSAauKfSEGfm3sjtwwKE99Wy18dyUqaOeZ25lFEf+oOdck3QruOMtp
n6auOoMR1CSdtTOQVdGc3e3OQR6/35fZggkH9N1oXhIhsST4IODR5TJB5zBQPxKvCQuENjKHwJTv
DU7rPtbiCqbOPANqVdjTjisOGVGZRDvtiy5fwVGLDhKNmAOkFbPjnsaDkYN0QnECTBiuTqQGSoUb
tnDdXeDxj0JnhmAYu8hhaEUCkWsHEGGbuaiu330DCOFXfBg9K27feY2+i8TPlelrtWqLVNNI/181
qJbHEIKHV54pF4h88BJchTedciMruT6KguJNkaLx3dCl079uhRc5ztQRcE8Gwlst81Epzpo+c14T
nBEBtHAiilKkQY+XrSsw14Q8Ytot+GxA32LCHecZQdugEUaD9FnLJ6TetXW4UWQYiRCMs6WLB/2f
0MIWqh7SjwvKgJY0hfSyyWlch40qZVNIr1ODjXmr0IVXhVMN5m6iPjMCMWGWfJJtFTS+0MlYay/w
yqkDKw1kaFhrFTa46P/Qu0qJ7ytS5bHDzFi7b7Ay5YhPg5nXwal8Mk9rbcxnplAghZD+15LAs45j
b76cQbVkjlhcV7ZjRQJVicCXfwSGwmViKQOFDiXV2SYU0vipeHUO7zK+sb+qGI79qK/fUuDMtfEm
cJCR9seOgOT2yPC4uwTO2tX4eTKTKs2NB0mFbqyqiCeHZAngiS0cQIIjcwoqsdkBWLpyT+70rmzT
njvL23q+CKovEdDl0CvCTvttJ2f5jDNBCjQRxfR2/yJK7lSMja9XZTXIpPjqxsb1282acod83eHB
QpKd1S91JWuajNrnDmZWTV4PHH948muVgiYLJfiKE5/AGu7YKiGxl9ILs2/uxUxilHhJ3QcgnKaF
uKGBfvFrPFLdL1jDTMAdzR/VGNnChdVag3iPMe5qmf65RWNFHMuomFnYMJUJ4m9bQylF38FtVEjq
Z0dmGF2PogKtntEmCNxkNoYCuAkEL1s5I85bGRmsSgZGlC5VwNhB4z3KewUtkfWkhx97f856k/ND
04A95S5LCuXI4RsSG9Bb7rOwg/FCmh0TX7pftVcjKroDZ9KrG+4Ty6knBcMjcwfy3xAm1MfG/wEE
RO1RAS3LaTQ5tcye9prOWxQtbfSkYwAQFeOAQpIyjfykqL/cDkGYP0RG02qPNzbhTWazX9jhQjff
tnGJE4p+QbdvNczTi4O0mxPBwaLip/A5w2qBC4rloZnhH8TKNVTEqoYesBzhDTpMImULOszOMLYZ
y/q0pCXCLxaI1Ju49TUWb+521YwtJU4bYuzUDpgAvFEt/llxGdxzm+mNoqpcZc4Xkr2Ymf8O35Mt
QuthTzJSutplsuRFhZroTRva9SLnU2pAOLvKo/ucWod2gnPPrS4BdUCKNCpis4DtZVFB7TmwvUiH
ikjDPP7mDzjrZe7EKZkgr8aQVgLDz3PiueWMRVH0kPwb8k7cHwuBNR8YHJw6cF/D8dLOQ538EqPx
xtD40Trjl4VOAUfNd3ogaIv2ivDC+aDyZ/fDFNlhhG9iIj2/6nx7xF7qsYPATKdWyZedjDZPKKBp
BTkr9ZHoSYTySes9y3QZduAqIpPwDpjbtpabZY9GqBSVCD02EOdZhNrKTbAiXB67zvdttoUHVmyQ
izuiB9Xh+iI1RGE6NhooScUYVMRbDTsjh7cdZ97//rCv6qkprKuFwwI+i+vA/M581nELjcY2EWx+
5tOoudN9R0Lo2mN2KTdF7aJsm9djUypOYh64XrXE0Zyp4/un6dAeSU1dcrK/QZhbP6bUb86+XZHc
xZBWJ1sp0X5hbfzJpB3awPSWqyLUcWjaD0SqMymPujfr67pQXog2K/36+kmKWYz3JjfSisi4iBoH
mqnWW9+W1Mmhd4NJ0hYm30jZQXfzqJFUD55iN77+B+dyvyxv5q0RvsYKDR/b7xJHYIGNb+ltcYJN
SnNHJM0K7zcaCCbb92lY3PgxPI3uhcVPBSu9o1cxgwlFnpd/j+MsSfPR/kWj66wF8uGCMZRudSNw
pa+2U6pqKGU4zaHw4/Y8COYurpOZmJBQ/QlcSycB1E5grPl9wfrIjyeh36Eg5+pjhX6MU7l+FTv7
ePAJupSPp4JrlT0ElaPDiZB5hgClWJt33zaO5IKpCdJ43QzvOquolNzHNRGk4WF+8Nu/8FLJt7Dc
GIksGnnZR2krgo7ni/fA8WVqXRdEn2D1niHat+douVogoSfJ6LSS+awb9NCl9ohvTYK1CbsCGIWP
sDhKS+ulMSrhKrlQa0Q3X4s0tTcfYEoRSDL6hiOuP/zGXPuZlTlCEyenkekHkWbkHZkLdDn8X55l
02Fo2taCBRvir+hMcY3fj18vps0A/H2YSZCQbd1UmZ3mENQynw+t3i1ipbNfqOTwx1HElHEY0cMa
d2WrEVbKxga3i8Alvp/VaZ2hdBXs6R2AdSUTMO6IWvUJXAukV89mEESqNXVSkh36Hm9YIYNuv292
iX5+eWMKiSojFVRvCjoxH2W8n//sgbv7VpRhMqHw9Ph2tVruKtubxc0T8zNt6ukXBa7dvPasTOpS
hdoOUdd4fq0si9dFo3INCv1PHXxJa2DnC/vSKd9ChFX10axN4MQiIKbyOUWvBZyilp6myV/vraUG
fxRf1mr1/3iF1/XNslqr11ufiljxMr4WoV2uwv5E/NiAf9Hut46ieVAHEXEaX7tqguu2Jlryd7hN
7XeJYcq+nNaIWItZdS5M1WKCZQEmbr0gPFB/Qs6DUjg13X/Yz43uD+UbrlgNJv8pHcijobIezdFA
0NRxSnjrX46Jq97Hzh5rv5GOHBdQQ6QtEce+8fkcYDOpI4Uud6xlbsPwAO6M6HnMK4sAt/6w+nlW
vZh+kk5creSEdDGxjuBed8058rsFo5z+T7IcyV292MRX7o1vo0vzdN3K7X3fwPG+1meQ/7+/AxOy
yRwn8AkzQpiCJKTklYYXMxH2lw8kXf+WrHBeWEIqMc8hVJaLHA5SOY9NH3QgDrO9HEh2rTnS6SGZ
Q9t+MZqML0BHPgAzO2UmZE8Nnp5CRR5YY1+SObA6AmWxm56mkzN/xE4gmIpOTGBp9SzY4sjUe0EW
SJwpEf3stDI5sXIRr7banEPIyNc6aKKZ0c8j/Awtq3u6xQbMa1e651IjfVVy82gAh+fofZ98Pjvb
MEa/vkqRiC0fwAlJim/IxvN5naKw8Gy8+h8NxDRnjnOsan1vtxXakJnBbFT1wau4IJcKE2uRGjA2
0uEVAo2TIJKYmCbf88Df3TYIVYaSYv+ATxXGTRutcH74TSUUJk24uky3esxq+wcYLgedII+8Rslb
eBYL+ccc5ly3DZMw3OBQ4h9c3HpDQU2pboYeFBKo0pCPTI/52X1iDCp/9DbQ6yqFG71kFYG3XDp9
fwz6xBbSyFc0B+06Z23foucl4CG80OSgr7rSVkKjGYjhN0lsrJWzBBTX8SHx2SajO26dDcXRgcMl
CZtD7Kr3sERF6chrcx8wezlhO6z9LNcHkQEmGTqXfK/sYuhZJk9VWdaXdbtIWbQGSZU8P8syg0Bv
cwmAYL0DBjHREflwPUHA+GzDzBAxwSuz0bi8JMJC6nU1gOODloe1Jy+8RCI/3bLEUa3kFxac5cWa
WfTlMS3n/A5Quk90Tw2rllGW5OqmWwSpHwJSINl1/T2PiqxexLxX0qUWDpdvopNQ5tW0FeG3Di6c
A1X7I23Nzu9VX83U2eYzafG0VyJVDKuTWvPI9mib743hrJK1zfd+THh2qzqqTFmDIjggE8lwsobm
FLHCJwIS9HDxwi0UkZysE/1uVVxDK3hdihn6lROTkKcGECH0kbGgJKw8gBk/AAS86D5Uu7jLIUOM
XTuWv6t+1cAXKgfxMwAFyzbYP0uGHLJUCGCpwNKz7yA/RYRANbK9G16YClQ3ouW/Ms+v7YpuYTxP
wNQt6cjTw+Va0wc/npM0EYS9YZFl1mCxonTrfR+CT+J7m0jRgQXkamWwfkzxMJrVlR9tdu2gEW0s
cNox5tylxJDquESrQGiokwnoeZ6foZbQdCfRzWr6wz+STVyUTTWq1KBeJghfOeoCjWZmsfS7kRhy
LUicSV/8WSfM8SD1feXsmB/WBT/qj5QJ7dPG45d7eDlhFD5c34CpDzIZ7h4wfyYtV5ypEDfTgEmo
Ju4HTbbqMk6NpNeTvAOe9oWuX87PViOBCuNg17SWNVQ+H29df2p80Jai3xjYDvdnNvcuSBivmN7s
Mdegh5TnJ9m0FSCYhlHNdZbjRQMM9aquBAqgoeMBfiLw+gt/rBQ1gvmx9vdgN/Fjuy9rWj+cida5
uojKIxqoKv+q+Hrzf2uQAl1JOp/Tp2hbePIyB2zYr5EqurWU8ITbodB8hdEy1LoIMMgSCwt/10M4
L3gDRvvUj/iOEfqbErqrG4O99GVBwEFhYZ+B2Hxlxk+C3WX2wVp3eSs4Shcj7uo+KNv2M5S7y4Mm
lC42JityZHWXNaYpgSdhRC2F5uBXNeSja4Exw1kWjJ+sD7dU52nKTJIoELQDgFNTnOfcQnDmcOTi
NKD+WWHl2qrK26QWEJGqMlvW9fG4l0AWwJx5tNM5rOGf+zeVJ+mEdz4bp88Fv8rxisfyN8SipZHC
nYzKdWspgpgvcFR1KrgZR4NImQQZWhMKYG+r8RTXYRQ/GGO/2gbUQnBOh13obJpec4KMlL+CEvB8
V/GfZhAd3HG1rUrxIyfWBsE2/0uaEEozPOzvPbnyu2SMjyLiqnt53T1cjK5iRM0hT7aFShU8uBvc
98g3lrhONmNIdXz4cIpZHCB8d1x8Qljb0HZc51DtLfJOSvbOQIDwX9BdtTrIEVyeCCZhi2H0TtMi
ZR102r5NeIOkM2PdrjxIkNHXk9P72WmER8o/rcw+EFPpT7NS1X7Q2Ba0Mk33SmhbsbRHqEdhHKUR
gyO9REfXQkZryQKxZt8zMYCImPRMRHRlMrBoMOvnOm/sNKx6jrgUTeGWFsCmaFUCi3Yls04fKOUr
w1FF899kf/X01ZUG/jfhmly26nBI+5GJYUUob/YFf0RNs7jqOwojPqoaYZJhsPUuPSkdgpEWHqB5
grvslNmg+dNLDrvGdLlUu/nCESoO65OSIJ0d7oIFSDgGb5jxUswytSUDUxZdGSAOIUZugSoa8QwC
dCwYu8dJHeIkJw/xyYvn/g8RW30AseKhXvDmvsNzU0oAAD8K7vGNArIwjm4fL5bucdLfS7nZo9I1
+GGDMwNS1+OX0VXRbx8jzKVrZG9S8IM8sBlN78zoHsDVD6rDd+vY123dO0pJORUtbnVAFHOsOUpm
cxsjb1/ByQAFFXbRiYsrzkh+e/utU4TRivCFNSHAr7CB354vhqCbEm7hyz301lFElamLI2QsW+u6
xCzxMYsWORWsY3wgSSfxBN+OEEnt/r4xSBJm9ByzC/Uk38zomPZUHyIKffnEP7bgNph1AWr1KrjV
47K/0ghkngDPuZjFTYZ6rHlfF53VIGoD73EiRwJ5Du4bQmUei8mfgpZw9ID0JAB9+1OR/J1JdLs8
H7zUT/sp6THg45kii9kWebwsh6dQkw7SxPatO+xVGY4co9myvW+q1/0Sze9ksXfe2LHyes1XdAZA
tqly4/zCb0jif1vVx2m0O089WwJNFhomhS/viASckCirXfEAUiC0YlSFq7bC16p/6IIkrdskYVF2
f1YRBT5neopJ7rtpAohCLRdf7b45RMrcLyIjVby/HKcgEIoBQ6X4HsBwuciSQ4BcFbEgC97MJu2+
i4FedkZ5CHd+1M2G1lJtRjInD9KXCMmVtE6y/jYFiGI0aC3MjHPe5dfxVlwTGAfZvDVmQOAil4IL
LgCwRkzYdKqgdj3TkJD/uYCNZMlI2nZab902ojY+TtYqZR5qdPAKKTIgUqkPvpvSMI7Z18oZDsN2
J7/XHxjYjKmDL2JEGv15SfhtGvo36ts3LqkhaSAj9rvo0D9FX1IAkUaqXuXCjdjHJBWzmYigvQGB
pKzoueDAp5+lKCS6I+8uXgM81agTPWeb2P2bmTeryTuo85qTBGlzE5n9PKh8h6jsD1UuinsIsFIu
v1PNxi4AKV4ouefDJaxpR++UQkjTdKh2I8fNzl8ogTS2l4ypQufOIfr7y/5HNPNL556494ik9PHz
42LUgBWDL6VgUpHbyifE7KqI40RLHxaUMCQVlWxQW6PZXFlgWe/zm071vQLLdLcMyWCWEYPhKkg5
n1Xz6cAhaURwjVpDVGZekKozq7tL3qSzoh7Ik6ICEbELFTDbTeK4B+bTrXab24gYBLEkxvy7vc9A
Cy3Q0a+/WO5EVhjTalCpH/xR4YCtwOjeJ3uyhWDf3IUQUUbsZMbXwqWVNQV78MNtDrsCLGJnjOcF
sgk4rdgUF40CancncENGeqfaQD/4s6+kAQLkvQttPkwqK9UTIiYigyTEWZrJdy8rPazK70s2FS1/
iK50nTC81IkdwH58THzeYxyYzMPOVvt80VpFdkJ5cD9/SqKbng7+B3KeNAbpBcJfMTjlh6MCDAyE
qj5G8m9Y3py3NLGSoGahJTv+FJExpftH+hsmpZPucbN9Bq3v+flggwX1PNzSVu+bwnb3RM9Ni9uz
ouHOA4zeWWP7FAJg9sKAqBqAdyCkBKldyw8nVTEHFPlEvYXnI7k5SoOs79nSPboqTZU1HZAArI0A
y7bFAR0Qk22hOlZ8q8Rdmq0fRmir6zhCk2Q+8H+G/+VPTZurIRPhRSGBGILHS1bv7XneOrrPtZfK
VovEYHUt3tsHUe1YCNNyLn8rrEBc3IZZo3K2g+198+a+xicN3wwZu6W9irWYXxRHgg0/mNGp+9QW
qPTn5pIsxilYfBSEaE816Ue9zbqunm4T1YpHwUspc9Spjn1SbGUWH7SneNPBnsepl1WiYRcCcwfe
6dTKS0G4MbAxU2GwE6c8DQShsDPKFbYTmJ5f61Wo5cnNcqzbcMJkbVQhJTpWpIpF8WCn84WEmNPj
K30M1MVCvymb7ZK70gxUeJ5R1YNNN3e3Goi27qc9esiOkNXNI+pA7oa1hos+yRvt/KBLQ9vhRRvG
Yt3hoh/5zx1IYGXu3wxHH3SrxAORPii2hXodDJFJktGbOlonOcWPHqyVmwBWr9WN+Z9zFHMqkWNl
gPYc75Jm755SfXUmT58PbpqXaFJXaT8QjOefIW3BoRBlU4IGCsl93owRFqVWM88DL2fR7H6uS34Q
ge5HLBWHexOSK71GnwJv4wDGpXAbTrag2kXfMtXhNrwmhzTPDF0XX1dzRCPrPQOvg8gQCSw6MPcA
AFBds7e8jiEyKpLZ9Ap/kxxmVusLKWUq1MpqIhs+XdKwm54T4iUBeFrCmIPcidqLlrsFQh/fMrv1
lBwT4/Uvy5Bf+P8e20GGRKos3nRpmdTvRunmtkjqROz0kEbjsoNrOBNWIGEZxUyaygVC84QE+zQS
GV+nW46yle6qMdNjISL0i7+US7PEqGhXTT8FeoIKZQYEaGXJ5MG3NaqF5v7UeEnUJDX89KQ+G2bO
JcelNF0OHpT+4xuMPtTnJeUKGCSoqMJeTlIYkZRMmbaa76D8OpI0BRAamHQi5iEK5Dcby3MIw1NS
GM9izL5EY1WKGgkV99fkG47c+rcwsgXC2uhUAwD2ACvd6gDXtbt+rHJtILNEHxaDbRJF9S0L5gQZ
k39d1s3s5fkq2w0Jvm9nAXFkqkORli6+JeXJ+c1met6AWYU6fTZmxOWH93Lj2hK1L1XXFZq/y8ZX
Tm563LI7i5K9m4kckF2ZDJVtVK0GlAWeo07N/WaSCRx+fCDVKfsOaqkiY2hdxUCKt/hZAQB+wljN
L28pHlzco4S79cUQDM2/r8DEfcu3qlAPNSL7y7pT1w/EyCj2f7zZmEc1Q+7wT8llu+nleEuE4iTF
IG9JoUe+k/Bo1Cxpoz+Mud+lVDNT890q5/g0ZT6Xs++mTaxKIrXYKzpwBLJ8T5cjyEWvcZgn0eZJ
bbpU4zXQGI2mxv/kk3iS4vp1GzRqmcP0mR5kd/OufgOJ1xhSRex+6RDsE2jODkJ8VByhNOyHy3Df
ekGzlWZfwrmB5KN+zSNqBIPY9yRw5bvp/VhaDrqn/Kh/vZmqc0Lf9OxctqIY4/Nc6Owo6O2l6o2I
WEb442rmG0sbmwNGeennsRl1TM14kGxFg4kPhGlzMHXstjFUsnYZp93EtF3h7H2BsKj9x3Vq6nbP
fUCs8jPo4nmduJehFBhJB+nLtQdMl792e4lXUauzAMBlhiA+V6i/CXl/MHdbfU5FsW2YFVguqKSk
lH0ZTk8b0Vx9Y0DfsguDWSioBf/0RX8/QoyBG0LT5bSoWC14napWQPYa8LyF68ekn8KR2YsEJXTv
MCQ9DGxZrk842nMorC+DHe9GwkjuaB6S13qlZsIUzeD9uPF/oxUDMk+lFKQ8nnyeYdBeaVEpvHba
AVA1AfoeotS8qESdaDmrdznH9z8ART1afPXtHgfYXZgkgpUw6JDNkE7nevQU3pkeCYeVUQW/Ef3/
0loP/n8cmhGV0Km/KSo1b5yT54egv5I6a1dsRZcUT4zZafWLdiIGPN6EQ/6VDOyaDztxUOVAvk9c
c+WJmrc2OFftns0J9gVt+AinFTfLUHbOtKCuOB1D899ObmfVmqA+kLrcA890ptyqVPe9tW2PUj+/
sultDmqIIANpe02boN32vj9STc45AzulQyklBRq3M7Iu5ijZiaqlhXdRCjXMmGCm4FFivLtEcuG6
BlX0qAaqDvKWNfoOM1Ui3w6lRFOxyzdKBxOUiwOhu/fhr2o0lPB88LfKIPCjQy1M+OutHvdCh44e
wmzDHh9mlQ82DgueUsU/WM7p2aA5F3ZGS7YvDvhEWy3cAdeEAIAj5eKKiYcl5i++aZEa9g3EG6Bh
PY30Xka685WoCXAYGEeL3yfHa9uc2cP5zSMnvszH/1GPP4+Gi4Nfh9rSHjad4Izvkc0DIfna0++g
+d5+h9HQ4k2G3XvKq3sLKLP492Dfa6Hzb7WtJm16my73ls/MfCILy+K9BrimNRWFxC7JtLwu1C9k
KimGeJ90kNo8K18m2zFTnczEJ3RxpLZ9VJf3SRXBUsGIsVyToyR9wFWsE4fMM0wIx4d4yP5lHdQ9
iNnzX1F2jkd8vs+dl49DWl+4qL/qNfCiU5byvx3fIqsrNi/L8hy/aFx+/o7O/0ZaIaJC9gfLbv6G
8WAgL4CvmRfUh9AWHC07E8jGCNzl2q0yZXTSZqIFdo2IFGnkH99sEfUg8wg7OY/b7ydDmIUibULZ
5ga4khD8ozPP06xs1xqSxGIQCBysNY8ontR+/fEtmDJG4OxkKAAhpjavoaMGR2ajBSCFeqGKV65C
Qm5fycOApykf/ks69oJQa15bwtAiHx0vtnrweQPNJAwTfKrKeyPiJ5J+FiDARcVNNFgzxrwyyzTo
Mkjc45SQRXT+6GjykZMFodlitW4R+lmwTL+fqWTKPhNNOIzyzIPVsHeCHWJl7D+KmkhFSpKxdQRQ
4eun7XtkIQPJjiUvn1HOcvCO7xdFgVFqOIe5zsuJ09IOfpUzLhVw/WVl+Jm3CIh9+1qzlAgBi0cN
cuSfvD7zXlfNrov4vBWMzulCRPCQqfvYDnrMMfyDOFRWi9ZwaJ9BDtfLv9TqpVeL++FxXIVLaGjP
+yMdZGb7K9bURbm4MhVF4KOYUg/S4lJJL819S4cvMAAGBGmAC6HwqcLIl7NJRQ3lMuBEH81hs4y6
X3PKC1f+mayH6LJm+gQQZBY2+TiJ+2PjzpJbPAZb0x1CS4zBEBf5PQYLmkmTlUIS5RN4qmHoFBwv
XkBizCuFbMuNsPC3poWfQdnH6YrZKTd790AZ4lYvWcP2za0aIxP3VWNPtwQWiv6mJgf7psKxli9K
ZljJVs83FkERl+ILVccwMZSGSgYgV8B4be+6DmoWtI9MjaXxwtfZ9KRtBxFI6gVp8WQOGanhx+l9
SiFuFR++kwgqmHZn6s7jQYD82bXfFwfrBDHcIv3aC13jSy+QMKMU8U31/PCa+wGCvUaMM/rD2pM+
htET8fryHw9ixYM9AvO+ZiiZgOqhvfezEQ/7MDnDxdCxFh5Jg7Tj4aDQAd4VsrzEDhB3zDsQKQ2v
CJk1UkEvlYlPy5cmhNdf+Ex6uP4hhTqtdQwKEIA4N9fTwj/iVueAVTAWM7bgX5t90E5smpn8ANfh
qdggl1St603Rt4F5/2oI8WJ3uvBWq9cImUMcEvFZcbH2kQmG2/m+A/UXQb7B3C+slKqc+8dIzoD4
o/PrjiqxOfv9D79yaabaH7ePytKLXJuJuO0VZB2fRskMjKYAHpSL8FxIE7AUr1SRnkTS9jtJAc4g
qhZY27NfW/IpVZs1KxmJcjlrE4x5/K3e7rW2FpS5nr62tgf/frPHyIy3L28jKI6gYEtWOlpm5Pyl
Vnp5xw1PQZh2XUR+XZpFQwzuSukyiBLIYibbi6TeSA4zIGdQ4N+tkOH3h2w+D68zS/Wpx7MHhuxC
3RXyn2hzw+vAK9+u6qGZ1tisDcIVgobTZFWMi8yZPgMzBSfOW15jc0DjdRog8WiHotuLeZQTmLoj
6hvc76H2nUT/IpGMWg6tVfE29mU6LMcWzi3ZHOKLkpQlAtRiZQkEeuUmSke6fGVuA+dmXF2+HxRf
KottWPij+q1WN30L7oPrMLsropfyYwrJswLJlKbA1/LYczOHI8ewB37O2dmHJQhF10HXeQIzqGGn
E6w3g5A8/Bl0kMYQpbp32+3QIkwnAb24xHPzXJAjmHORUHAS8rDCTodszbEEOPO5RRyGs4Se8Kp5
YJSe1ApEr+ZcB8qjr2XKMgrB7spx8bKIshjTh6qzVQrOq+srFlPGe39C/c/zu8kSlqzID+y5zn/I
WHDUT2EV0b5cW5W3IOIfHsze/3CZgX0A8gKRSzt3dxAsN7azs9cIv302WGyWwLW5U+jWxcD8+5sw
1NH0HSiSBizaok/goTprYDUM4B02N/KDKiZttmRdoeqSvDb+46Y2DmkSocNvYxHXbHSlnezD7aQT
V8GWMJ94VpcYK1P9FrpqYUBuiGh5kL3C0Z3sxCG/6+GvaGtWq4ZOAfoP3T8MN7AQae0QNSK2D4eC
tiKsNR1oNgTl5q7WMtnSS9HePnUNqs6njGA6YwbgptVBmknFK5AJYLhkpBIYuKh3yHpP1tJgd50q
rh0UUeuGKPw95Rmxh7SdpP0HLYEZ8T2+bcA9uhaP195CSEyxKWtIQwID6RJ4zxeu6I+G3HLOGru3
3nq31rq9B9AC5k3MVInwgQrXBAKrL68gGBwLbdumDwTi2dFCGW+0xgbR+koFwawS7R7TQGAolGb6
Mvw6kTpWgIaF4F1TDSmeOSR0garvi9mXim+dAi/A5Q7eMzxN5SP8wuwUVFO23jI/4HvsmCVdrSyn
g1GnZPwfxBH/x9CFPUKnLXI/7qSz/WdEcDMvGSFeYhut8M08rDEpF8UrjZ00LaBeQKj8pjnufN2J
TkWjUYIufSnbzBHG6X6l0yjyLC+EoNN67+3+psbOg0qekZHJOPlE67mezMRd1uOwcPNyihEl/ve0
GEWCNblPKcJQpP0tEJF1UZpuhyv4h/Yfz7vasP248LuLXDSxgCDYgREw1GPzpcurJYg25yqskmFl
Z6XbLbVJM0BCUTwGIUdgwZjOsdDwZVs1pskLrH+8tn1ATgj/ht8Xk6Lv75sgMhE2YNlmzDOhHikN
w3ma6TtK46v6fcfGbbMl0+OenztSlgYoY1x7gg4tE3T4hL0ml126VCCNQ4T46GY5nD9YYfdo6a1n
OxDuwWY3VPOuLuUiCEq7c61QF1GE++0n8rDc+c003lPfdb8qHAf7mxM53jWb2vU1OzN9aaiOKfGh
O3KeHBF3QjkP02bYBPFl/zqHWJiqYndMP9FTIFGiYwRVviQRdKCu2wLUe1Y8mWE0iV6V5V0KlsJ+
dPl/IwROLMHAW3GkIw/8yAPnHgABbzB+4bS8l6u12eg8ZVSLLvyKfJZ6W5DaiLdtTUDlWE+8VBCi
DnTq8SotpV1XNrbXq6HWa0BGEya6xT/BfJEC8OKcd3xPyBHLkQZb5UQEVnCV2WnWT5HymkLHOcN/
7KVZFNkgy4gzbQUHmIkjPOWij3gVdyLk17eCV5BEUXFskERU0UpajWlibbKPhtWJeTC1STPlNW5T
y+fZYeARFaug+x1NAcXFJQj1NIX+eOittUI762Hg75ufZTpvqJOwMtvJbcJdbQhnAOK5mhACFKYC
W9fDRppJ/zqjdRQTwBxSTNHFVml2yFJTjgRo3noCMwMl356EG8+zJZMEFm0VLK2s/pSvgoDd5Pzq
QhT6wTks0vBPqv1vTB6fUze3BfMhiQkEhevrXJgEPmX42x4TAxXZM1GpMWhcluLz/JThC7/aNOZZ
JZUJgyDBfLlxu2Kb8MQKiXa401ZytEoJr6sQued+qDnDAj+SgPO7i62ISTR+C8H3/MKKZNd7T43T
+zeFl/YTsbO+gHPYkbQfh9kUU9yd0yoN4TgQlQwSOz+CQz6mhpPekMAOUtKQEl+Faa+oC1goC3b3
HBceLMT2GYUl4yVJkEeQ1fbxYny+TVl1SQgmufgaab3vcoY7ew+vbvh+JvBr45OLWFQG7JEhV7Yi
67xy2djdSy3+43LyNYShn+t0LFrNndHWhJNb6mGmYdCS9yA1qr49wbdoH/PybzqctJN8DGzcx7hn
Az/RD5bo/CHgYpFSBX8/3V+JdWqebp7tQ7QBYOMXG/sZmeX1k5KRyfFAfEZC5dnp5dOKQ7a7owTi
tsb9rY8x2bikvWfQTfe1UQH0Icv3z85h3NzmQUyHPRedMQ+mYiiTr7IL0AVZiGRLxIaTB1jBNXMD
4yGbNeycq4VWuweMdQNf5HPxHRFLVeD8Q3Pc1/qzlcLbP2Yz201xYj9ieTz7NGFmE8jisuVjeaJa
akWv4zrvvN7ADksnsZ5PNuHKh+Cquv1MRLxGy+GwQXWMVfmZ0aX1mfASLWN4QjZKUOYDTCxWCDaZ
KMU01SmeQ+WLGI8bFTpHnsxXD9860q8ZAni2/6vzYDSy7DqCxYMYJxYGWlI1PJetFPS6wnpJ+0nU
DObn6sEXB3jt2WZEx2QC1xCWyDTlMUsxdmBKGKF+yC/KldQi1zqfzsT7y82/gIXotCg9GN8zS+fY
nzp5+sHEBDhFS0s65ULniCyWGeWG1nG9Nc7jiDa9I0WlnLFrK08cMediz8j+6IpKteQyV0Wt01nV
eiTIxI9uhb7Barp6+ef0VXYnALVJ2P2du59RiyOd2pzSMKgxk/IFFipW2DxniCQxcH/ZQODUsVnt
OuH8Fd1YbcRK5exo94TKKJyxLDvCn1rUIouq6qS2X4m0m0anvX+UMlAIn/iGrwNu8br7/4N3bQQk
G3J3+F3RjQybatygLd5IASZ9uZvqAFubhqBNC6taUbTe1NW9zlywoaXIsZEkBdh6ZXLZNYqtPIFu
Gt7rNWq/jJ3a4RgOtw6B1Qs4Ew++jzNQiiDBAAilao1rWW2dk9HplWFIN6x+qioiiR5H12jqr2aY
s00TyoXYl316xEjYEtT4Sw5EUiMw7S68or6iLLcMXnm5kW/KCYicY9sbQB4buTfv0kpKZYlvlwWZ
lE18CM24tI37EIHtP4hqE9BjVzS4bQB4fF1p08Od+gCHdES5auI0XfDWkdhQIPi4xrZ5Gldl8Sah
b6+q0UvJuqRU/k1RZS0mcfECHG4iGZujAlwDmx17iIY8cMqXBe1IFW0Ua6RMQV+mPQbocNESOQdV
I0mVwrcNwl32hZYwsfmdvbOEc7svL+JwI9ZPJvJnki7+JgJ4qMRzr+8WzDM1DUrPq7/GuB6qo5aJ
Y6Iwfnhaex8msWK8hKPl9+62yB5yevD2dB9Jw1EMXgM7ddb+4LbT1B+H5xqiFfHj1ygHr5+eRv+4
06f6lqtRJzI8GnBMmHRkMqWt28N6oOawfPDiIosLlJXVdYJz8BpzwpZINDUnVoj5KUrdsNSWTH4S
HQRvVkUaWLczJwDPqnB0Vb8wJA5iBOsqC5WPROeCY/nk0KKwmcKw8/N72Mx35XQtzXdWCRNgQzlm
9kkvr2QF0b++AvrMpaqNEdr2G+mDoa4R2TOkiaLO5kLQJ7P9sm2xqoZ0w+WikSScCin53J6Z0Gc1
pJxTy5bDJQ/FPy4/YUdS6iXxHdcZvEhRL6wCePH/k8gz2L9WjN8RRxQDTan6C1i3h2uYltvrgjTS
ZZprtGxn+AwwusWTtI2o02PZzrXrAQb3vH1nS75XM/UeU8NtoPXRJ8rQxlW3QNfDz+9cqxO1yc3J
OcZ56Qmt9lUMRrAKh9MToX8KQHyVoh1JaIKfsAl03veuJ8tvuaqINJXpgzpdIHLQyT7El/hpe1ZR
wmXt/JaZdMWtqWctXiwEzdc1qXm75RnOllDBJ+1Mf30tdQiOYu5qclcfbzgIhNnwyKOERvLOV+6w
souNEZueeplNAUpZPzgaOOU+11JzRTQ32Fw3+zQ22s7LnGE2vDA2wU7HONoTdYLjXYOe42ry0gIF
lcDkuluNmOPbnjLcs9hgsaPJyVLShoMTbQpXaAW0iL9sNgG9iWSOfALzD1181tzaW908iBFigPya
AOs5nIata1rxNv3SBIOzp/PZCUf1sVN+Clz+6uwgN+BssDg94ugx2grJ7rppfUPXBePY4TstpryR
K7KPhjRAXcBnJ6ueG5739UAQWO6GIy8SwF/nc6AhucePXgWT7icKoKEWA8OQDohmp9OKnuADzyCT
smLFQdUobQjcLYqyJaIOYRhn5C0DVapLVyKQ8NTsEnlxMBQS0ffVHwHx2JwHj9f3ZGce0UhKY8tf
F/0HKTwSnThT80VxuolZYnmW+Sf+mJN1ZcTlFhCfAFYI/04tJ6P/yXjlSuwPUATOOVKxOyXWnhfL
kGBal+NbyzNba8RS/Jm4Ij54OBfcrjBc2kPMS9XBK2LwYtvgdjaMRv+PpjCqxtYzYrtOx73+wyg/
nAcJ6j3rFb9eV1czuyM+DJ4dRwN4/gyK7RI+6SS4rYiTC5XVZ5nFKB6dWnsoqErWD0EnBuWz7kmq
iVa7pmQ8/j9DudBB/OnR1vpE9DksWA2n1gU6OVcU+SfPHde15QGiKJFuUxH1kvxqUVmZkK7L0C5H
L7Ts+rKQGOmn2P+jcstT0J9kNwzN5IGPzT5AczXUyrUmNit54p0UrdCFExtVYHY1NXKNRBonnpLr
G3MHabirh3zR+WqsxyeSgOcxxdMl6CzFPnKGS/RqrUfYNPxBevuE4YngmvOXTVlg3XMtdw3SATrg
IlYKP7XAEiUuKPM9HcjbbBRT24pppe5834V+TbOtorpoO/upr6kSQ5odxZG4skfO7o41Ue5/6s5Z
aW0H60AW6/7I6ATqBXhWJNI9mrEqIwgspOGe1F78AZ9G6kViZCER4WRBff5ned7ZaJg0TXAhJ7+B
kBGwP0ugfbsl1d6ZLQzpBFsDdDfnDal2bGth3pkrxihW9a+SBIoCEXRDClEc829PoBheRLOItQ2J
4YNDrxLMadXaHVHMLnKDi4BeXVv5SH0OwtGGiBOlbHnAixSFhnj9DeZCgIuEU/rvIQPlhaUcUB+e
egTy2G8Aizdq5JwIMcTyd6OZ7tr+nBbdOSUsJtRhmxH/DR7RW+xk87ecOxVqpFu8/57Fnjv2jC0s
eewIATGXIg1dEe3LtxqvbehyuikT5aiYctbUoLep/I9tcvCGLoCR98VMLk8LMMQLrcM77m1ViFLG
vC9z19mDpVIr0OkaBId8R4013ePXu6Ouc12YMgPWGPXYdvBZWo6D+nCPkE2nMSNfufZyuGKvNWHN
BWnVexg77EcP0MRYC7yriTW143ozbVTMcmm0Tscm+nt99ScWQuZtz/HZGUfQKf4MelhgCjcopu5s
Bx8sVxIGjRN8c9/2Ix7GItei5Q5jHgixKJ73HnbbO947CRXwAvL0O3yrquxQBvtiTXBrF3+hunQp
DE8zbhTjkNo1+tz2Xyc818nR2wmtGNO8/WJctwBBObvi5xXiKyb+/XBmKQIZuEn14zp3vNdrpl+S
M53yuwS5B30hM98Fg+LgTge0w4flV2dgb7UFRtgcT2bDtxXE/SS98Q5fxnKqbym1//BLiaAsNVcF
OC/s4hif9vrukoWKJ1KLMcwElzrGFIvN7SM05P/V2OcoUsQUO+PIKp/+Z/n73Pzr2RWFUi3eAaxw
LmkajGCNqmRAXR21grqltyK1J5x5uSEf9AWFXWPt/N5GCr3l+UGl6t6DROOqKiHA6L9Awe5wp3W8
wjVq2r/SH/h1DEYUbimV9BJvLcq3UjxdwxluuxioOdCIR6vKCwVbo8KrhRmxQ3LVLx6fMnwz0mkb
Rfys0L+XwY90KbjC6fLdCBXCny3tFA00TE54MBo++uOWoWlxzpSSsh5raxyQRXvVH6VK9SeTm+Es
olXv5zJet3pDZS9Wx8WtgTgCcMnqVoJfLXcNciMIzi41p+pVygJV0dYDlNm4NW9472jmWVFtlc0O
i63ICoL9x8+EVdeUCoHDlYnVxqWPuhbDWOdElifp0KADHJgf0s3v9CF7/0LpZWMp4Yc4sIqdH+2p
Fj688dqSr7w8ZCr1ei2F5Rwe65dDYLgdKeBAK/ytykkrYMweq3M2GizYrzig9hMjDIlg5An8wZY/
m6lIGI73sR0s8Rf/1lBjrK3pLoO7vbpnrK4wPWPu/khB2cSzs1cA14DBVAah8N+x2GBtm1H2uvYk
4aOjyxb4d6vTV0KuLgmYy5NGmXYi8He4IceUuvjAMsOE7rI09NYKs1ZYoSFHUCPGhVnhG86zrJu0
EOzbBZmcczYQwg5o4IKiADQjXhRCac54e/SsyGkDF0jMyxflA5kB2JyFxeEL02HX8BX/QDqej3Jd
dOqlo5HYXGxT4jA7Z92gVNMoq9ktW2t3AQ81ouWEHqN/y1CKNLj5R4ZkHhwZCwm6MjxuF2gFaM8b
88obenKA2a9zokjXjlssDa4bzBbpE3PIxwqKNHUCvF+Uo6PTJvKyj9RFS2DLt3ZJdT5H+xEQ2Gjm
3NtPidsleoQLOs+qqK9gc4P4qCoBsiVRXJAZlMgUqhUy1aN1JDkERxa6AhoOou0plqQLq2FJwLio
ded9F7b6KX2W0kl/JUI2TZ7yfGfKUDnOFnf8si74xMwWnRWDQjystmWwsKFaJkiu80QWCyg8QbgD
ap+Ap1ryBKRz3H17NtddSqrvOyFqbvUdtkJ4NqXCUGeYXYpWajt/M0URuK0gXgfP+YPgZuSyyqEn
p2K3cZvrGjtgbMPWR9AmP1YhtDSAMvlsDHlD77RvFhW7g+v/K7tmWaYgLFb3VCY5DtBTeu9BIj93
W4+OdLtxyMP9yN44TKGK585/QxHJZH1FRaVgs7WJr2PtHGBtTCGN4gUF4ozRunpPWkS3VSlIRRFR
WmMLNq27oYlAIFGfmt3Tfv/mwYQSNFdnk361dIhBbW/ofdaqunXDrxdawRIpWi54jRVRKsJCHaES
5vvYZR3xRUEne3lnpn24ymAig3GQjjDt4WDZmcpL0XXsJw9SjaRGvxU1f956kaW2h7BxTSoTzRFP
EDLcqD32uqfiC2soKwqotTG6ie3+SWVaxPtXg+mDQ7DOMrKu30PPMwNBZghY6lrn1a+9m24IvJQL
Qpy/5VTOMMkjPf7QkIu1XHyTo5f5lFHyiaPutOACFT918fm1tXTljY+K6pkGokcayp1nAnQuWaM+
b6nK+C3r22xOmeDw35KO90lP4jecVo1b6YNySnPtjCvRN61OY8fb8ScVutZGH2BAx7U8wdGiIISz
GU6oTmzCsFgHxD5vM5eUOc3xkBnVw0uOT+WBVOCcKCqtWFeMNnVpe0j95KgB1a5K7geICLbFn0gh
sqUD5V9jD40x8I7GaRj7SgPjDKOl16vZV4tWuHK5IN3t/M3M0nnGsYF0sNpLUapcBHuv9dbO8krv
E66pwINWIz4NjcMAdpojP94DOORkCxi49ohfTnz2IKzKStFheYHUo87s7bfCFuZPYkFwFq1gE2+z
sSO/vv4v9h/tB6ExqzysqLyoBr11fS8YKEYTjQ3V/SfRIHZkaaeQZsn5Sv58tyNm9evtSUi+hUnU
iBcnmJNMgV68UcVVKn9827eygsdptuOnXn//jxNL0bivX96qpeQkGkMlMK7oBCutLs4Y2B09tyXc
9/D9XEoU3B2igUiuTEnD+u4NAakSEgcznt+wpuZAcm0GXFLZNZrB/j3/NH5DJoIrmi8kIYrIxydr
cxtlc/Pjca/MdCIDui192uyMsaOdwy7JHhQYa8RK5V+N9Ql0poFSSTmWRBh5V5iEXSVG612QCMNJ
N4c68Svp+cu/krXvSiGcuj3hn8nZkfvB1o+0wdStH1rsKJ5as7tMx0bywkWcNBOK2muHbMotJbk0
Tg2uNosA/2oLdsqYQFQofyumnaca7qnNj84IvWXU8FKUwkGhBXczfWprJMjmG7D2r79oFMHrE22G
VMcRYOfAywuLfMKA8EvXyXxjvp7kGG3ybBnlFH36ED4hLrIk2jQLtQxYWnl/kWAhdGyb0im9OFxJ
YVDRXRCqkVHher6uCuACA5P1thO7Q9Pie21Oui9a6qiWP5473kQlP0XLhFpf7Ax+p0xzB6IMmO04
OM7T8fULAFqaV2MUOINVj2sTsR3/Kn/tU1xqH+nUy/yDVaRSKPXG8KtrlgiVMqLt8Ap+3+m4oAhq
3Ihz//S4OXEK2ptUJeoJiPdDpdLwQbecxf9rlhjlTDeYJlsDA2DIDk3cgrkhr2iCoMQ4vcVlgbHL
O0aZ0OAB+RN6613hFYAl8ncNKGOazlh+opPxbtHy9idGJ3XVghGuACjlvIR5Bnvq1zEPpQJAw9mE
8qF4W8l0wPEtfYStUsvqb2DvZKp2ABHUePtD/X6IH1cet9fsI1DxydhRCFXiNy7lV4DvMSx3TDoV
ZLazCiIg5i3ce9mhuZLNnRpfs7mjGc+wqfMrnRJdI7th/2QWLFCQxXFRCUULmpqO5tpDWY3J6XDC
H7HsE6P/fuIxCVYzlMynCHRYiTQ3Y3SndLHIind+0l2VQG+STFiBzObPyQBK1gnVQh+i/tuz7aps
KyV2OQtX7rEOHlY/TB7ZOoCEyWD5dRp168rjHTEzOUMJQc6AjI6hBTDChRgAzETDhZKfAaklwLBN
63OzOSvLNPPoDFS5z8x2oRMjI7G6+XQHZb5/dO0tjv1vfeTGnkDvsBsVoaiHAVmM5HVY4Bbti5Xr
DOpA1M2+550leanYgyimjzBdTsmD3ujQFOcxt4K8sJTYYT1Tct7Um2v9ipToy2Oe1iIO1KnIdyQY
JAMikRK+Gs4KrokKAOrT7kn+3ieEaaG/oNY4fw7MPxbOON5IdMvAYxlbbnphHH4r12Xl6pSwOsPC
asqxL6hAFRYzmxCFozfU+4nGTLpksCcnC2HavRVOj5QRNhfXDvevQE+qzBIuB5GaRawSJaaDeD64
zpEvtRUZqsz5P/zSGRKWHSDtondWdsBz5eX17Rc6MW97ZsKZxE4kuSKhrnQY69q0OdPMlEZo9srf
FXj2maJmZrHmYN2UrZ4rEcpNXpG6wFaTUKRunw3Xf7OFqeINY2zS4Lr9Wc302rqx8leUaU4tXjMj
dD0RDTpKHTHhMU4AY709YGsRo+/4vViD7mSyuZqyGoqoAtvczqjGI2On3plCPSsJCsO5HS2CmDkQ
5PaOpg1a5kdkQOMN+TAfr3+MhrLbl/Ifhu+qnNKhJK86r4sxlDIKx7np/5bao9kXz3DYV52ipQmP
jpEx0OF0ks8VjY3zI3EzeL65fjxtDJTeXSjVdo9Swp3Wq1v/rxwtVkZqzRvw6FJSkqFDAFZcuG5r
nJa9cN0IuYpZoSosXHY9lkQg76MYh7rn72+B6+EcZR55Ezbmv7rlDeokkST739+PVyRO4gPiXAq5
pTUCVOuYdj5MkV6gU4lpo56Rs0/dFJys/b7Y4xPh53KsG7WSPQxwIU6g2goTB9tuI9Mo7Nx3XSEO
Dqlx+3Q4eVfQpGL0Fxp+wObLrERQBA6vLK2OqUigS3DOviguz2jbROTRF9+6EC29IF3jZ20ZBqQD
5Y3dDzhScPKAO/xEdadt6SNJ1KNh+V7ep5p4duGeYQPWMNdL6I6bqrNi3GFu18z5pRd7Ro9zA5ez
C8pIYOEAlgEDyRxcHRyxCSgaZ/UBwWVBZhFjAxQe6OzV/Cedm9Oi5SbUy5YVs23NztAclkZ9xRW+
G9F9S1ZEmhB4CkNDrg50axChGyQy33K53fQ0rA+tPYir2r1BGW8mmYapxrKzAQ79JYeWpNgz9m9M
8FpbT5gS+XqfsFMHVApsBI0NTWkr+muYLgJYzxNizBy1mQTId/eb7n2JftDbTbcVMVFNHOXoW8Ro
5xEmkeFPpZUsdlsQRip1MItCr2xM8YDJ+JegLl0UoDGtUIR4Cz6ibLKvbJewqR6Vs4LDMsbZAoVK
5k/h5CibMOJA2z9WT746D5xvKJXws8slfJpO0CpsKvOCSq/Lo1TgjUTo8RroALulcqnmyljRGrpZ
j99jFhTgxyiqdK8hoAr7lRnOlCBrnCN7pDIe0KParNmmfeOq/t9e3V2awDwugqXYr7nkHldSKD/c
dNoFzi7Nnlp4ioYJqRd32pWwDHpxwiqFrY3SBLcQm8dJuAVmF5OhxS2OZq/QsRQ3BZSf/EA7tl0R
Z/2QXpwGneRq/J1JXjBd/qgwofLsMINPLXQoJuUh7/xBIpDeNsBwZdIEYvSrUCk/zu+mU13iDJXb
Ss1oTaAg1K8h1Ak9Q6jFmWml/rbQILK1n570QYKCmtDxdnGh2N4rIay2QF8+0pVSPDVZflczTtnZ
RX0C2aQfrfHTFfWjaQB/DhF75DHdtCzNHfi4Bja2RVDotiM2MdZ6njFGNKw4jb0QTgtMdrwDgy4f
xVrfiTX19QkfdU+NHlNJTn1SIptGPuF19R+Y/5exTksv9v1XI2QH9GDrFp/lBiACFTehR4NmcB7r
JCN1Y7thVpwF6yMC5AZ6klVF4Jja9IwHJWIRK6G8xkq7QCfLDsTsKAuL2fCxHc4YfLodPWpebsUY
FEwUTzjR+iGglHXt8H4FxPiYqSvZ1oJtqo1ipirtGpThxDedIW57sqBzBPlVS/rs2d61l2gQFJlJ
7pEBzjtzKYdk0dL6D5RHgPrrH5ByIGPOLPygcPLBM/RUHl/4rO4AQE64AwXcEA+GIlxx6pLirksq
t5TCAwWc0C0O9XdzPa/cfCb3G6c9rjB04Ep9MXGrSxU1A5aGsJSbH9R59uKzVcEpfFD/gge7jVRD
EZiXE0ZIpPWmJYTbHAe/lwDBXG4klhROl+P95J+ugNBVNGF0bLdzpw+uBp0v3I7tLnaNcCH+h8aG
GmVOlsMP2eU1yoDbCSPzeWCboF5OZyMjtQ4vZGEZ5uRYZuN+0wgLdDfQE57TY1rU1HJYHLc9FZMm
+juEOeJDiZ9K/plmSHt1vcQJYm8Lpq1X6bZTlXvtUAr5C7Kujo8Oeww6O6O8DXuuBItmij9hWwdg
OzTaAgGLWTM0WTe280hs8dP5YdeocbvpIHEXeOY4QhFy3lsJLUyvT0hkkdglQzR8JUYpx030xKyN
WdhUBUR5K63GKzCt4HmMEEcKnEHPjrx2ueQBB7xt3XcKf+QTw1Lb0k9hwRi3ZxIlSAys4/Ps013k
ZYw3kt5QmA/Xa2P81eaSvaYcxDPkau48iMdjtSZbmcYH8xVMs6DKmXpAT6ZCkgXBbDickWLrdxSf
/ifzmG7VdqNsM/jBK27bw7ilRaKC+fLE/WP+1CEG1MTo74fBuRIW8UIkL/w1Z1jYBTJVYp2868WR
OUB5PpDoDzq2g08hWJdjoDiSW/cvrHHmm5CcvjYcPYSltQgb8axNDZOzZkHHrW8G0S43Ql6kqk3C
JkoKtB47Z03ML8nSoX6zlCJoYYtaK89aufvT8fcj5thc/vWXntoRWDj0YXgYDrAU5c6OpiwjdJVY
rvz+HhbPB2LFilw9RvV0uErUm79jpMvBmtLSKqt4uYErvK0LwFcOO/7F3VuTp05LZeYMsl7E55H0
36gt8zyz7npDUwFrdEwykopP5ithpjVgMsRzYrCSWQgZmnHzSXuWoIIGfJan3eZnXB+COeBo6dYb
Pmp1q/2UiiR53dVLhQX3LqJHuQtFez86cH6e2st4suzJKMwZMp71PkP6ZlsLqEhG4BXVQPNd3PPs
AvGTB/OHTQBIxMQhlKf8zr8K9Ao+FegWlwSiZgMOvX/SF/IPLUPoPQMRUv/SnkG59N1dw9KMuT0h
c4l3JM/5nBneiTmZUSKrcvScEk9uIieTXGx7NO2CUnQFNzZ9SNnmxi9DjYosQ++asjSGaizrYBkL
BqGi3TcMCAqwiawDDpTT5xh3dRonua5bQCVROPVTBFVTtgEuvIHx+V2/TPrD09goXxT8yJik8L9x
AgdX60YqpLbykyk2oVGVkk3qG8Otm+H2jqLHYKd7UUxYqOvWkrT2AILBR4O2TT9UTLsHnyAi27dz
T1sZuLCZNDnltoj391hTWczvVCCf1kxan9CgXJ7cU1eg8T4n76JrUv33HdubFnr6p9NAdtMaa3bO
DYIyDaIk4hsFu4q6eaMtZv/X1trQxOdaOTOkW1aCYoHJEFKXXXMzLVYOMVGl9PSYD+1KyrtM1/bS
Ry4kcFn5wJ3FVRoFnd4D6p3kC2UaqedgUCSr4vODRKGcfZ1T4q35SbIjKRXnS/HeTrvpAEO8ievZ
ObLpJxj5T+4kWaUB6wa3A+3/L35PU4uwi6PveS+tZpycktiAmc67hAi378umylj0C0F/FIhwQF0R
FaN461sYyybnaolzQhU3hDY6Sys5y4CTxV5ZWdnMVV5R9ZIL4V9wbyUYSMBJq++/nWT4vo2pvSVl
LOrhOE9IDc51kPhNlaRZoebmpwutq/2HqSSt1TQLjr+zSCdArWCKjdBYX3PRRL6zieog45Jx/uc+
bQCY7EmMYpNaWx7tVrGQZ0JEJCrQMiCkLFvYT43D7/vVuC3LDnC+Imd4o9nR34Hu/NioN3lh1rLI
Ij242+7kve4LYRds3OMBsS449VvQ8p2zUeiHFWz+0TYS6dm+fnD82lxTGZj+SK4H9R6Krv2KnYuY
dxbV3pdTB6Q1NB+ntODt8v3TQk/mQ+Rkb72XLZaN9IQlL2BgMNvklq/WgRsPoKXJnRmCbqhGaeir
rSnnh6bFqph3WV9pY7gPCzbvQE8n/Lv0QXqhyRj56cQketQOkPAhQNAKUG2Pd0MnWD8u1THlm9gu
9eJZb7ySMdWCPzAPWxFUbMGmzhji6Fvz4QEAqpnTa5c1G4C38s6KLbJ9WrNvbTZp/L3MknWGAXK0
kvRrX3X7WXU8nHni41wehn4f0DpThfD4zBNed3dlxD7nWFyxlZqbJS0TR4xdXVMHfX3raZ1bUgbJ
CHLKEDYclafMH5VEkX+w8fJGw/HkHWfEDwUqgoJREXNaBvU1WrquPEavk3zDmMmjHq+l89HU4PIm
WEYyr/Ym+V07yLl3T0UQTArYUGD5wcYngJCKFA0M5ejS27TjBfOzB37uVxaUFsWlP/PmCr6A4L+I
QIpnRpaL1WuX+AdlvJ4rXHsyiF3HMehGXSl47ImyyZS7R8pTYGffhgDy1d/X0Nb37iDPMA75gaSG
Z7hp7bdYJz+S+NW410QeicEqekaWhKkQcGUsD2phSCZqkCcTewlcKSPDMzbmJ1f4DUkQmUSww0n9
Hrd8jBuHwg9WmsjaNE2oSjz4ImYBDRW2w1Mj/LwIWd/YHi+c7dtwgvTa2BLHcCMFq5jDOP/993wu
rwQTMbloLXVZ/FgXAA4M+48Gl8dIjm94f8wMnkdCPbePgH+baXm3jfoC7PVaeM7f6jG0teLPjc+h
qRDJT/PbKEky7/nZRECUlbg+Qmn07oousyhCfKA3FL5PpnhwHcoSodh+3Wu1Lhl55BfKXTqRsaEh
kpAQWjEHsZd70qNveRGkysnMI6tS22rM1QGSMLRN4iEy/PmapAChGUm8psmKbcal3ifxuUkT3sME
i1Yk/rYgc/nsehV/0yXXDDMxmnSuKMVs+S0MD8Edq8nm3Tn0nvyE41myiVt9DQgbTDoTasGgv1kO
zHO9ILP1Y/jebNcPibOHEcfryAoRI7gXfjtm5Rw/tVP33MyRwQbhlPA8qXTTyLVXzaTR5zOl7oEQ
vW2PeqJAUQHRVfEjukfHa5hKVVXNeR4IkLrpbvl94jVR81JtktNx3MfXLgrDSHpaWkGzAoSzfRR6
BIX4NGUlqv3nkGDtyBQG+beAvgsrP7SPF9tx+oClbTOWd/C4q/PM4weZsSYZIqm6/ny32WIqE4KS
GJcC7c4VVplgkqdlmSyq+KBK+5sVqhpI2m6ZPXDE4TKVgrguw3ObW4uUbsH+hT3nW0TSOXmy3n5v
D20esORULf9CjjB+WIwqb0tXGR7L4jWQY31aGSUqXyQJApoXOPSUWVdWK5pzJGfyfNqAjQTVm24y
Hm7CXIcoWm0TX0v4tOQzFYhxIDoqzDy7aHtzlGdep7pGNm7oz6bYOEIZXEeZHEqxLuriphL4oKCT
RR1yZMtqzun453I3YzJe3rKanMcoirAi3F6hp1SW5ocyggE/hz0zDul4gm+RtrQEgBkpsLUrw1oe
22S7il11dyVV/ic1qfYVsVhAzLOjrRnm0X/shubwzdMVFCRnHfJczzlWzFCtt8oO4Ft6mp3cRF5Z
FIm/rX8B6Vd8pcY2kz0TsCkomDH3Sa9CKZkySZ1T7ZxMcLnifeCkWairAvf6VJT0r2B+IM8YZte5
+sNgA6xmGJhNIExLAYk6UUnqzzeWVMYQBx3VpBNkB2KESjj5zvbjrmrOGQxzCDCE1GTgJtzmL+AE
TBCmZSz+03xE9Deeh6MUfrlfl2uDrwASGAaDWuTjp8p1XEYRTT2bTxuX4cHr+99DqDCKHDO2MmwN
5TkhpKyQfv0X3LFBdKAib0lpAW4hXQsdnn1JW3II0sJazn5uM6q6azh+4ZRj7wdm/M4aekHnIzkt
W8U60rV6X0T45SFDFWTSONjJW2SMJ2tIE7Iw99WWiSUZSXvMLdYpcUx5/sqM4NizEdPUrdqPCjfU
ANlQAJtQgAscmwUsMYMDXx2mehTokDDCZF/TyPXE25fzA74509F/WiYE9oVi25ECUPVXDLCrsyJF
yddLSdFiZbbZUYK/4f/NlUwqB53NyZLMOdTpSOcS9nu0MTi7OpvBKzrC1J8RXNe+FjGsgeaokFrD
Zav7pSM5lOxje9w/CHQv3q4pcHw4nMx898F2RFlo/inOE0zxFdzMjuVRR4wwZUNcRiDhagC7r7GS
RNMWe3IBbp13V4RA9gmKwUuBl926lEK0zFlX4QyVBmKifSTxdGqYcid25mNRLwqWl9BRKfD+gGHP
cSY4k1FpLfG2roEi23nSHtIQOVnb/OXB7VsZ+fDFaJ6dM+ipEnZ7yo+4RAMNFJGtlFSROQO/utoo
1m+dEenPQkUg1lbSGY/lExWGvJ7qHAcNXqyAmrEmRDLM+RRTFvK2EpyvTgMlW2xmdh6KKLHNwtBF
zJ1Oe788UwUVphz/esjCyuGveO+/B+MlGj7KN3F+IfIN9RScWQnmHNkNA+Q1yb6+f7QS9YdKM7OQ
tDZY6EQOX/TyEaAq1mDQAQSONMPk3UvhLim+TbGDqo8WhvHhXMBCJ4+OIYAKOc7oylSTXy60fZ70
6m6mnpzZ0Av3zsWsfI7bJk0kiuP9TwfTnct0jxzQ7CObTAI3da3oys43s1QJResyrMW4dGp+c2P0
GN7TGHXnseuYZeqDPb9476GZieiqgvA45YPF9nkMjB5u+O3P6PNwXCIAGC4I5fjVCqrutocQh/+r
Pm/hMj6UILTe7nC0wcOdT9pdcY9FtUtba/X/3dZWFi9ihWzPdEPF8Fw9b50cDvojwFvwVstGbykT
WqQ1rOZvXNEfT+ezDHqQlJMrtVPz+XF2jw8H3FZ13MNeWbcz+DswO9iZLFgxdKIYq5xXJGjIJaAb
9R2VQxSq7OWxlKJo9cRA7yqXnk6ovBS5h0IZbHGyEx988j9kgtOh7xTdDZtjCFbbBmsARmQhDZKv
NtpHUTgQ+WFXZlKy9l5LVVx4tm1Nd1u5R1ABZSZVhBMXZ438BMMpfrJKcxjMGmtc/+MtiDexscDq
QdgEhy3zFouBWWbvpm6I8heGtFdAg/x9yo0I+sabWFu0juKvnuTgPGRK3tlmlzRlQnANzz1RHn5M
Jw/jFHnhVGyluq7NEp8KcQIpeImxFz1immuBIfQxugyqZyqVh/Pt+HZenzoKzYaNUWh5gfu+migO
xQBy6HHle6EEGkrOs35+O2iInD9DonfWkS/knzm8y61ok1P+rP3nXDgTGClfGk7s1WbctVu/MEEH
gF+yNzrOVe9hnd0A6ea6qi6G7/eDOJH7SPXCkqMjxttxwEfHp7SduZ3tICL3OaeRWsEM6i3l3CzF
hnOZcF4pF0iFiv64JRYaiJ9HSGT/iQX9+WqqtLoUstmagvnB6V14rDYCfuf2FE9N0RJtgwnwjlx7
Y3FjpiTRYHpWsw7U19Pp90TKAN52Hlbxz8AYO1BWrzL/ZMU23uDKMzg6qVReGH/Tk6hdMak2thJZ
6bqxtrrlcN3+19ZVMtgig+R83GyrUKzZL3FvmB6ExPAxjCX0WOt2jxcnsw51gy0lHvhNaKE2wicp
pFvFol1F7eT3w+4TPeFd+vxrwDIwrA2uE8KyROVBR8RSIJhd5ag6d+AT/ROKg/MDO+kgwmzCy8s6
Hunxpb9bS/PuiAQI0NWWqDs17525Xx5og7NnEYYllhUPkyO899YiF5ulQSajp4i5Xkc++8bWY1UA
eS6E0Wdqnf2GtNFpoO64w8czkd2P37DCEJnL0FyMFBJJ/JGIiGC2htNb+HJmjJsSuA68e4roPlms
FW/0f+TH05cLSuxCKxyU3Qrp69XYgE/ef0tEhhdGNKwJCzOne8nw7XwsHdubyYFlPi1Uqkn+9cav
yWbUtEvX7GTYaOx2urHbK0Gkyu1SL/RiBvJUkCua0PmuuzXQga6ykMOtQHLpyOGOUs6IXQiLkfBt
o3OLwy54xBNeg0owhOQszkgUyUY7P75txdXAGULpfri/GlgrzHdtnSlg9Xk1CHJG0rB2MMcB/jTM
W4kWfWvmKWpnYjzN3wZC1tp5zy5D7jKrjNYi88a2IHy7XfkO7NMgDI1dMfzEdDO3nUJWLIBexoed
daweRSAKIf0jTFVf0jcUmbXnPxN4Pd6pv+FxityZpFc04+eDgRQzGHXF8Edy4VrtWdF6lgYsecYx
Tv7m8fkKfetUGpKM335hutVKsyfoYKy/I8NfxfVXrfwcV7arBQw0aN8g/kxa941Pji7F7pMEE6bx
B5OqvSzeBcI0c8g24Dwz+IOgU2hBUW5zvDfCkIfRmmaBA4LhY0ZPJdV3fMrNiEkhia+9XJf6n7qw
8m6uDbQk0QIGyakl3MTYt56/0SD0Tzy0A3ajNMbSYY1c5s05xaNykbbpRRyGxT5ZzbfQontu1PnD
SRV1ox1vBsIPF3yF2uuJqj2dqvqkOV8g4yT9YIeE3x1MR6KP4XRc5ZAfkIMUszxFSahNv2k5xJrA
PM5NnSowuf2UTg9DQRzJie46jJwDebYiQU/w4M9JVJYRCncvS46R3/uE4dOqHpH3Jc4+PbYm2vkX
K4hmHl9YfQnVMsV13qyaIJBVbLWvh+r6NklOtkbQHFZvRgWwY/LkO5wdMWHMme5qLuQJmvfO9r4b
TALU8yEIe6C9SGgmqcniO9CQq+S8FgxQSEZflbeeWKluCk6QN6HBcskC7OBvdsBqAHrnvcms9bQS
1Gj0IxJdmGfQkxQNP9hiX4KZOy4hoGb7IkUVQ1AdpVQhJdFfoc5Cl+OEDU1GNYpADKdLNjdB70m3
b0aBSnP5IJDI+GHx50kbOpR71Gxx7+AIEs/Jde74tdJxJirlpBq6+BKGI3zxG+vxswtfyRvWGEWN
R3OOyjvOMwWYQrvUjmi28CKYgXSlyPfs7E1jgzo3nLbuGgHQdyc868yHfl6x43SLW+c8O2cBl1iZ
lAspVvlIieBVEjWljdu1nC+blONAR9Ojr9IZ7mc9rZMJAHGTejKuH364XorRCL+M32zHZO9VmF4K
5vJ8P3UKMS1UrMB0FFoLW98/AAqUKvJz+Z8uQpmHeE1aUKDuwRhwwqyPcMoRhjyvRfSB5kQuV9LT
6dA5SoWIrhQYAOBWu9uyZYyDEUSYpg3M5uJzs6U0FUMr1tUIfIMkc8sl+OKNZHTBCI3I59RI5S2e
cS5/+GVkaKSeV/rGxrNajsgEIKD0JfTmlzG8OwLaK40Gw2KJLiVmM9KPmn1YutZz3LaXFxoWMWJD
EwTL/fJjD5zEtf/94IhyX+LNzixZI6K5fVZCT+5wvt56BB0/fScdedqUPnTQTNeG1A4NFMXe0L3r
z9klzwqTDE3HAM45RPqTua3DfMsQAtXwfbpgAtio8hiVQDJEacCsxKpEYYCzBvT/EYmSSAdMhdzM
/rQG61+eQZQYQjsBxzFHJPTO5vF4QpWW5X78+SQqc8g6Kt0/kW7Bfe+zVa2ZV1GDqmRqZJD35M+Q
zB9EheKUoVt+fpKU82RXVuTzWx1n+MIaAuBicz/xa3Kakqfg3t2vrSBzO66zMPUpTEyVZRohdj2T
haMU1jJX44lK0F2/vdyIqlhDxILoPJSTSshpQedHexmcCxYLjhnkZ9xagNR65nPbMjftdUg9K+hh
XHBn+/v+oh2OpvPJkiSQDJjxCV+blVrtUFWg/FAIOlSi9SicyHtQijVBSyoO8z1AqY1bX9O5t5J9
thlRapxS55qVzypk41gvvwzPaiiGIU64r292DXWydzZ75VtzMnwV5P+WW2+41Dc+oSkcnmUQNsU+
x1iP8xWVG9fJ/JRf6QMFDDqeEBY3fVa0CxBSkB8teTsy6gqO0c9JgmjCcsmWcKUdRQozeASRDGTY
Ll1VQNYV540R4HjnwyhXMelGHeoS66R7o2+W71sNgBttkDOKkVNXOKyxpmfsuNNoGlb8cukR/lYj
Ok859WEzm1+SPKrfhaa5SjRFQs4/dDnNm8R3Oudf20eiZXB+GyfZSZ20kxs7A2B0UAoSGVmjXln8
5JbfVhSULLGWWlYQPrFF0WRdSqHhzZRTysni+Wdv6J9xuaCBMuaVfVHxx3/a/swsuhMayHFVPqdw
aFLk9tU3fAWz4RqXMkp8uQSt+tkSBnkTb3sdLCJknrYTRB7aHQmEH8gXLUyCzwcCRW8Qygti6LIN
R3ERYthXCQZnoqis1373/8v5NKaNzbpGsBavoMYCl6yEem6al+hYd6VadT8dVs1HelR0/yPK1FDC
y1eiwxXGUrd5BR2E2mA9WpUeaPdxnjHgDtxyqWO0iqjanGmuM80UiHDvtv3I+FSn+LaBBQ3yUZ7K
QwX5KNzjabKrP+FopzcaCYefRkFjUdXUmyXxp2fwuDHEOe5/1a3LAv+IY3HaLRXWJ5FaDkCOCCVM
iCecJtSgyQ7rEEVE4WqMrSqCJSQ/ScGEKvHPXAjIFK/2rMgvkEEW+1HNVgPqb4D5laS7o54MMLoz
D++pvX6djKRRt+N0VqGmKjlKn4Z8znmwgod77K3vPfgy8YeWIv68gNdmYoURZi0Oij3Wx7+j+sng
7PKUHCkRmZnL5IAFdceia5wnpLWiwpQfHpvxOn0PG9FT89Gg84w/UETGX1ZDMufIusi6E//Roxhd
rqaATrKJTk0YabQYU39W+LsCwHq6mhEgAALRgF6rZ9YVo1IZ6IR+Gp2MXI9+9uLhKXuUio3nfc4k
08HQwMGsIsQcQlt4RrVOItjIPS1fJZKsazK4WKLDEoAZwN8lLYEpTEHKo7y7CyKtsF8wYvTy7R3y
8GVKvxiU0me4HKOYgE1Y7Jg56EZwLQEksYQ3q9pZpQsDwpWjZt29lXFH2aqNPWbharoFvnNTY7RA
/bAN0ezT/p33OuIhUAUtNc3AccaSN82oVhR2/hDybzvdZ1Sg4sZy77fn5wh1iAfQtekhqroLaLzh
bz3GFuroeJAFU7ibCBG5c3gRec7QR8PXjeWFwcllsyoNnfQizvKPq4YXsRLX2rqDDB41qdokk++B
WOM40rbd1Gvtn8+6BYdiRSK7PJZi6ZIe+6f5LUl8cA13s63LsYoRp97UKaHkTkzWs4uvJH6m5sBO
00jQxb1rrrecJX4XhkKt1gM7r+AelOhsZSwDRryQXGdC7nies711Nud2reYkaFt0iuXZmgaAhEGO
b3mMHQf+3MWWW3Ax7f9e2UtEuaYahX2sE27NcHj1tjc4fbBJER8RTHbZJwJifHORbZv0VoIdSpir
ru2N68/wreK0otf/TGdoxtEDnnY1MGE4Ntwf2LDLlX+At6ssWihMGzvwOdGWCGHiAUykj/TalPCN
M8TJzLyyAZ4YNymkPl8+FC52uHEtF84GMAvpbTNvb2f6hwJ1Eu0qQMd1qSokrth61x48LLyiu4OK
TzOFOUA70kmdNekZVOUW/7TAPWoxDByruRcxxkBpnhdcI2KuIA9YB3rhr2xz3g/Z/2r9cnAtyaXX
rkyN9bbTsisy19zBklzu6QyFr5ZFXUbtab2PhTBWL1irQPHsHWziBMiBuelft61+98SKNFJ50865
mFdM4XzdjTS1mwc2LYzF+YnDkIdh5w9b009vbTg7jBLYvJwlIIt2awb9T4AsRJP3HDnqnf1+rYAm
6Bkj4T4rb2jvDwjeUHrWuWOHQTv4Vlppkkj663+nyhJ1/DEJNpBbqRNe8p133ypSR3gP+QLNymw4
eZkodrN7P71FNSnnis2C5dE8Vzs7awh8V8C7B0tBdIPTciznPoEx4/6n/RpE2zjarmcXRsBKPjan
bk5e9Qx9+ljny7TmQlNCJbsNJdrffGNyyDYRDfygXhn+d3jlR/zFC2Sd95jz3sGKKI5BQtS32ieo
JZiMYQHiG7rktiKKNM0hhrfWVJq02aoKcmGWNZLjIXntBlIzs781ADmnrlNq0q92ITakRAegoUrO
fJ/QrDlDl8woEYYzyImKPtHdkO8DvUUi3NNGgW4pAJbPhMzCY0ke075s0b0rnQuUTVtzBWAepZtQ
sR8MuR3tvboyRrH9c+4dfo29orFB4B8ByN33Z+bJH2LQ3FWUfCm3uxXMMKG82PXIb0+Vms/Pe38J
rOdl3k9CwHyC+XY/78HfLTU+Uxf5hS7ZUmdkN77aVc/Hban2i0TVHsDkzKRL5QNjcf9iZ2HiU4vk
7arxqjV0Gv/rmncTRqpzsS2vpYc8rusKuOrTn1HZqdHy83+U6CeW/BYqtQceQwcTOexaq54f99yi
na2A3tsGBt9+M/ZyYHOL97SdFJCtWW2lbJR5gVocDhzz0q0X1dy4jfs5jhmjRul0YhSJW9nJZNhE
jdrPDu3A9v2Nr5DfVA7T4+uVPkgeN6THEKHkVcG9MoO6pE7nZlJ75stE8DDsQjJi7f+tW78eJlH5
dwAhe4fto8quq61m0q8pg/eQf2eQCzLG8m9KB5VVqYqETI54aRII0VCnAC8xj8wigiNbJOGP/Zkt
G8/d0ygtCYnIStrBJ5UIgMkB7Fbm5c4VnnwfieNPO8bjzlMcao+Uj+J77mp9o4UGZwgyy28BkL97
C2jUeUQ29fP8HYWaXxlIwGeq95EHQDZtkGDNpp5OmMUQoLYK3wCWDSm9mzWAolI00NT4oQ0+TrRV
7D6vYBB1QzbvwJsEKXWKjCeNBjqOiXHNkLeVgI0xQa0LA481VBV6eObwMYjAhKLJ/lg/aoumS8mN
2qeeZj5tXlJWWkWHXj8vobOdOkTpVl2Q41RwWXki5VbTOztO4Mfj7rOQV/c+kpDD7T2AHwiWQwDc
LT7aeX3ifCbHBtjFT0/khNeutajAlVV1D0svU3qpKeOvC7JqhsUvFUzUrtXCrfxxzaDmYKTfhljm
ZEOW1UpqOZ2KT3EGsrkXgofMi+hoNaYzKZAoamXr7ctVWCdqKHSSeEIeYWCnhhnGZreUmXqzrN6G
gwZvcy5vIKfxrFQjlgSGFUioejXTt3OBN/1kwzveDeKlwZNvrJTWyt68EgpY3SJ50BOOCLwr/O1X
9m1XSp3Q5V1LeWOPD0+LM3ivnAfSYQpJcIydK09sacyupZCBhx3yIS+dGI0tb3em5kRIdhUc6cIf
tlPiwBoJgBYopal6hjT+kBPG7G8/bwvmkasbQJlMO2wDJlrfkX6yYwlxCVXteS4fV858Ht7fmmZ+
X2E92iOwdsLGgkHwxd8ffYFYWXuEz4y4gtmt/a5pMbNBS7ZVzInq7jsJXotXHPzoF1oDrkP1Frmv
m+OhMoq4NYJTMZEMYJdArFQFO3lgtdHWXipE9qZ+2UTZO/irq262UgVTViye3e8q995pO/54TZFh
qLl47qNXPcKg2J9WCrbr73vDYAfzSrqA6MNCbQ/TGVfpxQPsA+yPJ6p3pd3NtRVjUu8BhKK5mBCg
erruourXuXEmZ1euzUu2fWg4Z0w15a3cKGsOaTGzdrWmJZmBvzJXwi5rWWQ7dM9zKGlr43CgZVP1
b/SQdAFF8peyYjA7xtmiF2wnAri61Rz1iqTs0x+ctG/P3Oddj3wtf5mVw5wuG+OPrDVvKdBZiSL8
S+dTgGEGwj7CUe+4wy1AtSzYEMvZzfjJGkbQVlQc2z+5RJ6jSv4xcHFh0GvrgcDAF42BafVsJvRX
OarchhxeMITsRG04R+WGBAdmes+CEGZM8fGgXksiC119Pzu2rQXbFimuc0GOF0nrdZLPt392qPmo
xGoo0zhbI8RUa0HCvioyHPeVoRNxGDoe4MPlR4s9L/ekJueRcbBWWlz7yuVcsDjHwquSrlyyiVDv
pe1trb6OoKZWIVCmpXhNs8bx0MGUhr0YRcpy5qwkmdLH2IIoXj2rwkORFGEPSgY2hsgujoU4WVvR
m1x3ZzzQfODH2hfP8FbID4TH1B4EoN3na+RffWHFVYl6G3SBqkeHTKmhNhSq+gXN2odIwJ1vin1A
tZTlXh0pHs7uY3F+s8mYk4RCokEMLwnC6txkJGFdGR/Pm3MY51fq+8494CHLbK/2pNDzAmthGVXY
qp23sfknvVj1Xkws5Gr0o6gqONsVXyTIVwMf0NVvdTKDSm69e2KAjbcLkPnPU6ENkFuJm9xM01D7
hvzAhXjg1qiLJekghguctXDFxUa9SmV9LPa5W0ruPeq1GLEuogQfAT+7usfyDHPhuEUR49gExztQ
kg1117lWONO6+Irz8Cb8Lfenra9HtcatEEyMVtmq9+h4oLN4lZIbMoGqMwR+vlUHjqGHRFv5Ngc/
Dje/DSOi2SUYs3YEGMQ+JQScXz3tAh0oIXUxQ4Q0TDkJSPdFCTIfQ3O7z/97DAT++KsQiVIHFp2q
8LJzQ58S2MmmxGGajMS0z+BygopgAAFDjWLcubRSIL31Pa1BAxQE6Nlsfargy1mhe7bDNDRTdBKY
CPMn+hhBr1pxYiibbLiwQPpnhB8+a08TZCw6wFAhJhG8emuHJsjsVKVZRO68swxY4IezJ+aWCkPq
idREVp6hs1oDabRwal5rNNRiRaKUccJNwk1srNRXV59v+NqgYkBOfBfV5Z82Mr1RJryTqXs/d1mP
O2KL0xBw7p4Tx1fMpxZGSIs2STFROEEljRS3P+fnaTBUtB51aUh7rN4uJp2ABCBRvZKPZLpCtdpK
/qUa5eCWbOIf1K6+ZreQKYX0X55ywErJGirllL8gKCLB1gurJDoIWn4CZF0GG3g9G0zP9F510Za+
MXxm+nkbQywb3D/o+5ve2ETrbXkDOVWArymRPtMrqgu1ln3tCdy/xy39BvjN5fE4N2pRwTBrPspb
yE32FUfR0v6KO4cE1aQDO6/DBriJIsPnoIB0p1mAm41Y8r431zTl14xgZVr4WnJT7f+8cj4/4+cQ
qN9WNERrNugnGYygL799Jp+mw8KxyN+NsGoL0H3fthwjTZ13yei5fg/Orc7PVc/A4fLXL11v08Nx
PkyT/zPaP2T/XqWJAljyikxrWEpVMnVcjnP7AaJe/ui2HPm38zXpSvn7pUfUxBt+d0YAQ/tDeS8/
Kx/L3pQhPmlAExzLKoJUIis4h6TdgdZO3ljPKkw15fCkNs/fieFBPpBTl7k6u+1tIq+vYxCVjjhA
WbK78C63C7XDaxSrvvEX2/q/9XI1GB0k7EkcflNkhywiyGfhf+ot2P7fNg7zb20b13MkSjwKAQh3
Lw2IDqWuQm1RHRB+LsAC69znGaZ9wi+BBJe8h3O/BI2o4Toc0p7+s6x7zs00gsKv+aksu7+oH1p+
W4A3M+ShtufqcN9tVbDhUOYiHdvWbYwmCUEDv6QH6dcUfe2qgNVtMuzBm1TFH2AEi/bWixeCE/z4
zTKlnq3cAZZpVdtYu/KBvSu2WFV5QQZEqK/MGV4EdGudZWygDEnOw8t/VmHF3ESszeosLAAlnppj
RzGOy3cgmaaqg9NdkV8/aT57vIpXCZemOn6f8lBPUN2ZZJbdf9PFvKgE5VQnDpmDrQp2kLppn47j
gJCZ2tQ8gBMRTbBqX1J6DTL+zj/2KTQw4SME2DUP3aE4z+JZybLk2QaerqUZPE2L2u/rO17rK8AO
kxMzRpNqHIqrNf3vY3F6VOD3fB2hJigG6GoV8rcccS3KRX6/J4AwuyefqMHBX9AlHN74XXnJDFX2
cDsJwId7AN3ecZREO4b51P532i5dB+V2E4OofmGPXsSu3i1k3vK6yHsnM4PXaXvKdlbi1PbnzTvZ
E3mJ7u6CCjpoGK3LAgQW14tA93C7Y9NgYblMi7vj8Axfa7kKjxIcFfWDLopPbRrJ4xOha41pSLLk
hSl+6iwHuK1RgO5V4Odr6cvKXBbjcHkw23+VH9gGChYEHwG+UseOHqBbV3TLHsvJmKqS96lvN2Ve
aiyOOqrcLiEH7aifCQqYEV4COO72uIeGQEs4xJbMf4lhQIssVPfhTTTsnedt1bYFNI89dKDeIN6l
cdf9K0XHiKnN7QvKpax/dFJ3oOlHDrcA0juVQfEU/1VHl1KNKMO9+1LrJxpN4kN1v8i4zh57Q4rj
0x85zlxgfo8MT2ytVdHGm9uFAk8/8Xx6eulAARVKKczad42/N+mArWdp0VmniZ159QDqVdQ/wt0x
mMSUVR/n4afFaFur7xg5K390OxO4ioUfuFW1FwMi/8Eqsf9sr2pyuKv3H2LMWp7ccPsPRLHmDUQP
Or+7sN4inwdSo7qbJKhYQja0i1VAD7dohjwi7dKCJ3tpbTjY/oliYXa1ThDmanH/7msIWMowGDuf
TYnSVLC5eo1e3tkoRuXHDI6RzktBwopLdd/QdvKmz7S2zfX3gR5mStE4grJJ4CJMg4xFH6Auxsyl
9ggQXhKRQ0hmRc8K4tz3NQZg0hcKUhPXnwNv52ZUcJBO84LeLz9dTQWyUirD0pq2I53syAs1NXOC
+xPu0TtQS5NzqOGLdHsyf7kCqoq/tVKNGjxqdNj4A0aqP9lVic0dkP7V7KnzkbSckBQtt+gycsqX
uRbx/Yu/JPYkZOg7a8zoL9skWT3XL+jH+5oTj2RjL0UosXCkZLNgZ4FQTZdzjQ65AMSAYnwkhX94
Wd4HniZnG59eRuIoA3DoZCR8GetQwwdsxZj4Ky9GUFiN6D9Z0ITgkzW5MikHEjp9n/kVb2cB85ii
VqC5gON0Ns1pSIr5UrkJREbNR2ILNrNQ6TQGSVYUtdj5HCC6FQtRf/DAGvo0hg2rCcTkHMuu1aSq
ouGRV9ndTnjzDThhvdQspXlc1R/W/OnqOHJ89aL68LF+gbgCrZNdUs15PKP+A8JHAfENh2jngjm1
PbWcNizbCk37QDxAWJOij3mebEAdK+YpNxwF+Q3AWhpklLxZ4AyMFAQCWP1ZGJpjiIr8xp2UHPz7
JxcLdlvRRzetfnc4YjaEVJ39ZLz5veg7ep2xyWcgP3Xp3tz/jfuY4u3FubZ+VCZuOk/ya+WPfiMb
CMRLZhZWmj3hKsdRqXbdNweocV8JSMP0yhGpfJY2HY8x0hLKvL8y/BMxTm2F7nNKrpeNCBIHbdSq
vD8o1ZoFmdK6pxNttJl6uvgBEaxAHjzAFFIAMONu6z4UrCqye+zoK1NIAz/s7GUhl08dCyI6T4h2
E0c0p8jsBqbcCJIz1ctnx8UvLMSQaXAupleCHdTJHIidSefR2dJpA4iH6ZP+giKEl3r4KNY2PIX0
rGIx24kL+ntfrrP2UyvqBWPbIjd4jum1+REmGOBzywYJSdtcsg3LsxANsBfdT4PyI6DuzZmN9r3i
VWQnIZgvSXR+bP/PZJ/cuhqJTsP5RGRl6WNB+iwMeTHgYUwG85FJIZnaXdVtRBJ07bezAXi7fBFb
bS03SfpkeeMr9zhtHFBG8AzUKRD6Qjjnq0b3tUHUVpts9ZEK/THhivI9yAO4vcnTIZH4vbTKqfEp
ZtOViDsG4oacgWJJiZic/gJDMtrLIHnNUTnrOGKjbLudPfC02qNfW05+3Aw+2Vc6EuQxuk3nq0TU
t3XKm8t/UzbtaA9HRq1RVjHzyFSHCGsDCmt2HbLHrlrxV9+fqjLcJ53ABsz0M/5qgtoPegKXhVGf
+jKOKygyoeRqOfBNF5kX1EJLOxKY4amqyVT9JbljlWjXsxeP2tfj0wFn2H51m9ejOTYeg2ExhpCj
zsuFVN9FrfQtChHVdcit3Z50qrYhBTw0BWcQNlhreeBP3eY1cnLqAA59ryggZ1gbGIArGD/UU3Co
xEV15GXxBxWVsAnDQJOC4dFo+BvynDXsJ2tBC+X9Tq33KysuRJjSUcstUoUNaKL/jKXrTyuNQ0PQ
pUTK5X5UCa53mXBRdGc6nNKyegIsn5Hf4nswEnBlOpD86XhyI3f8OJ3wj9dRdgD4kSLXRkQ7I2wz
mOJZ5dlqalox60wxyPKN9oT5q2oNCtckBrfNnJYs/FvTBdWbfwWJRgZ8lo9jxL8dsBHk/ygHPOQA
3JIEBtItvFch/6p+ETssR+5h+PsCke5EIdJ6C8YNkYF8UZrX1UK7KfgCGdJzrh1PonN3z9892DQw
lHr7XHj0wfPW8d2rLncznfqhPXZRplQvLYvoCztMr4BykkJePEjVCa5Sqmw0WzVAnAK9xksYiBBb
+R3EzTcsOcFCA6yfCAC8pJdZr2BmzFV9jUtq6JN6u1Bv7btolQr4ze4Bk5QJg3nIl6au9OD47RIX
ec4XjIgXFEiS6YBq7o6f9tb9APRQYerpYbMCB/X8ctfrR/rJSJsv9XOVI7idfc497jnXdBDjYiAi
fkK+4b5KAdk2UM+Q+pcxaw3liKlPlnGAdwHmFd/XV7qwaC2/LervnrFI64jQk1xPrX88rCbSRfN8
2sStzBwtqNqx19EF2U25RUYJhSv2YwEnGolbXxYAfc73LkJpXwnu9UG3TBKR94/7v+1RNTgbCmGz
M3ayCRU1W+ECqLNgLGwrcAfQ6VwvTT7u1pMFD+DGEPqPBNVf9J8Jsgq+meSIbRT16VnpdUL8raKz
TV3PAyHQvIJ49r0oiAXOWVnznzmsY+HOsf4dfn6/Kz3cXqu8XvexgHUeyGJhfcWgqWfhj8zFRo1i
bjUoHbtv/bylwOQE96Gn+crCKm940IjuJ4gl3vKALJ2axHOmpfTkMQD6Qye46H9/WL5TK5Fzneuk
lYQCeqQcpWErWYiLHTnDVqM9GUiIIsWSxG+EOio3er1COSvY0BmfcmvPnq152KUI4CKnjviUi6v5
ovWveVxRqtcA88C25ydplCfiphBghzB25lre/ljcKys11yWFhjKfAbRGTj9LDAJNZ7YSwho1gq93
6ezWkLhLRXYatbIQlrHzJUyNHUyB64+wlogDTTA4PknqUBDbtMnlwD1D8wWz55dlNnglYtykDzAW
6d5E5UdEwjMDkCFlQ2Dx8flhwPL03L9ekcPIY4x+JHbD5jAm4k1NYHT9LdxrVcW3+x903rjEv71T
UH/ZhidY7BUS1SdzKct8zvPg/ANzc6ZMeRkpM/uuWPtEDsHdU+w55mtgySFdQhXCrfYvizUpWbh8
NHTSMveuubb6sz+J363DwFllib8Qi66j3PPVOaW/uYjF6zzEPu1u/0r3WNV2aN4vsN5YS0Cu9B9k
QQwpDES9RZRAebaZ7mgKjcp6jkDaXSFhvxiB3jGHQ+LRssIoKDbDo0KLpzjL7uK2lZJaojqP4CUj
xss4RU7r4F0WHa0QOF1w81OylubmBwQ7hxv8NFIe/fy/LE+DjCkghSeYIzBvbKpOKqkzQI9RQ3od
UMxU5u2AhhcmgXi8fyzK3Yh4K4XbHDohjPt+lfglgJkYyfQ5LqIwH8FpsdUZRnLs+tsiq3TGeXZT
4XbFOFz7VTkJqzn7AMiIbfrI/nN9UR4/KCQq1qBIIVzrc7JwqU/+RnmvSF21vFOUVEo928aUjZq3
IGGLqAYohLigWS9pxDttUFMvhNaMtVeSLsUmXHwfkqPSqBwlSl3RCWo2i6mY0jDL6MFPHHpMJW98
une8IqzHgaHCtFdwteC/FudOCJc6WpmAGN1ckZBaJ3tZALIgqqdXS1YJRl28wrOU650ObeTQKAiV
26qsVRJ7MUp/63uMhaM+vjjUCpS5PpgEmvw5vtt1loQHfURl+MiSByc8ShpuF2cyN0J9hM0Y5m33
oP8Nt2H9rntVQz6sAfmuwAen3StfGw/iuJGmDDIyT0vK4v0r317Jaj8q8fS7SL5ITtyy4HrhK5Be
Xk1qy1KOpcnB24+LuWwcL1rn7AXRPWEsZ3Y/QUTp9OOaswNfumkZe0itZITTy0lYvnO08cfFGLsH
ilo8igKe2RwSSoJHJQI3Jbc0CwSoDs++cwJqEj0jAxf7PLpxbaW43IRzI4eUCSdKhiMCzcwZHwNJ
MUbDZiELl45PMRmgNpk40Xm0MN6WlRRCPnHJQs63aD/+Sv8wSyNPA2H35ZeiUZwsDvfrV8hLcjxp
ouqY3E61JAfaG5RwO67dS43rCD6nSCZMik8B3eMYHf7FYj3JG/vskabFYLnz/+S50hL5IoBjJFWb
0THw9Zm+2coSrL4mVlemKCakzL3U4MzGDNwqIUFEhuSl4gYhtcUz14Jor4U3DV92J6ehaxuqiOmL
+GdX0AQBnTYSsSCcyz9Ux4g0AJL+ibnqrCprgkjM1lUfIXgtTq1MERGq6kNrcL3B2cOMUXftbC7L
bAEHY16VtUBYXKP7Vtlhs6e4ACe4+2X0EVaql1862jlnIO2QAKFKQ6ezslQzIjbFJTkKtulmybix
WadDGT8uHtACoZjIHfbLi8J+uVAjzxtkXYXl/oSTJpUW1YrSRhi49M0MCcS8wnkGaws1b+AscldY
tZ5LhtdYG2vPRebs5JVyZSX5FCjCRao/Nua3twqut3DyC9IwQ2PvJ3BrmOVZsN8aurMq01Z7TAHJ
+kR3jq/iBb/fpq7EbBUwSgxeNBrxzqFS2+CmVaAELqZZhanWR0ataulzHDA7F7qGv8LsOUxj3al1
/00A2f9N6ipPv2j75g8zMyZWXlvARzS8b8q4o5kB3U5zdHetuisg/Siblk96ZtWxYLDxd4SsasIJ
3SIJrAcSvCJbScSdssF2RSvUiyYVgYUTPloxcRygT442yF7AAwGRPE6bC18J62IxI6POZUSe6nps
DuIs8VBLsfDij3grrgCzzSpY7uvcyi2oIoulYgQr6uPgxtU/sIS+ETbaVtHCYU5cU1C/LHLvqmM3
2r40NWrvr/Rw6TNDw07gWQT1ekFO9rsMGR/qxPXwvvaBEIxC8/ySxFDPsK7w18b8pJV2XuIHEbK7
WG3W7IUPYVx/ARIciPQ88Cx0+1chLvvI4D3ekquuflcQPaskJ6C92vmLlC8R3z82EyMAOClopJWv
fD1aFVPtTOEXb5N6N0yYnMbWr24UsirpU12CS4ly5ZZ78jTXiTATyGjLO+G2CJHw3kZTvs1p77EL
j2UxUJ+J0cs9pCkW9CfdEDYJPk+YXiG5X/ra3Wk7CE7DOQP76I1HWZfA2xRRnYuG/La0lFQuGS9S
uCBYiJ2ciQYSsMFflP/1qTrK3MFnN8RErN1ybDGZnDcJo9L3czDwQwR79auFm+FOOLn7zxWOmkNF
3CNCq1EZeBAnXdDK038G/F9hJd7YMVMDR/a5+rB/Pp37OxtTKANjonu/ViSumXgAbJCtf5VAVRY4
c1dTnu/1h9mTIUHpB/x/FeNeacf9cTcG0LRxEgqKIIH9DRNqTJnTMwmPFZtf3w+7bS4L1nR4VlqC
bsUdQUzW+K0XufOplAV2PI8mAJrJEij2dUxEbEF/4Cx0oRZwt3s2vrXl3HvqO4X252mAEnXSJ8N0
F2vsxUPD1pbmp3Gn7x91/eVSIWnwf8zA+nmwI1zx7/BrgDNc9ksTN8F80b5KO1ccBV7c2R4c9Hsy
meJmHd807AjB5lpWFFbP0NUA72owS0octIVpCS2mAhuY85UfnOYG5ATqZ7rm0p9Gd2ZYsRgekrMg
BoyY0HyjsuJ/p1l9mGdQM4O2LRYBfcrPdJiGfikM2F/alTA7sRSZ1Qc76P3tKF8mrBC1w6E/n/Yo
xZV3mTx38ofTmzm5sV9Adz9pFo9oYhxr21ZoYHrUKclyl/Qjl1GGJgBwTipDTdk9hHAxl0MGqoQi
qSRNxJuaL+IPpyw+T2HyHhrLKWFqGXQFIpmqoAS6AzgLFT+s99AigKMIkHWhTBXKm8gcySl9T7t0
ZrOhy2yPP8r0RMA7VoxekwaHbnePn2KosN2+twxSP8fbIaAlV9AFS9XzEmNfaDyQdgUcycL20TkH
Geko0QUCLvPD3V7QSy+h1VPMZM5mGcpNea0RUJqVJbxa9D8Y6RUG98K5SRHHsBJZbsptYMVkTT6T
OjXoDLjXonwMUBXbfds9Fh8OxPUdpsDjh8tbMa8XpzuvlRlKqLVdLLz+64Ld2hvYrIcWWWCa8zME
Vo56hp2dgyM0Ujv2Lz120Rio0K2fJIj6h4/fvomZ+lVJRyhp0rRh5szo+IWFsK/CcZBJBZfkGca7
60+PyEA7G9hEEPcpCktC23inhE4mc879B4oioEly++k4T6WnQo065L1ZxZon1R4AwwFqY4Tuxypl
hv2IpPN1mzp5bghF4KJ43W9y1l4/v6udgMs/TBWTUwL8BzluvjOVMhynHu8qO7S5Vs4EeCTstxDa
y21O152r/yUqGVVbJhrdX6HBCRyWm/hRAs9XzQjTrAtDlbUf+Oro/PghKfXV3arlprVvDGAF6zd+
R+PvZQG+R3hZRAU0Jbz4wTRCXnEttQL/APCgZo3fzhhyfK7ounUSkSsmv6WCUUmGm/efXbnCLXMn
fXLC8ydw0hKRD29KlPz2hwjY6s1xj3ct2GU4Xd7WxLL3nFCrMazFKGLdXzYY/pMFPRxy4lHgqExG
Grjxh7P7WccgKlvQcgp6h2IqVWBzvk4c2FaiCLV63bCgsw6Y0L+wO68Ksab74i9Wm45REbriQPnW
nB5STtLzOntMq8TXcHQEqrtqdyOrVqpfZcZ1ggQ7LjGLcEgPX0TSz7E2ovwH0gpewNdjBfbasaFP
NvulyGDnBb/hBMWVxs2oI3yy6+ED6QQrMfP5sfwTbjFXfetVk3XN/m01SFT11bY4hnlr8juem7Yg
tYb9nkzcUibCAVRzSt5tFVYMO3lNuLNPiurMR4bjXH5KtXAF1a3fWoA3A7xEoBGyNq3MckF5IKIz
aiJ3edB2ixhNSNWqnSM8eZmxlDvkUxWftRbk/RYjDBxko4Bk9GmDgKo7iNKLky4OZ5FoMNmPvUU/
34zpnQU5h/Q71ApaKxAJDAOum0mkKmBMVu7GZbZRmWkpKvTsOtSD3Z3KnL7bPdHx+Iv1NgqNcc+l
G28KL+Wb8mHCqWJ3183zPyTkQd9zGX7xielLXw5XtKEvDCyWph3aiRFRNc4bohYPMMIzB98u4LIQ
TaXJcNOhS+Fks8rwOfg0wHxhCu7Ed5rmjQec0FHje13roeFzrbzqcHgMKOEeaQYCBrTWliXhyuIa
hES5hVxQhXJFT0TvJY2THMD/CyriYWHWraEqE+7bjCAlEB45SZVenNycahsx1NtSYjYcDcwJJiCO
znsodcj7MRNbl+J4EMDVCKOtO706fe1S47CzBBXlxlWyrCESBVXh3cKWiRyDnVuCx677z3ATQZXO
cO5KLQV6dyyKkSiG2nhT4iY2EDdHuYI9lBpg7i7wrSDRExgVY47LviEhjUvnOQDan5Tvah60B8ep
lnzX5vfCD/tJHduNu8ehQzNGYqsIo8eBT0GFnjTdlU74S4y6LQef5a6XSKsm8oxl4+XSWitK4C/n
CIGgnUegFkoyZ5gPW36pafzjy7RFrIp7E1dFfTNKlic3lzZZIntTXTabKszdNjux0Td2GiyJEEKH
5GcBT7uDzQ80mYVrOgZ3uUvJ/b7pHTQ1gd2ItAs2D/7urTi8Gb6of4lu01vVHa9bDP0hJkxfdGwH
1lP/PgszAVdWEpEAmWUHNaSbDw9Z4EIMp8KOjtn0GcUHT5ZG9/HIJOm3plSFq7+IFm9IAhK+livE
q83WAAGZuzof24Z7sIyjxohiw51OM/nmBgntp2YpG+h5ETG53A/gY1sawMjXTkPeHquWYnbG1/us
Z30d4fyedOMCdNfGxAp6uAu2wDKtAzkqcltAsQcL4ONo883Y52Huwn13VeO+cRl/IpD6fJAxdNAO
ADARHF8bsluK16jtmp44uVNMgrM6c7mz6oe9z6PF79dnmIH6VfD6CkowW+/JNypPEKWl5YPEwsxY
XaLe/9I/fItt9zQ075O4cL/BvDbkUnxLMXxb/0Uf67O9jR4sT+JYxYwT+H+j1n46dOK+4BMUAW66
i2ldxpvWfnVp+vKU/zCwN9mQZPUClOGkcdS4uWJs2HCsRk4INNFHL23kcQPthGZzCKarNH7utSZp
ki79MT5brFr2Jqf+MAC66PMpHd9F0T0tdgMd+PH2MO56HtCryGOxtf/APgSqefZ0dRvC6dVzC3vp
Y7eTc87CI6EV6MKTZH1wzO1TnfMzTCcXK4MeQo0XNVTj/FRVlYvcXlTaxdLCjmh7Q7o31JrkPNFG
Fle4FrtYXb6aiHcftbkPthD/jOszCZIoV6AwYm2WSsTOEKZskOEUbwzmiwekK/3wDAk5Ms3Ikd4c
QStdFMgedsA0Bi3QMlnHf9sU7MhuLPXZB0PlEq1QJWTUIV1mffBwONe8R8czyvPDFA6J6lphh0c6
RkD/Cf3lqCo41FirkHZPx4384SEZd2yvXidQuHACeFMvvLh0OV6Gyi+m6yLA1gTZdNDD0pzeRtrU
dXJF9ESBij6uTupjdB4JaVa4MNOhkKnbtnXkxlUdq38wiXwEYXiurUJWeSELHxtPh0oOQ6Cym8f6
7J7inWMHAiMfPXbOmI2KsNno85BG+lqCPSj0324GxUPs04ugF1TAWnzKqZV7aXh+AnWcPEWw3vPN
4vMj1Z/zvI4ffjxkXrnj8llNqz2eXUuR1CrfuV8AQzKX303cm4ICEJ7IoKLbZo4gImkFogVFCP/n
G3Xs0jeCMQzV6BTNTl0BqbLacwK0bILh5oTQ2CqisbE+fuf9FMObBoagkyFoi13nuu2Vk2Sd6i3G
31I1AYVYwj90bbsBVNs+A+OoJ+/PN12uIEwInN1TV+cJXuaN4NSnpLVYqNC4/dTaXQh4E0JgBAJD
ob6xnyUvzqrm4WE+ZpKeQS8ehdV5Rx6d+j9Y36aOnNWb5IleV40oPLF8LMiXgC1ThJBhQZQKAoc+
IM2acNSispRnFlPv/NESkdyf8hfIbmzVvY/SYuXBQ+aZvAfB9GRKzWWBU4E+WPicdz9eHRZidX07
6MMIsEqOzWvDZhfl/AWmwZQigYCFfUcMuQzXGWjZx9XioHUZU5Me1vHsowg48CziFeDt5YFlYjWC
oQrjdsSGNavtmBiJgOFZg71lq3N1qhWc9SpN4K2mmcxxVfc3CDLDO5VbKSXcmgoxHVAOGeOM7FNU
ocBx6t3MANj8ooUwzkJWZmt/C6CIBrd2NsFeGjIYEE5rRnyA8IY4/2Z8vVYQhQ5Drdzzla61OY/f
RFxsZNe5dKVCB4A3u5dNmwAnNUP4AgoSP3uIb1zqEn+qikgO4ihiqKthVjcgMPw5o00dJNn0uzUT
nwtMmXcD0AqPtAML4FRkH+PhlIp3RYla2NkUifcg5IBfGHND+DCDky3l+umPJlaySetCJWXBZljx
i9bgZzArhg0apfWWvEYRvoero86DPsrltcD9M1I8yVUg6STdHUGvQyWJ/5zQt9nRW+XXEvlETH5f
EcvzNZh+tZyaPWUuMHDLxrqUtjmsVZbeiDXnw1A99ZyfG2TtEUyCIUUI6OcNnGyRcH8fMg9tTq2M
z+dXV//nSKBsTX2bNObUbo2CojEr7VZiIk5oiXAFz/d3PjyC4UBKTh1SD9w8aUf2cBnl0ZloPGgx
ddp2QxeEWf0mEev+1pKpyO9yvKmo7Xedvke5OObgXMVqlHtk+3XYqqIsx3zOGjcPv44uGjaa2RR9
qAcIXa9+anJ6bg8Z9UdrEK2WagWQ5ZuSJ4Ylf0vni3bwnW4tz/ie0oQt4rqEPEYnborUpmShyAH7
Ql7NslAnI1S9DCSvQ5WlYqGX5cccNy0y5lkgXy75S7nU/Y7bJbj9A8DFKL8/z7L4CNqb9IvXrgrn
MzD11lf63XBz9YLOnipmdbi447tpRT2Z9sbgJ1aL0rQqKuPwoxl7HRvJjvNbz97V3mapjQ/usgGg
rjq9WbYfg8MUKxDZwtsVy7h6ZItcX1/3SEoZJLhIqzubzk6Pj3ssN0ctmDayCOCHplKGqj1goZe0
8RlFgxhWbHRvYgIVZO6A6yrOKI6/2BwlA0msiVoWCfUXE7znSKtixLEc6hTIhNMogyw9JeFPaCDY
qhFEM+6kPHhiCBRcJYDFBx6182IVyHY/YmdrgIOmX6Knxei2dg3gk+KYSTZmZ2g8fLPYIzDgACKk
Gpfz2vXZXIDLUNph4D/PmbYAQmguZbAqIqPtkmakAJZ54Qrn5qVBXPOKklUJpX3aWhgVE8c/wx/R
diY0aBeJ0ycuwcEgP21VEv9gLl2AAS/3Ls8f2WyRSfEVZPxVoLHqVBGFekIuOuOAQty4WM8uFVvX
s9X1/te3KZmb+jsWTN2RkXpdGIJsopdRb3skO0UFc2hvss73ClnBKJdrnK1FSrQqKyF/26f2D5AF
OFH1vpK1OxrwCblQb8BlhICRpw324ORKZ01SQr6/lHP/gbYrWEdfMNfcuuNsJF40nF7gWsWQfbtR
o4r/jVd700CrRA6TkD26ay+r8Pre2xV2DZ+Aqlbevqa2Pmm3deIPxihanx1atnDDXEowtO1vi+hq
C9oyMYOb8MEEw+c9A2F153aNMla4u0mo2JBSxxJEwI43KhIjfd8uYGlX9aLdSfDoha5UXa+hMJPY
t4ap+2NHaXePtv/t5NWZGQk983p/gUBkZ6GW8DPF4KnBZZvpj0giFKuXp2eLHkL6qMyK7NK7Txtt
JJQpeXMZ8QEN//wbu6aV8Oi0c+wVW/ZnmFLHsQK3Cf5WAMsdE2sbDMuyu/XhIUTM4jAiMEuMk9f6
WjoifD9f12ighuvCt6S65fPVgK+w9CM2rYd/dzZwoOCtGHyynsyT5jFM1pPD9Vw8ssUjg85zkuWd
BlhyMEVmdfzqC6u4EIEKXxJd8SM+faYWcSk04b10zzl5+ZqsGmmydPvLUq4sOVHbEaaN7LTRqhEr
epK3vzdNqJI2pklwJjphGO/6go0CsTspAuGhEm5XMkiYCmCQMkt+INeyCr+BkNA0NTZCgqDSk9Rz
fmbPDzuUoSpbfKg9fU4uVRmCoCE4IPacbBW14YeqAeyHPL0VgYt36Q+IyG5xExufkDgH+hE3wqmA
SmqtNRqERUc3nOnfwHnWbhw0D3Fm/R3gJzV8qsJtij9tVZuAnQ3Lsb8WnvIZnf5u2omWY/WaxZwm
GeZE2/gcC5CndVEMqOXtbcGiWsQ5bgKG/Ebd+SGhEX27IEQnukC/S43IcVDL+OnCRz9/xyxNv5Uw
uDLu+0PPZjI7uSpkcE6NiM3cvWUYnCoPZA5yWkQQHEkHM9n+1eq4MsyUrorr/AfCLY5It8joIyCG
pojNxeBbyaUVW+n2ME2g2AeUbZC2p529R75C6UpiqUqApS+aTgh/8jex4BYr3NQWvcjJlsJZoWBB
12zFv7ELkR8EnBxudcfKN7nqYifjws4Ew82Z2d6z76807YTm+Nyib4M2CbGFiGni5vXHSaAszgN5
nkzFpOO2kzMktXhc4Sg/XF0xeoGMqeJIzzHMrX5KgW8LwZNoD6l+51Ux2ck9C1k/jX0HJW3KNGp5
qTuUoQVafAgQhJSrVheAdWeec6DevC5yRTn0kYHK5e3FLJFoJAUvKiD1GObh7OzFxYCdv9kprF+h
QPCDTJTf33KGB7s+haZQGTuAGKAq4wrE/X1oL3K6tMnKgjisN0NPyJe2JbRdg9sQO0U52NUDXrKk
mRzFq4gjpBzx0PucxjGXBmkNKtUa3oRlFo4zYtD6E1yiyunhaz6V4K/NzpA5N+akdD+wBM+UZtau
x+4UJoI9SOcMk+2Gja8QJlqAoPW7e5migCjjNM0b1RuZ0tAh5hlPGrxAiG8ZhiQba26kK0/aflqk
Pp4qIFDHQGMSmLsnpgMPeyaO3USDwbmTxA2Gq7d4RC4M67hn8aAzu6kuxstIP6CNW7sOCcWCP7Ra
zX/zZhwqBfp7K9pWxt7RwYI8XVPtHRGTnpRgOzE0cU5pKH40eJXKAYiJIySUJYZvrrJERV29jwEQ
54QdX3kkXZM+FF0fFqiVzW+9yHaGQZqpFaK0upYnfg6LsWQRU6soSyu5Ie94byx56bHMHqlEZcN3
wd/N47aSeCbAQBGMdSfQtdWAP6sx4OIEVhQrUmqlVB7zWmXWg+a4tq1golLFl0LowLqjMM1GaM1r
AjX4IuFEdQnVNqHvQS3B8dLWHUNkvJrLdB6TxLp6mCvp7JGC2LMYf/oC0+OCVP9XIWS9oovCNoFN
J4Lk8Zl7yZl+9YIGJmLZYJ6UsKuwqRL/ZFW7TXsyyOc3nH4RQ9NQfxWx5ERAv7lzA2D2o+jcnqKm
Hxytc4bOvwpwfJCGEFh0E4q18vS8teWPA0w8+RG5ORjSO72JUWndPzkkMnvfjum+8bSu3IZ00oTI
uxUbJCvRhEWkW0YiloeA+m2ljog12xncFMfKOLP9tsYeyrT/Oj2MiUs0URqfM7+lqwdb6fSXV0MB
zyQ+uNY41G632Vh5Upvuy3VmEKn9+v/jmnDKBAnuKZum3oh7b0kZ5VTyNosgD4jG9PANsXx7zY5a
yGQZoz1gP4ng5WGkOilJ5iVf1SNu2cQIkuWaEUUwOt59n3czO5oPogYbyOFxX1MeXltzLp1JxqmA
lI6pUbSJA6znRieMUorgmOHcz4ILx7UZu3nw84AnHgII3ACyWLS4+RMJTSX8xBI7HqujPIGqr7L8
Y3de1agEAmmp95X57/7dIMycLlbUw6eLKZugOlUWK9/05/PQUppJ2BBxIeUVVM7+vKc+Mfv5DUCA
MIbf9y3xhXtCa4axt3+PSErkY31y2ZWAGMxGbuUPLLpot5DaBYW9kRsByzIQx0nBs83kHaWoVrJN
PcWfZMpV+kx7i5FNgICrRSBIFdS6FAIbT0pTiCtvWl2c9MeKQ8aRhL5r3moJNRKEcS3wNr4grDKF
YKeFIaFA/St38ygFqmh1FhZ3TWAE8PbeOvK/3LWIc0So9R40w2E/sQl9aZEx+TDC7zu0c1JKjjGs
yNQlUudX5iTSQmyBibLSrND6ukSLDutNJ1bwrXQ0tsI6PRh58ClaJjybvVUeF1BQznPpf+fUM2F2
iIwiWIHPJLivenJahH9Zxib7k89shstYgKCBJ6fmGwXsVbPTcp4ywP2L/K0JZIuBDo0T5UzIGwEp
FzYv8HKQajYBjH4KePaEe4Qu8bzdgTS55x7OB7sxOtWlCUIvxV26+5JAQ64refGvbwD2IjucChmB
F1anqs0DWSIo2u13ikHlSPie/PkLS9GMwudlIJ46OBSDOQv7Ppae5Tk4EmJrbZYTaPVZWuwOXs1M
3y2EWzsJfNY2IrnOgWiqOzb1qZAYSwQT4Q1LgkcPWZ5neSFWs/bw1hNVUnhesNhEqrttdq8/vqK2
FLLmsC8+68BKrjLzQ+STxA+JyqZTw/9iwNvF1HDHu5ltb53MVMzq8pMCuxqcMARnhNlxRfURpK1c
darkh87q2F4MUKQXM9kP8eE+ItLIqR1vlrqpWm4D0Sa0gBJg3TvzOueiKu2/lEzjhEAcNhI6hNnj
DkxIQcWuIsud8Hp3Sj8NxOgA9C0pvOW7B7e+9tFUlumIHQmK+oZ+h0AGaLmR9xz+94CfmqVFyr97
yT91gAuKBBYxSfb39ifcmBMzKzIZOoomQRcIy4sDkQXlRFFlhlTSEdtVa1wTpVQ+sSDDU7BXAGTO
7ElonSSVbuPoIMWHR5yV3YzWxGhQ10s2Zy7tJR5C8dracGH+dtyOrCqvSMmI5g/8Sdl2UfR1xm6N
feeBJNlrzGR2b7ganCGUcX5nFi0GRrxG7FsJRGfcHrd8sQCvra/hsQOGmUT37c20aewwTlEnavQL
fuFFyZOcCNn/l9YuDS8/e5La2G3dRpUV/IdO6LYCy+jQxbWjZ3OI14EInyb1dj0GyyjpPH4LUSCn
ohjTs+jKTtqGzWGEQoxFdGQxGZWoXzcEB7WWDv1Ez8JC06HDdND6VB/yQ29xmMwJ4luQ95AOGPG2
chO25QrNENkkkcimBq1eIq3KdRMq3KZ4Jq7nJYXdP/Tb6HXaWJkh7SCj+wFk2WcvdLtRHkv30fs9
041wHMy2OpNX/v7paFeq//K7ZaIAtsA6J+grZ1fSuJa59ydTtPMkbnO6QN5aT0sJUjjP3ZacnmXd
tKxxVwUvo6lYjYh80SKRNsMbub03kNPjCUcpbHrZI1xQYOA2MVDZz6/LA5zGZ3VIvx21g9ItxWpZ
UD2dTkcCbJnvQuJd8kFwQIkEYxPERvmZbwRYkR8FElRRclk/wjiwKdTV57NuqU6kJkXZGEhwJtCj
z8cVva7Ls6RHSWVrB+h9kZpLM6EiOv0HcGy5jlej1q1o5VloFOiUM3JDuDuDOzTd+IR8kGol3H8H
dCinj4ns7hEPJFW+FYw0xyrq4aNTVQ6lCqXx3vFXNtD58lnQ4GBlYhKf7KCqZCNgcPrTa7N7ZIZj
TOX1K7PTRzKR5f9kkcX6wz1ILfnOCj3aXpIKmw7bVF4dL0Ihb5uBNO0uQ3QGQVsJe+ABMMPLiAig
dzuD4Wd6kAv6RCH5DVK2GLYZa92e6JFFfAe6b0SrOqVFzLw5DN2vaP0XYX81gKMd5RVJW/9jDdO9
cuNogC3OFVP1pLc2dHSoMD6WvDV3kujCCcJwWXw4S1q1iGFyQwqKPeBK4mLMCP/O0nJxK0r3R2si
D+Oy8BNgmJiIDEUqW3kCYgbid3LZtc3hqdYo9wAqgDkgyL9e7Wa/2yM9KrGc26tXv61q99lW8yCL
y/k+Bw6NgNFEvGyLQAtDC0MQ796O61nZK4Ch2YfYhp5TjBq55+BU9lh3teUd0mti/XgXtIcyZrfz
Ho2oJeSDrUcDMEWWB4XhKZP8ZfcoGodqifBTyGoKx5vN8RUM2GRrG/lkQKq+RNtcAEtAsHHJSpH4
6keQl7p/CgJi66O97P0Tl2nf6pkRkcnv1dkjrBd0/JPBkxVK034qjfTx2hhyyu2LYLIVLvYK3dVM
NiOxrObAPF6AwvGHausPwz10ttlyZOy3fIPfi3YZ1J33ymqcwktTU5QX71R2McsT29sVJkR9R6VZ
xLKznxXuQstybwvTQAPWtFebqCDUWS2pXbHKNIjjsGWGTxqJga/x2NCqeAolpiM4YbktRQDYLW76
LTJRZ1kKN20smDXA23FFef7TSNtuWyTpSZ6Tnx4ToC6ylB65YvYAM76ra8laNk4lzESI+JdMhyyw
9CJTzpO2o6oy0GPqvTUYm6Ocjd7N9c8TkyVJPdkQqZiwgjFDgf4bpG6FfJnkQLByiqfBwllQ8RDI
2yzQ60neCinmmtADvzxlDRjeUMYnEkRFq3x5bCWItmdG9+or7PMElgY5TzgltA08xlIpayZKCeDP
fPK9bJZdXbuBYBVxJRV46vUd0O/w2WDvGgd7KbCN46VLexq7FdEKWv087ayCLY2I5b7rEsbL30rU
bsiK29x1+9IL+xuMUWthkzHGGi8SGYrHBPtyJTFwGta5FLck6s+lIsjurXZos0IJq+lAzvSstdqW
TvsA/UCIlNwAL1HVtv87+oTPRO9sh6gNih9Y4W+foIkG0SHgjgVEeXySfvyVDLgdDEUVdLh5h8y9
PSBIxo/EtUzN2qubAuhKHwj4Kw7JKDxZjOfiL5bIAtEiD2mmYHnncD+mAVHLLZCbySIGOlWYLPkz
qh9wVYTZZLTsk8hNNYiMPhW1i8H7eE1re55UIPFtvc7ygUNbO9NGTFPu8UBALaqZgLkrRHzhGe20
PIoTtNr2KqszvC0PXOE3rpmeGZYNeFHnau86PHHFA4XCsNKVJeszWickbNryFXiPGEge1DZcArOP
Pk2AospiR6ElAgM5xjSe+oUXTGb01r8n8Y9XI9vV83PjLheqBaWduW0aRt1dSComLdArGY5NZY8b
6rNK5eRLb6gudduDBxF3kJRQX5F4QPFDATugJ/YlvaOol6QNqwbtZlPk6mFO3D325bZr81YCu6ZU
VoIIvbQRFFA3oZ8cbcTvoPz5v4N6st7yMyOaqHMhCi6iwdMqVEcBPdDqYiNpglW+GgR2O1MKbsBB
pOtBYLpNMECvsMzprb+mKE0YLps4UxfN+aQwPDHtAak9oJmdD9SAWeRNi75MIrJVcTxBFKO3HW99
Bwu8HUaSpaSYTAaJ9iHM5eTKoENsODaYtINUf9V2NlkNuXvzDA+vMIIR65pvJZBPjeUS9zqMi/O4
PtZJmrecQGL1kHxZdNx6GtwRGVEloVkAYsz126ZWsIjzn8JLkvwicyoSVxf0Bm9fd0UMqpFKg66J
ONdAOmuaHsjW2KzjTN9uAHrkqZ32ZCTYq2eQUZdc++sM4MIwO//UE2TeBYfMIVRHtL9RxO3D3djp
8IO4tO7gZwccA3bHSQP/+Gkzu2MsUggUZVJf3IS3phTZjh5z7LrA4kHSeO0+n5P+QxajciBrCnfd
Yk6wFbMlbEC2kHo3P/d5LODXeBRAuc8tVCdGkxNbu3gTg/ZQiXJTlMAZEc9j/hiORbskO1Oqy/vM
StzP6UTLFJGX0tMcxt8O8/v9/8KnNGKByvH3f38AAlkvHFnPLCAFY4dNNKkyBylnhC7AUKOPCGdy
zZ/MykMyliXnexmzrUjIbeDYx92z27kz4EQUeUA3Yzbo6pf9yQjqvSXDkipyurs0C7lFQqhc8Gvh
3jJSGhilDF6KZmWb1ZXR4hopRjhuHG/sIftwhv6oUtqppBWwcJ0Y+5NgxmWrLUNNTgGMdyaoAf/O
5s9yMQ2cEJujFDJVwkthV03OJ/OJfCpfYja9jTOec2TlJvLdHVzb0H7WnXrGxjsDY04uk97xUPyF
3exEnKKd6MJo/JERkbjEbLmPoPiqUI+0y+9FwTU9lU2t7N5udbwVLa8RXT2TqFgfS63YECT6R7tD
c+MFevc2qbGm/udeW0Llx6UI6x/xT7c/GEDY/mBaYKECXofuXhMPUuLwsiSivj1duySOku7l49LN
nBbRAfbAb1z2Py2vvBYjQo6mVjYIXefDRl6qZdKoA7UTAzN17gwB5wr0d2gH/1DRlSo27eQnpMMT
Vgkd+EyCXwM2NUrJLBIpzKTsX5dW43pOsd/qF3Q10jJoJ/qqcNVaUwOZeHEcG6qPZr6avbIvicyP
pRD4IjPsxCo9xSDTH7Xx07od6xytfy6KlqbupAtuOfT+dhbJ6V+1xcBIZEwviry3DZDRSS+3bFZj
HksjkL1YD7rbR7w3aCzyURzY+LITbeL2sYNlt92pn3RIjXQM6VYpoafI8a8pyBObpM4oV3l5KM21
rQva4VooW3BPpLtACllf1g7C+WBVj77snhCsIVvYXTsbbUSMLoOvcqcj3XOAkEiBGivU8Y4kWIJj
RwILb6sfbrysWcR5NcUvS+8tsrw/FoOPo90c1+bEKHAuzmRelyh8BkZvta0y1DdE5T5OJ53M1jkp
dF7TRO9DNVxCebb3xbWGD14ZaOyucBDDqplP/aGaE8kDfjcsoV72ZAlwtUM9PGpM3OOa5OQnB89S
ZhcpuxBBrM6AhxQjVXV84u8dccwcUOb8ZGSSQbQ5nEQc/vUFXhDrYeHG81pdTDZLnxrnJO/1ZN6K
AiTW2AoQzosdRsM5HJta/MPxzW4RNyCm1KzJJC5WzcP/aOX7HcNlQeDbChbLMGqbCFVzaX0lDAeQ
J87ig3L7ktw7gdmSlHscPVuOOWXf+84Q6zPFxt0ZvcMt/xQX5D4drftRBWc9dma7U/ma0SKZBfx0
BOpN2bXDLEVBeQcB5AtKpnkvJl2M9DRsUPCAluNG2bG195kIS28BrBuiJo5DItnEF8c0BSF3U8ge
Sk0WPr5RygUmqmEue9lyBEeU6x8ZZm8XQckjdWFUipMpRo08VAXSzYAkZcESTDRY0xsMhVdQSi9U
sB7pQydsdddgpom8Mnb9dMnBjjni1NlDGFqpaUhQWJ/IhrOy/tgkq2At9edtgGt6ubT+YID4fuSJ
2pf5WZnIFGSs3q4shdwtK6nqo+//OCcqQf7Xv8/talYniSYz8P3joAMMupVFkA16os0kf9Nyoh4S
KAlwNt8ZUUVpLKbHaXiTew+14yq031eiOgIxMYce3wfjkvizKr/AsSA61H1Tk1EsJizwun9RtsYJ
wnei7nH62vPTPsZ9rYD0lrGAegvArqgB5zdnsdmZUDGQVUAO/CKj+dWmJH04bR4tHx1EoxPARCYI
qaWYAKBR2GUqL52jPbTJkgM/Oo7SkGkP6q1eW7iozXHUkuYj1ekYQaCaO/M5hfuLUm3b5OH0ot27
xgaRwpuD9SNSDLAj5rjnBPX4ecB9Q+IZu1jW5HudR4FHHcugByFGvIzF47nSuS66seCaOqpPB6da
s47ahJvNS4gfQwVo3m9iutIC4fTYl3NqC+ZmylhaZglgP9NgQlShNjQa4Tw3inY8CnJHkuPlCN09
W5bLGIeebjgliNdwb5oLldQUIMFrygr0mBBQt5raIDph/m3RX3DCakDUWY0+wD9Hs654eQO/uWZw
k/M4/RM5Bjx5VhvbEYYRSh2G2u+q32pvvJPGoJl/YaHRGDxwaTL3whMN5/2dNVlDbs67hYqOE0+l
QeG0gJd905/HwG85dpkm4GlToRz27zgS5ufH783CZwls3dor2u3a8ojhmyxsdLOWkYF7M1o+AvP5
MpeDgqx05HFOqgVA/+qLdvlaR4IJw/EN75nQaeqRFqEN563IYGGJdwDYrfnJK2ff17p8SMgFjena
DZrqf9Y9stlkK4ZSD/wkzyyIpmFF5uLwGmnuznYNYUaVRzsCmTGCwN1pQb1FUChOfaijMeneRog8
7gdcbiaTe4RcILTG28fOGgxkojzJus10YfJkO73EvLmucbNrm0YU9zIAucQFiCeLeVYFZj9AlOew
4DvFdgIPg8WKwQd2ZcNz2dpUCdew6piyoWOjiOPhxGfx4VE+Nf8OPDiXryc9Y/3tYIXVp6g3rsmG
t7G1lyStWUDKykD0wqRt3bKa0IWSUPSvTW79p9NJOKpROM/KntZ8QOISRo1fzcDH5aAIdRWm5VxI
ql5p3/34HBwis3fgEyKis9ZvTr4BVIN5AC8Icm7dEKd8LF8j90ncnNtcIc/kiehfpYPd4gXtw4VN
0n5s9Jxga6fyzZbPuszcLfy2GJ/89/2ycfp3q0lrTX8gnVBD2Y8myFTAh/hx15oe9HZkT5OCPh9n
BJGVxSBKDnJwifu19pnpGtwDD+ewARd2iaTYJXsnR6ecoANjN8SJRUZS/7AeUyGbOLKSZXq7fVgH
6LcMeSRHZOTKg52hTerSS0/gI1e6NJfqHV6m17tqLMBImpbhFZdCE1RfQ7usv/WuiQpMxi6d+hka
aOdl9caXoKIM5jx71w8S9ZMk2aab+0urEW9AL5E8GrgwHmA0S7pmsFtshLJd7cmejIrwHBfx39rJ
5WyeJQwxMUAYReKs3jOlywqirjfA8uyQqDeyv5Ftwrj96tH51QJPVrswqPCYe1nYjka/FffUpzJm
jb2lFhbu78fYN7MIMUl5Ltxnk1Hjsjk0OpSORbIo+3yPJBQe3E+kWrXDXcLlIB21XIxEhrgJoDf4
tEAtMe3/yhRs2qeTW93/CPLKRIXbsSMQBlV8VYLdqhClsUO2/eIDBc2iu9r0o9EgnibSxIPPJvrV
MLTNWSHWoHkvNKcJou6j1Samq+TAUcPvAZTOA+ERaCeINsM7NZ/eqdvwC+NxyWG90M+x38MwMTRe
Wr4UtiiB9V8cv9NrBUix24wULRwbphI94UA3Tk6k+pXM9SDG/rfLLvaYgO6l1wxiAmBMEXmzOwyK
yHuyOYkkfIdDXMUrmEENEvFqLfC616IG+0gBa1cd8zZs83snEU9u0oEUVghQHKg3QRpyQteCL8ki
JMsDJoJh7aRekYZOt7K6CLppZQuQAI+azP/wfrSiy0tvw10lQdVMquGm+Obnv3LnRsB8Nvl6OGHQ
n6kLc7CEWKBdo/ttKHutFmVGCMuK61xCHj1D7DajdGK2Zsoc+R1PlumJFhgAimNKkqklPtZpgkkp
QYOQ2Xva8FtUPjbJrS/WjirAjPj2wgLqIVu1gVlz1xNF+vs0XYGQ7nlN0Ez8pu7GMavFWlwSnylf
164rmqBKoxxtZNZseogEdLgyott2XxWg463zaz22c71aXUokNuR4Q925HJVjfu7uIiGZDsekMzk2
LWimlNGoZGAHJ5BXf1Obkuy7Jhcb/ES0qGDcD880RUSoJAiNw9whCTPoOfaeTo94jZgI8mzOIAU8
h9xQLSckDcUDKxvRq+/OMAvKnE41SXhcmBHzAq7gD97yABqoxufQz6EYGDCWtC3Du22fw3gQ0YYx
8TyuI+7Cs/ilxWM5Ntqcil7T+9Ww1KNFYo0ZFjguL6IVbYV3EW75MdpFkuog5t0BFXsOdKNcXdGM
zZm9kYjXG2MpWCcKWiCPgyXAverV5uebQBNTY+lJsM2nBsrKxtXz70cUuogbQok5os+EuOMc4XRg
60ojUDWLEWMnNeENz33lht6hb8LYExCd03OSghiSvqSboF1/X8iuMaW+GpgKyuaEcwL/A8l/kHWB
iq789KfbL8XUVgGp8GZP95K9qmZbMNh989fWZvCF5GH/mMmOOwhx99qQqC3/D2cUwcHzkDF5Tvyw
3aet1k6oTvD00SkATbkkEg5oOdUfjT39gFemmCkqTCfu7rweGKweinoz+X0h1LTkDaeiWyLD5PbL
LU36JFLYHAOFFUJjhxtg89ryCZ+9RvCurgH1iy8zR08ry2yZbjc/JZKFX9T7wL96vuOIZmJ3VNsS
iYoefBJhRfszTGOr5dy0m19hfBWaMq4nnMqvPkCTiIxYFtDO8Jq7T7731b+HqwxevyhrM8dL4Kf1
IYYKgoKVGF/pqlqkApqJNIwlfDBHU4JXSQ49qZT1EE+kZLxc7/ICJaR7CmWCbhWI2pWyo+O1G2RT
RL0lzwIwqDDOg0t2y+U9s50ssbWrJUxoA1c049KIqmuSG22lWQ64Ob38y56YHY4/c3viauBCKxfF
ATritXaHzEoyHBKkNIPudWe6rYCeZ5J38m5PWn6ZW0b4WFIlpQJcV00E88CqnOa343fQemCIbpRD
4/WYd9VHNcgavTCgz+2l+3tjY4pySHoUOifLPwVbBC+UaLjpZiRErL/z4ugPwW2ShywIWWTOMV8V
EJiGXyWg3BRcJIyBzRodP7SdhtGohkm7FLSLlRZmBjLP3eVce7j5dAA5gm+/n3mGkGR/dC3fhjqY
coeQnVw4ltJShvuQo18udZuCJKN/v/OCGS0KI5DPwScHZa3M+VdGedkgBHloFqCwYMk8fldSN15h
uRu90fcOnU9uOafIt/gbL/Ep8laQQqTmSOi4+4c/niDn+GjEC7TO0wKhMdEsrmC11WhVmdCZgg/a
+U+Zd4wNQkgV//ZewBzGHibiRoKc1BEUXjIBv39ggtEE+HV+A4XjHYC5agx71EdLgEmfa3jjnsVL
JiMfhh2vEaOcqDrHZ5ahxuPkrbEvVpb5Op2iUTZK0TYOK3fsPoY4nGvlUV5UkV6gnalCnV80V05L
B2LzGYXhWTX/p5o638Jgotrz3iCQsBcIcxtPL6kub7LwYhluWkqksLTMQUgmH3MpjDZ2KdX0AZzw
/tjwxE4bpCyNLhZGK9KfNVu5l89BuaMn3DNPGnFMYsiHvXYuVuJoSfOuPl0FcJfU2KS9Xuzc36Py
mXYB+FWg1wOLO4hieTpMEFPJBMSviqpn+YqqmlH6c0SRhYdeplCuOaZ0LKx4KnPfONgCOtD/y0kP
5HgBfCyBejKp9ZudP8+UoPg61I0qN05DAbfWpyu/OdPybGRXCJeOcF55GcsiDWVmaT7jAYb5gkkB
fAMr+P2XwLgYpQ1M4psLsNhRgiWaNhSEidwrrI+Dyf6TUhrP5fx4h7g0e8ALxxI+VYtDL7YNqy/Y
YJy3tpkd1BeWCZgdnqp75oRcxsw+yepAxs8OpaOkUMkLN7uU3oPu7a4GXHfKpN6UgL1pfJX+MLiI
YEkFcMwFvkoDOmoVzoG7xYCwvLN93vnRZbPBpJFKAlNTxczOAGIx6OueIVVJqzkgpbb/k7DaAbK1
jERkKe2UTOss7I7FTq7wzoD7Cror2BpqFnlZDJMkAi7RggzM4BPVgUg4d9jhR6tyogsVBgRNP+cK
Un8oh2gQ50W8iUDEXnIi/7hI9nXqxpaRCwWT4a7EG9rE0HPBVfcwRELbVIfWHiz1KRvLANe/+EXt
9A6CpRK1CanS+59NscQD4DsHvzDVitQmF0kjkVJBrmLJ9R3YTC3lH/AWDMMFASARR1xobbnMAAkl
ytw7F4i9T7ebBVPFNPiBAvXVlNqyfUQIFbbXHjtxzyPP6cbUtO+Y5yk0qZ0kG5x2v5uRE6UOF51z
BZ+heXeWuMRBMLwQY025/Sl0OS1ID7CFuuz9lKYcaMtyS7AOPb/H9jPLjPc4SuYsaAdubB5zIGOG
r1cRHUSca8pfgDNmfl4brClzgDjlU4TYw8ou/JTb0KlyEhT+qbd6Ksbq3yAgEaNOEr6VlVrp1isR
4ho9YhlXj8x/x20hGBO32A9H/+VKd54Pl4pKYhYjD9fG2Jx+jlIVzmDVyagYPXZenDlDrUaHVNri
AwcME3vpCR5N9EFU1+yUzUNt4iwk/WfLlOwLO9pR5QhAf0M0BIY4Love4kBfdkAEBpWLzhg9r77O
raj9GAJhc/JlsHG2dyb2bAiGXpbnpMzxCuWP3vVMMldpokzcaBgpa+ne3OfWWx0coycpSpFSpFe/
DjrF09PKaLLtvLx2ehDmbWlyiirH5p++p6DRejyjVBAeAp9YbMrGPyjT3fl0t8f+Frsm/q07MiUg
jJmq77wgsjzis56f5OtmHPi/d97UJZEl48pVjqHsOg9jftIYWDvRC2QhExSogkcOMctoSvydfnfj
jYkdefLKe3XPZ+bXjPd2cpGhxsrwbDinPPCcchresVLUcuuNR292wt6/eZhs8KTXcriszv2jGD+r
Hm/vcwPC4dfaJx9yUhsP8SgVoKeuOCgMIlyF6ZZFe8ph2esyBr5C4LIGgmqUOAf1vyBELc5jnW3x
lxbwD1DMxybao8TBvS2UpSke1lxheJ1V8vMsANJQ+6kZ4Gv5oHE8ImtTwhY46qj6/+2sF+qShRu2
+XsZ3OKKoQiBnL4gaueFi+it0l2O0IuvwO9mCVKt1RSU6O5qpmBr5i1NXe45oRBMFTIWlW36KxPD
JJ1Gsi9x5aZ22ko2zBswoSUxTVWrJO84vpivN9DH2BItN0DqQgJKgCdq+YYMflAUzSy+1pGfpAaY
7mhtbbh/y+gKbJkevQCVvOHBg4KVCjYL6S63a62ahAwqZKvxVAhylgmW/PLq9PB/BYykSdS249mb
CcURV+bpqI0nyErZu87xoex20jk7dOWSswv88zNAxJpjnr4AIuUXevGuKEmrjP4E0tvbfSmzKvse
Bip/tVs3sayEmkJHUmVr111GOoc2o/jVFBKX1hZtd2AfPHa6bsy4zNkdF6HPe14Oy1MdY/aCLVlA
6sF/1c8x0IqmubhgSx9CN3f14zjZ6qjVleDgLAPMzFK05XIt/zsSn4cAuzVlR1SCBQH3gorzbxxq
RbUybew7vd6pSxDXPkvbihgkx008Ne8FrUH92GP3o/1RvRR6S/D1afMXFV4u0IwYO7qpN5LdGdkE
DG0p8UcnoEnwGPxALK5QizmFTRw3+N+sggh4uWAy3juA0SyhscsxPM3Tq/uJABfJaorewIMabfKo
xQQg3AoyvuslQHOgFGgCJi+KPMjfwFD9AmyytoFFH2a7zptESyx8vvix8vd9HMxj636tP+XSQfPX
FUmpjS2c83Rii6Vx2GH3T7loQIg08iiyaBjZiX2Up7oIq5wO0nA+vxUiI82wQgjRas/Dq5cZ1cVx
hCRc1YrWPsFlpeMnL1nXhbwt7lFmKiO9ce/gTulj2VbZPh4HhUGR0GOYo26IrNh9bcjThiuNozWv
pjwQPe5V5wnhRVHSpyWm4+mD53vCo1/loYZglnbJJB4JTkSzuiZ2rA4spWCN6BvNetBJINgenVA7
8uom/yY57CooeYeYvu37naQZD2Q5sbJGh0M26vnxEhkDDxzrLKd/EHip8XRoXR16oiEhs39biK8N
TI7hGUmIUXxp5b9riQxb0m7q9s6rQqol7Fc5zdKIwinmDiAnHht0N8kDKE6gOpgYwKUqDUexbSJK
c+AXXTcZoUnWlalQzn8tnuTJRMT0UUrVi/ryNK7ugYr7N6ZYVE0+4CvL7D32LzI9qwAziR74B9g8
clXjZmwkGi1TgJD9X+Byi/egYQ3/B/5/33ezTlmmZ/B/eXFZe0pehE7skyLMwbs7dohdceTYX1Z3
ySlbN1P+E8aJ3BckaQvkBgZwH9y5IMpPpaOdLk2OdnPs7f4IBBCymlLOmr1P64GT/qrY7xvUEWMN
bSbAB2CZf4tbIl4+aKNlqO/eB93+UVONQouLObQqsyyNo1Oc8UUead+ZzBdhJ6RxWFJsE14hI4pU
Xkg1pTecvADNESPM9Dxk5YVnLcmk/djZt2X4DbmDm6ujD0+lbrj20kVRHqMGJr3BvrSAlv+ZLaCb
WOQKlEXui3Q9laT2xmlKlNcP4XVST2CApsKTCjEYUJERdsV0KLZL0aYrPX1nzFeG+ISaXqDUZGCy
JIgHb++JsFz2z9vGia1sLQ50VozR+tu+ynTC8Ev7M9rcaroalegMAJcR9jTdAbD0h7fQYjIGaYuL
ZWQR+bb2AMs9LRRgj9AaikcL5u4qhzLhpA8zs9c/z1GJ4KALAetJIzdnybhAMyBfY+Xn8zcMJz/+
QO+lAjnKgjGgEY/YW8uzs0Tj++De85laUGo6uBEsbOOSzbTJYjNoKdt7Xnn/ykalC8JlRUk7w9cg
5VLYqudDCfQS0OUGZkk+seR2ml+JEdGlMBGVeQVaogwyWH4a90YRtEOlRBpQeK1kmjwpTQpv/LMm
8x7sqBspMYvOtI5PwSR/1ELZUAGPTaSZXdMqQvxlvLdYttnPiqbI1IGKoVIYz0YbpZl+QMGDbdY/
CDc3OJFqDZ8tKKgTp1jR/Ka1/Ao7k1884t/wLIVV2Z9WOBIVO7y/NCd14NH5qHl8gtWEDP6DBMEi
RJr0AxBOQsEdhQEDSFfI4ISamS5jDbLEQRNNWsWVD4Qbd7D0QKSn5tUZE5tWvNqSy1rGvmLqen/r
rw8WBRnClpn0o+L2BQ6c4XRunM+DtrAaBfqvVWdnXzccgU1QjLCHNT0TAnvFbSQPWBFueITehiA3
/UTJ4Ft3GUlnafeboxpGpUr++skohQBfEMCYaxzLe94rQBvv4iF+y3kS4zGWJMyp6TFgDFGy4IXB
Jr7xsyaMUm3WuGVbeAEe6xtnLqxgdBRoy33gw+v5PC4GYjnxmMzDlcV7gvp+DmJZfMJMsPeG/jWt
WakDMuBUFG/goTnhRtY7bB91PhmkxZ9un14V8UGaI+cLGkAnnc/X11FOpHtoN4zgFTS2eXbxpBAW
NmEBHgdTv4MGmFxJcE0WdZATu6A2lg2yyj75OqV5aHDNVG3MsPmxE/lcXbqqKCl3WhIEFGK6rxki
C+AqM0sUZCVQQ4LWTLQqAnxrrIaG+s2vlj6w8a06/vnQSr4oNUOUjfnJe/VvCwLpfgkH49B6HCjU
rQoqq9pytmpbY7eiP7IeThkRF1MBI4aqSEuXX+CXiFO5jHyAM4aMRTnUwrX4rOHp61OPFAVCqHSi
W7O2hIwNChbJvcv9WHZqS4WEYRVEegNZudWPAHm5P4XAZmTXK5aYWr4gal6pHyd2+zmfbvv4GbaF
4WZ7tv2Hs33qHGRytqQP7OArFX6n7mLKOWaug+8GojTPAqsNfC+/8Uwhuokc9OZ0FbmFquRbrjnc
7kyusFGL+9PT+DUl0pKj9vt6ecAdkTbbD1Tj5O1MZp7Sxahl50wF28hCbNhlWI5daHzCcDsvRxnp
aO9/fEehGqq5Xs9Bzm+U30YETkyvM2Usv3O4iOv1K7obwYsYMcxnME6hOxe59iIEuGp/31UAUUDL
hEEI7qQI8JfT7jYOwuLlKaouiC690OS4LYfNmVFO5Zc90BOPlpAYmI6i1nGypGyD9RFPW82P+nrn
rSd5XM327qMqoUBvrA4MYJGhNRw3BTOMCSBO+QH/fENt+mvqj05EZOej3zZfk+sfQPV0z950qa1Z
USI6rlKnq090LCylAcpnhhTamN85u7EmgmgqfvNCi0s7gEfohwYBjyXNanlZewME86/2ymn/VX5w
WKl2JdoQYVBH1NtkoXjig+Nv/4E8suE3i5c/XdGcm0AYdG4dDis+X08iMYy+hvzGjPwdrMXMlsAw
vXu90LFhgNUsuXpRc8aOtc+sUo6u37UtzLWOoeNom4wQ+lbL3eoCv/qnYHnWlXxW2jlcUmQ4snR7
PD5c6z+I+SogIs83nIXsdCEHzckJXti1DpwtZICSoxPu8HZ/Jvr4JSuimBStbXQUZe9F24p9tsKo
iyfCGN5GB/to5p3w6vaGf/FEp6WOIraY99okrjnEMJr1kN17KQ3rlOYj4OMjR6Dh395nUn6j1eKG
83ZXDvM/r0J4j2Y5VPx1wEL/pZkbCYJADmY1/gmm9Iu1627KoqZoD4HeOMFmHPxVmlRBSL6tLhL0
SmDUC0/jcWfQ+XjDctSBSCFc92id5q3X2VCPp2a5nLl+MECIEMa4XWOrjv0B9g/GtTayqQ5vgbSj
SoMjvqbLz9uwBwpQdnpY1uq9XwTEkAF10Q0GBiGMgbzNePqB0DS1FeLL02hVwdb2HGYrGLr/x7Kc
teH9KXfCzByz9oD04PrnkacXQv88EYqd+RvuKf/dZhKv8EsHicwrzLNF0Sy59f6ZDKuKdrfC6G2i
Vhd7mUE4lmrX7j0771h2Eo8lafiNEww4PYNgPy+9tBNUh4ZYrAztK2glHCfKnSBV7JR/SiZLMtQ3
JsZOZisoYoniUwZa4DG5kD5zUsvbepuTus6JCKbl2+GkS+aAXpc/EFAapY0penpLfJEmN0+l8l/4
W8Sc7ETbQpD+GjEULFm9LJgarCs6w3QfDNBvrGhf7nc6t2bvckbY52wu6yRh98hXE4rK0iwwHkLh
bPEQvcTusNbDDzZJivujZtkqr/L8MqLqTekHI51BJr2L/hIViZifkxwYFIBYzgtt9wLqSgcS4I6w
HbYG2rdCppSt383vkxMRCXqTn3P5gY/W8ryogL/JZ8gyiXD80/mFSRz8wuOolyvfO99PS2v4rFfV
qeA2G2K10FOIA6jZ/fkNU3r9OywyMxPi2hczhWnn5QJCv1icn4mSOQ/fkOZTfaMopWEWyj++jHGL
0k0VtKcfy/ec5e0q3fxnC5wx2gLRtDKbOt4JvW0E58DPow1Xhrg177Aez3ozxcyRDAiicAAXC0B5
/tswSVo5utKQ4Rlx+07T+wA3V0OL0QkIghW71G4KtzKWv+dpV9VrynAgVRs52Pmnj6LbH34+4S3a
a6ll6vuOEfoWyJ7imt+p82uSk2yh02M+Eywrw3BynwQEcOYnDMsOgvBkcG66QEjzSodiR6GAEYHx
6oIl2ETTzQOKAOn+H3MRX/CVRoukyKdIMG83brI5mGhRRBvT7TVDPKqkRbWEEJYCrDRFabUn29Oq
FejjAiNM93vXbe3tqXzSwTN4/DvA4QuQp0QK07ZvmmTL3P7BZTshiiqsHYAZX6yqcqFMIjIuUeOb
5dvF+5fXzK+BBYjjV9S/dEZlRrc6TcwODhGJo+nFKj3//iW4LDhsz3hjkrPHQl4sHBvNh2xXCIan
KvexpCFZvKeEBZu3W78ysFdY7REabTK2XFVNynQ4vKLef7kmNPzHfBKv9u7VHwfI1kiPzlh9Pi+I
OOB+djX/cRUxSIduDcA7Y3WjgQUAG6yDTDWrI9QZNx34HCVzTYrAMKHflnN4j0IvwLXEy7+MjAjw
cyBy2fAWPhqB/42Ix9K3LOIhxsrJK7krhYKO3JjBQfO5HHFEf1ChoU992B0n/g/Th9ucz/+fEtKW
M3xG5Y9LMhbQOTmvs9sPs2/O2J1tdIGHlldWXf0XUPGLBBmoE/FxFFqxqjJNN0Qnoviq/xi/wApF
SPeUM7TJyd7wUBM4caIJv4fd8Dhz93fIvUVZIvhctytfMUo1RYEPwtE1vs6vop/XJc4KVSWvn0v+
ggkfyrftqowZ2nAbxTRYRwqtP9PM86Mb9t5PltLZ4pL2t+ocssdbTCUz3yEbUbRmPd+LO9ZRbygU
q06SsR4SWaGIaV5CttCkOC++nZqJv2IhTeDdEPLIHpLWw/JVUuMn/2PFkJuuURjSGJr2Vegsq9AD
zSjhpRZWnBpo2wjmY65PM3hvIGN2lcg3v90MNcUSO/dddysG90Ye+AEx9Hz6ZWU9hNqVXv/75Np7
Ayqw7QH+IVNksQHF0Xsw7zK58x617PyB0VnXY7bnrvclvS2UEBMlt9lZosckhsPuYwWz9y9Ck+84
BzMSN6AZySm0Fm8VBcqZMpUtFIq9R1Wdq/sCbDB7ffbwpDRykoNvOSKRHe7O4AA5sz2N/ujG7hcv
94XmcaPUneh5UgX5RQaNMMJABGnjwpgON6q4Vv1L3tWFcsQALXXfWcwjHO8QWIPZnKZao6cNJlv1
0aJvbXuZGH/mf37XOkihbfbefxrSEszPKouhrSsDcU0zKUKhEQ1VsiUHCb6fQH9JmhYE4gbr3GcC
hJoTYUotI6gtiS0n9L2yAUjpMU7DMxHH17J6qUJSYZdUtVhtg2LMNtxdCMY1Rjet34Zzw3/RKTgb
2uJwoJieEabydnjjL7zenABFp+cglzHFg/vjHsfLPh5dFnAYmPU1TLVq1AnUxh6pDHQAFNkL31dU
Jyboji3mN/uBKogEh/T6C5J1310eemoYl5/pMtMgNvjnVDt0js4faEULKGNjmCRWzQaWQvi99s22
xpSM51wFaaGnHnRObcrtSq0jxz/w5qmVl+wbmTgfhgkv+5dERkCDQDj5QmfegPl6MpXyvYLQf/LK
jfOeWfo61pxkqaS++pPVVAWaGcb0WAS6obs1IOY2Z+SyYFoRlGMHMRlYjZQFqgK2/0IEXZLKL52e
huTzpQAOOdZyjcno+cgu0MmYNqjl5n72NFVHLUFpM+SxdZWRG4bTi5ywKdiD5TmffNt17tA17ob9
dhDEQch2QwRzMOfbLQwykC3DxClIhUfJWUrPQotF+judYHfnbCESN2lsFs2ozIJ7r8gkgcMVpP2K
SzKdjUGlAhyij4ZsjX3jbpLlGCJfQ36L/o0fvOOrIDl/B7tc64gcZt832PCQX1kmuioiE0vFoF7d
zOLu2TdRwY1Dg27aZqFu5/kZhBsAmeVNhBFtiRo+BDGfra4+1K6t/zrYbLupW1f7VNXgJ/mNqkQn
pR2Lk2CsRn6x+SfB0nopLQkb+C/c8q93rkL+ssSX5grqGOP0EOJB4liE05wvJHBXjKv9tKfV+5rK
PblECGgR2LTyG0k2iKM/3+0kePbgPq5WrYKceOhtmpdbbgW2WVLVQ5wvep8odW2WKAwnQYUfBJ82
5aQjOtXAymBcWFXXtTL9GTb1r6F78xUCJjVzoKUOLoDfojFECrW3MhLezTdn8ktWa5IkRs7WhFDs
Dj/tTnE4sXkTJ/fWTB5yFSJQJSBUMW/G4gR8UGdC4YXA6RGW4EeODjghFvcDWvCsy3djf0zSOsAk
AP3qqg09CuUuauAX7DJJPXUIuH3yKP1aVo37j5Iw73H0r0pYQml2n/Bfustg2S7bwYwmrPpV0/hF
k90DWyXMMncxMaxovSeWUouROzsmVouYmBC7z8nVKZjXoKjZSLWSV5djv5Zjv1w+7fmBx036pkB6
O98z1NU7S+CJh4fPRSq0VEs/QcZJEnCiDi7LNlUyFfOzK9uIsKbK6ifjG1Ts6BTx3ysQm71NFIz/
Pseo/47h6qvX436yYJUUlgqPyzlAS9AzzM7dl1mtfuix5Pn/Ki9YPCDXTAMHmM5SaNbgNMWeY/5j
8Wesk+PZAbeBA8ieNbZj1j7Ld8is8hRlDNAsvVqZ1zMSVnT+jjJ70ZzK/GAtvIR0OoYjNrvzaqdW
yjFWICBUb7j6O8KGWBm1w85rrjJy63nGbeWaSvMYpVV4jZFvAL29z5qhsglGeMhJmVsN4kObJAU/
chTqre6cRMuNqU1kLxYQ61NCAsABrv8jkZu6ajh/DFdb1dDQ5OOyFPAiKp73oJ/LVBJA4CQWymqZ
MtG6QI1R/x8etDFbXkApV0qV6pJVugXQjQ0pQWYg5x0d88KOxgopi83zbrDs17+/rx6w2F1eZANA
nxoVud/pAtQBYxozQl9szeZOV0lG2Vx1jlxtEWSBuYcyCaThrn2bcqy5Fl4x+NINaSzrIHdruDLL
WpwLvjmv9zo/1FZ+ErohPlkU+lJxI/qw+oKBv/K7MwBJ6CB3C5t16HCcee6eXfSOuY6V1Nvw7Mbl
gdPFl1kSyvjyDyvQoHt/UpFsLeJ20gx+isq+urwi5juP+N/GyGEYdraCjCZuskCrrk4cyWNq8ub/
Dib1uutYYUZIEW8rD/X5i5J9VWVYNNMKnBg9bka8WKeosx8kJrWfklS6xvZAvm4qPgm3OTSPomZI
XsFAAS4XU6JVSnHhzE7qC8Jz1S87iX60xiY5VzVKHzTT3nk92Wubuwc/1lQs1I33gMUlhD+HmBdV
t52WR/AQ9XjS1MGtAvkxqZmDefTGvdjq+L14RkY3k4M6xP7W0IhLE7DeKIt5+mKGGBPilsjZNVgQ
CYbIEROnajCyRYQv0fRJbz99QivUDrMFLyf+WBoPDxudraGGdd7NGKsSDFd0332K5zR6G3YnDdTd
89PZ7yTlrdqBpfEgEukDeOBiEWsGq58G3NuchmJy+uS2JExPJK72P3FZPDfB6bI8L9RTRGb+VOUN
Sqj/WUYH5bBvm44AzPo57aOleGlbihKLJIKeIUsEVjnnZK4fCmp1j8UD+hKRTi/66eZZCs173zK6
WljwnM9QIoMRitl5VfUu+DGH/rwXNNTRrJdFeZBxqhDprR7/Q822DwLnCI85tLdga6t4GQ9Qr4lE
CQdrG/4Gen/wN98LJRuX4lm710KvMXiYO3Z8JsYySgPOKYQCZ5S4+lcphuQHq/CMGNGy6bvlzkC6
dxwWHdQ6LUbE0nHkmURk+u/kjUHOf5/JRVwmncf0LEv9198tJoHYOm8Nvr2d1NRyY6aZwQbRwkvM
ik9KJDIp+fspa3Mzf0FqLRPc8tgnLmH3OsG+XtxEv3AAWpMK759cGcIGb1xXDypIM0JAhhiIzG6y
ylHTG/xJ6P8GpSGBFATpcztk4IN8aHOYj0T8ux0MoSA75TY89at0+WBFEsHc86C4P+1RTb7kIX0c
GpqfnRwAq2qj5d7pkG5Q99cY8Kx3XqYlFHOanhc7JbI91n6l2XtXtL3WYIQ4+B4osg35YkzM92IE
Ga6yT4IdsBoRvnMbz2iKLDHpREGvIP5SiWkKi6yJwcdHMOzOWR0Tse1hMByWgtcq5cNOc1BJithK
UPc70XgLA0I2NPbNqDl3MpAigz7d1lxICtTsVCebovKYMSSMGlz7IL8NVwZYmHn01bNJKOd7+XqY
e5pScVekoq23gFAsl6YDjBmEcsE8h/L3jSmx3GQpz1JsXUaDruWugzK4toBZ0eFvPhN+A6ASok8b
kjErfNEHcIk06aUtvY163ycd+qB6KD7zg6v7AeoWAmppGYkqI38kgvkKvgLuwh4fR3yReF9foi5H
p+S8tfnJ5JWFQ6qom2yaSz5YMK94Saw4TRgR3BzkdpTV2PsNzD3wwtQcl2a0LKu6BruJbVWG6kRK
BuqIY3CeTNJRZ+EG20IwsvX8af526qE/bbYYLYuHhDaexjSxbCndWWTdWoOyQOGsJNQEpBsfCD7e
wpgidSoRZ4eQ9D71qiyj3bFY4KDWywWXQIchP4z5dxDEQUcLwrEl1LBKj17nAORL4ZmfpNboVNdc
5JyEEQjVYpOtWrgaUruxOfFNtnH5OXsWsd7UJzNaYKaE2EbfNdL8WeE8nZS5JpC0X+bJAkmEyV3O
6lY3/BHl4Btxdjqg2v5s+2kKC7fl4yRP1hMy/3rfjD3o0khZEs4EkahASdBXRTJzMyZkBuABsGW9
BTTYmQ+HwKymQ0eAfb5sAtjvjRUqya6Sa2Tk+pEWTc6JUJio7X/Jj0lflTMOfrnIH1obZnGDhTZc
+EtwyvwNG8U1KVmgYED2aP9G3CG6HvLOyV1W2smT6MIuGFX/rmhXfv6XhA/+4E/eUmGkndxuS68P
+u2muDvgvm+avMtco8Ch3DQkt/c5vro3vJG2wwZRvlIhOQWgen16xVvmmMv2rAYBhtX1UukZX+NL
XUQ6oNHwxswxywZlyV/bXJxltRUcHNRibfAt6HV5om1czDPkffyJMtBuBoKVU5OqmVfrz+Fa/VR0
qbKo8Ya690eg7a1+VRWIYF6STfLDC7v6s6DNp2Ble7jK15RnQXZ3VyqWpbJ0A4LcHwoH3hFUyEiY
Kwr+rbTBJdkrnNfHmzo17URk4LBHadxU9SO1lWKGbwC+rnLhPhNyDNRGlRxm0JtfKP6l0aGnSiz4
Oz+VyzfxhZNLALZJbfHouG61h25tXigbrsHkBx8OXdkWupytT9uXFE0lS6HkfOw1ChVVjVuUXFT4
/N9kiktGfKj+mdfocv9X0207m4Yek6Oi0LIhtDsp1JySlTW+HouNhr7b9BqtZmfsamEfCKKXTY98
2/bpfZ8Shf4tlCtcuh1FuhRDLGQE7ZLVo8YPuwYQrKL8TRlhMPu5sKFfS/MYNET4AVUWfPmN3UQt
7PCPbHt5Y4beAsof9x28XsGWq7QS1J6jTU4pUq5p4f5Wq8AjmcZHzQjwh+39bJWMXQSL8YTMCc0Z
ys0QsBl3vkVWC2i7L4Z+5JoKi1W1KKybiK3yprtwj7GykRJy+L8A2ePx4GjNCN9dzzers1Fp0U86
B6iBZBFXSxl4kH23pqe2mJTC4JYCi6z3bjPY8bGo9+l+77zgZFqAu+xA7sN8XkUiWZItT6DNbRtW
IGR2/ZuL1c7D1DYSLw1RK9dEL/0XBA66nbXQS+PCt3cOf0HSTXgJddSLDtX20txcxmUJ/ezr9fQv
Gp+m6wMs2dqK+ZGviyN1wfFCJYxVY6n/77wa+mTFts8ulAHfK7os/loJLPY0P+p95LthKcK9ap/5
aRhjlmIcZFnyMwI/Q/c3bmqNbfWm427w6HQceqc+Zrxs6naOF2cqRAgBvspLrtpLRSlAJvS91MLl
x3zjuWc5ufMHqYtR3wYk1phK7YIimHBJIC7bBHJ8h/KQZF3Q0fN7D4CfwFWTzn++g6WRUSDklrpQ
Dl6to6moJ/HieHri6bD6dnod+1ph4243jQaRPCQHo4GBdybf3tFnnceo/GJp9jhb9B/YSZf3IGbj
mvtR8+dIlEuk5zQKNsTWTmiAWgpwqy6vveDlWeUR4n1FRpEcNr1Vt/fQH4M8vEiROX78zj1Rlkgp
vW5HFT3mHYaTjhGWZFU7ubmkdCL/wulwYwiOEz21U/J2/714fWgP5RgOnrTlsE90b2c1bXBD6ghn
70VS73vd2Ir3hc8aWAqzXVeyJyxNdKMn4c7jXWM3xPQpl7BeqZveznFqYJEfyc0NM7XzaQmLQH0W
645I11MQu+rCNWXFxPMFLj5FRQ9Bg+txPyeCL12YjaqJhDdRhY6MIspXkT/VKNXZbtJs4IE7Pygh
HQRtEnpmK5XSb9K5W3q3vw0UIzJG4BOiGlg6+528T4zhM/rihjiw0PrNSWMqPHu7k4+h34hNPETV
KA5TpezzFxCyMDNPqXNeQCBQuNDSkkon3TKj7pyTYRyh3aReJdPr35oRrgJdMX5Skn3dx+V+N9pp
OlP2MpCm6XmNN3XlN8L6QmHp2XNNaxJniMgHOqa/qNxd/KnIuqRz8nVAkaa8sIQQosKLt4o0wSw8
bfGodsvsWMVAMYxsIqWtXKTvuq0TsdlD5Pej5+wRck7fqtcpoclOc1IQK3Gw2YuRbQ093U5gX1kT
dgCPTcBF4AYx6HPOezOJ/uL/nttgA8ujkVAPh2aR3FHLwEpfr6r8l+2niheF2ID/eDgUS/n2W7nH
0cQ57pSAbUzokf5RObKmwWX9srtkNTAmLz8CdwPTT/zUOPj2lElEzvCUQvB201kcy5zOwDtWC1hl
ciO+spqOA3VQkdvea3UfF4PqxCYxaWYkB0zwzxiiVBkAEh9I+fY8wdMQuEwfQK5zyPp3/4dCV6y3
iXYSW5PlIZTOdZrHYVGc3Iped2cmZIHNljdGuRMiRt3Rh+jLoTKCAo3tocwN2SDLL9UqCq7ZlEUS
qYlFVYLBPF/0g0lAlJPgPnl0mHXdqWu6Z7KuDN54y13o0epm6wWUmKvHgt1a2oz3IGeXMzvO1RP0
uzWGSbl7OZaJ0q6RQMwwLlZNdiJkTXiKaoG4aibCGbH74HLxdgNDtQalwVajrrXG+3qaXVqp1tkY
FfzcREARduDLRGzT08cFwVa+Lg7we2mTdfyudFq2artKZ0gq0EUUHRc8J9+XfyrEzJq4RgyNCaRK
GxuwpQWHII0cIkpoveS0ozrUZn2a9sQrJVaybYHgBcAgNycQ6hOvi3reB3GDO5LRPSR5ty3mOCq9
TsHSq9PxoDiM8FG3SCJSD4afyNDbYGO5lnary5yonQyARukxqXfXOn2gPX9Uy3l/VHMxX3gn+L2n
QqUtjN1TVNAw/R3bCPrq7IHKd5kJSuBM5NhtTWy+z3HLlnp7axi0JVxmKYBHqon3Q0F/c52Hutgs
DmpVtmnlRrpewgQoiiuwEz+LIGzJTmwdoXEXkTDZixIM4+iyR7FREJ3K5r54ALYFhvMUQRtUKZZR
KN7W196AtQJMsm8g0NLqENzSXGeuwphBxX9YjLoiMxWYnFSocmQ1pVYWBG+l3ajZkzEXdaO7WWgB
jDvluNbdIcvT+oLFEkFbcsnxchCSUUp5Hh23Yz36Gs37l0XgNtYSAG/ypJMN58xxPckmCFZNPGrx
0ZWnnCmF7EIfvhMKJiBHZR51k2uOZGFk2ypC6XCxdhINsBEQ5ALq5PdlXbBp7cSP1XYUeYAbUt4K
YVBiSBTkz/wh5hotqBK2iurxCYyquLE7sDrAPRWtfGW1CZmn4lUpJ0lgua/gbnnBdFNbveD9etRF
j+U/BJ4YCDQ3Wzmzoo3+7UsobHHsqKpWZlfI9TKVEDNW/j43/kFCc03DzP0YSTJ0ZvC62gjts/Sg
/ai387WHPMPWKtYQnvymkz3VQrfurk4JLrDraALxrXqXYQ3Yw5znrOKGZHmgSrR2x+ncJehGaPkS
7Mq4RU0RUO5YNkvuw2dRNz3mWHi6nF3tI2c1oNcOqRAMbLKZfru/IwcZbEC+5z0gsdYnjkq+EZEB
iTa4yncievCCIjkzGWq56RVrPS/whLGr+Iz6Q5cUAbigiH301DTisvIBY3vXSdGsXmCF8SGtVLws
k42JIoLOUa3lt9qwelcpmovr7lnSW5sLC2F84p5li2ZTImi3EN0V71/BSoOjC9EdVIDs1l/QYUJR
+J9T+/tmmahcTG+s6AtXcIpqbBelhFXmeVhy0uGbiWOOgmYVlaDBg3wFMQhpSVya9tfEVC+MnRJy
q/3E0xyue10qICXgILx7k/qS0SIUe7mrkyDQBV2b/+xWvTBpM61gBslXFGOX3CXZ1BSaR85KKDEL
NdCOi6CRgmT5ZlO0AeV9rUt/3gTyAgOl8E7EQNvNbdYPeh4rmIrz5SdNK5BsAeP0bKHBwfCnnyDT
A+8KsxOatRVFG4H+4SOaxb8tz4QhrApJIoawf8UTfU4JKt5YqMCjg8zWInPMwow3kuBvQsEP8r3J
9IZPRDmno7iA8HAWZ4PFKW+T+uB7jVbOsc0eCSclkbMhnbj6NK+VCNZz0Iixc4ACof0nr2wJu/P5
OGq5hvGkjGCR5GWff5aTXfNY2cOK8c/pN0A+7PLYDAcGTXag9daEQuMI5fHjZ1YD9oyp+i5obfxc
NzulF19sekB6NLhX4Mw8xHMuF3mXdUi4FTYTNOf7JSgNUOz34PAW2jocZF5Y6tEKbn7i2OCbkZE1
d8P68fo/KuJQvmyfuBzi2heD+4tXn+DTRIKrrW24AumwEVNk/jDI44gRrKqt9QlfWBkahZ4zNCtc
ZHsErlOumkW0LorsZvGaUONotbalvFXZ4XZtG5QZ85l78pDda5lp3HtYrf3G1r3g+9YsNtFMFeyg
jtysIC+L1x9wuUYPZGYpOJJW996ItjwgUvAYljeSorz33I/aF9MbC9qhngykfd8wmnI0+gaEN3O8
LEqp3dZripjQS7YwtBMOz3IrgnLHBRSoykDZLPAi28x5XNy2I482zPqStJoHUiBXkFLaX2sPVsuB
zFMPpnVKP9jhy2E4FvHBf52fgAI0KQ+oV+ZvpKHrqCeckOotCDNknqRUa6Kav+slD2oyJp1WbpAd
JtFvMgjDyO+nI4s4U/A43KVdVgvuVihteR/fBc7D2JwghSIgC8bRSRbf/615DPzZrkDecvbuwe9D
H4wXE2v1B60oaaKwxBcmBuDQjsVbICvjG5bVEeNNgIbIFzq3qWHK/laZzqDrAuuXX+Ozn7rA1grS
Opf48Wzf3ZcY5GMsHbxa49pGkHtTb+0XReLzNDSxj+SnE/w0KFzUaR6vZK/lj2XOY2qsA8DGDb/E
l8aPbEx69GEMTet9gRkgpORbMC7WcRYFVJYTOJ4c3WZ/YMAk8UGb2SQ1P3wEeIbYyy/GTHk0wdQw
qGwHm65GPES0cHkBKOslhTfbv3jWJ1yv6b4d7SH/4K7Rg9TQhEKdgyeoTqUfIm+caxhhePYFeM2t
LRqpxVL2n51Ypl9NDzXaBPg9NGCIkqM+9jPD4H8ydXwKzGl91LXuxp0bzBb1yq/vuYVQby4w0by+
4unIouy4aR7LztaGNMKywK22IGnOpVeCjTR+F95UtqFgHx+ZkD+1Qf12NC89hAPN4R0IP+aBGUYy
5GTbCroZoN28wh0O7LHfdSoH/WM8mjt2vO9VYpW+wNuQ5YRmS8Fjkde3pLxtfk9jBaFJey3YNCVk
x6bjdKJagWmxdArUFiUC9SH2DAt/tGTU8Hc7rbiNkBCiWeyKodoIynBjVYdya+Hcy1cd4dG+nbWE
jE5ezEojgITfVHj0fXuaFSQ40/czwA1n/zDlstV5amYh8+3j7lY3AFAqvwnu/8bhoBoQ0RF2IX0W
6at6lSY1Z7MsRDcoJd0ePf0THVs6JPm5Z3l8DAgQw4sPKPYcd8cfTEi6M8Mj0O0xxYcDxVu4L+0k
VhvCXxLLCUvMq28ieHIFWMN74GhKNPrG9qMo4bY+5ac/NRCpUcybxpvkA8XnLPx0miFY74PkwkT9
SW2WgG077IRliYsxi4+ijpJBJuo7hDHX/Ta0yT2T/ilWZMSjc7P9DpUbqj2r6nN+EVrDCGB18bJE
PLFBHUygIh+FM4T+4MiGJnml6rAKroF7sALc6c7U3LCsOAPhpI27ZK0k+S8y+12BxOa6HxtKOvKX
ekGdsym25WCKJ55TFttyyF870cWzi5qQNrnwgaaeEcbsq7ZrxOfaOvL95c16SiuauBp2hvDIbieX
SPZlOt8qnnZFSKnkDeFfk616e4L1EtB/77B3QVaG6P0gRkur1wKMRKo+8KxY4IyNiKwu/AWIm9q+
A5frJ+Au/KCKrTERAaWIxB1YXwsYcO7DqTjSvo/gDqPlSRRWG/Z65SR5hkb6ySKe38DHe0lDDqj/
NqT0rz3WTABeHQ/gEfiFh1hIHiai1+Y979/cLJuEZlU+P6KzUzZuQTvEBUnSjKWLJKW1D8Kt7zzd
3+pYXZyNDER3r4mEedK0Fc7N3whFA+KFcQHafkQ6Mp88HWTUMy+JEXo8VUhZcClsEzd5GzR/FNnl
aDQhcz5H5vrqheVJIx87bCS3zf66gdKljniET+uG2/EWpX0zDZn6ZQo2AyxrMzpBmtroGTqzcQKt
7+VxlVUTXDi/KiwPTW+SLCGf1v4fens3Vljp/Ab9jhvn6/JQqzBnMfhx4O11z1jS9hzxW99scIY6
uI9VlZWQI334HhbTV4dAMQKaeoVo0Ftp4jLGRaVPQhrnUCDSFHyv5BfwdZRleWLAI49zROY7oV9v
aPCMZ1avtrYOm2TNCC02U9y2vQQ8sidQ6rYifAuF/Tae0Scyxw5GUdvXT/98XSRrTDPmcov+fu2g
ZScAWkGwTlWaaTerB20zwwkfORrQdd6/dCPTU/vM8LczsC4HOCmwh+E4uz7zVDkdObLBX+gEYYq1
ddRhbuWHIkjuwtVoMjqWxcv4cTu2J71zDSVG4I8PoJfLFM232RmYO5uUV2W58g6gZb3yWKny2IN+
jvgPb/kG21I2oBNc+py4qYFhW2ax5FQFGpWJmHvW78TbDcoAfd+0YEwH5grABMLg+jzfBTo3Jr4h
TK0xHmsyBl3HLucAgBpcFPN1eudLJc7I9QORWOF30RsseCdAVqNZ63+wzrW1np99DlAZp0KBhr2E
nFWU4amGamOQPzpzrVNxpHXy9j4O+fxkJ9sBU0OKZfkXky5YuSzUegmANg+rGPqGUHCYAysQAUdw
suYVg8EGWCeDw7+zj53wO7JLJTx9SA9Dsbx/mtN7AxHdkeVZQvHzqqCr6uI8RY2WJgNfTLBLfrv0
RMO0lQ4uTPRmhkSz+uDocXEW//gkZwD1B78ADfUelSLNS0pPG47+ve5eAMN79iAfVCGLxtjAZKBz
UZKOn5davXgtu6hZb7j4Hl05ISWQ7ObKbrIQJpkLiy2//O6UVywUwCsVYbczkwkkxBTkXV1Y4h6o
dUZkBR2DVuqIeWivSV3tRbyCizfKGt3E6oyO4vSmYsqu4bioSXqJWL6FLfSXLWzNtQx9nHGcrZBP
AF3l5sldvowwVNbLRT+KOBIHu9OKthabI4X708SIRDrt/xZAoyGMCiiLh0fJivnmSPvQ4IHIjc/d
2yiEe314kpb7iSNBAHE4JDYxCEgl21c5SertrMUdTYhl2m75/Z1r2u/OtOp3GX0PHQIzeEc1UI25
iKRNkGoAGw7dEsVK/ww2cMHuvXvSDhsOrqxmJxyTXPwEIk9lIJw/5HoZwFB4hY2+N3wP2I/PULln
wM/L+0xjTN6Heh21XJFscQ4ZuQ84/Qjco9dg5Lk+wAe5Nmf3pWQyS8udyMdzloy+5BBj7oKVh4DS
1Scs+HjS0y3D9n3+gpaMr16jI1RpZmGxJpAiMqOh5Cap8IysFa1mStz/rDiaygKimkxMcCiksSYg
CFaaKF9F6T88E7AullL6FLuc1+XvF2nJJQujYK/xmnZts6HtEITmSESsjJkiciWHaVK/lEk6Ug1P
4I59PFlvHEhXhZ98stsf31zIsYTf3KwOWyEtMRB6Kdwpn2Ou8TEQM+8wQkcTV8ZsUhdOIv/24eHu
VpwT+LWjkzLH4Dkhayv1AKNN8MgtyPOBxgy7aO3K+/g3Ta9k2fLEZFyneuB+UpwPqphpFnNVhPh5
Fn/lyDF1v5Kx1QE3uiKztE4qmN1Q1hQK9yY8M7YO4UpIceHrwgt24qo+ffpchaYvrTyYyD9vIS1y
SCZ1hQkAqIBXQ1j1I7QoO93zmADmOg3oON56bBzshZ59NKpvjH6erAwobU+ubsEWX8qod/YDiB+t
AUTSFnNK7n0DDmGheHDes65Hry2lgisEApoqiLJ3fWZPaYlSHWa9rdbPihFbT3NbwhhCAiQ0S6Fc
fV8V/B1cQ/znQltssjk5qltHD1dgdCYL6QJA8LNaHFCikc6vHK9QOaCma3mr7OgCjYLddcNROL7T
5YJra6c6VRVaJPUZKtjnYijAGlCYU1fQ6xy0mMM/yeZq+ArZONh+ZClaKIDoz2GOq+70VIaAawGY
jS+ED/G+f2EqR55RbrsbrNxhWL43zO9xQXpiAKP5AYZH5L/pjlSbhqqY76nvfi80tySTrz3+z7YG
tBXT2hvU0nLwMNZzo6gekbYY28dkj1rYo46uMS8WSVwPNDP+lIEOhmnm7hht/dc5ARGUdHmZEbJI
zaR6mhdN/BSAo9rvxkrfjxP5BTfsA3vsD3d9TmzvaZLQeJI21QEtIzKxNyQ11nVdRPEEzvGs1qIA
Hd3rzQCQeLEpSsNDVApwOqxp13M68qeSYWJNjzwbdvSsKJrSLag2pgMY/zY45FlGv1VUrEs5ivMk
FReIBg8GkAVdrhRaNeUJqtEKqodOKxO8QCFUyfh1I5hmeacCnQr631Do17NnDTA9u7MAYGdaBh8F
GPhCNE2pj4K15xg0eJ6fj1+cnt6dLkrUQkAqJaUzw6VVMJdHoh67Y3Y+5kpW4xyrIWDG+ynsCEyB
afiej3CAvPkEQscbjoZYcs9vM+UHkYpgxju1SnPfIcXUFegXV+sco8Dryz6qvEuXPetTsd5dpXgV
6FtjTE8kvmAgklmitqCBR9LhXictI5z0jJ+f5a+BJlETJKVwNViBeT64r++Zs8Qeh54NbJiy+3JY
OUTZRGzj2hZ3zy2OSIQmyAnlHdVbLrnkLTquz6g1zt08LrYUTJrhSFqQ7x2qmJPnLNoPCSmv9H6J
AWu4At4D7ZqdSbn/v2GWuTZ2Q03dG2hJZXe8yCYIjRIk8zEOalM/AdEZSgRWs+fxaNGRE0+u2p0S
o+V9y4CDsi/dz95djpEUMxlN9B/ygGtHrYnmNYh0m6h0Qf3mmCz/gWeByUX9j9QqNeW/SC7D6NdJ
zYbbtEo0uqhyR5uVAQw/SyiI/BZQzNtQxUigEf9oS6vOEKKAyGUaQSGWgCqVOIo6XqZPVXrm2+7M
MXOTLmI2dX6x+MVEXXvlHj0LCm7DKHBj5l2KWFPTlLxTxmpVLiTVEaFeWN1TQUsfa977JxQwE9hw
4nICFW3gtmnCPuA6rih6HBGu1vagm+9hsJqynDV5VBpLDErYddzROMdZ+7sCbQZO7TCefPtd30IY
yAmSImrnnefR3Fi8rupzh1S8QjBNy3VdBgK0XVy7qN1jHT9aJT1qOrfIMhc8KeG/KXE0zSL4xGCV
aflCAkNDj+Unx+NYj4dlID1iLkIA64hMCF7D2/rHADgjsC/4mJbgkRBNmPJ7t1UAwU9ANJrBoimj
t/+K7Dg/DSTPXZOZkbrV177q0qSVKmmr3v8UmljHnpWLwumPlhWEUh3gvFXt7QRoyg1jSbVFo2du
aPDaHYMqkS0P2WSh/4ke5xxg7woxrLGlutT3MQvxSDJ56IrhtoUCUDPfQvmHBnmg8hP61eltcSyn
TWpyuXl/9ppLOcpwlTQEDZdJiK+VA4yK4SPyqgRYKQophoJLgO0HXLQFjJh0aGHx9p5anXGNjtug
YuJ1s1YnQQ05gCSpG1D0dnDr5odCnBaj72a0AvJaoNNJoUAOCRxDY9yoPw8mfetRRbt2VnS2yhU3
frjlSjkfEsYE3cZZb4o1vFjzVVnk6X+oSxahuOPhL3nnbrmUmvgD1Rb3+y8jDPG/otW0MasxpCOe
nKKEQRR521dRPERS0lqs1ckZpC6FMegwUyIOwcdEDt3+usQglDrZYXa5GvrnyrVk6zHT2YmSSEVb
/Mea/RHBoyxdvhEViIiVraJOed34BXvqH50MdXHqDSHqZdM2pRSqwDvjKOxF2Ewk1SPJpipJMfXh
9koq/4LMBocUBjaC6/q1GK+WHEitQgwLSm2Cv2nhUVORugomwvKUD6pFU8Zu2AVz+vsFBRau8oG8
uwtbbp70hS4cBpw/L/dQieA/q+NxdCayWw0gJMrxFI0W+OaLGLrpbEJMj4B/lY2uMU6yqNzwKYDn
2f2U2/fhiS3rKX/uQLbTBVAPZxGlEBsshOAaI02OvTx1zZW1b1mB/zHoScOybPhpKd6oY+LKnpNo
QEJ7itaG45rYkWdk7XAxz+Qz7zaf4G1tsr5QXmBc5T8wKDPz43lMnW68oTW8EstRflLLaHDIgNHE
hVDUN2vlTszPJbCLio/Mj2M+R+gonX3wmLrAEXBpzV/UKkklcDe9aHSUou7JLWwWoBxuWN1spa6P
NS/a6sKY0J65Y78JNkbxrfkm21fyYPdnKB3rJQOv/0STQk0yRB0Hivf/vL6Zis41O0BUqbkpySQc
hsJcqZnXvphEaHWEwldOA6rtXc+IE4jzT4nFq57d0aAistVi90KSYN3nLyGupyMwG/r5qU6BCyaB
ZaM0T2daILD1Kj6RiJJypWPElX3BVPntnsqm6BRsrW87SqBvjCSsyMOK9KCMqFyfi09YNrRcaaDZ
Qong9lTuEBSWejc2d+rDwTjPDP8A0PRGLFOZSN8ddy2HfXQiI3EwEa+91opr8wulgWnzRdwMRu5p
O0+uGDOie5nt2y3K5d+GyUS/FgxUb8iosoqJD60PAsewReIeSLBaRoDEkjuYDWpNDlxtP72Uv6ks
KLlLr/mCVnGkZXwhK1/iUT4G7HCw/3XAQc1a7iUQBTWwqTt3hds3p4CoE0BcX+RzJevmQzJhStIO
6gDK+Fdp/y9KHSpgUudLOnBgD33qXGSPb09CIs71ZBheiG7PUd6UBxXSdenHQPxT7rb09BmtIYdj
gyUKmV+Nb+x06KQpTDRrHINS9/mvvJlSZlUp90tTB5pKfmwKRU64UmcHtLBgcq+4F6GwYuuTT7vS
9t9GtouY1vUGHwkjgIyUTmu1qqvicD26i6GQoN3H06nrNp5sk6K1GklqmOJkWMyesTRTKRh2wqX5
qSWEYy0FyX0386byYhOzxEVPnXzwBXMSc1QtUmSW0ieP9ixWB44pEiPssy51xuKupzwET12GDl/I
0ekhKzC9xsb7Hr0pivUk7ta88Drr0Nn3hBAzV43idTlxnLbI3Nb24tv7NUYWYa+gQEffsLVFZqXv
r3UXXmpS7cBz4f3BiaVtAra3HYuma5ZlDcFTiNxG9axufuVW8laMoCIJVeaJ5vwy0KPk/RM+g8jl
i6VYRp2t8MHRNjoCusoB4/K/AU5rzS/va8nVvVdKv2XL1QOXmLYqUK4qhL5m+XqkhyANbWxRoU1X
B8UG76207FD4fJHSnplcnhik2BTNuGOVEQBDYJf+gBhhbZbxjyVpf2Q6VrDlZ5iezW55gNq0JrqE
CsBkTTnjnfH7Q2JHTTuimmMdnY3D4OdQpxDUR7/SgBgqvr6PQIVkqbaPsDw4u1GoTwyFPQ/ax6Ou
ptUojUKGb3Y4+PyUikyuqxkogC/PQx6LcvYzemFJmkSwC3LNIc2gqWCs5016wIXa58s0Biby8Lhz
2fqznbb3XKjjA28+g55N0p0mPC19+OU1EUDPIEK9nDuXljwD4Pj0xRLADmA6P2dOejzI4oTQ+CZ2
gmIV25mEhH8kwcmD5uU+ShXoyWq/wGcxRJ0kE4PCPYkiT0MzLgNcboibq7OG39HL1K7NuzLUlMme
Frb1hZbBYG+0wA1OPYV7ZFQTgkQyDvBYYhjHmyCVbbIlXB+7QVdQUh9cl+UKnTrZrDri4+SU8dAZ
p/Q/i2sF6KyrfZfDMDrVbZveqZjzqNF9OyV+gbVp77D0KZpWDSYvRvT1VspGyuvwA5xReqOtU9lq
iHp37owFEgyl5rDCjFxhNZyBaMoJoS2AhOzeTjcWQuI8w6BvWS5hHKivc6IdoM/94JNKG8GHTt2b
ryQLAUfIVqcNScbO3knDPBMPwT7CijrMWCl3EYKIZx3aFG4lOf8oGNFH7/XlrHohA0VyL3IqnPOl
T163yDF4TTtR11z22tU+DPKp1tBiTNISzx3yAIgAdwAAPW2M/jyVuOGsPgIeeI6MXI8237mMOIys
+Ni5f65BHRPyXDnSGhr38EJh9PMtaBN6zBwoW8o7YmneKYNNI0jaVc0GVV9YlDhj2rqFF8bBHlol
u1nNaxo8cXO9zEdnMenXfrPPvhdZSZphpDm1G048Yr7Vv4/bGvVUhkUVgQ1y5uSoJmRvIeGAdQEC
tAuRrYjwo7KWjZ2A7H0hwxVKuZmAxMh+YD1dMlnstBx8bVyxYvWE5G6oamssW0R+aJ8wtjIJNVXs
+B3w3tJC83vgIIWagRyB/dREsisaiyCS3RAV6LNIeN4GQYT/lUjIr0lewHllkH2if+hab5pQaBQS
tIWQTZsxKwrna4E3lHU/6O8a9vLLuHXq6+CSRrrxyoFrsNqyQEzhx5EbY7M0CShBUVPJXHqbp/3c
uSTRzgzD/cufyPskkrpvNwNickPHuxQwswul39Dux24TGTMEXZ1MJkLkPvaKHzRO5A7pB0wNElkV
RqUKaH/KNUbFGVKttAt0bE0/fE2v7izZaTxQyBeP6vCqYvg+/kGRNIoTWWaZbYHM4/KQMNbAAf+O
TGZP7vDtRbmKI24Z/LxO3E1zn7WiNtKFmt2R6tReuZhsW02Gisui0mO85q4MrrvGWyULCG+QKWXj
2CE+AqJFYs3iZh4CNUnzg7131IefESP3j7QFouPCWEMEYHqO3rzE6gM3LXYTvxp3wL8w1n8erGfU
qO+rCYrl6jQzO1YW0zJAvLcnZsYxikt8QCiTp9v3/i7LgqdhyJ4skiiuUE0XMeymL/n/BvSDDFUj
3eYtIn2AgFessbdyqdiWuqq/9mXz4A9wDeT3aoFogX0c7YIdVFLTC8+JauYGcJ8YK6/8bNGr8zds
KLA8TiLu+eSuX8E1/YMsWNmhXaL1NEy+mKRJs9QpqxciYph0jdfSSW09HCHpTTRS0MAD2u7UMPsp
CgU78gyP7iKSyttBSmYkjos1FefEbFsrxu2BqN5WtgHhV1Tg7ZOymCnOn0+gV84/e0/HWIUgUp+q
2ybBuvlHe3lqSt2olNpu3WWNnnbl1vvaqstsW21L+4EbiSFmSnlmOOW1CE/FzaTeZEpZWlUD0717
MB0M3conqz8oJzodjepN2Rd/S7vgQH9IUsQRa2Xd9Ch+IxWVdtOK0xS5HltYpGFK6rvQI7e7Q4gf
f7qNrlXtxSk11X07Dnf8i3KLLjm4Ek3Ox8n1yDUXT0sDvJ/Hi7n8H4uOfzBuG4EX0AAnTyahtQl/
dq6INhZhyd29u54kUNMyn5lsCuBCGy9ZMpMrd/djoYB2tWozu98QHqE79NPSHzXZ9zzObIhICjbC
0OimRch+f3omLx6H46pR2Je/9YhuK89GuFVXaFtjAuvoRelcCnWFCuh0Y89JNUJb74y5tbequmKY
1NjF7TBPypE8sqi8DcJkHS8spQWIawImLDa8Rka1Cxtlf18XokVj/HaG1N3SxOjYZ97uL52JRQl4
Ekyw+FZou1kvqFtV7jvpR216O5BOyd538O2LsRECZkBdhSDrCEFRThdt/UL0RltWYDzGjj+uduYd
nSIqql/WL0E+MhcDyqH/rqhY4uiSEKT4JkL6wnobOu5DyRIlBAs6cmkF4VG+RkQf0DDbyvCrjmJt
ZMJA0H8pZh0R5Dewp9w19YD44yHpJrCKU3Nn9CpLAA+ccUA+Vmim2Zh8Gn8ffjoqtDZGN9N8mVgA
QmoBXmkYpXXgo3hszye6mVQtmokkcIEJIwo544jwm3ycN89m8bKr0nf8yCkIK3+pbZmBBmdYKqtI
aTpKOai8VaIRRnGptTB4hgPWWgN1NPPTKA10nnNepPCZv19kQEe61PwU2lGzPY7XZEx+Rc6T0EMK
+iG2yUJhEJhgHLBQDnPRCbCmNfX0FUpJmZqERneBaqbVs0GichCi9Z6VhYXSSbQGnbSzvN9hiNRf
q/6X+T2+bJRBCSVqnSavEmDSt7TMTjLph39PI8vxw60ZHbqfKV/KqilnncgqJmTb+Z1gjbMa4+bi
kpkL9Cvl1iyNVCJZg9WCGVpZiN5HzqdY6zaGUHOXJR7xTz85yds9kxrQBweRdQXOq91wAstonTRr
KAIvbzfRMHmusgbLX4M9/3gxMCe4ww8DBPpr21DK/mn+JwdlFU5sEJMZol1Gvl8OwWWkY4oE0hmZ
AthYn5mCjdIuZt0ATu+FWCEziKT6aPz6JcjoJUffhJM76O6VQQe5PZ23Vx6GOUJ39ogLWIDTOBfr
MIsZcBhGh++vXWFmfEeKtqVTudlqSimWLetsX055OL0Mua++aswPzSbImc7piQTSgkZrKnxRh1C1
6Y/FvmzsXbycC6oZYjFLbpKpmKsSqbpQDYfVqc80O/mqmDPbXdOipxmt8mOnRiRo6szuBjgCpi72
iEM0wRL4qxpjIl4PAt7RibA0BO3tyxn5QYUR1XtDvcUnjYOXOkza2na0uJLJEqPShzAdPUHAbfrz
GLGTDnEqomRb/DmWOwxlUmKlfIcWW1dLPU085E+xQMHaGSQyuLL5V5wvbWxVPOLNJj3jGMaeO2c0
I53zbvCL4/VmbFXX0iWErEzfsjoptIf2u1vYmbBASK81VPpu7BQzK6I4On2Z5H9Bgj6AF9k7uAGu
S0OB+65YPBWr5GQ3Op6tCex4oZhISj0WH4zSy+/tOavn8gO1qiU1+Vm2axcZ+0DfG7XVoh+HwV06
9/ocwHW49Pd6WGwxO+HW9BWoNl7Xj6jfSljb3Q0LbF6X23BIts24HLUn3OkSMMWub7NAV5piQed+
AFWXhyY46IFlL0WTy7WUqQVDJ30oHYdehoptUZG9Ldt6e5k/wJ8uox+fa9pZ+jrRlMF2GP7CBMxi
XGKtgWEcRJ2BCv4Xf/mfWEC2Patjbxlc+gg5dJVfnCDZ/+DywaQa1fDrvy+pYiUOsiY70Z0PT/8Z
EAQ+HZXHL2cWi484woqJbXGyJEGp1lsv0BW7TDC7VSc9LuZMnA/L85fzCRzLMH6VgBUzshk+v+6y
Xgy2TG41TAeGAwCsrLh1VdZ27k+TLfgWV3h4ZveCWO2nZGlp4/ADvUlNFPKW9JA2yVTWnba93RxD
G0oA4U9366vcrezoZq3GatJ3CxEbV2FfUfxN0G0ivAtH0ynCwYs3EsqWAyd3+mK5SjUXV4YwHXy4
XS/bhzBjp2OWi6Jf1tC7aBOI++gC5iB6aswnOQtHjKOqlTuzDpZETwCQtfUFjfOCMtPwKEE6MzqX
c+9CJGfXDN95eyhUvH6fDLPraTk0eFREraTNYKIlswG79Jer62eQIptZHSEZqRoKuq/FwyG2Nf+y
Hwo9pnwVx+tFoT0J7dVzdRu+HyJ3RZe5VNpkqph24TWAJnwAVvQnUEzTMEviZXtdby6x7USw93XK
wAMnEGIf8pn3WevFcTuU5NNk6h0S5DP6BisnYtdODuAdezfS7ZupKXxz3cTZGT1lBSwNJrs5/0PS
CxIVlPaiw7Yk/MK68Y6qFOndnRBaq7VdU2LK9S+ZHZR9XlzIrTnyAGaaSyPFdsaOfcWUARP6eL/Q
r8glNIwlg2FSQm+H5fFL/eg3rNS27k3dRGQnKLYQngJlyJ/cr8Vr5gNI27zxY6J5CGCmgwAJm+DB
e77scygsUGahKb5435b12uQtrwnXPs2RGrCDgC6ONQaFyxlwSDmmkr8br5epSmX3hz4urOH4vtfJ
ZSsF6DaxXN/ADXWiOUjH1FM53eUAgB98wKpjoHQ0TDWJW5T9y2WpI60bTCtcgu+3F6VkyKDR1Kid
CKiSfV/7QuF47ABsn5EUJuWftpdGwSSp9B0RlzEak29/YMXjZYZXCMjr4GBkwU9HdMIMuFgxwuFV
E8e3l53U6rJp/nI2601UCKMa5huuCNWpnhewOUR22QXDWPej1twK0IlGd8K2l5SPQrpAzMa3jXG2
B39hy4pzc3fgqAVs/KTDcZixqKJuYs3gBZnjFHlfci7KOFBRIhqZdsd+gyARSg74tTD6+XPxyn9w
U9HUIgq9evGCPG0qNgePASCZLA6mHvdedkIh5wZcY+HidQKWG1yZdX0k0kh51yEVRzAEwAwgLmW0
qQGcEKZdD49myPIeXUUKXMMc+NximIpbLijj10i0ZEsKsiwendR/3KvSusjjorfoH0NTnmZx9baR
9saSDZw09zBtCmLkid4fAM6var9Q6Cu52/ayBJfrTPr/EA/kk/wu11V+/7PiOMoZcNSjvY73fLut
h3ajtuvPzGNHA432yhd7+155EWK6qYRx8u4fvA83T6kBiMjzPdMp3FCKaLigEd/phyraaWoems5l
kE/hrtjvEI3s7qXynhRcz7TlSvXmkgaXm7H6k4Y8bAn1E8hGKKJxT0rgPlYfg34MdljJEUqB4jkB
BfyIj18CZrIfeaSSzWCx0A2pq/AWULQtTzBFhMY9KTmNyJQgielXrZJzZOc3PQWRGSShvTMDYMWH
VWulBRFUE8/5LBVdkAIZ0hPY3NnBxkK3pDoUVgPqp0vaWIu6WRvY4SwHqUNFOUM+W4nuQrrcFLzf
f46VsW37qjBjimgahSHpg0oS9VMK8Bjcj8CTNs8tVZiEOBhMxnUj+aZfr6Y8tK4fAFagRuAhijrY
I05S3fE7Hj/Velzf3x8h0LuANuMNzmRYPbNGFkI6AH4Wp0nLuxSLILGUepnPikJtApNxoynxZFr/
bQYa/B1jguDJHJGPFlD/x1R3M7r5lpJvLRXAOWIeThC0IKAfbCMHj0MHucFO/r77g8x3DLCe4QT8
5e9krNZXW00BJqbdxcC1Pl5AwGD+mtzKxK1e+Q6MHsW2xKe3SBjG1XUEkLxedhaBwozidrG/pyWG
3MM9Uuw4tPqcFMRBwPTfNKpiCToE5TpQawvtncNdVOlXp38xYN3yiIXAtXvGoirSqrQ+z9M4hlOA
hrZh5FrhUA2pCVuyRX+JAAiDpkOSox57pDOvrg0BIZUIDUlf9S03fbAB+XTBg6065Q78s6AKXKpe
CuGMMF6ObVaCArQzw32rYT3VFLDzPLUcsiHAkXlzLindn2+bP6cEbYkcjPT906Ryha4yiurhY+Kx
eyr8ip7s1GJ74zTWF1CjmuNPkL4uhV3WyAIiLQ6V9r5hNloHvPLCBoTE5FoN0YRalAlAJtwRsQtQ
5XYzXtWCRZ5PycQXXWYwNcIPlo2lOs4R8dy4ssnOm1iPx8VMpR+I5NxnE0YYytMbJC69YXGPy3Wl
kHkWwaSZ6hlryTIxuTKtsXquiFwyMhOvPHuk/wz9p48O2VEfbLMr01XZeAYVunhHObUld4OuiMI/
k8oztCJP7hJtS8i/7pWv8Lcap3CDJjEdWj0HioxBf9G4nXOvEYUmm9hUxTh0XQi6ZcGPSYKWJGNK
rvnrV3z7SUWK5LpbcDgaHnUeKhmyGHdQL3TZiRGN/OzPzNNFykaJpHiX9Up7MK9qCtU4R2CNrWDC
2Ma+6BPi5DQo5G36IYGjGWP//OdD8asjX/QxH3gUMMIW3563pjmEv9hERgEMYOvHFjwwKV4qJ6/C
QZAB5PS09fmUdQneMN2GofzM/j3it6/C5Cji4Ii5AtGcbGXhkIQnFonf8OqEhlpBIoQSSgY9QBxW
xuMUrLaV/laHKjQW16ty+lg0KjiUphUP/EcQi+qHd9d3YbDzlSGpPmZXgylafRN9/QckQ4t42aE5
WnAY7y5e+V9LWwwpvcrLm6PnO8CIgJQIXGmEAsZGoENgdaLy61IPbThZauQ3rxxeWaqlBYIiLyIt
/ff9Tzh6yaVNXx1xudPNrBd1OF7AjFyM6ndHOHdl/9Z2wkTtzT65Nsm8UmPzZFS39bDcoHFA8UVJ
bqiecjJG8N66YHmdp0+8Rjt2J657qsrQSJqY+N2TLkjPXbj9YGZa68+20DVTCqYSnyrivklwuKrT
2OzAjoWoaNMfv3/fkq/kq22Wyf0gUzuR++ye0cRGUxrfvnLONrGiXTjf4IBvjCIYA7JVFs50/alA
hy7+zxN+N4tOFCnEpCxHQK8dRo2K3Zu44/aXOYzld39++Mtp33vSdxhlIbtDt0wK493mWmdxEgCM
4M2xchSkMF96B8BAY82FU6h0xhFgR5T8EKaQSW9XFGfFQK1W/nneFJ0Bo23UawTHfYJPSSTcF64X
41vEzfc7xjQZteVlYBx+jbWfgsWxea0DtBTRJHAy1VhScKqQ0d350p2Oo4We0sutYHLt5MlKsPaS
RxRRHFN6afVvo8SibTUd1TEGzUVaRVsyE6Y/+X0qnYsOI8TOsp8HJ/+it9QgY0PKOTbxgGSuR5CR
eKRIiscb/DyYv1NQkKEwkUBc6zz2rgoqrgmAXSeHJoVz6rsXs1UHMuErR+yqvdaNpyyp5iIFxUKh
CCTDogIoz5vpsq1mJ8zGzhVCIbbA83C7gxoYwtPtoP2YOaB7p032OwdQBKrugzKU911tdLmB5tk7
1k5dQItftZ2BL+AdDK/4H/2nkSIT/+C/AYiCJ3evbd0BnQNwPYeulBmLxHSjZgCPCXoRykSSiO70
JFNnYA2mcgDp5cse7tdB7y8Tj7SAJkTlFKCwflVxujYy+QlZ9p2kn4BZ8BWTdJH1DkYdF9jbtY3E
1KfD2FHhpNKDcABuZ2t6ufHqXwy+J+HJJ1tGofs4Z6kIV7niuDJl6ENtGbhHE3j7lI+5hRDahjBn
MTDmmylSN7gr+FdwDHVXwS8EXAbttXqyibs3b5hvqXd+IVBr8CQSj+n1VF8exU5OzsuS6984/9e/
/IQ32iGPhickx/zvdJoHVpoib7VcLe5RzkV10JxC3kkrFuzCzvx7e5tR6VQZqHlpX0eeWaY0/T2k
VVNfoI1EThewhuMbiRDWMzkonMN+CrCazmNOzoTzcz8bcNu2gOtog/IQBmUjclF4S+BrQonDcaRu
YcX6QlbYorDM8FCzPjj97d+HOhlBWsuGvz6fBt2b/J/RWibc0jpbgkHJpZp0IGdfqXowfqFoMQ+n
YEnzwMixQAJNQIEOu3nO0ORymKSCJLy5QGfxHAlet+MvF/DXy5dQRlll9TrplZi5NlOtfTHR0yo5
4R94vC74eTbNB+mv18jgQk0G61hYsSzL7FbF0L7nxM3zeLPzOoHxPICXpdwVRIf5t4tXlc2oahuP
Fq/rgU89HUmoQINoebVJKS5JpeFolWyV4AOjyCqbqvUA0KmTq+nX9u5jUWra6loZnCnSGr1n9gU4
Gxw6jixrHNWPui/BPE0hnxTe4Xpi+CpXXvvDo/L7tc1Obm33h1ZRPLyxs3mVKv6A5BE8fdrx2Qib
sXfsdADbXFQWluTBZwUTjcT7XRQhVoMzwlxyxJOxw5jyRMzpfd9pfqVErBHZp1kN6A4yaBxuGVf+
aXc+ILSkiCCSpdkSgoHoJXepQRRPc54muphNr1T/AuRHNEd0ZIgdzzrAGm7dIeefJU9r71JPaZxV
w1ttYO4oHQKnDK5ZcP5m1k98rwphH2j/efeZ5z2FLdmjycM2KMNZIGiWkmHgP0N5jrIMYBubln/T
6rhDsO9bZG+m562FtvNUTSuk0Eu6hD/XRglarZZPPThdelbXD4kZuxTI7XqdYnqPMnvo+vRhmzR4
SJjpvssD49oweHdyalHz82L3CWiiNovtXNP8XVjFMV7u5n9NKjNxaFVprZI0ZXD7LgSCdez8ogab
XQ0k/QGtaw91eH5ZVVJmxLINlTDkYZhQMS4biU2/1HaZqoMst5kD2HgG3gy7W3/vKdx+dE3kMdL/
zXefagj5Uw3oEHVYH2f2AA0f+EpMB5FDzTldWDsBAJn2pQ3qUs7H2yw69Sxzwt2RjxQHLwJgoUq+
jTAitipuUcWN5mcep6AQFJnhRNswQOzsPm5CF1vcj8IaDEVvhPNXlJ8tOpUWpykFQ4HatC5gLT6k
5u1EaFKE66tgFcK/R5/bxIwSbSmuwJ/PEqTQSkUHzHxA+dp1ps2nwUUmO/Fs0J2YErEz+GuVo4Fh
dvp9ZroZUSdcVvCQL0rPxFMaQwT/Fi2Jmf0+meBbTI7Y9kmtYOJUSLxqE9FyUYh5maMZY+OlhpOt
oXw9UN2d5Ot4stFR9Fxxt3AOLdvq46lrE79QcAZBsoy265f7Zc+zibQHJ7nuOFue66sqdYnZs9W0
kdRbTuWTA4xmaYsZVo50g6NhcZL7yYQSCEzRmGifiW7jKt3SjE7n+XBgznHhSrt0qK8KXhFY/Ce/
tnWJgdYiePnSNbzGjqmxLgUZn+9hkjOXeHfSLU4Q6qw1rxlgJG6Dg9ZDcldI936U1ayOOXfhJKEg
Jy2l1ToF6PWez0p+dcC8/F80mbsuQe50M/MRQrE87m43+as0qrL+RB1ahpC7IcDd3QTlQ8i6LOon
LZh6uks1d0s1J7wj834ZTZ6ieHXFmHeYZ+bV8wngjEpkyyZDSURhk8gD27Sj4AVeLIQIzTXu0+zN
M2oSdZc2Sqcb+tbrTsY1DKTFK7ypoPmbcWr33Vlq8k8GP0g/ohkkubS4t2wNe/hyDGuxT0MBKiId
xWemjC55D+qXJ4bqUww3PiURWJkdsvMfssHN1Dhq1LPfL72UdByVnKrjxIdKefO5r96yPrcThiDx
SHgifEmVfDwthYYE9OXdcwM5Fr2UMCS3EtTGHDf4coWG+GJLuv22IxRWYZ6DVya1vT3r5cCzN5rs
+5b7Kih3oCkBv+ksNdYPPvDgLAS6VEaIp+hyYt00/WeJUtn0L7BL8wU6cUpDiJfl6ZJGfwqjW3CU
MD+mhcz6gYkfhzI67g5g3PAJVpBCumnyfHrHhyEA4VaAsu/y8HBANYCGOHh9iYuJThaAanulYN6H
+Py/P0fvWcV7Swojbgcqr2dU6hrZV7wVSVKm6lpkir+gSYSL2VY1/cseLN72jmglD39qx91fDYvA
d3PKVZIt297CZivw+fnJR/sTI7uM0d1om18Ongva4vmj7RVJWMkHgLwHYVqH9H4m7mI0yZzvCtif
UP+LtjskUraUbxvddMn0PmlYBLUkAwqZNDvSLQGHKr48VfFIuqgyyGRlyFNjDcyHp0ck4ZTg2UQV
pDLOORODlkqmEq++vf1GKBv4ortoezSE328WQPA9fCyehWm/IEKVPU3uQyLjlZyDi9SvYmK9ripL
N+fhpIyu5JgYBOQ0WraULzjDH02/adgKOq3cljIhsEdpvNfUV8E/ZP9pqlbRlX/+gTViXFjPWpGd
+pBuLZCKo9zeXUUoJNVWaoVWKol2wHltkd707wq0PUgwwuyAhhSkalT5WtaeHfTrbbUdRNAGsNx/
lCykjcGfx7OmoL5siiPspqUJbLxbkz6Vb5ENdn8lAaKNIddRdrKMmPEf7SA7g1EmDbJ9/KKFZGmG
/wCLSqRuPV1+V789m7k582Z2zdmBcqiPW+NFH05BRyREEoIb3CJYgx2WBxaskI8Yz4XN7V4+1Cmx
oXmSX9bah5MVjuT8g1ox8iOr2628l10BfNrdPsu7ZxG9I415bibIltrp017S1XNsVp8pePUcebCH
KSfLl3nwsgnEeIEowMXj8i/fx9Uj8GvbUSmpiobLhmwOR0/ihOiYA5rDHXcVIvdIBKcU0FiU+v3x
uFP5kJA1GoXUdoX15GxvIIWBf1f+TOL57C1YH0rzdEnlkoR/thlxxKj98/a99G9tWiHzEA0vb6iL
IEpRcBdSNDmgldp36NoyXvHAXttKnsLx8qS3iGjJAN/KaDKkGzwnvJ7QtUBpLVdtoDecxTGvgREt
ZSWc9Jw5Sc5APCN+mF9GVOY9ZOm0qX8vkQy2uDdEKiTksbPz0BHzV411l3g/bXBQhcznnuesxQES
iSNi0T/3nAF523nSSRML6cKdP/TC4Jm0Sms8FdKdT2qEB78232HlYnfl3PQ+SzJytpQIsr3c7bDR
zMcEbKkK9cFb/vczltWktJt1ooegAhysu5frwruztZZrPMvKrWVg/tYG/+3ZwGhBfdTlJL2CrOWy
TDGqp/TEPrLSBqF01LYLvN8mumZEOLDJukzak9mdccQ5jA2cg77/V0Q1p9rQtJjWQB/FZY4YQc5p
KGsf83zoyLZ1CcX39kWUygpdf2lC1Kk1WhMlF1zzim3j8Oyu7jp65o01cQtEKM8mV2kNCScLg3sA
JPHCRLyWThKIhjUctsM2k0lgMxiXzYvy7jKC5vMBA8r9hY306Ww/+7HlGwVrRzGJWVlFWuBYVg5n
sFR8yc/1mk6FNdhDZoGQJywhFko7GfAKm/4L8L3RpbVIhVuJ2sMLaBa0ZbJCjmdnhCzpZnezfGwZ
XJ2eb1yc7gibeRJUM0+ZGBIYUgCE2IFQhoVoLZCJfd0eP8TEHnLq0Of49UfDOCq4W1nLzp3pUjSo
cnU8azbKuVHK5A2/oczIWZrkKhHfToYS45wnKGHZAXaiN9sNK/nO5LP7klcfSXU8EZjH3Tf0iidz
VOOAn2JWqNLGGvFes/kf5Od4VFGyo8kmsO76ZRsLkKA4iC8Yn7C9CkVvMVNQImU8KcVLVPRRf003
owBiaUI6puUh4ua4ZE3nrwWBbCSq5IfSpj4MhmOcfQQ2V0bJCGYdxqEiJWjiiMvjvgQSllvo9oq2
3kiLqc81yzyrdvj2vTa/jxFbHtNj811g+nAa7i+xeQGpuAae+yTA+p21d+t5qClZLgRT+2CPLzu0
sJ/C4iTbGhZExtBDbw+3ETiyVF+EtSpr3U3U4/PuLXVDnRi55iMfIVLtzMccfl/OAxlJydO4afTA
wpO+Jxw+nep4ij8DREEZqn31fBp2tYl+5wl5QSVrdwe4AAklrpgNiZXwdOE6OHosGA1+uENqoYQ2
VpB+5DDLOgx7ShuJvBraAMw/h1T2uGPAr66YZ3oQKAGefcJpF45XeFPmm30S90atVTdxXVWCxwte
2KzfwP/AeXNpj/EnOWlLa+fZ4ddt1v+K1PogaunfKfLQpaNEA31Ewx/uAUV34dThC0SMTmFrXX95
CvYXqGDSFQLG3q8J8raRFZ7ouZZZMMyN/RLwFzom/UMUB7Y2aqW7a6RjylU/cVg5cKbuplSB+x0I
Fh2TmXZhqpn3AJDBWrrIZ850ij5mcI6sZBd1G8g6uAzXg9PGTwbmMZefUqwu8dn6irb1AbDxtQCL
cGI/8ETm6VQTeyUvgBosgxKzj4cMxmcv06D3GyOTk7HdL3VAJ1Sn/rxHqRm+EXuxMuOeQMfk/q/X
EeEUVZFMoYdctSklvsqad/VfajyY3LakILZXz6l4BFt0QKv1BcXeJU4gLH+XybCXFrtsF5xe7XCJ
uak/MEbQvxPFXsRqA2X+2WOwwIaCjpzapgcNCf4BZ3RsL7SfxGGzkAFVrF7Wqm4VuSWFmfBjsCd+
quucVsB1nu0VWYxoXmzKOGNLIKZvZwL91b0sDAt5vbkqPGvk9U91SWLk2QKNNhMtqxaM02EE7wv7
mEPvmbzcgo/AgD6QTyjMkH9OvZhnoc9QK9zz6A6jXjeiC1pjHa4V80fwmt3r1QkYB7rbnL4BKHNk
TjoQlk6fyXXFad2g/E0rOP8NhW1e42MZrP4h37dCzG9XbORzIwjaLbjkPpYHLyQ28vJxKv/XpkqV
oM7B/g5waXCSCl6ishrZ/EPiSYQsSMLlAZi4/hA3PkvfdfUno3nkTboZkAF5oAMPKnSrhTFlxpMJ
FP1HrjtBZET4aEQ+Hcpl8NglKo1Hj006DX/sswl7YpAxmvTJSGzbIFAE7pEtlJi4Jn6EmqIKYUSZ
eui7f5EL4OkHYjxPE55DHek9sMil+bhU7eakWkKbrEBF/tO7hJHjoNwZN/aNudRs+DGBY1hLCZKM
m4etnaKMHZan0SIXZllLYYA9uQGUHTSOckZ1JFdjGr9zunQ4Y9aZbApgHpcXvScv6EunFWwb5ixd
RwsApCfuLTyrBOqsLQbPoV7NefRfe6pEje44vu4tQG/6QG3o7PMnA44Vf6d3rp/aLCivrYBK5rIi
uyfCyMYrhocTFldnTrWpvTUdFLpVuKTLwBDVNPAn0He1iBNwhMDVBSpOTvYRJhnkKtXvK/OAC+hB
580pEHSHl+QyhaEUL/Y5Cf9yGuWN245TAHPyUfTjlLBBcvjGXd7dN3Yv68evP79zKDCLfvVK20bo
xq0NsjU/CKfHeUin4xzmk8Z9LxOpcoA2aS15DJk9PtyHrzjhQ+SoS0+cU+xjgiqJnrPSqihIAJK6
1HtpQwkbiuMSTMD9QogHumjNr2pGd6RjEJF287Yyfq1z3if4HVQjJLpfSUJ2+prm1BCF5+fYXvNu
vMyjQ4EuAuSno0wxyZvz3k+K9VyEcRz8H8AHKlDnwu017pTlO/ASWboaoMlIJ7jtdt462hYqF0vz
qDyIVnUYfqw7CAS2kK6VNnuFYfPLTPSe1ceueuECaf6bGKqo93AOcBqIQlsPHw5WXLvxhXyq+N3k
La97/UYFW4g3WLSy9GRgrGtddj3RA3MuEfuYb3bUDy4Xe+Gf3oq2u3gCP66/ZzORS7Nd74rKEaIl
JJU9GNmy7fLAkNMuLHSGBbTb8xrWeip1N3sVyIZ1TJDidbmV2uORgt54IAEntrLEGMRfKN1V6Nhc
lrvBXoGjVnKU6Zu0KbiG9hPIsQeLPMOI4MoHezTXjtakVLGvTF+JNFYxFBr3vQEDn8XfTsCynQbl
TFxJ2nb7lFsxjSKF4cdX/f3q2jYGbw9lkboq2xosLdGIg1pBZ92P39AInjFil9kNYL4qHL4OEthP
+fLGiINoqdioNxaTBH7tVsO5bdmHTNwMmMeLpAHVb+gZu9Z9zsnZFtrRWMbbtgwK7TaaH1uaIOEy
RFO951MX1B6KOarxbCWRjFmOLQNOYyDfYos6HCQPr4JROO7hirIzbnAMZkGSs1Q9PEFc3k7SyQd/
RReQzMRaRfL2TxF7im7nRgQxmg88sAqKa+wPIbst1ga2OZBpMdMy9/s++wQfLe0Q3L5F+Z8tixAH
DRovO0Kqqv6OhaBsssstKFjOaEoQk/YL/MMB9b7PZgCI7PDcn8PCStTMKqv3+IDO24TkLrhW6AXU
/hcBW3Ugw1u9U9zCs+WEw0vXtkTPG6ToXKKsEpNMrfE4nbG491nBTweOp94YYV4CKcYA09kpq7f1
qgocxBiqahR231lHX/50YpNxPr61kD8ZgMjZwiR/QvUT5JE/jXKHS/mCJ5fYDY6JvytMIiWgKlCV
9uGMuBi0duQkVLL8S1xTtxUOb65XXeyvivV8u3YyaYLf7aHtlsfsjYdU4XU131P4XG+l9rXvpylA
Yv0OTX8dTLMF8cS7M+URPlZbJp1Ag+QiMuK5p3GDuKsZRHIQGAk3Xo0d7f+Efv1eBdvgOya0p16r
I1y18IowJ2cxCScFiMtb+IPEPkU7h8/TIb2735dkni1MJRODjw9yD03W2LTEj3sgShzc4E2WsnKF
YMM5Lp7uv1+tU6ZeTGiLTil9IxgzbFsjmUY4+ki1eI9hig1dO6pFOo6T23UDIBJ9VPNZVKBw+OPc
A/3+Z4b1Jg5Tce5Y8yB+J83xkD3SZlL/j+5aSXv+HqHxFBmlpiRpXla4XL4y+gBqbKsg/BvLhFEh
22qyxMiofz7RO26rnGaMWXyqzwHefZTaW/3yurSvZQqd9f4k0J0d62jlT43XjIcOx7OuYRIbAkWh
E2Bm95WKvvn1CPeTQRngDgt9c+j0ITI/F3gk+SpsUquecYEoF7qt5jn5YLixXfrNPMYgZXFBKEhT
7oaUitoXwFcoN7MUlS93UXObxxGONGxJ6Wzf3ZvHMyBbR0OKByN+3Cqx+iVViROnOZHN9Y28qcPs
WvglIntpNPXSu/dXNue4DKF9hdfPQNVcvvo7GpbA5qqfCJ5Lncm5/3Xi/XdMbBGw4wMn/xChuzKw
VrlT5NsLay3pkunyNUUfk+urwxOebVJ/DB4G/R8ZCe5wJ+Jg6luHJcPZEcdYUnsajpi5BEzU/2af
nqvq7MLPtklOyPwmLYzHI2dwTGD9wq5Jmwi8ubKKsZ0XzZEmeFZFP8qQyG0xzG6BZapNewmZffch
dJYSZf4wfWC1Hx9G7a6rm6WsSyKE/pwrPF9KbRFgzP3QfTn8QBDuKdwWzFI8ebLRbdJH4wuh9/xv
R1U07zkqFnPVhng150JBShhtQJeJXJwyfIVeMc3FjTmhKSmAanrqfDbM4S+a3p85uBr1upssCJKw
vHlL9T62SQw1KEwBJdX2FmBcKWwui7ITm8Zo8X/Zs6yfiAMCKT3OpBv8N0moT9xYv8g4ZaXQ52oB
qkV+WMVPW+vRrhF2aY8fWSiXotMRmBh3yWP2iVCURTamoOIA/Sul0DJI4wTODIFUg8xs3QpDBKqB
40Q38hX2jUEkJVlB9U9+tf+xgHIuLstHImnOEeiR9Z8JCC31kuKtJMJHy/TzP+OzRno/OQF+C0gD
5svYDsZx+avSTkkR5uiXmfp3iN825VwZbWbDU6SIidMmw9Fuu821NgchDWnEAoGfHnhGYbmwJbq2
pSSffp8LujEz+VcO2UJ0pZRcj8nARjrHt6lnKlHCGaDNDAdScwVhcR7O51OXfQuGnpeGGQnJt3mb
mMCHq1XvVFVAHeFDsqYG2M1XhPwsPGQWfFgDowFLPUGE6iB/CU7ucCpttcRZPb93cCcXvL9VJJ7R
iaAJpWbpQBS6R35Glq6pMOA1GXVy9ROAABsR3HXgNgAgYjsl2lISdPCR7eW394Rx7//G9jQ3nL+V
1lZI0s00fapYiQ4ErkJP33hIkY0XcxMmK8dx3RKALcQ+pqJZbd3mlIVow0eR6pHIynkzGXqPZ/49
/qbGnxRpetp/73nAY3a5Cq4rhiB4QwvQ00Pf/+exu/KhwUJQuk0FsF1ohj0HGc5PLDlH2gBUjr1H
uEvOtmQonXE1lSNNZhU6sDuz9ucgk7Ud3WT/qrTg5PD8x+fL+Jjqa2dIXCr8cOuSaqAO+PnQYXq8
jzfv8vccXw5WZPCF91wbjreGnNPFtfu9kkus8OTj+DMfG0uee5gYujLLFm3bK0mUgX8nreWIDDj1
1D5E+uIdbSrFBxSHdDOSghpwnvboUjgApkUVJ2xsX78zg7Pccv+Rix1jx/Ej+uzWe1RNBGo0MykX
gxOn/Mx5F6xMfViBIo6p9JbsPR2yHI9VssbdG/WOvJ4tOQ6UsR/ZePpFFr4+jzNPF4wMZRB0TKff
5nufpoAbtukk/eC6h4bcVJU6bd2Rr1eymIuk3tKTPGrUic7QfzW1MPhY5QBoLjpJ69DvG8KqLSR6
hEfnhwLJmXLOmxANyxdRoO9rfSTfoQpHxSauKHnttXiC5nS4OVPDO1HbMvWNdLoL462jOmvq5pvv
iMASEk3DDVOtbU0ROgRs4LSMcrkV9NR7Pb/f3h5b8R49rYBEaE3vSNLJT78h1+ahKselH3WtdDeD
bosN9KNgFc5YRneZXWlJnAeXm1s/Wqe1cpTgPHJOkw/5aQ+US2EUyqyyp7a4Q2wM0qL462tp9JGv
vhOHqh8bLSLgFNWbD7EOBJzCGPV4fWN20IoY8JU6hUQGSAev4LuTPQRQ1TwK9A8W1I+dWNR1KH0T
rDYV2rWdlvksKPsD/S7mDqFV58gFfnGVjXLW54GazpT3kJtYANPcP0s5He818a/VjA5uKRk44Rpi
sCj+NhZixcx/BDbj5M0wDntb91y/Ai/fiPGAoTjyeXrGb9yU9mtqpaVkYU4oWU8SSvCoQg2Q/G0p
3EK0pkw38YNyQW+2rPjN7RssTMeS7NXW/PQ5fr+WKLY8zThrfSDIp6J9gSTffC8jSpeFXhgUI8O5
O5HFmWfJTOBNLXUfXA8ejgcCIMYshmuc8KzxijirOa4TWf/kwNMnVCvcHhOK975WR4AQOaf9sk+J
/niZZ3dxn5PAUZ4yA2N+QKiRVQolx5W8KnYDMtKzYL2yNep8IwFZFW0coV+S0heq9mLMoLX6DZe/
ijm0/h5SKvcq90gFPxNEnLHXXfeYJ/0C9PSjcwIS3ze39s3nbdWwi3rdKyrUDgqQZNdGCLFpKmoJ
icH4p9l9mZayN5aSfJm4KHPXVjZt43FGAW8kxzO2qkpcMe9zGJBOyUS3KBdl8z1ZdBQDKrXQUi/2
K8u1UEU5j8DsPxUPAzplD7DQxuZ4j+h50IkL5TeUs5Cez8WfvzLFNZnfu5tIhAOVfHjbVjMBhzq2
R7c8ltIS2VDYQyLGk2TK6RlhBKizXE0TaxkAdTbvYcaDZjJdV01FhWNCj7hJr1bg6GGKFr4321Mc
9W8m5QmGhBPEuhJ0V0M2QWXYNPni2lUSV9SjDlaWuI8YGtxblDLH42Arn/IFJ3yqSN8NVhXrayhA
QIsnuZENTJcM+mnZz7fRnpdvweOuUIMl6vErD6uchnLrcWwElYYm6fKEAZ5D66CCAO5WWXAjatGN
WdIUqkooGFf0NJY5+YZue/ySqaDBRgDMPPnF3MtqnfUsT44AhIHzGCV9fsDrS9EMrLQ8oSlozxwZ
+2D6pyrDU0T1kBd7zcW+6VgRxHT6RWhl0kCcf0mAsx6lup48punnSD0Cs0i7X2gqPcvO3I4pIN16
BTnKuyc7IsdBqvnVt8E4JQ3fuaTjt0+0NgvV7CNA82/eTRmPjna7DZZFAd9nkihiro1Fiq171gA+
SmrrJ8SXTkmmZ5wPxi/tn8lIvzjySWb1YYV098mGmawCvM8U4M5XJTmYA2vZE2Bz25GWid2vgguP
TAGCw0BgO+oKkCfIfyg5+3fxLVOBLNcqwoDh7p8FQ7+WDnbs5AqfHEbprLNChVqbvK6VxQOHqiVp
c97thN41uMxOPWADPalBtye2paC5fh1U2afW0DMAMDesrPD5MgmtLF54LFARaonEA6Sa4x434NA5
zv5OIawOK4jkZVQiqVPPIGHxMjxFVgxk6NfdjyCW6Xzr/mxL+NBtC4Pe4yFpeHd1us4jloLnSIo3
B8HlyJyonZ5OCIk9MYaNtfEfSLHsBSiMLad156JCyMU4w2NCLitwm5uYNSdP0KM3r4qibf7kJsTw
Ri9Lfi/aEB116Su5Is/lubNZgMAbKQMMlazT9AHhzK39cotlGdaBAo0UpijxMtcrAokHzWaJGjjp
5i6mPUU/klSjEbh5lStWcLAXl2rBp8E1SP/1ZodZ1TLskn/yEB57IlK7OdRbUf6TsNpMj01JlqO+
2n+OunnRfnUHnWVAQo7KTV7FIDMSq3aDFUO1+AE3RD1Ens1a2AsUDFL73sh7oHpMlGPk+UJgAPph
38p5hXPmaV8Mj74y599ltYFe7uMsPQ0tANELF9sc53Bq5WmKmjol4ZibrHHh/Vge3mGm8nto7Pse
WgDRh8FKNdq7p8Gr3I0Mmd35vK831W/G3Yri0TwUZUHmDj8yo+jvrer8ypljF/5SCVW539i+kamg
OUl06h+OdggV2FeuwKLs2fuy+w9Iei5xw1Nxtdi0KKtPOQSIOfgGm+Ksl7BhlRWwYqO4UNg1WbUL
SMMrR+IfJ/MSb5IDEcO8r+MC6s3rcDZ6cxrT1f1ICtH5ew3VXKAxTrpJkg+1oNeJH5KnXG7TEbAn
x+B9z7k+KL03J/t4+ctWOVoieMJVvMCkLZ+HgwSq4DSuUh/lPIHsUS+R1nFNKEa7KS1HtBjo8WsP
mmApA9eYA2FSXLWPGFRA2rEUj0fEvRVxhuXKWXpsAgm58sV5D7nH9h4052AvOjDMW4e3m1GJkZK6
sBJOMbM3ZuXTeh5YE0KHEWNjKVJN4ln4IZ2/+ja99ar3A07HCAZ0l8wzDn5rFLU7JIpBxJ7nl59D
AWPn8szCTr2zkssfJjD/YShcm2g4+9Ke3yU5HOyFBa9pQBHWwO66n9LZx5qxSAMyamEJqUcROMB0
dOUMUXC0jTwQne3gc+PJeCWHznCf00Hj1xytnKFg6uS09YVmgKZXh9jpX/5mrNJ8oEmmG8byECkx
TsmP5hoMYSQTx6qALIIGzoCDkGUgOel5DXbR9pdpMj2TNqcjtGsxvBaUMWCioiVxRMiNP8cyUg4J
NjUfswPeRBibtJhQk1sqjks1Br7EdmiVZlqyHK2g5oxVCLyrgLuzzukl1dQp1w+72ILZJV+P64aQ
vYLFsTdn8MiQ8SF8GawOcfBBMC8T8QL/Rx29/wq4g4eOp9wwaBEtP1YHiIGBz9VmJ/MVLL6ge7tX
G9VSJQzTI3P97yZZOgMw/Ii2BJnTtcFTJfGZ7g0mjdvqoJgpGZeqSJVmp8W9BWMBfZUQCbKieM7g
1RuV1d/dPRE2dHDrQBE0C6hMHQgwPD6DkOdvQpbTmpJOQvUQUqvUmQotLzfN7qp0xpoj6gTpraku
tZLjoN5pV6nT9PAeNfNeQvrpd27v7IuxHFnZ0Zh4aAKLKZYlGIwoffneYZaqj9SkNK1F6ehUgIbX
T7mnTmmecYZtn0RojEs8tQqDuYm0mulhuwIMxZwJ1JJnxdis2UnYVZmpn9Lup1aVN61aSKMU9qWS
FE+sU4faVrtfA1twESPh9YBRfdKzLtB2z3Jhrv9PzVUPwuEjqjpmNRyPHxQ1P3Zdx+u1oPD5QD11
SfE5g442wdpgITejkdBJX4btB5XxRWBoWdnJlICy/mouegXrLKu13h5JFBz+Gt7DIlusQ3ZjfpsF
mbLZt8AjDE6/JgKlWrca8Emwt4U76Et/1lk+Lkd/ki3VB6O9RP2JesQD7LU8xg459/3gDzuMgHzL
ANU9h/UI/T3s8jkaBA8/P/StZFKlvNzF58nWPQy/r7h16TW0VlGwtmP7PvVLH0CTGaHeXbV7Zpur
GGUFqKXMSeqTA+LtknjKNZiBpLhSfQ9Mmmx5TN3k/RLBEbqOi/ktz7SkUCeBbjaIEy4htNm59jqH
UxfjeVSCeVZFZctQ6JCS67YcY348nlm58RHk3voOkm2Tu6zt2kqKVsZ+AePTBD26F/8c7SymGELP
TR6FwwFRX4HOGNgffdCEKzIYFxuBDz6fUfd6yjN4N9H+nTGTvj1k+LprthvODur20y0syDgVk9HM
57p1QfNDofJNDoBjrLkdhvgnm25M0T2vQcJz5LsfUxvQrO0M9ACcpD3GbEd9M/+0CJ1dIIJM032g
KfhVdnR7jeCyl1OmbA/efEOByFVsXp14WZPu2jKOwR0dDQRi+U3eiUFFSPRDsJszlqWQEmWaCpIR
3ZcaS0nB/XJFJMqCH2t43U/PjJYNoBXcFDBXvzrhX361yNPxKKqHDgJrIuV49U1wkDyGIXouc0Vd
Q3oC+RAuw7eokaVLTXXwpth0zbfImmX3472zor+gJyLatAaaYAquRSJDA7S3SSgFHuEBkkaPWgEt
PYMtiLhxzF2XSp3rylb6BPj4pYSc+bU2aZSPNlUcgzyq3ykc3I34b8qpbZSK8o5LWfk2/FEl6i1B
vWCxIIRaTgJp+jIQURcxaPz6rSFh6YuUhEG5bOyXs7FOpEx2ws6W/tEN/YcZmx2AByLtFhdvHRwD
XT/9k8z4JE31digNxjOTPR8+XIiGbY219B7vj4A8NzOHs3yXQIK6te0ftfwCes36Km+BbfVI8cR7
kyhO8HfAQyAOuMW33UK7cnp/oBuBuK9UBHVKLGHwj2evpNwYQt0mmUtBxe3KJgCSwJiyiPPXMgyT
WXqu3NaCh3zlGMa2ZxEIkAr8uICZ8n6gXJWzgzqjzFxbNI+Xg5JqNRVFREY9MrhYMEwi7nw37i8d
XYTMdfJDOkDUtnDOjdQvo3eWX7zjIY8KJ67OS6RvGoIhPqbzP+VWfUoSenbjbJOptaoSSpmS6oxf
R4U+sovCkExQOS8/LaZQgh9cOcATl5+N1UaPvrnweRsNH8gQs/kmepVfT7mtHMzRsR/2V3SpZWP1
A4R4L7f2DhGn+8kJr3TomeXxy1YeyrG8M39V+qHL6PEYdh/vbfCM8ae2GO+t4nfQYH2gRx673nMj
+ML1yHUm58GLhjkDnHCRJGskYaC/uGVn5SCu19MwYsWdtd92RO03VzK5d73ncgzHvGVpaSeyfaPF
9rlXtWg8o83Z86D6mjdeJgenWEZiFhQ705x8HJQDYEuFGTIQ+LMmULBZTrduW9sy5e0lXF8JTEtS
YdwYaGTHLdPPFfStQFB+gYBa8YNAqKhQ9320dd99OFV9+fh8fjrF1tnxI4/lPg6mwugiVrT9J0fN
bqaWD4cK/WQKKMQ2bVvBv9VTzuR0Bj2leVoJTaXF4R+Ys0L4tc3G7WjKZsWhxfVzWb+TQ3+Gb1Fi
qxV+Zdm/7Z8VyGyPtgpmuw9LBxJ2v74Cx0aYaBbojDyktKX6xhgFctRKNDw5NrXESQfayHiMToIR
/mWmiUq5pjIDR0MrCYlr1kuWAD9WvYFDsl09lWy6JmtUzzzS/hpeM3x8oZb0ix3ZPswsO5+Hwh4l
gRCK3TxflE5ek28r7P3hSByl7lzFm12HhV01Kf+VhdJZ4ctEhjoybIANtnjAOpnZ7bL05OvGNomF
++O7QEWPrThMlrJYnFtfNshcsLw6jvyTGpKVt7m37wN2Cc8cozy7LX0Mvn11w6sFLQpHqP5nqOZP
TaaFjN11eSTwOyiRot971kCex8xHzmo/GB0DBpOpvBo6U20IyVl+RQqVV/58MzjJk5fpMKHk5e6q
2zt3HuzJtaY9iTCOptPWJ893XJ226LQcPqfPiE3HxunwCRcSPA9XxDxMjzt6VSnrnuObRUh8A4QM
oBD/ZZges1lSnBAkJi8219K7Mzgw0K0euT40FSucZKpNms2TBtOUuRl4SIozT1+t7F863m/5egUw
Yy3X1SIUmbnjfVa10GVuqhnv9+0etfBwr8LxTxASV1dRyBnVYtWRhTlY0y+HBHg6QNccIlC8nt0I
E1061OYEkPo3hyGeoBeA+qFQn+D9bhDpydPj0C+agOxwdJQ0bMDE5hDEh5dnCYIeytk2kU6Ldm2G
UV+b+SwH4a9EjVNXsnm3VPN1HJOXEpBcsjnUp+7Z0CHGeGR2fn8O1eL0fxrAn0SR/iy8jbAQBDdz
xNqyc1QIJD2LUVVsdSq0J8jhd2HNj1YprrR5/wFpB+sVIMWdnu9ZLOvekrcxwK1ZtLb94x8zu+d3
nvqbxaJpEYeKTATex/9OlB9DgpL+0HeUy7QfgAE391GpcBtf9PrpKox9KK011yNVcfvW6krd8SdT
8LwhSTWVp4AAlZ1/REebMI/Awd8n+CuTIuHnbM0BGLIIjnrpgFjEwtQ+uKJB1p3nl+bPy99mIIN4
yV44+6V6oBFNpVxmRibW/vXHdXopJBCTzw2ITpVBcsf2AoWwNj8eydbBMjGdsgWmqJgqaZw01iCZ
9e1IewJkQWJRB5IfZcAlEpK2L2E4wtmqp4/BMca+7wkYSxV++v+KfpdvI6ZOBuPv3PwFTrFmaD4a
TbAgGL+7a/ou/vycYD7UKY3GuVsFYKG9NeP/urZaUJEdJdoW2IwomHN3FL1DWZyoM7fsSfsLSA2O
UQFx/4O+XshVNKWW+qmBndDebXBC3oGWOFj31lCbt6WbYLoykAUulHFm8hIBt1cc+WmSWnXEXFxV
EUnf5FPfQvhQkKJKZ/A+kfD47Re6ReyCu3KL+U4dCAyM9bfr4QLwC/qJOrVT/T0J1ziOeLtn3IEe
bNXdF8GF5phhB18an7B2NlXGQFNsV0yZkf8xerwUPe/1UZUr80ACIpoA02IA6dzKDZNCRTFd/7DJ
Q2Ntxjhxo6QFmv77Vg+t2roLPAnX/E0j4UiWHVpSqtcr7zqPmB2bIa8lsKIVvug/4R3OSpXEyZ8/
zhZaeWX30xnco81QlTuPoTXhKTtRUVrFBkKZq0YS86V2EQFwI7m81Hazp6eGio2lfayl+XgxJkid
z07Z60VovB4IeFzDSJ6tUlDn7v8l6O0rAm4DwRGkDlbmbYXiHLMQC8S6WpmPgObuWro7Q/ck7UDj
U9d6p6LOkyzaXLt7B4Gu1dLFWoaCtZE7CZAIWIVoo8NKykXNLYOVexcTTFnUQiSh6RCkt5ZCPt2S
qJsL1z8BgewCFS4SWUQJrMsy1XUSqGOkqaT5pOtxUMVhFT76vD8gOiemsL6GKMUNja3BKwqHTfhC
bo2+tSzyw8O2l6pIAFYDQ4kWEenLI1AXtY0QlSeG3s/ehYDeXQVS+HJ2A2RDdtUAtAPR7nyrtUAq
BnlHSxDJ/jbBSJCY5lm7Fa5Xaq1PzlGPoxKypwLnyyJNGzpYzFFDVKG4Y1SyWamDn4GTs75wIEAP
Hw+driKaciQ3x6JXcGmSVqvgC9ZO3wWtvd2UG/7f6+KLVLe6c+xJe9nGeQUefun+HZtn2CYzvlIc
2nwu+exa7WOnxavLGiK3KuVCheMoygxZD1ShcKb2lFmWfw5/8ZP718ubJKwBkt+ZVjGylwVb9Bw6
TqjuPy6i737Z8yuAbv9MF0X1VASjBSSGcTw2wEIb1cPU5Hr+Ybv93hCGvOp0bWmhnj69QqC52edg
T6mtEUMJit7ogkpDfAUgoxz5yjwqxzWDuEZ4N2iepAnywUX5UVl6LveY4whp9VWH47XkeZwxU82Z
ng4L91Pg6Vh4M+5vWmEpT8eQPyh90dnyT7OmA4j0zHMv3Tlfe+GDb4f/Fg02KzlWYUvghGjV8OPs
CwV82wysfjTEePhExTcGla/f5+7tpOcPs67+kClYcSECBppZS6Bi8V2z8Jlf65ziFeeLnC1F6Hxa
w2KZfVx3gBa6b3mWF6wDE4yHVckUNluByrCXEDRO3wZqOLt/sqa7IrGJxA9YocZaNZ09OSdHZg84
u1yQchljrNW0/pNbr+xtEEoUK/lPKHKULCmMX4zby6T9qFeqlLm6w5KqmKJT1F+dO4ldzqUfUHRe
0G/vsdy7UfIwdGgTlJvZLDsRiR5MIklcTJP8AzMzgpc5Ooy1AfWs3fxSS/aPRMp+28epJOt+uq24
mO79MOMqVrE9vovCLFzNw1bdB1aILEuGkfeLJ/dY/gQqlW0nu67RXgzxd3lw+azIW3ecDYVESddK
zSmPNtLPq2Tlkb+kLgmkMt6UI/szSOLrNBwZXaAHNZmETPX3INg+ufACY5FZJqU5bwADanw/PO9L
qq3GC9i+hrFHOx7njdy6jBw3NxNBm0qmrjBTzpmJQmjGLv2uRNjoHfJKri9Kn4/zhw5CXhuBdu6C
sulYZVcir6IlDGSYpRkf2Ln+S5lxdAlo8Rxn2RG9R24NqdHNedxtLWtthGnFy4wXQzLR9EC3GmMp
fTAQfXljazPzIaW88EikGLF3pW+RsA4MKf+p1vlwhpyFmvtHqk6yN2zJN2Hzocaq6zxWRLSbRkhU
xuS6vpLggEtcu0WOuZSKIoHeGyD0UgiB9oQ7kLZ1taNUlswCk+HIzatZzxahw0IBz1TsT2tGlmfM
b5Ucr/gyE0Oogk+smFukf27K3Q8bI4M9u1j6ZsnnHeACx5kuke60e3XYnvppv6hQ6lvprnhrHDCB
VrdzhBYK+iqZyA14xJXX5g/Z5AwlRAB/YW6yLGBrFcHPTYENdX2veWbje4OUqEc8ry3MAACTmh0g
q2WifvxL1h9cix8yRXnjdW9XN6ncOpi8jzACrayX6IwaOyPqRkN5EkVGBvmEsUKlLxhq0+NpnmpU
9Yhz1Kt1aauWdlVnLZQhGZIgwlk2KrHzg5NSMWxxJtKrEUrLgJ49RAxQW71GMCrl2VBJ6A7cAFEP
yHzUI/WJAxQHjFJxAVzDD7gMCzbWNGZI3f+8/xxXrrhiXLhwSqhwO0DllFvoEg51A0/n/ykGr99V
geD6/fMtoHA2OR4xhdSAaH3vIFEIDeUSja6udXvcbeV/qWfPuw3o3FW3rLyVRLgdhDmWIbZCxsri
J3AZbdHknKicL8Gj/7XDYnNpn+sY8MbQdvpRcMTH7wGiJKgfjtn8xFU3rnrkpc9ysOzrCj9MD/JV
gjNSWgKejA26/HZvwzABXICpjpey1J3z/S0FBj9bPsW+FxjuHk7+nmjwPOrRvmS9Blune4gzeQiW
EW3iYRQoZV95iXF9augwZOKtQYjbZ8WJbDrjMBIEunZha03R8XqxijYVvNGpH1UWlMCMXWhiaegM
1pQizE+aCEcHHNHLLt5ykRGn4+7oHA3+UDyBd3FtGRZIYkjCOw09bQ5JIe5qnI87AvozJlxwSxTz
IcaXorbtXdWbAH9bu1+oMg9j7AmSrVsF0Iwo397I6+fUWjiN6IqYg6NRLKOMFWfFCHvhBw0f3X5K
cKYwDSVIO2ZGNEHTYkuUwGKUg9cCLp53MV4QIQPkoaBTY3PxGiK7I9OAvjZnePUeYmvAWWMmTrc6
dGieFzTZQ1vgD2t6KsPh8GLRWgXMHD91LOvGhYX++8sgjnuAwImsVwrdRWkQokbBR0/T/rQyGWFp
C3nQLrph7Neplx+8FKGeWZ4SP0x8BuezxlB7jXlqthitRCloVSuWWKhHPgxRLn/c0dVHgIHAy9wn
Abwr6kykVpG9PFOn6TZFVD2T80PI3wSM8OCQU6ARlCu92V28PeRgHI/7XJnL2NxE6C3Mic2aBZ+x
GgVd9azB76o8QkTFXoyKAkOj/IfjKNCpeVJNePLF/ptnWr6YlWC51eKBGJaCU3UdAD4pSYBQ/x+t
0s3aVUXvbVcaXdbGwFn+uhyesUC7mGSGJX06Ff+P22vaR4RwAewQXGdaRNAmAOv5gtY+xWELeMy2
WsduefmHYGWa3W+wsC5Q24dRk0ojf4DrdopBpiwta7W3DQODIstkpH11MrB09zTWo98zI5PbIvHl
+YrdvMJjxD8Lg+KtrTNENmV2rkUcsWJlmSUsnAq/qHLfXpO1qopEQvPFan0D0ABrINkNBBVvH9AY
1dWDgH7Ct/Atk1G6iB56GljO08ObXXKb91hXtMK/jLRZrACpKohATY9uKMNMP1xM8OhFQeiuHO+C
cl7MyFdgrYUUfbU4uF/rlI9HWW9cbykUXv00JGWDzsyhvBNf7F+S+7j1+uFfHAVORX78vhfWA3wL
EyT1QL9xrgGX4Gp8aA+7tmMbfsdy3vOwUMsczVNiJrOmHSfL39ZnEs3kMpQdbV9gzm9ilce2X+kA
YkfmZEn+qGh8chjHMJMXmzgL948SUSnXJ7An6pP6KnbAo0oLG2xFLrOr8eVli0wpYgX3juQaX5qj
KJTULar91740u8EV20KF0FR0HSJqcDoWcj8PwIhiW5RAVR5nUbFlfVECijPJZ9Reejk5qUXA6wKJ
rgVDnIK18KTPpbk9i7o9MggO4nw2YuXwylxxsKc9HhfLQuAvtLe9Zj+0G1pCYc4bW3WPtyQPPUrb
dOseEAmvofEZEvlaGescplHfnRTmQDoxWPh/Pb6meIMbRRoDX97M67lD0gVZit5v4Nhi81es9SXQ
uEtb3womJJmPFPBGPMx/dqZ6BEF9jsWQCTGHiWBLn+NidkIOp7tPiFh15RnnGvz0ZEk/97yVl6YM
XWuwIhlbcmJsGkai05hP0gFXkQKaXCSn2bXB9C+r84MgI1YI+LXCh8pSRXNgXeih3QO5v64hr0Hk
/Gc/4OydckTxTzFMycm5gDrT2ytCk8a9Hhvq+OaQWmw9xgCLCfXVUu1y34NibgjNegKItoxGo1oE
SlkNRHVOFzq2nGvIRXwrBgBDnNiA5aE425S6muCjYuTf8poruRwfQAI23oOb9CJ30QcdDBT85vqT
VEWLmh3XsRUf6+u2q703hjWgSWG1lgV+f/xKmiH95Uy3PNiRIce1+ZDX3XziUxeS6u35n5VG8bAb
B56dnt8qBU8PxdtSLJRtJl8sc6J2VplwFB686CEcKlyz4b3dkilrmxgs5oDURlvi4uoDO1VKzIIc
T0oq9x7pt+GTI0jCu/G2QcXIsM3KRU431yUCdm7xY6pl+pc2d9WZa3bZM3piSTmLq+wyQXZqxIX0
BoOQfyRxZ1YljT3TuITVzmzDQ5peuLvXPB0Zl/JeyZmHhGhhYDJ9gqyuZgpSr5XK4Uenq7DxEm0Q
RFiYp5rVg9POTFVndcbZoJPgmXZkSBxwgpNaMQ0LxX+GZzKICVeS4zG2E0cdheEQJMFJ8511GTlG
4DNf+MI4QrTR8XZDoAyYlWYtnDHe+CeEBqHqlMbuX4kD5WI9BYQEE4vEvG0LRDJPhhbgt74Lci8S
ZVfALqAYIW7jtVoH1MUV3cvssFdGoUvRQEITMTziArg1tlhJxct2iw8QlbVT3e8iVVkMi4YRGqjF
yW+uT7okwj5QLJBUAtWLqi+PkLbqYPq2y5oS9GxVEOPp9ED8aDGJbr0XbAk2g4P9kN0WIiEyXZXU
PkEclXLb6okSZyyOBGYVwkCs1d1uojl9w6QBHir8ky2daT2A61xORlIwGyFXHJOAVm+H/4/Tp8Pl
4u/2C4JY0j42fUmAfATNR+TdlpNloUVkeKDbY7s6x2QX9wbVW3hRM7U9gataT828womcN2Y2gqun
g4ly1A4eGbymI1qEpd4N9VwGqas7yLp4Nvbc6q6zvLz8JwFBputuSGZPGvkQ4/P288WRQrfGn/JE
RiJ2slHxunfNg7SX9/c4RGmBieEPKsUprY+sEcdmpgWeo/LEvZwkUpFP9/jHeu/RxnfH/2dLIVm5
vXDUgma9ZGuTVmClivY1VXNOdRUEbWdWn1aD6N6HmayAsSMtfjZjbdPe9Ofc5A4h/OjG4mPH6yeq
0/lPFdJFAsQmZ2hEKi2Wpbph93gLD+53PlV5ZiAuLYiJJnZtZOMraPibfarOvy5KbiQwg/Fq9XLs
fmd5fAFjVyQxe14/odR2d5/Ya1ZS+91C2nw0Gui8yYi0TnrY5FmO4AIHBBbXaTwI0o6nmKBO6hN6
cMh7NdFjg65QYbYOfFoWW3lkJePnYrue9kMstzXtliRl9EpNlfzTBQijoKAlUHuCtwY2Bajxlks2
Xw+V6pKEIHskFbWMbKtLRDQ/KqHfLUiFtDH6l0iHTC+qdD+OeT7dHcBh3k6vOuzqwxAHCaeU/BxW
gmqdFLjiLD77QsgdMUrq7O177Yw9bv7O4g9PkQ6uGewE25y7RpT08SOTyO4xNYe05lS48BislnHZ
mWA6jSn3VlRvkdrP8EUszoDf8s9nDdJYiU2lWjsDx3QbiBUQJ0GAlQOQdguy1SIHnBRfzh/MBVHr
XY5BsKhVLEQiknedb43tmYGT61TuWzdkKdrQypaL3vn+Pn12KyslMPeID8b2nPoab2f37AuOGb4i
t0441q2IVhtc8S/FjhLmslmS2h50gsPaTBoKglRkzk25ULYXJgmZR7T5XtG/YIky0430aTjuvr7H
x3gWwKMi7yf9rxtS+N8syet6LCDuFoxv+ptb4VNeFf09ZbVoDifYZoyf517SmcTreujWEqKW/QX8
cN8mJa6zfXC0fHxR388Fj/si7tK7ihM4Dw1lb1+6sDgLr5lX6X3MIi+Z2PCCAvH62X9udFtT02/6
CSV5OOSxoxdqDJz6L/E+LuIHLfhi6J0iFszyvpkdceflZSoiAtSKhdtbuNykr5T3LseHyd//sRKA
9Ab0romQqdGAy2vgwz2JXguIVhmVuIsvyTeAIixyr0ua0Rs6dIJUid1xYnGvMuXrrBJtCo4sNPUB
hP/K4uE5DAiE2RrNP1IpjZFsb3aymoBdf03o5WKL7aT8dXA94sO1Z4iV457Mps54PfxHUOvYQ22h
pRB8IrhAIEggcX5MjigKLHtSaYKtPGddOQWON+7sMz1UesqRnwEXqdx/dUXKFBw3QE/1UXdZzbnH
9IO2Fm7x0lzifmJo1DmwNuPIpH7vGFhJrRqih8RE8fvTMW7i0vvK4pw0oamsyX+bUd64F7EEeB2E
VhBAh1PQEUC+ZqKC+64MpJD71H+Na70Vr/OSkK5otKbH74CTddNows1SUniHMwcxm2E1NopHWSbZ
nXHRvwzCzAB3tRBTMt1QHFxV0AKn7HAH9nxeaM3nnFOKpgIhx6HD/Jj3BHEB6z+me3EIKZ1W7paQ
xJsXUxP3KATy/BQriFrLUsBDYfi0YMhOGxOkORF4BgdtWaIJMozqQbJM63OrFOdrSK16OngkYxt8
aNDECagxNHj4NhVwUc85loD5x5SQfKTwdtBzgYhS/GISVV654YLHx5h1tVdG2Xm416uO1NpSTYDK
bBWA0AEnRVcrEZN0qD5inp+rZ+Eop/HMSZBladD68mChHICbrVKc5IEdSnVIAYiKpGxIK+KcqpLV
Uzn9dKQClWuLyycCZyl6jfAbQLpV28cOONXc5bd/cZWaOnqsxRny0MJzyIRWjOCwBbw+tU8IlgO+
4cDPfu/jGWyna8dzqPwhT4POTbFGxKrTtIfKjG2v69wN1o/sxSv8F46o47KrkOE3FOEfTeYYPart
SI1fNDShYHmJ6yTYe34HWtD0QBrxO/5JkgUYKNZuq5dEdaofiWrt/XveHV07/nW5l4hy23BqJxY7
s7SK/YCg3SP2n+ixd3YnCOHQdoBhw3N9inDO1I4vpXD5MNLmaGRhn56Eb+Xijdcp35dAQZtdrOxm
m1sg1dg+FRkhiK14YlvWc+KoVu1QzRhp9CQsKMjdqS4sbiz+C73yG6GyBKtAiQ/6w9rf38YiLSXY
BSKV3tl6rncWQkIozacvfHuKq8zCH0GM+DhA3Vmf3PoXcW5aSGRi0KFbmE7inNWcikiOnVmLuI5O
gNgTrt+5u9PLCi/+9Uk9ASvYRI/ONz+umZPF0ybiWnM8+V29rF7Gy1nXBTJ+qJJkIL5pAjJar39t
esiIkXvV3VM9h5xUYJqTixeGkIS8gJq6C4BFHNBKHZhoxAMOMcDqlX92p07WtZ2j77c4OGEZgdNe
liayoMvIQF3XwtpVZ2YmkB2XbxKgBeXx9SPIRJhZe4wqtbTjd31ip47U9UYeFbJ1IfBf71OZj7JL
FYjQ+59f2I02mtmgMMprl6qwvpJVcxMcOVyFfaJOGhBRXvKnU+F1p444+12Icq3wTTGXnR+ttuG/
+pfG5Q5hGZLJXDEcHJyL4TXSJZikv5W7TSHzWrYZnJdUZjDDoXTECsKOwaOepC6V8TsBWo6ih+0T
6q7uqZlLdE00iCS08I3i0sx6ow3aPpLXh+YZCbWhryCoNu4edNoIk8LF4u2/M5RDV4Q+KkKKby6o
+b7RLA6hMD2dAp3zZ6crpR6yysKikVmctiit7splu0HTHxeJunAGPkpymBPyvKKFK+Ad7QQv8f0+
WA2ofl05RAQUPXT5EKstEX1FNbRLGefEPYlBRTb3Nh17/z/t7YqkmvNedjPTUpG1b4koKVV+eXEs
9tb+eb7WWFyTjzXr1uhdYM8LL4lGxjh4WKs72Us43F39SdKKIgnlPEGmqnsNJf5FHpM+vlAYVYeF
o5mpM3ctNjPE/aWBOmg54O3cP1dtDF5veY+ZjLBocRuXUyQ21dyqCEJ9xqLe2nR1owX3BQVZhI27
TH27PUMWmRi+xkzB8A6QbkS3LNAzW+MK/GNnmCyER/9Y6jOeB7xgf32cdOv3AH5bhRL+R6xb3XcU
05c1YNxoVI/IS3DHwjMh+ASqj17Unto9OZZnvVRrgMREqCugk7t3JuotMH+j998JLl4sKAD8AqJF
vLuabpNqUIF3numUH9g8NMwy1UxrgqmEkcGb2Gr6Vs3eE8xwTnULNDbWmu7k4dNqZKrNUuQRAB0t
tY1jrxho+TJQUlHwIc3CGyBh1s5ZokwAfvTQ+jNAmbIwfG6uq0Zb5wNerRhuL/OvmGXvsj3oj/er
KR42m9wnFV8E1xgbYLQYOwVXk2IvSdqnKFxF9Z/XE+9NKolm7IaKuMirFLisFhxa+6glvM8Nh0B0
uARdkobVFqptzIkf3uDqjHwEB7Opm+Zon95zN9GzL/H+VEb+iFWa4qHIsAmbolUkhKmT03CJ7jlj
s2do9je+u+XjZ1vLGOjhaeor+e9yxVwTqUE+i6Vt+gjfeHvuqXCESs3usV9ZRhxxEkKfvyiKnaVw
KHH5lJx23gsqZrxzqfiWcBzJRXF6N3KtuMiirnsQE972PCiEtWqHg2NKbWLUAWi2yY8dqWTUUR/u
CRGSSE1Gbvlg5v0YpaemkoKc3n/nQscu9wzQDnZXhhw/Vian+7/c088mUGWYdc2jwJk56iSUec6F
I7gfIqAeNSB5+fNpj/mL80EZf7+mRovdLZfmw2imwez6T3exNRRRnMMx5xael3SSbyydPkOwHwQ1
SAxERECrAeMfDkP7DE0P7GD0DNnZAvkDRzCwH3ybQN3Ob6qkOM3f8APVQLufGMTzD33gg5Dvkgb8
6VuzZnYmSbG+8U+wV3PONAdgAlwdf2AJzkYNTqiqzGl7Cz3iuxa+nbKJkoGXvk+Ef/AMNeNaEFDs
Fv2iUvvtxKkhIhPqmKTt+obPPcbllis1esUSb1b/evM5IgC6Q4KRe39lmMgSMXwKDjV5SVzEb7jp
PM2aAGsmMnb/ACY75mEBOE6b42NcrnK6zYvkDRTg5xnvoSq+vSaoogcyLKGsF1TGGFjWjZGZQcB7
xjpWZCiQV0qUC6SQKb22t06b5Wcsm/XhArRi7eBMmQCNJ50hqsixzfJzjGLxBWUwYAS6W6FMR/9h
2loIlTRHKePdCEdu/pQXWU8QVUkFI+m2Wa61GQNJdg+tDOc3HUJ30APsBrepCPRDNvWsSVIW1ZSA
sMgUe7e53w3QSI9LtLwW3UubpXqCm6rgzL3jbT4NUVo4cJ7avWKyg8/Qriv2viPLNGyGh+sikdfW
7bDZHIdYcNx4zH2auvpWhUtEXa/bzMTKMF/3oNJxE24wfcnZNHuQF5Sts83aZwvUKwOllwaPxvhQ
2YwmUn/0vsEwMx/QWasWOLYq6ZBcFmzJ78l9IM8MIt+xL0Dh0mFqAVPP0cSaqmIjM7ZO52fMZcIz
HmYRWzFirKPpXdH57NPgCN4pD3gXoEcVBAk9GghapwSWg+lz4G0FKnTGc8GhNHCCW7mssVV0vc9v
CtWIcas4ln593FHGg2WaCO+BjVkrIlemcYEwA832NnsxVo40kbvA8iBbs9YM68UdScuTYMtwxbD5
qW31l0qvvRsn8AuSkaMuymyLlcjANJ/Z8K0jzlQ3QS1wiOKBwZltxrJmygZqFzKBiY9mZxWRRyri
FPez/2eMCQJ9l8m9Dh/LuDNg3+G5MTKSI/8AC/0iKBaKnFwJTiHc8tZP6VPtDfzouKIZwwQkM2ml
4M6GVJ02AnukRsM/Gm0no3Zj15bvmWY2VxORmerNN34cqTiX+QjSXP2nkakQdT/s7HcBV0waNOjM
D5zDSlbtZi3M54nT9co5g2C4Ayx06YnPrkTTKcN1AsAQdxfDiukGjcvmgrFEEu4+iigSHqHid/wp
14xxC/9lKrQYjhxOjGvldiHWTCk463msZOFuzj3s8VuKnUkIIbQj3iikdRsFnIeRQi/QESrFkZBs
7q/3hFfnklLk0hPMkfe4j9watoUpO3Mmlt9SHU2nqoT6yTEk41YqABZNzhwE98B0UbFTbhXWRphB
A5ksVE4PU64HEU5EVZC4Z5V5XaDTFr73OGrVJ9IMJ4DNFL/X74wrOYvCgWxTdlRqJKnFBjnm4mMF
/MP9gsZvLdgedDgqvEY+0/NtUcv6L3nhEj5YE5cGDXRsfg22pdsXBab9VKItZjeTZ438pjtly3Jy
0zNQDDy4qz9LjMsFjriB/95qiXAc1cxTsBpFLB5uG17RTYk3L4kbnhmWGwFeFkliR4dAUX4NH7bj
+fkYYCu/i+asMrc6urNIIO0fqCe7Sev4WO8X9aHgIdf3RRJIHnlQHV7DXVG/f8tC3D+Ep11da5QA
aiU7/MsTHshzNOp6wuuGF5u/w5dSLp1RqM/s0hxcIAb3AbIVP2OXacqO6e7pn07CDiHuYp7Rwy02
F5ZNF85ZRCBFup1AfdhizYrGTE4TFxMoTJXvVdSi36hOew0gCppthFCsJ9cadukuUg9jwdc8/jBp
CtCq8LZ5VLxhELctBmkIR8RObwKxyGzNA0Cy1+kLnfHHH/rmvCs5SHhtxTdWVcq4pJxehTalJf3E
VcAoDS9Jv53ixCOnr5UJF3/+7wrvTLDqb5GJ+hDHajvGjKzAAHNh/fouuu44Yrz0H6ofw2FZpGk+
WalThJz0N1cF8qUwd7vWUXMVb2GH9XxgFwb2BjLA1caZ8Wpc6+sd29OLC6gxFLRerVJHGaN4ZRbY
NzrCEMcRESi01EmHhTaBvJPjlsh8kWfdgGsCkEeiiu8wZXD5OxoF8DiwMspl3HVea1e3h22RCUIw
6BSwn2ZsGz1+3y9n+aLELcar8B5YLckBKF3JX2A82XyZix6FYskE//SrqDqclp+FfToQUcTPCA86
8H453ymVf3p9IceeLsDAclO8WcrRQQppQGM3F3qvLHXPSTF3UcH8nPQRyHuLnLwMszZ7jklaKse3
YIp95sqxWU4dGMasXpb45bAvfZgaY0IkTUwizHdpCFbmZNLF+JR0dkEmn2TkrisXvYD3Hw/LIKCd
oU+jZ98MVSW5S3zOkWYtW/+dgIAG40buGzrd1Oz8xHS1ytFkNIIDygN5UsuaWjnkrmf8+9YDOqx1
SRexCicLzlh9JGo0/rWm87aobT1C7/InOhkZLJFV27R7cbooT7OF+10Jc0VdIJa9OJpfgNodE9jn
naDXYunW3+hNbpId+jV86wZ2sk+1AbomTSVpfs387ANZzZ8Ll7WqFjqrdX7Er15MM+WN6btmqgf/
qk4OMHZKueAg6Pfm0f/OjUs9V5PB9iTyprgMWOBqdeeIknNvttVTeRPYsQFpOKaTiJEhWGTARnxi
BxLD+GMvRqfHNrrDT+sOEky5BI8XAdzE52TBcRsQFL2kBlQ1vAlNDqXaZypvrBVOT/Gt7QpKQdQ8
tXnuhHrVBfSvPpxSrQ820L0KgMdtpEh7yonuJ+GWgtQh2HcZKz4UWJRgpckDSyRY21N+uZ6zgzGA
Ufn9A1+ECB19Q3X5VXfqSgx1I80EKEGKF3Zi4pNhR4hyffbCHBKJ4IzVZa0h8hQH2IhdpP2GQXdg
TgZ2yVdo0+OkFonKKGA0E3+pLWl/3zos0ws9Th2NjGuDk49tNrpFil1Jd1prePIlbLqWwp3INKeu
E3d+50HfAq1JDTkHzCtgbXPq505b/3efuJRBymnTG7D9XtNB12xL6PyKPRJy6pfLC4wKKxcwW6cO
/hvo1RU6cAgiSMER5mJS5NkIsQzDri+KpOxxD1Vyf/L6PUGfyJvmMvnoiPsFAM3am8X3xKWKPNQq
U0PMhtNK8Qr5o70PwCSUe30Q0Zou0f1TLGgwNQt7asqJoSR3/uAkiYe03LF/Awbl1J1y/0z2FrlN
1WajraI8NHUY3HBA0k4C0bwe4bUnGlzyyihzPeCnt90kpBZpps1liUy15x+XZBSYqEEyQd+eZ/+4
Oh6d4jTPIwkRM9mwXRMMRj4PG03u6mydTEkTaMnalt732kc7VGnMSAyEOBT6kQe29xpz75COPb23
ORVrilqi5WQHrdtIgB3G8wdAVCVfwSbsjECZczb6XZ4Q/i4ikiKrqX17qdT1ZuDwDY4T++oDsL/t
RfUKfrYkpk4ZXX3pGXFlquRn283sVLj8pGlbuPLim6dI45yVTouMtPGF5EB/n7B/zvK5D7GUnTI6
PsflzZ2wY/q8UzVA10hKcLCAe80hW2tj2ai+GXbbkrJrIURk3tex8f6eGe2XZX7zbMW5BR1TPPBU
YsnyVTS9e290hVXFU6MR68V737GJaRSxqfcsixKxmqye2+hDj08UDlYbHfbIrXBcF2k4wKi1QOn3
+j1mSEXf3fdxCN81UfznrXbeCOPwnYD7uO+fFU3bnwna9agFtP7WbnmsssIdZe9+Znh8rVErGzkM
q3HNVibO0yOakuWQBF0lUnzMhG+3YE9BCw2q6Pp09wDqPUaBcnsGq/8boJMYw1HIfJeEj1uSJTEh
06pMcIQhUB5YZC1q9mxPksJYgAlieUab0vFpgNS/pnmJZhCef9/BTGJgtkStkuyQ8DIHGOyFFI7U
YWJHQq7ZQraaAk50GoKuQIPvChRqry4957pawe8n89p+0Ho8HA9vtV/dshJpFgM68KF3ZDexO1yQ
3aU54xxWDG3+B7JWMZh9OSGwITgBCyWFFDWMJvYoo3ODhCkjqLuVCj74G0QccfsyPPh/yI1sQs4X
y3dg8d0S+4XjBloVHTv0E7H5ek64B6FE1LogAiQjk7Cx61W3oIiooPY2vXZ9LxEqf4aMc697JDyb
uAe24nSMurtmVP1HZTOFAgMR33JUXiydZ/CR6pZMAOpOHHuGN+5h7dvjoX3iIq6xaA4ZQSNXfG24
AeAPfpQD3Qs3CwoR0tQPY1hn0FqM0CY2OOWRa/004wFZAjzlfRdDB2RdEr1Jfg0mD3IxsDLT5RRT
YeF5sZ8jFEQ6YrqtbfrHrtPR7/ZYPs8+lx/2UxpPFJ1Tfwl5jKxXB8RdyL4e9PzE/8vB5Ca2z+cw
vDA7zAAIaHhapu+9i8y6DWfIsiECmh1n3VNZyqR4wcFWRgGURoDz9Qjrw/RV5fY4pwPeEDhoLOO+
aZa/Kf/DC9ttUx1HbWIjlmk8iZjWNWKisMgMbHq8ARNwOfgttZNGatq2g1yWF5kTqtAaRGM6R3Iy
DVuJtDweagb/ZDJoWIGhTmmmGtE1NnL66a6dqXy4WH9BuxMtog8sq70mfx1tD5ZYXVrQ8GL0K4MD
XEivJcpJ+hnFlx+2mr5BAIfB/0JCuRs6CVi8GtBE5viJVa2y9L6X97tYHD0QI1S46oAO1ULsWGJc
OSUslpMvBD2EYt7jkLcNkcZyWbyErkyfxlfh3Y+waQjRldPdNnFedWjaSskcJsQgksBk8a9RAuIG
+uJjiHKsC/oK1f+zNLr1MEYYZU+a+oBXmJvAvC1eFnM8C7BNofHJ1z5TUyxuhYWorfXdlf+cKkY0
E752rKi2UQhxiS3vAE5P6scMEY4wa9qaQRdhOdk6QSzFPuXsf9R/VQNUez+xOH9aL6Dx+aEVoEku
pLiEVCU5tSwL2EeMj/XlYKAEvzmQ1vCTm+zutINJW7rMw7Y9T0ClVxvkucMTANl/IHTtVxtJ8p5Z
Giwu4umAYS1EU/1iXNkZsJILi6dSSJ+EKS8nCaudozS9kAaBzkSVViq9Ojm3N+EU12hvJYEmjie0
g0tTgMoHDxsUMnBkP4mzfYQJxuOC+1SrjawqZtrT52TqXF7WnI1EnIGFaF3/57N2H/aMTFxq+Vgh
l9ZM5hAhb8POJ7si1OpMT27LvpKsSQ6uCHIkEROpCyMzK+dBhPuM4Mukxdtwc7z7+l1vsTNqjD3j
tfwWYW5zOmLawgCne6azvRDgs5u4LBGUIh5lU7161DbXYN93/alUJEZsbuf3LZdDAnj9OAN5r7Ss
/Pxoo//3qrZf5vJfuqZKaTJKD9VQOrDgD08Whj8ZzYZ/bsjrCHKLBiOPXXQT3GGZIkcLYI42yviH
snH91z/UwuKGjM/TbAyKHYCc/Db6Cn05ETiXx0hRhH9eMBgT3oGvfKCSt/fgTL7gVF+62kXXiR1o
QOtd21S8Y8zVg2AMGYyuAnU0ykPj2XALNi3jV7hZcMhnmAM7rEwHzCwPwuYoVvp2yRMZwXfevP89
EnZU3EDKiNIekYnEsEJpY+sHXWfQ/92YIyvA9ku7PrqudVnhhnaxbm5YGLKGRO245xvnEKrnqlN5
Ajcrx0Os8UcqV5LK38/mrhpUMPkCOyQ6cVT1B9TcF9Gj5rdo2a/pzOyU53kpfkpYs0NuORFeB7OC
31uRJXw/zWQvdf+yBc8oN79gl3J1fa16ccpVscgluo3k7MDAh9bwdGIuzxpaLzNALQrKvMFUN89c
bF3FnalvuEZu7Wc48no/dZ+bo1JogCoTG/B9pPfsl9ilng5glJ8JHSYI3HTU4kee9TVRcgShpk6E
fIbDvDBDmbCwM/blaPS/QQq+IY6WRKag218ky3wKhqoakzyyhei6G+QnOnGy91v+Dxjr63o24hrR
MXcQnQ1J2qCoQpLhegKQR8PjZeABehCAJKkeqDD7zvzbCnbazXzhHB5Oiy8ktB8FMsWTyCVx3PoK
xbivwnWAuf/PY2U67PBR7IIKbt+qYe1q8k1KFuS2+JpqT+LuZVdpA/df2F9VryJSDyC30FnWQaPo
XFfvotIY+5U3yaY9ZZurL5HXSBrecT9FZ+oLhRVe66Z7popjWPHcKstRk+mD9+iVUqYb2d9SJMXq
lMkkP5W/PbzXGqrhztXEc8lVQgu1r3sl109VrdZo2cotXY7omXUn+Y+6mv38+zCXFzM2T6Y4q5pK
psUnu9PTAgIdvR772lyNqkykU94d/3sLb6GIvGJjQbSC4i5vK6SaHXvhgdu+jFi2THQPmKlUuC2p
VtzuAz2qksTjCPsuJvo22B2prAS/VxkEUaF/7CV6TO6vORL7eJ26yjZM8h4TyEBOsEyR6bpzvjj6
UkyjjIu8ui09RfIfRLHRSUABQ46FGwfg/LN+Eewt/CcLsTZDyQcflvuMQW4WTEYzAn7Ygc2qJLBH
BnQL1HNVfCnplrbfvkinpGw5E/GVyYTjcg7YyN9mtraPzgbs+E4idBqeiZLhkY9c72JJAopnZtnG
bFeccwU/pt6Ljz6afMt2HltP7OAKUwTgXkRZaE1b5c9DoD2qhjND5JWN1NM81hlV26kqNTMLc7n6
tdUybeIw4va0vmHJRSsD9gOnMbqooZRPQnfsx0PeuGwUG7vgkgBf4yhHmYlFxZPS+WYWQ3sAkFKn
/zafZSHS9iVApFMte3kXSF7je2OB5zTCCXHEoutVgVWcBHJF0IaMGR2sTJBhhArSmqLNT6Y2hmoK
KT1d+azuHuht3+X2iy5OuvvuTh8JSVRIuNNyr+Bjl3Y6Hi+bwhlcW6TIv98Z6c50P648gr6aFBdq
+0T7Gd9Y8dOoFJkW00zIKyb2NJOn0mReYBzmWdDBHFediU00WVRhNuFCj1HWnMYHHl8nZPcE+juE
yg3nP8OBPh1Xu1/YCZFHskI2K9LcfPslj0fCyvZb8dfZqK78U0sPEP24qSRehWsI71PeVdfiQ9mI
0MeS8c+r6ohUjv/7gi63NjIuiqymVO8JQVlgI0RnvkfoQP0KsDG/r9BwExJAyliqsMxiHIMhikmr
bV2qdv8wlwitoX9zA88BiyyHOzvoM5arT+9624KaIYarbOkkFmv16JYB8m9ar8WIZsm/TdSY69zM
2RmLhVxfgysJt9liF8AlXb12GXt22VThfG0lUdhxeOU43kfuQ5pYCiyRunkwrdEDUjr+m+VfP4wF
8MkP6sqyd+cJCjq7OEDQTdmckCIIgqRCyhBQGPhRtpRXN1F3W9LqWSDx7jLFdHYAp0pILHBJMbrk
SA5AapzWI1JwY4ZkU9bMs9PCUBT3jNfkciQ4MfFrgGaS7IOsR9nobW71miFkxUMq8sA0BG6mVbKA
YHmxnd0pnW6nhjcFepuarkMuInEEZgN6EPHS+7eKMJdmKBgDj3ZR3MXL4YIIz+6fJYXiuWVvqbaO
yjWxycczSbX49J6CaHtPrjRHDXp4nmPF0W5+jYGO4zNgC/cQ9ZxFK52djbkOGDizNMWOZHGes9XW
tRY/0GVDCUyZyKyLNT8kZcSdQqCdxTu6JaSv99fJMlowiTloFQVTdLrwrkxa7DZNLKdhrusBPGRa
5XO+Iuv7wnhMzbsXkrtUjCX7rDyHizByV71OLK2vc6YlofGarBS5vjnbzaAhDOxbwy011UOCbaQB
/WfMT88iJfWqgjl6IZrWjiTRuQI39MWy+/QIjDtEQ2Y+HkZnrg3fc1F0njul7ondtzk93Uw3SN+7
oJCAMFQ0F2cPfDNTcKE8WyiT7zT4azk3CWQ6ttm2XQQV8JcNNA6sFfIuDE3Ku379KIKjFBy++ImY
/yKP6YWvZ7HSISyJlnDo/tvUauqnzwuGUxpEeOBxYR87CNM8hVEFxB79LcblflE2w4mcuorQlahc
9c3LvE7tppFwlBnqG/jXsfT42wmznrj8Lkh7Llm577l9I434FCtAc3dHQZTzDEUk7VpZlNstWl1N
fT8jsw0+4A+iXKJ9nPWqkyJvPzie6OfaHVjYM8HlIu8IYtiYdIMDqDUycpGjdw5eb6BBxvO9kVJE
XUozEP3+CaSobu/MWuERFd9S4hXKiId77TfqGavC2Jo+1XAgJSjIY0c2oooMq5u5gXLaadrgMMjS
Yf727WcdOSIlSAWxJexqzq/P2Or5d3FQPiXrJAkl6iIRyMS2fJfmTZMbkVXn0ePi6ZVks0ZXMvq4
Ueor9OyR9yUPo48v+Sz67g3oePnprnn54FOaNH3I7BtBoyoSApnA7uoRDxEuFf2qqoti8F7rSLQf
wycTqK3naGkjVZDhZvVzMyN/lsZvIbSyaYNAEVtVouqW7s0RNyO1Jtv2X/BCUWaeZY0U4/6Jyjws
8sNPkOlGeYm1+oc0J48jJagSJiMSMB6yG1dGgj7+9h5tR7170qaYQ9C+Hgpnmj/5/8C6P9ZJWHOK
nBlvHdAJFfOUi5N7wTBMB/h//N4bx04l3Olv2xRtWK2ebKjXEcEgNF51AQOeZq8wlKT4db1C5LBW
X48fJbGfCFyxAUykv1Y2hGpU2gNTQGOPtYp0b2D9alYKyV4kv+5WimdqzYYXJixgAQ068iN6ZinD
rdOMFxhyJVu2oeYzzAXRqBb67bpXkVVybKQCPCnRlmTJoChlb6EJYFRBjvANn07m7Md7PZAI/UMf
Vx0sexPeg7WFyVRzFjFYFzVpX6E4lh0+gTdSid6kWd0WGzYMy6y5uZTmUyGoeEbGG3dRw/FntRoK
M+/7CRxD9XeeP3ubvi9BK+lYgL99NGWmWQkwmweepMKDPyqTO2wzudxbKtYlGqCXuqeSCOkxpPzv
Turz7v1bgps4jnVg6PA73GqOX1dYTY4+hiThSxMUbZMURa3UVpm0GHFdC+9lY34DVbt9B/Q0ZZ1M
MjrbuFoNywtVPQBFE6zWCfYGnHjUPHZZ3Y/A2lOIgasZLtE+7gKWcXvSraKOd2PZPTBf030Dk67/
wE7wH4Yyy7yKdenp28V9MrB/Mug2WRisBQefr2b9FA+qzKb9evqMm3bgL1MzHPS3iE6N2i584nsl
NzskvEOWi4+yY85y9jH/kqn8SqqsOjPY4vMfxi6QBtvr2gtoy3teggOa2Sl+AIQSlbOuuo2NYGeo
CCGcVDHg9qjWN/iO1XTEDkJpx6gULLlUpqZArix8iO142iwSElRrW2DE8G9B7vq/K1XpJd//+qau
BThkCnWoru8nxT5E6lh2M9Ilp9N3ypGjXBMEC3Kcjf7YelNsRbwm7TOX6cn7Yiwv+LWSzBNgTCM+
qVAlFfYVcwcdkpQqb/aWv412Xic5E+1LfsM508JWaO5LCNR7tr76GELbhWnkiPURXVtUMAQNziMV
v7wXH5jhRxKz42Gn0PUw1r+rtCfHhu8P6MnHblrUuUJy65xJmybdjdBQwdGONiBMwNFTBNjXFVdi
L9tlIYQHAdAeF5F8JcH5kGThtt9P1Q5nMdo0/BqJnDeG4yeadj4wfod92aHXLo8sMaqA8vyLAv7M
G9BaqhKekUHMk1m1ykCmSLKWz/koODhSJreAjzbayWmBmeZlDQwe7pCqTsAXq/6bd0oYvEyjOHy5
ocJ9SDII7wMZG9hJg9M7XiOirxw/HaCKfw9bczVCXziObXceXrel0w8P2xFoyfoei+Y/2/U85vR4
kW2KuENzw3qB+nAFwDigesx++PerR7FkRTEqHRCCRDNHHT4PcL5Adv+tJqRVdGhzFI9mST5dfFUy
B7qJldoitax3GxczW7BsTvPu+UmhbLn58LEZFu7kZUHRCrDCldUgVPEEhFQfEwrXXSVU3waZSFf3
4pRlswduHRZgcym5tBCZd5SvNjOCAAu8prBh1cR1qv/PP6dSV7KB2eids6qQX8cvNR8BtkR5BOIe
iPT/5ebXRggIq65Uq+cGtFCxvv6Pu3Darv8s8ItkJniCPMuLucuHBfMsUnP0/Do6ieOGq4XeFtEo
ocQehPuRJwXTWJwUAU0kMluupyJDeaR5cYU9a+ybrv0cMYwcDnuVATS1QMg0L8uFiljDVVOlUEIK
8qqEU5ZkhPGssFRc8fqdUp4HO4FSEY6Ycok+sJL3nA0KnBtb5NruFiecEq8rDJUARx/c6YrEu2iA
lDCY1yg9tms3U8aCCLAB3BlgtD9X5ouXB8m5d+r+Fh+6LZm//ypT8A0d4YOx7jSfcsodBgHxufz4
3qzbjhxVTvpN3O+rLxTB4eIszybXknyHMeFu/F+G6n8egF+xWZCDIZUmDDxd3FyHhFpGLecUlMw8
lfXwM/PH1vsLCNjPj7LXl9JuaPfnA+Tw7ZuI/dyR7bfYxxhngOUSCdWgcpSPMrxsTzTCml1a8ndZ
r7LWS0HpMLCccAnQriTk0TLbzpUd8EshZsqhwQXtbY4gXV8mr3fitbDUfen1wPAla736+LDPxdq7
mIZtU5s7LN+KPAjH6LGAxu/XRT97kzgzCzKP7tAJ1v8pyqhnH2qfhzBsYZHBw3LUeFWhgyFPlvvw
V//NKgZoZtOszWBFfmoORAI8YlTuQqYIHTTMsBDhrD5An5nmpeSPkUzTNiCLMgDPOMyIS9HI+R1h
Os1jA9uSaT1B6tUc7ozE0vjIPqPvQfABdQIxNTfoUVgD2CbxaGuYLRbKzRcNXCPWljboT+GenbNp
m4azksgp9NDxn2NrmwV1kGER+yuPS7eaAqA5KeS4LWIibh0hk8TcGmqcaCGNJQkJATW5m4ffyRSh
t16hftPXPYLINMMdmtrYCUc2APpq4x4XgFgeKl4e6mMZz9gsQAM8kRefPxhbh4FlKqpmro0t4K/P
a2vGapZsPT3XZGajibC4cHv6D7mcL+WQb41lrdzLwex8zWm/xi3PhtmmM19AGoEzHzIqjVE3VAx+
GwrCmMo74SoHnMYbqDbSGiSh94T0RUYVFdc4FoYnAuqWytMvWA4SNyf9DV5PI/XlHyWDYVAt5V9f
VK9M0LhY6BvfKeNajs9mqglocVzeHtcU5+ywARAGzqlSeqdj+I8W7JQx99BLtV9JUKmPWmLA7OzC
2GkId+5+2loXgdQTLJwoD1Qna8sVP+c5oGge3xFcJ9Grt+HWSgEZTE0+OGTaWyjDXaILVEUs/wDx
w+r/HJ2jfayWu8bcyxgblOp/dhZlpYyNuO1y+GtBi8awkkO78r8s0axWiJ6V4RAap+JNXCmAcpRn
We5ZqNJOdyiybqKdoBpgcKqow7pwgJpOjVJrE/C8lWQFuxYUiBBJS1fpnplM4KxmAlRtMdUwkwY4
H038lH8N1Ww/fds81VIzAryDisN/QELJmKPwcOKreyIu7/eywSd3jp617FU7I9sEknbAgU99ij8w
F+kecQNAhyUWYZPp7dz61BeRaH4Jbc9U1QM3VlanxwdWkUdTvE35fhdt6PkKU+31PrM/2tmibpB/
RuclUtwd/P0rR9DxiWngMyWfWY5ItCVYMoGbdn2+RVGyP4w+61PE250s8C4qOXlxENfCXE34s1CO
489lIHjk8kf1rKfEaKfN5+cxA0+4A8oNBLt5u+jgriOvobqM2illrYNEdO03fLXg5GGnnnkJo5rh
rr8QF6VK8atrwSFBqr0Z6NaYYeHIpjjZAYDfLmB9a/xRjXWXgw/Ui9hqHtHkc4dw3V9chkCsyBHe
BNAQjNWJwptiKsdpHYFS+8cbDTA7uJZyCB243USoWxfLOXPQ81oCgnrcExmDRUSKyj9KVLPjtQvk
ImiI3paKdhLFz1ul/6F61TeXAOUOddhjTFWCbLMjBNlqhiAm0QaBH+Z1GJCeKwWSicgUb0l0hLTJ
kqTUsSVrDh6UlMjiwH0p0Rdqq9ajSkwY1Ui1jSD1nU05MGSFitYKzZgmwIjjMxAuthwLxUyGHOul
s5Ehe+RaGxafD4VJLGhQ1ABhGlD1XZX84mmci0L5XsF3j/NY3WNkVmyMQNGiTaC370oZb4CexchN
NFRlcv5X3tNF4xfrz231yRlE7hvvsgVz+4044Uu0ppZW30cn10ML2oh4+lCAN3MwlcUm5dBtIHUl
Z4hLdgKlA2Oo9591YsMuIqJZ7ZrQMOuXxsP3PZyoNa9AOlz1kivBhIj/6IN05EnPhULDR9F4V31C
NMmsUO2kl7tcUyh+WjSAXozxmCsL1odbEAV/1eZjoCalbXTo2WqzXv82Rd9Artl0TZazvgE18gyJ
g/zr473bGEHiSm6myRpkyK9D+I1n20xNdQQbH9igdn0fMiI7a1Q0sKa6bedYQdHZt1IQ/VjbC1RY
VILW4G1cEhH1VrBqZn+7XcgFwqDZ1dU/pWWM0blLkaZSMoexyJn29fhYL75BoTIdwBvSSgOCHLlY
dstLs+pN1HwIUNYlR/Z90OpK2++kpISug1d8RHfqwtwstq2UKlulu+2SwMJBgur1AvUJiZ4BDF2z
Q4gLwyKEf4vSxE9AfOW1PZYDS4yKL2Z/seGygrw91uzkL+l+9n9OFDF7OLmptPk0yVe+AuOL65b9
YavxrA+AHWvfejCD6aB24GEIuvVb3sOnwjvMBNVjYhagMipDVKTfsz/s2jvTESCPyuuNdISjXEYK
Xx3ipPRlBTnJ8zsiH/fqMobH1fS+eT9FjRTvB/kCFUfMLn2OMo2wIb5AtTW3OIaDPyxxBurxVDeH
JdsZqqHoXT0iFRLepqQ+oy4ZgATeCZFwXJFIy+QMLWtvL/aBwhgTYK7HE1v9l17s49g68/DvT2pE
kmti7+d8MWX8evLUeFsIfcKevSD5vhBKttaNNJ/nJ9zH8fBZqZ09PE4jwkEGgxO41YO6sngOdAkP
RHL0u0/RyLUSJ7eDbj7b1qhmpEXiW/UoBVq63K0XF0CabXBaV3yt9no5toiL2qwWyWwjnhZLp3bz
Q5oW4O90BIiogenxdjV3T71Vt4qqyjvoU7QzNkz7Ikwyspvf7O3SrFmZyaIqXlCicZrzynxSQbT4
4rRS4tTdhTeyQ7CKFhn/S1MIsspEEPmWuzw1LT8S3WOIuiEPZI2IiZt6khh1UDAUpclTLuuPV/gP
de79MrI0i8/K7/yTQF31BFtsoUTsOFpbdQeJuKLOnsaeCWbt6TmrdOe8gYBHGFRSX8Hl5Ek7XcYg
6eng7nPPn/mlnnKthqhVhVgKcRlMOhZr1rDMH6ddNy9zGv2mCp/ce5XHZghP1Qzr5qMXO22IxQAN
9+D57sUaWgrSEVdxsb7HkDOtvan5v9mNxs6CM+Gyn7PntsLNZPudP2c7+VBZs0C3z62afFXP7HZI
3H60dTd8ROBbv6kXkQfg7amhBHm5ZnwoCeWKuAjdmtMlWt28k4fddnBlcHM/XhOSivjKOmfMOInk
0ky6XPPy0D5RFIp7+4sEv3BTvILUerfVIVlIOaTki01+GkxcIT+5Y3N2wprcjsurrzTgeoMNrZrt
PMTFO+aI59AJqO/YhwhQcL2SdLmts/AbKgVJtFDqnkzL+HjyIHhuTv1Xp3/VOVINpUUJnnp3WIL3
dI186Dalvl8Jbrtc6otiStOPCwwpHyzBi45YYnbsSPedj3FCu5I4hu56LdyzJAhCgpa+kcGAOZre
sv8xqAFwWPESzRBwnQtrL0TG2oH+gRi2SLiZtxhDumM/2IwBKSCuAdAwVWDJXA/fdQC3net4AXWT
AhnaJnS78zPe4WFCbyX9CUIxZuE9ET+WkbGnxkG0wMHmN2RzKLT1ekvqFQzKqBlzwqsMZbrTsYFJ
/mUSuW7BmdqJ0bR10/axFyfNfm2xBOCw7attL1c6ihv2KSdG1YBpsQL+hX23+iL8Z5V1xsFLg3U4
WkAWGHZ3sqwczZ3stJfVwaPDAIqKBBuNxIfjwQhAFsuIjGHmRRHI6U3nOUbfYJ0Ftw0Snqi58Aw3
Gw6S9Rxz19ae3LSK66r3F5G3Y2/0+pUgjtaU1TDc4/UoU1H8L3D7gM6Iv1k/nJ8bL3uDoJ0R36Jj
rtaik1p/kawzok8SJ8/qOMpIeOv0KofcZAgfboZl0BawUvKG+RrIGsmTzykUZSwgu/zGwl68o2tC
mi0nvOGZBx11acQbDYJZ1+fpcpfBqMxZ01GVfdenDAYgjyMiO6FQ7V8q2CN932oHdUdkcHRlW9OI
H1a0VtWdTcItvyg36+G8wMOkRrYK97U7Ch7L97BhCjdpXIU85nwDlphS7bJ6Sut2iS+nhk1VMM64
D9cownUV12LPl+caVZhBfrMJDH6h8VMeLJ9F2qgHn5iI9wk/m/UBaZjyBH4wu2SNvbsqsrxX2iTr
HURNb3obM8YV7iE0hGoZ5Y7NOu+p+evWMzqPwRMTMeOCOIYRTHAXEEvQYFfoPZFakO2D1S36Zvh7
b8ztQxaIsG0HeGCkXxmcoHA3Zl3BySQ50DVYr14QaN5mBvK8GD5tNjZda7t6PKaLSOige+fGmbBS
2XEpG/QIYZa7FHS5C5DTl3DCd+tnqZqAEAdL3Y4oB2Z+z3lSX1jF3ywSK6FoYy+V5Mrrf+uS2i6z
I52H/DYHONjpmXLQfM4sP8u+4ux2TuwH/0lLcpYcC2HK8FlWM3VONd+QLBE3rR3ir0WbAyZnShHK
x/ePt6+TloTFG7g180nq+pssAKQjqApoOkgF6OgFDobPQ8M5Ii63bfvlJtszGDHbB/ExCzmiz7cV
4AkQfLIVWlX7pHB7SOJYJOJOyiL43BCwoq+I82aqdBrZkk11V6UNktnwkeALC6PkF46cyfYcPYRg
M00dmbvO+GkL3dgbhyKcYvw4XZ7UFlIeNGo5Rgf4+yfu2/juTMbpn3TsLnj1RwBPuIfbPa9Eah+m
1oLS8+ciE7aW/ccIrXSqPebQt62Z60YdaY0uTBTCuFF9B1W+ZtSgd+2zE8hkwesz3SH7OlptJYdX
mo5ehw2jl0bbF8DVwBFENuRAzJSuQ1XAgt4a7c8aTcbrMR/GpZEiXDHXMqZfHN3W8kXtenkmqbTP
NEeHBX5WiCnmWu2B+Jw3Ip4JA24phOHqfdKTSTFJfvmgqFfZQB+Dklbo0whEDrBlUWKuMwFBBpwU
38GOirtB2/eg1MR1W722pS0LwE6DY9Lwsf41jW5yj8BSrl5yKzUzVnx/02hfPMa//ZSGGjMaOQ9R
/lYo14QFCsJBdy4oZHJ8WBs4YkYPWd3LuUnGLaJ1qCE/BkC5Hd2ZNhxdscG9nqdG1SQOvGDK9WN/
VO7bQsMdV7gYzkWg+nB8ibFJACc9MKaOQ3IR+gm9CdmdP33VFFw1olCScWa4raQZJE25Dso3vny0
h+CIocGTg4DqxDTVJIygnMAtUMQPxYJ9t9WXbNmRFmi1gUNt+YpwkNZBb0rKozX5d5bRwJ2fu6Iq
MpaIxDcBGMjJP7w7ExMqopk05OjVmGcX2oiAo+503bwE9PZmCWtNjj9TEUr2AGODBjJZcivavN3M
xLHgg+aeVJPHL+6Gza2H83EqQdK0s+LEpRL5bjdrXJUh2VB0xFsAFvOyVru+QAk+v+08jXgdqJJ5
7b/nAs8mHgZLnDDh2bXtoqa/c+Tv+jghvPp01CcrSNv7zycoT+NI2zgDHZktSOL1bnAp+MMaWi39
eiKq0uPUOEO+QjBJTzxLhp8U+mlUMh/mXDzUeTSsuXLXBSQ7gf+pZYuoE16mb91qWte4xYaCEFRI
BzICm0GKlvaE7DsQe/X9/4eLHr54RiSKaNf6900FMiNS4U4+xYEaPx2MEzpDcv7b2QL3WdDZ9v6i
AfB6CUSf+NMRE+7I9JpMfXluN9mHwKxMLHOiDi/R+URc2nAysY58kDubEV5Dt6Qz0HIlufOYbaxB
OGppzB4xU6RInN0kFwhgX8veLgzwjxpQdXoLN9SZErh3A4hZqMOWMHy3pm2xxHuIwkyBJleZ3VDJ
FZRjHFDeLU9CT+W2hOVnDgX/HbcyMbCAmz5PEjGhx92kVzTOu4H+znWtIizDbnAaUO40tGQqTkbJ
4pppGroK+Dh9Zc8tVciB9pIpqev8XgpWBorf+zcGR7cx7TLJDU+RweifwKgn7XcWC+LDVnEpOogt
ROVdxPEWE4WQMU3njOX2478EB41eJQpUEjp/IXnZvjdAn87jTR63A33uTD3qf9RBzenLGs5qrfB1
DXkS3ubs0/c5Depydqhlw+a/pzSYnPwrKGw0nTDamhjRJ8m+DgV0jSeKvs3ElrPP6YIJsGBnveU3
LKvDaraS/xjL3M0dRdKvxlwjIwMxN3NPhfjLdf1vkqKhJFOxumDA/pCaHQzFTR5o/HXIiEWVyZfE
E/leCeB3xoiVs2Edz2oA4vsT0XCM/fzdCMYF3NPGkPB3G2qwc0fyu6DQVx8YZ4A9GDFO0TTYZFcn
g48s3ieM6nw2HO51UaqConKIYL3qD4u4unW6ycZSYd6bH8rNKg86/mpUUH8DBkGqB9IPCAoduqoQ
DVKrvaq+uYn8nvVmjNyKBDPVV+x03a5qDUlke5VczrzEn3jfeyUZHnbNHnlwwlE+M7lOJRuCIjHa
/7A2V+BVvCS/1pjUkddARm6pIPRJA6oBXLb5FAsUcGobtvKgU32oDzbEo5/dLceh/LBGCDQddZWd
RGQN3V/DTMTfIe/uxLd+L3FX0tUP7WkFxLH24XYIO6BRfRHCQAazibQNfSv8whIYN6G4nBOlPKB2
F2aFgbWYv4oDp+37gvW3t4xB2cXAHER3mt4kzh1mhsPFu/D6IkXDDRqUmZAFUGVwDy/NdTt6SYbX
lg5JQl/Nrmr8xcd1I5vAvnvnZqyfazs5nAOuT/WUkUpA7gHSaArFEO6ef9JTVKvAnyCmZEjxr0h0
krdFk9GCBw0RkqKkdFSTgAdYWSf5FHYSnlXHvd9MfKiLf8vwIxrXiDaYMbtQnvHaEKwQ0rzldNPE
ffta1P6Xn5kPAoh736/z9uhe9NwahdTRaG9+qg8sKwICm8m9fRkwDPfdU5QjNf08RzISh3TwAGAi
EncpIhVmkUnek8ZTOMRbm+jdn+QyCs4Z+2P8yfeZEuZ8ryPJKP2s8Bi3NF6Rg2A8bsSGZ2eV1CeR
mqatD3/ikd5PiUqVtbNbdIq8XQVYpBgOMFFYL1AQmBY6FhFNEURrKtt9iNQquiqs3vYrfabUaj8L
pUyon3OQBUi64dF2t9D6WDg75wDWepYfh4VWbGtL/GatBEfAGity8d1GqibtIxnWiVzC3cpjIIOW
VUyeFxlcLiMN2Gy9s61zyT4NC/c7kAYCZPvtKRw12sjAUDbvTruawKK2uiRfirLrclBdPndcpbo/
DXR2IOmQa4AnAtlUs/7zAIYor72ILbp9agN09awsInL9nkvqzJ2iG46Q7G4zQg3AWKwzXnMhgO4z
PUOZwt2cd21Q1Q8J9u+hhNyQJ9mhzC9P4dFb14xIwQYy16/ZMqiS4Jk6K05teCGjnmTb8PFKixxr
oEwVuoxjD8GVleOs74sIpyWwH+GT5w+HlSWrVO7lr1YIjkHIGWlIDkkACzzIoAk+UEULeDGnk7UF
WhaBnGvRzxWi2xeK+Fa5TaiFBmziCXV0IYXHx1hrNVd3MJFvxxUx+Jrq4bHYZJBLSg0SrFND3fYN
4l2VJAxC+U6bnGScJ2U+2rDnVCvCn+ZVa9+7Iy1U6P1t5sYMCsdeUwdxCslt16Y3XRZlidJNIMzU
DNojGBtU7p9/2S8/csPLgoGATj0ozUfg85NI4g867cu5cZxfxGDbryG0AQF1d8pueX2hL/OkCFC2
FgSLJm9jMu4b+E7iEeQF7CsKPtoT6skQyJFhIOw6AmXVJheJWfs18pQwqvWF5X4bS9/iUoDXOjQh
syy2wa7eT8uA8kbOp8PaX50Omc7y+99gQdb+BnBiJPOhA/JfXn6pbqEK7RlZ+3inCo/1nNHZK3e4
eYikBzOoJnIVWLqU7vpP37hfcxVrVRLSAo9/ZGdFhLI0bT2L32n+KsaPn8zSGngWXhTKj0ugwtoo
FTzYxj9o4+ZKE43ku0K82GZTciW4nEud1X6O5y/AfXdHSC3Z5fbeASQbktSjvsTbXflYTxba1jvc
utstPY7FMIZV89fUoT8zUGWqM5P6JkqciKMClQw2q/NYJhhYYZhynmxLZig8gaYobbsAjCTOVJ1L
ZdrKVtzkMuItf/p6PWn5k3ddK/BCemRsSzNk7itLHPwxUSIkX3Awz+Eg7utFZIK2T2cbyi21J6u7
NbaXvvS5mMFJQOb0iIimZHBRYNYWTLdCdD+QF0XGqaM92MLOn2RifZUF7+guPNQWTjNyNs53DaM0
Bs01yqG3MKxd5fq7FcYk1EiLZCfiWLpp4cHMDPAdfUBvChFJqRQm12GXMcuCe8908AmDN/ANQtWq
7nvD7Xn+78Ihj3Ndi9K8MPK8+dpbZIX0J2dFwCryEQ77IRl10R4IE7Hgk9h9IVVpeShSeFERfFcA
QK99tMKlwVkKg3mIZNDc7WOVAYJtWnI7Drvs3aG/g2sQeXR0ECVymH1DLCBqFBIH0T/fe0li1lqF
ZLGbi5SdtKmr4UMzie9v1SP459INNQwPgYLOu5Db1EVX8AXKbd7KMqcKICW24U+KZ7NVwjC4T9Rm
Zm/2WC6JPYmsjQs+sy6BkFI8wC/pktRe47iQwdVpYUJ7mVrbmFZLIYu0R10nhbZblrB8MEbKzSP6
nJz8gOKnfLqqLP04cEgZzxizfHYy91k0k872BzP8Qu70s8Pqj+cMGhINz06XA22I8pyPnAgFRGQp
O5zsWUYFssLLs3h9YSeGTq60Yy1oaM/2PR6toBkBuUeEpH2HAKQ68ffbAX82aad/SJ44NkcQPDx7
nzYGmhQQGQy68bFtMxvECrymCqb7I4CIKQWb2xa6pimOPKHAEnVBG9UHCIO5El66O68d+8MV8PNY
+tr5wMMJUKJO3jmaSunyv4HB140SpM97xcD4mSAj/3MflJXdstGJiaVjS1HTpQqtNsV5KU7peSJV
krxzTVVuZseC7EoTPTio+OZaLMLUvPG/u6iH0aGszlbo6qbdmaQGm7hvyPBStJZtce1rVSr6x89B
6/wD5VzZJpe53OCBVeCxl3oa6ZeMF+tV8BIlKyQAvaHshzrK/0rPBsT06w16ashZ/XfDLRpLDinp
qo4c77kL6Psi7mDdq2h8YnOX6ZNbzM6pzcUV8vyb/+O3HgoCZ9pIH+SCVIP2G40L0RfrzdMaxYBN
GvaKBFO0KDqT27uZAh+qsh5N3zO9ykODqrnW/2ekdFnlbrFDOyUiI1huEzcLM/vFBKf2ffECl7VJ
F2HRQ16QcxX2UqjHVlVuBJP74xklpNZPwhKEYvXZAtAPkjOjyYnfnE6FyDGTKY/rYXFbJ+/1KFFs
tN/UZHW6LzQ6O2Ur2wGdrnwPCs0Jq3p31GqDy44G2vehcohF6r95AyRKKEQ50tvVknTLy7xvy3Mq
+xD1zLEsWwgc95qnb+VknA/C1vmVr36gqga4yB9R8IZzihMLVqM9ANmWREQy7CGmPB6kel8Rvbs6
KOr5yBq76WEtc91qgGVNhPnLUY/Yg+mlWKgqYFM6N6yM5niJPojgqSg8EW+eMjl6d5vzf3eznAI4
9WpSYNYJpjy6OLQfe5o7YFWrM9qCHXo/poZWctqQvQ2m4pTAmd5ZiShEoSBmfmk4egR6IXI64jtC
OD4KgIXATBNZUK2U9BwvBg6vOyRtIV3t+ZeRuI8H8Ga8qTcEedL2ePkLCtkmbSBd3eKCXzmoVs4O
wxwVL8PXyWatpuo4WZoXzQTl1dJgi6R6QETONaJ1zHLLT4WAGe0vzi55X21IP56WSwNA3jGEXvAJ
UK3QLlkFEQnf9Q4isEFO91CCajWGkVSmFGBN8yYn5CkFkdL6AFWygSv1MpmXVR58n2yIHBok8AoR
IKElEcfaLFHop+xh+5ktCplHUDc0JUI3lVK2SAR1Ce90iywfrdg0EE4YL5+t+3I5Chdfo+Y13MB9
jTT5w9BpGY4D8Y+I3O1c0SLXsoJFALO3vX7WBu7+KnoGfEFoIlRTgeYhRY+t8M2tbXTXJvepGAMr
KAnmr+5QTASSAkWHZlZorHvS14YC567qHCEXB2VCRxMes7ZYfmI5sztw8QanjrUyjs2jGzqAvAB4
4mWOV7BMp/vOQ1uMRdB7yZX3I93eCHpIHyVilB+Otf18Y99FDUl5Bz0lSpoAXZmTPhVhTs3sNNgJ
XIds8Dc2ynv6d4ewBew9z1ttjJSuUnFRWKVvc6V436r4QCiFdEf6GiuwgwwwJa3vSb7YG71G0LwN
7R3HQaLA6UsTcyNvBoMRrQWdfX5VuGGgBp+xbYzowFpXakHgZYnlk3L3xCIaJPQd1iayp2rpaips
GSO2AkKzpRKbAn8oMjoZ8kOgHBg9T+u07qbyRpRbDaISCiGwsRQgFpUe9As+y6P8UM9wF8OOOcBQ
OS8zC7AqjiIGS3bq+M5vWbgPWWClfBE70Dq9QsB+C9FtudLORM5jpwwlTkfuubbtDHiUTT7yGn7a
HTUBAG4SMY2e6QIRqFzI0m4py3O3ewhU8EEp6yy7shkgI9mcnfs9CHLcQzVqXFkZ5oSp1aoepN11
JFoZSfHGaIKMzmzZzB9KhsTYxBM9ZNZLEGy1Wn2qHBvNDQLbp2JsKwAz6FHzQQPc93/aTuNvzhw/
y+GmxwplKGW5LyIUyKh+nyTz5fh3jfLgybvkxtgLfSHWIipvnI7+kuDykoEAAjN53UbV05lPnPZF
pNv74I9XpQLhRh7I/RNA3mXN/726rgJ3isLWNMjZwu8wlggTkuzcg6wKVNSZgSlY1mh2hDBLGfj6
aS6TnFAmJ1pVVwqeo7/zWcf6ukBbo46wm7xGRlg5rqKlTnx72dXATSkeS++ngrGKU2SaitXVtBpp
C4hDMmlfN1rWeeDXcGfAsloUDOhbXy1GeoctwzazKDw5fqKkdh0JlbGzJdVRU2f9ZecbJJXcddTN
WU+O28YAKxmfE5l5zRJNDJJ7NnoddF/H8HGt0PR/UuLFS3AJFJglnlAAvUG6KqFO/Tk7oOFw0UG0
Bv/EcRG2UrhyapYWGzo7dqv2Z+b1aRwUGAWsc3TLmowBm0fwrrRLYmEXPMTsUj2EeLqgoVqDkX4C
i0DtWh/UOhKrtotBaGmlDxUUPTT/3TXUu4UA746ndGDv47YPJ+/pS88i9LDPSUfy2QEQIaHx2Ya3
MeM73aTE9kb8cyfMrTUR/E55tB3VPt99tsxkMggPTwPTaqm0qxpd23vNZin13OdwArAeKHPf3Ahw
2wWZ6f50i+4oGxbBwmwnCkRbPPuQ9ICx3we/5aMp6AZZKdfA23BxArJHkLjbx9I88dd6sHu1iOrX
6Whj1+2hM/DKdy0r9PXMzioz0q3dT79/m+c0uk40col5B4MOLu75qCG4/4lnvs++omM1+o+tr8VE
bX4bRqK5Dah2Jp3r6nsar37uIu5w7T+9/1ILc32xTKO3REu/XRdrMcCm0wjuIlzFOr9+AjJWcBqr
Xw72lMrcFNVF8vmcJTJdKiEW4v+MYHnFAS9pbvfRrd2T1XwLMzWMXOTKUjtyHggUuvRt9DSc+iJM
DKLSp/jK0ayBrSSo+tyuNJvr8skPDo4nBye6PD1VzW3/vRVF+zQjNLgL8FAGGykxDQTZwo0vajUR
fIYNFXW6Wx+IzISZxNwozjZQXHIE/Xe3ndsSC6/8sHQ+kid7yujYPsL+dTw780o7FEum1AYd4bTX
210HGMJpKkcPffAPZiAISr9NONybVOwuXPugBnQbvQn7Xz7hyYGIhyTSMygu2fazEJeRKzn49cam
gjMpohIOZIqni0+LSEWALwwTxjEhGjJjsmdevIKycA29BarY7Q0evvMXqwBJ4w5ACyejZzKiFCEU
aV2B4tANLPE0TMlPUMwz1opkGUi+Ygtr11awi6mWqd1gDZ9Z1wsXdgFJhFpdseTbr+l6MuwpdZfD
Cz1sPQLQYv4dX2s/AiceTDUttitqNKZthEaenu3+rMSyNEZ8iSSvMS4CoU67Mt8sHKlQ2eEYEN4+
TbEFKrAGm42qUF0lAgmaNDYn+X4AokSYJjqHCLcQKtG8W86Vffq8hjvgJooJss0yvkcuujTcfDHq
pDmKgLPQE4zBntmgD7Z/RXyhdvPDpkLk3LrTudSi1SXVOz8UOTrpJL1fJYKjAf+iwg2Iv0tJ5v0o
lMkBBslZI0HFN5XfCuWRIf0Vs/Aueb9G4WbQjfBUXtzpypB4RZkWrpYOPdI1ZoDo+Lal2D10bWj7
LJlEoZmZnKGAhPIq5lsNJiZlxXWfpUfETuL95eDn90pAs9Lr8lOkYzIsvI13ui5t78uQo+X1GKKP
S91Tq4IowYaof/txrpzhCB/5qBdPpPlA4xW7zWpt54PT+XVVGsU6CUeZI/dlipJT+aMPZbPdMOul
5ZbWo9R6w0KSrPTW8fnVEClpJ2ZVNbmQQ0TAlRVsuR5C9ZB/WzWlLrcQMYWsLzmP3RERmVVCUS4N
Sb/k5z508bZIorRjdy0uE4VIw0bkaKvh/W6bRVjY2V64AcjWUIrH5b4Gc2uNrP15ImmSPFHjTp/n
FDwrs1yuSf4iLehbUWucDK0AP1wm1aTrsW+Ljs0giqEIAjlss368wXt6N8M5JN5nKoQTvJcLbLuo
rHFTbisGUgmZnQCi13f1R0kvGuvHd78pKmrxapY5wtkuD1yyiTKgt1gebDhI4aESpW0qUK9F3nJf
8Mez//ELW9QsjHl2OazhS7MiSSlsxUh0dKGRaWnC544BAQEdhMUX4zfNh4oxYXhScUdtu3yjfrej
TI1In++KqOWZ16IrdgJrXp83hJpBxBGC7wDpcJq+Bhb52Pr2lGTvn4mBeOa1Mus+c/FRSrIvS+W+
UYZjjZ81LaLXAekctG+d2SQeFYY7QpKSHbYpMYravsLoNHSfxHAUaVuqKDa4fk65zZG4H6BuCpyu
uQbrK/nuKCAnpvJlanXlYHdKbgw8aA++gZCh3bNz0EYDnHsh4IHHdTeGkpdU4AmcELlD1axSxkVL
FkiQ5grCglqWsD7tfsSUVSag8BeSDwdWAprmqM5SFHL7OO1DW5d3jogOmhUHO+iJqm/ViBEKfpGf
VPu3cc3u284IR5sKJbUetK1ggYCQ2gH47GGy3gwqkG9VFJEdQ+c91qN/fZwtKJriPwxbcLLwDIvn
+XlyXuQybJfHbI39/z1/cfGoIP4T275+QFtHMMyQMaH/o8YIQ5Yo0fatLc5+UgNKh+m3p2xS6+xf
50z4QOEdP6Gge3jpTTtc+Da1vAtpbgGq3gD21X82Svt2adkin0Ao9Tc5MvvnApC+1vReEvaPMIim
w/wD4pPxa78FqINolKzLrKtZ5xZTSELWFj96AWuPzFJC7fihifSGylZQL7RuhBd8f3Ajv7YnExeN
MnagyrvzoyIdtxbqIabJR6fk1aPVYdGf7iCyxcz+BFsZ3BGbal6fbSiX833e775vVwksgD5D08st
lmw12iDdGrr34vmixAF+MQIG51ekDdjclppnbc8BNvIBP5FSnQbgWMF/wN1lWY67YUGwuWKn/mYL
3TWHEvO8q6omSb+GBEJz868e7npvkXFebVJGIbtwkSHEe6IO2ZERTXCKHyDcczZMPRzy/wnP7vPd
p1ObOfvNsCgr0Q5KypYvXhrwvgjZ7HEGz8tOzNYF2bJ8z8Np8zJbx7xja8CykVSg8wAtZBAtflbz
Pb00bYlqBHViqencgPCT8x5WXkWS4ZouQ62x5d5VFsxoA8gmZrWmov3p7Pe7Z/l3h4WXPDOdbbMX
woGZ4EwxQK9b1sDi71GSSLnZwsaKHbyoqqRrchxUSeMGUm7g4AI2nbDOf0UunC44JGcQzCGCLO+5
1bjlKTO+pDDE03a8IDDGAazFJId4WPRZjJmD0VeClSi9ET0509gJwJIwAnXH0Im9gKyQCftw8ZRe
Ci/XVqboC7kAoAPm1F6aLC11d7UIBxb6wzZFiKOkAENpEiWSk0zzfNcuLz+QNp8hP2+8IwlKmUv0
eHLPkytAAjLrRmmaqrevd02Bl0fJDCgaQL/DXACxTjzycdTtu1EaFJTRIvbI0RxYjGsEvjhE+Dcc
TbM3PdKeB5XZ1z3SieylRzex1t7ipjiyN4Ix1Ll5+c3Jw0JzH3hGqBTc14Ef8J4YpsApSK3MHwJD
hpbNqwBcpbA5DFCqCcdqFyxueaUTnOXWd0pUQeVj1Y1drx9H8aQLdxGGjQ4dJYvqysDKAPKe4b1W
JeIvmINjpueeBKh69uUsxo1FuSCYGIh54b2OhW6bDmZPio2lUfsfwASPS20OQQV2gfcalI1tLNm5
jOOnyQ7PLvPIgXpBo3MlZhAvbMXq0HbmbHzi2l3RFgmtxJH60LDdeXvTKD25VEJavqRG5vvnqUwf
KG/ZubxyjqU38qht2Disz+5y6AA9PDQW2MI0uOJIU5itow3ny/YuYclEHrqGzdNdw1+Tdlvfx+v8
PvEeKGlpcBinBkBdzFwz+GRRkoifABln6PGQaXwSK07ah30eh9iEbrYydtT8U6QIqSvFDZEI/d7T
WtAdd7u8wUMflFO3rK7a4e+yZGIxbt99NwiujBubnGS9vePw4iVp5lR+1AvxlsGUviGHJD23C7wV
wGlNoK4vntRVqEIYGtvTr+4SgHGrbkjTX7830k46ABZvqrMLup0SxeIdplR89U31n7Clah7NEBEV
gZP4d8pro5/SQMk+jyVGLDN88zgiIVQrhjVPhs9Ir8GkkqapB/ygsj6BRjDS7D63NAXRemlpy+1E
JxQxHhjPY4DbYmPD4JTu9002Ir6ERBMlXbgWZXVqXPcFSl+imh2rEBeAm2VtismYTWrFl32mTlFy
WSEBJPaD3MHssfptPs2C7m8/8HORNwZ/2zwfBrUbjB3wjTO079RTyH2choWno6sVkJJFCoHJsruT
K89b70CioHFEAbx4VBtfb9FxM+E7OOzGclKb5vjy+XvMn/LJjDPCturaLYBHddarT8mn1ra/rnDd
c8mhOZjuqJDcIxrWSIZegvj3uI44jR0pPCOdPjzwBzVLaXefrTyQIoipqPsc+9fX5w72qFMxuSkD
HRSLzcvUjsjuy4UUbRq9QBtlU6OoWEVhm/c1rc2CJxLNIrQ+q5htYnBtDh5Jb8O4NWH4j8bvE1xB
03EJPGVGRyyfiuqpJmCYQsm5rDx6CZiB0cJO5c6qMNf6dMtBEOjfgN2sKsKceAQOUzkE+7okdt6r
tu+Lh5qrFrDRRrptjHrufbrFePygVOQFolOQcoYihyW48zIRjlxm/817HESYQmvf4Y0b5jZrlNzv
ZHoC26YiVS8UMqRO1nrX0E199ZCpD2PaFZw2MCK/v0+f1NUozJ9AeEr7piyhukGZFkrowa9dpaMr
YFTHVFrb+TjSaiJIDanPFmZZeos5T9UoN+MuaDryBxYZeXljcor5mn2GRvt1N9BL+INcslR/hREC
9+uDFPdwDzE5GNmJEwawad7GA9j4nvM2mV3I6HgM0p1c8wwXgZA+D3IAUVxhYAGAQxhDisAFCEl6
PqJ6LLSig6Foefh5iPXCsRU30wDvfpUA2oVBIbnwbg8SxB0dH3qIT16avgso0hrpqjr7n29QzXfK
6jj49BIHxvmjGxY+mDbU9RTqhwtCzCXL2KEuU7Zr+6D8BAmMuHrVsTf5QIrI7GwY6Ng65TBpb6xQ
3C/doVQB5cpU1ISbvGiOA946710XALGO8klCCbyDQFzLnq3aV3K5YY++GNDAzuC7e8TmISBQGB0z
1Ee4Vx3s2V8lm2h6ZH0VldptUd6IvBjSMlyf68/V9DcqmnrND0t9DzZ6dol+02l35lHvcPSQpvlA
ilkqDaqvnVq0R78d5IWkI8IJikFWZG+4hxNhp/lsBqAdXvI1jbh+mm8F605w1pTvWKIoYs/YKbFc
2EZ/lfz054tdxlK1DnHsL/d3+pqJ2ZcSdhXPANm8QrBdtKM4IsEUD3LBmTXcGlBJWMT20Usq8PRN
1rb0BgsxPTGi5DGpqSzlWJiKDyIf4bNcdxA00gktVo0/+GaehckImM5qntOVLpjPtDE8ejm/frvZ
AZTzP4o2yrWDUrM0uQi08AQCBLwSK/wYR+e3lTpxTxCwirakdFv1GTC+YgLApgFOCPAJUS1ieRzQ
fXZOWqBrdkXfTvG3EHDtYtjydvpR2iQqDxe77ny1hmZejz6OfWDTNFh91qW2t6hblMtF/vs0qoba
wUZRWdZqdK8k1JDjC7GR/hysQ1pYle5CXeYWdlnLziqCykzZP5XvIoTeOFcdRrgZSrasoBzo/HLb
Kt7TIrdVPztpnMbhTE16GyY4s6QYc3OQa2987W7a7WVCmiZpVAsxAQDRKh9vYrjIoazrbGrFUwWs
ezUHHdkWFYG1jsNeS28RvK2GHGy74zsqgz2S5OzyHkRz7UwupdDLlSfXOa/WYkHSm2jdt4hxr3o3
hhuiFWlv4jm281EBULtSZRLv3I7Q10WoqugnjMYfeKNHhbVCQd8KePOcz2tHL2smHakTzg+pgaWT
SSbzaw97G7lUJs1PT00o46XKseFhIYTNLqUAtgkDvhFEkUeq0dvRF4ESchnpSrYCs3lQ6xoRXSmL
0PQVFHCdcmRe4yN29zEZaw1VAfQCkllbdYoqfX9Zeb5dfNhqYVH6cq3RISAYf98KjWRhZB9aKJRA
McTrSi6plsCiuKq7lZgxS/1eM8gOiBs7cml8couzkuaAAGtBZnwkkRFBmR+uQepL2jUBj31BWAvs
lexQRRN4an7wARzIy2j2jw0em+qslWOyg0DTvgsiOzxwrAC1RqHZvnJSqN4Uai6EqUUUN/I35Gtl
jyNgwbMt+oJ81Cv8gquSV5VRK1uoOTl8wUOnPfQjn/cQg72n1gmEjRSrKM2q5ViVdoHOgyPEfk0H
Ha4qXFzn+IBf3541kYJxsnwZXwAgpwi9bL6ZMrf9hi1RSQgFN6nTrRsX7HibPPbboi7gfpAuKnjW
uRPVMIoJKThwtZbPFN/EcIHxsp1SphARMeJBFtVFiiww3aja132sHCPk1NxM/wmH0uCOvOIORnyg
iXD/tcqvfzfNpCPc3ecoGC0J0RAFJuToDMGqwMiEPqIFqVsCTi+eAzmpxfrOjXsLKLoxyPx1jq1T
0zX9l5DOmWDYY39oe1FUbU4YyIBLLc9Ln9jhPm/wQ77OewAXBMX+AiIRI7WhJG9hOc+4WDf8x/cr
Rqgz/NAxBo47G83xWWDRQoJTcRJjP2NaqaheSm8T2WVq4yTZA8hdiL9ZRfLPUl55ddMlLMnh8Sn5
s+8HGJWIULsVTfEDlJaKAkVdGpljnG4ZoHxQRLAElRY0YniPEWxO250pkap+XakcOtJlfwdpb34/
o3/ce/gp56HLbwb6qYk55TQxm5GAM6qTdXhW3KbsOFVmyKoc38ALCrvO45cJvFI/3YgoFwCNW5U6
zK5GWi8XNPf3bRs32X5Gsoc1JOrJuAwNPC7lec055G52Ayp2r9EVSBWDc5Z9gydiCmUFJWJeo5AS
zHII/kqTn1DTWYfxrWV7xx8GJ+7sJehyoWGCEMSiY3zr2iYJaSz2e9R1LCIJ3XKet2ffXlEz8G70
F4IGJgcylEpaBUMrAINq6O3i6CCnRgCy/ohXdjj4v2sXI0DT2n3hlEb4TIiIFXzVPu+0B4x909dd
GNylLR5WxuYnUjzuMZFAz9onBKD/E5QbDijEAqbhwTF22ihvDGSzmUQWcwUiOBfMMcjT8nK4IGdw
dQgaufXY3KAQYbc28E4m7aauIQwRSQX9WEHA8LA2rzzU9ZnlJ+rPIY6g9M95G8D/KZoS6TDgj05Z
E4GMqB/uChxztQUkdpqGuXGnM0OxV+UgMBsDGF7+AzEx0cgPZy3yQuM2xJ5lDUTqFpLzhszVWNWE
67zFGRqzHvbZcZ+s2O6awnfj9jf3iYn+wTXmFEqGTtgZs7J1HFCMWFMkupDlTNxBVLTU8SbA4IN3
JXYLSHZnnPed409GhUtyuLP1azc0xm0Pwbz4YbfTLmpGhklsNYN+oe+mkQCjpXA/WSZemne9101/
yurnK5IuWGjGT9SSaXvj37SjMXlFQ8KxC4r3Hiydcf2q7M1Op5eAxoa8/FTr3BnUSIAz3Pk15zOz
mUX/M+UFdJ+Fhe92ZKzD3g0GFGsVGOPtPzn1I5sh2a6V2haCPBpx1EO2nfPtTgnWg2K5+cXZQP0V
D00iorwAM0PZc3QROUz8It85eKCJMQDyBoZ2Y0t6mYj/3iRO1/mZXnG4vlwXIBB80arWtYDDnJnh
Q6clIaNFNDiHhuXDUxgOiXFz/uTViYF6Uo7jdfYdB7KTcTZUyHqrp3goIFEdnSGkW8j9MJO0lENs
FKvxEUVuxrUn3h+KtQMyLXef+Guz009rPTj54OKpWhwEApTF0bu39C8E3emuGek5T1aPhVkNHJyz
9BdJYLYPOxS54t1DaRZHISr4T5dovD2CqAevQDDI4gOERQqUPJg3zqYkLXeJ6dFxPEMDCVHTPBqe
QnGF8bQFIwn1ZwrZvId1U29d1l6JwYnJPGHEHJDTGazIZg16WNwHowBPFKgiFoH+QzOL4jowypeK
4J8NgGFpVSFw/qv9MQoYVlFilC6NlDwjzJ9ZGgPBXwLBH7W4e6JPsm4aIlx/7/AiK35BlKmisISF
3MggmEXA5Ku3D1Kvxd81ZEVpdhpJijLpY9s34rNt9uJQB6t9vAv5N5CqYU5x1DPRDQ4Rujl9vZOS
JgH+WJbDty52XagkpUniZs15xVyaz22UVjPbHciYwkvsADZfnf5+QGaUYsfsHAfZ3bfM5KRrflvg
sknIiu0Q0fEINdbn5+jYUwG+vZhydmO9OJkb08UmoMSLxf+33QF/W7cwB4ur2SjHd1thuSmfa5Un
0zL/JqAvd9dufAz7VQLu+pOE6OqT6aB+YYcJMYaeCvC3R7FYiM0+qegqNS9ax9OhLrxiBZfH7Lmu
8wd6hmgI7OJTubqO+AFJwna4skuZTCt85HCr9SV33j7G4xk8HP5uWBahwkTAR9ntWtsDISKVh4/Q
wcOF/cB/YAdq5hp6Bp64IwUg6GEGSO3EmX7LyZPZ46Gut4jphBiJAVj9ti5eXQzPLVSljAXA62AK
bvwERbrQQ9qMSKmhmNuVFOMaoVuPGAukiqKHpCYrkxaMQfjOQEirSwQARfI7R1e45/My892TVy7E
EzIxXF2VQGe0q8E4lXamHRkxSw8y6kSZ8mCk4XxmGapSOIn0XFScrEr1q3UOgHaNiIEPpn55vbXf
5qN62GjgcXCSnzIhb0pQj37/fH/7KX+CG5/Do3bV5845mhWXKsbA5Ces2KN0Yr2zHVOgl0iZZGxd
2DvrrJxnDtWIRQ2aO9R8f6f8y8GdZle/qp+Zck8EI9WP375+URR4n3XVOJoxhdUtNfZNcz9He6PS
dxMLUTrrTI+4pKh9p79C93/q6BjH4JtFtauDNk7ZG8N7aNo0zfmCr/Mpw/3ggqCAhqMjiKMIUGAO
aOPVdKIlbRIukMNCTasTdxu/Gbf/fvmyhukeN6ewRSFEF+yVNrPVEPNp5fNMPE6fqKtkDNUL8wYK
K4O9BVVMalHaPTUVl/GuZ575S1qBrFpY7GMjzrnDpSsJAqHNt+mLhcEpAzAz4NfZcld3Hp+uMRa+
+51WNBuejICBCploRXiAfcPkGaSquON6YKGIlKHokkUocP633BDNazHtJGZpHvKET3zrqqoiEqCE
ofpReIy+YrXJKVc2KAZU4SIQdf30Cb3YRH1XEKWnIOOg1B+obeZtrtz336Ib1lwuYUI+fYHsvC0M
nlC94XoN3OJ9O0UOzwoPp38Kym9lutN3esK4umJ+IJG74HDYiLGuoNwx6CK5D2sHCnrzNtaO32m4
ThpEau1D50urEzPSmPoCWGuZGtWVNdL+lw3fiJMMlbjtRn2mjEq1ZdLyS+WrHR+WS0T1MyhBx6b6
FG0OLiTpp2+YXmmqS0EuimJ1+4PhxAjUPAnzhlOcOSS55cJMBbREnAvfBNFlYzDQqXXTuJRNieeZ
7oFdIe5UzPz+1v6ok8L07GL9fdUyMZ9kZP2mDnjCreO1h94D68lPX9AtrJzLDZxi6pWkGeTo3LZU
PVHct0xh3Oyd9DuQt6vSCZSlFTAOrvNFUmUFYZ1ntNYOnPUuhrrMtswG2pZZ/NwoncBrM6EhLdp9
tjIuM7rnGyOHZy66zfUaTK7Oh16GbZwrCEIVEfORefeJpp2xDP3vcl0jxCl4O5UivpS6ps76BKvw
sF8g4ejQ1RUDZ69H/zaS5a5hRxeMvhFGMWkWwe2Kr5bKolCJenpf0AWdzzJ8H1wWXQ0S7UuyMG3K
kge+2vvSP3RZMxpdNMUxU7HveslELPNzGDA5dp0XA8JPxK57NDWCYzN6oMD1ouWDF3YkrW1QkA+d
TO282W9HAzmPkrlx5/TPVBeFppI3YHNkApuarN6FLW8HX4JK2HIc9bSMJbCxqf5Sl47YdskWtScr
gibvXiyLVM/zgaGDgQYy9wyptJ3Hxugjh5d6Ax4crSbpwjxrXtmjn2IMY/RDaM9BjmMU7CUwAylB
Zz/9PO33pgQ/4HmLXfbEPC4qzwQJKznnFgoY0jZCSng2S18D/GDVS7dBF9b9bpfetEkfIn/tCLCT
bCGYRlSURzzAEvIc2RTTjo20tYfkKuYQcXR7dRvruTHyzgPliKnq5a0EuENp8qiKzQMJUogGklyH
bxiQ4LMTeEBAKovtOt2rNAEq19CYthmwwCx1elm/BOG9ztAgFJbtAHWDe1oYY9nsda7q4UkYsqao
LPijmFGNIMpVfuX1GnB3O4T5baANqSF6nhvPdps8ybbV/zruZ8nj/MPYKfV0asl2HwrVLobIpqpQ
0TbB5mxVxyVsqBLR3aKCO2KlPXq0TPtU4WamkiV3a6iImYgGZHkmABbfqHTljSE2InivILfGgAOQ
Fn/6joQacNSUIiIa59jivJ5kSqysUHI933C7+oR8fTpHkY8gbQuSm9S42ymhQalF2rpLWLoKAaeA
NkLtK3mi+5LEACPXxjNpMqA7QXhtDA3aqmTRXfwRseL1n5iZKiIq0rHHQRf6826AXVjzPI/KBTfm
V3LzRYE/PA37Y3JPq8L1enCZU4PQp6SC+RSXzzfSIakIzYSXSXbO9bkA20nhx0Im+YIhwPcq5Bqs
+qaSNFqGpOe9He7l6wvjT/0DzGapV2789Ia+oPJATYfcLJQL+/M20tQDjSr5T4tC5cuVjQUlCQOq
FvnolNtza5Ne0Hn+XqPrK39xg6jmRZwqpA+SJmK+15oNlG1fR8VBUBR9KtPeqFqqKJVwM6lHssAw
bw57Wikf/gKVYU065hOVzKlHqFFF4H103CCImm/X8beLjyWJtDsBJ71/gqrxw3dW87K1wcdLLxM1
yjpS9+pzOUZdn+S8Q+Y7Nzm11ePzQqRx92tqloB61agf9juUAE5FijC+o0XLmzAZEY6W+ucEWn4b
axnDyyJOVjkRlteNc/52XSGDn0kq5GivcC40t5xqBWo+zZay18GxQI2uGYyw0dA4K27WoNY9FAaX
JVBg3pHccG+B+uvQT2k2+9VQATIkHEniVUJc8SORNzw817IWDHRUB8Jzs4Vn36G5FFHkv8OtGW+D
y/M3EgltaH8b07HN+uVCJgrdObPUGXjK6Y7l43A895cBejPSK4jKYTyPAuZDYm7VBEB7h9EDXqZr
roWvsIlNsVacfD0e7yK8uzgxs+jJE3TyB9X05HI5JmoD90kvArYBJUFRj2Edtcg30EerJ7rkClhs
REuBD5iUQ7oYR1yo4aMfQ0cn5rAK8RRf/lc10TIvT1SH5LrLVoQ1CfR1pOyGJahU50oItoIeh5NI
7gAvwD4277zjwbOYOu3QulO5TxcqRlx9kMJk16Jf7pv8VoclLoXt+x7ugrvewzCKHLOH2aFoG3ag
QH0CqxJmGEar3XdbpumjuVmulKMqru6SRoj1e/VNuoS1XSUzSrX+vpBWlrEjdeG7jVgfEmYWOg53
L+2XnLVn0XNSxubu4LLxs/EhNvPvWINvySSlgZQlC4VcVytRxbNTDg9mRQZI3GNrxy4dKz/CuSZa
Z0VqeEXq/Q+rAsCxVz3l42Zbx7+Ou349qOwMHkV+x/rhDh9NK1gg9DS7jcxrpb4OYS/OmY2Avm8i
iqgyBeM8vvZ8jH22J0kAxiS9JwoXuGMcH/nklOzk5bcV2GMJliTpKCDgq2Odsn2sqxTVbtPL3yrC
YKHjUNnoI+7wLIfBWOvUhkydZPf+LVZCZdxazsD3WLvd+Qkfcrx5L4ZUTp7SC93bBDGnomIwuX9h
+oGFZlrbDfa3JGvBIGmMdKvqCAo9mYfNSxS1tSrdILspTgqjmoomiX3XmW5UkCJ3C/RYwkWb8vKQ
tyvMbaC2SWLwAQsO36C0KtrkvtbrWwdOwzq/BR3RkWYuOdDinSMD28DoPyWxkfElrTRm7lyU2OKw
FiU2WemXm6dk3BiZCSk2gaAakVVM02sCnAYPpn0r09izVpsO3rhtQ2XjR4aVjo8WohUlvz2D2AJD
LfmhaLLrpAapGAX2Hzyt3yQRcx4DMSZkzCHnvONlr4LaPbipELvvwth8FvD4Sh+g9NxztT3L1hfl
c4TplXeyc76+rnoN6o0QqTACNbi1ijdZYjbvrMq5AlXIxFbjo0dQ8VCHxbi3U7g3zHqa4A1zAaWf
9vAahHaGcYlL6/SM/Gppz6vKWs6j0yUh5md+4XYNvzE/1RtdgiwQwc6prwylj67GYJOre11ooDh1
A55gL6cUoEfH4RwWvL6zwpF1SGz4vN+iAtJBNBJSumy4fHIVFRfqBgCKIF8D/ES7N8w92qDN/tav
WUm9G7StSSLOrgcQOHTeaLPUdRTDykwqRoZt3K3Z2aKqyTobI2IMW+Ws/XTzwltk5DVxxK49pLaA
Pc8rP3qmvDjD6+2lVmam6ttIJ29hVjql9OwncxTP0azU/VoKykgreuF9HJLmOx/8d7NYOFNu09oN
pxpT3flKbHV1dgi7Y0tZkRSKjPXd9Bh5Jub1FhrS23M9qIT1cEcgwd5p4sn2UdSZr60dpEjVCAkt
CCtz4dlPbYMg+Ttug0pT5FRv1m/P3JMkoYQEX/mRBov9V0H5OzGRHV4mDijxKeGD5XE81aJjBoWE
ZUlnKuqc8pU4/8GUyGb4aXtrM+JUiX73USymRkstkcbNIn/LsF0EmyajpClEUMscoZhLUOj3T9pU
69mko8m7yJy2jbUC2sJWAF9TvZXkTHExh8cxmx4tKymjKoLx75jQiWadLW35Eg53dlT7P/VN0wYK
H8o+T6Mlexl1CQ+T+XC9tBtLNbmoGwagndEbB5C+y5a9NF0M3enaLSaucDEcK/jp4w3LEBuuAJre
Wrl8aeXC3QNFnI9RcVJydI+bQ6C+LK4X5r92snDqgp0xK3pQYSg4dUPBtdqADJmuZvjLnhkElF5u
3NjZB27ejQCtdqnHOxfnqXr+kMjUjOxZdSESU2j7kzQM5Qh/pbmnnOm7V5FfLh9Rk52zJ0EbTQgp
EH6tF24ILvYQX07RLu2uAs8ZliAli3HueIHBchMUqTXpRY6RF6Eg4w5hD8pL1k+RHN49WyAdSg3D
YH4GNXZybL52bvj71zYRDaim6IsEWepabQ2+Tpq3JZiCICo9hqlPs8uoR9UIXwhHQOz8mTq1ozg2
H4fNYDU1fDJ1+3TcuvbooRudThBOTM3Xlfd9IaHbAKeOLVHgo6ECNoRhvyMx+Tz0UPV3Cl4Z7Ab+
MBMLkPI6aGpse7F/ApJV4/UFTZYmKP4+uCQRz2CurEi09uBGrOpvoM4vNsNnRLSRb6cgOKVfskYZ
PXfhARLGZegN7OW+6gpGOTQ0W4MrnW+7JhDGpBKUufXEPSQe9YyR016kaBRhovQOQRFBVpW5IW1t
G/1I4oHNIvXxZ3MO6Cr1dhtGQ0uxL4H8fx+WA3uy2oPCKdNkxl0ZsnFd8a4xW0KchP9OUZonRyCH
Z6dYSCBVR1+3hSb3+KKx3QhnTfbbLddgkf16GG22drRE4YQFwezFPOT8PQCorjlXkl77B1IWuIBM
5umIIUbJbS9b5i46j6BfyNCxxznJyfkqsfkxu0NuQ9c1Inz2eaNlvEn/JxdMlVtwxp2rH7o/nT1G
xv6CnvbpvPfQJAXvMeBRm80iPiw0asPslKL9c+aO/qKHik8wvP/09R58MBxmPuluvi9vDVwC236N
sV1xPW5y4mzRrkqaBT5i/l3Rf96QkfPiPAX/OyuKNRPe4kqZXjkVYsWEl0a5uj2e0IyCd+9rHNA0
s2bQvJNkp5I6VkOMm9kYLW+I/c6uBk4LeyO3MuVbN3qXnBAWt5+02dc3xKceskAuW87L2tD70ND6
W1wz/Xl84ObSgtRm9CBd8kg+lET5/sWbEQmLJIypxbL0jG/wflvwZcNGPnJbrjFSQT3pXL5C/4wm
1sNi7lITDN/iI7qoD2OOqJ23zWXDfizgLkofU0ac1dh3hkRFih87iI5BBE+YAopBZ5ynwzQaCMcs
LL4YtJsefUIEffeCcgGjfwJJ56r7AztiZBbw0yG4Tvjhkwb6HioB5vLPYFe8ST9KiugpNvARahXp
D4YCyMG92kM/G9p42dYHeFMtm68N8txj36GVVHCBecqIj0pKtBbdzk0NAg0WDlA4Jc+vjnfMMxBR
/r2Gc424jeH899hq8ccxd/9acyyuigr0ilc+tQ4RGlWi+58vm4TUjm/uEw2wSqsSWHtU6HiDrJ9m
jE3kHPCYgVugxJSJNfyf1auTu9uVw5MI9oM5NmoMKo3IPrrUAgrgw3/7z2hAXAF6pe7b9A0imGQP
V9rJImTD+YucWmOSzImDsNYjxY1coLh4+U/qAK1rmyvpvwRvYhzeGuqT1sLtyuop7gviFdRGXyBW
5vrZIdx7mTbBdvrMTOJCXpL6XUOhTYIHHqwnkbkTPaxqyrr65GRGUltkpAit4Nc1TupEYApyiYqb
wfq3Gtt3p3WOwQDO0o0pBFu80RnmtrQZwyRnha6Y6YaUYUYVbIldviT83QeHbXffUNke6lre4KsR
NU2sroZNyFRnqWxBersyBA6b2qyiXTuoWQgL0j3EvY0E0410hXGBoDDHtA68qNEZn425iJIvzKCs
pfhdz8OPyYQ32IyJc/w1tceX7jcJg8NkOF1dUEoyiB6i+UicdKDX3/wUavSG7OJtMfpdnN6sfSVr
l11j3MzHBC6pVXhWM+FY+iHyUmony4otHyHpViztrtRIU2bjS4r+UMNpog24Avdh2sq9VHXAtHEI
MmXMSuSn+yGnAXogK3tH+8d+o3wbfs8oqOPhpM2zoMdbYN43JiBo8dDi1LobksqaDMP/Ela5PZmO
yQ8Zfyt6eWaXo1k1okEjImiRCJdtbXQJ3yb5/AgZSZEyKsThBdnSbva8yDXZe0L1CeuK1LX/CVYP
PPV32oz20a9czWX9J73/n8jndggu2TVw8yEPCs1/FNBSaTX5xzQ/zY9OuL0UzSiv/yfCVcpwL/b1
eQ5IrlkrcmzRYfLuJ6mFPHxAn+uhyw2BVvw099KPVPeKIdZz9uV2TGdI9bv2JMj2SRFcPITFJ5hZ
hwQnipJbu7J0/2gooqtQtJMUpAZGY+0g1VKNOL++HFw4RTg0efMvztKShnT9/xEU0PesXD8tz43v
cImNTaRQdFId1ct94HWK/qLse7Ia3uNN5zCIn+vGh/xa8Kt1NeZvZjgSf+MU5rpu5/0icC6EzuEY
3am0jLAhJtk4Iv2xCMBMfPdJnaqgOt1Og74OLTzeUlBkelNefI0yvGfpyhYBpcorsBwqATbr/S/+
q+ZinpEoo7gvqmje1ZztYwCShl0Cg6pve7Kpk+/hlq0dHZZBRZ8QgOGGP7sVIANJ4pAVDRxff4Ny
Mozp9w+SfnaOyD0GHWXYwp3cl8HyiHZnYDLtWSz8OZIugt114ZdG5BSGRZcsHrRnQl9BVVaPBV3I
hWB3Vs+T34sekMxHscbn57ILhxSinLzaeh0x+Q3fq0NEaN0QYpRC37LB9HBNpy9fLYUw978C6+xi
8AaA3eo+Nsx9Fd1NWU1rsl14pZ7cZ/EAJldNYYBXhADWUsptXx+gQ7mIaMBvXb/byQz1ZpUKqC1A
CocKolP+JcNTl3VULWEsPt4kgkKul/Pe+dANT9aLR/P3Q6he+gN3ugCnLlpDCEjT3g21DpSJV/sS
3SosUdztoVEKEcmT3e/s+8Z6crg/ywVbZdI8nQVqidBsIGI0SaZMaQ5YaYNRqZOZzFvut6j2Bpt1
G7BcZEz1Bj+j608DQ9aCILAgbaTP7qLFgs3vWXjisQJum4hY4WNiokVbQB/7Hu0NJT0i0Gj3NHfM
NLJZm2uyj2fjAx2QSoC4GzTJr0AUJpcg09+XFm2sWUyhFwsH7N8FQg6NCoYmCRKf1CZlyeV7/io4
LyyY2BsWgfEU4IOwSjyLH7xckLJ6NGZRuMp/SYvj0zqWf54JZimSRQdyFUwPLSkiQNMUKNcCwNAc
gfFmBW6Gg1oAkogb1Sxhsp34e1XLHH0ISURF8U2pmxP/t/u9BcYnAIaFCVlPfIk1wM+BrYgCa/Dk
icNc5PrHI1EdH1waqGtyZvG2gNHlS1Rjx70X0tuHKMZvztCc2L/JABOLDB4HTzVUqQPQ1e4eoPJH
Tm272X/Hc8UASBG38W/JgRNgX9b0WrYjG66HGNBkhSyDmqNcFrlOXBEHcHmw6n8UWdJGdpUhXiN+
NpF3m9gV5ki0QwB+P2kFF6FgccVqJkYoCl2AM9oNztUPqjhxFljto48WOYU+w2HCOtAgZWNmpr5b
3bZCocOFcRpN+ivx8EvQhPIOLZzQ6508iiCU7JFUdhnzB2hG+dzGUFzMwjokxq9QDQv6E4xX5umI
rp8xnVQL+2BOgkqdaYC3nMjI+99GZNljU4nVaTDmWnHHh01poSoWKrRQC8AiZJqxJo79+b3cqX1L
UrNoCTZv4YGCvyoDBco/ZZhjuEyVwXroixJvqr/Pl09zZxDPSdam59dKtOez+hNZetSHmmdQEUAJ
EQgJx6Ara+SercuO6YP8k3ZASuZBtzkT83ZTyjxugMiuRRJxreazmjTIMK9vFhOACKpRKOopqduZ
JQuIXQYdvvjYvhvH9mDvnbs58+VPx8xKqtr3CGscNcgqALIfwXAt8OGUTFkh+hE3hb8KP/yOfhEl
qEeAPYPY9PBZp5SBrV0MGej8RePpbUGdtsV3IXiGkRiy91elcimlf38gxlujEVJiKA6t2pkHAYhd
UClainOCjmeUfvZ4DHiglLsgIgBh02o+6OvLyx5CXFWj4kiTJGnz0gwMe4n8ls0do7lu7Kw/DFVo
vP4k66xlGChN9fXSGma7yI+BueBh169VCXDXziYnXnhNrLGrqkSmG7Xt8BnzscXpwN8uAnaGiXmV
8TKbb08ygpX1IFoCu+Kt0FWMEP+O4zvNdEiCdYkbaeUBaMav+YXpkSj1KhDMF2Y+TaSUUhh/d//+
YPhicZqtnaRENgOAjYyJVkMlthBQIcFIh/00FPbz+uvp/zeqb/kc3tU/cZ6IAMPWVWgKHTAABcof
Z0zFoZEQXO7NA37NWD9ha+l0B7VcgsGg/Uwa2bbOuAjwOvLRcZrj3yD3A4G7gVDLb25ZtsRpNIXf
f8oTJR3vuPerSUpcvJZPoY+eCls8+IsCXPUh5Eie4sQyIzFz1drxNW/MJwTwbzw7B85iOaeDizGK
1y9LBaAAqgJBPv1psGSoODFt7zlOHSQKUduEFVqL1LLsBSLYihMp3RpAj2J21Pr+IedZ17oWIlDT
v5ugVcmeFW5GeVeBqFTP74rwR0utYFWFAhFlMGOJansJRMt9G0gbTqg6i4Kjhh+OVTkWw7dx5gyD
C2cfRxYaN3UnjN37MDZpVe1pnl1ihYEN5n4lFSEFcyhJjULVefnpqYkKKC5Xy4n0UX7++xzSZ2Q+
z/KxLc3pJTxMTG6lN8S3miJ1MrY0qjcLFzqOqhO/4dcX4mgY3/9wie3/yTlQY//BJCcrCSdNNEFc
4yHEMzm9lEfS6O4Swhr3NpdS3Ul6rWM+pvBOPJdvfxyWgNaeE7oww0npojEfSFNtQbw52GGcwvY7
o5PiNRHJK9GH7zBwT3EgJ0SbvK6io6j99VMUoaMi+HlzMg8fMTlovZS0FDreIQDytWWnGBEMccP7
vRl1iMgzP6JIOH9Kdo93Y8JW/NFm6B4tzbEOlt2XUIE5dcs7WvuLPUWDeImGiZemKQkIVv1J9zeH
0KgyUgmM1ClpPmGDO5FaVID5F1apmenBH/yz5t7o861sPg6eViNgUiC493MNjVLwhFgZziPEencS
QF/MjFABXVu0Yzlm8Ot7Zi/3i5DfqipdEnVOiVx1GzoJINPwoJsI4bpHSuIuNNbUxoRUIvrgeqPt
GDPFC3g/jCPJuYIbKL5AC3huBHdK5lKEbaD3VSIhmzlqCpKZWORqfX6zuSy+uZVPqyalIoX5vsBy
m+Li+p3pcIdVvDtQ7W9RYzrY0POVpP9pK9XvnjGrHguhZO1U6BiAXR6av1nvgolOHiQQ55z96TKH
PDpKrrlKoRu1HHDGv7phFTu8BDeG5JEC4ZrFxTIBm3j83xqIKsvo7pW6Wr/+zsM4cVvwGM95Bl0b
TdlZCTTnjQFcYFOT6tG191/YRf/h596mLizb4SsDyTTP0evApkRVYE8pCuqw9aTBlDGGqFBYaw0k
LYNgOjidCGlxj3ptk2+xGXIuQb3E5Wvli0S0sKFFlaZ6csGo6aez+4xpV2FJa+Ip4/MRoqgnJ0DF
Z5fw9Vt2yQ0EP6onJLEsNmG/AxW9Vuejuvg93kociR54lfU1vNEJNAQ6yqZlM/Q/y5wNNKB0IAYy
yGVXwl3+pKQzqhHQTW79sInmULWWBuoCkKs/DW0kGvP9w0Bi0FcEeogr5RySncD7xmGuNJLXV41U
1MxA534i4TRJkOyVczR9+FrGHohZGuVDjOnKdMGcZvTufvLQUtBDNuZ9vcA9EwQmmKTHkulZPKKj
QP8dAvUMGEFHW5QFVIgQahMwvaswldQqUORZ46kNQcEJ1Ukb7oT2ftx7+RMZ7gjFXJX47QC8fpZr
xBDXXEEnybyQt7UQoGuYxO15hAagpzjmJ6mlSIOknWUugx3zM2o9EO54bBLycxT9z2gv1SE13Osp
fm0wS9NjSLQMzLrWbBC37zAW4L3ZoFwnksimRELB3a0KNx3gst5WrOcyDi0Ha5yknWeTKk/RaYPd
pbRItBdaD+EV6nfKURRVpUeGXjbETFfz0vvRU2QAdCQOn0GqrAvWCvtuZGuDVFks7urMIg/DCmC1
vaFmLBP5EdgqDp9bPayZNfKE+CpQE3VfGfceNR39Jj5vbEE7nIAlmWIkk07B3884Ylc6+cOqJ/s8
zUrkqBpjOEhPfXf8pVJUBt/bh/tbwNBi1JWnOiaVlHKpCPFss78UsdeRoI4nE9T9mnOltEdJQVLd
XfqRCScAkOpMTQx8Q9ohAWOMMd9f1XBUeUgQmHkw7jb1OYIyxuh6udcoVJLL+wOjw34zTsdXvnpg
61rQ9VKuLCzkKrlbZpjVtTXXuu/JJKdk/gFa1HrdJhf2v0/0X5veJkxde4eTlI+lzsgVKofIKuLx
dR4oz41s1VUxr2mbpW1/3Oj2dfTlun5HWgZuIkj65nRMyQMZfffERm/XnG8BbleCVhPGldVEIxWc
YzfDPcneO8Elmk3ohxsPoEPuj2io6QmZ0QaP+kXt9U1DdKW+kQueGSDjHcveVFUv+bOI3M2y5uz4
WrYGEy3N9QTYuSgpYFLrZdnHRoA8zEWnnoMV0VAdR2HW4toIg88ACH/m+5MwQhhJsdFrE5kxv7xR
oX4Uia0v8OH0MYokcQElby+7qH4Oio3N53YGEPL7M7QDs/BDUvBruEVNdXqRvrCNTfK5EvMg4ERF
NylcbKHmHjpjIo35oOqthVb1LJlOelgwdq5IvMsSUhPL1QogSdO/6JUofXhmqt3o6ISHrycfpDEB
fHs4CAOM/YqBhGVDRGLIiE2zq759sgtSNxXbp1qoB9pfIe4rUipoyyFhIj2l1VciV8SRh947l0jy
bApaEJ5LQDteObzU4jMKl0I4tHVOv/YcSY/gJSTk9qL19Dv8HWHav/l2uNvcLfBsHznky7RjmNfY
ix9EqvNVtdlXHyOr7AytNFa0d5VAy20JXjUCg4XSFFl7fYDaZwApdDWMbRCuvqh+WetydqxDVl3Q
jNS9venVYvW3xuL4IdecfwBo6f+R/VLOGcTXzFbXFtdS/lw7NDtymhcad+AtoT4Az6UaXebQDSE5
UcQmXvLOyw0Xlh3SQJvjbxTHQGiZ42E5n+v+Ksc8RDH78Smw2m0SHCdA9h53Hw2YCSS5+BtN3aXm
+erreE74sZOT0xkBsT5OdjQR80iWHB24gfXFXHI5Md/yMRssSEhBhMoQmpkRfJfcT69HE9ljaFta
U66XGXkGCvg1vexabDmLLCJ1KojhTk3GHZ/sduihKA/DwKR3KmgehfosnWNY8vPXa1faiQWRPMbq
ECgXOJ6JNmwtn+eu3uFVBwOCUDoxjQyf8zWVs4dyq0MtdIV8s/x4nWs7F4faAHUwzVbq2hIX3I1l
kkK7ttD+a3x7RIYpWyTGC8YpHeetMGW8pQCalKu0zNS7mOCiis03YCowhAE7YI4wsEe48YD+T1Mz
lm7nv10LH+6xS5tUkD99bCxDhKNvmEweE2efXgi42zJEjH2i8G8hpcuZMFOjdwFILQhWE5O9weMS
jAVq4X+/MYVOUXmZiAJZI80qieGpkQvRmsQY1nyuX0BmzEvXP0Fx161FRqIROjvswRtzTJBKx1rs
0+rgTiItx7T3VePtnFXnHvaS8hk1AC9SuOibzkzpA1EsHt1zOgTBA9N42XSB58Gyl7lf+uCa/4Lq
oUqqnJx78CwUUjX9TLiI+N+t6jDPqWbBASiaXt1bwgO0yk0xoajZgWF6AtksAKbL+oY7jplps2Rs
01pSlYljMMzdEpHn9hjjrIIa5dI7o1OEg8bBUgUau/p3Q+bt0Siz6U7W1gGWkfYoLI1zOgZSBIEO
XPSzDk4CfsbsmI5Nlaa6ewzpc9vxXB45jIdAqLPekVzPABE/wbeHU6WElf3RfQ52+oKkNxbPxePY
VUQX5UMLboHYISvAqyZmMOTFT+/o/VSCiEX2+DjJmSw1Qv9sOYpVQfKJ4TncGoagggrx0/kAvALE
SHJ6hd0KZOrLfWrSB2U8RRgRRl6CpgLrSsTovGNPMcbsHXp01/ap6IAhJ/adN3Pm0YQpyxyzBwOE
tnWdIcTvk3ME3XjwwtpAHsY09XvDSexsB00ITjMDJEvwzLkh0edBmimGxs07PwSjennBquldDX62
Qh7jcS+ZJ8b9HxJml2UUExUAXITQqbyVu8L9gYEiuBZPX+Qy0br6RbCUKTx/rRhe1T6wrPIYU8Pi
yRsnvPPwMk3bn60HvVBbKNI6jWGn05hiHX1vf/RuYioAXj8JFkQsjtv3Dd+kDFGLRzkmPyhyPYVD
GuzAeTkhi4ywUN/2JG/HICn1pZQ4TK2CFPOV7Mxt8VyhC57yXQOmw987cb3CeCJJcbS8aboJt4NK
PoQ/4+8ijkM42I2IpWXRAc4lwYKUvXn8g/V8R3HCrS4G87oZVCAxAzfEUNC+Xd5VY83IBdIoe9zs
NqTfWuFIPQghBDEQZ/3GBzTqJA867tHUnhu4AFroKPOAuQqMQd6urQcEJTJfyWacs9Y27Enlq+7m
Hdq9i2+ecr+ttatlzeefa6XRUNLrA2roK6Gsn2KFy/yFaizGnQO/OE4sNwSyCxQbu05NoqkW1lbh
BK9ZbjtEcrEF9br8zdabvOfyHs/1rxrpE5d1ukrjI9psS5rAqraYL4gbpIlCLj3tIdCWmSUtkpNa
XbPQVO0s7058RkiRD7eQCS56clL4kF89VWxKSf4LjQHTlsz6EE2Xr4adsgBkgitIkgxisFHjaRJS
cHc0BFSoDwy2MZEEIiiCF7uIJcOGwmpNeCDRWsfsznT02ARYEcvA9NtvxM9hPS31mbIkwpqCE4si
5qoLuPg+hC/xKSE624aU3XjpA6AtIJEqy0PH+ncBv6G5JvCSqoCWg7jWPvnp1yuT5uQNXlvsH44h
mvdH7yol5OWB/BzgxqQzE3GCm5piKZ7JbY+JtWExsj0X+e29Ee9y5kdkj1q3BW39A/u3hByoa5zN
znx8wv+Bdczuhmq9yd4VIFMi2dQViXm83IWb6iirXANEmtlv8sOZOdKJAejQF+QgyynWVsIgf2cX
QAWKr3icfGuIVyWY5f4Y9tXiSaIf7Stzsb1RCrKU0iuvVBCKQQ+ii4HSlmWLudfk28yDPvUurGNN
xic2Jd763XEa3so0C5MauGERO6Tvkt41UnEkuC24a897tc4aCvdrgXqYd16C8qUCOlwy2otZmuI0
39XfzfBDJprMu8/y4y2wT7k9T6WATSba8YtHvvdg/aC9HxcEvB94LyTeigv5/XCKiIa9yhVWN0nT
Sv/TtpvEeLL2Gphn/ij5P2lKt9HjULO+CAY/uo5HyOIPTuOqusM59sVPzfJUMmIUXe7H/Gnh4k0K
WMFc5FqIGYaLcZ4ZWz4ES4SLTV/wYlDggMcJ+CLufplwrKMsbP5eG8TlGwyqQimw14tOnbRYn08X
xnsJq7Rswb/SUfjEA1qVW2sk6cCgq634U+MlK2wwpbGvApLlvlJiyj5oaP0/nVN/eLKWLm0DtU63
TqUfQgF4s8CIVk6SJx1GDdW+w0p6QjE/AeqWj/86Xy/A0orn20HCQ8BRPxejD0FcJaEVR77YeyEK
prVt+HPtIydNOYru3J3KUVahTOd/BXE2RRdNaAni2CkTnWQsnBume4ic7ji9yqsYY9BieZv74cMG
f4gdHqS39b4/xkX8Sf2f2bb+caI0ByW75fB3qddipQIjr+fbLeSxPn/vuIVXWHZHeLtuxTQUU+Lf
WXhdFMKkg6mzSEYG4l3Cs4lsHVHaiqcEuMf+PawTNZsEZOqCu+ovUITDGcnqmQD2+vZYeEyvCAbk
QVNqG+5NQwlhD53P2TPQytbkb1Q1OyMhh+U/uEXiQ6XshkOMPBbcBKZCFHe9gh5dF6eClrbr8Bdd
CqXOceFHDMnnBxAUgMZihqxnJ0Q1+Yaek5oQZLDis9ocCxpXgEOg69YMkZx8SjGUBuzFng4ZdnZV
9+nXSChHueFevuAEuzn/rUn+4i1KHSHeMYWxbF1vQUKG+zdrxvnU+IhfOhiay//DOXvv4+g4FmGi
pacgy+1UfbaMXCqcL/5fwEsDWsnXFiLO7rNW4nyUbT7xDN6Ml8nsIdRqtRvJl7c7zc0+PC+AnDM7
Ynjl/bCQegXOeWMhBh4ld2Mh+opQ3YXIKaHcRm2mYN6pDJfX/ApVXnd5I0UzOkPB68EDTdLU23z2
6QOGIMdxxcnD6+ytizajo52p8oE2v/wHL6wIPKqKVNOW+QIjOWlU9R8C/cw2634sBoKZXPX1Tzo1
bUafGpC7JXGPNo1IHNR8uiNrD9a7iDeAOQmqfMsu4IKcdzJz9LqbVPS6hotMXaVVvNPTtJfCiFtx
Jy5WfMiJfaN9bZvFtS4xDszL5D/byyDCtn7Ep3GDombJCmWSxVd8je4OUcOFexx4GJod8Tn32g91
rZQxKd9lnVY6i4GRsG5jo/laTTDHc61BhWRH+M/4JjDVi8EAmNySczg+Z547NqC+3tioCsrXYMZE
opK7zDMpKDMo8Z7yHN88AmiWh9rHKEWgSHxd0zYNLga9dyzrkVHAnxBw8pg7LImQ1BA2L8pasKIm
czK86xYZJCEb1wzFJXsn5q7Ja98T2rL1QppDhkzOSsWsHO1VJXQqv5k6o3CTRhC9yu5+ZPKMoQrF
Q7qnXJDZWxB2qEOkrSFif3Gta44gGVwEokai+E5O7BI8lvT2hAUjwv9XMjvYxdsXuUHCwvby8Xsg
R4KXC7esEpdc1FWzEW410fd2QtSxRQNTvrJQtYFO9hKsa6JG4Fplz+5/L9VeqiJeSt4/dgR+JMak
O80AusMX38YLw+kE0NcJGs2TcSfJSzex22EQZMR8dN6mj6gvKNh7veTBOv4H04rIOr9U9hTjF0xg
5cttVyP1nKxZ9xVngpHV2aFO8FfWRP5tNM7WDqYt8Gv3GtoBhxHLCvNmgmD+f9v8SzV4Z9svzHkh
nqW8N/2IGuoJdxfNcBabAhqBHKs2eB8QF11dPc54CoaAvyC+8HlfDUgJzZg4Kwe1j2p6ocQxE3AJ
7nQtwWBbAI9gURNg/jt2scRrbnL7Cbf5nH13BTpNX1IOfnmflONAivIv6VcnDh8k2mJI+Ivvbr2p
44TGp7AZ6avpZ8v+tQHKN2iDXTrn4o5IfjCQaTK3TOxCrq7enXt5OCjMtF5R0ZlrJYvy20u64zjm
+EVrAShBY0Bq6AiMjCDkkOO79Nrl0j3++UETcQa4v/iwfhDESSw0GScZkKIdLE/K5HNvvQ8boZEJ
wRpM9CQyR1Ri/EG6+J3W0WVo5+Yw2cm/75O6RrFlxq5t61FYDOT3etN/gHD6OPfsu4NptYOr35g6
gxlKvo39fNSGehM+ur86vcDYPBe0St6wC8fKBX/q9PfDpfrp5RNAeMrSJYAAmyArawK7Cm6oyjWj
+C9sfcmRumwSo81CJb/GjGoHpqS2nu/JYi6VVzoIreM3C1bTy0usAr0utRlCKWeBsqPOHQSyaiaA
1QzRRcJZJ9ZSs9frtsp/tJ+WjKSO8fyCfGDIBYDkaHtj/Kth7ze/hyOnTDW/TyUgHkcbidCIycNL
9qqos78X2G9gIXVXa4dV/KjltgGqC2zHAUfU1fqo1FW+UVevJ1l9ov3CVFRLfOxVf3odmxU95HdX
5vK1jAawWP7wQM+o8ch2L2lm5kOHm0K6r08Y4FhAIDt3h6PM53O16+AWcSbDARz1ovIxN5+nDX2I
ZGqiM40UDJXSizHOkWRfSOd0lbNJkRq3yr7EN6ce8zTcGa8wn4pmTUDOulWGLCzIsbC4gLAyj8OO
J8dGsq/q+al9lKBZhNpaEZI8zf6o8cv8edz6COJ+8KSYuB7QqAY7WJn9mUvDG05juuOhpS/BsxT7
g+3mBhvKo7cb8UP0xaaHjVpDn2+MNlEUuzhbw3LgWFURF9uXBXTDEG/piD7UhpXDFjYpIACbuKIv
15mtBbbwjbd3JosIwa8jUoie4z0fmcozPPUJg9rMAM/Tc269PDN9KWbuXVAJflCDVQVG/2dEFrH8
tL4Qdsc+KYb17rVL45ieQbPbaHnlnXyS4odl3/E6bXDu/YbDO35OizC6TDCPG+iWKcsvIt5d6YWL
VuTBG6UCZEI6Z2wWfUK/Sxo0oO8wTaGhB6qwPk8wQ6lHIFFj2JaFUri9guwoSI5qdWFOpoYsmHAT
U0zM5oaUL2iGztWM5dkg40DK6JgWXfMuU159DTjHXUOUlyDqPW6QvV/ZJS+uJqBOQXLvsLIUe4qR
pD5Z1+cML9cgSUyDRSVb6vjThyitHdyjD92PLX9aZhXL8Y/SeENs9B8S1fv5QTQWHeJatVdFVdyw
wEzGJlsu424i6TAC349NHQ1ohhoK6k8J1Ooz6TkkblnkJ//KeB7ryA/M3NmzwiCXJC21VR21c3Gy
Km0w7Xh0ZvVvAdEahJj02oUqDILk4mvvLRQlK/oXSF55Hys5q4pDMnx9TQFInbCueztVFa46J2NA
U6l3J5nQSqHHyVmsajqAN6Ix1Vrpj97TvZGUu1QO7n0bdskJA7Iulwpxk78G3NCXOZSvSPMGnJpE
06ebd4GZEBvwB8Q0jo/YCCpPTje3erOYnKx+7Cnwq45R3ZpmRpqSlJjvyyxvuOyQQLd+OGAfZFQ1
XT0X/FaBiG+SmrOltlFb3yO3SsX2W53EI2cjAYWMAISMC2bwmm9YIRYVaStR8yovv2o/uTcZK/Pw
0ZymRxN+7ZhXmTTcmgfyVgGUgaL2ESiP6UTBDBj5LMfc61uboiYoHXqVAWettbuCml8Je7SbLw+1
+CXs0tsWxonfSp3ItUcAKlBW3Ei8jjSZA3ly/vXyfJ1CVxUE+iV+M8QmugMK6ecMmsOt6DlUfKOB
RLh9YhCrg0ss+6mqHP/M+ZvanUe0eZt6qoWqWo3WErDRtE1v149qCUZvFR23LUHgtrv3F0rtENGH
93VmKRZcw1a4gkZOAYKft5SeRaYc3uya7AWIT9EcmFvu6G0z7qSVOKl5r1mer21o/1LAdp65EL7s
2iZYNfzIFLWDGzCZoHn3TXkZtI1dkDOxSBrttvoLYBdNV/KruxNmDqSDjZW0PXFQltmcpGSlv+Es
nt1Ht7N/Us4zpF7cf0rB+z1OMzDurMkd1v07dfNVXtvvWaM8e1joiYDwCe9EKiLVPB7Ra4MH7ln+
S3fi5EzprSYe+cOQCh+RqeyhoiG7u7HM3mrjOEwlZiLzZRv1y4W2Ude/RM4JunvmJCWf8JKjQMUF
q7Cqup3KAEHtHjMGNeNy0iDRyi7TYfQ879XN85Wye6xw/HBTYabB1bRY3gDnRAzCreepYhwc20ay
PtwLgoAWy1vzgdk6zYu+lT6AWGhgDRVUCvdZoAkPkxh6H39+cDAv4+UDI8fx4jJQC4j91KZxP8rn
qnZ1Hz+zW2TyONaD477/HzwNCbE5lVDsNqx8dTyvzRUGekxVY9J+xSgnpEYjvHlFFfwAS+Wg8hCN
tmRYZki+9SH1+8qJUGJrJU8v5Vi57dMaIi/HU7VEoHv8O6y9w68FkZbhW1N/3jIpMsDJO+mCH4vZ
LutSVTO4gHWuLCQyTHoJd6Wo4ijlp/48VbZV3eWB1JxwDpnxg/WoRtqv8EiCk+RgVJqeWYS3pVq/
ry/aeDmp/IW4Vm2Gkh9WytJP4wbu3FRxMkdAjC/3HmDC7w6qY/cDGzLtZ+X0eqFjk3RS/7kZCYk2
N+vTv2lE8mRWdEIjf8uDY6xYw29eZGqUkJIKOtPb6zQLmwY2eMLciF0H7arCwvLN1KcZyZPS8j4E
IxZR/zSiQ2LjQLkKD7nXXKki+tKfBBSKimub7F7HYVXrOSmXrLanem6cpw5HLhi0bGJrPZG4leGb
Uagd1+2yJ5pR/MFs6dB4b1qECxLU14pfy1w4v2Jr1ewyckUbKFmxny3BXsIOf4jc024did/2tMbP
p1A1DfVTwGcwlnglja2pA19u2JsZrPvNLmmfrkTcp18T9Qd/PeHbbWfu2iSRZG8Cksl/7aRFT6o5
hX1jeEpLkKxtjUZzboPcuddb5jrE/nGigebUEZbK4Cj32dsPlgRoP6BQSO5Z5+SIm0iRELv9wKxg
GY31nZcBCQKKIDPnv/+JQ0cp1BbyrJYP7sAm5Pj/vwlZIUDv081Tdr8yWuPPxisLskktb4K/KRNr
sBHowYgcEj7XRWtyntvvXhb5AJPsR0DI+VOH7cluECksGx9cj8lufmVX8dTqr1n3YBc0DiWGVwwI
jmQV6PGCO+vnxAhckuAr/wzLS5IS/gvgPJ4yv4AyzGBW4srjqVyrk/w16+2GupJQtUfwE5RuQW03
I3o+SOUqHe11fF18hRWs14vqNrwcAPmf3pIkViEQz8iKM6MSkjU3+i7/sYUBwHQS3i1Wg1n31SDt
Fipa8mNMUaiBSs7srHfurbDk3X54qSmZamWH44O1Tqeab2eR+LEK7XLLjwiWYaz/W52OKHyoWgda
OV11Bu1Cw2YKV60zUHV2xaQEG+cZvTK8cxKZ4c4nbwXPoldpn/C4Mi83CL/yAiSF8JiPw/zdukYv
PuhvZ2tFKvtLd6Hx7dg65mZ0yHXCnfX32kHtUMly/Q/okRXRHSz8EsAjmeOXEUcIhIHTuhJw0FC/
A0FN+Fb9hEilTTywsXv7n80zdM008Ef0TMlaBd90mSm4Rk6yIN/6ul40H5+5hsqGvsQycSIC2cDT
u9rptZSYSfUaYsJvdyFF4bzV9W6+obS6TFUHC5YT2f7AyaUpYrzKGBLLENHqw9sshSQiCxNDRm1P
D8Uf+b4PqF1ufAKqOA0oIG9EeZoCqNTQ9K8HZzvyz2yjKl39eOHTc35VJJMMZg7q/Jp5VgflZCVr
Ek9kTyyGfKEByv0f8hS+2rVcHtcQZ18lNzUgbVmJ6AQpQkZosHKVVH5OMP9Ie0dlnqIq4X32sA64
gvPmvr7ZNbbVW/idNEtiFT9zwTi3adXmLSi+zZ1nAHaE3aRyWNEF+JhCpj9sEFDY3Xb/K8nqeY5l
hQjX0ISWooKXi4P6SbNqPVVzQHvaCasyjby9Nr2ciVwjFXvZt26oaho81iS75gpeImRTotUx+T7Y
XKAzU/PCSk66JDHqkmwb1MlSKWpVtd2w2LL/ey3nLZ8+f0SllO0gFsnmkjUEqdzD2uKBQwEzJVTr
IQIUwXNylP1OiP1aTZxzAjOIhCGLnsNegnz/Km13fARpgeO1rEXnW18i/G9FIif8MnCPAoYfF0AR
JHj3HifGDQ6o3M17ut2Y/wJy39o4z01epz8kJIiYsSWCsaLJT7ar18vlrd40XW93ejkrZC9Kq7M+
tJyBF4WplEzoEIG9Jsbe9I31IX+4ExC7InLuxtbL3NZS2NtFYWZp+FXW/EKV2SVKhm8PLG65ViLK
8A1F3AuR3mRC2A3HHPPYk03gfpMdkIyAsQiv/qXuWkgKMfiYbOgCQsC7E9IGfNexEtZMbUG4kIgk
539UEFB8bXQrhfzhnvZnp0c0AsAjWWPt2S3IYjszv87bpOJCjzcDhXsgPB6SykvH3cPQZgJmgVVl
210zTI7WuO0jorgzF3sUvtq/adLNO9Q+HL1t9kNfL9VAoKmn4sOCt5UWsmP3hOyK6DmpRq5xdsCO
+QEHja3TUL7JZO9R+lL4ZV4jsTUvg63f4wD8sPCOytDctd9128eBoqrmJexr5F/mYQAM1VVaUKzJ
7XnwI+LSvedRg9m5XvNjjlflFx9k0gHOSVg2AFmCYlKLfcVP+WuoI+j6DiAFOs0qVJQPjoxG9iCd
CoZKsWvFbZcOb8X84FnAQLIWbc4II9/f0fa7N+okOlE9/h8IPHVbLlx1r4u8OZ7749pKyj0/98f7
d7+lOBWx4G+XmPS79i/ODwjVRjinDEdQ1iHQSsqH1x3EevMHNdocF2gj4uVIeWomkIv25cw1HvJ2
MyZDaTsYuaiXOr+l86kaSJOOHlKNy9MOs4TagdyBJz2sY7qkihA6kVXIXma1WylAFdjNm/PI5hQB
F2vOmr5VrzjXJ5STpvRLV8HZNFo9meXYNWGcrS2Wn56TBlceqbt3w1rZGSsK7opDL0s9dVI5vLRb
VGc+xjN51gmhmGhDFoxjB7kB+vLXbUau3ywsBf++55b/I5Xa8BI6Ln/ktheLYV9cVk4KZ5IRXdxg
zTuoTPiuqmfSLiw+sKqPO6H7vE1LyeBh4gyXM3mvfjcD+dI1uPgSLrhl7DeVmQMQCF8gG9yPP+Kp
gWh/2/W2JDmZ207y58CTwZ9fkxnHVCDyew+xbgisb706leVWBk+VhOpw5D3q8WR+iWmkseBPSwG/
JS2qvTtk+Vb1q4uRjBsnlvpo9+JiU0MZLUpyKUQhAKNxTRlrPPSCzlOKFeAYYCKbUo+2tJz7N01V
q8Tjpz5z7QiUGcGEI/lfUHJx7es+1kQCr9T0CI4uX3yHru+3tJ4rIxt/hMwIXiIEnwoYDxPdlzzt
cv2hNt0uHTaVrQpqkDxGaIfDqVf1i4bp9+Ju7iNbJgfg842dmG3fV5Flr3+iCp/gA4g8xhQPWI9d
lVZowY7DAU49mryNDLhn5fbqQSNA9ld2HHm/ZsMH0HddYhJ2A7xL/NudL6mClVltEc2tI8Uf8XaJ
SnFTi1cdiMOxvrozB/f7xvd0FnNSIFF/ZITt5lDunTQ9NOx8DoSbxohXV8T9Sj5utOcfmkrTMTcl
Suuenxg0h6bT0g+VnGgPQZqMTFs16Uit4Crb5QvEQgsDOf3AqlQ56iYr5BtpDBR5o39LgVdiDwya
UwBQSqW60bMIkMs0S8CL1XCchmOnpVffrW4+rxouRSB2lNFxoa6YwVPQADinTZe8AavSNuA3H7iq
+UUFEmkpuRtvcyhgoCGhJUxAT3F+Z02HV9uO9I2YSq/+5OJfp4GaNjgMVQwey8C4qr2oEIyDhsri
Rn47u5GOWwnfrhxcTdhv3x32Iy8nOkVtCGUuzlyY62hhEEhSZbolVVIDLGnObwKX3nzPesZyX2nw
Mp04qMW9yk7mw4K0WzwSoYVFeZP8/req/7pqz9i7bzXnCJAcd1ueZMUi+C4qn3i/PTDUbnXsiJCc
F0f9/oWYAUNhcNZYjI+0QOO+yrdVsMR57ROwKUp9DfolhQgZqUtDwEyEO0JekalXlJzGMHNwiayw
4SZ7+8e2BFV5bi0x7O2KXUJ6ADBx452Ll67AwCf0vD4gOO41by5GZbR4qunLsHFzYlhuXYTn3e53
b1KAKTJSEe75Q1c3JSPZgIs8XZDBsfkSDQuKgln6hs1vV26bIc9hQSHCw9RU6lOzRANRuiu4qdYm
hDfFP8fuL2Kxpvgxm+YiQTGAYKun554yDPXo0w78LvDJaxzhRIFcEFPYfJjF5Tls1bUrLbPLpAbz
mB7pwSQUT8w/ra+Y3k0JGgH/om9uneij4Rek+ltnd7jGKxzpmF68cllq7iGeO4FBmJ9sQEBOuDqs
uJUyQaVM+njISQ69uMBqlAX4s62v3rq3h4elXj2qNtTETJx7zOdFLqqOS8/EQ00WsGOUOXIl7/8N
QHHLU5qn9no4kSFnIxOB9iggMMmQVc0Uz+02AvUH58mI385nMxP43dUkrzzsz/wjUfkRXm2E5xlE
Zgs74Y4VefvFe7nEwUeUmVYRKZyozLWCeKl+/ku8KMfoMMIkxMp8Rn2kGg9niJVyQgWdJXUfiWeZ
nL1fIHZr2xhIoA6kHK7TdvTAn5OdGjraATS9doZtPglVDDYixgJwey2MBYvRfgYX3Dl5WlS2wAnQ
hJfqL8NP4Otc35VPlWSzJbR0CTrhX7mbhxKXGXzxZ9oN614EKf8X6Qjb2/EQcwKaEt/pX/qvOOH5
vgkZffTgzl7B7MYFrl1ms7sgn/tn7mKzkFh9WyFQG1PVnUbADsO07YJOIyKoL5j0+BLFzlF9j9fr
DQZQ2V/UhlKo6ng1g8lLZ46P+7+254gI/BqYYQ/U/jOhX6KVqW40QdpIENw1YrFRWNGQCPZUhQ0t
N5ShZVhVIq9aTcAYl7E5m4H6gFqXNFOHwfRFe2J436kMRSwEBGz0YSCprVZ85lNlJqF/EPXSHRrh
3qyqB+bYsoEQSPq3bd+s4AutTXNDi7YINUVKSmWf+SZSVJmph1VMcpjDNsKtOeoW3w/QXjrtvQkv
ns6SsQhG/7xUdX7DTiO/Zjxadtu0tiLkExftBTGuwJHAnbzyauqA9ExvgztB3LkPpANV9vzWkDky
JKhSTI77EFZFoipkKVZM8nlKVkRZW5Y9XAqu+ewJg/RgF/nUu3wCUlDy1FRukru+GJfF0742FQLc
6SESNtGTBQI1chQeybWB59BtX/WxgJyZbHZknsq54ucYdvqoV1bIeZIiugE4lZy9pP5Fj3oamYBV
kb1c5CQAnxBleGbEYrQIskSUWAHxfnoKvCAra7ULh5xIOSdPV8zfWw7jZwCjfvIZ/Gl6odRttYwi
Xc+PnKMlmOfr27vjnfmbBKncaTOwdgzmQAlul3jZEOT1AFTZERxekehK4+0XrLkXSTxqX8NHjR7S
9L00TfwH/LEtp+xFDD9wiW/UM9a/6WYCcD0I+Ib6l+nVkBkJc79YhdvnKnj7mRxUeM30NoP0i9WX
oN2qOwQawQwQ8Kqrcx1N6Lu4Fiv6IS4YAcRfZMdRFhcng6qnd2h84poH2h7gzDapwhhHa/hzqs3J
JMpi2YEvFd2FR/Uss4YVu1sRk4MMQDmKYJfAGWwHrtZ6jlKYWFjEB9Q30g/qsRNgoEkHYAw5bVzW
RPosaeumGGqznNZKae2Yct+pq/iuM1malDsmAy7QiBlDeyqaswCu4zMpcQ8RFyBHJu/+T5Y2e0fA
RfGYthrhds1U8fTAapduqMInOiJbMv6Mrl9fftbE7X7yl1pJpCaP4IeFjlQmGpHjPtK/4V/t7hhW
7XNNM8zA9kKWaVKxMOcw40CfJ5NJlpssoXNxDw+KsUvh5jiaD4e2SdQ+toWxHmevt1HvSHTcOJzx
WjDqN8u03MzSheDa1qYygJcqPYSx8efwXWNG0uvKx/t2NeeREzX8Wqhrdqw4JmJNhdo4HqNW2IbC
NWq2J8oHfgpLOf9YJ3u3TZdOeIFQXqFBUGLrg+CQDtoK3143y3ahIP8tGKD8GwHP9+H+sQh1o49b
dG2B0TqZI7Mh82ssC7whjnca8QnQFpVmtwPzDqv8RCPm+jNtkH9S08l5l7EfNzLax8OqtM2nS0hp
2lcD3HZkPb9aD0o3oAEjlzkQP1/Hzi6VmFWIB9OM5PbJ4QD1COVBreQdgvFLiLuovwBE8FtjQIXz
vMmi+nmPIpEoNQljajcJzQZwtjszJIaMAfuMPxZS324sROVIzPe+RvlxDhmnXXUJcAPKt1pfQ0rV
xuJS+mKp5h5qfeVj/PJs4oZ9rKiMvb5t2apiTQkZFfB3UnhK08LKDZEHqGLgD02w/axoFhtQog9u
qLgcRVHMO2Q0uwD0y0/5R4L+3tuCje2E1AWJ7VEBvbhmqPCA7yN0yedvYmkE6LfEX/gfxoVkk+Xe
ziJSOK1qNzutoZRmaW1pFu1I4FEJZ1vB9IteXF5R7bAnF2Lg8c2psO6O8HFd61QmvJPNSv3NLhh/
d7z7MmE8K1KSvL4OETbZv2az20YGE478mLo9i5FNN5e5BnsZBmQahCWIXOblmSkMpMIwJ0oAQDxR
hLv7MZAj0R+O3XNRB+juEKF2IPEnLYfJNns7/oQ3KY7IXBb0znAzhqq8P1Xb9ooFV/VBycIubHAi
Nd/9LlaDoYhjMEg+omuEKClnE1k6WhIOAdYbgrcMSb965zoVfI9e3o27HpzL9JGrj+XRlB29h55a
3LsoHeEOyowgOSlpd9RjT48ZPxxX2k2+XFkg5JU8BKoG5HFrmc2PvaCXZ+vONSdGymqwPpfJh9QY
OsKnVWiSHZ0xMliRxX796T64su8EK4FN6mmsEh3BJOXhTnlzY1ANHf6IlzbJACuMcOH7AgS45IqL
rkH9kZnJhLXNNDfDnetYNmYm6E+ge7+xWaj1iB0SqeldigEnju0qAu0ebl7oCGktVwzvSXOfJrhl
CZ+89UDUkEkQRIhKBxiBrkce5pq0ya20M+lMi7bpW/ZfNIeCl32Jvz9W6+gOsFR2cOd/betTphZq
fFrxGGXyOcvOhSkG553aoG5S5D9pmpK6jSVRpt2AV3FwIqrAFaUJzXmZnANva76DH+bsiJjlsp9e
ObIp49s1y8mVlKxMCyIt6ItLz0A9HMQX9HPnUthUv7yH4qj0H2H88V3Qcw7OuKjQOScNTf9oHlUb
nkwyvzX29fPHaqSCurvKeleMC0+UgKTpwCojzqQBrjlkE6KQc4YKER4r9JOR9aNkS6Rk11IorcVk
XgOefK+pRtqgqcOEG1FePmYcBVn2Zg/tySqqOgv6G9PiJET7jXGA+x1fLnScvu4RedmEEQpvtBUG
SdOADzx0pA2XyKGJPwuyj406pGMHibj2KqpX8Vc6TwS+AiHM95BFATgUEgr5Qp2ukyCUPEsD1r0z
LKw/bh5S5/9bOoy02YXuGbGmvJY0gcjJYv/UoglJnmM5hDvr6I1aTaGgV8UKCGIyRdWNnK57t4BV
b878Lyma1rgqOd1Twsx82zJ2wl5v8T2i2qAxtapzT8HkbW9H+Ydkw/FYMIiWrgcu9o+1Oi08U/zI
TndLgbob7FllrKfqfW3HuxnkpCQv1Ybxo4svjBDTN4mVeO/VDsT+MRkvqkuJIZt61EOIO+5KAyqW
/+mG6VD+uCvsN4YujRMse5dGY4SXHytcp06tBEksm6Ya3TOKCXf/g8JOu3SSh9m+2F7a7NPfvr61
YIj9qpkd0G3cfbTgDYCchDNk3ThwqcWtxIZiPrThrjwyhG1X7gsiYVT9nWgZ5ublw4J2+kQQA2Nu
FDZPM/hrt2/Uri9hgy6hHEkVal6qJq1ftVKyjYvPu5cszVbYfBeBFeLt9VinOYNibehgYQ1QvTez
egv2h/iklqRQ8i5cPkh29+Jv+ZJ9uLp6trG+pjLOFOMhPVAFHoa5FCy0okVvfp1uzb/m90+WIm4q
KOUjoX7zbGvikOHn5mugZC/C6DaqfPMugOIOof3/YlU2mfblItmR4hXogwqK+ktLtM2wPoZawBgL
zDL6CdpOmzr02jh4KCRyn8MloEBVAht+queYJxiIBQXoFhyn8DKVjnQeBy3UyF779OLCnEdwfJop
I6QsoEl/v4zC1bx+lSKGo435y48vvjL1LzEnInzUNJ/Uo5E3+vI3MWWOyNO/h3vulvU2rw27kxW8
6x4OrL+HTbJXCclAiyXFNmclshC9bf/MR5cvx1CYSnkFELh6RuPe4FO6+O+JqIMQpWY1S9Dbvym3
DoI1KMgGojBhartgcn0kNNVMCqVD7FIL8WQukhryfTFbUJaSPbVk4/shV0dC+cATh3LOrza/rmy1
c5rO//1K8wpwuoeFjE3fC2cyACydA52H6PMcjBiQk64Y2jAZz4XTONkTt4XdV9QVeYaEtYJkIs65
RfJ+L7QHwXqokW2gLt89VNPZgG+8Ul/eseyCpGgEbu5uiB42N2P1ja12GOV90WZHicYWEXGw8p46
ZJkLTH2GeeCdS2w0NpS4H2NxQAcAO6YfoEuWhyQp/M8aO+JsvyVAS1Aozox2UoJKR36Ov7vjq0Dh
JVjmeowPqBvw3eOrdZV0HLFDdB54lAa8rfQ5ludvR78TnbjzXt1VnZN4KqbcqNdun4MVMmxZG/rI
ONmS7vNMdyPhggEdRFMtERgx64Si0HllovsI3fGGdzFvu7cGlKYNlB17UGSR1pDrZDGND1vhHJPA
EnmBzf+OqQkTCdRuqGZdKSeYO0pDXFJFTFeiuIsVEfFqehY71jJFfqUtRiGGgbJdaq4RI3Cr5ItS
4Vj/cUj2kyEWcEVs8/qYXwXfpZkzeawkq6sPj6R94lYo4wLyc8oMhNmPPgikDizyq02tIIOydRyi
6DQRLbL4KTrSDGNSoQEYGFAGM9z+olFl2g6FNXzHqHcFN3J8uFePp3KBCfXzSbCxiNl8AO353V2r
IC3IihEg7P91BNDPW16ShhAagSLaWTBkXPDicmLs5jkRlEV8mXGjt9RYxHgFgy4xu8sRPmUNXDIc
Agm4sdvEvAGkPojKl02PKxQtcrGz4zfXmj6PgOu7ImV3rhVUGVxYjTdg49MCDoAGK+Mv1mWy5xNd
Q7i+JECJpsdeu0H1OF+8RBHFalU9Sm092ECGqyL1T2nIVEQChbXw+ZqHRZulJDLGyDm4cPAxmzzf
iGoyAwwMnoQK/Wek2/GX1kV78v9sr5FS6NU2rubfMMzmNo9UKVfwDw7BH7xk4oedAmLBPFRgbqaO
MX0oUkRL95YTdcJsU+t1636Midq+4YDbDH4LwCa6KqvbnHPKZ8N087ATFNLVVRkexvOyUBeuYYsS
hhx0PR7ymp2nPDl1eftqOv3k8LMlWLgSs6XSLSBsv+7NLuKfE19N74VQYrfhvI3121PKmcGWiSeL
e7wB2eMWbbI3dISC3gaOzSYT2IIiE10uGGn3gwN22fxUuk4A8o/jD99eiSsvac3aLUJc1hlgWE2x
88GjIaimv+cQb66qhLWzMf+FPAL2xSWH/3sxWVRlBNIWSHO6BtEw8xiqT913bj55nCI35ooW+uBf
Nb4s3OXcWEvca/Q2WYPd1EgKJ2WQL+uRNy/PjQN2Z8sBCv5I/IvHC/K7zjlxhax5fPAt5L8JJHMt
2uiT+PV+vtOenw/nohDqLe0NXuhei2VkklqwsTaNvfg8Pn78I9G4e04D40MZeVdydYFryNVEAFV4
G4z18o4aiZxwSZGT8YMYAhWIeQqNfc2ISMCF9ZhLJnKv8G7fHmGCLjIofI6HqcTtVrSuIY+sfN/j
Y6vr/YLbCmfRNnbQAlyxPRs15u79pnxkmfGh5PUJ5tSRSlwqNIj+g7kV7nxhM3hf+QnzAKXZ9Cni
ZFZZfWdY419WkvTku7VO/kM/DVHD2sAnMGkq0ZScMxX5dkIsXJkv/KKZcqUbpIk89Bfs28QSlbo1
B4Oz1so3qmzM9h4lrEnJnzuh5njF+f+s4uWIx9hwKvDbkXop+gT56/RaFZOVeu896lABwAMqWPY8
usTVSfvDuyL89xgPvrva94TGjDluYiz43LRZNpQPYE4F4GMJo2diUCegewcTpNj9w3q9lhIsEya/
TL8cN8EZA+4cQW9JD8jG7SE0Bs69z1cKhg1ZQB9V+OK6SJpWzgVZkkt5ICV7Ala9haapUPGE3i3I
FfCxV+LDYj2hfIW+K8tm5/jfKHTKgl6GY2bVy6fvv1dYjc0/xU57SdpRmvN1eLAmVhhX79ao0NHn
/nrtb7UmLPlspqTMLL6pGlLktbkSLcsirh9/jiEGsyN6FM/RXHUv3aS03H5IYenmtDnXXzwElzTv
NpdLJr6mQjjJ/eKdaHwxHPahJYPgLm4u4QXpX+/ubnNa/GrzsknakdMy7dQ0LHCQVXvZilNrBV6R
yoOCFwn8TGg1aoXtmUlqN3nT6Y4QVC16fAF+14/A/b10SC1QZIQ/zIlIW/HdAT+jwV5jtHwbtWk8
LgThDp9vN5uTFdsKLLt33vcUlWguQPMUrwF4fX03Cfw1WaGSJki1TpVIymj3h5UgnsuqNKMnndrV
Fu2jHV2+2mjHRxR+JtT81sT79OhKz5cFRpDsYnlZ8EivYy0HoRihwzTiu21FFdihCYUgg1xlfTJ9
V7RnPgVTyz3pJ0pgb4VRH+PtvJ52GTSN9qY0TjoNstUcUAADtimCWcIPoYXrgTHu9hyDu5E5pUcG
LVQSCwV+IGjYXmV6I5M2EOcPtVmxAdti+AXModHxIP6Z0bz6tL25K/KIq4MxRrq8tYVZ32TTTt1F
8E4jH2lWe8nxjeyAxYJiRtAaCajSACsUSBMfau9jYx/CaC/vN9l142bDKOwp1LIc1d4hzAdso0nM
/uBnu+etlfzG+kay3bq+CanaYGBCOJWgptbdPZGeIAnW0WomeUYl/jw2CPn40vVSlxcbk5u8OhqK
a85yQ1L2Y4Gly4DxySgFK8RW+EMb2t0YPW2ZTXS0mNEjnJeNVXEzI+EqlKhmJPKCB8ZsB5fExVOe
Rj7XWFG1OYXGf2BTeznjR8/EBMXCAUSEz3mzWyGkTZ6RiPMWPvvHxwH+q83nPVIL+5H0clu9Sl7u
ekoqpN+MT6kt+nlVxmq3oZuwUxF43bbbKKb3adWv5DGN5VgPUxWFDV9rI5B5xPob1Qe7r3VEbodj
E95Fh87MLB22zqPs4Y3TDVzMOMNUsgVmkwYyFBUj3SKM/DxZHnnzRCAJbkBw0EdKEjfZLRfUUyVv
OLRDyAECeWPjKD+Zw5lD4G6nar1XDZIl97AGCLrJJN3chhIyqRQvJqlHZo2eFQUlDaIOBTsZLLA8
6L5lkmgoyz8qv6QfcoYpS+8k4v2AT222ygz+fcWIll5TahgrvqeKZMsCNPBcNzOY2cMx452BkEJJ
tpFl26l/BSXJ9AYcYZhyzZHqpBga6gg14MWKoshQjBqC5HFw+qSDXlxzuOHMxV2FrF9xnAX5CC7K
70V1apvYuDP4l7ob00VqIi06z0MWVj8F5YiUeUWh5pDD4WvFVswHw/wK+HCZ16znFaRqFm43LN++
nTd2fkZHuTVcpJMoegMglOyOIctjALhF08rSHrNnimn0wzNxnbiXv3aZuXJqTmu9WfevVBV0AR/j
9gKFK2cRmezkE+gGmNG9ZwMiJSDYMR58PQ2PhlWEs4Z7PWwWkQIG8fH+/YdF7nny1LuY6PPWGLvB
GBsaXIjjNrBGHz97lHH6m68LMfXMD6xvCNFf2iDaPTZBJb17Rzdv+yeL8ed6fFFjWas9Eq+V70sw
Dwj2wWxqTjf1ckMzhfEijRQ8qjh9k+68JyC9RLeOAXl1ks4ybYpKNZaL+puRUYs0QYmaZMCB5JgU
rYhVDYgM69JGaNsdtGcQUkCyphAN6npj1vhE0qL0Z+Zdy2pbwRoM0OekenAZ3Gd6YUOdJ5eFDPQG
14uz+XI78xCznYzY3Vf7LaXXvcvua3bO/m4WHJm6Yc40Nw7kb/Af16pvFCUisY/46q1Hsd7zUtDr
I4NL8sFyX6X78PILZ17jOTPmLMN7+ujSuOp9yR6QbfldwCZK+7vsE+r3ParkLAcL6D4yn7HWBn4J
e1U8aTXqt17pvIKpbcoVvNz6B6j6w25GldsRNbaeqpFnSMvCRNZVgKSE2x0JG9ZJY6yEo+qDbYZV
B8ua25CdUifGRJksGJIYOsfuZU2v7VJfE+5/L9RhevqNeU8T31H3zQMHdqrM6toaOhvy/h+v6SBd
8G6tIHPL9NoOUXwRT3BhNx4PBgvUUTkPdLlIVth4mN9PTycowjYVQkRP5GQH7eztBDiObYEfY7ak
5IafYr00+SKdAOrJ62hW8UQLDY1rVpyW+h5QklYGIEQupA5D5zqdwcTi7dVgKix+PQIG5if7uNzx
zXd2CwVkUp85hw46fpNut9/UJncK8LSbUzsnYM6DVgrLqqge6BDM+oZbLC3Ucf28dwVyXcHLUcRE
TgWPhgrbH27Eril+KJ/ysArGBf4Knd2J/8h0JXR/qk9ZI88IwQRNZMKoO15izzMFkj3balt/WOnH
SC4JJ+9yTx+y+Ijy2FeSVnTBPWPHqWz0o+bnxYO52iu+wVLDcgYu3UehOTe4WrHlKxbAgSfxuVa1
nwuZI1PxBkjEt8rjOuumUec5+NXrjqS4sdHt109L7bk1WZD31bZcT87KQ64+hi3xly1NSu8lXbZh
g9LGr8QkkfwY0xt+gklc4n5UoEJPgccCb46GKt48V+eJ28Lay2vDXGUmsC51WAszfBLxSdtShT1u
LY7lZeReZHaHGi/QKEnmkpO2xdYoXc4pbVJ8sShBiLLSjWeMK5i2ACqyxLuzwHC17TZb0xYjdAGc
VHlh1YvIYaTWOeF/UdWXrCKY7PprdvNN/xmhbBuyFg0MOnihBd0hjAHOZvXevPeV2XHB5YbDtjog
Bv/K0Co3YDjKhP4AR6DlOBBCFUD+SLEqXnG+zOhEPOWSXwTN9tZ3BlH+2bHZONXYgHkCVs7ioEci
BqhDOwwWygoOqbW5TKF/dt9kai7MQUosytJ5+aURdPEfBd28tDV8SUZWht6w4xmu7oywXho3lugB
kPKX7w8Hl1/d8JXyUrw+esIkj5zhuFH33S8fWqP29jjb1glBy9S/k3IfTDbUxehl0TjLJjLkQmIo
32smsO2pP92VJdAaG87Zbv1moqWqFayYS6wXnAdeeAfTjCrWX9WWtvk6MnzknhjXG7h8NH+C30ad
7EmcCkW0Wft47y9rhw/DXaP2C2HpkWKvydyJWdmRg8fmIEsYbkH298tyWhcINMyCQJhvXMHg5h2k
1Tc9cypjkwNPRZSjIEUVmoxjsxTtFqL6Bx+U9MFT7aku09G+DTYoSokXSGLuoKR1aakSQXyGMJK/
IbBNd9rlhr5eOOnGZ7rAZmRmKK07R86RSYcSW+8KvhAoKGI03W8f6Bb0hMHgJa9z+Fo/ruhR/DgJ
h1Feyd95W6d30v2lI6YcDgi3mxXw3fHm5MmQGruNqY0yovRzhGpy9ypcvzwlnqIoPPXBB5fframe
yHTYw9+S4Vo4GRPzwSEmbQAli+WzKP6u0z/Bch0iIzBDsEk26m2LpGMkNDMsr9TJn74FMqFzmxMY
SG2yVWIvzc4gjANz8efv4dM+lBFlwBQJwGmTAaIETWGrI2BOYCfG+9+Vv1rO/dKs8s8QpvZwWw3P
2geGfW34GMu3J1RqWAhbvTJnLhH8gSJZ5GqL30bTtBl7+GGWdLLXkfEKYWh2ChuIT7Yo4jJnnkAg
qV5OITZCfSbx8A2B1Nz53nZQt49doImIMAMZeUWb052ZeAfrxlBl6jbsFGfhVsNMlp0qlrzHLuJV
Uhb8Whbrg6zLxvlfvWgLQoWu3NXdm8fDySt5J6RVqDNHvU3M4D+M9A7fUr7tvdHhljIprUhH4DFF
PuvKm38tbBB/pLF8t2BL91y27nq2jkq2Pk5j9o0/B1NoF0pd38M5WwIsvp7UYqrJL2ZGj2LoDnrb
zpTcEYONqqBk1UjDlgoFDeUZG9E7LeO7ObG4g0mu23IYEiKgR3U3S+lcAIEIkGNgJeLyVc537lvH
23Rv3YX3wUYNolC3tv4B+mFB7B0faiHG4EbWv1X5SeabUE1ZEBYI9jqRWQDxuobUkgn7oHj8Mlsa
xeMrdZOZaCbsH51t4AOsPOkutacuiCe3La/T4OJkA7q1hLLVRTvr8ExuJuq4t2zVBW6n/O6v+1TR
NZkGROUEwu8z0aGtdeJ4IxTFvNcWOAtp9p7BmNuUE52yZziAPxCZgQFfagWz29z/W2mgIW+qTjwI
CxXgF8UZkV9YbmgP82bXxtwmaBt0egcgnrC7FDKziKXOyHIr2VCBmkuIKXWJ+FZb5irdPRjMwXqL
hVNy4Mruk2jZoNsLncNAkzqebrv7sWU3a+cLjFX4hkcUwfxdg49LBJ6FhB59D2O1TfxFVTFpx9ei
Yx9F8KAcfiL2nnxmKy4JOh5cNBKano8fQCWRk+TtWpoCwIhLOnCywc2Znfg7GF/qp4MQZ4161auZ
LtT9jQP89lUotihX/uir6PyNKZJhZXLOXBpgo03duRK+brmRWlpxDGOkQX5aHuYoPRJVv9Cn+YB3
pL49Hfy7ZqYYDlnMdcH3AJuSZPm18bSDhvomStrOJaNVS1eB/N9unhMnxZFroEOYOYB2f3xMRlrm
yCvwFO2JlyrfLHLuuKpPBmwcaWq/AyNbEoCp/lVsWcpuFTaizGuQqyFrxfBVLBMIAnKk43LMJJei
YVyHUEmzs47Xm5VtZFY+ZKwXSIASNAywxJC0UyEbmzb9jmccxDGCo7GkeCjYIHYfrw0rbIGwuRBl
4LHoFiYeHsbBv5u+3/DAftrGlhEkLj34Fi/+xI/2aapnscEO2+YAIcfytxuGgH/KyowSsME9SvoS
8MMH8dgNg6byltiTqCmV821P4XQsSXoA2KP0W9ulCPAbibEOlYzxwOVzgwuCMBQlhCQCat9fyrry
nSu5aoQKaXdV1NzvrVqSn8P2SFkS6BLGdSeSTX+3gdimWe/R2O6QJIba4TvBChMEWphwPB4RnDxg
QGU8a0NQ5WDhlnVZTXrOuGajncNdsbyVHgnTr8zWm6GMo5Zgf5bTHD57YX2WnKGM0OrIZit1Vpyz
8B3KMUUcyumzw8mvogXuJi+uHr6+Ncx8PqnuGeynqg2F9aDTpDy2f/ljR6s2WLLFHfmr9NHbSXO2
gTKy/FwvoaDh2npPGOU/hapBrSyTo3/ybRC8JcLCcwYS4uFVlp6pIlQ/w3ksmofGmtBwO6WbynnX
Po4tWZrET+b8f0YWQjySyKtkyGVaRKNDU8xhkDSr0oD88TiHLQ6RkzGP2SAdpuQ5qnDLbbeOIy+5
J6C5IWnebLwNXI+qXUKpYKOwM5oSgpz9/fuDjyqSNJCembFO+TiuaAkV1CorvpYxnaZVzhTFVBHo
Ce3yUZNjnhA4YlAnOzR7+DRISTvbCUB/Ql2GZbUgFZ+/S70IEj+KN6g+6C+MS4k7cDm0Hub3fLaZ
y1q05RwzZxjRtaeznLRLUSo6C0B6mW29kI+IlCkD9uR1H8Ikb1Yen+oKD3W/iMZSco8GADJk8/an
L1sXODCO18FDDwDN+zm+iAuyiLpLxmCKj7StaUX3mL5HAbKWKl1XXJXVKbPvcXxp1JJ/7ZE4rdH0
ykUTnTx2feuXLUzGBxSwdRPzLML47KXExzyk/byJ2Zu5RDWsxoHtsc3X7kZR3izPk474e90XRzKE
BhJey4ETDu80tfF3SfgMBdXHqOMZmN3UN3JP23KbHA0trJnqfE4s0l4nMJ984fJfa7XR5C4k6x4y
MGjNZLVmPM8KzEg/1i1X3+8KD8iK6WDD/HpI0GCWAitWYDVI6nYajrZtJLbg9O+j4ElE1SSQSZhL
8wUc3ItZy1d7LDpo0AUeyMmZruXY+bx2JgFEnUWpxbxQ86G+jhnQLqRoUZShohaFu9AvMsVw8Ah5
2RQn6DZBDAVSVcq4mAg9xhFx0H4ddWHv2mePeohF/Gjp+RtWnajIrkLUvXRHBMRdamYXjOGF486P
Zf6h5Q2cfOWJ+qU3BDL6+g1Wz0GeTqIFsuCdWI2LK/bN1x8jrkQlYsYYwed6G1gPwTA5x0cnxlrQ
km3dJfhJhVbrG9OLQ9OLjKDVc1ixosZex289vpghkabVI0RBFL71SfGE/HC6Vne1sHAWf0RG2vlT
50w1g2q0yH8PEwc9cZh+rJzB6O/c6+AvyGlkefJIFLerbKwlRO6uaKtRRc12o8V9ASUtYjY8XwJO
tM/wrlgKauS9IzRkRCL27yhTLZbzpDIsIZuLEkiU/Meb405rXiYaU86rE0VJ/NRy3VcanwPj0Ztj
FY0fOLReMg/Ut/WS7kxdqmqS5ctqpQSFl0d5ppc3qVZOlaCPNQyO09Bsrs1fPZgJOBhPKWdyTmZE
O1+kNBKdLZKmN7STX2NBM48DiMOLtm96jZ2C3vGk+ChA/1VuB7rZtmHHpBtd/vtYaGRMiYsDVMlf
xgQ/iTMMBPIypNKjL72jmNdWFe6Y8QInwOvZTJe7ky9U+gS4MEhv+pxhleuuy5LSt0FL4iFHOPv5
Jb2r2jmH8E3KvSjfZPWh/oIIa3h16Q8qqngCu8As4ikSt80dM35dFggh5ICUR56nynsKHyFEbCAX
pOQe86aQWb/9a5hposacTHbeV1O2B0G4braJHGetv3MMbxujJ078HnM7QJcFUq65O9jVyr5wrtmS
3AliKMFYfZj8u7saF4/eTOVGS7pvmP/c1Xbrl0OGcAW1y2HkLH9qZf1acpQv4p9lv1U8xqenR9Ud
MR4nT83IoYicoRAXeRaAui8E+9hjnOgekoFddBQJeUZnH/R2EjmAyxBQ7R/NsDK9cfbmUM9Myz01
xgFGYRgVXUGepy/so7Ei/TFZgZzD9woxS/u1noyrGRfSzHS3BZdXXi1BHtSkXoIBa8igppQF5c9i
ak/bstz34pF9grfrrJf93tJbktc0XiBJy/LI8hrhxXoeq8jEYQGWup75VRAlt3YbKS3n+WpaXXTb
BRjIGRDc4C2cTasu5QUSxK/fYqfdZx7r0SN1sawZWLDl89Yu7yqEetKny8ECVRgqFuBJmkjTcVyN
e+0nwZu3Ia8eP2xkdgNTRdOtkBLSLTKxzxPerL37a+NUi2+nndbCM63pDnXJv6hP1Q9ByaGOW/1/
6TU45TmnxQ/Io7KjZmPx+c4/K2jmosJTAO2eY/6lo6M1fM2pYly06oV2+IJWumPrd30nrSKZKfvv
mwIC1WAGJLrMvBvSPc3VjXhf/DYPbX7c0IG8BuVd8Ntv4CC9kn2J7QdVgUoTHudXV8YdTUgKdqMk
gfklGENP8q8zR51MuPJzKpSuCu3H0J63G59gyzy0R3tNcp7OzySHfg/6tFtU6ZjS/NhyGtxk6DcQ
BemXisAG/qWy9eOWk6lUoh6Uq0A0ZnIwQr+6tIn3XjarVe++yl6Y3XCZzpVRbGXilIBIH8WugQn3
Q/2RZQWqBEitpnBh8RsNFQcH0oQ7m+NZPOC/cGa7gUq9KDNBDb6HWkHI7JcAZ/JqPUZEhk+k9J9z
qIcGjVFQgjEz/JrJ5qUcSct9M5jevZaECvWSt+u/sl2MqWKdiTxaU+e78uJXKXYU2v4xvU+mZk2h
QihBLUv3caF0jOX67CiyZJOio/1uknkRc/qRhsXQrlw9XGJcjH6JiMAsKDk8cZAkY5vJ9VTCimR3
GcDnHcquobUvh6CP/aDk+A81wwqm1dy7GcrTFaF3o431tGcT2HIDxPFnYg7h/zk1UxO9vCGnJfQT
lRrbWFgrzqAdUlnGES55Af9bOpBcwpoPLjEm3xE0/SdmT+uxTwMejlbOTrHBbZRU5NH69+wo4vAR
XTokfkDbRqLbD9Y/j47dHPwdfrtWBDEIKxCBXqKKN2JLXvyhageCi3TtOTsisu+WKB2FDnRW9xfI
GXrSBR6Ox/KuCJvpLVXupmtr/r1mmcZJbQ5LoyG3rkSQhmszKT74MxkuyRhFMQ4mb2S52hAjvrBQ
dgFdtCo9jew2tLHDchj0hUdhqJn8KkXxypCRhHL/+ToJDqtLz5g02ekX12K9S0rmhxWxxVKM8vhO
wDF46gf1PcRfK53HYLr60ksbTre2lb2qfWbDSXuz9gZBwP3kuwkpTU0v2JQcz8FYI28OoNSK/Gam
4adCATz2PvpLTtFCuw3N1VQUWJImZKip6BQD/FTAyl0c1zIiZG/8q3rAW9vAQbOfserxFthZGQ/E
ob9ubFA7JqaNCB9FPp2kME67zcQSvsjULbwmAS/PaBJWY1rLx8vAROVjlQvuPiS3zrAMopERbt0K
QU4T7SCN0+uexeTQ32aLrvcoyP/TMy5Y1rq0wV/XDRD/gKDKTk6myt4gbnJipKONgS8CciYs3HBU
NN59rNYEnvcJMbXubMQy/WQxFEE87cd5vxe42tpFtB6VPCtBprWps+YMEmBJSioDCuz5jz9EeJRs
MwCOCawUyu7rK25aMXCPaTD/Izv1LKDxufc/FawSdYOveQbUBxX4q6a0t5RVJ0fsqJzLmvFA1ayw
eYhd3m3es7QzMHA/nd2GJ9JdKxAEWnH/+Kag4yZgA1+HZDjNAwyzZ6LVIbDbnSU3sjVwvVEaZrSc
JYJO3guXgzQ1dy1LoVvlYW3adTfWLSjmsWufscq2c0xcSp6B9WlvOxNXVX+XD1yvAWDLwu9r6Vdg
PoDesW/gR4rgnW3hyaI9LpYPxGRUZKgirXWwwEQ9flVYRuFbBowpFqUyX1z0gszhgdLkoDZhcVna
GAFlDvRwo/acV2NvHcdRNSgUpfXVp7JPWyGgZieCROUJyZYl9eBvVVLoLgmxJLb+IKgdDC6TvI8M
SMRkkH+kN67P4TeDxcITztTUPKjjyMBbXuDvENMynEgOtAocC9H1KBDFYNdn/6i6dhLxtZD1RpdV
F1wDRG7q1JQENHKZ9XD01LeQuLLgsdf9GVHOLBYZT9yj1bg8jJCUabMhHtfchEXX6bVMn/CxZMRu
zspqSQe7B8tMnioOix3Bw1Ov9mmv5VFri98jDNEIrV0fyqu9aO578xVlDruVfeDDGLnfud0jQDaz
51Lp7tEEJaWL6fga3fjjagK/Kj35s3cfaSFB4Cc+KM6U03M5X6ghI0F3QJe7gOzL9nZuOP5QII+g
B3SnemO4RzP3X6eubUAjTtpVHxXi668WyzmAt4Uwr537PVzUFiRwuoGNUDQoEPRJaWIg4D7+OtGJ
2J0c2w4RtJ2Fbw8Bz7acrlsQIjGPiwGnypnZGh08mWP8xB++oxsHGOfyJXmBMoAudG25ujEpFEtS
G42RVM0ZpK/XCDq5kBSxFUYvwSXYNBEtYznQU0s1xn2nkk1gnieVOK1bVl1+mN8L2POkLTYlBBys
geJI9pNTd2r+p0cFHR64uAxrPVvD2PJpcfxeJetlwY5Yzui3E6AF+mRmlMMt6EHHr/dXpoK3L3iU
UlDRsT+FIr7JWps6YmR9GwMg63ryLXQVpI/jDMXV154ygdevS7etNJUz9jtdjtppnfv0tEi8Fj2v
/7pTZuQEgVdENh7WVbV7pXzvN7o2OVWJTUfRdtbxNoeKBSYFzP37r4Rgq40O9RgbYBeCM3DUApX8
aanV2wgClW6+m+ebXQ57aQISSRKBaT/+KgVErBhK0gAOu9UQQXSJxqW76daewHOV+lDyRF/vPRcg
LPcoxFG/O2H9o8tFqZl5lO9GZhSq0wfXyPtMBfQq1/sxJreUk6nU5JTbzarpzVw5Z93T4f7aMers
MsgD8vwoNlcv3+83WDLoQ2y1Qj2tJD10X79OJwv+VeUlTTYQB868gMIifHkdr/VY1O9WXyxVg7vk
f2iZyjHP7UY2wsEmhCvJrfszMSDp7M5v6LGEby5pP41yTFGVsBbs7ICEt2f6Sh2Xrjtx/EtzQ2gK
h1TIhTzxS2So+KQinJ1PyJQ8poBVxFefYaH7Kjmhr/CXqWd6crwzn9bJyaQXqVggsykr+eLyKUqX
wckI50LEyH4EYUFtJEw8fLQKyaMqpItlkXXosh798Vn/ZpH0CZ/OFXmGho1Tn9aNYTWPLpnGXf/r
GCMp00OsZoUd8/kCZH8xZElrVBdPCvWW5Tseycu7MsDnhu++Z7zHtaq+zFlg3O0cjhYmxEmMNK5Z
2sWE+dyTkQfyX8+ouZV74JrL++GxPnzLJs0aRO2g/X/dmTIbWs/OddDRvfB06hJeqzhO3EabushK
C2VkUpckjQUejlREsjRuz8rVH8qyS/WL4M3U45Sg/BELKHsxa6sEG7SI3mBjpt6V9AxtxSoPTx9w
jsYhjkV4GDknzvmu2LPb5ALGMmgOqkSanrFnwxjABIAiIfIp1L74U0l1zJUpY5zlvSYGkvvFZ1u9
Va7fVUC6UGmS8qtIfmy//s9WsZO43KuCvsfYfqBfVowAYt/8yd7r1pqaNsxVWNY27Ae5/A9hbUdL
oHWdLJlPHdVDGOWd3zT6+cvjGyxraLoQ/eayiW1WptB0AFDolwUdz/pVTjI968hYO5Kz2K6CdqVu
75oy4IoLvQeWhPD2Mtsn5G74vDciv3OmNB8INa/ZnoHVxPDPYfZhoemRYkxfsnq68fqfu/wR4v8m
3aSv0lfwprvkpmMjC2IP7MpCUcS1eNDhXns53ssTWEn23hFNU0xkWmG95hHWVtbgSXXfbhcL9vPZ
vAvo1y3A74jEoS2yMhqc800bHHJlhH8EsV3Klv2fH+pT+tQw6qz7r4w7UBFxIrlh5C6pmL0FEbjX
YQwf35aklcTFBFzhkz0hd9WHMJwtgG9ulTepIaUuU0W2qkwb26R4jocFUt6aFYNb2VWFCSmEU52l
ne44os1lDQFbdpGkzM4KEW7zjL12JSQhUrnY2JFfvwgyYT3G5gPaCHnLWDqgkVfdqpB8QlihRfGg
hNd0UTJ0EiME4BvY+HxcXSoJqPOQbM7AuX6+Tz7owVRTaBbGo4kvAswWGy6gcT77MRvOjDGIg8Wt
w/njGqCBMpPVfbbmKl3/jDoxqGW33EchYTBjPR1rNwgIEMKd292LH8bSmrvMJdkfVs5scpygDVIx
bqQ7WTcPX9NU2tE48GAgdAEC+a1JhyCaxxTxTcm8+IgeV0mIgimMHpAeWJz2B9f+zNu+6Ifvl++E
tEb62BTI8+0iMT3mRt+XfyUc810RChEpGAguT8FkK54C1kqT0lF3Wxm4oUXPBT0dxqzbTKCtYsSq
Os+wwwnmRvbHDExmqWCvrVEMpScXnldoVpZqNEfXdy7bsu/t8XkNtaQBJ/6hwdClNGqYANlLgh/d
puFgb1CRqeos0lnQJPsyRaJU0SHSFVYwIj+gRekWaUp2q3KlExxDGnJDhrxArZ4qPznXOBFanFb4
zOTKHLch/IY8VRkksqw7itns1oVwdoZFI5OnJf/FPOEJqiY/mlb4LTXfCO/CszN4HrTxnSswKf9M
GvpBowGzoWmXvWxAbit5WVPJRgPvf5oHlbbuYFJiDokpU9/LeMzo8XCcxI59JUuSB592X+nD9DWi
KXLFke+/vIwNrrKKnXdmP73jNTQPbA3iaX+NDb1G9or33oAVI/7qmwKmT3znMzVWVqwNzwHC4r5M
GcwPSJJhViz2VAGwhp6H4ZHL75TbmQn0QxlOk3bNI3jz/T57wUCk9Q6l4qgfFizXsxRUe7zXMjrW
cs3+PAp/9zJTuPXT9aEOunqm4gx7DEr+uqCgUiCPfHtw3MzI56dZL30wfdIIZ52+2n/9jZluHsyb
JZgVd/hCsxXx2d9VxQqqB2p9s0DlHOP08WbsxEhxsuVkHyJnmIggpF6rrd/0lkl4Id5Glw5b9gvZ
oVUaLJp1Z71U7SxckpxtACZGGNQdUqkY9/i/J59LlnsmhA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
