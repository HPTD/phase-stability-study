onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+PCS_PMA -L xpm -L gtwizard_ultrascale_v1_7_13 -L xil_defaultlib -L gig_ethernet_pcs_pma_v16_2_8 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.PCS_PMA xil_defaultlib.glbl

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure

do {PCS_PMA.udo}

run -all

endsim

quit -force
