// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Wed Aug  9 12:07:42 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/eorzes/cernbox/git/rx_kcu105/prj/m_top/m_top.gen/sources_1/ip/PCS_PMA/PCS_PMA_sim_netlist.v
// Design      : PCS_PMA
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku040-ffva1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module PCS_PMA
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire \<const1> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire NLW_U0_mmcm_locked_out_UNCONNECTED;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign mmcm_locked_out = \<const1> ;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  PCS_PMA_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(NLW_U0_mmcm_locked_out_UNCONNECTED),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
  VCC VCC
       (.P(\<const1> ));
endmodule

module PCS_PMA_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    pma_reset_out,
    signal_detect,
    userclk2,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  input pma_reset_out;
  input signal_detect;
  input userclk2;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [2:0]configuration_vector;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_out;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire pma_reset_out;
  wire powerdown;
  wire resetdone;
  wire resetdone_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire userclk2;
  wire NLW_PCS_PMA_core_an_enable_UNCONNECTED;
  wire NLW_PCS_PMA_core_an_interrupt_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_den_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_dwe_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_req_UNCONNECTED;
  wire NLW_PCS_PMA_core_en_cdet_UNCONNECTED;
  wire NLW_PCS_PMA_core_ewrap_UNCONNECTED;
  wire NLW_PCS_PMA_core_loc_ref_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_out_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_tri_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_PCS_PMA_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101010000" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "PCS_PMA" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  PCS_PMA_gig_ethernet_pcs_pma_v16_2_8 PCS_PMA_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_PCS_PMA_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_PCS_PMA_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(NLW_PCS_PMA_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_PCS_PMA_core_drp_den_UNCONNECTED),
        .drp_di(NLW_PCS_PMA_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_PCS_PMA_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_PCS_PMA_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_PCS_PMA_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_PCS_PMA_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_PCS_PMA_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_PCS_PMA_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_PCS_PMA_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(pma_reset_out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_PCS_PMA_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_PCS_PMA_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_PCS_PMA_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(userclk2));
  PCS_PMA_sync_block sync_block_reset_done
       (.data_in(resetdone_i),
        .resetdone(resetdone),
        .userclk2(userclk2));
  PCS_PMA_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispval),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(resetdone_i),
        .enablealign(enablealign),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .mgt_tx_reset(mgt_tx_reset),
        .pma_reset_out(pma_reset_out),
        .powerdown(powerdown),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk_out(rxoutclk_out),
        .rxp(rxp),
        .txbuferr(txbuferr),
        .txchardispmode_reg_reg_0(txchardispmode),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk_out(txoutclk_out),
        .txp(txp),
        .userclk2(userclk2));
endmodule

module PCS_PMA_clocking
   (gtrefclk_out,
    userclk2,
    userclk,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    rxoutclk,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output gtrefclk_out;
  output userclk2;
  output userclk;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input rxoutclk;
  output lopt;
  output lopt_1;
  input lopt_2;
  input lopt_3;
  input lopt_4;
  input lopt_5;

  wire \<const0> ;
  wire \<const1> ;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire \^lopt ;
  wire \^lopt_1 ;
  wire \^lopt_2 ;
  wire \^lopt_3 ;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;

  assign \^lopt  = lopt_2;
  assign \^lopt_1  = lopt_3;
  assign \^lopt_2  = lopt_4;
  assign \^lopt_3  = lopt_5;
  assign lopt = \<const1> ;
  assign lopt_1 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE3 #(
    .REFCLK_EN_TX_PATH(1'b0),
    .REFCLK_HROW_CK_SEL(2'b00),
    .REFCLK_ICNTL_RX(2'b00)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    rxrecclk_bufg_inst
       (.CE(\^lopt ),
        .CEMASK(1'b1),
        .CLR(\^lopt_1 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(rxoutclk),
        .O(rxuserclk2_out));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk2_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(txoutclk),
        .O(userclk2));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b1}),
        .I(txoutclk),
        .O(userclk));
endmodule

(* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
module PCS_PMA_gt
   (gtwiz_userclk_tx_active_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    cpllrefclksel_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drpwe_in,
    eyescanreset_in,
    eyescantrigger_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    gtrefclk1_in,
    loopback_in,
    pcsrsvdin_in,
    rx8b10ben_in,
    rxbufreset_in,
    rxcdrhold_in,
    rxcommadeten_in,
    rxdfelpmreset_in,
    rxlpmen_in,
    rxmcommaalignen_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxrate_in,
    rxusrclk_in,
    rxusrclk2_in,
    tx8b10ben_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdiffctrl_in,
    txelecidle_in,
    txinhibit_in,
    txpcsreset_in,
    txpd_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txusrclk_in,
    txusrclk2_in,
    cplllock_out,
    dmonitorout_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxclkcorcnt_out,
    rxcommadet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxresetdone_out,
    txbufstatus_out,
    txoutclk_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txresetdone_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [2:0]cpllrefclksel_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drpwe_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [2:0]loopback_in;
  input [15:0]pcsrsvdin_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcommadeten_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [2:0]rxrate_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]tx8b10ben_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txelecidle_in;
  input [0:0]txinhibit_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]cplllock_out;
  output [16:0]dmonitorout_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcommadet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxresetdone_out;
  output [1:0]txbufstatus_out;
  output [0:0]txoutclk_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txresetdone_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [2:0]NLW_inst_bufgtce_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtcemask_out_UNCONNECTED;
  wire [8:0]NLW_inst_bufgtdiv_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtreset_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtrstmask_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllfbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_cplllock_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllrefclklost_out_UNCONNECTED;
  wire [16:0]NLW_inst_dmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_dmonitoroutclk_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_common_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_common_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtrefclkmonitor_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxn_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxp_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierategen3_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierateidle_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllpd_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllreset_out_UNCONNECTED;
  wire [0:0]NLW_inst_pciesynctxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieusergen3rdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserphystatusrst_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserratestart_out_UNCONNECTED;
  wire [11:0]NLW_inst_pcsrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_phystatus_out_UNCONNECTED;
  wire [7:0]NLW_inst_pinrsrvdas_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout0_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout1_out_UNCONNECTED;
  wire [0:0]NLW_inst_powerpresent_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0refclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1refclklost_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor0_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor0_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_resetexception_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrlock_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrphdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanbondseq_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanrealign_out_UNCONNECTED;
  wire [4:0]NLW_inst_rxchbondo_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxckcaldone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcominitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcommadet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomsasdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomwakedet_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl3_out_UNCONNECTED;
  wire [127:0]NLW_inst_rxdata_out_UNCONNECTED;
  wire [7:0]NLW_inst_rxdataextendrsvd_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxdatavalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxelecidle_out_UNCONNECTED;
  wire [5:0]NLW_inst_rxheader_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxheadervalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpstresetdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED;
  wire [6:0]NLW_inst_rxmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobestarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphalignerr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbslocked_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxratedone_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk0_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk0sel_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk1_sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk1sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclkout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsliderdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipoutclkrdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslippmardy_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxstartofseq_out_UNCONNECTED;
  wire [2:0]NLW_inst_rxstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxvalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm0testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1finalout_out_UNCONNECTED;
  wire [0:0]NLW_inst_sdm1testdata_out_UNCONNECTED;
  wire [0:0]NLW_inst_tcongpo_out_UNCONNECTED;
  wire [0:0]NLW_inst_tconrsvdout0_out_UNCONNECTED;
  wire [0:0]NLW_inst_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_txcomfinish_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdccdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphinitdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_txratedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdaddr_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubden_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdi_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdwe_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubmdmtdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubtxuart_out_UNCONNECTED;

  assign cplllock_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000" *) 
  (* C_COMMON_SCALING_FACTOR = "1" *) 
  (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
  (* C_ENABLE_COMMON_USRCLK = "0" *) 
  (* C_FORCE_COMMONS = "0" *) 
  (* C_FREERUN_FREQUENCY = "62.500000" *) 
  (* C_GT_REV = "17" *) 
  (* C_GT_TYPE = "0" *) 
  (* C_INCLUDE_CPLL_CAL = "2" *) 
  (* C_LOCATE_COMMON = "0" *) 
  (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) 
  (* C_LOCATE_RESET_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_TX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) 
  (* C_PCIE_CORECLK_FREQ = "250" *) 
  (* C_PCIE_ENABLE = "0" *) 
  (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) 
  (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
  (* C_RX_BUFFBYPASS_MODE = "0" *) 
  (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_RX_BUFFER_MODE = "1" *) 
  (* C_RX_CB_DISP = "8'b00000000" *) 
  (* C_RX_CB_K = "8'b00000000" *) 
  (* C_RX_CB_LEN_SEQ = "1" *) 
  (* C_RX_CB_MAX_LEVEL = "1" *) 
  (* C_RX_CB_NUM_SEQ = "0" *) 
  (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_CC_DISP = "8'b00000000" *) 
  (* C_RX_CC_ENABLE = "1" *) 
  (* C_RX_CC_K = "8'b00010001" *) 
  (* C_RX_CC_LEN_SEQ = "2" *) 
  (* C_RX_CC_NUM_SEQ = "2" *) 
  (* C_RX_CC_PERIODICITY = "5000" *) 
  (* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) 
  (* C_RX_COMMA_M_ENABLE = "1" *) 
  (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
  (* C_RX_COMMA_P_ENABLE = "1" *) 
  (* C_RX_COMMA_P_VAL = "10'b0101111100" *) 
  (* C_RX_DATA_DECODING = "1" *) 
  (* C_RX_ENABLE = "1" *) 
  (* C_RX_INT_DATA_WIDTH = "20" *) 
  (* C_RX_LINE_RATE = "1.250000" *) 
  (* C_RX_MASTER_CHANNEL_IDX = "9" *) 
  (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) 
  (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_RX_OUTCLK_SOURCE = "1" *) 
  (* C_RX_PLL_TYPE = "2" *) 
  (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_RX_SLIDE_MODE = "0" *) 
  (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_RX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_RX_USER_DATA_WIDTH = "16" *) 
  (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_RX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_SECONDARY_QPLL_ENABLE = "0" *) 
  (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
  (* C_SIM_CPLL_CAL_BYPASS = "1" *) 
  (* C_TOTAL_NUM_CHANNELS = "1" *) 
  (* C_TOTAL_NUM_COMMONS = "0" *) 
  (* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) 
  (* C_TXPROGDIV_FREQ_ENABLE = "1" *) 
  (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
  (* C_TXPROGDIV_FREQ_VAL = "125.000000" *) 
  (* C_TX_BUFFBYPASS_MODE = "0" *) 
  (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_TX_BUFFER_MODE = "1" *) 
  (* C_TX_DATA_ENCODING = "1" *) 
  (* C_TX_ENABLE = "1" *) 
  (* C_TX_INT_DATA_WIDTH = "20" *) 
  (* C_TX_LINE_RATE = "1.250000" *) 
  (* C_TX_MASTER_CHANNEL_IDX = "9" *) 
  (* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) 
  (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_TX_OUTCLK_SOURCE = "4" *) 
  (* C_TX_PLL_TYPE = "2" *) 
  (* C_TX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_TX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_TX_USER_DATA_WIDTH = "16" *) 
  (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_TX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
  PCS_PMA_gt_gtwizard_top inst
       (.bgbypassb_in(1'b1),
        .bgmonitorenb_in(1'b1),
        .bgpdb_in(1'b1),
        .bgrcalovrd_in({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .bgrcalovrdenb_in(1'b1),
        .bufgtce_out(NLW_inst_bufgtce_out_UNCONNECTED[2:0]),
        .bufgtcemask_out(NLW_inst_bufgtcemask_out_UNCONNECTED[2:0]),
        .bufgtdiv_out(NLW_inst_bufgtdiv_out_UNCONNECTED[8:0]),
        .bufgtreset_out(NLW_inst_bufgtreset_out_UNCONNECTED[2:0]),
        .bufgtrstmask_out(NLW_inst_bufgtrstmask_out_UNCONNECTED[2:0]),
        .cdrstepdir_in(1'b0),
        .cdrstepsq_in(1'b0),
        .cdrstepsx_in(1'b0),
        .cfgreset_in(1'b0),
        .clkrsvd0_in(1'b0),
        .clkrsvd1_in(1'b0),
        .cpllfbclklost_out(NLW_inst_cpllfbclklost_out_UNCONNECTED[0]),
        .cpllfreqlock_in(1'b0),
        .cplllock_out(NLW_inst_cplllock_out_UNCONNECTED[0]),
        .cplllockdetclk_in(1'b0),
        .cplllocken_in(1'b1),
        .cpllpd_in(1'b0),
        .cpllrefclklost_out(NLW_inst_cpllrefclklost_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .cpllreset_in(1'b0),
        .dmonfiforeset_in(1'b0),
        .dmonitorclk_in(1'b0),
        .dmonitorout_out(NLW_inst_dmonitorout_out_UNCONNECTED[16:0]),
        .dmonitoroutclk_out(NLW_inst_dmonitoroutclk_out_UNCONNECTED[0]),
        .drpaddr_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_common_in(1'b0),
        .drpclk_in(drpclk_in),
        .drpdi_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_common_out(NLW_inst_drpdo_common_out_UNCONNECTED[15:0]),
        .drpdo_out(NLW_inst_drpdo_out_UNCONNECTED[15:0]),
        .drpen_common_in(1'b0),
        .drpen_in(1'b0),
        .drprdy_common_out(NLW_inst_drprdy_common_out_UNCONNECTED[0]),
        .drprdy_out(NLW_inst_drprdy_out_UNCONNECTED[0]),
        .drprst_in(1'b0),
        .drpwe_common_in(1'b0),
        .drpwe_in(1'b0),
        .elpcaldvorwren_in(1'b0),
        .elpcalpaorwren_in(1'b0),
        .evoddphicaldone_in(1'b0),
        .evoddphicalstart_in(1'b0),
        .evoddphidrden_in(1'b0),
        .evoddphidwren_in(1'b0),
        .evoddphixrden_in(1'b0),
        .evoddphixwren_in(1'b0),
        .eyescandataerror_out(NLW_inst_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanmode_in(1'b0),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .freqos_in(1'b0),
        .gtgrefclk0_in(1'b0),
        .gtgrefclk1_in(1'b0),
        .gtgrefclk_in(1'b0),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtnorthrefclk00_in(1'b0),
        .gtnorthrefclk01_in(1'b0),
        .gtnorthrefclk0_in(1'b0),
        .gtnorthrefclk10_in(1'b0),
        .gtnorthrefclk11_in(1'b0),
        .gtnorthrefclk1_in(1'b0),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk00_in(1'b0),
        .gtrefclk01_in(1'b0),
        .gtrefclk0_in(gtrefclk0_in),
        .gtrefclk10_in(1'b0),
        .gtrefclk11_in(1'b0),
        .gtrefclk1_in(1'b0),
        .gtrefclkmonitor_out(NLW_inst_gtrefclkmonitor_out_UNCONNECTED[0]),
        .gtresetsel_in(1'b0),
        .gtrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtrxreset_in(1'b0),
        .gtrxresetsel_in(1'b0),
        .gtsouthrefclk00_in(1'b0),
        .gtsouthrefclk01_in(1'b0),
        .gtsouthrefclk0_in(1'b0),
        .gtsouthrefclk10_in(1'b0),
        .gtsouthrefclk11_in(1'b0),
        .gtsouthrefclk1_in(1'b0),
        .gttxreset_in(1'b0),
        .gttxresetsel_in(1'b0),
        .gtwiz_buffbypass_rx_done_out(NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_error_out(NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_reset_in(1'b0),
        .gtwiz_buffbypass_rx_start_user_in(1'b0),
        .gtwiz_buffbypass_tx_done_out(NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_error_out(NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_reset_in(1'b0),
        .gtwiz_buffbypass_tx_start_user_in(1'b0),
        .gtwiz_gthe3_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe3_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe3_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gtye4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_qpll0lock_in(1'b0),
        .gtwiz_reset_qpll0reset_out(NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED[0]),
        .gtwiz_reset_qpll1lock_in(1'b0),
        .gtwiz_reset_qpll1reset_out(NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_cdr_stable_out(NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_in(1'b0),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_in(1'b0),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_rx_active_out(NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_reset_in(1'b0),
        .gtwiz_userclk_rx_srcclk_out(NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk2_out(NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk_out(NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userclk_tx_active_out(NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_reset_in(1'b0),
        .gtwiz_userclk_tx_srcclk_out(NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk2_out(NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk_out(NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .gtyrxn_in(1'b0),
        .gtyrxp_in(1'b0),
        .gtytxn_out(NLW_inst_gtytxn_out_UNCONNECTED[0]),
        .gtytxp_out(NLW_inst_gtytxp_out_UNCONNECTED[0]),
        .incpctrl_in(1'b0),
        .loopback_in({1'b0,1'b0,1'b0}),
        .looprsvd_in(1'b0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .lpbkrxtxseren_in(1'b0),
        .lpbktxrxseren_in(1'b0),
        .pcieeqrxeqadaptdone_in(1'b0),
        .pcierategen3_out(NLW_inst_pcierategen3_out_UNCONNECTED[0]),
        .pcierateidle_out(NLW_inst_pcierateidle_out_UNCONNECTED[0]),
        .pcierateqpll0_in(1'b0),
        .pcierateqpll1_in(1'b0),
        .pcierateqpllpd_out(NLW_inst_pcierateqpllpd_out_UNCONNECTED[1:0]),
        .pcierateqpllreset_out(NLW_inst_pcierateqpllreset_out_UNCONNECTED[1:0]),
        .pcierstidle_in(1'b0),
        .pciersttxsyncstart_in(1'b0),
        .pciesynctxsyncdone_out(NLW_inst_pciesynctxsyncdone_out_UNCONNECTED[0]),
        .pcieusergen3rdy_out(NLW_inst_pcieusergen3rdy_out_UNCONNECTED[0]),
        .pcieuserphystatusrst_out(NLW_inst_pcieuserphystatusrst_out_UNCONNECTED[0]),
        .pcieuserratedone_in(1'b0),
        .pcieuserratestart_out(NLW_inst_pcieuserratestart_out_UNCONNECTED[0]),
        .pcsrsvdin2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdout_out(NLW_inst_pcsrsvdout_out_UNCONNECTED[11:0]),
        .phystatus_out(NLW_inst_phystatus_out_UNCONNECTED[0]),
        .pinrsrvdas_out(NLW_inst_pinrsrvdas_out_UNCONNECTED[7:0]),
        .pmarsvd0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdout0_out(NLW_inst_pmarsvdout0_out_UNCONNECTED[7:0]),
        .pmarsvdout1_out(NLW_inst_pmarsvdout1_out_UNCONNECTED[7:0]),
        .powerpresent_out(NLW_inst_powerpresent_out_UNCONNECTED[0]),
        .qpll0clk_in(1'b0),
        .qpll0clkrsvd0_in(1'b0),
        .qpll0clkrsvd1_in(1'b0),
        .qpll0fbclklost_out(NLW_inst_qpll0fbclklost_out_UNCONNECTED[0]),
        .qpll0fbdiv_in(1'b0),
        .qpll0freqlock_in(1'b0),
        .qpll0lock_out(NLW_inst_qpll0lock_out_UNCONNECTED[0]),
        .qpll0lockdetclk_in(1'b0),
        .qpll0locken_in(1'b0),
        .qpll0outclk_out(NLW_inst_qpll0outclk_out_UNCONNECTED[0]),
        .qpll0outrefclk_out(NLW_inst_qpll0outrefclk_out_UNCONNECTED[0]),
        .qpll0pd_in(1'b1),
        .qpll0refclk_in(1'b0),
        .qpll0refclklost_out(NLW_inst_qpll0refclklost_out_UNCONNECTED[0]),
        .qpll0refclksel_in({1'b0,1'b0,1'b1}),
        .qpll0reset_in(1'b1),
        .qpll1clk_in(1'b0),
        .qpll1clkrsvd0_in(1'b0),
        .qpll1clkrsvd1_in(1'b0),
        .qpll1fbclklost_out(NLW_inst_qpll1fbclklost_out_UNCONNECTED[0]),
        .qpll1fbdiv_in(1'b0),
        .qpll1freqlock_in(1'b0),
        .qpll1lock_out(NLW_inst_qpll1lock_out_UNCONNECTED[0]),
        .qpll1lockdetclk_in(1'b0),
        .qpll1locken_in(1'b0),
        .qpll1outclk_out(NLW_inst_qpll1outclk_out_UNCONNECTED[0]),
        .qpll1outrefclk_out(NLW_inst_qpll1outrefclk_out_UNCONNECTED[0]),
        .qpll1pd_in(1'b1),
        .qpll1refclk_in(1'b0),
        .qpll1refclklost_out(NLW_inst_qpll1refclklost_out_UNCONNECTED[0]),
        .qpll1refclksel_in({1'b0,1'b0,1'b1}),
        .qpll1reset_in(1'b1),
        .qplldmonitor0_out(NLW_inst_qplldmonitor0_out_UNCONNECTED[7:0]),
        .qplldmonitor1_out(NLW_inst_qplldmonitor1_out_UNCONNECTED[7:0]),
        .qpllrsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd3_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd4_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rcalenb_in(1'b1),
        .refclkoutmonitor0_out(NLW_inst_refclkoutmonitor0_out_UNCONNECTED[0]),
        .refclkoutmonitor1_out(NLW_inst_refclkoutmonitor1_out_UNCONNECTED[0]),
        .resetexception_out(NLW_inst_resetexception_out_UNCONNECTED[0]),
        .resetovrd_in(1'b0),
        .rstclkentx_in(1'b0),
        .rx8b10ben_in(1'b1),
        .rxafecfoken_in(1'b0),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({\^rxbufstatus_out ,NLW_inst_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_inst_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_inst_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrfreqreset_in(1'b0),
        .rxcdrhold_in(1'b0),
        .rxcdrlock_out(NLW_inst_rxcdrlock_out_UNCONNECTED[0]),
        .rxcdrovrden_in(1'b0),
        .rxcdrphdone_out(NLW_inst_rxcdrphdone_out_UNCONNECTED[0]),
        .rxcdrreset_in(1'b0),
        .rxcdrresetrsv_in(1'b0),
        .rxchanbondseq_out(NLW_inst_rxchanbondseq_out_UNCONNECTED[0]),
        .rxchanisaligned_out(NLW_inst_rxchanisaligned_out_UNCONNECTED[0]),
        .rxchanrealign_out(NLW_inst_rxchanrealign_out_UNCONNECTED[0]),
        .rxchbonden_in(1'b0),
        .rxchbondi_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rxchbondlevel_in({1'b0,1'b0,1'b0}),
        .rxchbondmaster_in(1'b0),
        .rxchbondo_out(NLW_inst_rxchbondo_out_UNCONNECTED[4:0]),
        .rxchbondslave_in(1'b0),
        .rxckcaldone_out(NLW_inst_rxckcaldone_out_UNCONNECTED[0]),
        .rxckcalreset_in(1'b0),
        .rxckcalstart_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxcominitdet_out(NLW_inst_rxcominitdet_out_UNCONNECTED[0]),
        .rxcommadet_out(NLW_inst_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxcomsasdet_out(NLW_inst_rxcomsasdet_out_UNCONNECTED[0]),
        .rxcomwakedet_out(NLW_inst_rxcomwakedet_out_UNCONNECTED[0]),
        .rxctrl0_out({NLW_inst_rxctrl0_out_UNCONNECTED[15:2],\^rxctrl0_out }),
        .rxctrl1_out({NLW_inst_rxctrl1_out_UNCONNECTED[15:2],\^rxctrl1_out }),
        .rxctrl2_out({NLW_inst_rxctrl2_out_UNCONNECTED[7:2],\^rxctrl2_out }),
        .rxctrl3_out({NLW_inst_rxctrl3_out_UNCONNECTED[7:2],\^rxctrl3_out }),
        .rxdata_out(NLW_inst_rxdata_out_UNCONNECTED[127:0]),
        .rxdataextendrsvd_out(NLW_inst_rxdataextendrsvd_out_UNCONNECTED[7:0]),
        .rxdatavalid_out(NLW_inst_rxdatavalid_out_UNCONNECTED[1:0]),
        .rxdccforcestart_in(1'b0),
        .rxdfeagcctrl_in({1'b0,1'b1}),
        .rxdfeagchold_in(1'b0),
        .rxdfeagcovrden_in(1'b0),
        .rxdfecfokfcnum_in(1'b0),
        .rxdfecfokfen_in(1'b0),
        .rxdfecfokfpulse_in(1'b0),
        .rxdfecfokhold_in(1'b0),
        .rxdfecfokovren_in(1'b0),
        .rxdfekhhold_in(1'b0),
        .rxdfekhovrden_in(1'b0),
        .rxdfelfhold_in(1'b0),
        .rxdfelfovrden_in(1'b0),
        .rxdfelpmreset_in(1'b0),
        .rxdfetap10hold_in(1'b0),
        .rxdfetap10ovrden_in(1'b0),
        .rxdfetap11hold_in(1'b0),
        .rxdfetap11ovrden_in(1'b0),
        .rxdfetap12hold_in(1'b0),
        .rxdfetap12ovrden_in(1'b0),
        .rxdfetap13hold_in(1'b0),
        .rxdfetap13ovrden_in(1'b0),
        .rxdfetap14hold_in(1'b0),
        .rxdfetap14ovrden_in(1'b0),
        .rxdfetap15hold_in(1'b0),
        .rxdfetap15ovrden_in(1'b0),
        .rxdfetap2hold_in(1'b0),
        .rxdfetap2ovrden_in(1'b0),
        .rxdfetap3hold_in(1'b0),
        .rxdfetap3ovrden_in(1'b0),
        .rxdfetap4hold_in(1'b0),
        .rxdfetap4ovrden_in(1'b0),
        .rxdfetap5hold_in(1'b0),
        .rxdfetap5ovrden_in(1'b0),
        .rxdfetap6hold_in(1'b0),
        .rxdfetap6ovrden_in(1'b0),
        .rxdfetap7hold_in(1'b0),
        .rxdfetap7ovrden_in(1'b0),
        .rxdfetap8hold_in(1'b0),
        .rxdfetap8ovrden_in(1'b0),
        .rxdfetap9hold_in(1'b0),
        .rxdfetap9ovrden_in(1'b0),
        .rxdfeuthold_in(1'b0),
        .rxdfeutovrden_in(1'b0),
        .rxdfevphold_in(1'b0),
        .rxdfevpovrden_in(1'b0),
        .rxdfevsen_in(1'b0),
        .rxdfexyden_in(1'b1),
        .rxdlybypass_in(1'b1),
        .rxdlyen_in(1'b0),
        .rxdlyovrden_in(1'b0),
        .rxdlysreset_in(1'b0),
        .rxdlysresetdone_out(NLW_inst_rxdlysresetdone_out_UNCONNECTED[0]),
        .rxelecidle_out(NLW_inst_rxelecidle_out_UNCONNECTED[0]),
        .rxelecidlemode_in({1'b1,1'b1}),
        .rxeqtraining_in(1'b0),
        .rxgearboxslip_in(1'b0),
        .rxheader_out(NLW_inst_rxheader_out_UNCONNECTED[5:0]),
        .rxheadervalid_out(NLW_inst_rxheadervalid_out_UNCONNECTED[1:0]),
        .rxlatclk_in(1'b0),
        .rxlfpstresetdet_out(NLW_inst_rxlfpstresetdet_out_UNCONNECTED[0]),
        .rxlfpsu2lpexitdet_out(NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED[0]),
        .rxlfpsu3wakedet_out(NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED[0]),
        .rxlpmen_in(1'b1),
        .rxlpmgchold_in(1'b0),
        .rxlpmgcovrden_in(1'b0),
        .rxlpmhfhold_in(1'b0),
        .rxlpmhfovrden_in(1'b0),
        .rxlpmlfhold_in(1'b0),
        .rxlpmlfklovrden_in(1'b0),
        .rxlpmoshold_in(1'b0),
        .rxlpmosovrden_in(1'b0),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxmonitorout_out(NLW_inst_rxmonitorout_out_UNCONNECTED[6:0]),
        .rxmonitorsel_in({1'b0,1'b0}),
        .rxoobreset_in(1'b0),
        .rxoscalreset_in(1'b0),
        .rxoshold_in(1'b0),
        .rxosintcfg_in({1'b1,1'b1,1'b0,1'b1}),
        .rxosintdone_out(NLW_inst_rxosintdone_out_UNCONNECTED[0]),
        .rxosinten_in(1'b1),
        .rxosinthold_in(1'b0),
        .rxosintovrden_in(1'b0),
        .rxosintstarted_out(NLW_inst_rxosintstarted_out_UNCONNECTED[0]),
        .rxosintstrobe_in(1'b0),
        .rxosintstrobedone_out(NLW_inst_rxosintstrobedone_out_UNCONNECTED[0]),
        .rxosintstrobestarted_out(NLW_inst_rxosintstrobestarted_out_UNCONNECTED[0]),
        .rxosinttestovrden_in(1'b0),
        .rxosovrden_in(1'b0),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkfabric_out(NLW_inst_rxoutclkfabric_out_UNCONNECTED[0]),
        .rxoutclkpcs_out(NLW_inst_rxoutclkpcs_out_UNCONNECTED[0]),
        .rxoutclksel_in({1'b0,1'b1,1'b0}),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpd_in[1],1'b0}),
        .rxphalign_in(1'b0),
        .rxphaligndone_out(NLW_inst_rxphaligndone_out_UNCONNECTED[0]),
        .rxphalignen_in(1'b0),
        .rxphalignerr_out(NLW_inst_rxphalignerr_out_UNCONNECTED[0]),
        .rxphdlypd_in(1'b1),
        .rxphdlyreset_in(1'b0),
        .rxphovrden_in(1'b0),
        .rxpllclksel_in({1'b0,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_inst_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_inst_rxprbserr_out_UNCONNECTED[0]),
        .rxprbslocked_out(NLW_inst_rxprbslocked_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxprgdivresetdone_out(NLW_inst_rxprgdivresetdone_out_UNCONNECTED[0]),
        .rxprogdivreset_in(1'b0),
        .rxqpien_in(1'b0),
        .rxqpisenn_out(NLW_inst_rxqpisenn_out_UNCONNECTED[0]),
        .rxqpisenp_out(NLW_inst_rxqpisenp_out_UNCONNECTED[0]),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxratedone_out(NLW_inst_rxratedone_out_UNCONNECTED[0]),
        .rxratemode_in(1'b0),
        .rxrecclk0_sel_out(NLW_inst_rxrecclk0_sel_out_UNCONNECTED[1:0]),
        .rxrecclk0sel_out(NLW_inst_rxrecclk0sel_out_UNCONNECTED[0]),
        .rxrecclk1_sel_out(NLW_inst_rxrecclk1_sel_out_UNCONNECTED[1:0]),
        .rxrecclk1sel_out(NLW_inst_rxrecclk1sel_out_UNCONNECTED[0]),
        .rxrecclkout_out(NLW_inst_rxrecclkout_out_UNCONNECTED[0]),
        .rxresetdone_out(NLW_inst_rxresetdone_out_UNCONNECTED[0]),
        .rxslide_in(1'b0),
        .rxsliderdy_out(NLW_inst_rxsliderdy_out_UNCONNECTED[0]),
        .rxslipdone_out(NLW_inst_rxslipdone_out_UNCONNECTED[0]),
        .rxslipoutclk_in(1'b0),
        .rxslipoutclkrdy_out(NLW_inst_rxslipoutclkrdy_out_UNCONNECTED[0]),
        .rxslippma_in(1'b0),
        .rxslippmardy_out(NLW_inst_rxslippmardy_out_UNCONNECTED[0]),
        .rxstartofseq_out(NLW_inst_rxstartofseq_out_UNCONNECTED[1:0]),
        .rxstatus_out(NLW_inst_rxstatus_out_UNCONNECTED[2:0]),
        .rxsyncallin_in(1'b0),
        .rxsyncdone_out(NLW_inst_rxsyncdone_out_UNCONNECTED[0]),
        .rxsyncin_in(1'b0),
        .rxsyncmode_in(1'b0),
        .rxsyncout_out(NLW_inst_rxsyncout_out_UNCONNECTED[0]),
        .rxsysclksel_in({1'b0,1'b0}),
        .rxtermination_in(1'b0),
        .rxuserrdy_in(1'b1),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(rxusrclk_in),
        .rxvalid_out(NLW_inst_rxvalid_out_UNCONNECTED[0]),
        .sdm0data_in(1'b0),
        .sdm0finalout_out(NLW_inst_sdm0finalout_out_UNCONNECTED[0]),
        .sdm0reset_in(1'b0),
        .sdm0testdata_out(NLW_inst_sdm0testdata_out_UNCONNECTED[0]),
        .sdm0toggle_in(1'b0),
        .sdm0width_in(1'b0),
        .sdm1data_in(1'b0),
        .sdm1finalout_out(NLW_inst_sdm1finalout_out_UNCONNECTED[0]),
        .sdm1reset_in(1'b0),
        .sdm1testdata_out(NLW_inst_sdm1testdata_out_UNCONNECTED[0]),
        .sdm1toggle_in(1'b0),
        .sdm1width_in(1'b0),
        .sigvalidclk_in(1'b0),
        .tcongpi_in(1'b0),
        .tcongpo_out(NLW_inst_tcongpo_out_UNCONNECTED[0]),
        .tconpowerup_in(1'b0),
        .tconreset_in(1'b0),
        .tconrsvdin1_in(1'b0),
        .tconrsvdout0_out(NLW_inst_tconrsvdout0_out_UNCONNECTED[0]),
        .tstin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10bbypass_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10ben_in(1'b1),
        .txbufdiffctrl_in({1'b0,1'b0,1'b0}),
        .txbufstatus_out({\^txbufstatus_out ,NLW_inst_txbufstatus_out_UNCONNECTED[0]}),
        .txcomfinish_out(NLW_inst_txcomfinish_out_UNCONNECTED[0]),
        .txcominit_in(1'b0),
        .txcomsas_in(1'b0),
        .txcomwake_in(1'b0),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in[1:0]}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in[1:0]}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in[1:0]}),
        .txdata_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdataextendrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdccdone_out(NLW_inst_txdccdone_out_UNCONNECTED[0]),
        .txdccforcestart_in(1'b0),
        .txdccreset_in(1'b0),
        .txdeemph_in(1'b0),
        .txdetectrx_in(1'b0),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txdiffpd_in(1'b0),
        .txdlybypass_in(1'b1),
        .txdlyen_in(1'b0),
        .txdlyhold_in(1'b0),
        .txdlyovrden_in(1'b0),
        .txdlysreset_in(1'b0),
        .txdlysresetdone_out(NLW_inst_txdlysresetdone_out_UNCONNECTED[0]),
        .txdlyupdown_in(1'b0),
        .txelecidle_in(txelecidle_in),
        .txelforcestart_in(1'b0),
        .txheader_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txinhibit_in(1'b0),
        .txlatclk_in(1'b0),
        .txlfpstreset_in(1'b0),
        .txlfpsu2lpexit_in(1'b0),
        .txlfpsu3wake_in(1'b0),
        .txmaincursor_in({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txmargin_in({1'b0,1'b0,1'b0}),
        .txmuxdcdexhold_in(1'b0),
        .txmuxdcdorwren_in(1'b0),
        .txoneszeros_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txoutclkfabric_out(NLW_inst_txoutclkfabric_out_UNCONNECTED[0]),
        .txoutclkpcs_out(NLW_inst_txoutclkpcs_out_UNCONNECTED[0]),
        .txoutclksel_in({1'b1,1'b0,1'b1}),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpdelecidlemode_in(1'b0),
        .txphalign_in(1'b0),
        .txphaligndone_out(NLW_inst_txphaligndone_out_UNCONNECTED[0]),
        .txphalignen_in(1'b0),
        .txphdlypd_in(1'b1),
        .txphdlyreset_in(1'b0),
        .txphdlytstclk_in(1'b0),
        .txphinit_in(1'b0),
        .txphinitdone_out(NLW_inst_txphinitdone_out_UNCONNECTED[0]),
        .txphovrden_in(1'b0),
        .txpippmen_in(1'b0),
        .txpippmovrden_in(1'b0),
        .txpippmpd_in(1'b0),
        .txpippmsel_in(1'b0),
        .txpippmstepsize_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpisopd_in(1'b0),
        .txpllclksel_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_inst_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpostcursorinv_in(1'b0),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprecursorinv_in(1'b0),
        .txprgdivresetdone_out(NLW_inst_txprgdivresetdone_out_UNCONNECTED[0]),
        .txprogdivreset_in(1'b0),
        .txqpibiasen_in(1'b0),
        .txqpisenn_out(NLW_inst_txqpisenn_out_UNCONNECTED[0]),
        .txqpisenp_out(NLW_inst_txqpisenp_out_UNCONNECTED[0]),
        .txqpistrongpdown_in(1'b0),
        .txqpiweakpup_in(1'b0),
        .txrate_in({1'b0,1'b0,1'b0}),
        .txratedone_out(NLW_inst_txratedone_out_UNCONNECTED[0]),
        .txratemode_in(1'b0),
        .txresetdone_out(NLW_inst_txresetdone_out_UNCONNECTED[0]),
        .txsequence_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txswing_in(1'b0),
        .txsyncallin_in(1'b0),
        .txsyncdone_out(NLW_inst_txsyncdone_out_UNCONNECTED[0]),
        .txsyncin_in(1'b0),
        .txsyncmode_in(1'b0),
        .txsyncout_out(NLW_inst_txsyncout_out_UNCONNECTED[0]),
        .txsysclksel_in({1'b0,1'b0}),
        .txuserrdy_in(1'b1),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0),
        .ubcfgstreamen_in(1'b0),
        .ubdaddr_out(NLW_inst_ubdaddr_out_UNCONNECTED[0]),
        .ubden_out(NLW_inst_ubden_out_UNCONNECTED[0]),
        .ubdi_out(NLW_inst_ubdi_out_UNCONNECTED[0]),
        .ubdo_in(1'b0),
        .ubdrdy_in(1'b0),
        .ubdwe_out(NLW_inst_ubdwe_out_UNCONNECTED[0]),
        .ubenable_in(1'b0),
        .ubgpi_in(1'b0),
        .ubintr_in(1'b0),
        .ubiolmbrst_in(1'b0),
        .ubmbrst_in(1'b0),
        .ubmdmcapture_in(1'b0),
        .ubmdmdbgrst_in(1'b0),
        .ubmdmdbgupdate_in(1'b0),
        .ubmdmregen_in(1'b0),
        .ubmdmshift_in(1'b0),
        .ubmdmsysrst_in(1'b0),
        .ubmdmtck_in(1'b0),
        .ubmdmtdi_in(1'b0),
        .ubmdmtdo_out(NLW_inst_ubmdmtdo_out_UNCONNECTED[0]),
        .ubrsvdout_out(NLW_inst_ubrsvdout_out_UNCONNECTED[0]),
        .ubtxuart_out(NLW_inst_ubtxuart_out_UNCONNECTED[0]));
endmodule

module PCS_PMA_gt_gthe3_channel_wrapper
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;

  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe3_channel channel_inst
       (.cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(rxcdrlock_out),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .rxresetdone_out(rxresetdone_out),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(txresetdone_out));
endmodule

module PCS_PMA_gt_gtwizard_gthe3
   (gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxoutclk_out,
    txoutclk_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    rxmcommaalignen_in,
    rxusrclk_in,
    txelecidle_in,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_datapath_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ;
  wire \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  PCS_PMA_gt_gthe3_channel_wrapper \gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rst_in0(rst_in0),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_rxresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .rxresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_7 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0 \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_txresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .txresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_9 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gtwiz_reset \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst 
       (.cplllock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_0 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.cpllpd_ch_int (\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gtpowergood_out(gtpowergood_out),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in0(rst_in0),
        .rxcdrlock_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_4 ),
        .rxpmaresetdone_out(\gen_gtwizard_gthe3.gen_channel_container[2].gen_enabled_channel.gthe3_channel_wrapper_inst_n_6 ),
        .rxusrclk_in(rxusrclk_in));
endmodule

(* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000" *) (* C_COMMON_SCALING_FACTOR = "1" *) (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
(* C_ENABLE_COMMON_USRCLK = "0" *) (* C_FORCE_COMMONS = "0" *) (* C_FREERUN_FREQUENCY = "62.500000" *) 
(* C_GT_REV = "17" *) (* C_GT_TYPE = "0" *) (* C_INCLUDE_CPLL_CAL = "2" *) 
(* C_LOCATE_COMMON = "0" *) (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) (* C_LOCATE_RESET_CONTROLLER = "0" *) 
(* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) (* C_LOCATE_RX_USER_CLOCKING = "1" *) (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
(* C_LOCATE_TX_USER_CLOCKING = "1" *) (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) (* C_PCIE_CORECLK_FREQ = "250" *) 
(* C_PCIE_ENABLE = "0" *) (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
(* C_RX_BUFFBYPASS_MODE = "0" *) (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) (* C_RX_BUFFER_MODE = "1" *) 
(* C_RX_CB_DISP = "8'b00000000" *) (* C_RX_CB_K = "8'b00000000" *) (* C_RX_CB_LEN_SEQ = "1" *) 
(* C_RX_CB_MAX_LEVEL = "1" *) (* C_RX_CB_NUM_SEQ = "0" *) (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_CC_DISP = "8'b00000000" *) (* C_RX_CC_ENABLE = "1" *) (* C_RX_CC_K = "8'b00010001" *) 
(* C_RX_CC_LEN_SEQ = "2" *) (* C_RX_CC_NUM_SEQ = "2" *) (* C_RX_CC_PERIODICITY = "5000" *) 
(* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) (* C_RX_COMMA_M_ENABLE = "1" *) (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
(* C_RX_COMMA_P_ENABLE = "1" *) (* C_RX_COMMA_P_VAL = "10'b0101111100" *) (* C_RX_DATA_DECODING = "1" *) 
(* C_RX_ENABLE = "1" *) (* C_RX_INT_DATA_WIDTH = "20" *) (* C_RX_LINE_RATE = "1.250000" *) 
(* C_RX_MASTER_CHANNEL_IDX = "9" *) (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
(* C_RX_OUTCLK_SOURCE = "1" *) (* C_RX_PLL_TYPE = "2" *) (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_REFCLK_FREQUENCY = "156.250000" *) (* C_RX_SLIDE_MODE = "0" *) (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_RX_USER_CLOCKING_SOURCE = "0" *) (* C_RX_USER_DATA_WIDTH = "16" *) (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_RX_USRCLK_FREQUENCY = "62.500000" *) (* C_SECONDARY_QPLL_ENABLE = "0" *) (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
(* C_SIM_CPLL_CAL_BYPASS = "1" *) (* C_TOTAL_NUM_CHANNELS = "1" *) (* C_TOTAL_NUM_COMMONS = "0" *) 
(* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) (* C_TXPROGDIV_FREQ_ENABLE = "1" *) (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
(* C_TXPROGDIV_FREQ_VAL = "125.000000" *) (* C_TX_BUFFBYPASS_MODE = "0" *) (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
(* C_TX_BUFFER_MODE = "1" *) (* C_TX_DATA_ENCODING = "1" *) (* C_TX_ENABLE = "1" *) 
(* C_TX_INT_DATA_WIDTH = "20" *) (* C_TX_LINE_RATE = "1.250000" *) (* C_TX_MASTER_CHANNEL_IDX = "9" *) 
(* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) (* C_TX_OUTCLK_SOURCE = "4" *) 
(* C_TX_PLL_TYPE = "2" *) (* C_TX_REFCLK_FREQUENCY = "156.250000" *) (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_TX_USER_CLOCKING_SOURCE = "0" *) (* C_TX_USER_DATA_WIDTH = "16" *) (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_TX_USRCLK_FREQUENCY = "62.500000" *) (* C_USER_GTPOWERGOOD_DELAY_EN = "0" *) 
module PCS_PMA_gt_gtwizard_top
   (gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in,
    gtwiz_userclk_tx_srcclk_out,
    gtwiz_userclk_tx_usrclk_out,
    gtwiz_userclk_tx_usrclk2_out,
    gtwiz_userclk_tx_active_out,
    gtwiz_userclk_rx_reset_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_userclk_rx_srcclk_out,
    gtwiz_userclk_rx_usrclk_out,
    gtwiz_userclk_rx_usrclk2_out,
    gtwiz_userclk_rx_active_out,
    gtwiz_buffbypass_tx_reset_in,
    gtwiz_buffbypass_tx_start_user_in,
    gtwiz_buffbypass_tx_done_out,
    gtwiz_buffbypass_tx_error_out,
    gtwiz_buffbypass_rx_reset_in,
    gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out,
    gtwiz_buffbypass_rx_error_out,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_tx_done_in,
    gtwiz_reset_rx_done_in,
    gtwiz_reset_qpll0lock_in,
    gtwiz_reset_qpll1lock_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_reset_qpll0reset_out,
    gtwiz_reset_qpll1reset_out,
    gtwiz_gthe3_cpll_cal_txoutclk_period_in,
    gtwiz_gthe3_cpll_cal_cnt_tol_in,
    gtwiz_gthe3_cpll_cal_bufg_ce_in,
    gtwiz_gthe4_cpll_cal_txoutclk_period_in,
    gtwiz_gthe4_cpll_cal_cnt_tol_in,
    gtwiz_gthe4_cpll_cal_bufg_ce_in,
    gtwiz_gtye4_cpll_cal_txoutclk_period_in,
    gtwiz_gtye4_cpll_cal_cnt_tol_in,
    gtwiz_gtye4_cpll_cal_bufg_ce_in,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    bgbypassb_in,
    bgmonitorenb_in,
    bgpdb_in,
    bgrcalovrd_in,
    bgrcalovrdenb_in,
    drpaddr_common_in,
    drpclk_common_in,
    drpdi_common_in,
    drpen_common_in,
    drpwe_common_in,
    gtgrefclk0_in,
    gtgrefclk1_in,
    gtnorthrefclk00_in,
    gtnorthrefclk01_in,
    gtnorthrefclk10_in,
    gtnorthrefclk11_in,
    gtrefclk00_in,
    gtrefclk01_in,
    gtrefclk10_in,
    gtrefclk11_in,
    gtsouthrefclk00_in,
    gtsouthrefclk01_in,
    gtsouthrefclk10_in,
    gtsouthrefclk11_in,
    pcierateqpll0_in,
    pcierateqpll1_in,
    pmarsvd0_in,
    pmarsvd1_in,
    qpll0clkrsvd0_in,
    qpll0clkrsvd1_in,
    qpll0fbdiv_in,
    qpll0lockdetclk_in,
    qpll0locken_in,
    qpll0pd_in,
    qpll0refclksel_in,
    qpll0reset_in,
    qpll1clkrsvd0_in,
    qpll1clkrsvd1_in,
    qpll1fbdiv_in,
    qpll1lockdetclk_in,
    qpll1locken_in,
    qpll1pd_in,
    qpll1refclksel_in,
    qpll1reset_in,
    qpllrsvd1_in,
    qpllrsvd2_in,
    qpllrsvd3_in,
    qpllrsvd4_in,
    rcalenb_in,
    sdm0data_in,
    sdm0reset_in,
    sdm0toggle_in,
    sdm0width_in,
    sdm1data_in,
    sdm1reset_in,
    sdm1toggle_in,
    sdm1width_in,
    tcongpi_in,
    tconpowerup_in,
    tconreset_in,
    tconrsvdin1_in,
    ubcfgstreamen_in,
    ubdo_in,
    ubdrdy_in,
    ubenable_in,
    ubgpi_in,
    ubintr_in,
    ubiolmbrst_in,
    ubmbrst_in,
    ubmdmcapture_in,
    ubmdmdbgrst_in,
    ubmdmdbgupdate_in,
    ubmdmregen_in,
    ubmdmshift_in,
    ubmdmsysrst_in,
    ubmdmtck_in,
    ubmdmtdi_in,
    drpdo_common_out,
    drprdy_common_out,
    pmarsvdout0_out,
    pmarsvdout1_out,
    qpll0fbclklost_out,
    qpll0lock_out,
    qpll0outclk_out,
    qpll0outrefclk_out,
    qpll0refclklost_out,
    qpll1fbclklost_out,
    qpll1lock_out,
    qpll1outclk_out,
    qpll1outrefclk_out,
    qpll1refclklost_out,
    qplldmonitor0_out,
    qplldmonitor1_out,
    refclkoutmonitor0_out,
    refclkoutmonitor1_out,
    rxrecclk0_sel_out,
    rxrecclk1_sel_out,
    rxrecclk0sel_out,
    rxrecclk1sel_out,
    sdm0finalout_out,
    sdm0testdata_out,
    sdm1finalout_out,
    sdm1testdata_out,
    tcongpo_out,
    tconrsvdout0_out,
    ubdaddr_out,
    ubden_out,
    ubdi_out,
    ubdwe_out,
    ubmdmtdo_out,
    ubrsvdout_out,
    ubtxuart_out,
    cdrstepdir_in,
    cdrstepsq_in,
    cdrstepsx_in,
    cfgreset_in,
    clkrsvd0_in,
    clkrsvd1_in,
    cpllfreqlock_in,
    cplllockdetclk_in,
    cplllocken_in,
    cpllpd_in,
    cpllrefclksel_in,
    cpllreset_in,
    dmonfiforeset_in,
    dmonitorclk_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drprst_in,
    drpwe_in,
    elpcaldvorwren_in,
    elpcalpaorwren_in,
    evoddphicaldone_in,
    evoddphicalstart_in,
    evoddphidrden_in,
    evoddphidwren_in,
    evoddphixrden_in,
    evoddphixwren_in,
    eyescanmode_in,
    eyescanreset_in,
    eyescantrigger_in,
    freqos_in,
    gtgrefclk_in,
    gthrxn_in,
    gthrxp_in,
    gtnorthrefclk0_in,
    gtnorthrefclk1_in,
    gtrefclk0_in,
    gtrefclk1_in,
    gtresetsel_in,
    gtrsvd_in,
    gtrxreset_in,
    gtrxresetsel_in,
    gtsouthrefclk0_in,
    gtsouthrefclk1_in,
    gttxreset_in,
    gttxresetsel_in,
    incpctrl_in,
    gtyrxn_in,
    gtyrxp_in,
    loopback_in,
    looprsvd_in,
    lpbkrxtxseren_in,
    lpbktxrxseren_in,
    pcieeqrxeqadaptdone_in,
    pcierstidle_in,
    pciersttxsyncstart_in,
    pcieuserratedone_in,
    pcsrsvdin_in,
    pcsrsvdin2_in,
    pmarsvdin_in,
    qpll0clk_in,
    qpll0freqlock_in,
    qpll0refclk_in,
    qpll1clk_in,
    qpll1freqlock_in,
    qpll1refclk_in,
    resetovrd_in,
    rstclkentx_in,
    rx8b10ben_in,
    rxafecfoken_in,
    rxbufreset_in,
    rxcdrfreqreset_in,
    rxcdrhold_in,
    rxcdrovrden_in,
    rxcdrreset_in,
    rxcdrresetrsv_in,
    rxchbonden_in,
    rxchbondi_in,
    rxchbondlevel_in,
    rxchbondmaster_in,
    rxchbondslave_in,
    rxckcalreset_in,
    rxckcalstart_in,
    rxcommadeten_in,
    rxdfeagcctrl_in,
    rxdccforcestart_in,
    rxdfeagchold_in,
    rxdfeagcovrden_in,
    rxdfecfokfcnum_in,
    rxdfecfokfen_in,
    rxdfecfokfpulse_in,
    rxdfecfokhold_in,
    rxdfecfokovren_in,
    rxdfekhhold_in,
    rxdfekhovrden_in,
    rxdfelfhold_in,
    rxdfelfovrden_in,
    rxdfelpmreset_in,
    rxdfetap10hold_in,
    rxdfetap10ovrden_in,
    rxdfetap11hold_in,
    rxdfetap11ovrden_in,
    rxdfetap12hold_in,
    rxdfetap12ovrden_in,
    rxdfetap13hold_in,
    rxdfetap13ovrden_in,
    rxdfetap14hold_in,
    rxdfetap14ovrden_in,
    rxdfetap15hold_in,
    rxdfetap15ovrden_in,
    rxdfetap2hold_in,
    rxdfetap2ovrden_in,
    rxdfetap3hold_in,
    rxdfetap3ovrden_in,
    rxdfetap4hold_in,
    rxdfetap4ovrden_in,
    rxdfetap5hold_in,
    rxdfetap5ovrden_in,
    rxdfetap6hold_in,
    rxdfetap6ovrden_in,
    rxdfetap7hold_in,
    rxdfetap7ovrden_in,
    rxdfetap8hold_in,
    rxdfetap8ovrden_in,
    rxdfetap9hold_in,
    rxdfetap9ovrden_in,
    rxdfeuthold_in,
    rxdfeutovrden_in,
    rxdfevphold_in,
    rxdfevpovrden_in,
    rxdfevsen_in,
    rxdfexyden_in,
    rxdlybypass_in,
    rxdlyen_in,
    rxdlyovrden_in,
    rxdlysreset_in,
    rxelecidlemode_in,
    rxeqtraining_in,
    rxgearboxslip_in,
    rxlatclk_in,
    rxlpmen_in,
    rxlpmgchold_in,
    rxlpmgcovrden_in,
    rxlpmhfhold_in,
    rxlpmhfovrden_in,
    rxlpmlfhold_in,
    rxlpmlfklovrden_in,
    rxlpmoshold_in,
    rxlpmosovrden_in,
    rxmcommaalignen_in,
    rxmonitorsel_in,
    rxoobreset_in,
    rxoscalreset_in,
    rxoshold_in,
    rxosintcfg_in,
    rxosinten_in,
    rxosinthold_in,
    rxosintovrden_in,
    rxosintstrobe_in,
    rxosinttestovrden_in,
    rxosovrden_in,
    rxoutclksel_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxphalign_in,
    rxphalignen_in,
    rxphdlypd_in,
    rxphdlyreset_in,
    rxphovrden_in,
    rxpllclksel_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxprogdivreset_in,
    rxqpien_in,
    rxrate_in,
    rxratemode_in,
    rxslide_in,
    rxslipoutclk_in,
    rxslippma_in,
    rxsyncallin_in,
    rxsyncin_in,
    rxsyncmode_in,
    rxsysclksel_in,
    rxtermination_in,
    rxuserrdy_in,
    rxusrclk_in,
    rxusrclk2_in,
    sigvalidclk_in,
    tstin_in,
    tx8b10bbypass_in,
    tx8b10ben_in,
    txbufdiffctrl_in,
    txcominit_in,
    txcomsas_in,
    txcomwake_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdata_in,
    txdataextendrsvd_in,
    txdccforcestart_in,
    txdccreset_in,
    txdeemph_in,
    txdetectrx_in,
    txdiffctrl_in,
    txdiffpd_in,
    txdlybypass_in,
    txdlyen_in,
    txdlyhold_in,
    txdlyovrden_in,
    txdlysreset_in,
    txdlyupdown_in,
    txelecidle_in,
    txelforcestart_in,
    txheader_in,
    txinhibit_in,
    txlatclk_in,
    txlfpstreset_in,
    txlfpsu2lpexit_in,
    txlfpsu3wake_in,
    txmaincursor_in,
    txmargin_in,
    txmuxdcdexhold_in,
    txmuxdcdorwren_in,
    txoneszeros_in,
    txoutclksel_in,
    txpcsreset_in,
    txpd_in,
    txpdelecidlemode_in,
    txphalign_in,
    txphalignen_in,
    txphdlypd_in,
    txphdlyreset_in,
    txphdlytstclk_in,
    txphinit_in,
    txphovrden_in,
    txpippmen_in,
    txpippmovrden_in,
    txpippmpd_in,
    txpippmsel_in,
    txpippmstepsize_in,
    txpisopd_in,
    txpllclksel_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txpostcursorinv_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txprecursorinv_in,
    txprogdivreset_in,
    txqpibiasen_in,
    txqpistrongpdown_in,
    txqpiweakpup_in,
    txrate_in,
    txratemode_in,
    txsequence_in,
    txswing_in,
    txsyncallin_in,
    txsyncin_in,
    txsyncmode_in,
    txsysclksel_in,
    txuserrdy_in,
    txusrclk_in,
    txusrclk2_in,
    bufgtce_out,
    bufgtcemask_out,
    bufgtdiv_out,
    bufgtreset_out,
    bufgtrstmask_out,
    cpllfbclklost_out,
    cplllock_out,
    cpllrefclklost_out,
    dmonitorout_out,
    dmonitoroutclk_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    gtrefclkmonitor_out,
    gtytxn_out,
    gtytxp_out,
    pcierategen3_out,
    pcierateidle_out,
    pcierateqpllpd_out,
    pcierateqpllreset_out,
    pciesynctxsyncdone_out,
    pcieusergen3rdy_out,
    pcieuserphystatusrst_out,
    pcieuserratestart_out,
    pcsrsvdout_out,
    phystatus_out,
    pinrsrvdas_out,
    powerpresent_out,
    resetexception_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxcdrlock_out,
    rxcdrphdone_out,
    rxchanbondseq_out,
    rxchanisaligned_out,
    rxchanrealign_out,
    rxchbondo_out,
    rxckcaldone_out,
    rxclkcorcnt_out,
    rxcominitdet_out,
    rxcommadet_out,
    rxcomsasdet_out,
    rxcomwakedet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxdata_out,
    rxdataextendrsvd_out,
    rxdatavalid_out,
    rxdlysresetdone_out,
    rxelecidle_out,
    rxheader_out,
    rxheadervalid_out,
    rxlfpstresetdet_out,
    rxlfpsu2lpexitdet_out,
    rxlfpsu3wakedet_out,
    rxmonitorout_out,
    rxosintdone_out,
    rxosintstarted_out,
    rxosintstrobedone_out,
    rxosintstrobestarted_out,
    rxoutclk_out,
    rxoutclkfabric_out,
    rxoutclkpcs_out,
    rxphaligndone_out,
    rxphalignerr_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxprbslocked_out,
    rxprgdivresetdone_out,
    rxqpisenn_out,
    rxqpisenp_out,
    rxratedone_out,
    rxrecclkout_out,
    rxresetdone_out,
    rxsliderdy_out,
    rxslipdone_out,
    rxslipoutclkrdy_out,
    rxslippmardy_out,
    rxstartofseq_out,
    rxstatus_out,
    rxsyncdone_out,
    rxsyncout_out,
    rxvalid_out,
    txbufstatus_out,
    txcomfinish_out,
    txdccdone_out,
    txdlysresetdone_out,
    txoutclk_out,
    txoutclkfabric_out,
    txoutclkpcs_out,
    txphaligndone_out,
    txphinitdone_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txqpisenn_out,
    txqpisenp_out,
    txratedone_out,
    txresetdone_out,
    txsyncdone_out,
    txsyncout_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  output [0:0]gtwiz_userclk_tx_srcclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk2_out;
  output [0:0]gtwiz_userclk_tx_active_out;
  input [0:0]gtwiz_userclk_rx_reset_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  output [0:0]gtwiz_userclk_rx_srcclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk2_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input [0:0]gtwiz_reset_tx_done_in;
  input [0:0]gtwiz_reset_rx_done_in;
  input [0:0]gtwiz_reset_qpll0lock_in;
  input [0:0]gtwiz_reset_qpll1lock_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output [0:0]gtwiz_reset_qpll0reset_out;
  output [0:0]gtwiz_reset_qpll1reset_out;
  input [17:0]gtwiz_gthe3_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe3_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe3_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gthe4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe4_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gtye4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gtye4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gtye4_cpll_cal_bufg_ce_in;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [0:0]bgbypassb_in;
  input [0:0]bgmonitorenb_in;
  input [0:0]bgpdb_in;
  input [4:0]bgrcalovrd_in;
  input [0:0]bgrcalovrdenb_in;
  input [8:0]drpaddr_common_in;
  input [0:0]drpclk_common_in;
  input [15:0]drpdi_common_in;
  input [0:0]drpen_common_in;
  input [0:0]drpwe_common_in;
  input [0:0]gtgrefclk0_in;
  input [0:0]gtgrefclk1_in;
  input [0:0]gtnorthrefclk00_in;
  input [0:0]gtnorthrefclk01_in;
  input [0:0]gtnorthrefclk10_in;
  input [0:0]gtnorthrefclk11_in;
  input [0:0]gtrefclk00_in;
  input [0:0]gtrefclk01_in;
  input [0:0]gtrefclk10_in;
  input [0:0]gtrefclk11_in;
  input [0:0]gtsouthrefclk00_in;
  input [0:0]gtsouthrefclk01_in;
  input [0:0]gtsouthrefclk10_in;
  input [0:0]gtsouthrefclk11_in;
  input [0:0]pcierateqpll0_in;
  input [0:0]pcierateqpll1_in;
  input [7:0]pmarsvd0_in;
  input [7:0]pmarsvd1_in;
  input [0:0]qpll0clkrsvd0_in;
  input [0:0]qpll0clkrsvd1_in;
  input [0:0]qpll0fbdiv_in;
  input [0:0]qpll0lockdetclk_in;
  input [0:0]qpll0locken_in;
  input [0:0]qpll0pd_in;
  input [2:0]qpll0refclksel_in;
  input [0:0]qpll0reset_in;
  input [0:0]qpll1clkrsvd0_in;
  input [0:0]qpll1clkrsvd1_in;
  input [0:0]qpll1fbdiv_in;
  input [0:0]qpll1lockdetclk_in;
  input [0:0]qpll1locken_in;
  input [0:0]qpll1pd_in;
  input [2:0]qpll1refclksel_in;
  input [0:0]qpll1reset_in;
  input [7:0]qpllrsvd1_in;
  input [4:0]qpllrsvd2_in;
  input [4:0]qpllrsvd3_in;
  input [7:0]qpllrsvd4_in;
  input [0:0]rcalenb_in;
  input [0:0]sdm0data_in;
  input [0:0]sdm0reset_in;
  input [0:0]sdm0toggle_in;
  input [0:0]sdm0width_in;
  input [0:0]sdm1data_in;
  input [0:0]sdm1reset_in;
  input [0:0]sdm1toggle_in;
  input [0:0]sdm1width_in;
  input [0:0]tcongpi_in;
  input [0:0]tconpowerup_in;
  input [0:0]tconreset_in;
  input [0:0]tconrsvdin1_in;
  input [0:0]ubcfgstreamen_in;
  input [0:0]ubdo_in;
  input [0:0]ubdrdy_in;
  input [0:0]ubenable_in;
  input [0:0]ubgpi_in;
  input [0:0]ubintr_in;
  input [0:0]ubiolmbrst_in;
  input [0:0]ubmbrst_in;
  input [0:0]ubmdmcapture_in;
  input [0:0]ubmdmdbgrst_in;
  input [0:0]ubmdmdbgupdate_in;
  input [0:0]ubmdmregen_in;
  input [0:0]ubmdmshift_in;
  input [0:0]ubmdmsysrst_in;
  input [0:0]ubmdmtck_in;
  input [0:0]ubmdmtdi_in;
  output [15:0]drpdo_common_out;
  output [0:0]drprdy_common_out;
  output [7:0]pmarsvdout0_out;
  output [7:0]pmarsvdout1_out;
  output [0:0]qpll0fbclklost_out;
  output [0:0]qpll0lock_out;
  output [0:0]qpll0outclk_out;
  output [0:0]qpll0outrefclk_out;
  output [0:0]qpll0refclklost_out;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1lock_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  output [7:0]qplldmonitor0_out;
  output [7:0]qplldmonitor1_out;
  output [0:0]refclkoutmonitor0_out;
  output [0:0]refclkoutmonitor1_out;
  output [1:0]rxrecclk0_sel_out;
  output [1:0]rxrecclk1_sel_out;
  output [0:0]rxrecclk0sel_out;
  output [0:0]rxrecclk1sel_out;
  output [0:0]sdm0finalout_out;
  output [0:0]sdm0testdata_out;
  output [0:0]sdm1finalout_out;
  output [0:0]sdm1testdata_out;
  output [0:0]tcongpo_out;
  output [0:0]tconrsvdout0_out;
  output [0:0]ubdaddr_out;
  output [0:0]ubden_out;
  output [0:0]ubdi_out;
  output [0:0]ubdwe_out;
  output [0:0]ubmdmtdo_out;
  output [0:0]ubrsvdout_out;
  output [0:0]ubtxuart_out;
  input [0:0]cdrstepdir_in;
  input [0:0]cdrstepsq_in;
  input [0:0]cdrstepsx_in;
  input [0:0]cfgreset_in;
  input [0:0]clkrsvd0_in;
  input [0:0]clkrsvd1_in;
  input [0:0]cpllfreqlock_in;
  input [0:0]cplllockdetclk_in;
  input [0:0]cplllocken_in;
  input [0:0]cpllpd_in;
  input [2:0]cpllrefclksel_in;
  input [0:0]cpllreset_in;
  input [0:0]dmonfiforeset_in;
  input [0:0]dmonitorclk_in;
  input [8:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drprst_in;
  input [0:0]drpwe_in;
  input [0:0]elpcaldvorwren_in;
  input [0:0]elpcalpaorwren_in;
  input [0:0]evoddphicaldone_in;
  input [0:0]evoddphicalstart_in;
  input [0:0]evoddphidrden_in;
  input [0:0]evoddphidwren_in;
  input [0:0]evoddphixrden_in;
  input [0:0]evoddphixwren_in;
  input [0:0]eyescanmode_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]freqos_in;
  input [0:0]gtgrefclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtnorthrefclk0_in;
  input [0:0]gtnorthrefclk1_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [0:0]gtresetsel_in;
  input [15:0]gtrsvd_in;
  input [0:0]gtrxreset_in;
  input [0:0]gtrxresetsel_in;
  input [0:0]gtsouthrefclk0_in;
  input [0:0]gtsouthrefclk1_in;
  input [0:0]gttxreset_in;
  input [0:0]gttxresetsel_in;
  input [0:0]incpctrl_in;
  input [0:0]gtyrxn_in;
  input [0:0]gtyrxp_in;
  input [2:0]loopback_in;
  input [0:0]looprsvd_in;
  input [0:0]lpbkrxtxseren_in;
  input [0:0]lpbktxrxseren_in;
  input [0:0]pcieeqrxeqadaptdone_in;
  input [0:0]pcierstidle_in;
  input [0:0]pciersttxsyncstart_in;
  input [0:0]pcieuserratedone_in;
  input [15:0]pcsrsvdin_in;
  input [4:0]pcsrsvdin2_in;
  input [4:0]pmarsvdin_in;
  input [0:0]qpll0clk_in;
  input [0:0]qpll0freqlock_in;
  input [0:0]qpll0refclk_in;
  input [0:0]qpll1clk_in;
  input [0:0]qpll1freqlock_in;
  input [0:0]qpll1refclk_in;
  input [0:0]resetovrd_in;
  input [0:0]rstclkentx_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxafecfoken_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrfreqreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcdrovrden_in;
  input [0:0]rxcdrreset_in;
  input [0:0]rxcdrresetrsv_in;
  input [0:0]rxchbonden_in;
  input [4:0]rxchbondi_in;
  input [2:0]rxchbondlevel_in;
  input [0:0]rxchbondmaster_in;
  input [0:0]rxchbondslave_in;
  input [0:0]rxckcalreset_in;
  input [0:0]rxckcalstart_in;
  input [0:0]rxcommadeten_in;
  input [1:0]rxdfeagcctrl_in;
  input [0:0]rxdccforcestart_in;
  input [0:0]rxdfeagchold_in;
  input [0:0]rxdfeagcovrden_in;
  input [0:0]rxdfecfokfcnum_in;
  input [0:0]rxdfecfokfen_in;
  input [0:0]rxdfecfokfpulse_in;
  input [0:0]rxdfecfokhold_in;
  input [0:0]rxdfecfokovren_in;
  input [0:0]rxdfekhhold_in;
  input [0:0]rxdfekhovrden_in;
  input [0:0]rxdfelfhold_in;
  input [0:0]rxdfelfovrden_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxdfetap10hold_in;
  input [0:0]rxdfetap10ovrden_in;
  input [0:0]rxdfetap11hold_in;
  input [0:0]rxdfetap11ovrden_in;
  input [0:0]rxdfetap12hold_in;
  input [0:0]rxdfetap12ovrden_in;
  input [0:0]rxdfetap13hold_in;
  input [0:0]rxdfetap13ovrden_in;
  input [0:0]rxdfetap14hold_in;
  input [0:0]rxdfetap14ovrden_in;
  input [0:0]rxdfetap15hold_in;
  input [0:0]rxdfetap15ovrden_in;
  input [0:0]rxdfetap2hold_in;
  input [0:0]rxdfetap2ovrden_in;
  input [0:0]rxdfetap3hold_in;
  input [0:0]rxdfetap3ovrden_in;
  input [0:0]rxdfetap4hold_in;
  input [0:0]rxdfetap4ovrden_in;
  input [0:0]rxdfetap5hold_in;
  input [0:0]rxdfetap5ovrden_in;
  input [0:0]rxdfetap6hold_in;
  input [0:0]rxdfetap6ovrden_in;
  input [0:0]rxdfetap7hold_in;
  input [0:0]rxdfetap7ovrden_in;
  input [0:0]rxdfetap8hold_in;
  input [0:0]rxdfetap8ovrden_in;
  input [0:0]rxdfetap9hold_in;
  input [0:0]rxdfetap9ovrden_in;
  input [0:0]rxdfeuthold_in;
  input [0:0]rxdfeutovrden_in;
  input [0:0]rxdfevphold_in;
  input [0:0]rxdfevpovrden_in;
  input [0:0]rxdfevsen_in;
  input [0:0]rxdfexyden_in;
  input [0:0]rxdlybypass_in;
  input [0:0]rxdlyen_in;
  input [0:0]rxdlyovrden_in;
  input [0:0]rxdlysreset_in;
  input [1:0]rxelecidlemode_in;
  input [0:0]rxeqtraining_in;
  input [0:0]rxgearboxslip_in;
  input [0:0]rxlatclk_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxlpmgchold_in;
  input [0:0]rxlpmgcovrden_in;
  input [0:0]rxlpmhfhold_in;
  input [0:0]rxlpmhfovrden_in;
  input [0:0]rxlpmlfhold_in;
  input [0:0]rxlpmlfklovrden_in;
  input [0:0]rxlpmoshold_in;
  input [0:0]rxlpmosovrden_in;
  input [0:0]rxmcommaalignen_in;
  input [1:0]rxmonitorsel_in;
  input [0:0]rxoobreset_in;
  input [0:0]rxoscalreset_in;
  input [0:0]rxoshold_in;
  input [3:0]rxosintcfg_in;
  input [0:0]rxosinten_in;
  input [0:0]rxosinthold_in;
  input [0:0]rxosintovrden_in;
  input [0:0]rxosintstrobe_in;
  input [0:0]rxosinttestovrden_in;
  input [0:0]rxosovrden_in;
  input [2:0]rxoutclksel_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxphalign_in;
  input [0:0]rxphalignen_in;
  input [0:0]rxphdlypd_in;
  input [0:0]rxphdlyreset_in;
  input [0:0]rxphovrden_in;
  input [1:0]rxpllclksel_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [0:0]rxprogdivreset_in;
  input [0:0]rxqpien_in;
  input [2:0]rxrate_in;
  input [0:0]rxratemode_in;
  input [0:0]rxslide_in;
  input [0:0]rxslipoutclk_in;
  input [0:0]rxslippma_in;
  input [0:0]rxsyncallin_in;
  input [0:0]rxsyncin_in;
  input [0:0]rxsyncmode_in;
  input [1:0]rxsysclksel_in;
  input [0:0]rxtermination_in;
  input [0:0]rxuserrdy_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]sigvalidclk_in;
  input [19:0]tstin_in;
  input [7:0]tx8b10bbypass_in;
  input [0:0]tx8b10ben_in;
  input [2:0]txbufdiffctrl_in;
  input [0:0]txcominit_in;
  input [0:0]txcomsas_in;
  input [0:0]txcomwake_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [127:0]txdata_in;
  input [7:0]txdataextendrsvd_in;
  input [0:0]txdccforcestart_in;
  input [0:0]txdccreset_in;
  input [0:0]txdeemph_in;
  input [0:0]txdetectrx_in;
  input [3:0]txdiffctrl_in;
  input [0:0]txdiffpd_in;
  input [0:0]txdlybypass_in;
  input [0:0]txdlyen_in;
  input [0:0]txdlyhold_in;
  input [0:0]txdlyovrden_in;
  input [0:0]txdlysreset_in;
  input [0:0]txdlyupdown_in;
  input [0:0]txelecidle_in;
  input [0:0]txelforcestart_in;
  input [5:0]txheader_in;
  input [0:0]txinhibit_in;
  input [0:0]txlatclk_in;
  input [0:0]txlfpstreset_in;
  input [0:0]txlfpsu2lpexit_in;
  input [0:0]txlfpsu3wake_in;
  input [6:0]txmaincursor_in;
  input [2:0]txmargin_in;
  input [0:0]txmuxdcdexhold_in;
  input [0:0]txmuxdcdorwren_in;
  input [0:0]txoneszeros_in;
  input [2:0]txoutclksel_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpdelecidlemode_in;
  input [0:0]txphalign_in;
  input [0:0]txphalignen_in;
  input [0:0]txphdlypd_in;
  input [0:0]txphdlyreset_in;
  input [0:0]txphdlytstclk_in;
  input [0:0]txphinit_in;
  input [0:0]txphovrden_in;
  input [0:0]txpippmen_in;
  input [0:0]txpippmovrden_in;
  input [0:0]txpippmpd_in;
  input [0:0]txpippmsel_in;
  input [4:0]txpippmstepsize_in;
  input [0:0]txpisopd_in;
  input [1:0]txpllclksel_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txpostcursorinv_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txprecursorinv_in;
  input [0:0]txprogdivreset_in;
  input [0:0]txqpibiasen_in;
  input [0:0]txqpistrongpdown_in;
  input [0:0]txqpiweakpup_in;
  input [2:0]txrate_in;
  input [0:0]txratemode_in;
  input [6:0]txsequence_in;
  input [0:0]txswing_in;
  input [0:0]txsyncallin_in;
  input [0:0]txsyncin_in;
  input [0:0]txsyncmode_in;
  input [1:0]txsysclksel_in;
  input [0:0]txuserrdy_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [2:0]bufgtce_out;
  output [2:0]bufgtcemask_out;
  output [8:0]bufgtdiv_out;
  output [2:0]bufgtreset_out;
  output [2:0]bufgtrstmask_out;
  output [0:0]cpllfbclklost_out;
  output [0:0]cplllock_out;
  output [0:0]cpllrefclklost_out;
  output [16:0]dmonitorout_out;
  output [0:0]dmonitoroutclk_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]gtrefclkmonitor_out;
  output [0:0]gtytxn_out;
  output [0:0]gtytxp_out;
  output [0:0]pcierategen3_out;
  output [0:0]pcierateidle_out;
  output [1:0]pcierateqpllpd_out;
  output [1:0]pcierateqpllreset_out;
  output [0:0]pciesynctxsyncdone_out;
  output [0:0]pcieusergen3rdy_out;
  output [0:0]pcieuserphystatusrst_out;
  output [0:0]pcieuserratestart_out;
  output [11:0]pcsrsvdout_out;
  output [0:0]phystatus_out;
  output [7:0]pinrsrvdas_out;
  output [0:0]powerpresent_out;
  output [0:0]resetexception_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxcdrphdone_out;
  output [0:0]rxchanbondseq_out;
  output [0:0]rxchanisaligned_out;
  output [0:0]rxchanrealign_out;
  output [4:0]rxchbondo_out;
  output [0:0]rxckcaldone_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcominitdet_out;
  output [0:0]rxcommadet_out;
  output [0:0]rxcomsasdet_out;
  output [0:0]rxcomwakedet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [127:0]rxdata_out;
  output [7:0]rxdataextendrsvd_out;
  output [1:0]rxdatavalid_out;
  output [0:0]rxdlysresetdone_out;
  output [0:0]rxelecidle_out;
  output [5:0]rxheader_out;
  output [1:0]rxheadervalid_out;
  output [0:0]rxlfpstresetdet_out;
  output [0:0]rxlfpsu2lpexitdet_out;
  output [0:0]rxlfpsu3wakedet_out;
  output [6:0]rxmonitorout_out;
  output [0:0]rxosintdone_out;
  output [0:0]rxosintstarted_out;
  output [0:0]rxosintstrobedone_out;
  output [0:0]rxosintstrobestarted_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkfabric_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]rxphaligndone_out;
  output [0:0]rxphalignerr_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxprbslocked_out;
  output [0:0]rxprgdivresetdone_out;
  output [0:0]rxqpisenn_out;
  output [0:0]rxqpisenp_out;
  output [0:0]rxratedone_out;
  output [0:0]rxrecclkout_out;
  output [0:0]rxresetdone_out;
  output [0:0]rxsliderdy_out;
  output [0:0]rxslipdone_out;
  output [0:0]rxslipoutclkrdy_out;
  output [0:0]rxslippmardy_out;
  output [1:0]rxstartofseq_out;
  output [2:0]rxstatus_out;
  output [0:0]rxsyncdone_out;
  output [0:0]rxsyncout_out;
  output [0:0]rxvalid_out;
  output [1:0]txbufstatus_out;
  output [0:0]txcomfinish_out;
  output [0:0]txdccdone_out;
  output [0:0]txdlysresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txoutclkfabric_out;
  output [0:0]txoutclkpcs_out;
  output [0:0]txphaligndone_out;
  output [0:0]txphinitdone_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txqpisenn_out;
  output [0:0]txqpisenp_out;
  output [0:0]txratedone_out;
  output [0:0]txresetdone_out;
  output [0:0]txsyncdone_out;
  output [0:0]txsyncout_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  assign bufgtce_out[2] = \<const0> ;
  assign bufgtce_out[1] = \<const0> ;
  assign bufgtce_out[0] = \<const0> ;
  assign bufgtcemask_out[2] = \<const0> ;
  assign bufgtcemask_out[1] = \<const0> ;
  assign bufgtcemask_out[0] = \<const0> ;
  assign bufgtdiv_out[8] = \<const0> ;
  assign bufgtdiv_out[7] = \<const0> ;
  assign bufgtdiv_out[6] = \<const0> ;
  assign bufgtdiv_out[5] = \<const0> ;
  assign bufgtdiv_out[4] = \<const0> ;
  assign bufgtdiv_out[3] = \<const0> ;
  assign bufgtdiv_out[2] = \<const0> ;
  assign bufgtdiv_out[1] = \<const0> ;
  assign bufgtdiv_out[0] = \<const0> ;
  assign bufgtreset_out[2] = \<const0> ;
  assign bufgtreset_out[1] = \<const0> ;
  assign bufgtreset_out[0] = \<const0> ;
  assign bufgtrstmask_out[2] = \<const0> ;
  assign bufgtrstmask_out[1] = \<const0> ;
  assign bufgtrstmask_out[0] = \<const0> ;
  assign cpllfbclklost_out[0] = \<const0> ;
  assign cplllock_out[0] = \<const0> ;
  assign cpllrefclklost_out[0] = \<const0> ;
  assign dmonitorout_out[16] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign dmonitoroutclk_out[0] = \<const0> ;
  assign drpdo_common_out[15] = \<const0> ;
  assign drpdo_common_out[14] = \<const0> ;
  assign drpdo_common_out[13] = \<const0> ;
  assign drpdo_common_out[12] = \<const0> ;
  assign drpdo_common_out[11] = \<const0> ;
  assign drpdo_common_out[10] = \<const0> ;
  assign drpdo_common_out[9] = \<const0> ;
  assign drpdo_common_out[8] = \<const0> ;
  assign drpdo_common_out[7] = \<const0> ;
  assign drpdo_common_out[6] = \<const0> ;
  assign drpdo_common_out[5] = \<const0> ;
  assign drpdo_common_out[4] = \<const0> ;
  assign drpdo_common_out[3] = \<const0> ;
  assign drpdo_common_out[2] = \<const0> ;
  assign drpdo_common_out[1] = \<const0> ;
  assign drpdo_common_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_common_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtrefclkmonitor_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_error_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_error_out[0] = \<const0> ;
  assign gtwiz_reset_qpll0reset_out[0] = \<const0> ;
  assign gtwiz_reset_qpll1reset_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk_out[0] = \<const0> ;
  assign gtytxn_out[0] = \<const0> ;
  assign gtytxp_out[0] = \<const0> ;
  assign pcierategen3_out[0] = \<const0> ;
  assign pcierateidle_out[0] = \<const0> ;
  assign pcierateqpllpd_out[1] = \<const0> ;
  assign pcierateqpllpd_out[0] = \<const0> ;
  assign pcierateqpllreset_out[1] = \<const0> ;
  assign pcierateqpllreset_out[0] = \<const0> ;
  assign pciesynctxsyncdone_out[0] = \<const0> ;
  assign pcieusergen3rdy_out[0] = \<const0> ;
  assign pcieuserphystatusrst_out[0] = \<const0> ;
  assign pcieuserratestart_out[0] = \<const0> ;
  assign pcsrsvdout_out[11] = \<const0> ;
  assign pcsrsvdout_out[10] = \<const0> ;
  assign pcsrsvdout_out[9] = \<const0> ;
  assign pcsrsvdout_out[8] = \<const0> ;
  assign pcsrsvdout_out[7] = \<const0> ;
  assign pcsrsvdout_out[6] = \<const0> ;
  assign pcsrsvdout_out[5] = \<const0> ;
  assign pcsrsvdout_out[4] = \<const0> ;
  assign pcsrsvdout_out[3] = \<const0> ;
  assign pcsrsvdout_out[2] = \<const0> ;
  assign pcsrsvdout_out[1] = \<const0> ;
  assign pcsrsvdout_out[0] = \<const0> ;
  assign phystatus_out[0] = \<const0> ;
  assign pinrsrvdas_out[7] = \<const0> ;
  assign pinrsrvdas_out[6] = \<const0> ;
  assign pinrsrvdas_out[5] = \<const0> ;
  assign pinrsrvdas_out[4] = \<const0> ;
  assign pinrsrvdas_out[3] = \<const0> ;
  assign pinrsrvdas_out[2] = \<const0> ;
  assign pinrsrvdas_out[1] = \<const0> ;
  assign pinrsrvdas_out[0] = \<const0> ;
  assign pmarsvdout0_out[7] = \<const0> ;
  assign pmarsvdout0_out[6] = \<const0> ;
  assign pmarsvdout0_out[5] = \<const0> ;
  assign pmarsvdout0_out[4] = \<const0> ;
  assign pmarsvdout0_out[3] = \<const0> ;
  assign pmarsvdout0_out[2] = \<const0> ;
  assign pmarsvdout0_out[1] = \<const0> ;
  assign pmarsvdout0_out[0] = \<const0> ;
  assign pmarsvdout1_out[7] = \<const0> ;
  assign pmarsvdout1_out[6] = \<const0> ;
  assign pmarsvdout1_out[5] = \<const0> ;
  assign pmarsvdout1_out[4] = \<const0> ;
  assign pmarsvdout1_out[3] = \<const0> ;
  assign pmarsvdout1_out[2] = \<const0> ;
  assign pmarsvdout1_out[1] = \<const0> ;
  assign pmarsvdout1_out[0] = \<const0> ;
  assign powerpresent_out[0] = \<const0> ;
  assign qpll0fbclklost_out[0] = \<const0> ;
  assign qpll0lock_out[0] = \<const0> ;
  assign qpll0outclk_out[0] = \<const0> ;
  assign qpll0outrefclk_out[0] = \<const0> ;
  assign qpll0refclklost_out[0] = \<const0> ;
  assign qpll1fbclklost_out[0] = \<const0> ;
  assign qpll1lock_out[0] = \<const0> ;
  assign qpll1outclk_out[0] = \<const0> ;
  assign qpll1outrefclk_out[0] = \<const0> ;
  assign qpll1refclklost_out[0] = \<const0> ;
  assign qplldmonitor0_out[7] = \<const0> ;
  assign qplldmonitor0_out[6] = \<const0> ;
  assign qplldmonitor0_out[5] = \<const0> ;
  assign qplldmonitor0_out[4] = \<const0> ;
  assign qplldmonitor0_out[3] = \<const0> ;
  assign qplldmonitor0_out[2] = \<const0> ;
  assign qplldmonitor0_out[1] = \<const0> ;
  assign qplldmonitor0_out[0] = \<const0> ;
  assign qplldmonitor1_out[7] = \<const0> ;
  assign qplldmonitor1_out[6] = \<const0> ;
  assign qplldmonitor1_out[5] = \<const0> ;
  assign qplldmonitor1_out[4] = \<const0> ;
  assign qplldmonitor1_out[3] = \<const0> ;
  assign qplldmonitor1_out[2] = \<const0> ;
  assign qplldmonitor1_out[1] = \<const0> ;
  assign qplldmonitor1_out[0] = \<const0> ;
  assign refclkoutmonitor0_out[0] = \<const0> ;
  assign refclkoutmonitor1_out[0] = \<const0> ;
  assign resetexception_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcdrlock_out[0] = \<const0> ;
  assign rxcdrphdone_out[0] = \<const0> ;
  assign rxchanbondseq_out[0] = \<const0> ;
  assign rxchanisaligned_out[0] = \<const0> ;
  assign rxchanrealign_out[0] = \<const0> ;
  assign rxchbondo_out[4] = \<const0> ;
  assign rxchbondo_out[3] = \<const0> ;
  assign rxchbondo_out[2] = \<const0> ;
  assign rxchbondo_out[1] = \<const0> ;
  assign rxchbondo_out[0] = \<const0> ;
  assign rxckcaldone_out[0] = \<const0> ;
  assign rxcominitdet_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxcomsasdet_out[0] = \<const0> ;
  assign rxcomwakedet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxdata_out[127] = \<const0> ;
  assign rxdata_out[126] = \<const0> ;
  assign rxdata_out[125] = \<const0> ;
  assign rxdata_out[124] = \<const0> ;
  assign rxdata_out[123] = \<const0> ;
  assign rxdata_out[122] = \<const0> ;
  assign rxdata_out[121] = \<const0> ;
  assign rxdata_out[120] = \<const0> ;
  assign rxdata_out[119] = \<const0> ;
  assign rxdata_out[118] = \<const0> ;
  assign rxdata_out[117] = \<const0> ;
  assign rxdata_out[116] = \<const0> ;
  assign rxdata_out[115] = \<const0> ;
  assign rxdata_out[114] = \<const0> ;
  assign rxdata_out[113] = \<const0> ;
  assign rxdata_out[112] = \<const0> ;
  assign rxdata_out[111] = \<const0> ;
  assign rxdata_out[110] = \<const0> ;
  assign rxdata_out[109] = \<const0> ;
  assign rxdata_out[108] = \<const0> ;
  assign rxdata_out[107] = \<const0> ;
  assign rxdata_out[106] = \<const0> ;
  assign rxdata_out[105] = \<const0> ;
  assign rxdata_out[104] = \<const0> ;
  assign rxdata_out[103] = \<const0> ;
  assign rxdata_out[102] = \<const0> ;
  assign rxdata_out[101] = \<const0> ;
  assign rxdata_out[100] = \<const0> ;
  assign rxdata_out[99] = \<const0> ;
  assign rxdata_out[98] = \<const0> ;
  assign rxdata_out[97] = \<const0> ;
  assign rxdata_out[96] = \<const0> ;
  assign rxdata_out[95] = \<const0> ;
  assign rxdata_out[94] = \<const0> ;
  assign rxdata_out[93] = \<const0> ;
  assign rxdata_out[92] = \<const0> ;
  assign rxdata_out[91] = \<const0> ;
  assign rxdata_out[90] = \<const0> ;
  assign rxdata_out[89] = \<const0> ;
  assign rxdata_out[88] = \<const0> ;
  assign rxdata_out[87] = \<const0> ;
  assign rxdata_out[86] = \<const0> ;
  assign rxdata_out[85] = \<const0> ;
  assign rxdata_out[84] = \<const0> ;
  assign rxdata_out[83] = \<const0> ;
  assign rxdata_out[82] = \<const0> ;
  assign rxdata_out[81] = \<const0> ;
  assign rxdata_out[80] = \<const0> ;
  assign rxdata_out[79] = \<const0> ;
  assign rxdata_out[78] = \<const0> ;
  assign rxdata_out[77] = \<const0> ;
  assign rxdata_out[76] = \<const0> ;
  assign rxdata_out[75] = \<const0> ;
  assign rxdata_out[74] = \<const0> ;
  assign rxdata_out[73] = \<const0> ;
  assign rxdata_out[72] = \<const0> ;
  assign rxdata_out[71] = \<const0> ;
  assign rxdata_out[70] = \<const0> ;
  assign rxdata_out[69] = \<const0> ;
  assign rxdata_out[68] = \<const0> ;
  assign rxdata_out[67] = \<const0> ;
  assign rxdata_out[66] = \<const0> ;
  assign rxdata_out[65] = \<const0> ;
  assign rxdata_out[64] = \<const0> ;
  assign rxdata_out[63] = \<const0> ;
  assign rxdata_out[62] = \<const0> ;
  assign rxdata_out[61] = \<const0> ;
  assign rxdata_out[60] = \<const0> ;
  assign rxdata_out[59] = \<const0> ;
  assign rxdata_out[58] = \<const0> ;
  assign rxdata_out[57] = \<const0> ;
  assign rxdata_out[56] = \<const0> ;
  assign rxdata_out[55] = \<const0> ;
  assign rxdata_out[54] = \<const0> ;
  assign rxdata_out[53] = \<const0> ;
  assign rxdata_out[52] = \<const0> ;
  assign rxdata_out[51] = \<const0> ;
  assign rxdata_out[50] = \<const0> ;
  assign rxdata_out[49] = \<const0> ;
  assign rxdata_out[48] = \<const0> ;
  assign rxdata_out[47] = \<const0> ;
  assign rxdata_out[46] = \<const0> ;
  assign rxdata_out[45] = \<const0> ;
  assign rxdata_out[44] = \<const0> ;
  assign rxdata_out[43] = \<const0> ;
  assign rxdata_out[42] = \<const0> ;
  assign rxdata_out[41] = \<const0> ;
  assign rxdata_out[40] = \<const0> ;
  assign rxdata_out[39] = \<const0> ;
  assign rxdata_out[38] = \<const0> ;
  assign rxdata_out[37] = \<const0> ;
  assign rxdata_out[36] = \<const0> ;
  assign rxdata_out[35] = \<const0> ;
  assign rxdata_out[34] = \<const0> ;
  assign rxdata_out[33] = \<const0> ;
  assign rxdata_out[32] = \<const0> ;
  assign rxdata_out[31] = \<const0> ;
  assign rxdata_out[30] = \<const0> ;
  assign rxdata_out[29] = \<const0> ;
  assign rxdata_out[28] = \<const0> ;
  assign rxdata_out[27] = \<const0> ;
  assign rxdata_out[26] = \<const0> ;
  assign rxdata_out[25] = \<const0> ;
  assign rxdata_out[24] = \<const0> ;
  assign rxdata_out[23] = \<const0> ;
  assign rxdata_out[22] = \<const0> ;
  assign rxdata_out[21] = \<const0> ;
  assign rxdata_out[20] = \<const0> ;
  assign rxdata_out[19] = \<const0> ;
  assign rxdata_out[18] = \<const0> ;
  assign rxdata_out[17] = \<const0> ;
  assign rxdata_out[16] = \<const0> ;
  assign rxdata_out[15] = \<const0> ;
  assign rxdata_out[14] = \<const0> ;
  assign rxdata_out[13] = \<const0> ;
  assign rxdata_out[12] = \<const0> ;
  assign rxdata_out[11] = \<const0> ;
  assign rxdata_out[10] = \<const0> ;
  assign rxdata_out[9] = \<const0> ;
  assign rxdata_out[8] = \<const0> ;
  assign rxdata_out[7] = \<const0> ;
  assign rxdata_out[6] = \<const0> ;
  assign rxdata_out[5] = \<const0> ;
  assign rxdata_out[4] = \<const0> ;
  assign rxdata_out[3] = \<const0> ;
  assign rxdata_out[2] = \<const0> ;
  assign rxdata_out[1] = \<const0> ;
  assign rxdata_out[0] = \<const0> ;
  assign rxdataextendrsvd_out[7] = \<const0> ;
  assign rxdataextendrsvd_out[6] = \<const0> ;
  assign rxdataextendrsvd_out[5] = \<const0> ;
  assign rxdataextendrsvd_out[4] = \<const0> ;
  assign rxdataextendrsvd_out[3] = \<const0> ;
  assign rxdataextendrsvd_out[2] = \<const0> ;
  assign rxdataextendrsvd_out[1] = \<const0> ;
  assign rxdataextendrsvd_out[0] = \<const0> ;
  assign rxdatavalid_out[1] = \<const0> ;
  assign rxdatavalid_out[0] = \<const0> ;
  assign rxdlysresetdone_out[0] = \<const0> ;
  assign rxelecidle_out[0] = \<const0> ;
  assign rxheader_out[5] = \<const0> ;
  assign rxheader_out[4] = \<const0> ;
  assign rxheader_out[3] = \<const0> ;
  assign rxheader_out[2] = \<const0> ;
  assign rxheader_out[1] = \<const0> ;
  assign rxheader_out[0] = \<const0> ;
  assign rxheadervalid_out[1] = \<const0> ;
  assign rxheadervalid_out[0] = \<const0> ;
  assign rxlfpstresetdet_out[0] = \<const0> ;
  assign rxlfpsu2lpexitdet_out[0] = \<const0> ;
  assign rxlfpsu3wakedet_out[0] = \<const0> ;
  assign rxmonitorout_out[6] = \<const0> ;
  assign rxmonitorout_out[5] = \<const0> ;
  assign rxmonitorout_out[4] = \<const0> ;
  assign rxmonitorout_out[3] = \<const0> ;
  assign rxmonitorout_out[2] = \<const0> ;
  assign rxmonitorout_out[1] = \<const0> ;
  assign rxmonitorout_out[0] = \<const0> ;
  assign rxosintdone_out[0] = \<const0> ;
  assign rxosintstarted_out[0] = \<const0> ;
  assign rxosintstrobedone_out[0] = \<const0> ;
  assign rxosintstrobestarted_out[0] = \<const0> ;
  assign rxoutclkfabric_out[0] = \<const0> ;
  assign rxoutclkpcs_out[0] = \<const0> ;
  assign rxphaligndone_out[0] = \<const0> ;
  assign rxphalignerr_out[0] = \<const0> ;
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxprbslocked_out[0] = \<const0> ;
  assign rxprgdivresetdone_out[0] = \<const0> ;
  assign rxqpisenn_out[0] = \<const0> ;
  assign rxqpisenp_out[0] = \<const0> ;
  assign rxratedone_out[0] = \<const0> ;
  assign rxrecclk0_sel_out[1] = \<const0> ;
  assign rxrecclk0_sel_out[0] = \<const0> ;
  assign rxrecclk0sel_out[0] = \<const0> ;
  assign rxrecclk1_sel_out[1] = \<const0> ;
  assign rxrecclk1_sel_out[0] = \<const0> ;
  assign rxrecclk1sel_out[0] = \<const0> ;
  assign rxrecclkout_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign rxsliderdy_out[0] = \<const0> ;
  assign rxslipdone_out[0] = \<const0> ;
  assign rxslipoutclkrdy_out[0] = \<const0> ;
  assign rxslippmardy_out[0] = \<const0> ;
  assign rxstartofseq_out[1] = \<const0> ;
  assign rxstartofseq_out[0] = \<const0> ;
  assign rxstatus_out[2] = \<const0> ;
  assign rxstatus_out[1] = \<const0> ;
  assign rxstatus_out[0] = \<const0> ;
  assign rxsyncdone_out[0] = \<const0> ;
  assign rxsyncout_out[0] = \<const0> ;
  assign rxvalid_out[0] = \<const0> ;
  assign sdm0finalout_out[0] = \<const0> ;
  assign sdm0testdata_out[0] = \<const0> ;
  assign sdm1finalout_out[0] = \<const0> ;
  assign sdm1testdata_out[0] = \<const0> ;
  assign tcongpo_out[0] = \<const0> ;
  assign tconrsvdout0_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txcomfinish_out[0] = \<const0> ;
  assign txdccdone_out[0] = \<const0> ;
  assign txdlysresetdone_out[0] = \<const0> ;
  assign txoutclkfabric_out[0] = \<const0> ;
  assign txoutclkpcs_out[0] = \<const0> ;
  assign txphaligndone_out[0] = \<const0> ;
  assign txphinitdone_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txqpisenn_out[0] = \<const0> ;
  assign txqpisenp_out[0] = \<const0> ;
  assign txratedone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  assign txsyncdone_out[0] = \<const0> ;
  assign txsyncout_out[0] = \<const0> ;
  assign ubdaddr_out[0] = \<const0> ;
  assign ubden_out[0] = \<const0> ;
  assign ubdi_out[0] = \<const0> ;
  assign ubdwe_out[0] = \<const0> ;
  assign ubmdmtdo_out[0] = \<const0> ;
  assign ubrsvdout_out[0] = \<const0> ;
  assign ubtxuart_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  PCS_PMA_gt_gtwizard_gthe3 \gen_gtwizard_gthe3_top.PCS_PMA_gt_gtwizard_gthe3_inst 
       (.drpclk_in(drpclk_in),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(\^rxbufstatus_out ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(\^rxctrl0_out ),
        .rxctrl1_out(\^rxctrl1_out ),
        .rxctrl2_out(\^rxctrl2_out ),
        .rxctrl3_out(\^rxctrl3_out ),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in[1]),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(\^txbufstatus_out ),
        .txctrl0_in(txctrl0_in[1:0]),
        .txctrl1_in(txctrl1_in[1:0]),
        .txctrl2_in(txctrl2_in[1:0]),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    rxresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]rxresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0
   (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    txresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input [0:0]txresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]txresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(txresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1
   (E,
    gtpowergood_out,
    drpclk_in,
    \FSM_sequential_sm_reset_all_reg[0] ,
    Q,
    \FSM_sequential_sm_reset_all_reg[0]_0 );
  output [0:0]E;
  input [0:0]gtpowergood_out;
  input [0:0]drpclk_in;
  input \FSM_sequential_sm_reset_all_reg[0] ;
  input [2:0]Q;
  input \FSM_sequential_sm_reset_all_reg[0]_0 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_all_reg[0] ;
  wire \FSM_sequential_sm_reset_all_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire [0:0]gtpowergood_out;
  wire gtpowergood_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;

  LUT6 #(
    .INIT(64'hAF0FAF00CFFFCFFF)) 
    \FSM_sequential_sm_reset_all[2]_i_1 
       (.I0(gtpowergood_sync),
        .I1(\FSM_sequential_sm_reset_all_reg[0] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\FSM_sequential_sm_reset_all_reg[0]_0 ),
        .I5(Q[1]),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtpowergood_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtpowergood_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10
   (\FSM_sequential_sm_reset_rx_reg[2] ,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    sm_reset_rx_cdr_to_sat_reg,
    rxcdrlock_out,
    drpclk_in,
    sm_reset_rx_cdr_to_clr_reg,
    Q,
    plllock_rx_sync,
    sm_reset_rx_cdr_to_clr,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    sm_reset_rx_cdr_to_sat);
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output sm_reset_rx_cdr_to_sat_reg;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_cdr_to_clr_reg;
  input [2:0]Q;
  input plllock_rx_sync;
  input sm_reset_rx_cdr_to_clr;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input sm_reset_rx_cdr_to_sat;

  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_n_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;
  wire [0:0]rxcdrlock_out;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_2_n_0;
  wire sm_reset_rx_cdr_to_clr_reg;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_reg;

  LUT6 #(
    .INIT(64'h000A000AC0C000C0)) 
    \FSM_sequential_sm_reset_rx[2]_i_4 
       (.I0(sm_reset_rx_cdr_to_sat_reg),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(plllock_rx_sync),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxcdrlock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(i_in_out_reg_n_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'hE)) 
    rxprogdivreset_out_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .O(sm_reset_rx_cdr_to_sat_reg));
  LUT6 #(
    .INIT(64'hFBFFFFFF0800AAAA)) 
    sm_reset_rx_cdr_to_clr_i_1
       (.I0(sm_reset_rx_cdr_to_clr_i_2_n_0),
        .I1(sm_reset_rx_cdr_to_clr_reg),
        .I2(Q[2]),
        .I3(plllock_rx_sync),
        .I4(Q[0]),
        .I5(sm_reset_rx_cdr_to_clr),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h00EF)) 
    sm_reset_rx_cdr_to_clr_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_n_0),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(sm_reset_rx_cdr_to_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2
   (gtwiz_reset_rx_datapath_dly,
    in0,
    drpclk_in);
  output gtwiz_reset_rx_datapath_dly;
  input in0;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire gtwiz_reset_rx_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3
   (D,
    i_in_out_reg_0,
    in0,
    drpclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    Q,
    gtwiz_reset_rx_datapath_dly,
    \FSM_sequential_sm_reset_rx_reg[0]_0 );
  output [1:0]D;
  output i_in_out_reg_0;
  input in0;
  input [0:0]drpclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input [2:0]Q;
  input gtwiz_reset_rx_datapath_dly;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;

  wire [1:0]D;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_datapath_dly;
  wire gtwiz_reset_rx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFF0088FF00FFFFF0)) 
    \FSM_sequential_sm_reset_rx[0]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(gtwiz_reset_rx_pll_and_datapath_dly),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000FFFF8F8F000F)) 
    \FSM_sequential_sm_reset_rx[1]_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .I2(Q[2]),
        .I3(gtwiz_reset_rx_pll_and_datapath_dly),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000000E)) 
    \FSM_sequential_sm_reset_rx[2]_i_5 
       (.I0(gtwiz_reset_rx_pll_and_datapath_dly),
        .I1(gtwiz_reset_rx_datapath_dly),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .O(i_in_out_reg_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4
   (E,
    in0,
    drpclk_in,
    Q,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    gtwiz_reset_tx_pll_and_datapath_dly,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 );
  output [0:0]E;
  input in0;
  input [0:0]drpclk_in;
  input [0:0]Q;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input gtwiz_reset_tx_pll_and_datapath_dly;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire [0:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1110)) 
    \FSM_sequential_sm_reset_tx[2]_i_1 
       (.I0(Q),
        .I1(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I2(gtwiz_reset_tx_datapath_dly),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .I4(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I5(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5
   (gtwiz_reset_tx_pll_and_datapath_dly,
    D,
    in0,
    drpclk_in,
    Q);
  output gtwiz_reset_tx_pll_and_datapath_dly;
  output [1:0]D;
  input in0;
  input [0:0]drpclk_in;
  input [2:0]Q;

  wire [1:0]D;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h1F1E)) 
    \FSM_sequential_sm_reset_tx[0]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h0FF1)) 
    \FSM_sequential_sm_reset_tx[1]_i_1 
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_pll_and_datapath_dly),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(D[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6
   (\FSM_sequential_sm_reset_rx_reg[0] ,
    \FSM_sequential_sm_reset_rx_reg[2] ,
    E,
    rxpmaresetdone_out,
    drpclk_in,
    sm_reset_rx_timer_clr_reg,
    Q,
    sm_reset_rx_timer_clr_reg_0,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \FSM_sequential_sm_reset_rx_reg[0]_0 ,
    \FSM_sequential_sm_reset_rx_reg[0]_1 ,
    \FSM_sequential_sm_reset_rx_reg[0]_2 ,
    sm_reset_rx_pll_timer_sat,
    sm_reset_rx_timer_sat);
  output \FSM_sequential_sm_reset_rx_reg[0] ;
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output [0:0]E;
  input [0:0]rxpmaresetdone_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_timer_clr_reg;
  input [2:0]Q;
  input sm_reset_rx_timer_clr_reg_0;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  input sm_reset_rx_pll_timer_sat;
  input sm_reset_rx_timer_sat;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_rx[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_2 ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_userclk_rx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxpmaresetdone_out;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_timer_clr_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg;
  wire sm_reset_rx_timer_clr_reg_0;
  wire sm_reset_rx_timer_sat;

  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_sm_reset_rx[2]_i_1 
       (.I0(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .I2(\FSM_sequential_sm_reset_rx_reg[0]_1 ),
        .O(E));
  LUT6 #(
    .INIT(64'h2023202000000000)) 
    \FSM_sequential_sm_reset_rx[2]_i_3 
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_2 ),
        .I4(sm_reset_rx_pll_timer_sat),
        .I5(Q[0]),
        .O(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxpmaresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_rx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFAAF00000800)) 
    rxuserrdy_out_i_1
       (.I0(Q[2]),
        .I1(sm_reset_rx_timer_clr_i_2_n_0),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  LUT6 #(
    .INIT(64'hFCCCEFFE0CCCE00E)) 
    sm_reset_rx_timer_clr_i_1
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(sm_reset_rx_timer_clr_reg),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(sm_reset_rx_timer_clr_reg_0),
        .O(\FSM_sequential_sm_reset_rx_reg[0] ));
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_timer_clr_i_2
       (.I0(sm_reset_rx_timer_clr_reg_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(gtwiz_reset_userclk_rx_active_sync),
        .O(sm_reset_rx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7
   (gtwiz_reset_userclk_tx_active_sync,
    \FSM_sequential_sm_reset_tx_reg[2] ,
    i_in_out_reg_0,
    drpclk_in,
    Q,
    sm_reset_tx_timer_clr_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    sm_reset_tx_timer_clr_reg_0,
    plllock_tx_sync,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 ,
    sm_reset_tx_pll_timer_sat);
  output gtwiz_reset_userclk_tx_active_sync;
  output \FSM_sequential_sm_reset_tx_reg[2] ;
  output i_in_out_reg_0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input sm_reset_tx_timer_clr_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input sm_reset_tx_timer_clr_reg_0;
  input plllock_tx_sync;
  input \FSM_sequential_sm_reset_tx_reg[0] ;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  input sm_reset_tx_pll_timer_sat;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_tx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_userclk_tx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire plllock_tx_sync;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_timer_clr_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg;
  wire sm_reset_tx_timer_clr_reg_0;

  LUT6 #(
    .INIT(64'h000F000088888888)) 
    \FSM_sequential_sm_reset_tx[2]_i_5 
       (.I0(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I3(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .I4(sm_reset_tx_pll_timer_sat),
        .I5(Q[0]),
        .O(i_in_out_reg_0));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_tx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEBEB282B)) 
    sm_reset_tx_timer_clr_i_1
       (.I0(sm_reset_tx_timer_clr_i_2_n_0),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(sm_reset_tx_timer_clr_reg),
        .O(\FSM_sequential_sm_reset_tx_reg[2] ));
  LUT6 #(
    .INIT(64'hA0C0A0C0F0F000F0)) 
    sm_reset_tx_timer_clr_i_2
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(gtwiz_reset_userclk_tx_active_sync),
        .I2(sm_reset_tx_timer_clr_reg_0),
        .I3(Q[0]),
        .I4(plllock_tx_sync),
        .I5(Q[2]),
        .O(sm_reset_tx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8
   (plllock_rx_sync,
    i_in_out_reg_0,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_rx_done_int_reg,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    Q,
    gtwiz_reset_rx_done_int_reg_0);
  output plllock_rx_sync;
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_rx_done_int_reg;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [2:0]Q;
  input gtwiz_reset_rx_done_int_reg_0;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire gtwiz_reset_rx_done_int;
  wire gtwiz_reset_rx_done_int_reg;
  wire gtwiz_reset_rx_done_int_reg_0;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_rx_sync;

  LUT6 #(
    .INIT(64'hAAC0FFFFAAC00000)) 
    gtwiz_reset_rx_done_int_i_1
       (.I0(plllock_rx_sync),
        .I1(gtwiz_reset_rx_done_int_reg),
        .I2(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int),
        .I5(gtwiz_reset_rx_done_int_reg_0),
        .O(i_in_out_reg_0));
  LUT6 #(
    .INIT(64'h4C40000040400000)) 
    gtwiz_reset_rx_done_int_i_2
       (.I0(plllock_rx_sync),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I4(Q[1]),
        .I5(gtwiz_reset_rx_done_int_reg),
        .O(gtwiz_reset_rx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_rx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h88880000F5FF5555)) 
    sm_reset_rx_timer_clr_i_3
       (.I0(Q[1]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_done_int_reg),
        .I5(Q[2]),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9
   (plllock_tx_sync,
    gtwiz_reset_tx_done_int_reg,
    i_in_out_reg_0,
    cplllock_out,
    drpclk_in,
    gtwiz_reset_tx_done_int_reg_0,
    Q,
    sm_reset_tx_timer_sat,
    gtwiz_reset_tx_done_int_reg_1,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    \FSM_sequential_sm_reset_tx_reg[0] );
  output plllock_tx_sync;
  output gtwiz_reset_tx_done_int_reg;
  output i_in_out_reg_0;
  input [0:0]cplllock_out;
  input [0:0]drpclk_in;
  input gtwiz_reset_tx_done_int_reg_0;
  input [2:0]Q;
  input sm_reset_tx_timer_sat;
  input gtwiz_reset_tx_done_int_reg_1;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input \FSM_sequential_sm_reset_tx_reg[0] ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire [2:0]Q;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire gtwiz_reset_tx_done_int;
  wire gtwiz_reset_tx_done_int_i_2_n_0;
  wire gtwiz_reset_tx_done_int_reg;
  wire gtwiz_reset_tx_done_int_reg_0;
  wire gtwiz_reset_tx_done_int_reg_1;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_tx_sync;
  wire sm_reset_tx_timer_sat;

  LUT6 #(
    .INIT(64'h00CFA00000000000)) 
    \FSM_sequential_sm_reset_tx[2]_i_4 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(plllock_tx_sync),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_sm_reset_tx_reg[0] ),
        .O(i_in_out_reg_0));
  LUT3 #(
    .INIT(8'hB8)) 
    gtwiz_reset_tx_done_int_i_1
       (.I0(gtwiz_reset_tx_done_int_i_2_n_0),
        .I1(gtwiz_reset_tx_done_int),
        .I2(gtwiz_reset_tx_done_int_reg_0),
        .O(gtwiz_reset_tx_done_int_reg));
  LUT6 #(
    .INIT(64'h4444444444F44444)) 
    gtwiz_reset_tx_done_int_i_2
       (.I0(Q[0]),
        .I1(plllock_tx_sync),
        .I2(sm_reset_tx_timer_sat),
        .I3(gtwiz_reset_tx_done_int_reg_1),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I5(Q[1]),
        .O(gtwiz_reset_tx_done_int_i_2_n_0));
  LUT6 #(
    .INIT(64'h3000404000004040)) 
    gtwiz_reset_tx_done_int_i_3
       (.I0(plllock_tx_sync),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_sequential_sm_reset_tx_reg[0] ),
        .I4(Q[0]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .O(gtwiz_reset_tx_done_int));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cplllock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_tx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_gthe3_channel" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe3_channel
   (cplllock_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxcdrlock_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxresetdone_out,
    txoutclk_out,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    rst_in0,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe3.txprogdivreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    rxpd_in,
    txctrl2_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]cplllock_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output rst_in0;
  input \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input \gen_gtwizard_gthe3.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe3.txprogdivreset_int ;
  input \gen_gtwizard_gthe3.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]rxpd_in;
  input [1:0]txctrl2_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ;
  wire \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;
  wire xlnx_opt_;
  wire xlnx_opt__1;
  wire xlnx_opt__2;
  wire xlnx_opt__3;

  assign lopt_2 = xlnx_opt_;
  assign lopt_3 = xlnx_opt__1;
  assign lopt_4 = xlnx_opt__2;
  assign lopt_5 = xlnx_opt__3;
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC
       (.CE(lopt),
        .CESYNC(xlnx_opt_),
        .CLK(rxoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__1));
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC_1
       (.CE(lopt),
        .CESYNC(xlnx_opt__2),
        .CLK(txoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__3));
  (* box_type = "PRIMITIVE" *) 
  GTHE3_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(16'hF800),
    .ADAPT_CFG1(16'h0000),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .A_RXOSCALRESET(1'b0),
    .A_RXPROGDIVRESET(1'b0),
    .A_TXPROGDIVRESET(1'b0),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CDR_SWAP_MODE_EN(1'b0),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(15),
    .CLK_COR_MIN_LAT(12),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG0(16'h67F8),
    .CPLL_CFG1(16'hA4AC),
    .CPLL_CFG2(16'h0007),
    .CPLL_CFG3(6'h00),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(4),
    .CPLL_INIT_CFG0(16'h02B2),
    .CPLL_INIT_CFG1(8'h00),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DDI_CTRL(2'b00),
    .DDI_REALIGN_WAIT(15),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DFE_D_X_REL_POS(1'b0),
    .DFE_VCM_COMP_EN(1'b0),
    .DMONITOR_CFG0(10'h000),
    .DMONITOR_CFG1(8'h00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("FALSE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER0(16'h0000),
    .ES_QUALIFIER1(16'h0000),
    .ES_QUALIFIER2(16'h0000),
    .ES_QUALIFIER3(16'h0000),
    .ES_QUALIFIER4(16'h0000),
    .ES_QUAL_MASK0(16'h0000),
    .ES_QUAL_MASK1(16'h0000),
    .ES_QUAL_MASK2(16'h0000),
    .ES_QUAL_MASK3(16'h0000),
    .ES_QUAL_MASK4(16'h0000),
    .ES_SDATA_MASK0(16'h0000),
    .ES_SDATA_MASK1(16'h0000),
    .ES_SDATA_MASK2(16'h0000),
    .ES_SDATA_MASK3(16'h0000),
    .ES_SDATA_MASK4(16'h0000),
    .EVODD_PHI_CFG(11'b00000000000),
    .EYE_SCAN_SWAP_EN(1'b0),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(5'b00000),
    .GM_BIAS_SELECT(1'b0),
    .LOCAL_MASTER(1'b1),
    .OOBDIVCTL(2'b00),
    .OOB_PWRUP(1'b0),
    .PCI3_AUTO_REALIGN("OVR_1K_BLK"),
    .PCI3_PIPE_RX_ELECIDLE(1'b0),
    .PCI3_RX_ASYNC_EBUF_BYPASS(2'b00),
    .PCI3_RX_ELECIDLE_EI2_ENABLE(1'b0),
    .PCI3_RX_ELECIDLE_H2L_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_H2L_DISABLE(3'b000),
    .PCI3_RX_ELECIDLE_HI_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_LP4_DISABLE(1'b0),
    .PCI3_RX_FIFO_DISABLE(1'b0),
    .PCIE_BUFG_DIV_CTRL(16'h1000),
    .PCIE_RXPCS_CFG_GEN3(16'h02A4),
    .PCIE_RXPMA_CFG(16'h000A),
    .PCIE_TXPCS_CFG_GEN3(16'h2CA4),
    .PCIE_TXPMA_CFG(16'h000A),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD0(16'b0000000000000000),
    .PCS_RSVD1(3'b000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PLL_SEL_MODE_GEN12(2'h0),
    .PLL_SEL_MODE_GEN3(2'h3),
    .PMA_RSV1(16'hF000),
    .PROCESS_PAR(3'b010),
    .RATE_SW_USE_DRP(1'b1),
    .RESET_POWERSAVE_DISABLE(1'b0),
    .RXBUFRESET_TIME(5'b00011),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(0),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(0),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG0(16'h0000),
    .RXCDR_CFG0_GEN3(16'h0000),
    .RXCDR_CFG1(16'h0000),
    .RXCDR_CFG1_GEN3(16'h0000),
    .RXCDR_CFG2(16'h0746),
    .RXCDR_CFG2_GEN3(16'h07E6),
    .RXCDR_CFG3(16'h0000),
    .RXCDR_CFG3_GEN3(16'h0000),
    .RXCDR_CFG4(16'h0000),
    .RXCDR_CFG4_GEN3(16'h0000),
    .RXCDR_CFG5(16'h0000),
    .RXCDR_CFG5_GEN3(16'h0000),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG0(16'h4480),
    .RXCDR_LOCK_CFG1(16'h5FFF),
    .RXCDR_LOCK_CFG2(16'h77C3),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXCFOK_CFG0(16'h4000),
    .RXCFOK_CFG1(16'h0065),
    .RXCFOK_CFG2(16'h002E),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDFELPM_KL_CFG0(16'h0000),
    .RXDFELPM_KL_CFG1(16'h0032),
    .RXDFELPM_KL_CFG2(16'h0000),
    .RXDFE_CFG0(16'h0A00),
    .RXDFE_CFG1(16'h0000),
    .RXDFE_GC_CFG0(16'h0000),
    .RXDFE_GC_CFG1(16'h7870),
    .RXDFE_GC_CFG2(16'h0000),
    .RXDFE_H2_CFG0(16'h0000),
    .RXDFE_H2_CFG1(16'h0000),
    .RXDFE_H3_CFG0(16'h4000),
    .RXDFE_H3_CFG1(16'h0000),
    .RXDFE_H4_CFG0(16'h2000),
    .RXDFE_H4_CFG1(16'h0003),
    .RXDFE_H5_CFG0(16'h2000),
    .RXDFE_H5_CFG1(16'h0003),
    .RXDFE_H6_CFG0(16'h2000),
    .RXDFE_H6_CFG1(16'h0000),
    .RXDFE_H7_CFG0(16'h2000),
    .RXDFE_H7_CFG1(16'h0000),
    .RXDFE_H8_CFG0(16'h2000),
    .RXDFE_H8_CFG1(16'h0000),
    .RXDFE_H9_CFG0(16'h2000),
    .RXDFE_H9_CFG1(16'h0000),
    .RXDFE_HA_CFG0(16'h2000),
    .RXDFE_HA_CFG1(16'h0000),
    .RXDFE_HB_CFG0(16'h2000),
    .RXDFE_HB_CFG1(16'h0000),
    .RXDFE_HC_CFG0(16'h0000),
    .RXDFE_HC_CFG1(16'h0000),
    .RXDFE_HD_CFG0(16'h0000),
    .RXDFE_HD_CFG1(16'h0000),
    .RXDFE_HE_CFG0(16'h0000),
    .RXDFE_HE_CFG1(16'h0000),
    .RXDFE_HF_CFG0(16'h0000),
    .RXDFE_HF_CFG1(16'h0000),
    .RXDFE_OS_CFG0(16'h8000),
    .RXDFE_OS_CFG1(16'h0000),
    .RXDFE_UT_CFG0(16'h8000),
    .RXDFE_UT_CFG1(16'h0003),
    .RXDFE_VP_CFG0(16'hAA00),
    .RXDFE_VP_CFG1(16'h0033),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(16'h0030),
    .RXELECIDLE_CFG("Sigcfg_4"),
    .RXGBOX_FIFO_INIT_RD_ADDR(4),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_CFG(16'h0000),
    .RXLPM_GC_CFG(16'h1000),
    .RXLPM_KH_CFG0(16'h0000),
    .RXLPM_KH_CFG1(16'h0002),
    .RXLPM_OS_CFG0(16'h8000),
    .RXLPM_OS_CFG1(16'h0002),
    .RXOOB_CFG(9'b000000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00011),
    .RXPHBEACON_CFG(16'h0000),
    .RXPHDLY_CFG(16'h2020),
    .RXPHSAMP_CFG(16'h2100),
    .RXPHSLIP_CFG(16'h6622),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_CFG0(2'b01),
    .RXPI_CFG1(2'b01),
    .RXPI_CFG2(2'b01),
    .RXPI_CFG3(2'b01),
    .RXPI_CFG4(1'b1),
    .RXPI_CFG5(1'b1),
    .RXPI_CFG6(3'b011),
    .RXPI_LPM(1'b0),
    .RXPI_VREFSEL(1'b0),
    .RXPMACLK_SEL("DATA"),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXPRBS_LINKACQ_CNT(15),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b0),
    .RX_AFE_CM_EN(1'b0),
    .RX_BIAS_CFG0(16'h0AB4),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CAPFF_SARC_ENB(1'b0),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_EN(1'b1),
    .RX_CLK_SLIP_OVRD(5'b00000),
    .RX_CM_BUF_CFG(4'b1010),
    .RX_CM_BUF_PD(1'b0),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(4'b1010),
    .RX_CTLE3_LPF(8'b00000001),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFELPM_CFG0(4'b0110),
    .RX_DFELPM_CFG1(1'b1),
    .RX_DFELPM_KLKH_AGC_STUP_EN(1'b1),
    .RX_DFE_AGC_CFG0(2'b10),
    .RX_DFE_AGC_CFG1(3'b000),
    .RX_DFE_KL_LPM_KH_CFG0(2'b01),
    .RX_DFE_KL_LPM_KH_CFG1(3'b000),
    .RX_DFE_KL_LPM_KL_CFG0(2'b01),
    .RX_DFE_KL_LPM_KL_CFG1(3'b000),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_DIVRESET_TIME(5'b00001),
    .RX_EN_HI_LR(1'b0),
    .RX_EYESCAN_VS_CODE(7'b0000000),
    .RX_EYESCAN_VS_NEG_DIR(1'b0),
    .RX_EYESCAN_VS_RANGE(2'b00),
    .RX_EYESCAN_VS_UT_SIGN(1'b0),
    .RX_FABINT_USRCLK_FLOP(1'b0),
    .RX_INT_DATAWIDTH(0),
    .RX_PMA_POWER_SAVE(1'b0),
    .RX_PROGDIV_CFG(0.000000),
    .RX_SAMPLE_PERIOD(3'b111),
    .RX_SIG_VALID_DLY(11),
    .RX_SUM_DFETAPREP_EN(1'b0),
    .RX_SUM_IREF_TUNE(4'b1100),
    .RX_SUM_RES_CTRL(2'b11),
    .RX_SUM_VCMTUNE(4'b0000),
    .RX_SUM_VCM_OVWR(1'b0),
    .RX_SUM_VREF_TUNE(3'b000),
    .RX_TUNE_AFE_OS(2'b10),
    .RX_WIDEMODE_CDR(1'b0),
    .RX_XCLK_SEL("RXDES"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b1110),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_MODE("FAST"),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL(1'b0),
    .SIM_VERSION(2),
    .TAPDLY_SET_TX(2'h0),
    .TEMPERATUR_PAR(4'b0010),
    .TERM_RCAL_CFG(15'b100001000010000),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV0(8'h00),
    .TST_RSV1(8'h00),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h0009),
    .TXDLY_LCFG(16'h0050),
    .TXDRVBIAS_N(4'b1010),
    .TXDRVBIAS_P(4'b1010),
    .TXFIFO_ADDR_CFG("LOW"),
    .TXGBOX_FIFO_INIT_RD_ADDR(4),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00011),
    .TXPHDLY_CFG0(16'h2020),
    .TXPHDLY_CFG1(16'h0075),
    .TXPH_CFG(16'h0980),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG0(2'b01),
    .TXPI_CFG1(2'b01),
    .TXPI_CFG2(2'b01),
    .TXPI_CFG3(1'b1),
    .TXPI_CFG4(1'b1),
    .TXPI_CFG5(3'b011),
    .TXPI_GRAY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b1),
    .TXPI_LPM(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b001),
    .TXPI_VREFSEL(1'b0),
    .TXPMARESET_TIME(5'b00011),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_EN(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DCD_CFG(6'b000010),
    .TX_DCD_EN(1'b0),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DIVRESET_TIME(5'b00001),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b100),
    .TX_EIDLE_DEASSERT_DELAY(3'b011),
    .TX_EML_PHI_TUNE(1'b0),
    .TX_FABINT_USRCLK_FLOP(1'b0),
    .TX_IDLE_DATA_ZERO(1'b0),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001111),
    .TX_MARGIN_FULL_1(7'b1001110),
    .TX_MARGIN_FULL_2(7'b1001100),
    .TX_MARGIN_FULL_3(7'b1001010),
    .TX_MARGIN_FULL_4(7'b1001000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000101),
    .TX_MARGIN_LOW_2(7'b1000011),
    .TX_MARGIN_LOW_3(7'b1000010),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_MODE_SEL(3'b000),
    .TX_PMADATA_OPT(1'b0),
    .TX_PMA_POWER_SAVE(1'b0),
    .TX_PROGCLK_SEL("CPLL"),
    .TX_PROGDIV_CFG(20.000000),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h0032),
    .TX_RXDETECT_REF(3'b100),
    .TX_SAMPLE_PERIOD(3'b111),
    .TX_SARC_LPBK_ENB(1'b0),
    .TX_XCLK_SEL("TXOUT"),
    .USE_PCS_CLK_PHASE_SEL(1'b0),
    .WB_MODE(2'b00)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST 
       (.BUFGTCE({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_289 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_290 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_291 }),
        .BUFGTCEMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_292 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_293 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_294 }),
        .BUFGTDIV({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_357 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_358 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_359 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_360 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_361 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_362 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_363 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_364 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_365 }),
        .BUFGTRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_295 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_296 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_297 }),
        .BUFGTRSTMASK({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_298 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_299 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_300 }),
        .CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .CPLLFBCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_0 ),
        .CPLLLOCK(cplllock_out),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(\gen_gtwizard_gthe3.cpllpd_ch_int ),
        .CPLLREFCLKLOST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_2 ),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(1'b0),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_258 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_259 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_260 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_261 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_262 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_263 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_264 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_265 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_266 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_267 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_268 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_269 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_270 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_271 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_272 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_273 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_274 }),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(drpclk_in),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_210 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_211 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_212 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_213 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_214 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_215 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_216 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_217 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_218 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_219 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_220 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_221 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_222 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_223 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_224 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_225 }),
        .DRPEN(1'b0),
        .DRPRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_3 ),
        .DRPWE(1'b0),
        .EVODDPHICALDONE(1'b0),
        .EVODDPHICALSTART(1'b0),
        .EVODDPHIDRDEN(1'b0),
        .EVODDPHIDWREN(1'b0),
        .EVODDPHIXRDEN(1'b0),
        .EVODDPHIXWREN(1'b0),
        .EYESCANDATAERROR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_4 ),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTHRXN(gthrxn_in),
        .GTHRXP(gthrxp_in),
        .GTHTXN(gthtxn_out),
        .GTHTXP(gthtxp_out),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTPOWERGOOD(gtpowergood_out),
        .GTREFCLK0(gtrefclk0_in),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_8 ),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(\gen_gtwizard_gthe3.gtrxreset_int ),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(\gen_gtwizard_gthe3.gttxreset_int ),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .LPBKRXTXSEREN(1'b0),
        .LPBKTXRXSEREN(1'b0),
        .PCIEEQRXEQADAPTDONE(1'b0),
        .PCIERATEGEN3(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_9 ),
        .PCIERATEIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_10 ),
        .PCIERATEQPLLPD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_275 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_276 }),
        .PCIERATEQPLLRESET({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_277 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_278 }),
        .PCIERSTIDLE(1'b0),
        .PCIERSTTXSYNCSTART(1'b0),
        .PCIESYNCTXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_11 ),
        .PCIEUSERGEN3RDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_12 ),
        .PCIEUSERPHYSTATUSRST(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_13 ),
        .PCIEUSERRATEDONE(1'b0),
        .PCIEUSERRATESTART(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_14 ),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_70 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_71 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_72 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_73 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_74 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_75 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_76 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_77 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_78 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_79 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_80 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_81 }),
        .PHYSTATUS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_15 ),
        .PINRSRVDAS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_325 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_326 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_327 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_328 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_329 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_330 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_331 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_332 }),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLL0CLK(1'b0),
        .QPLL0REFCLK(1'b0),
        .QPLL1CLK(1'b0),
        .QPLL1REFCLK(1'b0),
        .RESETEXCEPTION(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_16 ),
        .RESETOVRD(1'b0),
        .RSTCLKENTX(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({rxbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_302 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_303 }),
        .RXBYTEISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_17 ),
        .RXBYTEREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_18 ),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(rxcdrlock_out),
        .RXCDROVRDEN(1'b0),
        .RXCDRPHDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_20 ),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_21 ),
        .RXCHANISALIGNED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_22 ),
        .RXCHANREALIGN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_23 ),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_307 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_308 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_309 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_310 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_311 }),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(rxclkcorcnt_out),
        .RXCOMINITDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_24 ),
        .RXCOMMADET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_25 ),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_26 ),
        .RXCOMWAKEDET(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_27 ),
        .RXCTRL0({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_226 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_227 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_228 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_229 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_230 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_231 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_232 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_233 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_234 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_235 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_236 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_237 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_238 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_239 ,rxctrl0_out}),
        .RXCTRL1({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_242 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_243 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_244 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_245 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_246 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_247 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_248 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_249 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_250 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_251 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_252 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_253 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_254 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_255 ,rxctrl1_out}),
        .RXCTRL2({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_333 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_334 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_335 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_336 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_337 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_338 ,rxctrl2_out}),
        .RXCTRL3({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_341 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_342 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_343 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_344 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_345 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_346 ,rxctrl3_out}),
        .RXDATA({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_82 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_83 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_84 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_85 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_86 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_87 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_88 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_89 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_90 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_91 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_92 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_93 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_94 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_95 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_96 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_97 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_98 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_99 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_100 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_101 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_102 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_103 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_104 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_105 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_106 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_107 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_108 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_109 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_110 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_111 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_112 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_113 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_114 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_115 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_116 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_117 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_118 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_119 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_120 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_121 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_122 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_123 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_124 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_125 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_126 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_127 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_128 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_129 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_130 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_131 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_132 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_133 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_134 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_135 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_136 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_137 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_138 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_139 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_140 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_141 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_142 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_143 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_144 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_145 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_146 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_147 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_148 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_149 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_150 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_151 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_152 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_153 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_154 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_155 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_156 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_157 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_158 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_159 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_160 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_161 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_162 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_163 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_164 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_165 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_166 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_167 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_168 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_169 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_170 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_171 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_172 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_173 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_174 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_175 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_176 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_177 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_178 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_179 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_180 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_181 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_182 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_183 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_184 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_185 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_186 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_187 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_188 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_189 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_190 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_191 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_192 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_193 ,gtwiz_userdata_rx_out}),
        .RXDATAEXTENDRSVD({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_349 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_350 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_351 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_352 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_353 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_354 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_355 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_356 }),
        .RXDATAVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_281 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_282 }),
        .RXDFEAGCCTRL({1'b0,1'b1}),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP10HOLD(1'b0),
        .RXDFETAP10OVRDEN(1'b0),
        .RXDFETAP11HOLD(1'b0),
        .RXDFETAP11OVRDEN(1'b0),
        .RXDFETAP12HOLD(1'b0),
        .RXDFETAP12OVRDEN(1'b0),
        .RXDFETAP13HOLD(1'b0),
        .RXDFETAP13OVRDEN(1'b0),
        .RXDFETAP14HOLD(1'b0),
        .RXDFETAP14OVRDEN(1'b0),
        .RXDFETAP15HOLD(1'b0),
        .RXDFETAP15OVRDEN(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFETAP6HOLD(1'b0),
        .RXDFETAP6OVRDEN(1'b0),
        .RXDFETAP7HOLD(1'b0),
        .RXDFETAP7OVRDEN(1'b0),
        .RXDFETAP8HOLD(1'b0),
        .RXDFETAP8OVRDEN(1'b0),
        .RXDFETAP9HOLD(1'b0),
        .RXDFETAP9OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_28 ),
        .RXELECIDLE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_29 ),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_312 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_313 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_314 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_315 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_316 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_317 }),
        .RXHEADERVALID({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_283 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_284 }),
        .RXLATCLK(1'b0),
        .RXLPMEN(1'b1),
        .RXLPMGCHOLD(1'b0),
        .RXLPMGCOVRDEN(1'b0),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXLPMOSHOLD(1'b0),
        .RXLPMOSOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(rxmcommaalignen_in),
        .RXMONITOROUT({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_318 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_319 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_320 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_321 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_322 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_323 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_324 }),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTCFG({1'b1,1'b1,1'b0,1'b1}),
        .RXOSINTDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_30 ),
        .RXOSINTEN(1'b1),
        .RXOSINTHOLD(1'b0),
        .RXOSINTOVRDEN(1'b0),
        .RXOSINTSTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_31 ),
        .RXOSINTSTROBE(1'b0),
        .RXOSINTSTROBEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_32 ),
        .RXOSINTSTROBESTARTED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_33 ),
        .RXOSINTTESTOVRDEN(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk_out),
        .RXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_35 ),
        .RXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_36 ),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(rxmcommaalignen_in),
        .RXPCSRESET(1'b0),
        .RXPD({rxpd_in,rxpd_in}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_37 ),
        .RXPHALIGNEN(1'b0),
        .RXPHALIGNERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_38 ),
        .RXPHDLYPD(1'b1),
        .RXPHDLYRESET(1'b0),
        .RXPHOVRDEN(1'b0),
        .RXPLLCLKSEL({1'b0,1'b0}),
        .RXPMARESET(1'b0),
        .RXPMARESETDONE(rxpmaresetdone_out),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_40 ),
        .RXPRBSLOCKED(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_41 ),
        .RXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .RXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_42 ),
        .RXPROGDIVRESET(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .RXQPIEN(1'b0),
        .RXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_43 ),
        .RXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_44 ),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_45 ),
        .RXRATEMODE(1'b0),
        .RXRECCLKOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_46 ),
        .RXRESETDONE(rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSLIDERDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_48 ),
        .RXSLIPDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_49 ),
        .RXSLIPOUTCLK(1'b0),
        .RXSLIPOUTCLKRDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_50 ),
        .RXSLIPPMA(1'b0),
        .RXSLIPPMARDY(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_51 ),
        .RXSTARTOFSEQ({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_285 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_286 }),
        .RXSTATUS({\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_304 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_305 ,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_306 }),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_52 ),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_53 ),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .RXUSRCLK(rxusrclk_in),
        .RXUSRCLK2(rxusrclk_in),
        .RXVALID(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_54 ),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b0,1'b0,1'b0}),
        .TXBUFSTATUS({txbufstatus_out,\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_288 }),
        .TXCOMFINISH(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_55 ),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXCTRL0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in}),
        .TXCTRL1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in}),
        .TXCTRL2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in}),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtwiz_userdata_tx_in}),
        .TXDATAEXTENDRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_56 ),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(txelecidle_in),
        .TXHEADER({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXLATCLK(1'b0),
        .TXMAINCURSOR({1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk_out),
        .TXOUTCLKFABRIC(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_58 ),
        .TXOUTCLKPCS(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_59 ),
        .TXOUTCLKSEL({1'b1,1'b0,1'b1}),
        .TXPCSRESET(1'b0),
        .TXPD({txelecidle_in,txelecidle_in}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_60 ),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b1),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_61 ),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b0),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPLLCLKSEL({1'b0,1'b0}),
        .TXPMARESET(1'b0),
        .TXPMARESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_62 ),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXPRGDIVRESETDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_63 ),
        .TXPROGDIVRESET(\gen_gtwizard_gthe3.txprogdivreset_int ),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_64 ),
        .TXQPISENP(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_65 ),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_66 ),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_68 ),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(\gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_n_69 ),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(\gen_gtwizard_gthe3.txuserrdy_int ),
        .TXUSRCLK(rxusrclk_in),
        .TXUSRCLK2(rxusrclk_in));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_meta_i_1__2
       (.I0(cplllock_out),
        .O(rst_in0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_gtwiz_reset" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_gtwiz_reset
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    \gen_gtwizard_gthe3.gttxreset_int ,
    \gen_gtwizard_gthe3.txuserrdy_int ,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    \gen_gtwizard_gthe3.rxuserrdy_int ,
    \gen_gtwizard_gthe3.cpllpd_ch_int ,
    gtpowergood_out,
    cplllock_out,
    rxpmaresetdone_out,
    rxcdrlock_out,
    drpclk_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    rst_in0,
    rxusrclk_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    gtwiz_reset_rx_datapath_in,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync );
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output \gen_gtwizard_gthe3.gttxreset_int ;
  output \gen_gtwizard_gthe3.txuserrdy_int ;
  output \gen_gtwizard_gthe3.rxprogdivreset_int ;
  output \gen_gtwizard_gthe3.gtrxreset_int ;
  output \gen_gtwizard_gthe3.rxuserrdy_int ;
  output \gen_gtwizard_gthe3.cpllpd_ch_int ;
  input [0:0]gtpowergood_out;
  input [0:0]cplllock_out;
  input [0:0]rxpmaresetdone_out;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in0;
  input [0:0]rxusrclk_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;

  wire \FSM_sequential_sm_reset_all[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_all[2]_i_4_n_0 ;
  wire \FSM_sequential_sm_reset_rx[1]_i_2_n_0 ;
  wire \FSM_sequential_sm_reset_rx[2]_i_6_n_0 ;
  wire \FSM_sequential_sm_reset_tx[2]_i_3_n_0 ;
  wire bit_synchronizer_gtpowergood_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2;
  wire bit_synchronizer_plllock_rx_inst_n_1;
  wire bit_synchronizer_plllock_rx_inst_n_2;
  wire bit_synchronizer_plllock_tx_inst_n_1;
  wire bit_synchronizer_plllock_tx_inst_n_2;
  wire bit_synchronizer_rxcdrlock_inst_n_0;
  wire bit_synchronizer_rxcdrlock_inst_n_1;
  wire bit_synchronizer_rxcdrlock_inst_n_2;
  wire [0:0]cplllock_out;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.cpllpd_ch_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe3.rxuserrdy_int ;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire [0:0]gtpowergood_out;
  wire gttxreset_out_i_3_n_0;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_rx_datapath_dly;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_datapath_sync;
  wire gtwiz_reset_rx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_sync;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_datapath_sync;
  wire gtwiz_reset_tx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_sync;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire p_0_in;
  wire [9:0]p_0_in__0;
  wire [9:0]p_0_in__1;
  wire [2:0]p_1_in;
  wire plllock_rx_sync;
  wire plllock_tx_sync;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_3;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_2;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_3;
  wire rst_in0;
  wire [0:0]rxcdrlock_out;
  wire [0:0]rxpmaresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [2:0]sm_reset_all;
  wire [2:0]sm_reset_all__0;
  wire sm_reset_all_timer_clr_i_1_n_0;
  wire sm_reset_all_timer_clr_i_2_n_0;
  wire sm_reset_all_timer_clr_reg_n_0;
  wire [2:0]sm_reset_all_timer_ctr;
  wire sm_reset_all_timer_ctr0_n_0;
  wire \sm_reset_all_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_all_timer_sat;
  wire sm_reset_all_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_rx;
  wire [2:0]sm_reset_rx__0;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr_i_3_n_0;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ;
  wire [25:0]sm_reset_rx_cdr_to_ctr_reg;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_i_1_n_0;
  wire sm_reset_rx_cdr_to_sat_i_2_n_0;
  wire sm_reset_rx_cdr_to_sat_i_3_n_0;
  wire sm_reset_rx_cdr_to_sat_i_4_n_0;
  wire sm_reset_rx_cdr_to_sat_i_5_n_0;
  wire sm_reset_rx_cdr_to_sat_i_6_n_0;
  wire sm_reset_rx_pll_timer_clr_i_1_n_0;
  wire sm_reset_rx_pll_timer_clr_reg_n_0;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_rx_pll_timer_ctr_reg;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_pll_timer_sat_i_1_n_0;
  wire sm_reset_rx_pll_timer_sat_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_rx_timer_ctr;
  wire sm_reset_rx_timer_ctr0_n_0;
  wire \sm_reset_rx_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_rx_timer_sat;
  wire sm_reset_rx_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_tx;
  wire [2:0]sm_reset_tx__0;
  wire sm_reset_tx_pll_timer_clr_i_1_n_0;
  wire sm_reset_tx_pll_timer_clr_reg_n_0;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ;
  wire [9:0]sm_reset_tx_pll_timer_ctr_reg;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_pll_timer_sat_i_1_n_0;
  wire sm_reset_tx_pll_timer_sat_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_tx_timer_ctr;
  wire sm_reset_tx_timer_sat;
  wire sm_reset_tx_timer_sat_i_1_n_0;
  wire txuserrdy_out_i_3_n_0;
  wire [7:1]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED ;
  wire [7:2]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00FFF70000FFFFFF)) 
    \FSM_sequential_sm_reset_all[0]_i_1 
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .I3(sm_reset_all[2]),
        .I4(sm_reset_all[1]),
        .I5(sm_reset_all[0]),
        .O(sm_reset_all__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h34)) 
    \FSM_sequential_sm_reset_all[1]_i_1 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[0]),
        .O(sm_reset_all__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h4A)) 
    \FSM_sequential_sm_reset_all[2]_i_2 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .O(sm_reset_all__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_sm_reset_all[2]_i_3 
       (.I0(sm_reset_all_timer_sat),
        .I1(gtwiz_reset_rx_done_int_reg_n_0),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_all[2]_i_4 
       (.I0(sm_reset_all_timer_clr_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[0]),
        .Q(sm_reset_all[0]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[1]),
        .Q(sm_reset_all[1]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[2]),
        .Q(sm_reset_all[2]),
        .R(gtwiz_reset_all_sync));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_sm_reset_rx[1]_i_2 
       (.I0(sm_reset_rx_timer_sat),
        .I1(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDFD8888DDDD8888)) 
    \FSM_sequential_sm_reset_rx[2]_i_2 
       (.I0(sm_reset_rx[1]),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx_timer_sat),
        .I3(sm_reset_rx_timer_clr_reg_n_0),
        .I4(sm_reset_rx[2]),
        .I5(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .O(sm_reset_rx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h00004000)) 
    \FSM_sequential_sm_reset_rx[2]_i_6 
       (.I0(sm_reset_rx[0]),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[0]),
        .Q(sm_reset_rx[0]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[1]),
        .Q(sm_reset_rx[1]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .D(sm_reset_rx__0[2]),
        .Q(sm_reset_rx[2]),
        .R(gtwiz_reset_rx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \FSM_sequential_sm_reset_tx[2]_i_2 
       (.I0(sm_reset_tx[0]),
        .I1(sm_reset_tx[1]),
        .I2(sm_reset_tx[2]),
        .O(sm_reset_tx__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_sm_reset_tx[2]_i_3 
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .O(\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[0]),
        .Q(sm_reset_tx[0]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[1]),
        .Q(sm_reset_tx[1]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .D(sm_reset_tx__0[2]),
        .Q(sm_reset_tx[2]),
        .R(gtwiz_reset_tx_any_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1 bit_synchronizer_gtpowergood_inst
       (.E(bit_synchronizer_gtpowergood_inst_n_0),
        .\FSM_sequential_sm_reset_all_reg[0] (\FSM_sequential_sm_reset_all[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_all_reg[0]_0 (\FSM_sequential_sm_reset_all[2]_i_4_n_0 ),
        .Q(sm_reset_all),
        .drpclk_in(drpclk_in),
        .gtpowergood_out(gtpowergood_out));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2 bit_synchronizer_gtwiz_reset_rx_datapath_dly_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .in0(gtwiz_reset_rx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3 bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst
       (.D(sm_reset_rx__0[1:0]),
        .\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (\FSM_sequential_sm_reset_rx[2]_i_6_n_0 ),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4 bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst
       (.E(bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0] (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (bit_synchronizer_plllock_tx_inst_n_2),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .Q(sm_reset_tx[0]),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5 bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst
       (.D(sm_reset_tx__0[1:0]),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_pll_and_datapath_dly(gtwiz_reset_tx_pll_and_datapath_dly),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6 bit_synchronizer_gtwiz_reset_userclk_rx_active_inst
       (.E(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[0]_1 (bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0]_2 (sm_reset_rx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.rxuserrdy_int (\gen_gtwizard_gthe3.rxuserrdy_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .rxpmaresetdone_out(rxpmaresetdone_out),
        .sm_reset_rx_pll_timer_sat(sm_reset_rx_pll_timer_sat),
        .sm_reset_rx_timer_clr_reg(bit_synchronizer_plllock_rx_inst_n_2),
        .sm_reset_rx_timer_clr_reg_0(sm_reset_rx_timer_clr_reg_n_0),
        .sm_reset_rx_timer_sat(sm_reset_rx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7 bit_synchronizer_gtwiz_reset_userclk_tx_active_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (txuserrdy_out_i_3_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (sm_reset_tx_pll_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_tx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .i_in_out_reg_0(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_pll_timer_sat(sm_reset_tx_pll_timer_sat),
        .sm_reset_tx_timer_clr_reg(sm_reset_tx_timer_clr_reg_n_0),
        .sm_reset_tx_timer_clr_reg_0(gttxreset_out_i_3_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8 bit_synchronizer_plllock_rx_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_plllock_rx_inst_n_2),
        .Q(sm_reset_rx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .gtwiz_reset_rx_done_int_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_done_int_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_rx_inst_n_1),
        .plllock_rx_sync(plllock_rx_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9 bit_synchronizer_plllock_tx_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (gttxreset_out_i_3_n_0),
        .Q(sm_reset_tx),
        .cplllock_out(cplllock_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .gtwiz_reset_tx_done_int_reg(bit_synchronizer_plllock_tx_inst_n_1),
        .gtwiz_reset_tx_done_int_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .gtwiz_reset_tx_done_int_reg_1(sm_reset_tx_timer_clr_reg_n_0),
        .i_in_out_reg_0(bit_synchronizer_plllock_tx_inst_n_2),
        .plllock_tx_sync(plllock_tx_sync),
        .sm_reset_tx_timer_sat(sm_reset_tx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10 bit_synchronizer_rxcdrlock_inst
       (.\FSM_sequential_sm_reset_rx_reg[0] (\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rxcdrlock_out(rxcdrlock_out),
        .sm_reset_rx_cdr_to_clr(sm_reset_rx_cdr_to_clr),
        .sm_reset_rx_cdr_to_clr_reg(sm_reset_rx_cdr_to_clr_i_3_n_0),
        .sm_reset_rx_cdr_to_sat(sm_reset_rx_cdr_to_sat),
        .sm_reset_rx_cdr_to_sat_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  LUT2 #(
    .INIT(4'hE)) 
    \gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST_i_1 
       (.I0(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .I1(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\gen_gtwizard_gthe3.cpllpd_ch_int ));
  FDRE #(
    .INIT(1'b1)) 
    gtrxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.gtrxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h2)) 
    gttxreset_out_i_3
       (.I0(sm_reset_tx_timer_sat),
        .I1(sm_reset_tx_timer_clr_reg_n_0),
        .O(gttxreset_out_i_3_n_0));
  FDRE #(
    .INIT(1'b1)) 
    gttxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.gttxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hF740)) 
    gtwiz_reset_rx_datapath_int_i_1
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_rx_inst_n_1),
        .Q(gtwiz_reset_rx_done_int_reg_n_0),
        .R(gtwiz_reset_rx_any_sync));
  LUT4 #(
    .INIT(16'hF704)) 
    gtwiz_reset_rx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[2]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_tx_inst_n_1),
        .Q(gtwiz_reset_tx_done_int_reg_n_0),
        .R(gtwiz_reset_tx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hFB02)) 
    gtwiz_reset_tx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    pllreset_rx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    pllreset_tx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .R(1'b0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer reset_synchronizer_gtwiz_reset_all_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_all_sync(gtwiz_reset_all_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11 reset_synchronizer_gtwiz_reset_rx_any_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[1]_0 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[1]_1 (reset_synchronizer_gtwiz_reset_rx_any_inst_n_3),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .\gen_gtwizard_gthe3.gtrxreset_int (\gen_gtwizard_gthe3.gtrxreset_int ),
        .\gen_gtwizard_gthe3.rxprogdivreset_int (\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .gtrxreset_out_reg(\FSM_sequential_sm_reset_rx[1]_i_2_n_0 ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .plllock_rx_sync(plllock_rx_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0),
        .rst_in_out_reg_1(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .rxprogdivreset_out_reg(bit_synchronizer_rxcdrlock_inst_n_2));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12 reset_synchronizer_gtwiz_reset_rx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .in0(gtwiz_reset_rx_datapath_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13 reset_synchronizer_gtwiz_reset_rx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14 reset_synchronizer_gtwiz_reset_tx_any_inst
       (.\FSM_sequential_sm_reset_tx_reg[0] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .\FSM_sequential_sm_reset_tx_reg[1] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .\FSM_sequential_sm_reset_tx_reg[1]_0 (reset_synchronizer_gtwiz_reset_tx_any_inst_n_2),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int (\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .\gen_gtwizard_gthe3.gttxreset_int (\gen_gtwizard_gthe3.gttxreset_int ),
        .\gen_gtwizard_gthe3.txuserrdy_int (\gen_gtwizard_gthe3.txuserrdy_int ),
        .gttxreset_out_reg(gttxreset_out_i_3_n_0),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_userclk_tx_active_sync(gtwiz_reset_userclk_tx_active_sync),
        .plllock_tx_sync(plllock_tx_sync),
        .rst_in_out_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .txuserrdy_out_reg(txuserrdy_out_i_3_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15 reset_synchronizer_gtwiz_reset_tx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .in0(gtwiz_reset_tx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16 reset_synchronizer_gtwiz_reset_tx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer reset_synchronizer_rx_done_inst
       (.gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17 reset_synchronizer_tx_done_inst
       (.gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18 reset_synchronizer_txprogdivreset_inst
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe3.txprogdivreset_int (\gen_gtwizard_gthe3.txprogdivreset_int ),
        .rst_in0(rst_in0));
  FDRE #(
    .INIT(1'b1)) 
    rxprogdivreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_2),
        .Q(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(\gen_gtwizard_gthe3.rxuserrdy_int ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEFFA200A)) 
    sm_reset_all_timer_clr_i_1
       (.I0(sm_reset_all_timer_clr_i_2_n_0),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(sm_reset_all[0]),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_clr_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000B0003333BB33)) 
    sm_reset_all_timer_clr_i_2
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all[2]),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .I5(sm_reset_all[1]),
        .O(sm_reset_all_timer_clr_i_2_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_all_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_clr_i_1_n_0),
        .Q(sm_reset_all_timer_clr_reg_n_0),
        .S(gtwiz_reset_all_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_all_timer_ctr0
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .O(sm_reset_all_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_all_timer_ctr[0]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .O(\sm_reset_all_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_all_timer_ctr[1]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .O(\sm_reset_all_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_all_timer_ctr[2]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .I2(sm_reset_all_timer_ctr[2]),
        .O(\sm_reset_all_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[0]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[1]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_all_timer_ctr0_n_0),
        .D(\sm_reset_all_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[2]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_all_timer_sat_i_1
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_all_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_sat_i_1_n_0),
        .Q(sm_reset_all_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h40)) 
    sm_reset_rx_cdr_to_clr_i_3
       (.I0(sm_reset_rx_timer_clr_reg_n_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(sm_reset_rx[1]),
        .O(sm_reset_rx_cdr_to_clr_i_3_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_cdr_to_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_rxcdrlock_inst_n_0),
        .Q(sm_reset_rx_cdr_to_clr),
        .S(gtwiz_reset_rx_any_sync));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_1 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[1]),
        .I2(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ),
        .I3(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ),
        .I4(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ),
        .I5(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_3 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[18]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[14]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_4 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[24]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[20]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7FFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_5 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[12]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[9]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_6 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[7]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[2]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_7 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[0]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 }),
        .S({sm_reset_rx_cdr_to_ctr_reg[7:1],\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[10] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[10]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[11] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[11]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[12] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[12]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[13] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[13]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[14] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[14]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[15] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[15]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[16] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[16]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[16]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[17] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[17]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[18] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[18]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[19] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[19]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[1]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[20] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[20]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[21] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[21]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[22] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[22]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[23] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[23]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[24] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[24]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[24]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED [7:1],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED [7:2],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,sm_reset_rx_cdr_to_ctr_reg[25:24]}));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[25] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[25]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[2]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[3]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[4]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[5]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[6]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[7]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[8]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[8]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[9]),
        .R(sm_reset_rx_cdr_to_clr));
  LUT3 #(
    .INIT(8'h0E)) 
    sm_reset_rx_cdr_to_sat_i_1
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(sm_reset_rx_cdr_to_sat_i_2_n_0),
        .I2(sm_reset_rx_cdr_to_clr),
        .O(sm_reset_rx_cdr_to_sat_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sm_reset_rx_cdr_to_sat_i_2
       (.I0(sm_reset_rx_cdr_to_sat_i_3_n_0),
        .I1(sm_reset_rx_cdr_to_sat_i_4_n_0),
        .I2(sm_reset_rx_cdr_to_sat_i_5_n_0),
        .I3(sm_reset_rx_cdr_to_sat_i_6_n_0),
        .I4(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[1]),
        .O(sm_reset_rx_cdr_to_sat_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    sm_reset_rx_cdr_to_sat_i_3
       (.I0(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[5]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[2]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[7]),
        .O(sm_reset_rx_cdr_to_sat_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    sm_reset_rx_cdr_to_sat_i_4
       (.I0(sm_reset_rx_cdr_to_ctr_reg[22]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[20]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[24]),
        .O(sm_reset_rx_cdr_to_sat_i_4_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    sm_reset_rx_cdr_to_sat_i_5
       (.I0(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[14]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[18]),
        .O(sm_reset_rx_cdr_to_sat_i_5_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    sm_reset_rx_cdr_to_sat_i_6
       (.I0(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[9]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[12]),
        .O(sm_reset_rx_cdr_to_sat_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_cdr_to_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_cdr_to_sat_i_1_n_0),
        .Q(sm_reset_rx_cdr_to_sat),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF3000B)) 
    sm_reset_rx_pll_timer_clr_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx[2]),
        .I4(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_rx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_rx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(p_0_in__1[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_rx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_rx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_rx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .O(p_0_in__1[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(p_0_in__1[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[0]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[0]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[1]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[1]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[2]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[2]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[3]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[3]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[4]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[4]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[5]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[5]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[6]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[6]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[7]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[7]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[8]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[8]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__1[9]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[9]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_rx_pll_timer_sat_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_rx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_rx_pll_timer_sat_i_2
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(sm_reset_rx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .Q(sm_reset_rx_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_rx_timer_ctr0
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .O(sm_reset_rx_timer_ctr0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .O(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .O(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .I2(sm_reset_rx_timer_ctr[2]),
        .O(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[0]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[1]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sm_reset_rx_timer_ctr0_n_0),
        .D(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[2]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_rx_timer_sat_i_1
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(sm_reset_rx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hEFEF1101)) 
    sm_reset_tx_pll_timer_clr_i_1
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx[0]),
        .I3(sm_reset_tx_pll_timer_sat),
        .I4(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_tx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_tx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(p_0_in__0[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \sm_reset_tx_pll_timer_ctr[6]_i_1 
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \sm_reset_tx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \sm_reset_tx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .O(p_0_in__0[8]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I2(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(p_0_in__0[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[0]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[0]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[1]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[1]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[2]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[2]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[3]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[3]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[4]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[4]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[5]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[5]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[6]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[6]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[7]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[7]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[8]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[8]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_tx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__0[9]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[9]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAAB)) 
    sm_reset_tx_pll_timer_sat_i_1
       (.I0(sm_reset_tx_pll_timer_sat),
        .I1(sm_reset_tx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I4(sm_reset_tx_pll_timer_sat_i_2_n_0),
        .I5(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_sat_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sm_reset_tx_pll_timer_sat_i_2
       (.I0(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .I1(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(sm_reset_tx_pll_timer_sat_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    sm_reset_tx_timer_ctr0
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .O(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .I2(sm_reset_tx_timer_ctr[2]),
        .O(p_1_in[2]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[0]),
        .Q(sm_reset_tx_timer_ctr[0]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[1]),
        .Q(sm_reset_tx_timer_ctr[1]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(p_1_in[2]),
        .Q(sm_reset_tx_timer_ctr[2]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_tx_timer_sat_i_1
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .I3(sm_reset_tx_timer_sat),
        .I4(sm_reset_tx_timer_clr_reg_n_0),
        .O(sm_reset_tx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    txuserrdy_out_i_3
       (.I0(sm_reset_tx[1]),
        .I1(sm_reset_tx[2]),
        .I2(sm_reset_tx_timer_clr_reg_n_0),
        .I3(sm_reset_tx_timer_sat),
        .O(txuserrdy_out_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    txuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_3),
        .Q(\gen_gtwizard_gthe3.txuserrdy_int ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer
   (gtwiz_reset_rx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_rx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_rx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17
   (gtwiz_reset_tx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_tx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_tx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1__0_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1__0
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_tx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer
   (gtwiz_reset_all_sync,
    drpclk_in,
    gtwiz_reset_all_in);
  output gtwiz_reset_all_sync;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_all_in),
        .Q(gtwiz_reset_all_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11
   (gtwiz_reset_rx_any_sync,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[1]_0 ,
    \FSM_sequential_sm_reset_rx_reg[1]_1 ,
    drpclk_in,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ,
    rxprogdivreset_out_reg,
    \gen_gtwizard_gthe3.rxprogdivreset_int ,
    plllock_rx_sync,
    gtrxreset_out_reg,
    \gen_gtwizard_gthe3.gtrxreset_int ,
    rst_in_out_reg_0,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_1);
  output gtwiz_reset_rx_any_sync;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  input rxprogdivreset_out_reg;
  input \gen_gtwizard_gthe3.rxprogdivreset_int ;
  input plllock_rx_sync;
  input gtrxreset_out_reg;
  input \gen_gtwizard_gthe3.gtrxreset_int ;
  input rst_in_out_reg_0;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_1;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_1 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe3.gtrxreset_int ;
  wire \gen_gtwizard_gthe3.rxprogdivreset_int ;
  wire gtrxreset_out_i_2_n_0;
  wire gtrxreset_out_reg;
  wire gtwiz_reset_rx_any;
  wire gtwiz_reset_rx_any_sync;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire plllock_rx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  wire rst_in_out_reg_1;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire rxprogdivreset_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gtrxreset_out_i_1
       (.I0(Q[1]),
        .I1(gtrxreset_out_i_2_n_0),
        .I2(plllock_rx_sync),
        .I3(Q[0]),
        .I4(gtrxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gtrxreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h1)) 
    gtrxreset_out_i_2
       (.I0(gtwiz_reset_rx_any_sync),
        .I1(Q[2]),
        .O(gtrxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_rx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_rx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  LUT3 #(
    .INIT(8'hFE)) 
    rst_in_meta_i_1
       (.I0(rst_in_out_reg_0),
        .I1(gtwiz_reset_rx_datapath_in),
        .I2(rst_in_out_reg_1),
        .O(gtwiz_reset_rx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_rx_any),
        .Q(gtwiz_reset_rx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hFFFBFFFF00120012)) 
    rxprogdivreset_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_rx_any_sync),
        .I4(rxprogdivreset_out_reg),
        .I5(\gen_gtwizard_gthe3.rxprogdivreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_0 ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12
   (in0,
    drpclk_in,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_0;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire in0;
  wire rst_in0_0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__0
       (.I0(gtwiz_reset_rx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(rst_in0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14
   (gtwiz_reset_tx_any_sync,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    \FSM_sequential_sm_reset_tx_reg[1]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    drpclk_in,
    gtwiz_reset_tx_datapath_in,
    rst_in_out_reg_0,
    Q,
    \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ,
    plllock_tx_sync,
    gttxreset_out_reg,
    \gen_gtwizard_gthe3.gttxreset_int ,
    txuserrdy_out_reg,
    gtwiz_reset_userclk_tx_active_sync,
    \gen_gtwizard_gthe3.txuserrdy_int );
  output gtwiz_reset_tx_any_sync;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  output \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  output \FSM_sequential_sm_reset_tx_reg[0] ;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in_out_reg_0;
  input [2:0]Q;
  input \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  input plllock_tx_sync;
  input gttxreset_out_reg;
  input \gen_gtwizard_gthe3.gttxreset_int ;
  input txuserrdy_out_reg;
  input gtwiz_reset_userclk_tx_active_sync;
  input \gen_gtwizard_gthe3.txuserrdy_int ;

  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire \FSM_sequential_sm_reset_tx_reg[1]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe3.gttxreset_int ;
  wire \gen_gtwizard_gthe3.txuserrdy_int ;
  wire gttxreset_out_i_2_n_0;
  wire gttxreset_out_reg;
  wire gtwiz_reset_tx_any;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_userclk_tx_active_sync;
  wire plllock_tx_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire txuserrdy_out_i_2_n_0;
  wire txuserrdy_out_reg;

  LUT6 #(
    .INIT(64'h7FFFFFFF44884488)) 
    gttxreset_out_i_1
       (.I0(Q[1]),
        .I1(gttxreset_out_i_2_n_0),
        .I2(plllock_tx_sync),
        .I3(Q[0]),
        .I4(gttxreset_out_reg),
        .I5(\gen_gtwizard_gthe3.gttxreset_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    gttxreset_out_i_2
       (.I0(gtwiz_reset_tx_any_sync),
        .I1(Q[2]),
        .O(gttxreset_out_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hFDFF0100)) 
    pllreset_tx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(gtwiz_reset_tx_any_sync),
        .I3(Q[0]),
        .I4(\gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__1
       (.I0(gtwiz_reset_tx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(gtwiz_reset_tx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_any),
        .Q(gtwiz_reset_tx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync3));
  LUT6 #(
    .INIT(64'hDD55DD5588008C00)) 
    txuserrdy_out_i_1
       (.I0(txuserrdy_out_i_2_n_0),
        .I1(txuserrdy_out_reg),
        .I2(Q[0]),
        .I3(gtwiz_reset_userclk_tx_active_sync),
        .I4(gtwiz_reset_tx_any_sync),
        .I5(\gen_gtwizard_gthe3.txuserrdy_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0110)) 
    txuserrdy_out_i_2
       (.I0(Q[2]),
        .I1(gtwiz_reset_tx_any_sync),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(txuserrdy_out_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15
   (in0,
    drpclk_in,
    gtwiz_reset_tx_datapath_in);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18
   (\gen_gtwizard_gthe3.txprogdivreset_int ,
    drpclk_in,
    rst_in0);
  output \gen_gtwizard_gthe3.txprogdivreset_int ;
  input [0:0]drpclk_in;
  input rst_in0;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe3.txprogdivreset_int ;
  wire rst_in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0),
        .Q(\gen_gtwizard_gthe3.txprogdivreset_int ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0),
        .Q(rst_in_sync3));
endmodule

module PCS_PMA_reset_sync
   (reset_out,
    userclk2,
    enablealign);
  output reset_out;
  input userclk2;
  input enablealign;

  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(userclk2),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module PCS_PMA_resets
   (pma_reset_out,
    independent_clock_bufg,
    reset);
  output pma_reset_out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign pma_reset_out = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module PCS_PMA_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign mmcm_locked_out = \<const0> ;
  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  PCS_PMA_clocking core_clocking_i
       (.gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  PCS_PMA_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .pma_reset_out(pma_reset_out),
        .reset(reset));
  PCS_PMA_block pcs_pma_block_i
       (.CLK(userclk_out),
        .configuration_vector(configuration_vector[3:1]),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pma_reset_out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk_out(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk_out(txoutclk),
        .txp(txp),
        .userclk2(userclk2_out));
endmodule

module PCS_PMA_sync_block
   (resetdone,
    data_in,
    userclk2);
  output resetdone;
  input data_in;
  input userclk2;

  wire data_in;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(userclk2),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync5),
        .Q(resetdone),
        .R(1'b0));
endmodule

module PCS_PMA_transceiver
   (txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    Q,
    \rxdata_reg[7]_0 ,
    data_in,
    pma_reset_out,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    userclk2,
    SR,
    powerdown,
    mgt_tx_reset,
    D,
    txchardispmode_reg_reg_0,
    txcharisk_reg_reg_0,
    enablealign,
    \txdata_reg_reg[7]_0 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  output data_in;
  input pma_reset_out;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input userclk2;
  input [0:0]SR;
  input powerdown;
  input mgt_tx_reset;
  input [0:0]D;
  input [0:0]txchardispmode_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input enablealign;
  input [7:0]\txdata_reg_reg[7]_0 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [0:0]D;
  wire PCS_PMA_gt_i_n_118;
  wire PCS_PMA_gt_i_n_58;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire enablealign;
  wire encommaalign_int;
  wire gtpowergood;
  wire gtrefclk_out;
  wire gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_done_out_int;
  wire gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_done_out_int;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_tx_reset;
  wire p_0_in;
  wire pma_reset_out;
  wire powerdown;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_reg__0;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire toggle;
  wire toggle_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [0:0]txchardispmode_reg_reg_0;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire userclk2;
  wire [0:0]NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED;
  wire [16:0]NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) 
  (* X_CORE_INFO = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  PCS_PMA_gt PCS_PMA_gt_i
       (.cplllock_out(NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .dmonitorout_out(NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED[16:0]),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_in(independent_clock_bufg),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_out(NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED[15:0]),
        .drpen_in(1'b0),
        .drprdy_out(NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED[0]),
        .drpwe_in(1'b0),
        .eyescandataerror_out(NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .gthrxn_in(rxn),
        .gthrxp_in(rxp),
        .gthtxn_out(txn),
        .gthtxp_out(txp),
        .gtpowergood_out(gtpowergood),
        .gtrefclk0_in(gtrefclk_out),
        .gtrefclk1_in(1'b0),
        .gtwiz_reset_all_in(pma_reset_out),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_rx_cdr_stable_out(NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out_int),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out_int),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userdata_rx_out(rxdata_int),
        .gtwiz_userdata_tx_in(txdata_int),
        .loopback_in({1'b0,1'b0,1'b0}),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx8b10ben_in(1'b1),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({PCS_PMA_gt_i_n_58,NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrhold_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_int),
        .rxcommadet_out(NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxctrl0_out({NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED[15:2],rxctrl0_out}),
        .rxctrl1_out({NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED[15:2],rxctrl1_out}),
        .rxctrl2_out({NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED[7:2],rxctrl2_out}),
        .rxctrl3_out({NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED[7:2],rxctrl3_out}),
        .rxdfelpmreset_in(1'b0),
        .rxlpmen_in(1'b1),
        .rxmcommaalignen_in(encommaalign_int),
        .rxoutclk_out(rxoutclk_out),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpowerdown,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxresetdone_out(NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED[0]),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(CLK),
        .tx8b10ben_in(1'b1),
        .txbufstatus_out({PCS_PMA_gt_i_n_118,NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED[0]}),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispval_int}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispmode_int}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txcharisk_int}),
        .txdiffctrl_in({1'b1,1'b0,1'b0,1'b0}),
        .txelecidle_in(txpowerdown),
        .txinhibit_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprgdivresetdone_out(NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED[0]),
        .txresetdone_out(NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED[0]),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_1
       (.I0(mgt_tx_reset),
        .I1(gtwiz_reset_tx_done_out_int),
        .O(gtwiz_reset_tx_datapath_in));
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_2
       (.I0(SR),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(gtwiz_reset_rx_datapath_in));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_sync1_i_1
       (.I0(gtwiz_reset_tx_done_out_int),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(data_in));
  PCS_PMA_reset_sync reclock_encommaalign
       (.enablealign(enablealign),
        .reset_out(encommaalign_int),
        .userclk2(userclk2));
  FDRE rxbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_58),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(userclk2),
        .CE(toggle),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    toggle_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(1'b0));
  FDRE txbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_118),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_reg),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispval_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(D),
        .Q(txchardispval_reg),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(mgt_tx_reset));
  FDRE \txdata_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(mgt_tx_reset));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 149120)
`pragma protect data_block
HeVBtbCTZ6CpSe5Ju+I7f6o3unM7ygxqd8NLtXEMzfwB68qda/188DBzKfgi5gnUFJxkNVSA6Bwy
jWn+llPAYPWyeihkyN/12pu63IrACtK9Ks8ckLrmc20eosilac75r3ul/npyo9L5sEtTm03uyW3h
J8JztfdQ7qYiLD8FcGpXkP+j1CrHyuhT9Pf+PwH5NUsG9TwR5HtOo6KBl1zdGAOCSJD1y0AOJK3T
PLB/3Z8ta/HS4brzGxEd0gpG4LJt0xTGc+okfucfdHpD7k+oy1uHEetFysyMyxnHxAufyfAq/rOX
dVrjqdhDUxEfz+VPHJTs4G0D8Y0XJFXULxyCoHukoMPIbABBgGYO9SUtErVHIPsTlOCzyM7nwJvJ
UtKGCmj3m5Dc4C/b0BIDD/POq89wnAMBl7SLquyDQCwv8Gboejvkinw9Sfws3TV+8J+084H0fhu2
6sRpIDj6875EDec1eVmSjeeh4VyMH/pi5wlowLRBPA+irsSKbFj070t2izWniX0v1W3JHrW9IZCC
zOB7OQrWRBw3XbNYKhTfVx0DazwNFxF8WkLV6OP1NjDaY5qJyoTHlyGo9YhZzoYChe5AOv5MJiIV
mTNbuB3IlepwUSYA+WLqPTQwuKdhnnycWh8Zqw6llPA9Kzic9PRi5k6a2RYFi6aNhiaIelbyfdUz
tY1hbkhwlQTss9K3gFrmO7Nu4e1X0JotF+pf+cJ3Hms63pcPlSb1AqK5j7XS+UZC8qqaJEeAG9PZ
QCoVEMgEZTGiRQCh5K5+h6Kd3dN0NzDDgtGt1ag4zzAXrH8R0uWJ6GqnZyGoCbUepMdA+nWERXX3
+pwfb+0w0Z3oy+QykQwaA4BY+5UX8Zr1sBQZpmCaamPTPRz0KcbB9jUNYB27JtwLkDJaR36rAJFb
uB0nKfbzPWuOWp0oWc8sKIW9H7+FpSlpK6Oo5rWoirX3DJSxAyXFtMIZUgZZ1qbRNN4BZB8OnFjg
CcJLzFxwPJjfI8TuDITimpZRv9S45fYPdWLLsUS2RiYToKF4Kgg/i930BwKIwvEvSLD1fvNcldZF
TW2C6qXzcmdsuufFfHCOcwO2T7FUC1D53njir3TeJ3qOLF3lBJB9521EFhTsMzs0iAVeRM6NloaP
GirzMUNBmAgaPXbeLbADK+B/jh0n3vjiIxpUZ/nwFsuBJUUig+NZ5hJG5ZJD7XShFNZnffoIWJ9z
jrOm7UL6QBTtjXLybmdS2/18htO1u5SygNUJepegUf4wpIzDlaHbJNmjzurtrAXUtYyZI8wGpl8S
YcMyt7CuJ4UI10JRU5CkkyWjeM5B7Xt1p271prM48zFlTEQXlaF+ZurNnUgjuvS7CbqmaZVMDtBp
apLtACH4NGWj/A2q4o2Hin7PprkM7DIiFIIFv9xm0cz8CoPcQCaVC7HXSpOL8YeukkAAKE6b8SCq
JZ0MZk09HR5F7UNQR2IxJypWylrJ30qZFnzBCEDX4w6jQVqpDxQsFGXJQnWx5rYZD5hR6tDa4tOA
qvmUKR7qqwAK73Vqinl41RIEq8GKvAABnFYuANl3WR0x0DVVmaZ3VbFxn8oekPxPtX3PATFQj+lu
5vHe5DBdF8oSrE3ux8HOkUut9kTNc9lQjW3XIlD6TokQ2vAQ1hNQXqUVcbTkj04BfJ4pKGsvpSQI
s/tIxqCNUv2ye+OI1VovTDSZlqYy4eIc5FJJprswKX5BqwUnxXoeWNfGo0foVX3+ncux3b1DmEI/
9O/k5zvtgwBy4ge8SCcI99MitzvR+a3WhB7gtclPOv1w3CA8T8axXveuyazKK3kF3Dc0lqP5ADkf
zG8I809zRFD9EjiVsU++e/lZWJM/YkCpBSDRX2bHu9pMrBogq/iRYaVLxIGbrRDApWx5mQyVUCl5
fQAWDrDXNbns28MhX9aoVmUC0Loae8Ll0AW8wjDKCv3XbZ3tO+aIxUm/b3DTC3vwLrH0DU2uVEAO
27+Cs7+ETyunuA/DXZkV94/jrmxULlpPmEQ7Ro1Pu8gLEYQM4Pymt/QhINj2KMzPKHbRwjWNUt/O
zylOKVshju41edfy98J2nlqHH7iNqQJ2BXJ3QTLmJo1SaA3OmFFyZD8g0CxdCgg/+l9VxC4BLKGa
zPUpw2QuKCdG1cG6hg/laueSPHvElag5F+celqHmvYqtO9F2BUrl1VdoAjnruoUMvXSrHdzHiwqB
oMsrfmGjMiYUzn8OadL5PaomsWwBUPsAMv1EqjFwFzfh/rHvDF7QWXBePI0pvkEhQs7IGf3yQKjL
KoepV7sjxHLq1x6KxIwQk0piE19an51uYUOSDDHnliNzPUIjeuxQq3guIvJMEXpC7FFutGNDshf3
SxAs93jp7Em18qe+Pan9VpRuCPgyGtbNBInhTxLHdDYsb4GaflCll+xygFAhhk4nlK7t33sKdWYW
uOPCy/BM1SrynAK0zH7U7yOI8JBMXOaenVoAyndt1ud5FS3xMmqHFh8bIa/uNNen12ZnqPymMZ/9
Wcxtx+FE/dRydeOp1INGPZ7UoDvl6aY3SEOcKxF4Cbqeev19eGEOM3QZT8eujZCCIUO3lcjbd+dK
rioPWpjD5o8iIxSOfx0z4JfzuVbug9/R1FF67fssHEuVtnX07ez23oX9pJWwHbePlHEGel76DiuR
rT2vRUNDneZNiSfB+pEryrCslvs/aXzkKYxf/89P3rrjqGhgHe8hyMlj51Oxx7wZISEYO0dQfp/w
vjvh4drDC38+ZVcB7dmrqeViaDaXmYlt2qsozSPujLrcg28pwnIuHEM2Zde6xm8p7h2LRlRovC2P
/L8JQOu2ESvekN1scnlqFzH16ifeH4I8aAgf67UgeiZwQD3U7MERPMeUZVDzFpBvPoGDyuzzhRkx
d846WohH/QohuQjnT3xTcsYxrH30V0vYL9B9TbSL0IrMlb2zwBs4OCp5FRpoVWL2SgtarMgrbAzv
IlOMwn8Xz0pOqMYILU5fKM4l7zMlRYdVqJQdSgtctHx562556sVt+mIE9wdnLdnw2y0DoKEKHFTJ
SSuTy0QKpebsTeNl7ChbIfr/rUhvgidFGdWmILaYzBrSb+ebAD4Rbt+dSirECqE4uVAqKWrgf5Qc
zBO81KtTirkDVHpjjkxP1oDs9nMNLLbCFh/YOlA9Dc3OW9Fa6QivFW6N7vbuKDnU2s2Qs4WoTaVC
rSXXlEI5pqhbRxHkQ0IiEs6WXes9Q5H69H2p16y8NKCJJF3oCuRxkm7varBWHjTMMR7YJt1Kr5Tq
rwQUIEgj9kHDwlVIPz94GPqiPZWrqdWhrdhBoVNfGJfr097c84NCGJAQ6kufiNdoBjqgJsJRaTlb
2n+weQaQPgCphC62WsLeeornzbt/6qcCPqHV51VRnIg8AbLqCW16UjiEsFrYE9S/8ku2Cf9jg34K
4tni+XMmVyAf0yXGn8xvoPQRpWiJhelbzYTI0OlCkhh7CDT1Vdy7pdqWooeBUpZGkCsGzTOpx/aL
pD+A33RI4MxGJ6T2RAZOIBMGp12lbK0z4PleS1wIOvyV200jP6shjzEVA21CBG/mlhNwyTNMmvAB
E0rxwJQwGW6LHzw09IcjWoCmfeDTtK902/iizH6lwjktFE5mbVZs0rwDtxYhSMHWZ8iD5vEtyopd
//bh4aahwJL0m54cyWwa7s2JQrNWRzG4pyksdj5RA+HxzSdhrYwS/O4Zq7xil8UP85DIwDmiof2+
9xKDWjtn7abelCtESrAHw+Cs6P3rWfcn2feu1IPwPab3/Nxoju4tO5f8hXPqY/Z34dzaNVfIY4ff
wPooo3gI+TpS9U/KPY15XDBnYCQknto/TF73qVlac8HnIf5AKw0d3cxlRq0L4otcwApRLCctXwnh
l192ZNnEKCIhXuqVSe5QY6a8XFg5QRrWx+J0f1093Cux6hmtW9SfHfTPct8DpFvhU/rVmlWT+K+s
mzoggAO1R87/VjU33XRBvym8Tc2dM5179k+9kFma00jZfot31TwF5QGksOpflNHLFG4D68z6NUBn
keC4JGBvAPsN8096uQ6remyKobutFnh4f8jLP1otJAoKAwdk6izJkugC4EVVy+jX+YwgJSGar+WZ
32LvRJnk5MqtaKUL3+fBqzhO8U9oTYfuzP08bXIEzjlSD7Xg8fi+bsTFOnJopLItmsDvpcKIbz5b
0tDautyU/kMkJnTJHnN9+rMqX+ffDIUzwevJVvv3Sc3U8PiSfFx3H+r5EQ/SeOVAGHecJH8ImwAt
B/Ci8J++39ID2vhoOeUaCryXSr0xk5xz27nWdWD1NMMSL6+s454nvdB6TiyxkkED8zE04oZwWmlf
OEKt48npB+TUxy0V9ofOTKyx6UU/+aZUMae16oyFMtx9Kl1yuq63Wt1w46D06jFKNH1JTGKojJLF
/ohnioiZENjTcEdJz/lOHVByKI0H4kQVwVpsuvxV1DhbffL+DTl+p33npYRlbFEtEhWFOWELVdn1
TwHwHnFYmpMyS+CMVN1PcLYIPpMcDN3J+AY866dBOD+sHAnM9Mfcxj5wq1wym/zs+JY8qWLKwp1m
f//ndmLsUMpqPHa4G/icarABs2dEhUfUxEFjtX2EUZbSDj/vQDAUUi/Omf0eOg3dVWDamcWWMSGv
uMSWKpjI1PZz2XMk2nB08kz9IQFNKeo0XcQ3QMAB4KAiNmmm+oDJX13pctwcato4pyNSn3kzgzCO
974pxUbbw0qiw0/GncftoZ4CSXIQjV4Rmu/zOvFYon2522LFywJf1neRKaK8U5D61tuQ9Y7TUXoY
ytJ6kxSOKANaadIgV4ac4w51/LVqkQIygj639IHY8frbnfmdGUrt/mylXMHJd64+HwZrb0VIW39c
9IgYBviyQS7XsdTLnx5WP0L0a3huuICllf7RZrkLBfRIPw6p5T2lkQIIoC+nO3Ymo7W+c210w4wa
luR9Nlory0ufH7euqvv/3MXvlhyAI5krimP9J+xfHuLEI+7D69ZbwhM90xFnpn4fgYjPcwNB00Km
/OIWYcnlDc+nzNxYwpxyF2iwtZTadGT778lkwef/iwmtmET0JyhI4FlWldquyc5lA8lAUq9FHb+t
F/00JlTfFWfY2BNUKYR4ToiRpqtwcJfjBWjdcE3RaTWWm2VB2WogTotz9PbUD4YPFx+nlwMKAzB4
LUdW8cksSGYBWxnzGDoJsosEDH3B/3XGcRRt5J9qa7EKJewwWZFmBWlZ69Y6RLzKE4JLEWi3vQId
xpYS/t+Hxpxa5Zxsj0GJKqFNoCTqorntTaZMEz0IFw4Zuv2GbTFYeeJE7xPZ4XfyObtZ2vGfb+pa
z+dxG6NhqgJ6WvZXktiHOi0TbJ5FnZghJgpJuVG67bRmIpbrDPsFYeP+0or+NrYRTA3TYq9MmgFT
nYySLTmTTB/H2PpT3fiPt1O1S4V+P7Z8GBrGShx+OuaX2mZmyndOjlmZAp1YiG3J8jJrZH6mvI/6
yvc0X6QzS6MweSI/DS/nu+Ao+cMJC2Fdio+GGo8p53nWSlh2MQabFQ8R1NpHuijWjr/3bYEDIrIL
WBEhU12p9o9ISuvgRSQtst6QziXwRNi+o6xkj6sN+6Iiu9Awq9vEZa9U5l6DvXVnqOBDque1QFM9
xLdXtS3sjF29wqjdbJYtoet77DuR/ONEhQBLsvV6K2lid8MDF0Xd9eQH0lE4tBJsi1xZUQCDUFyN
+igMNfzYNbKR3i3jw9PDNMDDPiF+aOlM9DDKY+AXfbER+kgO8dvH3WXvmpYgesmvUyJIorqF+VXA
cWOAphVkDf28gGiO24nskfWjAA7L5Jy35NjrhJSym/c7IMVZVPClzeEEjWLlv35EfHyFU3bolAc6
rCpuy3FIr78e/ZTITSFFgJcWD4PRVqN+eEQLZEaDkpcvS/30klGEOOOIycx3dDl/2BgVLBJLzoGG
D/snfEBDhtssy4DGGeC7J3DY656PRk/hlUUj8srL0z07eZdVHnDOp3Sfwc4lt4kFG9AoPrz2u+0W
fCLqZsUvAbla8gkbpueJpGEAMMeoC9FB6mxeUujtBhlicsB3jTjdkIXhxUeNZyvpkt0fUAks0bBk
nPPcOrAkrnj6Xph55f/1LBavLwsRYDaMQPSC9LPfbatbjlu1q6OuYV7k89E8u5Qsm8pwZzJlKwLJ
PVLc/Ylz/IAp5mdI22i+YmCj71sFMWeM9Tex0gUDjgpwmuEIcpuCrIYC2xJeYVi1HODzw9+EzAz1
GsEqsephvnJzUvVD21eGRc+ILY/8vt25QRp31TX9OxTRwlhwB+3KTjKMwXK+6nrm9L7KPklD/qfH
gcqmpsaPzM8bMXU5ihc8koWKGxbNhkzAavpVAurbKfUFSPLVcepRH9XFt63dpnK52t2Ph4HdQRJr
i+NRrwlQM0mrj9YWJ6NybjE+zwLmpXgouHchL8fcktfJLF8MTWtzV3hfJKjPn+kGUcVBh30el9We
PK0vWnvcYuM0Iu87/dZytv2BazOzofHol/+1S0h0W62tibzLv0pkWQ6Auy4Wh1hyaEXHLU061iwn
hrjdrGXa1+03DvtdEIU+VHs0VdaxwdMGc9ztpZzoxF8btHwyF9aZjI8FIx+QIjzB3z2cDPFa3EFB
T9Zpkh4/YihcHK2xdm4YXQntxuZsGy6+/tkL9oLwPvGc2Sgf0iOu34EJvEkGYq7JvDAPN99Mnmp3
E7HXRH3XIQIMoYUVifGDOdEwbkRe6H+q2Lir7y6ZSWtu+j8lHkEEFo+6cU4+P2mX5+wfrEGSbwrG
XWLKvCUUjXjLxkSbSvmtD5n6gNbJv0/32MLSE4V+B8Te8cLBr6qKuudoMjuRugz0vYFDnKhEUA+0
y8OlUFYZMTgHMHnsr0Rr/TF9DleFh610pMGWS3TPfO1uCgDTtpexSxDsLeGxnbzbvNiVnuOU2Two
SKq17ba67B0DmWhdZibtg2/pOYmxrVSSV6sTIIq1Eu7oNJj9vN8ei9wHiIZB4P1qoMIReNtm5nc/
SnLH6BmOJVGZIum7eBMcGoS+LkBdt+tMCIsCyIHCLTS5K6Tl80foJ5+2G45AJMzPMvUzlgJCzyYb
HTdCNU0znc41I8AmdH8mc9r8bsUzDUKiEX8/HKKjwJwxj3ql81ou1RQ6wzv05fdIa/TmmDmpNvST
mfTx0NsMEseIWZ3Bc5iMrX0t8MlEYPSZlIRLfJdsJ/qfheP5JW2Nf3RrVV8R2EaO+IhLVgmV6+Wh
Wzwcu/fdF7WqQLuuxAnmUVrgEVf8z13d2dEYTCy6QbRbDJPIJxn/n8JUaiEkmVY473Eh47ETcMls
zPyZOfnKRG0eCNGMQO3tEdsdNzWTitYl8v//Ck0UQom0HNTA87R2fpQop3pkAbmN2WIcUNq/XV3G
BHU45HpsmrkkVanjWkDn5N2scdeVPjTD7Q79AeFxGvbu+Mp2JVaniOnvMbLJ4psT0o8wHWgx6VFD
9t3e2yOa9xdt9RFGIAw2IBBJhaMpJtj/UByTKwL1DgDCvPs8u74+2JsR0z4Xs2yBotBk/UQPjDSS
Fw0kIe6ZcBkRqP5+SpEBWgHG1pUECqq2ZWF12CZZVfxkDSM9qb14Yd6ZvKFDLK2IHniQArkvEmWJ
OJfOWy1UHVzyC9lsFUQ44zmjO9Ed4duuzVGbTUoiO1QCuAZZP5aJjzG1makTYcN3VhBSqmiW9Ryd
Hv6HrFOrtCKJYOnezeFhCemlLzL+V738MrHZUHNV84/yes4xvwjLKY5jwkxn4b6MwfTkVxqznpoL
zffmhoZsO7Ugfcc8XIJEoN7LPWlqYxEnABQKjbPM/InzAytswGyEid1Thu0zxmw/jLnk1rU+P1c+
DnCwq7Hvkap3pGsZvD23DDhBzfd7YhwfYcwppbGvkld99m0fwK1c6RGCk3MiMU57480A367sXvdh
xZd+VsvponjqmM6KoulZ83hR8S/jO9GWpuuNqVt9jdXJpEOFLPHy/8sTrWp03Oo8mQnFi2RU/9+O
j9dY/OnXvNH+CFuGJDIzn8k1l7s7AwBcmyoG30F5eLHFIEGmc8E6DRTbkyoNDGL3rmcwTD8pRzVM
cSpKdhHnpSh318z8VLJXoq4KNeL7BKKr5LkTBp1oBaWqKSYJgJYHaD3CTErza8Qs6WMM45IjRlN7
yZEPPiFwEARYNldzES+uU2rsGEE1nl6SX8LfwpRyz66KxIvUfp8aA552ejbUr3CBM9CywnSVWPig
fLc8D+AvP5Nnx3zptgOJjo+TR1sAY6vGbqjAY/OqaKjp7gZmRfhKIkuPsb3M0orS2bhf0cOOJWaR
VJ9VbvCmnJ3D15zeR9EsRbQis57blopYRxCiCNBQM+QF3CHnSPkmIAD72VeXj/w5fmDacgIbdGGu
pQlySAi9tx23xFmkoht03MUXdtMos722t4a1klJlA6M/qPUTOSMP916LwaxCYUAz6R33vXRSCwLk
VYAc9+fkvtO2E9IilB2bb0pQN1rWlWBEYHNoNxYAfz7p6vieXWHenRgnyCvLxQM1Trrcn5S8pjAm
qDl+K8pAWkLdZXbVReLpOWu20/CLH2FhcBv3mgXzHF9bI08VOThPYXYt5K5Y14gPpakvTBH+hFmQ
P8v2BIi2ydIVDHe7ymzU3SVZEHbj4/8ZNIJKrIoVUPLp8y398sF7h2pbrwq/LXlU0pNNXl19kLeB
Ksn1TbWfRPr2spvHRNAP2ksL3Wc/m13lywk+u7q1/TiuuM7lDaK/d+w/tTX3JnrgDrtNdl5Ejwqc
Lfkc/Xh2Cy+XvPaYXv/Fs1/w1EOCmoCQSxt4Ktj2/d7bqGKDYiPimKfr4txCyIwdv7nTM1GsWMeM
qq9+ByGGawIyQfQXud/zYb3HnS4H6ONlM9c7fFCVQDqhHOjEAEfisCCc/TleXspHXjS6kIku3vKi
tuMYpl/xVsfEvFfQg2V8XvKUeMQhVysV5P2ozyyntrHK56D45zRW+1C7jjnxsT4E7SZN86m8tlCP
+SD0tvVbWnIZk8VzctMjP8LJhyvvF6J0mxZORHZOSSANyNddCPVqWDKDkm/0kKlJuJFQc8er3Mbm
RoZA64R5ISrq22la63uNnP2rOVI+zo5b+FCe5TKGaheuKlYUXzTS3D1nkivO8jXYJzsJGi6ObfZO
GW00GJU9Wl1gXR1LI7oSvcjH3mvh7rFbqYzjeslFfydiMVExXrT4RZJg1KZ0ngRyda3GVf/aMHza
0XmPM8glzH3Se1j7KLtohOfxSMb+ado0pH7jy5TOrYseu5VcoY3QSdYgS456iakVsLg3NsbKUJuk
bDRCr7ZBP9NLaaYMYU99Jg1cT3C7HX+HvBzwU2JRE/LaPyilN/uN0RIEMPYv5Xh0PcMF2bKp7Hnf
gsnaGZsiyI+3pCBJqvpUgiGyDF862WQggMbI5ogmDixHhB7H3KXZktIbqOfiBDKIMYHkl6mEL1CX
iK1kcEE2b2s6JwNVFlRygMs7G7OPuTsl8clt1T3uY0lnP///rhy8vbTRtWw5H8zVjZGPIaeHnhSH
SNRyuPGj/roIQLu5jMDMaNK8dwcNQCmfgznFmbItSSAxmnzwQBdOwXOdNiPvZNsdvOoEan+I8Bxw
Uplyh0ZkdBD45zFF+5SFi2DmSYS+pdMg7lvPKMccWu0e5g9p/nVamWgHkxD33fFccj2Qs40xKMS+
AWQfQncmxUjVxhXh2D/IWq+fl/QaO6q/XTi8dSDXL28hws5poOUkhoTOTwthRwETW6R3/knRgE9b
uAbFSKb86xk0s75HYGKGNpnEm7hloT9Xq6vlLa2RvkUuJ84ILrGP/pBXZiWJeziXe8B8R+bZm0i+
Vs/FQnJIQ/oWtmpEoAnqyufdGfMexJutAILnUSvcha3MBNWsLTnLXETqhDCDAJkep3XLRtSoh8Fe
0bk/9bXC9DdvRJveiE6IHttd+/Iq14DcjGV5P1wY+FLjpDEIHrhvr7loOecpM8NEvtHxI6az1wnq
0UabSpVcj/C/yLmD8Lw7XmsiNk3ZBhLIBw07eL65V+4UBRzsyRLbkDyj5t++ARgCcO8N+zWsdzmP
99wE/zwZOpYxMdqquJV2Ci+we9IAQzg7sHiIMIZ0FQ6yRQoWIubsDMePqWVS6Sw/E/I3oZS/p8N7
aGalEkqqLt2KkWx+N7TKc/ZGsk4Unyq75ny8AjxQhi/oR4XaJiQj9LTnsQbTDMWL+ub60rNc6Nwp
sb2YxG/4Wv65LTQN+rxXoBcWD3fmt8iRbKi2CaEeRNEpYyE+o/vbK4I6gnIwGt5tJvFY6khqlQep
ZHBrgLGeupDwaaa341s+UQXvxPKv8HSDR5VvsBfD1N3Iq/MhlK24po7PNxut0Cx7hDjqgHeLgsjV
nt1gkdQO4iOadhbaeavjO7i2KYAc4Ws0LC4qVduVwOwMCvbRvlNBo45V/1Y1AwwQu2pkUjx2kqzy
+TGw1f8GozSB9M2ErFPZSjxoQwMX5i39kZEbCdw3xQ7w5NI9aKfQuKbyO/NkoQXEnqU+fjRLEX74
UerwIzXe7CQ5vmO2e5kMFUBpdE69TS45/hRum/xGkbijbXQdBf9lAH3SgkzU8PT/Xx6tl6aQQucP
Z7Pf+ErhJZuBiQH0or5HSnzADESiegiVFH3/DYhx35Xb47qOt1GslRLCvH7DgQKhVaExhaUyJXov
ylVRb/SzcPPMddvZCmATm7c9D9ROA4hqrJzm5qc9QRV+D2/iiRx1va32fPyGp0MufMSpdUP0YtAd
/XoRV2ceqQUoAS481uSCa9TKrxKVUxtZSvTaBy0gYPGitaZAD0LX2e7C3ZSnKSBcfOqaRDeWbraf
4s2le4gcCgSk0dMBvlCpv3vxjtmd+oOhlZ9b6+DSkT8/qHhCce6cM2gaIa4uhETyA0wscr5JhdHP
K3oWmAkd5lMQuAlTA+Qo9VeA+DLaDMOABx8z7zZEHgzYuc35DROAD9c6WzRzeZey9J0hptDUn8VB
jB+DJ7gxEmg1GQSSxO1ECQzgHsJJEwMO9lm0GZllcrUztKAWJ+CiVcUH8cXE3SfOTQj4LpPgVLQU
ysxSF2D4iddepmbwS3aD4Ybg/vNh3OXXWur5Sb0alpbR5x2ijA9SmtIbm8bfMDriUaQqz9e7fh6h
V606WAmrYTLjaaeWTtjwKvbErA6NV45usWHaxnsHuZSGCYGp+da1o6LzMjYfHPpO5TGVrqD1S1Ae
I9VqUPrAZiZr9qVJxo8XFK7SgtC7Lc2cXUfe6o677a60NkYtgosPABKVKP3Vhe4NVXGuewJFLdHJ
6JUX8Sp1KdT7ged1a4pMotZq369LNV7KNFsVgfc4Sg+J0jXfKP+rMLyWOTnoFi6tnpGWNe81yvxC
qb23W9n/zCG5WGac4PEzoMnMbr+HssmwiOqr1YFjyc1VbfX3wIy60TgTtcAdQTxqwKX+77IChtEw
vBwE8ZhA69L2GlYXyaJXg04XoYUX1fuocySEQdt+jbxn2aK6t59I7O9R9Qf/9LvOSq/F6PcJZo4w
AwV5t0Vs5kDa+oip1gbmWwR32IxPs6+tek6REX5vZkfA429T9fNGTNxILsJS1N1XNBR4sVK5i6wu
SLw1oEPap1CRi04jFAMctn/pWggLEq1ByJT/ZV758v1x2toqYAyc/CVU4nnZaMRxPw2iGU/QJAF/
Rb654s8Yi39h5MUrzfxOrr+3qs5s8M9sLjVH05+eVLx17hGSu36RWJmx6p6gh7+2kBo98TaLeYY/
qQgLG2PKIhKakUjbhDmUWcATmC0eiaV0H60dyV6oQIfvQjO3k4aTz61hDwZNjqJkiMem9BD4dbHJ
5MjeJqL8PLIOGLtGFojcoJZDNOlnmrZv3VdIMyXe3mecJyqOjvf8I6+pbLSDkZcKm8elFgzan2bo
WEAo7wlCD0Wf4kJH6vXEVAQI+8xWbXbrjIfloHVYQkHi6yc/K/b9P7HvytRPENukFA3jDTFVlQ5Z
Mboww2YXjIc04cvB9XrIp4fRR1itUvRoPe8BzjXzuno3hdEwbLrwDlZ1DQHCkJtkoUlwtJw74Dke
0mPiyeRwYd4S5gwF+L0Na66BzDo9PdwoGXJ3+U/4goTejmbagZCd5yka2ZR9Wn7vZBT+Re899r4y
DvqMwNdstDDAqvB7LRHtShJaO2qBd3e5rm/lRPXYdunv/KG5MFCTX1XsOBRdYxn3zMMhyS+KWZ5T
AxKzulHDgwYo+kgJUFvv471PaWRj2xZ+UR+vNCR0U8bLWwepxTg4rhLB9GjktaojrEQJkM7jgMe6
vP2lQQarZf1WCy/lpBg1uTSSKscWznRTY2fvyfG21vlXZfmZUZXs0Zo9EuIhyL4Vk+9o0DhBUtC0
m5v6vghhEdzNDqmEMdWnVHaVKAHHKbZNhjWD3wvA0wncUtoWwVmmyYwyOKQ24l3MZsBu8eBvchyL
gQzHAz4hh2IEhKjZbdkEh69l1QzJ1p7TGkItf+Gs/7Qk+wsBmf4nJKiaYaLqcL3J3GpkJDwShpqd
hJKQQrZjjtx2NmtjaJ9gtY91m/vmbTDMDe8eTZbcDH/yP7+nX1wWZkfZfHW4bltt4/igMPgz4GId
9mcELA05oTiGA4M+O4R9Il5nWs/i9LGd/CSDWM3+LoIgNUA1hlugjiz/Q8i7WyH0HSossEq7alXo
rADNgj4IIoDiQ1zBedsPXAn3DASZeB1bSSrQnFmutozFAhdCvXiZJfFPNxFzmdwlFNrwxpvGISEJ
C0XziEdCgmNgOT4ZUSE/PcUeyxjM8ZY1Nn+04Ku0saf/03RqOZoi67ZJy4wvd9jLmu2N7pbgvhUQ
Gt/Ry4RWaQ5nsb2vcdHcSgt70orLcQ2INpm3bwXgt9PyeRH/SyVM2nWzTySxHu2+bG0ZGa2RMsvc
81Ij9V6N6L5DAOrjHjvDxMYoT75Q82kHMKzu3V1saiKpK37SwoSM1p06MPMkvD54ltx+n2524TGB
Pq+SfOQILORG/msYWD3qljqmVsKPr3RdDENgDj3byaTnmtVsMaE7r2zDQbYyl/SJz9bOa4Onvn7D
8dy9p2u8E+MniFMXTBPBzQe5hURqy7DhhduTGKreGTrQzT+vb9Wl8IRwC76p3HNx8NDWo9tsTOh+
FNF/KPLD2iXfoamDy8hdVLdg0alE2vBIyjG7z6w8Y7RbSNg27g0NRaUxFmskUSQsPdADbTSWJoDs
fZBYotokW0fdetxTqRra3HdLtCrEUgSEjfwBXPAZM3DJlzewmibfY+PhHfkD0sspZ3UZGtj+WGLj
FYSdbc8gta5CkRnMsuALk6K22J1Um4zyab71hwireswGSewOXnjJ1KTa9QnVRd0qGbv5Yn3ropAT
79Yd5gwdVd55awozQ8hqZkKsUG87P27aLNBvUPE0ZAA3H51hiXBYgZkhDEN+s0L1gR0tZPGcN8Lq
vyEGrvsx/zo3q/ZheRMIf8Cy+/vGFkzirvkxNeJIC+4cNAxH2gf3HFVT00Oss/oVuuEz9wdO5qT4
THKqZsfy2Ni4rHLcQNfTm3K937AK3dII9I+J/Rj+WV2otiMgFDPkFk54jDp3byZKtvVZt1dFjWr5
QWdd1dPnyMlsDcQcTIOl+PYurElIQ+1AS4QsCSg6anRBwVAafd6Dx3N1yEHIPjlHt4xQrywGyEiF
75duktSTt8+a1MZek7BazaQZmH+VOxTEg0EF8cnMpPiWfd5J8/O+F9ESG2hdeWNcbPeOnWa8PGf/
i3dSe9SITf8DPup6WjRheXsu2LxouiRFFtyukDaG/p9n7Ql5TuiKofihZwmjsVbyCifEqq8nFHLQ
gLz/Avs3VtMjIRL9TRydLneKHMolBz0+BjuYfLkhH990pbjWNnGbx76PZjwoq/XMyC/TxThxhzWR
zneOwy+0BthxXVq6JaOQ4l0Eg6Ipqiye/SOoisrpJlNboJDNOB/0gxqvkNw1sRzxzSGGvkmtf5M3
QZMIHA/inMiKByUOIcjegkhe4ZHPdArygHdH7PJ/QKuwvQpxMSd3Nn+FcPdAq+cKPov22pn6ShWx
nrPuL7ExRIYT+XPOE8rdOceh49TjUJvc8pr4D96o8pqPlz/BuZm+RW+P+i+PZJUj4QgZ+5JXhjjw
lRii4a/YMULeXn6o7aLg0Zr/GvzdFw2U5lmY0hCkXxMiwOWFmrgxoDagZnmybhtuuXt9jga0VFqg
UsXkJW3eCBmR/mihpptUyH/KUeP/djZ/0pOfAQLQEPKm1Nviv/7eNWFzFZ7sEUttFadf/0tch2VF
06lwW3Sdv9MYelIUvFnUzl3JXmcr7LAnjmLyqHTDbUGJJ9Jh/8+uPKzw03ZVa/jF9SOZxwGfoNvX
U6K6nh385HqXpEMfN/qTQ/ML0/NiqA7CA0zVfYE723/LkQJyvHjNWT2460X4TPvkoOVoa1BowSgn
6e8ANbVqNWnQWnwz+uYSNzAT/3ChkgoDj1+ZI3Elj+JEChZPIJlT1IK/HAsU1kMy+UgMzBN3KG+Y
nhEulTrVfNKSunmFo7hObHR823ctHKVeFMRXDhYJyfAC2xqNSPo20iaVWOtWEiwPmzzMpR4Eufh7
j2w2bbTnD+PyQeT44RuDgOtYiyKWXF4S/9UpFZyDrpP87eGcGZIU6zckiFNzP51v+YoEHPkt6SXk
xjZTEaKCu6+YQFCYlXbK5tLusqPlEMn9DAUDGSC1Nx77d+x/CHP9D8JL/ZqgSrfVl8Tfu+N3DJRY
K4EGoAuXgbYeoPCsyZ2GJn848xoTrXIxpGV4khvafxD50wSa+kvKnzD2YFryk8IGG7mhL8s0Mx/k
vkrwBuAR8his+5Cwmf6sy/P0IJBNfyEQmHtSEEnd+OWuOsMgX3TdVpo9t0jpOK2BEOuOaFlnwaG0
+K6DYn8cSJ0u2hhgPqY4y6moswgjjuCbj0VaZt2Kg9e7BYyGjUwwaJ2M9D7CtBg9O4PAetR5tRMN
BnI1gJX5MK7EXebeoEYROoblRRirvyKvOzbVhIzNXnq/dxZrq2La136vpUt5DSCmnXpZHswuTxWi
z5Vhi9sL8MeC0s01DRFznvQmS5AEWFPo8ROObFctVlM2Hb8J0pUWmBu8SWb9ZIArZu/T+ljuJakk
pHvEwqjm3DnaGOdLpFuFhVVXZwNzSJeO87Uv43kspfWBSp8VBtI8K9MuG8FpD38qfDNtWmUCFdZ6
VHhpVHmG3HuYpwxfHnhIyzNH6HwUl7N8Ml+b2TYEPiP86viFsLbhSZsLRA99/hR1cJKGFMsGj1La
E5LWQU6mn4W/H3WIZdgo1QAWL6Oy+pSpqlgMru9ueLdTGUpG/oyUtI2BnSeBTGq3QAdQR44e1KDI
DmaCox5WpVdHk+2UYNmNFN4tdWEeQIpvu/7z1oxgqT9ULBBFYDy1BbOlCfiktjq/cVYJQS8XzrMk
X7ZFz/7fh3R6IINFCHLLK+IGZ74dqZAlTAoY3boslQfQJR95WWet/2O5bImd02katD/zQayCaZza
CQUXEROgJekVfutjT/8RT94JnDTO4F2pAjdJuRVCfLTV9c+Xwv07Rvi5t8k6nrvcn2jaW82fn23K
pJcVey5rS3ZZDmKsZOfebqJjAwglgKqqcUIc0++I+T+0I/zQQm83mByQXThGMzquvnR0d7u5nZeb
JEdBDHLALdIbn60S+972Ab53Vnh8F1M55CXbMxsRaCBtbaV1b5vKD+k4FUKnDaBB1J2hZVs16UiI
h5W87ZKIq8ElgymJm4N5w4TKfJsuAU+Wcp7FNjmMK5Udy5tLbgMfrbLZKJaVBzU7OP3gjhVh9zkp
E6LF+BuhuHC9nnPuivNZy3UIw3CATk23vt01YyitBMgfxm/RbaOIy20A+2UepCluV0K6ev/0I9eB
XmXX32Bziy5LbiBVMtdzT2RpUyLqFjIdOe+T569xNzuqBvYk27F7koIjGEemgMG/DpkIzFHLgMij
mGz+9HRnSQHSm3vEJUKbkjGV9uASvBkyFC2Sd4zY0jYwYTKLsoD5eG7d1CB8b6Q8TiiG18w3pOtz
nyBYS06vxxyUtHNSnKTQjWYqiDyb83ZKtHs4s9wEOWqowVdx+FiWdVy+tiMJ3irye7uITIMnDVNE
rtl5ZGo226OG1e6fMuZ97KlAtE5ZYZXIDv8hnbkZvEyvFmZhESj9LiWHYuJ1Upbcnjtz67FCEyO8
8hBmKkGf06IIGeQmHhqY8w9lEp6CCkCh6wdpIDwJpn902YXNh+p61Sby3ACW6DBLLpfh/EGv2oX6
AYMaxA2IqqOp1liXhoP0uN5Pl+r/b6kO8rPfdiN6jztNCF2K0aC3trP/J3MQ6HP7KCE0c7H31Uyt
/AsRtYmtGTvjZty9EdB/2+PoVD6JL/Md6E3iKUJM44KeZKx7XXvo93xRhy7CGy+obGj6UV6xTREX
0qBrW/9GHCuqP3KLLenPHpw5wGaBRonEPbgxErk31eNHX1eqGw/M4y0is1jf4liyF5vMe8fYQmqn
k34HNdlyF8C0Fw4ScuvIG9OJhuB7joAa9OEhb5NJYX8fEqDNeVMD8n4u8q7TYmjA0qGMxKGiluRq
zM2IyBqF80G/MyqQ4O0DckJ612jwc0GGC/b63+mUycCfDexrkS+nf1hdXMfFUOu8dB0G7Z1gzkJ0
5u8FbW4w7YdHdj3x0xHMecrfZqL1hczYaOi93U4Fy4KAxs/arWvczjFOPbBq0d6KmG9EtrKY0uTz
UuU7uCIhkF2esiYf6FmQo5XK4NFwvbe2vmBosQW41qqnqvQARha1VRbzfmbcNNTIAZ5TShUPW+0z
a+XJ3Nfvqyufzx+Fh4vXIU6Ua2b61HFrKdN35i7PxMh3oIGlWd1YyF3SrtPChXZ6IoYPzIUUh9Ip
/oOlImjU8PC3wn8whg7dO2lQY0ETnomxhrJ9jqaAZwjtZKYLa2b4QGVQXpEhYZ0hvyFNnMlNoxGU
/LNuQf3pUiZyAfKqWJQ5zvG5jVrpw+Z8d8UOHEQNoYRhncOcDTpiH229TaQMlZkJKATq8ZsQRqKb
BjeXjdVElNTEDXPWOxoBIrKBFPg8HsUJ6NsoGx+SKFi6l68e3IE3mTVU3sRpc0TbWTx4HHFq8ZUE
W9I7sG6i7+etuuSje0pSvJBiNjooTyT70pYQUl584XFk00Mg3seLyXHLCDz6xAXxOVerS0pfwBzD
uLrhrnbnq609o171nH+km6G+z5OW5Hqvgud3vceYvL0P6hfAjvDsAIrBUQoZzg4UrvWGqYnlMBE+
/yhTi1jEZrTMIYt3lGoe8/P2nDB7dnWIvMtwOexX6YT18Ue61Cf5bKxtEEeUpvnaDrXC3YeKON1S
UYEIiGqiYv4rRKzsJx2+BVuI4Bs1mSCW6JdvCtdodxhvZiZeuWlsYSL/+YRbN53uRohrvnlwwzRc
nozfgkxoxBKNf1zG/nLFOylJW1mLVaXCtTG6guPNt223p8ZcpjZADJs5tiSyPar43tDufC6Jv79R
yGqvotO4473ECYKMvOj2hic/xmlC49lF77h/XowyIXvhjOjQYZOW/yOUe+F2CB7jgoQ2GaCzsAml
s0XUCoIwYiM1woxfkkIkw4P6IpPvW8+Ixz0rhYakKGFvYrOBHOZhFdxht2tkQFdatTpGudawnXre
az+Q4WkU258AOgJBNvEy5ZegG4uasoFGCEdL5M1UYTUpkIgG5o3gv8AEI5UYExj1qKfhexF6vYoK
kMRMv/k4Olen5ceq2adPZA3POHlYKMOkNSx7InuDx7VnlX9D5704X6xhizrD2uJYvPrrd2tFnDCo
LOoyo1qr9MS6dAYvy+NjzzeaRaGBbEQ+cjyrYib/fvRznMt11pgaXQ+93VZMnC0cxyfUoPBuOcel
B8ytVy0HRXObKmqqnuFypyZ/ZLWe7u3+PBwG+zvuwS+u/RUETQudcpQtW1OrO99GxZ2oa/zDZpkw
vPBGk7AUKHAsDhaeQ5dI3uUdzUnR6PwUuJFJkl1r2uNuUm5RzWRyPTNeoZL1sUmizJHskUrXH/sB
ZqinnwF1Y6RoPCUbbtBQmK76G03cDOjf2JKCRoNvpISWrOxB98W1vtwp2Ne5SnDkrToP2QxpIJkU
CDs8QFJTvfL9E3Rr2+M2SjSV3qRG/nDPWegf3wxw5HUr5QeVRrvkLp1EAgPDL2jhrP5E2f8W8hj9
SdAafHHZFCbOkh+b2Sb32DetsFs8fQaBNkRQK3Mmo2PPNxFbTlID1h4BaFepHTSEFpaVEvrivUkF
QhsjGZg+ZtNQJO55/gh93YJC9vUJDFNdtpwz/7AaOEuiX3G+n+rvlWpXRfVlLM1fVfgzkt57Lt7p
TvJ1xg1delTx/JPzZlvsX06/ZtlZX3qf5/Oz7gm/AKbOx3lhvvIuTC3M0njvelF0eY0Hu8UPT+gf
9ZorUq1ES5J4JSOq0LK7cXqIGHwhlSS0zPBK/EY48+DJDFZ2Kl+8wKJX9FHs4UVxnQYki1qwVMaP
D9Gc+LU6JgwjqlyLBP93iHaJDG26ebV0eBS8UrKfL5MgBDi1QKxafKIP36lw3X38AHwaxvjWWRec
FZ3TpYE5gR5khnoD41d+9AOti8BaHmQVsCwa7Eser/AhNC/HlTjd4+srYzoraIdYfd4M6vncvLNP
MCT7S6Jnl5rblchC6MoVVfequuzQXxj3glc+OdbYNQ2/IkIDFelT7/PgcdajTXPSCSbS1fmRnVXC
C+TUZFQgF9+Xyb6OWrRgJfdXQr0NUYewg/6Kewl9Di8HaqaHahQQfDZ31tMlHNX+V+8j1ll2/P7n
J8yQLWcTam4oCeIH2hsqBqQ/dzpcA1PwGXFfi4oYtpaVyccBqWhc3d9Xcz7VAb5qjyop6eits4Fo
DEXTrtUnJiBkT2DJN9BDDy/JC7hJYe3U4KJuSRaHhogWMHfVzPfZ1ieR1B5VnnPwAj3M57S7QT1s
I0yQzCKe+xfjdtOX/+3sfeyTcKBrlfqPwdb2culM+cp87ngXJeVPx/CO9f5tCbkTk9BbVQXIdG+t
FOKxzxH1s5Uz1PU1lqMxG0aGLKsrcUea/N0EJqEHqAAKcX4pV/4IqYDQ+TgUsOlazsAJqb99kdfC
eR3N8ULNlzG1qm+7nqZYzgRCt2zwGk9PJOVRchY2vynFSRLxCmZS3O13bAgzG6/7nLI2VVFH1bvM
xc6efq4EVGDExK1KY8EeRQILOZwbHR32Q5nXWTGPkjdA9Tamk25wH3EzRjVsCPVwxMCsPPq6yuWJ
crQ+Sg0ruYa55+y0x3E17oau+J0Hn8M9rL1AAcAQYKF2zSjaVkkBFzYlAmfvmiL2lrOperFq8SOo
3h7FO+FxLzfXZJjv2vq4DVX7HH8JobsbftnPq8QX0H5U7N/aPROx5wspcEN616c+hhLmwK1uSsV+
dl5KemCNn64TIb2B7R4ZXPhIlLv0L9dk2iQ5U5yr6WX7ENSneiQFFSrvtM5f6ltFu1yyrfXYj4yL
HKSj0hZy+SymEwDZTK3n75L7uXYNTCqdtc5CEx6EFVHCwc6BBfte50djk3RdZSNAF2XXc1HCGgKB
JBw21fceFJtAIKtCFa63kTkfK3N7akUATbRJCkCQTZ7BmmSZOgDhOQXtJX8bD80oMlk+k8hLHPod
W0DnjglgnE1IEBJD9AMACtspDhdGQ4d0p7m1iqpRtcyX6Tr5f693tktwA9C60B37j5YOyOH8l7F2
nNOb48hLj+QslSYhd3BMXkVdNrKZQoXAxU7QmPthUzvIOBLbzJhpVTmv52XId/+aQbd4dlHozGT4
7YCk3X1dikEeI2q40WD4hHxX+BXVIG5Ebwz4dLg1J+FMJjdwIOekglcT+5RK6Q2Bk6mo2LUDrL7h
9oHGmgfi34pFuUwuHycA6/jvwOFUYF+Do3p7B0ZgofOVizhLsxAkOQjxQwPr3TsDICI/m2bRNpwC
BbPd6I8/Uqqb1pOVwkIyvUs7zPbszdUJg/koZY4ExCDj7bmBvWgMTcoD1SMqUNpkIqqzJyyPgbGh
tVm80JXmvKpfUYUc32MjSxcirLN6MUgkthrdGOxLwfazFplrRhCK4znIL2oyK0oBw5rFcr2zAHDe
+5RPFIw1zui4qlywXnjgrp+CtKCN3U0LSSp3ot+x51zUn1IjS/8spBcHITX+t5fpZtwTyp0AfLNN
ICq1wznlGgxB3cmV2R4Dp519w5HEQ5+uxfSEHIIIQU1tlYfc6X9t0zWz7fkpNVb2V8ahlQXEFhVp
fPxWRB6NqJM/ytMqj4z/dpSZJ/VbAeZ+fjRpI5SqnCMqDg+sI3d/v/5656nj5hmGhjVaAdPXw0jp
o/B3SfCV17T03UBrxq9uN5p3Yj3uvKqns1OwcD3qc/sywhqEfj/gY6c1XmrSIvN9p/FNh96HrMt+
A2x/aGEGmcJteTJhW5CIXJ3FxlxZhPci+w40CCtov3RL4eKz7vHVn7huJZlWkWFsssIE/8Xkti4a
6ffmpjU9S+F7EV+vqPeCekqRtZvJvQDU9DjvR5H9e98bd1hNecINnLavPF46Wv+F4tKsZ03LoUbT
HJx7wrjc2JcPjH21zLeQj53OrdR0gcC2/IqwPTAJa0cNwuJ3DnQ9j00FRqjkeEzSDr9K2GQC0Yfz
RGA170urA1VfVEJVvZKL3uc1VOrhlUWebkJbOfDTPhtzq1tD1XgiKAIWrgu3ws2SDcXRXyaBIXpt
ACbgG7Lp4yJ0LJAiLR420ctk7P0KD2X7Wul1GesTMCXkx/NWYaiy++/h/TCSSdii4eJnaFyQMn+O
bR3x4ysDPSaaISESm/17JIppaz8JfRRBzK2qN991d+w3JkXbazNtVYvx7m+MYACsCIF11Uc5RUgz
BPHunrPKVtHtCvOGCM9RJtEJ6ieDhN03FvA5nCE5trtiBVBhG2yUz3CZLQYwgdPSBk22KJUXpweP
YaVee8Aj55zwb8Lc8gq4U/DpjPxTHC9sl3WSS9en9DMakOoFdjBbiHpZu6wlgpVDQbVW2TgGESJp
/QWS9sk+JVtkROlRGYRMW5Skcd/EsiNNXKMRdo/ooox6IWPw79+9LyV6EkcxproviThKkx2u2+Ty
jF0m045b8WE4V1IA++ZOx92KINHUXP9QT08beKaD5bBuGfmLaOcCi4wJj5/pjS/oj13oP+Dk9hDi
0++Csf4P7N1b9i9e/0WX3430ck+rlZ+fXARPmgiw2RVBPE2MxsCMAF04d5KtmBnb3Frm0Bha0YqM
RdtZQbQZL1P4iZ7+W+LxYDE6bgJYS5EcLaHlOmAdSccBHJLpL3gsf6vPKFU6I3AysLxWr7Nzzo14
lMIhImUas5qqRIlkXIQE73qEIvSnhdNUATATtBcCsAiJH1f0N5tq7uP8lHtfGWKUUgwwEsJ4r2Vm
iMwCuLqW2swR8RQ6YXtVCHfUalS580GTFh77IHREK/KVlsQm7bdiEWEuf79JvPzBzAXeJ35kbOKN
adDOvYsjfABNj+GdrQd+rcj+C0AGNow5pnAhuoWTR6OUlnnrghfVQ6UlfoQJ2CYooOHfZ9R8A249
UwsGz+/xO6Id0W52Qk6ANE4g6mH7TINH5ebaRrnseTRJThzZZJgBghR798vC9OhFEYeHkq2dEtuF
a+ogC91peLsW0bx8tI8E0qB2veVl4cr62+/9VZuPidXZ29IxSlChXhGr957NyAGE3LWEbYjKQrGL
lh7pSvt0LdYDtv4VVsVt7dDUnW+c2oUUa78abtPU2q36ACg7UapXpQqksERGp2PnIWa5XRyUWjGB
4rKOYPBVllofWvHuolohBJsq0zvBHQZxdL3JU7TP6FoCFAbjq/1PpF1VdPUG/RwDdsOF72xY8oJ3
N5zdL9YdTgT/0wHl1oXH5a759Z4BpSdqUfZYJZ1PSzC2yrUl8G7+JRFN9KpHBLWyl7pcF1M011ql
hT99aAxoVWWE0RKZbx5M0yMTitqXmMpCFJQL/2Cvzkcl1vVM/8WpEUmkjsFoQEh8h9ncQCRhpcec
J1c9+h1sA9wgoxLUuxtam/QBleBqiKlxpo4PGuQJJrqjGFz2uecaGwxjj/claQyVSEB9CrrO2LCC
Ikb2aeLTtHqfZ3+oOxnD5vz4Jck5QSJdRfwHnCAX/VVrKwnEI9bKY/6fIRDvfLjUTXoDv+gASIYI
qUxzASO6F6wmg/lTCAqlMmYTUZxZhYqvBhaEHXzUJGkYfJFubr8HMs1nrjfpyYx2J0gn2efdfBaN
OL11q4Rpd3bJ8l3yyS6IMfjVuitbolIfe4fs5sdGYQp7lDnj2LQVdtcV7Sq/1EYAO6odgg4KtapS
BNgfq4OU4jP1cVaJERozgUejAI0A14JAtHZsdlixiaIxvZlyZ3T4ceU1Cbo8Y2DB/oY6ya+CPC06
s2fmgdA+1xNt8LG3bRYXP2476GhkVGwcYHNkiUcFHViPXdHAn5WB5+8s8hthAPSBcYocDd1FLB+n
rOLfGm+EypjCcZzAZIKObdJlQLfz/UxEmfDLK/FJLeTS55dUK4OkF/8yQd6+sk8s3JMVwAJGiJKu
IzsqmfZavNKfWctn+xoJc0AHOT02C503zfDfKqdrHU5W/nqxjUBtLPeT+HebmPxIT72xqxrXnz7n
DtOIR5vGmsXtydF0dC6vimuKzXgyJ7eCV9YvcOJ1As9tkmIveL1SBkGq/DYEzNDysoExLTrjfGqp
UVeLaGu1mgq+2WfogaiFCATWCK/eSkGVy7bsyosSt+XInxVs78zXgQz2LcjIbtMC18lV/NRI3dQ7
h7/ujJTzfOnf5aWVFc8tzGtDK0vkWPRvjfxKegVaq0mITHRAQl6sy1+/dwWWVsB2y2iYh0W1zOoP
n9DDriR+iJIcik1vRQ/oE3ocYBtvUdN0L9qaNtMIPo2/PeGkTzouxlEpd/VM/Hy6l91jumsKAnGk
B4mgHB5OwqYQ3kJOVd8Owh3QZthNSlPpF2CNvpqorI+H8zacrfufVxDZp8FaX8mYNE1q0926Vfgx
uv0cqgbyYlXi+lfpgruvFbMNj5Tm/OMnwyYKtNVjrPxexGg9DbzBK3hv+kMHzNmO0rFYTZywmqcA
OtxuJ9Iq+mbDKb1lfB1ODZzF25GkVduJY/CNfjgYOZh41pgPOaBAFzNWIFmsCIILER55oLpADxJR
vEBpb54fN9cYpR4PMQDG/G3mdVk/AWDP2rwkarlBkHlLXn8iO4M7MlTAcLDDkGJPdXj91/nigRWW
piQGzFqV9fLSyTme1opZDtPdfGx8iqaQcYP4FbWEJT6+2h67zQqgwMga6lRIcwMYA9Ha47V5znVO
3AmNrACwdPdvLq2luKH2gs13Xc8spPtaNkT0HKhhPwnYGiFod3Ddj95e8jiHccoD0h9jnyxgE6A9
x7SpmTOVg2WPJX9Wo/qV0Ywqg4VAGFZOhqK6WR2Y1AVOXH2LgBLrAz4nSkbaxU1BhiXmAb5YGV8V
bhJBH1seBDMsr9o6M2W4ELkx/N2NYfpVjVRf9pll9YcAGCffKlgG+9n+kuDWShqW39AB2L7ePa1T
MkY6/Iybfgwta0zyPBIrnFNrRpDHQcYeDsaFKcitAnkT07Cb48RbWTc9t3P8FBXagdzfw/UOxYCA
7zleRWU2b/QKCFOU3HdvzF9KnQX9pzBbRxBnBho6da5lpDFA+SStcliXdcMwd8a4otU8Cx8adRrF
kfTaj9MQW3onPjhhCPZf67P6fZVwhdjLszSSGW23GadRjauZH+K4H6vbgHmhapbXAYgTN4zpSGgY
hqcCjelnxjaKz5oFV+nkE/sebWpa17pcHG4F+eKRFWBnze0KGvNe6YQoJxeHUztG6krkNo01wGF0
XDEgf6cqTx0j1CAlhC/lRZ5a/anOaliqzGr0BS+TW+JkkEL7KS1StGdKgSY8XEleLFZD8JpEyF81
QQTZnUHATAReLoCCBhemifS1aQzepytU5U0Uu1up7CU6cWdxueP6Z6D2mk1LOX2ObARJ+i5IKfFE
dMLy/ne1OY9rG1uQtfedt9P88T+kleVtoaaRQIioSj5mxBjTd68z0W1P1LiEPEdeK6G9uEbU7AEm
hO/+P5SYFb6ARgICEsDBEMFG2R2hxhgn2CjYeum7dy2vrMrjalQ0c4iBneBKBw48NjULa/TXzo+T
5jKNk+f9WDmRA1nuU92KGb6NGdh7QNi0H6axO/iVp4NNQVfaAf93nz9bIQwneknKen6vPqTtUuX4
MY7xTEfSztLjiFOrpj1VVpp/xDMjr8b5AdVqvXeqJQgaReaDRJEtmW+v+jJ6W6FOauFtGQoCZaPB
BycEcjstFQXFmxD9FO3KMessPOr3VsZOTkTkwQbOHEo53ibsHzrbMLjlESWgzBYdtd1jESlHQv1y
eCnMIFv24KuChQQhL6Fwkdzd9uP+c1nbq6jJjSZSnTk3acHVCzFB/wuLZyxH08d7c3VVAevbFJXQ
ZC2LrxYejWTMNTZp4K7VmBMUMkL701wajq7JfW/ppWRnwxF/aqd8z5L5OBjdnHwQaVEb1BwF7n+z
ALmlneokCCE+8MNpn0xQJIo22RowOO1D0eVIBIGKoHdNeNZxuDapHbQ3tS+yFbC+lU/ihcBvLhyZ
0zyN/TpuRiqOlk0LQY6XYNnNAbZxPCE3j9YIakcsXVz2MXIdxkm4mcJQCoa2xZ71Wnt6D7JrRTJ1
N2suz3ubhbI7HMsRqGWS7BJ3KDU9rUuotHr3Lon5DI/IhkVoRCiKgHGe+C6ArBZq1Ju9ncgWCfZA
DeaI/iVEuT2I4hV210xE8JrqoQRhaahkyDh4gIyIkWv6Azo5jfZyTGVesiCYbxSDDhp3P5Mt2ejI
0STNvba/agkiG8isqQKskfbQd7gt7F+/rZg6UF5EC4o79X9C050cOcNXsloWemyJ75kpyc4kgpFG
ogWcfzh62CV1aYm+6dqgEitktFy9qgSx3pJC2xKktVrclGueuXIlfgD/GBU+VOv/rfl6MVODOxOM
KCmLvRb+29QqCn7UmBPKA4nI/6toB/tgEJ+oWRMUQNzBYc/RRUOCPfxsr51JTAISPLiONA3AvQ0R
3ep+u0Voqv8OyXy+Sj6N84L/pUSWIIYmYuYpA4lnlFG/PVcJQEwph+k4yVSKw9W5i+q4I6UCMZkU
Xg2KhV1UFRh05JkxE54g4zZi23pdoOdfnc74J71KXAC5rSukAneFKf7BCCEvaXN2r9V9XM+bSXUe
wEdL3Fh101XULgm9/gq9bgR/JsXD3gLLS/42Vo8DFxIsRrssf4tv+yFIzF01KFte2vXvztnXbnoN
QjMVws8zb6CUoAy7pzsZLNTb/VLkctwXGHT3JjiD1vl1fCmHtKtUtf2LEb71+XWSMpx7jGthbbDQ
eXo8gmnj5HBYa5HmI2fNutcbQDpi/WWHrpfYArlhF3NH3jB3s5ObLtYDoMVsJ2vGBg1G81WVgJvj
5TnwmMY+hl+lxqaJvbssX54NL+bb3mKJdWJvaz1TP0ytXQVJhuqr1Cq3MmZRyNAoGXCBII8H8sYG
aTEXYKNdNnHRKINWq76E1zFAnsgyF+BLGq+4P7FDLGzfpV+EXV3Rg2dQG91+8eM2qzis/Bl2kTlE
4jUckdPQfGZSn5OWaNnRGpTAsd/b3Xw7RwkoLAeWaBfD+glWZqrBEVY/+HeGfiCYkJSF2/94RjTM
65jMY+3E5o6F+QJvzm3ZTO+Of9VR+9gtLRATEfbnqmWh3JawT5KY3V87HO9ejM7czcF4f64Tu3w/
jUkqnM2pcbLeyX+HLCOp7vG/QQK+NT6bVg6OCuQJaKvJbI0KIhKljkMy6eh3DIxaKftE2m5Nh2vI
kzvSrPrZs3pdlhQIAWEE7nowMabH25KFp5iQ1IUC9w8pOApc/5RMBKMlWYLPRiWSqFDcEEZd3X4L
yD/s47wgkeSNC/3xIJoNwZa+k9P6zHc5jvTJ6IvoEvzhX+K7/4EHijrPMPmcjaO1NSjERlwkUfR5
YLlkO+AqSd9L9s8B1uCI+xeGT9JWJvFguPlNFSHhhtR1yAnjKCjUA8XDDZ3ZiYrwwuPaS3wU+4U+
uDr6FgeEipLcE6uw1/BxwSfSZwE9f3BerQu+thjc2AL5yfr5uTNzQXkVji/9ehJLCSUR386CauFB
v1kp3vaSqX6F+3jinXQ0xst60f67YdD5kmLgg2JtMPxcSAoZ5lvXV799cPgvjIj2V21DoiqqQ8PR
EMRJw67sb19nz0VU39CKolEid92DZY3Kf5MITIbDqCv1GeF9DEdtTpyT9xAf6cRNmfkSB1TzatxQ
9dhnlI613FfbbOGQxqUntpRf7NbIpedY9LbN9GlosbU3AGtSFC2fmNsJqoBJLVHgIejybXVJtmlt
68LE5pgsMEmSzdeVbdUoBuRZOn/vVz3Em5OimBcd1eCpdlb7uFmbPK003zNPWuoerF75P2mVEvaS
DWzFSwlRRe6vMx8HhlIXMavfaagDVsHEBtQGc5W+K/2CxWwph/w3+E/sED35DcFPAZIZgEEz7/I4
VgnWrAEyD6jznaMEPncrayb3hnGbgO0YT9+0lOiXtyIvk+IO5VZRiL42qGnSddj5dj1qYhiCd9zV
RVm9yorufMzwcGJ0Zuqlz62KjO8nEx8ANORp/DN9+v41B9g8s9suO9H2AGnm1w4V5mUMzI4VhwUk
qpII0SqkI4tAbTowNMWioSevUxUZAUYvVAde3U/oeOYqg/wIARM+w9+g4HS0ATXNTiRByvc0Jiv4
5J1YrbqjENsi0IrElVGczsXNrMYoyUbNDelx7pzMKrzagPLMS+C/N4NqQHjimPwqb4G0uodbQ1WC
wtrxpNS3KBniOjseuZmSmguz7VQ0kOMHlFQFpHymmwbAvwL7LFYw4Wi2mhKJcmCLELYe/lz0xwrO
ivu9fwqX9Oik2E6rR2GiuN4KIE24lsUnXr3NpFYYKhjppaPvkEluHqFCqDL1oWE+BkWXVqC79Oms
d0EPHl5JTO10rsa4ZEKl112i+OK+AJWmtjmT+jckBvOxe82xcIeMMK7PQyVfOz3qtabPsieaYPmK
g2wzNAT9YBzWGFWoTCb6tACLtN8nMUIQ10agiiwAJZbLuJid55SuUaRyx+U1swqJ0yd37lNytvcd
39MTBR5d62B0MUz74jX+AUlswwwFO3PjAmajF9k2bbkYHtY4Z2e7HHayDfWrpyb2scJXjFXVEjBC
vMr4rRhD0O2Vg9e2aFv9ZcPe8zLZ0rTTEaKfghqbaFrTMFxfVqtiLGpkWt/+Nw8m8HVlRVwjDNFl
icAJ/SpDS+XGiUhkXhviwqcLqmma7XZd1y5SRgI7vJF9XCqMq1gFM7qaNpsvGSVj64PnyTeiZiqJ
0F1elAYqOwxE+IIXNL6wKxRrHaJJKfaD9+5IPCn2xoWiFFKsQ+tzZ9l9MQ8gW2FupO3VMkFje85j
1YhDhNOK5YNDBIFVqLw/lmgjRnSziHDaFnht1pANDZfmXIZK9TlQ4lV95CLF+rBgIakpKqS9unsz
Z02PtArMHsjPirFJMdES8ceEr5CrjPqkrRHljmuQIkbcIJ69opeAyuGGAveSx39zVacYqHfQnokj
qOn8CJJeSencokxm6Gn0VkivHP99NbXD10Elf/rm9v1DUbtg6fGtjxVCJgTct7KHl3xml0TTS/BX
grQgxVDAisPdP9u7EzWHEauiX4zil+g47FWF4UN6b9N8+J3xEyzWpBRCLjyVMgI/1oDNZdH53FQ2
WbNKfPyFqZi60PAwcC/L0IvLMGCQMRaA+QDyFOg1R4UBPVm8XfcM7OEHgFyMEkL1Zt8p0rHIeOec
3vrXTvwgYqz5hk1VZwCVaXhm4NoaT7SftQyVpE7O1A8dhz2FIMwsDpn5mczVKDtOWHToI6lE9mBC
yB1keYkc4CPrrgpo2xDRDDMpb48wvmRETJ02KoQ3dYEvz8W76Cjseh+a3R8Ce9l0rTMwsKJhQ3qQ
Iq1rt7gmWl3b5mcG3mpOIbnvL++BgTuY3yMdwOx69vKp4UPzXKvILTqgTqWQG+PY1DNAZkcZFR/I
zsAxNBNUKks2WhKYD5Q1cSm9VzVHWrsE2Esh6KYiGHAlO447+bDN+j19LOSKsqxgOuQc4hHEXjeY
BqKq84l4l3/cwPzJhmRsFz671f/2BAq86jowa21ndQGtH7vMu7aHx4TwrFCcHGxEJF6G6qvSwDTk
Z91sxYy1aY3Hk/TLRFa55g1iJkdySc0tQ8CDPGUBCZGXSlpksfITjMdbz7YlZw1YPXPQ2Ym43N3D
7oAOW9axXYMb8ThsSMCjpD3u5cdLmmIMfkkMdc8ofpHUBT3cd/sg1PZ1wTmPRJEGS5uEMDkOXOHB
uYF+dUoIpDUZDcJy8Mk2UnSkBaC9B/rVcsgTgJfhSh1rxvKtavM807WwqWTmbarU7XOdpdJqBuvE
T20VRpZ4yIr65joiu9wezOX757OTskZmAGxAW/KF3bQORjRWrs2vyTXo1fpsKbX7+YBJ/bCXoc9E
Jee99wNFNPm0/cKOwZ/Ws53MFr9J/BeP144XuT1MitfYPGHt+yWmVHMdfCw1CWa2pKNHEjG0pDly
//dNzZ+P9pcWs69X/PTpaoyIeqIv0G/HHh7S4LNih84izDa0r/207AN2l9zf6eTjKni//1b1Uje6
sa2C0LyxJ1xwBdJgLSkTsiFIF+V2m4h/526TkzwMjcFMABkWxk3DmCP65BS9eST+ChD1dJgTtipL
PMjmQ8znjDYpLdzuKUil2SmuLfdgS6cTPk+jaY1qzRMzDrDV0zqY+9kAvrasrBFFkLBrwIK9vrD/
KFZ3N+wC/wj0BGu52/I08UrGsPb62JzYctlbsgV8gBeci5sv6FVqNDsiZZudQKoi+o/YNxPTXbkB
BXtFQU3nirEZLvPjsRPWEejagBoTHdp4ddupFy+t5i5XTh2VOb6yKTJ0ERI86ho06cz/UMU2fnXy
1b+Hlbn4W5+8kKr0+5TIVmN4Dw28LfdUPqGy2k3DZrUAoOeoD+ngiAQzmnNz3139Wu5d6wlSxdkH
qpaN9Zonq8u9HareGgcAkHoC2DLxVffvLGdPyvu+0UFAZ1m3hti7CJKOYna4TsmCdHN9aP3phc/W
1ygxuGRFSos5X+ev+iCpfZn+fNZxeQ5Wyp+zQDePd/maGZabsaTPjNsplbMtXfZ6XMhSxJ5TEufN
6hutiv6vWzCYG49XYLPZ8SC6wwZmHFuYccNXIF8EK/na+BbGYWC0bJVRfmzZe3WikH+vUL00bv7S
MlSO1Mk4NFEXHUF2prr+mnzuzJMm7f33FH4JZEPW8ooNB0q9mWYy9ynCORTAHbi+5Rh61G3GF4Qg
nTzPDw4640j/raOKVLi4H/SBTPm7H4iFIwl1NxiJ1mAQC17G7yaoGow1BnIVjqXRwNco7CZaGPz5
VF8rw6IJan5VsbTH3dC3RM/mT1l0EXOX5Y+x3hIgmrdpDZ9EbYGdx1Cn9v8D1Xi5hFMJ+6i0SJzX
mAnyGw5oYgKMPtHt8eGtgRhsTXgTM+7H8yhZ1fAusqryvtpQRAtCfKCvt4Lo2uYoKiCVN1Q4GXuS
o5gytHhkBAkwrMjSaEoln4/RPH0WoXQkOEskNbqovxiDEB0M28Zz0DR6tOFgqx7eUEOYt8DCfU+K
APHBFPUR91iMCyJpVK+smVZb974i+WS0wIBqqISliGOm0CN/XIxGkeeQCCJWRmMkcBtw/pWj18R6
/KJmXfClgns+J6naK4WhOmLOtko32zkTg4arKAR6m6unepvKTaECuUQ1lHcCL58t8eDUxKCYh5tg
xAFRwDUm2FogE1QzPTGaxCyETKOT6ShTj87GjgMK63/9NnWf1K8aTe2qN5J1RTyuyv7SqpGcNw5A
TKlno1nuzQlSQmUgJaWaFIUfGkegkk9b4jLU20Ox9RzClBONv7KxtGVP4J7e4l/Pg7LpCncR4ijQ
0Af8o95qQK/CjBd5d7bxGVKH0fAx2K0wRIxL8984+DuaOWzUPZTuPcDENUgfdQ313YIXjzuhlSWP
ZivCClnEMU2uq32Ly/rgxW48nWi6z89APZ0TINQFbOsZoq4d7wyJhOrWqtKYgCJyEsnB93UQQ9FJ
e+MG+jEFtsEX7SREKo2IN1vSj6n9v/cQuYX+Bddc24TO6Qpe8mTkaPT6XH0fT1hVPARaA9Vr7xEj
y1/wxSgN71HdhDVWAx529cBVcus8mu/aKIcYYqxL6F6La9rdDedPCpLc0yTUyu7arULazNjjMoDX
uoF0LzoiGP3YyetYon6jCnYBDZVhLRuYIehbqdnmO18pprn709bP30BZRQhc7xNckryc1XgBvPE/
fXOpFOMgVvNajlBykkC+Ap5Af9GEu65lv+wu7xcqIcsHPjVfHcV9FC6ohyd4EgsaVLwbsvGUuSTL
K5DUvQaJ3GUiHh1BCQ/pLUYlwJn2EnQwD+4S2G4OZrw5XrVf8quJ7pnCkpdF+RaYAidEQ//s4lzj
XWK1Kn6+6xjX3HKV3s1xEhYYy/qHE5wRMxFk4xAGE+SW4b5f57ellw/6J7oUF5G9ZmFZrDyXnZj2
cEJMQRDgW+pq5B2hMt/PI3T6VgiqPOMcs+aecIShUofT0JW04pFeFdqITw6Vzcml+xqLZbMI31Aw
rskTKUoVpOiMPhrXtK113KtApqZnMG2yV11fv/eidpyqDQXa3glFHWpvcI0b40JxUsoMUCKw1j4l
k8tuQAEg41MNYVYdfbk6GHedbxNQoDLFiI+VNHHvi+22MMhhOvAGtADD5DIDpiDCm1n42dGhymPJ
h0vkHSiUlkWOqew69/tWSGvTy793OemPAr5NJzffDOYvKNfqFopXOziwpQRi+WqxTUqk74dfH4PQ
89b+vhYXkqDQN5RkV+ruDxbLHAQUeB3ImZPlIjwQt2pi1G1kPNUCfVnc7pBVOiJrQFOVJvMakYNL
J/pZlw4PvMikch8lhTzat5FM4Ds7oEYkhoEg68FIyI3Bt3Uqij2fWdN/DHl/ejvI/jiOFNC8wAzM
+xc3nKkqJDWKfkXMt2lHCXAemqif16mx/hOgmmgZhrTFKv+dmho1c+UujzEys36bthd7/1gQOmge
3DGFFYS/p26ii82j6/iDJKQU7RoWudxrIHAKzWNjzV3s34cC9JJE+qS+qigd22hKyFq3Tu2sfUGk
OFvP/drsOffFTHFQ+nPAjiFgtaI27sUdgdxn4L/eSzYfRfE97s90gIDIbrEnegDuXZkWZG/3ABpZ
NcY96emkiwtlFGn4C7GAWdvWeioZ5NIDmqYOP/HATzVjzsDQvIT+TYOz5gLNgiMxHYxR2iOxViKv
Xz3L6GqOxcNB+gVlKMMgNGf2mGkpA+nHwwbhx6NPJhA83dhRDK8Gxu2w7UCx6NfcYCwNlciBLJyR
6hxxvdoFEVpsp7Fjpboj/qXR133NR2LEFXGLdC0eVFzngkanbfcPlUpwueA9Yn3r9dedOCZzIPg8
l+twvyuynJ7aY2kF4qRtdrgJ2pMXQFFbJdScgM6p6auCwWuncxyxdtO20lnfxu1rmwxY2ruQruJh
Ex0ZIGIcwyTYLV4U6vST8jzUkL8o5KCUHzw4WyDOY7wqY30tYTL1Aub8VbDMgdOZATJ0eUi4Dq+6
212VOJ+0ayltkjW5B2Pi3SQ30/dwFzEdJ5twxKKfFL4CaX74qo5KqliDk2SovVlR3MZoTyTHGRaj
6JhSTwGU8deunbk+l7TMB1FoYNQ1lVPba9blY1HArVOPbwPSZhuOFfiKHkSmfI5PJQpE/IrU8I3t
8PtjBsK3TtR/QFTFP0Y8DRmiTcTeyNFoXWCpoj2UuBM7m1QJBMvRFejQDI+mF2szs7XDrSMJud3d
P0n1ycZKvUGYQ9JtD/wmYctzhOTiw9v4DsBllVSB5v7HUvA7tCYyMJecwSrtBSwpNwfXgAKNc2yi
MR9aFBsm+FhOxGBMDu4+97Fmi4ft2BpuK7xnX9rq1mp4W7sy6gU3IQFvLeErS0jbqEKXAPZT/L08
+q+RQDqFEwoU8ZtAk81A73xYdYPtFpBLSu9TGIlsvbLDw64P0WtP9WQQF/bOEjdO9wJWaFwqHDQb
0wX7/4aEHr3e12+PXdde/qqrQuN74mKtrgfTwLBVFBw242cEr8yT6aVBd2QWElGqxAFB94QkVQEE
pncw/+7FaN+3tv1l5XqRm46lve9Y2LM4NSu0Gu3vuNhypcAceGfSQY9MyhmugqTjvikBTL4mFJz2
ud+ZZkqsoP9LZM2GfIjhG/PPmEH2uMPm3K6nJV9jIKXl03V0UR3HFh7BnLx1t4Q2B1KgbTy/11fS
z07lhd5W7tZkj8aBcBo1ecIDAoOka0VT80onO7yi+0k+LS2Z3XNXO9PMhWFq5J73K7MXaDWsIkAK
yjy0v2kHgH4WI/Tk3eYWy1i7oSuCWg6ktR3zHDALkP4JdpLNTW7dDZfIIKUSrmWrICjiRTi3xfEO
oHaiCctJ2h5F/+ZU5THovFDuIXRXyomGXyGyGRzmGBj5zw8k3FW1qfI/h5ScHA6eao/5YRDmD8Fg
gxajd184BmTksmI1jdvxmEDHRPkya10kF4/jOXVdigSy/iyDJ/A1Pgdr3YqC7fvpvhfbjjlMuVHG
EPJIpfWm/Lb13CBg2m0gtNu4pPx4sXKhjIqKIPrlY2c/cOT5//YMgHtPW7/P1FbKu/jSzi+3VQuw
dsgCs0jQYdbaB/0uPEq6NOGpxnWxiiq4wXVxAJRfbEjysyCIYf8lPEzE3rmNrAEhxBZQ8tiuCcSb
i9SUCVzMAAolWhdbqC6KpMgOcVxG1KewyLXCzXBtlkv/26t4k05qnqJoYFN7QomHvjmWkvvAxuGY
n7Ez/hHYzMnWjFKqyuD5tklto5ItMpbddI77nQ3Ul9VaUvbgRTOe9SmBbLPn9wh/L6Ub295BzQEF
o8uB0yPGzubk7s6ZMcQKd1oIWCV28MWAIyzgguQ0ftRw6fimT+cNmw/nh3J7BL37dMLrL/6p9sd7
IQ6KwZXD1Xswlxg3fgv85spMd/eeJLr/rVCRs/iW07QaryvRnJ2ZHCIgmxzETR7OkGOtZ6wXwpOk
pDARp3Ge/uyg4Vq4LkdIOW2FEJ2jikxAzv/mUHy77UbJwXCG+Go/d4UmsZ1S2UTxEFaDhrzOi7IR
w0UQt2ERLus9LgEJmni9cHQR6mAn2wJqw4n+18vIFxLlpXIcC/MyJ8MU40onRTChef+6S1WoTq4p
/0FUv5EZmQZK8X2g3GW9GVt7CUvdD6xii7maMrBOUi5rgyaQnFO/kSFzy1Y9vyJ3jtPPxeFnX/zX
FvbG0d3VGD/7k0CRk6KfVux41sjdFLeiCC20RsXkUwPNBzVCJkuKuZCFnk0y/26z+acJyaFVFDno
lmRGqZTN28PxZyoZkAJ2n2/r0EObMC8vPB8Qyp9PfvuivxscrcokaYgJHOel74vTeXPIA8+wDHjJ
1nuo/esMKY6SQOd1Oe6guJarnJEpDcsrF9kUIWwT2PE5LSxhZrUTnjdPicCTkC7p3tfcb6CIof06
2yyb3g7PYKina1XXmY5vhGJTQaaCjB0s3yGG7c054Osk3fJ/X1iJKMsUs+LxVZz8/UIyKFMwoLZr
JnAWgZJnKPTiBQeS84hyEQYa6F8AjxuptJKBjcy6OmNGChJjOfV9Uic8/0YHoRrEau4AMOS65DzE
d58XE5k0u9alCpRVhjef+96Qu0v9AcCVUFHeqUBdMoa4wVSVj8INuOVzCbsFZgQYaAA2aSLTh7c4
zTK0X7qlItRO70PxBe9nWcAe0iaXx+zNau47GkHAIap8D3UFABUAMyOiougEwZt+BMKMAuaA+Jo7
W5larGUDk0XEwFEeHmXBD9dyNKeFNq1HblvLaceBdDlq3Qqg7nEf/EkEQbe9FhLxssfd1rGl6dcm
1BkpA0tv1XvaBjcKvbvl9BuJT/01bF6xn67+7Y0sgch8+5yCAqW7uAfqlH+yZ1/l5Kc2TINcX9Sr
XGO1b+Bx8jMuYpItFpVjn9yZl4J32fN5+KbkBmzY4Gf/N6+sGuBHLxqy2s4KPm3hcAvHOCyYupih
8vE2fW5uLGVAKicxomSlUjtgSn31jPh55KXQYVBZ4SyzUOcNlGzPmLcnydR7G0dRoAn07HWkNOMO
Zs3J3aT6STcj8uJrUF5iAG42Gt4M6K87VQ31WVE9dzqhS9plobON5oB7UyeTSinvHiWFrbBDabuy
yEnVpjHWX/pgBeb8v5J1d5iddMtPhmRwoXaddnrc72a8aHHb1xvoHLmGrAduHaESwjjeZQrZ0obC
PVus9Iq/++PpENaneu5d1lD9tUyD5l2vtLalsVBTGKYpmrxAhL4tR5TdrMEKjNqECsPe+2/Eroi2
BxJnPS+cddUmtfMUxmN7gI4m+1NMp+qN5V8DawGzajHw/02eE0kuTjLFrX2TYH+VC6eqpY0JXTdI
VTFhe2ztVnn2Kdkjh0gFvsweYfRCWnKLHXmErs2M7jwxpHBka0WuqwQSAmpRESnIwGs5yVgWoXPP
yO+LLvL0htU+vJo8f0QR8JOnlbG4PMObKOitYzuH+/WY/R/Nv/7FpxT+j5tIvrpybsoLKXsguJpP
gU7DINN2WDAT/uAjgu0Umvt2itDOOPiMwCrbDV9pp/rRpKQNO9uu+rnaNhRY6mReterpLSSejb0n
BW9lcUURJ67Ri3WZJfSfDM+zCiaoB3jO4YWuvUOZ4MAbcjE4BD0DR4zCwxoOqJ3INdW7Ub4SnZZs
pX++iIqZGoW90Ft2bLcemDP+LFT46meh5nyiMfb7ep1wYxjwl1ZY8RHVeSi64QoNcYH39SnpUTec
yaH1oG6xUu+ot7RElMBKOt4KM8FkNFBqXHToI9/NWsR63V+9Q2bz055JlVldpwvkie+9eYWoXE0E
cOV0sOWbWsTUC/C1xJmFES8inRrSPPCgDg5UAG6qkbOJn4uWmY9K+fxIArJdfPDzvOYdg+T/D8v9
tBEnyRvg92udKsoIyYxkmkgC+HuRbrHrqocY0cFFaHp4smD6dlwe/Kzu5sxCafZdN7dp7Ev3kZo8
GJbd2Ew05CskEMVM+NZwviwpOwDtlLTLVS13QKbc1gZSPCNe9Slg5PyZbU0SOV37uWle39NJ0qjq
wWZm3hM4+CSMoZa2ubQExtuNtLHZEQdKyVD7sNdryBwQ3QBglKlt77VP29HNJy+GMCAc73KtAoN7
Q8PDh34jSiqHGAnd4ilX4pDzrOEHw20Nnl4/W7GqX1yge133spJfX0TI5gs8udymRvG+HlX3x+Ly
8eyE3jRVyKuLpdcNdkVteAKUUmWzRmcJixQO6c8JPYnp+bwkNBTSm0S+N0lCCKz4SVnc3uC5ci7r
F368XgNK+CbcOiMdPU1kvxaCZquX80w+ftc7amYfqdmU0+1EfoJ8k2y8V6+hredlzCymiDyIG/ni
0KMKROYicc0sf/U6SmjoQCduYKwmH6gDa9PEhWcElUWes0kWdo/pAuiX2cKZlABojtMddM+YBvJQ
jRbDU68/CyojPgliEUr1vHVqctbTM3w++4HITmCzS5xwtsFGC+/FFxNVFgZkDXYSuCUtHXU9uGP2
IEtxn+c/UjWIfsVnnPeV8OJYXm81PvhNQ8HWtGVOoXdaO4iIiflctx+xEMDWbJP4QMQ8PEyifa4G
BXxjGc4apESWAGhmSnipPbxw0BfHcPBq/vihttHEd9q6Dq44+5Bh7mbxMGMhsipazNgkqW0LIrZn
EAeULRvne2Fi6zbaf+cv6C4LCDgRMhMwuCrYYyn64qb3GOV5z1hK1qOXGklI1sAQsul0AlN0iCiG
p6M/LJb7kGqaXr5oRXwTgCdSZvfONbbldsPu0mSKpVpycAt3kjGMBCW0v8Zfcl6/KSzW4PkpliiB
R73frAS53zc5OUqMlPpjFEh0kvZrS5QI/1s+sFLiiyWJ7doqHNb/c3K+JxdOvdSw0bldn0K10uSw
+b20dtHccSmj3SFGQO7GDuWwjAGsR5O497+revU/7etYxBHl9KwOCUJjrV1FNET/gdgXwHjPaxxs
4ivNUK04o+OupcH7lVsh34agQKFu9fbEYE43A1uR8ChkFMy/W9jUDv22Nlp6UfN1s+VNwAMvmFnN
Pm2514hmEtiyM/4EYdctxFSAcykq7QUUkacUwzjsj+1YTSHFub135PBh3OTW2VYL3uWXx130d/fP
zTAo0P3X/h3sxFvgX4eilg6oaHjg/zjUzTdXjFjx9JXlwzAgypHHg2eZXBXe1joe1gJTRrFTywH6
WJQN9hMnTibi5Qf4igesqHSws8kamY8Tw0DHU7ZaowvVb6ezjs351nZLFL6E4vU8Vxpd0IA3FmXv
HUbbtKMuwEwicqQfVdXdPxE2fP9ruVHmqfS4tQ/aIljKvLqrOa0MhfB32SCvsyz+kXkt4TJ5l6zA
Y3h1yF7+Yg97ejiI65j/H1TJ2VzMZfdbP2k767FBWug9v/6XUgFMvmdDpcNg0UEvIkDszIF7xZID
B6BmjgzXKRP22p1t/e+eUcwZgSvQ82a5d09BihKMKLrDl5i2OmkZMpg4Yi78lLo9R/a3AYXHU3b2
1JyjOFu5g2FbRyWXPCYzYB5VSWJdyLhByUl3QG52IKSjesAkQASRRobNDvmF4vzOf2SbgatIYlc3
Ewgf1vK0S62C6ABKwRz2XpgqSZknLmWyMXRgcgPIxASfhiiwtMKrjeiQ8l9OsoqPXPtC8+yAeJ7N
7WqtGKFyfbgzF8cETKNNeO8ndH8CufvC0nmqJ7rw0GNpR+x4lo3im/iTDhLiKORkIEGmQwuycfc9
40gtnbE2ArzGRYCn/ZNx2+ulTf6lOOJ2JzUxAhk4+EqVOFgXhWPnXgbcmdrHuB7Z5RxgObVaG57S
/XKSBq/5pmon7+Ueo/h1BzhMmZ3nt5Q3rQ/6OwfCCzjOj9KBDk1GIvVJphGrKKgZZjIAIRop6I7C
CHnJwxsmaWN7ZJBnY2Qj5O4EGm+6sFBmTKmxoUdxT/+Hc2uGkVofGIEjL24vIaTvSSp/53v+79KR
eK0YinP1Hvg3+TzrrVl6XVma6M6ax2BWDWbLScS4NYnaTr5FZUdeNVAdRqa0kkpLIe4k4U8h8eS/
q+w2UdvtOvxPyhlsEWcNBEeIpR4L3sXKB9CXnkx+bUprqJ40zeHSbWKD4ao2Uvuvd5vcP56d64Di
MEk4gAgdSPn+9owpO4lguwXUzwZiqhIuly8X6bsttWvohixkonstSexo5fdEn0HfQo4LZopgDcU+
6pf2xMBKqRzAacbWlRiToqm5B9KX5NhbaQEkTcFiabyd7MFUm0OhDeJ/WREKfVrHwaJOF5n2/dNS
KMRiIhT7VJ+yDCg39SrML6bbFnc9TE3cVVRPRYrnSKCCa6yXwHIXIEhEkYf1AUf69Xg5z6b7RY1f
3bq7Ttbafcpm/uDrAS8E1IfcACNDTbvAsoJTHQXvb3UjhudyGW8hBPFRhv+NaU9/c9cDX5Yfvq8U
aPrns5YL/a6bpxvAv0TY6AQGAZM7FdXVIVul2EAA7aNjNFwH0BXEebER8gP9221GD5nkFobNsBf+
eppg8QC2Ofx4ZUsYXB/gc9XYfNd5ov3RhrYGFAOjJYF1Avv1U20qxsNw1kdHQXaxMKTUo5sRkSvQ
Tocx5MNhh+CM6J2T9T5dEhfXZdJ/V77TEz3WuIgqugs/1xKRAOxOKlDUsC/BXxh4Pdnd1V9SQ892
KxGMM9jc+FVj6FnbMkh06A5IP+Wo3HWYuHWVe5lVdojLvj5M31wZnXP+YkjweYEA9yYueFji6Q0U
cPApckCObUs2prGLrLZqM/AEYRS/GmTbIe30xT4JF0jpn0MDwA75i3SIp0SLX7bjfDD/jKhUlgxC
iAAjsKRRmuirysTXPaPRNjrPhrnCXe4GPD1mEZW6PZfudkaIgTVAem5BDjiNbBnPsrOYcG0OH8v5
iHc9eT0a9mtLboznSeWPUsRAQoqtNdmVgn+I6VvKueWsPiRWyP/YzQgYswZY88k2E78KakVeePsj
VN4SeWXh4G/Nq6Q/tKlrJsi+mikogS6/FXXhTVeD83G80SiRIOG3a6ty6PB7licGVPe/K/887ui8
pNdG14FIR0rAneUd4EBGn2gunRCrGxBTkDW5T+5GgvyJNM1bd4zO7lm0HoQIcbVeHV9mnhPIqxNY
PO/uw46RK/+h8/M2UjJ3vw0KCLlvcrAPtHhF53h+7ifaQnBqoM/d4zPxPAgldKjBwxRx7N3PH74y
1p+zhbMbjo9JEpXmSH7q2BRG4LiNYztBTX/SgQi+jNQCni90WBSzlormU1s1vfBb5u043N+5Lu+A
MJSq60qkZIZqfJ3/xlSI6IVfPHcYcVZsjw7+/7aV7rNM1/jOyuK/YVqTKUz9wV+fluQ0j5vSAvvq
IvcdqoOlBP/QYJL8UuhPh4NMft7oreJh9gve5KyN/93M1ABw+3Ol5BK4XQ6jxW/THNKH1VRpDnn7
QoMHbHgrbyItxBoJOpZn3ZH5n+PgatxKTAJiz9Z2JZUmH1AG9stsCJqAxnSF20M4aetfzxNCZt5Y
FvrmFR8kD8xm35PNC5UdER4ScrftpfaV6ok3qhhN0EZxnebRbyeied9USkeBTYB8Gm2KeQpEEvGc
q5WEMBYkJLZK5nvNdOR2iTsGI0u2d53b9ed/e9fK0GSXsin+M3SyvL2sDdow+uCkImK/ztzfgsdj
7F5V0E3Zl8L3qIPIVFBFbCCEncrTkktrUIMHeWR9lEGFjUt5kZ5K/d4/sLZVFbjLKKoIRO7JKueE
5gnNxpWBx74RIjbqUuQtl8rWpPs3UynlS9TYYo6zIhlmqCUArjGnfF7n7zySjMezLDepNPMGZ3LS
Z/EKPSUjaas2nKG0+hB8MTaHuJGyNx+Nh3zqFkR6RXZTT0e0U1Oeo2TLVDAI2oYMRhoJTuJ1ULC7
yP77/zj7GJcjBpPIRbvUmxKbutue7c7JD/b57Zd/JQEM6wHMyfs67dpemDySMDGmJlw1OhAuu/1N
k+UrMQm+JM4mhlCtqgHivoIQKtU2KR+BG6YQ736RiyxILfktJvL47qFemcjXt4H90EtR4Fm4mppM
C82WQB+33L9mgkHEBhYoAJ1/gq1yZxJHuk7LtHw3gt8xxwGTSvR0e1KUlot5OqPoT/h8yXIuB8cZ
Pxp9ukE0tiwT6cxXAixfZgtJKJ42UR571JJdee9ANMwQXqM46m2GYAlcIt3xHhbT6Fmppvc9Pxwb
uSVStvVdeY8uWlqsfqTd3Mb9VaCyO6898kq2eQqEpktkrzAH9ic8UU6e92ysr57BNAybNiRU3ity
y5466pdlglRNP9kfdlfTFL1659sbFQ38fuF3EFKRuNzGq+MkZBSbmNNxuqwECkKyYPd+uzDYd4nI
c9fJ1i3j1sBbLCb6A4zbBGHc3eUFa0ixQZoMq5XjIM6nXmTKm1DD3bIZTZGAUCEYtcbkjLwuqBKT
sg8Kwk66LSqFYITmCEIBDthnYo0Pepslj3SYUklqAIcslvCiz5whkFN2nT6yEiQYFSnClD5pjktn
HSwT3iOl+rPVZKSd1jiM7fLZ4wCkQD57nlfjvf948ph5Sd/yuzitBDqQXfkCftDiGKSN9TBtKcdp
L4b94LRifT/I9ZFNwLEVoV37OSCzq81HszNcTSQdvfAVLCxqecqFDtp53Mx4vUwHydbcTJaWMtZj
J1mfu10Aj1iCRam2Tyg/8ty+vYcqTlc/iXAlfgAKO2OIabocnboJaz+wpMqTHJaJjYkkj4UeHBfg
dElLm2GrSAiWWveMsZcY47TE2lV3bMfkRhRyRg4qp7QjCONgjvgJzFiNJ/xJ2qCleUtiN3MLphZt
F0kK8aR9aYGStwUQ6+8UDe49Z4KsKAf7RXHci3gLQR071rJ0BGNyyILNeeEnV42Ub67dMCF9VH7z
lAmJ0iGodMNFqqMphzRh9SkSr8Ngnj2YqB3Cr5DFmPboLgSu5w8upxTAgSAp2NMV5N/9cUoXaURC
+eJwZwlCaXNhzxhtrKcDw2MKt7Eckk+GsE5776uwCngANryK/Qs0aIKy8vS5HszMl0CqZYO5hB1f
0+4gGGpjwItm3zpekiT0NGZkIm6a85FnYVuRooMd3vDTSfhaprmMpeN4v2HC5wWkQR4z6noOwoRi
xF10/DvmyXXI6ZsXCbTl4rwoB0qrIA0u1AA63nm8KAz7YM4D9lSa2x9k59sQCZrh5r0PB982dWUg
m39KoSZnQ42gLxJzFzzfYOZDRHNkCO47TzCjJAN+VTgGxa/qJHHAtHcV15QOn+tQBf0I1p9W6WZY
clUqCUEgtVoXmBYiv9Slv78sMIGoUg1VUeMxPtck/Hl9Dn4JEShhoIIjkWWcWuy3j7uCSg4qpI2a
5KCgV2K/U8HBlW/I1BgmjYDvBS0/eefWuGEpCGmkoEoyJUD7GBvRjVshyzlqptlScomICa3YEkfc
HCxyaH6enMTo4KejgXQ7Td+WJzF3hHxo6PHQ2794XoKtyyJBiTq5bdQG68mc9UdLR+Ifi61Jo8oJ
ALLNloba07zIihSrunO59W/IxrWLcMUi+fhVhwgJWaTzlRkI2G1oqOIWyWl4o8iJ11l5G+f4FHM7
iEiwNJJR2pMdEUh1rVbp0IunSvaHvBXWOcoTMx756bsGjrFT/tCYP8DkFdCgMMgRJf8E/NE5MBPi
5qAPIsmRCcss7igtDZY37FY8KzWE4N8KRWtS7/xKuG+FY0fMYNLXjqq+9Xu9Chv6at99xlIc/ROn
dvowz9a74BbnSvh8cGGKoGrFAKKdC2hK5i7ALZVVp0ZEQ6PFBfvii/i3hpUy9qzFC7N8No1Dak02
pC0yNGi1SrtYNnMvseOLnSmWxTIxfCcG1EJpC69frsZVqCn7319GtwxHJ2RxxYZ3pwcmY8VlEQQH
VgIde46+tikvbYINanN/s5iA6aar7S4Vnw7jK1effmpEziMG85YLSxJBmSFxzp/+mILybiInm724
wZ4VuQVnVodB1Q/570akQKWJwzZSAo3BvUdle7jZMSOzN1aSgVat6UmuDoLe1dwr0tEHvGG/emjO
qOMO1cpBLTqLR8s9nu0WCmnhW77orq/agqiIQDVBB1QmSOt0tgtzGvhiqh+XNjdzo9RbVcxXAnXR
VY/j/4G3xbeK/wpZwQP948WSr7IDprVXV1QxS26hABkN/IoDQjNehmYY4j3ZDM5mKz7Z3qRZZNKV
MkXNkLoDSibg46KhHEICOyiP3JzvPHYt1N9Ze4+xVO0yOIs4GPN5BQE40FS3PwUS5fNJgfl9d9xe
wABf6Yf6TOaHmhRbj3rf4NT0gwrDk7L1Wh6811sBT0L3cM29bBQ2NR/Mm+t0dahwrtJh17ax1Bh1
lQ5m0kI2Nu7AMQqBJt6zAFz04YWFBjB9Jx5z2Q5rx1uMIxCevb1miFHjXR97Y2yE/7reYNcSoX0W
MMrWbpMQin9uYzd0ZN1qYksMsiLz6ePPiPBRIcRJ3EbLA315QNRYZ8kIMpE6oQ2p1hzI6N9O2v81
B1pBFIZ7Z5CazFpfYsOmss+S23tM3K0Oh6jyx5UG5l6t0TZNglpAZFrfyP2NaihjJrNKy7kb2z7N
TQwsWI6B1SArx3ur3yK6cSTNQc/34V7ma6hcvihE3Txrinf6oI7/LXM1SiaLJTEN+pIncpeKWoD6
iQr1cSjp2n0Bwc8hN4oAaUYDbWhLkzLwl3ffmLrtqtScsbih4zDnLaDSKZjcv4foA82Tsywrg/pv
q95m3x8MyepCanouxEcLqCVwksopuL0oZj7KyYNT5uWBpkn93wGTe0uZW+VCrZ73W9YcH9FEyW3q
vFWOI/035xDctZgd0R1Of4kNoszn4C/GWY6XkBuk+wi22uSDWwO/Kr4C9kJIrZvJ3Pux58/DEU7M
R82GocJZ3Fgy3A6D6B++/Qs+vSPvoSLtM8M2yf5/jljNkjkpQgWepK8zG+y5uTti43U1ZchZGjY5
LYn/4Lrp3bQUrrJRJb2Mfq0rmp/0rhrnfS6Iry5JYyIga7c/HvxqZay12hCDRkf8m+po3TV8lYbn
ED4Fts03axVfuj4QwDvWFlA/0PgkGQZnlH4D//VdKq/urRKihkmwkSephUx0/Ed/U3/pvvGRzhbZ
pq3LYwgsQRuIQ0Nr6vpQQdzIhRuuPdlT2MRbTw/FPCn8Pzz5bPU7c1BdIqfveSh6d3NF8QLqTJ0u
/qvpORrhMTE34dOv6BValm8AOGMF1DJkqbUkZH/HRGsYVPfIuqGxUzErFs+Y7kzARUy2sdz2JkTd
f+vO4o6HA/xMDMqO0k+Gh001yqHkFsimsSPUSb2gscyJ8Exw6inXAb8JY+jhf/mEGU5Gd0YhDLRw
Kpr3NiG9xaIFE2MyFkviIsMOS4zrGZNbBZgSHI7/T4lBC3bpR0ocrrNsKi4sWOiG67rtmFsV2vp9
mbHHTA4vrslsMbRZR49uksBfmD5R5AF+qW1zc3SQVLRHOvXXBP8VrsQD6YT2ervmexfSWnx9zZmn
sG6K8l9xmFGNu97pwGyMpW+nYZQ5nUkQCd29+fej+0fdVcF0OsiZCzGMGMZulZDgnNrKegKJqHwa
mogsNYGRxV9ZmtG7arq5xOPxj/4yAS/IJa7qh6YpNzAlnXj92DJqnhz0CwCq1DoFRd4RAKBDv/B6
MXNYWvX/3ysQPK6XSfG5cAcq44PYadlcP0uVdrbDHV6ck9S+fmJ94Jcv4iiJFsjRIQl4u6tVUcxr
mcMdxCO1CFHdlfVtgV8DUaH1it/OXjtvFjslRmv8yjfRplAoAmbJWyJiYpvHOY5H+2z6A2Ti9kI9
aW32otX+U3C/+622/qm+WoMT4oZWvCPo9o+FwJFccgRLjR692V0xy0ZNkIXxTy4tBZrQBtPIQEPp
gfNKBvZq/RTOTPKWK+WRmysAFrjBemeeMnTHacNDdvn11Wc/rOOuQQSHgASUCeK3nk6WrW/TgMbG
fDDuXotyxuH99dX9vCASon9Cvm2U4PSWpL7ZPxLgV0qHDzVAozQbxB5W1sx45e8Ud0aqAXEQsElL
zJVAgZn/3vAn1ky8QhAx9FipLUQcgmlX7ANZnrqfpCFKUe0oHBW7sf2hXyOPn+WRuEzRKZSTHM8D
rraa1UVZgK8lRaD9eIKVhlIXVNgsfjC7NL6JHsaVjyLbRz3G9f1xmLOAuUfCFPD5aF1oVWhVmiMk
pwnGJ0FsdoZ+FCMHKtIe8MtsqjLkn/Cv9uxzX/bGijVFyQtsXo/2MOp8x2YHpBnKwakpQeMe5/j5
0HdCo5i8f7fR4RXeZUiHRu/83+nkTPth8wbWN+L0Ima5oy3hgTH+lRdXfvIrf19nNtvusjeNuGUF
39MM2ec3ztOtxxIB84xLaMZJRvyCMLy0e3SMDumPLJ/dTZ29XK94z1gzftwqw+8CvGV01RPx05T6
l0LNJLOUcWMhUSofsXl2gAkMlaZwdvZ6Nx/iDLYix1v3bgWrUf+++MCPgK4rxf5/OqZKbgIajxbP
XMAxiICiLI7bJEGEwTv3jn9hYC7Mp5vj5KYESBfcIgtD5MAaB5NmL8GFiyZ3Goh88XQkZ7VTeVfB
KRbv4Pmpqih+dLU7NHghKpsXIPkHdu7/1cqWuVLIHBxRhn0bTf8eE+Q0TnAOL8ucGfm3LEOShK5B
gVojTfT3umzXdq8fs2SNvrcFH6mWOWN2e+/MkjQsikM2vblANcjlUExjLmm+fU5GvTOQdajBQJTH
QK6iIu9EyFd3v6xSQ1ZZZSaZvjtGsnMY1vUx8CfMD8m7YQDWgEraQxlOcJB5H7wmtjudTVB2b7Zn
DvoNly9dQ7dW7dFFo+QY4+dK9Qul5L16tTNqtTKiSZ7aQLN/yzBHvg7XGma8rpOQIu+xVbMUFpa+
mzEw6DOl+M4qHFvotNHHtrzXlFGZ32atlVmBNP2Y/+hNcY8+fvykQTWOrMDWpoAg0dQVZ5ybx1/o
MTl2VrfouD5ru9epZRvIWhZnreHoQBrrKDkH5VIf5zqhfyhIB/Yq+zLa6poQXjwUaO/NPaF8EH+T
514urhrkJqZeLUEa+fp/Q34Ao/u/AU/EzC8EfCBKomjb+clZKVC86p48LerM1Y7vTqkojKYrKejE
WlXRDddU2xMRj9p3M5qgXk20i/p8tM7jnlAU0uRNRQiA4AAl06pUn2uUHKvNCV8370QcoQBZc6a+
Dv38MAESRyp2EtMpb7lu1HmNSHjurcLFh4mLHnRBssCr2yzgEaWEylsz0HXaCfj6NbXDvzNVmyLL
OPHEW+H0c5RHPCpISkCyCCVlMtgtEhfjC8Cp5tuc/CS5c1OV/5115ew1zG1Omx/P2aDbUHHihkYi
fNjV50NeQfgBGUkQoR0zWn6y2rTNnvuOsXAyBxF4sZmlPWrZmabEs2Zm759kKwtIu8VO48RahyqU
7Ir3IBTuXzE1aUq9VjzKTo6ZFU00dfqGn+evYgu9fAiqeW45yHY4FrahMMK7LAIg8We0aCxfcPGT
NWkQiF41gl6O8lNV4ktcqaCU5O0V6WtwOo6bkupP5x5oc/3aH16iHBzQLUqLB63PNSFphQagiApf
Js64R+ztC4MXE954ECS95tPCCxI1uIlLXfnCCRVDXID9+g0PbNbmJiyRHyVoYy8GffuNpwUrLThI
ittTA6jkx9g1iDa2+5kcdM6FRQZa/++6XzkuxRy72PDVwXZY9zwaRPdIZRlX/q/ddqyF9V+JX+Cs
W3WY4hh3bvMKrkZ3zKbTob5in3SR6hbJtoXNBq0z1R+5vxhsK9Y/JOpUjBKUxMfuZmbqPoLZx4ZQ
6sD44wNfCvOPmGNtIEYlZaoRiXu+J/b7aQ3FNP8aQp7Ro8sfYwSEpkHaa2hiWuP2gpAGm5iwOjJv
ylaRiXSnte175CrFF63DW3GILjAv4XTBJxdaHOBcVI8IEDvLgXOPIrx+t6enGenDPqtFcDVUGy+D
NGa4AHh6AwGKeR8KZoghZKujtP29SDCXJ1tzmyJVTEUpouivRWILprYaqihLX6wgz+U740jBcOSB
oG6Bk9OKD4pHIrcGQ9JoRIm2OlSN/TWnjJozXR994GN3Um2pSsXhKczO6DOzR8Zs8gprRRP2dAxJ
J6mvROMTJFgUR4tiHp01QJEeS6B72GKZvo2xNxXu6WAJ3M1LGf1Ro8PlEEC2NVvfgUHyD2c+/NWG
sZzTDZy+YQjuQa3xEYCsIMcPBZ8dk4NngEmE5YsNuUS4AW3nMGlvsan/UhIRYwJhUKXDUfdi7FZU
qlKdbc5+WxJSqjHvjpFgTMDzYLVmC9/9m0ygIZyzbXBz5yk4l6cwpUzSKOIbklQgsVdIUSoiHgVJ
SEE2CHeW0sRGnIrstQJ6grfzKZz2kcBacVo2sj6BCo8PfXSWLPGc05wvxb6qWt6PS+bgsBvd0p62
81RAC7vK+4wpiAn13mwC9C4VPqKFycUM7T1ZNlu+bRM2IgnqgTbIFVK6omoAVnt2B19DVDVmjdJg
QvZfn5HDjbOCLrK6cO/1Cx2KiLJe++/JRtwtqtqMN9Whk9bOn8b/DVTUF2GA938RtChXbtbBBb5g
7L3AQjQpMUMl3QO6/dz2UEJBl6Q07BoLut9WUjndxKg6Tyve5rMjbZWpjSYsZgRwJHPgmZ22LqU0
Y/e0UCFx0KtafePNna7phMNxO17QAPLZlgZF7SRT/edQbfIXQoD8eaq6OFligA0w489cy4iz0qRX
rae2hIsIUIrIMEU5wpjaaQXb4xVJsinx0rb9J5c8sTL/wi6Lpezx5Tyfn4mSa7KxxkH4KA6GMtbb
Xb1IAQEj/tfMwgAfezLe19+rJAzPj7N3Nba/V2Z+qCK6XLVEnNGtMAZf797hEhRKDj0QQELVjHMK
Xdfs0IGuyrpTWV8pNn5e/xwSa7FYsI8H9ley12hGMLgBNvQzw0ORHMv90wwdiE534/6EOULvN+Fu
9HgU+3JfEzfXF9fNgZHRkUVD7lnInblgbsEFwfLvLlo2qsr8wfi5MeEJ8yTU9452i1r8hREEOeHV
Tu99dYutTCwd4Jr1Me8sJ9WHB1rXuvq9FugszAQ2Hrrs3mvDuaaSAbnB8qoh1H1zPTpDdBKaqOWs
igwPaZL/GgXcbbohcC1YQBtUcnLTvzj5KA8SLyubHVoE56vrTe/0VhY89gexzNaP3xLs1Z2+EV+H
uZ/SjCKlRtsW2RF1wXWk2scUg1hfDn5CGP4sEsBi5A79P4eXoSeG8CvaYgbmImAENCU+i9TdSdKA
osP4SPl1/F8500rncApA1fy7wKn7E0MJON+wPTya0+fPF7QZQEmFqXzA0aqJ5gT/eWCuqA7Whcq1
6v9FtD25wCso8+yMI48nVQXcaeFM0m1yIIBeqDxtt2qHxxOCbY2w69YwbQqsEnynXb0epPz6F4BX
RIsBsm54F+olBKyVxha2+wXstB0zgKJEOznGg6ua/GB0+oSaGCaHYgBHUk5xU+djN2AtgHG5PQpm
Wr5lsbaYAVC7a8fUD+9en+ogpUtSI6CvjTNuHoUAbbO3gYynv8f3yar4SjI6Qo1zhf56xotWUR0t
kWozUHsUDiNVLuEYpQljh9AR6TfXZgjvx7W4hvQK3GTbtClCpoGPbmJ44N8CqQVP7xsICMxdHTFz
7WTy83g8xc7AETiShehcv9P0PzBw1ThEhpGDRXaxbK0HgUVSmDbj9a4Ol79bdl0YnlMjYBlwwveP
G3mbiofc1v+Ax+TWmmRGrK/chSHOhHUtDT2J0m/U+vfYZASDP9CdyiftRGy7zdpE3dRjg3weMbVL
5XRuvDVJ80VyOGSM8/re+WXW9/nYo9PnJN5fyVm/cJukmT1HzA63qq09fEtmtf52Kzdxb8VGAGfD
lFt0lzWRkEwxPYmCG2o1C/aK5j/rIQXxCDfcwM2FP7otxt4vc3PcsnsY/EnQV2Fw4RJ0iBic6lUX
17985iAvE8r4vqLHvgPRPzWiw2Kb5URAoEIDC46VbMtxnauEzI0ennMoeKPg+IsIpaUHpQy/te48
RYWaCYgtIEramM0iUsw0dZ+OEyXCg+gIv6NpuAuN3HZya+pg5jJWBQf/ie5hJN0oTdBc9x7X0436
ULXr4C43pERV97kY/WsfN77aQ2MmF2FW6WrDgho9KAe1+JUBrZo1n9OezJw6Jr8WFPGTa5ei3MeD
7XJQgg41hLPQCF05Ps67e/c+x/9XIhAsSu3loUnNAHrBstVO52l01hh99zdwDKsr+BNt28oxHrvI
2YmXeez36gt7IQHodjVjNYlNKYB02aBkY0tGZKQCjzRrrr6doqo35iPAg7YRyRCsE39IEMkftNrl
aASpw2eCXiyRifF74zJwAYslcm8rnTB0D0YnybMSL5TLUPaCeqxSZzg9r4/6VO+3JFT2MMbWP7Je
ddD27MAvHmZccN7C8PZPNEXKqjgRW7j5qotmTNGuSJVY6rmKX1xixrmNKslUS5rwNeAqyadhLnpf
cpNL6hxd0abnUxVVUBUcgbijDpd5wpyX+Lzt18PpMAK3ktoAwrpdgn9MWqIQ7lYyMj/hQibXNXwI
tXrY24tGaGiuPGfa1OyGtRdBL26Bpbk9eMOrdUzCpEAVbsLKszPdt9hKS8jWHBY76sFgOVFYOTD7
7wG1LzxSOaNiNKoLBDMcsuCGpUA/q++n7XBQ4NMPwghY80Q+JJhAppYRkidUTeCTr3DlcWKem8TX
iz3nTeIEX1MsC+Hz1BTnOk+QdeG6PFTOYZBKpyEE2pfHU8WvuhJqjlGrrx8xxJA4oRlkcbLGF6uP
vPR+mXTTsqalgBz68wZqdWpl2OdoCoyeT8EhxQvoqVmfqU9a/V64QBhyOqYhks8Cs7wKJo4Y/sdV
Or5gBOhAf2Y40kUFcc62XO2rUlXmmCn4+OF6CpCVzOYx0pW3zdQ8s6416VA8LfE2X9pSdV/SWJPn
9C3Z2vMVst6TlWiOxFQxN4pJOskaHVjxAMXE8Q1CETp342SGu2Ds3DgG2B2XOpI1nLyg/dBgJRkS
xbzyCIglpjrEkbj5OUtXj4DUjDlLSC8vdy4kjjSG/3wbU5HIRXg4S1aKokxR0ZKcQ+XgNMrPYqn1
1+mNqinszMh9lcTfwVnsaf9q3r7xO5PFrPEm427EdNeA3MMglf2yvCf+Tpvz6c5oc4aTee8LTqub
pD6iENjQjLAobF+3Xuw4Bb7xuSoEsq9cws//wy3CAQFeLgbEgoJQ5c4TSmFRTddnB44cPX8ROQQG
hZSGe+t/gcLx/RuI00m3f9Vpi+0Ctiv3ABnga8bVTwhHSQCf8NITvAasPJwg88RvQ6vLb8AQ4pn1
hGFL70jvfBVKy5as7DHHHvfs0uKze8E+XoRGIzwFSnVtRtTOJgcrmjgzGtCDacVgsjZpo2Q9eYZD
dcf9FyE56c7zuCsUwf0EwRwJFBPPthOmJDrUP7avPyagBtreVu19OGeDg2EXT2DeMaPFmlpsXh6x
kzx4OdpF0HQwqcxe2/qZCpHRJeuwJgiTPdNWkiFqChpMJJ3VGp7vQ1S4og5jhSZaukvGfWP9+ZId
Hwy8yyxSOojx1deqXA8OaAPtP+4VZs/Vdgq4u5++vyKv9YmrWr9BJ9KoyiNoc4D3IENJuOlS3Wbm
uIEmhRFRpAHyp6iCO0VZZPysSe+IhGSgWEzt1tYTcmBdqHM3Gh6Sqbaadz1A7GMSe9zN6F8d1iTE
j9H3rlFtXumJfEB0XvPVpLIna1r5SPCroaaZM5AcQh5s418Xghq7EnLyBMp95RWmE/NwDDgP2dTB
M5HR0Z4N5ivQdpikUVT5u5sDsKbIHNMmjM46DehBC9w/0ElTsf02Jat7bfBf/T2774hCh3nf85yg
3cir3u1FvxoRzGRQ0LWfUGC/yMFCNanzEXswH8gOVtc3/WsN7sZpDpg62RBkhqtttU1LEIgZyZDC
HYRepTge1K9DMNf/NVuSG3OciUL4NGHjnR+bwI/8uNOBGX4dWPizuWWkzH7Vht2ivBNUUAtZ5YUa
wx0wc2EvFzNd7l0Rg9+jz9UKXAH5UUIxcB1fJkFkXvamN7GbiqdRfmb3Y2bF53rCFV4CVuvtB9kt
3KswWnqvhCYw0QsihdnNl7psk91N438Sxf8Gd0nlAoJb18rTcd7K7w9CucZOz1chokYLkbJ9tLPQ
whkT/9/rW7WU7YwEMihNHwTelaN1gcHa8UAc/Gjoep9nvNviqEUZLKaFIhQngU4xHySn5ELHi4/X
YhJcNMZ/48EBf5/6VQyT/h/E6WqVFK0DcB5L9I9pVW5WsJyGStaRozS3jTXnzfOkWfW77gza3KEX
jFvcWAlB87LCRGNvI7H6bHFeZqZO8rKhPgo6OeZebgIXL72Cl36N++zVDMVaoPvLppcJliMOGFzs
j2Q1D6Jx+Fs5123FCNo4Y2KaOL+WMPUMH4qEbyFY+1uVgbhpi2Y7PuhErpPn0PXiHsG//eMRmGlS
EK6l9Btq944LUKTgieNugOS1KRx1jcF+vbOi5dimubmWKIwTKtpztoa2PYvU0IOqCuZgOsk99SZf
OrjcyRilcNUuGcgTW5Fzxlm+vlZkky2pZntCQtDfhv4Q/zx0ZZH0vOa00i8nyhldEuLqk1MspNjL
BAPc9NA1o7w24hkFkTPzmzuHSYOmauqzgsmB2+ov3Rl63x2B4J0WzQpjiRygqUKkn4OT65iCLZpQ
ChdHrvu+2oj2RA0pgMEillm5gY4ambFLrjFGoBirvNr0w713WJFvncGR+rlsugnFT3vOOWClmip0
hHTRF5H75FKVZPWhU7G2ZURFW5tMS9uddiuTy2CgZHINIAUw2secPkI8XRuk/HyT/u6dUtm1EomD
oiXRF7CsOs9AELQ5s3ohe5xXLTG2tDyACiwkvcumrSOGyASIyM8tBfqes/HVlqdEpgAH3ifpxlJG
GuvHtSQKzrdKsvSs0nIOdpT1ay5ZKXFO99zPH7ILcbT+oNuf2P5zZdmaNMO899r4NjLnCpqBoRTh
+aQ2AD939+NSmCiYSz/0XjgrjVVFxRAzs87NeKQ1M3Fd9NeeQAPe3BiZ8AckEg//doGQw6NPaGaE
OoMsoes5JUKrnkPXISHr+Y0WQbvuFg4bqMbtIQ+9qOgXrtPQYG6XoxodlPHKbIi+gHWouA6cvbz2
aqujgyDq8YRqAwxNDSFz7BpHpZPfZMphGCRGfg20vwBWBqTe82oti2zqWKYTjoOy6PyxVAp3hSf/
oKU8ZtzlbYVIGzXSnokID/BNfcBpEvuSthbP8t0LyCedWnUcGiMwObQgNKFKDoX3l1D+KuhpzHN2
o96xN2Qy2sSCoseW9J1vKpn4+6Dfqom1N0d7O5LAMdfnKNchYHh7wqZFzyvpAQ0k2Dzi+6ftwmte
/11lbIemovoPg3Czk3E96twlObSQmU76zNRsstcnz2TCZSXUbBHsRuGP90qcZkenzhMtxIB+hxV4
n4ejyqWCwblTu12sI84+KBsob95Y+A0dTgwCgu8iNu9/v0uDRwXo23KwL2ncEoh5PuIe9Vrz81i4
pjVEtXfi2cr6YRiumQH0cxcg2H+q5pK6xmjZEVQcJ+WKwWWan5lNLBIf+g071GPcnb2t/TxwbZgi
biuYt+GAHAiMg3I+jXPSgr+FFeVNbcXujfHDw7a17S4HrzLGqCAksIsnpnTt9+MOVo/mLoBIzYKj
gcvHH3/a/7ekWsS36luVCK1o5UHJsNrmFLDmTP3IZqB40By+bR1pZcGVs0Og5TaxVx5wPuVQBgFq
mAn/i9UUSofOGeE7gM2VUc6ksZPGpKklYsgytXK+XOumoLxT9fdJIc1gI8eoL8W7lJpc8q7wjWHw
Mvtd/qkrsz36+TgdlBkz/K84ucJikDjBA0xhYBL+Csj40QYStQBYTs3rmMqYJv2CFnlUHNOnI99X
QBJ80lWumD0Xcr0x3jr++1ze5r38mBcbTP9kcwfD/9cFSpRwWRUp4iS1viqn4EiPTaKeoK2vFbVW
mREmfyi3B1u/DDo9TgsojGJx/OlinizCQoUWU/LKH473gnuzp+nFwjXwtbqfY8PCaLAOTxl/L7eE
oIiWa7cOnwkyH05hqMG4Gx9+/tpfzFeZrOkzpa5QrU8BVO3GJqwtuXaYauzrA5mVObWgEzzf0soh
J5JoU6FS/2VgiPjdSAzcOsBWBYrZ+TzOvmZsK8xfqUt0V8Q3BuiB9DcDAJf8AFujsAoOKFLJ1fJS
NTDRo+SWO9caTfUIjZl8WN4sQUTOoa6YIkCzjdhHDzai69xPrRTJAMNCTChxxoKzmA4N7dUoLxTP
xUR4nshvMWWTRYlNIf/iZ20X62zZlRfm2pokAPuif+INSCPeHSgx0m10sz6QN0AzR58o+INgDZ7c
p5O6X5xUT7+hpjAmGSdAzxhfgK2kf64xQYKkYS46uYD5yGYPeSnkcSNfLDTwRSIfCtiME0HcxLwX
km4uUFUNvuuKQK7BUULnyYK6mIgK8te8gJB7OkqwuV9NYGyTU6vBdK/9rEJa8OuvrLR9fZzJ277/
PiCxjA3QDZVU8mp4AUvZrTaB6gTtL8dQZ+Zt3F8gGibhEpB/vyz+jo1rpqwDD9d6A84G2aEf0u0F
ebtO4QLYIKfZGXKlCzS9a6mNDFMzN9Dyh6+i8wDanCSmTZrduxtKKdkeXzL/rjMOzX5uv9Aeo2Li
mM0EJVzizhP17AcxsfxLcr8ttNI2ef/VUgzLfVPECiN56wKS/QHTkV+O2QtjFmEnJsZL92irv4OP
MKcrKjxuzrFWYyWCXmdFc/vOxWZ4GRxkzIfBCLWGxLgfWnDDCJjeycfnrlzEfSLAJIj1+ipRKdJQ
mY5BW4R7xJd0HW/xzLE0vracFFIaBG0eqV4CDb//y93hUjYRBOF9boZQ1rR1iMasHZZgTDBJF1Jf
LOtk/sFWmGAMv7TGtTycnMAxYabqOS4fbTVJ+90r54v0S8oI1men/01lXPLltjtLg27+XE10wxMh
ooripN5PHazEDa1KY6UcLaT0TtR3Ll3vwdDw1GMYCIjYoNszPORFYZWNdHpNKAI2RXnn2K3eLYZ8
a/hXR0J8iXkJ+lu2b5w09MxoJOBtV67r0r8geZfEispsw+mZX8EYb0SGdGCejSrH+Mm7JMd9X5HH
0llgHcDCBRMR4EMopE5xG02Sxbn2mXZmgGyi8qqHgY9VdczFiGShEnyVgs/I44t/y6HAnVS3zPDR
PPkzWp2taxXrh6XhK/t3FNExJCs/LPRtC+qkKqMozd6F4ismef2R3hyjxnuDGTqPJwo3G/JDYPWy
hVgaXQ1HY5Luuis2GCVj15hLFTibwUvht0LR+2RMnvjINwagTDrGwe3BOG+JJK3+jDPsgUnoBFzn
kyOjfDmlkA/zNeaYqEbXrH+RdBfWrNyPskTwvF6vV6DQO/xD8xIvOhEFtG5K4U53bCEZ5lqMLycR
Mmc6tlOY5BV8U1t+soVb0+S0mc/SSJcyhKNAkLYKdvN6O4O/izQAmOhQPhj1OdMYlmRKvOVkrbWU
tTpJIc1MPaRl8khPZpjpIPu9hAVtpAl/xN6fHr9ANXpAwP6mMoRV09RQ2TlrGnEIGzd2eQLIGV7S
YiEDJvKc27mkjRWVylS8GocZ533qpgFlLQbJXqNVWQGYwUz72TY0EC0+ljZM3HJvENKOOvBhmGjG
rddD+l7rO5YWl7kO3IG7j8IBXlfvkBAaBNVyR4/AR9BODWADusYCIEeb6uL5SM8FfwutV6zxu0Ee
eWaYCOJbZ3pEa9BIlJ4oWHBGa4Jfy+TKZP0QqBdr6PlixChNH7wNBC30SVTJWrVDDUCPcgNBBAA7
9a24FuKzzVOnCkZOLMvXbkyKUhCztR1P/SO+IMdoRFEgjVXG5tVjy7D7FZDkxmpLgh3IrQn4SM5m
zTvwnPCaUgTzs/IzOuwDuLzICGbWHGSTFWaLjCe310jUoRGDivD2S/x/OZggUN5rLszw/ezzclJY
156vfHLFCbkf3ZHspciv0haIFKPqlITL6D689SmDztFmXNbrfBXP036xa/lsSpXNDUHgpHUMov33
dL2PANT3ezD/Y8pqwsRueFa+Ma406FQyjci8bfOKWm7QP4rt/RUyFtxuhlvPfTp+YR0Skz9mP01h
+4hKYmE2VI1zVbF2zJQIJRg0RVYbO1HZ8OcXZk8y1go0c7ffmE5PLXhEmGpo37hLVk3+gljffmAe
MroB9Tp+vb8IIsmdbZ13fquorkW16Oti5N9eCwaHBx9nt/3W1XzO985jKl6znhjzGAM4RxFgFAOH
Jum7txxdcQUnhXvftSjVbWQ2Xig6XYZoovgOrlpH3tn79bBdI5/wZEe0gVT6vxWBk+Q+MN/X5Dl1
BarG8ORX6vzIIknbJnmz1mqh7FltO87PnC8nW1W3ZbNcOWwDq4vU8SDLOloNF+Te8lU2rlWjcX9B
4/f3+LgFqRX5MKTrAS7z6QWeZi9KywxLLzuDIW5sTgqUn8HCWsegccRNiNv/OFXHwHHxrETNXa3v
03p3ojNTtP5z6WV+AIXG9PB4LJPKZC8Av9AOifIhM9bVEEQ+kLkxwflqRKtJZLF860QgodvZQk9e
CLFhbUiVOtnNNEwxkqtREGk0Qhh4+XhfqGaCqCFIoxB9SIzIK3q011hkMsrg0mVtf1yKYNZK/cJn
BwUCYpB7ipSNZ8Dx8M5ZmOvAxLvmyyBbngZOZOJcFYhyfH+0FpHnf4X9ty3t01Wt31PXLMvF7zZn
4JLj/mkOEtYQX5zEOEnqJxhveM+9fpm13f4OIXt7dcZs7H2qyomDU+PZNb/tkvD3ZeGt1y0fUmvx
NDZH50u1Ekk/Jukwfh7G6CNpvgmyU6KK6oPw4IN41MxM1LbpVhGqhhMKSGH1fqXdNGEKl+dyS3ON
h047mtbxtVSdqvB48CZnNl3tiba8Rtbi8oXna5K8beLuM8EJzpBRcnLovbAC4rxXweC062koIA3v
g5fNnSiT/J/hsyQIeC74sfytuSx+hrzmcZaBJdVKEyAs6vwJV+uF/KN8UUZfXOxDfiSZvNgAG9NI
dMpptKfOMlPjoFA9YW7WdRdQSSUxMfwmhPJ+78ISf8JAAAi7QKNEntojiryUf1H5q27JAcP6u0th
8Iy0Nv1NsXu9ax9RDpffGRelP7V5pk4D4tI45WooOpA5LLW898+0FHYijTUzKY+JX/hCRfYp5THf
kzqzBfmVlJ5lLCAEU5LZjBTsMeh4D9zshDQqylYdCPP2NqF7HnTzJMf0/ayIb9SqyOF7mxO5cpDi
TCIuZ8jePqCGbv0MNeIlHRH+4/qwyus8Gqe1gCdn85/yvrkzQaJrLvHW4AS0RlEIDSIz7ixQAIOH
qoLF0QcW6QvQS1Pwiwd9ufEx3hTssptw3JkkV9XXi35G8wROFSBRdKZqGc2MmSV5XvvRhkederBF
JE/DiTJRvwLTQbz2d04r6mZuE6fHo7bMKcDKVQdlh/Qs7hAKGF0Xl2ypTdAD3JlqYnHZr4stHOK7
VnaHFUsnaAvavwIhocWCuSRU8v5hwX8AHOAHnRndYGDt6CLUDPhW21XPKKOABs1w267vBGrfBkwB
nrg2A8QMMfbzFKdBQhp+/ohdOlkQO873IGqtUPZEgK6pkKzxuN9ejYnZx4lXJZYHZxbea5SYXzOA
r7ZoG3YlIfAUsRWeqxsKsqnxj1kk4ufoMJfmH1twyrNi6On/PUI0gRyWkdJmKgBOllxhZmqJ3/BX
0LpjCDcP3gcxm+QlGDGpi6ThlCS5fB7AcfCs+ltfx9Mh75/vCXvgZjDNnawFKmsHh3dB2dM/aiLN
eV3X14k0X97sfFsSif2by0kCIUstP4KztLv2i64Sb28ghcj0ipzHGaeJTgM6UPT2x3igdcNB5/En
8upBDLRC+1iwBKDnBwPTh1ptdplpQaop013F2mQY6ilnYaILtOoJ6ig/ezjFNe4uODpfd4VqiR62
nw4C/PZmhnaFGYMn9bN18SWRBPVlB7TsmGCyqmOXQHsZkmwUhzk8ez4gWuxcMwaRW7PQVXTn0iNL
h31iz+6JzpcTxIMz4wbC60JLVzmdRWzBVAbZW4jQIEoOZ44MNwHFaYePSLkE1QCQXDL3thyNopIP
Hvve4YcH3e8myMlgeBe5JxSScyWSB3P94LqpiykUro3Y5SGeUS+8pTAGPf+gmDnZb2oMsA2nqzwd
3ztFku/tWkvCGdnjkwBa+T5XCq+4UIcfVGT1FEbDJyMdL7J756LEspeSjW6pANsF5vtjEcUEJyOJ
o4PH7Wirge69STcoLLqzeKe5ofufdQg0ZBCpmZvExE7NMHzQvQCDQ5JXZ0ZiTOjBPx7eMOBgZ6kq
Ud/tgEsbS/1dWPajmeaLYtuZQdGH1oQvXoNhYVKIxsStPJd9L14V1SUuJRGpa8pbn4d8C4SXkRHr
KAvq/N8Ex6ajx2SNdWePUd54IZtOJumw2jUBLHFE8dHoKBpan+r7w+NdP0+dt7tQ1HdTlsfvjL98
btLCQfLtH4vUqwXWPjxcLG/Rshgl0nTHCoZYpNrfNbhHDdNaLwuc9zpdUpXf1i2bmP509MuIHZn+
Fw71qW9lQjVwKVtl+g3P4wAynbx58uOf6yVbWZRGUW02MA49fjrT4isYfipudBmNGGDTEnDNydX7
/LmGgWP5YbwEEveEAlRcXHsmY9MExbixvBIBHYUq/3TOI8KyZyVh0IalUzg9fwILRyRy1dsCqxlp
G8k/cMazUwsk2hNxskpnPJ7jqN1131Oq6UwwH2OQhx+tB7XJpcqXw0I7guYg/wdH8y2popagVy/Q
nKADIkwJzGC3CeGhNGrcSEM1hB6xKyWfOFv4R8KkiHdDKwT0XrtBwTCGdB2hrY9F8A9Ig+O08tNB
fo1LIhhZ6z/9zHuoJI6q5bSH8SQSqvl2V6e018Hcw0xwJnZR40j7DK7MzJA3gRH1S+j62eLk6m3q
kiJLU4pdKcBAb+mGE2vJFLnVIAhwrFqkLzH1Ue095GIe2l/TK3Goxw1y32PNTqaooNzFhJq6WB6R
mTPZitjATLOeU3sAqer4OJy1+kxhN+79p8oGSxxlg6fRAmwxCTPAibKeRjzSMKMlPy1v0ygY/4c9
J52uTlwypYQomlWXsexlUdvABoA1LqPzWFpAcFYuPB7OroPF7COkS80sSFOdZ9SoVsPzD6qmrO7e
6QyxfAiFtbuUMdZUs3UbAHoVDVK/FOrfVGYovguaSVqoaJSF+OaAIH3dp1afOl8TA1uUkplwLydr
9mYt5jOOQjT80ZR4JbqAbBY52XEtFU6MBd0MpqvgNOLwr547GW/OlbbhGiAfmEOzU5kL1Dmw9JxI
pZQWQ5zXvA3j3HRuZDHMUwhFW+wr4Fx0cojWayfmn8TZdRIRNwAf+XPKpwGLhJ3wU9EG78i5MXBh
UVBDCMriR1ATEbzACCEv61Me4qfBYFv47d3kkttBarQn+g3R5Z7agMdbrZ21x5ZOAlYjfb7ERhf/
+6IP1I+Dbzo8Rg3qxbCRozm87TCX6lsOuEBpsPtBA0to9IhrTCbYyyY47/FQqfeixNr3PFrDXOX1
kqYGSejVUmwgJPHJ/JEKJo6EX6QxnTAzceJAVs47yKjmF55gB1pixRxg2Zy7FiKKUHccj2kMPMBc
FBfiqdqFPsLLSSCrs5E+FDBZ1ZWN7JLIdbE5uwYPf+pxPlAfjQV+Dwbeu5WSOq5ipyIep4iwNQQY
kN3UiyXrdz40VpKnhIBFUGyICo6ZTnU5J9MpuP/AzygRqr4dAJm4LMBw3eegHR3h9iSW/bX2k23i
9TrmEtcAZulpDCimNqfdiu0IHZN69Uy3QzSR5ryXSxyqGns2sg9JYicLr19ynLyAA0bnKrJPLwiJ
8EJ6hOmHpQ1SNK0naMGhJ5R0yMjUYrsIRJeLMzHQmgQBNMNfmzIUHM08s4CUkURG23dz7WkvUxrY
BdIa383eL8UmMSt8sLTvKpsjhexYn4bnDx4xugccfM5FrdYAKXUYNGq73d3FjBVxxx5F7cumDeB1
AflFEanAVAf/RNP2CFDk/LkyDFnsMEP+3DBBm9/ynQepZD3SXyumPvgVgViIwOxCkdBtZt0mqIRK
ACMAjUigPvr5+7yewydn7MVycupSM01dz6NBEiGVG3/5PMJsdO1zSMJkuhFX04pOgOl9qwLHc9XX
Z67gcVGCOITdZUMpLdK7+DJlej8wKGXxQLP012BabsXLHr3V+WV8axrp0E41vyRk/3+n69mkOy4p
IBVnWVVzam/cVIOW/JCWvkCAY9UdUbnP5P60j16lxMqNknXGKLVcgIzFLBJHbX7vCDSFTNY3O5vb
UQ2c+PsSaRrx8o2FyvzQMZnimPEc0LgWMIvODu6Ohb8bSAifBE1V9fUDzrS4bPXMteehRZ8mJU/T
B42SCk/JUGpsgW50u5SfbWMKWKv7wHKAuyKmAKV0AW2MJkAetIX7hOo/R/jEDyUKL/Bd8wnOJipb
Nbk1wV5f135g7O5LOzPBXmXJNfZ3wyw1dl/AvsAT48n93H3LQu2uu5UDvuni6168w1orOWDFSv5N
MihvVcFxEFRL83GA+Mc9a0IBLQ8D5mpcXE5cB2+ALVPilF+FL5JsGGwhWwc5xouKaWM/ie3Gx+W0
L36QU4VETqQbxG7620zzYSu5n3hRnmIZnk98EGSLPYqR+LpoCeaakMdJcr9AgL0ehwaD0K8qTW1u
SeKjshvKHCxdwMh85X+8yv9B9CCvUjESIx883Gv78GR5eyJMckfl+2XjY8iGkxpuH3KEPqYywvOw
RXabAFs/q9bQLU+pFZmpZ908Y+K2jgOYKrYqxHTy6riv0FJuA9uIXGMyLYJxE/TDn2tPLiT2x1zO
ni942B57vD53VIpNQ7nLjIvWnsgOUNdDb1jfL6abSG2ye7rJa/z9Iu7aGlL0qm9urpv6qMSyEGoh
RuCeiiRKf5VFxiYsltulYICIcZIdQMEVfImnDUWcnNxC1Scxqs1WKgqiPKj5GVKGNQim/NxTnNlV
G9TERsVzNUiTLNEqxwa+pGn1qdw6SXhqRu9FUuboeU8H2Z60Y+Sa+8hV9ctPoK3CflP6mT3MWJ4c
RZp5JGksmztKwUyC4sfoa203PanhOy+Br/WY0uIIdHSWq9IMZE2EaS4w2O0PoH4N431iW/ah9NaY
IkEpQLz+qGRS7AgDht633/cZcGSBoxJ3c5roi1HtXAe9QY+q3dPMIUe9wlXWi0mmQE+DPVv1p1rx
f+QhTK6Jnf8HDGul9ASVJB1HsXqGU2e1H4CWjcs379bi5x5tlcnHQwPIZW3WaPaoYu5HMR1CKneL
momaSFtB3vhHoEjet3XgEsVjVgTNZ/tqGfU+cXU3/0oDby0eHXswA2ZkKIOYpFz+txss1uH7HNwH
xQOyi88vGJ5F76omUfBp1Dffgo6cH7jnyly7WgVzZ0/uNqteskwTCQlPG8RTANA+G6yFeubI36A1
9qs6annUurDmsKJu9RqdoqwLor+jNBqF7wQbBJhQyUJ6C+GdmBicuxFpiaSnWgFspdvOSxwA6IZY
ri9UBxaQJ2F0Wypix1PKL5Ux/Ts/ssWplkygQQMxMDL+ssHYrnVXXWyU5zZRaxqZZ3S1lNpxXd+H
8LovB3fBmSL+jUmg7FRp289pmnBcABmpPUgmS8qT7zSgT5IxP/UpnDetbWa45ostln4T5lSIIR4G
4gZP7QDFkNohk9H2egJs0hD/qDVxcJWIjXQ8G9TYQIM42AQk5PZFzo1ExuR0cA76rC+UmnnrZ8dj
wx19wsQ+B6a/7GVNSB6bg6Atl0n3oapUuzwjYv4E5gRv3CevKJiFd3/hTz88S8hO8CtVxFFAg8sh
fJo+LOSwkHpF5RXL+6IwBq6JsZPcMbUBtjug0UCJA5mvm9gYCErmOIP3FVxyo53b46MoZquQvjIm
Dw/3DZc0kdOuf26Xq1pLkwuoQafG/uyLU/cp5aWwW9qXxpSZQ2CbwRshRm3k8nRcYeK5Iq9ChGYY
ND+Au5U5Rmg741NF7tRWEaJKV99IoTJNkFpgDVgsI68lZmKxZFeX3EZZSeyfqQAeqxeh7sad7Ook
7UteGYfLmfud6yJgg5Nft2AlOVeM7lKkF7rRgD763iw+n2+AGJxosQ+Y3izTVpeHhXkheEjgqvRp
MjRZCSQkW9ICjAawOJ+qQcU4AfiUg/DzbPjy2geBh/VSKlMifE5FBLzZk5AJ0x2yNtJ7OfzduVCG
GuqyfunwdSZVOqroT+9MmiirylVr716UqZfF/dhjNybFb6Lswk0qMPlGL1i5nvNQhC8BZgyuhtlJ
zjD9z9u/Tub7rp5s1HWv19+cnM+eLQIMrsphYNs4afAkqpNhVW7I5xjTPhBFk9tPo1Fr6DxYrMnd
e7BF5BQ17Gn8W2cyaYbu5W9jPORpiKAyPYOzqwv/IvG8KyPl9AQ/k2lR2foi+Jf6uPNry7aurUiF
wltIZ0gTDFlkOWpDPACnQXL/L6jwBZtE70dJCmV3aIioXGwaYreYZxP5OW8VVWcP2wyEnW7b4kzr
xYRttqZzQlWMH+DS9hpxJxyB794JCI1CBpHzr0A0OGiXvUyWM/v7SyQErljy4GpdimEgDkOONstj
gbXJaPgK9YUcrJyGjBQV8W58CfY+FlnfgaV/Yx58jjCzbcjh4Pt1tyFboGmd/KqDlXlqR+LYynEM
mkRm6fyVJjcKJi7+cEFNp1+PBZj//a0E0bR6TRrhcJlHCloCTSi6xsa+0fiFYZtf98P1IRzhF78i
ZVVoIvjdr9RROSMO3gCNMphOFR21YeFDx9Bw9jDZ5ytNdsqWOch6HcGIhOwlbiAb9hMqQpBqD0XR
VeDhG3gdBemIPImtIjXdHA0aClrMVxoJVvYqCI6p4SY0ooEiUJBtiTtLovAduP1iYU7CHZZbAGi6
ErXdjPjPnm8Izh3+hC6W3A3FP+TLhuVhZyJS/fJGhX5e9DiswGJtIceyyI1JbKITpRzQMLb/5OT8
UBQbcHBua8/m6GkNnbE0cDmS/qSCmhjT+p5rzxTfpYRyz9uHtm14qkbgPKwZYINYe1k60dxUIi6C
XYGYux83ITGrqOsLXHb6YM7lS49mGY8V8JxU7vKDPOryiYKsD3vpFeXrtiD5HHHLzjm/ppXeUoUk
W8iH5GP730plangVD7mJ3uq26eTT298Qgi9l8ghohT+FKH5iWlQ6C+4uJUqGAd/L95gsFBecUJmg
7SUsF96gLXGbNmP5RW3V3pOFwtRhueVVDb/6NCZHbQiYZBlTSc9vH1RZm0Y+CthlBVJcpm7Bm69r
Beb7eOpfh9o3JCO89QWlJyphsa+MmKcJ4+G3KxhXVMyXNxQ8pIxEcB2kwzixxC9/ORybzyRPDpso
fFXGkrR/YMd4KYKkxWeQEdVGnLFFKykgg+cqre+6EPG40YrEdkUUacNm57bNqgv5n9Lo0Mn2RcUB
7hCcb4WkCVPrnDPn0WxGD1yjOm3ZuyOh03Iu2ar/BPmq5EoJqRaGCvl+iudC8tRrYcsB3eR/5XXX
ntwMniRA9zqPHBBClA4nIE1HBd6uMq/xmiwU+LsWXnUQ9HlwXwI0FNzljSWwzEhyvWoU6keejz5B
rOCO5bHD4jJ4GLYxHgRcp5cuucTiYMY2FH5U+YFgZ20kbaZj/VxXfN05zbz2Ft1FsJASVJjSiWtu
ejdi0pQFT/fQ6uCCSfyIj2hhKBzMKMX3ZXmiqlPq86zlmMzbU/CCCsIPN3TV91Z/37+BTL5MGkHE
bZQZzJD2EXRr0Qd643jUs3bzowtKe0P9OHQDNHB7tbeSCvV8lW5Nau9739x89fzPMTcGZ+XQUUBC
fvNAaJr7B3D1rFbvC3uXo5BDFGHbXJ8edS5GP4eWi8QnAvf/6PswJrwjeLG8ln8UhP9NUbpNGjOe
uGhRBwoLU0DcsfWeMy8Tljxv3/vAVF3ZVYgbRJ+hbAoA2A1CnlPxNxW7tqdD2aIKHjVyeXERNyU4
sv0WMOP8aTGBsE9A1JWXB4s7nv1FJ/e09dCffX+H5Dk0n/ydXS/lxySM+HyrrmyWw+Guonx3KjjU
wauOSti9OAwnKuJr4rNLlUX6nbS18fCGgfl/3bE5zkRRUf1OwDpi63E0Be0jXYSOyW4NVCQvMtcV
IYSlllIdcriYsSpaLMOLdGj92s1p1hzPmG8IJLNRwXOiy+oodizSrzXlaSTsSzXwBOeHRRsxFWZj
BilkiIE8+b8IS+jmXBDrA82Dl3LmOMjmRRGoFd7HEhTL8+zbiYw7QZgqgea7yBHHR0e9zOfvDyi1
GTGR9N8pJ1Jst1VQvEWpmGPDDcbp+lmeEmP/p/Vjzqmrlx9AZ5CwLLM7c/uB/I35cdjUsfTU6Bqp
dCmS8b382rqLwH5DY8Wt5qz/9W10P+kXXbqgLqcXGsmUgzvZom6ay9102BX5Y/XEKCT0Z4UEhx8o
pE06Bb5vm0106rddzS9u36TXaALPDxIorEuEffAuOHRQsJ/UMrfrniWRZNZ3bFImXmB4DKJoPZ2L
I+ZyOXZ4q3n2XSZsMkM1V+/RWKYI0+FAF/bdw5lXOCuPfJweNyZTasvNcukqonHVDQWEVkTLjR+4
4yBGsvFu832Q44wpuOEfrEqrifnF/vptgpakwJZ2+y6UulP8Gku3P1Pg//ak1TO47JpA6DOQ9H5E
Wy8RPeZlAFwYndwk0JfckDQOg1bIB6HICMnHx/adz6W7rNNkWJpU34YpbpzCVPs2E2nSYI7ykHZd
utkUDsoM9AxZJIrH6LwPFxNUsn356cMbTUq/+dHqvzDxYFGr+YNHsLC6gJo054YAiT9USMTSw0Th
KBVnH6P4PhPmwqXzKzONyrP+IOiXAgdzEWSB/+/1Y1tngcm8OQ/yWADJzy7UuDKloa1i82vnhvmg
qpzvqgW0AO8cl6dK9baa/ahWsVvOOKe6OJyOmDUAWhubxWQA3q2faz7Ntimt8oBKwp/gZwaOP+AK
KnP1TIiqzahknhvf4svNtGnNGrkdQwM+9J1ibIYM36/5frQJBD55xqqcdx7ZsWbUZM2wo7OquLtR
cYiNOhvHRjYUkjENBiHfNklHeoqTF6B2WlVq3IuchYWVKIlsKj5gekfdj6waiT2a8E2Rx4iTn+6p
kmZzbD4+hEBg30nqlZSO8kr2BBMS3xYAYJlrihEs0mndGELGw3NWGjBUCYhIw4lYSh2RPlIlrtSr
MXmnfQIR4q5xXThzs03dCHQjPRvtZkVHHNJNoEp2Hcs+CMOb7H5olIQpT/5E8c2MzyeMNyw55OYI
OjdFOd6MDA0sIlmNkCuvWPTatdUEQAmLgwEdK3MBBRRJSBnblEBgKshfUOX403xAXwQLShV48coJ
83n+Qth3yNphyMKIdr7wJFJnGR+qDQBe2QMB8FbyUVz1tq7/hiENIn3oJabzTkTtBPzRTG1VKBOv
5wJpTaWVsJqS+9fubWC3C6ePlME/K22wA4+FoOMBcL4IG2bW72PRML05Oj7pkfVsJStd6JpjTxBs
qzV7JunNbU160ZPo4ISJISSM/PukZ8d6yFW1PpOAENnjN2t6kj8rLVfK/ysCFxU5iq06P//MrPnI
vsB3kGTwJVV+0HRrrPj+t4IIVcF9eyvR+xrcQgnVuEIfOkXVMSjeC/e92qPnkGnw/bysRK4QdZO9
1Wgmds9C++JC/THFqv/Xi01alwHenHyWN+1mmzmHkedYoc4sF+AvGLfX6nn8g8LrpFtH2a1RlkCj
XqQd1aq1bOuFZH6sjLiF7FAAPVge26Ozfz0C673iicU5iMfDM6DinEGiNwTcSYXWhcby4le+YtAM
3oxoK5ievFEDaZ13qjdcCgr0XFf3LZZv5YzfVtmtF8+r4rF/5mdZ6ew3yVdRbxLxcSWX01RF0m0H
rLXQbWLKoZEYzPutbL2WkFNNjVyQe27av/6HRSlUaQnnAEMMqvls4g2jzKrmCa/gEZmCVnL+YoRt
2LQsWwObYIQ/Ao7qQ7i1zWwnDvpDHaSJ0GWclEKxnNZJt3UDxmEOWWiFu6sOePEdoLAzEjTeLAJb
9h2AkwDQPVLc+1iOgQsy3Yc6VzLxqYaHYQmcpMDtkEdapKa+jXOmJr63hvFm+h3Om1gGzUZnEpWa
aE0ndhRqnoqIQ8lYx62zvzEKFZ7zA4FPqdOsX61lfqN3E+SYjN0jtdJdEnKdNlKqeVEtOLQWFi4v
9fEqbh1J2U6aq0xTjdBM6tIah3fjOt3u4JSJMP5FDdATS4g2NmPTFWJAhW1ypp/6sA7p9kQzzzoV
kweEXm6IU1G1LqVdhgfQBDJ0v4O6o9v/VJ3Uzt1buzeMZQhdI568mwie67NutpYp+DFwiY7A1yiv
M5QsujnU8Hpd1vqXiIS6HIiy+qsjCXq6hBkZkUeOuXI5UYwGYiLKqNl95goqg22PTA+65qHV3IBT
byFi96QIhwffByAOPB93DquTTDP4Ozh2JxXHhHR+Cw8vLHnFdqhYJzrYjrE4b65Ctwl2IROaM+Mk
/61z8rz0UxMCvAvUqbDy9QYFbfqR/jscdX6WRye6MgwCM8M8oSYzE/a7qJdadRkx/Qr1HmZyC4kg
PgIvBlLMsWHwsIUaJncjOkerrt9P0fOpfSLCxchULd+9LiJ6JI7v6k8/0W0Z59gUcR9YGLZ0ELzE
fUyKr16mAuE5oZkj2jlUWOIckLY1L06T1FHT/36KYfc4TYtbHdpgCotlPAup23G60CJ3egHXB3GM
+Ku49Hr88CCf+zQqOa9lOh17TICKZJn/g8xoLsvrOj9EjYdcmzFU3EOcxolwDFGdCeGmiz23sNNd
wDgupBxL3UNRO/zHsgVlITdqI/+aNZRTMFEUAyeZYWc+vKUC0YRE0zxltxzVMBCIUR4uWwaGUPlv
goQdnqsyNQjjerNNU+hFupfZ2y+7AOI6XeHEM2T7XZqEGGYWMTJxvmejUo6EZmhHLuHbLYHRrwW8
fqvv5nE/+OI19fssRxCNWY4+hiGqr1BiE99uJU5d+6Vv5jJxwpMy7FTDQmw6RC0HZ7o/RulnyMNG
/ZoFKcVSIdXsd/oombydWTCrcMYntXg2EfdSAd83Cgq9oBGppYlMVUTpeR2H7PWfUXIIPhpTpQuG
472MVwvXfgNqM6AGYz1pUaKFjseIBhUwUXF+56s4W3tEWHq/e4IQvfLvdTKk7FLBPT8xQYDdKZIz
vjByOScBJnWBhjmhrDayaWfe0YWVKoMy6PQwOyMfPeqxys6BNn4sL1sjFoK5tGgPaHmuiucCSvBH
TuBtsMfJeF81SaE+pnz+X9HB4zuFpNFAB1t9OaQQqLpkORfmrz6K4LcY2XCiN+7QqKaSnCPbkCYC
VnQI7WFt2cXOYFDIZlbwXT9AOwoPJrmcc1kDozCUuDW67d0DAr77vknscc7jPhY3eJLQgdwc8H6d
/xS5ELqLLeOB0W2ZZxkmNeNT02XAEEjcg+Jm0N0YHEntKy1eG6eLvLg9bgM4BoNmC5Hl0yVY/6B3
J7bkJx3meZgARZoXd6lR0muUH5JMw7fyz2NeTLvj7xnVp8mfWd2NEAriYnPBo28teAA0UWIWgL0+
CepgdI3luxoglU4Ys75Qb2jva0kjFgoCnkcyMR1Ka9R8gB9eF1lQUaRnfBPLGxBf94Rjm7ahumHS
B89CdVcvSvASg1uwJDczNyWSLG/7N2FtlbMJQ78uVS+LdGjU8DE7Zlimi3/bFXzvlXrk4EFJg/jt
udatFaDRjAznhJudDWR1+3MviBaTlrQ+3QEQEAF4VuldKUH4baAJGK9b9CibU+Pzp2cfs4ZT+ikX
1qfOUv5l+I2xXndqO10ISqTUOORfvIAS/SPWVsG162IIi0ZsqlzWNts7MAhtCyes00UXyLZxq+Ii
+QEW4sXwsHMWy4Brt2HvMJUHesJCkoZWEsWZPJgw26X1pVntKM66aF0z5/zF2gnTBZusgtS71eKM
ud5r+rfKWs5S5DukG+HVXhacNo3uh3Fu1zfP0UE4U1R+2BNkSWSA2oVIM12wmY1x/4uNGfjnC6vA
Plcfd+xRHrYTR8K2XoNtXAjNOWC9DFP1roek8SjsIcm+sFAfJ8NvhGy7thELSvCerG/+VrGhHnU/
RHW2qFnZ2OJ5i5WKXpKnOt4O7WT9y/dtUOPKWmNcpMXcTpGM2ULrhWLYncF1KBpqSYqEK5jIpuIO
/aj3n6Fw1fPqfUFUoAnWsdGqwfGV/qx9sKXB/CIi5vYa0qFxoRwR7t5W130KBK/h/vbJgLZ/Pqks
jB9/e9xjBf6JIeZd9HkFf7SrnMNLN++ta6OjD7hd0rhMtH5xaGCjrhwEBpEoyY1xV2C+ciiJp/+2
Mb2Uuj1mlX52gtvPEUyfDCieem/l4fJI0ppvZ1dmuLZ0Tf24dFJmQu7TGoIFV4cCPDsskKh0I4Fl
0Kx+Gnag+kS5rSB83Oldv6vRftBWI05kLs72xUv+3LqSG5AMROpsFzBTFh5/AboKqjKTtLwZqMFs
FI50hjO5Pd6weLllQlu8vBqWGRDl0jcrr3YCTjXNJHkYKMt+JrdX1Ir6wzlmUzsdsn9+jbLQlcs3
dPMa5VLY7yP9R+/XJUZi0Wf2swqE1uDO0alDj7rASF1tME7ed+3zBfeLw+yGhoYzQhuI5DJc2o5m
dyoFP+HtJe2A9ohHJEqhV3MgE9t0CEr0JCkZnN5exNWI+9UiegRMySGGTtFWPsn3Mba1ellKa6bl
DeF24iqUALAxthHshgPK7Uow0GtVAtlMTlI1EDYayVI5ywxUsSki84U8nE1X/3tK4p8eeKHBxxqX
3O2xSdAhPcR5OvqoXW7aD6h1jZOjNqi48Y8qq0Olfj1yVq7yejqG+/omva1xcdiBMptNkJ+V2i6C
qH6NQiV1COUlZXfXRRDkFU8RFZts9phz123ntLmDUcxfc0ZYFYbKAMUp6RnD1Q39N489vdRHNIQl
X7wu7gfjtosx7qkZskSJvieeR2WS4EtHSDf6YODd+m/veUKoFpVhpa012MJhJsfkBqAWI+79wn/y
FdSmdoqUiTMgSzlPNSRoJmva6Sultt0OhNdw2cZpW3TR99wYs55ajzd9e29A3PE8BU4NFFRoLAYV
oLf8CQsqO7UbCTnXQrpFtLw7IJG9laB3JamY1rUG/S4t86NPuJySwL4EyRFioSqdUIdgfbiBJytz
weSIKvY+aXVhdW0rJAsAoqJ8jcikR01KGwu/jLQVRkySPGfuNcJ2bEYSE+du7PEJQQotV1wpS/4J
vkJFavN9/Q4CUdG6Jfk3/EL95iRALMBD4tRWSB+qrgQJJJin78DNDXFx0zYqJTKp7m/BI+l12TUR
tzHFfYuIW9gf7FaoEAlbzpqR4YiB26o8wgjd44vkEjQmjOh6kEDiZuvjbRXkrq+uEyll5mdsNzX3
PivXwbAh27hZh4uOJjCFJ2+2hOLuxB7UpPnjmxEJHJmLC7oY4sLcD3bbY20TTU/oS+9tmfMOLWCs
+fn2y79yf/6NoMS7/t59R4FZ9GZwMPO9bXGOe7wVqd/uc8VU5yLCtz0+UJvYnlzanhOSmHxFayju
sjO8lDifQcLL4ao4F4eHXFwhzUDKl1GtckhHqfhZPclFPQY3+ktsDPTeUuF5SkEiyes8KqecmkDV
gFqF7a6ZG838vWBtw4MPfpPROI5gLOSQe8X1KbY1sXynEDBlyfimLND8lrDn+3+ca5ErqZTHjG2s
E5vJAi/v/pBLesFIJJbaPFIo4Ze9zoxBFxh+en4e7VKFFEbmdwmsi4pwY6PmrDRg6vHT0CIv3C3W
LpHrSbEysKn3+cDevItDamiDxWld5XYKfopGCB/dSmmwvR61qCcuchdjm1MiGvveWpXDCoKn10ZZ
ertoEe6HS1xlC1BPgcQM96QsA8SBHa/Wpwcz11QasdJjCyBAnLu6oTiC9f39jzvxO/cqLcyPSNp8
erPRQgYrNGaLrfBoSEjKBKJlLhUEbMwxQRys1alFyqD2C7jzRBwEwWb3NMALGdhRM/ufbPM9WWhD
IxW6d4OifrM4P1l1mmzc9lKXxWC92WR4r5YJNjCCDw6KZWHlqQwnK+mVsvSGoVV4HaTSqU3pVFq2
U2IFHv5D6ejKdrmUCTNq9VJ1K/Olgi2xGhgI+Ank1v4+XIMepkft/bcYysG5bOycY5G9BXipwiSz
xnfuKcfIzTmq7zx6utd6d7av3m+s/ww9f+VF0FF1QEX1o43nqeeGZNXFBF3EVaH42MmcaHfBZy9B
e5AhVhosZUZcipBWo13JrEJCnxfk4taVfiEDgjIDee7M9bDH3fFNdxmAM8pK0+Xv2T2Om4NRdY7I
z5KZjFgtSuDNxmclkUrVrL/J/RuHkHbcdMpro1aJZJGdjfo+JYwBg7QHyQHLECjLXFqZnYGuJoQN
mJbl5VjWw6H7+to1INTB+KqMpKWjPeWiDZUnvy8EJ0D5XydrID3rSKwm1wYkE16Bsp+N6iO0T/0M
5FlZxSRnUk9ryX4WYGusI0X27hOIyx1CTg/oOCqPcoy8OxmjDXn8OwZUKbwlqTzKH/8ug3X1UvGP
tG4zcGLG5iVEGB+9aKlAu2UMp5jIJgGA3yELCLq8ybLS7hEWAe+Fto5EiTQGqsEHU4EqKMXybyOi
16cMZ+vx3KvpMiUnJdeegxG7TbqM/cJ2z1w2vaYgpVdgglY6hlC/6bdj/nZ0YjM4N2Um1BAu+TV0
KC0rKLdSM14+0Mc12Bdp06krSKqSeUb3P+6gsdQ1GbN6CWnnr9EW+rooTQAXspFaRFysbE9gFQq6
i/HenZvFLWY41xSu/zPy0LQzRA3JlsmEZMmdFd8OTDn09JN2iAv7218gSVhJKFf2KX8CVfP4/y4J
I6gFvg4wsl3ozhgAZYTsqWXFuc4fSRUIZOjLqux9xvxLmiPWpveeQeGlv2juCms1HAyLajf1ogcI
OYJzoITDYL60fsoeHyV2UjlgV8NKcvp6JCYDjRWsuBK0mDZ5cDVd/PCp9pOBG3DJyC2j2nZoyUXf
/iwVVkO/sBqjJ8yde7AOjVIeM/0/21m8/wXY+rZ8cRLg8/xjyCC6kJ+mmePwx5/ATeR09ypazQ7P
DyDfBZM5TPngKDm60agq4ow/uUOvId6SWvTQZd+UXDakVmyCzo+0G5qzppDpeMZmLcwCFu+jNv6C
5SFcnfUG7qu+JuDSzdZqStzMNuOGGdoqakBVa1U2k2nuXvW3mWNNvtDqdiOv6Nl3gQlCKnlVTsUO
fapGM2Cj9M5aIc84ZFNfTETfKvGSgGNw+k/1z2xnh2wI4kicGuGXGX/6nPa5ixNN7gqZRhOzZRK3
80e+UYxZsBYJIpRP/m04w537V28xYY5c9QbllvBUnEPxoIo5xEnwq2kJXLWjq7erv+Db7U+2ep2l
LakFRaFualZTeRVwePbx/tSOlXxdXdYvptWOru5orW46C9Py0EW8FixUWBZRgp1BjVVZ7wmCVx2E
sC62yHvFM2/nBF8V31VbCd9ANXZPjFFXp+Sb2mmRSMzAAAzCMmW32fQv/2xkFN81qPplHdyVb5j8
moS8TFB5vLKKgB61GtfWYJb/AGz52O9lHlrgguSl2zcvMB3IXSbUtGoV9F/g56hT2p3x9Wct9E2I
cxuCSAmq/OAVpOzhfjQ4N0McQLUfefyT0SMth/H2dNgWpVAS1BTSB1Rm9RdHRv2ptfWLUxEgK6rl
GIR98OeraRDfUJiNbmSm0j6gadCATgr9tMXFBMBVY2pnorOXoTt6vHB0wrijqUrlP/04DI/UCCYQ
JFg1i24avrfq2qxMW9WFvxX/KLT+LK1owgZ9d3ACNM8nobGk1ph3DwsORPC7auR33ouQN0hLC0ea
anPbAopqIWGk/is7BBmzQstlomHlXp+UT4HW+LARdZXUj56dpKPD++ijFHPIqnYrwYUjq6BZQ3Yg
hhkssCACsmtpic0UOMaxcAaWmZhC7MNGLz1OV2mm5ftoLb+lcUj/b/dhJioopjo7Z0OGcjvbblHg
hlueuXVw/e4KqIQnWKrscPGlwJ3v1CzBlDQiSoGQwfDjAUpON4lYBdxpX3Y5XhB9V6qjv3STZ9G+
WW/GVf/cMJZnVc4RfcD9rC8dfZk0m5nJbEpxD1OB/yY05eLHzfNkVz+QHZ2VZYMV7EboLhYdIiLb
zknJ8KiNtkcC2qnWVpgqPc+pfLpxVSHPPO/u/uEiWMMNLEcrk58nksEmwEB+2esUA/H9C+VzU7hZ
8nbBo7svYqCTCVFg6bKQBekCxQxTnbZZ0c7NoWqq0asMyoEmmszJm2KBATSdYqyr3OXh6Bsfgezl
NWE3+EOTEFiN71g7bVBoJOZSuLBrdDa3NVXtbbuu5Cv5ViF933LmLFGq+WT6Vn2q44Gg8oaVFeLg
IAwpR2ZZHzK6Z0yaOdRE9jU53ubF6rYKiQDhklwVD/2dCGrVXOH4gWLRwKgXuvo8/NjQDaT23XwG
bzvZI364pu3NL0Z7jA3SWoc9VoftmAas4oI2o93ATIqLKftYkx/m/j2e8Fz4CPLakl6T38IwRtpb
42ieDT3WM3+dcnJv5quQ9yhDuCEjhgennrKJJuKK6E18W244IP5mbMDg2E+b+hU6w997ekZELr90
+lMIr4pEepukG8r3hxsgHbKlII3hJHQRYQr9lbwUG5InI0fgPLXDIDGuqB0JAqgkXHqHqz5EoUHq
OWkK/ws2oNbOiYzj3wMGbf0Xh+L6sK3KfPFBRZGPPubVFvn0TrnZDZsSI5vkmxY5Y1KuWOg9Uvk2
Gg9PQFww1XZ6LpbEwlbF7XoybfuE0mwUzPu0GQHlVGaeWtp6ox7DK35yIn9/N6WE1OGO2QpFq/V/
g5BTPQFl5PD3r/cxresGmWxVeHFdnA3sf5Zi68p+dvzWiO0bAVo46Z7DQhC2dHHHn6dgLe2xxa4o
uWpIxEWnmnVAcWYksdHFBii/BQ59YKl5aHp2ouk47Qh0ucw0FGqLD540wqwlriSd8SPRY4mhCcOJ
INn1AkVPfYKbEJSDCJkVO5llYYhtoyBNOc+6peK5C3r5PHqTKRqhBxc0XHSX0bIS+QKPZV9uGfVy
jPncDjwRK8qj7cvkSVJQX/SKwEbn01ebQlMOrloEySe8n06Ksuvribv1MzaBbXPnXnnHrBqSIYst
gf4olS2+oq7GyzzB3x2GmC/af8qqjcHjKl2iLJLQR59PIgc7lCJMY9Pu8m917O1hFpdUE/gf6CjH
1wMK837hT9eBXP96BN0RPpv5V4rNBKAV+vVIh/zINVu1SKaFRLRdnHV8XcVvBHfH0R8XIEHRZX/0
YKX5BtUPqGxLrn5qjH+WtYbn/3ir1qPkLvuSTD2lX+PP9ZEQnoLKaYku95SpH5FgCjuv1Pf60Fh2
Gda4Cv8PlNGRz0tl+aLPGLd1CEKLp+a7LQ9mdr7cN/ZuWRK5zpNsr+OdVQIhdaBcGxcc18AwrBEK
0LxIfgSR0bE9QfM4SpZ9LDS+k1AKND9hFNbE9a4J0PBt73N+GZEGyx6YQBDCZY+rw7ChMiZobx6G
1y8d1fUIf5bOCqKYRlejviXSn4OI1QtZ85Mo5d3SZq68/3enVW2CG66rOeCgNJApvUi2bKdQWCpe
61klwBs8kbFSzBWppR8hBLYkdT9fcx8qVnHm7oZRdMa1LRCIAodg+BaQEflI65ASqVffSAy/H09A
rhKXegfM7VVjMQng81BTYzmNnTG2iRFREVxT+Y6Nj2fX5Blo5CdljOsIxlmbupTgAmrxnyLptkO9
4fNRihpsh5d/sUtgxjX2nU0hnw8DBdskHPR0J8TqESQcU5+EXS5/QOD02fdkAXD++rX0ZkOi0vup
HDwIf6Quh1dZaBZKTSM+gpSu6mvHK4sQHsu9jllWOgBfFIW63a6C8wx4UYls96II//WXjpo1BxpG
MvH/Jg60qy3e5HUn+QpjEVnC5led1gqcWHU1/OHoXv2xYM4vLX3v5NHKXdCoDYUZfk6u2HZvVKRf
yQwvVOXIFgF4C0SDskoKH22sYhHfHfD7dADJ0IMYCawh0keRKmOgn7LlsEg//zxbOJ9KDqi/qehB
KGC8bwbj7SnbhYynpFwE+AwJ1Y5Y+NiKHizv6RRIEFCT7Kse+mKNFRNePOfEN4J6yiPtCOyrjjMp
RDxFqKE8zzOGjZuu3pmmzViMns5p9Bb7gdQ2v/v6XS1/WPFwp7kb/qDVfI10E5OufZDfH+JzhRnH
hSCzejfkcYVKVdaaAsCO/p64Qukx5pK5BYzVC9fVDN2EIS2JKsY7nrFNM4c1Dv6N0QOQvirdc1Qc
t0I0aDw51HqDMquo9+pDHKEAP2jUc2FrA7HWxb9ToUhX/yh5zAUK2UzoEN5ODdYg8YhffXE+DCpn
ZSWagFZoztLYBIRk0+MgrAuwSetKjRlB5vJcGspoAEyymzHygf+hHEF3mF9wrTBa9Z7XQvY7u3c3
IjhzgPjoZAwt2CPy6xBslyaUbErOntDt0M+XCVcxL+6f10zcIVFz5ARn7JCDLqf9g7qGabA1WKH9
H+FeifdRERjvvtbRCeaJCBPjOSl7rV1A5LIQ3xyauw/a03kNb1mLL2x+fGWrPYILZS2cREm0lJ4p
A8/zy7suJWmZPobt2LBLVix6P02f6AYx3xkUBHXyVlgR8O1UBWvgGoFvpK8MNjx2xaTsDtF09QiT
iXOSuQ5uSshle4WuiOvct+WfevpNUqEO27bKRLjnl/0krGIZPIZ79mIKbs2x2wVnVf/Ik5fNCr7M
olfww4zvAbCSJpkD5XGvi/ih/d1vlBeYbAQB156/293wSglpCrdQdT0O6OSsUBFNKz2gpEre2+z2
gdCTVEvDKWeqXNTKgvCfgcDV3nGmul1McEiyrj9QMmG8VxTLvaQr9XCnbsaJ44lcSpvcE8dEsq1e
mRCFj/seF6ME4eYUaQ+ztg1BCvTShNABXTQkZQI02KKBKueGrR16MkMxGFRd4elZKeEwiSk/uOB8
MmuWewoye+QA0+jVYU+dc2FZOa+c0jtiKXOMbC45A4vvj0MPhofnt54lHYn9zzoyIjKnnhNKwlPh
F4GC4jYeEEP9EDP/7/Ypt6T8N0spFiwWtApKVM2tUg1glWrnYpcoRdFKWOH8B/SUpC9HhJRqA6jV
XzznssifUIIfxQ6rMZhFD1TPbTMg5ocQ5saSiPGlZcRiScy0zCKfPPf19FbNEsbdXSvG+JawlWxK
hB8VLOeCBOGoi85Urpq55EaIbLqqYY1aiJAYadABvjxATy5bYQp0uJ38yt8/qcqiu2fGDCzSq5Jg
cCfJ8ffwtdr+DtQYlZiGWnuYRo/QKAgjVLXFxvvVqOEkfkusgw6CRZc+XaPRiEWNgL/FUYLFPZnb
hDxDO/gU85XFtzjEYx8/Rtxrtc5zGIFl0czChEWI7iiXJLYQye55HT8oSEjXh8vZPoRzW7CmcLM6
xkm6veTjWexp+/Avwu8AzFDDaADop/hTOBDOba/teUEkZh5Ero8y9XGDiSqosBkUhJ53U8vZUH7x
Qp75AMxLBr8nXiexAHQpKE/dl5IGb+7JZPtjWyWEGP/oNloj95A/rsiWrYBT6d/IYaQJ1YnN+nVw
CA9vNqPsoGwnlyuePXMHAV9aTEPko4i2kAfbsmFBBeAnFY8EnJ+yKn131tBA1dNozCwYgkiC13xt
X6QlbPeAru8WNpDC1mY6pv0B+QYGUyA0HC7SSd+lwL27PXrG9DndPgWUsW6U4KQ9NL1tF5Rlrji1
AgaBfTRZzYWTWb9aUPY8g6eft5bA4Ls9KZuYc9cePjFi575cd+qp8kJ3mJhd7YMlSpbd0P5x7cC3
y3ULnUtXCu+/YXcsanQtd7yGKVptXIM22BcD4QSAOpFDSbSFAIp1lOvqEHMCtzGgl90e7Fb+1q18
x+KBiARGg6pkvDzMYkyqm9VUo1jwO29y4kFYSneLNEXf+WXqjCMf+/6XtYIG0MLWNq2osS5L7qMg
BTPkKwgYbsvO0C8oU6hBDQO9PoiBArg9lPuAa/R943rrI33weGLS5amJ1PS2yw0xl+hLk1Mgsnvn
WlcNRhtqTurUOe0Up4q8o4NYoa8RBNuAdsIETfPi+PoWUXHl+ObF7wAIfGpKPwc4d7v1HXpNJxS8
uNzgzyU27BYLv63bCfX+VPURmZ1iJzBplj2vLp6XvpyOhlIrcYSIWIB51ds/HeCU+cbXdzmLomfg
3liVUgySx+/gn19bbwi0q3qB3WR3n4IUZRIwrOKXVkLQziTj5YwHDtQxHBOYOcFsbmejZEc83aQy
Xxf/QtEsPoifmQe2rDAjJWL2ObCuAekExkjXKSEErY52rmACgkBsC5/mN9xsaY5BpoP1QXImNKdp
zgaM3ZTMmZrVphBojh9o8lerquRh40nclARdU6Ob4WBuiA6mSF+9PHnraOnfhkgPjAiOSP6T+3Cy
hyW5FX2/aitynSRMM+0wm9GIRCqWBA39hPrC09n67drXD/gbD0NVS+KJoRnQMzTk4Fxvyc56Dv0q
QNy37Ns7I1raoUy/DtTdCT7en2qMgHvqUOJPQhQOjdXSVQ92UUm152R3ZEuqL08CeBRaTUeR/zDc
S1HL1f2lWin72Mou3CDOWBm/8osBAysF9t0g18R2yMizwrBgGPlLNhMmlyowhRoLN9+7Uytly0B5
HsfTGb1tp+IXXCDma20k2JS4mroasojCyl3D2C6B290HKnXoZMNCKATT+gRqBTB+KCNaXGZ8CbSX
AEnKR+9IA0eJHgKO1/7X5YJ85J+Y2FFnP0s3gvAcqcdi8/iegh12S56ELclB4eDkh7w7YW6xRbcU
NRF+h0J878BIw6Dt70qLJgyRjUrkO77iyyArFvAnAymnNRzb2HvMZsDUfn+noC3iiTsnkYUQDGBh
lYALaSRhS2vyKGfIDTyOfhpuGh9gyzQUWNdf52jZk8Q/pi85PFOspNYIeaf+1chGbffO5FAui1Qa
tSXQ4donHhpShOMkWmZs2vWdaWQttboWad4B7PcqoOcTiJWXISbs17cd19TiHcxp2qD8tkEA2dBG
j53/W+/bQ6RgAAufb5/p/ROlFn5Negj1tl90xvytDo2iT8vh+pBv/MdOLYVSWUZFpEWP3IskFfMJ
fbC7QbPVuCURjBisanRYU5zc2uiIGmLWiak55fVgctM/8z/UK9ir5dm1DnPiL0BAbA7d3qoY0764
wCgUQ79uaZfZIQW0jqR0K1PiIv1xv5F1FFkk48lsvHbiJCdfGO/ToV3QHfMEaa/DsE1BfMny195E
5287Qqs65jsCduQO9Uv4cM9vEOLb/A2VxMimm7hUumYFHeqa5XRcumtXINZpz46cK4pdVybTFbqn
g9ATVJ1UItnUSzszPO+79q50DZg/eFKLDErduS8660/czWzPH6qvuC90sD8ds9q5sTvX+26xRjwE
0QQDeJWidSXjcmQUuDqFA1IqBi8sH04EvsoGMAqnEtEoU4yqk0vDzQczbes+rYY/QGQ+07A2ImU+
SBsHVeITG0cjWR5IPqLupHQV1gFeyYYQufh94Pxp0vSzT5PogBxwdwgk5uDztmaJQ/RkSkZ2P9ZR
ME/6GNeInhRE6Vgn1GP57m4S7BP6Tz/4wO9jNXweKbWDg/sGJVCt70NAMKMUd3GAEexMBsSXuBR/
ny2LmvNC48ipgOGIMEtBtNvdRRbkuvvK7kVIjuebJC4XCB0m0r3w3p60FZVAZRfBJS0kbZJREih+
F/5HGTMYoDmKAzm3S9YGd7LwCBQhRVonyVWUkR63C/O7wP4/svMiaprteljxYCdcfojIGXwypMU1
uGlfmH7frWo6Wlfj80L7wYqxH8AayS/zwaMScEvnxZmyoojOv7VAED2ytcrOUvrslSKLOn2IaWD3
SbctwX6Krd56gpVBgIkKyaMizZJ+17K84Lyz+SN1XqPIsfsqDD/F2g6j4aZCGbhHTXUCMrVOoguk
fA1RcT56CBsrrA7FJISFkMLlMIqc+5pfWEOoqCu1Ar95Vc3l+D9TS8yGhnBc7US/qXCWzBzNGNYC
OcA/LRu3gglJ/DEfr9ZfamKosCMSTSrzuh+deU6Kf0yO4/6PGQF5csV7b6yAFPxIvhBHMDVeDRif
/B0qhtBDxcQWuHwL1Bjke7jH36g46SJHstTZRQcozBOrOND51JjW5PHTWGVZktRgeh9VGHZQDEK7
fV5nHuPCszDq7KfKesAAVEE08CE5jBPMlzJcBDTSwl3v/qVcweF6jubjVSXNJxrN3LpjP8/bGwiB
fUKc3pgyvXpKm9bRipLDI8h0RqkbzOmIYMZAjI9hVDtOTcp/6ANhWdCvAFUu/X/SKHw0721wsSPs
yYSSlTG77zo8nUvNNwKxqbcJlAoVJyDt/wqDOHkQWkZ6y6XlS7bQw9vxKkbWW6416iyFBSNtStch
F0l4YlajOD6jVrWTnvCJXneKNuUXTW5inih5k9opZd/f5NmXUTptM/SZzRg/L1DeRNOTLPmFy6A1
XzyyV8B5ADdhoHBmvJrcGZr9fitPzN6le0M9/G3KmRwnDC/AvvCkI9g4v1DjaOQwJ5LVkhq1vxpw
+Ns7MPyBsD6nw4so+8LpvBpYeMvpwRc/TsLWRelHzVa247gmRuzf3Pib0cf1xZp9tgWDA/WYccCc
Drnt4mUlIh6pFhto3Xi+egAUnyHDevBqPQxt2LcqRwOjSnCtBU/COHoGFFczb6PxYZJrJMtifcbC
fr3Hgn2s8AD2H42/DPDW3bXTYnniX+jgHVsdh+fqQmBBdvl3TR5rSHKncqGTwpFhn+rqzwfVBrPI
PYXCOBgfgtnSl7bLBHWXMbuzVpdgdYaRtmD8jPneqVlTeWUFuT0sl/Soe+oilZwz8IsvTYmGQ+YR
PQaE0BUXyLGmeuk6UHyD7CLyo7VENorO5ZmmyQ73zXFGb0Z1ly9t34zk8TCdj6TJq2+uSpmaOTHw
mm4hJVMozK7NwB3nwcB6QvgloTlOjs8P0R1aTppm0riI0ren++XIgy5EtrQwcZDyaa6Z6OIIzV25
k5UAI2oRN/Uej0gbpTHmNz/a1LgCtANSvXNQkfYWv2gCuHj6kTjsC6v0KPLmYJ7Du+N1c1sG5kkf
2y6xqkkOZ9CNsJI30onq+4FkkHPuEUT5awZIPc2odFa1nT1lUhwjXGqZqh2faM1UNz1xlonTQ5EJ
ypYtAc5RlKG+l6ncNSuLk8/rGV2o/8eXcugOcB2raMuvyWsCtAhP7x1SmKA7524JsfC5YejPybAf
Yg3N+dX0la1TIN9thNC90rSwdKJ1a090KiLt7Jzr+EXPh4pKPKQSN0Ls6JIZkKb1yoGFOOSzwELa
VYFFWeaHq5WLtlcXqRBjptReRRcn9ZyM7N5Pvw0sAL797aTMWWIdidpEOiEmVOX6P/HmnbwSIDwx
+JNuctBfM42hfYgJlLxu7bCqjFmtotqOYQhspid6rTpG/xmDbibN2IDZVylitEHfUpaD7Pp1n/+E
dp+R97nB3Cr7JirktBHq2nCX6RsEi+FGg5cYnX/Kf9GIgD/6/0gqNDxb4DQWyQ77850O2ZnCXybQ
us5xCO/siF5HfQKZ5b4zeNKSxgkrgK7gHAY2x4+AmNTmWIV1PqVZGs7ljei3J9FZFNJ1ZMIbnMT0
BZyfTV5NPLsRtoUv4tDVv6Vam7MtF6vpiDw3g9a848K84mHsKUytGf2v+2XCXTv8v4wS/6sNbIvu
ejAEr7C4mzG5sfYlSZJ7QJq9JzQ4+jfo/IF682+r4VvpkCljYezO6LfBYSX3gmxvclWiBYVRxcF8
deaY6Ht8/NkLIbkxZ52+i7byUDfFKkm6DOz2HRYk2SUEBX0UVm27B1UYFnHRzKY4+HV16+kBo2Lt
YTf2LI0wapVQd5Rk0vC8n9TfOK+2DYQpxfF5necjYnalOh7ebDp5lMupRDhumj07mC322fhofTM4
Y7sRwpfi0j/7dxp8+xid4JKDrfWqkrdUEOGY1c2rkYbuHuwyekzxrqgXN3p8WCxrSk3skwfC0at/
AdX308I4Rh19Tdw6REZfBwn+gseph8EGGEh8imjiHjGeeJTU8cd13nIL5bLk0GpDrtj23vGy5e4m
ZFSQdVC85x0AbTMu8rQ24kf561dv6lDqEL0zrIcIFtwuBOsjJs8AAH4Y/pR8g9HYN9DsDS84ybuI
AuINwNQKWxZYnZVQd12MdCacV3WsJB6Dym3m0LFJrrnOm9qK9oqpmVPkEZcXdFn5SwchLte/k/AD
NmGCS0KqLli9rhsAh/kNT+VDf9xMAnE989nCYKVWqfCxS5Yo68hEJF4MKUbYL15we3sdnI/BuX4a
J/J5b7pWjlVXfWuwf2l8dWTokA6yG8ijhM1V0usUgZhY2qHfto2fwZENm7CuL5LOHsTZErenZT+P
bWQWdlqy+cp5kQ52E+B04v77i/AVUJQGVxs7u6rLGghd9h+eYg/Nxyt7aIRiLRHtomAekurBYCnV
RPgIhJ+BbnKPsm/t3dkZru3l8+YlDP3aFQyPluCtzLxT6UOFGgsR/G3hRqFJLlIBwFDLwwiQ281D
DjihvQh9vvxvVx64nP7ayNgEwP/HYZ19LWJsJ3oPfRNVqxRAi0CR7HQBVRbK7aZAOEvmd7EMijAc
bC3zV/Nl3LaBEpLQaN8sN7x0KEaOG7Bq2YNKf+MnX7HxVyK2GQLhlzyWz9Ld9Y2522hhfPuB3P8T
eos5D64CAw2p4oDaEodYxsrPXxGLxBQMSD8uOf/MWFBlP5gJXxIt/n7iM9Np21yYmUbAY+74iWm6
l/zr3nFhoJLbDvuRiogTbrTgYVPJ94EybyiFFkFTyRNHGujiwEuRKolJf1AASMlZJNHjgVZR/Axl
aiKqErIm5VHUeoPSFdTU8KP2/OY50cVIfMuTjUPJX1puRxnMuN4UM2HYBn5p1b/ZXBBxJxPg9wg1
GMwYbspbbnxCF924Xpy6TDAZqyKrf/fYWhs/2CAJumsdwT3IuyEpkI58CBfV6TPcCfzBcjcYZRaL
dXI/IHsyf6K652XiGfLwSMAFO6cUNIPICFamSbpER9G9tU2GY3TCSlTzH4sIMnlB8kiQwKZGpTpQ
VkL9EK2iU2Vx70jpkvxnMgrA7UArQoQUGuom4CFO6zz5fMCez0mVglgRaJbPMnxFMYcVEXhB4QTa
9nB8DC5kvt8oou+lgPxVM/0esi/G+QmFQ02leVGtStFiZwjyi+kDYsQFjo/6u2hEC2xjZo7Mm5Oq
j1BjOO/l83jRJwnMHposYfeKHMq3EOVzj7/mH6BXnE27mZOsEeKvA1LgyOy+uLHltrabP9xLOD19
8Ne+iIjrw3rMae7UzSiv39p0MHLWUbzmyMZsyOPXuJLXeyHIWfJXmJ7KdP0gdejE75R7gXJm6nEY
a9PF5JXkXmZzLtKNNdfBz6jHgw+tcf2mAlUg2OZO8dLEbiN1PuoKUilKcIcOox7Bj+nQrTwjExc0
0ms6ai6I8uaA4VpmC9CWBMA7R6okE8/bS1LhUUqAHBrEsdQQAQ2sc6YL3SoFa3LxvTfLAQs5iq9M
bjCU9YHPx65hwTtODObIrAi2FYWGJjwmzaFKHesjTSWQQ2sPtbxkJyxIBTKnxft5n25uPsyyMSHv
rbGv2HRcKzAK1LTYFrU8EUjWvtR3/Xxab50G4OWOIrGpqlvPtn5po24ZHspX+KRcj17QO1bTri09
JmaKsqVzc9Db6N7RGM19X51OBv5/sjsPKaMD4/lvUrZFiCEn6KexmJLB0n+LfEftwDFf15tiOhqU
yTey53GjoIKL6vshu6O9TEQOWuScM6GX3Y6F6FdSnsOLsKbrwdECzMA/HYWRq6jH4sZVzkyzMSUx
LTYOIS/jS3dZzdocw+G1DkJIgSg/CoFs+KFkeVPhoIDOCZA9i0oiK8IWyc1s2GS2Y3d6D2mYKGU2
C9k7WwO4vPouLP25S5eBiVs0A/H4100kKqp2f/sdSnZt6C9By7RQYAWGXiefxlJUr9jj2J8yfnCp
QJZeuclDatH5K9ew5ykr5/+AL93MCpJaSwQEI22lPO1IWsJpGY8FL2OeQ6g3iU/t1EsRqpZCiWF/
H0J+vx3Ru9e4EstkO9lKB0c91XAh1gvqoZjKoVJTfBDpDpTpHFrj5vM8deM/ZDRqotpEtos2kVHG
wbEWl+B/6TAyyC6l4gMAo/rM99Zp8dOvZeuawizCroVkeC/82LEZ3muczQHab6HvfnQ68cT61uvE
LKarYxQe2LlrjQfoOI7MoNWCmnmBtyamhaEiexK3lma8AvEsMHtNWtfp0ZoOSPalZTszUx0jmCps
xY1hn4CVNuLRjxK1fnUnsq/olw4yg5fJeeEvemc4Lid2PlzcXedSjvBqs8ezYha0S60d4Xki+DLo
kaumR+RYZWHl+WDqBr3nKdqed0lU2gEsGckZxWXYbnYtoSKbyTG+bqITvzMQvEYo4RUcmeE6Sc/q
1nM2eSWTeMvtaG+LTW0uqGeO/jsyb0aWvaZOm8HE1BPs/sjUqSoq3BfC98YAebYKuIc1pBvhCjpj
A8gr0W9/yty5EDkqm6aqGCOMBMcVhSGImiI6e0hl3hKOAlKddauA0wQq3EvJV1LjHuUw5KDGqNEO
IhVV+IdT57Iku9r8+hT0I1bZk/+frx1ByF67fsc4pGQJMQQ3V2Z5py9GY5IA7LwpBBLTaoGlakQy
GSh5u32pI2I4wUMAWrv/vQP4Z3N5AWiiNGVA65pj4ohJw2TeGPP87qiCHM+MtSaZe+/Dd/9tqyHz
/nlAttVUAPms7Q53t/9mbLH9Ij6oS/2m8Tixp4JdAE8Fh5vd5o6EaHLY5w1QtixqvFpT5nYGkDt0
tUV7nNpj/3oBbNPFhGxEVmyKJYNz9gfk0938TY1BM1q65BEsJ0VvfGyAWW3dvF3BRnQkCzrYjct5
82jT/aEJfPTLO3LgPXd+HeYDcrLYxcPkmsbeDdxGkpExXdF4W6tLgKRIK2/S38XU+prb+o5YlE7x
DYTkkpTahu9UKBCK7FWqhxoiAQHuXlA7uSYqWam/SQwxNg6StMQ51WHt+1yEvcH9NUNeTuFP1clG
kRF8BRa/R57uThJy7BfpTwRI0dSUCyzAmQkhjw6RtyaLz64Tx016fLO/gnaQ/eba+QZ2AwcIncza
w6/IloHi73cp+Wn6YBh/eZP1zp5sHsNOTN6x/uYlNRElzKuIo6AltVzG9KmfsrC9acGaS0hV4s2Z
mCeEpeeqbzBJdRAy1s4DOs9wXK0JVzIIicSjWzOm228T/zbtKbkcbKjrV+xipokHYRZiWpLsHOM3
2UBBCMxV49NssIvvaXZ8072N4Hrr5l6sm41kJXwkrFzDksppgCp/R/wwxrcievVv/ciob42k2OWB
g9+vGqX4meNLgqBSGSuXg8LnkNfQw9PghMYX32dpCXvrjR8V6VGI0I1SjA9spxwVS9DPt5W5/6Gu
kgbwItfgsDS+fMahU9D5xr1XObfAGX7XSPr5+DQDWTZ7pSzEFgbpk0Ww1mGt6+ukvsddTXQKNQAx
gbtMPPCs1siukq6rS1l/YysJj5gVcFnmfawCRvuP3tx6cFrdSIv+LVF1tPR2R+gxXUhxn53V80xf
xcR8YlwuqlylqmDnfCL2BWrwv5AkENumQ1lpoLOjygnm/ipQDHqxDDHXFKS2q9FyQei7U5QrKRR4
RASvBZRGXClOYWp2Ww2XN5qG+xrU9E0RiMyR1VhcCcnV3lDljqFrP8bWAubog47mGToJYft20aR9
zEEQsD8Md8CkYxAyhEpJ7oWYaGOMVgPb3qSTltDKS3vfdgtvWUHc+NkcaW2P/tyMv973qpFQTCzz
h8RPJtudYOpt+9IgBlKmNLhlTtJAnk/jhHO11iLE/iDrhZyiHZISQz1GK7m2rlcyB0iQ89K8KJsc
l/nwyBWGknsvLA6U0yfGXZxcmG29/hcjWaTPxjlFKHDjVgHmDFbdveBZWW/21mhI+d45FLfotXqS
3r6abVkjFnNLrCHoQ7dI7ftLRr3W1IPD+XGBU61XFMewKrQBgKt/QokIGKfYvbGrdwvmQYhdA5MB
6PcqIX3R6leVZ5HT+J/gNdro1sws8U7yPV1/cyT3UCWfFLovQ4/lpYJ6rFbeGKLMt0uF343w0bjc
anwvDxukF+aeMGVJhiD5aIl3WlwQHKUT5iGuKSWLudv3+YckWRHlsWf64CphQ+L5FGSu2G+xBAEi
F8uxUrhhBhnX35DFA37v9iKOegS4TowdyrSxlSJwlPSOFjAqD+en5afFu2mIVzOvzgJv/FAOqh7g
vtAreLvhK1WCxMumYIGzlgi4zmCUZhFfrHMn5hhysRB+5VHC0UeCUXPJcF+fgcY7AdGTzLJgR+el
CAzKQIuabCpeV5F9hXlpEUXS3pI8ktt6VaQm2YWrGBH8RMGrWD0W9w7+DS6yjMcxWg+8+SMk2e0J
FzCHPg59rrDp0LqY2jb3gkHUKAr3/7LeRCZsksJdj89onWy8vy2U+CPaqThtg8DzNzU141mrvKhK
UjElzPY/TuVKWPp+vXuXx/3/XrI1nF7y3gxw4MgTuh4wCgpVZeOK4fO7l/10JHgVBg1/k3CDl7R0
5NI9Gppm3WFFBcAwuoGZZYXH3cXd/nWp6Dc6+Cx2f/iZgGVWXtUfJzXIZUogKs2RzfCkKUwaNcm+
yMxjTWlVYsMjsGvubW4vGt++8sSZI0ckB8oLVRXfueAXriu9Tv6L2rFJPv3SIJMO22ROLJYW4OFV
hPtnDIyQJ1pk0veC0ExDvQMZh0CGwZ5h1TkABHLbcYo/yo9QokUkfVKjlH1ed1vecxPFCAFOnYyC
F5mJLQxVhruNp+2N6fd6ijP5aVEzYEmFIrM73CNNRBKVhc2Tqkx8W3R9AqCaC9CTx+EJIGPeRNi+
YEAOKOpMcfkdO8lFtbn/GBh89WEgTVRSGm8vMfOzBJTOxFHushlPxIBgoRteX8+0I20VtSyIB037
1Gqb59XrvJxBnSYtsosoex4bt/hZER7T1v01aKZ32xL50Zkt33Vf1zhzKBGvAG98ACZ8aNI9yHH7
gsfDiUZVkdYzh2AarCwoMPKcR14mxDNK+qbcdcil8xpo6PechKfbfYHYCXC5TEwll/wZxUcJWh9K
viRKzlZjKHUGnXyOPJCsKsk2EtwDRkkVbI6giwNp6l6cm+FJgTykN1OA+IagFbuueCunrUHXQisL
W0OF1A86uTc2yiJTRHdWMgcGdqf8goMnejrZdiNW7g1ibMUyYN1syayrOT7inrp6HsYgTZAcoKUr
CYv39iAgWskPBCU/xzs6GUl/sQ8bgz7B2JxWkfB3gRjFgAJeADjRblGAS2gIE1CkgOxWkZS48kp5
Kb9cbvlUKBXnWP2R3CdsIHPosHwSLhpf9mL/KGteYB30YnY0AtJpV65NSukyArOwsNNESNdTEXV3
90e1o5ElKWBez2/XbsoRRTiJVFkQbGrGxo054x/VvSroujCUxHwTqp4ntMan6C9Z0sInvaaeFYo9
RS3pbrxzznWo0n/U/Mi5e1mlcjzAxzHvYOTvpLX1xDtvDaChUYEZrnUeaavR7yphXRPE0HoBxOfi
+lsQx4wpaqVJ+61tQ429xotQo8ycJ4ctZe95JR1SngbOLEcY5MlXaLQbO4d8JH6rK3tEdlI0mLQA
bn34TAiXhUBKdkldOKnrrdueqfqwLh4HAk2OCMWT/hpEdzPz2x26JeuDjzIFRm3mjjoCsyPfpb7D
dEsfsTPMSsY0DlQgjd0bHWOMZ6J8wNYPWtpKoEeOtYkU4JPMOglcNr/PPiydQSfxz4gxmERrxKif
FGvfzvR7qBa0Yrax3vydxclDZEppo0XyT66Z6BfIS23+QhLfcinfKRkRk9ZA7+QMpY7ROHHcFgrg
LlL6BM49Hk3v4dpH85aQj9GZJzWUMk74fozzylCK3hxr/yukw8/3pv2ADUJXN2TKp2ZQABZZv5j0
KJyQxokJov1IcjgkdrxtKzLif7yckJ/DS79TfBw9VhtZgvPlEfxb1bABlYfSHdeJoan0OPbAiZkN
j8+K+efkvdzcQnnEjBbvgquPVxMoKoj1tknr/DoF4NM4eeruGB1DJFBIbZFCnIUkUZ8GDxlPJ8+c
8QaF7YMrncKIKaQa6qV9vb1XUeHugh2bGJpabav7IxGDwRX1BlU6QB8Myn/4tm4AGH49O3ws5EU6
1tPqv6jhqFHLA9QCRqfLgMFr+wLqejRu+t0enDbt9zcfjvCurt3drehl9eLTXimi69OgNWqSDQ4e
Qv25H3BnnEktlgtaPJaT5r0czDMEU8m+HPUgDbed8CfzPBn4gmH+R6a2iBZ5qwlsu3aoq40HxQ9X
qnc5dmcHYKwOoBDp80ZMhIy7nxyRDq6+FoaPI0PL6+Sf/1MiHNnTFKOzGwPmNKLaJNVCUUK1bLbY
sy4DzT0QFKASrcI+yVztibRsSiziUOv7awcrANvofYXFRSjGpImLy6t99a8iGfPwz8usDy5N+dQU
53wz4b5rmd1sC/byhfAy7fjhoVQ4hanDYMy02PyF6A/v2V8ShgL8JWhnB+el3Fy0VQNgHpArALSZ
vRFOJEGFbFuV5q6RHqqXhee+rc8orYjI2qoEqMF0D8UPJThH3N+B6jML16dUycUbKkCxwpyU8YxA
L1sCXV4mPXZJCQ9/ytfAaWEwCbSzabU6M/37UbFPDBMBZNpETSrpdk3hkTi7TQXbGD9Y4n0jid4X
RXgoeyAOWlDfGOzC9J7lALCtmzhHCAV+6BKQfk3JX4QN+Eec3tXIHffKybp/Yhcwe4WxF0GBSm1r
XthSdRxcFUmOjfHxGWxuBG/K5KnnKKAw7idml53hUu6yxqDVH4JV4q0pgIHZHkx1V2j8u91d2AVz
11+W3zlhFhO0lIgrZbKYIhVda/AodyGVIxdkfBPkl1Lm+5plvjIsAh+xUHUxVuDHFNvya/SF40ml
DxdUFltO+DySoD50F7m5VWxLLCWWrrFFnJyN9pJRGpob2VEhZ4HrnLvAlFTbX2ma8HfPlAJmrgjc
TNIR3/0rmT2Tn8PsFK0Pn1+01WfON55pv6VQ+SmiJKLwK6DttK+S2sbBsOYC7uhsJG1MTske1wtT
ZfYCT+wrwtsAkmH89qorlqoCdW6ddR8iKtFJkV5b0cYgjt3bWCXEm3RaHUIVOH0Bhp8C3gZKWY9J
m3h4yorxGKGDmGKJUyPsdE7WJrzeTZ8jPbegnl0gd1jDTLYN84f256nAE86aCtqJM2QYRM1nlVpt
3IWoUKGLsSo5t+i4w9PnED48PeGc8Py449J7iN1Cru9M7Y5kkIFZE/zBRq6xj86tpydKPvT7Wo7y
bJtlo1meJUjTYqrSbByO4uy2iWCMtjEAxkc58VE1N7GRMSOUwfYzCJUEh74yBf9R02u1vRgy5FYH
0LKW409zjI3UuMzPiOj5Q+WZGkhwrmqbFI0PEiJsFqkhCmBK1lAMLldMY/0+ODXBubDy+7SoYB0e
rD+j1tOW9ADToEdj/t42OS8uvWOwZgZ3GyZvIi2uWJ+vsldJLIG1E8yfzxalKwjmQte1SKE2Lu0f
3EciZRXt75eq6wiAlTbGH4kRmp6SnQe5vSG14NHB+xAPfLsUFD64dwOY00Ep30bapJIim2rgRwsK
ZqWcioOE9asJ/+nuMKUm+AJ6T+48JVb7RTfFxOMTIa9dpqvGDttNK/tAk0kXmVM4SRkMjPwfoWRM
jnCMwNyPe0xXyJJ5ZqjzjFWnrSdmP57DCF0gxKeNWUTzf9K3AhxL+7XEBQCX6KSpkv9jjL9E1+Hi
l6SHhNnjp4m6o8nllF+P1zpEGA4QcFfH6rieXPTV+WCDC1Oo61l82S3v9TMedLeWcDrAizSS2SyD
x8Qsqa/aNNSLmA22Qhtg2p6RkZrQOikotfR9W1p/cMLX1TugN7Gt2LlZLgXO61+usuGa798ttaCJ
x42+IfHHIft07D7B/NdU3bcSpoUlVJzb8+e7/L//LOi0jaUTzBqyW46s6joD8gMvLLtRhwcCgVxl
DjkxG2zHuNCmKjeJAXc3BY0Bprs+jgD7PwLHpnkVLr8V2ICaEGTNRShCQHZS9GNEWaaILAmQS5kD
dvTHjBkYjnsfGpvzI69rUdTaSzEKAI2tiN6AGIRk7YToenK/pEKDX4PIiw6eAx2Yo7tZh6emLCko
bq2h1AFg5/CKGl3JDi7cB9OjpuLPjl2tH4Cga8ag8fGRyNYxcjwBLWk+3xoz5/yx6n5vb03Lb4tv
FNFTJeSdPyUuGN6SVP2gX4Ynq5qdMPu8+KK5b88RMJxADoKuy2Rl6itWM+ttuBBaXGiZuNTU7DD8
fsDIr6TUKHpyR+dCbF037aDyFx5g9DV9/JNyKBAOkQytSnREyzM64ngRqAB7SmaSpkWw8dDr9Y/+
QEqvjMqEQGCYZdTCSsOTPrFkgOTySWN+SaMQSRLVzGJhEGHUYM+pHmBtBFtRKi/9Trfbquhhf9Dx
QrGPckMlKIrjR/YTAovrOXkCbs27PY0N39stOtYo14ooDtJKjnER/mbKWYxdDpYhEtxSo8ay18eu
t+bIT4Q7yj99SctX8mCY6rqDD3Ef2TozSvIbtp3SnYMdacz+Bw9sqivFyxnoQdFNu+j0hC/i2vR8
ghs3ct2Rv7joWbf/BQ08U9SSwLrUCz1VpBdxKUUx9tTIoPMZ2DxS8ZV/Gb60qw41Gx0omTel1HHq
CCBkvw7ZLGhc3D6eQ3TfX/IR3B+lJhQiG3q/Y80UUpvGdwTdllDQJsVheJ0s7Z7uhifPZL03Kaos
iov2KNGewQAg1bjTO5bKYd4Bk/eOBJk5Jz+gWJusnGq5VMsb71EWMlPN6oFxcZrefHEPqRZD+Ez9
BGYEc+PuZIA1W2X4Ev/htXRU7P/2fNuZe0lPKvoIR/upK+34XVAVEFvbVfzL6s4Z70eA5IehaJQt
p4gcqVgVjCJrNLbF8DNMAnxgfqwKzqFECWremCD8M/pRiNRTgwZ/ivezhLiqlABixtLDOL+8rCZH
Vha9cHIQAfALWBRiIfw8DMyIIdaEAJTN1sAr16Hv1+AYM7Ufo8msNPHYgrNaNqh5ctDXUSEK3wbA
55EO3Q/76cUWsTxT5ZIcq0myr6mR9S0JrRWRx/DWbpYHphJoCYEejk5eTXjYt2Fc7FTc5RzEt3lM
PFQkA+1ywoT1x4cj4p3Wce8saiTAE0ffdsXO8nQUbqdM2+yz9vUVmoSHS+18H2s1cCxMKPWhca9i
EheDJXKWo3loA8MQ4ebPB+zFjhahqgfBfQCyP6nlyhCXm4rhxxBgEaNEAYNlpPixlxQKv4obvI/J
Rv72ffNcj57lIXvQpDmj38uonDPJhj3gqVaZfhXicMSrNMkWdqNCpyu6z3kBDIT34472jgkYJHD8
a+hu1taHezVf3bVYOOyJVRJM6UDVB/KPP8UDEs3hqeG1o9trmTAgZevyoVChk5HTh3cWuFTYbWMg
LOgLVagR248ZQsUzPkyLwUwDf2mtmYKNlAUTG6oKn82w5q4qyaxVXmCr+iaMH6zpeHpFl2F8g/b1
fPp68S7w3EhR/q2MHlGkBErWrn5SWcKlaY3zIu6OlnmEB0TM/5822NYjjhz2U/wO4H98MACtnIij
E7ju4vzSguGIWoSndoFaQdZTmiaq9HKCYckXFQWSJBghJTvJ2/XT8DAS28gYvVPvpiHhI64bgq9O
KxZe6RiLj0Q2tXmIyH2kng6vMN3ez4iQqoFWFD73mwmvuFUMKagK3lVRrFuWVnOhpGRiW8crlH+O
uyosRLPuxCb+49X2vOYPoogDDkVPduqqtpkB10jAOyf1Z+lLAn9GU7je7x12y3UMLM7ZnB+kWZC4
styA75Ayffo2qOHLDOKOXMG+o+vu4Zpqiu+/cjXiuXesF4noIizX2hUSS2QUpyRtI/J5vCVT0jKR
6SJ/PPAMwgMXbP91OFtBSZb/zjoBtpVdHlAUczWqeFOe5klNJxBrJ++Rvvre+F8jxc6OLj02XcHI
P7xIM6Oqg1m/lzFRjzpyGgenQAQW+oWWNyvLydXzW1b7PaveI8Xh7UKDnFSLy0mCcOmf/T/OGsvu
l3uucy1tEUc0wL9tt+sSxUY8H3yRJwbT9Y6rRoyrqZLDjCOGrf53kDtqlw1jjJXh1JFMsJxJmOcs
w1tqgy82sjnn+gOAYAfoDHz1BFuNzgDyAP4dponV5ClknzExb1K+ph0lzgT92k6Ss38usIA8EC7Z
CIDr/83JT7RgZ7IdtlvdyunDBEZId7j2Ai+zBVJKZurIDBcNxS5//ySmTHGLRIku1BsbdddEkKda
MhUzl6gAyGBsJDWSjrIGATZ5KArV/n5YHY8lg0WLnFFe9t/H1uSfcsViAYB5I1x24IUJLUre6P5G
UlRzQvIkkBpxRh7YFeDMe0oi99cyL5ryh5+EJ23RPwHl+kf7BxhfpxwZU3EiZQR03y8t9i65v12W
gKzxTa0sC6NzMlMwIzW3y3x9c8bUQ1KVa3VuiE2X4qyzwUsI0eezkfxHyt7PfmmuUs/2a/KeGNRf
LhXnp7RequmTbouESsyHi+h2vYo6kONpM2/XOLo20HUiwkmA0Ej4OwAS+0nLM5xA3XRudlq9gpb7
6hguLDPrUELdaFOOlsEcg/0HUvd8lRDqjQ8+OOLpoGI8ssIE4hxmw/g9qgBQwvn7hDrKYuGKqgw0
0cx7p3Wqt4HwzUV2Fz8gZ2SXK+b82jT4k56zFsndB0CDWAc1Soah7TmRGUJSfKLniU1GQ5Riu6xl
OIyfVoBrDQ6hgUVPb0M+kDYAV9y/7zYzmHWES1m6Aj7pkH6ze1JLof6QMa+SnM+eaZFiWdvwfc5j
Th5WSCz3d2pXYYU9PXMIY4JDhGHZqHd1GIITVxy0D/c8fX76dQTs3l0Q3sKfK7ZAYzebKHeubBuH
+vob7TsV1mwGsruNgBj36ybghGm+2Su/K67J/KEZmi3V0tb11zrmOG1nSPbaXa99rJqIZNq2r/CS
AWbJO3T9fK06Wq0gDeuUrHby38Thd+oEB0g77sq9wKRmyecK4m2+aZ552v0uxXB2Kj8fmTL6oLKL
0vtZHaipfkzQVVxc5UBIIgdCGRmemdmwtCoYQPkMke1iMw98WP53GZTEHfPu7QysL/lzXw6mB7I4
EpSjzasrbjAR1/qY6RP52g1jnkubD4mLt39ESgKjwR+Y2KVzed/P6avpBEMha7NUbbRMgYNvmDlN
B26nLvv2giLjv2+448Wi2cc8DFMOsIRCK+jZPp8Zc9zNEEQqpey2xs2w3vD5bhV9siGfZ39HyT/W
f+RCGdbMhzOWwilP2ZekYfFDlDj27zlMq3s6194GxKZfoD165SVOkzgO2fNHJ8EroMaqKbffuRFM
eNwpuCe9lcK+a/Tdm7utKXwW5dIJKCTTjZof7t5wTA03XevJKOG5qGZlKmr1NfAknV4HrB22GuZL
AnZ89nFxIIXPxmLixt14Z9qd1iewBbzSGHmC41ejR9rEvspmjz1Ki9HmZu+Ur+GSiagLPpeBpQxp
at/NLMJcT7BPqNUVCK5K+1OBPO91BsUh36d3uw53Sv1XdHZJ58p1aJY9smMp553kYcdRiouXiXj0
AHvC6wmuTFYzJUh9UQifjmFvvSyzuhe0ngz6qlIBkypW1UGQciAEfEVNlPMe9aECG2fBINTU01uv
g/FAebxKciYqa5TR2fgL5m+gvISQgOpke0HiLz6i2z16MLarAwwiNc7Qc+fFm5w+FuFNbEzRYF4b
HQjpyqT9u889zYYtu3MpNQGg3n7PQoORSj+obvh0dQKVP5ClbfbI7qJlrCDeZXtAhWD/gSImS9X8
GQT+tfkKctQAPv4lR6R8q1Uav9hFbDO2n9sVNSqmLnuWdAHLCk+m3dc2rv08aBj6CwZFtih7DDP0
1S5eixwvS+2MB4kabHT8IAbW9fh6oSHem5cfHRSCgq2UQVfieQi2d1xGiBJY4oTm/GvDFiWHus5Q
Tpl/9Tm4NptdLWYG71X1NVR6z+vFPu10sA7t/3DmymB2p8BDPoWV3/cXN/+dMwVpocVtSfj/0p+b
DQ4A6qg9Abb03iA8di7mIgHjZnwVURGPjHca0N1QHmSTUOTg5igQkY6jQizV07eEQzhYJ2mg7RQq
bAFVkAfndPWFgj8jKVcyoDDGN+t/soikAUTb3Tz/Thn0/phYyGJfqZkTwA3MPlXlJRIuilmzYfJF
0B99FITg0V43VMRuJQSFn9t9plcObJxAQ1MDg+Uexr1Bh7xBjUak2DLgIopy03hdVe2J5scytonX
/arPxPXn6GX+odKCDtwkXxDMabZOhNGnDhlT/U/cvM9iofUx3zumUf7d6srHr/zv5B9PnSpS2QMX
EXy3i122dDZ0S0McxXhBH4BYpBd3wV7RKtCF3so5OVfBqMAf9Nz8NVPFXV4DbYDI+JqrDpNV4VJv
QeX2XdqC8LTSEdHobFSM6CMeYUFssI2rWSjh/9z4UYLWiQMSKWlPBbY+MvzRuqkRCJOWcKNzfbAR
YTIy4f4+Ftw8S/3vsL006gf+nIPlYf8ka0EQ6Qr/31U6xeQtKqN61GuzvN2brnIwUjnze0LxDL/R
SJBTlF1xKyTtFTymCKoKqrQBv+FT33K23eof24uv+Ff/qve7w2u8rS19ZaVKLgExDRC097ILv3gS
yoHNnyAkZd8G6bVGHH6PKjPvAPYP9B+zQQgKRYiRzx584Lzto/kotXn9Jt0+FR7su8305Gcy5sjU
YispCLA2tPqS7aX2zbl177o9CKSbzEmaqyoqe+YveAFw0xq1nbhCAnazCpjgiHhtQj3AVrGROJxc
Uj9aM5Czrc6MwxocNGRzJds45QZn15DdOTSypQaa4CnbI0s9A5N8zKlOgwjaWy97K9uRGwChkm8/
fxoaV1sFn6Ifj+s0gzMZSkY3b7R/lFZIYA5z3Wd0O/M7/FMsmUC1DItHBSU2ocLhO+i5oveGdVYB
viN/F3xGMlr3zrCf2GRK7LdYWyCJL6hwD/ozrl1Sg0MQ5Uy/7wgP4iDJj6FfQpoaGVmEQtNKUHxf
/3dJjLjAL8zttVD6WXA83NOOQ/MTClWDlVQsjtw6u2bAGVLlwKXQkER2x/cT0RjBw3C3BfqBHlwA
s2pUrWZzhOubhQoOXRQp8ZM7Yo0YdF4F74Shy9zGhZAWzedbRxQ9SXNEdW4dl9IcGQOpGlcW12cU
L6gVcchygZOIas/SzT1vX3odukf7/gdTyO3e3KwWvhj5erfBxJmjfOxJVQ6hfcB/3+7XRHUD6+VS
CweazItrbXKdM7P5BmaKGMZGkWPM1Q2if2g9klgT+UF5u+rprMhizPWNlWSic/yqeGpzglk7+TB/
ABdUQC+uu+KmvKxr1pxv6XPV5OGuviR6H5bVjMMBxEI5s6xMdzqZLFWkTThxbvotGoCz3lsJGADr
zMbsRmaBoJoW90S56GG1cFSBmAY7lrSplsAcUjAc/jBxlcYdUBd/81UMAXlJQ03jjEHWHTpzp5MJ
clsBAw4pq6DEtMUiZfhQypqBikxsa2kIetYDi5QKvm1WUFFa5AwJyeCKm8Zi+KIk2f0UU2jdOl2i
EcKhoep26frtynFR0npvk2QO+PVyLma1/s9XO76Bs088NzGvrV97n+VPey8qjz4Lfj9cfioKCMgG
aKcpGvgxgbUbO/Y49q8DHR28WW56MAPLyQku+gEqlp9RbtLOTvcH6dL7PtJOkIqB74EaFYMIuHZe
NB67uhAqy0zA980Eyeud67SR2ueOvvfZA5eDcdtdOFFBvg4NqJTdkP3W1RELJiYbS79rjumu7r/c
djWXzUbjiUXnCB9k7PH29VGx60gzIQD6p/0km6kHYAo5xDjRgCFmwF0BpC1H9r+g6ylYgbDqoX8u
hv9Z9l+OLrrPDcnFGlKqK9mlSnQLpijx8z6Rm3tnjogetkKeeZ2ZGDllx2pcb8DijQP1LJpogQhq
k3xzDjdf5A9br8QyEof6b4ijezo/Yd72djCyddHwI7kToLgeEUCsuYxalDwY3Dd+7HuTOw03Nbsc
Acay+1bjNpNsqhdn8CM4WzqUZAp+aaIlT/zbJoos53h/KAgF5C38J1fZ1VQQcMewKIxrZFNfAIh6
hsEVxy0AIEMAleYZMwfe4RDRlTdJYT+DGQaRMjDLMex5FDZXeKwMiBY8ChSsXIi2jMN73r1nng9p
SzIYNAow/4OJsoTyfNxjDWtpAVHr5W/o5Lvu1Bmm8z/B3AB3H/F/k8R4PcXayqsWIo+6pmwO6ZnX
fhutexCp/l/g9ffGshKK70snUM2st2UBWckpMSSY2feiccROhZcam7VzZLed3PE9xkiVp7hRi9Sr
4xDeoebrpAqhT33a4acVdsRn2epF7zcny43FmTBaEYQPYeyPmgusUr5QbfOHmZJOqFSTxrJBwCBh
g2grM4IOi8KNOme7bgWE6Y565NKVjFGgUamYBqEJvZ5KQLMVZ/ctfwgLc5xxrqGCnXbxAible7Jq
ZpdNPBUbCA35dkH8ysSH4R6Y3U3V1pSgM47hhWC9Te4VGy7NvSCI1jS++SIkiaeB4n5JZLRQCJZS
t1i0RCTrma+5/QYAAJeAoTuifA1TOwspf7s4Lb9BzbB+pUcTYNS2wUhLCHN0kHEy246oWWE/APiM
tXXfQl3eA+V1vUYVVGd9hKvWTNVg4R55q2ODH1IzggxWqV6wHIZpYMm5YO+5e9ibshlDql85p0GN
mg1wDrYoMvPTZqYefFSthOUpUkYmjDM23MQPSAsKPYxp8wkiOvYNajD7BYe683Kdd8JKya2aXqmK
wE+IWAHRGUTqmKbiLxu06YwJIA+xlqtMrAXy02YVV496fOGBoSb2xAkviYyW0LMA9KB8adlw2lOa
CpRqvwBfE83928daM3egeLtIgtpNHpKfevzF3dxfs9sjjN3U68adnAu0kqmHnOTaeO2C6G9F35SV
KDQtU2bW/4QhmUVkWHEB4aNmC8VwMHVjVYLx+AcUGH4Rcv4IKJA0dMKnrq7OA+qdSRn7XNg2v/+d
8wjED5PIClxVrDhckyzFI2iWCqCJRn7Zb4MX+i9x3M7jU5LoZuRzhMFjm4rY4WXM80S0Rd5ePHZY
2tIEU/fyG6osWGgbpM3QIunjHw6MbMpYw7DTkzdPFlIiGq5j+RDdt4EE+4xlVLJisUMudd/UAqF0
gLGgiwV1Qq94oQOREIRZt9/eFYHQg6a4ECLabcCJ/oimM17Om7HFNEI1SEOyjGO0v2k5v+Cfouyt
2he6aY98I2hN/7k04FfhW7ynPVFOhYhoUUKqNRHJ5BHyyukjJ3MTMFuaf3NJRpSf9rfCM6+vF16p
tnZplBuwc5+BJPqcDInLeE0V/RGFMsWPmEcYSQjaZKNBVrxQA1IQZ3FVMVF4Jgvf77PPc7Z0XaHV
0mmBvE4nV/uLi1+8mE/om1cdvny8GflGvnR+L4Upi190tl6gaDWdl9ypj3AjN5xDCYQfLizTkglo
9bakxKDK77E4zboUrgr6DHVriXG+Pr0cm5VBw2pwBFTYOQM1VFuqqH8ZiF2Bxz7akXRRohPuIUEo
seVKdQcjE36k2FWTvAfunrECrGVmsooZGD6m1IPY9xGu+p/huDA+3fCrlACyoMNBIzotiITJXe8s
ttEqe0ORPJti7CnroXo2f4YasjOuzkm7kjgCiS3d+21jIN6aW/W3iX5ZNmC9QtLeRK9Eb66H52sL
iBO2iVFikHFfjJoB3qEm5vrFf//woT53DTJ3v5X1AH/Z5ox0hTueM1dOENKdPBMJcUa3CGZXt1cY
SBXQtSF2hFke1bn+Spx55ERSCzn8AVseDlbbhmFjWPmMALtd7xR3xOHSQiHNsq2EPgbV7GZLY9lm
aJnmB6JdjangBO5oz+g3Fswkchu7QFUPHan7jYe9ZjhBYO4AaLo08XQT3UfqAuriiDKZLInDDFCN
81fFHziQSGFTMnAzPdib9CFn51ApzhtbqhtPJnz9kPCjMUcK6y2OON0AveTeoHsnlx2EXOJGDaSW
FnDkAuUNl05EBccYSadmADPSvBn3p7cIwpev4O93JxcMRp0oNBA/B37XSRBDDA8rRCozBIGzSUDA
P4lRUvaceCk6qhh5xOt+sps+mxUA2Gzi2dhQUWt24ykLKzeRQkk5qZcWsw+piXZYls3N2Q8MGnZt
Mf5ne1bAOhN3lVaHNwFzQ8RjZvRReDQz1bmcyC3o6vm28mMrbODZM3JgNCZT+ozZ6uKF95r90/Hx
QmXb/2Ty+LWxsZuhaURUJSKEKBWgprEF1FA74vJs/A7o9fqvjQA5YKW0uhU63ZfbNuyrzQdi/ymw
iWXZ8CqtuUNCG94kwvXubG4MkemkUTGlNlQpo+5dkeoOGDzTKBqOqrMZqhNKeCNtmhZQ/sZZgBJm
ZNbb/Q7t2PHRpNx9T4sTHx/bblxm8Aeuz7ZqSR0N/eByrlP3tvYKc5O58omKzxbBRfsTXS3vZrBo
aZoI6Lw7BDd16l2y2FALDyuaO08mhEvKZrgiEUNg0QJRIHtD7mQWQpGGiQZu3Ip3EdYgJumuC3ZT
Ye5iH/7AAqchhfTDVXxXPnkHQi23pbECpyO9HrlJxwp2J3sfgoDHZEBqQJaotJxGHUo8kF5beaL/
cUpCa6KYtYdwoVdGLm9CYjU4/P3vzIHYTGSL0SfAPl1b8UZVJ0vnFTP66hpCLYAPkYV22GNSmOK/
f4vRnO1BLmKcPsLC/FpCPokGmV0SmN5ecs7xZ1QgA4cQrGH6TgRmxlLnNAFtah0+rm5msHdwCc9+
WG1Py2KRbay7KFnLqBPi0jUcK2kNPZ/jmwevPZZ0vMIP0imu5TU2ZaQ2l74vI8/+AGgyX2wk+jBU
QapjhuzLgtkchOz8bebJBe/+iSAQiQI2Wo7ZaBEfTaZdf/TfuUs+evj6/FbCbHXZ4j9Pi+ERDKyW
oxT2wh7uRs6FMgmaid2/65PO8Cb3qbHDfdUPJ9rIUeBx94NW/ROC6RH4/HbbxpZ/JtnwL9qN+n3k
dtv5IwFoxgKqW5Y9yJgpaJs4GY04kfHR2/uyLL2QwOILRLZwMurlgUCkezRZxNe7EUG4FLf6vUUd
+jrJe1WomUbRfWZfSwhwkwyzjkNQCg1m3On04byijWktoRfu2GIuXZhITqanZ064npGRUSa3VwPQ
a3ANQQoW2uX78fgx+os3EVFdFWDGqMXOHbWnLDtwUdEtOVT8/21g2yDqbEwa4AyTgDPROpVUXiI8
TnhDmn3GzDlCaX/zqb46nv7KdLQgdDtHZnO6W6RjE+90BgF3NPHB2+OmPtMpi7CBaxx60jQhAJqI
T4PYjTHh/dmqkM4ZehQUUVF9AGYn8vZcd35Fpp0cqOgjKTFLxviQcMB9CBLm3rWDRSxOHrufRIj0
uvsDDTa7YkdnWkMei+4Qh6ds43cIN7vV03o51O6Y0CnJHAj+AS2J6zllW97EZQcIEZm1MVISmOyY
MLGpsTDKC6CTZwWp/WhbMe0VRsiEOXYh/663+s2YzQ3rklXOTnUt2UOig96rUPxxqdLhgthPCILx
ZnfE9I3qU0lYqzLMDYoZwke/HJ7hM42cMShTh0VU58tNvmBTShOL00V4hPci6RUw4i39Dh5gpWkY
xYo5k1Lbpx+IMAR1o3r8uwo7geokESgSoVqy/jBIvrO5eP/K+eB5M2cI07G+zJCbrqjA1G6vwEmI
do5J27j0tvHKD6adi+bPWlpfKWEahdCC4jbViD5W02WjrpkKg/63+5/JsCZisl+qETL5UlOnfBfZ
iBfeu3uS+MJ+CtZo4mKpT/9am1t4Z2ZUCIs9GJtr3HkufOPVilPspH0G1WijFU0Cao2DuqyJbK1X
rYwSURA1IPGREECJj6K4Q1W52syvMdfh6b//WoE8utc+cbVrdu32AQF2+601MuzqamjaH8Rf1hSv
xq7V5qwFIeGJbQm38M+Kkch8cAts3MwVNBdSaO4pBjuJ6oV6Ar5Z/bzk9sMfDT6UEqzIvCXKyNXF
daB6rmZAxapNP2JtV3IQryqo0LxYNRQU/DsQgwNRvmrdC0WtRVIuBEy6cHjcFyCbwTKyO20jxzWu
bioJAL2DMlfah/YB9v2EH/m2MM67eZ9hnz1Qu0foWEPTFPgy8gdVHP9xauG/ktVEzCWHiGPQFuX6
EpdlDNDA3EeA5lV9tNAshAZ2tTA3MohxJGNJlj7bNxbRtMzWaz+mjPJw0YyFXbThPfauzZCnDt3m
/hL88lPIqKEM2bzyho1m2ZRBP9ygmyr8vraLKWDv8ibPO1uXZNrFreLdxkmZH/DotYwBHIKSQ1jN
avxXzETJwaSMlJrRlHGd2eRZFP/K4bwleMMPkSngVSqT20amtU43botyav4az36y0QVkIVHJI/XK
AQDB8nIAipmCgH3FYAwu+3rJ9rlXghLSsH302bXDoY4SOfX3atEPOSADHYWpKh+c8NnZE8cGarSL
LZpvE7aNCyBkrECpeafmvKjgzH3ByqhmToXndSutXXP77+3dHSRgp2dpTb71ZkY+RxWVv2r6GUeD
4NYMVV468xq443qgNpy9sy1kSpzAXjs4Dt888s/2pf9eaAWs7j6MW291u3apWQeYc6RhR4qUHy+z
+3svN11j7siUJCjMYCGlVHFLsQS5TYwTqCXtMypq/aiy6ASOBgNZ2HQJDIGrtvVWccBWPTWp8X/F
CDY3EaM6+MHhcOEfdFA9uLWJTjIGfUz8ujakb+XRCLHjtJjzhzT8cXulcYbPQRN08GooQUd1/3/R
Nl1ivwFeTvRtVEjbm1RcGE0KgVCJb7gnsrUvcHQbCH36g+jGur26Wr4r+Y3eQF61WmzklzA/n1VP
aCVDm8A21wVOfTVVmH4BRBZn7+9hslQUFcH3gvbIv58BKnEMpLXsYjHOd6nDnQEwVyc4uDhyBcKK
BoAWBNVGJ73OhG1ty8W6DKkma1DH69NVxHrnsDHUF3SvUPHHqT/g8aveOSoHvjiuD4pgJS5OmIi1
B7dldBIUOf9vjHF0p0wViLT3OD9zmG4RYoXb+KooyCKZLGH2dHq/nTxor0aBjSi5xTFE6Vp/EjGl
u+i+cfKmBxzFzEXvmEjbhcCf2L7zlBpLLR6R9KEhbPa48WejK3jemcLxGYc4ogmk04hr1A9DAIRY
43TNJ1v1bHMCbg5SvzvDzcA+PjuFHbsBGUMGq1UW/nSuLJyMtA7Uuhfo/Qw4L4PSVyndziJIbGYD
AyNITtem33a3FCCiABURuTnJo3BHdmcAeXy7F+Jz+5MO9jR3Dkb79i2BH1iM9NvSfHxiMDq2OPn0
wi4MrmDIzl0PsAz6sa/yzd36srdDLyUwH92K24wp8rJKRQulz9wXk1UPfNz7Hl8VySDGQj+HcvqW
IjTM61Qp8OIcpWhdN3rIGZ5Oh/ATm42C8Ef5/SxBMlzVjjwiO/DaDBxdUO6WF5FteC1NrJveftdG
fbx3S2P4ItK9qANciSoMN6P30D5Tqcy0dUJ+ed87QWE3W3cA4zfq16lffVrcw1sPxbLYrelGfbSJ
9fQCFFR9AZOnFAIAvkzHat46nMsxTIOMRMBgOGkhqX3wJLhrJrROe8vnhlzylddKq4GIR9gV2vB4
n/ZE/RyNokBRKVA07oXkilSkzP0HavZ4vd/pBuI/6K1iLj+Cw+oTPVhYuasVOASxNPUUzDJA3JMA
2OizTd41bikSMqOL4zalQJwgSm6GaprpiUX3KVjxlenoN9P2I6rc8fKO/lRQ9cfCkDMh7Rokopos
J/7dNVM+s0TR34y5OVcf9WWKm3RCD/KapLWsqh1bbvjjbOcyqu42aGIL+hqvpkidqJhNqQ6COy4Q
WWRz4xWazsqvQljhOBtlj6JEurws99rEbhNClDtYwe0qnWZaskjFfFuC/g15iNJi5oCjc8wEox/M
rtkvCIfh8EwiSXHFAf1zvsy1+wOkBGCnFPbXtWB/Jcw+Gw4PBRe7dfZbKoiFEaw27YLDV7oVtFG3
1W+5LZDWDIWecFhMPc2HOuhrzl/SewFGffk9gd7Buy4E2alm9cTruy6eC71K5wUTOUXLqefFXq8h
N9ExeaQzIKfwCa1/zSARfVFydV28HQDevRiw1o+4nu9F8OVX++mhZORvv4FrDwEuEN5WUHAL63mR
7ax8lJZ3Jn1WjUHmmG/0GzIlot3U6LXB+QGNe3tZtS67rhGasC68r0R6I4Y4d80vuMV6dp9NxDlZ
JlbiHc+uQMEkS0dAY4133B0BSQ3WoDTtLjf10SjjzXq8udIOjVmCUZwOydGWfMEPRCsOmWey52mp
fzlyM9GJqE4YPQ2SGjPsP/EkzWF5DEd/PVaEziiowPXZJrAKTiCHNW3EvU1IIqVqrVACYYEtr6R8
VRdtqM55blaYoYI7FHKksdf5W7kL/xdBguNPJIJ2v6FCpwhcsF8Y/qil0/59Zd0jJvuzGpsiBex2
T0uUv1UZRQP8v0SDrLVGipc1B/ACMih52Q3HwGVC3YTWbU0znnsXWUvEU2s1no7LP9xP2b0UY0vX
oxaUl07DZMLNr3t+GvRlqXIsRmUAqSx2up/4VQeDaom8vVnpr6DM3CXa4BZuW6xR2xXShtBxQef6
YMgzgF8DkWX4YujmynnxasrHa8o4qgu6pn86dZ0arihCgWDcSE3UPLl+onjsZRY1J50Z/HXdVfLg
1md12e6Bf542EA6WXGLn+O0ggAEnVezuwXVsO4HA9Hg5TeszqA+hSq1KjirL/pq2sUroPCcgjGLG
zPLDkrKeXm5caEYbFgpXDljU/RiIr5Us4Ty9wTOvWRmQY/EMn13E+S/XVrwqnGW9Rbod03oCOK8P
lSBpQNAukCWx/K/y/s2hlJ7CxnkgLrZKDjvoZW9EmMqS3XPehEQILZg7WPktCgIgkn+B1G6qebwQ
d9Nou8d30qD0DZJd4Fpk8Pg04mkmVz+ZwAkkEpVFcQkRwPmWoTGp5L3+lzh/rDL29+MfhP7kc8zA
s3BvoinLfHNVDcA1XKjZSwUhWjZYgekjYp/xKlJm2or292Vr2i5NtgLY7qU+mCypJIQKGTihsmFH
MMevz9W1Yr/m0+z4w2SB6SQEgVneva/K+T5cP5C8BWOUGnE+//SkAaH7Kltk1jr7uVQYOdTz30Fs
6zgAi6Ubm40ih31kHof+FLkeOSXHbWWiRZY3DIzSSHTul3uW2QoNBxu2ezB9UahA8kqk/K291QQy
mnAfemElUYYUOn/uPWPmTqrjAOvTTxVNTHjVwM+WHwsnBeajGDrsfgcGRMw+PGjsv5srDK9mbrfe
2OuTK9XGDN+n+eTWW3cBnmruJ8e133gK64jN0rw4J4Wo4ut/CnXAOMhSZRJV36NDceb69dpTGMTd
CInEqI71TS1WlncnVxf0EsSWq/TJAcrDoC5WlFh/p6eAa0x2yrz4wo+mbVlQASZ9he0Mh/N/dBj1
n4DtGczKZqKYZPFc9kCnFPzKupIy49mwjbLXA2lVKLN6IXI+oecoQ6OOgklA0PdxDp9P8iP2q75E
XI/mMdl6aY7OGt1wB0Qn2RC0iWTYllDiS7Hkxs7JSAxB8FmL7eA7+5wiadDUMHTHAKq5gNOIZHGG
rBypNuKfjOe1zfHFrqaDStE7kF1odmanreGXTsv+idAElfC6OR3yumahIbFbhHBg6Bxen+p/c7wG
V2F648se+5ZL2g45h1ltCxSWdls4kwJOncK+pxnX+cuJJ92PmSbDBr0ptt3UiKWhUfw7LPXBMqtH
IPwxMGHZznPQdbC1HtTCo0RM/ua4oZqwJyyN8ajsxpX4fd48ar7xwucqcIUaivgiP0lnDLSAjhf6
VQRG8afH6mjcspiGGim0KP/sPJM6xFD3GKALGtUj5+ZncfUooy1zZWkDofkuT9SoNjB0U+IIyusk
XAjddlBsX+eeeu9SMpeqYb7blzlZ080MpILEH0r1wjdY2wrFuUuN9+78oO6+EhRADsvPyWQZbqCB
E+87jxQ9O/qJsjyLqf/Vk0ddCcOjkBtlfShVg+/WO9AJ9S3uhbK1Rr4YnDdWLjIxOH1Tc9UKvIHx
nVz971ZtinwwJR8raleRK8axEKwPqgNRB5DFSVHcz7YaWRekvH9x1CAHlKX+qBvNpP7Z5AMWYSSS
y+blchSWG3g9W2OjLo4y4FBXH5W2pYhhXL8RKzfP9PKVSnmVRJwZ54WuujcJOtsl7weYgOr/53qA
rwsl1TDC9yV/G6zMVdkaa/q2lhX0e86KdI/fK4BzTVXFiDkrpXpLVNKjgaPAv5gve3mG0+Fvxu1R
BlzwV9MQoSVdoP8M59i+fP2XMoC2ytpznS1L9JY/xygjhvCJCbrMhHYKhT3/zcWEpjPeda6znzWU
c/0mcxAgULGhNX1TgszWGovJuBn8BLAViKSd2Hueznuwfbhxzz7rzFdzSltew+JUN0dBEidzvTt2
X83aI8TyIjunlB9GKGnL+S9Ep6eXz+55mlPItQrIQ3gu3vmMzf1dCX9w2r8HddZhoWhacgumwiY5
HQacZgy0ZA7IvDZF9d/salYyy72N5QQ6r+5RyTNTyHJgSTVICf9KMp0a5L6pGwcxtikhBrTkr+Nn
uou4xk1x8S1Fda3fRQ3Gb1Xq5Bi1PY35XoQ8Kb1e0qnVe7i0MHr/r5g/G6rLwB3/ZM8Wwhd10Ufu
Obu/QBqKOCvDaBowD8TfAATKMhkTviBrpghSWKr5Zel0qKluH0jU6HIeaDhQh6i/RxNvJi9kdm0V
TTOrEQ54l69+NoPCc3NUSCU5ERXVWwdfGaMRawxvW2d/WvlehIgx8Ma9Tcbgc/mJcKmiih1lb2wg
b3TAwSPxRF0fLqj9tZRZkx7+NtdK2/LofCdTM+9GygAVafLZl5+vO2zXoGjzgd9Ynv9Ui5PRTeis
9/dFE8H2oVtwBlGAAvFvMTYGut/D9NJlMkhayxGfSfWHUPZXCEa1oMCzGhTxSiBiW/T3bZUYWvCJ
b2qqCVEB2yBDq92H7/UH8iLECD+bxuLvgNkDSeKQClizG13HVZeD9K5YPrIvt/kgRmnDPJUi+SnS
a6woBcwmhmAgpyy+0Z8cETJmrbkE1UrNGR9CwcGh3pQ3EXpCHiFa7nHVAyFt07yqEoOPQ5jnn3bH
i4HwrD3OqB4+BgVKstMKIYV+7lPCh8sw3EjQotcMe8KdupHFL5NXl2s5gNoph1SoTQffjW11EyLy
6iKDsVPvTorfEVdMleLz+6j7PVVaK1indlp1LB+Z29JVAc1NWlgPtoJDZBMBN0k1reuiHI+dDAoZ
xWGy4j56IfLc3zpRy+Kdb4NW+mBQCjhj/mhItnfgAZCK8VRdOTPfdUlD94wgtJ2HUUjrlvB+ap8Z
6h5r4wxCk+Bwm4DHYb3qKpQROIsx+fqCw+Y1vppQY4HhCs1j9FX5R6BnxRFvpKTm1jvh1dEednfy
NilZo64TR4JhLxsHfG0p/2I3dV4xygVqG+hWKSIO8HCOdRbeAAQuaNUcnB244j+dd0v97t/0qTaT
EQcvRxjnWZ47ROBf1e1OnX5irvEOHiMFLvxjRPigy119OjPrwS0zJSiiGe2gFEQVgf5vXavxxFYj
GE94gy+Hx727Y2VjtDP/fNWFzQ7MTMmUOcUl99VFObf695ohFQvlY3kIQUBuH1OK8J3wN9tp2UG0
Ki/FKqX6TljgEKo0G4yAUuWSKBbhcBIy57RsjRQTiIWc7qtUyVghjoIVOhUgwqdjDHL9AGXozuql
Co/YWdDSxpwOe+uVGdqJtu4iwuAeS+GjFdfaOyAJz0aQHWaylij5jvKH0J5FukeZ2xG6WnTivHHm
s3wAvnmh0NllsAnkqGI5qy2/0vVD5tTaSfw9o269VO6OKBdy2kATrn6c0SxA7ArSavvDlw9befWM
b1JIK9KpDV9/5biSG8Qb1J10kHJbA7nYKVee4oeyUJe7jS0oM6kJL6fFUno9gjsdHc+9/s48sRw1
qFXbZGYKpGSRPRvn1/zA2QJkNkp1vhevUdhqrhu6xe7l+dzH/P4mG72B9HYfEQd3ghkHGktdh5hn
QpuFVQdOV/Au0wW5IvUwFMJuzXVsPt1wVuak4Lw5nbP55kBfkJbGSTmYWd/NoaGttqNEKDx6y9si
TXymqL+JrGHNK8pBJ/n0buRg+0M1SS+y51y8f0VZ64FsrM18O1KHOkp1dQJVUdcMsTb947r1cxRE
KpJs5SQ/668SLATcrRK1d+sLLXajeyPWLOPid8mqoBT/jDkI5V9cCgN0tNYHVXNMx2vwQgQnNd0F
Vcq91w1WFjG17nTZ5EyAWSjwxvDfPoMaPJUajTW/yV4BvvMOk+VkUJPsd1n3xNEz77vIPKXgIAaF
oCfhfwkZruZ2x5d33elJddOQlzmx4cThJ/nc87OyPogdYYBWE9341AvK6ZFYhd2GC7QIaq3G5fKn
X30mUOmQ1h2TpkcV6F7k+8yUJxMYUbqcQJVSNGP619c0DwWAwWdydTpfQY/7VQTiPH2r/1m8mOWl
snyc20nAZ0pMrGOqk4jvjUjEWDyMfezFJuI8af57f+tAINiUkQNN2h07P72urflII8tWePwo0UBw
v6no++wTL8Py8pUg2BgysFXgq6HheNO0W0MNPZp/q+/HlFIi8VhOudv54XJyd/3syDzgKfDYCsmx
93ctVCXzABuNnfg9F6sOL/3V87fvnccya690CBI9k6pv7nDMPCviyun8jOZ2A2ofbsZKe6Fv9rcy
S0d53DFVF29AbaXjmasxUQd9jRPOG0n/m/ID8FCJzGQbGBMg7c7bRZ9s8jBVBO948PdMERZSdjZu
XeFM5gtEsS8WeHnKmEbPNqk1BqtFHDwn3Dcg1O5th3eTSgZqSmJcahzmAE6KU63pGgSJ9d6qoBFQ
C7SNN3qakx0AAmOfJS+22rYAVJHWh1oBNfnwHmQTQyjs+dSnv8OoNb4iehxi48wQ25aUd62KZ8VV
uP7hFF8x181wCMzXm6/iL0WVYA9nHcSjnyMiDk6mIjqcQctS3q/WpRTbr7/Y+lL++g7bxhGYpt85
P95JurKTMgsceet35lONG+4zzs13NrzWeV5h56pxfhgRDCBKwAVUNg+Y1gHTXETzPQF7pQRZICRs
mLunvQeNuaJnAMXYHVVvblzswbCZQs9baB7MaPfZfGcCkcSoPN82zsCcpqgMRbB/clDfEy1wtFLh
KdRec+1twvaR9lM9l6gSkO8v2UBLKEc8e+wVLosz/rdvDNyosHeXS3bqTFbgz3AjciqpBVPWTwHf
PY3ugzTo84RJwztgYaf+Be4hhx3CD0cpLBnpS0S5wg3u0RNNjIg2OhnHn7urG6k926nVAhasRV+F
7PVhti18Ebx4O4u/TypBYq5NeFAcD+S7+8/2LUrxXwHKDNTgYUXchfQUX+lukn2QlytYT6FXpVe7
CeKxcXRVOMZtVe6tPNF0jHWw0RIrxGyC2Xp9k6+fb6g2IppCNWmpz9gDKZv0NP1e+HfTzt1quKEX
iEDWZMClK3cRzEfUdXTrC/6CeyCogBhIlOtyNTWI63Ups6zKPdShJrpFdO9Rcm6p31ERBTdyQSCl
PazjKkGBxOTv7u6qJVxQXXrb0hgx5jppsrfOSXouG6t3Q5EICEsbeCLW9BPAVRVAVP7LSR4DbLHW
kDeXb2vvgDJ9JRYyAWLkSTGNF2AR5AUtooGqIUOGodJXPvFfBGArJvJF+wHNovsSCr+Pc9Mh5yYY
acMnkJNbOccJ0ITppYNFsxxr7PEfqO+QFY3QE40HK17Cc4NmoXYXc/WSZtHs9FIEXbvkxsv6zo0H
RVzFMJ5Ft3hYN9bntk8WVN7IB6AQyJ+sbkgoxGmZmHZ+d0a/yKlPjFVPuDlDoUL7yozIzjEk/KmC
nXP1ulnaNjo5FWAh50cu5o1FcSl/SXaiy3rqk7AGD8+xsa6X+OMn851ou9W3d5X3CV8nwlV+c0K5
yMivh+xs/voV24Z16AWm1Qn7Oo/rl3BytSwggbrdE3cxaSC4zGyxPSoQpGscPN5P+xU1E5fTR2cO
q4a3Nif+Lmry6GUAVv7V49EhrKM22BDnMcLPkC9t1Vh83enLNPuaVzLKaQfkiFbkW45AVCKMvMJM
OlShhtVSck9eFq7cJmXE9+51Z8jtNBhJ63CihrFdlSMWbBwxwCeaaOEkkHr+hwxkPzdQCpCvGVYF
zDT7i6KnK9NNC7X0lDgl/HkHXZvl9GMJrhMX9ljCzM+urpoqw4PKYuDOkHpcpGpzPS/y6kNKGJKt
2Qf8eGNwt+H8dY5XcFIgan6x+QG0ckn70dvrD/tkzvvcepVxcsIZhdmyh2XBGJSbbaCq3phBP2qL
MzTs+tAg86AAUmNK1ApBpJTPgaLD4IqEYufJMY4Q4LpBlfx1CGJyD6HKFtr6cwNhTysoiQ/pGe+A
UPL/A3FVhFXMBAgg8m3Zq+w0k40ldZYSaiHkw1hTov//TN1PAPsHzJ5gIfhOubnsAzXTiFvpaTRG
V8kHGzgRtyMA6fxJlb0JnOxr/T1c818s9iiD4H8zDi19+9CCsIf4plGXYphCbUVIvNIHtGj+ZEFz
fWWiPhOHVxzcCrgi4zsG4FnZPcFonMxKVFpOgnxFqHnuzOIRBP2R4Gj2iq3ZlwdNlqrtZUJ9nTnH
BpraON0/2Ln68nE07aK8b3eRq/Bd0YHkrLfyoitdXL2mV2KsSbd7rBhPn1+I4Uyd8mCP/5CCIC/2
tHgZPDx2TRFduQdzir2tJEhiAm/WFmpBA3OFH7xsfIe4wGr6OkYCqoU/6I3Xzj2rA4sHHpt8Fof3
v3xro1n0ltv1VuXIQMJwe7JTwPC3Bb5mph44esSBUTMGfZ6rrTzOWvQi1DWV6hnqLNna0uDpeDD+
IlxE40PGB3uBhhDwPksbq7F13dBDB2YBeo1MHIMQcgUlnnsPXvDygRVKxtRVakjua9KFdph81cgJ
BUPk6tcFp8YRH4F4HH5Fk+M1ztidQZ5BaKIrwTdJ7nxolNrwmg34tuSR6lxcq395RNvpiGGtehPm
XDFD/KKMTKHNK1q1vKs2vOJWLV+w0xXPWHekZmBz+Thsb3iKUgQqywM/j2080/T164RPsJ1rgV8J
H3eEpEOpu3rHOL6Fr48jmbKrZfsYVhDmPafbAdbESCImOarQK9VmWz0PQrPqEqdKZTZvLLRLHIAN
7xsMX+U13dYXf7q8E7suYuVwitMHg7FjUmucbZDiEeui2a2aZHTyKuSIN+y1Dz/PflIwcfls20DE
VD1Wi1ZIfCROIfFgvF3oJKYhNpnfOysPRcHmSwLbS1naK8i+dGdkMNGhQpYLrN9dU66uvkjLR/AV
L8KUKRBqKScKE9HQSuyy31pka+T7nb8RpnC8C9ysaFTtF+gzhgITedPkv6bEnm18G96Z8l1e+tgO
XHVrtFWGYaB5/uX4xgDv7lLh+tCISofiEuU/iJAiCGvA6RUF+TDaItoEFU4T0IZTla3m+pUvJWsm
FNUQbHdbg07p8zvgmZUUA0zVDi1rm9bJKUxCMRJ8jC5+khCpdCkZE3gDSlJFbLYEhsWXW/DU7DE5
22TRoqO/BKgj8sfOr+GdI41Fb7xeMTDx46JqYVa2w2+2HPuN80fEViPh9GLxrA5DRN7Z2VSqavTH
b6YVneFHkA6X9tzIHqJ5pzztsQXFfkIGk01GTLGQTfGAPTViyLBdY6Cr++6/ASW73sMLRkCPgTxD
2jtN69Rd9+gyArEkdrthDLCGyV3244NKUJp05z9K/h9g/RBSVXQHEv3EwGVY0F36e3uyuRbcaEZl
apBM2iqMJbPCwit6ERPuvjkfTz5WmCU7VcFyymjXs6irt0fOCCVMv1aZtmFdGKMnEIOP/yHCBlHQ
7k9r1ECFMNtOv3bezu8vFoiEgjQaQM12JcRcpcwlY3pCwc13epW6DM3xLZKxWy/5Koid/sT41blW
nHXPaHHzPIDIWKlmSBL/yse+o0zgt0k76dRdr7QLqizAAz+pypqimLRQ7Ygepz27C3cjmMsX0rzd
GmJ7T00uNYm0z2JO/nMMabcMa1oYynFM3JtYf2CXirfISBM2rmOZzqM4EClCgL3N/YOzvTVbknh7
DYN52mSYCAUXh7I7L+5o+wIcQe4Y00HQPH0FTnQPpB7Bp/vnNaSyDd3ooWwncATT3gLqp2ngbuxQ
9y7SuR9aOyPSTefdhqgGW7bMldWxelkImksq1/FsNTTBWDZhr38qidb8jNoHhUpfAX/LalG6NwKY
dZ1TP7XrSJKbVHLndwsSZW41lW87J/dDSaVRU6OhszXeyIH2PWvZZsoh76zFukpG+Ak0r1cr5lRm
18NQu61Va1rYBhYuonUI40yMLZyz/X+o8pqBL6UQIgCK8S/Tl8XaZhaTi+0Tp8iCdlzm134C+ux5
lz3jM4D7bAkoOMiM+SCzD7rF+QqkknZ+OSMyfyQWSQnh9XyeHeuCEL6HjXcc2EAwRENT2mSyM3Xh
ItOhDimJjsCESI2VzU4pHjeGd3qrBenmuXRZXciZUQzMDC09MMMMqoS4hQdICyO+Si5odM5sELjp
iZo38IonqGHayQZEZlKv82xfmqGHsZP5DIJRwcUx5pYFsnjfWZbySAGqJp0aamn/IuGE6Mk4/4Df
fhLnusZEgNuSOsMtivNFYnIwzopaPq6Kn/GdmLkPn0+NiUsqD99hiUPrMqX7WHQGOuVNxdhk3AO7
8/7QN6TM7SEdAkigZwCxcs8RbQh/CO3bUUgbvtf3stAiBgmdMTbw50XsUjjn7oZXQivZY9+Zzm0j
tsxEBnIo7SXl/8LsfTTOD4XFQEi5reydlwMhmmxRRqhNE9DR1FZCfWIgjphc2OX2jsCDcRzqPXpb
DISgFn4JITSiTxnz25gzHM47reS/cw8IIAmuhp2lNl2o9MSBUq6rLqUQbLXvgfRkyzb0TbNUY9lW
vtNkFqL8PpJ7QHy9DetzpGLmEofQP9F1YxO1DY8EyfooXgdpvAhbuk4CdvhKvVadGQWM0WKeY3CD
XH05bg3Y2V0eMGe/HzHmuNIM0mtJlNAFB2z8I/WeQbl92qREu2VKwiLLktKCaVuefRWHqrZPGGp+
L83bqrlQ0H+GUTfkbsI8jBtEMOKLQ+IoeFwa3E5+tBM96E9bXx8UlzzXkRaDVEyZ8ka9chMY4PSd
UZSNh+E5KVlUu9Ox3ctpcJgCtRR18bRahbq6d4zbU/lDu1t1oVzHASMRbHKY00hCvQju2nVh19St
hZAOkbSPxkj6W4sF/o9AFbsLI5QiH7g1QkbPIm2C6m8uvNIGmW6oafrdY/gcIWNOgr9JeFp2ivKK
KUlIoOHticf6Q9ypLAeahRGEmlvTPJjtQao/4Ud++6QtUOjwBAeV82bbcQ9GQXnBW9JZ0JvfWkCf
X7RAmLsLuR6lk9DpYwEbvLmOtUYaeLH9VpyFVyUK5nehxtTN87EWRTvESsSy7gwAB9bzVoeEEEEF
gWTOYCiW6W2j5MO3B1V8PqBCuDZYxcjUB996DxDHmJymyIBXVlB20NihH4ElnSRBRnEFfBuXmNgT
Sn2qUz84s+eyob56MoG3oiFcF43T9BFkKwNHLfMbW0kg3jj89RCln6SzQk1y/OXoewi7uDWa6975
mKG8waBDMUscgX1MrmcFu8PHECPt4qFGYQ5GOGQiltkXnmtDV3sj0ZO4d2aXNvlqaxQT88RLIf9R
QIQJQayIr5PY04TOtEORxPz4jXc4ysDHorig+To2RRDWfNC2FTIY3OpGTStQHcU79Sd56p/KsdH8
hgERyWHtKJEOGKwnFsI7jQrnil1hiz06fUrwZOgXwPmp2AyIVPZWJ8pBM/MCE3otQ/X1ZzkQIchv
itWViPvra0yC7c6GlrqESWA7J/B2xLoK3jhrBlVDFV3gkvrAPF2g9312Q9xOQ9vzrJfwFBsJv2oE
Rm3rlDzmaYDG8Qsws8z1IewvdHlJFUrMG43bOwBJPxkKvQMSPYlcB1A6DmXKio35nD6AzxbUcrUP
ciLjtdjMDuTdzOUc/xdVWfkxd41vmjztzWG85krHZQanOXRjzcZFlxrFFCiVSYskyOBGGzocF9eN
63lxg9S5GBxmUKdEUz16uDEe57geUhLjajYxwKr7C6Csej3nBwVjI3GIpb4r5568RMFhfWU7SGyX
ddX86J+LbKxQOTDT0RZdrydzvTfiDrtexya4dVtirroGFCOn9PYZeZjzlt7CA2ZzHbVClkpXAAHF
RKU9zkQYfmXBppVwxbo/Y1BLt94jhubt8v7V1G3zDrDFKJoCKHwsZxMtBmExRSz+RL5rva+jBrN0
vF2t/FWls32TewHqxzoIU8LA3WLM2enyFSZbQKwlUhjN3BhihcOoGtayalvdESbWNKMQMZqgUjPH
MgDoM5IeZALgHPZ7fXw2hBvGw0fg6c66BamZYU4PmymQ7BS0EwaMUbLmfbaSRDHHwHbR4IqfCum6
p4xcAfzXnKeMPB0S6tb46P72ZUh2dIVuUr1T/sjV7ta7vFcaSJV6fWrn/AEiUlRGkyRSH/dLzj1E
Wbr+R/hSjEEOQf1r36kToD+tIi7xwhLbnf/JivI39rOTfVf1Fk1XAkJLCLtziMfC9/Rwvj+BYKte
9G2h3EIgK2CmKuSsFTNfQyBrlc0pkzUFOymnLTbkaPCTZWrICsnfKNK1am9m/9V+rBkDGKNrtXtq
ioe1BLXPP6IJYTj/l++o1axjfJecRL/S+aErj5r2Bgyt+cyNzTAjFfu3ldFCVXnqboTXJEhZBF1+
0U3FzLXgO7YKhxA5p0pd6zyt7RgbSMcEdvowA9OvLwH2PZlAZZXuaVjGa9AW4EIz6eFTaGkPlqp/
xpKRgaKqjRRBS/drjub8F0iNUl1dx+xSHT1P4Fw1rj3snvXE6PTh3TdzTTV8ZB1PrKuE3T8WmMed
YI+Ey1zwth+NbKVxtaEmzDWCg7OotgbsIAhEhpkcLcITQ3K37GEHOFOvAul6iFghHRjAB+v4JyOD
ISd+btvx/YiDdM3C99BT6ADCDd5uatkFdVCdWRwe+RK2gGVMk9SoVVswSRqDWxBK4vmT5HAWY8L6
vpUCHnngNiAzQAXoPGp69x+lMm0nmlr6vpV2HMT3qWwyqQWIa+ZofgVEzmgd6+8Hv9xaGs5O28W+
pkkuURYcnnjsdWi4SAO8GIq7Iq3cSumbPr3apDIM1mHf7scrVbnKfLuj3Zsme1cwdMB8knAKM93I
+psLTTg38scoY0aAAXfgBeedkQA1C+YuaeGufJ4fzJlgoKjy3pcrfqRISisEc0sqEl1zgRqtQMaF
k0rDcFBzyizl8bD1wrdMJxgZt9kDd5nY9/IAGE2keoC5to8C+o69xeJhQXd/ADx8Noj/lFeN0e5Q
Ici+dKeLT7Es7mV23a2wLuzGDwT+VCSiCHnfMSVoZw0dlDuE4+l1GQoBljFxst4bH+IY0kPHThLb
0YFeura4Ja4Hvbbx2C0y8BaGn1lS+DziL8/76zx7KzVy51M3/1R/W408wLU+7yimi2jvhP5chkFB
tcvXzMrsHiTR2Am+Y6VG8+062sv0206qntj/LKp6HohpI0yOwbAa4bgIifP5YHPF/uOrJGur+RdP
KgWQm32jL1iB4dCx5KQIDgzxDYMVNq3REyPZwAyESBkXHO5X36cQcOg5tFD/8N1QsuXm7975MD4M
wIzJfe6vinr0AsGNo9mjauPdcDK1poZ7I9KJZrWpKRr+9G4x8C5y6gi9pMEBMz4yq7Ek3zLGqahE
NCcqCYNwq7SHWCqI9hn8hCAzaRowfJ9P66cdtKlbpiFn+7R4fWOGX1nvY5BRT2UA2xJmPokmxExq
/2X6viH5XovV8lzZbVdtaDg0+Q+ut7Qd4IotDx7g0sDR5ywbL+CswMrZjZfukP/GdRWo9HU05K88
B4mOnGQAets1/YmTX/4Sx02kmQm2gEM977PkGHFnBzHSvpzeg6uc+N/436efstVBmBvb/cvkhszn
Ov1YfenITkEEzQbLgqSmbkSkCbOJR+3RlA3n4qsiMkOwVomcJhSkV8BGAZEjkzDkzu2FFVytYaTd
b74KBn50dUSNw+8bdtvuqdBemcW2StGllmH+d5LyFl/kTzJ8bgT3qWimZOHZvqGIj3EEODf1FcFA
BhGNrfm/VD3yY+W8vaFMAOT0V5ctGwQfX0k0G/3/25qes5UZaz9Yh9VNVW98Rheg37cYtsA4Ejh6
DIpR3hLhhg9ieWJVtd5Jz3mvrAjtPM8wBv3g00z7qQCCNAc8f0YRcVGB5w6f2GZNfSVPxDeiQUmu
E18T1FqxgOISvMmjCUcMz/jsTdpl0YjrjfdrEpJp7RyFc1WR+GNJk6n6Qww15mi7kT0k39Unf5LE
0f/ietwKPX81sVyk4LjohpaE5OTRsu/n9ed/AAjHVfyoEBCRhWT/N8Yq8WY/2F5W4+gKh5+1kV3V
ErYkOR7iQZfHoO4GMk5AXPi6Njn1C0Ssc56Edzu9z4gtThRA978jWt2sjxEWxpDR+3R2NBOzUxlX
8b6+KdZJR0PLqRM0KuHsZt8ep5Pqx4GoJiUDFwnSZClpiy32SfdWQqdnmwf6QBcstutp7r5yFyEZ
oiI23LNI+TxHSuClWbGFqhV9W+t85Mu0ZC2X6fkZdiSqtieQvLmDmRs4RcEdeJuPGpWXn0+RkXnw
BgEDW2RIOORILjxu6Lx0ehCH6ntnrP7O85vVRvmmGuV0/tqFaepikOCA6H0h7yz/WLpu4MTBRJOT
mn5EC3lWrdeI/WvqpAYw4gUFqO0uXoYN3SkmzUihaGS/t5j2gBT7Q3seeEpDSjjMe8bzcUMLk1BU
IPDBodHoILcTKQ/qNmzPg1NjKq4vfb7BiB7WTfZ5+hAd8ObgbVorrd2vjnsQSF703kgUuUFclt3n
qCfybc3Fg2FI3CPLPieCGT2opcgYPADDUXk6jug84+sFTfOAkD/HflxZgJvHkT0xHKyPfDSTVzvS
FfjX/JNhEJFWq/vkQ038cViXW7XZsX2gzjHYmYbR78ea9tLKR2FJ+LAQjEbbaKAWTa0Y2BH6rDZI
8FKvMKf6IvLOmhC2n2cTZ/wGN0K4IHtN3W3aUoAdLgI++uVr2HaPDRCk0JVlBAsPkuUlnKvBLQfd
VyVG5HSf13y/RqtPvMlhXNtY6QJScX23FoVyG17LhX87znYumVTYEIdcGqyTuXMxNkE0UKBn6Zxg
wsmsEezAG4YXoSiXVltU8s4iBkx9K07SAedplwKQeX/pDQdUJec7I7V7/EDLNvFHSOd4L1O+H63T
Tfhp9JtGPzJ8WZyIinRQhEYnO3a/sInGHP3wSEKXQUI6vFc8CaCPDwaMLCCPd0UIonr9lMx9sgCN
Dew2mXBkYOSdmOPjoGfsr6xFF8QX8ZmSPOFNy9nbvOODD+weXdJIToWI8czna6vL9ck7Keft4inY
6Yj06KFjNOnOSpI5oShEiiCzgeOkzBZRB7t1v6Ry71Zz4zLcfHw/Dt14jqy5uu3FvKgiM4+l99Om
QBsRjOX6EyyxOIoA4EUYZO4zte7DS5RiUat++vlcPaTDA9IX1f6EKTfU/bLbwU8nchCygGF/gR0C
POkbx7rH30Nkq2FF0A7W8J4L5i9XNKWflUXjAhzQtEE9B/quVwcTsVPp4GZfJyZOA14NUKbspFbf
0C6sVhqPqhTL2gxoRrpi9Bic95+T6ebRd3iqSEZ3QIqHBAjNwDOH7uzmC7FyTU52ZdUBR4k74WHF
Y1OTXGTIeRQilevFC6v/m65+PzI/YljmQssz+NMCN/hpXj6m7z7N5juUHgLkBqojwdPPwTZaK/Wh
ZD427p99DELI5J0/bSEuBw5okXI5A5jiy7hXcsvli+s2y1pgYECfoxhAOezZXOIFVfimErMWENOd
I+I1bzJUIYEl/SwOOgJWoMncY0hqnFnOozUmUcrV/lBBtU8KzybXeFnlr1oppEclReiNoqJNGsY5
osjGnCzHsENPxScNPOgWEJhg97coDTIsKY21CVYd293cUe7z1cShh/dOMdMHUA8hpo2GRNhwnLZE
9Esoon4KYrwgfkkeM7I3fNrdvfwsPFLHfa/umn66ZVr8+NWp+0uN1vNM7MhHDS3t+rBu50Frewor
apqTt5yQduhqrJ2Xdfi2iGEguEuA5hubqYZcPWS6MECQyKtWzihqJlFKHCy8+BZHook2v2kw3l13
FdFQEAr82jv5JwOYxAxMwe6L2L5VqBxOuwuI/CE1WTr6A31eH0MHdUapO2lGfeJTWUjYecPaAKDY
+gQrBCsau69bXqIbeaqE3c/9Gtq2TGEor3gxVgqpHVCIjCICeeAd1JOt5wltLhYS4WSeBjIJqL2h
BE3ysWnexdCgkWcK0Y97aolEGTdBJnbp9WBG3og8oSTshBTOK/Bj1EHm/1Py+XaCgFBr9ONkwE89
SmWf0kEzUXhrVqQh0g6cfW4Kypg+IQAEwwG9deodMX/2o0nGMbxLgSbWzqnKaZYeOBGz4vcLDZDy
zcjb79wU+Ri8DUAHLzClnB6kMp2fgl/sk2SqQNULzi4kKXRnMHi9u+SKAE6ETcMEPLryOl5zFfq0
biC/5o/T2/+BGb0Cd+QYEicRYiwkX2KuV8u/f0WcabXKV1JDjfoobhntDqKbNNN+P3ABuqxaR7S1
iXKwSw7AOU9FySJKbniCI6jwisw/dbqXUb0lE6ZQT3Phz9aWtD9D984fAJTl27JEy3P7i32aPemI
/GeGjdpov8Jyx3763SajNJWch+krAgc9kxvKk2jaMnGEwJ9XP/QiBnnqwjh4BJLqrxkt3KELChyR
OwZMxakHcTwtUBt1O/3zTpjyw0Q/b/eHc0p9t2eFVRnTZSwATPbYOUMtAEJ0tC7NID4MvSaLsHzZ
n7CYBELeljGmMu1+l1fAwUs4hcqNcYLo+AOhC41w78gtUFFz0ttfCWXHc/gd4aw+q17uObhAHn6a
dN1G6PiCYgzonXwcy2TGeQa7XAf4PV+IvXkAaJ2VfWNnQMJQcfGsrHVpITzvgTZy6dPPM/qG4ci9
xwUVjgAHAiS4QCfvfkdAf6Q2IhkZCH5ks1POMLJi3FkPqUJEjd+cTl7t8yDcfRwXUnhU6lo7OKho
kFs10UXll6wXRZt/diYKe3Q/AtQDuXC9S3+k33rIGOCtABv+pP85LTpD3kKbIcdmUicTo/W0kr5p
TrZv0wSPqiEuwQDH/hm0ESVeoKAt3vZFrL5sneUwMyRWu1W3iCp/SJr9fcMLPse+UF8iM99Wjqgi
Fj+Vgfd4j3eYq+Aae01jnFHCM/twns5aanhk5rhqfxa0BWogNfeDkupMmwQ3FDf3DKO0/GgapeV8
tEuAo9WqCrtosLA4s1XoLJk2Nma9YGCz2RKj8XJXKdj54hdhvgsTsnh03IC5GAqB250yf9qRRut2
IX/K8AJG5QIBYNp17NfhhRgnN0sydV3EK1db3wCkLZXq7uKzA4+793fijjitvcFvlTQa4xbVqYwW
C4kd853TdJO4oaBDuGp2gpJ11ofyUeDiocKGiRJQDMf9mQL7xIxpm6ORLmLt4eBo5IBiZjob3Cvd
dFXTYNgtbg1DuYXSIvh0LidPnB52ytrl384vqEWD2+99g/iW+mke5Z+ZDVity6RU1YpZTql4IrJA
9vyOhW1ThwQx86tl0IvG0kVz76b7to0/CBJmFFqJszOrLzpFTZdo1JEKx8QYyuZzaT5gZ9GtkxSH
OWVH+ZjrlJ3oWD+TPT3h3voSz29gqyNguP2y+8mqFAVLLSGPlXQL5wuHbqiZ9EP+UYu7CSTgNoak
sg/fRQKWkS8tXhx7+91gdz7fHNymzztPZHNEzUBV7px16F5+MqIJFrzkwzlOPGHO/5gEJyN7Vmd9
FjT676snZTkza4XOvOXmwNc7xD06igXHNgVxR+uySHV/ANzjc6+dtAUrJFjJzyHmXsm7C85ARQIO
1iWiqLdYk6+WUENxNNHzDfo+91N+xVEMNGbECH2Xdg9vRavqey686GYqtiIllaut3AbqTrVcXVRR
5T17cD015Vip9jEYCrwhalIfzli2sEIeoZ4yTNltJg68q93pWjFwWWD6yerZK53qJM7YGTGvVJ05
efE8O4WHB3CGNV8fBIeauVW3hVwVZLe3vDygkZBDcxMt0dTtUWxlI9k5SjrnuRP/6BeRSrEq4kIE
3l2ETxBaVWIjPXgRsw3E5s2g7tOhzdz123C0L/u+z7OC5uL0/dMeU0zuyeTnNXtKim6KbntG+fln
a0QLDWW0RXxyiC2hq0EyXi4QZg2v4CQ/EW6p4fS+eHkN0hJb0HNB0o/aQ9+MpawkF4MzPmM2UEjZ
nk+9y+EID3b7SQm99CbjcKXdSzrzS9zIhY/M1VF/7qBpe1591tqRgPp3j1dR8Y/BuhWfC/3UlrPI
liSyC6olSwJ3pUWr+nVdkVfmOXMclNFl6VMvmN+GsyCwBWTXRQUi4YCqjsxZR8o6Y7BDaupBT/zp
ge285qQWtx7hzS4kKUhpfLR8fdnHuGvo4oBnJ7Ln0DehTkNMFXbA/CO1ZE+C3dfLchZOLsJeJN8+
CY8z6SxWITw0C0S646C4+ALA9nxbu02KzP2neUp4DKFIyKD83B0m6qMDPLJa8Va1WuZd2A5wrbNY
UJYSQy+WaiMJ6CLM83/okW812JCh/u8BX1kySELINPgpe+c1wAte4a+McQC1ISOw+HXZkaLuntCd
/7VOYw0jr543jVqoK8MWk4H18v98JJFosuyuVklfLZ5q1FkR6Sc6UQcRMxC1eJWeAWPwCmoXxkSp
zdaFfWaln/X6AVBNlC2Cgzm/yAsQnGRDV7njgqD98gmgnbBAnMyh5Fjr/d9dJGVIl/jw0xhrGiJx
I60A224mXENZYowTf2cNEwo8+dep6GfXu4K9aofCsom8cPZyl4NMVRph9okV0dFhuUEpxgKtOQsf
lo+AwnF9L4ZfbLQmNq5pYU8LH0pc1OoUVILR8KIPFgkHcK7VvUH4YtTAEyA5Z3UB4Hx/urEzTTAs
FH7aQ9DLvBi35CNN60bMC89LZJDgrEi+UrErueArc8k5ZoQw4rG4khEMQW5KjuKDWKrZvej6gE7g
7TUdHPd3J+ERLKWs8O464NNABG0vDaHX+Z22SQD9YMalBPYvK9Av3jqVAhZAw3K+O99h+zvpLe2U
pTVJRZaNmKvkM9+xP5FYp0ZIG9ftX7P49wkMr1OancLZ7LbCpUNHnkOEJMigEZ8vVr080F+wkCCd
ynPHsA6PfbUZp+hbnke0BAcM1Jo3uQBcuJLPgEJBnn36rxBodN05VLVu3h/yyLkMQjMWBAJTzHyz
jGsmcoArSzYqxOSsMUVRUXryNH2lJe9DAUdUR70VxyZLQK0kalNXENYFg4kRlWft0UKvJrvrQEfg
RDOXkS6X56+s+d3l3TCvmEwzzHolTDtI7Ihl2SY9Smb1jyDv38is8fYlS4nkqYIEzUI/ZSb8QVeE
izIYyKLrvXIc5uXnZpMSRXkhOjW9HIU1Xo3TXzEjv3boTcAY+950b182b777ZBxWmS8nOthqeAoQ
4iBz0MNGQXE6PaIJ6T9kH/QL7dv5iSEmQw4pbwmqrAZ6RPG4dUtKCM/jNyDzC6r1C4qOfUgGVF6V
MzQw5w9ZxPt/SV84F860rkjjihENhiIjAOPZZqMAXMvvKr1X1dfb7BH8gyS9qKB26UEywPNX31Fu
nJ595omEWXee2OXdZFFS3L35SU0gr5ml3Lv6EjBjOThTfG0m8sy7WLlBAT3c25cmCqSy6UeTHd6G
ChLJZZL+L5Fl1wncVhuE2fZvHYli3/7lKpUmmXIVTORleFOUGEJXIrpl1AXmNvsFJi6g/Qoqupcr
Av4ijdeJcmabMw9rVJlRJJzDXGHmr6b+HrtK8NgL1v4um5eOCNs/RtMhu0EcSxRAaUG9IRmn68tt
Z4Q6iIoB1oeGBxTRnEjMQSrqcO3YmECLWq+zSK5FdnFOGy18kS54RGvPcNYoteVU3QUvKtTdV4Fi
WsGKze7Iwh14lgJg6p2RHvCLaUlT29Drs8eYrQGJ9nKRBOcKzBYxHMWCWEn453zZF0udKbNJ5Mwb
ro00ydZnKiFCKqSWEO0/ZAkn/CAVO8lW5/hvJFBgEZvPdeXCtrQXUZSWTEALUP1p9oLSeDx+MeZd
gUYqV8LovOvjDEyaB6cCOZbpCD3ElyPC700ZA3YtecJ7wFLAePzeJ3OmmfVv3sWkzZFUyog3I9HI
c42ut5MH0HRrzoc1/KpYu9sfw09WFyPK44gmVvx9vyqPm0CTUKNKHYbr7+a8uxAkP0+7gWUicZC9
bgjV2zuFY/Xi4tNGzjq1X2VOpFO0WOqHMQhBe7jBEewvx2dq7Leg12g9qcMFTt4ly4A+V1bgEnFj
95JDxX07k2OWiwc1QxtW9h01fhnmbl1nDF6TH+OLZfmzenrDgHh+Lqec56o/JK2FvNHLuw9Sq3bN
vJ45Fli1clizZRxlZpKup9XIazNncNawUjAdlBeVmGwgeSSlkXoPBjwmr7MW8B3vvx62GXM7Ljxo
RdpKNgW23JBkSL9+pfhHHzUQxi+i2K5jryntKbSvokf+nCmoz5cUHUa6EnOcnbdr85rdhsiKK8K+
AlhorQ8TB7sQGgloBy1muXCBts/DdphI+Jsg+cMco+p4KH6Q1YI9FM9o7gPBKWNjnjd50XmqFAGo
OKt3cohtcEKHRBvzpTS/h/QRIRM9kgq3y+HmA58k/vuHJwA9yglGARPH0CAdthU7B/7V8+NkhHME
H9QAeWECy9fRsgU52tLU/r9DPJdUnpZCsHSLBHLc0bZfy/P+DVP31uz6C0g5pIzBocl0yTn8e4tI
dtj9UmMA+Td4tqY7en/i5F8wXvhKoDHf2ZujkkzvmM45v1GrrOUtCu3RfIZGoI/96yjCXJe7vNPa
Bbp2sGyVitEgEL3vyIELAiRUOcnV/3AoGxcqyVrt75bWMVYvqWbmGcJLQevnUebH7QDhTSUMczXm
agwsbVOaCPXBWO8S42c9E5KuKZYzup9SBQ4NO8qwC13lpm64PnjaJ43D2RT5fKEuzbPIUz8qsKCX
gOVN03JP31wTQCk2+N/E3g1lTnLky2bOFU3l8tr0xjvLIcvfbXHCccg5A4tbM9gHnIzeg5C1n1IA
wtmvz+csY2kTE8UwhBxHL1N+hdX9VCmnEBQrCvAxWCOP62229AJfLKNvLes7ueQiNMcJcIG+tyie
zpDkWbsdCNRlDi9/IzIM7TEt/7Gdr4vljy8TV0wPOfxxQX3UZQAsAHEoWYFt18WudDaJs6PWEecF
Sk+YgNfl1StITYYq7ItUy5la2coAY/dO51cW3h3IwQcN5H0I2M6Q3+tkcjoE2h/Y5dwAKaALHsS3
RQ+Z8IPO6Y6RH3I29PTc4UucNcdJITz2Dv34391KLWBVbLPeQDAvElCcuqvTGN2BK0M/f1O23ADw
FYzLrJbedkgXDncBjJs4/X25kGqkjp/jUMaHSE8EkOOiMdjVzcVVWh6uVXoM7uYycPuVvBx43T2k
x+K25LzGEng+i9Qb+s10sOqxaLLd3iyOwDPb1vsZkkQZG00X4A/KRItHOUw65i/9WjXiTZ1pJbUJ
izCjoSg3dZj9CI2KqpYPN6EaBCltAcfcl5Pt0iW4FrKyLRvpGEhXgQX+jzrOQAOTb/ADZFk/t9Dk
vmkFSIB9Hm7g3evEPJOdzWxdWmSTGCBvHBnhaulVNMWX7bymWmbR3wUOy48a3m9FOvmtXbG9LD2Q
lZKjrOwL4ATjMvjy6162kbDV5wi3xDxwamKVrHW7lSmgIF2iYX7K2TZhQTaie2Nf3WGX8oHzWm7x
/XjsEbFSVsl2Ef7qoZHBOdXJKeAs/kjrFVmWXtYU6wVQN958DKiLZfvBNz9/mP+uZeNRljD9PVmm
pXQNb+WNJs0B9iK/W9aw5JYbiCKdZ1q+W0UFz2VUD8u+bg5l3PfXAu4uMr96FsJnH1ToEERJERWe
bN97aWyIqmzUq3XwcnPpI1dTgcyeI1RI6WjgnUwrRjiJ70BzPVyd/5XtBf30vH1Ho8AqlcqQknbj
QrYyuPxAwbMKdgoKazOi/qu4ps4MWB9rT2QfX3KB8GLrftPHFFqhHq0/RmdNpNFXByCNxgTCsnl6
+poGrr3wC+SpEUuFJ8kSgwYOQhUpt5H3x2/aJI9PbG9ey12UTxvbw05QeWCLeZLyPdEGebxk5aCh
x+wVKzBYxjCytPIiCmxgdaYJY2SvQAn1eIXGAeJ6IFM4Fi5+YXV0CR3C2BZakVrG6pcSGoDUNXS5
oziaSa26E2uhDqlqMtJBV0RxtFdKaxH1IVaOO57skakzxeWpuIehpen+nxobnmtryVgG+RGG+Mrv
xpqBBPYFwiLOKt94HsYOeW6u32PEcCcduhvmD8wzMR3O+xTmN12zur/HwJFhRKFW2nvTv7v5kwA4
ZzXzQTijnfTNaQMgWx0e5RXO0Kz2kqWQ+Yt8HOeSberfrW6r11brf66ycdLEv5hjlq0axrdhV5pC
5b7rnf9kjpqA1N3LnC76rc9+7v38H5I13CUHLdxfxNC9tOv6QqkmS+VYgNTF5ToEXimBoiWM7q+d
pT7sWPGS267YgNL6tRJYDG9Dvx9p2IU+ZCxp/pMuGHShrcEOQLN/GsaKen7kDngvSO+hPbFjmsj+
CjCDjv8KIW7JujXT6tMiRySxZv1E6u0I3wMG1O8dD26tYnZhC4I25KdERzRG7M1Z5HCNyLPpY6uV
sKafxt+y1Nj3ClDfoLdFjr5tBQ3jznZJH7O7c5wo0ID9c33qubIKRg2bgc7rCwUAXL/nsEkb1b3E
kWlEpZLqrHeIR+11iYt+quEOxMwW9AMAPYuBZCUhVYwm1haJgYMZ7XzIxf62IGS72Fj6pEg0A2M/
TrAaAGlUfJ2t9j0CxYVenWoJrs2l8Lxaiv+JaHEFMcyQUv+/4Bixt7Hcs821MXIkTfTyIpXiWHUH
Qb2ZzH8hV97vXFjXsi+e5CUOK0iSAULdOqc7TWhkP2Jpuvzz7/P1rffI5NsYvNHDAPMnz93xux31
2YVJ/9XluLGp7xJwV5TmA/ZZAvs5ac80oyjk6YRcclcH6AuKzSL/zuj0uj223v6SwxEpmr6SawAW
EnY0VfLZCa22/W9HMVsCIh0hXr8GpErW9uS0MsoRb528pJJxObJYLGOiX3uNblQXJ6MgNJZlq2eF
zPIHWvBLBjQDyFEQGl61agIDBsWTmNbL9sAEaU8E62rQhCr0MJTPZrNkymhm22IbO7YoYSI7IQON
MEINb8edw9KTasYdSq38SKe0YioOW3Y2jvqV0MSLhPUerRbxj9cqCrlHbEqYPnwlFv83BwcMcVFc
2QX/6z6Sg7BTOgG3i8lQhoJ79Cx7RmbK5hRqNmS1flwOXP5czdXZ41v6F1WHuPvIcPeXoQmAVYwL
qSfcOyMI8e2JgM9Z4TTZZONBoQCdzSIFIlPcggRTvsSLHJWd/baS1QA6EXQSvt0Tv7xncJ1DB4hM
Q8ARFJolQHc1xM9x0t1GYdg9aEwqqO+SzAF3wzOcQwLVGhNmeIsVC6JZl3XgPy9hc5Xr77m1qba3
xSyiQXJcbbEP2daTZUgKjTVEBuSeXhgg8+ltwDG/IumjTGbv70A+j80MDjoP65yogylGFj1yNRo4
JuZ5KVXbIKpzTI7hDduFD2RFa75WOFBEFZvYXEdf0uMR1gDfXVAhzDHetfBGahwdDpSbCyMLS/fZ
7aDmEpnyH3qOZLQAjPcM7d/B3VUYohOFl+U7p1yk09PIYOl6dTBImaD3G6XdEGzxeFsfuwT/NKeZ
C3dv8LTa0EGfwM+gFJrQZp2RLWlpuYNvMhT0QDpXBFV7bSpcpsbYMMd3tkR72DeMUSC/dmyzZwG5
c7KMJj7SWNmfPyknFOS0GSTgIusFfmGidvKYkxrNoCZahQBjROJkcySseUrcdz3ejTkeUPaOdpUJ
zwnfygHi4D5rAYqCxGUnYryYnVxVWXJUwedy5bu59rDpX/hoSwqbs6Kdv5YQwinKk0b1ydV0VMfB
Rs6+DwrSk6AlKjPNVcyXriaEwrcnWyVTI+f04HmA6sRFHulNepViTe+YOH4RrOjKCW3LuWfEqt6h
MTYMceImgZ/V9mwgkX0BXKDAGrpUVZhAnCDTvAKVGqQOu4mg22zRJPXGddf6ZR6rY69slZfz4+z9
EUhHSZHZS6PQrBCPjRaITToQfjc4+pQIjiMGjn4yBMBGii9F/dWXx7ECQeUH2XvT1vZ4PnU3fAod
zYGYuBbzkoijKYjkxm5wGsMH74MJkse4G/qetNKLbdUkO/wCYE+uKydPaa6f9RqKO3TxrFCcG0Tr
zkGJJsrEV0LdWJTay/yvV29vpgJ+QbNkMizj4XxPLkSF0j3xEvSPKRlwdqAG6x9himFTVmRbdcAL
eOCam/5yN3SM98uw8nysNeCdOAj5v1Dxbdm4DWPL+Q6yL122ND4UgNocBvJ9CKR5/gbzr8E7OHLK
ZUJGdlZEUmOWEDFfAbRboRno+lZAkPlMGFhnL0LRxMytPTQO2omFS8Fhz9xfafgol+dr52KLacLh
TtC9imeu8xAlHU1A+nTGznfL3gFe3s81LCPKdho69P3b9WdSqVw7arVvYgPZ9LT6WUbWCSKuIy4Y
3ib+/7a3gAC5d8StIH82IdeDcMcIW8o2HrXJn1C3KDwAdq1qh47r9ptMJzxxu3DP1C3sJWlCOvQq
uncxmsc9iiVg9q/7PNgx9M086z6vYllLVctFXuaXAuq0S1L10tvEtgaZjq8kbjhA5nEVwYfp9J1+
CR2dNWMn2fodGkYEpjCj8W4am2BLA8THYWFBrgWQGAYHvOZCqE9bK5MISjkvIVusoKgBY8fCEalb
motgOsydhY60MxXrpqbPFvI5VQQunMYz8S6L0n9UeTWu4nc/uGLjqUShO/gu3QRtKqRgh8C2GxlO
cHHom1dBy7DX/7B2m00w/NCx8ewZwLcTFD144osVO955qQG1ud6RPNHKFofcg3BpGQzCNQ85WjQL
YVL1ZaHhVLbnEZO+p/6isEznfOxAB4oHgDnL2iaYraTo6kTese3KyTsdwLu9XVFxCfMatW/KQu/B
QP2mB3FnQa5Cq6N//ec0RfVhUosBVa/+HQgArfE/kQAVck8lHt8ztneu0EcyOXa1wSpq3NIPR9DD
f9/vCB7XRNrT9JZgcJx5/2Inc2zaZhkMXLvse/B4bS72izlWBXeKONukD6RNRUurSBLUQPH8wMxG
C+d4msYkDZx7OnLxuAackHgo2xV4RyLl7iQoufvWXLNTmiOnhN4KHaeomSwMYuqOsTCIG4PN488/
N/oKAwnepbKqBWtiNFiLlELitbpkXPRyyhSjaJy/k1bzgWAE0Avwfb2UnIN9XQg8jBRtuGGvPn9E
KfTt/P3IylKcavYlktvCjOK7163fr04MocorpNimXH/S7q7sOGFja1ExIKqteBy0S+yKpSGquqV5
eTe9pH+S6X4BvHIWpUfrc8Obac+I3KlBB9QJHgXSUJAopVwyFHH15tcErW0pKJfK3fVuXwBRh/yf
n+NDZtwpe5EOcFqPVJf7hQ6Q2kMt31PDdqivVOucEIyRebtOw3+u0GH5D/E6gIjTWBU96q7cl53X
TJPoQG9//DzuJlEUkXfVeXXD43yJ9ySoZ2wTsL+o/qp/o3+LPAe0ma98rNSCyhlKQcpWLA2T1n78
zhX48CvUYzrxE7qLMd5cQcICPigl/08Oz/gSA2us3VCPPR09fGwdDUQck4lP6YW6b+JYUM5iV4Tu
GVpO7MN83W5hW1+rBSthFjWgFS2uqlQsFAdv7GxXQ6uxczhO4dJFHWBHR9cTpX0lyUVOE7yMb+eH
KGYKzF7ZP2chgbu90n4mkBPtKfNQukX3n7b9rUEoVp0OYKG2wHFgotYkOlGDb/RYbFZBbemCxfyP
Jhofqv700FfAUDCmjZ2+sTJTc4iayrTDkXUY1SyPL7Yf99WszRdF3XJRJLqx/YKWJGr4mBBX1Aba
L/QGVatD9kCp2LzwLBU1WC0HI23TuGeWtpzzqBFYted2dGgPFvfYziOOeaV7b3znhj74yZvMC5TH
9IUwm90wPsqEOBhSyVOc6xq6E5LALHPjd7hQFnjFg4K5DRpnct2al6rZS+4TkUx9d/HkwJ7ITyh/
vjmlz4Zp6Jf3FUC9MDyerv4KVzxmEqmhN9UfPhKzulnVzc8v4tZIn98cHruKB+D49v433ilGc5yn
qU01FjlIut8NxSuP6QQJkS8dwa0HfGU0rymPh9od2IBoRnSQKGuIPyhbLwjH4knJSJ5ljSRN/KW4
6S5jqahzOz3YfjCbeb0IsZZ36WD4GFmPNt1XVeSw5Fzywt/84FBFXCTKAmVV9pw0RoIM4qwFwqeC
b7kVTePLYO506Mcryl9XG23wIcSztscEdgZGerMCVDEqVSzQYj1Na3ssJ4mnty5jGn/2vhfn2SD3
9/wc4K55AE+FpBw0nDOyjMhgRhFgPvpEua+XCx/Bd/KAokxeHpBfbDdWaWoHdtfljBePU4F60MSy
zRfFdByKx8o8ehTcp+grTLJfuqjIbHxovF30+clOV2gTTf1pwHbrfRyLIcVL8ZcPHHG9epbzUAmu
EDEzEDbchzpMse/K9LSdHk0kZ45VukyhBntstn4WBg4Oa49RWk2LY47YoIUj6g1MWdFAgtQpePJs
h4Q+/Yglqb1/FMsSNz1cQRqTQfj4jKQnvekeKJ1A4QSwpFjgs6o1aKcSfEjkqx6w7abv6+N/eQtH
f/lnxAfmAcf/G3tbRETmpGxLWvqLiKBuLgUo5k1jTEMGo7O+78jGDCYJO+C59ZhO8IEBxq+14p9r
tzixSz66zG8SuHcL/CWsjMzRKppibPCsVMfj3NT86/aQPANjh7YujlTvUHWl7bJphUHzfem4W00l
tD3qBjulgS7k1dsm3iPNFtkN0/TpqdjU8GiTso7AwyZR9xNPbaSEGDnVGpVIA3Ls4fQQ6PSp0P2H
k1j3QkHhHUMMa4sBJrH8GSF/rGkuecikXUfP7IhjKVY0pDswpDc1WNe33UPP7iuMeN0+EPt+YmMT
rLSA9hClC+sR2HgemqvjK6h01vHq8BqtkQ8s01eV7ZTvKaTvyDV3Zilk68+GHcCnLJfxZWg7nLFj
DG1lsFvRacmtDCwe5yiLORdSmfXtlaEkULIzlV0y6xhdJbuYKxUvG9UZPFaXp2UKghfVuyUwcykd
yW+VvXX8yfCLUCIR1S8QHEqlGWIQumilTZgtcj87LDqnqPmeBhCWrgis2PRyM5dc8F6Gm2hB3b6e
/oQA0X2MWoX7zg7d/VxN3I7ju4KT5tmIQhW/NJFsV/twC3lcvWQMNU/UiQJohn9WIiNTk1EgiwlH
0oGkNNSbKycCc34DVCneFk9oxNghqhGeGCPlgm0/pHb5Ih3OQzC3zv2EzZuWtLa/UPfl1AA8OH9F
9QX23XJ+s3XhQmaBCHI0rSIq8+BBNTkyc7lbake9Cx6C+KK2ayVuLPNPcNPKqHHapLwtCb09cyze
VzrM/TYGOJJOxRczC2Q5wCs8fqW4++iZYBzLxSN+jc9P1GCrbnW1PJXKQDh9ZlBHyxIcIwPB2B0b
aDNIOZBKjUoyEbGuxYcrKYG6AAwNL07raxCUqM6S51XzMt2S3lWx3f3CT9B9AaC+shx4D4Ls0yaI
0RN/mOh+9gEqFaJ9Nq0eo8uXKJj/u6Vttx5Tp+WGdVfz5xGLDIubM7cfxuCikUB8t5HJjB3I8uZ9
8tc4UGo0VCv0XyJLUzBWLFCi6PGc9IlaRXwaMjVGshUgvuiy02uHvwuedzWw2huzsHdFfLBVnlvQ
PAVwkxXfoc/lDABtz5qlxohDMVFoGXZQEzT0t7fQQRwcxAzXOpIehrC2kBYO8+aXuU7taMjP20XQ
1ANQoq0FSZaCNAcsSBXJPcZrnyRt7dBHlgsoXXV1CdnQR73VtnP1vw9Xq4rRLj/SvZptoMS5WLdI
tL2BIu+T92zcLZxqmRw3gpWMXDVfEkmdEWBc/Z/4TwP51QVFvheuZ1tbQx8vmynfkxt/p1BDzNZS
lOxCG1pbfyrQw0hbHx4swhWcy0OGw/I/31ZPnU4qZj1Kxc9igJzoC8P0fQ3qsBrK99ofN8v3TbcE
H+nlXBXnRYk6Ap8GeAFODdsFx36hT1QlkpvD8QRbhKIMUV7k8Bl4H2/kt+aNf1/bSrQOU81VCt2G
MH7svdQGMDKZ0jbB0LjMc4SpVu7WrK8vVvCBlWU0Vg2M797vyxdu8DruDsq4W56GoNFplRjBstuX
kEv2n6xaQC0p+HToEtMJc+1NRNepZA7klKrV3HXnFbzHQpILaDt2m4AtoPzg4tTo3NX7ka3vE7iQ
1SdwMDfFigpXn4DOGgZQQQq7UhaI0EyiQ8IV63tVHHDlTmM6tGXIyMpYrDyc5ub6ZweLJ0GecvAh
H5hoXs7A98NGJn69+UFpbnSQhlALM31Rz6aA2NyDWOfL8r4aRM8Ydw+rGmfJGucYVqRZCWPpDuQN
oCxFXhFjkcapKfzrhZtSr22+XMeUFo7cU0GjC8lsEuaDhv1BZTSnMuuk0k3vLB34eEx/+6FhZrNm
r2bNRRoEYvuMOt0gdLg3111MKAs8n/FNsSXTS+biDG+C3nWokQllWCaD7cJX22emi+I7q7JKyqJl
egXViptcH8GN8XJVAJfBmq0Wjw18KSjcK9gU+jwe8YJz5OYsPi1w8ogE2alB6QWIQlCD3X06eLNr
6za580bvdO9W4ubu+rB0nnA8xAxmna+Mw8WUL4QNUxdhjK63UqvcP2Zd4w3XQDoLega4iJmzHe6k
dXlGvxe/4eOB/DQqcOOWBjO4jXWmROv9HR5KkA2e4gDAFPUiNWPMUV2lPV9V18rYo64ukEm719Ya
wrt4GsXp1cRaeAHROoMVX2RY6pnuS55WsCQMOYkkP3MSFfjsgdaMXe9APsjISL2nOcKUiTa7dxYP
H5obUkwJWpwiQYKAAkp9iaWRJyYvR6r3PxpEWObhMMUaXx/uhf/37TVWkNwAuHcV8PuKxby3ePCc
xEeVPleU8dFaGdoMMBxKQunBkUTOR17icOB61y5CQ4SV6nTjERfhMCGWAlPEfkWbX9B58Xjy9C2S
cEfv0kvKdKuBBoLOW95Q3lIxVT4+qpOsVNeO65KJ9m/USAuQYA1EU6JxlB4Lh6z3pjRxNObUQpgz
GnG72RbBen/zslCyfXwCAIp5OunwLBA2B/Y53E/bbdPoS7NSFYB3wLnxqzwznM+TUiucxBDun8z0
I7Wm85dsJOHc3p05r+u45xt100fN/z+Obuk7ripRjwlwgW437OMkQFs8pdxQsU2aDa+6BSX7Ir4b
87t57JJlUgrl6VmFxycGMLhSQWgrXa/2cvjKOwzcm8aotrYHkWrcXEa7RA0hZfeeLANLoVRLT4+b
jp5RQJJ41uRdf/Cc0q9cefEElA5BQ5BYMka+hCtELwkt9RFY1gPgYH1qVfVofnoUYKdw4rDxmaSY
ur6chyGfEUwS2j92/B50mvM6a5cA0Eknp0n7yFFMY5m+VINLlaeL9fkFwGC2bCsemoKCJd5pCd6N
EtuY2Je5w/sRmc6G+g3dRUrJKjEsOzEMBSUWHbLJfAIMfr/0Mc54XquoZ09DhYSI8EQWtZtUITee
ydtcViDaGNAKpLJ75DGp06gNf/gQWbDUccbpnomUenWNB5YbbgH8ruA2DfHknLIqGQ5ez3vYX/H4
Uz3psk60LBTuAMBPYoqHt3r9HKuiWYiJhu8j2yuYvhTmaFHybevg/OKUa+j8VDL5FDmSlwds1TYx
M7UAUsu4c2JBgswa8dYQmWQXgsQWmmP497ufsOSywpUJZVbaklPP2khp7EsVhjiG8SU1zdpP3gfp
mAEvDcOhqH6Clp9pKusQENwYpiOpbNX8Nl3LCngU8TICcuLXQa4bV22dZlSXc0s5JB5RvIGNmuMs
B/HMGmYJieQW+sjxTe87OXMvFPLFAn5Fm81TxkK1II1hR0DA7y37Znox7ZT0gSyPRnTtpXG11OwH
NMdxccroC4HG5hmr+bUlAyejKnDR9W6MRlLEhC0CVtfXNDXBS4h/CCL6u77fuPrZKXiJvxECE0ax
8AIKULqoOTKNPmaE2la7XRGPOdmRt3mHyFY9tXBpUGBDVLjhUHGxPlkNmXPZ7SHPCRcb5pEKOxMJ
+l0dIzWrgTH1yvuzrEvsM+RhfI8cI90o1IKXXfO4gHu0mWHpUsGMmY6kkOFSRusW82YuZsREMTsS
poSI//euDQ6o3EXOrNabQ2jzgO39dt7SRdIKw8OEP/pelUN903bBivQxrCOCZw2JoiY6EAeaP8zY
GMmFk2M6WpTeElfk6uL7a8Yhqie+PIdZddBc08RkqpH8grh2MkwBhueAF7WtJR3vGxF/PtahLtc0
wcOgmi2cgdseReJApeyf6smYgax8z0jVfWJDVNLqbtXd4miBcy0x9KOBxoPLzIV6xtXLHx4mweiw
GlQSpathQ9+jmJIRyGhs3iLzr9iTHVdFxuGtgKPhwOZWFzm6WaYm1fsTEb/XQC3V3sKapaP5bCLN
qfnX99jg9AEq7Gn7bMqfUDpdpa9ATl4gtw3Jk2b0R4qNX/YWTcgaUfKCcfk1u4Pa1cW0rw6n7ytl
4mo6C6cyMnQWw1xQlSQyj+iK5rfXCEG449vywnR5MzkQTI6SSEMpwmFg4jJm7KRKylBk1vdr8IHU
5P+zzpVFVqxiDzOQEoNlqpLmpKcqi7MHNguV4tMfFDA+9TmKf/bM0i8FytVMuP3glboy2FTqsq1Q
Q/01QaEANoUVl0Wxe/oHg+kXCNEkkiOmyxOldgabMuv79OPiYU544AD9Y1NlvgsnupPyDIuo+6Rf
xwelQsmy2TuNGxksOEOekOySKGMza4Oues+zqQifJ6t366MZ8R+WUDqg+v4QjF+7xlq79S8ovcx5
t9ab2G1+OcclO5CJs/qgKDkzqrNuJFGVjDkfwIkxEgzn5UqUE9fLjKqE8j2r7whvEWnFAS0v4qan
/ELxI8nH5PxEUgOxlEacT53sKjpH0HpWD6m+AfJHrDfZe4RiQKl6ENzc+UgdNDbdxepUWo9d4jgI
jjYb4GFa6w/jTo7gKCeUpCJbPywe/X7+t5Ci1/wNOPqx4H0kGv6mZTxsLaaJtKjGUI2qCszoEngN
LBM03X+N0H6T6/tNdM2bEzf8VsGDOcy+Pmsdswr8r0w2x628Kv020DYh8Vdx1l9SVdThfcjPa7Yw
gXeMcXq3nt1O/++AA6It//gAA/Ap+gDUeRUu1/jb31dSgZ48xK50Lxt8iTfwSf/BgwDE1RJu/53k
2MBzzQGg0/V5R5ca89L1ZAd70SajeyMEn89mtUM2b023YeqPUmYBteG6aE7m18enET1nnTSLqdnC
L+m/OGfMIjdxIhplc3ZaNmBwD8er6TEVk9iNqBRskghulNE9mTqbROjk7GFmZKsnsTneQd/dXy7l
Qvar0NMumYF7KRo3NzBhcmpg+dLbxJrLlZGLDtneeFurW5RNhWGj4k5vZQzDhL7ezZHdo8pa9QIy
Hqb9YWo9+bmmhU10KJqXncw2ibvtEKe5q8s5zWik13o6QoGTsU24/wvG/nHY097mzZHIwOfC0VvX
IHwidt5Z2/nWlCr1ZMbTR6E/sJbumqKA3rb9xJ/dtNaALqOhRf4lp7ELvIPdARitemhzz4ovjMFd
41eE3V6BKULdfBeYLaB8H2Af+rOgH3JqD+jccvaKx9LaSjW42Ngus+M992SKHpOqU8pVC/Mhrt0g
EDIIZrNfbdIN1pnDr3UbO9TarvmdszfOh/xdcGEYN3BwtlHB9LDerYzrTXtDrZcnFCwRIsDUMppp
OJe0fMAj2e/0nYXDTAyVEw/8/ZHm1rJnWNw8ywkS6xCju0EuYwnfQwKkTX3SiEod6l3qgv+kOLiC
C1Z/StWoA1q/3GQIOqEXA8x1rlKpGdFr6uklk2H4bznA2pDsD3xAgeouFqrkO/b7J2bfSzwxojsB
csdt9uxe7UYrvs+JdGyz+pfgosOVL5jOx12BGoL4lovp2tI2rd3cfCj1t5uxvl/zj9WzgpN2N95U
mhq17C3yooE/acXJ6hZ9cCUiX0cy6rrShqYgwTJxxO/VXSoz6Y+qEaTjbtOtZbbERcziFLcoSkgy
c1zuBV+8olWYYbfvA+w4Ekehd+UOj8nbTXxmgAz3nho+PAeG3MDR3CmkfTq0i24n2zops1Jj+cyT
0hxVM8Tmz8si6JNf060sOw6vcidwczPOlgk523QRM/Lf07AAkhAZvkMItgp7vnSys/68ljz6+GtR
1tCC7J0tm7T85o4t4giONPTLNkE4zl1d8DD6DUbUxqiFHCWhy628ZYY2w9Yb4q2BfhymkpfeiRok
jHcsm23kFUNUn0eMezEVd6DkmiK69ri0IW58cIwKfNNI2I/Mdh8tt1J1geapp0aAeLig+sl/cOyQ
IUInXi+RZF8+mIf2bLultwEMfxLCyA081dW+RehiUtmApehOxLcS/MwtCzOKmt+Flhfr9B8HUrWw
l82DciXHqS8S9cmw9m1lO8cRgqfoe621M2zJYxHA8kKCjNfUD3Uw/IBoAyJHVJcO9jtOa5saNeSA
tytadTkfQHXEHaXuev5b+tZM/WVPxO37Ubmj871RLEyYnG4KJYMzB9h1HvVbHNS+dIkr/hZCckc6
bWt0DIRvhtr3ZSXniMA+B3mz1rFFMBRgNXn491yhTty2n243eIZFBbo4JVBiYuidcw1HP2uBIAOF
zk0U0A8R17OoEahRT1RRTK/4RYKK5eZwvXSRoYjsWyqWC5NrdRVYSFi1AL1Adrr/SqH4UXarTf1V
DpDZgEb1ycg9ndSkriLrATeK9ZNmfUOxQnHpN2MU6sKumgL8BlKbZhVS9HDhFySEXYFmC95VPdYn
uWLJMPjCUPfIEDp7NF7C+6+XIzRgX7fmFPYdKj6ML1Xh+F/EzTxkFUg07AqFHUjRaIhsmYzFdazY
TF5UAdYpwEZkXra9Icd1erOQpvqo6lyd2qzpGjBBzhBXM7cP2Ge3UUGlkfV0JhjPzmXBkX/l2mw8
jR8TrBHR5CoKE6e417l0IOvnLOtsvQFu6uCN5V59hVAseoeHHmjadT3LMvSYLkEocBewUBmGhldz
1rUh1H7C14tX7Fe6Enb3iynUNQFHZF1Ujfhebs+q+8PEn/st0VXhtQHqycyN5BErt/DuqB0QzPPV
xIsJ4sTZhAhzoNFT4VAymbahFUOvrWImTbnZCtCS+9KbAxiWSA1gcY7ElwGS76r6YyzWyNOnetnn
CH3RnkzxCEfH5xPxkZgINoQXHUGB8VQibkUdAhXYV6sMUw9LhRhKKXkTXEDTF1inI7fu82HO8lM6
NP1Emf5MRyx0sFGvcvPnvB3aaCTylZV/WeILjrkSwWZF389Z0V3+Nc0tHadEJ8wrZQZa07AG0bm1
/W/gZvGql3sq2xzS9i/bU6iNZS6kRpHWaXwaxJ106bKRVTyrfzhPOiViQalvBA+EqwDGXuYsI3zh
tUjF0DST5na2UeMcWYv0KtZWWLkxaLBtaUMDSLGd+39c0y7Ce49C6ANjvBZe7h82n1toNtGROXjJ
R4FdJLTkVC6NOPCCe/0ugxGPM/Mi18bz3wZmaiYNeOQ0D00beC6Ip3qlcyLgJd3za9132wLvVuw4
R+Gs10HxBhHXhile5MBvw/DbzeCSoQecFUElY4DNv6vA85qK8Q+qV7VOEiimJFaERs8hXTneP9IH
h0Wln6WXADSUItpjBZ84slZtLYXJKfL3VLao2wRHg3y6OohrPb1VGS54yDebju4FBH1A0o0zbgXV
70cuIGWElxs2ZHgEXfPEg/Ea2G8FfOYUkqvEY0LgaMZ0Pcygzf96WSWosXqV8eflwBOcjxKfNlfM
77SBNm5jv7cLzf/xRca7/jZaRKnY2JM9UJfmI7hC/z+bnLU8NPIYHyU0cKTVkSFOLOK8YYgITFfF
IbjVs+A02PQoo0pyfpgqVgH5Wec9ssN/6PbH0BZmr6hOFkh2ixTfjbm5V/Rx/awRkgRk0Y0JWRlS
9C+uEJiHMgSGwNKAO9rtJB/kD+NYyb1MIlKJcn4x8zsqTbrSDV2EILZLtUR1H14ngEKs5xASVkfk
X0SXoiacjcvkvr0F24Po+wSUPX23XXN+XsQcg3UD6T1rjw+x7RXcTpTBy5ZoN8bfQdFeGkofZlHV
bvDOo424zFCo04R7LJBxbuEo/LSRI68yBmKcGyfrAELAng7swVrVIlfOfnEkQKY5YVZESj1m6ZSy
xF25IVm3BqKk9QBV+J3MVgMF+az7LCIunK9leb4OIgnWbw4d+l0rIxwnriej0yLOGbl3cV83GCRn
6KDJB0MnyEf9Kt+BJTYmxWW6b+btTG6Gd5jRY07P9G7ggnY2ya9gcltj0Tg7Tg0xACUpZAkMV0wA
4w1QXx2Tv3h1jXgfmrEVrrMV+/K7VregDg1CjFMCoa+KwhhiLDYEuRtugjeH4SNcw9+T8UfAUGeG
xq0yrvSW8OM1ydd8PS3KmA41+nJLdP3lzY1mZFKKN16xgyW1Qy2/cEZjc7LV/iSpgczfv9LmYPkx
TSeNrDkNEOTiKDsPIJMbEFzVskON4nMeHln+J8kEp3LA/zG/VC50xZQQwmDY4zNL8iXNYpCLUUog
/XxgG44sI+FJx/Zebwim6SQdr9SXhZ4QFZD7kvAN4/qGHpAty4fsZQqtkqEQTxxAqfhiXxiAeT4W
4z4sMfso0fbyGXcKpEh4ynvhdLO99ylsr9YjQPTWXEXGQK06W2JPI8TV1H5B6b7gAcusMMFBvtch
CnUsF0PL+eqvOSAzKmO3VXtTOq+nYvH1hCIC6AgHFsXSy1HFIaozIv3+UxhLjX77YvgBe0cu43Kn
e97VGv0eyvrn2HelqMC8xCjY7lcMXO8XflgGuqlxydSN6EpnKx076W+Dfpyj9VStERJDAV71hXBT
uSVP7gCb9piCgdwoJW31ZHrFyCDwsE2GrZkUnujTOyT2Lvlz9y84m8leECJTU8+h9tQlR53nPvbE
zCY9+/Nhxb40h0S4ielPSGBeD4xq4WL/XFFuv8AmuX5GRd7b4ZaFMIAm/rOPGxoi0F1QryWcTt2V
uc7q1Hs36BZTVTDm0llzfR71ujFoR8DXJnBciCl6A46MfesAelxNhMegCDRccBg3LyVWnxwDW9Iv
vXxVy+liSLCLLRqySYcnGjSg9A4DViF6Lma3vPIM72vgRxEHSRbEDZEhrtE/Xot0tVzCpPxI+FBx
++eX3EenQ6rimHxUwqbSXBn4CAqYUW354LwfCszPi8HtdIkw09TJjnmxuuyrvUIQZazJltMN9aoK
/WGXsw6r8AmpOPqp5175HhH5BqKUZ8nukLikHnv7fO+/pFUR8fnBX7gLPjwh/0JgLnL4JRNHvoKp
OPFYVpRSh5K9eqX4ud9HazGOB0npOJfeAgvDIma2MqdMlij9Ayf85e8DI8BBnmVWNQuC8wfzC/Sp
NicDpgAKimqZD2sYDLYaUlpC+iEMJw5w1nAcB+3HfBzAaMa5hktCXDJ71EPacTIHDyA9DYyBBNj2
wsxpxrz5qcObSNJhNFkCOd3dyS+Bz52LajROaDgewryNO0sDDMeWlSSWf4/L3VLg1TmBTSVxVsfo
u2V436vJPKyxzyMIiaFpza1Mk0z9Wc69JK5h0W2rhJCPZPNjsdxMdsGmFz2+qFqsXVnfg99aSgI/
nj6DmN1qVy2dCk47N1pHkTdy5mogJQfsU515xlfIzBP8lVMZYxOmsDcYyqPWv57zXXmeORoMjdBt
RDTXm8YrDbvJD9zcpna2hWSzU9gf3N0XxRHAWxAI9USauf168WNht5EtFwWCDL4MaEcQ8+ztu6GN
z8bxIBEG5hA8/DJeWsm3zuNldBRcN8OeVLPovhHS1Hn9c2kTa7i1lkQ5Fxw5Nbpm5CwjU2NV/p/n
FMvhYG1q0jzCyIcyrAebS3GQQM7shN2HS2EuFV8fN7Hwlp7skjpG8qpSPcv41HC5ZYC7Oi26wBMz
tBmQM2vPs5AAoimZsZsnyoiiKZTcYgHylFOoDG4p/oqjcr7obCarRbtlvZxAhZC2lc4WbBbWgEKW
xv3GIWr142AzXkNmc0bYs0R7HvNNeIRYJGxsfUdUR6x4WgXF8hTtPIINUwaLzTXWnR+PbWx45QbR
cUUEQgJD5syZlCABvktOYCtO28mkuW1KE3aJ+X5zueYuw21n3Tl9ZsoebWuRscPoJOfTdyePBzHC
mKZzHMTxb36U8YmhoNB081pez53koeY+u5n7am2mkQiAx95sv8vi6mF1PE1ILT8yIr5g7MjzrAq2
JAPja2Tmx7srRFf8gaztWvVjsQqObw1MvxWNxrkkeF58BBg1wL5lYkLM/wDqmdTUWZB943qABmur
vZiCn6pGiqjGgTcOq69yquUUVBLkmA/WsvkLsAmKGMsOi/mblohYMybm7zjfIF7IeBd+Hp24sU/N
vGtU3/M5VhEE/2eOXytSHVfDZ2QDiXGnuR+bwDwEuJz/xFI7EQXzScyzlOTrVk3mLViKYul8sz6A
BWSYgdLejUA11H2BYy3dJaKWMqcuyzhQwGY6/VlqR3YyN+qQyQYm+8mGCSUf+qiphk/YxHdskRfq
fW23RYjiHDfkBcezGsJ6JXCI+wycsXYzcf1iPW9TTlvmDh2mPzuw9qrJZ+YjRII2nUEpbnVS3Xwz
lJJLD0WElf6GGTrAa1262XyH04eQQJ8d5CNLp+/olJH9EOPqT2Ol5aGK8kLzeMBwJs5IDTTJzUgz
cwCQdiPbwvXVe6Aj5s68oYV85a5BxyD5s/Obx2f2JzmC+oifb8G/fDiPIDtJAz9dgph3h4RjGjEm
cpXIwjuii32vPiZ1puuvGU25NTULZsepJCcYC2cErgNMx8I3K9Hu3etKrnrMp5lI0GSsEU0z4rGx
M+hK3W3UEoDhlTBWfgtv+tUAB1U2OGcXPmNdN4HX0uOawsXyzfCAn/Uq771zLY1cnu3a6No7vePt
QWOlmOfeFyou5nmWmcxt7MobJzk7L7dwiusv6MK60oZQ8IYJhCpAjWngzzMJoTRmkzD2Jmb3v1DZ
YeNPyzmuAdpJCMIrouqOVnFaETiLlbAAbTVulgjMaSEydrog/T5qvuoSDn0twK4FkYauzp6AtmBL
cOwmEEBCRCxCGcLeJouq8S+PhvsrONa8dLLUeszqnGT1ZVheUtiUGLRG0udznEt+3m2oFif+UPBk
yOP8ZGFUdxG0W4NmsL0W/BeiZByUqEsYgAiHRjLYiGUSaYQimDtOoCbL7fbqnETdLXwqf4os0xQL
p43GPXBRkNZplMxpqRwEqApTbL2S31PZULWEoMPXF6+XkWQYc+g9I8nIaDiJyO96VC7cpWXoAyie
zpEzDUl04bzVzS+7EWahZwRmdmdGcyx0Tm1bQEFIdIfTGvkPjLYidfz1LiXpOu9/DACxWgWKQBbW
6r9bQrz0+KGOORz8naKnhBYd12eNQ4Dx3qexu0mQX2WhbGh4AUXMQQi0LC9QCc8cE5RK/RjrwEic
CtEKtLJUpte2gbAekPMwKcrvlGIrPYQxHK1dzS79yhA4j39SWXi9RE4ETpHhOBksKItMFE4lmhCx
nu0DJKUF5GOJ14mBqqYmigcLP/f6jLyIOzl4QLVzg106oMreZg/otgU4D5F7neGF7ml1RJh/kFBU
1hXyHuz+rQKgDo6QAN47GZDnaFFJHkzdh7i4Zfngc+EDuSmS6CcdF/Hi6NUqhYTWyDz/UO69GstD
DJom5bfB2+F+BarxF+sQNSQ/x+YIKSxPaJHZCOX9tWtqFG2qFDrZi+G1TYTnE6zzisV4Q19hXu+p
tohye1o8UTR4GqXadRql4YZxLbCnGY0+2wusOUhsNDEwXExYHreW2zk0vMiClqvgvp1UWZSeIPwS
n53rgz0Oj425ifm2pwsOFlZl4teeiu8GZYUhzR8gnJ+1CeA6rrO9rLe9/JgoyvLc8RdfmjoSJYOx
yEpJ8G/SMruldOhvkvUx9L3ctjq2Tr/3jttzEQqjA8zYB+luf2KELLx8gyqcfB0Dc7OMJjA2kMT2
PUfAdxOrKtAZK3tjpqgNkmINVl4Qt1RJQNVRoP4auFtETFUjvgx6odWskq4ExidC+bgPZgsjM3MR
9U7YhNSqJMeZPE1p84OcFWmBlzRxBDDjsLhguHLFN9u7bVNVFSJnbEyW/9AMxkzdH00li1gfVntL
baNjhID4ZRtW2YGOGDrhJOEgjwOrB1rdTKd0CbTEL2VfnRSrfM2GU7n1uXzPEo8FiJ6nAdoqshB8
H9JVPss+/aWu1atK6uXdPTw5rhI8piFb5/vzUu9dyrhGQ8jeLbufggIO+Pilekd4Wp6u9CJskh1m
f83X+RQB7UuDDL1JE8jXp5uvQiYda8HNOeMLdFvx5d3iTTIC+kxiaKHkBzhDx3+sMUixehcbqRVK
VecVAxA/O1ZD8E4EvWJyUT15uDz1qNIQUshFnbaaAFRa0wlDFkeonVgAu1hZNnhoZjjQ7jeQSsis
NNMQUERYA9FR++k84oon12N5omABIEbCwv2aWMn7Cb8YVc+UpRc4qIYBMtOfpKtVOZ+372FeS3Ni
B1Ox3TsxbAsjlSJTSeyZHJoZw2e1THBMxcCiTU1rjyDDb1wHq1+pm8LwoQn/JnKG4P/9jMg3QFms
hAnOD6xH4v9qMQD8cIe/7/Kg9Klo8p0R0e4gQPXNyB3RqY85ejSPbVwAMdlEOpe04UFJaCkDnyG+
mFaj4bIe7z8ihu+iGNa6QYxZG4IbKd59mQaVpNWYrZI5oWFjJa1FUJGY461DYJkBDsIeamJbsHVI
HE7B3V2qc3fpgJkFQ5KIauItgMQoycgLXgYPu0N9/bFBtwmrpcTMRAE5uyavhdRfog4Oqv37sBMs
Q8w4teMOiXwkW4KnRT0TsS1xm95VMoL+02xcX5Icv2TzBrx8ZwIvHrmGnzWKJZqrIDTpo4LQ6yd3
eree0RBt8I4dy3zuxC4GM1OILhXTLpXG/p8Y7T2PnoAyPQpy+dQxsL7MF3ppelOztaJYiK7bkbt2
N1RUsxw65uZQPPF9afXL6RiDJYSmLd/lVDLmHVWsEQTchO1L1oy/vjykQQNUAgAFfi+zyzVriLld
Uo8iSBEkYvhXgUxpm4MDM7Eex2pEMnWkPqIXKIa+bembUA7odMV2G9loGVo77GEk9Mun6FO8vQgm
cSfWgNKJFI0WQYt0O5vmmgNiaMEz0rI0OjHDZvZZJTrt4wA2ekhHH2pgAWarUrvEvWsFjGX6TOxU
90jGPzDxc0zGxUDd9grX4EJeyGysWIxmBZBm3uY10kKzZorB4DDY6hJLxJyobk8zyMp1m2PEx1la
wmFlXlBX3sZ40nuuIRypnAehLAy3gPAbfsOr4BtkgvryDb3TkWcNpJ86ysjzLxv0zhbg9DnGjx9g
c0NocPbE/+e+qEqvExNKnAxZ92YXgZSTYud4OcKtOenyZw5zhVmYug2tMhxfCgucnRUFhgnsAVT2
amk3Zn2zcsPwu/tGtrZ2jmgZIYfWBfsGl+9pcfGW9GmhRe+pStcKTTprKuYBv50NByITX8h6I29e
h3HuoKNY/prnton2QEvJVdFpDgYZbYUrxv8roanrQvx5mGz5F6zNsZ2zMB9/BL8WPgy6zRXUqtSo
Wy+RFWyntnlLGwnhIj5lu6etqkedcyiDVnaUxiKVNINTmtQVWvWVZDnuC+V7vgNhBvWPU/87e/jp
YyPTiL0lAmTdO3YEv2KFpe2h+CUVCbU7GyDDLjiNbqf5hN47/kWuHPaejTGyel/XZwyoEoj7SV1p
zKbJLw8XJ7mzGuMyqr/z1Nk/qALeR3L3cq/W095BIgeCYuj3Lc+z76s8Czp61aCO7zGV0Xk4AY0+
cZ2omAUaJiRL036zSLqJxVLTQRbrrUiv00XUNvLrq6eZadvQ5+vjYPgCPzCJ15D6b9VFVeuo0Jdh
L3OYp8hux9+rnGHz8yUfizrsobegHbYta61AuhkD66r6ClZf1w6JiJ/felKnmJmQtQrVuMPnwhL/
r/UkmAYIm/QLOPgZGxIohSNNkTLFdHImDHbW1Vt2m5WdEjicnTVB99NB3FeAdu05Kjm1qL4POSwn
GFvI5qyArdwOTguvMtUPITG4xocXkTyj7dbsXTH2AZUbmHvNN/OYODYhOApFtz206W8Ir4jG70sx
VQd9jArtOrGzUW41QLQJbw4UX947pBi47mpVb8/smCwuMpiEB7axAwF5Xd/sLt7jBq+9dC5tQZKC
FrHk9SaCuYP+FUYIZp7ecjiQrXp5RM5b+n3kXCycxTZdtdJULAq4SwOj229iWDL1I/CyLMkyI3lM
gQVOgA2Vzy28qydgnfuLA9j1BTuer2VEpLBnf/r9mLvNiy8Pkyklx+CDhhWlpN/9xgtEqC+GFR3H
q2IT+nrNvCMuwIoSyW9f9iKIIhi/o9rel3k7RuhQLEJUCPE1LXWFURRa2WTuw1XPf44ovlBgPloB
ssm0uwmLXgb3sw5vhLyVUril8aMbwBzA9OiZftTctsXcJMEwonRPJ/LeOUkD3lEelHh4SZKSdwHj
dWNWohY8q6u5N6ZVtXi2wmDhtquAaICKvd/5dq6D5lbPEFONgWeCGddw2W4QddiAmq4Ie2aQS/cT
EaqPKYU++MVh1b0ceuryXm/wWh6mC39LJce/zPgqO/5n8SoEQQVmTiT9Ocv62U7jJJozGtiW/NYx
dYXBt9m01L7m8g2TPExx9PtqJTpP3wcXws9mZtPs85U9DU5WKpysPrV4qOJfaqUCaYvKvV0bT3hC
Fc+DVl/6kk/RqPMefN7/ur8C8yLn3il4y3luPhtacMexnPOcpD9f66XmW6Gdz3N40mKe9jyHr2/4
gWpz2kosq2KgEITTguqTwcoDWkjlunmoI2GYK0ZrBUQ52WjEqy5dXReDvPj/bvoRaZcW2AuP4QFU
tT+fbWStqItyvd3l2spgXVPgzFIX1bNn0nlRNw9bo5Mw+jKepZhYTrThD8HnnFu9nhXMR9OatUPe
2Kbi93+lluFIue4H15LvXgiIDXskY+LX0BqFCKyflnDtGjXCXgMXggDUueB7R5Y2DA9urgC0r7dN
T+H7nSv5Q3AfVL9t/nO9wPt0Df0gyoUxdeCo7AyL2c5qRWLeAV4i2OuBiAffZeR72tCo0wG2BkZE
ZIQfgrWPQbeLUZcCH52QLEkHFuZlai9w9DMo1JiM7bprcw19S8JN6HuzW88a64c18MQZSdlNluRa
MSrhaPuj7hERsRiPvLmp4W0wHfH2MUmM1wYb8SqxFXQCUTwvre6rM+VsZ3XSo2nntsNXFAZv6eb9
zP0p2A6ZQaSxj2ie3ac/1jdUFAgauptV3xgt1DU5LhHRavo7eaFZKO9rtNMNt9Y+OtZRr88O7FpI
rzdLsrOb0kYSkNYKBSOGrKtuu2ZAbmQdSoZclgZi1Ul6ya/ItnfQUGXQtU/9b9SB2QCfzdjjOqGI
eUfNEZ8iErCcsZ/yeSipK6jZaA7BMR6cfnNR72CUEmPLbsnUAh+DErrR5Z84fZ7P8VC4lwXgwTi8
yma2EhuWX/b+fsI5J5fTM3KoRYFv8u6pQ2xMK0rLW2Crjphi/9D6xuzx+v0dPX/sZ4nSSEDSUfMf
uHN9V8k3de1yjsIloqnVh+QwAyMYVqPH9H6BZJxmmDu4lDITxVbAtxu4K0m6LvAuzG4Yz1ljIkUs
N/MnBylEF68ss1rW31SKxHwJW8F2g9+4zgkTFpyxkkizO0Ilvcqo+xdU+6YWw1tXNlEz/bCVXjeH
jvnSEslf8R7+rLNf6iUjm233mL7nb0AzXAqxDKRXH2/CyAzOuJzw3UYteEIZHPAd/1suOcMa3Myf
k66AWLt5tiHkpNGuFm7XlvVhNl6MoDjexGi5mNZKT6mYuLw23qDhxh03N4/2JWtQHqlYYFKnm07H
Ay/BX0LgNKbRaeQNnXs23Wa0SXoLYD55+g4lr82jLYDR93Xr8VosK3WoLpLVgdvF1NOzEUqBrQfQ
Q27KsgONYUBaNq5d1ZNT8yBtItk3rGgJdQvGo3iuZGPLBnrwogOkPmU0aJUdaM9ABS8XU5mWBao2
7TV8OCzMzmVmE0NsrGXip9j8r+22VMB0ETo3Fd1OK1OMucAeghPWcu77q3ZFil46tAaA/Pbw9mcT
ecaslrkeb0R7LZJZVY6krdpqLm67GR6D3qVlmOloqOSygUyVldo6vNokAsyRcRixbJXiQGpmyDay
Jqw8Oc/M3IYek6mwyAKakCLSI7XuPgN3QKmkZbvimu7tk24+gK5H8dmurGK/p1eggFFRAZTHNqXM
GLIBO6l1b1c5FvlOVwMb6rhLcdjugl1o7oONuZr63zNDxcgAu8P/hkdg0zmoi/9zRPwI/oSlVzw3
/1i1FoUa83smkomRIG0E2x9ZCvsJ0aaxALvhNbRdX9f53bjPmLhjVfMLgWfTdPtPnDvx9B4RdYab
Eo3WiC8U17iBFTgxTKKxa4TGWxetLU8V5SNsQDBTQ/ypYEsmX2sGFrAGKyhy2xM5kvaA2zaQ5kh7
Dk4DfE1Y6EeS5t2SyHNPfxCWROKQA8ioEjYRRnpcWetUuc9lAQDs/E1+gaowWSCVcDYLVRDpHzb5
K8ajqyi4JB/f9FCR56X/XNZfZJBIXxaiGPGzG35uukGO5FD9ma+b7VeiWlHtLnXgPmxz2xdW5FRx
FOM9/Q+K3/wWnZ/LTn3XuDdKkNt5boN9/WYPwK3xEpdhaBeaLV1pgy/V0MA+qM4sP7odDWdgobcO
+tlSOb3pm5bJFtDWdCOz2FsYtcylbqIr2vVKBztTabADlwPFVOmjXEEnndVe9fM57pFI838Nskps
RwAWdXgZ+PUlg8HgDO99gFu8KzzEuVCD1u7jKqvW9LWOSnVpY/A17Hwz0cR2THsbF6rf6QCMCdAM
LLXnSp4kaQ33k1l14kAPcyUiRqmT5atjl0HkXzxvaEKQa8dRDOlJ0ech/3Us56KIvzGtRBy3bCVL
8LMCEsz+VMZAFMF6/kGYNEwQD2OocenCbAbPFgP6zCHQgotucQrZQPTMdN8idYA2MxZ+oeZWySCt
AcQh7twU0GLZjL29Lz8WfbK4cGmK+hAKuKmh4ceLecBjuO4qEY9i76ehqkO/Njkl4Wmn9kjArtsT
P+9BTWwIwtVxZ2YcrtzYjbVs7Hakjks1tl/NC4VOO8OvuRodxBvG1OdLxKp7GzVa+2glhCf8IuyW
IVoZigR1Wdv0Iu/rSRCqVxOaPo6vVsuEpI6P4vrFfBDzxu2EUHXgKHyXwfvWXMDcR5qJExat/hRe
1+6wmAcMAoBgn3MQuYI5kPd3iP3DtrtquNtiy4La52DQwCLo1PKV4GYI1XuVngQwWM+afFFRvsPE
SCd4W/42FxJd6SBsRiAYtkxrDzU6wQ+gENNe8HIiws7d+WAe20OXY4C3pToGxHNlyglzjEmw0HK0
mMVCZcBgKQpp9ZkVkit0NbkiPllDYlFY/IBPcvtOyvQ+FjY+d7bmtDymwYYaIrvlXCyvbFIMFR4J
sTy0cg+wZHo2NhDoX5MBJluOc/cm5D1MPnIYsI2N2Lk1dBFiPLlaVxUf4vjFjf6a3T+vHd3BHiCy
Be2YwU9CywA/JuVl4KYQRJg7HUJYTzx5yhx/4+NJOS+AAImUY4xm3KKHQ6v/pjAaywaG0oTUbMQD
Ro1hKs2EvQO3DGdZDv7+W3N9HSnZ8Y+Iu8AQGw4efC12XDRQEWJiD0GsCmA1Fp/eqk8/DNbBJMHK
hZUaaT9StHW9nhs0u7irK55HKNQZkEz2bluB0DNONGAkSxg3iqOBxvcSiqvxmc6KY2ZQ0yLur4dv
qaIk3M6jDU0S/IhxZzr83jOqGpNBiT+9Rd6cIpB6e9OB4VoAk5eP4jSIs6tiuSipwDQPWKfu1Jxf
nrCrsn1Z6nJ+S6q+d6ooAhBBC/SyBPWq7CpMdMX5ybd0c7zpTeobzvoRRYLktQXIXDrOyB0YnBsp
QU49VwMq6fZSU0/gEcLKia5mN2qyGMjhNyQNi0lZKM6BoL0czGdg27J9g4plYxHQM6PBryyRB80P
naxnQoxKQ6PilcYVfjNX6xfznKRs2vy9fpO1XC8RiDC+3Ns7l7BHGMK5CpnwSAJ85iYQSqv4ovUP
fXvJnqI5IWC19lydN67IxG1+7zo4MlRJub8cgRQEWpvNiKIDiqE7VyBW/FfCD4dik8Gc2qn6T3Q9
IrDt8LHiAfvzsrSwVaizQw7fQXQ54xXS7vEgIPPLR7SPQEn9cNi7EiYU4CIl+VP/g4L1+LUoO/L5
PgK4Fc3dOADYfehgx3K8DJozHuDG5iWZoNBF92p8aD6JU0ahZnm7omPr2YmZoB2QTmjh9Mw2NjS3
M9rJ0QQMlvmJvTYwE9FlwT1SFaC0bFayvt9wQJ2dh2mAkJwsCTUvnc/Ru7Li/k6haVBQababyoVx
EsvrIP6AZx2YfFyCNUSaXQuiAAAHr+t22Ko8I2xuupL/nsodW+eaDN+ZFprKjGwZm1GfyrZXsSLL
+jdjx2MqjuW3vlGoF+QabuZgRO/eENf1c912s1U8Y+dTFW0+yptF1zsLkxKtJoX5t+FKjbU+jkhP
r4gfnczM1+AhwdZShMpu0dxDvO4amWvwPXwOmlED4ouh8QBwOmbw/b2KDlRZKY9jrqMB7JXNO702
s7tu9kTyB7IXDurt+gHQYtvj27PSfP/POw4pXb/MWkySN1fp0B2gKCSWRY2NCnkWw4n3HzU85k2Y
Id3dLRKd2aSossrAQxmfFh2Z9fdo5rHQIrTac24pFGXLYGpYjBNDKp5FnPs5tjjLzUj8T63siogb
+800vgwC0qhzIG85GkLhezOJyN1hKQkw0A4/1kG9c1yu91La7rD2E0VjTJNlEb6qs0WBKD8P/31Q
90M6QUCYlFuEMApZ0mB8JQnuyiav9QLA/jhA5zakxDvMQY77NtDMymXdM0vlnWUOP7dU+5s2NzxZ
1d5svz2JBFtIitu7L9m8yicFs694x2b1vdBq6DCyIYC14J0OVwXkurtpQiFrl7ImvTj44mKCCvHZ
duMXZNgnSLiX0zvRdEJBGgIjl+rpU42eucMlPCId7ZmklutUT5LhE9ncbxPbpLCYjYaVAVFINrqQ
a4DxGK3/3/bzD29zeo3nENR5K6sZkh7s00KeuJ03/QY2oK2Wz1j7ih/W+l7Lad3pXm75uNNRAgDq
wXMwDQQUSNGexrKL7HRD94Yx7my5OJkzo0OH48DUp36Mb+nGgyAMMamp4tPlVaYiEBHX5qDoursn
m0tFQPA5ZZwl/oo6rSnzEuQTI2LNV8qwESvpEQFaLcS/6PQ6ZZEDkO64bj27fmzcPBJd59IdXVZA
yGruziBUWHi33BjxhYF2xAWGAMV1GwuDG9W6vi3W7Ght5opyAr2jCc1xyzEojYtk2uSI5GIucYHM
lAFahHRA6gcDcN3w/UboCM+aZHhkBoYmdodYr6I4+0QBDfBaVrLR8Y9f+XMua0ClqTrK0GbPoxmm
I9UiXd4Xh230fQP81BMpKYDrSCooQsLRwJ3d4cZ6oB6LVcXv3hzGng4QpGnHSEuwvMLCZeIF44gB
mMsTi4X9E0NYl2m5lHGMZg8pi49x8N3vcxHSLtAMiKwPPToHa7ZLy9WBaVdkIEHVLk4+ccdDiqAS
lWmnzpU8TZq/eYKROJ8JXpuWbecheigzRbCr6j+b8YkRuE+RX0FqiC/0l6I8lkjYtFLk5+Ic1RMi
j/JfDzfNMjqEZufg2vw0A7bzBm6qElyxvxhLqjE2Hxz+69CV5p9H4fVLYMWV2J6uvHkwoa5Z0f7t
WSLAx8ztg+E/FxQ39vafxuc1m/F92P6+spxWLkZoCqFBrvEcGOUoj44uMXRV4YzRWHDg15aIkC2a
Hy5X+ZvzzgYGEA4bQEB34uQ3TtGgd/VRE0ZfalE0DXGJ4bk0JdBk9/bkA84TnZXrmC4NQ8Q+A/OY
t0D1Znmsbt2wEW2iDGNCQQ+2JblPBDhoMQ4x7JDeoo/zeMkXF0O+6NMYZi2jvS/jc5qaV0hG9mB+
I47DUwUCHIBHZO/jU5dNh1AsGccNkMzra9mIWvmoPIbGJIBtYieueR5WwkS/HbvHiyisc6M+TR8G
+rsb8kUgqX97C8MzqKy116xACSH16WpMrooRi2c/5MHGauC7U0TfWqBrZV1Hg0VccOio174DYdqE
DSML/4tHLeVG2FKBLrEqxbOXXAfP9+zN7AN8FYvE/v5L4uNNac4bb2hycFF/dn3PmLIhqDXM1Gmq
Mj7Y8uqybJlDQrKVk1zv9ic3QqhWKKXj8LpWQtPhJdrKod4XbZEjhDfYsdfII2aJYZjJdbrG+3ZL
rq7rHHjozfMqd5Gm90VVU2sQtZEdhTuFmP1u20p8xlzAszW8qvCVKQkc32JISlB3RyOEWM3aeENt
vQgWszIFEDdKJ3+OwnDbNinyi24W13wbrchM5mnmbP1ER1ARnEs77sO/q1RyD853Z4ltixMlnxBb
ULrir1bHDOq7mDamZjTEj2NocB4a8lkOEgKP1ffYfG8xyJvfnm+GTF8QukSCJ8hLLgXyhCeT+WzO
vBS319CG/Udayq99ml3M8ATSoi7mNRzOuKLQYgU4EYNej9/Ln0BJ6UzJEb8n2y5DdP3GSqCGUcOC
8Xib6CUW8IlNsi6gWdWLuKycuHHimE2joa0Nknw5D9w1J4UEUctPWnUp/ARSWVTwBaMrQaOxR36E
bfPYNCtLoBNKo8PzQzkPIqbO1YAZaBnwY0dCS58ZDrbeMZonspfMjeS3YJ03/zxfkI05Yk5Dr5en
kiaBxEjAo+uiHSdaUIT8nitM2yAg+jinlBnw+7X0hBMRLHvkaQctddXy52rv+RfjxEb9b5oTgtk5
/CY2o9khW8SJueogkkVTtIBkwEvozMBiMhGVNBhDtc7Ctz0/eZcNBzCQIAAQXn9l+7Qn+sbOuE6A
MeW/w0YZunxJ+9EtePQbtZWBWbm7GyVjvaXa4OPbw+zBgCOqZwZ8sYsHL4EEbdHSYPeSjSTUYk5R
fFJuES0+s1fGZPMU49FBpaNmaE6r9IsxIbx4wXFU28ixNXoxVkZrbb/y53Tk4sd9eebHmh8ITN+p
5E/uE5ruvsqnwPAIfI6pj7ARrGtKsBm0mOodLg0UPp0hTsBM/rwChOApwUAD/VKSQ2h3gxg4ws5b
XmKdXpLqbOJjIYq2jprQ/rZG4iZJf0Rnp88DAIuKb/XG51lVOmsEZdXZ90U+RNspiif7+/mB/v6Y
CxDuthW49E5/zzQxIFt1I+VKKNCPyej6mt0R7w1T8F4AVfvFIXkscWmHaRZrrl+ndHuGpGoP4PDv
SVNiEJdqarntwDs4F0r3vajh6oeoBdzmw2dF5XUenLrulKKOXTB7ryVXkShT+NnkKiW58ZJGiXTN
tvkg62X8I40dr08Q9cuvlQBCJNerAQ18r9pwjeCO0Z2RoViNC0YdV6pWX0JDIBlEsIygx6YauCUN
rT3GLTdSIxZj//2RRXOmKmB0DEofsXTJ+gaWAylMW1dPcRmlSbuY/i+wScb8Bzm65ckEJhJWKQie
TegOBhJFdHv3nmjqfUhNhMgQptGYQ/AQM6x6fD1Epe1ob9vaLhMBVSi9vKW3CYvn20b8gYQl8r4Z
hXABafP8BCpWbDxxQ6xK9qS6HUTPrOwGzC1GyLns5yJHjqvZ8yiE/vmRzhJTB1qKoB+dkMiYv4Cr
2ZcZcnfD9IQ6Ij8MO0luyJHH+e/maJ+UDEsr4ToJpyED11U7FQkdT79YtMbVE2+NUprSJ70t+Kpm
/PVQ+2SZxqhzsq8uLMr2idwRk6vsKy+WEITJk+BNIsg8YnzwTYCm//E1A+btRP9YdGHwjXP4c26H
jXgj1MDfuc1DxFLWL7N0NE6ECWE1B7IxXPksPjONlMHbrSthccnRHbVJ3h893lp/hSSvLsOrZzeb
sqdeFar/bksRSzq335xNbR7nILjQNnZzaOB54DJnveuIVjFDVtP+rqQCJBEtdraYKkzgkzwU2OyX
mSX3gMoUMfEnQebzo7xd5pn1Jg/H7uzCyDlyTJisY0qZCwKjFYJ6DDjO5q7Qu9J28WBkvL/QMkHd
WX6d27rQ2ttTmZPQ384K5hyJQCTdvyWOCaVto4PIXELOSN+4sSAcevxtb/5v0cM6r6mhhuzy9WMv
+kQ9h5/dy7mM3eIY+Pb5YHovVS8Eefr+4x4gI+L2s+rp+Yzpz0Gz809UBcccU2fOwOvsoOyul9Mm
6OXw38P3Rr3gxsrNokFBfMRS9Crvtm3JA1npS6HJbGVVpG3S5IXHupVj22uEoZYjVU2CV9NjcjFT
a1BSU85VHgv4F0Ovv/FIEpODbbGKUoplQF7wrP0lEUPUfQKp5i8WlDEp+WrqmGQIAID+GSfJU/aS
vGS8xH9kKVRPMmk8q6PgqBeKChFoQOVjhvILzuIEBmrOX6FYBb8mwkE74DepHe/F0R4rjFbLRixm
bu/cjb1ls1wB5OpApZ3m9VdutS7qlXnDk4QdnbyjRgOi15lGaWm08p5g8Rl4HEgl0CljJX9I1fuf
vBd8ekS/6llkd3rxBafsDYzdW6qGmK+Tq7O3AA6KIuFw3qlwLKXajgzXdqmMZ+d/4z72Kjjo+ag6
7V/j86YYnEm8EzM3K4CM/5FtpFIRKymNeZELtJImQQ0TcJgRZGXlaC/tgdTNN7eW772Vcpx3CNm7
NIFefEplQkvSOhf4eHXsKXu4veXRMC4UrFTYUfUKbX1p1DtFl/xmfMMDGCo8WX3pTrGmLf0CESJ9
Xn8o0thyAU9PDlrI5YxWAAcXKgigX6VitkFzFUArK0TDPqIh8Ft0XTd61g/yoD4hall5Fd9GkkRv
Emm+m2kfVa55QrjYUECU/xzwe4SP1SOr8YunKswXuOvqkP1OdYCdX55lyEk1jrstjuizDze3pfmc
eT8wZ5PufxWlCcYrxPK8aKt2XBa7USw3jmVdatAsyYbKT1xyIqGTLELn4hb7Uub77C6zdbuEnMCX
LMLNHoLjbcRq9yI41opubTRirfyAZrHEEw3scKfCx9fWYXzsdWxerr60dcnNV3rRgRPap5B6tg8M
VY6hV+WE/1wRCX7aUMh44OHRjOAqzqrkC0HTfwnAQO8qtQ9TaQXVCWaLAXljqpWzuMCnbhlZc0I6
hrzDP/BHjYX+x4+hBrASFnSvsHhmp+jD+EEyuqbXn5y0jap8KFFSbL0bNG1rTDUrVedsiCP8NlT0
z0RlkF0zPKgr/TzLFhupUzuBGfr5wkvJRKDCQgblTlDNSeoECzy0dM8dF7PhNB81C22KH9Uu6cX0
3neZAr6mLHny2XL5qb5u1ERiyQQMp2B7U3ZY42EUq4vcnadv9purHTc7oP0nQrJ9VIklXY9P7Ldo
PngbAkXEKGZeJasLNe8GmnQQ8sz0uH79xJywgt20tNCFZG+QIdUklcE37SjoiUMPL4TgFGRS5mrA
lN7m/ZduSfQqTqIc58Zc4OWSkAy3qgaLtzQCGkYpF2xdIW150msWGJGlaF2+Vkiijm0dVb9azdyg
klB6uUAzsJMD+REWxFQ8EfZ+N0sU7xC1CYNIYnJcfxffRggvaT5irCsNZWmlXtbqsHtqp5Ta19vj
FVR0Ayo64EtoLYaM3GksDGZ/au2zL7qjAImGgmZn+0qbv+uX74gpB5lIQ8gr7qh/Y0e3TMixUFHb
EODAX6wsL07BLaQi8b7cQZN8OmwpjCz99uGaPAx2BE/7oW5DnOJr9JN03aohV0dS0R19DrM535o5
gG0bMSCRiWeTtSeWRsuKCV10gJ4OHr1Gy6UPPQYlX6seCQlbGKtcyv31OAiA1bCaz/I8YkMV53x5
vemGKHylFYM7+2B3VhywigFtAmxsnwFKk3+ht7r1cPyCwR53f1momb0EHcZCS20vN5xHXv222vcM
pxxwtCOM5tjj6rTGVDyHPFp5U9DL3fvX0+uvdgOK1iOCZdImTbqWQW7W6pmuH5zQufc0kBjdLcE/
Hep76MU8cbT04PR3Tuo/gWAyrKqWWjqCeFegYMqkdy5pyDxpDGMmei6yu3Sqe/lJTMSlPtq1Iyxb
nL+KSlgvtSoUA+NhC42tZoH7E7M8d63HUM4qFOByB6+/emGfgTGqUXfUXtfxYowzp+vS+UrMUd5p
QjG7lKmc3CYNE2FQ6D/NrTxTcGWCLfofJiFOUBtt0OQgmvauZpaFN4tV+KJ8CCMbx0HLpRxkHimo
E4fmQep/aiV38BWT9T7Le8LGMqY+Ym24US4GAji6msaeQagxY/N3ywQxDFtRw3EVf8vjebxMUXtm
ALayEnDfNACYn/890KJZ9AGgQP1XiiK2eaLacp9uNRVezQ8Ix/wvAWgPOaCbHyt6cHpGcoCBQKNc
oEb/8kvIY+abhzL4ivwtYlI4FCGVp3/5qzduJBXIThhv7I1abtGsgkwULIAuqTlIBXVcUej9ecmB
gFh3VJmSrvkxTA0gBKOhpUYqhd/ELHCsQjyJAWExhCVE3w4CQBzq+/28N5yCjPmKpFkgEHQlmIBC
AwXb8QHbNfjfp1jCVYpmFMeaQ2mh05hiT6Xn+z7glMbz+1P4E6OpboPTEs+kw3oI2pvWuAP9Dc/0
4fGVW/YpHkKfDD9iY7RZUK4ER9kqLgDqfXvxYxgdXeZIriPVHV+x7A5WotQFHhYX7h1SxaDbFguF
4JUdEHJ6f0ni/bHpJq7gfcNSNyLBQsoyJ8lhG/v3/b32DHdJUBAlf1iB7RNYd2SK5ZL5pv1SPgPI
ltoULX+SkXvU7abMO1qzd8LUAE/KA66uhB7vspLdGTVZgAnDex65xU2ftDH9y1Ep20KnRe84vAED
+0qccsFvovWGsa2DMhcnCNmZzEIg8ARN/cCeco3NQDoQ7eyKmS2QP3kAI4hUQOxp4LMNuopyvAij
Su21l0OzpUZGmbupSs5OUzjC00lOWDB9zPExlcNnzMpBLhC/j82dAVHSBP8hDdylj7Vfhj0NW8ar
sdQCp+OG09P2lQNB23Lx2t+DDF+07OMdaGZPAxFioCcupKJG+El1h1RelJposLxC8dESPfFMz9m8
GS6ZImeBsbpus3nvaNZISSodxaKqz+gXyX/Ev4Is7U8M71f60/644kkgoXlwL9Ct06RKucXtJjal
tCpGyHkKIObg/IlS6AlgJmbtqeVpnbSiMMEvbCvALIcGdf8oGTvSXF2/Am93x8QKqrYTFnLVynJz
O8N/pTeC7rHKZuDgDXzzf2pJsCgocDUushrPgCc4qDmDRNW1I0YIxTEIOIN9lycSmv6sMsNSnLqi
a5TGMLxyshX7BN6jne3PXHb82AozFTU1aV5/ssVKtdj2BDAHwYSYiaVSCcihtKROqRtid7vksBcx
goylo/zXWFVmIMMv4cNiTzMTte4p1IovuBKJmOFRB+HATp1R4fEXpO7KoB6EJ+CYRxQqmACRph8n
TSZ7/9Ce+TeU1F1+CTynpGtO4xPOQF0fCNKxwXP4T3VBByF+6fDCdn+t93Zo/ti1RAR4AtS2GbkL
IATuRfv+5HttqOvokIyWcDhqcPKXM83+2UXsE1LK/HK1qxIb6oR+samUQyhB+P63ir2lerAgiVUQ
SRrm78bLnMFIR0KoTSBCz3oc1bqdAJYENaFgEVMeZ5TmFXB+TBlvR9iRPsg5VEVMwLShoKU9AxFr
B8tK/+Dru1Dm60m0Pp2CCZooIoVcFHogF09C8rMMnP9yu+rxONY3Q4Bjec5L4rRW6Q6W8XRfwnvd
z2k0vodMken/8yuaeDLNCuSK+RaP5FoOzZeljNeqxQUw5Z15NoMYnJA1RV2O2Pc+vLst3oM6QFAE
HoZTFQZy1mqYj7/xg0rt9FWqbA5fUqwRK8wpL46czk4VJRMu+Y5jy3ww9DFJehho/+9WcXIpTPmJ
bomkcjQIWnxTzX84di3wJw0V7ixfBudITU3s7PTg0RbWASondgu2chaxeNciYn4XZyVm157vjrCO
NE1Fb/CzOZBfEuMtsagrfJsrvyyAppf4G0Zzg5vTwRvdUykxq7vtP/7UWfSxFYgzCOdrRLw/UMW5
pOGV3lCDhrK655plKcQrriRVJi+GV0aw+jl+ZazZCl7/29H6mZWWWnChYvylxKRPz2zHWNJ3F53j
Gl0DoVALl2yRmIzV/xQUDNEC1wsXOIy30wrQfMowpfMP+4z52KWwWolkeRF/FM6hHoFgH8j8NFA8
WJFoE6YFwbe6B7r8fvbSTylDvdfIsN+sd8SghrEE+UVkOraDt8o8UocgCfk71SuOiWzQ/EqIObZ1
ZBmB/u5XXYSXuT7ZfqpP45IQXewgNe24ieR7XpMb73vOiQBXFeC6hA9UcIG3qbJwUAF20AIYMN8S
qdzgzQS6N82HRvzdrLal3ni1lHW92UXBKJJFr+QYFu/YGK15x20WqYMsatEZf1m+j34LGg/7tWSS
95jcZyKa9FxIXDZVCZbKzQPC1AwWaC+OBj0ukBjtjB2uHSUanVSDh3fGkYH4yvYB5p9rDnVtml/8
+yZhd+x2xkbFFyaf8hZ8ZnIDJAYAon6w1H141uC8huZJzA6qCfMmpjIb7Xa24dc8OnhAzPbbbcID
VNCc79WnLIbQ54Ph61s5K/6qDFXhhHNOD50GXjYrj+HMb2NHSX0teiCWij5TV+HvPsRJBIosKW6c
Ew0d2Ba2Wfn8pHuHy2rCCfbEWY1k8BQFefbJH4HjjhIUj3yJWpwuCKXlOk/NeRqc0wJL4NyrWZHa
QklpgxWiHQVeT/gC3tNwCU00KduWvgIlUgDv8Dn3yrsAN78WkhHP8u60zoyxEVPdaAO6K7YXpHT2
tWnbu3zkJjYZzu/HnHI8Xr5ZLi4Pgr8rxanFv/n2wu+MefNN27QvMgd3WFLgC6tic1FnkYv0lwUy
DUCjxqJhffxERfI55wXTpgurIOSER7Yu/N72zOxSe63i1/HEmdLYRaT/4lQqysOBNdiTQH9UJVEi
QiahSzrWhJI/QPE/DnjANDRDa7NXM1qBLluxCBFL2yTuCdLKbnDnGhbz6UWxwow/okrVB2cM2P64
FMkjvTXBZ+RRP/A2NcV8sJzQ1vZRqCHVxgOrdGgIYgKPHPkcaJ/F8R0fKmEbdP1ekLoyXUYGBY0C
kHF02mSGMFtszrnLU63E8ICQccxsy0/oVJZ10hcZ0W7d2QrxXfqDkjNIY2LpYVOGNGOScOTcytjG
+Bl18fLqDUfeiBJugkAwY7twPP+ueNgVNqIRsgEinbIP1ZyYaa/EaCNUsE8tcR7BBOLH8RkMGQ61
8FxYAyKU51T7J2qryiDyhjImZtoJoeHWW3JxTd9LQlP7l4AtpfkA+BHwhCc719wi3oBWb73kn9yq
tmln8ZHgE7RMFIwzBQIf/wwnrlvKemKnlbp8M7YuFPGwYkk7TduIVGS8MEqZBnNR6n7TURry7m1+
SzrdP6UYW61wvhKiZUNf9DP55pBGfwMLxo15KVHVX5ucREvvEQiUeAA/f2txFo8ry13QaCHUiYxc
L3bakYkg7Okg+2ayC+9xV1vDKyDqWD3sIwbCSyb9ydp0W6DjwegtNa7NLo/jB+YUeuvnmtyxUTCQ
ncU3lZ98aksHjlS1EOnpwQRvMphWNwU1QjxxXGkVCkEGjCCtbNsdMf9hFxnnH18di3uBsu2cbz5i
L6viiz6JCuuCmckkXx2z96TY13FY5jx3EKJz8yEtT3jzkJnVGS//7UZyEWURQX58W9MCQmMj0r/8
UCmzE4yKixycz4tUJr1Cex5kT53nPB8/6Ck+MaU4avIZqrnxh5Gp4GUj6+IZbS8wYUoDATBOv6WQ
cowelKhtvbjMHF4v59NurCWQRzN/jbEjVE1FYu+Gq5ABD/nimgfGLoxkP45n+y1d1dwGX0goamFk
Y5D7g6qYKtknrdTdePzqeJp/WyK8fLaAYb26lYWknBchOIbgdwrnVrHDRmEm0oQ4y9QQFqYdK46D
Hkd0ARl4AaIJ/6nThhKsRLv62tiS7ecmI+vVRiG3IF0l/elGp1bRV2WZlaTAILmG6TdW007olYwR
cH6zAy/ZINM5mvJ84selTthcjixymivLtU86HHWNvK4ZG/lucc8bJ5RoXO8Ye+xe0YLjMBc15bDA
ZJ6QozK9UBg7hGdqmDt6Svc8THv6x8YctIHKfFS5p5LZN+Pat2e6AN+g5LQq6AoOIbDAH9nGEh2Q
RnvuO1/DOMpWT/CTylFvYu2O03iTr/3Qyq6TpgT7o8Qbyq+z2T40cjYf5u3VcvViH8LCOwNheoMW
fe2CUnCeVzGs94LY/QjVyqpy4s08lntfpGTgS3LgEzB6J2cZUqhWKud7S0zvA4E8v58+4iOXqfIh
9Md0eptkMHDz6rPUtRZt+9BHQ3rqnvnfJX8q7QtcBRLebZGa9MQr3osnImBaghK6gtvh8ug+sCHp
WpTrJ0uqiyqAgzY5MbKEWC8zY0ONuWFMjJ0KDdUFG3mjzHm8u1yWuC6NRs8kRR1wKb12gXFt+OQg
5vEqArn6WFyXreDXMGgsNJ8QIrNRWBAr9JEXYARZCTol1D4Bl3tIbG09GWbLfxbtcRIE0Bi2BluP
pk6Coy+cGMsnYaJruXEtcmxXdpajJ8IB0NMeFb4Nc806mtmQgGcb4flX94Cc+mMKY5rNp87zlfMq
sTO4bZ3jwhBoIdQBM3EI4a0xHNCxHf7qVxf/FarMwJinHxE9EsP7Ou2ENj+Z2o4QKtVVBy9nkSoS
3DDMCEo9NSzr9YVkDdBSS8iS2AIVQG1yYahsnaGoLGoHgCvKIlZYW/cDSkd1skxXg56RmiLGAT2M
0yTUUZjrCaUVVid2NfSTARMo+552eiUzuuqCbEznlsagmNCnP5f1q8GPfhTxyjyRd6EdYaP25xCL
Y62/einqsktyhmG09B6tRWpP4G34loZ1kX6ZS6me7A/N4O+UPWZaRj7jkWVqvx5Voz00/FFy5kIm
/iQy2Ffmt2X3gAewHknnIv11gEV1nGJx+QM5xdXiPIsgHH6Mx3+0W/7N41P0ygI07mBgikcioWx8
DQxRaUcn7tOWKk2d6RHTBTlByXvA5jC8z3A4Ky5Gny3uMHn4JHmX2YjeP76RSfV67mivwSRMqA1w
4urwtUrokuOYE4ZEj8JDq7tHAOgfhbRSQqKHkAYOgzewpsHjQmUTjn2TuUMTmOMupxMuPfFM7kwj
ef5vkdeBtXyhi4vXQ5HDkFFKY2ecjpq3W1Hj9M6gOwKigpsPKPs75ijr15hVR1BshoSnAoyNx0+o
76uo1gvWSvdGW9Ps4zCqkz1bSaR6UPSoIs9j1vaMwPthUAwnP6wmIM1oQOnhBUnJQpWHUD3XB4LH
HiSRAfMPR4/x4kHxxWnbLgnzuAlPm3cqGpsE10tcqGeLKaScZ/Pzb/QUR5EZnC7K0bnC+OXiIuyE
RBpXmT56Yl+YtO+JMTsrFXSNwdZblSIluL5HWPXQ30knUX8JNVhaBAaTLTyLmE2Mzd26fadmCweA
SPZj4zymnjeNDezRf63NTv+58QDrioYzxWtFSMVRnz7mcjS0/UMpBzCa/BqoTJRZhT0JLcl+NVnl
o1Cl4XL7urx73X/HAn9y3BWRC6LjznT+Tud+1fBJ1jKLzZT3rEfomBYmE3SHFO3yAf5TU+M8EH19
LMQ90OkhYfavTeTZEqHKC2CHiunddl0sR8PvQKMPsXZWEku2A6ovgATD1oR+XVGJ8aInLzwbKwFE
nuf6/uH/+zZGIfDN2hvxsDYeZGfH2vWGNbb9IgTvnstpxeqdGp/IQklWt2xkPrTqKF+qTR2Rn77U
2jjzWYskRS/I32nWxvWffTiB2WF8lB6nOG8aMBp0qBofa2Hhb1FYJpySVvNR742HGSmdLKONNFrI
CsDoaFJK0okZHVM4kEmDM/OIplqnRbOKWYPa1FEqigwig7e/bsSf5KSYQ2sCcFAevxR+M5wunNir
y3v5NyJFx8i5zhB/YDmgj2039z3fWDLnKjaigG2bzqQ55AocjJf30IUQUqNGgXg0ogAYaB6Oe27c
OMai18XxjXbfUeyAIRZ8g9xH0yZMx9rDHHgMFMPpgqCYTitSh24uKfwwwzTKCWFwpPS0zhe/KIDR
lgx1dG2lKRkKMrPdeYSkq6QTXsZEtFyDB4fbsR/DRosw1ALJgfoQEupdnyazaXBPD3DOVCbM4v3Z
/sWNm5hgVzFf8HeGlprk76hJmrfjJ7lHzIWTmx3OtsfeMBBS9xVL/xDMVm2Q8BrAcoKoyZ66rWGf
8l9PAiYE5VxUEZiQgwb2R7TCFRNp78OJyvTiK7nY9Ffk2XqCqKAVocumtejHBeT0DUGjzeIwXR7X
60zi8BmeKTzR8NzXmuE7EVUTZlGjssUi5dnX+IbmhjAadi2skIYXZShFCItImVyq1o7SeRD0jNJZ
rHo7E8GhKx6aozdQxcEjQDlSamsFNnSw7mD1qI/ZbEfVnjH7BTZIR57SAfXV3PS6UyUG18hH+fSf
/0xzHfN5R0BvPzaEjWKUWJ6hQvPRKhUKtjRKwZYuwuPTGckwj6vMe3SJ/ahqG74JGlDlgW9zARob
gftAc1CPFadkWBQwbtt9ePl/dC1sNwOfVEnOWNXit8uEO7P8edUjrLRb3G3qlFm4DT+3L9Ewx+UJ
5OHmSP9mC2xXNs0+gG222/QrSwW7LnRSY4PY4YHsiphALX08cK+58QmcWgn6052ME71jdlMdJHGL
w1GmdZPn/79glHfOk7pqWA1W08H7iFzlxzWG7UlDetE68vZvA1fA/s8yVLGPx9+FkrDvkFtsgOJv
mq1GoVg5juZ6O6oHUi/SSLoqDD1w9N2Zrs08zwHmR4CyWpsI3NswiYAd4HcC3ntrPV93j/Z2j2CD
ZlXMI5VhHvOFgMKnJWpNlkWC3LFO/xTqXh14rBPyiD8GMkplCVNOFLgAUp7wtgzyiF+ckMOxTP3G
fMOBs+1WDdnd8GUwmc/fpWrebNnsLMD8NEmscdd3wAIzczCfSupdp7sEaavkF81ESLAU9Ss01m1f
qO/sgVmqv3YCY/i9xFV7t0RDJHBZzO3lkS2TCzsL4c/JAWeiH1PpLBZIUFNMx8GB5kf94EWsAhuw
Fgq0NRc+JVK0YQW0VLSeZmUxHd0ccfKioQXWT867oQWvTORbeRtTofwGAyzvhK0vzR10zYfTC1qr
a/jO0W3Os8NrSg6xcSp6R9jEmzeu2SSVH40KzaOuzgxqx1s3Oaf5hKPzQgKoLRmtdnY97oo/Vlnp
UHPbLzLVJqih/HhqOPJkWuXcdVGsSWLKiEZIcI/NrT9FJYeLKsWfRHi3GExu/Q+RGr1UZRZtuchZ
RfC1HSA1sUnrkYjMUDjQqCphkvCXMNhZhVmBHhOb+dyhbp7at39iMFSYCLpybi6QsMHQSzgZ85NG
y6wvV3JFJ99yOMPVYrrlXJ8r4qgxTYVAtvrZMMuX4CqAwG0l0c48s1FlzL8HX7BFjyf7AAclfyoS
Kdv18mc2FhfAM8/OygJ019pb3Wvwd7Xw2Jt9CM1o+0U7swnddhdSJxYKiCeEh0CJq51tu/s8/lX4
uRTXC9pqnrA7Lkt73UDts/XHvR2764+KgsgZAQrMJPwTHsVtNeYZt5r5O7dYECVnkAy3g2O9WSBP
L6h6vkhrIKZqPyvMc6xCxmW+3UotxLM1d2os089/CBbU2/PNf2xmYasCO6oqWNEX+aSi2sUH33Ef
wuLRN6r71h7e/Sm45XtdtTeQM1cBj8klE8TLzLEcekeGscJDlYlvbFl5WMWRt7bFmMVgJNuBuV2u
F1+r5VFnEuLaHnwhOGIPG2CDYho989ZxUTYb5vigniki8O5e9Td8HrtWD4boER2ILnDvMuRXT5pQ
K1lXlrFNK/4ZO5ygYvbpbzobZWzdshuNfBNwZ1YRruWDPHt99ksIavGxHknmHmsJEFBLRdu7pC+0
1H9MliNh2l6f9bMYo1b83kqNBxu4yEh8ngTgBRfs/FZxz4D9xoeBwjfVQzB2bBzT+dhiuUQOZ8xu
QK+66IbIRonw7Cpfydy/TkSMVDgw01/nNPQ1dX6+9mqw9NG1SGA4LM6gY+xLNq5dyZz6MQ1gdqb6
Ox45rkREiwtlkfAh/KCihTit4VZkhtFeNYB3QCJCKWUnGxof9b5ilz5Or+VA2kYwujETsKhkjRAe
xZIXG3y+hJeYgdY/1via7VVf5O32ORRsgoiCtZHqoxrrmjdTZHxQblMMMpo338J8ZcII0gipshTQ
/WmoOdpQgNkzdK5aSwTztX68MBzC5LLq0Imq83e3n4YYFMINhAiZbuAG8QAkWah+o0MY3VYvMUkn
K3WvQfujE8F3z9Za022gVIQxTi8OGrHvgSF1COd9fmCJQ+TdxyklLcwbHaaktUh/Vq7n+Xfh0+cm
rF259AIrwjaDSHu79OMZyyDGUq0CWdnyCtFn3oTIjt4ko8hCpO5Bzncnn5PlGi6cMRi8dh10GAVQ
igQTmUPun0TA1te7KztPX5W0uGNydAyqJPlnEfAy90BbCgROsVjG8PsgVvuT7d1gcvJ3TNqXvY8Q
VdSAeWneR/aPiyYaoZ8yoFB5RPZcSKDER+oz1Xb1lFqdNF2s7II59AnvW76Kbxc9vuEDqAkFSWhX
LpyFvrK/Y5UG9oo1F5qdqmyy6scYPeoXG9avo1RTLukP8UneF8KGq0rlREfB6LkMShcaYOy+l4TR
6uoRTO0A5sgpiBb9CwZi0bBiDZzoXK7BX+26QHtrSQPxg5Z2ELS4k814aydOMrOypaRhCYJ+du6j
kbSYpIuS/646lrejBD+gngKovF21DTtYWJFI4xO65L307DqiBrD3CsE7DsiSE0RHJET7KytVe/hX
hV9gBEVUVx339l03ktp41EQv8EOIaUgxjlgGBEbdmJ7Kjm5cfaoa3fyMf4OWvNpB71G4MacBfXpZ
ViKtlKsMfiT2FFtJJuDywU9RGGPmEDklC+SaKMQp9QidfMmG+q18xeX03kWiJkKjQ0hBnSHn8/gX
pKc6d9aXG1m/9Xi+BkJb3ZfY/n0Pj0tDVp2xHQ9rtU3N0OtVcxrtP7kQM4M3rTzw4EvkhQcqRM45
mQG+hIGwY0bMBQeRGhxcXb79H3Dd2xGKoBan7mGUKkKbP3YI1H0ydDcBujda5l4H10suJP7IDH1U
jdBwNvR5nDa/eBpUpSBtf9tPt9WKrpMVVf/FW8RJJqgDhNznfT2/aNha+mw/YrtX78QmNjaFEcEp
XpItjCy+orighelZxRG16M+vmASGyRfWZ+XCtLHyB/tyhICPX30tizZofHtxjooBXLMUWhO/Aqfq
2vXJaR+7oG9uNSeqRDfcOOZBz58ZD1kpMiqjrYi/MkWJjQMZKtH6NiIDWqfXOJOGoBI8pmXQ5oy5
wjtyPtbe+8qGTuAwYxeE8y403qmJLAugtcUm8+B2yB1KYBtc7jHf/YzJesTfRZ+E2rL/VyL8h6fc
D0X4TiNT/Qv5h8eaeBbSMbvNmst1j3NVDQWFIhdUmkPYY2vSWEvgl+GfTcMRPo4YQOWWu1lDSnVa
s2PNyT+ZfitiYKPd37vDl9BdRVAzkwJCfrHQvVzTSDvbYBXieAqBZhptY+uDu/YaZSvszzTgkRHq
a9RNgJzSje2MSCUUYH4mW2k5LPHHwor/S5Fc9hKWDjxcgzxT3u64Na968MKcwFqVZDVqHhyGH+3f
laQpJwLm3WfLKUx51+1zxvamsOPdu79+B4P54EafI0PO+3Atqnug1cMg2fUHNq06rDYC4TjNIE4i
AOOvpk1n+LYc6/fCdQYzYvM/Dh9/mAFHTsGIiLNCGyBWLmaOsXknezhPx63+y4Rqov4CUozvAfqm
8tLjqUJwM0wM9t+ff6X7mMIT9JcjxxlNZYrTeCmUS599vRYSVBMUt1QgYfQaLCqscVIM/7SlazKT
DvsZlVofv/mgz2rNVSEfWM5pGvqTtqIdKC/h9us42rHYNmdbAH/1XUY860wk+nLuL4kLVfdfQefa
pTWt8b7zWYvRw02aBfCExD+ExTwKz87M9ySaHmv6f8SMqde03zb+JoamcwbIM1Z8GwDN+m4pRSuL
5R4PN4XADmRKijK1y0ITcqUyahS3e38JS+dc/y5WcdnJtLNDWp0lS0hccxu159xHFcSv33xdIWhL
0jMf9wmrvOtVk3S0ajyDRBmYbcVJ9b96aNqXj131e8AGsGe9bF6gsJyfG6/K3gHReL0O6b0mUFef
mQAwpDOLb9sjFrNCzlslbv+xeaprcJEPsPm3V1KXOAW/f5zzT3TKbQPrEU5R2bA2956RwbchKvyL
8lzpa3cL9rgr9n6zkCD2xmWueLokzoS7RsuwaJOtvDJm+QEvkd890CmeZUTV8yYa3nP7/DJD176n
h78HOrHNRILoio8928NqQzso7mUSNCKd72CLczSfgZx0/A5dAPI9ol7R1a/6Agmis8HUyuEjft/6
5kfhEiv13DQ3KqNj3a7g/aYa3C6RNsTBBSCE3K/gYf6KMkuymGI4C147ExVSrPZhk+ZVQV+64EW0
qpuhQZLiCJnMiJWhJVeTwooGf+dKTcTiVuiH7SZG2YhjKkd2xUXqHXU4Q8EoPH1Hu56J+FrWb5oh
v/1hI+QQus/GQFFaTk6M6PY/4nnsnyA3B9NjBqCMJGcYEntvpMloJAk3M1tQKxkVQBEYCwXQDh+/
QCYnqhE0j/ItfcAx3J7aiKPiC1fcQD/gqUDBH2EIsjj5edegPeEK4XgzHycH7MLiNq6m7pnXwWQJ
mjEuag/m6fFLSEh8FSX9cqYvl2l9wrv09MwBhwlt5UCg6qz40Y8ZPC0vHyxpjNXg+7TpuTrp7hYX
cvcn5iE9E5n67cSsp0gtKYKYLnUh/NjOMuAPDI4SFCgmqDaVpzE1mF2grahjHybNfIvPTKBVsIA3
fVFGdgPOxxLzErCK5LuQezvHwkiFL4eLuPaNCMNck4t919Vo577zxd2BbRsjv3gGmg4byqG6vKP2
GC79zETejKLpL3W/wuNod4GuQPc8F5cgRpVw3aDfB+a3SPIqNII/KZ9auT9TuqxdpBnJu6p/Vfs9
+Iyc1dQPwSqvYKpZYKweiboRxgvN52wbAfVcBnGzT99yoZa4ZmfG+qll0MA5PSC8zUfsHVOoD2yi
SjOVSKVPYRsZEN+s38Ge6o1Frt7O6a7u3vjol6zmSO7ZL/XSMvBTLjo1sv2pDLH2ERcXPROrso6u
FiSlCqumThK7Nrak1ZxUChWZiwclOcvHd772ZS+pDq0EA1yt43dmoimMxlONAY2dcucnzezBSMSy
GQKXgRNcrGn9mPdGIkpypI/1YNUE3YEXR7hN3CDRFgi++2UETlZQ4foHIpAgT9eaYgpCeRy9/MN+
WGqP08/yBGMVzvNDXMpK1dk/OIfFd0XL5ZmwYRCGFZw+j4YWtwYUrErZTniFxmK6ZfJms3U75n3E
hy+OtY1s4WtjbPcZ+7lYcRP9sXY7sj5cgsr2RVTfWmnStqXuJr/0WXt6sNilmhd8pOuEkiHKPd9m
UcBMp47Xk0iW7QjJfZqtHU0H4qfe5s0Q780mcug1fdkhptn/R4TgzbYDA91J2kvbootZZcCdcuKs
ZCDEFDW/p1hVuDWtGu/lqpxJYQz3QLB+Y6eADl7Aok81S14s5Hy6UUNAzTNgs4CI2FftnWg73/fQ
GMKsHbVaeSKqAO02fDIbadBMX/P9I55jtNCDCYjKMZhyspvFJE7vcqaAehnPzkmCit2a3hCW/pE3
8SVtf78F7frSBbPswPdXXle+sV981+CU1z+eMFpHXOD//n1tNZmD8zJ/5sm+AN1XA5Nzxktq4JOn
YgWQGTCAd9wl8r5V+ALUPh40eb1l5N09JRUz71ME9+zpia9q26D4Txn+Jaw7FtwU+0DHm2RFrgLj
PvYWbsuTQhnvbyTfyCpQ0Y40+7HL2EYJZXi6yCZTVTnPPNPxgwXV6ED0Bi8slTwGn565O4vJpSKN
uSoJ2jJ8mx2ubczGfVNQr6PavD5Q8bqW+0Advlo164SBFMnJ5Dm7AXz8OFSWBAmvv3dOVsnA4wgO
dYdxeM27KOcz5c//l8DT3tjmiMfB7pIE9C0xjiwppt/rZXXlQRL2cYp87BiIwOR0cVUZT6/nSyWs
p3V5E2ctV0uNNetFec51LxQNEBw3/t1HFkX1jmz4HEGz7m2d+GCVXqZhQ+m4RHTePcgOk2KeV5PB
3VTdHbLQaX1U1p3xWDzMdGnpW9ypcW04CWyRPHYejLBIs+0lgM9mY+fki9OOcibELV27WD0TBGrq
Pop9nprvndNfpBQIMFLIx60rhaVNwNWpKcJ2sZkRL9KdjmH/bU8fIdgbE54Ob38u4QrsqWYFUxc3
0FdrqDXeb9Q/I5i9I/83lOaFYDtpBKiVODm3LU//lRxaX1CpmN0GV35Vf4Sjl2w4XkCqnRrSrjLc
NycZ3kBfOGv4V/AfHvEsJDMRimOkJTcv6Y3+fwvo7oHGNs4UaBHz9HUbFvsMe1HpWRn+IZK0N+Xx
o8xFdhWXC4s/LNRFwO3FlrCrKbIWH/UmzMTRHXCHmey19HNjZRJkbNNwT4C61CN9xEWF3u+Iyjne
9Asm2LEuZuYGeBbbY1TaBIgFTKarIjKwblvDRKLGCQOB2HukPQeQzy5+gExbprrJktJ3k16H5iE3
y4JUWM1V4vpzsz2yy/AlOMtr0oI34vKYPyb0xgeuR2RHCpkBALPQ0zZ5Ev3/WRIi6D/WyCD7/x84
S7rCFClbKxso0q5ABVumpsJD9BsdZNPS0qLJWc0JW2JsEOl6ytID0TqjH75Eyc4s9ZbmYaD9q9U7
rgORdPWn6peBEcRdgJriKm7Nxh28bhsUn8eQT17CLcoirAz6OvmpoVKe878QjjkuQiZD/5IpPo9j
wUWJXGsIzmwUJ5SSovEs6OVKgCNgkHblzPuyVrUGgebdWTSETqIgOds7TQ9P+1F3d6UuHx+L5yMF
v/5sDh84UPwYRNUyiENLL86KtjnCCb5+1HDaMHeGEq6psuCbp+7mlBR1/pdQy4jaGGq8Gy4Au1ai
haMzAFC2VrYhoNbD3Ffnp9BNKy77yjajYKK/D8dhi12Br7cXChr52bIW87ZQ5rLR/kKy08D2G8Ye
2o4lJ5sELQ8yAv54fzxS6NDnLMoHW5LoKHk/TQHofoS4GcU5T0lpikAdaBxXb0W5U37K89Sz4zXZ
xfIz5tb7Zo06dTS1aBkBNP/mZt9T8+b64coAbqEvFKLxLA8se11pWkbpwiJU/eRJxXbCD7kztLZi
61Fln7bZtAuGD9tFnjBn0ppgsYhFjDrTAw5+KiLm/YHYBc839uJyPM+JvusvJP2stP6X3qO+g5e7
+10xaOO20cVpqFcJ5nbJ+M+HTOjo2k6z3ei5Ya0POabKloMa88GLDpZu78UVTjcIl4y5CT9orjB5
xJRiUO+FXh4Q4KpVnYPbE+pjGXt6Ga0dhMm7/dmbg8zdahadhiiRuQd/nJGRvWQrm9bSvYkrp8a6
BNfzgEtTAI1usi0eMXQBrdNhQR2yH/t9sfqNHyTCkWdpTfLrAhXT0Tdp+r3hbW2lRhaXseaIbOYx
weCrIXZtUClh7stazk8k3uMfn5fP7rvruEbaUjPPiZWINtg7SPhtG/SVBoG7hbWHnn9RG/xLlFB/
9o+N5vn73Bg97Tly9B6ro5+FglpjK97yI+iqqPa/oC8tLXTl3jmJlu9DsdEBzTJ7ux9DGcgCcUoB
KD67/gWzJSLF26OYvhIWGdHP5nPVeks3MVpD+etsjWcT6uaafamAfVyeby6BjAeXUPLB2SyuTE5v
Zqmaeyk6o94rMTbrNmKxeBeEL17nKIjamFVR83QYZHx49agN98QNWl47H6hTQrVbWboXG/ZCozvs
KfXIwqPX3ULzhkUD1ZABOH0BUYDlt0/J0Tix0lK9bxc3mttcNDoN1vG93rE9ERupboOnVLKVsaNW
dHif8Po+sGX/YilADxb+OUNRazmnra26JEm9OYGn7FZubQM5CEUVejYtjoWJV24djVOV/U35pn8f
fgo7lr2QZHHuA5wquWrMOeaEyceaTXDSlF2RLFkeGwPW9fj8zP/1VvkaLru2kUIr0eG7BQhSSnW7
qHJL+70nm6BupFFx8eLreAyQTVOdwkzil3u7GjC5dcWyxXo/kaMLj1glXWO2+J8+sieHD3XEvogJ
Ob172NOWbzYLkeFfdDTFsJquQ3ojtRv8mN0H+RfoZUifIr1nPOd2sXynVXWlvCg2lvmD+5GkEJO2
EUlJamKZbmDLUvzxA/fhypV1/Og+jagk3sY7Do0I7Jr8n4Utrp/Lzrt1ZQJIgSg2S/C3wvrTJMWa
D7YTavzquDMpnWiBK3U0s+hu8NDMW5qZfvowehMcE30R6H6HrKfs70dHNy0HJzH5E28r+7wMqS13
9Zkna3EXKZeXqluf8zh8/yJegKe8BxHdRxgSftSkeQ958Wm5LyZCKPjIhXTJDk1fx4VvizGyFjjF
7bk+U5rMPW96KZLLNvFQ5Hzdk+d/aZ8RdGzn35R5lMEhXGjwSe94HosbHKoF1HzGQG2O9aLW2fC6
s/i5y4nB4R8+2XdY9uZnN0H21D7VE553XlAUTrxNnnmLqCMSN39SvYbaY6yRSe84ZN32edq2ox18
P2m6JXo1hLNQGAj72pz0OoxT88NturzZiQqmV60SasJXQ9NMXZ8C8Z8GLnIa4QhhOnWqPRKP0Zx+
rZ17XeZ6mUHMSnhGpye+yphdZtMtBGM1buYc4+aU7tDmo5H4dVHuhGn4FMl3H1Kb33UL28mZsuot
ROmtstmVz7QjRMGXGVMlpJdyEdWtxy05g5PXZ4ToguZrRZiODYr8lIoFIy3SKFQ6KGkc6DJ6injs
KwucuKTqTp4F/ZYPNok8/TgrrtNaSNQX7NBDl9ch460Qj8R+xCe6AcxzufLCrJ5x6zFLY9J4bTlG
9xqIn/WRZcSeUGWdh0jxrwyE6f5EQ6uEyRAs8gj0INlFiHTHQcHOm51LhrrpCqh7qXozbbhRZhPr
looN8KAA+19m8S/ZW5KqOqKU6E1HNkZ8dVHq3NgAM56089frM7SzCiep9x/hJw9yA3gjptXi8GYj
3Su7WK3hSb47jATuuOw9xFik8mWHR76AIt8ts1yStvGctX31MNdl83GsPD/7XvI3P4v8w9Ms7pqj
q/aFnR2Yy144AcMOQ34gtSIRxepCVgGDOlk2+bMYjETpP/0c2Ahfmc1HZ/ex+mrw7hvYwoGZUd9V
r+2wwQ3OxMx6GCVNsDzP6RW6Uym3n4KiNZNww/QI/7ZAJKjtAlmquB3EHhPOV6xL01rU93vnFEz+
j0/PXbbGJhksCF2NQhLCc2AXhxVebiF/YQpgrMFvUF28FEbeO1UeAw4QQAisIqAMh3VKkNWDugIH
JOHea9KJexkovw6reVNuH1QWPWJJdTNTIrwNnbLW2y5EZFRPt/HzXQIoPO/FlMyU/fHbmYXVxtIn
xYRGHgzpAOowi891bTQFV8/6oi4OUooH43h6tvQexAk+JmAwau+ibYshIKEuda/OFd/De9ww8/MZ
wFq12jdPmljuDPHuxREplrw9HBtjmHC3wXqwdlHr39AoPkYgMSl69XmhozlXWRxiZ3Xtky2HCdvM
YmqtUmfDlZEVT1MmIrRPFX6qbj10JsyywjVUIcvz36Ake4tIPlTtEDcumdGNtPj6TQ1luvhl0WXr
jmXON344SSOIOoDt0uxlLfcyxyCciBsLJnr8vMP5QaND/hAeaFEbLHZgmZO1ve7VNhooCgsXdMcs
5nY6kmoauZB0ZYbNiqWPcrRN/b2MVf/p/NvHf539eaUwoa48Xu+QGetkYaPKcWIFNuNKmZ/jpucR
WInp4cRjCqYLVF9hNP06X29OCksbvTGM8/Bx+WN+cJOXL+IKFUXiM9HMKex2bVwOSjZqE5vO4WbA
PCRfDp0N0C/rI4b05pKt1iy6UdQiCJViOGakIevgusniurbm+CKAPh9S7o3vzElCEzqsj+Y5O7Ei
FnYnbAK04DYrKxTRF32QdloNYlcYiweDfIsoDaTrMDWalm1n3vX2knkTAVGE91kY5rE17Y8LRIAE
DHWqJPbMsL+urUCTDBYOC7bRr6qlUmn6B/wkJcfIGfesqIztszqX0PGMg93PAobBMSLZlIyyOlAz
8damzswXm8K6JwMbfhh6DCjmmHJrU4IgWFJIx1x8O6bbpKDR+7Uzfzq1iGEGp9z1dRNUEbVY+eIn
qT5X9RagXZSA9XjhwrcV9CKQya0mubx2nTI4T2IwRG3UcYBpwfLBqeFQCxuwLGxG4/eBaLntN8FI
q+dE7I5lfecWVhnq4NV6ROOUqUzL1PhPnO7RvDPVkF1H1380CmwJj2zpU6z5Xd9CZHhPLB5N8AKw
p2lCGaw2iKBwkWCdlV03SkPtbA3dXQNS7oRauBrxaBN5mAmI/AtHJMI/PR92XGexdX1hQYNUQURE
dvnyT1y59Zd06jqRwn+xQcCAVSyOKIojA2X/WrWHjFl5mqaY8Ho+ivV0leMrRO7PXReEu01gZUKz
3ryDjCsD+pyep7Ntpi1umZLoN8/DhoiFRl7g9jSN+ijXMwrio+PQIMhqf0U1LGD3G40KqdTFLmuv
VaNjlUdh0XST9RTM/fV4FKN29CnOy4FjGA3H41I579MUSTGvJwoWRx0kCtzFhMqHr0UjUZwyMqVI
TqJ6R73JeK7W0XqiJZxU1gemrUe3sarxsQG2oVoJJMPSr8W2plnDj/+/Dhl8+QEnptKmDlXWwqHu
ijZ8HVX96WzI+m6536xXkRTcRSDmCfFRGI94hIo1sS2AJcdgIDLvzXYPIvCjM0Zs3pelQHvQUmv1
mPEAwHOVAUpm/mhcvt1JjN7ActCmveWsp2+8WcX5pGfCPbCsZDmwX9ywAZ3m3toOfcAed6XQj1tw
wmF0k9N8HKs3jW/NZjW0tVjsnmHAD3zfPXC3lqNMU7SJ8IIjog+Syx/ThBG7uEf8xtF8uVLlbpO4
IkNau1bNnhIvucADWAvSGod+8VzPHjso++ZQ39TFarREMgM2D0g3X30c0xFUSGnfzq0LA6mS48Qu
lDCUVvR4NbB6d11/m61wJ3sGtWuMo9CQ2+SDW6EK8C+B46AHZR4Ib4tlU7jCeM4RhVJ0x+zmGSrx
6oR8s2nH8RBtG7X2FXzBKmuxrbUp409iKAyq7P9mUJjOtZiYmbLDcCKAAtRcBeAbaCTIgB5lBi2L
6iLNL+PVsI7CQyNiIYmKijWjHF+DpC4mG+JYB/fHNYAdVwQUXhbHJt20vx2fVtexY2FQvJ8cTjb0
q70y1bDXSB5SkjlL2ovIxUXNOiTRkcIIQ0qr0TX3AedNbWqo8gCdt7jNNiBp/8m40F4+uFRnrcHy
PoSe9ZZ87yfOV4zLD1rCQw43IoE8Q2GVbN8vc2oCpeNYE1q8cxezBhVcy2cXe9AbUQC3UNzVywBO
MBZ/qTYIQodRFpgE25+Ynv9JpAsGB3NqSDbOJWLFezSSqIJHm8jDXjXiqUvZRQFX+bNdMUCZGvgJ
O9Ko/j9d1YMjyOwf+U41Xv3+9ouu9AetYF2Alh8ioWwBLngqOQ1YmgWOCXIR6NAGtUzu+Df3hSJI
VPXFtBnudz/UVaRQv7ZvdhEIFpflfBamTGOzmF9DLwdsAAzKNujMfwpH73sNODcjkGme5340zs8A
KNjGNiF6k6fD/gd/RC819f9kHVgwPSBY0aD1Cp3InN8YhT/Q1OQeObuTgJBkrNg5MR4YSRIyJtz1
9jLGmX802xiXoGhb5GY7B88tlaM2Q2+B+nx69nnh2+LUHSue/Ezy+CKap5JC7yJe0JUgY3vQgz0e
W2BpB04DXJ0yvpszE/vtoVvNR3z1fbrsGo6F61QvVNgPo5WtH4nQob0VbZR38z5KO8lZA8ky79h7
QoR6qPyaseAm1NOEuLO/KnEzObg+iSQ7nSJPKxnJHydwGlsSLYZvVvRqaHpQG2wSrjrdf+Jin+mh
NjbZR14ekneqMlSlqN91SUQs29k2Whw7VsDhDLSI+by02IUyZh0nUot+Ke+YOZAvhmNq9HNFxu1j
JfC/uSJfYC2HqtHoVfSRQHJawRasjhKeNPhdKyl4E+o9gyQlFiwi0QAK3ECRrlHBya28k9ApjBQy
RkUj/y5/sZTu3QF5PoHcBsf59EqDwzwzdG7I8UMAiUle8heiPiJN8jtW3AI2kNvPNwfdIWnNf9uJ
zZiEBH9H4ZizIKKDS9k9JSFg0N3JEJ8KCnazMEuI5LzMUq4iJ2BPPBzPqrBSXiHlETnNnZo+T0W7
zaxiGhycJSJVvZy8Fz93djmutUaeSzeKqhlPiMEnzCO34MpK4DIpXbreV84N70vKJAwO8t/1Kw7Y
Y51sMbabkEud9CrE/3OlK5j9Pkfmr+wm8Chlesfth4/o7j4owAK7qPefFUxZk2keaWAVtSaEPpo0
2PstKfQE9WCw62FetyhmSM1MWL/dXb/kRQnPnXav7kaYTKfcgIfxQfGuGa1olPys9xanmzri8+WB
vm+QrHU5ZBZ8jMuXvTk2e6fl/q6aMtSlcqKFeW+5LIIu1iMJZycyIdeGnmwFRMTaEhKwjm+89X9g
1uyw7nlOW0n5V+M5pduCaHsHGt0r+vo1c3jkRpM0tySB50bEw91sXRZc5cOfL8uX15vv4teSvLRB
lw1a6UJ+wXdVu0Iu7v8TYzhNd/PpX97OuxOQeJB4vblWWtgMrCRcK+6Ni4uPawzlKjI7F0QoJPL3
SYJSW228PBEL5alvtg1PILhMXKnVY5MPfip/jAigyJ5k6e/pp2qOn3MTfH5ut9R436u3wt+LKyQN
UithfN6dr2oR9gqUIHsSx/gv7IOGJeQSC37Au3UzAF8zi5FrQwij04Xw82N2lkdGcn7zYUa/5Wut
m5m2W2AQ8HSS1zPoCIAqoRdSQxyZptM9nQaaRM4hMWq3mXbZbS9faBW++ZwGUI8H5vwD+2LOcGca
RrV/lJbmm+yV567pmfO3gE9rr0OxIuh98obeChZ5FlGbEGzpdH/+dqFzIRBw6jYLOhGU6kj2lR4w
GR5g9FK6+pmcRYiKmr7ZcN3KuUTIu8nodUJT7AsLKxMNAvoZ2JrTvC+pCdnoSqeFnO9lTZ/xQ7/R
yurE/AIA1Pgj4wRsEuhtPIUKb24k+7Grjh+H0ideqbxYMkpFBUy72sxHddEXcvYKhUjHrKhhHB9L
cXzpuc1bcz+zGX/PCQZpGlHAJz7kPsDWpbZad1I6t9/aAyU6OIYndoqmCk2QY4fHQzw6nih8P8OS
w7Vf4yBeRG3gQ2OAAGmpErOp1bu3LhDiZsY+UXNoJ2UsRxXeHiUdQLJOWyCvmy8ZG2fMvs7sB/LC
bROvKEDIELHl2HP1iD5cu3F+HOJCmcFzUtEoPVTwA3NZloJRgN28TX/pgmReMK4U7BdfkMeVVQSZ
C9Ox/cKjN4wwz9v+p35oZXcPdS+4amcz07QPvX1eq4W6rlxmugaltswR8EeTbp19PhnAHgc1MT6u
Ly7LDRH4HmMopYFJZgW2w9UHrQOvCkO0ufgWhLFukGX0ruJxBQrV9IXQKJYo48fKAd57gQnDFFjm
J/cVvaoTgiRPVhZ70ehAh6CJPdaBa/lKhAZ+z+FBh5EijIkhv97XRlnAXn9QxHAaE4SADKPKJL4f
PsKSoUvv+Yh1m1Aqp5Z9mX0+igHgIKA/ghzmLtfm+lVOU+nnB1mkdetGUzIApK2UWZ/SwCtYVtrS
aELZd3ZoKKrwVL7G3ZTTU/xhqNEBNxsg8H+C69kudolsYVnmPDmbxrYt/+ez2szn2ZClLujwo0Iv
szvZqYTdtaoqlgv5mMLg+K+t1INhb7LULhq+WZqnDeyYZWLLF2JK2pqR0qYLCMkxFZDuNXaKL6Zh
qLMESsx2XBnxvX46+KxfJFZL+8eYf0QoJGCNVeNOWnGpnAb2BpTOnoqPmYST4rCKK8BG9+UxNZRp
8WiMR35TwzTxqNpXVaWEeRRsDgsZGq9LWe0s6RTINXT6mO6tjMpKZUPS0IK6Fdzp3Xr/4jJet7Tt
8cthyEeDtnjqONRKX/wJpGD1n16I/4S84JYOSXp4sPFphXWbukXHDjl9t8sMcxhUSZ7V+8810zZ6
ATssPsDpLX0uuiOQLOvzLgHZAtdG5aNVE/698sTd6+jlQmDniNu4opkgq1+pJOBwc6ila1njhFLp
MvjwLg24UDesJB/nFUqAwNtdFch9M58Sah707ueqdQ/bCKA0oavi4/wsJhwabKaArQxtadsZXXKb
k/oxjQXn4EX0Yi+/jPyh/S9l4qcWq9WoFio1YAx+puGGqF/TV4iEtlFVnulvbUEPrpjkmXTcPoAu
3gPeyE60a62z1P9bYNxiGWm4XXUeU46GaKmo7A7UP/ywX9dW13k7lBdmJrL5AegDcJi+yue7E8yR
5IQX9mUGQVaI1yyM+SsKxIq+H+7XczaxWsTvFOn2BZOpIFqUxdG3Xh2xDbs8PwVkHPVYcWYztXrV
0xrf4NTmSznOiC6H/9H0TV8g6EyxJP0hJNN+1tA9f2gk3CALfsjdxumVlE8tWbLNUNxBDJQkaazj
WUEVafy9xyeT92nIS243wrm4ntfnT/UCH1Dkg8MMHHHoRwLIF37U1kA6m2NNOjVjj59mUqoPjsyh
cF5KERfK3VWUALATb7KWrgcsAvYDu1eGRUDHyPL1VGT64aUdlvSch8Y+F67KiDpWirXWs4mTxUt0
LW0KfRGga8DyyQ53yaEGjY1Mww8Trrjkkv8ExQhk15Oop1nwXFvyIY767BJn6IJc7IbzTcPhkPe0
nChIApJxK94ONm8nYZgkJ0Hu+c6OICk3/HmuCzfPO7Glc2pMLanka+/pnCsC3quJpFZKF7N861+h
cXBmVskN1PNLKowOhBs9f8JlVQIm7mDQw32JgQmH9brBWQyHy4q/xYCkJ+7XcXQHMVbB7jRlsDE8
9uU/88EPpLD7/a6xKCSOG40QksETWmaq46Td98AEgnZ8qUAxCghXmp/lBXCMzQwwcZB5PhPc8lZB
NdDYsRR/ByK0tHJTjUuiGtLiXXajfZ8DqyULzCWnS68UwzcDC8o14uzzyNFMyl3YoIacDAU99Nrw
hNaXIJ3ZXa0TGWJqCUZtNmBNmhV9gSW346lUeaUtKK8oZqAkl24Y/5jdoMfyc4zIGCzadj/xZ3vs
I0IaFXlXTCMPFhzrOFEIwsflojrB0fqMTCQWUCXmWVSPGx82BgpmM2Yp4ABOZk0gNG5U3UxthEBU
65InJamJh1QUV30nUqJlQp9lOfy7GHO8lSgaUCgQDyJGJE4wgYWWVz6NBx4+7+xGjUDJweiziC2w
W1aDxEmjq0PIT0tvcTrX7sZHaJ5WBKOAK/nDQzCWIYf45rW1QUhhD+TCbOqJ/YDz7mf1G7a4YOf0
VfSkmCzeDHcOQNIA1HjOi857+h/eIEA5zJ+wsb5jAGOC5BaSeyRY2aSrWJwDwQHLcMNIBiW54yD2
USLS9yW+fDspuVFEi3/yBvBIro8HFS48UOnLIbrCRnxQ6R/plbYizWL+oX7V+jsxJGzFDOce4fFY
wgPxdkwTkdtp5tEx4WKK96eTFjDe/7KxUq+sN0WSmwotKI7qaktXdiPbvB5Zc3HnhZrwARcHgZz6
V5QdPIPfKB3aA5UM31J+i9FW6UsiqJtlURmw15prSVNqk46ULthmheVyPC8kUNc0fi/F9DSgzLKL
kjP2N35zetJKltgMFKZga6K770dHMqPjhnYWrvg/z3g261MoJF5s4XeOj6qlyR05KW2yfEqPakbw
iU2HKr2/LfbBXGpq7ZvQ1jVuSB6pZIXStosCIDVkLdcynNsM91YTGtW68nCPpjNInMTKYp1o7Rqu
a1Z/QRS41xhzzKGDeSqP9Fjk5/KkZQqeAWJgO2E9gcT+9zE0phsJylnNBKUBP9S28EqmsKofVrlg
w3S+8q422ThR5tg3DbD81BlO1wGz6dFqhjBAFIT5uaWF7tXReGmE/8pDKiiur+CQB+J6jRF7Je14
3NU6GGdyKeXcbQZzxHFefFVulXCbAPyAlqbY17J7CDQ1ZAiObaaaLqqdhUOEMDe2AiTRjXyzSytP
oYTAzuj5xn6U+NhTGTCgGu3Y6YHKmGh+19/Bot3bbFAFqQY2MSfqcZ4n+/McKutdhYLlXXZUaQAW
buCXsXFoZkwlWPROAhKEEeSqzRE4wqX7zCXtZDQfiqaQ/8L8PVY5Fhrxwaikq2YtyhZMpoSdEIAm
qjl/+jVjNVMtYg2DtYfhr0ryJBViR+ObdHcVmJ2JRO5eqiay0Sb9ZyPopw24PU/stCnOnav7TPnC
oL8Y25XcTRpRxVCu2eVfpt3GFZ2L5086W10hPh+Xc5QOikfAEGG/Feh0RiL0Fzq+MxMbzmeOt0bU
e/Fr3SLUfHxHI2TlDxZBmzcLFjcqRBzofSf7HlU/KYiZDixqLreFgrCrSdIpqOYb4YfI+rcG1C84
r29n0QJVvu0Yln1s60z5c0ZiVbExLqaXdmay8IsSk73s7RyjlQpldpKmEX72V/Rz0k1NS5RMSwVD
R/3F4FMR8PQSASENTtQL6RhM+uTRRfzgnqKvWIrFiHfkXWdz/AfT130+VhJJQQzM8S1QL0+N/NBK
pVL1t7vTGx1GpCCJy1eoBecV/hPoO199KbRYlfqZjOepI5f2RxZBAgLqdI7BUNVpsIQKxqmqmhW1
xw128weqf6ScHOCoobUVSmwnLZEKhNmAKAZOE8Ji2iDzwVVyXU/HJIUdtkCzw87F9Ix98Im5nsCr
t+1rcZwScPwaSdxvvh3iIQh2vDT/ZExgB8HjgieRxq3AkJQPWcGhsWG7FyAyt9nRyzKz3QM88Tqm
e7JqBufgTX82DJMHftdM0cugOoZJMw4x1XcJaQG9wHm5CupXabzgVWgGYDKjHUabF7H7WUbNOlzs
KkQmUpZ5m8kenOrW5pEPl3st5/uo+jBRMw8eCEzKodX5Ftlch6KpawIQqci81eWkA9Ve4MwfWPVl
XeFqcux73Pm5DFmeUpAyO7zlpXPYBoWup9ufqNIwJNSxCtIG62XIW/PWAXhhsqoS7HKiRIqrSLC2
M/MnYD90hw65l38v46w+p5KuGT4Yk8qqoFRtibKPF3Bz+JVTUxL6e6Vr6+NsVpuuyva9P8Apyivk
ZKxgZWfyeKl3zYTwslQM99u7sQEY1mIJ2wLlBwzxyZ5eZkhEckCQJozLvXPOlHIyNWUaIpo4VnQn
nhNbq8A0BWkjg3jMbhyJmib673R08bak+gGTfI3Z8prmmcQfruxZAZNgseYlLsM1lb6vDFKCKM0k
vPtqvVvAaWWzKuJzQIoOkXsCSBpTA8q3Y3tt8uv+eAqGAK0zHoBRhZVfL9kgcGCeTsJSgqDSI8nF
MToMNwg05i7p1sEMf9DkDP9R+ipGF0ztCAoXUoQRfpYcl8I9hCYJ1vnjnDXHfBXTjBJlkN+uxHQ/
iGeAdo/aVuuNvJm4CbQo+UWiLMufI8fwZLKGZl32SZC/jbELMUXIKojoM6bo3Bdeur+wG7NhyGJU
02rjVWCr/HmL7fDc39f+yzNdq5QQ6i7pI1pCvXg0HvM0b7qHQ51K6TsY2Wpag582KFEWAl+JPAhr
lGUFudWryKitdNlHy46O9F9MkA5HjtgTDKdIl11zRkOtF620TY1sSThZ4QHUvXCvOAYnYGgd4dLP
EM3j+H2Uvq83TMLDKFcNWVgXplYsYBL3SFCTbtlvxXA6Wf3/rmK7y8UzI2x6Qwd65c2htrECSCc6
SSA9Lq4Un+skW17wgDUdwROra0GetRzPvmunVWlR0mQJSep0bnbQVjj64KOuUKp7SqPl+8A+60OI
Y/Pj0mmpeawlBMmSJuqjEWyPHpPKQtM+f5aTuFVy4qt4ZvNRKqBiQbBhtAE8B8xffQsmXcXILG3v
yWarTQ1oVk2kA9kohYKriPLN7MkvLjHwCeUL61abC+664ri0aiMKSqYq9IbvxRbY1+SosjNfo2vc
siOQI0YZM6vmPZIYV2pAuc1gxnfiyFei3BReSjtBpMQv4c4q19KapLXii+AmvV1St5gzo8e+DsxB
Lm7r6udO45al5yobt4L8qVoeVCsTLkDfmHkEE0tSxdIA/Ky18IMfcw5mjuS7RTrbgXHYU/hpFo7V
gom/aNBov8y0TN+ALs9sjtpyHOeOmxEAju14D24ke4w1AgOR0tAcNDWMCJHqW3yPTXeGuZX7ORya
9V5rL+WaPrqLZn9v388UKAiWEi4bKrJ4aEXgGbrasN1rxiHtUKQ9GbAMI0p/ex4qxTeR4ri4j708
6yKkwQOXfB0LwxTG16gG59TgBX3zSoWxkOwLvqBhiVyWiqmsSz2M0nKfPW7n3j0LlULKTjq9q61b
hsn/VIk0Ectf0kpyFO50ENvTPqGp5caqeKEomnLzgVhjVS7lGzZlZUwA7jFS7UY0Jhw5g+mXV8ST
K/zRMxpElYYMf4EsXN30tRqYSRo7S3c9iDyPnDr5ZV9gnOSkKNFGcC9SIX3r/kJ+AgHidf51UeGw
JAUhhgwB6WctOPLYsuFIaRc7YeL1nX+L0I5Np557KBI2FgcLP+mVwDiVo9eaFK9QkxiA5eyCzFWl
CzlLv4PPzIIOnjxeEyuygiAQAJYxzeNaqcA5wYPFp0SVJioaNDgG102wN4h017wfGhrQHIkg/YXY
UuN3axkxGkG2EQ84OfCSpS+GtD5sTXtmbFRWrldKkbzEUccrV23BgbfoNIVXObhmPiCTy2bupSC1
zQV7XopuAbJuYbdhVh1Hw6n32YuZwavvsb6ebcxmweNpCR3wxfFL3A0X7/JvRfA2fVKy5ChS0DBP
EF1qt8DsHVk7JMaqoeEZLJsVpjJ+d7CFuEvQ0JHIOaV1Ng6oPxBfzHALRDCS3sFRFdUPSg5of3+I
POjWARVprweENdTCi57kZOiFeAv5vkjoc3PKJsJ149Frt+M+x70yUUBFvsYZkQTLpvQt8X6Recfs
pitEDIeNZRNe7XkJu3hITcewOu56ILHuQO0OGYZYeveyRyTBFi9froPFcqZy2PUUlSdoGKRVN71n
Nb3XHQrbNE/wVQCjRW/IUQfKRHkgrGm1zUu8MskRHrVEWahpWWjs6qXmcEA5zLCnYtYj5TC5x4aH
JzcIvkq9NBPHuda+oQHf9RMVhjzINqhDJJzcxh9diKfeGXY0J3Osm90/V9ISLdztKhGzV3eizSbc
KiYPvdTYKJr2G2b+xnJNSSq4LJrgmhf+RJ/BeTBm59bh1lLeiZRLEodxNbXpBGzShp1exU704w+e
kLToBuQph5acTtKBBB0k1wGyTKO2xrxJmVHICHFOQbA0/YDXgATZzpP98zitOrGoUa3cD9cZjy/C
UjyJJ/ejpEnrwtE8Mxrbqfaj41rb8xuTITTSAxvsQjPQvYdJvETrtiEr5z05Nj/EjbpiYfbZmx/v
3LZ6/df3Fixs5fv5eZ6T3ePJHsH3UpkppioOKHOpWjhPm/W8wisp4tSLHDy0h2eZlXcVRn75UBQ5
XgSksNx7gh9u8m6JN9K+W8AAzHHNtlZfEivMxn6eKNkl9UyXvdg127PglEMszBNVV1ASQlL+ZY2N
Z0Xa5pLVsRIdXviL1M2+GpfDPRpv8m3D5iqlhhSuFg6/f8ZOr66dShsNuoXFE7iKjGHHzrj9g247
VTzayuKNEO/DE6d0aZYxk+IkGE9wLnUrcUbVmPXKNznOmln0lrUAq4DX2etVi6iBLPYDI8oavt8m
epL/m+l2P6TC/zjDgcEUqLtNOvusuW5w0B40mHcXKooVaokZpRe8vXDacBl+ORp21y2nbYN/iz6z
Y3SAhufkNM/UmvQi3lfrkQ49de1ZO0VnxuZ7/TLyja87lGgpjQP4n/Z+MruPj2OOmcCxQ/YwqJFi
vqW0wHlPmrTsq13SbU3FDhZ0rhxeI+ovNr+GwnblVGI+vrUAOUpi23uKUQQwdi7id6if4vSTjxeH
Srpz8Y6PhG71f8cz5sv6YHZ+cXFCoxBPd5vHrnBzG+Nw/0Y2x7tsD0iKpQns3qYeMq1k831iTz32
Dm5gQDDnWOzoe47Mn4TO3g36ypYa2eV+OMdZub3E7XbsGUmFMFS91WgojeVpz5TkkC5Mg02phffY
uxFLkYO2q8JSHD3Re2Ace9UE+D5THjK3HCoojEQcGgMBLgYBvswMk9WdhE+red9m2v2cSEGjiDBq
g1S3Tf7yPY7xD8fA+OsRHdWSX1wyhefmt66ck5sNj+l920hV1TfKX1THJJX9Dg+4LmsBI4jkse6g
NZkN7LdnUtpuBOZdYYIiMTEdm2TsCBcDVxM370HEmiU2waecQ4g2l8nhUhudGq/qHmpWbCnGZRDS
vHHgop6x4WkKFlHiTmXkeCXhqNm1w2PXz4vxWh2hOYAJgmnS8G7d8W/f2wnVF6j8PivqBbZPUF3I
XUDHt/R0NTf/31+C+zRIe3CtG/nZY8yarEREKGHWaMHlU5XTfPdr4ODCnbRNdXQdUkV1XxLoAPqq
8S0BipOst1bDw9YSHV/DiSWdXkkMR/JXq4V9kv6qxLpYI+PwwkTnob/+y9HHSacltTbB/U0l2WFo
WzIvIC0qiUyjsWK9hTOXW2J6BRuwqtqM1m8atOlIJ6V1SfZWsdFIFTto6R9jpwjMdffmx532ZUzL
ifveqH1HwjNARp4jNTAOQOW2iA2j8d9a6tA5ZxsL1L3WueYCl2J2anMxOtY+Zj297UzDDwMibowv
CLtU+BFiIONn8e9qPPL0PSu3XFlW7Y0GFxatQJd4ZTEkCBs5vT8dnDpJGdFpm3LACLUzdD3Jo0kU
n8VAQL1IDJa8xkYq5ykbLtdqOZlTxzDhTcQikRUf8siyu/K4oGdwK09qODaz3GcxRcBV/LR+dna6
ZcFY0Nr0zEjJwBVW637eDFxN/tv/V4KpRt7T33C8kdJh1v7mzZ4S/03xz1o7mGqPPN23CqyD0Tuu
gqCgE/44jRsZS4MIuyP02VBSiyWQ7MUAqfZbCl+4g/pUPJccTrMj4HcfHoDyqb4/j3BTFb+hrEYM
zh9rdrZhHR5R/xOaBjlzZtYOIjsHf9WAWBgjI4GO4FsiVqn4BLbwKATFwoTxU8h1brMFbch1nHKo
ipatnWiO50684BAtLqHsneocK7mFYqcHcc/zR/BTWRaxQT73lk/aKfCeu+gbywU6gwAnU03LS+k4
OXv72uM2PyTw96P9FiJvZ2YikORRIL1yuEaGPfX2CCKkJUhKYSokj0e30Ms33ttEl9DgggJ4Crca
bSJykJBT68IGjR/vjxFQi50benrpESRosnVGzNb1sO2FnjhalL8sVIL9JAustvOMi0Oh/E4zkemL
LkNUN1VHZzYTvhPPzkcfhrT/z1h3X+yvTgnXfONJkTpYalkm92tNUrRV3MKriLrQzeWW3Aoy50jc
72ex8hQ4jQQZiat8YsOo6J76oybEu2GDFWB8eRaWbL7xFXbQ5rmi+r4rSrjwX2UPfEJi4lk3RgYL
xNYs0HMZLQsE+ierSuNosB98UZl7e8WfOIZi73qQEClwUqjVynG/tSAhfGCcsXlgISOOXrJAmdXE
n6/Tva8D1ZuIJBx/2XDOb13caNHW2fd3xBq7iY9fKJ35LfBDp+PXPm4keT0LoUu9p4UrSKZflUK5
bFvk4jFgWVUt/xuKuZE8bOfpe8v5t0nF5c9GJqXVTzni/ypuav5JEHLMvBpLUhmfgy4AY9ZyjO9s
ffgGMtGyCYhTQnE/370ng5CwAtUgqFvwkPMAYMSnarjKBfguYAj3Ij8LyB2OdfvNrpCEMGOE7lc3
TCDcoWaZs7diiyaEQ3NCe+cKmmwz3W3/Z8WkZssIMKJW/Gg/TL42XbyFZrUHiqUzBr1Sd1mi/Z48
n8p3VT7neYLDcOx58jHk/2V5btVo9bRHx4eYtwV+HcTJWqhFKnlOCwm1A+f4ZH+Hia/OIaFOQrk/
o1SksaEyDCUFtqF/00/jl8jjlw3hK6lABjCLF9eMpo1gP66ch8gse38pfoSF2vbDxNwtpumNTDwR
FsjZmN85QPYv/qV63QdLEroVfTTMXcA1DuAwc1PopK4QAKbPkNm74KWZAjAKsAB6fmxy/1DWKb7Z
xEZ6ZUyvR1evnfChJy9jjIMCWWjNn6gqS/XBJZ0hJb1yaFS8i+MeuqGO/r46tPRNXYN34U32HBhi
n9TFz0fJiRsimjfkoyiUCwjevrV4HMC1W9LbBHa41H7XFsp89z5vdczW1ZguhcufqCOrfF2ZPFZA
l3l0ZhlIfaS5KNluUhuCzCzqWsIBL2fgY1dvN0GTPPgh1KdvxOiXSMRrvLbIlhiKXlcGP20gii2v
nQt/lxgkIPQ1V5uXUjWLvsV+38tkzzXGf7kCpM1toqQfjIdE7/chHmrrGeWQ8d8M1K4gWGAognsc
Bo4mWfBiDtZbunEO9ikkJGzdOIepSXLPnwpAf2Uvtm4Ajk9xioCUfdtcVSBZGBpNpPtynnBAUOqR
HiHzX3wjQLYtHn7Kr8fmi9ptT6XCSEGIgiLmYdUE5AS24SDzSsnQr/Rv7W+/R6DaCSA9JOxvfDmy
Bt2B8GurjtOlX/KzWZuZNtjLDqlo3JuHbD5WDyWvo6YWC3RU+GmVUkyNJcBYV96jlou7Cb/5VvC4
W77Y26Mj0VrcC4h7hGjnW5Qlb2B4RNoLEd0u3nc3zY7H5WBOLDlgbeYSNAiSOFp4z5JU7miVgFRx
X7f0mKRS6XWO8oG2MkK7cWkx9VXJz0U3/RqhosjNSFqZv/eHNXZTlAtMONpuRI9q4cwVee5OwIg6
AuMB82fj0jSiKg+UuiKUa/lWEG/ZcszeVnkC3MKkRUmoM2v6cudrNDopRwmsp/dpQUsIzcQsxGGD
dn5vTan2HF896FoFf490ipFzxV/993tH9+nkn/1Ve+mn4A1xPAniBfvi1nZ449UXJ8l12Lh7R/Sq
gIEZlwTtuYzvN/lxR+m1kJefJfElgs2FIbLUbXp3sVy44Ta9qqDHt25SnR28W/eymPd6CbyBO03z
UP18ML3wOQNjpe7SuG12A/G1fLKONN0Q2a0IAJy+TeCn2UdsuXc7Dys1NnkE7MghpJhpz1qj0q7E
8RuTsPB7ZTAyH89Tlmbnpi7/waDOUw7+yvaakGlmw+zbxXwfWgIMaD3SsYU0s+D3AfePpZa/twGg
VJp8SYWARpFJu8taya36RLRBM0uMtcpL9x7xaJ/ZseWcKJAeES1YtqTv+26LI999XGY68kMhA42h
pIVY8CzIG6VAb9TU2+Tm+ewhjkcxTbStv3m2WklOBDJyXTFcj5qQ7RNyPhfCO8dTydCwD8vQmVEi
NT6do2ZkEV1sGSB/ni7rGT+P/pf/7BJ2MN34c8mB59VGksolTEsdfwk5F21iEkIOOxA1UM11ccjT
Ctq2LwR8WJhOwZ2UPHBjuocEGczHNHcF93Bx1UtDd/kvnU7FCAqZjJPtfbWtBHadAlwKIRJREVyI
b90zHWGGbeqf26F+8ibxBLK4+SI5J9yLw2kt+eHOHLmwY4me9J/bosSasCVcLoL4BLbWZPoJoj0N
VZ5mIydoIQHaQOVtx/9029Ig1ijOjqGl3zr+0wv7hpyBn4LvPcAqZnquTrGEGOf9770R1LXnMvTC
NidnO+2NlM2o6LGXzBHkIfyc6lURwFZ5Gg8PmDcBCQ+KupTCKZ39Sm7qc11EJAlcjnx+x7FODR33
xWvbKOcm/WlXtwOBQaqtnK8eTWa+mJo03br25aeBuhceocOcDbqOzEPtRAnpIK6bqSCkEaa+bkUk
/b4lhA9C7DjHRYflXSGdxhyy/7f+VEs8TVRYWIGpE0RgVs4Im+yijDTUE0/6hUpqOMcZz4lizmTw
hh2usfPUn1EbyxOf6ksekS1gYfctIPcb1oNH2uWVlRu5heL7KEcvwXtXeArRODN+Uw+LqDitjzdF
6C2VUvPLRVk4YZIR+cJXfqMDm+TkNN9SwXFi2chxUmgc8KkN8U/Ro42RZme+Mxur4yZhfsN5PRnx
+SRJ9xciMhnZn2H7bUWexlmLYqvhdn0ue4PVRRRx4M4qLP5zBOIP0RMdFMd9dSGdeZcmHyXGX8tJ
/9GlL8FHTEpGCu0VF8snKHnn3pQ1ZmmOBZT/8Bi3wPqnQWXQNW4aRyRf0pK8hNnW+SQW7SW745pv
WZapipyr+0I2Oe6WQ344dHJp4hbTow3dbuRrU2tyHsi67tU8Q83cU3xXnmzZvgASINRgC+jac5iT
CRTZwyLVDJeh6uSs82hs2FnORnPNsjdTk3ygtYF/KGDdM+WhSNnE3S/zOrRk8PF7OgKs6aIUAGaH
P6fshf2ORoA9oCxQQViWis1B7V0CBRDYXmlp1IR/dFXjmK1/MTRh7Eym5GsiCE6p8IHcnibsKggx
mUj7xmTMd3/4Sg2AJd8gNW2SnE3qrU0du4yNgbJC7aNdl/ZN3Jjrdu18HXYTT8wS9OC78UVn2Ezd
MRI53cqYkN/XlBy9k07QC79UKU2k5YqPu7nSBanjyugC3Y+/QAoRDBSAXMQMDBi6aJUCBRMKjlVK
xWt6Fcdg9cl7p/ksD09OFNY1aHrT5mEVHuSXRwDLMFXUxEMVTf1sMWXjmPInSse6y2elQCm0+1P5
a9qtNRM8DkTwaEZPnBh0m0igO+qRtKUNa8/9/iyKBkv/nkkpAZ8heLl6HiQF4RWO1XC/QFYF6bjX
GDCzJKoUMqUi5Hu1sJPRwxChDW4wvVq6BlYKGVUQ9coB4YdkKDDFNvall1pIo99eGsSxXh7xBNH0
eR7Yp2LgmRxuAR8RNvm1X672/3bUrZgRQMQJa/loT6rKClXUQ+5df/0UQrVQjcqg5OSTOc5i+Sh5
M7VlXgvW8yTRh1cB1G8UKTIvVwHN5FicJYmIlD6Bq8GMxvxmigJOcJ6b8cjdradtGtz3PQWBYElX
lCCLcdurtuTiV4yQuG1YhHX2OywV2EwCcuFC/05WxOtajhODhcqwtvwRcH0VlDjAj1GnWMmieUfh
c8ms7RmvGmMaK/EUr0S4oQKYe9j8Zbt/fOIM7bN4yK97kIY/QQk3xEGPNJX01++BYree+ksp7LKY
OXLMhbGjEwiddQZSX0zAe4ZATXGXIK/++5sNNXwyLehOegBuvB2RERSWSyc5vMF1A3hcGbDKgtQe
shQxfnJNjdc9aDoWQAgFGwH49DLjxnF5HijlNP+Je4LMThFY0Xyxf4jjOXhoEnhnblV+4wBswTCX
nPfv6eX3WCx+1ENr9Y9mTF8xAKJf8ceV1cKGIniVeWoRUhelk1676ED30KmPysPYmqoh1ZES/FQG
0NIGZ24LOsc8gal41FXa4EH0u/VJIZDkXLIsK6eKYDstJcOi/TfXQV35HcLmIPK1PQbwIW7Ao2+P
VqXElKqcXBiJM8WSjd7jIYBB1E18x3pbdQjjU8col9MbiiUIpxfKb1YyWGUfHQAeDkBnsl9PBDim
Ev6EsTuYjk4YZdgoDR6A4/PriOu0lFbMuH+QXnzYjztwUbQyyG343Ea5TkPxmfyxV/iiYH9g/PoE
ueY6PrB5Y/ciI5FGG9Zk0u/h069pP1AkFeaL5NGeBCRvrVV3u1PmEk9Pctdx03xpih/tGRdJNQ1B
5SJEybcwP2vcuF0xSdTGIFWqCsLOngozWFW4wI9ncn1YLZet5GR0wXjrrizkCKffc+W5NvntH9wG
f60oF3ePc1TkXzsOJyXMLxx1yHaCrCGKL8R85wFgvcXhRT1lgcnxxKSfh4cHSF7po5jMuN+OECB3
pDZHNBpt/brzc20JmU3T7d1fPnABSdEqppfWUNGFweUP+xfrNmM4xDQVcDkboZe2Lv+vw5KGqXZh
yp7hTwj9Z/2i6qlCVnBSCHUXD4gkC2evNrFd9bX33YFhm2irfFg5aGgPOicMZDmlKglntJepejWl
5PjHcgToWxsrAghddpvDX3JZnAap1G91ygYTDDXi/bE5Vg4+PSXyDdLWD5+vWQGRJtkbV3ykzam+
MBLpQuqTRu3Uz0qQX+foDAUffjvI+NkO4vdYUrSPthq/t+6bhst9WTikQwQNsn2qz5yt1aWTg2le
iVziLM3aexyLXo8aY+Wyolu94XGdLGG5hEVXaG+bmS1BZ16k3dzuvK1mp54YngN/GIaK/zsU3kDV
oe9CEa84ArlWiI0gCchcKk8n0k778ffurzI9xK1jsd1vjYll+5Y2yBzpdzYBXOW00CNTDZz3roHg
YfOBZMhxa20M73LsFAoiYiR/aNjqJunnbCJCdhLlgykbMkCOHRDBFrAFoeKIPI2pep3AjEDt+2XW
7YIXEjUO/ELFaMH6hW3aWTTH22132C5FvEZk+DPiNcwnWKCJqTOULTSBnH3YlM/l7EQgvceLb8Ui
+HjVyZzekV5c/T7HH4H+0hZLL/d7h7rWq7vxjLbWLs+X9sQ31FKBlyL4mhRVQDYg3qECzwYn5opO
x/gNnuG1dH8gRwlXBve353p9aDznzdGVXxPCqbo/0eYdYS/QH+7/76rVKO6TmgtZDuUV7KcdlgO9
k1ECb/Ul00qtINe651afT2vZ2VY9GaSNzyYtCI+2xi65idht7IXhWuygX22V1FNzTKwP84iiGKzR
dlq4+W/cJ+PrBo18jTKWCa5PpX3XWpO/H/Oz1Z8a2hNcXlyVp4HVDkZryrQnEm6Fl2xmQJ411Z+D
+w940TwUEjgg6Sa9l7hQj3nMJBR2RWcNUj0xD8EgcbTcyfq8qpLyDiAlNwnHwdwcfEolCIj26ybP
XJYBevgM2AFzVFgs7eqSaRrwFz/hKJNOQHLWrN30nUlZIMf/kSW40BoKlJAykSqAVyfHiyWpASVh
wT5gyefLRSOzh9nev320RjbrhrapqpFdBhoeQ5t5BQHqWmt9HZNp1rSMhD0UCUpwJARmWcGuVybP
PPzkvGnxU3WlaSsaG3fgGlcuiFJXtYzl1e0Ft0ZIRWOeGVyXa/RkZHovL8GPrqJY4eiEweVj3Gcc
aZPIimpuzlMkCJKvtpOp0j37mKtqmf+Oov5XGzRw15kqEw9nNAXitOtvY0k+dPOfQIQe1WXUQQzD
LNbwO1Why65Qs9LHd9JFe6jSfyg0kP4HDuRfn71ncPfXW6O5bf7J638OCdwTUSqPJDTfzlYL1AOK
MWmifjCAFH8oKyE7fPU7CX6oLNW5TJZWgMGuwDv1PvOz0N2XPraX5fjVr+mHKg2iof+ZuxB0KnqF
Cw/4HKoy1W2j8izu6mGmnaWy9PjqhDOVRS7SLtRrD9gsbRKYVlVqgGo4Ed/+8V4wjnweuj3fY9ST
lvBVggx8/UzEiJ0hYfrPOCng968CKf5l7POc9IKlwmfyJQdj12bnkcOKvz7Di4LZ/aCy3EL5dqIE
Gv4DcRUHwv9flG5ZS3BFLnCPoKsUplxQ+HmVNn0e49Ga3YYZO+W/8mpkJ8hVs28YlMWFqNRbB3Wl
kdzzvdSFQ1GuA4jLFG/j4+wRf8YRtWGNXxB/Y0/OHUaUZo0D3Yn4+Mt1QEW8aNKgjXTEDz70Nbyx
BMfuv/z+9CXDUbfTKvfyg2jRiBxELYklfg+NYz2ZtCcf3pSBFg5WoCdmz0qu3mfP+baKo5sF5RKq
kDN4HdJwDPG/t2B/SrqH6oKJsgKgy9YWbSFCpCpkMx5OZw9jAn5uDUerVK28mdv6SU11Ra2CkfB0
GlZe3lS925kT9cP3asWFaEy57OiX7XM5obWbyYzLZqpIE305xFiabdn9Mzd2j93Zn5NwylAytOVO
uzwHnwHeChdIfDldgIHMUJptapJpqBaGlkVR3KixFZ+Q014PAMIyTYfr1qahfUBK7kEJEk4efKm5
AMI/7zVdvTbd14NAGdzkgns8jQADZtPLAJ5uNNecxOjFqf5q1eFzOlKtbvq52tlImes/D5pybsiX
LQKu5a6TBr4vx9wklb2PUc4K7+6ftAyYmzun2NUXWWi5FTJHQekUNJNiGbW7/Cu8LjQkrifAOYqc
uRwht+vzDYotum6nYSDKhCcaP4I3VH+z4gKSkRftYaKvCZ4zNxC2sfvthjFCsoncqSCnACWyRAhe
fbN2uhmHTL6emdsMx5LlOFxc0F9AApLrqhenCWR82PJWBSDkrv3ocbk+xjKGDEQAy8IuHAGsvxY0
Eliu9a1tLSFzyvo0EFJPW4ZPYiv9RPzSDrYWy0cyU4mnMszYYFSaOhj5lESTuBHN4XHx33Rd/YoS
UGzjhOs8+BuHfr8c/Zjl/XIwHWSYbHv1P5urNjiISuQtA/TURk3LY1I3kx/RYpiYMPhKD2tz3JLb
gMM2EaXyizlweA/gWznlibnrS063z2G/OxuxT1rlYIn4MdCnZJ66O1VvpO6WVzo2xLnoS1U8txXk
5BRAasOFYr+e2n8nvG1E5mAD/tOEd634z0wu9cGBjJe4Xln2ARz8edOwPoA28EtKN8XjYUgatgGB
N/1kbwZdEFuOFH22hkKYb4Hse/5udNyqvcvfZLeUXgRxIzhFLj8U8AvfeTeKlAbhlAaIINLOxnUb
LGvPOb035th5GwA8YxEjtj8Oyf5LRhNapO5CjTHG4Y3Da6iU0iCpHcvrdAcbEF+oKk4lTJ79I97Z
1RFEMUouUpVTdhcpD6hzkkZ6vyivxJ8rx/sordQ0FFbSgGylLh0JV2FmiMvlHtXm9FCaz9lxgPpE
pASyRGtTQv6LQTMyVN15RmDWsJwSjV80o/5LveWpVGpyA44DLN/9m0wSUp6/Zmh9smXGrS0yuinJ
fhT9PcvLDFIvGkHX9hpXbgbFeoTaGaysTh0LjaEyp4W8wguHpWEOxjBl2zXtIeFeWB+afG+TqutU
Lfp2MZkWr+8Rjs9fAXkxDCniAQGPF0mFxXWXs9jZIekaGzLlWXbqi6hG2Ga4Sucm2xj/VO8oAI4M
5gkSBR1ca594Wt3o12oSQYe5DExARUCv51Y9/ZrxmBkk7PZCvUFDjtjs/CcJy7x8iaeqUOZ01rQ0
NqqByaEve4fWKuEHCSCtCN++TvEAzqphB9QEoMgrulLqCZcxKQpkT9hwYJDjVHsxgE2IWvA85Z8n
XSdVTZfxSOJ0Zl69IHkjVyzxhHGN3IlN1XlCbifzqPWP4uABo/vi80dS67zlQPwWXweob4oYnAX6
3ejWNtcyjihTDBYUGyCBwjbo+UkZhpw7zwFX48/y8bv5fyyGYw6jHYQ1+TuCxujgyp4Sqc34Ko0D
DyNxUPgYleqYky93n6Fzzxc708ATFgrZaaUsDXwSvUyJbSzPOyxILR8MVHFXZV1gwRzVHIAbtDnn
g8tFzbqcQXjP3XAfCtv466TinKgWrLayRj25+7MbaKySCbOMsaW/Zy4OsQewuPI1CtNHklz2+Dpa
MMGlPTJBnTBJGB0DdoEeB1FoRD1G0DRLnxUyZMTArt7Uw4M4b+JlvBCZ9ERgzg7V3hWH0odiG40c
D18GWNdKyZ1DQMpa35RnQnOLjI/bcrBXY/mtsEizrr2EzxAkdtwzSij3eCtW3IP9/5TK+B2DMOvJ
uYCAbHgMMF+DoyckiO7xnxkIyqMM89BWiL5ctHqRx80zbeA68tJiai8Fx6cXKTAIBt4z9hQJIGHw
6mvOmxCx2pUl7DyT6L8dyBvFcR5glxsP668t2NvASVgydg8QDANuwdjAeJP7leuAqGLB5zYOuA2k
NVTWVtSpDRHwIJ7tDyng1fA7Us1aozlA01eJYlt3XRuwZYRGQwWH1o5EDKbRWLj1nYAcvNRBTh6j
dmc6rqi0cxYyNwS1Ijx0k7wTYvtTZzaGgtTNmQZfZknfzDcoGT2Q9/WXKQX+wKbNfc6PgxGITV3f
y/6H23wNKtT8CzWk4zT4mskxcL9i25qx2MVSuAApcUzuhDlWRDqNP4oAhZ9obd6s9h/HYKM0RMwO
U8YJdpofdYbZy1oLiRcfSn2AXiztj3ikfoa17fdx2ihHReZVOKIZeaScXc74KbeQbzKI0jT76XYG
Jz2M18pogi4TVDRa/UHDkwBOs2grRcaJ++IMIZI6bLmKV6KXTrxoprP7IYlN3H1o6dbZqTKVFONl
Ysx3E1EinOMZsnqlLApmu3IgnGQTEl9HPTeJd/x8Nc0reXSi6EUfy/VL5EuFhsLTaDdYarVeaKSN
SXGaq5OBfn5mfERuydaIymG5mMSI4d8qlziFO5s0V4RDLc3gEam95ieW/Lye4W22ftTtaBNdluFk
NsEtAltwnj5KfND0jF2u5qcwnDSUDvsLqyJ20jqmqBwBOPqX2+TxkSD0tJTIvr9ccy7t6bxWE5P5
RsvVr9sY1hEdEcW/YlOPhJ0C7hQR6nGijUOwX8rd4lMsiQ2HKs+fcJH3rQYpc1ceO/ezPE6DLWUn
eUbDu9ijKY32n8dcl2+wh04JXT7soegoWUBIzQQq5Ea7I6uRbKtepkm8d0vyxt3/hsq6ksQ7mGGV
QbiYNVpEDkjTs+G1Ff53UeauZmsBEg54SItDgJ/I9HO4ghKNN4A/yB10qeYvcJwTwTl88rOXhGxI
5QsLEuKOwPIeCdjED34EosV01Rwhad59UTYyFhsjuWIHSy93J2i4t8ScbD/Jc/2cNE3NccbG5OJ4
KQkMYw8psbi0m0h4XlcfgUtZ/yL+LPehRpRUbzbqstOfxoqxuLaaVgOzYIUP8HLKSoKo98mow7VV
pc+p3Lo174LlU2eNRWvmpWCRMF4SUa1sVVw6wTH5pIKz/oU3Wq5uLWUw+8gY83cYmZ7+NyubcDJy
Oqr5X4FpZrpWfPzAPnizPdrT22HaDVDEFzDnzDxxJDgM5pIGcSvlfpYn2eK7I9E1u1Eed8IJa8kN
f8QbVJa+5+nC0PruzesxqURrREShpxCh7p8k75psYIe9zijl4B9b0AM/DDBVL+qpyKJRoJXFax6O
J76t8aEdEX09BDEpSwiKJsM9M7gqfggZL2g+5e78D9QYu9qR1/8Vt8BoamDBsELCgjKd1nCJLeNH
t+lHmYIZo+q8ZwUC/8v1K8R9barRww671h/bcksgUrjN6nHvrqO2GtwdKgCSs2d6xaw+1qF9oRWv
qXmVQE1MgBNA4pYR/pZyaI0hXBkx7cDe15umvTonB6XYDTVz96f4aVLI0jya4iOwNhv/7OXwDiYH
2P3fiUTT5qGLSamu+BUdr0Eh8B0sCh7iOcCCrMYTOdhC7BeNtBovwI8HaR8ekWK1ITR8mLsmYtMe
CEFtTO6o3LGCcyoxxhA+Pt2A12YfMSyYTmJEz7WgbpJMs5Gl2/Tf6sGa55j59AvpR5ZOqeGFD9YF
thcMKlFFCjhz1aHXX62VFURs+7xMyOx2cYxKmLYzEIZKnLmZMmhFxZ/FoQHKEYeWxDdsUCL/Px/j
v7v64fqqatr1nkjvGsQp5PO+D4vhbKdXXzHMBsUG4RG+qe8tvBY8LRex2LP3trXMU33LmGN89+w0
bs4APBm7SEsx/heg4YKSJ2d0DgdzhN3Rhv8vHracOMcthqRIy0h0WgBO49v2qrA8Z7HKszdpfWoQ
vUqWMM+pKQebEk2Sgz7VyNEtXKohWsHovvIE7JIU0ytmnvdUvD8YLJM3alDSh5R/xUmpWfTRzSZp
fSr83vvcXW+skjLZQNs89wQbcD6SS3ib+6hFq6oMZmdBsCbVM9I0zELXkEUpkCoUT6Z6luWdX6X+
Q8Mfg7lm71IO44BoyqjMjlekRMucnifSKPjRUPVQi7aiQp1pxaF6lrG7MFatcVTRlKfD99u0fgMT
/hgnB67OHt8ZupZmPHtd2oAQ4GJzXSZCpMbOK9qniOX9ibgaH/Ho2NBunLw4Gf2vH19xrY68Ukag
dE+w8zl1ZdasdJdw3/BBbGNjXDN0s+aZZSjEh5OLZrCJnr05sFajTLcnHeQ5Y1Ek3gZKL1sIcVF0
DSdFghD32WUyFf7LwXuBBdhKRy57s3e0wFZey+d5y9K4wJ9/ZHdH6n4J//IRwZVRVuL2aqPwau34
uQXv2HpCFhyyd1Y+jhC60m3diq7PbG3VvrspKCiExQM6u1QaJQUFbcJK453fDo46aAv47OKBHIK5
VCTojqpAO9uQCOvYqbPt8jsdY3hqNowI0XaDWq2otrHLOkkML5s8VScWj4oiuZeFTsKLvferyhkv
1EN8RZObhfARxmiIj6ecERi4JcxUd4kn9beNtvC6XDGIGec2Vv0rEaJKVuvbpD/mH0W8zuUX7ehy
FNjkYg31krRiZIAqIZS+zyqwWTJQHBlqXZkAnvTXhRdEGUESSCuAIKrB6p7Duz0i8P0txJD5Dtat
WNyjqYxUbOflTvudGKhULPRR1zd5MMbXmNCn7LXDlL8Ga2mGqpuu4lzPmr4sFsFRJcyHPRjWNebz
nmgFrgdC7WNdWiAynGetgwhpdOG6UYWh0YQSrGXC0cum94Pk9k1kWZETBqlEhw9bxRhuKrk80ytt
E9+hvS0dG06gUuawcK8YZTUmXBzZ5pE5xkkKm5nOomdKParhoV/Hk4WwUpoKhHQta6/gMkcYobk1
hoOLETbXYTs4oAJFmZ6c2FTucQRA49zRpDhcuL8FLbNHHfcqKh16dcQQUYbye3l8vjEWoiszkZWB
zV5+QK+GJRna5fF+sBcjs+VOs/uiVj/OGi4PDA3LzpyKMSg51DZNBTQmBEut5+DK3XRJ8F7Ymbbf
ehOydyeTBZh4eTxnDv68JdOncdsOpUfoiYZyk4QYJgL7ZpdSgdLKQr7OqvUbaz3DTFEQBq1UJBph
9oEqktup74XAtHtII6sQpDRxt1qb3o6LLbFEcmAZ82zi7c/OxW0EMsc961K4rfs8W/hU90IO2+/T
Pwk6WQCT7v6+9uTUfU7IqP9gAVexVyso4AiIjveEsPLKGI4Sl/JWBCqRLiBXyJraafqI02jK4p1V
rdx9l+NDRtOGk4fv4lKTiOuhHgSSsktx4FngwW2dOMW5nSOP1TR9NmaMJA+q6R7JOPJZp1KCHrFZ
drFAyhOQwGUVZFGzY5AHDHCYTnGTNU8bUkfILRHEFUpLnNGyLQ9lr2pO0hrcSGraT8cs24KRNgQO
ir+QDLOKS2brKU1mkAQdG2TyFk1WrxM5dp4Rl5CLjHHN+MoSeYM9HL6SqLUVUq1gSJ4idtaYYBzW
u21omcoE2reKfFQdCtdQq9PYNR9apeZMbTEmf7cvUMlHdEdIGj9vOYyrQBtcER8UOPVsHrsEycQl
Sjqp6jy6sOkrnr5KAz1PeLpG5UD0jq/oS0eGYIMQaWGZgQ73H05eMobbn8FR0dLQy7N9LGwGWvZX
i9oCf8BnoX0Zz//FY6zfpGPK4Pqprzp0WLtFo8KTudD2+Dfa8oZ4x7IhXFnbc4cao/bMHOY4pHrX
PUXr8L9sHRmdruOrfZ9EpGWA1DOIbhNg57uP/vRc05PbZ8tBGlLzNPke72b/lcae5Ra0cnDusNRx
7j+/OPz4xwUIpVpTlaWxRhlIY/XkBJWJtefxPRklP/up5LqvQZ/YyzIWyzllg997yX24eaFaCmVL
TqLg54dRk+2S0/xXBoKO+PBUSIzxLVZFBA0fLUMnHUqPClBwKqqvi6QovpUkLEfsjIVNQ2HtjL4M
wTokx4htriyDGg4Z9xeolV80y2nWNXgSmXK6LqrZuYDClueNJL/+1uU/GiLR8WGLW/jThBcj0UAa
U2iVFtDaWqATSGUIr8VmQFHlzQYnjliOuUiFG2s2Dq4GdBPf4k4Ku/YN8BSLOiomlaL4DAhjN5Hc
ETdM22MvVtnvFnjVjmECpYEeSGibRouzsfD+qyCsxR68iw3KehOoOQPVefLVWtUBfXmkN+36LfCK
M0AGEVE84Tp8QEPt4brZeweBhoHeXy40WdqLvTN4+Qwzk0DCVcwmBnNc5kd3VUkB/Dih2c1feaN3
yZu0z/s04V3VFH1EBHm1+NQLFtBqDHTGN1YcYcGnLb0k17/YxXMhXQ26wI1fpbW4ycchI6zZnUzI
jo8Uqj+TtdmUf5MRpw/rHkw5R49XqvR26gaGAIu6eR9PFKj2Qc4/IFyXW0x+qxhxy0qgaD9epJ9P
GYDGexQVSljVwDJg/P2h2J5LQTjUyIwg8k43MA6CHNJPsX9uM74M3EB9RjpPrLi22LhWkwIdWejL
LJH3O0JmTgM1sRnhv/xzYtRzczXoVbaOB1nVo7jibUdGP4YAyo/e5SQdlVVgvmZkMGkHtzpr6m33
cK9eNiXokKkd1NIE4wsGhPp2mg2u6CdUGS//3PeBHmwhdGeidHEWqtCf7aeyPKzR88oEzkV8RvWz
M0fSiNcoHn4K+ZxhjziDR1PdijOrH49iKaWNMUlzh6mmHnZToS2aO9tIhEMwwWzFXTnx0zQj3JR0
XmxJ8OsFp2eV3z/fLn6Sn7x0YvcxnK/1BI1sAq7kzy6Yihs9qkVjs3YRoGInGkXGSFbxlSd3XI5N
Zj2StIA1ciadJtFk0udX6elvETXYnjfVQjeUehjXTAUDpE5mTCf7qixaDS3pXOD4wx7bloLtxw4W
iTYVCvFM3Vk80uWCEBHcGIL+9OVvliob7m5Ij1AA7dJoCLXEOhvydNh1Mt8WQVsBrBkTWv4LBMSq
zBDbdCqteA7C6ylJhkbM57dMADUhcHe7YYEPb+EoWsCuKpuB3kk2QY7XYMHWRL9HScy8F9IWyBMK
fmD9EMT3GO08TWr5Mt6W67LjziIoFgGJv2Q8XhznKkQO/IPx/FsuN2FljwE6TaTlIQc2eIgoytrS
wGZhOPhH8F6vIqJYCWC4SDOXi921LQuutrmzTrUYWFG3QuCjQv+zT9NEcaUGo4l4jnwjPRkKXsDJ
YTKrVnWz+6VR9UPmKqM28lJ662Rf+i03LRD8fURzn0oFNL1uRAiLZ3Inc13wXbs5vg8nGNiMcTDd
SvE3mvymo3f1biSSLdUfI6m6qV8keBMYTFXScYj2Lh/QLD1EIbCShHe5w+QbYTwdzgjy0ZVgSqg0
MFFaq/PcswayxPOQi0dqx8vNqpcgdYhg5AjdtEeMHatZS6QYIvHztqsEZiCgfXG0Y6ZKvcf9brwT
NnnCKFZGOSa6oPJVCQWtH9qqOTC2Klmq92BSW5fHYHLFCFjoRSa/tVKIvyeV6XCKkGBp9HiNY2Vu
uZjTKXcULKz77hVQirn6e0HeflpIbWnSPSxHEaUdS8QNq010Vrc7YRwYZI7wCeiVmogxJXGCuBY7
H57dEnB3zxmwbPzQwCUT0gmWlHuaEP+wHrZP9qfgLdp3aUCcLit2zFbe/ekmas1f439cl/MmKSRq
6wvTvmTJUa0AgNlHFszBLSqFxwZDOkyOAOLlWVRXsup9IXpZY/K9i6zzFe8oSi/M82508Vhgfa7D
emMWPHBodltztfRanTrdEqWZWWI6N1xO/wTNCWLJ0f0msQ6DQzQjhYVfhMcEvdkiE88DZzQN+84y
mJbG5CRCUlGvKRL0S4XW+hqdbVJ6kupGsKrB1zXlOzZdqDFkBpRW8+x98b2leUxsfloirBG8KLac
+4qd7VhK2q9M8DV+Iubvh6Cyhk+bQQpYlbW9vqoKqFvgOJUPQ0s17FqoFT0FYD3SWeQMfai0TFnH
rSLrsJidp4mwAE+zTbQ2qbn9p81BB0ezeZaSHoxXRFMPR/n+nVSBOHddl2EjHSYSkRYuopsO9jRQ
uvVUJx+e2kjXwBMsrqZY3hLCg2u6lp05mhzruoEbJDhf6lSspZVO5aYjbrQVK1EowKcS9ceL3J/E
kblHdCM+SlPCk5zZtYurC9U2CfRZj1AG2CkaXEr+JbCYENLWKa/czpGtm+BcbweSMwJCRb9cUH3O
HdI2t2VeaHRm36keDTNX02ebUsEqt+su/WkkhRleSPrV7jFoRXeSM1T7aU4DHQ7L6uvzIGgGT3fI
NL7K3ebVdjBvoySQL16FeTptEZYha5Cb33IhTAE7LWD9RHEwKNkOHdMVExdgDDksijhRRL2h4OHD
dFsdLPKB4eugUoZYEXmdTTw9gcXT2411yEdUAeHCZ81lezn4ggWrotT7mZ6l+CMaZ8kpDxZBjhG3
rQFViWL54atks9QzrZrIiCvaiQh0HlepV8YAVgvGhU69E7FZ/byNcGNdXjwyT4HOQ0+NL2rWdln3
yo9+HyYUMQj9tYli+x4iWbtTOZueq0SjJqGj1QvAOfDbJOVVYAUwcfiWhUzHSPslF2fB18FIQ2ng
DhxAGlnauYFqEziALy3VNXAxH17Ds4ZYMR3p3e8FR9VLipd4A6SacKNUGYkeRO1bmIUf8uc5jwr2
fh4f08eBY/O8yRjQ4/Cfram3VhAmDvmQ8dIfR9ycKL1rqcaQMZ8yZ8LU/PHpfNkG0fWIbdFCMH+8
l6s0Hb+ld6PMuf2jUxWoD+u3fclhuoh7Av4E4NhctKphTtadXHwrv1hPcdAbcSy/nrA79VPH4zx5
RXn7IzjILkHWyx8HYFi6UxB8zgbCyJ9NXSbQpmdK53h8YTGZPW8EFe/vYo3+2K/X346Kn7Ur3EPq
8v5572tYwSrEK2zDlvVoeDbhwLchRwJroqaFYSj6JQ7wztCUSS48LzX3JkmwYipXgW9wyYSZBRZB
nTX8RfinwvrYDWSNtiRz3IS8DxNMe1wAu0RIu+Nzadogk9RRV/4dZlMoXmJcmlIpkuATSvTuOIpO
WYaWf4B+GqlL8H99sBxWTSRopWhcL4QVtAMYMPmxke6vX4O5iHcAQEpm/r1zOXdwuV73sTWQVM2s
PuF8qrLoSx6RGEtPSldysWDcRvQPGX/CKs0TokMjZpbybLhBJ/n6hw3cWSva8g2z02uuchQHHExs
J8GYnIqEWzEZeYd96Sgk0MFgTlVJVTeSgvx3atrOuE+dEuA2mLyFYU/h9QdyzZ4p2zKeynZhfXX6
tvEp3K3Z2DH4nso1kgshFagTuDUtrZRRSeWHpkqRPqU8g6i/gbqlS4DoFUHDnjuIidyOndn6F0pq
jdmf9Lb13nVIbr+pWLa1KPcqLmBMiKt4s8RGIcUmqxjnwPDLaysP9WtQ44NMrQMk/Y8Mily+mdQi
UwmyPeocZsijL6ExvJ990Ak5aJZuBUHxf6vqmXKSQ43gExmPcKs0+jXv+551gWeffSy0nAXaq1n5
cJrsyhI4eUa6drovaICGhqBoueCwZrupUj8Gm4AImbXyD7iz4q1Z824QFmaTTCy5n1uka/kPL3x9
r3Z7nVbJO1qWDfkumIajcY3EJ3330SgBeuG4V6YUfIoImYvuqlEZlEnj/mr6kx467knSpSQoC1ry
B9rUwq7qPmoRxwPEKPmGVMhA0rxzpty2NJ0i6ikxn4wylwMEfBZdGyk9njm6u/RABcWKQufCfNHN
niIXtCdiGRv72teJPKZrlcFlSn1jNiLhzR0o1vkiJt3MFVYGqA5TrbVlJn6YvUly5tqVtYEt4OvG
+hX8O5o7GJxEjIiLOVbLkHJ28EiRo3DzPSR1vvX1wqOghwomI1i+6YlgMRBnLCNGIxog/NZiYD7u
nQlnX4PI4Ul7vXq0KnnvUVtWRS+aZgcrlYwptHF7QA2nvRIhiBBZy5Q7+EOOY/hyXgT5GVSntV8x
Ef0BHwrxWCKz9O5514YkSaE++OaWHu4Wvhl5ZdQ4eBw39WFzyK6KLqO1WVsGBz4HT/NKgPLKWUBu
MX9RSw7yp483tChW2s5PeVfNTFYSqVZnQstSNkvGCBdOpHkpGYwRsOaEe5UcmMWDNPQxWXm6+J65
NLTIF5txVV3pge17gX2+NrAskUtj1FOywxbXx/z5eKIm3wWmjJXqg+7GsLCDiQfjZzpA28zS1Vgc
S/9QtYmOASViKrfb7ww5nHG9NBzrkHtrYLisFVPG0Rw9plfa0exRaeDojnFnzk2icC9rPHqLqIMS
gLB8F7Sv+O09EvGnWsu33NHFGXlPsxascVeyTU3WeyHkzPEFw7WCihhjPSnIXmeyWIB7eR+Q6RcM
nR/c4Eo4+q+SwJ6Q6cwCn5nrJt8Y0U4PonU85CJq0qkZdJGoge6tXV1ZmVcboSSkPP8MBS1VK/oX
XV6wvpAzWGmgJTsed2MRym5gs9xwRDb2tyEO5YAYwjMDdjGUybce4uT4AWtrVIBNsiGtViY0XCy5
RTi8bnIH0lWkPHknPhWoJPXE9K24CeWzKXwDPiBd/t92gAv9qSi3eVxI0k3xXdF8DdGVHmtqoeMw
aTUr8dPeXvl56Caf3cK1FEbkQHiq/Y+ZVmxpeg/bFw3TUOFcc+RUfI43ixop7GDl18MPKt0Y9bnL
4XEWSuvir6Xhdk1PTLqQRFc3JZ4oaNFY9+ix4Wf6VEdk15+yQMbZKieDDFkwfgY2Emm/52yREnb3
dxZKUHGEO1CV5jRnC9Shc/51JQGRDSTwQLAKUuIrKU7jDs8zHT+VSMuoT70m8qr2DAOxkrZTXjdE
pHDf061XscIRUobQK8NpkpRd1oIvbuieHaPE2rnjgBDkpcoNbvyLEeyJKsjaQP5wD2iK94GlndjL
MAXz+TeKyEuK6jGcGV+0OcGiZ1UEQykznIsoHfbXBnZCv+KZHES/7QX71Ji/7WReodnO079ewLPy
DBs8nAoBw3xTHYSCbygDewz8aCcZXg7FUsFgpLoj87yRVM6S7P1wrZNOwsEO6sjIUbdvnSB2F+5D
n+pwpxRVK4SzBOHsN82jXCrBvg8iDxQdUbibE7+YEWKyeYqF4aRAppowwmDcWh/ecYtf35btLDD8
vte95xMwiPnz7GBnr8agwwFcvPeWuNPGR84pVRfiWUqPw5RPibtSarBu4GXZbWUqaSwoh8gHDYDB
G1tqajSQkHPRCCta2A4pPkoXzQME4oeq903MvxrcbdsfmwOjK1OvQsBYxorL9KcudgVuMMx7lAso
EC02zj8J2Yq7Rgtj2L2h6kcxXmeqVSlRIlJ+nqu3Sf+sAmklUil7qwAapkoRMzmPng9u6Uo7LTMG
X3yh8L6c7NSiPpPjf4Hh9gjs2PpNavy4VRu1vXC/lSnOEyctuEBlxgIoUylZzpshOZjvlGj327cx
/Nb2xRXD+v73DZU6bWSCUIGZjqwmLig0FMTMIyW+0dwnppbX1JSmLtBisN82egNfYSL6NikqJzt1
U0dJA71sI558vyfdx1lrrbC64htdGj4uZDvQ24CrFJ3/PIpKh8CT9gHIQ3xoi1Kq5tY4IzrPTbsX
n3R28xuItXoK80WU5+/jyGcyzHXt2q38cmZ7RLsI5zDgvZFCxXXPUG9fr3Oe7Hgcz49Q1xzKxtG4
cb0Wzf9xFcDn5OkD0H5v/UGH4tpkszsYcQz9OUNwZ6rA5T/TinkYZXMs4NAGEb4KgBtKkgFAC6aB
3rxr/lr3CjHwC76dCELL92DYV3QN/rEmiOf/+pj5Gi96sz/zqF2o6uE28zNIRYuV0C4bcABKQsNR
gA1F6oKyaT9vTrExVLPEwGHtgRzy//29o3/8ZrjbrbvA2axZK4nHDt0G/qN0zRoVANJ9y7A2XPfU
sthVjHFcmCcj+wMF+oGASiVbB88UCBwnUN4PCmI47hWTF9cfiy+IrjPfhIhmHVYl5w13PhNyZrkv
fV5mcJ2aEhamrVShXOWsU3yr/h07doj8ng9x3NfVfcnQD7VDioqb3K2siZ6idwEQ0Qzbfx0JSOBy
r3j+nP4WohYDVSwe6CYh+dRIW3aB52b2pB3fpU58QcohAqX2zaa9aZGSY27SI0YZRDOMQ4/yfx0x
dFBbhWQ0lctOFsxIQVVjpStbAx3OwunTTJ0FLf/5J87ONujeAzXEsWiImJN1XsTmKKcgJ67AKGCz
/b5Z0eG4vltXddK65CHHSd1MY+zoD3SgnfnnU3X+rjaAhA73gVObd9NzQ24/oYHTw9NrDq+h6nRI
m0ucQKZkXlNBhJb3xT+e/TzTdtWS21AmxXqcZX3iL8wUaXFqI/KWHhm3q4Uae/c8CxD921Tl6gxD
ujQgPPrlgfBQhlH4MDkpDL71t0IZ26v6HgGfrYoFtl5YMzU72hJW1LY3ghAcAYP3fZut5A5K5eV5
i7fdqzEbzBlMNorYk+SaOnQYJovOqN7r+Z9EuNeGFBeEjDsETnoMwLnJUFrAD89z6sH59K7uLqYD
Qb0iRQhy3UUMZ2GE4v1JETHZKDNcoYenglGGXtKkjLDyA7w+M5oa4XCTfVLM9A7hlbC44nR4sbqY
eSYMkLr//u75+u9lXs7Mo1M5P1mr264g/QVJwW0kn6xc2jARBGj22Hga1q3GYiCLBR64wXr2xHwM
Ipxl3E7Srswi8EbcSE7f0NMGp4RcOfbiZHQP3vprYU9T4AMywwtfh6R8xyJoFfN8pYVRV30Pv2jz
AJ3MMu55HHEeEfw2ltTHz9pxCRMNHu4/GSrEGTMVpMY9CcpCegyjCsCwCmG11NeNWZ1Pc+u0Jjuc
QA0vJmAumZTo+dj9eKge6XQfroHOt63C8nJlv7LXqxixvso+4kBH2OfsJ+aNgB8kqISWw29QoUH7
ZFmRd9RlB+ijVHdLMv356SIbO7QYhwimN+GAEwUkkmHscFu7Oza4woJ9iJ3btPugYk4K8bb2yK1B
W1TC/4/3p96QxHS+00BYYu40Oj/vBx/MQkPLB4+Eie3blPldC5eRvmlyMrWoBaghVjXBs4X1jWbi
9yBFM+1Adgvn2DTuwDT6CUQKOA/p0GfklS+hm6kAOkmfwqDEQWwZgdYzDoYYFLmGnEA4nMSCZjIP
OHwxgUfD03gKAW329oQd6hR/5AGaUyHOhYhRk+6/EhRXCST3thtolg7NQ/x7GPU9/ERwtgU2RH9N
KD30ufgVxv/fKEWzrK5qFE43Q1UE0aNcznC0gMtSclKUDfoGWzEp1kVu4ilzQPpIGWCSKNzXgC+u
AKOSw0buXcPvkH4Z+5L11VjAfjY40bvFjysBcQXnhSsOpSKvTlmrPthPt/VshjwdbO+OyUB/CO+Y
OH/7tG72Ks729CCE51+RTc9StL0LkHhxmC/4lKJEKX4wpy5fulPdOnpGA15u6MBKevwBPk3IMBo3
fKmUnTAdXVKmNf875spEz2qXsZ2tsVStXO++yJR1YlcJ/6yRcwiKxnOISiAGGgkPDx9DnAK51Se+
LhOLXDGsWYwtscWS67/u5XjKjtWg3XMZudc78JZ9Vo5tlKmSkDXC8P1ehb1QhE69nupbjazrcRTi
+2365dwMBcIlgouXMBSQBG6vzsZmgypwvT3eFU5T29a4GHq4dpzUtCKI4NbAhOOW9EV4sUJsoV5u
U3az+59KANrxVP7Qz7E+DPw1T7zgmWeVaekG1XzL+v4wsnf7WyqVlRVAj4sUpM0PXXH/W1nVJ258
jVD7RC5mn89RiF3ROaeKRfc9hoFrmWBYQqsZU19UfIkiF63V2IGsQ0zSN2vFxUTDm0avRZUYGSns
uSlSJ/iN2IvNUa96ScoBIM10/CMZQ9hOvX6yvl7k4AWCwQTZIx3Sk/Xc9o4C+BD6sbcE3dS3unnw
b9f96GaxAxL/BbfsLhNstm1yzcfAc5Oorl7XJN4/DIcyvLF8UaaTsQQAgeOXhZlUhwoQGQ5eQyIO
gr5htBd1K6ThZYNooRIWc5ZT7mnTA/DQGrIGVuuxcrz85VAOuKxz3pYU6re+/VyzA3xo/1aYjJVX
lu3oYP1H7Um4618VdAzPqLWkljKODYbRtJdy/FJYljvpe9ewJJsRuljj+L1tZe6JZjNBbg/sZNNG
pugB4OKZxPUk/zu6MJPK7aKwGbxGMSS9tmxZiwd1hICLjfkAMXIH0+WqJQ4Hbn/qAmQXeN37l9ml
y5oqfJVGJSjawh0YzeUF2+k//Ipxe+RnSO45xDtyMJQjL0j7174vC6bp3QfFacdYUWq0o2t0rmFT
OQr5tiWJw51homBwr663eOLgyMeReEQYCJinxky5fMnuuHjeQyObvgu622YHeVycnp8tAEzHSD4t
2omVdwFQj1rLH/bXp4Pz2XPNwBMTioSvwLl7L+FqpIHjE8p8JdaM9glJsSFx9nTNuaNmD7+P9KDl
ciLGl+DN66BBB5lPZ9RN/q5mNuO/mPk+AnlcQ+WhbQ+ho5ED6r051EnD/dTMSGlerVRrVNM3geyL
5ExVPiIpJao8g9fqJEkWP2rFhWk1toQ6+OghDpkvk3sPmaNjCGhRw+ZBE3y/gQAb7gNqpC49NZ4p
QadUo6B/tI27mUniWRaQiVgohvi1jePRi9nOxahnBzYfl1Mp4hiYny+whq8FxOgCIoQIhFCKEbrC
q3Dnp4u+/ICncjaL1oVNVQ3Ml35ylLNlUodgkVCH20IXe7RRY3tsK5s+DqrNfN9Md/DFn3jKQohU
I4MKFVUf7eUhwhaC3+6irpG8VXL3oDbslgSB/xKqGpcGDMCwrH3TRO5i6MtpuivGx75c2elscmy/
9Qxi4SZ6B+DMCG4rA1LwXmnqXL/RbvBM6VuVBBrs4egsPvkS6hHUbPYKiHRXV92XNRlfgXUqcwQZ
Tu8/2b1jyn7yQ58W37Bxm77LGzQAinb5ot9R3L2E5e/G10pHZAb7TPyq9uOZj/LZ3PQHf7lGCAw7
5qd56Y4XWdg/67apbWg+6m4H8MLobWkwnbZQT4SEUHozOYgozaN1lRVZzthNTG7SoidJkdv80/B+
Hi4JtpYT1n/TIDaLuOzEfebN3AqhfFaqII+5e3Wshu190YueYotg4bOMv0glrCg351Y3xuGuOyHL
F3tnps73dLapyRUIGaJqHr0amsBAhn8tGfP+f04jntVgceyljsutEQzadBT2DuVqPrmVg//E432E
0T4JtgITVUyuUUzMGAyI7xwrm4JhtGiKLNUbvB+wzi2fwrQXiJW6ZSHZ6/WbYRCN0kKr96QfnnSg
5TUulvcUXjeDQHyAK1XP0Lurjxx59v7U5IKrXBSQv6uOHj+pBDYmXJ7YisFklju8ci94XgezYsST
dLRnMomhxcvHW3dP0tIzRlUWr7IqVnq81NyZIz81YjhQncXElQRaTvkoCNdP5P1txcV3gEJVe8bP
CEYo41AY0MEII01Ih6kb5QAe1n9UX3bU1rF/9SG56ZzybQ6mwlTUqxw2bynEWoJ/pAmT2Pl+0Ml7
QYkyy4/BY5voacw3asF0FVVQt89dhT9ZBkLtz9V+CDOa9lM3pZHTxbqbHiQt9sQ9wzGv+8edRXZt
/lgYqWI2KqQn0c6xhoEeIu7UUvJ/qfmu5Qt8/Y44KBs6z7xV3/Le7r14oZRt+p7ulyqYctdDzBxo
FST1E5wLN1nhIZLM7jByKFCsP2z6nu34g1EAqwXI0UFj4OKmqaN5myVYvPFuhdWfjOXyC6ZerFi/
d+fLfd1UEPM8msQ8T7rqk86783SSjyB+z1iNx/7xHOchpl1W7QSHTYMHDd3LZeEzdXcekudZ2+Sp
e2Tv1lHSZtP2RWCEYg2iZsubWFl/KDIbwIhijnTBB1uDyqaRfOZsOxnOudqEjiPEFirdjo5boLxX
c4hAhYA/Ly3kcjgpqQ8VeaH3B3eHBN9JNixhQPKmBqs5gjQFALA40luNWKepwQbhy4txSQDEnd/B
KqLN8IyK9mODMf40gTJkIyrKb24K88r3qdZE1v4mzdA+xO2pwILLnsZUddFuCu+cpSRI5YLRyQRF
PaX8wl3KmyiGzQ/m5714zW4swN15nf33LG3BbgOc6cooZ87bR6CJVx+Nb6UljUc0IzNnr2rvtNU9
dotU9qatLt6A9U0re+zBK1SYT85dlsqRmGQHLZFT9p1WePMnUt8k11AA/SlQnH9+03pBPJgNY0JW
IbTF5XHZoh7wwybjc3q5TDJPoB4fWFXWog21I4ntCVrAnecpvjz7n7g3kY117kPh0lMgzeYE4gMp
Ccadlj6qVGvJ9mGPwj8Y1LIi0m7FiKsNq8akrTpr8gwaBTKzdmTxwAcrTKWHgOmZYtHTeSNQjjaB
KE3FIrWii6RTm5rHCj125S7Yv5laX5N9JRM4/bQJjzUfSiawfwAHKboYOG99MhW23wPdtJiT6EU+
7WpEiWUc3QhaaY61yNz87vDs2F+lJZgbN2jifCncsZ43eh8VmuiYsgm/jJKgHsP5H/Y49jzwjNqq
NKODbMoVXX7IQv15U1wg1DFF/flFEPvpTp5IrRFzmFV9k+b+XGHg//B2iCbsWvUs4sntk3aBV7oI
u7x/Y5eQFP6i5R/+fI20xACGm68CHmlqMhPnm41nEnMDfXhqxIMYyBZ6/NtFJ+LBSFGH26D5Lfh8
GLPN4HIvQUGXX0rVYWcpCpM9HDgsz5tpampqJU+ruUhRZITb4/UxCN6LD9LVi68vAyJ+JOj0kWKv
aP8fen93coiV44EGttHm6aNcLnFgsHLyMxevVL1As71xCIhl26DdDxjd+ogjXk0qFlrPpfjD5Dd3
O57J670fhwewFHVYETbkQT5RzhD3oBKvRozWQqu1HicChC/EGJUAfLeIg7gYlpmwng+FX9oIRuP+
Pogn6r2w8Dij1uPsnGQdYC0rMuTB+bI/EQ7L9ztP4rqZ99z9rgK5bz+PKVuVrYNr2qbERYD0298F
ETBwYkFNJlhMIYnuOmD3uUKMTCcWYT/nUVA5YYBMlCwokens/5C3bPaT+hUTs8l1VoRUyIjyJnzu
vY3AT1BpCfZ6QdzTrhcgoIvSRYLS4uy9sv3ZU16XsQPAd60DnSUn5xGT2XfO04uR55wIQa3HIrEl
FBYCnfa31UX5K77ZclV9A5xU8IhLqqZzOSyGWUcYzeq9AQN4UoPDeZU1a1x7UxvRxHGdcYMRZeGO
VEcLFcjUbPytCXStcprdEslY1UsT7ue6sA+Uil6ixtm4rq8RUv/S0WYE9zQisnjaIYpcHRUsENYf
F4ylCAzRHsRgPqQN2GXVZQ0Y5cMnbtntipOUMAnLRb3Cy6VkcmvWBrf+4QOk8NmCMYp2icx4Cp2D
ysrDRe0a0Pao3FodWe2P0coMCi8jFqYYHbj2q2437jhwpAbekxemX6/x/rP2b+ZqWx0eYNeabG2L
fJf+yjP7WkV8ovmKExZej4VfGz5YmtawuC1UT3floFGSAwht2cQru2frZomMbUZjn/9YG46lbJKA
AbcW0z9egrKa80bX415iQIYZ6PQNgKjHZGhRMnWjOigaeA5O+8FnfqnLEwTML0vm2OFdUMAQCsOH
JXMl+EdFMkvZfR6acJwmnX0mq5rYUtmu7FAG6Gcy+V/7ZRKPh/E3/hkPStSCZgwVnlOkhe9w0bZd
I7g2stD101XFwJayC/ySVpePzqQcZnLUBjvqIrxpFUB7RI97AgMAW+TwYFcarKNvZD3Ku6H6vsiq
aBM2nGjVFJ5yCHNflcOw5OKCJ5PrRVSi03cjhngvU4gWdYHFAKNv1FRlGj1kM3eJBGYiDuA5cBzI
CDEPucgjlSHzbR4pUrEgQt8/9PxURq1iB1YFBXN2gExl1e1jvnk/GkbAtueCqOS4wca/qfKZxRW9
xnBN571zBgZKv2Uwony7KlrhZYbvkEEoj1rVYzYtKkag/2NIgZhL7aD6y4nFsCfktylHsjk9BUs3
opZrQqUsr3fmfDX1oJ+s3BQvvurI/kHKf4Z1McqWMVhJbNMHCeC3pj3mGGDARFARJDJikhiuDcVn
8S4ahcS5nujD5yHY+L95cCIyG7gt87Qx91V8kBpKs2RjSdTcGdBTkFkbSN0D7OTkDpSb8uefbVbt
Oj7mV+TeCGJtJ2Y4l3JEWOopJT8XB7psHL3Lmu2wXFtZboClxEeeAQVqgNeYDE1xfUbx3UHhV+SR
dE7tB04SaFzk+nKdGVMWSt0AM/xrFKgutNZ/fTbt2a+YII9fa475JYvBhFwHhewo+kat9fQfG9A8
yejZ2wwbSW5JhL+sdMDMTkYHXtY027TpeHfVwFBM+f/RaGOrliaRxMw1+Vto3yLC9QULiaVOTeu2
UYtyYXusvej5c4ViDzGOucEDF8TvsIxubLB9Myo/0fq50r11GKCov86cUahSKvErTKtyFO8GLTh/
Y2rXRvhD3/tNobiiVznocBRhksbD9rY54npfONKJNwJQaFXM48ETITwaH19WAILZJYnDP3gLPxJt
amLROOabY8150iubUmtAdFzDXdrEtGoQiz6LI97CM8bpYyw5d08kV6E/ScYB77QRDbAqc1hj01Fz
hWcWJSPtKXQN396ZxCJ4IUhXqDSxw8q1kQ9626HRWMmwEbh5IlgsqtoJUoMDRdw8Njvu+02sRZT1
HnBoN+PtO/tbIFtP6nQ+EqsGV9tssNvS6W/wHYdnAW73fps4BIwFmrhz0J4CcxJL+ZQC8MZCcIBh
jA+OmG1JfXBYv+ULiq8e+5DWqrBf4tf9vn+YppX/oY4uosFv5Rlig5/DuiK47D0kI9de2N5SY/10
Aop6Go2BwiRcpcpyjoeNgPm+dngx2kJhLJTYLWV01utdBZ6esUxjsTyRPhaXeSZnY1pgEusnO8cC
3aoEWpkKLL5d8M6s0KTfu6F0QOIh9q80cpfMU+2yVk4xTDH10/0xJIyEET1SiJKhAoi6XeB3oYE/
4nIQosnJr44IDkQxvYjzjKspDDDdMgc9/QK8eoRmU0nGi1oQMcDdCB4Lt+nU5R/e6A7KDCesA9G6
lENLoFUNsBEUpLC41tBEYNUXMn09L8OtzvEJWx3S9dWS20XcNIpqt/qFLTjZvAZ0AXaMsBAbCnBH
akMKj+rWHCm3auAdEZqf8ydX7zRledwMbuZ80qJDpt0hKj02UhB0HIA4TpX6r2pi0c2WjISoKE/N
CghjZEkckj1PEft97AO6vfPWy4MiwVkbqmzkcEt0Wif8vQtPxu1SiKwt3mG0kaVGO4+agdhygoQ7
TGoo3oKMfEXr/xnEwyfTKcf9efXPuLNfki098VsiftsxLl03wGuLxaekLYtPokOnfBnlvWZpGi1K
ij+FeI2LL5w4xT1NZhBAjwkWZP3mEKNSRqdWHcIbGOAZnV5gdfCqI+SjDLem66nya+QAZCpnjuRP
KueBfEhGC152fvvjzJj70itrBVA0ns/uczyHl36eyaEdyYM7U7v/IRCUMh07kZXHe0bs5fIVI+Ib
NKFdewxJp+r1+jjSK3aTmjxHpQdb9DebwRukwtdnrg0Zm90uRi6fGTsWOLPN+7A3AYu+oJQ+VBHu
CF2IMILsSId3vUIW1/6O8BVK7+SECGLGcurUqp8bWuLJyoiiNXhSMwENZVhoI3kPByNTn89gMDRd
zdtF5zjv1BtowI3aPb3H/Z74j/F2WFQ9/cIjAIN0RIE8K8lXyuVD8jwLZNfsrT81eftCgCPeDirl
BBKXitXLocq3kcr3UlbDXEI4QHCKM9bllfQWfvrlSzVuLU3fVX3eYkaDyXpXRtruJgR/Jlf53E+D
gq5S+6Al+eSPTs7CUU2lpZngT6kL6ccN5FPBIhT/c8/CunRPg75VYjIP5omKF6YIPl5LgcDvzXfa
z2RqPc+wIXpLWmyzeUvi5lbezig3t5YepEy2WzKmA+mJapnU2aSKuh2sd9fHeFkXL7wqUoqMwmUZ
9zVhjLpbX9vcGHMNnHIhwoWSG6VX0bIJB7Gz0I8RphyI8du+qxya6x7AE0rk5Gmrmd9px5RtJilm
n8wrY0X1V12DkmKtRwn7LBG1nWm/QPG1Yg8JZv6jnPU0PJf2hCU1NTiENYJ9y4qFSmPNHc5E8c/O
W8gSEmZVKTCqi8rgiRu9IqXgyelLJ5f7+QnkjJs6/On4KEbhQ6k/p2qAanpT9psa+kK0ke9OAZq9
yDH6NjeB0qaqPLxl94OdxYjlv+Ygj0n5sFrn9qDYRDzT5HWqBAbsbIh5Mmxh3jr/85UDfaw0+Gv+
+NtGme913Wbgd0pfz1JmnzUAa8CFgsWOJpImOR1lFZkHRZxn9J7Dyr3guVbQFrUeGpKfZB0gWoHI
aLFity8o+HSZJARXA29/fNa1YkbbLUkh7TQCEdVcO874OwV/b30UZ5biDihRzkeCf8FzQg6k6+jF
95U7Zsxzh/yPmlf1q0R1CkhCy4IIYu8X9xEO0F4TvQA06iAydCRs2G8FTxLpehisTmZ/NvZUJ3C3
URHqjVctbHxORJJz5IPQCR8QeI2o3T7pAHt0UJOt8n1FEdx9UFrCgNkCmW2gc6jCescV3yQ4hD3B
ZN2hyXtGadMDP64W5ZOuEg8DppMNqrwbEOcxWASF2ae00NYvfFyL5H6RQbgU3jdVh9pWyCZjLWEM
21AtATn3+iSWCzh6qnrxUY2+ZOsUVfiXUNA1+ZCjnJPOooHBUwX09IZEqbni8OWD7RQH4Qdp9Xsz
IPReyrIBOBrIzeeagSR9T9OV4MDxgZuv0jmkg0jVsSXNx6rTj00rLkp1AqtUfI9Ts2qCaKeOaO41
W6/WWSqbimUlOGpV5mpz3+z2XKo8KWd5AS8qglS1pcJYveEJ5Su8Hw9WDUZtNlHvdK4zrM5B7ehN
yFaMNoITdZ7Ko81d7SePEgfWLl0y4pQtzZ77dM4DG/pQsW1L5w/jwgtR53yKrP0mCJDGRTygR8VB
mG4CK3WmE7s=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
